/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesstotext;

import static accesstotext.AccessToText.Properties;
import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import sun.dc.pr.PathStroker;

/**
 *
 * @author Aly
 */
public class FilesCollection {

    private String FinalPath;

    public void MoveFile(String FromFile) {
        File afile = new File(FromFile);
        File tofile = new File(FinalPath + "\\" + afile.getName());
        if (afile.renameTo(tofile)) {
            System.out.println("File is moved successful!");
        } else {
            System.out.println(FromFile);
            System.out.println(FinalPath + "\\" + afile.getName());
            System.out.println("File is failed to move!");
        }

    }

    public void CreateDirectory(String Path) {
        File files = new File(CreateDirectoryPath(Path));
        if (!files.exists()) {
            if (files.mkdirs()) {
                System.out.println("Multiple directories are created!");
            } else {
                System.out.println("Failed to create multiple directories!");
            }
        }
        FinalPath = files.getPath();
    }

    public String CreateDirectoryPath(String Path) {
        // create a calendar
        Calendar cal = Calendar.getInstance();
        // get the value of all the calendar date fields.
        Path = Path + "\\" + cal.get(Calendar.YEAR) + "\\" + (cal.get(Calendar.MONTH) + 1) + "\\" + cal.get(Calendar.DATE);
        return Path;
    }

    public static void main(String[] args) {
        File afile = new File("G:\\AccessToText\\From\\Campaign_Template.mdb");
        if (afile.renameTo(new File("G:\\AccessToText\\BackUP\\2016\\8\\2\\Campaign_Template.mdb"))) {
            System.out.println("File is moved successful!");
        }
    }
}
