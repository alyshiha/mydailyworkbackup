/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accesstotext;

/**
 *
 * @author AlyShiha
 */
import Config.PropDTO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.*;
import net.ucanaccess.jdbc.UcanaccessConnection;

public class AccessToText {

    static Connection connection;
    static Statement statement;
    static PropDTO Properties = ReadPropertiesXmlFile.readProperties();

    public static void main(String[] args) throws FileNotFoundException {

        for (File file : new File(Properties.getFromFolder()).listFiles()) {
            try {
                DriverManager.registerDriver(new net.ucanaccess.jdbc.UcanaccessDriver());
                Class.forName("net.ucanaccess.jdbc.UcanaccessDriver").newInstance();
                String database = "jdbc:ucanaccess://" + file.getAbsolutePath() + ";memory=false";
                connection = DriverManager.getConnection(database, "", "");
                buildStatement();
                executeQuery(file.getName());
                statement.close();
                connection.close();
                ((UcanaccessConnection) connection).unloadDB();
                FilesCollection FC = new FilesCollection();
                FC.CreateDirectory(Properties.getBackUpFolder());
                FC.MoveFile(file.getAbsolutePath());
            } catch (SecurityException | SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public static void buildStatement() throws SQLException {
        statement = connection.createStatement();
    }

    public static void executeQuery(String FileName) throws SQLException, FileNotFoundException {

        boolean foundResults = statement.execute("SELECT * FROM " + Properties.getTableName());
        if (foundResults) {
            ResultSet set = statement.getResultSet();
            if (set != null) {
                displayResults(set, FileName);
            }
        } else {
            connection.close();
        }
    }

    public static void displayResults(ResultSet rs, String FileName) throws SQLException, FileNotFoundException {
        ResultSetMetaData metaData = rs.getMetaData();
        int columns = metaData.getColumnCount();
        PrintWriter writer = new PrintWriter(Properties.getToFolder() + "/" + FileName + ".txt");
        while (rs.next()) {
            for (int i = 1; i <= columns; ++i) {
                writer.print(rs.getString(i) + ";");
            }
            writer.println();
        }
        writer.close();

    }

}
