/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.unstructured.controller;

import evision.unstructured.model.FilesSetter;
import evision.unstructured.model.Template;
import evision.unstructured.service.FileService;
import evision.unstructured.service.OutputWriterService;
import evision.unstructured.service.UnstructuredService;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collections;
import java.util.Map;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Aly.Shiha
 */
public class UnstructuredController {

    public File Run(Template FileTemplate, FilesSetter FileSettings) throws IOException {

        //Output Counter
        int recordNumber = 0;

        //Define Output File
        OutputWriterService.setFilePath(FileSettings.getDistination());

        //Loop inside the source folder
        for (final File file
                : new File(FileSettings.getSource()).listFiles()) {

            // load a real file
            try (Reader in = new InputStreamReader(FileUtils.openInputStream(file))) {
                //load stream and set template
                UnstructuredService ParserEngin = UnstructuredService.parse(FileTemplate, in,file);

                //loop till the end of file
                while (true) {
                    Map<String, String> record = ParserEngin.next();

                    //check for empty record as a end of file
                    if (record == null) {
                        break;
                    } else {
                        //Check if valid Transaction
                        if (!record.equals(Collections.emptyMap())) {
                            //count records
                            recordNumber++;
                            //Add Record to Result
                            OutputWriterService.printMap(record);
                        }

                    }
                }
            }
            //Backup parse file
            FileService.BackupFile(FileSettings, file);
        }
        //returns the data file as file object
        return OutputWriterService.getFilePath();
    }

}
