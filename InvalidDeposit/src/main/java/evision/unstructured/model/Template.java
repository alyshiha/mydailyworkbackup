/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.unstructured.model;

import java.util.List;

/**
 *
 * @author Aly.Shiha
 */
public class Template {

    private String Name;

    private String StartString;
    private Integer StartPositionIndex;

    private String EndString;
    private Integer EndPositionIndex;

    private String SkipHeader;
    private List<String> ResetTag;
    private List<List<FieldsMaster>> DetailTemplates;
    private List<FieldsMaster> ConstantTemplates;
    private List<FieldsMaster> ConstantTransaction;

    public Template() {
    }

    public Template(String Name, String StartString, String EndString, List<String> ResetTag, List<List<FieldsMaster>> DetailTemplates, String SkipHeader, Integer StartPositionIndex, Integer EndPositionIndex, List<FieldsMaster> ConstantTemplates, List<FieldsMaster> ConstantTransaction) {
        this.Name = Name;
        this.StartString = StartString;
        this.EndString = EndString;
        this.ResetTag = ResetTag;
        this.DetailTemplates = DetailTemplates;
        this.SkipHeader = SkipHeader;
        this.StartPositionIndex = StartPositionIndex;
        this.EndPositionIndex = EndPositionIndex;
        this.ConstantTemplates = ConstantTemplates;
        this.ConstantTransaction = ConstantTransaction;
    }

    public List<FieldsMaster> getConstantTransaction() {
        return ConstantTransaction;
    }

    public void setConstantTransaction(List<FieldsMaster> ConstantTransaction) {
        this.ConstantTransaction = ConstantTransaction;
    }

    public List<FieldsMaster> getConstantTemplates() {
        return ConstantTemplates;
    }

    public void setConstantTemplates(List<FieldsMaster> ConstantTemplates) {
        this.ConstantTemplates = ConstantTemplates;
    }

    public Integer getStartPositionIndex() {
        return StartPositionIndex;
    }

    public void setStartPositionIndex(Integer StartPositionIndex) {
        this.StartPositionIndex = StartPositionIndex;
    }

    public Integer getEndPositionIndex() {
        return EndPositionIndex;
    }

    public void setEndPositionIndex(Integer EndPositionIndex) {
        this.EndPositionIndex = EndPositionIndex;
    }

    public String getSkipHeader() {
        return SkipHeader;
    }

    public void setSkipHeader(String SkipHeader) {
        this.SkipHeader = SkipHeader;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getStartString() {
        return StartString;
    }

    public void setStartString(String StartString) {
        this.StartString = StartString;
    }

    public String getEndString() {
        return EndString;
    }

    public void setEndString(String EndString) {
        this.EndString = EndString;
    }

    public List<String> getResetTag() {
        return ResetTag;
    }

    public void setResetTag(List<String> ResetTag) {
        this.ResetTag = ResetTag;
    }

    public List<List<FieldsMaster>> getDetailTemplates() {
        return DetailTemplates;
    }

    public void setDetailTemplates(List<List<FieldsMaster>> DetailTemplates) {
        this.DetailTemplates = DetailTemplates;
    }

}
