/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.unstructured.model;

/**
 *
 * @author Aly.Shiha
 */
public class DataBaseConnection {

    private String instance;
    private String username;
    private String password;
    private String ip;
    private String port;
    private String tableName;

    public DataBaseConnection() {
    }

    public DataBaseConnection(String instance, String username, String password, String ip, String port, String tableName) {
        this.instance = instance;
        this.username = username;
        this.password = password;
        this.ip = ip;
        this.port = port;
        this.tableName = tableName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    
    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

}
