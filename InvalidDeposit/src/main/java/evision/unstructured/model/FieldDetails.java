/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.unstructured.model;

/**
 *
 * @author Aly.Shiha
 */
public class FieldDetails {

    private String SubStrings;
    private Integer SubStringsIndex;
    private Integer Line;
    private Integer BeginIndex;
    private Integer endIndex;
    private String DataType;
    private String Format;
    private String SpecialCharacter;

    public FieldDetails() {
    }

    public FieldDetails( String SubStrings, Integer Line, Integer BeginIndex, Integer endIndex, String DataType, String Format, String SpecialCharacter, Integer SubStringsIndex) {
        this.SubStrings = SubStrings;
        this.Line = Line;
        this.BeginIndex = BeginIndex;
        this.endIndex = endIndex;
        this.DataType = DataType;
        this.Format = Format;
        this.SpecialCharacter = SpecialCharacter;
        this.SubStringsIndex = SubStringsIndex;
    }

    public Integer getSubStringsIndex() {
        return SubStringsIndex;
    }

    public void setSubStringsIndex(Integer SubStringsIndex) {
        this.SubStringsIndex = SubStringsIndex;
    }

    public String getDataType() {
        return DataType;
    }

    public void setDataType(String DataType) {
        this.DataType = DataType;
    }

    public String getFormat() {
        return Format;
    }

    public void setFormat(String Format) {
        this.Format = Format;
    }

    public String getSpecialCharacter() {
        return SpecialCharacter;
    }

    public void setSpecialCharacter(String SpecialCharacter) {
        this.SpecialCharacter = SpecialCharacter;
    }

    public String getSubStrings() {
        return SubStrings;
    }

    public void setSubStrings(String SubStrings) {
        this.SubStrings = SubStrings;
    }

    public Integer getLine() {
        return Line;
    }

    public void setLine(Integer Line) {
        this.Line = Line;
    }

    public Integer getBeginIndex() {
        return BeginIndex;
    }

    public void setBeginIndex(Integer BeginIndex) {
        this.BeginIndex = BeginIndex;
    }

    public Integer getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(Integer endIndex) {
        this.endIndex = endIndex;
    }

}
