/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.unstructured.model;

import java.util.Date;

/**
 * @since 1/4/2017
 * @author Ali Shiha
 *
 * Model for the folder output definition
 */
public class OutputDefinition {

    private String runningfile;
    private String filetype;
    private Date parsingtime;
    private String extension;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getRunningfile() {
        return runningfile;
    }

    public void setRunningfile(String runningfile) {
        this.runningfile = runningfile;
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public Date getParsingtime() {
        return parsingtime;
    }

    public void setParsingtime(Date parsingtime) {
        this.parsingtime = parsingtime;
    }
}
