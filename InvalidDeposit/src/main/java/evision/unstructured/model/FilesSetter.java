/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.unstructured.model;

/**
 *
 * @author Aly.Shiha
 */
public class FilesSetter {

    private String Source;
    private String Distination;
    private String Template;
    private String Backup;

    public FilesSetter() {
    }

    public FilesSetter(String Source, String Distination, String Template, String Backup) {
        this.Source = Source;
        this.Distination = Distination;
        this.Template = Template;
        this.Backup = Backup;
    }

    public String getBackup() {
        return Backup;
    }

    public void setBackup(String Backup) {
        this.Backup = Backup;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String Source) {
        this.Source = Source;
    }

    public String getDistination() {
        return Distination;
    }

    public void setDistination(String Distination) {
        this.Distination = Distination;
    }

    public String getTemplate() {
        return Template;
    }

    public void setTemplate(String Template) {
        this.Template = Template;
    }

}
