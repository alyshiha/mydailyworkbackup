/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.unstructured.runner;

import evision.unstructured.controller.UnstructuredController;
import evision.unstructured.model.DataBaseConnection;
import evision.unstructured.model.FilesSetter;
import evision.unstructured.model.Template;
import evision.unstructured.service.SQLLoaderService;
import evision.unstructured.util.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aly.Shiha
 */
public class Runner {

    public static void main(String[] args) {
        try {
            //Load config Files
            FilesSetter FileSettings = (FilesSetter) XStreamTranslator.getInstance().toObject(new File("./ConfigFiles/File.xml"));

            //Load Template Detail
            Template FileTemplate = (Template) XStreamTranslator.getInstance().toObject(new File(FileSettings.getTemplate()));

            //create new instance from the controller
            UnstructuredController unstructuredcontroller = new UnstructuredController();
            File OutputFile = unstructuredcontroller.Run(FileTemplate, FileSettings);

            //check if file contain data
            //call sql loader to load data to database
            if (OutputFile.exists()) {
                //load database configurations
                DataBaseConnection DBconnection = (DataBaseConnection) XStreamTranslator.getInstance().toObject(new File("./ConfigFiles/DataBase.xml"));
                SQLLoaderService.Run(OutputFile, DBconnection);
            }

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (InstantiationException ex) {
            System.out.println(ex.getMessage());
        } catch (IllegalAccessException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
