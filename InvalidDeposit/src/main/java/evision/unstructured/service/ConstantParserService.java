/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.unstructured.service;

import evision.unstructured.model.FieldDetails;
import evision.unstructured.model.FieldsMaster;
import evision.unstructured.util.ValidateData;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Aly.Shiha
 */
public class ConstantParserService {

    public static Map<String, String> transactionConstant(String Line, List<FieldsMaster> ConstantTransaction, Map<String, String> constantTransactions) {
        Map<String, String> constant = constantTransactions;
        for (FieldsMaster FieldData : ConstantTransaction) {
            if (!constantTransactions.containsKey(FieldData.getHeader())) {
                Boolean FieldDataFound = Boolean.FALSE;
                //loop on each field detail
                for (FieldDetails Details : FieldData.getDetails()) {
                    if (!FieldDataFound) {
                        //if line contain the given substring
                        int FoundIndex = Line.toLowerCase().indexOf(Details.getSubStrings().toLowerCase());
                        //get specific location occourding to template
                        if ((FoundIndex == Details.getSubStringsIndex() && Details.getSubStringsIndex() != -1)
                                || (FoundIndex > -1 && Details.getSubStringsIndex() == -1)) {
                            String Value = Line.substring(Details.getBeginIndex(), Details.getEndIndex());
                            //Check If Data Match It`s Stracture
                            if (ValidateData.Validate(Details.getDataType(), Value.trim(), Details.getFormat(), Details.getSpecialCharacter())) {
                                FieldDataFound = Boolean.TRUE;
                                constant.put(FieldData.getHeader(), Value.trim());
                            }
                        }
                    }
                }
            }
        }
        return constant;
    }

    public static Map<String, String> Parse(BufferedReader reader, List<FieldsMaster> ConstantTemplates) throws IOException {
        Map<String, String> FinalStucture = new HashMap<String, String>();
        String Line;
        //get field template
        for (FieldsMaster FieldData : ConstantTemplates) {
            Boolean FieldDataFound = Boolean.FALSE;
            FinalStucture.put(FieldData.getHeader(), "");
            //loop on each field detail
            for (FieldDetails Details : FieldData.getDetails()) {
                //stop if fond with right format
                if (!FieldDataFound) {
                    //loop in tranaction lines
                    while ((Line = reader.readLine()) != null) {
                        //if line contain the given substring
                        int FoundIndex = Line.toLowerCase().indexOf(Details.getSubStrings().toLowerCase());
                        //get specific location occourding to template
                        if ((FoundIndex == Details.getSubStringsIndex() && Details.getSubStringsIndex() != -1)
                                || (FoundIndex > -1 && Details.getSubStringsIndex() == -1)) {
                            String Value = Line.substring(Details.getBeginIndex(), Details.getEndIndex());
                            //Check If Data Match It`s Stracture
                            if (ValidateData.Validate(Details.getDataType(), Value, Details.getFormat(), Details.getSpecialCharacter())) {
                                FieldDataFound = Boolean.TRUE;
                                FinalStucture.put(FieldData.getHeader(), Value.trim());
                            }
                        }
                    }
                }
            }
        }
        //Check Mandatory Field from template
        if (CheckMandatory(FinalStucture, ConstantTemplates)) {
            return FinalStucture;
        }

        //no valid transaction return empty transaction
        return Collections.emptyMap();
    }

    private static Boolean CheckMandatory(Map<String, String> TransactionMap, List<FieldsMaster> ConstantTemplates) {
        Boolean State = Boolean.TRUE;
        for (FieldsMaster Details : ConstantTemplates) {
            if (TransactionMap.get(Details.getHeader()).trim().equals("") && Details.getMandatory().toLowerCase().equals("true")) {
                State = Boolean.FALSE;
                break;
            }
        }
        return State;
    }
}
