/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.unstructured.service;

import evision.unstructured.model.FilesSetter;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.RandomStringUtils;

/**
 *
 * @author Aly.Shiha
 */
public class OutputWriterService {

    private static File FilePath;

    public static void setFilePath(String FilePath) {
        //Create Folder And Random Data File
        OutputWriterService.FilePath = new File(String.format("%s\\%s\\%s.%s", FilePath, System.currentTimeMillis(), RandomStringUtils.randomAlphanumeric(8), "Dat"));
    }

    public static File getFilePath() {
        return FilePath;
    }

    private static void createFolder() {
        //Create Parent Folder If Not Exists
        if (!FilePath.exists()) {
            FilePath.getParentFile().mkdirs();
        }
    }

    private static void WriteData(StringBuilder Structured) throws IOException {
        createFolder();
        //Write Data To File
        FileUtils.writeStringToFile(FilePath, Structured.toString(), true);
    }

    //pretty print a map
    public static <K, V> void printMap(Map<K, V> map) throws IOException {
        StringBuilder Structured = new StringBuilder();
        //Sort transaction according to field name
        Map<String, String> SortedMap = new TreeMap<String, String>((Map<? extends String, ? extends String>) map);
        for (Map.Entry<String, String> entry : SortedMap.entrySet()) {
            //create transaction string 
            Structured.append(entry.getValue()).append(";");
        }
        //new line as transaction end
        Structured.append(System.lineSeparator());
        //export data to distination file
        WriteData(Structured);
    }


}
