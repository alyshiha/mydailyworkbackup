/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.unstructured.service;

import evision.unstructured.model.FilesSetter;
import java.io.File;

/**
 *
 * @author Aly.Shiha
 */
public class FileService {

    public static void BackupFile(FilesSetter FileSettings, File file) {
        //Create Backup Folder If not excist
        new File(FileSettings.getBackup()).mkdir();
        //move parse file to back directory
        boolean s = file.renameTo(new File(FileSettings.getBackup(), file.getName()));
    }
}
