/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.unstructured.service;

import evision.unstructured.model.FieldDetails;
import evision.unstructured.model.FieldsMaster;
import evision.unstructured.model.Template;
import evision.unstructured.util.ValidateData;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author Aly.Shiha
 */
public class RecordParserService {

    private Hashtable Transaction;
    private Template FileTemplate;
    private BufferedReader in;
    private Map<String, String> constantTransactions = new HashMap<String, String>();

    public Map<String, String> getConstantTransactions() {
        return constantTransactions;
    }

    public void setConstantTransactions(Map<String, String> constantTransactions) {
        this.constantTransactions = constantTransactions;
    }

    public RecordParserService(Template FileTemplate, BufferedReader in) {
        this.FileTemplate = FileTemplate;
        this.in = in;
    }

    public Map<String, String> getNextRecord() throws IOException {
        //retreive next transaction data from file
        Transaction = getNextTranaction();
        //if no trasaction file return empty map 
        if (TranactionFound(Transaction)) {
            //extract field from given transaction
            Map<String, String> FinalStucture = MapFields();
            if (!FinalStucture.equals(Collections.emptyMap())) {
                FinalStucture.putAll(constantTransactions);
            }
            return FinalStucture;
        } else {
            constantTransactions = null;
            Transaction = null;
            FileTemplate = null;
            in = null;
            return null;
        }
    }

    private Boolean TranactionFound(Hashtable TransactionData) {
        return !TransactionData.isEmpty();
    }

    private Hashtable getNextTranaction() throws IOException {
        // create new variable to store tranaction in memory
        Hashtable TransactionData = new Hashtable();
        constantTransactions.clear();
        //line key
        int recordNumber = 1;
        String Line;
        // Loop through each line, stashing the line into our line variable.
        while ((Line = in.readLine()) != null) {

            //get costant values
            constantTransactions = ConstantParserService.transactionConstant(Line, FileTemplate.getConstantTransaction(),constantTransactions);

            //get the starting index of the transaction start if -1 it mean contain in any index
            int StartIndex = Line.toLowerCase().indexOf(FileTemplate.getStartString().toLowerCase());
            if ((StartIndex == FileTemplate.getStartPositionIndex() && FileTemplate.getStartPositionIndex() != -1)
                    || (StartIndex > -1 && FileTemplate.getStartPositionIndex() == -1)) {
                TransactionData.clear();
                TransactionData.put(recordNumber++, Line);
            }

            //get the ending index of the transaction  if -1 it mean contain in any index
            int EndIndex = Line.toLowerCase().indexOf(FileTemplate.getEndString().toLowerCase());
            if ((EndIndex == FileTemplate.getEndPositionIndex() && FileTemplate.getEndPositionIndex() != -1)
                    || (EndIndex > -1 && FileTemplate.getEndPositionIndex() == -1)) {
                break;
            } else {
                if (ContainResetTag(Line)) {
                    //reset the transaction if found a reset tag
                    TransactionData.clear();
                }
                TransactionData.put(recordNumber++, Line);

            }

        }
        return TransactionData;
    }

    private Boolean ContainResetTag(String LineFound) {
        //check if line contain a defined reset tag
        return FileTemplate.getResetTag().stream().filter(k -> LineFound.contains(k)).collect(Collectors.toList()).size() > 0;
    }

    private Map<String, String> MapFields() {
        Map<String, String> FinalStucture = new HashMap<String, String>();
        //Loop inside the field template
        for (List<FieldsMaster> FieldsData : FileTemplate.getDetailTemplates()) {
            //get field template
            for (FieldsMaster FieldData : FieldsData) {
                Boolean FieldDataFound = Boolean.FALSE;
                FinalStucture.put(FieldData.getHeader(), "");
                //loop on each field detail
                for (FieldDetails Details : FieldData.getDetails()) {
                    //stop if fond with right format
                    if (!FieldDataFound) {
                        //loop in tranaction lines
                        for (Integer Line : ((Set<Integer>) Transaction.keySet())) {
                            //if line contain the given substring
                            int FoundIndex = Transaction.get(Line).toString().toLowerCase().indexOf(Details.getSubStrings().toLowerCase());
                            //get specific location occourding to template
                            if ((FoundIndex == Details.getSubStringsIndex() && Details.getSubStringsIndex() != -1)
                                    || (FoundIndex > -1 && Details.getSubStringsIndex() == -1)) {
                                String Value = Transaction.get(Line + Details.getLine()).toString().substring(Details.getBeginIndex(), Details.getEndIndex());
                                //Check If Data Match It`s Stracture
                                if (ValidateData.Validate(Details.getDataType(), Value, Details.getFormat(), Details.getSpecialCharacter())) {
                                    FieldDataFound = Boolean.TRUE;
                                    FinalStucture.put(FieldData.getHeader(), Value.trim());
                                }
                            }
                        }
                    }
                }
            }
            //Check Mandatory Field from template
            if (CheckMandatory(FinalStucture)) {
                return FinalStucture;
            }
        }
        //no valid transaction return empty transaction
        return Collections.emptyMap();
    }

    private Boolean CheckMandatory(Map<String, String> TransactionMap) {
        Boolean State = Boolean.TRUE;
        for (List<FieldsMaster> FieldsData : FileTemplate.getDetailTemplates()) {
            for (FieldsMaster Details : FieldsData) {
                if (TransactionMap.get(Details.getHeader()).trim().equals("") && Details.getMandatory().toLowerCase().equals("true")) {
                    State = Boolean.FALSE;
                    break;
                }
            }
        }
        return State;
    }
}
