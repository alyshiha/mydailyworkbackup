/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.unstructured.service;

import evision.unstructured.model.Template;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Aly.Shiha
 */
public class UnstructuredService {

    private final Template FileTemplate;
    private final BufferedReader reader;
    private final Map<String, String> HeaderRecord;
    private final File RunningFile;
    private RecordParserService ParserEngin;

    public static UnstructuredService parse(Template FileTemplate, Reader in, File file) throws IOException {
        return new UnstructuredService(FileTemplate, in, file);
    }

    /**
     * Build the parser
     *
     * @param config the config
     * @param in the input stream, the client is responsible for closing this
     */
    private UnstructuredService(Template FileTemplate, Reader in, File file) throws IOException {
        this.FileTemplate = FileTemplate;
        this.reader = new BufferedReader(in);
        this.RunningFile = file;
        this.ParserEngin = new RecordParserService(FileTemplate, reader);

        HeaderRecord = new TreeMap<String, String>();
        if ("False".equals(FileTemplate.getSkipHeader())) {
            //define file readers
            FileReader headerstreamreader = new FileReader(RunningFile.getPath());
            BufferedReader headerreader = new BufferedReader(headerstreamreader);
            //get header data
            Map<String, String> Header = ConstantParserService.Parse(headerreader, FileTemplate.getConstantTemplates());
            //add values to header
            HeaderRecord.putAll(Header);
            //close readers
            headerstreamreader.close();
            headerreader.close();

        }
    }

    public Map<String, String> next() throws IOException {
        Map<String, String> Record = ParserEngin.getNextRecord();
        if (!HeaderRecord.isEmpty() && Record != null && Record.size() > 0) {
            Record.putAll(HeaderRecord);
        }
        return Record;
    }

}
