/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBCONN;

import com.ev.AtmBingo.bus.bo.AtmStatByCashRepBOInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.DisputesBOInter;
import com.ev.AtmBingo.bus.bo.NotificationBOInter;
import com.ev.AtmBingo.bus.bo.PendingAtmsBOInter;
import com.ev.AtmBingo.bus.bo.PrivilageBOInter;
import com.ev.AtmBingo.bus.bo.RejectedBOInter;
import com.ev.AtmBingo.bus.bo.ReplanishmentDetailsBOInter;
import com.ev.AtmBingo.bus.bo.ReplanishmentReportBOInter;
import com.ev.AtmBingo.bus.bo.ResponseCodesDefinitionBOInter;
import com.ev.AtmBingo.bus.bo.RevenueRepBOInter;
import com.ev.AtmBingo.bus.dao.ColumnDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.LicenseDAOInter;
import com.ev.AtmBingo.bus.dao.SystemTableDAOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.LicenseDTOInter;
import com.ev.AtmBingo.bus.dto.MatchingTypeDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkGroupDTOInter;
import com.ev.AtmBingo.bus.dto.NotificationDTOInter;
import com.ev.AtmBingo.bus.dto.PendingAtmsDTOInter;
import com.ev.AtmBingo.bus.dto.SystemTableDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseMasterDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.ValidationRulesDTOInter;
import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author AlyShiha
 */
public class Session implements Serializable {

    private synchronized void getmemo() {
        Runtime runtime = Runtime.getRuntime();

        NumberFormat format = NumberFormat.getInstance();

        long maxMemory = runtime.maxMemory();
        long allocatedMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();

        System.out.println("free memory: " + format.format(freeMemory / 1024));
        System.out.println("allocated memory: " + format.format(allocatedMemory / 1024));
        System.out.println("max memory: " + format.format(maxMemory / 1024));
        System.out.println("total free memory: " + format.format((freeMemory + (maxMemory - allocatedMemory))));

    }

    public synchronized void GenerateSession(UsersDTOInter logedinUser) {
        try {
            System.out.println("Before");
            getmemo();
            GetNotificationsList();
            GetErrorsList();
            GetPendingAtmsList();
            GetTimeFrom();
            GetTimeTo();
            GetNetworkGroupList();
            GetATMGroupList();
            GetUsersList();
            GetCassetteList();
            GetColumnsList();
            GetCurrencyMasterList();
            GetMatchingTypeList();
            GetTransactionResponseCodeList();
            GetSettledUsersList();
            GetTransactionTypeList();
            GetTransactionStatusList();
            GetValidationRulesList();
            GetTransTime();
            GetDefaultColumn();
            GetNonDefaultColumn();
            GetAtmMachineList(logedinUser);
            SetUserPages(logedinUser);
            GetATMGroupAssignList(logedinUser.getUserId());
            GetTransactionResponseMasterList();
            GetLicenseSessions();
            SetIP();
            System.out.println("After");
            getmemo();
            
        } catch (Throwable ex) {
            Logger.getLogger(Session.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public synchronized List<String> GetUserPages() throws Exception, Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        return (List<String>) session.getAttribute("UserPages");
    }

    public synchronized void SetUserPages(UsersDTOInter user) throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        PrivilageBOInter profileBO = BOFactory.createPrivilageBO(null);
        session.setAttribute("UserPages", profileBO.findAllPage(user.getUserId()));
    }

    public synchronized void SetUserLogging(UsersDTOInter user) {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.setAttribute("UserLogging", user);
    }

    public synchronized void SetIP() throws Throwable {
        String ip = "";
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if (req.getHeader("x-forwarded-for") == null) {
            ip = req.getRemoteAddr();
        } else {
            ip = req.getHeader("x-forwarded-for");
        }
        session.setAttribute("ip", ip);
    }

    public  synchronized void GetLicenseSessions() throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        LicenseDAOInter lDAO = DAOFactory.createLicenseDAO();
        LicenseDTOInter LDTO = (LicenseDTOInter) lDAO.findLicense();
        if (session.getAttribute("client") == null) {
            session.setAttribute("client", LDTO.getLicenseTo() + " - " + LDTO.getCountry());
        }
        if (session.getAttribute("version") == null) {
            session.setAttribute("version", LDTO.getVersion());
        }
        if (session.getAttribute("UserCount") == null) {
            session.setAttribute("UserCount", LDTO.getNoOfUser());
        }
        if (session.getAttribute("EndDate") == null) {
            session.setAttribute("EndDate", LDTO.getEndDate());
        }

    }

    public synchronized String Getclient() throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        LicenseDAOInter lDAO = DAOFactory.createLicenseDAO();
        LicenseDTOInter LDTO = (LicenseDTOInter) lDAO.findLicense();
        if (session.getAttribute("client") == null) {
            session.setAttribute("client", LDTO.getLicenseTo() + " - " + LDTO.getCountry());
        }
        return (String) session.getAttribute("client");
    }

    public synchronized UsersDTOInter GetUserLogging() throws Exception, Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        return (UsersDTOInter) session.getAttribute("UserLogging");
    }

    public synchronized List<TransactionResponseMasterDTOInter> GetTransactionResponseMasterList() throws Throwable {
        ResponseCodesDefinitionBOInter cdObject = BOFactory.createResponseCodeDefBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("TransactionResponseMasterList") == null) {
            session.setAttribute("TransactionResponseMasterList", (List<TransactionResponseMasterDTOInter>) cdObject.getCurrencyMaster());
        }
        return (List<TransactionResponseMasterDTOInter>) session.getAttribute("TransactionResponseMasterList");
    }

    public synchronized List<AtmMachineDTOInter> GetAtmMachineList(UsersDTOInter user) throws Throwable {
        DisputesBOInter mcObject = BOFactory.createDisputeBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("AtmMachineList") == null) {
            session.setAttribute("AtmMachineList", (List<AtmMachineDTOInter>) mcObject.getAtmMachines(user, 0));
        }
        return (List<AtmMachineDTOInter>) session.getAttribute("AtmMachineList");
    }

    public synchronized List<ColumnDTOInter> GetDefaultColumn() throws Throwable {
        ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("DefaultColumn") == null) {
            session.setAttribute("DefaultColumn", (List<ColumnDTOInter>) cDAO.findDefualt());
        }
        return (List<ColumnDTOInter>) session.getAttribute("DefaultColumn");
    }

    public synchronized List<ColumnDTOInter> GetNonDefaultColumn() throws Throwable {
        ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("NonDefaultColumn") == null) {
            session.setAttribute("NonDefaultColumn", (List<ColumnDTOInter>) cDAO.findNotDefualt());
        }
        return (List<ColumnDTOInter>) session.getAttribute("NonDefaultColumn");
    }

    public synchronized List<NotificationDTOInter> GetNotificationsList() throws Throwable {
        NotificationBOInter NotificationBO;
        NotificationBO = BOFactory.createNotificationBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("NotificationsList") == null) {
            session.setAttribute("NotificationsList", (List<NotificationDTOInter>) NotificationBO.getNotifications());
        }
        return (List<NotificationDTOInter>) session.getAttribute("NotificationsList");
    }

    public synchronized List<NotificationDTOInter> GetErrorsList() throws Throwable {
        NotificationBOInter NotificationBO;
        NotificationBO = BOFactory.createNotificationBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("ErrorsList") == null) {
            session.setAttribute("ErrorsList", (List<NotificationDTOInter>) NotificationBO.getErrors());
        }
        return (List<NotificationDTOInter>) session.getAttribute("ErrorsList");
    }

    public synchronized List<PendingAtmsDTOInter> GetPendingAtmsList() throws Throwable {
        PendingAtmsBOInter PendingAtmsBO;
        PendingAtmsBO = BOFactory.createPendingAtmsBo(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("PendingAtmsList") == null) {
            session.setAttribute("PendingAtmsList", (List<PendingAtmsDTOInter>) PendingAtmsBO.getPendingAtms());
        }
        return (List<PendingAtmsDTOInter>) session.getAttribute("PendingAtmsList");
    }

    public synchronized String GetTimeFrom() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("DateFromVariable") == null) {
            session.setAttribute("DateFromVariable", (String) getParameter("TIMETO"));
        }
        return (String) session.getAttribute("DateFromVariable");
    }

    public synchronized String GetTimeTo() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("DateToVariable") == null) {
            session.setAttribute("DateToVariable", (String) getParameter("TIMEFROM"));
        }
        return (String) session.getAttribute("DateToVariable");
    }

    public synchronized List<NetworkGroupDTOInter> GetNetworkGroupList() throws Throwable {
        RevenueRepBOInter revenueBO;
        revenueBO = BOFactory.createRevenueBOInter(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("NetworkGroupList") == null) {
            session.setAttribute("NetworkGroupList", (List<NetworkGroupDTOInter>) revenueBO.findNetworkGroup());
        }
        return (List<NetworkGroupDTOInter>) session.getAttribute("NetworkGroupList");
    }

    public synchronized List<AtmGroupDTOInter> GetATMGroupAssignList(Integer UserID) throws Throwable {
        AtmStatByCashRepBOInter atmByCashRep;
        atmByCashRep = BOFactory.createAtmStatByCashRepBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("ATMGroupAssignList") == null) {
            session.setAttribute("ATMGroupAssignList", (List<AtmGroupDTOInter>) atmByCashRep.getGrpassignuser(UserID));
        }
        return (List<AtmGroupDTOInter>) session.getAttribute("ATMGroupAssignList");
    }

    public synchronized List<AtmGroupDTOInter> GetATMGroupList() throws Throwable {
        AtmStatByCashRepBOInter atmByCashRep;
        atmByCashRep = BOFactory.createAtmStatByCashRepBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("ATMGroupList") == null) {
            session.setAttribute("ATMGroupList", (List<AtmGroupDTOInter>) atmByCashRep.getGrp());
        }
        return (List<AtmGroupDTOInter>) session.getAttribute("ATMGroupList");
    }

    public synchronized List<UsersDTOInter> GetUsersList() throws Throwable {
        ReplanishmentReportBOInter repDetailsReport;
        repDetailsReport = BOFactory.createReplanishmentReportBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("UsersList") == null) {
            session.setAttribute("UsersList", (List<UsersDTOInter>) repDetailsReport.getUsers());
        }
        return (List<UsersDTOInter>) session.getAttribute("UsersList");
    }

    public synchronized List<CassetteDTOInter> GetCassetteList() throws Throwable {
        ReplanishmentDetailsBOInter cdObject;
        cdObject = BOFactory.createReplanishmentBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("CassettesList") == null) {
            session.setAttribute("CassettesList", (List<CassetteDTOInter>) cdObject.getCassetes());
        }
        return (List<CassetteDTOInter>) session.getAttribute("CassettesList");
    }

    public synchronized List<ColumnDTOInter> GetColumnsList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("ColumnsList") == null) {
            session.setAttribute("ColumnsList", (List<ColumnDTOInter>) mcObject.getFileColumnDefinition());
        }
        return (List<ColumnDTOInter>) session.getAttribute("ColumnsList");
    }

    public synchronized List<CurrencyMasterDTOInter> GetCurrencyMasterList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("CurrencyMasterList") == null) {
            session.setAttribute("CurrencyMasterList", (List<CurrencyMasterDTOInter>) mcObject.getCurrencyMaster());
        }
        return (List<CurrencyMasterDTOInter>) session.getAttribute("CurrencyMasterList");
    }

    public synchronized List<MatchingTypeDTOInter> GetMatchingTypeList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("MatchingTypeList") == null) {
            session.setAttribute("MatchingTypeList", (List<MatchingTypeDTOInter>) mcObject.getMatchingType());
        }
        return (List<MatchingTypeDTOInter>) session.getAttribute("MatchingTypeList");
    }

    public synchronized List<TransactionResponseCodeDTOInter> GetTransactionResponseCodeList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("TransactionResponseCodeList") == null) {
            session.setAttribute("TransactionResponseCodeList", (List<TransactionResponseCodeDTOInter>) mcObject.getTransactionResponseCode());
        }
        return (List<TransactionResponseCodeDTOInter>) session.getAttribute("TransactionResponseCodeList");
    }

    public synchronized List<UsersDTOInter> GetSettledUsersList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("SettledUsersList") == null) {
            session.setAttribute("SettledUsersList", (List<UsersDTOInter>) mcObject.getSettledUsers());
        }
        return (List<UsersDTOInter>) session.getAttribute("SettledUsersList");
    }

    public synchronized List<TransactionTypeDTOInter> GetTransactionTypeList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("TransactionTypeList") == null) {
            session.setAttribute("TransactionTypeList", (List<TransactionTypeDTOInter>) mcObject.getTransactionType());
        }
        return (List<TransactionTypeDTOInter>) session.getAttribute("TransactionTypeList");
    }

    public synchronized List<TransactionStatusDTOInter> GetTransactionStatusList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("TransactionStatusList") == null) {
            session.setAttribute("TransactionStatusList", (List<TransactionStatusDTOInter>) mcObject.getTransactionStatus());
        }
        return (List<TransactionStatusDTOInter>) session.getAttribute("TransactionStatusList");
    }

    public synchronized List<ValidationRulesDTOInter> GetValidationRulesList() throws Throwable {
        RejectedBOInter mcObject;
        mcObject = BOFactory.createRejectedBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("ValidationRulesList") == null) {
            session.setAttribute("ValidationRulesList", (List<ValidationRulesDTOInter>) mcObject.getValidationRules(1));
        }
        return (List<ValidationRulesDTOInter>) session.getAttribute("ValidationRulesList");
    }

    public synchronized SystemTableDTOInter GetTransTime() throws Throwable {
        SystemTableDAOInter stDAO = DAOFactory.createSystemTableDAO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("TRANSACTIONTIME") == null) {
            session.setAttribute("TRANSACTIONTIME", (SystemTableDTOInter) stDAO.find("TRANSACTION TIME"));
        }
        return (SystemTableDTOInter) session.getAttribute("TRANSACTIONTIME");
    }

    public synchronized Object getParameter(String paramName) {
        SystemTableDAOInter stDAO = DAOFactory.createSystemTableDAO(null);
        SystemTableDTOInter stDTO = null;
        try {
            stDTO = (SystemTableDTOInter) stDAO.find(paramName);
            return stDTO.getParameterValue();
        } catch (Throwable ex) {
            return null;
        }
    }

    public synchronized void clearsessionattributes() throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session != null) {
            Enumeration attributeNames = session.getAttributeNames();
            while (attributeNames.hasMoreElements()) {
                String sAttribute = attributeNames.nextElement().toString();
                session.removeAttribute(sAttribute);
            }
        }
    }
}
