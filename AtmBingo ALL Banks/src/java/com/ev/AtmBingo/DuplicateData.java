/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo;

import com.ev.AtmBingo.base.dao.BaseDAO;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.Serializable;

/**
 *
 * @author Aly
 */
public class DuplicateData extends BaseDAO  implements Serializable{

    public DuplicateData() {
        super();
        try {
            runMatchedData();

        } catch (Throwable ex) {
            Logger.getLogger(DuplicateData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void runMatchedData() throws Throwable {
        for (int i = 58; i < 1000; i++) {
            Generate(i + 1, i);
        }
    }

    public void Generate(int from, int to) throws Throwable {
        super.preUpdate();
        String deleteStat = "insert into matched_data\n"
                + "  (file_id, loading_date, atm_application_id, atm_id, transaction_type, transaction_type_id, currency, currency_id, transaction_status, transaction_status_id, response_code, response_code_id, transaction_date, transaction_sequence, card_no, amount, settlement_date, notes_presented, customer_account_number, transaction_sequence_order_by, transaction_time, matching_type, record_type, match_key, settled_flag, settled_date, settled_user, comments, match_by, match_date, column1, column2, column3, column4, column5, transaction_type_master, response_code_master, card_no_suffix, abs_amount)\n"
                + "select file_id, loading_date, atm_application_id, atm_id, transaction_type, transaction_type_id, currency, currency_id, transaction_status, transaction_status_id, response_code, response_code_id, transaction_date, $index || transaction_sequence, card_no, amount, settlement_date, notes_presented, customer_account_number, transaction_sequence_order_by, transaction_time, matching_type, record_type, match_key, settled_flag, settled_date, settled_user, comments, match_by, match_date, column1, column2, column3, column4, column5, transaction_type_master, response_code_master, card_no_suffix, abs_amount "
                + "from matched_data  where transaction_date  between sysdate - $from and sysdate - $to";
        deleteStat = deleteStat.replace("$index", "" + from);
        deleteStat = deleteStat.replace("$from", "" + from);
        deleteStat = deleteStat.replace("$to", "" + to);
        super.executeUpdate(deleteStat);
    }
}
