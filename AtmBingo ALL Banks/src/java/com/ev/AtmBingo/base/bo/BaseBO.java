/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.base.bo;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dao.AtmGroupDAOInter;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class BaseBO implements Serializable {

    public Object getAtmGrp() throws Throwable {
        AtmGroupDAOInter agDAo = DAOFactory.createAtmGroupDAO(null);
        List<AtmGroupDTOInter> agDTOL = (List<AtmGroupDTOInter>) agDAo.findAll();
        return agDTOL;
    }

    public Object getAtmGrpasignuser(Integer userId) throws Throwable {
        AtmGroupDAOInter agDAo = DAOFactory.createAtmGroupDAO(null);
        List<AtmGroupDTOInter> agDTOL = (List<AtmGroupDTOInter>) agDAo.findAllaasignuser(userId);
        return agDTOL;
    }

    public String getGrpName(int grpId) throws Throwable {
        AtmGroupDAOInter agDAo = DAOFactory.createAtmGroupDAO(null);
        AtmGroupDTOInter agDTO = (AtmGroupDTOInter) agDAo.find(grpId);
        return agDTO.getName();
    }

    public String getAtmName(int atmId) throws Throwable {
        AtmMachineDAOInter agDAo = DAOFactory.createAtmMachineDAO(null);
        AtmMachineDTOInter agDTO = (AtmMachineDTOInter) agDAo.find(atmId);
        return agDTO.getName();
    }

    public String getcardcolumn() {
        return "decrypt(card_no)";
    }
    public String getencryptcard() {
        return "?";
    }

    public Date getTime() throws Throwable {
        BaseDAO bDAO = new BaseDAO();
        Date now = bDAO.getCurrentTime();
        return now;
    }
}
