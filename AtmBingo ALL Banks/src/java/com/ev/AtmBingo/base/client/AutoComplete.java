/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.base.client;

import DBCONN.Session;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.ReplanishmentDetailsBOInter;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class AutoComplete implements Serializable {

    private List<AtmMachineDTOInter> atmList;
    private ReplanishmentDetailsBOInter cdObject;

    public List<AtmMachineDTOInter> getAtmList() {
        return atmList;
    }

    public void setAtmList(List<AtmMachineDTOInter> atmList) {
        this.atmList = atmList;
    }

    public int findByAtmId(String ATMID) throws Throwable {
        AtmMachineDAOInter ATMNAME = DAOFactory.createAtmMachineDAO(null);
        AtmMachineDTOInter atm = (AtmMachineDTOInter) ATMNAME.findByAtmId(ATMID);
        return atm.getId();
    }
    private Session utillist = new Session();

    public AutoComplete() {
        try {

            Session sessionutil = new Session();
            UsersDTOInter userHolded = sessionutil.GetUserLogging();
            cdObject = BOFactory.createReplanishmentBO(null);
            atmList = (List<AtmMachineDTOInter>) utillist.GetAtmMachineList(userHolded);
        } catch (Throwable ex) {
            Logger.getLogger(AutoComplete.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<String> complete(String query) {
        List<String> results = new ArrayList<String>();
        for (int i = 0; i < atmList.size(); i++) {
            if (atmList.get(i).getApplicationId().toString().startsWith(query)) {
                results.add(atmList.get(i).getApplicationId().toString());
            }
        }
        return results;
    }
}
