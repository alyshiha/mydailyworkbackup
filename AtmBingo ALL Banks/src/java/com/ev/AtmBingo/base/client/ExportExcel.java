/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.base.client;

// File Name SendEmail.java
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.CashManagmentLogDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.AccountNameDTOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DisputesDTOInter;
import com.ev.AtmBingo.bus.dto.RepStat13DTOInter;
import com.ev.AtmBingo.bus.dto.RepStat2DTOInter;
import com.ev.AtmBingo.bus.dto.RepStat4DTOInter;
import com.ev.AtmBingo.bus.dto.RepStat56DTOInter;
import com.ev.AtmBingo.bus.dto.ReportStatDetailDTO;
import com.ev.AtmBingo.bus.dto.ReportStatDetailDTOInter;
import com.ev.AtmBingo.bus.dto.ReportTransDTOInter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.*;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class ExportExcel extends BaseDAO implements Serializable {

    public String exportexcel(List<ColumnDTOInter> coloumns, DisputesDTOInter[] SearchRecords) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Sample sheet");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        Object[] coloumnslist = new Object[coloumns.size()];
        int index = 0;
        for (ColumnDTOInter coloumn : coloumns) {
            coloumnslist[index] = coloumn.getHeader();
            sheet.setDefaultColumnWidth(20);
            index = index + 1;
        }
        data.put(1, coloumnslist);
        index = 2;

        for (DisputesDTOInter Records : SearchRecords) {
            Object[] coloumnsData = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                if ("TRANSACTION_TYPE_MASTER".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiontypemaster()).toString();
                }
                if ("RESPONSE_CODE_MASTER".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getresponsecodemaster()).toString();
                }
                if ("CARD_NO_SUFFIX".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcardnosuffix()).toString();
                }
                if ("TRANSACTION_DATA".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiondata()).toString();
                }
                if ("TRANSACTION_DATE".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiondate()).toString();
                }
                if ("SETTLEMENT_DATE".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getsettlementdate()).toString();
                }
                if ("TRANSACTION_SEQUENCE_ORDER_BY".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactionsequenceorderby()).toString();
                }
                if ("TRANSACTION_TYPE".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiontype()).toString();
                }
                if ("CARD_NO".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcardno()).toString();
                }
                if ("AMOUNT".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getamount()).toString();
                }
                if ("CURRENCY".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcurrency()).toString();
                }
                if ("CURRENCY_ID".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcurrencyid()).toString();
                }
                if ("CUSTOMER_ACCOUNT_NUMBER".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcustomeraccountnumber()).toString();
                }
                if ("RESPONSE_CODE".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getresponsecode()).toString();
                }
                if ("RESPONSE_CODE_ID".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getresponsecodeid()).toString();
                }
                if ("TRANSACTION_STATUS".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactionstatus()).toString();
                }
                if ("NOTES_PRESENTED".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getnotespresented()).toString();
                }
                if ("ATM_ID".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getatmid()).toString();
                }
                if ("TRANSACTION_TIME".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiontime()).toString();
                }
                if ("ATM_APPLICATION_ID".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getatmapplicationid()).toString();
                }
                if ("COLUMN1".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn1()).toString();
                }
                if ("COLUMN2".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn2()).toString();
                }

                if ("COLUMN3".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn3()).toString();
                }

                if ("COLUMN4".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn4()).toString();
                }

                if ("COLUMN5".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn5()).toString();
                }

            }
            data.put(index, coloumnsData);
            index = index + 1;
        }
        List<Integer> keyset = new ArrayList<Integer>(data.keySet());
        Collections.sort(keyset);
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            Calendar c = Calendar.getInstance();
            String uri = "Trans" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".xls";
            String pdfPath = "C:/Program Files/Apache Software Foundation/Tomcat 7.0/webapps/AtmBingo/PDF/";
            
            pdfPath = pdfPath + uri;
            System.out.println(pdfPath);
            FileOutputStream out = new FileOutputStream(new File(pdfPath));
            workbook.write(out);
            out.close();
            return pdfPath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String exportexcel_Rep_3(List<RepStat13DTOInter> SearchRecords) throws Throwable {
        Object[] Space = new Object[1];
        List<String> coloumns = new ArrayList<String>();
        coloumns.add("Date From");
        coloumns.add("Date To");
        coloumns.add("Cash Added");
        coloumns.add("Remaining Cash");
        coloumns.add("Dispensed Cash");
        coloumns.add("Switch");
        coloumns.add("Dispute Sum");
        coloumns.add("Cash Over");
        coloumns.add("Note Presented");
        coloumns.add("Switch Pending");
        coloumns.add("Switch Over");
        List<String> subbcolumns = new ArrayList<String>();
        subbcolumns.add("Card Number");
        subbcolumns.add("Trans Date");
        subbcolumns.add("Trans Sequence");
        subbcolumns.add("Authorization");
        subbcolumns.add("Amount");
        subbcolumns.add("Trans Status");
        subbcolumns.add("Transactios Source");

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("ATM Reconcile Detail");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        int index = 0;

        for (RepStat13DTOInter Records : SearchRecords) {
            Object[] Headerlist = new Object[4];
            for (int i = 0; i < 4; i++) {
                if (i == 0) {
                    Headerlist[0] = "ATM Name:";
                    sheet.setDefaultColumnWidth(20);
                    index = index + 1;
                }
                if (i == 1) {
                    Headerlist[1] = Records.getAtmname();
                    sheet.setDefaultColumnWidth(20);
                    index = index + 1;
                }
                if (i == 2) {
                    Headerlist[2] = "ATM Name:";
                    sheet.setDefaultColumnWidth(20);
                    index = index + 1;
                }
                if (i == 3) {
                    Headerlist[3] = Records.getAtmid();
                    sheet.setDefaultColumnWidth(20);
                    index = index + 1;
                }
            }
            data.put(index, Headerlist);
            index = index + 1;

            Object[] coloumnslist = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                coloumnslist[i] = coloumns.get(i);
                sheet.setDefaultColumnWidth(20);
            }
            data.put(index, coloumnslist);
            index = index + 1;

            Object[] coloumnsData = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                if ("Date From".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDatefrom()).toString().replace("null", " ");
                }
                if ("Date To".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDateto()).toString().replace("null", " ");
                }
                if ("Cash Added".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getCashadded()).toString().replace("null", "0");
                }
                if ("Remaining Cash".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getRemainingcash()).toString().replace("null", "0");
                }
                if ("Dispensed Cash".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDispensedcash()).toString().replace("null", "0");
                }
                if ("Switch".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getSwitchtotal()).toString().replace("null", "0");
                }
                if ("Dispute Sum".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDispute()).toString().replace("null", "0");
                }
                if ("Cash Over".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getCashover()).toString().replace("null", "0");
                }
                if ("Note Presented".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getCashshortage()).toString().replace("null", "0");
                }
                if ("Switch Pending".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getSwitchpending()).toString().replace("null", "0");
                }
                if ("Switch Over".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getSwitchover()).toString().replace("null", "0");
                }
            }
            data.put(index, coloumnsData);
            index = index + 1;
            data.put(index, Space);
            index = index + 1;
            Object[] ReportHeaderData = new Object[1];
            if (Records.getSwitchover() > 0) {
                ReportHeaderData[0] = "Switch Over Details:";
            } else {
                ReportHeaderData[0] = "Switch Pending Details:";
            }

            data.put(index, ReportHeaderData);
            index = index + 1;
            List<ReportTransDTOInter> SubRecords = new ArrayList<ReportTransDTOInter>();
            if (Records.getSwitchover() > 0) {
                SubRecords = findRecordDetail_Rep_3_2(Records.getRepid());
            } else {
                SubRecords = findRecordDetail_Rep_3_1(Records.getRepid());
            }

            if (SubRecords.size() > 0) {

                BigDecimal totalAmount = BigDecimal.ZERO;
                Object[] subcoloumnslist = new Object[subbcolumns.size()];
                for (int i = 0; i < subbcolumns.size(); i++) {
                    subcoloumnslist[i] = subbcolumns.get(i);
                    sheet.setDefaultColumnWidth(20);
                }
                data.put(index, subcoloumnslist);
                index = index + 1;
                for (ReportTransDTOInter subRecord : SubRecords) {
                    totalAmount = totalAmount.add(new BigDecimal(subRecord.getAmount()));
                    Object[] subcoloumnsData = new Object[subbcolumns.size()];
                    for (int i = 0; i < subbcolumns.size(); i++) {
                        if ("Card Number".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getCardno()).toString().replace("null", " ");
                        }
                        if ("Trans Date".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransdate()).toString().replace("null", " ");
                        }
                        if ("Trans Sequence".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransseq()).toString().replace("null", " ");
                        }
                        if ("Authorization".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getAuth()).toString().replace("null", " ");
                        }
                        if ("Amount".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getAmount()).toString().replace("null", "0");
                        }
                        if ("Trans Status".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransstatus()).toString().replace("null", " ");
                        }
                        if ("Transactios Source".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTranssource()).toString().replace("null", " ");
                        }
                    }
                    data.put(index, subcoloumnsData);
                    index = index + 1;
                }

                Object[] TotalData = new Object[2];
                TotalData[0] = "Total Amount:";
                TotalData[1] = totalAmount.toString();
                data.put(index, TotalData);
                index = index + 1;
                data.put(index, Space);
                index = index + 1;
                totalAmount = BigDecimal.ZERO;
            }
            Object[] ReportHeaderData2 = new Object[1];
            ReportHeaderData2[0] = "Cash Over Detail:";

            data.put(index, ReportHeaderData2);
            index = index + 1;
            List<ReportTransDTOInter> SubRecords2 = new ArrayList<ReportTransDTOInter>();
            SubRecords2 = findRecordDetail_Rep_5_4(Records.getRepid());
            if (SubRecords2.size() > 0) {

                BigDecimal totalamount2 = BigDecimal.ZERO;

                Object[] subcoloumnslist2 = new Object[subbcolumns.size()];
                for (int i = 0; i < subbcolumns.size(); i++) {
                    subcoloumnslist2[i] = subbcolumns.get(i);
                    sheet.setDefaultColumnWidth(20);
                }
                data.put(index, subcoloumnslist2);
                index = index + 1;

                for (ReportTransDTOInter subRecord : SubRecords2) {

                    totalamount2 = totalamount2.add(new BigDecimal(subRecord.getAmount()));

                    Object[] subcoloumnsData = new Object[subbcolumns.size()];
                    for (int i = 0; i < subbcolumns.size(); i++) {
                          if ("Card Number".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getCardno()).toString().replace("null", " ");
                        }
                        if ("Trans Date".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransdate()).toString().replace("null", " ");
                        }
                        if ("Trans Sequence".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransseq()).toString().replace("null", " ");
                        }
                        if ("Authorization".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getAuth()).toString().replace("null", " ");
                        }
                        if ("Amount".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getAmount()).toString().replace("null", "0");
                        }
                        if ("Trans Status".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransstatus()).toString().replace("null", " ");
                        }
                        if ("Transactios Source".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTranssource()).toString().replace("null", " ");
                        }
                    }
                    data.put(index, subcoloumnsData);
                    index = index + 1;
                }

                Object[] TotalData = new Object[2];
                TotalData[0] = "Total Amount:";
                TotalData[1] = totalamount2.toString();

                data.put(index, TotalData);
                index = index + 1;
                data.put(index, Space);
                index = index + 1;
                totalamount2 = BigDecimal.ZERO;

            }
        }
        List<Integer> keyset = new ArrayList<Integer>(data.keySet());
        Collections.sort(keyset);
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            Calendar c = Calendar.getInstance();
            String uri = "ATM Reconcile Detail" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".xls";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            pdfPath = pdfPath + uri;
            FileOutputStream out = new FileOutputStream(new File(pdfPath));
            workbook.write(out);
            out.close();
            return pdfPath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public List<ReportTransDTOInter> findRecordDetail_Rep_3_1(String RepMasterID) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT   B.TRANSACTION_DATE,\n"
                + "         B.TRANSACTION_SEQUENCE,\n"
                + "         B.COLUMN1,\n"
                + "B.cardno,\n"
                + "         B.AMOUNT,\n"
                + "         B.TRANSACTION_STATUS,\n"
                + "         B.TRANSACTION_STATUS_ID,\n"
                + "         B.NOTES_PRESENTED,\n"
                + "         (SELECT   replace(NAME,'\\',' ')\n"
                + "            FROM   ATM_FILE\n"
                + "           WHERE   ID = B.FILE_ID)\n"
                + "            NAME\n"
                + "  FROM   (SELECT   TRANSACTION_DATE,\n"
                + "                   TRANSACTION_SEQUENCE,\n"
                + "                   column3 COLUMN1,\n"
                + "                   AMOUNT,\n"
                + "                   TRANSACTION_STATUS,\n"
                + "                   TRANSACTION_STATUS_ID,\n"
                + "                   NOTES_PRESENTED,\n"
                + "                   FILE_ID,\n"
                + "decrypt(card_no)cardno\n"
                + "            FROM   DISPUTES\n"
                + "           WHERE   TRANSACTION_DATE > ( SELECT   DATE_FROM\n"
                + " 		                    FROM   REPLANISHMENT_MASTER\n"
                + "			          WHERE   id = $P{parameter1} )\n"
                + "AND TRANSACTION_DATE < ( SELECT   DATE_TO\n"
                + " 		     FROM   REPLANISHMENT_MASTER\n"
                + "		     WHERE   id = $P{parameter1} )\n"
                + "AND ATM_ID = ( SELECT  ATM_ID\n"
                + " 		     FROM   REPLANISHMENT_MASTER\n"
                + "		     WHERE   id = $P{parameter1} )\n"
                + "AND record_type = 1\n"
                + "AND matching_type = 1) B\n"
                + "order by  B.TRANSACTION_DATE";
        selectStat = selectStat.replace("$P{parameter1}", RepMasterID);
        ResultSet rs = executeQuery(selectStat);
        List<ReportTransDTOInter> records = new ArrayList<ReportTransDTOInter>();
        while (rs.next()) {
            ReportTransDTOInter record = DTOFactory.createReportTransDTO();
            record.setAmount(rs.getBigDecimal("AMOUNT").toString());
            record.setAuth(rs.getString("COLUMN1"));
            record.setCardno(rs.getString("cardno"));
            record.setTransdate(DateFormatter.changeDateAndTimeFormat(rs.getTimestamp("TRANSACTION_DATE")));
            record.setTransseq(rs.getString("TRANSACTION_SEQUENCE"));
            record.setTranssource(rs.getString("NAME"));
            record.setTransstatus(rs.getString("TRANSACTION_STATUS"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public List<ReportTransDTOInter> findRecordDetail_Rep_3_2(String RepMasterID) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT   B.TRANSACTION_DATE,\n"
                + "         B.TRANSACTION_SEQUENCE,\n"
                + "         B.COLUMN1,\n"
                + "B.cardno,\n"
                + "         B.AMOUNT,\n"
                + "         B.TRANSACTION_STATUS,\n"
                + "         B.TRANSACTION_STATUS_ID,\n"
                + "         B.NOTES_PRESENTED,\n"
                + "         (SELECT   replace(NAME,'\\',' ')\n"
                + "            FROM   ATM_FILE\n"
                + "           WHERE   ID = B.FILE_ID)\n"
                + "            NAME\n"
                + "  FROM   (SELECT   TRANSACTION_DATE,\n"
                + "                   TRANSACTION_SEQUENCE,\n"
                + "                   column3 COLUMN1,\n"
                + "                   AMOUNT,\n"
                + "                   TRANSACTION_STATUS,\n"
                + "                   TRANSACTION_STATUS_ID,\n"
                + "                   NOTES_PRESENTED,\n"
                + "                   FILE_ID,\n"
                + "decrypt(card_no)cardno\n"
                + "            FROM   DISPUTES\n"
                + "           WHERE   TRANSACTION_DATE > ( SELECT   DATE_FROM\n"
                + " 		                    FROM   REPLANISHMENT_MASTER\n"
                + "			          WHERE   id = $P{parameter1} )\n"
                + "AND TRANSACTION_DATE < ( SELECT   DATE_TO\n"
                + " 		     FROM   REPLANISHMENT_MASTER\n"
                + "		     WHERE   id = $P{parameter1} )\n"
                + "AND ATM_ID = ( SELECT  ATM_ID\n"
                + " 		     FROM   REPLANISHMENT_MASTER\n"
                + "		     WHERE   id = $P{parameter1} )\n"
                + "AND record_type = 2\n"
                + "AND matching_type = 1) B\n"
                + "order by  B.TRANSACTION_DATE";
        selectStat = selectStat.replace("$P{parameter1}", RepMasterID);
        ResultSet rs = executeQuery(selectStat);
        List<ReportTransDTOInter> records = new ArrayList<ReportTransDTOInter>();
        while (rs.next()) {
            ReportTransDTOInter record = DTOFactory.createReportTransDTO();
            record.setAmount(rs.getBigDecimal("AMOUNT").toString());
            record.setAuth(rs.getString("COLUMN1"));
            record.setCardno(rs.getString("cardno"));
            record.setTransdate(DateFormatter.changeDateAndTimeFormat(rs.getTimestamp("TRANSACTION_DATE")));
            record.setTransseq(rs.getString("TRANSACTION_SEQUENCE"));
            record.setTranssource(rs.getString("NAME"));
            record.setTransstatus(rs.getString("TRANSACTION_STATUS"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public String exportexcel_Rep_2(List<RepStat2DTOInter> SearchRecords) throws Throwable {
        Object[] Space = new Object[15];
        Object[] mastercoloumns = new Object[15];
        mastercoloumns[0] = "Date From";
        mastercoloumns[1] = "Date To";
        mastercoloumns[2] = "Switch";
        mastercoloumns[3] = "Note Presented";
        mastercoloumns[4] = "DIFF_NAME";
        mastercoloumns[5] = "DIFF_VALUE";
        mastercoloumns[6] = "Cash Over";
        Object[] detailcolumns = new Object[15];
        detailcolumns[0] = "Cassette Name";
        detailcolumns[1] = "Cash Added Count";
        detailcolumns[2] = "Cash Added Value";
        detailcolumns[3] = "Remaining Cash Count";
        detailcolumns[4] = "Remaining Cash Value";
        detailcolumns[5] = "Dispensed Cash Count";
        detailcolumns[6] = "Dispensed Cash Value";
        detailcolumns[7] = "Notes Presented Count";
        detailcolumns[8] = "Notes Presented Value";
        detailcolumns[9] = "Different Detail Count";
        detailcolumns[10] = "Different Detail Value";

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("ATM Repl.Activity");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();

        Integer index = 0;

        for (RepStat2DTOInter Records : SearchRecords) {

            //Master Record Header
            Object[] MasterRec = new Object[15];
            MasterRec[0] = "ATM ID:";
            MasterRec[1] = Records.getAtmname();
            MasterRec[2] = "ATM Name:";
            MasterRec[3] = Records.getAtmid();
            data.put(index, MasterRec);
            index = index + 1;

            //Master Column Header 
            data.put(index, mastercoloumns);
            index = index + 1;

            //Master Data
            Object[] MasterDataRec = new Object[15];
            MasterDataRec[0] = String.valueOf(Records.getDatefrom()).toString().replace("null", " ");
            MasterDataRec[1] = String.valueOf(Records.getDateto()).toString().replace("null", " ");
            MasterDataRec[2] = String.valueOf(Records.getSwitchtotal()).toString().replace("null", "0");
            MasterDataRec[3] = String.valueOf(Records.getNotepres()).toString().replace("null", "0");
            MasterDataRec[4] = String.valueOf(Records.getDifferentname()).toString().replace("null", " ");
            MasterDataRec[5] = String.valueOf(Records.getDifferentvalue()).toString().replace("null", "0");
            MasterDataRec[6] = String.valueOf(Records.getCashover()).toString().replace("null", "0");
            data.put(index, MasterDataRec);
            index = index + 1;

            //Space
            data.put(index, Space);
            index = index + 1;

            //Datail Title
            Object[] ReportHeaderData = new Object[15];
            ReportHeaderData[0] = "Details:";
            data.put(index, ReportHeaderData);
            index = index + 1;

            //getdetails from db
            List<ReportStatDetailDTOInter> SubRecords = findRecordDetail_Rep_2(Records.getId());

            if (SubRecords.size() > 0) {

                BigDecimal CassetteName = BigDecimal.ZERO;
                BigDecimal CashAddedCount = BigDecimal.ZERO;
                BigDecimal CashAddedValue = BigDecimal.ZERO;
                BigDecimal RemainingCashCount = BigDecimal.ZERO;
                BigDecimal RemainingCashValue = BigDecimal.ZERO;
                BigDecimal DispensedCashCount = BigDecimal.ZERO;
                BigDecimal DispensedCashValue = BigDecimal.ZERO;
                BigDecimal NotesPresentedCount = BigDecimal.ZERO;
                BigDecimal NotesPresentedValue = BigDecimal.ZERO;
                BigDecimal DifferentDetailCount = BigDecimal.ZERO;
                BigDecimal DifferentDetailValue = BigDecimal.ZERO;

                //Details Header
                data.put(index, detailcolumns);
                index = index + 1;

                for (ReportStatDetailDTOInter subRecord : SubRecords) {

                    CashAddedCount = CashAddedCount.add(new BigDecimal(subRecord.getCashaddedcount()));
                    CashAddedValue = CashAddedValue.add(new BigDecimal(subRecord.getCashaddedvalue()));
                    RemainingCashCount = RemainingCashCount.add(new BigDecimal(subRecord.getRemaincashcount()));
                    RemainingCashValue = RemainingCashValue.add(new BigDecimal(subRecord.getRemaincashvalue()));
                    DispensedCashCount = DispensedCashCount.add(new BigDecimal(subRecord.getDispensecashcount()));
                    DispensedCashValue = DispensedCashValue.add(new BigDecimal(subRecord.getDispensecashvalue()));
                    NotesPresentedCount = NotesPresentedCount.add(new BigDecimal(subRecord.getNoteprescount()));
                    NotesPresentedValue = NotesPresentedValue.add(new BigDecimal(subRecord.getNotespresvalue()));
                    DifferentDetailCount = DifferentDetailCount.add(new BigDecimal(subRecord.getDiffcount()));
                    DifferentDetailValue = DifferentDetailValue.add(new BigDecimal(subRecord.getDiffvalue()));

                    //Detail Data
                    Object[] subcoloumnsData = new Object[15];
                    subcoloumnsData[0] = String.valueOf(subRecord.getName()).toString().replace("null", " ");
                    subcoloumnsData[1] = String.valueOf(subRecord.getCashaddedcount()).toString().replace("null", " ");
                    subcoloumnsData[2] = String.valueOf(subRecord.getCashaddedvalue()).toString().replace("null", " ");
                    subcoloumnsData[3] = String.valueOf(subRecord.getRemaincashcount()).toString().replace("null", " ");
                    subcoloumnsData[4] = String.valueOf(subRecord.getRemaincashvalue()).toString().replace("null", "0");
                    subcoloumnsData[5] = String.valueOf(subRecord.getDispensecashcount()).toString().replace("null", " ");
                    subcoloumnsData[6] = String.valueOf(subRecord.getDispensecashvalue()).toString().replace("null", " ");
                    subcoloumnsData[7] = String.valueOf(subRecord.getNoteprescount()).toString().replace("null", " ");
                    subcoloumnsData[8] = String.valueOf(subRecord.getNotespresvalue()).toString().replace("null", " ");
                    subcoloumnsData[9] = String.valueOf(subRecord.getDiffcount()).toString().replace("null", " ");
                    subcoloumnsData[10] = String.valueOf(subRecord.getDiffvalue()).toString().replace("null", " ");
                    data.put(index, subcoloumnsData);
                    index = index + 1;
                }

                //Totals Data
                Object[] TotalData = new Object[15];
                TotalData[0] = "Totals:";
                TotalData[1] = CashAddedCount.toString();
                TotalData[2] = CashAddedValue.toString();
                TotalData[3] = RemainingCashCount.toString();
                TotalData[4] = RemainingCashValue.toString();
                TotalData[5] = DispensedCashCount.toString();
                TotalData[6] = DispensedCashValue.toString();
                TotalData[7] = NotesPresentedCount.toString();
                TotalData[8] = NotesPresentedValue.toString();
                TotalData[9] = DifferentDetailCount.toString();
                TotalData[10] = DifferentDetailValue.toString();
                data.put(index, TotalData);
                index = index + 1;

                data.put(index, Space);
                index = index + 1;
            }
        }
        System.out.println(data);

        List<Integer> keyset = new ArrayList<Integer>(data.keySet());
        Collections.sort(keyset);
        int rownum = 0;

        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);

            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            Calendar c = Calendar.getInstance();
            String uri = "ATM Repl.Activity" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".xls";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            pdfPath = pdfPath + uri;
            FileOutputStream out = new FileOutputStream(new File(pdfPath));
            workbook.write(out);
            out.close();
            return pdfPath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public List<ReportStatDetailDTOInter> findRecordDetail_Rep_2(String RepMasterID) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT   cassette.name,\n"
                + "           cassette.amount,\n"
                + "           CASSETE_VALUE Cash_Added_Count,\n"
                + "           (REPLANISHMENT_DETAIL.CASSETE_VALUE * cassette.amount)\n"
                + "              Cash_Added_Value,\n"
                + "           REMAINING Remaining_Cash_Count,\n"
                + "           REMAINING_AMOUNT Remaining_Cash_Value,\n"
                + "           (CASSETE_VALUE - NVL (REMAINING, 0)) Dispensed_Cash_Count,\n"
                + "           ( (CASSETE_VALUE - NVL (REMAINING, 0)) * cassette.amount)\n"
                + "              Dispensed_Cash_Value,\n"
                + "           NVL(COUNT_NOTES,0) NOTES_PRESENTED_COUNT,\n"
                + "           NVL(NOTES_RESENTED,0) NOTES_PRESENTED_VALUE,\n"
                + "           ( (CASSETE_VALUE - NVL (REMAINING, 0)) - NVL (COUNT_NOTES, 0))\n"
                + "              diff_count,\n"
                + "           ( ( (CASSETE_VALUE - NVL (REMAINING, 0)) * cassette.amount)\n"
                + "            - NVL (NOTES_RESENTED, 0))\n"
                + "              diff_value\n"
                + "    FROM   REPLANISHMENT_DETAIL, cassette\n"
                + "   WHERE   REPLANISHMENT_DETAIL.cassete = cassette.id\n"
                + "           AND REPLANISHMENT_DETAIL.ID = $P{parameter1}\n"
                + "ORDER BY   cassette.seq";
        selectStat = selectStat.replace("$P{parameter1}", RepMasterID);
        ResultSet rs = executeQuery(selectStat);
        List<ReportStatDetailDTOInter> records = new ArrayList<ReportStatDetailDTOInter>();
        while (rs.next()) {
            ReportStatDetailDTOInter record = DTOFactory.createReportStatDetailDTO();
            record.setName(rs.getString("name"));
            record.setAmount(rs.getBigDecimal("amount").toString());
            record.setCashaddedcount(rs.getBigDecimal("Cash_Added_Count").toString());
            record.setCashaddedvalue(rs.getBigDecimal("Cash_Added_Value").toString());
            record.setRemaincashcount(rs.getBigDecimal("Remaining_Cash_Count").toString());
            record.setRemaincashvalue(rs.getBigDecimal("Remaining_Cash_Value").toString());
            record.setDispensecashcount(rs.getBigDecimal("Dispensed_Cash_Count").toString());
            record.setDispensecashvalue(rs.getBigDecimal("Dispensed_Cash_Value").toString());
            record.setNoteprescount(rs.getBigDecimal("NOTES_PRESENTED_COUNT").toString());
            record.setNotespresvalue(rs.getBigDecimal("NOTES_PRESENTED_VALUE").toString());
            record.setDiffcount(rs.getBigDecimal("diff_count").toString());
            record.setDiffvalue(rs.getBigDecimal("diff_value").toString());
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public String exportexcel_Rep_1(List<RepStat13DTOInter> SearchRecords) throws Throwable {
        Object[] Space = new Object[1];
        Boolean footer = Boolean.FALSE;
        BigDecimal CashAdded = BigDecimal.ZERO;
        BigDecimal RemainingCash = BigDecimal.ZERO;
        BigDecimal DispensedCash = BigDecimal.ZERO;
        BigDecimal Switch = BigDecimal.ZERO;
        BigDecimal Dispute = BigDecimal.ZERO;
        BigDecimal CashOver = BigDecimal.ZERO;
        BigDecimal NotesPresented = BigDecimal.ZERO;
        BigDecimal SwitchPending = BigDecimal.ZERO;
        BigDecimal SwitchOver = BigDecimal.ZERO;

        List<String> coloumns = new ArrayList<String>();
        coloumns.add("Date From");
        coloumns.add("Date To");
        coloumns.add("Cash Added");
        coloumns.add("Remaining Cash");
        coloumns.add("Dispensed Cash");
        coloumns.add("Switch");
        coloumns.add("Dispute");
        coloumns.add("Cash Over");
        coloumns.add("Notes Presented");
        coloumns.add("Switch Pending");
        coloumns.add("Switch Over");

        String RunningATM = "";

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("ATM Reconcile Summary");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        int index = 0;

        for (RepStat13DTOInter Records : SearchRecords) {

            if (Records.getAtmname().equals(RunningATM)) {
                CashAdded = CashAdded.add(new BigDecimal(Records.getCashadded()));
                RemainingCash = RemainingCash.add(new BigDecimal(Records.getRemainingcash()));
                DispensedCash = DispensedCash.add(new BigDecimal(Records.getDispensedcash()));
                Switch = Switch.add(new BigDecimal(Records.getSwitchtotal()));
                Dispute = Dispute.add(new BigDecimal(Records.getDispute()));
                CashOver = CashOver.add(new BigDecimal(Records.getCashover()));
                NotesPresented = NotesPresented.add(new BigDecimal(Records.getCashshortage()));
                SwitchPending = SwitchPending.add(new BigDecimal(Records.getSwitchpending()));
                SwitchOver = SwitchOver.add(new BigDecimal(Records.getSwitchover()));
            }
            if ("".equals(RunningATM)) {
                RunningATM = Records.getAtmname();
                Object[] Headerlist = new Object[4];
                for (int i = 0; i < 4; i++) {
                    if (i == 0) {
                        Headerlist[0] = "ATM ID:";
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                    if (i == 1) {
                        Headerlist[1] = Records.getAtmname();
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                    if (i == 2) {
                        Headerlist[2] = "ATM Name:";
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                    if (i == 3) {
                        Headerlist[3] = Records.getAtmid();
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                }
                data.put(index, Headerlist);
                index = index + 1;
                Object[] coloumnslist = new Object[coloumns.size()];
                for (int i = 0; i < coloumns.size(); i++) {
                    coloumnslist[i] = coloumns.get(i);
                    sheet.setDefaultColumnWidth(20);
                }
                data.put(index, coloumnslist);
                index = index + 1;
                CashAdded = CashAdded.add(new BigDecimal(Records.getCashadded()));
                RemainingCash = RemainingCash.add(new BigDecimal(Records.getRemainingcash()));
                DispensedCash = DispensedCash.add(new BigDecimal(Records.getDispensedcash()));
                Switch = Switch.add(new BigDecimal(Records.getSwitchtotal()));
                Dispute = Dispute.add(new BigDecimal(Records.getDispute()));
                CashOver = CashOver.add(new BigDecimal(Records.getCashover()));
                NotesPresented = NotesPresented.add(new BigDecimal(Records.getCashshortage()));
                SwitchPending = SwitchPending.add(new BigDecimal(Records.getSwitchpending()));
                SwitchOver = SwitchOver.add(new BigDecimal(Records.getSwitchover()));
            }
            if (!Records.getAtmname().equals(RunningATM)) {

                Object[] TotalData = new Object[11];
                TotalData[0] = "";
                TotalData[1] = "Totals:";
                TotalData[2] = CashAdded.toString();
                TotalData[3] = RemainingCash.toString();
                TotalData[4] = DispensedCash.toString();
                TotalData[5] = Switch.toString();
                TotalData[6] = Dispute.toString();
                TotalData[7] = CashOver.toString();
                TotalData[8] = NotesPresented.toString();
                TotalData[9] = SwitchPending.toString();
                TotalData[10] = SwitchOver.toString();

                data.put(index, TotalData);
                index = index + 1;
                CashAdded = BigDecimal.ZERO;
                RemainingCash = BigDecimal.ZERO;
                DispensedCash = BigDecimal.ZERO;
                Switch = BigDecimal.ZERO;
                Dispute = BigDecimal.ZERO;
                CashOver = BigDecimal.ZERO;
                NotesPresented = BigDecimal.ZERO;
                SwitchPending = BigDecimal.ZERO;
                SwitchOver = BigDecimal.ZERO;

                RunningATM = Records.getAtmname();
                data.put(index, Space);
                index = index + 1;
                Object[] Headerlist = new Object[4];
                for (int i = 0; i < 4; i++) {
                    if (i == 0) {
                        Headerlist[0] = "ATM ID:";
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                    if (i == 1) {
                        Headerlist[1] = Records.getAtmname();
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                    if (i == 2) {
                        Headerlist[2] = "ATM Name:";
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                    if (i == 3) {
                        Headerlist[3] = Records.getAtmid();
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                }
                data.put(index, Headerlist);
                index = index + 1;
                Object[] coloumnslist = new Object[coloumns.size()];
                for (int i = 0; i < coloumns.size(); i++) {
                    coloumnslist[i] = coloumns.get(i);
                    sheet.setDefaultColumnWidth(20);
                }
                data.put(index, coloumnslist);
                index = index + 1;
                CashAdded = CashAdded.add(new BigDecimal(Records.getCashadded()));
                RemainingCash = RemainingCash.add(new BigDecimal(Records.getRemainingcash()));
                DispensedCash = DispensedCash.add(new BigDecimal(Records.getDispensedcash()));
                Switch = Switch.add(new BigDecimal(Records.getSwitchtotal()));
                Dispute = Dispute.add(new BigDecimal(Records.getDispute()));
                CashOver = CashOver.add(new BigDecimal(Records.getCashover()));
                NotesPresented = NotesPresented.add(new BigDecimal(Records.getCashshortage()));
                SwitchPending = SwitchPending.add(new BigDecimal(Records.getSwitchpending()));
                SwitchOver = SwitchOver.add(new BigDecimal(Records.getSwitchover()));
            }

            Object[] coloumnsData = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                if ("Date From".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDatefrom()).toString().replace("null", " ");
                }
                if ("Date To".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDateto()).toString().replace("null", " ");
                }
                if ("Cash Added".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getCashadded()).toString().replace("null", "0");
                }
                if ("Remaining Cash".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getRemainingcash()).toString().replace("null", "0");
                }
                if ("Dispensed Cash".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDispensedcash()).toString().replace("null", "0");
                }
                if ("Switch".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getSwitchtotal()).toString().replace("null", "0");
                }
                if ("Dispute".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDispute()).toString().replace("null", "0");
                }
                if ("Cash Over".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getCashover()).toString().replace("null", "0");
                }
                if ("Notes Presented".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getCashshortage()).toString().replace("null", "0");
                }
                if ("Switch Pending".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getSwitchpending()).toString().replace("null", "0");
                }
                if ("Switch Over".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getSwitchover()).toString().replace("null", "0");
                }
            }
            data.put(index, coloumnsData);
            index = index + 1;
        }
        Object[] TotalData = new Object[11];
        TotalData[0] = "";
        TotalData[1] = "Totals:";
        TotalData[2] = CashAdded.toString();
        TotalData[3] = RemainingCash.toString();
        TotalData[4] = DispensedCash.toString();
        TotalData[5] = Switch.toString();
        TotalData[6] = Dispute.toString();
        TotalData[7] = CashOver.toString();
        TotalData[8] = NotesPresented.toString();
        TotalData[9] = SwitchPending.toString();
        TotalData[10] = SwitchOver.toString();

        data.put(index, TotalData);
        index = index + 1;
        List<Integer> keyset = new ArrayList<Integer>(data.keySet());
        Collections.sort(keyset);
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            Calendar c = Calendar.getInstance();
            String uri = "ATM Reconcile Summary" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".xls";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            pdfPath = pdfPath + uri;
            FileOutputStream out = new FileOutputStream(new File(pdfPath));
            workbook.write(out);
            out.close();
            return pdfPath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String exportexcel_Rep_4(List<RepStat4DTOInter> SearchRecords) throws Throwable {
        Object[] Space = new Object[1];

        BigDecimal SwitchAmount = BigDecimal.ZERO;
        BigDecimal HostAmount = BigDecimal.ZERO;
        BigDecimal OffUSAmount = BigDecimal.ZERO;
        BigDecimal SwitchDifference = BigDecimal.ZERO;
        BigDecimal HostDifference = BigDecimal.ZERO;
        BigDecimal OffUSDifference = BigDecimal.ZERO;

        List<String> coloumns = new ArrayList<String>();
        coloumns.add("Date From");
        coloumns.add("Date To");
        coloumns.add("Switch Amount");

        String RunningATM = "";

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Switch Reconciliation Summary Report");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        int index = 0;

        for (RepStat4DTOInter Records : SearchRecords) {

            if (Records.getAtm().equals(RunningATM)) {
                SwitchAmount = SwitchAmount.add(new BigDecimal(Records.getSwitchamount()));
                HostAmount = HostAmount.add(new BigDecimal(Records.getHostamount()));
                OffUSAmount = OffUSAmount.add(new BigDecimal(Records.getOffsamount()));
                SwitchDifference = SwitchDifference.add(new BigDecimal(Records.getSwitchdifference()));
                HostDifference = HostDifference.add(new BigDecimal(Records.getHostdifference()));
                OffUSDifference = OffUSDifference.add(new BigDecimal(Records.getOffusdifference()));
            }
            if ("".equals(RunningATM)) {
                RunningATM = Records.getAtm();
                Object[] Headerlist = new Object[4];
                for (int i = 0; i < 4; i++) {
                    if (i == 0) {
                        Headerlist[0] = "ATM ID:";
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                    if (i == 1) {
                        Headerlist[1] = Records.getAtmid();
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                    if (i == 2) {
                        Headerlist[2] = "ATM Name:";
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                    if (i == 3) {
                        Headerlist[3] = Records.getAtm();
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                }
                data.put(index, Headerlist);
                index = index + 1;
                Object[] coloumnslist = new Object[coloumns.size()];
                for (int i = 0; i < coloumns.size(); i++) {
                    coloumnslist[i] = coloumns.get(i);
                    sheet.setDefaultColumnWidth(20);
                }
                data.put(index, coloumnslist);
                index = index + 1;
                SwitchAmount = SwitchAmount.add(new BigDecimal(Records.getSwitchamount()));

            }
            if (!Records.getAtm().equals(RunningATM)) {

                Object[] TotalData = new Object[8];
                TotalData[0] = "";
                TotalData[1] = "Totals:";
                TotalData[2] = SwitchAmount.toString();
                TotalData[3] = HostAmount.toString();
                TotalData[4] = OffUSAmount.toString();
                TotalData[5] = SwitchDifference.toString();
                TotalData[6] = HostDifference.toString();
                TotalData[7] = OffUSDifference.toString();

                data.put(index, TotalData);
                index = index + 1;
                SwitchAmount = BigDecimal.ZERO;
                HostAmount = BigDecimal.ZERO;
                OffUSAmount = BigDecimal.ZERO;
                SwitchDifference = BigDecimal.ZERO;
                HostDifference = BigDecimal.ZERO;
                OffUSDifference = BigDecimal.ZERO;

                RunningATM = Records.getAtm();
                data.put(index, Space);
                index = index + 1;
                Object[] Headerlist = new Object[4];
                for (int i = 0; i < 4; i++) {
                    if (i == 0) {
                        Headerlist[0] = "ATM ID:";
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                    if (i == 1) {
                        Headerlist[1] = Records.getAtmid();
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                    if (i == 2) {
                        Headerlist[2] = "ATM Name:";
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                    if (i == 3) {
                        Headerlist[3] = Records.getAtm();
                        sheet.setDefaultColumnWidth(20);
                        index = index + 1;
                    }
                }
                data.put(index, Headerlist);
                index = index + 1;
                Object[] coloumnslist = new Object[coloumns.size()];
                for (int i = 0; i < coloumns.size(); i++) {
                    coloumnslist[i] = coloumns.get(i);
                    sheet.setDefaultColumnWidth(20);
                }
                data.put(index, coloumnslist);
                index = index + 1;
                SwitchAmount = SwitchAmount.add(new BigDecimal(Records.getSwitchamount()));
                HostAmount = HostAmount.add(new BigDecimal(Records.getHostamount()));
                OffUSAmount = OffUSAmount.add(new BigDecimal(Records.getOffsamount()));
                SwitchDifference = SwitchDifference.add(new BigDecimal(Records.getSwitchdifference()));
                HostDifference = HostDifference.add(new BigDecimal(Records.getHostdifference()));
                OffUSDifference = OffUSDifference.add(new BigDecimal(Records.getOffusdifference()));
            }

            Object[] coloumnsData = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                if ("Date From".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDatefrom()).toString().replace("null", " ");
                }
                if ("Date To".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDateto()).toString().replace("null", " ");
                }
                if ("Switch Amount".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getSwitchamount()).toString().replace("null", "0");
                }
                if ("Host Amount".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getHostamount()).toString().replace("null", "0");
                }
                if ("Off US Amount".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getOffsamount()).toString().replace("null", "0");
                }

            }
            data.put(index, coloumnsData);
            index = index + 1;
        }
        Object[] TotalData = new Object[8];
        TotalData[0] = "";
        TotalData[1] = "Totals:";
        TotalData[2] = SwitchAmount.toString();
        TotalData[3] = HostAmount.toString();
        TotalData[4] = OffUSAmount.toString();
        TotalData[5] = SwitchDifference.toString();
        TotalData[6] = HostDifference.toString();
        TotalData[7] = OffUSDifference.toString();

        data.put(index, TotalData);
        index = index + 1;
        List<Integer> keyset = new ArrayList<Integer>(data.keySet());
        Collections.sort(keyset);
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            Calendar c = Calendar.getInstance();
            String uri = "Switch Reconciliation Summary Report" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".xls";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            pdfPath = pdfPath + uri;
            FileOutputStream out = new FileOutputStream(new File(pdfPath));
            workbook.write(out);
            out.close();
            return pdfPath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String exportexcel_Rep_5(List<RepStat4DTOInter> SearchRecords) throws Throwable {
        Object[] Space = new Object[1];
        List<String> coloumns = new ArrayList<String>();
        coloumns.add("Date From");
        coloumns.add("Date To");
        coloumns.add("Switch Amount");

        List<String> subbcolumns = new ArrayList<String>();
        subbcolumns.add("Card NO");
        subbcolumns.add("Date & Time");
        subbcolumns.add("Sequence NO");
        subbcolumns.add("Authorization NO");
        subbcolumns.add("Amount");
        subbcolumns.add("Status Name");

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Switch Reconciliation Detailed Report");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        int index = 0;

        for (RepStat4DTOInter Records : SearchRecords) {
            Object[] Headerlist = new Object[4];
            for (int i = 0; i < 4; i++) {
                if (i == 0) {
                    Headerlist[0] = "ATM ID:";
                    sheet.setDefaultColumnWidth(20);
                    index = index + 1;
                }
                if (i == 1) {
                    Headerlist[1] = Records.getAtmid();
                    sheet.setDefaultColumnWidth(20);
                    index = index + 1;
                }
                if (i == 2) {
                    Headerlist[2] = "ATM Name:";
                    sheet.setDefaultColumnWidth(20);
                    index = index + 1;
                }
                if (i == 3) {
                    Headerlist[3] = Records.getAtm();
                    sheet.setDefaultColumnWidth(20);
                    index = index + 1;
                }
            }
            data.put(index, Headerlist);
            index = index + 1;

            Object[] coloumnslist = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                coloumnslist[i] = coloumns.get(i);
                sheet.setDefaultColumnWidth(20);
            }
            data.put(index, coloumnslist);
            index = index + 1;
            Object[] coloumnsData = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                if ("Date From".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDatefrom()).toString().replace("null", " ");
                }
                if ("Date To".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDateto()).toString().replace("null", " ");
                }
                if ("Switch Amount".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getSwitchamount()).toString().replace("null", "0");
                }
                if ("Host Amount".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getHostamount()).toString().replace("null", "0");
                }
                if ("Off US Amount".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getOffsamount()).toString().replace("null", "0");
                }

            }
            data.put(index, coloumnsData);
            index = index + 1;
            data.put(index, Space);
            index = index + 1;

            Object[] ReportHeaderData = new Object[1];
            ReportHeaderData[0] = "Switch Difference:";

            data.put(index, ReportHeaderData);
            index = index + 1;
            List<ReportTransDTOInter> SubRecords = new ArrayList<ReportTransDTOInter>();

            SubRecords = findRecordDetail_Rep_5_1(Records.getId());

            if (SubRecords.size() > 0) {

                BigDecimal totalamount = BigDecimal.ZERO;

                Object[] subcoloumnslist = new Object[subbcolumns.size()];
                for (int i = 0; i < subbcolumns.size(); i++) {
                    subcoloumnslist[i] = subbcolumns.get(i);
                    sheet.setDefaultColumnWidth(20);
                }
                data.put(index, subcoloumnslist);
                index = index + 1;

                for (ReportTransDTOInter subRecord : SubRecords) {

                    totalamount = totalamount.add(new BigDecimal(subRecord.getAmount()));

                    Object[] subcoloumnsData = new Object[subbcolumns.size()];
                    for (int i = 0; i < subbcolumns.size(); i++) {
                        if ("Card NO".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getCardno()).toString().replace("null", " ");
                        }
                        if ("Date & Time".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransdate()).toString().replace("null", " ");
                        }
                        if ("Sequence NO".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransseq()).toString().replace("null", " ");
                        }
                        if ("Authorization NO".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getAuth()).toString().replace("null", " ");
                        }
                        if ("Amount".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getAmount()).toString().replace("null", "0");
                        }
                        if ("Status Name".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransstatus()).toString().replace("null", " ");
                        }

                    }
                    data.put(index, subcoloumnsData);
                    index = index + 1;
                }

                Object[] TotalData = new Object[2];
                TotalData[0] = "Total Amount:";
                TotalData[1] = totalamount.toString();

                data.put(index, TotalData);
                index = index + 1;
                data.put(index, Space);
                index = index + 1;
                totalamount = BigDecimal.ZERO;

            }
        }
        List<Integer> keyset = new ArrayList<Integer>(data.keySet());
        Collections.sort(keyset);
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            Calendar c = Calendar.getInstance();
            String uri = "Switch Reconciliation Detailed Report" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".xls";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            pdfPath = pdfPath + uri;
            FileOutputStream out = new FileOutputStream(new File(pdfPath));
            workbook.write(out);
            out.close();
            return pdfPath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public List<ReportTransDTOInter> findRecordDetail_Rep_5_1(String RepMasterID) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT   decrypt(card_no)cardno,\n"
                + "\n"
                + "TRANSACTION_DATE,\n"
                + "\n"
                + "         TRANSACTION_SEQUENCE,\n"
                + "\n"
                + "         COLUMN3,\n"
                + "\n"
                + "         AMOUNT,\n"
                + "\n"
                + "         TRANSACTION_STATUS\n"
                + "\n"
                + "  FROM   DISPUTES\n"
                + "\n"
                + "WHERE TRIM (transaction_status) <> TRIM ('E*5') AND TRANSACTION_DATE > (  SELECT DATE_FROM\n"
                + "\n"
                + "                                   FROM   REPLANISHMENT_MASTER\n"
                + "\n"
                + "                                    WHERE   id = $P{parameter1})\n"
                + "\n"
                + " \n"
                + "\n"
                + "AND TRANSACTION_DATE < (  SELECT DATE_TO\n"
                + "\n"
                + "                                   FROM   REPLANISHMENT_MASTER\n"
                + "\n"
                + "                                    WHERE   id = $P{parameter1})\n"
                + "\n"
                + " \n"
                + "\n"
                + "AND ATM_ID in(  SELECT ATM_ID\n"
                + "\n"
                + "                                   FROM   REPLANISHMENT_MASTER\n"
                + "\n"
                + "                                    WHERE   id = $P{parameter1})\n"
                + "\n"
                + "         AND record_type = 2\n"
                + "\n"
                + "         AND matching_type = 2\n"
                + "\n"
                + "order by TRANSACTION_DATE";
        selectStat = selectStat.replace("$P{parameter1}", RepMasterID);
        ResultSet rs = executeQuery(selectStat);
        List<ReportTransDTOInter> records = new ArrayList<ReportTransDTOInter>();
        while (rs.next()) {
            ReportTransDTOInter record = DTOFactory.createReportTransDTO();
            record.setCardno(rs.getString("cardno"));
            record.setTransdate(DateFormatter.changeDateAndTimeFormat(rs.getTimestamp("TRANSACTION_DATE")));
            record.setTransseq(rs.getString("TRANSACTION_SEQUENCE"));
            record.setAuth(rs.getString("COLUMN3"));
            record.setAmount(rs.getBigDecimal("AMOUNT").toString());
            record.setTransstatus(rs.getString("TRANSACTION_STATUS"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public List<ReportTransDTOInter> findRecordDetail_Rep_5_2(String RepMasterID) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT   decrypt(card_no)cardno,\n"
                + "\n"
                + "TRANSACTION_DATE,\n"
                + "\n"
                + "         TRANSACTION_SEQUENCE,\n"
                + "\n"
                + "         COLUMN3,\n"
                + "\n"
                + "         AMOUNT,\n"
                + "\n"
                + "         TRANSACTION_STATUS\n"
                + "\n"
                + "  FROM   DISPUTES\n"
                + "\n"
                + "WHERE   TRANSACTION_DATE > (  SELECT DATE_FROM\n"
                + "\n"
                + "                                   FROM   REPLANISHMENT_MASTER\n"
                + "\n"
                + "                                    WHERE   id = $P{parameter1})\n"
                + "\n"
                + " \n"
                + "\n"
                + "AND TRANSACTION_DATE < (  SELECT DATE_TO\n"
                + "\n"
                + "                                   FROM   REPLANISHMENT_MASTER\n"
                + "\n"
                + "                                    WHERE   id = $P{parameter1})\n"
                + "\n"
                + " \n"
                + "\n"
                + "AND ATM_ID in(  SELECT ATM_ID\n"
                + "\n"
                + "                                   FROM   REPLANISHMENT_MASTER\n"
                + "\n"
                + "                                    WHERE   id = $P{parameter1})\n"
                + "\n"
                + " \n"
                + "\n"
                + "         AND record_type = 3\n"
                + "\n"
                + "         AND matching_type = 2\n"
                + "\n"
                + "         AND trim(column4) = 'ONUS'\n"
                + "\n"
                + "order by TRANSACTION_DATE\n"
                + "";
        selectStat = selectStat.replace("$P{parameter1}", RepMasterID);
        ResultSet rs = executeQuery(selectStat);
        List<ReportTransDTOInter> records = new ArrayList<ReportTransDTOInter>();
        while (rs.next()) {
            ReportTransDTOInter record = DTOFactory.createReportTransDTO();
            record.setCardno(rs.getString("cardno"));
            record.setTransdate(DateFormatter.changeDateAndTimeFormat(rs.getTimestamp("TRANSACTION_DATE")));
            record.setTransseq(rs.getString("TRANSACTION_SEQUENCE"));
            record.setAuth(rs.getString("COLUMN3"));
            record.setAmount(rs.getBigDecimal("AMOUNT").toString());
            record.setTransstatus(rs.getString("TRANSACTION_STATUS"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public List<ReportTransDTOInter> findRecordDetail_Rep_5_3(String RepMasterID) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT   decrypt(card_no)cardno,\n"
                + "\n"
                + "TRANSACTION_DATE,\n"
                + "\n"
                + "         TRANSACTION_SEQUENCE,\n"
                + "\n"
                + "         COLUMN3,\n"
                + "\n"
                + "         AMOUNT,\n"
                + "\n"
                + "         TRANSACTION_STATUS\n"
                + "\n"
                + "  FROM   DISPUTES\n"
                + "\n"
                + "WHERE   TRANSACTION_DATE > (  SELECT DATE_FROM\n"
                + "\n"
                + "                                   FROM   REPLANISHMENT_MASTER\n"
                + "\n"
                + "                                    WHERE   id = $P{parameter1})\n"
                + "\n"
                + " \n"
                + "\n"
                + "AND TRANSACTION_DATE < (  SELECT DATE_TO\n"
                + "\n"
                + "                                   FROM   REPLANISHMENT_MASTER\n"
                + "\n"
                + "                                    WHERE   id = $P{parameter1})\n"
                + "\n"
                + " \n"
                + "\n"
                + "AND ATM_ID in(  SELECT ATM_ID\n"
                + "\n"
                + "                                   FROM   REPLANISHMENT_MASTER\n"
                + "\n"
                + "                                    WHERE   id = $P{parameter1})\n"
                + "\n"
                + " \n"
                + "\n"
                + "         AND record_type = 3\n"
                + "\n"
                + "         AND matching_type = 2\n"
                + "\n"
                + "         AND trim(column4) = 'OFFUS'\n"
                + "\n"
                + "order by TRANSACTION_DATE\n"
                + "";
        selectStat = selectStat.replace("$P{parameter1}", RepMasterID);
        ResultSet rs = executeQuery(selectStat);
        List<ReportTransDTOInter> records = new ArrayList<ReportTransDTOInter>();
        while (rs.next()) {
            ReportTransDTOInter record = DTOFactory.createReportTransDTO();
            record.setCardno(rs.getString("cardno"));
            record.setTransdate(DateFormatter.changeDateAndTimeFormat(rs.getTimestamp("TRANSACTION_DATE")));
            record.setTransseq(rs.getString("TRANSACTION_SEQUENCE"));
            record.setAuth(rs.getString("COLUMN3"));
            record.setAmount(rs.getBigDecimal("AMOUNT").toString());
            record.setTransstatus(rs.getString("TRANSACTION_STATUS"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public List<ReportTransDTOInter> findRecordDetail_Rep_5_4(String RepMasterID) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT   decrypt(card_no)cardno,\n"
                + "\n"
                + "TRANSACTION_DATE,COLUMN1,\n"
                + "\n"
                + "         TRANSACTION_SEQUENCE,\n"
                + "\n"
                + "         COLUMN3,\n"
                + "\n"
                + "         AMOUNT,\n"
                + "(SELECT   replace(NAME,'\\',' ')\n"
                + "   FROM   ATM_FILE\n"
                + "  WHERE   ID = FILE_ID)\n"
                + "   NAME,"
                + "         TRANSACTION_STATUS\n"
                + "FROM   DISPUTES\n"
                + " WHERE   transaction_date > (  SELECT DATE_FROM\n"
                + "\n"
                + "                                   FROM   REPLANISHMENT_MASTER\n"
                + "\n"
                + "                                    WHERE   id = $P{parameter1})\n"
                + "AND TRANSACTION_DATE < (  SELECT DATE_TO\n"
                + "\n"
                + "                                   FROM   REPLANISHMENT_MASTER\n"
                + "\n"
                + "                                    WHERE   id = $P{parameter1})\n"
                + "\n"
                + "AND ATM_ID in(  SELECT ATM_ID\n"
                + "\n"
                + "                                   FROM   REPLANISHMENT_MASTER\n"
                + "\n"
                + "                                    WHERE   id = $P{parameter1})\n"
                + "         AND RECORD_TYPE = 1\n"
                + "         AND MATCHING_TYPE = 1\n"
                + "         AND EXISTS\n"
                + "               (SELECT   1\n"
                + "                  FROM   TRANSACTION_TYPE_MASTER\n"
                + "                 WHERE   id = DISPUTES.TRANSACTION_TYPE_MASTER\n"
                + "                         AND UPPER (name) LIKE '%WITH%')\n"
                + "         AND TRIM (transaction_status) = TRIM ('E*5')\n"
                + "order by TRANSACTION_DATE";
        selectStat = selectStat.replace("$P{parameter1}", RepMasterID);
        ResultSet rs = executeQuery(selectStat);
        List<ReportTransDTOInter> records = new ArrayList<ReportTransDTOInter>();
        while (rs.next()) {
            ReportTransDTOInter record = DTOFactory.createReportTransDTO();
            record.setAmount(rs.getBigDecimal("AMOUNT").toString());
            record.setAuth(rs.getString("COLUMN1"));
            record.setCardno(rs.getString("cardno"));
            record.setTransdate(DateFormatter.changeDateAndTimeFormat(rs.getTimestamp("TRANSACTION_DATE")));
            record.setTransseq(rs.getString("TRANSACTION_SEQUENCE"));
            record.setTranssource(rs.getString("NAME"));
            record.setTransstatus(rs.getString("TRANSACTION_STATUS"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public List<ReportTransDTOInter> findRecordDetail_Rep_6_1(String fromdate, String todate, String atmid) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT                 /*+ parallel INDEX (matched_data REPLANISHMENT_INDEX)*/\n"
                + "       file_id, loading_date, atm_application_id, atm_id, transaction_type, transaction_type_id, currency, currency_id, transaction_status, transaction_status_id, response_code, response_code_id, transaction_date, transaction_sequence, decrypt(card_no)cardno, amount, settlement_date, notes_presented, customer_account_number, transaction_sequence_order_by, transaction_time, matching_type, record_type, match_key, settled_flag, settled_date, settled_user, comments, match_by, match_date, column1, column2, column3, column4, column5, transaction_type_master, response_code_master, card_no_suffix, abs_amount\n"
                + "  FROM   MATCHED_DATA\n"
                + "WHERE   TRANSACTION_DATE BETWEEN TO_DATE (p_fromdate,'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE (p_todate,'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND ATM_APPLICATION_ID = P_ATM_ID\n"
                + "         AND RECORD_TYPE = 2\n"
                + "         AND matching_type = 1\n"
                + "         AND NOT EXISTS\n"
                + "               (SELECT   1\n"
                + "                  FROM   NETWORK_TYPE_CODES\n"
                + "                 WHERE   EXISTS\n"
                + "                            (SELECT   1\n"
                + "                               FROM   NETWORKS\n"
                + "                              WHERE   ID = NETWORK_TYPE_CODES.NETWORK_ID\n"
                + "                                      AND UPPER (NAME) LIKE UPPER ('%ONUS%'))\n"
                + "                         AND CODE = (SUBSTR (DECRYPT (CARD_NO), 1, 6)))\n"
                + "         AND COLUMN4 = (SELECT   SYMBOL\n"
                + "                          FROM   networks\n"
                + "                         WHERE   UPPER (NAME) LIKE UPPER ('%123_ONUS%'))\n"
                + "         AND EXISTS\n"
                + "               (SELECT   1\n"
                + "                  FROM   TRANSACTION_TYPE_MASTER\n"
                + "                 WHERE   id = MATCHED_DATA.TRANSACTION_TYPE_MASTER\n"
                + "                         AND UPPER (name) LIKE '%WITH%') order by atm_application_id,transaction_date";
        selectStat = selectStat.replace("p_fromdate", "'" + fromdate + "'");
        selectStat = selectStat.replace("p_todate", "'" + todate + "'");
        selectStat = selectStat.replace("P_ATM_ID", atmid);
        ResultSet rs = executeQuery(selectStat);
        List<ReportTransDTOInter> records = new ArrayList<ReportTransDTOInter>();
        while (rs.next()) {
            ReportTransDTOInter record = DTOFactory.createReportTransDTO();
            record.setCardno(rs.getString("cardno"));
            record.setTransdate(DateFormatter.changeDateAndTimeFormat(rs.getTimestamp("transaction_date")));
            record.setTransseq(rs.getString("transaction_sequence"));
            record.setAuth(rs.getString("atm_application_id"));
            record.setAmount(rs.getBigDecimal("amount").toString());
            record.setTransstatus(rs.getString("notes_presented"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public List<ReportTransDTOInter> findRecordDetail_Rep_6_2(String fromdate, String todate, String atmid) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT                /*+ parallel INDEX (matched_data REPLANISHMENT_INDEX)*/\n"
                + "       file_id, loading_date, atm_application_id, atm_id, transaction_type, transaction_type_id, currency, currency_id, transaction_status, transaction_status_id, response_code, response_code_id, transaction_date, transaction_sequence, decrypt(card_no)cardno, amount, settlement_date, notes_presented, customer_account_number, transaction_sequence_order_by, transaction_time, matching_type, record_type, match_key, settled_flag, settled_date, settled_user, comments, match_by, match_date, column1, column2, column3, column4, column5, transaction_type_master, response_code_master, card_no_suffix, abs_amount\n"
                + "  FROM   MATCHED_DATA\n"
                + "WHERE   TRANSACTION_DATE BETWEEN TO_DATE (p_fromdate,'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE (p_todate,'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND ATM_APPLICATION_ID = P_ATM_ID\n"
                + "         AND RECORD_TYPE = 2\n"
                + "         AND matching_type = 1  \n"
                + "         AND NOT EXISTS\n"
                + "               (SELECT   1\n"
                + "                  FROM   NETWORK_TYPE_CODES\n"
                + "                 WHERE   EXISTS\n"
                + "                            (SELECT   1\n"
                + "                               FROM   NETWORKS\n"
                + "                              WHERE   ID = NETWORK_TYPE_CODES.NETWORK_ID\n"
                + "                                      AND UPPER (NAME) LIKE UPPER ('%ONUS%'))\n"
                + "                         AND CODE = (SUBSTR (DECRYPT (CARD_NO), 1, 6)))\n"
                + "         AND COLUMN4 = (SELECT   SYMBOL\n"
                + "                          FROM   networks\n"
                + "                         WHERE   UPPER (NAME) LIKE UPPER ('%VISA_ONUS%'))\n"
                + "         AND EXISTS\n"
                + "               (SELECT   1\n"
                + "                  FROM   TRANSACTION_TYPE_MASTER\n"
                + "                 WHERE   id = MATCHED_DATA.TRANSACTION_TYPE_MASTER\n"
                + "                         AND UPPER (name) LIKE '%WITH%') order by atm_application_id,transaction_date";
        selectStat = selectStat.replace("p_fromdate", "'" + fromdate + "'");
        selectStat = selectStat.replace("p_todate", "'" + todate + "'");
        selectStat = selectStat.replace("P_ATM_ID", atmid);
        ResultSet rs = executeQuery(selectStat);
        List<ReportTransDTOInter> records = new ArrayList<ReportTransDTOInter>();
        while (rs.next()) {
            ReportTransDTOInter record = DTOFactory.createReportTransDTO();
            record.setCardno(rs.getString("cardno"));
            record.setTransdate(DateFormatter.changeDateAndTimeFormat(rs.getTimestamp("transaction_date")));
            record.setTransseq(rs.getString("transaction_sequence"));
            record.setAuth(rs.getString("atm_application_id"));
            record.setAmount(rs.getBigDecimal("amount").toString());
            record.setTransstatus(rs.getString("notes_presented"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public String getCurrencyName(String ID, List<CurrencyMasterDTOInter> currencyList) {
        for (CurrencyMasterDTOInter currencyrecord : currencyList) {
            if (("" + currencyrecord.getId()).equals(ID)) {
                return currencyrecord.getSymbol();
            }
        }
        return "UnAssigned";
    }

    public String exportexcel_Rep_6(List<CurrencyMasterDTOInter> currencyList, List<RepStat56DTOInter> SearchRecords) throws Throwable {
        Object[] Space = new Object[1];
        List<String> coloumns = new ArrayList<String>();
        coloumns.add("Debit.Acc.No");
        coloumns.add("Debit.Currency");
        coloumns.add("Debit.Amount");
        coloumns.add("Debit.Value.Date");
        coloumns.add("Credit.Value.Date");
        coloumns.add("Debit.Their.Ref");
        coloumns.add("Credit.Acc.No");
        coloumns.add("Profit.Center.Dep");
        coloumns.add("Company");
        coloumns.add("Indication");
        List<String> subbcolumns = new ArrayList<String>();

        subbcolumns.add("ATM ID");
        subbcolumns.add("Date & Time");
        subbcolumns.add("Card NO");
        subbcolumns.add("Amount");
        subbcolumns.add("Sequence NO");

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Journal Detailed");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        int index = 0;

        for (RepStat56DTOInter Records : SearchRecords) {
            Object[] Headerlist = new Object[2];
            for (int i = 0; i < 2; i++) {
                if (i == 0) {
                    Headerlist[0] = "ATM ID:";
                    sheet.setDefaultColumnWidth(20);
                    index = index + 1;
                }
                if (i == 1) {
                    Headerlist[1] = Records.getAtmid();
                    sheet.setDefaultColumnWidth(20);
                    index = index + 1;
                }
            }
            data.put(index, Headerlist);
            index = index + 1;

            Object[] coloumnslist = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                coloumnslist[i] = coloumns.get(i);
                sheet.setDefaultColumnWidth(20);
            }
            data.put(index, coloumnslist);
            index = index + 1;
            Object[] coloumnsData = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                if ("Debit.Acc.No".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDebitaccountnumber()).toString().replace("null", " ");
                }
                if ("Debit.Currency".equals(coloumns.get(i))) {
                    coloumnsData[i] = getCurrencyName(String.valueOf(Records.getDebitcurrency()).toString().replace("null", " "), currencyList);
                }
                if ("Debit.Amount".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDebitamount()).toString().replace("null", "0");
                }
                if ("Debit.Value.Date".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDebitvaluedate()).toString().replace("null", " ");
                }
                if ("Credit.Value.Date".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getCreditvaluedateto()).toString().replace("null", " ");
                }
                if ("Debit.Their.Ref".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDebittheirref()).toString().replace("null", " ");
                }
                if ("Credit.Acc.No".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getCreditaccnumber()).toString().replace("null", " ");
                }
                if ("Profit.Center.Dep".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getProfitcenterdep()).toString().replace("null", " ");
                }
                if ("Company".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getBranch()).toString().replace("null", " ");
                }
                if ("Indication".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getIndication()).toString().replace("null", " ");
                }

            }
            data.put(index, coloumnsData);
            index = index + 1;

            Object[] ReportHeaderData = new Object[1];
            if (Records.getIndication().toUpperCase().contains("123")) {
                ReportHeaderData[0] = "Detail:";
            } else if (Records.getIndication().toUpperCase().contains("VISA")) {
                ReportHeaderData[0] = "Detail:";
            }

            data.put(index, ReportHeaderData);
            index = index + 1;
            List<ReportTransDTOInter> SubRecords = new ArrayList<ReportTransDTOInter>();
            if (Records.getIndication().toUpperCase().contains("123")) {
                SubRecords = findRecordDetail_Rep_6_1(DateFormatter.changeDateAndTimeFormatprocedure(Records.getFromdate()), DateFormatter.changeDateAndTimeFormatprocedure(Records.getTodate()), Records.getAtmid());
            } else if (Records.getIndication().toUpperCase().contains("VISA")) {
                SubRecords = findRecordDetail_Rep_6_2(DateFormatter.changeDateAndTimeFormatprocedure(Records.getFromdate()), DateFormatter.changeDateAndTimeFormatprocedure(Records.getTodate()), Records.getAtmid());
            }

            if (SubRecords.size() > 0) {

                BigDecimal totalamount = BigDecimal.ZERO;

                Object[] subcoloumnslist = new Object[subbcolumns.size()];
                for (int i = 0; i < subbcolumns.size(); i++) {
                    subcoloumnslist[i] = subbcolumns.get(i);
                    sheet.setDefaultColumnWidth(20);
                }
                data.put(index, subcoloumnslist);
                index = index + 1;

                for (ReportTransDTOInter subRecord : SubRecords) {

                    totalamount = totalamount.add(new BigDecimal(subRecord.getAmount()));

                    Object[] subcoloumnsData = new Object[subbcolumns.size()];
                    for (int i = 0; i < subbcolumns.size(); i++) {
                        if ("ATM ID".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getAuth()).toString().replace("null", " ");
                        }
                        if ("Date & Time".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransdate()).toString().replace("null", " ");
                        }
                        if ("Card NO".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getCardno()).toString().replace("null", " ");
                        }
                        if ("Amount".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getAmount()).toString().replace("null", "0");
                        }
                        if ("Sequence NO".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransseq()).toString().replace("null", " ");
                        }

                    }
                    data.put(index, subcoloumnsData);
                    index = index + 1;
                }

                Object[] TotalData = new Object[2];
                TotalData[0] = "Total Amount:";
                TotalData[1] = totalamount.toString();

                data.put(index, TotalData);
                index = index + 1;
                data.put(index, Space);
                index = index + 1;
                totalamount = BigDecimal.ZERO;

            }
        }
        List<Integer> keyset = new ArrayList<Integer>(data.keySet());
        Collections.sort(keyset);
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            Calendar c = Calendar.getInstance();
            String uri = "Journal Detailed" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".xls";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            pdfPath = pdfPath + uri;
            FileOutputStream out = new FileOutputStream(new File(pdfPath));
            workbook.write(out);
            out.close();
            return pdfPath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String exportexcel_Rep_7(List<CurrencyMasterDTOInter> currencyList, List<RepStat56DTOInter> SearchRecords) throws Throwable {
        Object[] Space = new Object[1];
        List<String> coloumns = new ArrayList<String>();
        coloumns.add("ATM.ID");
        coloumns.add("Debit.Acc.No");
        coloumns.add("Debit.Currency");
        coloumns.add("Debit.Amount");
        coloumns.add("Debit.Value.Date");
        coloumns.add("Credit.Value.Date");
        coloumns.add("Debit.Their.Ref");
        coloumns.add("Credit.Acc.No");
        coloumns.add("Profit.Center.Dep");
        coloumns.add("Company");
        coloumns.add("Indication");

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Journal Summary Report");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        int index = 0;

        for (RepStat56DTOInter Records : SearchRecords) {

            Object[] coloumnslist = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                coloumnslist[i] = coloumns.get(i);
                sheet.setDefaultColumnWidth(20);
            }
            data.put(index, coloumnslist);
            index = index + 1;
            Object[] coloumnsData = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                if ("ATM.ID".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getAtmid()).toString().replace("null", " ");
                }
                if ("Debit.Acc.No".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDebitaccountnumber()).toString().replace("null", " ");
                }
                if ("Debit.Currency".equals(coloumns.get(i))) {
                    coloumnsData[i] = getCurrencyName(String.valueOf(Records.getDebitcurrency()).toString().replace("null", " "), currencyList);
                }
                if ("Debit.Amount".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDebitamount()).toString().replace("null", "0");
                }
                if ("Debit.Value.Date".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDebitvaluedate()).toString().replace("null", " ");
                }
                if ("Credit.Value.Date".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getCreditvaluedateto()).toString().replace("null", " ");
                }
                if ("Debit.Their.Ref".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDebittheirref()).toString().replace("null", " ");
                }
                if ("Credit.Acc.No".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getCreditaccnumber()).toString().replace("null", " ");
                }
                if ("Profit.Center.Dep".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getProfitcenterdep()).toString().replace("null", " ");
                }
                if ("Company".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getBranch()).toString().replace("null", " ");
                }
                if ("Indication".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getIndication()).toString().replace("null", " ");
                }
            }
            data.put(index, coloumnsData);
            index = index + 1;
            data.put(index, Space);
            index = index + 1;
        }
        List<Integer> keyset = new ArrayList<Integer>(data.keySet());

        Collections.sort(keyset);
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            Calendar c = Calendar.getInstance();
            String uri = "Statement Report" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".xls";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            pdfPath = pdfPath + uri;
            FileOutputStream out = new FileOutputStream(new File(pdfPath));
            workbook.write(out);
            out.close();
            return pdfPath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }
}
