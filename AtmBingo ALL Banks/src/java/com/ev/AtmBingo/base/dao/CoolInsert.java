/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.base.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.DisputeReportDTOInter;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class CoolInsert extends BaseBO implements Serializable {

    public CoolInsert() {
        super();
    }

    public Object GetStatment() throws Throwable {

        //DisputeReportDTOInter uDTO = (DisputeReportDTOInter) obj[0];
        String insertStat = "insert into disputes_report\n"
                + "  (atm_application_id, atm_id, transaction_type, transaction_type_id, currency, currency_id, transaction_status, "
                + "   transaction_status_id, response_code, response_code_id, transaction_date, transaction_sequence, card_no, amount, "
                + "   settlement_date, notes_presented, customer_account_number, transaction_sequence_order_by, transaction_time, matching_type, "
                + "   record_type, dispute_key, dispute_reason, settled_flag, settled_date, settled_user, comments, dispute_by, dispute_date, "
                + "   dispute_with_roles, column1, column2, column3, column4, column5, transaction_type_master, response_code_master, card_no_suffix, "
                + "   user_id, corrective_date, corrective_user ,export_date ,export_user)\n"
                + "   values\n"
                + "  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, to_date(?,'dd.mm.yyyy hh24:mi:ss'), ?,  " + super.getencryptcard() + ", ?, to_date(?,'dd.mm.yyyy hh24:mi:ss'), ?, ?, ?, "
                + "  to_date(?,'dd.mm.yyyy hh24:mi:ss'), ?, ?, ?, ?, ?, to_date(?,'dd.mm.yyyy hh24:mi:ss'), ?, ?, ?, to_date(?,'dd.mm.yyyy hh24:mi:ss'), ?, ?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, to_date(?,'dd.mm.yyyy hh24:mi:ss'), ?, to_date(?,'dd.mm.yyyy hh24:mi:ss'), ?)";
        return insertStat;
    }

    public void save(List<DisputeReportDTOInter> entities) throws SQLException, Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {

            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(GetStatment().toString());
            for (int i = 0; i < entities.size(); i++) {
                DisputeReportDTOInter uDTO = entities.get(i);
                //  statement.setString(1, "");
                statement.setInt(30, uDTO.getDisputeWithRoles());
                statement.setString(1, uDTO.getAtmApplicationId());
                statement.setInt(2, uDTO.getAtmId().getId());
                statement.setString(3, uDTO.getTransactionType());
                statement.setInt(4, uDTO.getTransactionTypeId().getId());
                statement.setString(5, uDTO.getCurrency());
                statement.setInt(6, uDTO.getCurrencyId().getId());
                statement.setString(7, uDTO.getTransactionStatus());
                statement.setInt(8, uDTO.getTransactionStatusId().getId());
                statement.setString(9, uDTO.getResponseCode());
                statement.setInt(10, uDTO.getResponseCodeId().getId());
                statement.setString(11, DateFormatter.changeDateAndTimeFormat(uDTO.getTransactionDate()).toString());
                statement.setString(12, uDTO.getTransactionSeqeunce());
                statement.setString(13, uDTO.getCardNo());
                statement.setInt(14, uDTO.getAmount());
                statement.setString(15, DateFormatter.changeDateAndTimeFormat(uDTO.getSettlementDate()).toString());
                statement.setString(16, uDTO.getNotesPresented());
                if (uDTO.getCustomerAccountNumber() != null) {
                    statement.setString(17, uDTO.getCustomerAccountNumber());
                } else {
                    statement.setString(17, "");
                }
                statement.setInt(18, uDTO.getTransactionSequenceOrderBy());
                statement.setString(19, DateFormatter.changeDateAndTimeFormat(uDTO.getTransactionTime()).toString());
                statement.setInt(20, uDTO.getMatchingType());
                statement.setInt(21, uDTO.getRecordType());
                statement.setInt(22, uDTO.getDisputeKey());
                statement.setString(23, uDTO.getDisputeReason());
                statement.setInt(24, uDTO.getSettledFlag());
                statement.setString(25, DateFormatter.changeDateAndTimeFormat(uDTO.getSettledDate()).toString());
                statement.setInt(26, uDTO.getSettledUser().getUserId());

                if (uDTO.getComments() != null) {
                    statement.setString(27, uDTO.getComments());
                } else {
                    statement.setString(27, "");
                }
                statement.setInt(28, uDTO.getDisputeBy());
                statement.setString(29, DateFormatter.changeDateAndTimeFormat(uDTO.getDisputeDate()).toString());

                if (uDTO.getColumn1() != null) {
                    statement.setString(31, uDTO.getColumn1());
                } else {
                    statement.setString(31, uDTO.getColumn1());
                }

                if (uDTO.getColumn2() != null) {
                    statement.setString(32, uDTO.getColumn2());
                } else {
                    statement.setString(32, uDTO.getColumn2());
                }

                if (uDTO.getColumn3() != null) {
                    statement.setString(33, uDTO.getColumn3());
                } else {
                    statement.setString(33, uDTO.getColumn3());
                }

                if (uDTO.getColumn4() != null) {
                    statement.setString(34, uDTO.getColumn4());
                } else {
                    statement.setString(34, uDTO.getColumn4());
                }

                if (uDTO.getColumn5() != null) {
                    statement.setString(35, uDTO.getColumn5());
                } else {
                    statement.setString(35, uDTO.getColumn5());
                }

                statement.setInt(36, uDTO.getTransactionTypeMaster());
                statement.setInt(37, uDTO.getResponseCodeMaster());
                statement.setString(38, uDTO.getCardNoSuffix());
                statement.setInt(39, uDTO.getUserId());
                statement.setString(40, DateFormatter.changeDateAndTimeFormat(uDTO.getCorrectivedate()));
                statement.setString(41, uDTO.getCorrectiveuser());
                statement.setString(42, DateFormatter.changeDateAndTimeFormat(uDTO.getExportdate()));
                statement.setString(43, uDTO.getExportuser());

                // statement.setClob(40,  uDTO.getTransactiondata());
                // System.out.println(statement.getMetaData().getTableName(1));
                statement.addBatch();

                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
    }
}
