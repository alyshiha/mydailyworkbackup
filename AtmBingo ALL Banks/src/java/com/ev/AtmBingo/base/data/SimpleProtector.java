package com.ev.AtmBingo.base.data;


import java.io.Serializable;
import java.security.*;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.*;

/**
 *
//ThisIsASecretKey
 */
public class SimpleProtector  implements Serializable{

    private static final String ALGORITHM = "AES";
    private static final byte[] keyValue = new byte[]{'T', 'h', 'i', 's', 'I', 's', 'A', 'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y'};

    public static String encrypt(String valueToEnc,String keyValue) throws Exception {
        byte[] keyVal = keyValue.getBytes();
        Key key = generateKey(keyVal);
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encValue = c.doFinal(valueToEnc.getBytes());
        String encryptedValue = new BASE64Encoder().encode(encValue);
        return encryptedValue;
    }

    public static String decrypt(String encryptedValue,String keyValue) throws Exception {
         byte[] keyVal = keyValue.getBytes();
        Key key = generateKey(keyVal);
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedValue);
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }

    private static Key generateKey(byte[] keyValue) throws Exception {
        Key key = new SecretKeySpec(keyValue, ALGORITHM); // SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM); // key = keyFactory.generateSecret(new DESKeySpec(keyValue)); return key; } - See more at: http://www.digizol.com/2009/10/java-encrypt-decrypt-jce-salt.html#sthash.7iP1001l.dpuf
        return key;
    }

    public static void main(String[] args) throws Exception {
        String password = "test_123";
        String key = "ThisIsASecretKey";
        String passwordEnc = SimpleProtector.encrypt(password,key);
        String passwordDec = SimpleProtector.decrypt(passwordEnc,key);
        
       // System.out.println("Encrypted : " + passwordEnc);
        
    }
}
