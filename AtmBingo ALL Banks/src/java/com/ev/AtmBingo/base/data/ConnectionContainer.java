/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.base.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Map;

/**
 *
 * @author Administrator
 */
public class ConnectionContainer  implements Serializable{

    HashMap<String, Integer> Connections = new HashMap <String, Integer>();

    public void add(String ConnectionsID, Integer Status) {
        Connections.put(ConnectionsID, new Integer(Status));
    }

 /*   public  void UpdateState(String ConnectionsID, Integer Status) {
        Connections.remove(ConnectionsID);
        add(ConnectionsID, new Integer(Status));
    }*/
public void UpdateState(String ConnectionsID, Integer Status) {
      Set s = Connections.entrySet();
        Iterator i = s.iterator();
        while (i.hasNext())
        {
        Map.Entry m = (Map.Entry) i.next();
        if( m.getKey().equals(ConnectionsID) ) {
            m.setValue(Status);
            break;
        }
    }
}

    private Integer GetState(String ConnectionsID) {
        Integer n = (Integer) Connections.get(ConnectionsID);
        return n;
    }

    public Integer Find() {
        Set s = Connections.entrySet();
        Iterator i = s.iterator();
        Integer State = 3;
        while (i.hasNext()) {
            Map.Entry m = (Map.Entry) i.next();
            String value = (String) m.getKey();
            State = Integer.parseInt(m.getValue().toString());
            if (State == 0) {
                UpdateState(value, 1);
               // Connection
                return Integer.parseInt(value);
            }
        }
        
        return Find();
    }
}
