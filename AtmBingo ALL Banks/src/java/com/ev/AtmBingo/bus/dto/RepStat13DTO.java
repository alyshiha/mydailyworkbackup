/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class RepStat13DTO implements RepStat13DTOInter {
    private Timestamp datefrom;
    private Timestamp dateto;
    private Integer cashadded;
    private Integer remainingcash;
    private Integer dispensedcash;
    private Integer switchtotal;
    private Integer switchpending;
    private Integer switchover;
    private String atmid;
    private String repid;
    private String atmname;
    private Integer dispute;
    private Integer cashover;
    private Integer cashshortage;

    public String getRepid() {
        return repid;
    }

    public void setRepid(String repid) {
        this.repid = repid;
    }

    public Integer getCashover() {
        return cashover;
    }

    public void setCashover(Integer cashover) {
        this.cashover = cashover;
    }

    public Integer getCashshortage() {
        return cashshortage;
    }

    public void setCashshortage(Integer cashshortage) {
        this.cashshortage = cashshortage;
    }

    public String getAtmname() {
        return atmname;
    }

    public void setAtmname(String atmname) {
        this.atmname = atmname;
    }
    

    public RepStat13DTO() {
    }

    public RepStat13DTO(Timestamp datefrom, Timestamp dateto, Integer cashadded, Integer remainingcash, Integer dispensedcash, Integer switchtotal, Integer switchpending, Integer switchover,String atmid,Integer dispute) {
        this.datefrom = datefrom;
        this.dateto = dateto;
        this.cashadded = cashadded;
        this.remainingcash = remainingcash;
        this.dispensedcash = dispensedcash;
        this.switchtotal = switchtotal;
        this.switchpending = switchpending;
        this.switchover = switchover;
        this.atmid = atmid;
        this.dispute = dispute;
    }

    public Integer getDispute() {
        return dispute;
    }

    public void setDispute(Integer dispute) {
        this.dispute = dispute;
    }

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    @Override
    public Timestamp getDatefrom() {
        return datefrom;
    }

    @Override
    public void setDatefrom(Timestamp datefrom) {
        this.datefrom = datefrom;
    }

    @Override
    public Timestamp getDateto() {
        return dateto;
    }

    @Override
    public void setDateto(Timestamp dateto) {
        this.dateto = dateto;
    }

    @Override
    public Integer getCashadded() {
        return cashadded;
    }

    @Override
    public void setCashadded(Integer cashadded) {
        this.cashadded = cashadded;
    }

    @Override
    public Integer getRemainingcash() {
        return remainingcash;
    }

    @Override
    public void setRemainingcash(Integer remainingcash) {
        this.remainingcash = remainingcash;
    }

    @Override
    public Integer getDispensedcash() {
        return dispensedcash;
    }

    @Override
    public void setDispensedcash(Integer dispensedcash) {
        this.dispensedcash = dispensedcash;
    }

    @Override
    public Integer getSwitchtotal() {
        return switchtotal;
    }

    @Override
    public void setSwitchtotal(Integer switchtotal) {
        this.switchtotal = switchtotal;
    }

    @Override
    public Integer getSwitchpending() {
        return switchpending;
    }

    @Override
    public void setSwitchpending(Integer switchpending) {
        this.switchpending = switchpending;
    }

    @Override
    public Integer getSwitchover() {
        return switchover;
    }

    @Override
    public void setSwitchover(Integer switchover) {
        this.switchover = switchover;
    }
    
}
