/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface AtmJournalDTOInter {
    String getCurrencyname();
    
     void setCurrencyname(String currencyname);

    String toCSVString(String Separator);

    String getBranch();

    void setBranch(String branch);

    String getIndication();

    void setIndication(String indication);

    String getAtmid();

    void setAtmid(String atmid);

    void setExportflag(int exportflag);

    int getExportflag();

    void setOperation(int operation);

    int getOperation();

    void setRepid(int repid);

    int getRepid();

    int getAmount();

    int getAtmjournalid();

    String getCreditaccount();

    int getCurrency();

    int getCurrencyrate();

    Date getDatefrom();

    Date getDateto();

    String getDebitaccount();

    Date getJournaldate();

    String getJournalrefernce();

    String getTransactiontype();

    void setAmount(int amount);

    void setAtmjournalid(int atmjournalid);

    void setCreditaccount(String creditaccount);

    void setCurrency(int currency);

    void setCurrencyrate(int currencyrate);

    void setDatefrom(Date datefrom);

    void setDateto(Date dateto);

    void setDebitaccount(String debitaccount);

    void setJournaldate(Date journaldate);

    void setJournalrefernce(String journalrefernce);

    void setTransactiontype(String transactiontype);

}
