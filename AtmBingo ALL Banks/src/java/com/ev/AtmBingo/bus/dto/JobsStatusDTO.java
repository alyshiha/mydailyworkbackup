/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class JobsStatusDTO extends BaseDTO implements JobsStatusDTOInter, Serializable{

    private int stackId;
    private int loading;
    private int loadingJobId;
    private int journalValidation;
    private int journalValidationJobId;
    private int switchValidation;
    private int switchValidationJobId;
    private int hostValidation;
    private int hostValidationJobId;
    private int matching;
    private int matchingJobId;

    public int getHostValidation() {
        return hostValidation;
    }

    public void setHostValidation(int hostValidation) {
        this.hostValidation = hostValidation;
    }

    public int getHostValidationJobId() {
        return hostValidationJobId;
    }

    public void setHostValidationJobId(int hostValidationJobId) {
        this.hostValidationJobId = hostValidationJobId;
    }

    public int getJournalValidation() {
        return journalValidation;
    }

    public void setJournalValidation(int journalValidation) {
        this.journalValidation = journalValidation;
    }

    public int getJournalValidationJobId() {
        return journalValidationJobId;
    }

    public void setJournalValidationJobId(int journalValidationJobId) {
        this.journalValidationJobId = journalValidationJobId;
    }

    public int getLoading() {
        return loading;
    }

    public void setLoading(int loading) {
        this.loading = loading;
    }

    public int getLoadingJobId() {
        return loadingJobId;
    }

    public void setLoadingJobId(int loadingJobId) {
        this.loadingJobId = loadingJobId;
    }

    public int getMatching() {
        return matching;
    }

    public void setMatching(int matching) {
        this.matching = matching;
    }

    public int getMatchingJobId() {
        return matchingJobId;
    }

    public void setMatchingJobId(int matchingJobId) {
        this.matchingJobId = matchingJobId;
    }

    public int getStackId() {
        return stackId;
    }

    public void setStackId(int stackId) {
        this.stackId = stackId;
    }

    public int getSwitchValidation() {
        return switchValidation;
    }

    public void setSwitchValidation(int switchValidation) {
        this.switchValidation = switchValidation;
    }

    public int getSwitchValidationJobId() {
        return switchValidationJobId;
    }

    public void setSwitchValidationJobId(int switchValidationJobId) {
        this.switchValidationJobId = switchValidationJobId;
    }
    
    protected JobsStatusDTO() {
        super();
    }

}
