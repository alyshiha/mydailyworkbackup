/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author KhAiReE
 */
public interface UserWorkDTOInter extends Serializable {

    String getName();

    int getValue();

    void setName(String name);

    void setValue(int value);

}
