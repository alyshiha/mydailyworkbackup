/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author AlyShiha
 */
public interface RepStat56DTOInter {

    void setTodate(Timestamp todate);

    Timestamp getTodate();

    void setFromdate(Timestamp fromdate);

    Timestamp getFromdate();

    void setAtmid(String atmid);

    String getAtmid();

    String getBranch();

    String getCreditaccnumber();

    Timestamp getCreditvaluedateto();

    String getDebitaccountnumber();

    BigDecimal getDebitamount();

    Integer getDebitcurrency();

    String getDebittheirref();

    Timestamp getDebitvaluedate();

    String getIndication();

    String getProfitcenterdep();

    void setBranch(String branch);

    void setCreditaccnumber(String creditaccnumber);

    void setCreditvaluedateto(Timestamp creditvaluedateto);

    void setDebitaccountnumber(String debitaccountnumber);

    void setDebitamount(BigDecimal debitamount);

    void setDebitcurrency(Integer debitcurrency);

    void setDebittheirref(String debittheirref);

    void setDebitvaluedate(Timestamp debitvaluedate);

    void setIndication(String indication);

    void setProfitcenterdep(String profitcenterdep);

}
