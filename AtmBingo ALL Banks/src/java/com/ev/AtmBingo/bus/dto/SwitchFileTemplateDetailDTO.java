/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Administrator
 */
public class SwitchFileTemplateDetailDTO extends BaseDTO implements Serializable, SwitchFileTemplateDetailDTOInter{

    private int id;
    private int twmplate;
    private int columnId;
    private BigDecimal position;
    private String format,decimalpointposition;
    private String format2;
    private BigDecimal dataType;
    private BigDecimal length;
    private BigDecimal lineNumber;
    private int loadWhenMapped;
    private BigDecimal lengthDir;
    private Boolean loadWhenMappedB;
    private BigDecimal startingPosition;
private Boolean mandatory,negativeflag,decimalpoint;
private String tagstring;
public Boolean getDecimalpoint() {
        return decimalpoint;
    }

    public void setDecimalpoint(Boolean decimalpoint) {
        this.decimalpoint = decimalpoint;
    }

    public String getDecimalpointposition() {
        return decimalpointposition;
    }

    public void setDecimalpointposition(String decimalpointposition) {
        this.decimalpointposition = decimalpointposition;
    }
    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public Boolean getNegativeflag() {
        return negativeflag;
    }

    public void setNegativeflag(Boolean negativeflag) {
        this.negativeflag = negativeflag;
    }

    public String getTagstring() {
        return tagstring;
    }

    public void setTagstring(String tagstring) {
        this.tagstring = tagstring;
    }



    public BigDecimal getStartingPosition() {
        return startingPosition;
    }

    public void setStartingPosition(BigDecimal startingPosition) {
        this.startingPosition = startingPosition;
    }


    public Boolean getLoadWhenMappedB() {
        loadWhenMappedB = loadWhenMapped == 1;
        return loadWhenMappedB;
    }

    public void setLoadWhenMappedB(Boolean loadWhenMappedB) {
        if(loadWhenMappedB)
            loadWhenMapped =1;
        else
            loadWhenMapped = 2;
        this.loadWhenMappedB = loadWhenMappedB;
    }
    
    public int getColumnId() {
        return columnId;
    }

    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }

    public BigDecimal getDataType() {
        return dataType;
    }

    public void setDataType(BigDecimal dataType) {
        this.dataType = dataType;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getFormat2() {
        return format2;
    }

    public void setFormat2(String format2) {
        this.format2 = format2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getLength() {
        return length;
    }

    public void setLength(BigDecimal length) {
        this.length = length;
    }

    public BigDecimal getLengthDir() {
        return lengthDir;
    }

    public void setLengthDir(BigDecimal lengthDir) {
        this.lengthDir = lengthDir;
    }

    public BigDecimal getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(BigDecimal lineNumber) {
        this.lineNumber = lineNumber;
    }

    public int getLoadWhenMapped() {
        return loadWhenMapped;
    }

    public void setLoadWhenMapped(int loadWhenMapped) {
        this.loadWhenMapped = loadWhenMapped;
    }

    public BigDecimal getPosition() {
        return position;
    }

    public void setPosition(BigDecimal position) {
        this.position = position;
    }

    public int getTwmplate() {
        return twmplate;
    }

    public void setTwmplate(int twmplate) {
        this.twmplate = twmplate;
    }
    
    protected SwitchFileTemplateDetailDTO() {
        super();
    }

}
