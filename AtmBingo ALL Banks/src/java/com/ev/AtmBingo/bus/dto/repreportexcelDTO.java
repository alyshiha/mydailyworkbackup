/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class repreportexcelDTO implements repreportexcelDTOInter , Serializable {
    private String coloumn1,coloumn2,coloumn3,coloumn4,coloumn5,coloumn6,coloumn7,coloumn8,coloumn9,coloumn10,coloumn11,coloumn12,coloumn13,coloumn14,coloumn15;

    @Override
    public String getColoumn1() {
        return coloumn1;
    }

    @Override
    public void setColoumn1(String coloumn1) {
        this.coloumn1 = coloumn1;
    }

    @Override
    public String getColoumn10() {
        return coloumn10;
    }

    @Override
    public void setColoumn10(String coloumn10) {
        this.coloumn10 = coloumn10;
    }

    @Override
    public String getColoumn11() {
        return coloumn11;
    }

    @Override
    public void setColoumn11(String coloumn11) {
        this.coloumn11 = coloumn11;
    }

    @Override
    public String getColoumn12() {
        return coloumn12;
    }

    @Override
    public void setColoumn12(String coloumn12) {
        this.coloumn12 = coloumn12;
    }

    @Override
    public String getColoumn13() {
        return coloumn13;
    }

    @Override
    public void setColoumn13(String coloumn13) {
        this.coloumn13 = coloumn13;
    }

    @Override
    public String getColoumn14() {
        return coloumn14;
    }

    @Override
    public void setColoumn14(String coloumn14) {
        this.coloumn14 = coloumn14;
    }

    @Override
    public String getColoumn15() {
        return coloumn15;
    }

    @Override
    public void setColoumn15(String coloumn15) {
        this.coloumn15 = coloumn15;
    }

    @Override
    public String getColoumn2() {
        return coloumn2;
    }

    @Override
    public void setColoumn2(String coloumn2) {
        this.coloumn2 = coloumn2;
    }

    @Override
    public String getColoumn3() {
        return coloumn3;
    }

    @Override
    public void setColoumn3(String coloumn3) {
        this.coloumn3 = coloumn3;
    }

    @Override
    public String getColoumn4() {
        return coloumn4;
    }

    @Override
    public void setColoumn4(String coloumn4) {
        this.coloumn4 = coloumn4;
    }

    @Override
    public String getColoumn5() {
        return coloumn5;
    }

    @Override
    public void setColoumn5(String coloumn5) {
        this.coloumn5 = coloumn5;
    }

    @Override
    public String getColoumn6() {
        return coloumn6;
    }

    @Override
    public void setColoumn6(String coloumn6) {
        this.coloumn6 = coloumn6;
    }

    @Override
    public String getColoumn7() {
        return coloumn7;
    }

    @Override
    public void setColoumn7(String coloumn7) {
        this.coloumn7 = coloumn7;
    }

    @Override
    public String getColoumn8() {
        return coloumn8;
    }

    @Override
    public void setColoumn8(String coloumn8) {
        this.coloumn8 = coloumn8;
    }

    @Override
    public String getColoumn9() {
        return coloumn9;
    }

    @Override
    public void setColoumn9(String coloumn9) {
        this.coloumn9 = coloumn9;
    }
    
    
}
