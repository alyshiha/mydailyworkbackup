/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface BlackListedCardDTOInter extends Serializable{

    Date getBlackListedDate();

    String getCardNo();

    String getComments();

    String getCustomerNo();

    void setBlackListedDate(Date blackListedDate);

    void setCardNo(String cardNo);

    void setComments(String comments);

    void setCustomerNo(String customerNo);

}
