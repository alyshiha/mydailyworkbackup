/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.sql.Timestamp;

/**
 *
 * @author AlyShiha
 */
public class RepStat4DTO implements RepStat4DTOInter {

    private String id;
    private Timestamp datefrom;
    private Timestamp dateto;
    private String atm;
    private String atmid;
    private Integer switchamount;
    private Integer hostamount;
    private Integer offsamount;
    private Integer switchdifference;
    private Integer hostdifference;
    private Integer offusdifference;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    public String getAtm() {
        return atm;
    }

    public void setAtm(String atm) {
        this.atm = atm;
    }

    @Override
    public Timestamp getDatefrom() {
        return datefrom;
    }

    @Override
    public void setDatefrom(Timestamp datefrom) {
        this.datefrom = datefrom;
    }

    @Override
    public Timestamp getDateto() {
        return dateto;
    }

    @Override
    public void setDateto(Timestamp dateto) {
        this.dateto = dateto;
    }

    @Override
    public Integer getSwitchamount() {
        return switchamount;
    }

    @Override
    public void setSwitchamount(Integer switchamount) {
        this.switchamount = switchamount;
    }

    @Override
    public Integer getHostamount() {
        return hostamount;
    }

    @Override
    public void setHostamount(Integer hostamount) {
        this.hostamount = hostamount;
    }

    @Override
    public Integer getOffsamount() {
        return offsamount;
    }

    @Override
    public void setOffsamount(Integer offsamount) {
        this.offsamount = offsamount;
    }

    @Override
    public Integer getSwitchdifference() {
        return switchdifference;
    }

    @Override
    public void setSwitchdifference(Integer switchdifference) {
        this.switchdifference = switchdifference;
    }

    @Override
    public Integer getHostdifference() {
        return hostdifference;
    }

    @Override
    public void setHostdifference(Integer hostdifference) {
        this.hostdifference = hostdifference;
    }

    @Override
    public Integer getOffusdifference() {
        return offusdifference;
    }

    @Override
    public void setOffusdifference(Integer offusdifference) {
        this.offusdifference = offusdifference;
    }

}
