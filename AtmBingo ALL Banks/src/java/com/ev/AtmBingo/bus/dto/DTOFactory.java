package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.bus.dao.AVGDailyDispenseDTO;
import com.ev.AtmBingo.bus.dao.AVGDailyDispenseDTOInter;

public class DTOFactory {

    public DTOFactory() {
    }

    public static StatementDTOInter createStatementDTO(Object... obj) {
        return new StatementDTO();
    }

    public static DepositTransactionDTOInter createDepositTransactionDTO(Object... obj) {
        return new DepositTransactionDTO();
    }

    public static DepositDTOInter createDepositDTO(Object... obj) {
        return new DepositDTO();
    }

    public static HolidaysDTOInter_1 createHolidaysDTO_1(Object... obj) {
        return new HolidaysDTO_1();
    }

    public static RepStat56DTOInter createReportRepStat56DTO(Object... obj) {
        return new RepStat56DTO();
    }

    public static ReportStatDetailDTOInter createReportStatDetailDTO(Object... obj) {
        return new ReportStatDetailDTO();
    }

    public static ReportTransDTOInter createReportTransDTO(Object... obj) {
        return new ReportTransDTO();
    }

    public static invalidReplanishmentsDTOInter createinvalidReplanishmentsDTO(Object... obj) {
        return new invalidReplanishmentsDTO();
    }

    public static CashManagmentLogDTOInter createCashManagmentLogDTO(Object... obj) {
        return new CashManagmentLogDTO();
    }

    public static userPrivilegeDTOInter createuserPrivilegeDTO(Object... obj) {
        return new userPrivilegeDTO();
    }

    public static privilegeStatusNameDTOInter createprivilegeStatusNameDTO(Object... obj) {
        return new privilegeStatusNameDTO();
    }

    public static matchingErrorsDataDTOInter creatematchingErrorsDataDTO(Object... obj) {
        return new matchingErrorsDataDTO();
    }

    public static AccountNameDTOInter createAccountNameDTO(Object... obj) {
        return new AccountNameDTO();
    }

    public static ExceptionLoadingErrorsDTOInter createExceptionLoadingErrorsDTO(Object... obj) {
        return new ExceptionLoadingErrorsDTO();
    }

    public static LoadingInsertErrorsDTOInter createLoadingInsertErrorsDTO(Object... obj) {
        return new LoadingInsertErrorsDTO();
    }

    public static replanishmentCashManagementDTOInter createreplanishmentCashManagementDTO(Object... obj) {
        return new replanishmentCashManagementDTO();
    }

    public static AtmJournalDTOInter createAtmJournalDTO(Object... obj) {
        return new AtmJournalDTO();
    }

    public static NetworkTypeCodeDTOInter createNetworkTypeCodeDTO(Object... obj) {
        return new NetworkTypeCodeDTO();
    }

    public static NetworksDTOInter createNetworksDTO(Object... obj) {
        return new NetworksDTO();
    }

    public static AtmAccountsDTOInter createAtmAccountsDTO(Object... obj) {
        return new AtmAccountsDTO();
    }

    public static RepStat2DTOInter createRepStat2DTO(Object... obj) {
        return new RepStat2DTO();
    }

    public static RepStat4DTOInter createRepStat4DTO(Object... obj) {
        return new RepStat4DTO();
    }

    public static RepStat13DTOInter createRepStat13DTO(Object... obj) {
        return new RepStat13DTO();
    }

    public static RepPopDTOInter createRepPopDTO(Object... obj) {
        return new RepPopDTO();
    }

    public static NotePresentedDTOInter createNotePresentedDTO(Object... obj) {
        return new NotePresentedDTO();
    }

    public static matcheddataDuplicateDTOInter creatematcheddataDuplicateDTO(Object... obj) {
        return new matcheddataDuplicateDTO();
    }

    public static CardsTakenReportDTOInter createCardsTakenReportDTO(Object... obj) {
        return new CardsTakenReportDTO();
    }

    public static bybranchreportDTOInter createbybranchreportDTO(Object... obj) {
        return new bybranchreportDTO();
    }

    public static branchcodeDTOInter createbranchcodeDTO(Object... obj) {
        return new branchcodeDTO();
    }

    public static BranchReasonDTOInter createbranchreasonDTO(Object... obj) {
        return new BranchReasonDTO();
    }

    public static allbranchtransactionssearchDTOInter createallbranchtransactionssearchDTO(Object... obj) {
        return new allbranchtransactionssearchDTO();
    }

    public static allbranchtransactionsDTOInter createallbranchtransactionsDTO(Object... obj) {
        return new allbranchtransactionsDTO();
    }

    public static usersbranchDTOInter createusersbranchDTO(Object... obj) {
        return new usersbranchDTO();
    }

    public static useratmbranchDTOInter createuseratmbranchDTO(Object... obj) {
        return new useratmbranchDTO();
    }

    public static menulabelsDTOInter createmenulabelsDTO(Object... obj) {
        return new menulabelsDTO();
    }

    public static PrintcardtakenExcelInter createprintcardtakenexcelinter(Object... obj) {
        return new PrintcardtakenExcel();
    }

    public static replanishmentHistoryRepDTOInter createreplanishmentHistoryRepDTO(Object... obj) {
        return new replanishmentHistoryRepDTO();
    }

    public static repreportcassetteexcelDTOInter createrepreportcassetteexcelDTO(Object... obj) {
        return new repreportcassetteexcelDTO();
    }

    public static repreportexcelDTOInter createrepreportexcelDTO(Object... obj) {
        return new repreportexcelDTO();
    }

    public static disstatprintDTOInter createdisstatprintDTO(Object... obj) {
        return new disstatprintDTO();
    }

    public static AVGDailyDispenseDTOInter createAVGDailyDispenseDTO(Object... obj) {
        return new AVGDailyDispenseDTO();
    }

    public static swglDTOInter createswglDTO(Object... obj) {
        return new swglDTO();
    }

    public static statbytransDTOInter createstatbytransDTO(Object... obj) {
        return new statbytransDTO();
    }

    public static atmremainingDTOInter createatmremainingDTO(Object... obj) {
        return new atmremainingDTO();
    }

    public static cardissuerDTOInter createcardissuerDTO(Object... obj) {
        return new cardissuerDTO();
    }

    public static bycardDTOInter createbycardDTO(Object... obj) {
        return new bycardDTO();
    }

    public static TotalWithDrawalsDTOInter createTotalWithDrawalsDTO(Object... obj) {
        return new TotalWithDrawalsDTO();
    }

    public static MissingJournalDTOInter createMissingJournalDTO(Object... obj) {
        return new MissingJournalDTO();
    }

    public static TransStatDTOInter createTransStatDTO(Object... obj) {
        return new TransStatDTO();
    }

    public static BingostatsearchDTOInter createBingostatsearchDTO(Object... obj) {
        return new BingostatsearchDTO();
    }

    public static DuplicateListDTOInter createDuplicateListDTO(Object... obj) {
        return new DuplicateListDTO();
    }

    public static LincManagmentDetailsDTOInter createLincManagmentDetailsDTO(Object... obj) {
        return new LincManagmentDetailsDTO();
    }

    public static EXPORTTEMPLETEDETAILSDTOInter createEXPORTTEMPLETEDETAILSDTO(Object... obj) {
        return new EXPORTTEMPLETEDETAILSDTO();
    }

    public static DisputeRulesDTOInter createDisputeRulesDTO(Object... obj) {
        return new DisputeRulesDTO();
    }

    public static EXPORTTEMPLETEDTOInter createEXPORTTEMPLETEDTO(Object... obj) {
        return new EXPORTTEMPLETEDTO();
    }

    public static ExportLogDTOInter createExportLogDTO(Object... obj) {
        return new ExportLogDTO();
    }

    public static CorrectiveEntryLogDTOInter createCorrectiveEntryLogDTO(Object... obj) {
        return new CorrectiveEntryLogDTO();
    }

    public static AutoRepDTOInter createAutoRepDTO(Object... obj) {
        return new AutoRepDTO();
    }

    public static DeleteTransactionsDataDTOInter createDeleteTransactionsDataDTO(Object... obj) {
        return new DeleteTransactionsDataDTO();
    }

    public static LicenseDTOInter createLicenseDTO(Object... obj) {
        return new LicenseDTO();
    }

    public static PendingAtmsDTOInter createPendingAtmsDTO(Object... obj) {
        return new PendingAtmsDTO();
    }

    public static ATMMAchineLICInter createATMMAchineLIC(Object... obj) {
        return new ATMMAchineLIC();
    }

    public static ReplanishmentTempletesDetDTOInter createReplanishmentTempletesDetDTO(Object... obj) {
        return new ReplanishmentTempletesDetDTO();
    }

    public static SettlementLogDTOInter createSettlementLogDTO(Object... obj) {
        return new SettlementLogDTO();
    }

    public static ReplanishmentTempletesDTOInter createReplanishmentTempletesDTO(Object... obj) {
        return new ReplanishmentTempletesDTO();
    }

    public static BingoLogDTOInter createBingoLogDTO(Object... obj) {
        return new BingoLogDTO();
    }

    public static NotificationDTOInter createNotificationDTO(Object... obj) {
        return new NotificationDTO();
    }

    public static DefualtColumnsDTOInter createDefualtColumnsDTO(Object... obj) {
        return new DefualtColumnsDTO();
    }

    public static ReplanishmentReportDTOInter createReplanishmentReportDTO(Object... obj) {
        return new ReplanishmentReportDTO();
    }

    public static UserWorkDTOInter createUserWorkDTO(Object... obj) {
        return new UserWorkDTO();
    }

    public static ProfileDTOInter createProfileDTO(Object... obj) {
        return new ProfileDTO();
    }

    public static UserProfileDTOInter createUserProfileDTO(Object... obj) {
        return new UserProfileDTO();
    }

    public static ProfileMenuDTOInter createProfileMenuDTO(Object... obj) {
        return new ProfileMenuDTO();
    }

    public static ItemBingoDTOInter createItemBingoDTO(Object... obj) {
        return new ItemBingoDTO();
    }

    public static ProfileMenuItemDTOInter createProfileMenuItemDTO(Object... obj) {
        return new ProfileMenuItemDTO();
    }

    public static RejectedReportDTOInter createRejectedReportDTO(Object... obj) {
        return new RejectedReportDTO();
    }

    public static DisputeReportDTOInter createDisputeReportDTO(Object... obj) {
        return new DisputeReportDTO();
    }

    public static FilesToMoveDTOInter createFilesToMoveDTO(Object... obj) {
        return new FilesToMoveDTO();
    }

    public static RunningGraphDTOInter createRunningGraphDTO(Object... obj) {
        return new RunningGraphDTO();
    }

    public static JobsStatusDTOInter createJobsStatusDTO(Object... obj) {
        return new JobsStatusDTO();
    }

    public static ErrorsMonitorDTOInter createErrorsMonitorDTO(Object... obj) {
        return new ErrorsMonitorDTO();
    }

    public static MatchingMonitorDTOInter createMatchingMonitorDTO(Object... obj) {
        return new MatchingMonitorDTO();
    }

    public static ValidationMonitorDTOInter createValidationMonitorDTO(Object... obj) {
        return new ValidationMonitorDTO();
    }

    public static LoadingMonitorDTOInter createLoadingMonitorDTO(Object... obj) {
        return new LoadingMonitorDTO();
    }

    public static UserActivityDTOInter createUserActivityDTO(Object... obj) {
        return new UserActivityDTO();
    }

    public static MenuLabelDTOInter createMenuLabelDTO(Object... obj) {
        return new MenuLabelDTO();
    }

    public static ReplanishmentDetailDTOInter createReplanishmentDetailDTO(Object... obj) {
        return new ReplanishmentDetailDTO();
    }

    public static ReplanishmentMasterDTOInter createReplanishmentMasterDTO(Object... obj) {
        return new ReplanishmentMasterDTO();
    }

    public static ValidationRulesDTOInter createValidationRulesDTO(Object... obj) {
        return new ValidationRulesDTO();
    }

    public static RejectedTransactionsDTOInter createRejectedTransactionsDTO(Object... obj) {
        return new RejectedTransactionsDTO();
    }

    public static DisputesDTOInter createDisputesDTO(Object... obj) {
        return new DisputesDTO();
    }

    public static LangDTOInter createLangDTO(Object... obj) {
        return new LangDTO();
    }

    public static UserAtmDTOInter createUserAtmDTO(Object... obj) {
        return new UserAtmDTO();
    }

    public static SwitchFileHeaderDTOInter createSwitchFileHeaderDTO(Object... obj) {
        return new SwitchFileHeaderDTO();
    }

    public static SwitchFileTemplateDTOInter createSwitchFileTemplateDTO(Object... obj) {
        return new SwitchFileTemplateDTO();
    }

    public static SwitchFileTemplateDetailDTOInter createSwitchFileTemplateDetailDTO(Object... obj) {
        return new SwitchFileTemplateDetailDTO();
    }

    public static NetworkMasterDTOInter createNetworkMasterDTO(Object... obj) {
        return new NetworkMasterDTO();
    }

    public static NetworkDetailsDTOInter createNetworkDetailsDTO(Object... obj) {
        return new NetworkDetailsDTO();
    }

    public static HostFileTemplateDetailDTOInter createHostFileTemplateDetailDTO(Object... obj) {
        return new HostFileTemplateDetailDTO();
    }

    public static HostFileTemplateDTOInter createHostFileTemplateDTO(Object... obj) {
        return new HostFileTemplateDTO();
    }

    public static HostFileHeaderDTOInter createHostFileHeaderDTO(Object... obj) {
        return new HostFileHeaderDTO();
    }

    public static FileColumnDisplayDetailsDTOInter createFileColumnDisplayDetailsDTO(Object... obj) {
        return new FileColumnDisplayDetailsDTO();
    }

    public static FileColumnDisplayDTOInter createFileColumnDisplayDTO(Object... obj) {
        return new FileColumnDisplayDTO();
    }

    public static CassetteDTOInter createCassetteDTO(Object... obj) {
        return new CassetteDTO();
    }

    public static BlackListedCardDTOInter createBlackListedCardDTO(Object... obj) {
        return new BlackListedCardDTO();
    }

    public static SystemTableDTOInter createSystemTableDTO(Object... obj) {
        return new SystemTableDTO();
    }

    public static DuplicateTransactionsDTOInter createDuplicateTransactionsDTO(Object... obj) {
        return new DuplicateTransactionsDTO();
    }

    public static AtmFileDTOInter createAtmFileDTO(Object... obj) {
        return new AtmFileDTO();
    }

    public static AtmFileHeaderDTOInter createAtmFileHeaderDTO(Object... obj) {
        return new AtmFileHeaderDTO();
    }

    public static AtmFileTemplateDetailDTOInter createAtmFileTemplateDetailDTO(Object... obj) {
        return new AtmFileTemplateDetailDTO();
    }

    public static AtmFileTemplateDTOInter createAtmFileTemplateDTO(Object... obj) {
        return new AtmFileTemplateDTO();
    }

    public static CopyFilesDTOInter createCopyFilesDTO(Object... obj) {
        return new CopyFilesDTO();
    }

    public static BlockUsersDTOInter createBlockUsersDTO(Object... obj) {
        return new BlockUsersDTO();
    }

    public static SystemTimesDTOInter createSystemTimesDTO(Object... obj) {
        return new SystemTimesDTO();
    }

    public static TimeShiftDTOInter createTimeShiftDTO(Object... obj) {
        return new TimeShiftDTO();
    }

    public static ColumnDTOInter createColumnDTO(Object... obj) {
        return new ColumnDTO();
    }

    public static MatchingTypeDTOInter createMatchingTypeDTO(Object... obj) {
        return new MatchingTypeDTO();
    }

    public static CurrencyMasterDTOInter createCurrencyMasterDTO(Object... obj) {
        return new CurrencyMasterDTO();
    }

    public static MatchedDataDTOInter createMatchedDataDTO(Object... obj) {
        return new MatchedDataDTO();
    }

    public static TransactionResponseMasterDTOInter createTransactionResponseMasterDTO(Object... obj) {
        return new TransactionResponseMasterDTO();
    }

    public static TransactionResponseCodeDTOInter createTransactionResponseCodeDTO(Object... obj) {
        return new TransactionResponseCodeDTO();
    }

    public static TransactionTypeMasterDTOInter createTransactionTypeMasterDTO(Object... obj) {
        return new TransactionTypeMasterDTO();
    }

    public static TransactionTypeDTOInter createTransactionTypeDTO(Object... obj) {
        return new TransactionTypeDTO();
    }

    public static CurrencyDTOInter createCurrencyDTO(Object... obj) {
        return new CurrencyDTO();
    }

    public static TransactionStatusDTOInter createTransactionStatusDTO(Object... obj) {
        return new TransactionStatusDTO();
    }

    public static BlockReasonDTOInter createBlockReasonDTO(Object... obj) {
        return new BlockReasonDTO();
    }

    public static EmgHolidaysDTOInter createEmgHolidaysDTO(Object... obj) {
        return new EmgHolidaysDTO();
    }

    public static HolidaysDTOInter createHolidaysDTO(Object... obj) {
        return new HolidaysDTO();
    }

    public static TimeShiftDetailDTOInter createTimeShiftDetailDTO(Object... obj) {
        return new TimeShiftDetailDTO();
    }

    public static UsersDTOInter createUsersDTO(Object... obj) {
        return new UsersDTO();
    }

    public static AtmMachineDTOInter createAtmMachineDTO(Object... obj) {
        return new AtmMachineDTO();
    }

    public static AtmGroupDTOInter createAtmGroupDTO(Object... obj) {
        return new AtmGroupDTO();
    }

    public static AtmMachineTypeDTOInter createAtmMachineTypeDTO(Object... obj) {
        return new AtmMachineTypeDTO();
    }

    public static MatchingTypeMachinesDTOInter createMatchingTypeMachinesDTO(Object... obj) {
        return new MatchingTypeMachinesDTO();
    }

    public static MatchingTypeSettingDTOInter createMatchingTypeSettingDTO(Object... obj) {
        return new MatchingTypeSettingDTO();
    }

    public static FileColumnDefinitionDTOInter createFileColumnDefinitionDTO(Object... obj) {
        return new FileColumnDefinitionDTO();
    }

    public static NetworkGroupDTOInter createNetworkGroupDTO(Object... obj) {
        return new NetworkGroupDTO();
    }
}
