/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface ProfileMenuItemDTOInter extends Serializable {

    int getItemId();

    int getMenuId();

    int getProfileId();

    void setItemId(int ItemId);

    void setMenuId(int MenuId);

    void setProfileId(int ProfileId);

}
