/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface LincManagmentDetailsDTOInter extends Serializable {

    void setRemaininlic(String remaininlic);

    String getRemaininlic();

    void setOutlic(String outlic);

    String getOutlic();

    void setLic(String lic);

    String getLic();

    void setInlic(String inlic);

    String getInlic();

    void setAllmachines(String allmachines);

    String getAllmachines();
}
