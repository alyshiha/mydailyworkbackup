/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface AtmFileDTOInter extends Serializable {

    public Date getEndloading();

    public void setEndloading(Date endloading);

    public Date getStartloading();

    public void setStartloading(Date startloading);

    String getBuckupName();

    Date getFinishLaodingDate();

    int getId();

    Date getLoadingDate();

    String getName();

    int getNumberOfDuplication();

    int getNumberOfTransaction();

    int getTemplate();

    int getType();

    void setBuckupName(String buckupName);

    void setFinishLaodingDate(Date finishLaodingDate);

    void setId(int id);

    void setLoadingDate(Date loadingDate);

    void setName(String name);

    void setNumberOfDuplication(int numberOfDuplication);

    void setNumberOfTransaction(int numberOfTransaction);

    void setTemplate(int template);

    void setType(int type);
}
