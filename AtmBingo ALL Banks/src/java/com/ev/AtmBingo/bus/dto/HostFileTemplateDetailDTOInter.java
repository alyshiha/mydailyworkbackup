/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Administrator
 */
public interface HostFileTemplateDetailDTOInter extends Serializable{
Boolean getMandatory();
void setDecimalpointposition(String decimalpointposition);
String getDecimalpointposition();
         Boolean getDecimalpoint();

         void setDecimalpoint(Boolean decimalpoint);
void setMandatory(Boolean mandatory);
 Boolean getNegativeflag();
 void setNegativeflag(Boolean negativeflag);
 String getTagstring();
 void setTagstring(String tagstring);
    public BigDecimal getStartingPosition() ;

    public void setStartingPosition(BigDecimal startingPosition);

    Boolean getLoadWhenMappedB();

    void setLoadWhenMappedB(Boolean loadWhenMappedB);

    int getColumnId();

    BigDecimal getDataType();

    String getFormat();

    String getFormat2();

    int getId();

    BigDecimal getLength();

    BigDecimal getLengthDir();

    BigDecimal getLineNumber();

    int getLoadWhenMapped();

    BigDecimal getPosition();

    int getTwmplate();

    void setColumnId(int columnId);

    void setDataType(BigDecimal dataType);

    void setFormat(String format);

    void setFormat2(String format2);

    void setId(int id);

    void setLength(BigDecimal length);

    void setLengthDir(BigDecimal lengthDir);

    void setLineNumber(BigDecimal lineNumber);

    void setLoadWhenMapped(int loadWhenMapped);

    void setPosition(BigDecimal position);

    void setTwmplate(int twmplate);

}
