/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface LoadingInsertErrorsDTOInter {

    int getErrorCode();

    Date getErrorDate();

    String getErrorText();

    int getFileID();

    String getFileName();

    String getTransactionData();

    void setErrorCode(int errorCode);

    void setErrorDate(Date errorDate);

    void setErrorText(String errorText);

    void setFileID(int fileID);

    void setFileName(String fileName);

    void setTransactionData(String transactionData);
    
}
