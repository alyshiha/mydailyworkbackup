/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Aly.Shiha
 */
public class StatementDTO implements StatementDTOInter {
     private Date datefrom;
     private Date dateto;
     private BigDecimal openingbalance;
     private BigDecimal closingbalance;
     private String atmid;

    @Override
    public Date getDatefrom() {
        return datefrom;
    }

    @Override
    public void setDatefrom(Date datefrom) {
        this.datefrom = datefrom;
    }

    @Override
    public Date getDateto() {
        return dateto;
    }

    @Override
    public void setDateto(Date dateto) {
        this.dateto = dateto;
    }

    @Override
    public BigDecimal getOpeningbalance() {
        return openingbalance;
    }

    @Override
    public void setOpeningbalance(BigDecimal openingbalance) {
        this.openingbalance = openingbalance;
    }

    @Override
    public BigDecimal getClosingbalance() {
        return closingbalance;
    }

    @Override
    public void setClosingbalance(BigDecimal closingbalance) {
        this.closingbalance = closingbalance;
    }

    @Override
    public String getAtmid() {
        return atmid;
    }

    @Override
    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }
         
}
