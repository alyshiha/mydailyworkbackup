/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface MatchedDataDTOInter  extends Serializable {
  Boolean getIsjournal();

    void setIsjournal(Boolean isjournal);
    String getCardno();

    void setatmname(String atmname);

    String getatmname();
void setcardnosuffix(String cardnosuffix) ;
String getcardnosuffix();
    void setBlacklist(int blacklist);

    int getBlacklist();

    String getColumn1();

    Date getCorrectivedate();

    void setCorrectivedate(Date correctivedate);

    void setExportdate(Date exportdate);

    Date getExportdate();

    void setCorrectiveuser(int correctiveuser);

    int getCorrectiveuser();

    void setExportuser(int exportuser);

    int getExportuser();

    String getColumn2();

    String getColumn3();

    String getColumn4();

    String getColumn5();

    String getComments();

    String getCurrency();

    CurrencyMasterDTOInter getCurrencyid();

    String getCustomeraccountnumber();

    int getFileid();

    Date getLoadingdate();

    int getMatchby();

    int getMatchingtype();

    int getMatchkey();

    String getNotespresented();

    int getRecordtype();

    String getResponsecode();

    TransactionResponseCodeDTOInter getResponsecodeid();

    int getResponsecodemaster();

    String getRownum();

    Date getSettleddate();

    int getSettledflag();

    UsersDTOInter getSettleduser();

    Date getSettlementdate();

    Date getTransactiondate();

    String getTransactionsequence();

    int getTransactionsequenceorderby();

    String getTransactionstatus();

    TransactionStatusDTOInter getTransactionstatusid();

    Date getTransactiontime();

    String getTransactiontype();

    TransactionTypeDTOInter getTransactiontypeid();

    int getTransactiontypemaster();

    int getamount();

    String getatmapplicationid();

    AtmMachineDTOInter getatmid();

    Date getmatchdate();

    void setCardno(String cardno);

 

    void setColumn1(String column1);

    void setColumn2(String column2);

    void setColumn3(String column3);

    void setColumn4(String column4);

    void setColumn5(String column5);

    void setComments(String comments);

    void setCurrency(String currency);

    void setCurrencyid(CurrencyMasterDTOInter currencyid);

    void setCustomeraccountnumber(String customeraccountnumber);

    void setFileid(int fileid);

    void setLoadingdate(Date loadingdate);

    void setMatchby(int matchby);

    void setMatchingtype(int matchingtype);

    void setMatchkey(int matchkey);

    void setNotespresented(String notespresented);

    void setRecordtype(int recordtype);

    void setResponsecode(String responsecode);

    void setResponsecodeid(TransactionResponseCodeDTOInter responsecodeid);

    void setResponsecodemaster(int responsecodemaster);

    void setRownum(String rownum);

    void setSettleddate(Date settleddate);

    void setSettledflag(int settledflag);

    void setSettleduser(UsersDTOInter settleduser);

    void setSettlementdate(Date settlementdate);

    void setTransactiondate(Date transactiondate);

    void setTransactionsequence(String transactionsequence);

    void setTransactionsequenceorderby(int transactionsequenceorderby);

    void setTransactionstatus(String transactionstatus);

    void setTransactionstatusid(TransactionStatusDTOInter transactionstatusid);

    void setTransactiontime(Date transactiontime);

    void setTransactiontype(String transactiontype);

    void setTransactiontypeid(TransactionTypeDTOInter transactiontypeid);

    void setTransactiontypemaster(int transactiontypemaster);

    void setamount(int amount);

    void setatmapplicationid(String atmapplicationid);

    void setatmid(AtmMachineDTOInter atmid);

    void setmatchdate(Date Matchdate);

}
