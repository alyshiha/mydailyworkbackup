/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface bycardDTOInter  extends Serializable {

    String getCardno();

    String getCurrency();

    Integer getTotalamount();

    String getTransactionscount();

    void setCardno(String cardno);

    void setCurrency(String currency);

    void setTotalamount(Integer totalamount);

    void setTransactionscount(String transactionscount);
    
}
