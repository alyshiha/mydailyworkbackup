/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.sql.Timestamp;

/**
 *
 * @author AlyShiha
 */
public interface RepStat4DTOInter {
void setAtmid(String atmid);
String getAtmid();
    void setAtm(String atm);

    String getAtm();

    Timestamp getDatefrom();

    Timestamp getDateto();

    Integer getHostamount();

    Integer getHostdifference();

    Integer getOffsamount();

    Integer getOffusdifference();

    Integer getSwitchamount();

    Integer getSwitchdifference();

    void setDatefrom(Timestamp datefrom);

    void setDateto(Timestamp dateto);

    void setHostamount(Integer hostamount);
String getId();
void setId(String id);
    void setHostdifference(Integer hostdifference);

    void setOffsamount(Integer offsamount);

    void setOffusdifference(Integer offusdifference);

    void setSwitchamount(Integer switchamount);

    void setSwitchdifference(Integer switchdifference);

}
