/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;
import oracle.sql.CLOB;

/**
 *
 * @author Administrator
 */
public interface DisputeReportDTOInter extends Serializable {

    int getAmount();

    CLOB getTransactiondata();

    void setTransactiondata(CLOB transactiondata);

    Date getCorrectivedate();

    void setCorrectivedate(Date correctivedate);

    void setExportdate(Date exportdate);

    Date getExportdate();

    void setCorrectiveuser(String correctiveuser);

    String getCorrectiveuser();

    void setExportuser(String exportuser);

    String getExportuser();

    String getAtmApplicationId();

    AtmMachineDTOInter getAtmId();

    String getCardNo();

    String getCardNoSuffix();

    String getColumn1();

    String getColumn2();

    String getColumn3();

    String getColumn4();

    String getColumn5();

    String getComments();

    String getCurrency();

    CurrencyMasterDTOInter getCurrencyId();

    String getCustomerAccountNumber();

    int getDisputeBy();

    Date getDisputeDate();

    int getDisputeKey();

    String getDisputeReason();

    int getDisputeWithRoles();

    int getMatchingType();

    String getNotesPresented();

    int getRecordType();

    String getResponseCode();

    TransactionResponseCodeDTOInter getResponseCodeId();

    int getResponseCodeMaster();

    Date getSettledDate();

    int getSettledFlag();

    UsersDTOInter getSettledUser();

    Date getSettlementDate();

    Date getTransactionDate();

    String getTransactionSeqeunce();

    int getTransactionSequenceOrderBy();

    String getTransactionStatus();

    TransactionStatusDTOInter getTransactionStatusId();

    Date getTransactionTime();

    String getTransactionType();

    TransactionTypeDTOInter getTransactionTypeId();

    int getTransactionTypeMaster();

    int getUserId();

    void setAmount(int amount);

    void setAtmApplicationId(String atmApplicationId);

    void setAtmId(AtmMachineDTOInter atmId);

    void setCardNo(String cardNo);

    void setCardNoSuffix(String cardNoSuffix);

    void setColumn1(String column1);

    void setColumn2(String column2);

    void setColumn3(String column3);

    void setColumn4(String column4);

    void setColumn5(String column5);

    void setComments(String comments);

    void setCurrency(String currency);

    void setCurrencyId(CurrencyMasterDTOInter currencyId);

    void setCustomerAccountNumber(String customerAccountNumber);

    void setDisputeBy(int disputeBy);

    void setDisputeDate(Date disputeDate);

    void setDisputeKey(int disputeKey);

    void setDisputeReason(String disputeReason);

    void setDisputeWithRoles(int disputeWithRoles);

    void setMatchingType(int matchingType);

    void setNotesPresented(String notesPresented);

    void setRecordType(int recordType);

    void setResponseCode(String responseCode);

    void setResponseCodeId(TransactionResponseCodeDTOInter responseCodeId);

    void setResponseCodeMaster(int responseCodeMaster);

    void setSettledDate(Date settledDate);

    void setSettledFlag(int settledFlag);

    void setSettledUser(UsersDTOInter settledUser);

    void setSettlementDate(Date settlementDate);

    void setTransactionDate(Date transactionDate);

    void setTransactionSeqeunce(String transactionSeqeunce);

    void setTransactionSequenceOrderBy(int transactionSequenceOrderBy);

    void setTransactionStatus(String transactionStatus);

    void setTransactionStatusId(TransactionStatusDTOInter transactionStatusId);

    void setTransactionTime(Date transactionTime);

    void setTransactionType(String transactionType);

    void setTransactionTypeId(TransactionTypeDTOInter transactionTypeId);

    void setTransactionTypeMaster(int transactionTypeMaster);

    void setUserId(int userId);

}
