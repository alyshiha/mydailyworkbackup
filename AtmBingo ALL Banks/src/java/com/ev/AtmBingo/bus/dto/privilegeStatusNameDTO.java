/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public class privilegeStatusNameDTO implements privilegeStatusNameDTOInter {

    private int prev_id;
    private String prev_name;
private String symbol;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public privilegeStatusNameDTO() {
    }

    public privilegeStatusNameDTO(int prev_id, String prev_name) {
        this.prev_id = prev_id;
        this.prev_name = prev_name;
    }

    @Override
    public int getPrev_id() {
        return prev_id;
    }

    @Override
    public void setPrev_id(int prev_id) {
        this.prev_id = prev_id;
    }

    @Override
    public String getPrev_name() {
        return prev_name;
    }

    @Override
    public void setPrev_name(String prev_name) {
        this.prev_name = prev_name;
    }
    
    
}
