/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface PendingAtmsDTOInter extends Serializable {

    void setAppName(String appName);

    String getAppName();

    Integer getAtmId();

    void setAtmId(Integer atmId);

    Integer getDone();

    void setDone(Integer Done);

    Integer getAtmName();

    void setAtmName(Integer AtmName);

}
