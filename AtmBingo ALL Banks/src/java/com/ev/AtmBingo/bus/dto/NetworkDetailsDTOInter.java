/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Administrator
 */
public interface NetworkDetailsDTOInter extends Serializable {

    Integer getAmountFrom();

    Integer getAmountTo();

    String getBin();

    Integer getCurrency();

    Integer getGroupId();

    BigDecimal getRate();

    Integer getRateType();

    Integer getTransactionType();

    void setAmountFrom(Integer amountFrom);

    void setAmountTo(Integer amountTo);

    void setBin(String bin);

    void setCurrency(Integer currency);

    void setGroupId(Integer groupId);

    void setRate(BigDecimal rate);

    void setRateType(Integer rateType);

    void setTransactionType(Integer transactionType);

}
