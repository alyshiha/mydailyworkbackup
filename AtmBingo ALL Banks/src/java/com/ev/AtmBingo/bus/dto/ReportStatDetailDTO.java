/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public class ReportStatDetailDTO implements ReportStatDetailDTOInter {
    private String name;
    private String amount;
    private String cashaddedcount;
    private String cashaddedvalue;
    private String remaincashcount;
    private String remaincashvalue;
    private String dispensecashvalue;
    private String dispensecashcount;
    private String noteprescount;
    private String notespresvalue;
    private String diffvalue;
    private String diffcount;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getAmount() {
        return amount;
    }

    @Override
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String getCashaddedcount() {
        return cashaddedcount;
    }

    @Override
    public void setCashaddedcount(String cashaddedcount) {
        this.cashaddedcount = cashaddedcount;
    }

    @Override
    public String getCashaddedvalue() {
        return cashaddedvalue;
    }

    @Override
    public void setCashaddedvalue(String cashaddedvalue) {
        this.cashaddedvalue = cashaddedvalue;
    }

    @Override
    public String getRemaincashcount() {
        return remaincashcount;
    }

    @Override
    public void setRemaincashcount(String remaincashcount) {
        this.remaincashcount = remaincashcount;
    }

    @Override
    public String getRemaincashvalue() {
        return remaincashvalue;
    }

    @Override
    public void setRemaincashvalue(String remaincashvalue) {
        this.remaincashvalue = remaincashvalue;
    }

    @Override
    public String getDispensecashvalue() {
        return dispensecashvalue;
    }

    @Override
    public void setDispensecashvalue(String dispensecashvalue) {
        this.dispensecashvalue = dispensecashvalue;
    }

    @Override
    public String getDispensecashcount() {
        return dispensecashcount;
    }

    @Override
    public void setDispensecashcount(String dispensecashcount) {
        this.dispensecashcount = dispensecashcount;
    }

    @Override
    public String getNoteprescount() {
        return noteprescount;
    }

    @Override
    public void setNoteprescount(String noteprescount) {
        this.noteprescount = noteprescount;
    }

    @Override
    public String getNotespresvalue() {
        return notespresvalue;
    }

    @Override
    public void setNotespresvalue(String notespresvalue) {
        this.notespresvalue = notespresvalue;
    }

    @Override
    public String getDiffvalue() {
        return diffvalue;
    }

    @Override
    public void setDiffvalue(String diffvalue) {
        this.diffvalue = diffvalue;
    }

    @Override
    public String getDiffcount() {
        return diffcount;
    }

    @Override
    public void setDiffcount(String diffcount) {
        this.diffcount = diffcount;
    }
    
}
