/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.sql.Clob;
import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class invalidReplanishmentsDTO implements invalidReplanishmentsDTOInter {

    private String transactionData;
    private String fileName;
    private String atmID;
    private Date loadingDate;
    private String reason;

    public invalidReplanishmentsDTO() {
    }

    public invalidReplanishmentsDTO(String transactionData, String fileName, String atmID, Date loadingDate, String reason) {
        this.transactionData = transactionData;
        this.fileName = fileName;
        this.atmID = atmID;
        this.loadingDate = loadingDate;
        this.reason = reason;
    }

    @Override
    public String getTransactionData() {
        return transactionData;
    }

    @Override
    public void setTransactionData(String transactionData) {
        this.transactionData = transactionData;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String getAtmID() {
        return atmID;
    }

    @Override
    public void setAtmID(String atmID) {
        this.atmID = atmID;
    }

    @Override
    public Date getLoadingDate() {
        return loadingDate;
    }

    @Override
    public void setLoadingDate(Date loadingDate) {
        this.loadingDate = loadingDate;
    }

    @Override
    public String getReason() {
        return reason;
    }

    @Override
    public void setReason(String reason) {
        this.reason = reason;
    }

}
