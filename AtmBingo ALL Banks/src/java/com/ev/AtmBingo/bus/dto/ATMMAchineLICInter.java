/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;


import java.io.Serializable;
/**
 *
 * @author Administrator
 */
public interface  ATMMAchineLICInter extends Serializable{
    Boolean getLIC();
    String getApplicationId();
    void setApplicationId(String applicationId);
    void setLIC(Boolean  LIC) ;
    Integer getID() ;
    void setID(Integer  MachineID) ;
    Boolean getDel();
    void setDel(Boolean  Del);
}
