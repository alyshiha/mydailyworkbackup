/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class SystemTimesDTO extends BaseDTO implements SystemTimesDTOInter,Serializable{

    private int dayNo;
    private int seq;
    private String arabicName;
    private String englishName;
    private Date startTime;
    private Date endTime;
    private int working;
    private Boolean workingB;

    public Boolean getWorkingB() {
        workingB = working == 1;
        return workingB;
    }

    public void setWorkingB(Boolean workingB) {
        if(workingB)
            working = 1;
        else
            working = 2;
        this.workingB = workingB;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public int getDayNo() {
        return dayNo;
    }

    public void setDayNo(int dayNo) {
        this.dayNo = dayNo;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public int getWorking() {
        return working;
    }

    public void setWorking(int working) {
        this.working = working;
    }
    
    protected SystemTimesDTO() {
    }

}
