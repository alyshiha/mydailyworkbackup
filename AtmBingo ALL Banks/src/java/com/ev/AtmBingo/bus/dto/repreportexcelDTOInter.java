/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface repreportexcelDTOInter  extends Serializable{

    String getColoumn1();

    String getColoumn10();

    String getColoumn11();

    String getColoumn12();

    String getColoumn13();

    String getColoumn14();

    String getColoumn15();

    String getColoumn2();

    String getColoumn3();

    String getColoumn4();

    String getColoumn5();

    String getColoumn6();

    String getColoumn7();

    String getColoumn8();

    String getColoumn9();

    void setColoumn1(String coloumn1);

    void setColoumn10(String coloumn10);

    void setColoumn11(String coloumn11);

    void setColoumn12(String coloumn12);

    void setColoumn13(String coloumn13);

    void setColoumn14(String coloumn14);

    void setColoumn15(String coloumn15);

    void setColoumn2(String coloumn2);

    void setColoumn3(String coloumn3);

    void setColoumn4(String coloumn4);

    void setColoumn5(String coloumn5);

    void setColoumn6(String coloumn6);

    void setColoumn7(String coloumn7);

    void setColoumn8(String coloumn8);

    void setColoumn9(String coloumn9);
    
}
