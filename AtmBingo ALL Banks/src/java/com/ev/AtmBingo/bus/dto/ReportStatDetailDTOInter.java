/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public interface ReportStatDetailDTOInter {

    String getAmount();

    String getCashaddedcount();

    String getCashaddedvalue();

    String getDiffcount();

    String getDiffvalue();

    String getDispensecashcount();

    String getDispensecashvalue();

    String getName();

    String getNoteprescount();

    String getNotespresvalue();

    String getRemaincashcount();

    String getRemaincashvalue();

    void setAmount(String amount);

    void setCashaddedcount(String cashaddedcount);

    void setCashaddedvalue(String cashaddedvalue);

    void setDiffcount(String diffcount);

    void setDiffvalue(String diffvalue);

    void setDispensecashcount(String dispensecashcount);

    void setDispensecashvalue(String dispensecashvalue);

    void setName(String name);

    void setNoteprescount(String noteprescount);

    void setNotespresvalue(String notespresvalue);

    void setRemaincashcount(String remaincashcount);

    void setRemaincashvalue(String remaincashvalue);
    
}
