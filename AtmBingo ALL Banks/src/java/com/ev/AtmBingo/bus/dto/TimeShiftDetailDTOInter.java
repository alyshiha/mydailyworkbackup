/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface TimeShiftDetailDTOInter extends Serializable {

    Boolean getWorkingB();

    void setWorkingB(Boolean workingB);

    int getDayNo();

    int getShiftId();

    Date getTimeFrom();

    Date getTimeTo();

    int getWorking();

    void setDayNo(int dayNo);

    void setShiftId(int shiftId);

    void setTimeFrom(Date timeFrom);

    void setTimeTo(Date timeTo);

    void setWorking(int working);

}
