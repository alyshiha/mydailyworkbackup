/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class RepStat2DTO implements RepStat2DTOInter {

    private Timestamp datefrom;
    private Timestamp dateto;
    private Integer switchtotal;
    private Integer notepres;
    private String differentname;
    private Integer differentvalue;
    private String atmid;
    private String atmname;
    private String id;
   private Integer cashover;
    private Integer cashshortage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAtmname() {
        return atmname;
    }

    public void setAtmname(String atmname) {
        this.atmname = atmname;
    }

    public Integer getCashover() {
        return cashover;
    }

    public void setCashover(Integer cashover) {
        this.cashover = cashover;
    }

    public Integer getCashshortage() {
        return cashshortage;
    }

    public void setCashshortage(Integer cashshortage) {
        this.cashshortage = cashshortage;
    }
    
    public RepStat2DTO() {
    }

    public RepStat2DTO(Timestamp datefrom, Timestamp dateto, Integer switchtotal, Integer notepres, String differentname, Integer differentvalue,String atmid) {
        this.datefrom = datefrom;
        this.dateto = dateto;
        this.switchtotal = switchtotal;
        this.notepres = notepres;
        this.differentname = differentname;
        this.differentvalue = differentvalue;
        this.atmid = atmid;
    }

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    @Override
    public Timestamp getDatefrom() {
        return datefrom;
    }

    @Override
    public void setDatefrom(Timestamp datefrom) {
        this.datefrom = datefrom;
    }

    @Override
    public Timestamp getDateto() {
        return dateto;
    }

    @Override
    public void setDateto(Timestamp dateto) {
        this.dateto = dateto;
    }

    @Override
    public Integer getSwitchtotal() {
        return switchtotal;
    }

    @Override
    public void setSwitchtotal(Integer switchtotal) {
        this.switchtotal = switchtotal;
    }

    @Override
    public Integer getNotepres() {
        return notepres;
    }

    @Override
    public void setNotepres(Integer notepres) {
        this.notepres = notepres;
    }

    @Override
    public String getDifferentname() {
        return differentname;
    }

    @Override
    public void setDifferentname(String differentname) {
        this.differentname = differentname;
    }

    @Override
    public Integer getDifferentvalue() {
        return differentvalue;
    }

    @Override
    public void setDifferentvalue(Integer differentvalue) {
        this.differentvalue = differentvalue;
    }

}
