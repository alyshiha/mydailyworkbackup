/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface LicenseDTOInter extends Serializable {

    Integer getAtmNO();

    String getCountry();

    Date getCreatedDate();

    Date getEndDate();

    String getLicenseKey();

    String getLicenseTo();

    Integer getNoOfUser();

    String getVersion();

    void setAtmNO(Integer atmNO);

    void setCountry(String country);

    void setCreatedDate(Date createdDate);

    void setEndDate(Date endDate);

    void setLicenseKey(String licenseKey);

    void setLicenseTo(String licenseTo);

    void setNoOfUser(Integer noOfUser);

    void setVersion(String version);

}
