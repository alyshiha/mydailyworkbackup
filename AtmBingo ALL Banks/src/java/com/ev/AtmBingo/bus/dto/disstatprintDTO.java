/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class disstatprintDTO implements disstatprintDTOInter , Serializable{
    private String filename,loadingdate,atmapplicationid,atmname,transactiontype,currency,transactionstatus,responsecode,transactiondate,cardnum,
amount,settlementdate,notespresented,customeraccountnumber,transactionsequence,disputereason,recordtype,settleddate,comments,settleduser,disputedate
,absamount,NotesPresented,Autho,GLACCOUNTATM,Branch,transdata;

    @Override
    public String getAutho() {
        return Autho;
    }

    @Override
    public void setAutho(String Autho) {
        this.Autho = Autho;
    }

    @Override
    public String getBranch() {
        return Branch;
    }

    @Override
    public void setBranch(String Branch) {
        this.Branch = Branch;
    }

    @Override
    public String getGLACCOUNTATM() {
        return GLACCOUNTATM;
    }

    @Override
    public void setGLACCOUNTATM(String GLACCOUNTATM) {
        this.GLACCOUNTATM = GLACCOUNTATM;
    }

    @Override
    public String getNotesPresented() {
        return NotesPresented;
    }

    @Override
    public void setNotesPresented(String NotesPresented) {
        this.NotesPresented = NotesPresented;
    }

    @Override
    public String getAbsamount() {
        return absamount;
    }

    @Override
    public void setAbsamount(String absamount) {
        this.absamount = absamount;
    }

    @Override
    public String getAmount() {
        return amount;
    }

    @Override
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String getAtmapplicationid() {
        return atmapplicationid;
    }

    @Override
    public void setAtmapplicationid(String atmapplicationid) {
        this.atmapplicationid = atmapplicationid;
    }

    @Override
    public String getAtmname() {
        return atmname;
    }

    @Override
    public void setAtmname(String atmname) {
        this.atmname = atmname;
    }

    @Override
    public String getCardnum() {
        return cardnum;
    }

    @Override
    public void setCardnum(String cardnum) {
        this.cardnum = cardnum;
    }

    @Override
    public String getComments() {
        return comments;
    }

    @Override
    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String getCustomeraccountnumber() {
        return customeraccountnumber;
    }

    @Override
    public void setCustomeraccountnumber(String customeraccountnumber) {
        this.customeraccountnumber = customeraccountnumber;
    }

    @Override
    public String getDisputedate() {
        return disputedate;
    }

    @Override
    public void setDisputedate(String disputedate) {
        this.disputedate = disputedate;
    }

    @Override
    public String getDisputereason() {
        return disputereason;
    }

    @Override
    public void setDisputereason(String disputereason) {
        this.disputereason = disputereason;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public String getLoadingdate() {
        return loadingdate;
    }

    @Override
    public void setLoadingdate(String loadingdate) {
        this.loadingdate = loadingdate;
    }

    @Override
    public String getNotespresented() {
        return notespresented;
    }

    @Override
    public void setNotespresented(String notespresented) {
        this.notespresented = notespresented;
    }

    @Override
    public String getRecordtype() {
        return recordtype;
    }

    @Override
    public void setRecordtype(String recordtype) {
        this.recordtype = recordtype;
    }

    @Override
    public String getResponsecode() {
        return responsecode;
    }

    @Override
    public void setResponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

    @Override
    public String getSettleddate() {
        return settleddate;
    }

    @Override
    public void setSettleddate(String settleddate) {
        this.settleddate = settleddate;
    }

    @Override
    public String getSettleduser() {
        return settleduser;
    }

    @Override
    public void setSettleduser(String settleduser) {
        this.settleduser = settleduser;
    }

    @Override
    public String getSettlementdate() {
        return settlementdate;
    }

    @Override
    public void setSettlementdate(String settlementdate) {
        this.settlementdate = settlementdate;
    }

    @Override
    public String getTransactiondate() {
        return transactiondate;
    }

    @Override
    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    @Override
    public String getTransactionsequence() {
        return transactionsequence;
    }

    @Override
    public void setTransactionsequence(String transactionsequence) {
        this.transactionsequence = transactionsequence;
    }

    @Override
    public String getTransactionstatus() {
        return transactionstatus;
    }

    @Override
    public void setTransactionstatus(String transactionstatus) {
        this.transactionstatus = transactionstatus;
    }

    @Override
    public String getTransactiontype() {
        return transactiontype;
    }

    @Override
    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    @Override
    public String getTransdata() {
        return transdata;
    }

    @Override
    public void setTransdata(String transdata) {
        this.transdata = transdata;
    }
    
}
