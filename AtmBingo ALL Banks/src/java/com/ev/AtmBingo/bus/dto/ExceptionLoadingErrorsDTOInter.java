/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface ExceptionLoadingErrorsDTOInter {

    int getErrorCode();

    String getErrorCondition();

    Date getErrorDate();

    String getErrorName();

    String getFunctionName();

    void setErrorCode(int errorCode);

    void setErrorCondition(String errorCondition);

    void setErrorDate(Date errorDate);

    void setErrorName(String errorName);

    void setFunctionName(String functionName);
    
}
