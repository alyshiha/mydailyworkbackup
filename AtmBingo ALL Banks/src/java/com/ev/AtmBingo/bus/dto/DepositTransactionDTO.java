/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Aly.Shiha
 */
public class DepositTransactionDTO extends BaseDTO implements DepositTransactionDTOInter {

    private String cardnumber;
    private String escrowdetail;
    private String valuteddetail;
    private String refundeddetail;
    private String rejecteddetail;
    private String refrence;
    
    private String atmid;
    private Date transactiondate;
    private String escrow;
    private String refunded;
    private String rejected;
    private String valuted;
    private String error;
    private Integer rowid;

    @Override
    public String getCardnumber() {
        return cardnumber;
    }

    @Override
    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    @Override
    public String getEscrowdetail() {
        return escrowdetail;
    }

    @Override
    public void setEscrowdetail(String escrowdetail) {
        this.escrowdetail = escrowdetail;
    }

    @Override
    public String getValuteddetail() {
        return valuteddetail;
    }

    @Override
    public void setValuteddetail(String valuteddetail) {
        this.valuteddetail = valuteddetail;
    }

    @Override
    public String getRefundeddetail() {
        return refundeddetail;
    }

    @Override
    public void setRefundeddetail(String refundeddetail) {
        this.refundeddetail = refundeddetail;
    }

    @Override
    public String getRejecteddetail() {
        return rejecteddetail;
    }

    @Override
    public void setRejecteddetail(String rejecteddetail) {
        this.rejecteddetail = rejecteddetail;
    }

    @Override
    public String getRefrence() {
        return refrence;
    }

    @Override
    public void setRefrence(String refrence) {
        this.refrence = refrence;
    }

    @Override
    public Integer getRowid() {
        return rowid;
    }

    @Override
    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }

    @Override
    public String getAtmid() {
        return atmid;
    }

    @Override
    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    @Override
    public Date getTransactiondate() {
        return transactiondate;
    }

    @Override
    public void setTransactiondate(Date transactiondate) {
        this.transactiondate = transactiondate;
    }

    @Override
    public String getEscrow() {
        return escrow;
    }

    @Override
    public void setEscrow(String escrow) {
        this.escrow = escrow;
    }

    @Override
    public String getRefunded() {
        return refunded;
    }

    @Override
    public void setRefunded(String refunded) {
        this.refunded = refunded;
    }

    @Override
    public String getRejected() {
        return rejected;
    }

    @Override
    public void setRejected(String rejected) {
        this.rejected = rejected;
    }

    @Override
    public String getValuted() {
        return valuted;
    }

    @Override
    public void setValuted(String valuted) {
        this.valuted = valuted;
    }

    @Override
    public String getError() {
        return error;
    }

    @Override
    public void setError(String error) {
        this.error = error;
    }

}
