/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface SwitchFileTemplateDTOInter extends Serializable {

    Boolean getActiveB();

    void setActiveB(Boolean activeB);

    int getActive();

    void setAtmadd(String atmadd);

    String getAtmadd();

    List<SwitchFileHeaderDTOInter> getSwitchFileHeaderDTOL() throws Throwable;

    List<SwitchFileTemplateDetailDTOInter> getSwitchFileTemplateDetailsDTOL() throws Throwable;

    String getBuckupFolder();

    String getCopyFolder();

    String getDateSeparator();

    BigDecimal getDateSeparatorPos1();

    BigDecimal getDateSeparatorPos2();

    BigDecimal getHeaderDataType();

    String getHeaderDateSeparator();

    BigDecimal getHeaderDateSeparatorPos1();

    BigDecimal getHeaderDateSeparatorPos2();

    String getHeaderFormat();

    BigDecimal getHeaderLength();

    String getHeaderPosition();

    String getHeaderString();

    int getId();

    BigDecimal getIgnoredLines();

    String getLoadingFolder();

    String getName();

    BigDecimal getNumberOfLines();

    BigDecimal getProcessingType();

    String getSeparator();

    String getServerFolder();

    BigDecimal getStartingDataType();

    String getStartingFormat();

    BigDecimal getStartingLength();

    BigDecimal getStartingPosition();

    String getStartingValue();

    void setTagendvalue(String tagendvalue);

    String getTagendvalue();

    Integer getTagenddatatype();

    void setTagenddatatype(Integer tagenddatatype);

    String getTagendformate();

    void setTagendformate(String tagendformate);

    Integer getTagendlength();

    void setTagendlength(Integer tagendlength);

    Integer getTagendpostition();

    void setTagendpostition(Integer tagendpostition);

    void setActive(int active);

    void setSwitchFileHeaderDTOL(List<SwitchFileHeaderDTOInter> switchFileHeaderDTOL);

    void setSwitchFileTemplateDetailsDTOL(List<SwitchFileTemplateDetailDTOInter> switchFileTemplateDetailsDTOL);

    void setBuckupFolder(String buckupFolder);

    void setCopyFolder(String copyFolder);

    void setDateSeparator(String dateSeparator);

    void setDateSeparatorPos1(BigDecimal dateSeparatorPos1);

    void setDateSeparatorPos2(BigDecimal dateSeparatorPos2);

    void setHeaderDataType(BigDecimal headerDataType);

    void setHeaderDateSeparator(String headerDateSeparator);

    void setHeaderDateSeparatorPos1(BigDecimal headerDateSeparatorPos1);

    void setHeaderDateSeparatorPos2(BigDecimal headerDateSeparatorPos2);

    void setHeaderFormat(String headerFormat);

    void setHeaderLength(BigDecimal headerLength);

    void setHeaderPosition(String headerPosition);

    void setHeaderString(String headerString);

    void setId(int id);

    void setIgnoredLines(BigDecimal ignoredLines);

    void setLoadingFolder(String loadingFolder);

    void setName(String name);

    void setNumberOfLines(BigDecimal numberOfLines);

    void setProcessingType(BigDecimal processingType);

    void setSeparator(String separator);

    void setServerFolder(String serverFolder);

    void setStartingDataType(BigDecimal startingDataType);

    void setStartingFormat(String startingFormat);

    void setStartingLength(BigDecimal startingLength);

    void setStartingPosition(BigDecimal startingPosition);

    void setStartingValue(String startingValue);
}
