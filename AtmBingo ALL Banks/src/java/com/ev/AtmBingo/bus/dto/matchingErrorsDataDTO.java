/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class matchingErrorsDataDTO implements matchingErrorsDataDTOInter {

    private Date loadingDate, transactionDate, settlementDate, errorDate;
    private String atmApplicationId, transactionType, currency, transactionStatus,
            responseCode, transactionSequence, cardNo, notesPresented, customerAccountNumber, errorMessage, transactionData;
    private Integer amount, errorCode;

    public matchingErrorsDataDTO() {
    }

    public matchingErrorsDataDTO(Date loadingDate, Date transactionDate, Date settlementDate, Date errorDate, String atmApplicationId, String transactionType, String currency, String transactionStatus, String responseCode, String transactionSequence, String cardNo, String notesPresented, String customerAccountNumber, String errorMessage, String transactionData, Integer amount, Integer errorCode) {
        this.loadingDate = loadingDate;
        this.transactionDate = transactionDate;
        this.settlementDate = settlementDate;
        this.errorDate = errorDate;
        this.atmApplicationId = atmApplicationId;
        this.transactionType = transactionType;
        this.currency = currency;
        this.transactionStatus = transactionStatus;
        this.responseCode = responseCode;
        this.transactionSequence = transactionSequence;
        this.cardNo = cardNo;
        this.notesPresented = notesPresented;
        this.customerAccountNumber = customerAccountNumber;
        this.errorMessage = errorMessage;
        this.transactionData = transactionData;
        this.amount = amount;
        this.errorCode = errorCode;
    }

    @Override
    public Date getLoadingDate() {
        return loadingDate;
    }

    @Override
    public void setLoadingDate(Date loadingDate) {
        this.loadingDate = loadingDate;
    }

    @Override
    public Date getTransactionDate() {
        return transactionDate;
    }

    @Override
    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    @Override
    public Date getSettlementDate() {
        return settlementDate;
    }

    @Override
    public void setSettlementDate(Date settlementDate) {
        this.settlementDate = settlementDate;
    }

    @Override
    public Date getErrorDate() {
        return errorDate;
    }

    @Override
    public void setErrorDate(Date errorDate) {
        this.errorDate = errorDate;
    }

    @Override
    public String getAtmApplicationId() {
        return atmApplicationId;
    }

    @Override
    public void setAtmApplicationId(String atmApplicationId) {
        this.atmApplicationId = atmApplicationId;
    }

    @Override
    public String getTransactionType() {
        return transactionType;
    }

    @Override
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String getTransactionStatus() {
        return transactionStatus;
    }

    @Override
    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    @Override
    public String getResponseCode() {
        return responseCode;
    }

    @Override
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String getTransactionSequence() {
        return transactionSequence;
    }

    @Override
    public void setTransactionSequence(String transactionSequence) {
        this.transactionSequence = transactionSequence;
    }

    @Override
    public String getCardNo() {
        return cardNo;
    }

    @Override
    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    @Override
    public String getNotesPresented() {
        return notesPresented;
    }

    @Override
    public void setNotesPresented(String notesPresented) {
        this.notesPresented = notesPresented;
    }

    @Override
    public String getCustomerAccountNumber() {
        return customerAccountNumber;
    }

    @Override
    public void setCustomerAccountNumber(String customerAccountNumber) {
        this.customerAccountNumber = customerAccountNumber;
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String getTransactionData() {
        return transactionData;
    }

    @Override
    public void setTransactionData(String transactionData) {
        this.transactionData = transactionData;
    }

    @Override
    public Integer getAmount() {
        return amount;
    }

    @Override
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public Integer getErrorCode() {
        return errorCode;
    }

    @Override
    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }
    
    
}
