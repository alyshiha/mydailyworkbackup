/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class atmremainingDTO implements atmremainingDTOInter, Serializable {
    private String atmapplicationid, name,group;
    private BigDecimal remaining;
    private Date fromdate,todate;

    public Date getFromdate() {
        return fromdate;
    }

    public void setFromdate(Date fromdate) {
        this.fromdate = fromdate;
    }

    public Date getTodate() {
        return todate;
    }

    public void setTodate(Date todate) {
        this.todate = todate;
    }

    @Override
    public String getAtmapplicationid() {
        return atmapplicationid;
    }

    @Override
    public void setAtmapplicationid(String atmapplicationid) {
        this.atmapplicationid = atmapplicationid;
    }

    @Override
    public String getGroup() {
        return group;
    }

    @Override
    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public BigDecimal getRemaining() {
        return remaining;
    }

    @Override
    public void setRemaining(BigDecimal remaining) {
        this.remaining = remaining;
    }
    
}
