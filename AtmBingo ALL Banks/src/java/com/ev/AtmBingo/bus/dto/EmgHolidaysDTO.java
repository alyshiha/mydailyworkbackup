/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class EmgHolidaysDTO extends BaseDTO implements Serializable, EmgHolidaysDTOInter{

    private Date pFrom;
    private Date pTo;
    private String reason;
    private String eReason;

    public String geteReason() {
        return eReason;
    }

    public void seteReason(String eReason) {
        this.eReason = eReason;
    }

    public Date getpFrom() {
        return pFrom;
    }

    public void setpFrom(Date pFrom) {
        this.pFrom = pFrom;
    }

    public Date getpTo() {
        return pTo;
    }

    public void setpTo(Date pTo) {
        this.pTo = pTo;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    
    protected EmgHolidaysDTO() {
        super();
    }

}
