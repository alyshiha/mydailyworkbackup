/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface CashManagmentLogDTOInter {

    String getAction();

    Date getActionDate();

    Integer getId();

    String getIpAddress();

    void setUserName(String userName);

    String getUserName();

    Integer getUserId();

    void setAction(String action);

    void setActionDate(Date actionDate);

    void setId(Integer id);

    void setIpAddress(String ipAddress);

    void setUserId(Integer userId);

}
