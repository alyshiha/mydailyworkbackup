/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class PrintcardtakenExcel implements PrintcardtakenExcelInter , Serializable{
    private Integer cardCount ;
    private String atmapplicationid ,datefrom,dateto;

    public String getDatefrom() {
        return datefrom;
    }

    public void setDatefrom(String datefrom) {
        this.datefrom = datefrom;
    }

    public String getDateto() {
        return dateto;
    }

    public void setDateto(String dateto) {
        this.dateto = dateto;
    }

    @Override
    public String getAtmapplicationid() {
        return atmapplicationid;
    }

    @Override
    public void setAtmapplicationid(String atmapplicationid) {
        this.atmapplicationid = atmapplicationid;
    }

    @Override
    public Integer getCardCount() {
        return cardCount;
    }

    @Override
    public void setCardCount(Integer cardCount) {
        this.cardCount = cardCount;
    }
    
}
