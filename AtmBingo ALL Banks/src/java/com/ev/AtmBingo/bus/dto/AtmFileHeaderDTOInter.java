/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface AtmFileHeaderDTOInter extends Serializable {

    String getStatus();

    void setStatus(String status);

    int getColumnId();

    Integer getDataType();

    String getFormat();

    String getFormattwo();

    int getId();

    String getLength();

    String getPosition();

    int getTemplate();

    void setColumnId(int columnId);

    void setDataType(Integer dataType);

    void setFormat(String format);

    void setFormattwo(String formattwo);

    void setId(int id);

    void setLength(String length);

    void setPosition(String position);

    void setTemplate(int template);
}
