/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.MenuLabelDAOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class MenuLabelDTO extends BaseDTO implements MenuLabelDTOInter , Serializable{

    private int menuId;
    private String arabicLabel;
    private String englishLabel;
    private int type;
    private int parentId;
    private int seq;
    private String page;
    private int restrected;
    private List<MenuLabelDTOInter> childMenu;
    private List<ItemBingoDTOInter> ibDTOL;

    public int getRestrected() {
        return restrected;
    }

    public void setRestrected(int restrected) {
        this.restrected = restrected;
    }

    public List<ItemBingoDTOInter> getIbDTOL() {
        return ibDTOL;
    }

    public void setIbDTOL(List<ItemBingoDTOInter> ibDTOL) {
        this.ibDTOL = ibDTOL;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getArabicLabel() {
        return arabicLabel;
    }

    public void setArabicLabel(String arabicLabel) {
        this.arabicLabel = arabicLabel;
    }

    public List<MenuLabelDTOInter> getChildMenu(List<MenuLabelDTOInter> Menu,Integer Parent) throws Throwable{

        List<MenuLabelDTOInter> Result = new ArrayList<MenuLabelDTOInter>();
        for(MenuLabelDTOInter MenuItem : Menu)
            {
                if(MenuItem.getParentId() == Parent)
                {
                    Result.add(MenuItem);
                }
               
            }

        return Result;
    }
    public List<MenuLabelDTOInter> getChildMenu(int rest,int x) throws Throwable{

            MenuLabelDAOInter mlDAO = DAOFactory.createMenuLabelDAO(null);
            childMenu = (List<MenuLabelDTOInter>)mlDAO.findByparentId(menuId,rest,x);

        return childMenu;
    }

    public void setChildMenu(List<MenuLabelDTOInter> childMenu) {
        this.childMenu = childMenu;
    }

    public String getEnglishLabel() {
        return englishLabel;
    }

    public void setEnglishLabel(String englishLabel) {
        this.englishLabel = englishLabel;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    protected MenuLabelDTO() {
        super();
        childMenu = new ArrayList<MenuLabelDTOInter>();
    }
}
