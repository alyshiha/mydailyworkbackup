/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public interface AccountNameDTOInter {

    Integer getId();

    String getName();

    String getSymbol();

    void setSymbol(String symbol);

    void setId(Integer id);

    void setName(String name);

}
