/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface CassetteDTOInter extends Serializable{

    int getAmount();

    int getCurrency();

    String getDescription();

    int getId();

    String getName();

    int getSeq();

    void setAmount(int amount);

    void setCurrency(int currency);

    void setDescription(String description);

    void setId(int id);

    void setName(String name);

    void setSeq(int seq);

}
