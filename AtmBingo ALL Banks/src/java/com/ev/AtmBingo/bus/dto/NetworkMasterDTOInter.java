/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author KhAiReE
 */
public interface NetworkMasterDTOInter extends Serializable {

    Integer getAmountTo();

    Integer getAmountFrom();

    Integer getCurrency();

    Integer getGroupId();

    Integer getRateType();

    BigDecimal getRate();

    Integer getTransactionType();

    void setAmountTo(Integer amountTo);

    void setAmountFrom(Integer amountFrom);

    void setCurrency(Integer currency);

    void setGroupId(Integer groupId);

    void setRateType(Integer rateType);

    void setRate(BigDecimal rate);

    void setTransactionType(Integer transactionType);

}
