/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.sql.Clob;
import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface invalidReplanishmentsDTOInter {

    String getAtmID();

    String getFileName();

    Date getLoadingDate();

    String getReason();

    String getTransactionData();

    void setAtmID(String atmID);

    void setFileName(String fileName);

    void setLoadingDate(Date loadingDate);

    void setReason(String reason);

    void setTransactionData(String transactionData);
    
}
