/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface DuplicateTransactionsDTOInter extends Serializable {

    int getAmount();

    String getAtmApplicationId();

    AtmMachineDTOInter getAtmId();

    String getCardNo();

    int getCardNoSuffix();

    String getColumn1();

    String getColumn2();

    String getColumn3();

    String getColumn4();

    String getColumn5();

    String getCurrency();

    CurrencyMasterDTOInter getCurrencyId();

    String getCustomerAccountNumber();

    int getFileId();

    Date getLoadingDate();

    String getNotesPresented();

    int getRecordType();

    String getResponseCode();

    TransactionResponseCodeDTOInter getResponseCodeId();

    int getResponseCodeMaster();

    Date getSettlementDate();

    Date getTransactionDate();

    String getTransactionSeqeunce();

    int getTransactionSequenceOrderBy();

    String getTransactionStatus();

    TransactionStatusDTOInter getTransactionStatusId();

    Date getTransactionTime();

    String getTransactionType();

    TransactionTypeDTOInter getTransactionTypeId();

    int getTransactionTypeMaster();

    void setAmount(int amount);

    void setAtmApplicationId(String atmApplicationId);

    void setAtmId(AtmMachineDTOInter atmId);

    void setCardNo(String cardNo);

    void setCardNoSuffix(int cardNoSuffix);

    void setColumn1(String column1);

    void setColumn2(String column2);

    void setColumn3(String column3);

    void setColumn4(String column4);

    void setColumn5(String column5);

    void setCurrency(String currency);

    void setCurrencyId(CurrencyMasterDTOInter currencyId);

    void setCustomerAccountNumber(String customerAccountNumber);

    void setFileId(int fileId);

    void setLoadingDate(Date loadingDate);

    void setNotesPresented(String notesPresented);

    void setRecordType(int recordType);

    void setResponseCode(String responseCode);

    void setResponseCodeId(TransactionResponseCodeDTOInter responseCodeId);

    void setResponseCodeMaster(int responseCodeMaster);

    void setSettlementDate(Date settlementDate);

    void setTransactionDate(Date transactionDate);

    void setTransactionSeqeunce(String transactionSeqeunce);

    void setTransactionSequenceOrderBy(int transactionSequenceOrderBy);

    void setTransactionStatus(String transactionStatus);

    void setTransactionStatusId(TransactionStatusDTOInter transactionStatusId);

    void setTransactionTime(Date transactionTime);

    void setTransactionType(String transactionType);

    void setTransactionTypeId(TransactionTypeDTOInter transactionTypeId);

    void setTransactionTypeMaster(int transactionTypeMaster);

}
