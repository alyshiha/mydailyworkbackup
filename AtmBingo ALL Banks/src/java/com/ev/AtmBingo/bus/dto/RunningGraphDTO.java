/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class RunningGraphDTO extends BaseDTO implements RunningGraphDTOInter, Serializable{

    private String label;
    private int countRunning;
    private int coutnBroken;
    private int countStandBy;
    private int countDown;

    public int getCountDown() {
        return countDown;
    }

    public void setCountDown(int countDown) {
        this.countDown = countDown;
    }

    public int getCountStandBy() {
        return countStandBy;
    }

    public void setCountStandBy(int countStandBy) {
        this.countStandBy = countStandBy;
    }

    public int getCoutnBroken() {
        return coutnBroken;
    }

    public void setCoutnBroken(int coutnBroken) {
        this.coutnBroken = coutnBroken;
    }

    public int getCountRunning() {
        return countRunning;
    }

    public void setCountRunning(int countRunning) {
        this.countRunning = countRunning;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    
    protected RunningGraphDTO() {
        super();
    }


}
