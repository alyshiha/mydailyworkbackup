/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface NotePresentedDTOInter extends Serializable {

    BigDecimal getAmount();

    String getAtmapplicationid();

    Integer getAtmid();

    String getCardno();

    String getCurrency();

    String getMatchtype();

    Integer getMatchtypeid();

    String getNotepres();

    BigDecimal getNotepresamount();

    String getRecordtype();

    Integer getRecordtypeid();

    String getResponsecode();

    Date getTransactiondate();

    String getTransactionseqeunce();

    void setAmount(BigDecimal amount);

    void setAtmapplicationid(String atmapplicationid);

    void setAtmid(Integer atmid);

    void setCardno(String cardno);

    void setCurrency(String currency);

    void setMatchtype(String matchtype);

    void setMatchtypeid(Integer matchtypeid);

    void setNotepres(String notepres);

    void setNotepresamount(BigDecimal notepresamount);

    void setRecordtype(String recordtype);

    void setRecordtypeid(Integer recordtypeid);

    void setResponsecode(String responsecode);

    void setTransactiondate(Date transactiondate);

    void setTransactionseqeunce(String transactionseqeunce);
    
}
