/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface RepStat2DTOInter {
    void setId(String id);
    String getId();
void setCashshortage(Integer cashshortage);
 Integer getCashshortage();
 void setCashover(Integer cashover);
  Integer getCashover() ;
    void setAtmid(String atmid);
 String getAtmname();
 void setAtmname(String atmname);
    String getAtmid();

    Timestamp getDatefrom();

    Timestamp getDateto();

    String getDifferentname();

    Integer getDifferentvalue();

    Integer getNotepres();

    Integer getSwitchtotal();

    void setDatefrom(Timestamp datefrom);

    void setDateto(Timestamp dateto);

    void setDifferentname(String differentamount);

    void setDifferentvalue(Integer differentvalue);

    void setNotepres(Integer notepres);

    void setSwitchtotal(Integer switchtotal);

}
