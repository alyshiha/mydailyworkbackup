/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface TransactionTypeDTOInter extends Serializable{
boolean getReverseflag();
void setReverseflag(boolean reverseflag) ;
    String getDescription();

    int getId();

    String getName();

    int getTypeCategory();

    int getTypeMaster();

    void setDescription(String description);

    void setId(int id);

    void setName(String name);

    void setTypeCategory(int typeCategory);

    void setTypeMaster(int typeMaster);

}
