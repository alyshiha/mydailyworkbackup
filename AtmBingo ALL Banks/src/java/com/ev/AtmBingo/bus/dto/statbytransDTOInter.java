/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface statbytransDTOInter  extends Serializable{
 void setTotal(Integer total) ;
 Integer getTotal();
    String getAtmapplicationid();

    String getName();

    Integer getTransnumber();

    void setAtmapplicationid(String atmapplicationid);

    void setName(String name);

    void setTransnumber(Integer transnumber);
    
}
