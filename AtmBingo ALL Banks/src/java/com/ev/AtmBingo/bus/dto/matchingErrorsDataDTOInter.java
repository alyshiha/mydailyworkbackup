/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface matchingErrorsDataDTOInter {

    Integer getAmount();

    String getAtmApplicationId();

    String getCardNo();

    String getCurrency();

    String getCustomerAccountNumber();

    Integer getErrorCode();

    Date getErrorDate();

    String getErrorMessage();

    Date getLoadingDate();

    String getNotesPresented();

    String getResponseCode();

    Date getSettlementDate();

    String getTransactionData();

    Date getTransactionDate();

    String getTransactionSequence();

    String getTransactionStatus();

    String getTransactionType();

    void setAmount(Integer amount);

    void setAtmApplicationId(String atmApplicationId);

    void setCardNo(String cardNo);

    void setCurrency(String currency);

    void setCustomerAccountNumber(String customerAccountNumber);

    void setErrorCode(Integer errorCode);

    void setErrorDate(Date errorDate);

    void setErrorMessage(String errorMessage);

    void setLoadingDate(Date loadingDate);

    void setNotesPresented(String notesPresented);

    void setResponseCode(String responseCode);

    void setSettlementDate(Date settlementDate);

    void setTransactionData(String transactionData);

    void setTransactionDate(Date transactionDate);

    void setTransactionSequence(String transactionSequence);

    void setTransactionStatus(String transactionStatus);

    void setTransactionType(String transactionType);
    
}
