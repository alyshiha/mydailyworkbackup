/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface DisputeRulesDTOInter extends Serializable {

    int getColumnId();

    String getFormula();

    String getFormulaCode();

    int getId();

    String getName();

    int getValidationType();

    void setColumnId(int columnId);

    void setFormula(String formula);

    void setFormulaCode(String formulaCode);

    void setId(int id);

    void setName(String name);

    void setValidationType(int validationType);

}
