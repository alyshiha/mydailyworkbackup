/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface BlockUsersDTOInter extends Serializable {

    Date getBlockTime();

    BlockReasonDTOInter getReasonId()throws Throwable;

    UsersDTOInter getUserId()throws Throwable;

    void setBlockTime(Date blockTime);

    void setReasonId(BlockReasonDTOInter reasonId);

    void setUserId(UsersDTOInter userId);

}
