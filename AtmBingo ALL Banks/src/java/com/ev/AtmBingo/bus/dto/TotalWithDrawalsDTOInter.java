/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface TotalWithDrawalsDTOInter  extends Serializable {

    String getAtm_application_id();

    Integer getAtm_id();

    String getCurrency();

    Integer getTransaction();

    void setAtm_application_id(String atm_application_id);

    void setAtm_id(Integer atm_id);

    void setCurrency(String currency);

    void setTransaction(Integer transaction);
    
}
