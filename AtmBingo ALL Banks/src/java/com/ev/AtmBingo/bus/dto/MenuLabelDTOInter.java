/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface MenuLabelDTOInter extends Serializable {

    List<MenuLabelDTOInter> getChildMenu(int rest,int x) throws Throwable;
  List<MenuLabelDTOInter> getChildMenu(List<MenuLabelDTOInter> Menu,Integer Parent) throws Throwable;
    public int getRestrected() ;

    public void setRestrected(int restrected) ;
    
    List<ItemBingoDTOInter> getIbDTOL();

    void setIbDTOL(List<ItemBingoDTOInter> ibDTOL);
    
    //List<MenuLabelDTOInter> getChildMenu(int profileId) throws Throwable;

    void setChildMenu(List<MenuLabelDTOInter> childMenu);

    String getArabicLabel();

    String getEnglishLabel();

    int getMenuId();

    int getParentId();

    int getSeq();

    int getType();

    void setArabicLabel(String arabicLabel);

    String getPage();

    void setPage(String page);

    void setEnglishLabel(String englishLabel);

    void setMenuId(int menuId);

    void setParentId(int parentId);

    void setSeq(int seq);

    void setType(int type);

}
