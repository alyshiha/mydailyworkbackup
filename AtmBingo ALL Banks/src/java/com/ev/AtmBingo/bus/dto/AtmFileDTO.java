/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class AtmFileDTO extends BaseDTO implements Serializable, AtmFileDTOInter {

    private int id;
    private String name;
    private String buckupName;
    private Date loadingDate;
    private int template;
    private Date finishLaodingDate;
    private int numberOfTransaction;
    private int numberOfDuplication;
    private int type;
    private Date startloading;
    private Date endloading;

    public Date getEndloading() {
        return endloading;
    }

    public void setEndloading(Date endloading) {
        this.endloading = endloading;
    }

    public Date getStartloading() {
        return startloading;
    }

    public void setStartloading(Date startloading) {
        this.startloading = startloading;
    }

    public String getBuckupName() {
        return buckupName;
    }

    public void setBuckupName(String buckupName) {
        this.buckupName = buckupName;
    }

    public Date getFinishLaodingDate() {
        return finishLaodingDate;
    }

    public void setFinishLaodingDate(Date finishLaodingDate) {
        this.finishLaodingDate = finishLaodingDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getLoadingDate() {
        return loadingDate;
    }

    public void setLoadingDate(Date loadingDate) {
        this.loadingDate = loadingDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfDuplication() {
        return numberOfDuplication;
    }

    public void setNumberOfDuplication(int numberOfDuplication) {
        this.numberOfDuplication = numberOfDuplication;
    }

    public int getNumberOfTransaction() {
        return numberOfTransaction;
    }

    public void setNumberOfTransaction(int numberOfTransaction) {
        this.numberOfTransaction = numberOfTransaction;
    }

    public int getTemplate() {
        return template;
    }

    public void setTemplate(int template) {
        this.template = template;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    protected AtmFileDTO() {
        super();
    }
}
