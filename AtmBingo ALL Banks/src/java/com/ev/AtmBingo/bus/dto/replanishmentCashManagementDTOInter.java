/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface replanishmentCashManagementDTOInter {

    int getAddedcash();

    int getDiffdispensedjornal();

    void setDiffdispensedjornal(int diffdispensedjornal);

    int getDiffjournalswitch();

    void setDiffjournalswitch(int diffjournalswitch);

    int getAtmid();

    int getDispensedcash();

    Date getFromdate();

    int getRemainingcash();

    int getReplanishmentid();

    Date getTodate();

    int getWithdrwaljournaloffus();

    int getWithdrwaljournalonus();

    int getWithdrwalswitchoffus();

    int getWithdrwalswitchonus();

    void setAddedcash(int addedcash);

    void setAtmid(int atmid);

    void setDispensedcash(int dispensedcash);

    void setFromdate(Date fromdate);

    void setRemainingcash(int remainingcash);

    void setReplanishmentid(int replanishmentid);

    void setTodate(Date todate);

    void setWithdrwaljournaloffus(int withdrwaljournaloffus);

    void setWithdrwaljournalonus(int withdrwaljournalonus);

    void setWithdrwalswitchoffus(int withdrwalswitchoffus);

    void setWithdrwalswitchonus(int withdrwalswitchonus);

}
