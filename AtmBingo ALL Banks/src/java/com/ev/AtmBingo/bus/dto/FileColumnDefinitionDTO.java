/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class FileColumnDefinitionDTO extends BaseDTO implements FileColumnDefinitionDTOInter, Serializable {

    private int id,DISPLAYINLOADING,DUPLICATENO;
    private String name;
    private String columnName,cname;
    private int dataType;
    private String property;

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public int getDISPLAYINLOADING() {
        return DISPLAYINLOADING;
    }

    public void setDISPLAYINLOADING(int DISPLAYINLOADING) {
        this.DISPLAYINLOADING = DISPLAYINLOADING;
    }

    public int getDUPLICATENO() {
        return DUPLICATENO;
    }

    public void setDUPLICATENO(int DUPLICATENO) {
        this.DUPLICATENO = DUPLICATENO;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    protected FileColumnDefinitionDTO() {
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
