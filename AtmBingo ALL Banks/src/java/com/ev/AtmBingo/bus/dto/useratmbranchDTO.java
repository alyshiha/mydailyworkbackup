package com.ev.AtmBingo.bus.dto;
import com.ev.AtmBingo.base.dto.BaseDTO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;
public class useratmbranchDTO extends BaseDTO implements useratmbranchDTOInter, Serializable {
private int userid;
public int getuserid(){
return userid;
}
public void setuserid(int userid) {
this.userid = userid;
}
private int atmid;
public int getatmid(){
return atmid;
}
public void setatmid(int atmid) {
this.atmid = atmid;
}
}