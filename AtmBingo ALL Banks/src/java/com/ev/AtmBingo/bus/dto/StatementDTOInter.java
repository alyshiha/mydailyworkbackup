/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Aly.Shiha
 */
public interface StatementDTOInter {

    String getAtmid();

    BigDecimal getClosingbalance();

    Date getDatefrom();

    Date getDateto();

    BigDecimal getOpeningbalance();

    void setAtmid(String atmid);

    void setClosingbalance(BigDecimal closingbalance);

    void setDatefrom(Date datefrom);

    void setDateto(Date dateto);

    void setOpeningbalance(BigDecimal openingbalance);
    
}
