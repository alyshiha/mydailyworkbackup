/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Administrator
 */
public class NetworkDetailsDTO extends BaseDTO implements NetworkDetailsDTOInter, Serializable{

    private Integer groupId ;
    private Integer currency ;
    private BigDecimal rate ;
    private Integer rateType ;
    private String bin;
    private Integer amountTo ;
    private Integer amountFrom ;
    private Integer transactionType ;

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }
    
    public Integer getAmountTo() {
        return amountTo;
    }

    public void setAmountTo(Integer amountTo) {
        this.amountTo = amountTo;
    }
    public Integer getAmountFrom() {
        return amountFrom;
    }

    public void setAmountFrom(Integer amountFrom) {
        this.amountFrom = amountFrom;
    }

    public Integer getCurrency() {
        return currency;
    }

    public void setCurrency(Integer currency) {
        this.currency = currency;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getRateType() {
        return rateType;
    }

    public void setRateType(Integer rateType) {
        this.rateType = rateType;
    }
    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
    public Integer getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(Integer transactionType) {
        this.transactionType = transactionType;
    }


    protected NetworkDetailsDTO() {
        super();
    }

}
