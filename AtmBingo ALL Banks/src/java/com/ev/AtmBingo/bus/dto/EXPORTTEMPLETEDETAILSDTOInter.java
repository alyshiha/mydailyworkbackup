/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface EXPORTTEMPLETEDETAILSDTOInter extends Serializable {

    int getColumnid();

    String getFormat();

    int getSequence();

    int getTemplateid();

    void setColumnid(int columnid);

    void setFormat(String format);

    void setSequence(int sequence);

    void setTemplateid(int templateid);

}
