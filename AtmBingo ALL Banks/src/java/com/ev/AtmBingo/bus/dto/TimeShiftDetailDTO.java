/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import com.ev.AtmBingo.base.util.DateFormatter;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class TimeShiftDetailDTO extends BaseDTO implements Serializable, TimeShiftDetailDTOInter{

    private int shiftId;
    private int dayNo;
    private Date timeFrom;
    private Date timeTo;
    private int working;
    private Boolean workingB;

    public Boolean getWorkingB() {
        workingB = working == 1;
        return workingB;
    }

    public void setWorkingB(Boolean workingB) {
        if(workingB){
            setWorking(1);
        }
        else{
            setWorking(2);
        }
        this.workingB = workingB;
    }


    public int getDayNo() {
        return dayNo;
    }

    public void setDayNo(int dayNo) {
        this.dayNo = dayNo;
    }

    public int getShiftId() {
        return shiftId;
    }

    public void setShiftId(int shiftId) {
        this.shiftId = shiftId;
    }

    public Date getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(Date timeFrom) {
        this.timeFrom = timeFrom;
    }

    public Date getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(Date timeTo) {
        this.timeTo = timeTo;
    }

    public int getWorking() {
        return working;
    }

    public void setWorking(int working) {
        this.working = working;
    }
    
    protected TimeShiftDetailDTO() {
        super();
    }

}
