/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class AtmJournalDTO implements AtmJournalDTOInter {

    private int atmjournalid;
    private String transactiontype;
    private String debitaccount;
    private int amount;
    private int currency;
    private int currencyrate;
    private String creditaccount;
    private Date journaldate;
    private String journalrefernce;
    private Date datefrom;
    private Date dateto;
    private int repid;
    private int operation;
    private int exportflag;
    private String atmid;
    private String branch;
    private String indication;
    private String currencyname;

    public AtmJournalDTO() {
    }

    public String getCurrencyname() {
        return currencyname;
    }

    public void setCurrencyname(String currencyname) {
        this.currencyname = currencyname;
    }

    public AtmJournalDTO(int atmjournalid, String transactiontype, String debitaccount, int amount, int currency, int currencyrate, String creditaccount, Date journaldate, String journalrefernce, Date datefrom, Date dateto) {
        this.atmjournalid = atmjournalid;
        this.transactiontype = transactiontype;
        this.debitaccount = debitaccount;
        this.amount = amount;
        this.currency = currency;
        this.currencyrate = currencyrate;
        this.creditaccount = creditaccount;
        this.journaldate = journaldate;
        this.journalrefernce = journalrefernce;
        this.datefrom = datefrom;
        this.dateto = dateto;
    }

    @Override
    public int getRepid() {
        return repid;
    }

    @Override
    public void setRepid(int repid) {
        this.repid = repid;
    }

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getIndication() {
        return indication;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    @Override
    public int getOperation() {
        return operation;
    }

    @Override
    public void setOperation(int operation) {
        this.operation = operation;
    }

    @Override
    public int getExportflag() {
        return exportflag;
    }

    @Override
    public void setExportflag(int exportflag) {
        this.exportflag = exportflag;
    }

    @Override
    public int getAtmjournalid() {
        return atmjournalid;
    }

    @Override
    public void setAtmjournalid(int atmjournalid) {
        this.atmjournalid = atmjournalid;
    }

    @Override
    public String getTransactiontype() {
        return transactiontype;
    }

    @Override
    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    @Override
    public String getDebitaccount() {
        return debitaccount;
    }

    @Override
    public void setDebitaccount(String debitaccount) {
        this.debitaccount = debitaccount;
    }

    @Override
    public int getAmount() {
        return amount;
    }

    @Override
    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public int getCurrency() {
        return currency;
    }

    @Override
    public void setCurrency(int currency) {
        this.currency = currency;
    }

    @Override
    public int getCurrencyrate() {
        return currencyrate;
    }

    @Override
    public void setCurrencyrate(int currencyrate) {
        this.currencyrate = currencyrate;
    }

    @Override
    public String getCreditaccount() {
        return creditaccount;
    }

    @Override
    public void setCreditaccount(String creditaccount) {
        this.creditaccount = creditaccount;
    }

    @Override
    public Date getJournaldate() {
        return journaldate;
    }

    @Override
    public void setJournaldate(Date journaldate) {
        this.journaldate = journaldate;
    }

    @Override
    public String getJournalrefernce() {
        return journalrefernce;
    }

    @Override
    public void setJournalrefernce(String journalrefernce) {
        this.journalrefernce = journalrefernce;
    }

    @Override
    public Date getDatefrom() {
        return datefrom;
    }

    @Override
    public void setDatefrom(Date datefrom) {
        this.datefrom = datefrom;
    }

    @Override
    public Date getDateto() {
        return dateto;
    }

    @Override
    public void setDateto(Date dateto) {
        this.dateto = dateto;
    }

    public static String changeDateAndTimeFormat(Date d) {
        if (d != null) {
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
            String newDate = df.format(d);
            return newDate;
        } else {
            return "";
        }
    }
// method to return properties as a CSV string on one line
//public String toCSVString(Course c){

    public String toCSVString(String Separator) {
        String record = debitaccount + Separator
                + "EGP" + Separator
                + amount + Separator
                + changeDateAndTimeFormat(journaldate) + Separator
                + changeDateAndTimeFormat(journaldate) + Separator
                + journalrefernce + Separator
                + creditaccount + Separator
                + "11100000" + Separator
                + branch + "\n";

        return record;
    } //end toCSVString()
}
