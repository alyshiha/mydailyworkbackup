package com.ev.AtmBingo.bus.dto;
import com.ev.AtmBingo.base.dto.BaseDTO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;
public class usersbranchDTO extends BaseDTO implements usersbranchDTOInter, Serializable {
private int userid;
public int getuserid(){
return userid;
}
public void setuserid(int userid) {
this.userid = userid;
}
private String username;
public String getusername(){
return username;
}
public void setusername(String username) {
this.username = username;
}
private String logonname;
public String getlogonname(){
return logonname;
}
public void setlogonname(String logonname) {
this.logonname = logonname;
}
private String userpassword;
public String getuserpassword(){
return userpassword;
}
public void setuserpassword(String userpassword) {
this.userpassword = userpassword;
}
private String branchname;
public String getbranchname(){
return branchname;
}
public void setbranchname(String branchname) {
this.branchname = branchname;
}
private int branchid;
public int getbranchid(){
return branchid;
}
public void setbranchid(int branchid) {
this.branchid = branchid;
}
private int status;
public int getstatus(){
return status;
}
public void setstatus(int status) {
this.status = status;
}
}