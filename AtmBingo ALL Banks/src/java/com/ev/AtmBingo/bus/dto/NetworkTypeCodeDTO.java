/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public class NetworkTypeCodeDTO implements NetworkTypeCodeDTOInter {

    private Integer id;
    private Integer networkid;
    private String networkvalue;
    private String code;
    private Integer startposition;
    private Integer length;
    private Integer columnid;
    private String columnvalue;

    public String getNetworkvalue() {
        return networkvalue;
    }

    public void setNetworkvalue(String networkvalue) {
        this.networkvalue = networkvalue;
    }

    public String getColumnvalue() {
        return columnvalue;
    }

    public void setColumnvalue(String columnvalue) {
        this.columnvalue = columnvalue;
    }
   



    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getNetworkid() {
        return networkid;
    }

    @Override
    public void setNetworkid(Integer networkid) {
        this.networkid = networkid;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public Integer getStartposition() {
        return startposition;
    }

    @Override
    public void setStartposition(Integer startposition) {
        this.startposition = startposition;
    }

    @Override
    public Integer getLength() {
        return length;
    }

    @Override
    public void setLength(Integer length) {
        this.length = length;
    }

    @Override
    public Integer getColumnid() {
        return columnid;
    }

    @Override
    public void setColumnid(Integer columnid) {
        this.columnid = columnid;
    }

    public NetworkTypeCodeDTO(Integer id, Integer networkid, String code, Integer startposition, Integer length, Integer columnid) {
        this.id = id;
        this.networkid = networkid;
        this.code = code;
        this.startposition = startposition;
        this.length = length;
        this.columnid = columnid;
    }

    public NetworkTypeCodeDTO() {
    }

}
