/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author KhAiReE
 */
public interface SettlementLogDTOInter extends Serializable {

    String getSettledUsertext();

    void setSettledUsertext(String settledUsertext);

    String getMatchingTypetext();

    void setMatchingTypetext(String matchingTypetext);

    String getRecordTypetext();

    void setRecordTypetext(String recordTypetext);

    Integer getAmount();

    String getAtmApplicationId();

    String getCardNo();

    String getCurrency();

    Integer getMatchingType();

    Integer getRecordType();

    String getResponseCode();

    Integer getSettledFlag();

    Integer getSettledUser();

    Date getSettlementDate();

    Date getTransactionDate();

    String getTransactionSequence();

    void setAmount(Integer amount);

    void setAtmApplicationId(String atmApplicationId);

    void setCardNo(String cardNo);

    void setCurrency(String currency);

    void setMatchingType(Integer matchingType);

    void setRecordType(Integer recordType);

    void setResponseCode(String responseCode);

    void setSettledFlag(Integer settledFlag);

    void setSettledUser(Integer settledUser);

    void setSettlementDate(Date settlementDate);

    void setTransactionDate(Date transactionDate);

    void setTransactionSequence(String transactionSequence);

}
