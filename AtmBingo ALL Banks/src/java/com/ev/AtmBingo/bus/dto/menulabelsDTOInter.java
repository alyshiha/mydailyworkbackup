package com.ev.AtmBingo.bus.dto;
import java.io.Serializable;
import java.util.Date;
public interface menulabelsDTOInter extends Serializable {
int getmenuid();
void setmenuid(int menuid);
String getarabiclabel();
void setarabiclabel(String arabiclabel);
String getenglishlabel();
void setenglishlabel(String englishlabel);
int gettype();
void settype(int type);
int getparentid();
void setparentid(int parentid);
int getformid();
void setformid(int formid);
int getreportid();
void setreportid(int reportid);
int getseq();
void setseq(int seq);
int getvisability();
void setvisability(int visability);
String getpage();
void setpage(String page);
int getrestrected();
void setrestrected(int restrected);
}