/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author Aly.Shiha
 */
public interface DepositTransactionDTOInter  {

    String getAtmid();

    String getCardnumber();

    String getError();

    String getEscrow();

    String getEscrowdetail();

    String getRefrence();

    String getRefunded();

    String getRefundeddetail();

    String getRejected();

    String getRejecteddetail();

    Integer getRowid();

    Date getTransactiondate();

    String getValuted();

    String getValuteddetail();

    void setAtmid(String atmid);

    void setCardnumber(String cardnumber);

    void setError(String error);

    void setEscrow(String escrow);

    void setEscrowdetail(String escrowdetail);

    void setRefrence(String refrence);

    void setRefunded(String refunded);

    void setRefundeddetail(String refundeddetail);

    void setRejected(String rejected);

    void setRejecteddetail(String rejecteddetail);

    void setRowid(Integer rowid);

    void setTransactiondate(Date transactiondate);

    void setValuted(String valuted);

    void setValuteddetail(String valuteddetail);
    
}
