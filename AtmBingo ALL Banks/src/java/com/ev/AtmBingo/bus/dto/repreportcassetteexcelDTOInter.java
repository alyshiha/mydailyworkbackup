/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface repreportcassetteexcelDTOInter extends Serializable {

    String getCassete();

    String getCassetevalue();

    String getDetailid();

    String getRemaining();

    String getSequence();

    void setCassete(String cassete);

    void setCassetevalue(String cassetevalue);

    void setDetailid(String detailid);

    void setRemaining(String remaining);

    void setSequence(String sequence);
    
}
