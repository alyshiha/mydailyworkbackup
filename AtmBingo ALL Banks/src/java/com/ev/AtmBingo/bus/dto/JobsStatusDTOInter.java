/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface JobsStatusDTOInter extends Serializable {

    int getHostValidation();

    int getHostValidationJobId();

    int getJournalValidation();

    int getJournalValidationJobId();

    int getLoading();

    int getLoadingJobId();

    int getMatching();

    int getMatchingJobId();

    int getStackId();

    int getSwitchValidation();

    int getSwitchValidationJobId();

    void setHostValidation(int hostValidation);

    void setHostValidationJobId(int hostValidationJobId);

    void setJournalValidation(int journalValidation);

    void setJournalValidationJobId(int journalValidationJobId);

    void setLoading(int loading);

    void setLoadingJobId(int loadingJobId);

    void setMatching(int matching);

    void setMatchingJobId(int matchingJobId);

    void setStackId(int stackId);

    void setSwitchValidation(int switchValidation);

    void setSwitchValidationJobId(int switchValidationJobId);

}
