/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface ExportLogDTOInter  extends Serializable {

    Integer getAmount();

    String getAtmApplicationId();

    String getCardNo();

    Integer getExportState();

    Integer getExportUser();

    Date getExport_date();

    String getCurrency();

    Integer getMatchingType();

    Integer getRecordType();

    String getResponseCode();

    Date getTransactionDate();

    String getTransactionSequence();

    void setAmount(Integer amount);

    void setAtmApplicationId(String atmApplicationId);

    void setCardNo(String cardNo);

    void setExportState(Integer ExportState);

    void setExportUser(Integer ExportUser);

    void setExport_date(Date Export_date);

    void setCurrency(String currency);

    void setMatchingType(Integer matchingType);

    void setRecordType(Integer recordType);

    void setResponseCode(String responseCode);

    void setTransactionDate(Date transactionDate);

    void setTransactionSequence(String transactionSequence);

}
