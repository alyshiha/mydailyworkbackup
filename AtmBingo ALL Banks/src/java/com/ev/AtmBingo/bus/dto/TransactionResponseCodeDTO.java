/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class TransactionResponseCodeDTO extends BaseDTO implements Serializable, TransactionResponseCodeDTOInter{

    private int id;
    private String name;
    private String description;
    private int amountType;
    private Boolean amountTypeB;
    private int defaultFlag;
    private int masterCode;
    private Boolean defaultFlagB;

    public Boolean getDefaultFlagB() {
        defaultFlagB = defaultFlag == 1;
        return defaultFlagB;
    }

    public void setDefaultFlagB(Boolean defaultFlagB) {
        if(defaultFlagB)
            defaultFlag = 1;
        else
            defaultFlag = 2;
        this.defaultFlagB = defaultFlagB;
    }


    public int getAmountType() {
        return amountType;
    }

    public void setAmountType(int amountType) {
        this.amountType = amountType;
    }

    public int getDefaultFlag() {
        return defaultFlag;
    }

    public void setDefaultFlag(int defaultFlag) {
        this.defaultFlag = defaultFlag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMasterCode() {
        return masterCode;
    }

    public void setMasterCode(int masterCode) {
        this.masterCode = masterCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected TransactionResponseCodeDTO() {
        super();
    }

}
