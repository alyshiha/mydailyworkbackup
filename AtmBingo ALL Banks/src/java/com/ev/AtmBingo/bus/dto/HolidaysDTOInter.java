/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface HolidaysDTOInter extends Serializable {

    String getSmonth();

    Integer getDayCount();

   void setDayCount(Integer dayCount);

    Integer getDay();

    int getMonth();

    String getaReason();

    String geteReason();

    void setSmonth(String smonth);

    void setDay(Integer day);

    void setMonth(int month);

    void setaReason(String aReason);

    void seteReason(String eReason);

}
