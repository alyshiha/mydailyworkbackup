/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author KhAiReE
 */
public interface BingoLogDTOInter extends Serializable {

    String getAction();

    Date getActionDate();

    Integer getId();

    String getIpAddress();

    Integer getUserId();

    void setAction(String action);

    void setActionDate(Date actionDate);

    void setId(Integer id);

    void setIpAddress(String ipAddress);

    void setUserId(Integer userId);

}
