/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface MatchingTypeMachinesDTOInter extends Serializable {

    int getDefaultMatchingSetting();

    int getMachineType();

    int getMatchingType();

    void setDefaultMatchingSetting(int defaultMatchingSetting);

    void setMachineType(int machineType);

    void setMatchingType(int matchingType);

}
