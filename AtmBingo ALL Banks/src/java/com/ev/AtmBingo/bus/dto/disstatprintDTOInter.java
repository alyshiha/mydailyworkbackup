/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface disstatprintDTOInter  extends Serializable {

    String getAbsamount();

    String getAmount();

    String getAtmapplicationid();

    String getAtmname();

    String getAutho();

    String getBranch();

    String getCardnum();

    String getComments();

    String getCurrency();

    String getCustomeraccountnumber();

    String getDisputedate();

    String getDisputereason();

    String getFilename();

    String getGLACCOUNTATM();

    String getLoadingdate();

    String getNotesPresented();

    String getNotespresented();

    String getRecordtype();

    String getResponsecode();

    String getSettleddate();

    String getSettleduser();

    String getSettlementdate();

    String getTransactiondate();

    String getTransactionsequence();

    String getTransactionstatus();

    String getTransactiontype();

    String getTransdata();

    void setAbsamount(String absamount);

    void setAmount(String amount);

    void setAtmapplicationid(String atmapplicationid);

    void setAtmname(String atmname);

    void setAutho(String Autho);

    void setBranch(String Branch);

    void setCardnum(String cardnum);

    void setComments(String comments);

    void setCurrency(String currency);

    void setCustomeraccountnumber(String customeraccountnumber);

    void setDisputedate(String disputedate);

    void setDisputereason(String disputereason);

    void setFilename(String filename);

    void setGLACCOUNTATM(String GLACCOUNTATM);

    void setLoadingdate(String loadingdate);

    void setNotesPresented(String NotesPresented);

    void setNotespresented(String notespresented);

    void setRecordtype(String recordtype);

    void setResponsecode(String responsecode);

    void setSettleddate(String settleddate);

    void setSettleduser(String settleduser);

    void setSettlementdate(String settlementdate);

    void setTransactiondate(String transactiondate);

    void setTransactionsequence(String transactionsequence);

    void setTransactionstatus(String transactionstatus);

    void setTransactiontype(String transactiontype);

    void setTransdata(String transdata);
    
}
