/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author KhAiReE
 */
public class SettlementLogDTO extends BaseDTO implements SettlementLogDTOInter, Serializable{

    protected SettlementLogDTO() {
        super();
    }
    String atmApplicationId ;
    String currency ;
    Date transactionDate ;
    String transactionSequence;
    String cardNo ;
    Integer amount ;
    Date settlementDate ;
    String responseCode ;
    Integer matchingType ;
    Integer recordType;
    Integer settledUser ;
    Integer settledFlag ;
String settledUsertext ;
String matchingTypetext ;
String recordTypetext;


    public String getSettledUsertext() {
        return settledUsertext;
    }

    public void setSettledUsertext(String settledUsertext) {
        this.settledUsertext = settledUsertext;
    }

    public String getMatchingTypetext() {
        return matchingTypetext;
    }

    public void setMatchingTypetext(String matchingTypetext) {
        this.matchingTypetext = matchingTypetext;
    }

    public String getRecordTypetext() {
        return recordTypetext;
    }

    public void setRecordTypetext(String recordTypetext) {
        this.recordTypetext = recordTypetext;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getAtmApplicationId() {
        return atmApplicationId;
    }

    public void setAtmApplicationId(String atmApplicationId) {
        this.atmApplicationId = atmApplicationId;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getMatchingType() {
        return matchingType;
    }

    public void setMatchingType(Integer matchingType) {
        this.matchingType = matchingType;
    }

    public Integer getRecordType() {
        return recordType;
    }

    public void setRecordType(Integer recordType) {
        this.recordType = recordType;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getSettledFlag() {
        return settledFlag;
    }

    public void setSettledFlag(Integer settledFlag) {
        this.settledFlag = settledFlag;
    }

    public Integer getSettledUser() {
        return settledUser;
    }

    public void setSettledUser(Integer settledUser) {
        this.settledUser = settledUser;
    }

    public Date getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(Date settlementDate) {
        this.settlementDate = settlementDate;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionSequence() {
        return transactionSequence;
    }

    public void setTransactionSequence(String transactionSequence) {
        this.transactionSequence = transactionSequence;
    }



}
