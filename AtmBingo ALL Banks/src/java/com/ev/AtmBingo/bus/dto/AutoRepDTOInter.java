/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface AutoRepDTOInter extends Serializable  {


        String getrowid();

    void setrowid(String rowid);

        String getGroupName();

    void setGroupName(String groupName);

    Integer getCurrency();

    Integer getGroupid();

    Integer getTypecassette();

    Integer getTypenumber();

    void setCurrency(Integer currency);

    void setGroupid(Integer groupid);

    void setTypecassette(Integer typecassette);

    void setTypenumber(Integer typenumber);

}
