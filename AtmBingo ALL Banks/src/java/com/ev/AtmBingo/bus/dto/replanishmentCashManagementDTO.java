/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class replanishmentCashManagementDTO implements replanishmentCashManagementDTOInter {

    private int replanishmentid;
    private Date fromdate;
    private Date todate;
    private int atmid;
    private int addedcash;
    private int remainingcash;
    private int dispensedcash;
    private int withdrwaljournalonus;
    private int withdrwaljournaloffus;
    private int withdrwalswitchonus;
    private int withdrwalswitchoffus;
    private int diffdispensedjornal;
    private int diffjournalswitch;

    public replanishmentCashManagementDTO() {
    }

    public replanishmentCashManagementDTO(int replanishmentid, Date fromdate, Date todate, int atmid, int addedcash, int remainingcash, int dispensedcash, int withdrwaljournalonus, int withdrwaljournaloffus, int withdrwalswitchonus, int withdrwalswitchoffus) {
        this.replanishmentid = replanishmentid;
        this.fromdate = fromdate;
        this.todate = todate;
        this.atmid = atmid;
        this.addedcash = addedcash;
        this.remainingcash = remainingcash;
        this.dispensedcash = dispensedcash;
        this.withdrwaljournalonus = withdrwaljournalonus;
        this.withdrwaljournaloffus = withdrwaljournaloffus;
        this.withdrwalswitchonus = withdrwalswitchonus;
        this.withdrwalswitchoffus = withdrwalswitchoffus;
    }

    @Override
    public int getReplanishmentid() {
        return replanishmentid;
    }

    @Override
    public void setReplanishmentid(int replanishmentid) {
        this.replanishmentid = replanishmentid;
    }

    @Override
    public int getDiffdispensedjornal() {
        return diffdispensedjornal;
    }

    @Override
    public void setDiffdispensedjornal(int diffdispensedjornal) {
        this.diffdispensedjornal = diffdispensedjornal;
    }

    @Override
    public int getDiffjournalswitch() {
        return diffjournalswitch;
    }

    @Override
    public void setDiffjournalswitch(int diffjournalswitch) {
        this.diffjournalswitch = diffjournalswitch;
    }

    @Override
    public Date getFromdate() {
        return fromdate;
    }

    @Override
    public void setFromdate(Date fromdate) {
        this.fromdate = fromdate;
    }

    @Override
    public Date getTodate() {
        return todate;
    }

    @Override
    public void setTodate(Date todate) {
        this.todate = todate;
    }

    @Override
    public int getAtmid() {
        return atmid;
    }

    @Override
    public void setAtmid(int atmid) {
        this.atmid = atmid;
    }

    @Override
    public int getAddedcash() {
        return addedcash;
    }

    @Override
    public void setAddedcash(int addedcash) {
        this.addedcash = addedcash;
    }

    @Override
    public int getRemainingcash() {
        return remainingcash;
    }

    @Override
    public void setRemainingcash(int remainingcash) {
        this.remainingcash = remainingcash;
    }

    @Override
    public int getDispensedcash() {
        return dispensedcash;
    }

    @Override
    public void setDispensedcash(int dispensedcash) {
        this.dispensedcash = dispensedcash;
    }

    @Override
    public int getWithdrwaljournalonus() {
        return withdrwaljournalonus;
    }

    @Override
    public void setWithdrwaljournalonus(int withdrwaljournalonus) {
        this.withdrwaljournalonus = withdrwaljournalonus;
    }

    @Override
    public int getWithdrwaljournaloffus() {
        return withdrwaljournaloffus;
    }

    @Override
    public void setWithdrwaljournaloffus(int withdrwaljournaloffus) {
        this.withdrwaljournaloffus = withdrwaljournaloffus;
    }

    @Override
    public int getWithdrwalswitchonus() {
        return withdrwalswitchonus;
    }

    @Override
    public void setWithdrwalswitchonus(int withdrwalswitchonus) {
        this.withdrwalswitchonus = withdrwalswitchonus;
    }

    @Override
    public int getWithdrwalswitchoffus() {
        return withdrwalswitchoffus;
    }

    @Override
    public void setWithdrwalswitchoffus(int withdrwalswitchoffus) {
        this.withdrwalswitchoffus = withdrwalswitchoffus;
    }

}
