/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface UserProfileDTOInter extends Serializable {

    int getProfileId();

    int getUserId();

    void setProfileId(int ProfileId);

    void setUserId(int UserId);

}
