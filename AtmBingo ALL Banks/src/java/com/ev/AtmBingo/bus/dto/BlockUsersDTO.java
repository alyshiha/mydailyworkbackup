/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import com.ev.AtmBingo.bus.dao.BlockReasonDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class BlockUsersDTO extends BaseDTO implements BlockUsersDTOInter, Serializable{

    private UsersDTOInter userId;
    private Date blockTime;
    private BlockReasonDTOInter reasonId;

    public Date getBlockTime() {
        return blockTime;
    }

    public void setBlockTime(Date blockTime) {
        this.blockTime = blockTime;
    }

    public BlockReasonDTOInter getReasonId() throws Throwable{
        BlockReasonDAOInter brDAO = DAOFactory.createBlockReasonDAO(null);
        reasonId = (BlockReasonDTOInter)brDAO.find(reasonId.getReasonId());
        return reasonId;
    }

    public void setReasonId(BlockReasonDTOInter reasonId) {
        this.reasonId = reasonId;
    }

    public UsersDTOInter getUserId()throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        userId = (UsersDTOInter)uDAO.find(userId.getUserId());
        return userId;
    }

    public void setUserId(UsersDTOInter userId) {
        this.userId = userId;
    }
    
    protected BlockUsersDTO() {
        super();
        userId = DTOFactory.createUsersDTO();
        reasonId = DTOFactory.createBlockReasonDTO();
    }

}
