/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class ReplanishmentReportDTO extends BaseDTO implements ReplanishmentReportDTOInter, Serializable{

    private int id;
    private int repId;
    private int repId2;

    public int getRepId2() {
        return repId2;
    }

    public void setRepId2(int repId2) {
        this.repId2 = repId2;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRepId() {
        return repId;
    }

    public void setRepId(int repId) {
        this.repId = repId;
    }
    
    protected ReplanishmentReportDTO() {
        super();
    }


}
