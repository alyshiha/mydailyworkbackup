/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

/**
 *
 * @author shi7a
 */
public class DepositDTO implements DepositDTOInter {

    private String cassetteName;

    private int cassetteCount;
    private int cassetteValue;

    private int retractCount;
    private int retractValue;

    private int recycledCount;
    private int recycledValue;

    private int depositCount;
    private int depositValue;

    @Override
    public String getCassetteName() {
        return cassetteName;
    }

    @Override
    public void setCassetteName(String cassetteName) {
        this.cassetteName = cassetteName;
    }

    @Override
    public int getCassetteCount() {
        return cassetteCount;
    }

    @Override
    public void setCassetteCount(int cassetteCount) {
        this.cassetteCount = cassetteCount;
    }

    @Override
    public int getCassetteValue() {
        return cassetteValue;
    }

    @Override
    public void setCassetteValue(int cassetteValue) {
        this.cassetteValue = cassetteValue;
    }

    @Override
    public int getRetractCount() {
        return retractCount;
    }

    @Override
    public void setRetractCount(int retractCount) {
        this.retractCount = retractCount;
    }

    @Override
    public int getRetractValue() {
        return retractValue;
    }

    @Override
    public void setRetractValue(int retractValue) {
        this.retractValue = retractValue;
    }

    @Override
    public int getRecycledCount() {
        return recycledCount;
    }

    @Override
    public void setRecycledCount(int recycledCount) {
        this.recycledCount = recycledCount;
    }

    @Override
    public int getRecycledValue() {
        return recycledValue;
    }

    @Override
    public void setRecycledValue(int recycledValue) {
        this.recycledValue = recycledValue;
    }

    @Override
    public int getDepositCount() {
        return depositCount;
    }

    @Override
    public void setDepositCount(int depositCount) {
        this.depositCount = depositCount;
    }

    @Override
    public int getDepositValue() {
        return depositValue;
    }

    @Override
    public void setDepositValue(int depositValue) {
        this.depositValue = depositValue;
    }

}
