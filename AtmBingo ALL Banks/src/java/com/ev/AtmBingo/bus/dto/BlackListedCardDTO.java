/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class BlackListedCardDTO extends BaseDTO implements Serializable, BlackListedCardDTOInter{

    private String cardNo;
    private String customerNo;
    private String comments;
    private Date blackListedDate;

    public Date getBlackListedDate() {
        return blackListedDate;
    }

    public void setBlackListedDate(Date blackListedDate) {
        this.blackListedDate = blackListedDate;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    
    protected BlackListedCardDTO() {
        super();
    }

}
