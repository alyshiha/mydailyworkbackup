package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;

public class allbranchtransactionssearchDTO extends BaseDTO implements allbranchtransactionssearchDTOInter, Serializable {

    private int defaulttrans;

    public int getdefaulttrans() {
        return defaulttrans;
    }

    public void setdefaulttrans(int defaulttrans) {
        this.defaulttrans = defaulttrans;
    }
    private Boolean defaulttransbol;

    public Boolean getdefaulttransbol() {
        return defaulttransbol;
    }

    public void setdefaulttransbol(Boolean defaulttransbol) {
        this.defaulttransbol = defaulttransbol;
    }
    private int userid;

    public int getuserid() {
        return userid;
    }

    public void setuserid(int userid) {
        this.userid = userid;
    }
    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }

    private int atmid;

    public int getatmid() {
        return atmid;
    }

    public void setatmid(int atmid) {
        this.atmid = atmid;
    }

    private int branchid;

    public int getbranchid() {
        return branchid;
    }

    public void setbranchid(int branchid) {
        this.branchid = branchid;
    }
    private int reasonid;

    public int getreasonid() {
        return reasonid;
    }

    public void setreasonid(int reasonid) {
        this.reasonid = reasonid;
    }

    private int transactiontypeid;

    public int gettransactiontypeid() {
        return transactiontypeid;
    }

    public void settransactiontypeid(int transactiontypeid) {
        this.transactiontypeid = transactiontypeid;
    }

    private int responsecodeid;

    public int getresponsecodeid() {
        return responsecodeid;
    }

    public void setresponsecodeid(int responsecodeid) {
        this.responsecodeid = responsecodeid;
    }

    private int responsecodeidop;

    public int getresponsecodeidop() {
        return responsecodeid;
    }

    public void setresponsecodeidop(int responsecodeidop) {
        this.responsecodeidop = responsecodeidop;
    }

    private Date transactiondatefrom;

    public Date gettransactiondatefrom() {
        return transactiondatefrom;
    }

    public void settransactiondatefrom(Date transactiondatefrom) {
        this.transactiondatefrom = transactiondatefrom;
    }
    private Date transactiondateto;

    public Date gettransactiondateto() {
        return transactiondateto;
    }

    public void settransactiondateto(Date transactiondateto) {
        this.transactiondateto = transactiondateto;
    }

    private String transactiondateoper;

    public String gettransactiondateoper() {
        return transactiondateoper;
    }

    public void settransactiondateoper(String transactiondateoper) {
        this.transactiondateoper = transactiondateoper;
    }

    private String cardno;

    public String getcardno() {
        return cardno;
    }

    public void setcardno(String cardno) {
        this.cardno = cardno;
    }

    private String customeraccountnumber;

    public String getcustomeraccountnumber() {
        return customeraccountnumber;
    }

    public void setcustomeraccountnumber(String customeraccountnumber) {
        this.customeraccountnumber = customeraccountnumber;
    }

    private String cardop;

    public String getcardop() {
        return cardop;
    }

    public void setcardop(String cardop) {
        this.cardop = cardop;
    }

    private String custaccnumop;

    public String getcustaccnumop() {
        return custaccnumop;
    }

    public void setcustaccnumop(String custaccnumop) {
        this.custaccnumop = custaccnumop;
    }
    
    
    private String tempname;

    public String gettempname() {
        return tempname;
    }

    public void settempname(String tempname) {
        this.tempname = tempname;
    }

}
