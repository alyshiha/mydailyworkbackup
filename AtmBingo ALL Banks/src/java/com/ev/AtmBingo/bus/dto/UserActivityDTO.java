/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class UserActivityDTO extends BaseDTO implements UserActivityDTOInter, Serializable{

    private UsersDTOInter uDTO;
    private int countSett;
    private int countDis;
    private int User;
    private int maxcountSett;
    private int maxcountDis;
    public int getMaxcountDis() {
        return maxcountDis;
    }

    public void setMaxcountDis(int maxcountDis) {
        this.maxcountDis = maxcountDis;
    }

    public int getMaxcountSett() {
        return maxcountSett;
    }

    public void setMaxcountSett(int maxcountSett) {
        this.maxcountSett = maxcountSett;
    }

    public int getCountDis() {
        return countDis;
    }

    public void setCountDis(int countDis) {
        this.countDis = countDis;
    }

    public int getUser() {
        return User;
    }

    public void setUser(int User) {
        
        this.User = User;
    }

    public int getCountSett() {
        return countSett;
    }

    public void setCountSett(int countSett) {
        this.countSett = countSett;
    }

    public UsersDTOInter getuDTO() {
        return uDTO;
    }

    public void setuDTO(UsersDTOInter uDTO) {
        this.uDTO = uDTO;
    }
    
    protected UserActivityDTO() {
        super();
    }

}
