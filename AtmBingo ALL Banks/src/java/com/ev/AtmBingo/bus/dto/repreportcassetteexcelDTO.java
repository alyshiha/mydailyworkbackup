/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class repreportcassetteexcelDTO implements repreportcassetteexcelDTOInter , Serializable{
 private String sequence,cassete,cassetevalue,remaining,detailid;

    @Override
    public String getCassete() {
        return cassete;
    }

    @Override
    public void setCassete(String cassete) {
        this.cassete = cassete;
    }

    @Override
    public String getCassetevalue() {
        return cassetevalue;
    }

    @Override
    public void setCassetevalue(String cassetevalue) {
        this.cassetevalue = cassetevalue;
    }

    @Override
    public String getDetailid() {
        return detailid;
    }

    @Override
    public void setDetailid(String detailid) {
        this.detailid = detailid;
    }

    @Override
    public String getRemaining() {
        return remaining;
    }

    @Override
    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    @Override
    public String getSequence() {
        return sequence;
    }

    @Override
    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
 
}
