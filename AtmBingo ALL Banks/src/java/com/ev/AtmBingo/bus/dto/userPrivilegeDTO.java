/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public class userPrivilegeDTO implements userPrivilegeDTOInter {
    private int userprevid;
    private int userid;
    private int previd;
    private int menuid;

    public userPrivilegeDTO() {
    }

    public userPrivilegeDTO(int userprevid, int userid, int previd, int menuid) {
        this.userprevid = userprevid;
        this.userid = userid;
        this.previd = previd;
        this.menuid = menuid;
    }

    @Override
    public int getUserprevid() {
        return userprevid;
    }

    @Override
    public void setUserprevid(int userprevid) {
        this.userprevid = userprevid;
    }

    @Override
    public int getUserid() {
        return userid;
    }

    @Override
    public void setUserid(int userid) {
        this.userid = userid;
    }

    @Override
    public int getPrevid() {
        return previd;
    }

    @Override
    public void setPrevid(int previd) {
        this.previd = previd;
    }

    @Override
    public int getMenuid() {
        return menuid;
    }

    @Override
    public void setMenuid(int menuid) {
        this.menuid = menuid;
    }
    
}
