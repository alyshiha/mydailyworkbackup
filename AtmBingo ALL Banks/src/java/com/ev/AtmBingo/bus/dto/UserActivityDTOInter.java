/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface UserActivityDTOInter extends Serializable {

    int getMaxcountDis();

     void setMaxcountDis(int maxcountDis);

     int getMaxcountSett();

     void setMaxcountSett(int maxcountSett) ;

    int getCountDis();

    int getCountSett();

    UsersDTOInter getuDTO();

    void setCountDis(int countDis);

    void setCountSett(int countSett);

    void setuDTO(UsersDTOInter uDTO);

     void setUser(int user);

     int getUser();
}
