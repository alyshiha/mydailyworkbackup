/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface atmremainingDTOInter  extends Serializable {
void setTodate(Date todate);
Date getTodate();
void setFromdate(Date fromdate);
Date getFromdate() ;
    String getAtmapplicationid();

    String getGroup();

    String getName();

    BigDecimal getRemaining();

    void setAtmapplicationid(String atmapplicationid);

    void setGroup(String group);

    void setName(String name);

    void setRemaining(BigDecimal remaining);
    
}
