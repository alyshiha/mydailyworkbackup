/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;



/**
 *
 * @author Administrator
 */
public class BingostatsearchDTO implements BingostatsearchDTOInter , Serializable{
    private int matchingtype,recordtype,userid;
    private Date fromdate,todate;
    private String searchtype,atmid;

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    public Date getFromdate() {
        return fromdate;
    }

    public void setFromdate(Date fromdate) {
        this.fromdate = fromdate;
    }

    public int getMatchingtype() {
        return matchingtype;
    }

    public void setMatchingtype(int matchingtype) {
        this.matchingtype = matchingtype;
    }

    public int getRecordtype() {
        return recordtype;
    }

    public void setRecordtype(int recordtype) {
        this.recordtype = recordtype;
    }

    public String getSearchtype() {
        return searchtype;
    }

    public void setSearchtype(String searchtype) {
        this.searchtype = searchtype;
    }

    public Date getTodate() {
        return todate;
    }

    public void setTodate(Date todate) {
        this.todate = todate;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }
    

}
