/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface HostFileTemplateDTOInter extends Serializable {

    void setTagendvalue(String tagendvalue);

    String getTagendvalue();

    Integer getTagenddatatype();

    void setTagenddatatype(Integer tagenddatatype);

    String getTagendformate();

    void setTagendformate(String tagendformate);

    Integer getTagendlength();

    void setTagendlength(Integer tagendlength);

    Integer getTagendpostition();

    void setTagendpostition(Integer tagendpostition);

    String getAtmadd();

    void setAtmadd(String atmadd);

    Boolean getActiveB();

    void setActiveB(Boolean activeB);

    void setHostFileTemplateDetailsDTOL(List<HostFileTemplateDetailDTOInter> hostFileTemplateDetailsDTOL);

    List<HostFileTemplateDetailDTOInter> getHostFileTemplateDetailsDTOL() throws Throwable;

    List<HostFileHeaderDTOInter> getHostFileHeaderDTOL() throws Throwable;

    void setHostFileHeaderDTOL(List<HostFileHeaderDTOInter> hostFileHeaderDTOL);

    int getActive();

    String getBuckupFolder();

    String getCopyFolder();

    String getDateSeparator();

    BigDecimal getDateSeparatorPos1();

    BigDecimal getDateSeparatorPos2();

    BigDecimal getHeaderDataType();

    String getHeaderDateSeparator();

    BigDecimal getHeaderDateSeparatorPos1();

    BigDecimal getHeaderDateSeparatorPos2();

    String getHeaderFormat();

    BigDecimal getHeaderLength();

    String getHeaderPosition();

    String getHeaderString();

    int getId();

    BigDecimal getIgnoredLines();

    String getLoadingFolder();

    String getName();

    BigDecimal getNumberOfLines();

    BigDecimal getProcessingType();

    String getSeparator();

    String getServerFolder();

    BigDecimal getStartingDataType();

    String getStartingFormat();

    BigDecimal getStartingLength();

    BigDecimal getStartingPosition();

    String getStartingValue();

    void setActive(int active);

    void setBuckupFolder(String buckupFolder);

    void setCopyFolder(String copyFolder);

    void setDateSeparator(String dateSeparator);

    void setDateSeparatorPos1(BigDecimal dateSeparatorPos1);

    void setDateSeparatorPos2(BigDecimal dateSeparatorPos2);

    void setHeaderDataType(BigDecimal headerDataType);

    void setHeaderDateSeparator(String headerDateSeparator);

    void setHeaderDateSeparatorPos1(BigDecimal headerDateSeparatorPos1);

    void setHeaderDateSeparatorPos2(BigDecimal headerDateSeparatorPos2);

    void setHeaderFormat(String headerFormat);

    void setHeaderLength(BigDecimal headerLength);

    void setHeaderPosition(String headerPosition);

    void setHeaderString(String headerString);

    void setId(int id);

    void setIgnoredLines(BigDecimal ignoredLines);

    void setLoadingFolder(String loadingFolder);

    void setName(String name);

    void setNumberOfLines(BigDecimal numberOfLines);

    void setProcessingType(BigDecimal processingType);

    void setSeparator(String separator);

    void setServerFolder(String serverFolder);

    void setStartingDataType(BigDecimal startingDataType);

    void setStartingFormat(String startingFormat);

    void setStartingLength(BigDecimal startingLength);

    void setStartingPosition(BigDecimal startingPosition);

    void setStartingValue(String startingValue);
}
