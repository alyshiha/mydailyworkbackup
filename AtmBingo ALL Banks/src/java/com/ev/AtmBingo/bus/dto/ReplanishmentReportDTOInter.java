/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface ReplanishmentReportDTOInter extends Serializable {

    public int getRepId2() ;

    public void setRepId2(int repId2) ;

    int getId();

    int getRepId();

    void setId(int id);

    void setRepId(int repId);

}
