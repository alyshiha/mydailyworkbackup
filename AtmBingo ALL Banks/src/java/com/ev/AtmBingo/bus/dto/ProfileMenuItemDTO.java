/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class ProfileMenuItemDTO extends BaseDTO implements Serializable , ProfileMenuItemDTOInter{

    protected  ProfileMenuItemDTO() {
        super();
    }
    private int ProfileId;
    private int MenuId;
    private int ItemId;

    public int getItemId() {
        return ItemId;
    }

    public void setItemId(int ItemId) {
        this.ItemId = ItemId;
    }

    public int getMenuId() {
        return MenuId;
    }

    public void setMenuId(int MenuId) {
        this.MenuId = MenuId;
    }

    public int getProfileId() {
        return ProfileId;
    }

    public void setProfileId(int ProfileId) {
        this.ProfileId = ProfileId;
    }

}
