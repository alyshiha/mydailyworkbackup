/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class LoadingInsertErrorsDTO implements LoadingInsertErrorsDTOInter {
    private int fileID;
    private Date errorDate;
    private int errorCode;
    private String errorText;
    private String fileName;
    private String transactionData;

    public LoadingInsertErrorsDTO() {
    }

    public LoadingInsertErrorsDTO(int fileID, Date errorDate, int errorCode, String errorText, String fileName, String transactionData) {
        this.fileID = fileID;
        this.errorDate = errorDate;
        this.errorCode = errorCode;
        this.errorText = errorText;
        this.fileName = fileName;
        this.transactionData = transactionData;
    }

    @Override
    public int getFileID() {
        return fileID;
    }

    @Override
    public void setFileID(int fileID) {
        this.fileID = fileID;
    }

    @Override
    public Date getErrorDate() {
        return errorDate;
    }

    @Override
    public void setErrorDate(Date errorDate) {
        this.errorDate = errorDate;
    }

    @Override
    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String getErrorText() {
        return errorText;
    }

    @Override
    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String getTransactionData() {
        return transactionData;
    }

    @Override
    public void setTransactionData(String transactionData) {
        this.transactionData = transactionData;
    }
    
}
