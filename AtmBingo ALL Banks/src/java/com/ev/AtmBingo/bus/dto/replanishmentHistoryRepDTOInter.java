/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface replanishmentHistoryRepDTOInter  extends Serializable{
 String getAtm();
 void setAtm(String atm) ;
    String getCashfeed();

    String getDifferent();

    String getDispendedcash();

    String getDisputecash();

    String getFromdate();

    String getRemainingcash();

    String getSwitchcash();

    String getTodate();

    void setCashfeed(String cashfeed);

    void setDifferent(String different);

    void setDispendedcash(String dispendedcash);

    void setDisputecash(String disputecash);

    void setFromdate(String fromdate);

    void setRemainingcash(String remainingcash);

    void setSwitchcash(String switchcash);

    void setTodate(String todate);
     String getCurrency() ;
      void setCurrency(String currency) ;
}
