/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface NotificationDTOInter extends Serializable{

    Integer getId();

    void setId(Integer id);

    String getAction();

    void setAction(String action);

    Date getActionDate();

    void setActionDate(Date actionDate);

    Integer getUserId() ;

    void setUserId(Integer userId);

    Integer getType();

    void setType(Integer type);
    
}
