/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author ISLAM
 */
public interface FileColumnDisplayDTOInter extends Serializable{

    List<FileColumnDisplayDetailsDTOInter> getFileColumnDisplayDetailsDTOL()throws Throwable;

    FileColumnDefinitionDTOInter getColumnId();

    int getId();

    void setColumnId(FileColumnDefinitionDTOInter columnId);

    void setId(int id);

    void setFileColumnDisplayDetailsDTOL(List<FileColumnDisplayDetailsDTOInter> fileColumnDisplayDetailsDTOL);

}
