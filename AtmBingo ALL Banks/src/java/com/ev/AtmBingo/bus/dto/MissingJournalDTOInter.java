/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface MissingJournalDTOInter  extends Serializable {

    String getApplicationid();

    int getTotal();

    String getUnit();

    void setApplicationid(String applicationid);

    void setTotal(int total);

    void setUnit(String unit);
    
}
