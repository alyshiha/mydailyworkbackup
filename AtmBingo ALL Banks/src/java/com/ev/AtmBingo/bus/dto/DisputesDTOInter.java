/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;
import oracle.sql.CLOB;

/**
 *
 * @author Administrator
 */
public interface DisputesDTOInter extends Serializable {

    Boolean getIsjournal();

    void setIsjournal(Boolean isjournal);

    void setDisputekeyuk(String disputekeyuk);

    void setcollected(Integer collected);

    Integer getcollected();

    void setatmname(String atmname);

    String getatmname();

    String getDisputekeyuk();

    void setcorrectivestatus(Integer Correctivestatus);

    Integer getBlacklist();

    void setBlacklist(Integer blacklist);

    Integer getcorrectivestatus();

    Date getCorrectivedate();

    void setCorrectivedate(Date correctivedate);

    void setExportdate(Date exportdate);

    Date getExportdate();

    void setCorrectiveuser(String correctiveuser);

    String getCorrectiveuser();

    void setExportuser(String exportuser);

    String getExportuser();

    String getrownum();

    void setrownum(String rownum);

    String getreasontodisp();

    void setreasontodisp(String reasonToDisp);

    int getamount();

    void setCorrectiveamount(String correctiveamount);

    String getCorrectiveamount();

    String getatmapplicationid();

    AtmMachineDTOInter getatmid();

    String getcardno();

    

    String getcolumn1();

    String getcolumn2();

    String getcolumn3();

    String getcolumn4();

    String getcolumn5();

    String getcomments();

    String getcurrency();

    CurrencyMasterDTOInter getcurrencyid();

    String getcustomeraccountnumber();

    int getdisputeby();

    Date getdisputedate();

    int getdisputekey();

    String getdisputereason();

    int getdisputewithroles();

    int getfileid();

    Date getloadingdate();

    int getmatchingtype();

    String getnotespresented();

    int getrecordtype();

    String getresponsecode();

    TransactionResponseCodeDTOInter getresponsecodeid();

    int getresponsecodemaster();

    Date getsettleddate();

    int getsettledflag();

    UsersDTOInter getsettleduser();

    Date getsettlementdate();

    Date gettransactiondate();

    String gettransactionseqeunce();

    int gettransactionsequenceorderby();

    String gettransactionstatus();

    TransactionStatusDTOInter gettransactionstatusid();

    Date gettransactiontime();

    String gettransactiontype();

    TransactionTypeDTOInter gettransactiontypeid();

    int gettransactiontypemaster();

    void setamount(int amount);

    void setatmapplicationid(String atmApplicationId);

    void setatmid(AtmMachineDTOInter atmId);

    void setcardno(String cardNo);

    void setcardnosuffix(String cardnosuffix);
    String getcardnosuffix();

    void setcolumn1(String column1);

    void setcolumn2(String column2);

    void setcolumn3(String column3);

    void setcolumn4(String column4);

    void setcolumn5(String column5);

    void setcomments(String comments);

    void setcurrency(String currency);

    void setcurrencyid(CurrencyMasterDTOInter currencyId);

    void setcustomeraccountnumber(String customerAccountNumber);

    void setdisputeby(int disputeBy);

    void setdisputedate(Date disputeDate);

    void setdisputekey(int disputeKey);

    void setdisputereason(String disputeReason);

    void setdisputewithroles(int disputeWithRoles);

    void setfileid(int fileId);

    void setloadingdate(Date loadingDate);

    void setmatchingtype(int matchingType);

    void setnotespresented(String notesPresented);

    void setrecordtype(int recordType);

    void setresponsecode(String responseCode);

    void setresponsecodeid(TransactionResponseCodeDTOInter responseCodeId);

    void setresponsecodemaster(int responseCodeMaster);

    void setsettleddate(Date settledDate);

    void setsettledflag(int settledFlag);

    void setsettleduser(UsersDTOInter settledUser);

    void setsettlementdate(Date settlementDate);

    void settransactiondate(Date transactionDate);

    void settransactiondata(CLOB transactiondata);

    CLOB gettransactiondata();

    void settransactionseqeunce(String transactionSeqeunce);

    void settransactionsequenceorderby(int transactionSequenceOrderBy);

    void settransactionstatus(String transactionStatus);

    void settransactionstatusid(TransactionStatusDTOInter transactionStatusId);

    void settransactiontime(Date transactionTime);

    void settransactiontype(String transactionType);

    void settransactiontypeid(TransactionTypeDTOInter transactionTypeId);

    void settransactiontypemaster(int transactionTypeMaster);
}
