/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

/**
 *
 * @author shi7a
 */
public interface DepositDTOInter {

    String getCassetteName();

    void setCassetteName(String cassetteName);

    int getCassetteCount();

    void setCassetteCount(int cassetteCount);

    int getCassetteValue();

    void setCassetteValue(int cassetteValue);

    int getRetractCount();

    void setRetractCount(int retractCount);

    int getRetractValue();

    void setRetractValue(int retractValue);

    int getRecycledCount();

    void setRecycledCount(int recycledCount);

    int getRecycledValue();

    void setRecycledValue(int recycledValue);

    int getDepositCount();

    void setDepositCount(int depositCount);

    int getDepositValue();

    void setDepositValue(int depositValue);

}
