/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public interface ReportTransDTOInter {

    String getAmount();

    String getAuth();

    String getCardno();

    String getTransdate();

    String getTransseq();

    String getTranssource();

    String getTransstatus();

    void setAmount(String amount);

    void setAuth(String auth);

    void setCardno(String cardno);

    void setTransdate(String transdate);

    void setTransseq(String transseq);

    void setTranssource(String transsource);

    void setTransstatus(String transstatus);
    
}
