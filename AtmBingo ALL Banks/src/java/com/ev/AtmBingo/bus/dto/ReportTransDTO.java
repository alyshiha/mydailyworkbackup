/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public class ReportTransDTO implements ReportTransDTOInter {

    private String cardno;
    private String transdate;
    private String transseq;
    private String auth;
    private String amount;
    private String transstatus;
    private String transsource;

    @Override
    public String getCardno() {
        return cardno;
    }

    @Override
    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    @Override
    public String getTransdate() {
        return transdate;
    }

    @Override
    public void setTransdate(String transdate) {
        this.transdate = transdate;
    }

    @Override
    public String getTransseq() {
        return transseq;
    }

    @Override
    public void setTransseq(String transseq) {
        this.transseq = transseq;
    }

    @Override
    public String getAuth() {
        return auth;
    }

    @Override
    public void setAuth(String auth) {
        this.auth = auth;
    }

    @Override
    public String getAmount() {
        return amount;
    }

    @Override
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String getTransstatus() {
        return transstatus;
    }

    @Override
    public void setTransstatus(String transstatus) {
        this.transstatus = transstatus;
    }

    @Override
    public String getTranssource() {
        return transsource;
    }

    @Override
    public void setTranssource(String transsource) {
        this.transsource = transsource;
    }

}
