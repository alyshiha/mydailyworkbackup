/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface ReplanishmentDetailDTOInter extends Serializable {

    String getCassetename();

    void setCassetename(String cassetename);

    int getCassete();

    int getCasseteValue();

    int getCurrency();

    int getId();

    int getCassettecount();

    void setCassettecount(int cassettecount);

    int getDispensedcount();

    void setDispensedcount(int dispensedcount);

    int getDispensedvalue();

    void setDispensedvalue(int dispensedvalue);

    int getRemaining();

    int getRemainingAmount();

    int getTotalAmount();

    void setCassete(int cassete);

    void setCasseteValue(int casseteValue);

    void setCurrency(int currency);

    void setId(int id);

    void setRemaining(int remaining);

    void setRemainingAmount(int remainingAmount);

    void setTotalAmount(int totalAmount);

}
