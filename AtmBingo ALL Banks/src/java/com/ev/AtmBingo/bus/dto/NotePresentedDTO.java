/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class NotePresentedDTO extends BaseDTO implements NotePresentedDTOInter, Serializable {

    private String atmapplicationid;
    private Integer atmid;
    private String currency;
    private String responsecode;
    private Date transactiondate;
    private String transactionseqeunce;
    private String cardno;
    private BigDecimal amount;
    private Integer matchtypeid;
    private String matchtype;
    private Integer recordtypeid;
    private String recordtype;
    private String notepres;
    private BigDecimal notepresamount;

    @Override
    public String getAtmapplicationid() {
        return atmapplicationid;
    }

    @Override
    public void setAtmapplicationid(String atmapplicationid) {
        this.atmapplicationid = atmapplicationid;
    }

    @Override
    public Integer getAtmid() {
        return atmid;
    }

    @Override
    public void setAtmid(Integer atmid) {
        this.atmid = atmid;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String getResponsecode() {
        return responsecode;
    }

    @Override
    public void setResponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

    @Override
    public Date getTransactiondate() {
        return transactiondate;
    }

    @Override
    public void setTransactiondate(Date transactiondate) {
        this.transactiondate = transactiondate;
    }

    @Override
    public String getTransactionseqeunce() {
        return transactionseqeunce;
    }

    @Override
    public void setTransactionseqeunce(String transactionseqeunce) {
        this.transactionseqeunce = transactionseqeunce;
    }

    @Override
    public String getCardno() {
        return cardno;
    }

    @Override
    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public Integer getMatchtypeid() {
        return matchtypeid;
    }

    @Override
    public void setMatchtypeid(Integer matchtypeid) {
        this.matchtypeid = matchtypeid;
    }

    @Override
    public String getMatchtype() {
        return matchtype;
    }

    @Override
    public void setMatchtype(String matchtype) {
        this.matchtype = matchtype;
    }

    @Override
    public Integer getRecordtypeid() {
        return recordtypeid;
    }

    @Override
    public void setRecordtypeid(Integer recordtypeid) {
        this.recordtypeid = recordtypeid;
    }

    @Override
    public String getRecordtype() {
        return recordtype;
    }

    @Override
    public void setRecordtype(String recordtype) {
        this.recordtype = recordtype;
    }

    @Override
    public String getNotepres() {
        return notepres;
    }

    @Override
    public void setNotepres(String notepres) {
        this.notepres = notepres;
    }

    @Override
    public BigDecimal getNotepresamount() {
        return notepresamount;
    }

    @Override
    public void setNotepresamount(BigDecimal notepresamount) {
        this.notepresamount = notepresamount;
    }


}
