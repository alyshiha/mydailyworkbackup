/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface RejectedTransactionsDTOInter  extends Serializable {

    int getBlacklist();

    void setBlacklist(int blacklist);

    String getRejkey();

    void setRejkey(String rejkey);

    String getcardno();

    void setatmname(String atmname);

    String getatmname();

    String getcolumn1();

    Date getCorrectivedate();

    void setCorrectivedate(Date correctivedate);

    void setExportdate(Date exportdate);

    Date getExportdate();

    void setCorrectiveuser(int correctiveuser);

    int getCorrectiveuser();

    void setExportuser(int exportuser);

    int getExportuser();

    String getcolumn2();

    String getcolumn3();

    String getcolumn4();

    String getcolumn5();

    String getcurrency();

    CurrencyMasterDTOInter getcurrencyid();

    String getcustomeraccountnumber();

    int getfileid();

    Date getloadingdate();

    String getnotespresented();

    int getrecordtype();

    String getrejectedreason();

    String getresponsecode();

    TransactionResponseCodeDTOInter getresponsecodeid();

    String getrownum();

    Date getsettlementdate();

    Date gettransactiondate();

    String gettransactionsequence();

    int gettransactionsequenceorderby();

    String gettransactionstatus();

    TransactionStatusDTOInter gettransactionstatusid();

    Date gettransactiontime();

    String gettransactiontype();

    TransactionTypeDTOInter gettransactiontypeid();

    int getamount();

    String getatmapplicationid();

    AtmMachineDTOInter getatmid();

    void setcardno(String cardno);

    void setcolumn1(String column1);

    void setcolumn2(String column2);

    void setcolumn3(String column3);

    void setcolumn4(String column4);

    void setcolumn5(String column5);

    void setcurrency(String currency);

    void setcurrencyid(CurrencyMasterDTOInter currencyid);

    void setcustomeraccountnumber(String customeraccountnumber);

    void setfileid(int fileid);

    void setloadingdate(Date loadingdate);

    void setnotespresented(String notespresented);

    void setrecordtype(int recordtype);

    void setrejectedreason(String rejectedreason);

    void setresponsecode(String responsecode);

    void setresponsecodeid(TransactionResponseCodeDTOInter responsecodeid);

    void setrownum(String rownum);

    void setsettlementdate(Date settlementdate);

    void settransactiondate(Date transactiondate);

    void settransactionsequence(String transactionseqeunce);

    void settransactionsequenceorderby(int transactionsequenceorderby);

    void settransactionstatus(String transactionstatus);

    void settransactionstatusid(TransactionStatusDTOInter transactionstatusid);

    void settransactiontime(Date transactiontime);

    void settransactiontype(String transactiontype);

    void settransactiontypeid(TransactionTypeDTOInter transactiontypeid);

    void setamount(int amount);

    void setatmapplicationid(String atmapplicationid);

    void setatmid(AtmMachineDTOInter atmid);

}
