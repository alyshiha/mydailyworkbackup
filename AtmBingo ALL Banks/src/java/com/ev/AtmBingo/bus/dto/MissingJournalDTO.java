/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class MissingJournalDTO implements MissingJournalDTOInter , Serializable{
 String applicationid;
 int total;
 String unit;

    @Override
    public String getApplicationid() {
        return applicationid;
    }

    @Override
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public String getUnit() {
        return unit;
    }

    @Override
    public void setUnit(String unit) {
        this.unit = unit;
    }
 
}
