/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface FileColumnDefinitionDTOInter extends Serializable {

    int getDISPLAYINLOADING();

    String getCname();

    void setCname(String cname);

    void setDISPLAYINLOADING(int DISPLAYINLOADING);

    int getDUPLICATENO();

    void setDUPLICATENO(int DUPLICATENO);

    String getProperty();

    String getColumnName();

    int getDataType();

    int getId();

    String getName();

    void setColumnName(String columnName);

    void setDataType(int dataType);

    void setId(int id);

    void setName(String name);

    void setProperty(String property);
}
