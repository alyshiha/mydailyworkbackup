/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public interface AtmAccountsDTOInter {
    void setGroupvalue(String groupvalue);
    String getGroupvalue();
    void setGroupid(Integer groupid);
    Integer getGroupid();

    String getBranch();

    void setBranch(String branch);

    String getAccountname();

    void setAccountname(String accountname);

    String getAccountid();

    Integer getAccountnameid();

    String getAtmapplicationid();

    Integer getAtmid();

    String getDescription();

    Integer getId();

    void setAccountid(String accountid);

    void setAccountnameid(Integer accountnameid);

    void setAtmapplicationid(String atmapplicationid);

    void setAtmid(Integer atmid);

    void setDescription(String description);

    void setId(Integer id);

}
