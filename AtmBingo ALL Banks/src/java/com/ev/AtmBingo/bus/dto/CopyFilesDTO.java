/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class CopyFilesDTO extends BaseDTO implements CopyFilesDTOInter, Serializable{

    private String sourceFolder;
    private String folder1;
    private String folder2;

    public String getFolder1() {
        return folder1;
    }

    public void setFolder1(String folder1) {
        this.folder1 = folder1;
    }

    public String getFolder2() {
        return folder2;
    }

    public void setFolder2(String folder2) {
        this.folder2 = folder2;
    }

    public String getSourceFolder() {
        return sourceFolder;
    }

    public void setSourceFolder(String sourceFolder) {
        this.sourceFolder = sourceFolder;
    }

    
    protected CopyFilesDTO() {
        super();
    }

}
