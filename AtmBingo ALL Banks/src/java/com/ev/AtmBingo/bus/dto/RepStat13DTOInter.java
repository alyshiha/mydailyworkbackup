/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface RepStat13DTOInter {
 void setRepid(String repid);
 String getRepid();
    void setAtmname(String atmname);
String getAtmname();
    void setCashshortage(Integer cashshortage);

    Integer getCashshortage();

    void setCashover(Integer cashover);

    Integer getCashover();

    Integer getCashadded();

    Timestamp getDatefrom();

    Timestamp getDateto();

    Integer getDispensedcash();

    Integer getRemainingcash();

    Integer getSwitchover();

    Integer getSwitchpending();

    Integer getSwitchtotal();

    void setCashadded(Integer cashadded);

    void setDatefrom(Timestamp datefrom);

    void setDateto(Timestamp dateto);

    void setDispensedcash(Integer dispensedcash);

    void setRemainingcash(Integer remainingcash);

    void setSwitchover(Integer switchover);

    void setSwitchpending(Integer switchpending);

    void setSwitchtotal(Integer switchtotal);

    void setAtmid(String atmid);

    String getAtmid();

    Integer getDispute();

    void setDispute(Integer dispute);
}
