/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Aly
 */
public interface RepPopDTOInter  extends Serializable {

    Long getCurrentcassetdiff();

    Long getCurrentcassetvalue();

    String getCurrentcomment();

    Date getCurrentdatefrom();

    Date getCurrentdateto();

    Long getCurrentdispute();

    Long getCurrentswitchdiff();

    Long getCurrentswitchvalue();

    Long getLastcassetdiff();

    Long getLastcassetvalue();

    String getLastcomment();

    Date getLastdatefrom();

    Date getLastdateto();

    Long getLastdispute();

    Long getLastswitchdiff();

    Long getLastswitchvalue();

    void setCurrentcassetdiff(Long currentcassetdiff);

    void setCurrentcassetvalue(Long currentcassetvalue);

    void setCurrentcomment(String currentcomment);

    void setCurrentdatefrom(Date currentdatefrom);

    void setCurrentdateto(Date currentdateto);

    void setCurrentdispute(Long currentdispute);

    void setCurrentswitchdiff(Long currentswitchdiff);

    void setCurrentswitchvalue(Long currentswitchvalue);

    void setLastcassetdiff(Long lastcassetdiff);

    void setLastcassetvalue(Long lastcassetvalue);

    void setLastcomment(String lastcomment);

    void setLastdatefrom(Date lastdatefrom);

    void setLastdateto(Date lastdateto);

    void setLastdispute(Long lastdispute);

    void setLastswitchdiff(Long lastswitchdiff);

    void setLastswitchvalue(Long lastswitchvalue);
    
}
