/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author AlyShiha
 */
public class RepStat56DTO implements RepStat56DTOInter {

    private Timestamp fromdate;
    private Timestamp todate;
    private String atmid;
    private String debitaccountnumber;
    private Integer debitcurrency;
    private BigDecimal debitamount;
    private Timestamp debitvaluedate;
    private Timestamp creditvaluedateto;
    private String debittheirref;
    private String creditaccnumber;
    private String profitcenterdep;
    private String branch;
    private String indication;

    public Timestamp getFromdate() {
        return fromdate;
    }

    public void setFromdate(Timestamp fromdate) {
        this.fromdate = fromdate;
    }

    public Timestamp getTodate() {
        return todate;
    }

    public void setTodate(Timestamp todate) {
        this.todate = todate;
    }

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    @Override
    public String getDebitaccountnumber() {
        return debitaccountnumber;
    }

    @Override
    public void setDebitaccountnumber(String debitaccountnumber) {
        this.debitaccountnumber = debitaccountnumber;
    }

    @Override
    public Integer getDebitcurrency() {
        return debitcurrency;
    }

    @Override
    public void setDebitcurrency(Integer debitcurrency) {
        this.debitcurrency = debitcurrency;
    }

    @Override
    public BigDecimal getDebitamount() {
        return debitamount;
    }

    @Override
    public void setDebitamount(BigDecimal debitamount) {
        this.debitamount = debitamount;
    }

    @Override
    public Timestamp getDebitvaluedate() {
        return debitvaluedate;
    }

    @Override
    public void setDebitvaluedate(Timestamp debitvaluedate) {
        this.debitvaluedate = debitvaluedate;
    }

    @Override
    public Timestamp getCreditvaluedateto() {
        return creditvaluedateto;
    }

    @Override
    public void setCreditvaluedateto(Timestamp creditvaluedateto) {
        this.creditvaluedateto = creditvaluedateto;
    }

    @Override
    public String getDebittheirref() {
        return debittheirref;
    }

    @Override
    public void setDebittheirref(String debittheirref) {
        this.debittheirref = debittheirref;
    }

    @Override
    public String getCreditaccnumber() {
        return creditaccnumber;
    }

    @Override
    public void setCreditaccnumber(String creditaccnumber) {
        this.creditaccnumber = creditaccnumber;
    }

    @Override
    public String getProfitcenterdep() {
        return profitcenterdep;
    }

    @Override
    public void setProfitcenterdep(String profitcenterdep) {
        this.profitcenterdep = profitcenterdep;
    }

    @Override
    public String getBranch() {
        return branch;
    }

    @Override
    public void setBranch(String branch) {
        this.branch = branch;
    }

    @Override
    public String getIndication() {
        return indication;
    }

    @Override
    public void setIndication(String indication) {
        this.indication = indication;
    }
    
}
