/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class TransactionTypeDTO extends BaseDTO implements Serializable, TransactionTypeDTOInter {

    private int id;
    private String name;
    private String description;
    private int typeCategory;
    private int typeMaster;
private  boolean reverseflag;

    public boolean getReverseflag() {
        return reverseflag;
    }

    public void setReverseflag(boolean reverseflag) {
        this.reverseflag = reverseflag;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTypeCategory() {
        return typeCategory;
    }

    public void setTypeCategory(int typeCategory) {
        this.typeCategory = typeCategory;
    }

    public int getTypeMaster() {
        return typeMaster;
    }

    public void setTypeMaster(int typeMaster) {
        this.typeMaster = typeMaster;
    }

    protected TransactionTypeDTO() {
        super();
    }
}
