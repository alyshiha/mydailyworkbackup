/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface ProfileDTOInter extends Serializable {

    public int getRestrected() ;

    public void setRestrected(int restrected) ;

    List<MenuLabelDTOInter> getMlDTOL()throws Throwable;

    void setMlDTOL(List<MenuLabelDTOInter> mlDTOL);

    int getProfileId();

    String getProfileName();

    void setProfileId(int ProfileId);

    void setProfileName(String ProfileName);

}
