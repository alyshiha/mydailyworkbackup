/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class bycardDTO implements bycardDTOInter, Serializable {
    private String transactionscount, cardno,currency;
    private Integer totalamount;

    @Override
    public String getCardno() {
        return cardno;
    }

    @Override
    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public Integer getTotalamount() {
        return totalamount;
    }

    @Override
    public void setTotalamount(Integer totalamount) {
        this.totalamount = totalamount;
    }

    @Override
    public String getTransactionscount() {
        return transactionscount;
    }

    @Override
    public void setTransactionscount(String transactionscount) {
        this.transactionscount = transactionscount;
    }
    
}
