/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author ISLAM
 */
public class FileColumnDisplayDetailsDTO extends BaseDTO implements Serializable, FileColumnDisplayDetailsDTOInter{

    private FileColumnDisplayDTOInter disId;
    private FileColumnDefinitionDTOInter columnId;

    public FileColumnDefinitionDTOInter getColumnId() {
        return columnId;
    }

    public void setColumnId(FileColumnDefinitionDTOInter columnId) {
        this.columnId = columnId;
    }

    public FileColumnDisplayDTOInter getDisId() {
        return disId;
    }

    public void setDisId(FileColumnDisplayDTOInter disId) {
        this.disId = disId;
    }

    protected FileColumnDisplayDetailsDTO() {
        super();
    }

}
