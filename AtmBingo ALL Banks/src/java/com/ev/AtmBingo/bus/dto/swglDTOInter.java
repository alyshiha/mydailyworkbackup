/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface swglDTOInter  extends Serializable{

    Integer getDifferent();

    Integer getGlvalue();

    Integer getSwitchvalue();

    void setDifferent(Integer different);

    void setGlvalue(Integer glvalue);

    void setSwitchvalue(Integer switchvalue);
    
}
