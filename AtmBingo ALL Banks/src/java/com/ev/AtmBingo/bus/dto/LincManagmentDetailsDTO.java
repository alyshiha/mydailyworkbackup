/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;
import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
/**
 *
 * @author Administrator
 */

public class LincManagmentDetailsDTO extends BaseDTO implements LincManagmentDetailsDTOInter , Serializable{
    String inlic,outlic,lic,remaininlic,allmachines;

    public String getAllmachines() {
        return allmachines;
    }

    public void setAllmachines(String allmachines) {
        this.allmachines = allmachines;
    }

    public String getInlic() {
        return inlic;
    }

    public void setInlic(String inlic) {
        this.inlic = inlic;
    }

    public String getLic() {
        return lic;
    }

    public void setLic(String lic) {
        this.lic = lic;
    }

    public String getOutlic() {
        return outlic;
    }

    public void setOutlic(String outlic) {
        this.outlic = outlic;
    }

    public String getRemaininlic() {
        return remaininlic;
    }

    public void setRemaininlic(String remaininlic) {
        this.remaininlic = remaininlic;
    }

}
