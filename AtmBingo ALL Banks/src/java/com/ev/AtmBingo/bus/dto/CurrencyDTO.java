/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class CurrencyDTO extends BaseDTO implements Serializable, CurrencyDTOInter{

    private int id;
    private String abbreviation;
    private int masterId;
private  Boolean defcurr;

    public Boolean getDefcurr() {
        return defcurr;
    }

    public void setDefcurr(Boolean defcurr) {
        this.defcurr = defcurr;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMasterId() {
        return masterId;
    }

    public void setMasterId(int masterId) {
        this.masterId = masterId;
    }
    
    protected CurrencyDTO() {
        super();
    }


}
