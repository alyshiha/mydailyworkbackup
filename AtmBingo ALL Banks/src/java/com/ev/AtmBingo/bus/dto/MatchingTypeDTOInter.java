/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface MatchingTypeDTOInter extends Serializable {

    int getActive();

    int getId();

    String getName();

    int getType1();

    int getType2();

    void setActive(int active);

    void setId(int id);

    void setName(String name);

    void setType1(int type1);

    void setType2(int type2);

}
