/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface CurrencyMasterDTOInter extends Serializable {

    Boolean getDefcurr();

    void setDefcurr(Boolean defcurr);

    int getId();

    String getName();

    String getSymbol();

    void setId(int id);

    void setName(String name);

    void setSymbol(String symbol);
}
