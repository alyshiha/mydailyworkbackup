/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class ReplanishmentMasterDTO extends BaseDTO implements ReplanishmentMasterDTOInter , Serializable{

    private int id;
    private Date dateFrom;
    private Date dateTo;
    private int atmId;
    private int createdby;
    private int cardstaken;
    private Date createddate;
    private String comment;
    private String uname;
    private String calcflag;
    private boolean rowstatus, cardtakenstate;

    public boolean isCardtakenstate() {
        return cardtakenstate;
    }

    public void setCardtakenstate(boolean cardtakenstate) {
        this.cardtakenstate = cardtakenstate;
    }

    public int getCardstaken() {
        return cardstaken;
    }

    public void setCardstaken(int cardstaken) {
        this.cardstaken = cardstaken;
    }

    public boolean getRowstatus() {
        return rowstatus;
    }

    public void setRowstatus(boolean rowstatus) {
        this.rowstatus = rowstatus;
    }

    public String getcalcflag() {
        return calcflag;
    }

    public void setcalcflag(String calcflag) {
        this.calcflag = calcflag;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getAtmId() {
        return atmId;
    }

    public void setAtmId(int atmId) {
        this.atmId = atmId;
    }

    public int getCreatedBy() {
        return createdby;
    }

    public void setCreatedBy(int createdby) {
        this.createdby = createdby;
    }

    public Date getCreatedDate() {
        return createddate;
    }

    public void setCreatedDate(Date createddate) {
        this.createddate = createddate;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    protected ReplanishmentMasterDTO() {
        super();
    }
}
