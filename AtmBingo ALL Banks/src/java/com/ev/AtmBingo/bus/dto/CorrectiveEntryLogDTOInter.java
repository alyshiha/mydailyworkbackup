/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface CorrectiveEntryLogDTOInter  extends Serializable {
Integer getCorrectiveState() ;
void setCorrectiveState(Integer CorrectiveState);
    Integer getAmount();

    String getAtmApplicationId();

    String getCardNo();

    Date getCorrective_date();

    String getCurrency();

    Integer getMatchingType();

    Integer getRecordType();

    String getResponseCode();

    Integer getCorrectiveUser();

    Date getTransactionDate();

    String getTransactionSequence();

    void setAmount(Integer amount);

    void setAtmApplicationId(String atmApplicationId);

    void setCardNo(String cardNo);

    void setCorrective_date(Date corrective_date);

    void setCurrency(String currency);

    void setMatchingType(Integer matchingType);

    void setRecordType(Integer recordType);

    void setResponseCode(String responseCode);

    void setCorrectiveUser(Integer settledUser);

    void setTransactionDate(Date transactionDate);

    void setTransactionSequence(String transactionSequence);

}
