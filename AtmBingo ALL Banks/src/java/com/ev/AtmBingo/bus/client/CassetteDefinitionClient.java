/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.CasseteBOInter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.faces.event.ActionEvent;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class CassetteDefinitionClient extends BaseBean  implements Serializable{

    private CasseteBOInter cdObject;
    private CassetteDTOInter cdDeleteAttr, cdUpdateAttr, newCD;
    private List<CassetteDTOInter> cdList;
    private List<CurrencyMasterDTOInter> cmList;
    private String message;
    private Boolean showAdd, showConfirm;
    private Integer listSize;

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public List<CurrencyMasterDTOInter> getCmList() {
        return cmList;
    }

    public void setCmList(List<CurrencyMasterDTOInter> cmList) {
        this.cmList = cmList;
    }

    public CassetteDTOInter getCdDeleteAttr() {
        return cdDeleteAttr;
    }

    public void setCdDeleteAttr(CassetteDTOInter cdDeleteAttr) {
        this.cdDeleteAttr = cdDeleteAttr;
    }

    public List<CassetteDTOInter> getCdList() {
        return cdList;
    }

    public void setCdList(List<CassetteDTOInter> cdList) {
        this.cdList = cdList;
    }

    public CassetteDTOInter getCdUpdateAttr() {
        return cdUpdateAttr;
    }

    public void setCdUpdateAttr(CassetteDTOInter cdUpdateAttr) {
        this.cdUpdateAttr = cdUpdateAttr;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CassetteDTOInter getNewCD() {
        return newCD;
    }

    public void setNewCD(CassetteDTOInter newCD) {
        this.newCD = newCD;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    /** Creates a new instance of CassetteDefinitionClient */
    public CassetteDefinitionClient() throws Throwable {
        super();
        super.GetAccessPage();
        cdObject = BOFactory.createCasseteBO(null);
        cdList = (List<CassetteDTOInter>) cdObject.getCassetes();
        cmList = (List<CurrencyMasterDTOInter>) cdObject.getCurrency();
        showAdd = true;
        showConfirm = false;
        listSize = cdList.size();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void fillDelete(ActionEvent e) {
        cdDeleteAttr = (CassetteDTOInter) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void deleteRecord() {
        try {
            cdObject.editeCassette(cdDeleteAttr, cdObject.DELETE);
            cdList.remove(cdDeleteAttr);
            message = super.showMessage(SUCC);
            showAdd = true;
            showConfirm = false;
        } catch (Throwable ex) {
            message = "Cassette Already Exists In Replenishment";
        }
    }

    public void fillUpdate(ActionEvent e) {
        cdUpdateAttr = (CassetteDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    public void updateRecord() {
        try {
            if (showAdd == false) {
                message = super.showMessage(CONF_FIRST);
            } else if ((Integer) cdUpdateAttr.getAmount() == null || cdUpdateAttr.getName().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                cdObject.editeCassette(cdUpdateAttr, cdObject.UPDATE);
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = "An Error Occurred During Updating";
        }
    }

    public void addRecord() {
        if (cdList == null) {
            cdList = new ArrayList<CassetteDTOInter>();
        }
        newCD = DTOFactory.createCassetteDTO();
        cdList.add(newCD);
        showAdd = false;
        showConfirm = true;
        resetVars();
    }

    public void confirmRecord() {
        try {
            if (newCD.getAmount() == 0 || newCD.getName().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                cdObject.editeCassette(newCD, cdObject.INSERT);
                showAdd = true;
                showConfirm = false;
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void resetVars() {
        message = "";
    }  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }
}
