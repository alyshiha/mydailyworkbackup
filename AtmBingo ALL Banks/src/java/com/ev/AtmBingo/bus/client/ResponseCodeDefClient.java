/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.bo.ResponseCodesDefinitionBOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseMasterDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author ISLAM
 */
public class ResponseCodeDefClient extends BaseBean  implements Serializable{

    private ResponseCodesDefinitionBOInter cdObject;
    private List<TransactionResponseMasterDTOInter> cdList;
    private List<TransactionResponseCodeDTOInter> cdDetailsList;
    private TransactionResponseMasterDTOInter timeShiftAttr;
    private TransactionResponseMasterDTOInter timeShiftAttr1;
    private TransactionResponseMasterDTOInter selectedMT;
    private TransactionResponseMasterDTOInter newRecord;
    private TransactionResponseCodeDTOInter newRecord1;
    private TransactionResponseCodeDTOInter timeShiftDetailsAttr, timeShiftDetailsAttr1;
    private Integer myInt;
    private Boolean showAdd, showAdd1;
    private Boolean showConfirm, showConfirm1;
    private String message;
    private String message1;

    public List<TransactionResponseCodeDTOInter> getCdDetailsList() {
        return cdDetailsList;
    }

    public void setCdDetailsList(List<TransactionResponseCodeDTOInter> cdDetailsList) {
        this.cdDetailsList = cdDetailsList;
    }

    public List<TransactionResponseMasterDTOInter> getCdList() {
        return cdList;
    }

    public void setCdList(List<TransactionResponseMasterDTOInter> cdList) {
        this.cdList = cdList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public Integer getMyInt() {
        return myInt;
    }

    public void setMyInt(Integer myInt) {
        this.myInt = myInt;
    }

    public TransactionResponseMasterDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(TransactionResponseMasterDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public TransactionResponseCodeDTOInter getNewRecord1() {
        return newRecord1;
    }

    public void setNewRecord1(TransactionResponseCodeDTOInter newRecord1) {
        this.newRecord1 = newRecord1;
    }

    public TransactionResponseMasterDTOInter getSelectedMT() {
        return selectedMT;
    }

    public void setSelectedMT(TransactionResponseMasterDTOInter selectedMT) {
        this.selectedMT = selectedMT;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public TransactionResponseMasterDTOInter getTimeShiftAttr() {
        return timeShiftAttr;
    }

    public void setTimeShiftAttr(TransactionResponseMasterDTOInter timeShiftAttr) {
        this.timeShiftAttr = timeShiftAttr;
    }

    public TransactionResponseMasterDTOInter getTimeShiftAttr1() {
        return timeShiftAttr1;
    }

    public void setTimeShiftAttr1(TransactionResponseMasterDTOInter timeShiftAttr1) {
        this.timeShiftAttr1 = timeShiftAttr1;
    }

    public TransactionResponseCodeDTOInter getTimeShiftDetailsAttr() {
        return timeShiftDetailsAttr;
    }

    public void setTimeShiftDetailsAttr(TransactionResponseCodeDTOInter timeShiftDetailsAttr) {
        this.timeShiftDetailsAttr = timeShiftDetailsAttr;
    }

    public TransactionResponseCodeDTOInter getTimeShiftDetailsAttr1() {
        return timeShiftDetailsAttr1;
    }

    public void setTimeShiftDetailsAttr1(TransactionResponseCodeDTOInter timeShiftDetailsAttr1) {
        this.timeShiftDetailsAttr1 = timeShiftDetailsAttr1;
    }
private Session utillist = new Session();
    /** Creates a new instance of ResponseCodeDefClient */
    public ResponseCodeDefClient() throws Throwable {
        super();
        super.GetAccessPage();
        cdObject = BOFactory.createResponseCodeDefBO(null);
        cdList = (List<TransactionResponseMasterDTOInter>) utillist.GetTransactionResponseMasterList();
        showAdd = true;
        showConfirm = false;
        showAdd1 = false;
        showConfirm1 = false;
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void onRowSelect1(SelectEvent e) throws Throwable {
        selectedMT = DTOFactory.createTransactionResponseMasterDTO();
        selectedMT = (TransactionResponseMasterDTOInter) e.getObject();
        cdDetailsList = (List<TransactionResponseCodeDTOInter>) cdObject.getResponseCode(selectedMT);
        message = "";
        message1 = "";
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void onRowUnSelect1(UnselectEvent e) throws Throwable {
        cdDetailsList = null;
        message = "";
        message1 = "";
        showAdd1 = false;
        showConfirm1 = false;
    }

    public void fillUpdate(ActionEvent e) {
        timeShiftAttr = (TransactionResponseMasterDTOInter) e.getComponent().getAttributes().get("updateRow");
        message = "";
        message1 = "";
    }

    public void updateRecord() {
        try {
            if (!showAdd) {
                message = super.showMessage(CONF_FIRST);
            } else {
                if (timeShiftAttr.getName().equals("")) {
                    message = super.showMessage(REQ_FIELD);
                } else {
                    cdObject.editeCurrencyMaster(timeShiftAttr, cdObject.UPDATE);
                    message = super.showMessage(SUCC);
                }
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void fillDelete(ActionEvent e) {
        timeShiftAttr1 = (TransactionResponseMasterDTOInter) e.getComponent().getAttributes().get("removedRow");
        message = "";
        message1 = "";
    }

    public void deleteRecord() {
        try {
            if (cdDetailsList == null || cdDetailsList.size() == 0) {
                cdObject.editeCurrencyMaster(timeShiftAttr1, cdObject.DELETE);
                cdList.remove(timeShiftAttr1);
                message = super.showMessage(SUCC);
                showAdd = true;
                showConfirm = false;
            } else {



                message = "Delete the details first";
            }
        } catch (Throwable ex) {
            if (ex.getMessage().contains("ORA-02292")) {
                message = super.showMessage(CNT_DEL);
            }
        }
    }

    public void addTimeShift() {
        newRecord = DTOFactory.createTransactionResponseMasterDTO();
        cdList.add(newRecord);
        showAdd = false;
        showConfirm = true;
        message = "";
        message1 = "";
    }

    public void confirmAdd() {
        try {
            if (newRecord.getName().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                cdObject.editeCurrencyMaster(newRecord, cdObject.INSERT);
                message = super.showMessage(SUCC);
                showAdd = true;
                showConfirm = false;
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void fillUpdate1(ActionEvent e) {
        timeShiftDetailsAttr = (TransactionResponseCodeDTOInter) e.getComponent().getAttributes().get("updateRow1");
        message1 = "";
        message = "";
    }

    public void updateRecord1() {
        try {
            if (!showAdd1) {
                message1 = super.showMessage(CONF_FIRST);
            } else if (timeShiftDetailsAttr.getName().equals("")) {
                message1 = super.showMessage(REQ_FIELD);
            } else {
                Integer count = 0;
                count = cdObject.getDefFlagCount(timeShiftDetailsAttr.getMasterCode(), timeShiftDetailsAttr.getId());
                if (timeShiftDetailsAttr.getDefaultFlagB() == true) {
                    count++;
                }
                if (count < 2) {
                    cdObject.editeResponseCode(timeShiftDetailsAttr, cdObject.UPDATE);
                    message1 = super.showMessage(SUCC);
                } else {
                    message1 = "Can't insert more than one default flag";
                }
            }

        } catch (Throwable ex) {
            message1 = super.showMessage(REC_EXIST);
        }
    }

    public void fillDelete1(ActionEvent e) {
        timeShiftDetailsAttr1 = (TransactionResponseCodeDTOInter) e.getComponent().getAttributes().get("removedRow1");
        message1 = "";
        message = "";
    }

    public void deleteRecord1() {
        try {
            cdObject.editeResponseCode(timeShiftDetailsAttr1, cdObject.DELETE);
            cdDetailsList.remove(timeShiftDetailsAttr1);
            message1 = super.showMessage(SUCC);
            showAdd1 = true;
            showConfirm1 = false;
        } catch (Throwable ex) {
            if (ex.getMessage().contains("ORA-02292")) {
                message1 = super.showMessage(CNT_DEL);
            }
        }
    }

    public void addTimeShift1() {
        newRecord1 = DTOFactory.createTransactionResponseCodeDTO();
        cdDetailsList.add(newRecord1);
        showAdd1 = false;
        showConfirm1 = true;
        message1 = "";
        message = "";
    }

    public void confirmAdd1() {


        try {
            if (newRecord1.getName().equals("")) {
                message1 = super.showMessage(REQ_FIELD);
            } else {
                Integer count = 0;
                newRecord1.setMasterCode(selectedMT.getId());
                count = cdObject.getDefFlagCount(selectedMT.getId(), newRecord1.getId());
                if (newRecord1.getDefaultFlagB() == true) {
                    count++;
                }
                if (count < 2) {
                    cdObject.editeResponseCode(newRecord1, cdObject.INSERT);
                    message1 = super.showMessage(SUCC);
                    showAdd1 = true;
                    showConfirm1 = false;
                } else {
                    message1 = "Can't insert more than one default flag";
                }

            }

        } catch (Throwable ex) {
            message1 = super.showMessage(REC_EXIST);
        }
    }
    public void resetVars() {
        message = "";
        message1 = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
