/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.BlockUsersBOInter;
import com.ev.AtmBingo.bus.dto.BlockUsersDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class UnlockUserClient extends BaseBean  implements Serializable{

    private BlockUsersBOInter buObject;
    private List<UsersDTOInter> buList;
    private Integer listSize;
    private UsersDTOInter unlockUser;

    public List<UsersDTOInter> getBuList() {
        return buList;
    }

    public void setBuList(List<UsersDTOInter> buList) {
        this.buList = buList;
    }

    public BlockUsersBOInter getBuObject() {
        return buObject;
    }

    public void setBuObject(BlockUsersBOInter buObject) {
        this.buObject = buObject;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public UsersDTOInter getUnlockUser() {
        return unlockUser;
    }

    public void setUnlockUser(UsersDTOInter unlockUser) {
        this.unlockUser = unlockUser;
    }

    public UnlockUserClient() throws Throwable {

        super();
        super.GetAccessPage();
        buObject = BOFactory.createBlockUsersBO(null);
        buList = (List<UsersDTOInter>) buObject.getlockedUsers();
        listSize = buList.size();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void fillUnBlockedUser(ActionEvent e) throws Throwable {
        unlockUser = (UsersDTOInter) e.getComponent().getAttributes().get("unlockedUser");
    }

    public void unBlockUser(ActionEvent e) throws Throwable {
        buObject.unlockuser(unlockUser);
        buList.remove(unlockUser);
        listSize = buList.size();
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
