/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.bo.ValidationRulesBOInter;
import com.ev.AtmBingo.bus.dto.FileColumnDefinitionDTOInter;
import java.util.List;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ValidationRulesDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class ValidationRulesClient extends BaseBean  implements Serializable{

    private Integer Type;

    public Integer getType() {
        return Type;
    }

    public void setType(Integer Type) {
        this.Type = Type;
    }
    private ValidationRulesBOInter drObject;
    private ValidationRulesDTOInter drUpAttr, drDelAttr, newRecord;
    private List<ValidationRulesDTOInter> drList;
    private List<FileColumnDefinitionDTOInter> fcdList;
    private Integer fcdInt, ruleSelect, addType, coln;
    private Boolean updateFlag, activeBool, dateSelected, pickBool, betweenDate, disFlag, valFlag, formulaFlag, addFormula, betweenFlag, nullOperation;
    private String formulaStatus, message, formulaName, checkUp, columnNam, operationStr, firstText, andValue, secondValue, allFormula;
    private Date firstTextDate, secondValueDate;
    private List<String> operationValues;
    private List<Object> columnValues;
    private Integer[] valuesList;

    public Integer getColn() {
        return coln;
    }

    public void setColn(Integer coln) {
        this.coln = coln;
    }

    public String getFormulaStatus() {
        return formulaStatus;
    }

    public void setFormulaStatus(String formulaStatus) {
        this.formulaStatus = formulaStatus;
    }

    public Boolean getUpdateFlag() {
        return updateFlag;
    }

    public void setUpdateFlag(Boolean updateFlag) {
        this.updateFlag = updateFlag;
    }

    public Date getFirstTextDate() {
        return firstTextDate;
    }

    public void setFirstTextDate(Date firstTextDate) {
        this.firstTextDate = firstTextDate;
    }

    public Date getSecondValueDate() {
        return secondValueDate;
    }

    public void setSecondValueDate(Date secondValueDate) {
        this.secondValueDate = secondValueDate;
    }

    public Boolean getPickBool() {
        return pickBool;
    }

    public void setPickBool(Boolean pickBool) {
        this.pickBool = pickBool;
    }

    public Integer[] getValuesList() {
        return valuesList;
    }

    public void setValuesList(Integer[] valuesList) {
        this.valuesList = valuesList;
    }

    public Boolean getBetweenDate() {
        return betweenDate;
    }

    public void setBetweenDate(Boolean betweenDate) {
        this.betweenDate = betweenDate;
    }

    public Boolean getDateSelected() {
        return dateSelected;
    }

    public void setDateSelected(Boolean dateSelected) {
        this.dateSelected = dateSelected;
    }

    public List<Object> getColumnValues() {
        return columnValues;
    }

    public void setColumnValues(List<Object> columnValues) {
        this.columnValues = columnValues;
    }

    public Boolean getActiveBool() {
        return activeBool;
    }

    public void setActiveBool(Boolean activeBool) {
        this.activeBool = activeBool;
    }

    public Integer getAddType() {
        return addType;
    }

    public void setAddType(Integer addType) {
        this.addType = addType;
    }

    public Boolean getAddFormula() {
        return addFormula;
    }

    public void setAddFormula(Boolean addFormula) {
        this.addFormula = addFormula;
    }

    public String getAllFormula() {
        return allFormula;
    }

    public void setAllFormula(String allFormula) {
        this.allFormula = allFormula;
    }

    public String getAndValue() {
        return andValue;
    }

    public void setAndValue(String andValue) {
        this.andValue = andValue;
    }

    public Boolean getBetweenFlag() {
        return betweenFlag;
    }

    public void setBetweenFlag(Boolean betweenFlag) {
        this.betweenFlag = betweenFlag;
    }

    public String getCheckUp() {
        return checkUp;
    }

    public void setCheckUp(String checkUp) {
        this.checkUp = checkUp;
    }

    public String getColumnNam() {
        return columnNam;
    }

    public void setColumnNam(String columnNam) {
        this.columnNam = columnNam;
    }

    public Boolean getDisFlag() {
        return disFlag;
    }

    public void setDisFlag(Boolean disFlag) {
        this.disFlag = disFlag;
    }

    public ValidationRulesDTOInter getDrDelAttr() {
        return drDelAttr;
    }

    public void setDrDelAttr(ValidationRulesDTOInter drDelAttr) {
        this.drDelAttr = drDelAttr;
    }

    public List<ValidationRulesDTOInter> getDrList() {
        return drList;
    }

    public void setDrList(List<ValidationRulesDTOInter> drList) {
        this.drList = drList;
    }

    public ValidationRulesDTOInter getDrUpAttr() {
        return drUpAttr;
    }

    public void setDrUpAttr(ValidationRulesDTOInter drUpAttr) {
        this.drUpAttr = drUpAttr;
    }

    public Integer getFcdInt() {
        return fcdInt;
    }

    public void setFcdInt(Integer fcdInt) {
        this.fcdInt = fcdInt;
    }

    public List<FileColumnDefinitionDTOInter> getFcdList() {
        return fcdList;
    }

    public void setFcdList(List<FileColumnDefinitionDTOInter> fcdList) {
        this.fcdList = fcdList;
    }

    public String getFirstText() {
        return firstText;
    }

    public void setFirstText(String firstText) {
        this.firstText = firstText;
    }

    public Boolean getFormulaFlag() {
        return formulaFlag;
    }

    public void setFormulaFlag(Boolean formulaFlag) {
        this.formulaFlag = formulaFlag;
    }

    public String getFormulaName() {
        return formulaName;
    }

    public void setFormulaName(String formulaName) {
        this.formulaName = formulaName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ValidationRulesDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(ValidationRulesDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public Boolean getNullOperation() {
        return nullOperation;
    }

    public void setNullOperation(Boolean nullOperation) {
        this.nullOperation = nullOperation;
    }

    public String getOperationStr() {
        return operationStr;
    }

    public void setOperationStr(String operationStr) {
        this.operationStr = operationStr;
    }

    public List<String> getOperationValues() {
        return operationValues;
    }

    public void setOperationValues(List<String> operationValues) {
        this.operationValues = operationValues;
    }

    public Integer getRuleSelect() {
        return ruleSelect;
    }

    public void setRuleSelect(Integer ruleSelect) {
        this.ruleSelect = ruleSelect;
    }

    public String getSecondValue() {
        return secondValue;
    }

    public void setSecondValue(String secondValue) {
        this.secondValue = secondValue;
    }

    public Boolean getValFlag() {
        return valFlag;
    }

    public void setValFlag(Boolean valFlag) {
        this.valFlag = valFlag;
    }

    /** Creates a new instance of ValidationRulesClient */
    public ValidationRulesClient() throws Throwable {
        super();
        super.GetAccessPage();
        drObject = BOFactory.createValidationRulesBO(null);
        fcdInt = 3;
        fcdList = (List<FileColumnDefinitionDTOInter>) drObject.getColumns();
        drList = (List<ValidationRulesDTOInter>) drObject.getValidationRules();
        formulaFlag = true;
        addFormula = false;
        betweenFlag = false;
        nullOperation = false;
        betweenDate = false;
        dateSelected = false;
        activeBool = true;
        pickBool = false;
        columnNam = "Transaction Date";
        andValue = "AND";
        addType = 1;
        disFlag = true;
        valFlag = false;
     FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        operationValues = new ArrayList<String>();
        columnValues = new ArrayList<Object>();
    }

    public void addPopup() throws Throwable {
        if (columnNam.contains("Date") || columnNam.contains("Time")) {
            if (columnValues == null) {
                columnValues = new ArrayList<Object>();
            }
            Type = 1;
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add("<>");
            operationValues.add(">=");
            operationValues.add("<=");
            operationValues.add("between");

            dateSelected = true;
            betweenDate = false;
            nullOperation = false;
            betweenFlag = false;
            pickBool = false;
        } else {
            Type = 2;
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add("<>");
            operationValues.add("in");
            operationValues.add("not in");
            operationValues.add("null");
            operationValues.add("is not null");
            operationValues.add("Like");
            operationValues.add("Not Like");
            columnValues = (List<Object>) drObject.getColumnValues(fcdInt);
            if (columnValues == null) {
                columnValues = new ArrayList<Object>();
                dateSelected = false;
                betweenDate = false;
                nullOperation = true;
                betweenFlag = false;
                pickBool = false;
            } else {
                dateSelected = false;
                betweenDate = false;
                nullOperation = false;
                betweenFlag = false;
                pickBool = true;
            }

        }
        updateFlag = false;
        formulaStatus = "Add";
    }

    public void valueEffect(ValueChangeEvent e) throws Throwable {
        fcdInt = (Integer) e.getNewValue();
        drList = (List<ValidationRulesDTOInter>) drObject.getValidationRules();
        columnNam = drObject.getColumnName(fcdInt);
        message = "";
        if (columnNam.contains("Date") || columnNam.contains("Time")) {
            if (columnValues == null) {
                columnValues = new ArrayList<Object>();
            }
            Type = 1;
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add("<>");
            operationValues.add(">=");
            operationValues.add("<=");
            operationValues.add("between");

            dateSelected = true;
            betweenDate = false;
            nullOperation = false;
            betweenFlag = false;
            pickBool = false;
        } else {
            Type = 2;
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add("<>");
            operationValues.add("in");
            operationValues.add("not in");
            operationValues.add("null");
            operationValues.add("is not null");
            operationValues.add("Like");
            operationValues.add("Not Like");
            columnValues = (List<Object>) drObject.getColumnValues(fcdInt);
            if (columnValues == null) {
                columnValues = new ArrayList<Object>();
                dateSelected = false;
                betweenDate = false;
                nullOperation = true;
                betweenFlag = false;
                pickBool = false;
            } else {
                dateSelected = false;
                betweenDate = false;
                nullOperation = false;
                betweenFlag = false;
                pickBool = true;
            }

        }
        updateFlag = false;
        formulaStatus = "Add";
    }

    public void fillDelete(ActionEvent e) {
        drDelAttr = (ValidationRulesDTOInter) e.getComponent().getAttributes().get("removedRow_val");
        message = "";
    }

    public void deleteRecord() {
        try {
            drDelAttr.setColumnId(drDelAttr.getColumnId());
            drObject.editValidationRules(drDelAttr, drObject.DELETE);
            drList.remove(drDelAttr);
            message = super.showMessage(SUCC);
            addFormula = false;
            formulaFlag = true;
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void fillUpdate(ActionEvent e) throws Throwable {
        drUpAttr = (ValidationRulesDTOInter) e.getComponent().getAttributes().get("updateRow_val");
        columnNam = drUpAttr.getName();
        activeBool = drUpAttr.getActiveB();
        addType = drUpAttr.getValidationType();

        updateFlag = true;
        message = "";
    }

    public void updateRecord() {
        try {
            // checkUp = (String) drObject.validateFormula(drUpAttr,1);
            // if (drUpAttr.getName().equals("")) {
            //    message = super.showMessage(REQ_FIELD);
            // } else {
            //  if (checkUp.equals("done")) {
            drUpAttr.setColumnId(drUpAttr.getColumnId());
            drObject.editValidationRules(drUpAttr, drObject.UPDATE);
            message = super.showMessage(SUCC);
            // } else {
            //   message = checkUp;
            //}
            // }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void operationChange(ValueChangeEvent e) {
        operationStr = (String) e.getNewValue();
        if (columnNam.contains("Date")) {
            if (operationStr.equals("between")) {
                dateSelected = true;
                betweenDate = true;
                nullOperation = false;
                betweenFlag = false;
                pickBool = false;
            } else {
                dateSelected = true;
                betweenDate = false;
                nullOperation = false;
                betweenFlag = false;
                pickBool = false;
            }
        } else if (columnValues.size() == 0 || columnValues == null) {
            dateSelected = false;
            betweenDate = false;
            nullOperation = true;
            betweenFlag = false;
            pickBool = false;
        } else {
            dateSelected = false;
            betweenDate = false;
            nullOperation = false;
            betweenFlag = false;
            pickBool = true;
        }
    }

    public void SaveChangesValidation() {
        for (int i = 0; i < drList.size(); i++) {
            try {
                ValidationRulesDTOInter RowData = drList.get(i);
                drObject.editValidationRules(RowData, drObject.UPDATE);
                message = super.showMessage(SUCC);
            } catch (Throwable ex) {
                Logger.getLogger(ValidationRulesClient.class.getName()).log(Level.SEVERE, null, ex);
                message = ex.getMessage();
            }
        }

    }

    public void addRecord() {
        try {
            if (drList == null) {
                drList = new ArrayList<ValidationRulesDTOInter>();
            }
            if (updateFlag) {
                drUpAttr.setActiveB(activeBool);
                drUpAttr.setValidationType(addType);
                drUpAttr.setColumnId(coln);
                drUpAttr.setName(formulaName);

                if (columnNam.contains("Date")) {
                    Type = 1;
                    if (operationStr.contains("between")) {
                        String dateValues = drObject.genarateValueDate(firstTextDate, secondValueDate);
                        drObject.createFormula(fcdInt, operationStr, dateValues, drUpAttr, Type);
                        drObject.editValidationRules(drUpAttr, drObject.UPDATE);
                        message = super.showMessage(SUCC);
                    } else {
                        drObject.createFormula(fcdInt, operationStr, DateFormatter.changeDateAndTimeFormat(firstTextDate), drUpAttr, Type);
                        drObject.editValidationRules(drUpAttr, drObject.UPDATE);
                        message = super.showMessage(SUCC);
                    }
                }
                if (pickBool) {
                    Type = 2;
                    String pickValye = drObject.genarateValueString(valuesList);
                    drObject.createFormula(fcdInt, operationStr, pickValye, drUpAttr, Type);
                    drObject.editValidationRules(drUpAttr, drObject.UPDATE);
                    message = super.showMessage(SUCC);
                } else {
                    Type = 2;
                    drObject.createFormula(fcdInt, operationStr, firstText, drUpAttr, Type);
                    drObject.editValidationRules(drUpAttr, drObject.UPDATE);
                    message = super.showMessage(SUCC);
                }
            } else {
                newRecord = DTOFactory.createValidationRulesDTO();
                newRecord.setActiveB(activeBool);
                newRecord.setValidationType(addType);
                newRecord.setColumnId(fcdInt);
                newRecord.setName(formulaName);

                if (columnNam.contains("Date")) {
                    Type = 1;
                    if (operationStr.contains("between")) {
                        String dateValues = drObject.genarateValueDate(firstTextDate, secondValueDate);
                        if (drObject.createFormula(fcdInt, operationStr, dateValues, newRecord, Type) == "Invalid Formula") {
                            message = "Invalid Formula";
                        } else {
                            drList.add(newRecord);
                            drObject.editValidationRules(newRecord, drObject.INSERT);
                            message = super.showMessage(SUCC);
                        }
                    } else {
                        if (drObject.createFormula(fcdInt, operationStr, DateFormatter.changeDateAndTimeFormat(firstTextDate), newRecord, Type) == "Invalid Formula") {
                            message = "Invalid Formula";
                        } else {
                            drList.add(newRecord);
                            drObject.editValidationRules(newRecord, drObject.INSERT);
                            message = super.showMessage(SUCC);
                        }
                    }
                }
                if (pickBool) {
                    Type = 2;
                    String pickValye = drObject.genarateValueString(valuesList);
                    drObject.createFormula(fcdInt, operationStr, pickValye, newRecord, Type);
                    drObject.editValidationRules(newRecord, drObject.INSERT);
                    drList.add(newRecord);
                    message = super.showMessage(SUCC);
                } else {
                    Type = 2;
                    if (!columnNam.contains("Date")) {
                        drObject.createFormula(fcdInt, operationStr, firstText, newRecord, Type);
                        drList.add(newRecord);
                        drObject.editValidationRules(newRecord, drObject.INSERT);
                        message = super.showMessage(SUCC);
                    }
                }
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void resetVars() {
        message = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
