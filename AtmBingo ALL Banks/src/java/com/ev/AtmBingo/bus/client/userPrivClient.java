/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.menulabelsBOInter;
import com.ev.AtmBingo.bus.bo.privilegeStatusNameBOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.menulabelsDTOInter;
import com.ev.AtmBingo.bus.dto.privilegeStatusNameDTOInter;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author AlyShiha
 */
@ManagedBean(name = "userPrivView")
@ViewScoped
public class userPrivClient extends BaseBean implements Serializable {

    private int userscount;
    private int pagescount;
    private int privcount;

    private privilegeStatusNameDTOInter[] selectedprivilege;
    private menulabelsDTOInter selectedpage;
    private UsersDTOInter selecteduser;

    private List<UsersDTOInter> usersList;
    private List<menulabelsDTOInter> pagesList;
    private List<privilegeStatusNameDTOInter> privilegeList;
    private List<String> privList;

    private Session utillist = new Session();
    private menulabelsBOInter enginmenuLabel;
    private privilegeStatusNameBOInter enginprivilegeStatus;

    public int getUserscount() {
        return userscount;
    }

    public void setUserscount(int userscount) {
        this.userscount = userscount;
    }

    public int getPagescount() {
        return pagescount;
    }

    public void setPagescount(int pagescount) {
        this.pagescount = pagescount;
    }

    public int getPrivcount() {
        return privcount;
    }

    public void setPrivcount(int privcount) {
        this.privcount = privcount;
    }

    public privilegeStatusNameDTOInter[] getSelectedprivilege() {
        return selectedprivilege;
    }

    public void setSelectedprivilege(privilegeStatusNameDTOInter[] selectedprivilege) {
        this.selectedprivilege = selectedprivilege;
    }

    public menulabelsDTOInter getSelectedpage() {
        return selectedpage;
    }

    public void setSelectedpage(menulabelsDTOInter selectedpage) {
        this.selectedpage = selectedpage;
    }

    public UsersDTOInter getSelecteduser() {
        return selecteduser;
    }

    public void setSelecteduser(UsersDTOInter selecteduser) {
        this.selecteduser = selecteduser;
    }

    public List<UsersDTOInter> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<UsersDTOInter> usersList) {
        this.usersList = usersList;
    }

    public List<menulabelsDTOInter> getPagesList() {
        return pagesList;
    }

    public void setPagesList(List<menulabelsDTOInter> pagesList) {
        this.pagesList = pagesList;
    }

    public List<privilegeStatusNameDTOInter> getPrivilegeList() {
        return privilegeList;
    }

    public void setPrivilegeList(List<privilegeStatusNameDTOInter> privilegeList) {
        this.privilegeList = privilegeList;
    }

    public List<String> getPrivList() {
        return privList;
    }

    public void setPrivList(List<String> privList) {
        this.privList = privList;
    }

    public Session getUtillist() {
        return utillist;
    }

    public void setUtillist(Session utillist) {
        this.utillist = utillist;
    }

    public menulabelsBOInter getEnginmenuLabel() {
        return enginmenuLabel;
    }

    public void setEnginmenuLabel(menulabelsBOInter enginmenuLabel) {
        this.enginmenuLabel = enginmenuLabel;
    }

    public privilegeStatusNameBOInter getEnginprivilegeStatus() {
        return enginprivilegeStatus;
    }

    public void setEnginprivilegeStatus(privilegeStatusNameBOInter enginprivilegeStatus) {
        this.enginprivilegeStatus = enginprivilegeStatus;
    }

    @PostConstruct
    public void init() {
        try {
            enginmenuLabel = BOFactory.createmenulabelsBO();
            enginprivilegeStatus = BOFactory.createprivilegeStatusNameBO(null);

            usersList = utillist.GetUsersList();
            pagesList = (List<menulabelsDTOInter>) enginmenuLabel.GetAllRecords2();
            privilegeList = enginprivilegeStatus.findAll();
            privList = super.findPrivelage(utillist.GetUserLogging().getUserId(), "");

            userscount = usersList.size();
            pagescount = pagesList.size();
            privcount = privilegeList.size();
        } catch (Throwable ex) {
            Logger.getLogger(userPrivClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void handleMasterChange() throws Throwable {
        if (selecteduser != null && selectedpage != null) {
            System.out.println(selecteduser.getUserName() + " " + selectedpage.getenglishlabel());
            DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:singleDT3");
            repTable.setSelection(enginprivilegeStatus.findprivassigned("" + selecteduser.getUserId(), "" + selectedpage.getmenuid()).toArray());
        } else {
            selectedprivilege = null;
            System.out.println("No");
        }
    }

    public void saveChange() throws Throwable {
        String result = enginprivilegeStatus.deleteprivassigned("" + selecteduser.getUserId(), "" + selectedpage.getmenuid());
        if ("Record Has Been Succesfully Deleted".equals(result)) {
            for (privilegeStatusNameDTOInter selectedprivilegeitem : selectedprivilege) {
                enginprivilegeStatus.insertpriv("" + selecteduser.getUserId(), "" + selectedpage.getmenuid(), "" + selectedprivilegeitem.getPrev_id());
            }
            result = "Record Has Been Succesfully Saved";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", result));
        System.out.println(selecteduser.getUserName() + " " + selectedpage.getenglishlabel() + " " + selectedprivilege.length);
    }
}
