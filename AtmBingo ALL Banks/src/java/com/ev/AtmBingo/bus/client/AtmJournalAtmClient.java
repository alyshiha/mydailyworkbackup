/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.bo.AtmJournalBOInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.RepStatReportBOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmJournalDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.replanishmentCashManagementDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author AlyShiha
 */
@ManagedBean(name = "AtmJournalAtmView")
@ViewScoped
public class AtmJournalAtmClient extends BaseBean implements Serializable {

    private Session utillist = new Session();
    private Date datefrom;
    private Date dateto;
    private String atmid;
    private int atmgroup;
    private List<AtmGroupDTOInter> atmGroupList;
    private UsersDTOInter user;
    private RepStatReportBOInter repStatEngin;
    private List<AtmMachineDTOInter> atmrecordsList;
    private AtmJournalBOInter engin;
    private Boolean export, realesed;
    private int listcount;
    private String type;
    private List<AtmJournalDTOInter> operationselectedRecord;
    private List<CurrencyMasterDTOInter> currencyList;
    private AtmJournalDTOInter[] selectedRecord;
    private List<AtmJournalDTOInter> recordsList;
    private List<String> privelageList;
    private String excelcoloumns[] = {"ID", "TRANSACTION.TYPE", "DEBIT.ACCT.NO", "DEBIT.CURRENCY", "DEBIT.AMOUNT", "DEBIT.VALUE.DATE", "CREDIT.VALUE.DATE", "DEBIT.THEIR.REF", "CREDIT.ACC.NO", "ORDERING.BANK", "PROFIT.CENTER.DEPT", "COMMISION.CODE", "COMPANY", "INDICATION"};

    public Boolean getRealesed() {
        return realesed;
    }

    public void setRealesed(Boolean realesed) {
        this.realesed = realesed;
    }

    public Session getUtillist() {
        return utillist;
    }

    public void setUtillist(Session utillist) {
        this.utillist = utillist;
    }

    public Date getDatefrom() {
        return datefrom;
    }

    public void setDatefrom(Date datefrom) {
        this.datefrom = datefrom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getPrivelageList() {
        return privelageList;
    }

    public void setPrivelageList(List<String> privelageList) {
        this.privelageList = privelageList;
    }

    public Date getDateto() {
        return dateto;
    }

    public RepStatReportBOInter getRepStatEngin() {
        return repStatEngin;
    }

    public void setRepStatEngin(RepStatReportBOInter repStatEngin) {
        this.repStatEngin = repStatEngin;
    }

    public List<AtmJournalDTOInter> getOperationselectedRecord() {
        return operationselectedRecord;
    }

    public void setOperationselectedRecord(List<AtmJournalDTOInter> operationselectedRecord) {
        this.operationselectedRecord = operationselectedRecord;
    }

    public void setDateto(Date dateto) {
        this.dateto = dateto;
    }

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    public UsersDTOInter getUser() {
        return user;
    }

    public void setUser(UsersDTOInter user) {
        this.user = user;
    }

    public List<AtmMachineDTOInter> getAtmrecordsList() {
        return atmrecordsList;
    }

    public void setAtmrecordsList(List<AtmMachineDTOInter> atmrecordsList) {
        this.atmrecordsList = atmrecordsList;
    }

    public AtmJournalBOInter getEngin() {
        return engin;
    }

    public void setEngin(AtmJournalBOInter engin) {
        this.engin = engin;
    }

    public Boolean getExport() {
        return export;
    }

    public void setExport(Boolean export) {
        this.export = export;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public List<CurrencyMasterDTOInter> getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(List<CurrencyMasterDTOInter> currencyList) {
        this.currencyList = currencyList;
    }

    public AtmJournalDTOInter[] getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(AtmJournalDTOInter[] selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public List<AtmJournalDTOInter> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<AtmJournalDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public String[] getExcelcoloumns() {
        return excelcoloumns;
    }

    public void setExcelcoloumns(String[] excelcoloumns) {
        this.excelcoloumns = excelcoloumns;
    }

    public int getAtmgroup() {
        return atmgroup;
    }

    public void setAtmgroup(int atmgroup) {
        this.atmgroup = atmgroup;
    }

    public List<AtmGroupDTOInter> getAtmGroupList() {
        return atmGroupList;
    }

    public void setAtmGroupList(List<AtmGroupDTOInter> atmGroupList) {
        this.atmGroupList = atmGroupList;
    }

    @PostConstruct
    public void init() {

        try {
            atmid = "All";
            if (!isSunday()) {
                datefrom = super.getYesterdaytrans((String) "00:00:00");
                dateto = super.getTodaytrans((String) "00:00:00");
            } else {
                datefrom = super.getBeforYesterdaytrans((String) "00:00:00");
                dateto = super.getTodaytrans((String) "00:00:00");
            }
            user = utillist.GetUserLogging();
            atmgroup = 0;
            atmGroupList = (List<AtmGroupDTOInter>) utillist.GetATMGroupAssignList(user.getUserId());
            atmrecordsList = (List<AtmMachineDTOInter>) utillist.GetAtmMachineList(user);
            currencyList = (List<CurrencyMasterDTOInter>) utillist.GetCurrencyMasterList();
            engin = BOFactory.createAtmJournalBO(null);
            if (user.getExport() == 2) {
                export = Boolean.FALSE;
            } else {
                export = Boolean.TRUE;
            }
            type = "0";
            repStatEngin = BOFactory.createRepStatReportBO(null);
            recordsList = new ArrayList<AtmJournalDTOInter>();
            realesed = false;
            privelageList = super.findPrivelage(utillist.GetUserLogging().getUserId(), "AtmJournalAtm.xhtml");
            if (super.findPrivelageOperation(privelageList, "VALIDATE")) {
                recordsList = engin.findjournalatm(DateFormatter.changeDateAndTimeFormat(datefrom), DateFormatter.changeDateAndTimeFormat(dateto), atmid, 1, atmgroup, type, utillist.GetUserLogging().getUserId(), true, realesed);
            }
            searchrecords();
            selectAllRecord();
            listcount = recordsList.size();

        } catch (Throwable ex) {
            Logger.getLogger(AtmJournalAtmClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Boolean markPriv() {
        if (super.findPrivelageOperation(privelageList, "CHECK")) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    private void selectAllRecord() {
        DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:singleDT2");
        repTable.setSelection(recordsList.toArray());
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public void searchrecords() {
        try {
            selectedRecord = null;
            if (user.getExport() == 2) {
                recordsList = engin.findjournalatm(DateFormatter.changeDateAndTimeFormat(datefrom), DateFormatter.changeDateAndTimeFormat(dateto), atmid, 1, atmgroup, type, utillist.GetUserLogging().getUserId(), false, realesed);
            } else {
                recordsList = engin.findjournalatm(DateFormatter.changeDateAndTimeFormat(datefrom), DateFormatter.changeDateAndTimeFormat(dateto), atmid, 2, atmgroup, type, utillist.GetUserLogging().getUserId(), false, realesed);
            }
            selectAllRecord();
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(NetworksClient.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getATMName(Integer ID) {
        for (AtmMachineDTOInter atmrecord : atmrecordsList) {
            if (atmrecord.getId() == ID) {
                return atmrecord.getApplicationId();
            }
        }
        return "UnAssigned";
    }

    public String getCurrencyName(Integer ID) {
        for (CurrencyMasterDTOInter currencyrecord : currencyList) {
            if (currencyrecord.getId() == ID) {
                return currencyrecord.getSymbol();
            }
        }
        return "UnAssigned";
    }

    public String exportexcel(String[] coloumns, List<AtmJournalDTOInter> Records) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("ATM Journal");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        Object[] coloumnslist = new Object[coloumns.length];
        int index = 0;
        for (String coloumn : coloumns) {
            coloumnslist[index] = coloumn;
            sheet.setDefaultColumnWidth(20);
            index = index + 1;
        }
        data.put(1, coloumnslist);
        index = 2;
        for (AtmJournalDTOInter Record : Records) {
            Object[] coloumnsData = new Object[coloumns.length];
            for (int i = 0; i < coloumns.length; i++) {
                if ("ID".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getAtmid());
                }
                if ("TRANSACTION.TYPE".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf("AC");
                }
                if ("DEBIT.ACCT.NO".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getDebitaccount());
                }
                if ("DEBIT.CURRENCY".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(getCurrencyName(Record.getCurrency()));
                }
                if ("DEBIT.AMOUNT".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getAmount());
                }
                if ("DEBIT.VALUE.DATE".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getJournaldate());
                }
                if ("CREDIT.VALUE.DATE".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getJournaldate());
                }
                if ("DEBIT.THEIR.REF".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getJournalrefernce());
                }

                if ("CREDIT.ACC.NO".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getCreditaccount());
                }
                if ("ORDERING.BANK".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf("UNBE");
                }
                if ("PROFIT.CENTER.DEPT".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf("11100000");
                }
                if ("COMMISION.CODE".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf("WAIVE");
                }
                if ("COMPANY".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getBranch());
                }
                if ("INDICATION".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getIndication());
                }

            }
            data.put(index, coloumnsData);
            index = index + 1;
        }
        Set<Integer> keyset = data.keySet();
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            Calendar c = Calendar.getInstance();
            String uri = "Journal123Visa" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".xls";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            pdfPath = pdfPath + uri;
            FileOutputStream out = new FileOutputStream(new File(pdfPath));
            workbook.write(out);
            out.close();
            return pdfPath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Boolean confirmPriv() {
        return super.findPrivelageOperation(privelageList, "VALIDATE");
    }

    public void confirmTransaction() throws Throwable {
        Boolean validrecord = Boolean.FALSE;
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord.length != 0) {
            for (AtmJournalDTOInter record : selectedRecord) {
                if (record.getExportflag() == 1) {
                    record.setExportflag(2);
                    operationselectedRecord.add(record);
                    validrecord = Boolean.TRUE;
                }
            }
            engin.updateJournal(operationselectedRecord, "2");
            selectedRecord = null;

            listcount = recordsList.size();
            if (validrecord) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Has Been Confirmed Succesfully"));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Can`t Be Validated Due To It`s Status"));
            }

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Confirm"));
        }

    }

    public Boolean datesPriv() {
        if (super.findPrivelageOperation(privelageList, "SEARCH_DATE")) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    public Boolean exportPriv() {
        return super.findPrivelageOperation(privelageList, "EXCUTE");
    }
    public Boolean releasePriv() {
        return super.findPrivelageOperation(privelageList, "RELEASE");
    }
    public Boolean reexportPriv() {
        return super.findPrivelageOperation(privelageList, "REEXCUTE");
    }

    public void exportCSV() throws Throwable {
        boolean isexport = false;
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord.length != 0) {

            for (AtmJournalDTOInter record : selectedRecord) {

                if (record.getExportflag() == 2) {
                    isexport = true;
                    record.setExportflag(3);
                    operationselectedRecord.add(record);
                }
            }
            if (isexport) {
                engin.updateJournal(operationselectedRecord, "3");
                String path = writeCSV(operationselectedRecord);
                prepDownload(path);
                selectedRecord = null;

                listcount = recordsList.size();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Report Has Been Exported Succesfully"));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "please  validate first"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Export"));

        }

    }

    public void reexportCSV() throws Throwable {
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        boolean isexport = false;
        if (selectedRecord.length != 0) {

            for (AtmJournalDTOInter record : selectedRecord) {

                if (record.getExportflag() == 3 || record.getExportflag() == 4) {
                    record.setExportflag(4);
                    operationselectedRecord.add(record);
                    isexport = true;
                }
            }
            if (isexport) {
                engin.updateJournal(operationselectedRecord, "4");
                String path = writeCSV(operationselectedRecord);
                prepDownload(path);
                selectedRecord = null;

                listcount = recordsList.size();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Report Has Been Exported Succesfully"));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "please  validate first"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Export"));

        }

    }

    public void exportexcel() throws Throwable {
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        List<AtmJournalDTOInter> exportRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord.length != 0) {

            for (AtmJournalDTOInter record : selectedRecord) {

                operationselectedRecord.add(record);

                exportRecord.add(record);
            }
            String path = exportexcel(excelcoloumns, exportRecord);
            prepDownload(path);
            selectedRecord = null;
            listcount = recordsList.size();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Report Has Been Exported Succesfully"));

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Confirm Record To Export"));
        }

    }

    //write from duplicate array of courses to a CSV file
    public String thousandSeparator(int amount) {
        return String.format("%,d", amount);
    }

    public String writeCSV(List<AtmJournalDTOInter> Records) throws Exception {
        Calendar c = Calendar.getInstance();
        String uri = "\\AtmJournalatm" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".Csv";
        String pdfPath = utillist.getParameter("JOURNAL_EXPORT_PATH").toString();
        pdfPath = pdfPath + uri;
        //create a File class object and give the file the name employees.csv
        java.io.File courseCSV = new java.io.File(pdfPath);

        //Create a Printwriter text output stream and link it to the CSV File
        java.io.PrintWriter outfile = new java.io.PrintWriter(courseCSV);

        //Iterate the elements actually being used
        for (AtmJournalDTOInter Record : Records) {
            outfile.write(Record.toCSVString(","));
        }//end for

        outfile.close();
        return pdfPath;
    } //end writeCSV()

    public String getNowDate() {
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return sdf.format(date);
    }

    public Boolean isSunday() {
        // Get calendar set to current date and time
        Calendar c = Calendar.getInstance();
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public String GetStatusField(int status) {
        String statusString;
        switch (status) {
            case 1:
                statusString = "Pending";
                break;
            case 2:
                statusString = "Validated";
                break;
            case 3:
                statusString = "Executed";
                break;
            case 4:
                statusString = "REExecuted";
                break;
            case 8:
                statusString = "Released";
                break;
            default:
                statusString = "UnKnown";
                break;
        }
        return statusString;
    }

    public void printreportpdf() {
        try {
            if (datefrom == null || dateto == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Date From & Date To Is A Required Fields"));
            } else {
                String path = (String) repStatEngin.printrep8(super.getLoggedInUser().getUserName(), datefrom, dateto, atmgroup, utillist.Getclient(), atmid.toString(), atmid,realesed);
                prepDownload(path);
            }
        } catch (Throwable ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", ex.getMessage()));
        }
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void recalc() {
        try {
            engin.recalc();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Has Been Calculate Succesfully"));
        } catch (Throwable ex) {
            System.out.println(ex.getMessage());
        }
    }
  public void recalc123visa() {
        try {
            engin.recalc123visa();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Has Been Calculate Succesfully"));
        } catch (Throwable ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void realeseTransaction() throws Throwable {
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord.length != 0) {
            for (AtmJournalDTOInter record : selectedRecord) {
                record.setExportflag(8);
                operationselectedRecord.add(record);
            }
            engin.updateJournal(operationselectedRecord, "8");
            selectedRecord = null;

            listcount = recordsList.size();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Has Been released Succesfully"));

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Release"));
        }

    }
}
