/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.bo.CopyFileSetupBOInter;
import com.ev.AtmBingo.bus.dto.CopyFilesDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class CopyingFileClient extends BaseBean  implements Serializable{

    private CopyFileSetupBOInter cfObject;
    private CopyFilesDTOInter cfUpdateAttr;
    private CopyFilesDTOInter cfDeleteAttr;
    private CopyFilesDTOInter newCF;
    private List<CopyFilesDTOInter> cfList;
    private String message;
    private String oldSource;
    private Boolean showAdd;
    private Boolean showConfirm;
    private Integer listSize;

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public String getOldSource() {
        return oldSource;
    }

    public void setOldSource(String oldSource) {
        this.oldSource = oldSource;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public CopyFilesDTOInter getCfDeleteAttr() {
        return cfDeleteAttr;
    }

    public void setCfDeleteAttr(CopyFilesDTOInter cfDeleteAttr) {
        this.cfDeleteAttr = cfDeleteAttr;
    }

    public List<CopyFilesDTOInter> getCfList() {
        return cfList;
    }

    public void setCfList(List<CopyFilesDTOInter> cfList) {
        this.cfList = cfList;
    }

    public CopyFilesDTOInter getCfUpdateAttr() {
        return cfUpdateAttr;
    }

    public void setCfUpdateAttr(CopyFilesDTOInter cfUpdateAttr) {
        this.cfUpdateAttr = cfUpdateAttr;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Creates a new instance of CopyingFileClient
     */
    public CopyingFileClient() throws Throwable {
        super();
        super.GetAccessPage();
        cfObject = BOFactory.createCopyingFileBO(null);
        cfList = (List<CopyFilesDTOInter>) cfObject.getCopyFiles();
        showAdd = true;
        showConfirm = false;
        listSize = cfList.size();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void fillUpdate(ActionEvent e) {
        cfUpdateAttr = (CopyFilesDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    public void updateRecord() {
        try {
            if (showAdd == false) {
                message = super.showMessage(CONF_FIRST);
            } else if (cfUpdateAttr.getSourceFolder() == null || cfUpdateAttr.getFolder1() == null) {
                message = super.showMessage(REQ_FIELD);
            } else {
                if (oldSource == null) {
                    oldSource = cfUpdateAttr.getSourceFolder();
                }
                String[] inputs = {oldSource, cfUpdateAttr.getFolder1(), cfUpdateAttr.getFolder2(), cfUpdateAttr.getSourceFolder()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace(":", "").replace("\\", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    cfObject.editeCopyFile(cfUpdateAttr, oldSource, cfObject.UPDATE);
                    message = super.showMessage(SUCC);
                } else {
                    message = "Please Enter Valid Data";
                }

            }
        } catch (Throwable ex) {
            cfList.remove(cfUpdateAttr);
            message = ex.getMessage();
        }
    }

    public void fillDelete(ActionEvent e) {
        cfDeleteAttr = (CopyFilesDTOInter) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void deleteRecord() {
        try {
            String[] inputs = {cfDeleteAttr.getFolder1(), cfDeleteAttr.getFolder2(), cfDeleteAttr.getSourceFolder()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace(":", "").replace("\\", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
                cfObject.editeCopyFile(cfDeleteAttr, null, cfObject.DELETE);
                cfList.remove(cfDeleteAttr);
                message = super.showMessage(SUCC);
                showAdd = true;
                showConfirm = false;
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void addRecord() {
        if (cfList == null) {
            cfList = new ArrayList<CopyFilesDTOInter>();
        }
        newCF = DTOFactory.createCopyFilesDTO();
        cfList.add(newCF);
        showAdd = false;
        showConfirm = true;
        resetVars();
    }

    public void confirmRecord() {
        try {
            if (newCF.getSourceFolder().equals("") || newCF.getFolder1().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                String[] inputs = {newCF.getFolder1(), newCF.getFolder2(), newCF.getSourceFolder()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace(":", "").replace("\\", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    cfObject.editeCopyFile(newCF, null, cfObject.INSERT);
                    message = super.showMessage(SUCC);
                    showAdd = true;
                    showConfirm = false;
                } else {
                    message = "Please Enter Valid Data";
                }

            }

        } catch (Throwable ex) {
            message = ex.getMessage();
            showAdd = true;
            showConfirm = false;
            cfList.remove(newCF);
        }
    }

    public void oldValueSource(ValueChangeEvent e) {
        oldSource = (String) e.getOldValue();
        resetVars();
    }

    public void resetVars() {
        message = "";
    }  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }
}
