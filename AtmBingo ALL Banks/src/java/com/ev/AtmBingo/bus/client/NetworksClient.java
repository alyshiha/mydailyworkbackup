/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.NetworksBOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.NetworksDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Aly
 */
@ManagedBean(name = "networksView")
@ViewScoped
public class NetworksClient extends BaseBean implements Serializable {

    private NetworksDTOInter selectedRecord, newRecord;
    private List<NetworksDTOInter> recordsList;
    private int listcount;
    private String atmname;
    private Session sessionutil = new Session();
    private List<String> privelageList;
    private NetworksBOInter engin;

    public String getAtmname() {
        return atmname;
    }

    public void setAtmname(String atmname) {
        this.atmname = atmname;
    }

    public NetworksDTOInter getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(NetworksDTOInter selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public NetworksDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(NetworksDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public List<NetworksDTOInter> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<NetworksDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public NetworksBOInter getEngin() {
        return engin;
    }

    public void setEngin(NetworksBOInter engin) {
        this.engin = engin;
    }

    @PostConstruct
    public void init() {
        try {
            atmname = "";
            engin = BOFactory.createNetworksBO(null);
            recordsList = engin.getNetworks();
            listcount = recordsList.size();
            privelageList = super.findPrivelage(sessionutil.GetUserLogging().getUserId(), "Networks.xhtml");
        } catch (Throwable ex) {
            Logger.getLogger(NetworksClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public void searchrecords() {
        try {
            if (atmname == null) {
                atmname = "";
            }
            recordsList = engin.getRecord(atmname);
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(NetworksClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printreportPDF() throws Exception, Throwable {

        String path = (String) engin.printPDFrep(sessionutil.Getclient(), super.getLoggedInUser().getUserName(), atmname);
        prepDownload(path);
    }

    public Boolean insertPriv() {
        return super.findPrivelageOperation(privelageList, "INSERT");
    }

    public void addRecord() {
        newRecord = DTOFactory.createNetworksDTO();
    }

    private Boolean isfound(NetworksDTOInter Record) {
        for (NetworksDTOInter rec : recordsList) {
            if (rec.getName().equals(Record.getName())) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    private Boolean isfoundpdate(NetworksDTOInter Record) {
        int i = 0;
        for (NetworksDTOInter rec : recordsList) {
            if (rec.getName().equals(Record.getName())) {
                if (i == 1) {
                    return Boolean.TRUE;
                } else {
                    i = 1;
                }
            }
        }
        return Boolean.FALSE;
    }

    public void saveRecord() throws SQLException {
        String message = "network name already exists";
        if (!isfound(newRecord)) {
            message = engin.insertNetworks(newRecord);
            recordsList.add(newRecord);
            listcount = recordsList.size();
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean updatePriv() {
        return super.findPrivelageOperation(privelageList, "UPDATE");
    }

    public void updateRecord() throws SQLException {
        String message = "network name already exists";
        if (!isfoundpdate(selectedRecord)) {
            message = engin.updateNetworks(selectedRecord);
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean deletePriv() {
        return super.findPrivelageOperation(privelageList, "DELETE");
    }

    public void deleteRecord() throws SQLException {
        String message = engin.deleteNetworks(selectedRecord);
        if (message.contains("Succesfully")) {
            recordsList.remove(selectedRecord);
            listcount = recordsList.size();
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public List<String> completeContains(String query) {
        List<String> filtered = new ArrayList<String>();
        for (int i = 0; i < recordsList.size(); i++) {
            NetworksDTOInter rec = recordsList.get(i);
            if (rec.getName().toLowerCase().contains(query.toLowerCase())) {
                filtered.add(rec.getName());
            }
        }
        return filtered;
    }
}
