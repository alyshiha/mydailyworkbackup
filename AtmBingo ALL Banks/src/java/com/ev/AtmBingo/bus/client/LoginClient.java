/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.CoonectionHandler;
import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.base.data.SimpleProtector;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.LoginBOInter;
import com.ev.AtmBingo.bus.bo.PrivilageBOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.LicenseDAOInter;
import com.ev.AtmBingo.bus.dao.SystemTableDAOInter;
import com.ev.AtmBingo.bus.dto.LicenseDTOInter;
import com.ev.AtmBingo.bus.dto.SystemTableDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Date;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.*;

/**
 *
 * @author ISLAM
 */
public class LoginClient extends BaseBean implements Serializable {

    private String userName, bankName;
    private String password;
    private String errorMsg;
    private Integer counter;
    private PrivilageBOInter prBO;
    private final static String version = "2.1.00";
    public static boolean validUsers = true;
    public static boolean validDate = true;
    public static boolean loginFlag = true;
    public static boolean validVersion = true;
    public static boolean validLic = true;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Creates a new instance of LoginClient
     */
    public LoginClient() {

        /* 
         FileModificationTime t = new FileModificationTime();
         System.err.println("Hwa Da");
         System.err.println(t.runelclass("D:\\AtmBingo"));*/
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            if (!"Trial".equals(sAttribute)) {
                session.removeAttribute(sAttribute);
            } else {
                if (session.getAttribute("Trial") == null) {
                    session.removeAttribute(sAttribute);
                }
            }

       }
        
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "DENY");

        //if (session.getAttribute("logedinUser") != null) {
        //try {
        //response.sendRedirect("faces/main.xhtml");
        //} catch (IOException ex) {
        //      Logger.getLogger(LoginClient.class.getName()).log(Level.SEVERE, null, ex);
        //    }
        // } else {
        // }
        // bankName = super.getClientName();
        // session.invalidate();
    }

    public void showMessege() {
        if (!errorMsg.equals("Success")) {

            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, ""));
        }
    }

    public final void test() throws InterruptedException {
        String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
        File fin = new File(pdfPath);
        File[] finlist = fin.listFiles();
        for (int n = 0; n < finlist.length; n++) {
            if (finlist[n].isFile()) {
                System.gc();
                Thread.sleep(2000);
                finlist[n].delete();
            }
        }
    }

    public String validateLogin() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
            HttpSession session = (HttpSession) req.getSession(true);
            CoonectionHandler.getInstance().returnAllConnection();
            // LicenseDAOInter lDAO = DAOFactory.createLicenseDAO();
            //LicenseDTOInter lDTO = (LicenseDTOInter) lDAO.findLicense();
            LoginBOInter myLogin;
            myLogin = BOFactory.createLoginBO(null);
            LicenseDAOInter lDAO = DAOFactory.createLicenseDAO();
            session.setAttribute("UNAME", userName);
            Session sessionutil = new Session();
            sessionutil.GetLicenseSessions();
            // super.setClientName();

            //test();
            Date EndDate = (Date) session.getAttribute("EndDate");
            String Result = myLogin.ValidateLog(userName, password, (String) session.getAttribute("version"), (Integer) session.getAttribute("UserCount"), EndDate);
            //String Result ="Valid";
            UsersDTOInter logedinUser = (UsersDTOInter) myLogin.findUser(userName);
            sessionutil.SetUserLogging(logedinUser);

            if (Result.equals("Valid")) {
                // if (SessionCounter.getTotalActiveSession()> 5) {
                //   Result = "Sorry,License expired";
                // return "Fail";
                //}
                //System.out.println("Users: " + SessionCounter.getTotalActiveSession());

                if (session.getAttribute("Trial") != null) {
                    session.setAttribute("Trial", 0);
                }

                sessionutil.GenerateSession(logedinUser);
                return "Success";

            } else if (Result.equals("Password Expired")) {
                // HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

                try {

                    String url = "./ChangePassword.xhtml";
                    FacesContext fC = FacesContext.getCurrentInstance();
                    HttpSession httsession = (HttpSession) fC.getExternalContext().getSession(true);
                    httsession.setAttribute("uid", SimpleProtector.encrypt("" + logedinUser.getUserId(), "ThisIsASecretKey"));
                    fC.getExternalContext().redirect(url);
                    return "Fail";
                } catch (IOException ex) {
                    Logger.getLogger(LoginClient.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else if (Result.equals("New User")) {
                //HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

                String url = "./ChangePassword.xhtml";

                FacesContext fC = FacesContext.getCurrentInstance();
                HttpSession httsession = (HttpSession) fC.getExternalContext().getSession(true);
                httsession.setAttribute("uid", SimpleProtector.encrypt("" + logedinUser.getUserId(), "ThisIsASecretKey"));
                fC.getExternalContext().redirect(url);
                return "Fail";

            } else {
                if (Result.contains("Wrong Password")) {
                    Result = "User Unauthorized Please Check Info";
                    if (session.getAttribute("Trial") == null) {
                        session.setAttribute("Trial", 0);
                    }
                    SystemTableDAOInter stDAO = DAOFactory.createSystemTableDAO(null);
                    SystemTableDTOInter stDTO = null;
                    try {
                        stDTO = (SystemTableDTOInter) stDAO.find("LOGIN_TRIAL");

                    } catch (Throwable ex) {
                        Logger.getLogger(MainClient.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    session.setAttribute("Trial", (Integer) session.getAttribute("Trial") + 1);
                    if ((Integer) session.getAttribute("Trial") == Integer.parseInt(stDTO.getParameterValue())) {
                        if (myLogin.lockUser(userName) == "Done") {
                            Result = "User Has Been Locked";
                        }
                    }
                    errorMsg = Result;
                    showMessege();
                    return "Fail";

                }
                if (!Result.contains("User Has Been Locked")) {
                    Result = "User Unauthorized Please Check Info";
                }
                errorMsg = Result;
                showMessege();

                logout();
                return "Fail";

            }
            logout();
            return "";
        } catch (Throwable e) {
            logout();
            return "Fail";
        }

    }

    public String logout() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
            HttpSession session = (HttpSession) req.getSession(true);
//            Session sessionutil = new Session();
//            UsersDTOInter u = sessionutil.GetUserLogging();
//            u.setStatus(2);
            try {
                clearsessionattributes();
            } catch (Throwable ex) {
                Logger.getLogger(SessionCounter.class.getName()).log(Level.SEVERE, null, ex);
            }
            session.invalidate();
            //    Runtime.getRuntime().gc();
            return "Logout";
        } catch (Throwable ex) {
            Logger.getLogger(LoginClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
