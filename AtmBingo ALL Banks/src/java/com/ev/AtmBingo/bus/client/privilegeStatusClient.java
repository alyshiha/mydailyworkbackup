/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.privilegeStatusNameBOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.privilegeStatusNameDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Aly
 */
@ManagedBean(name = "privilegeStatusView")
@ViewScoped
public class privilegeStatusClient extends BaseBean implements Serializable {

    private privilegeStatusNameDTOInter selectedRecord, newRecord;
    private List<privilegeStatusNameDTOInter> recordsList;
    private int listcount;
    private Session utillist = new Session();
    private String atmname;
    private List<String> privelageList;
    private privilegeStatusNameBOInter engin;

    public privilegeStatusNameDTOInter getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(privilegeStatusNameDTOInter selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public privilegeStatusNameDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(privilegeStatusNameDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public List<privilegeStatusNameDTOInter> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<privilegeStatusNameDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public String getAtmname() {
        return atmname;
    }

    public void setAtmname(String atmname) {
        this.atmname = atmname;
    }

    public privilegeStatusNameBOInter getEngin() {
        return engin;
    }

    public void setEngin(privilegeStatusNameBOInter engin) {
        this.engin = engin;
    }

    @PostConstruct
    public void init() {
        try {
            atmname = "";
            engin = BOFactory.createprivilegeStatusNameBO(null);
            recordsList = engin.findAll();
            listcount = recordsList.size();
            privelageList = super.findPrivelage(utillist.GetUserLogging().getUserId(), "privilegeStatus.xhtml");
        } catch (Throwable ex) {
            Logger.getLogger(NetworksClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public void searchrecords() {
        try {
            if (atmname == null) {
                atmname = "";
            }
            recordsList = engin.findRecord(atmname);
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(NetworksClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean insertPriv() {
        return super.findPrivelageOperation(privelageList, "INSERT");
    }

    public void addRecord() {
        newRecord = DTOFactory.createprivilegeStatusNameDTO();
    }

    public void saveRecord() throws SQLException, Throwable {
        String message = "";

        message = engin.insertRecord(newRecord);
        recordsList.add(newRecord);
        listcount = recordsList.size();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("carDialog2.hide();");

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean updatePriv() {
        return super.findPrivelageOperation(privelageList, "UPDATE");
    }

    public void updateRecord() throws SQLException, Throwable {
        String message = "";
        message = engin.updateRecord(selectedRecord);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("carDialog.hide();");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean deletePriv() {
        return super.findPrivelageOperation(privelageList, "DELETE");
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printreportPDF() throws Exception, Throwable {

        String path = (String) engin.printPDFrep(utillist.Getclient(), super.getLoggedInUser().getUserName(), atmname);
        prepDownload(path);
    }

    public void deleteRecord() throws SQLException {
        String message = engin.deleteRecord(selectedRecord);
        if (message.contains("Has Been Succesfully Deleted")) {
            recordsList.remove(selectedRecord);
            listcount = recordsList.size();
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public List<String> completeContains(String query) {
        List<String> filtered = new ArrayList<String>();
        for (int i = 0; i < recordsList.size(); i++) {
            privilegeStatusNameDTOInter rec = recordsList.get(i);
            if (rec.getSymbol().toLowerCase().contains(query.toLowerCase())) {
                filtered.add(rec.getSymbol());
            }
        }
        return filtered;
    }

    public Boolean ValidateAccount(String AccountNumber) {
        if (AccountNumber.trim().length() == 16) {
            if (AccountNumber.trim().substring(0, 3).matches("[A-Z]*")) {
                if (AccountNumber.trim().substring(3, 16).matches("\\d+")) {
                    return Boolean.TRUE;
                }
            }
        }
        return Boolean.FALSE;
    }
}
