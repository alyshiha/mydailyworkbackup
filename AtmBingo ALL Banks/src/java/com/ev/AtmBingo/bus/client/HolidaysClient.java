/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.HolidaysBOInter;
import com.ev.AtmBingo.bus.dto.HolidaysDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.faces.event.ActionEvent;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class HolidaysClient extends BaseBean  implements Serializable{

    private List<HolidaysDTOInter> HList;
    private List<String> myMonths;
    private HolidaysBOInter HObject;
    private HolidaysDTOInter HAttr;
    private HolidaysDTOInter newH;
    private String message;
    private Boolean showAdd;
    private Boolean showConfirm;
    private Integer oldMonth;
    private String newMonth;
    private Integer oldDay, listSize;
    private Integer newDay;

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    // <editor-fold defaultstate="collapsed" desc="Getter and Setter">

    
    public List<HolidaysDTOInter> getHList() {
        return HList;
    }

    public void setHList(List<HolidaysDTOInter> HList) {
        this.HList = HList;
    }

    public String getNewMonth() {
        return newMonth;
    }

    public void setNewMonth(String newMonth) {
        this.newMonth = newMonth;
    }

    public Integer getNewDay() {
        return newDay;
    }

    public void setNewDay(Integer newDay) {
        this.newDay = newDay;
    }

    public Integer getOldDay() {
        return oldDay;
    }

    public void setOldDay(Integer oldDay) {
        this.oldDay = oldDay;
    }

    public Integer getOldMonth() {
        return oldMonth;
    }

    public void setOldMonth(Integer oldMonth) {
        this.oldMonth = oldMonth;
    }

    public List<String> getMyMonths() {
        return myMonths;
    }

    public void setMyMonths(List<String> myMonths) {
        this.myMonths = myMonths;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HolidaysDTOInter getnewH() {
        return newH;

    }

    public void setnewH(HolidaysDTOInter newH) {
        this.newH = newH;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }
    // </editor-fold>

    /** Creates a new instance of HolidaysClient */
    public HolidaysClient() throws Throwable {
        super();
        super.GetAccessPage();
        HObject = BOFactory.createHolidaysBO(null);
        HList = (List<HolidaysDTOInter>) HObject.getHolidays();
        myMonths = super.getMonths();
        showAdd = true;
        showConfirm = false;
        listSize = HList.size();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void resetVars() {
        message = "";
    }

    public void fillDel(ActionEvent e) {
        HAttr = (HolidaysDTOInter) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void deleteHolidays(ActionEvent e) throws Throwable {
        try {
            HList.remove(HAttr);
            HObject.editeHolidays(HAttr, 0, 0, HObject.DELETE);
            message = super.showMessage(SUCC);
            showAdd = true;
            showConfirm = false;
        } catch (Throwable ex) {
            message = "Error, please delete again";
        }
    }

    public void fillUp(ActionEvent e) {
        HAttr = (HolidaysDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    public void updateHolidays(ActionEvent e) throws Throwable {
        try {
            if (showAdd == false) {
                message = super.showMessage(CONF_FIRST);
            } else if (HAttr.getDay() == null || HAttr.getDay() == 0 || HAttr.getaReason() == "" || HAttr.geteReason() == "") {
                if (HAttr.getDay() == null || HAttr.getDay() == 0) {
                    HAttr.setDay(oldDay);
                }
                message = super.showMessage(REQ_FIELD);

            } else if (newDay != null) {
                if (newDay > HAttr.getDayCount()) {
                    message = super.showMessage(INVA_DAY);
                    HAttr.setDay(oldDay);
                    if (newMonth != null) {
                        HAttr.setMonth(oldMonth);
                    }

                } else {
                    if (oldMonth == null) {
                        oldMonth = HAttr.getMonth();
                    }
                    HObject.editeHolidays(HAttr, oldMonth, oldDay, HObject.UPDATE);
                    message = super.showMessage(SUCC);
                }
            } else {
                oldDay = HAttr.getDay();
                if (oldMonth == null) {
                    oldMonth = HAttr.getMonth();
                }
                HObject.editeHolidays(HAttr, oldMonth, oldDay, HObject.UPDATE);
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
            HAttr.setDay(oldDay);
            if (newMonth != null) {
                HAttr.setMonth(oldMonth);
            }
        }
    }

    public void AddHolidays() {
        resetVars();
        if (HList == null) {
            HList = new ArrayList<HolidaysDTOInter>();
        }
        newH = DTOFactory.createHolidaysDTO();
        HList.add(newH);
        showAdd = false;
        showConfirm = true;
        newH.setDay(1);
        newDay = 1;
    }

    public void ConfirmHoliday() {
        try {
            newH.getSmonth();
            if (newH.getaReason() == ""
                    || newH.geteReason() == "") {
                message = super.showMessage(REQ_FIELD);
            } else if (newH.getDay() > newH.getDayCount()) {
                message = super.showMessage(INVA_DAY);
            } else {
                HObject.editeHolidays(newH, 0, 0, HObject.INSERT);
                message = super.showMessage(SUCC);
                showAdd = true;
                showConfirm = false;
            }
        } catch (Throwable ex) {
            // message = "Your record can't be added, please try again";
            message = super.showMessage(REC_EXIST);
        }
    }

    public void getOldValuesMonth(ValueChangeEvent e) {
        try {
            newMonth = (String) e.getNewValue();
            if (e.getOldValue().equals("January")) {
                setOldMonth(1);


            } else if (e.getOldValue().equals("February")) {
                setOldMonth(2);


            } else if (e.getOldValue().equals("March")) {
                setOldMonth(3);


            } else if (e.getOldValue().equals("April")) {
                setOldMonth(4);


            } else if (e.getOldValue().equals("May")) {
                setOldMonth(5);


            } else if (e.getOldValue().equals("June")) {
                setOldMonth(6);


            } else if (e.getOldValue().equals("July")) {
                setOldMonth(7);


            } else if (e.getOldValue().equals("August")) {
                setOldMonth(8);


            } else if (e.getOldValue().equals("September")) {
                setOldMonth(9);


            } else if (e.getOldValue().equals("October")) {
                setOldMonth(10);


            } else if (e.getOldValue().equals("November")) {
                setOldMonth(11);


            } else if (e.getOldValue().equals("December")) {
                setOldMonth(12);
            }
        } catch (Throwable ex) {
        }
    }

    public void getOldValuesDay(ValueChangeEvent e) {
        oldDay = (Integer) e.getOldValue();
        newDay = (Integer) e.getNewValue();

    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
