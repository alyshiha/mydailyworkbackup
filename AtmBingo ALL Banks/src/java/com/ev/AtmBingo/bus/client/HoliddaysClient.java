/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.HolidaysBOInter_1;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.HolidaysDTOInter_1;
import com.ev.AtmBingo.bus.dto.privilegeStatusNameDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author shi7a
 */
@ManagedBean(name = "HolidaysView")
@ViewScoped
public class HoliddaysClient extends BaseBean implements Serializable {

    private HolidaysDTOInter_1 selectedRecord, newRecord;
    private List<HolidaysDTOInter_1> recordsList;
    private int listcount;
    private String holidaytype, holidayname;
    private List<String> privelageList;
    private HolidaysBOInter_1 engin;
    private TimeZone timeZone;
    private Session sessionutil;

    public HolidaysDTOInter_1 getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(HolidaysDTOInter_1 selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public HolidaysDTOInter_1 getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(HolidaysDTOInter_1 newRecord) {
        this.newRecord = newRecord;
    }

    public List<HolidaysDTOInter_1> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<HolidaysDTOInter_1> recordsList) {
        this.recordsList = recordsList;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public String getHolidaytype() {
        return holidaytype;
    }

    public void setHolidaytype(String holidaytype) {
        this.holidaytype = holidaytype;
    }

    public String getHolidayname() {
        return holidayname;
    }

    public void setHolidayname(String holidayname) {
        this.holidayname = holidayname;
    }

    public List<String> getPrivelageList() {
        return privelageList;
    }

    public void setPrivelageList(List<String> privelageList) {
        this.privelageList = privelageList;
    }

    public HolidaysBOInter_1 getEngin() {
        return engin;
    }

    public void setEngin(HolidaysBOInter_1 engin) {
        this.engin = engin;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public Session getSessionutil() {
        return sessionutil;
    }

    public void setSessionutil(Session sessionutil) {
        this.sessionutil = sessionutil;
    }

    @PostConstruct
    public void init() {
        try {
            holidaytype = "All";
            holidayname = "";
            engin = BOFactory.createHolidaysBO_1();
            recordsList = engin.findAll();
            listcount = recordsList.size();
            privelageList = super.findPrivelage(super.getLoggedInUser().getUserId(), "BankHolidays.xhtml");
            timeZone = TimeZone.getDefault();
            sessionutil = new Session();
        } catch (Throwable ex) {
            Logger.getLogger(privilegeStatusClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public void searchrecords() {
        try {
            if (holidaytype == null) {
                holidaytype = "All";
            }
            if (holidayname == null) {
                holidayname = "";
            }
            recordsList = engin.findRecord(holidaytype, holidayname);
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(privilegeStatusClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean insertPriv() {
        return super.findPrivelageOperation(privelageList, "INSERT");
    }

    public void addRecord() {
        newRecord = DTOFactory.createHolidaysDTO_1();
    }

    public void saveRecord() throws SQLException, Throwable {
        String message = "";
        message = engin.insertRecord(newRecord);
        recordsList.add(newRecord);
        listcount = recordsList.size();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("carDialog2.hide();");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean updatePriv() {
        return super.findPrivelageOperation(privelageList, "UPDATE");
    }

    public void updateRecord() throws SQLException, Throwable {
        String message = "";
        message = engin.updateRecord(selectedRecord);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("carDialog.hide();");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean deletePriv() {
        return super.findPrivelageOperation(privelageList, "DELETE");
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printreportPDF() throws Exception, Throwable {

        String path = (String) engin.printPDFrep(sessionutil.Getclient(), sessionutil.GetUserLogging().getUserName(), holidaytype);
        prepDownload(path);
    }

    public void deleteRecord() throws SQLException {
        String message = engin.deleteRecord(selectedRecord);
        recordsList.remove(selectedRecord);
        listcount = recordsList.size();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public List<String> completeContains(String query) {
        List<String> filtered = new ArrayList<String>();
        for (int i = 0; i < recordsList.size(); i++) {
            HolidaysDTOInter_1 rec = recordsList.get(i);
            if (rec.getHolidayname().toLowerCase().contains(query.toLowerCase())) {
                filtered.add(rec.getHolidayname());
            }
        }
        return filtered;
    }

}
