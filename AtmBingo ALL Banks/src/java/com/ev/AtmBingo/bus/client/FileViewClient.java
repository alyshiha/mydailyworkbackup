/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import java.io.IOException;
import java.sql.SQLException;
import javax.faces.event.ActionEvent;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.FileViewBOInter;
import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.sql.Clob;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class FileViewClient extends BaseBean implements Serializable {

    private List<AtmMachineDTOInter> atmList;
    private FileViewBOInter cdObject;
    private String cardno, sequence, texttocomp, transfile, hiddenText, message;
    private Clob sa;
    private Date loadingFrom, loadingTo;
    private Integer atmInt;
    private List<AtmFileDTOInter> atmFileList = new ArrayList<AtmFileDTOInter>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getHiddenText() {
        return hiddenText;
    }

    public void setHiddenText(String hiddenText) {
        this.hiddenText = hiddenText;
    }

    public String getTransfile() {
        return transfile;
    }

    public void setTransfile(String transfile) {
        this.transfile = transfile;
    }

    public Clob getSa() {
        return sa;
    }

    public void setSa(Clob sa) {
        this.sa = sa;
    }

    public List<AtmFileDTOInter> getAtmFileList() {
        return atmFileList;
    }

    public void setAtmFileList(List<AtmFileDTOInter> atmFileList) {
        this.atmFileList = atmFileList;
    }

    public String getTexttocomp() {
        return texttocomp;
    }

    public void setTexttocomp(String texttocomp) {
        this.texttocomp = texttocomp;
    }

    public Integer getAtmInt() {
        return atmInt;
    }

    public void setAtmInt(Integer atmInt) {
        this.atmInt = atmInt;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public Date getLoadingFrom() {
        return loadingFrom;
    }

    public void setLoadingFrom(Date loadingFrom) {
        this.loadingFrom = loadingFrom;
    }

    public Date getLoadingTo() {
        return loadingTo;
    }

    public void setLoadingTo(Date loadingTo) {
        this.loadingTo = loadingTo;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public List<AtmMachineDTOInter> getAtmList() {
        return atmList;
    }

    public void setAtmList(List<AtmMachineDTOInter> atmList) {
        this.atmList = atmList;
    }

    public List<String> complete(String query) {
        List<String> results = new ArrayList<String>();
        for (int i = 0; i < atmList.size(); i++) {
            if (atmList.get(i).getApplicationId().toString().startsWith(query)) {
                results.add(atmList.get(i).getApplicationId().toString());
            }
        }
        return results;
    }

    public static byte[] stringToBytesASCII(String str) {
        char[] buffer = str.toCharArray();
        byte[] b = new byte[buffer.length];
        for (int i = 0; i < b.length; i++) {
            b[i] = (byte) buffer[i];
        }
        return b;
    }

    public String[] SplitString(String Text) throws Exception {
        return Text.split("\n");
    }

    public String clobToString(Clob key) {
        Object value = null;
        {
            java.io.InputStream is = null;
            try {
                is = key.getAsciiStream();
                byte[] buffer = new byte[4096];
                byte[] breakpoint = new byte[4096];
                breakpoint = stringToBytesASCII("braaa");
                java.io.OutputStream outputStream = new java.io.ByteArrayOutputStream();
                while (true) {
                    int read = is.read(buffer);
                    if (read == -1) {
                        break;
                    }
                    outputStream.write(CreateArray(buffer, breakpoint), 0, read);

                }
                outputStream.close();
                is.close();
                value = outputStream.toString();
            } catch (SQLException ex) {
                Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
            } catch (java.io.IOException io_ex) {
            } finally {
                try {
                    is.close();
                } catch (IOException ex) {
                    Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return value.toString();
    }

    public byte[] CreateArray(byte[] a, byte[] b) {
        byte[] result = new byte[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    public FileViewClient() throws Throwable {
        super.GetAccessPage();
        FacesContext context = FacesContext.getCurrentInstance();

        Session sessionutil = new Session();
        UsersDTOInter userHolded = sessionutil.GetUserLogging();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        cdObject = BOFactory.createFileViewBO(null);
        atmList = (List<AtmMachineDTOInter>) cdObject.getAtmMachines(userHolded);
        
    }

    public void dosearch() throws Throwable {
        if (texttocomp == null || loadingFrom == null || loadingTo == null) {
            message = super.showMessage(REQ_FIELD);
        } else {
            atmFileList = (List<AtmFileDTOInter>) cdObject.dosearch(texttocomp, loadingFrom, loadingTo, cardno, sequence);
            message = "";
        }
    }

    public void getFiletransaction(ActionEvent e) throws Throwable {
        AtmFileDTOInter m = (AtmFileDTOInter) e.getComponent().getAttributes().get("textfile");
        sa = (Clob) cdObject.FindFiletransaction(m);
        transfile = clobToString(sa);
    }

    public void printFile() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            Integer Seq = cdObject.PrintLines(hiddenText);
            String path = (String) cdObject.PrintLinesReport(Seq.toString());
            response.sendRedirect(path);
        } catch (Exception ex) {
            Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();

    }
}
