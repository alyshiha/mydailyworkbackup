/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.DashBoardBOInter;
import com.ev.AtmBingo.bus.bo.NotificationBOInter;
import com.ev.AtmBingo.bus.bo.PendingAtmsBOInter;
import com.ev.AtmBingo.bus.dto.NotificationDTOInter;
import com.ev.AtmBingo.bus.dto.PendingAtmsDTOInter;
import com.ev.AtmBingo.bus.dto.UserWorkDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.management.Notification;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author ISLAM
 */
public class DashBoardClient extends BaseBean  implements Serializable{

    /**
     * Creates a new instance of DashBoardClient
     */
    private List<UserWorkDTOInter> uwDTOL;
    private List<NotificationDTOInter> Notifications, Errors;
    private List<PendingAtmsDTOInter> pendingAtms;
    private DashBoardBOInter dbBO;
    private NotificationBOInter NotificationBO;
    private PendingAtmsBOInter PendingAtmsBO;
    private List<String> newAtmsList;
    private String notifInt;
    private Integer settledCount, unsettledCount, matchedCount;
    private Boolean listFlag, noAtmsFlag, onlineUsersFlag, newAtmsFlag, pendingAtmFlag, notificationFlag, errorFlag, tabVoew;
    private List<UsersDTOInter> onlineUsers;
    private PieChartModel pieValues;
    private Session utillist = new Session();

    public Boolean getTabVoew() {
        return tabVoew;
    }

    public void setTabVoew(Boolean tabVoew) {
        this.tabVoew = tabVoew;
    }

    public Boolean getErrorFlag() {
        return errorFlag;
    }

    public void setErrorFlag(Boolean errorFlag) {
        this.errorFlag = errorFlag;
    }

    public Boolean getNotificationFlag() {
        return notificationFlag;
    }

    public void setNotificationFlag(Boolean notificationFlag) {
        this.notificationFlag = notificationFlag;
    }

    public Boolean getNewAtmsFlag() {
        return newAtmsFlag;
    }

    public void setNewAtmsFlag(Boolean newAtmsFlag) {
        this.newAtmsFlag = newAtmsFlag;
    }

    public Boolean getOnlineUsersFlag() {
        return onlineUsersFlag;
    }

    public void setOnlineUsersFlag(Boolean onlineUsersFlag) {
        this.onlineUsersFlag = onlineUsersFlag;
    }

    public Boolean getPendingAtmFlag() {
        return pendingAtmFlag;
    }

    public void setPendingAtmFlag(Boolean pendingAtmFlag) {
        this.pendingAtmFlag = pendingAtmFlag;
    }

    public PieChartModel getPieValues() {
        return pieValues;
    }

    public void setPieValues(PieChartModel pieValues) {
        this.pieValues = pieValues;
    }

    public Integer getMatchedCount() {
        return matchedCount;
    }

    public void setMatchedCount(Integer matchedCount) {
        this.matchedCount = matchedCount;
    }

    public Integer getSettledCount() {
        return settledCount;
    }

    public void setSettledCount(Integer settledCount) {
        this.settledCount = settledCount;
    }

    public Integer getUnsettledCount() {
        return unsettledCount;
    }

    public void setUnsettledCount(Integer unsettledCount) {
        this.unsettledCount = unsettledCount;
    }

    public List<UsersDTOInter> getOnlineUsers() {
        return onlineUsers;
    }

    public void setOnlineUsers(List<UsersDTOInter> onlineUsers) {
        this.onlineUsers = onlineUsers;
    }

    public Boolean getListFlag() {
        return listFlag;
    }

    public void setListFlag(Boolean listFlag) {
        this.listFlag = listFlag;
    }

    public Boolean getNoAtmsFlag() {
        return noAtmsFlag;
    }

    public void setNoAtmsFlag(Boolean noAtmsFlag) {
        this.noAtmsFlag = noAtmsFlag;
    }

    public String getNotifInt() {
        return notifInt;
    }

    public void setNotifInt(String notifInt) {
        this.notifInt = notifInt;
    }

    public List<String> getNewAtmsList() {
        return newAtmsList;
    }

    public void setNewAtmsList(List<String> newAtmsList) {
        this.newAtmsList = newAtmsList;
    }

    public List<UserWorkDTOInter> getUwDTOL() {
        return uwDTOL;
    }

    public void setUwDTOL(List<UserWorkDTOInter> uwDTOL) {
        this.uwDTOL = uwDTOL;
    }

    public List<NotificationDTOInter> getErrors() {
        return Errors;
    }

    public void setErrors(List<NotificationDTOInter> Errors) {
        this.Errors = Errors;
    }
//PendingAtmsBO

    public List<PendingAtmsDTOInter> getPendingAtms() {
        return pendingAtms;
    }

    public void setPendingAtms(List<PendingAtmsDTOInter> pendingAtms) {
        this.pendingAtms = pendingAtms;
    }

    public List<NotificationDTOInter> getNotifications() {
        return Notifications;
    }

    public void setNotifications(List<NotificationDTOInter> Notifications) {
        this.Notifications = Notifications;
    }
//Notifications

    public DashBoardClient() {
        super();
       
        try {

            uwDTOL = new ArrayList<UserWorkDTOInter>();
            Notifications = new ArrayList<NotificationDTOInter>();
            Errors = new ArrayList<NotificationDTOInter>();
            pendingAtms = new ArrayList<PendingAtmsDTOInter>();
            pieValues = new PieChartModel();
            dbBO = BOFactory.createDashBoardBO(null);
            NotificationBO = BOFactory.createNotificationBO(null);
            PendingAtmsBO = BOFactory.createPendingAtmsBo(null);
            FacesContext context = FacesContext.getCurrentInstance();
             
            
            
        
         Session sessionutil = new Session();
        UsersDTOInter logedinUser = sessionutil.GetUserLogging();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.addHeader("X-Frame-Options", "SAMEORIGIN");
            try {
                if (logedinUser.getNotification() == 1) {
                    Notifications = (List<NotificationDTOInter>) utillist.GetNotificationsList();
                    notificationFlag = true;
                } else {
                    notificationFlag = false;
                }
                if (logedinUser.getErrors() == 1) {
                    Errors = (List<NotificationDTOInter>) utillist.GetErrorsList();
                    errorFlag = true;
                } else {
                    errorFlag = false;
                }
                if (logedinUser.getPendingatms() == 1) {
                    pendingAtms = (List<PendingAtmsDTOInter>) utillist.GetPendingAtmsList();
                    pendingAtmFlag = true;
                } else {
                    pendingAtmFlag = false;
                }
                if (logedinUser.getDisputeGraph() == 1) {

                    uwDTOL = dbBO.getUserWorkGraph(logedinUser.getUserId(), utillist.GetTimeFrom(), utillist.GetTimeTo());
                    pieValues.set(uwDTOL.get(0).getName(), uwDTOL.get(0).getValue());
                    pieValues.set(uwDTOL.get(1).getName(), uwDTOL.get(1).getValue());
                    pieValues.set(uwDTOL.get(2).getName(), uwDTOL.get(2).getValue());
                    unsettledCount = uwDTOL.get(0).getValue();
                    settledCount = uwDTOL.get(1).getValue();
                    matchedCount = uwDTOL.get(2).getValue();
                }
                if (logedinUser.getShowNewAtms() == 1) {
                    newAtmsList = (List<String>) dbBO.getNewAtms(logedinUser.getUserId());
                    if (newAtmsList.size() == 0) {
                        listFlag = false;
                        noAtmsFlag = true;
                        newAtmsList.add("No records");
                    } else {
                        listFlag = true;
                        noAtmsFlag = false;
                    }
                    newAtmsFlag = true;
                } else {
                    newAtmsFlag = false;
                }
                if (logedinUser.getOnlineuser() == 1) {
                    onlineUsers = (List<UsersDTOInter>) dbBO.getOnlineUser(super.getRestrected());
                    onlineUsersFlag = true;
                } else {
                    onlineUsersFlag = false;
                }
                if (notificationFlag == false && errorFlag == false) {
                    tabVoew = false;
                } else {
                    tabVoew = true;
                }
            } catch (Throwable ex) {
                Logger.getLogger(DashBoardClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (Throwable ex) {
            Logger.getLogger(DashBoardClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
