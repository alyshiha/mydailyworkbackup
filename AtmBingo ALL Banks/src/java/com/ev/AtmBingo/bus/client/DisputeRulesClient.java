/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.bo.DisputeRulesBOInter;
import com.ev.AtmBingo.bus.dto.FileColumnDefinitionDTOInter;
import java.util.List;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DisputeRulesDTOInter;
import com.ev.AtmBingo.bus.dto.ValidationRulesDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class DisputeRulesClient extends BaseBean  implements Serializable{

    private DisputeRulesBOInter drObject;
    private DisputeRulesDTOInter drUpAttr, drDelAttr, newRecord;
    private List<DisputeRulesDTOInter> drList;
    private List<FileColumnDefinitionDTOInter> fcdList;
    private Integer fcdInt, addType, ruleSelect;
    private Boolean updateFlag, activeBool, dateSelected, pickBool, betweenDate, disFlag, valFlag, formulaFlag, addFormula, betweenFlag, nullOperation;
    private String formulaStatus, message, formulaName, checkUp, columnNam, operationStr, firstText, andValue, secondValue, allFormula, colname;
    private Date firstTextDate, secondValueDate;
    private List<String> operationValues;
    private List<Object> columnValues;
    private Integer[] valuesList;
    private Integer Type;

    public Integer getType() {
        return Type;
    }

    public void setType(Integer Type) {
        this.Type = Type;
    }

    public Boolean getActiveBool() {
        return activeBool;
    }

    public void setActiveBool(Boolean activeBool) {
        this.activeBool = activeBool;
    }

    public String getColname() {
        return colname;
    }

    public void setColname(String colname) {
        this.colname = colname;
    }

    public Boolean getBetweenDate() {
        return betweenDate;
    }

    public void setBetweenDate(Boolean betweenDate) {
        this.betweenDate = betweenDate;
    }

    public List<Object> getColumnValues() {
        return columnValues;
    }

    public void setColumnValues(List<Object> columnValues) {
        this.columnValues = columnValues;
    }

    public Boolean getDateSelected() {
        return dateSelected;
    }

    public void setDateSelected(Boolean dateSelected) {
        this.dateSelected = dateSelected;
    }

    public Date getFirstTextDate() {
        return firstTextDate;
    }

    public void setFirstTextDate(Date firstTextDate) {
        this.firstTextDate = firstTextDate;
    }

    public String getFormulaStatus() {
        return formulaStatus;
    }

    public void setFormulaStatus(String formulaStatus) {
        this.formulaStatus = formulaStatus;
    }

    public Boolean getPickBool() {
        return pickBool;
    }

    public void setPickBool(Boolean pickBool) {
        this.pickBool = pickBool;
    }

    public Date getSecondValueDate() {
        return secondValueDate;
    }

    public void setSecondValueDate(Date secondValueDate) {
        this.secondValueDate = secondValueDate;
    }

    public Boolean getUpdateFlag() {
        return updateFlag;
    }

    public void setUpdateFlag(Boolean updateFlag) {
        this.updateFlag = updateFlag;
    }

    public Integer[] getValuesList() {
        return valuesList;
    }

    public void setValuesList(Integer[] valuesList) {
        this.valuesList = valuesList;
    }

    public Integer getAddType() {
        return addType;
    }

    public void setAddType(Integer addType) {
        this.addType = addType;
    }

    public Boolean getDisFlag() {
        return disFlag;
    }

    public void setDisFlag(Boolean disFlag) {
        this.disFlag = disFlag;
    }

    public Boolean getValFlag() {
        return valFlag;
    }

    public void setValFlag(Boolean valFlag) {
        this.valFlag = valFlag;
    }

    public Integer getRuleSelect() {
        return ruleSelect;
    }

    public void setRuleSelect(Integer ruleSelect) {
        this.ruleSelect = ruleSelect;
    }

    public String getFormulaName() {
        return formulaName;
    }

    public void setFormulaName(String formulaName) {
        this.formulaName = formulaName;
    }

    public Boolean getNullOperation() {
        return nullOperation;
    }

    public void setNullOperation(Boolean nullOperation) {
        this.nullOperation = nullOperation;
    }

    public List<String> getOperationValues() {
        return operationValues;
    }

    public void setOperationValues(List<String> operationValues) {
        this.operationValues = operationValues;
    }

    public Boolean getBetweenFlag() {
        return betweenFlag;
    }

    public void setBetweenFlag(Boolean betweenFlag) {
        this.betweenFlag = betweenFlag;
    }

    public String getAllFormula() {
        return allFormula;
    }

    public void setAllFormula(String allFormula) {
        this.allFormula = allFormula;
    }

    public String getAndValue() {
        return andValue;
    }

    public void setAndValue(String andValue) {
        this.andValue = andValue;
    }

    public String getFirstText() {
        return firstText;
    }

    public void setFirstText(String firstText) {
        this.firstText = firstText;
    }

    public String getSecondValue() {
        return secondValue;
    }

    public void setSecondValue(String secondValue) {
        this.secondValue = secondValue;
    }

    public String getOperationStr() {
        return operationStr;
    }

    public void setOperationStr(String operationStr) {
        this.operationStr = operationStr;
    }

    public String getColumnNam() {
        return columnNam;
    }

    public void setColumnNam(String columnNam) {
        this.columnNam = columnNam;
    }

    public Boolean getAddFormula() {
        return addFormula;
    }

    public void setAddFormula(Boolean addFormula) {
        this.addFormula = addFormula;
    }

    public Boolean getFormulaFlag() {
        return formulaFlag;
    }

    public void setFormulaFlag(Boolean formulaFlag) {
        this.formulaFlag = formulaFlag;
    }

    public DisputeRulesDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(DisputeRulesDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public String getCheckUp() {
        return checkUp;
    }

    public void setCheckUp(String checkUp) {
        this.checkUp = checkUp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DisputeRulesDTOInter getDrDelAttr() {
        return drDelAttr;
    }

    public void setDrDelAttr(DisputeRulesDTOInter drDelAttr) {
        this.drDelAttr = drDelAttr;
    }

    public DisputeRulesDTOInter getDrUpAttr() {
        return drUpAttr;
    }

    public void setDrUpAttr(DisputeRulesDTOInter drUpAttr) {
        this.drUpAttr = drUpAttr;
    }

    public Integer getFcdInt() {
        return fcdInt;
    }

    public void setFcdInt(Integer fcdInt) {
        this.fcdInt = fcdInt;
    }

    public List<DisputeRulesDTOInter> getDrList() {
        return drList;
    }

    public void setDrList(List<DisputeRulesDTOInter> drList) {
        this.drList = drList;
    }

    public List<FileColumnDefinitionDTOInter> getFcdList() {
        return fcdList;
    }

    public void setFcdList(List<FileColumnDefinitionDTOInter> fcdList) {
        this.fcdList = fcdList;
    }

    /** Creates a new instance of DisputeRulesClient */
    public DisputeRulesClient() throws Throwable {
        super();
        super.GetAccessPage();
             FacesContext context2 = FacesContext.getCurrentInstance();
        HttpSession session2 = (HttpSession) context2.getExternalContext().getSession(true);
        HttpServletResponse response2 = (HttpServletResponse) context2.getExternalContext().getResponse();
        response2.addHeader("X-Frame-Options", "SAMEORIGIN");
        drObject = BOFactory.createDisputeRulesBO(null);
        fcdInt = 3;
        drList = (List<DisputeRulesDTOInter>) drObject.getDisputeRules();
        formulaFlag = true;
        addFormula = false;
        betweenFlag = false;
        nullOperation = false;
        betweenDate = false;
        dateSelected = false;
        pickBool = false;
        columnNam = "Transaction Date";
        andValue = "AND";
        addType = 1;
        disFlag = true;
        //activeBool = true;
        valFlag = false;
        fcdList = (List<FileColumnDefinitionDTOInter>) drObject.getColumns();
        operationValues = new ArrayList<String>();
        columnValues = new ArrayList<Object>();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        ruleSelect = Integer.parseInt(req.getParameter("ID"));
        if (ruleSelect == 1) {
            disFlag = true;
            valFlag = false;
        } else {
            disFlag = false;
            valFlag = true;
        }
    }

    public void addPopup() throws Throwable {
        if (columnNam.contains("Date") || columnNam.contains("Time")) {
            if (columnValues == null) {
                columnValues = new ArrayList<Object>();
            }
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add(">=");
            operationValues.add("<=");
            operationValues.add("between");
            dateSelected = true;
            betweenDate = false;
            nullOperation = false;
            betweenFlag = false;
            pickBool = false;
        } else {
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add("in");
            operationValues.add("not in");
            operationValues.add("null");
            operationValues.add("is not null");
            columnValues = (List<Object>) drObject.getColumnValues(fcdInt);
            if (columnValues == null) {
                columnValues = new ArrayList<Object>();
                dateSelected = false;
                betweenDate = false;
                nullOperation = true;
                betweenFlag = false;
                pickBool = false;
            } else {
                dateSelected = false;
                betweenDate = false;
                nullOperation = false;
                betweenFlag = false;
                pickBool = true;
            }

        }
        updateFlag = false;
        formulaStatus = "Add";
        resetVars();
    }

    public void ruleSelector(ValueChangeEvent e) {
        ruleSelect = (Integer) e.getNewValue();
        if (ruleSelect == 1) {
            disFlag = true;
            valFlag = false;
        } else if (ruleSelect == 2) {
            disFlag = false;
            valFlag = true;
        }
        resetVars();
    }

    public void valueEffect(ValueChangeEvent e) throws Throwable {
        fcdInt = (Integer) e.getNewValue();
        drList = (List<DisputeRulesDTOInter>) drObject.getDisputeRules();
        columnNam = drObject.getColumnName(fcdInt);
        message = "";
        if (columnNam.contains("Date") || columnNam.contains("Time")) {
            if (columnValues == null) {
                columnValues = new ArrayList<Object>();
            }
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add(">=");
            operationValues.add("<=");
            operationValues.add("between");
            dateSelected = true;
            betweenDate = false;
            nullOperation = false;
            betweenFlag = false;
            pickBool = false;
        } else {
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add("in");
            operationValues.add("not in");
            operationValues.add("null");
            operationValues.add("is not null");
            columnValues = (List<Object>) drObject.getColumnValues(fcdInt);
            if (columnValues == null) {
                columnValues = new ArrayList<Object>();
                dateSelected = false;
                betweenDate = false;
                nullOperation = true;
                betweenFlag = false;
                pickBool = false;
            } else {
                dateSelected = false;
                betweenDate = false;
                nullOperation = false;
                betweenFlag = false;
                pickBool = true;
            }

        }
        updateFlag = false;
        formulaStatus = "Add";
        resetVars();

    }

    public void fillDelete(ActionEvent e) {
        drDelAttr = (DisputeRulesDTOInter) e.getComponent().getAttributes().get("removedRow");
        message = "";
    }

    public void deleteRecord() {
        try {
            drDelAttr.setColumnId(fcdInt);
            drObject.editDisputeRules(drDelAttr, drObject.DELETE);
            drList.remove(drDelAttr);
            message = super.showMessage(SUCC);
            addFormula = false;
            formulaFlag = true;
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void fillUpdate(ActionEvent e) {
        drUpAttr = (DisputeRulesDTOInter) e.getComponent().getAttributes().get("updateRow");
        formulaName = drUpAttr.getName();
        message = "";
    }

    public void updateRecord() {
        try {
            //    checkUp = (String) drObject.validateFormula(drUpAttr);
            if (drUpAttr.getName().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                if (checkUp.equals("done")) {
                    drUpAttr.setColumnId(fcdInt);
                    drObject.editDisputeRules(drUpAttr, drObject.UPDATE);
                    message = super.showMessage(SUCC);
                } else {
                    message = checkUp;
                }
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void operationChange(ValueChangeEvent e) {
        operationStr = (String) e.getNewValue();
        if (columnNam.contains("Date")) {
            if (operationStr.equals("between")) {
                dateSelected = true;
                betweenDate = true;
                nullOperation = false;
                betweenFlag = false;
                pickBool = false;
            } else {
                dateSelected = true;
                betweenDate = false;
                nullOperation = false;
                betweenFlag = false;
                pickBool = false;
            }
        } else if (columnValues.size() == 0 || columnValues == null) {
            if (operationStr.contains("null") || operationStr.contains("is not null")) {
                dateSelected = false;
                betweenDate = false;
                nullOperation = true;
                betweenFlag = false;
                pickBool = false;
            } else {
                dateSelected = false;
                betweenDate = false;
                nullOperation = true;
                betweenFlag = false;
                pickBool = false;
            }
        } else {
            dateSelected = false;
            betweenDate = false;
            nullOperation = false;
            betweenFlag = false;
            pickBool = true;
        }
        resetVars();
    }

    public void addRecord() {
        try {
            if (drList == null) {
                drList = new ArrayList<DisputeRulesDTOInter>();
            }

            newRecord = DTOFactory.createDisputeRulesDTO();

            newRecord.setValidationType(addType);
            newRecord.setColumnId(fcdInt);
            newRecord.setName(formulaName);

            if (columnNam.contains("Date")) {
                Type = 1;
                if (operationStr.contains("between")) {
                    String dateValues = drObject.genarateValueDate(firstTextDate, secondValueDate);
                    if (drObject.createFormula(fcdInt, operationStr, dateValues, newRecord, Type) == "Invalid Formula") {
                        message = "Invalid Formula";
                    } else {
                        drList.add(newRecord);
                        drObject.editDisputeRules(newRecord, drObject.INSERT);
                        message = super.showMessage(SUCC);
                    }
                } else {

                    if (drObject.createFormula(fcdInt, operationStr, DateFormatter.changeDateAndTimeFormat(firstTextDate), newRecord, Type) == "Invalid Formula") {
                        message = "Invalid Formula";
                    } else {
                        drList.add(newRecord);
                        drObject.editDisputeRules(newRecord, drObject.INSERT);
                        message = super.showMessage(SUCC);
                    }
                }
            }
            if (pickBool) {
                Type = 2;
                String pickValye = drObject.genarateValueString(valuesList);
                drObject.createFormula(fcdInt, operationStr, pickValye, newRecord, Type);
                drObject.editDisputeRules(newRecord, drObject.INSERT);
                drList.add(newRecord);
                message = super.showMessage(SUCC);
            } else {
                if (!columnNam.contains("Date")) {
                    Type = 2;
                    drObject.createFormula(fcdInt, operationStr, firstText, newRecord, Type);
                    drList.add(newRecord);
                    drObject.editDisputeRules(newRecord, drObject.INSERT);
                    message = super.showMessage(SUCC);
                }
            }

        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void resetVars() {
        message = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
