/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.bo.BlockUsersBOInter;
import com.ev.AtmBingo.bus.dto.BlockReasonDTOInter;
import com.ev.AtmBingo.bus.dto.BlockUsersDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import java.util.TimeZone;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class BlockUserClient extends BaseBean  implements Serializable{

    private BlockUsersBOInter buObject;
    private List<UsersDTOInter> buList;
    private List<BlockUsersDTOInter> unbuList;
    private BlockUsersDTOInter buAttr;
    private List<BlockReasonDTOInter> reasonList;
    private UsersDTOInter holdUser;
    private BlockUsersDTOInter unholdUser;
    private Integer myInt, listSize,listSize1;
    private TimeZone timeZone;
    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public Integer getListSize1() {
        return listSize1;
    }

    public void setListSize1(Integer listSize1) {
        this.listSize1 = listSize1;
    }

    

    public List<BlockUsersDTOInter> getUnbuList() {
        return unbuList;
    }

    public void setUnbuList(List<BlockUsersDTOInter> unbuList) {
        this.unbuList = unbuList;
    }

    public Integer getMyInt() {
        return myInt;
    }

    public void setMyInt(Integer myInt) {
        this.myInt = myInt;
    }

    public List<BlockReasonDTOInter> getReasonList() {
        return reasonList;
    }

    public void setReasonList(List<BlockReasonDTOInter> reasonList) {
        this.reasonList = reasonList;
    }

    public List<UsersDTOInter> getBuList() {
        return buList;
    }

    public void setBuList(List<UsersDTOInter> buList) {
        this.buList = buList;
    }

    /** Creates a new instance of BlockUserClient */
    public BlockUserClient() throws Throwable {
        super();
        super.GetAccessPage();
        timeZone = TimeZone.getDefault();
        buObject = BOFactory.createBlockUsersBO(null);
        buList = (List<UsersDTOInter>) buObject.getUsers(super.getRestrected());
        unbuList = (List<BlockUsersDTOInter>) buObject.getBlockedUsers();

        reasonList = (List<BlockReasonDTOInter>) buObject.getBlockReasons();
        buAttr = DTOFactory.createBlockUsersDTO();
        listSize = buList.size();
        listSize1 = unbuList.size();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void fillBlockedUser(ActionEvent e) throws Throwable {
        holdUser = (UsersDTOInter) e.getComponent().getAttributes().get("blockedUser");
        buAttr.setUserId(holdUser);
    }

    public void blockUser(ActionEvent e) throws Throwable {
        buAttr.getReasonId().setReasonId(myInt);
        buObject.blockHim(buAttr);
        buList.remove(holdUser);
    }

    public void fillUnBlockedUser(ActionEvent e) throws Throwable {
        unholdUser = (BlockUsersDTOInter) e.getComponent().getAttributes().get("unblockedUser");

        buAttr.setUserId(unholdUser.getUserId());

    }

    public void unBlockUser(ActionEvent e) throws Throwable {
        buObject.unBlockHim(buAttr);
        unbuList.remove(unholdUser);

    }  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }
}
