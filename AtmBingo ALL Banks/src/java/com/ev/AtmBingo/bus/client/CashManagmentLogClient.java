/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.CashManagmentLogBOInter;
import com.ev.AtmBingo.bus.dto.CashManagmentLogDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;

@ManagedBean(name = "cashManagmentLogView")
@ViewScoped
public class CashManagmentLogClient extends BaseBean implements Serializable {

    private List<CashManagmentLogDTOInter> recordsList;
    private List<UsersDTOInter> usersList;
    private Date fromDate, toDate;
    private int userID;
    private CashManagmentLogBOInter engin;
    private Session sessionutil = new Session();
    private int listcount;
    private List<String> privelageList;

    public List<CashManagmentLogDTOInter> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<CashManagmentLogDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public List<UsersDTOInter> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<UsersDTOInter> usersList) {
        this.usersList = usersList;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public CashManagmentLogBOInter getEngin() {
        return engin;
    }

    public void setEngin(CashManagmentLogBOInter engin) {
        this.engin = engin;
    }

    public Session getSessionutil() {
        return sessionutil;
    }

    public void setSessionutil(Session sessionutil) {
        this.sessionutil = sessionutil;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public List<String> getPrivelageList() {
        return privelageList;
    }

    public void setPrivelageList(List<String> privelageList) {
        this.privelageList = privelageList;
    }

    @PostConstruct
    public void init() {
        try {
            fromDate = super.getYesterdaytrans((String) "00:00:00");
            toDate = super.getTodaytrans((String) "00:00:00");
            userID = 0;
            usersList = sessionutil.GetUsersList();
            engin = BOFactory.createCashManagmentLogBO(null);
            recordsList = new ArrayList<CashManagmentLogDTOInter>();
            listcount = recordsList.size();
            privelageList = super.findPrivelage(sessionutil.GetUserLogging().getUserId(), "CashManagmentLog.xhtml");
        } catch (Throwable ex) {
            Logger.getLogger(CashManagmentLogClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public void searchrecords() {
        try {
            recordsList = engin.findRecord(userID, fromDate, toDate);
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(NetworksClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public String searchUserName(Integer UserID) {
        for (UsersDTOInter userRecord : usersList) {
            if (userRecord.getUserId() == UserID) {
                return userRecord.getUserName();
            }
        }
        return "All Users";
    }

    public void printreportPDF() throws Exception, Throwable {

        String path = (String) engin.printPDFrep(userID, fromDate, toDate, sessionutil.Getclient(), searchUserName(userID), super.getLoggedInUser().getUserName());
        prepDownload(path);
    }

    public Boolean datesPriv() {
        if (super.findPrivelageOperation(privelageList, "SEARCH_DATE")) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }
}
