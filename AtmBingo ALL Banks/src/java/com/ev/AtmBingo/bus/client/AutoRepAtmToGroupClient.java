/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.AtmDefinitionBOInter;
import com.ev.AtmBingo.bus.bo.AutoRepAtmToGroupBoInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.CasseteBOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.AutoRepDTOInter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Administrator
 */
public class AutoRepAtmToGroupClient extends BaseBean  implements Serializable{

    private AutoRepAtmToGroupBoInter atmObject;
    private CasseteBOInter cassObject;
    private List<CassetteDTOInter> CassetteGroup;
    private List<AtmMachineDTOInter> atmList;
    private List<CurrencyMasterDTOInter> currList;

    private List<AutoRepDTOInter> repList, groupList;
    private AutoRepDTOInter brAttr, brAttrUp, newBlook, brAttrGroup, brAttrUpGroup, newGroup, selectedMaster;
    private String message, message1, groupName;
    private Integer groupInt, groupInt2, listSize;
    private Boolean showAdd, showConfirm, showGroupAdd, showGroupConfirm;
    private AutoRepDTOInter myObj;

    public AutoRepDTOInter getSelectedMaster() {
        return selectedMaster;
    }

    public void setSelectedMaster(AutoRepDTOInter selectedMaster) {
        this.selectedMaster = selectedMaster;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public AutoRepDTOInter getNewGroup() {
        return newGroup;
    }

    public void setNewGroup(AutoRepDTOInter newGroup) {
        this.newGroup = newGroup;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public AutoRepDTOInter getBrAttrGroup() {
        return brAttrGroup;
    }

    public List<CurrencyMasterDTOInter> getCurrList() {
        return currList;
    }

    public void setCurrList(List<CurrencyMasterDTOInter> currList) {
        this.currList = currList;
    }

    public void setBrAttrGroup(AutoRepDTOInter brAttrGroup) {
        this.brAttrGroup = brAttrGroup;
    }

    public AutoRepDTOInter getBrAttrUpGroup() {
        return brAttrUpGroup;
    }

    public void setBrAttrUpGroup(AutoRepDTOInter brAttrUpGroup) {
        this.brAttrUpGroup = brAttrUpGroup;
    }

    public Boolean getShowGroupAdd() {
        return showGroupAdd;
    }

    public void setShowGroupAdd(Boolean showGroupAdd) {
        this.showGroupAdd = showGroupAdd;
    }

    public Boolean getShowGroupConfirm() {
        return showGroupConfirm;
    }

    public void setShowGroupConfirm(Boolean showGroupConfirm) {
        this.showGroupConfirm = showGroupConfirm;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public AutoRepDTOInter getBrAttr() {
        return brAttr;
    }

    public void setBrAttr(AutoRepDTOInter brAttr) {
        this.brAttr = brAttr;
    }

    public AutoRepDTOInter getBrAttrUp() {
        return brAttrUp;
    }

    public void setBrAttrUp(AutoRepDTOInter brAttrUp) {
        this.brAttrUp = brAttrUp;
    }

    public AutoRepDTOInter getNewBlook() {
        return newBlook;
    }

    public void setNewBlook(AutoRepDTOInter newBlook) {
        this.newBlook = newBlook;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public Integer getGroupInt() {
        return groupInt;
    }

    public void setGroupInt(Integer groupInt) {
        this.groupInt = groupInt;
    }

    public Integer getGroupInt2() {
        return groupInt2;
    }

    public void setGroupInt2(Integer groupInt2) {
        this.groupInt2 = groupInt2;
    }

    public List<AutoRepDTOInter> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<AutoRepDTOInter> groupList) {
        this.groupList = groupList;
    }

    public List<AutoRepDTOInter> getRepList() {
        return repList;
    }

    public void setRepList(List<AutoRepDTOInter> repList) {
        this.repList = repList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AtmMachineDTOInter> getAtmList() {
        return atmList;
    }

    public void setAtmFileList(List<AtmMachineDTOInter> atmList) {
        this.atmList = atmList;
    }

    public List<CassetteDTOInter> getCassetteGroup() {
        return CassetteGroup;
    }

    public void setCassetteGroup(List<CassetteDTOInter> CassetteGroup) {
        this.CassetteGroup = CassetteGroup;
    }

    public AutoRepAtmToGroupClient() throws Throwable {
        super();
        super.GetAccessPage();
        showGroupAdd = true;
        showGroupConfirm = false;
        FacesContext context = FacesContext.getCurrentInstance();
        
        
        Session sessionutil = new Session();
        UsersDTOInter userHolded = sessionutil.GetUserLogging();
        atmObject = BOFactory.createAutoRepAtmToGroupBo(null);
        cassObject = BOFactory.createCasseteBO(null);
        repList = new ArrayList<AutoRepDTOInter>();
        CassetteGroup = (List<CassetteDTOInter>) atmObject.getCassetes();
        groupList = (List<AutoRepDTOInter>) atmObject.getGroups();
        currList = (List<CurrencyMasterDTOInter>) cassObject.getCurrency();
        listSize = repList.size();
        showAdd = true;
        showConfirm = false;
        
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void groupSelect(SelectEvent e) throws Throwable {
        myObj = (AutoRepDTOInter) e.getObject();
        repList = (List<AutoRepDTOInter>) atmObject.getAll(myObj.getGroupid());
        message = "";
        message1 = "";
    }

    public void GroupDel(ActionEvent e) {
        brAttrGroup = (AutoRepDTOInter) e.getComponent().getAttributes().get("removedRow1");
        message = "";
        message1 = "";
    }

    public void GroupUp(ActionEvent e) {
        brAttrUpGroup = (AutoRepDTOInter) e.getComponent().getAttributes().get("updateRow1");
        message = "";
        message1 = "";
    }

    public void deleteGroup() {
        try {
            String[] inputs = {brAttrGroup.getGroupName(), "" + brAttrGroup.getCurrency(), "" + brAttrGroup.getGroupid(), "" + brAttrGroup.getTypecassette(), "" + brAttrGroup.getTypenumber()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
                groupList.remove(brAttrGroup);
                showGroupAdd = true;
                showGroupConfirm = false;
                message1 = super.showMessage(SUCC);
                atmObject.DeleteGroup(brAttrGroup);
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
        }

    }

    public void updateGroup() {
        if (showGroupConfirm) {
            message1 = super.showMessage(CONF_FIRST);
        } else if (brAttrUpGroup.getGroupName() == null) {
            message1 = super.showMessage(REQ_FIELD);
        } else {
            try {
                String[] inputs = {brAttrUpGroup.getGroupName(), "" + brAttrUpGroup.getCurrency(), "" + brAttrUpGroup.getGroupid(), "" + brAttrUpGroup.getTypecassette(), "" + brAttrUpGroup.getTypenumber()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    atmObject.UpdateGroup(brAttrUpGroup);
                    message1 = super.showMessage(SUCC);
                } else {
                    message = "Please Enter Valid Data";
                }

            } catch (Throwable ex) {
                message1 = super.showMessage(REC_EXIST);
            }
        }
    }

    public void fillDel(ActionEvent e) {
        brAttr = (AutoRepDTOInter) e.getComponent().getAttributes().get("removedRow");
        message = "";
        message1 = "";
    }

    public void fillUp(ActionEvent e) {
        brAttrUp = (AutoRepDTOInter) e.getComponent().getAttributes().get("updateRow");
        message = "";
        message1 = "";
    }

    public void deleteRecord() {
        try {
            String[] inputs = {brAttr.getGroupName(), "" + brAttr.getCurrency(), "" + brAttr.getGroupid(), "" + brAttr.getTypecassette(), "" + brAttr.getTypenumber()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
                atmObject.editeAtmGroup(brAttr, atmObject.DELETE);
                repList.remove(brAttr);
                showAdd = true;
                showConfirm = false;
                message = super.showMessage(SUCC);
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
        }

    }

    public void updateRecord() {

        try {
            String[] inputs = {brAttrUp.getGroupName(), "" + brAttrUp.getCurrency(), "" + brAttrUp.getGroupid(), "" + brAttrUp.getTypecassette(), "" + brAttrUp.getTypenumber()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
                atmObject.editeAtmGroup(brAttrUp, atmObject.UPDATE);
                message = super.showMessage(SUCC);
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }

    }

    public void addGroup() {
        newGroup = DTOFactory.createAutoRepDTO();
        groupList.add(newGroup);
        showGroupAdd = false;
        showGroupConfirm = true;
        message = "";
        message1 = "";
    }

    public void addRecord() {
        if (repList == null) {
            repList = new ArrayList<AutoRepDTOInter>();
        }
        if (selectedMaster == null) {
            message = "Please select a record";
        } else {
            newBlook = DTOFactory.createAutoRepDTO();
            newBlook.setGroupName(myObj.getGroupName());
            newBlook.setGroupid(myObj.getGroupid());
            repList.add(newBlook);
            showAdd = false;
            showConfirm = true;
            message = "";
        }
    }

    public void confirmGroup() {
        newGroup = DTOFactory.createAutoRepDTO();
        if (groupName == null) {
            message1 = super.showMessage(REQ_FIELD);
        } else {
            try {
                newGroup.setGroupName(groupName);
String[] inputs = {newGroup.getGroupName(),""+newGroup.getCurrency(),""+newGroup.getGroupid(),""+newGroup.getTypecassette(),""+newGroup.getTypenumber()};
String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
Boolean found = Boolean.FALSE;
for (String input : inputs) {
    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
    if (b == false) {
        found = Boolean.TRUE;
    }
    if (!"".equals(input)) {
        for (String validate1 : validate) {
            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                found = Boolean.TRUE;
            }
        }
    }
}
if (!found) {
  atmObject.AddGroup(newGroup);
                groupList.add(newGroup);
                message1 = super.showMessage(SUCC);
                groupList = (List<AutoRepDTOInter>) atmObject.getGroups();
} else {
    message = "Please Enter Valid Data";
}
                
            } catch (Throwable ex) {
                message1 = super.showMessage(REC_EXIST);
            }
        }
    }

    public void confirmRecord() {
        if (selectedMaster == null) {
            message = "Select a group name";
        } else if (newBlook.getTypenumber() == null) {
            message = super.showMessage(REQ_FIELD);
        } else {
            try {
                  String[] inputs = {newBlook.getGroupName(), "" + newBlook.getCurrency(), "" + newBlook.getGroupid(), "" + newBlook.getTypecassette(), "" + newBlook.getTypenumber()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
               newBlook.setGroupid(selectedMaster.getGroupid());
                atmObject.editeAtmGroup(newBlook, atmObject.INSERT);
                showAdd = true;
                showConfirm = false;
                message = super.showMessage(SUCC);
            } else {
                message = "Please Enter Valid Data";
            }
                
            } catch (Throwable ex) {
                Logger.getLogger(AutoRepAtmToGroupClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }
    public void resetVars() {
        message = "";
        message1 = "";
    }
}
