/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.bo.TransactionTypeBOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeMasterDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author ISLAM
 */
public class TransactionTypeClient extends BaseBean  implements Serializable{

    private TransactionTypeBOInter cdObject;
    private List<TransactionTypeMasterDTOInter> cdList;
    private List<TransactionTypeDTOInter> cdDetailsList;
    private TransactionTypeMasterDTOInter timeShiftAttr;
    private TransactionTypeMasterDTOInter timeShiftAttr1;
    private TransactionTypeMasterDTOInter selectedMT;
    private TransactionTypeMasterDTOInter newRecord;
    private TransactionTypeDTOInter newRecord1;
    private TransactionTypeDTOInter timeShiftDetailsAttr, timeShiftDetailsAttr1;
    private Integer myInt;
    private Boolean showAdd, showAdd1;
    private Boolean showConfirm, showConfirm1;
    private String message;
    private String message1;

    public List<TransactionTypeDTOInter> getCdDetailsList() {
        return cdDetailsList;
    }

    public void setCdDetailsList(List<TransactionTypeDTOInter> cdDetailsList) {
        this.cdDetailsList = cdDetailsList;
    }

    public List<TransactionTypeMasterDTOInter> getCdList() {
        return cdList;
    }

    public void setCdList(List<TransactionTypeMasterDTOInter> cdList) {
        this.cdList = cdList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public Integer getMyInt() {
        return myInt;
    }

    public void setMyInt(Integer myInt) {
        this.myInt = myInt;
    }

    public TransactionTypeMasterDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(TransactionTypeMasterDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public TransactionTypeDTOInter getNewRecord1() {
        return newRecord1;
    }

    public void setNewRecord1(TransactionTypeDTOInter newRecord1) {
        this.newRecord1 = newRecord1;
    }

    public TransactionTypeMasterDTOInter getSelectedMT() {
        return selectedMT;
    }

    public void setSelectedMT(TransactionTypeMasterDTOInter selectedMT) {
        this.selectedMT = selectedMT;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public TransactionTypeMasterDTOInter getTimeShiftAttr() {
        return timeShiftAttr;
    }

    public void setTimeShiftAttr(TransactionTypeMasterDTOInter timeShiftAttr) {
        this.timeShiftAttr = timeShiftAttr;
    }

    public TransactionTypeMasterDTOInter getTimeShiftAttr1() {
        return timeShiftAttr1;
    }

    public void setTimeShiftAttr1(TransactionTypeMasterDTOInter timeShiftAttr1) {
        this.timeShiftAttr1 = timeShiftAttr1;
    }

    public TransactionTypeDTOInter getTimeShiftDetailsAttr() {
        return timeShiftDetailsAttr;
    }

    public void setTimeShiftDetailsAttr(TransactionTypeDTOInter timeShiftDetailsAttr) {
        this.timeShiftDetailsAttr = timeShiftDetailsAttr;
    }

    public TransactionTypeDTOInter getTimeShiftDetailsAttr1() {
        return timeShiftDetailsAttr1;
    }

    public void setTimeShiftDetailsAttr1(TransactionTypeDTOInter timeShiftDetailsAttr1) {
        this.timeShiftDetailsAttr1 = timeShiftDetailsAttr1;
    }

    /** Creates a new instance of TransactionTypeClient */
    public TransactionTypeClient() throws Throwable {
        super();
        super.GetAccessPage();
        cdObject = BOFactory.createTransactionTypeBO(null);
        cdList = (List<TransactionTypeMasterDTOInter>) cdObject.getTransactionTypeMaster();
        showAdd = true;
        showConfirm = false;
        showAdd1 = false;
        showConfirm1 = false;
        cdDetailsList = new ArrayList<TransactionTypeDTOInter>();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void onRowSelect1(SelectEvent e) throws Throwable {
        selectedMT = DTOFactory.createTransactionTypeMasterDTO();
        selectedMT = (TransactionTypeMasterDTOInter) e.getObject();
        cdDetailsList = (List<TransactionTypeDTOInter>) cdObject.getTranasactionType(selectedMT);
        message = "";
        message1 = "";
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void onRowUnSelect1(UnselectEvent e) throws Throwable {
        cdDetailsList = null;
        message = "";
        message1 = "";
        showAdd1 = false;
        showConfirm1 = false;
    }

    public void fillUpdate(ActionEvent e) {
        timeShiftAttr = (TransactionTypeMasterDTOInter) e.getComponent().getAttributes().get("updateRow");
        message = "";
        message1 = "";
    }

    public void updateRecord() {
        try {
            if (!showAdd) {
                message = super.showMessage(CONF_FIRST);
            } else {
                if (timeShiftAttr.getName().equals("")) {
                    message = super.showMessage(REQ_FIELD);
                } else {
                    cdObject.editeTransactionTypeMaster(timeShiftAttr, cdObject.UPDATE);
                    message = super.showMessage(SUCC);
                }
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void fillDelete(ActionEvent e) {
        timeShiftAttr1 = (TransactionTypeMasterDTOInter) e.getComponent().getAttributes().get("removedRow");
        message = "";
        message1 = "";
    }

    public void deleteRecord() {
        try {
            cdObject.editeTransactionTypeMaster(timeShiftAttr1, cdObject.DELETE);
            cdList.remove(timeShiftAttr1);
            if (cdDetailsList.size() != 0) {
                cdDetailsList.clear();
            }
            message = super.showMessage(SUCC);
            showAdd = true;
            showConfirm = false;
        } catch (Throwable ex) {
            message = ex.getMessage();
            if (message.contains("child record found")) {
                message = "This record can't be deleted";
            } else {
                message = super.showMessage(REC_EXIST);
            }
        }
    }

    public void addTimeShift() {
        newRecord = DTOFactory.createTransactionTypeMasterDTO();
        cdList.add(newRecord);
        showAdd = false;
        showConfirm = true;
        message = "";
        message1 = "";
    }

    public void confirmAdd() {
        try {
            if (newRecord.getName().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                cdObject.editeTransactionTypeMaster(newRecord, cdObject.INSERT);
                message = super.showMessage(SUCC);
                showAdd = true;
                showConfirm = false;
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void fillUpdate1(ActionEvent e) {
        timeShiftDetailsAttr = (TransactionTypeDTOInter) e.getComponent().getAttributes().get("updateRow1");
        message1 = "";
        message = "";
    }

    public void updateRecord1() {
        try {
            if (!showAdd1) {
                message1 = super.showMessage(CONF_FIRST);
            } else {
                if (timeShiftDetailsAttr.getName().equals("")) {
                    message1 = super.showMessage(REQ_FIELD);
                } else {
                    cdObject.editeTransactionType(timeShiftDetailsAttr, cdObject.UPDATE);
                    message1 = super.showMessage(SUCC);
                }

            }
        } catch (Throwable ex) {
            message1 = super.showMessage(REC_EXIST);
        }
    }

    public void fillDelete1(ActionEvent e) {
        timeShiftDetailsAttr1 = (TransactionTypeDTOInter) e.getComponent().getAttributes().get("removedRow1");
        message1 = "";
        message = "";
    }

    public void deleteRecord1() throws Throwable{
        
            cdObject.editeTransactionType(timeShiftDetailsAttr1, cdObject.DELETE);
            cdDetailsList.remove(timeShiftDetailsAttr1);
            message1 = super.showMessage(SUCC);
            showAdd1 = true;
            showConfirm1 = false;
       
    }

    public void addTimeShift1() {
        newRecord1 = DTOFactory.createTransactionTypeDTO();
        newRecord1.setTypeCategory(1);
        cdDetailsList.add(newRecord1);
        showAdd1 = false;
        showConfirm1 = true;
        message1 = "";
        message = "";
    }

    public void confirmAdd1() {
        try {
            if (newRecord1.getName().equals("")) {
                message1 = super.showMessage(REQ_FIELD);
            } else {
                newRecord1.setTypeMaster(selectedMT.getId());
                cdObject.editeTransactionType(newRecord1, cdObject.INSERT);
                message1 = super.showMessage(SUCC);
                showAdd1 = true;
                showConfirm1 = false;
            }
        } catch (Throwable ex) {
            message1 = super.showMessage(REC_EXIST);
        }
    }
    public void resetVars() {
        message1= "";
        message = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
