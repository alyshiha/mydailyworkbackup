/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.JobMonitorBOInter;

import com.ev.AtmBingo.bus.dto.ValidationMonitorDTOInter;
import com.ev.AtmBingo.bus.dto.ErrorsMonitorDTOInter;
import com.ev.AtmBingo.bus.dto.FilesToMoveDTOInter;
import com.ev.AtmBingo.bus.dto.LoadingMonitorDTOInter;
import com.ev.AtmBingo.bus.dto.MatchingMonitorDTOInter;
import com.ev.AtmBingo.bus.dto.RunningGraphDTOInter;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class JobMonitorClient extends BaseBean  implements Serializable{

    private List<RunningGraphDTOInter[]> dynamicRG;
    private JobMonitorBOInter jmObject;
    private List<RunningGraphDTOInter> rgDTO;
    private LoadingMonitorDTOInter lmDTO;
    private ErrorsMonitorDTOInter emDTO;
    private ValidationMonitorDTOInter vmDTO;
    private MatchingMonitorDTOInter mmDTO;
    private List<FilesToMoveDTOInter> ftmDTO;
    private List<String> columnHeaders;
    private int activeFile, fileToBackup;
    private Boolean check, check1, check2, check3, check4;

    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public Boolean getCheck1() {
        return check1;
    }

    public void setCheck1(Boolean check1) {
        this.check1 = check1;
    }

    public Boolean getCheck2() {
        return check2;
    }

    public void setCheck2(Boolean check2) {
        this.check2 = check2;
    }

    public Boolean getCheck3() {
        return check3;
    }

    public void setCheck3(Boolean check3) {
        this.check3 = check3;
    }

    public Boolean getCheck4() {
        return check4;
    }

    public void setCheck4(Boolean check4) {
        this.check4 = check4;
    }

    public List<RunningGraphDTOInter[]> getDynamicRG() {
        return dynamicRG;
    }

    public void setDynamicRG(List<RunningGraphDTOInter[]> dynamicRG) {
        this.dynamicRG = dynamicRG;
    }

    public List<String> getColumnHeaders() {
        return columnHeaders;
    }

    public void setColumnHeaders(List<String> columnHeaders) {
        this.columnHeaders = columnHeaders;
    }

    public List<FilesToMoveDTOInter> getFtmDTO() {
        return ftmDTO;
    }

    public void setFtmDTO(List<FilesToMoveDTOInter> ftmDTO) {
        this.ftmDTO = ftmDTO;
    }

    public List<RunningGraphDTOInter> getRgDTO() {
        return rgDTO;
    }

    public void setRgDTO(List<RunningGraphDTOInter> rgDTO) {
        this.rgDTO = rgDTO;
    }

    public MatchingMonitorDTOInter getMmDTO() {
        return mmDTO;
    }

    public void setMmDTO(MatchingMonitorDTOInter mmDTO) {
        this.mmDTO = mmDTO;
    }

    public ErrorsMonitorDTOInter getEmDTO() {
        return emDTO;
    }

    public void setEmDTO(ErrorsMonitorDTOInter emDTO) {
        this.emDTO = emDTO;
    }

    public LoadingMonitorDTOInter getLmDTO() {
        return lmDTO;
    }

    public void setLmDTO(LoadingMonitorDTOInter lmDTO) {
        this.lmDTO = lmDTO;
    }

    public int getActiveFile() {
        return activeFile;
    }

    public void setActiveFile(int activeFile) {
        this.activeFile = activeFile;
    }

    public int getFileToBackup() {
        return fileToBackup;
    }

    public void setFileToBackup(int fileToBackup) {
        this.fileToBackup = fileToBackup;
    }

    public ValidationMonitorDTOInter getVmDTO() {
        return vmDTO;
    }

    public void setVmDTO(ValidationMonitorDTOInter vmDTO) {
        this.vmDTO = vmDTO;
    }

    /** Creates a new instance of JobMonitorClient */
    public JobMonitorClient() throws Throwable {
        super();
        super.GetAccessPage();
        dynamicRG = new ArrayList<RunningGraphDTOInter[]>();
        ftmDTO = new ArrayList<FilesToMoveDTOInter>();

        columnHeaders = new ArrayList<String>();
        jmObject = BOFactory.createJobMonitorBO(null);
        activeFile = jmObject.getActiveFiles();
        fileToBackup = jmObject.getFilesToBuckup();
      //  rgDTO = (List<RunningGraphDTOInter>) jmObject.getRunningGraph();
        columnHeaders.add("Loading");
        columnHeaders.add("Journal Validation");
        columnHeaders.add("Switch Validation");
        columnHeaders.add("Host Validation");
        columnHeaders.add("Matching");

        check = jmObject.getStatus(1);
        check1 = jmObject.getStatus(2);
        check2 = jmObject.getStatus(3);
        check3 = jmObject.getStatus(4);
        check4 = jmObject.getStatus(5);
     FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        
    }

    public void checkChange(ValueChangeEvent e) throws Throwable {
        check = (Boolean) e.getNewValue();
        jmObject.disableJob(check, 1);
    }

    public void checkChange1(ValueChangeEvent e) throws Throwable {
        check1 = (Boolean) e.getNewValue();
        jmObject.disableJob(check1, 2);
    }

    public void checkChange2(ValueChangeEvent e) throws Throwable {
        check2 = (Boolean) e.getNewValue();
        jmObject.disableJob(check2, 3);
    }

    public void checkChange3(ValueChangeEvent e) throws Throwable {
        check3 = (Boolean) e.getNewValue();
        jmObject.disableJob(check3, 4);
    }

    public void checkChange4(ValueChangeEvent e) throws Throwable {
        check4 = (Boolean) e.getNewValue();
        jmObject.disableJob(check4, 5);
    }

    public void refreshValues() throws Throwable {
        ftmDTO = (List<FilesToMoveDTOInter>) jmObject.getFilesToMove();
        lmDTO = (LoadingMonitorDTOInter) jmObject.getLoadingMonitor();
        emDTO = (ErrorsMonitorDTOInter) jmObject.getErrorMonitor();
        vmDTO = (ValidationMonitorDTOInter) jmObject.getValidationMonitor();
        mmDTO = (MatchingMonitorDTOInter) jmObject.getMatchingMonitor();
        activeFile = jmObject.getActiveFiles();
        fileToBackup = jmObject.getFilesToBuckup();
 
    }

    public void rematchChart() {
        try {
            jmObject.rematch();
        } catch (Throwable ex) {
            
        }
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
