/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.AtmGroupBOInter;
import com.ev.AtmBingo.bus.bo.AutoRepAtmToGroupBoInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AutoRepDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author Administrator
 */
public class RepGroupAssignClient extends BaseBean  implements Serializable{

    private AutoRepAtmToGroupBoInter myObject;
    private AtmGroupBOInter atmObject;
    private List<String> sourceList, targetList;
    private String[] selectedSource, selectedTarget;
    private List<AutoRepDTOInter> groupList;
    private List<AtmGroupDTOInter> atmList;
    private Integer group1, group2, status, group3;
    private String message, selectText, selectText1;
    private Boolean atmFlag, repFlag;

    public List<AtmGroupDTOInter> getAtmList() {
        return atmList;
    }

    public void setAtmList(List<AtmGroupDTOInter> atmList) {
        this.atmList = atmList;
    }

    public Boolean getAtmFlag() {
        return atmFlag;
    }

    public void setAtmFlag(Boolean atmFlag) {
        this.atmFlag = atmFlag;
    }

    public Boolean getRepFlag() {
        return repFlag;
    }

    public void setRepFlag(Boolean repFlag) {
        this.repFlag = repFlag;
    }

    public Integer getGroup3() {
        return group3;
    }

    public void setGroup3(Integer group3) {
        this.group3 = group3;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSelectText() {
        return selectText;
    }

    public void setSelectText(String selectText) {
        this.selectText = selectText;
    }

    public String getSelectText1() {
        return selectText1;
    }

    public void setSelectText1(String selectText1) {
        this.selectText1 = selectText1;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String[] getSelectedSource() {
        return selectedSource;
    }

    public void setSelectedSource(String[] selectedSource) {
        this.selectedSource = selectedSource;
    }

    public String[] getSelectedTarget() {
        return selectedTarget;
    }

    public void setSelectedTarget(String[] selectedTarget) {
        this.selectedTarget = selectedTarget;
    }

    public Integer getGroup1() {
        return group1;
    }

    public void setGroup1(Integer group1) {
        this.group1 = group1;
    }

    public Integer getGroup2() {
        return group2;
    }

    public void setGroup2(Integer group2) {
        this.group2 = group2;
    }

    public List<AutoRepDTOInter> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<AutoRepDTOInter> groupList) {
        this.groupList = groupList;
    }

    public List<String> getSourceList() {
        return sourceList;
    }

    public void setSourceList(List<String> sourceList) {
        this.sourceList = sourceList;
    }

    public List<String> getTargetList() {
        return targetList;
    }

    public void setTargetList(List<String> targetList) {
        this.targetList = targetList;
    }

    /** Creates a new instance of RepGroupAssignClient */
    public RepGroupAssignClient() throws Throwable {
        super();
super.GetAccessPage();
        myObject = BOFactory.createAutoRepAtmToGroupBo(null);
        atmObject = BOFactory.createAtmGroupBO(null);
        groupList = (List<AutoRepDTOInter>) myObject.getGroups();
        atmList = (List<AtmGroupDTOInter>) atmObject.getAllGroups();
        group1 = -1;
        targetList = new ArrayList<String>();
        sourceList = new ArrayList<String>();
        selectText = "Select all records";
        selectText1 = "Select all records";
        group2 = 0;
        status = 1;
        atmFlag = false;
        repFlag = true;
        sourceList = (List<String>) myObject.getTargetList2(super.getLoggedInUser().getUserId(), group1, status, group2);
     FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void statusChange(ValueChangeEvent e) {
        status = (Integer) e.getNewValue();
        if (status == 0) {
            atmFlag = true;
            repFlag = false;
            group1 = 0;
        } else {
            atmFlag = false;
            repFlag = true;
            group2 = 0;
        }
        sourceList.clear();
    }

    public void userChange(ValueChangeEvent e) throws Throwable {


        group1 = (Integer) e.getNewValue();
        if (group1 == -2) {
            sourceList.clear();
        } else {
            sourceList = (List<String>) myObject.getTargetList2(super.getLoggedInUser().getUserId(), group1, status, group2);
        }
        selectText = "Select all records";
        selectText1 = "Select all records";
        message = "";
    }

    public void userChange1(ValueChangeEvent e) throws Throwable {
        group2 = (Integer) e.getNewValue();
        if (group2 == 0) {
            sourceList.clear();
        } else {
            sourceList = (List<String>) myObject.getTargetList(super.getLoggedInUser().getUserId(), group2);
        }
        selectText = "Select all records";
        selectText1 = "Select all records";
        message = "";
    }

    public void userChange2(ValueChangeEvent e) throws Throwable {
        group3 = (Integer) e.getNewValue();
        if (group1 == 0 && group2 == 0) {
            message = "Select left side first";
            group3 = 0;
        } else {
            if (group3 == 0) {
                targetList.clear();
                message = "";
            } else if (group2 == group3) {
                message = "Can't select the same group";
                group3 = 0;
            } else {
                targetList = (List<String>) myObject.getTargetList(super.getLoggedInUser().getUserId(), group3);
                message = "";
            }
            selectText = "Select all records";
            selectText1 = "Select all records";
        }

    }

    public void saveList() {
        try {
//            if (group1 == -2 || group2 == 0) {
//                message = "Choose the groups first";
//            } else {

            myObject.saveGroup(group1, selectedSource, group3, selectedTarget);
            myObject.unsaveGroup(group1, selectedTarget, group3, selectedSource);
            targetList = (List<String>) myObject.getTargetList(super.getLoggedInUser().getUserId(), group3);
            sourceList = (List<String>) myObject.getTargetList2(super.getLoggedInUser().getUserId(), group1, status, group2);
            selectedSource = null;
            selectedTarget = null;
            selectText = "Select all records";
            selectText1 = "Select all records";
            message = super.showMessage(SUCC);
            // }
        } catch (Throwable ex) {
        }
    }

    public void resetVars() {
        message = "";
    }

    public void selectTargetTable() {
        resetVars();
        if (targetList.size() != 0) {
            DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:blockReason1");
            if (selectText.equals("Select all records")) {
                repTable.setSelection(targetList.toArray());
                selectText = "Deselect all records";
            } else {
                selectedTarget = null;
                selectText = "Select all records";
            }
        }
    }

    public void selectSourceTable() {
        resetVars();
        if (sourceList.size() != 0) {
            DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:blockReason");
            if (selectText1.equals("Select all records")) {
                repTable.setSelection(sourceList.toArray());
                selectText1 = "Deselect all records";
            } else {
                selectedSource = null;
                selectText1 = "Select all records";
            }
        }
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
