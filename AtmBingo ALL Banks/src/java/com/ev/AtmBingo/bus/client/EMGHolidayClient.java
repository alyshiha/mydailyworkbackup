/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.EmergencyHolidaysBOInter;
import com.ev.AtmBingo.bus.dto.EmgHolidaysDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import javax.faces.event.ActionEvent;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class EMGHolidayClient extends BaseBean  implements Serializable{

    private List<EmgHolidaysDTOInter> EMGList;
    private EmergencyHolidaysBOInter EMGObject;
    private EmgHolidaysDTOInter EMGAttr;
    private EmgHolidaysDTOInter newEMG;
    private String message;
    private Boolean showAdd;
    private Boolean showConfirm;
    private Date oldDate;
    private Date oldpDate;
    private String reasonArabic;
    private String reasonEnglish;
    private Integer listSize;

    // <editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public List<EmgHolidaysDTOInter> getEMGList() {
        return EMGList;
    }

    public void setEMGList(List<EmgHolidaysDTOInter> EMGList) {
        this.EMGList = EMGList;
    }

    public String getReasonArabic() {
        return reasonArabic;
    }

    public void setReasonArabic(String reasonArabic) {
        this.reasonArabic = reasonArabic;
    }

    public String getReasonEnglish() {
        return reasonEnglish;
    }

    public void setReasonEnglish(String reasonEnglish) {
        this.reasonEnglish = reasonEnglish;
    }

    public Date getOldpDate() {
        return oldpDate;
    }

    public void setOldpDate(Date oldpDate) {
        this.oldpDate = oldpDate;
    }

    public Date getOldDate() {
        return oldDate;
    }

    public void setOldDate(Date oldDate) {
        this.oldDate = oldDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public EmgHolidaysDTOInter getNewEMG() {
        return newEMG;
    }

    public void setNewEMG(EmgHolidaysDTOInter newEMG) {
        this.newEMG = newEMG;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }
    // </editor-fold>

    /** Creates a new instance of EMGHolidayClient */
    public EMGHolidayClient() throws Throwable {
        super();
        super.GetAccessPage();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        EMGObject = BOFactory.createEmergencyHolidaysBO(null);
        EMGList = (List<EmgHolidaysDTOInter>) EMGObject.getEmgHolidays();
        showAdd = true;
        showConfirm = false;
        listSize = EMGList.size();
    }

    public void fillDel(ActionEvent e) {
        EMGAttr = (EmgHolidaysDTOInter) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void deleteEMGHolidays(ActionEvent e) throws Throwable {
        try {
            if (EMGAttr.getReason() == "" || EMGAttr.geteReason() == "") {
                message = super.showMessage(SUCC);
                EMGList.remove(EMGAttr);
            } else {
                EMGList.remove(EMGAttr);
                EMGObject.editeEmgHolidays(EMGAttr, oldDate, EMGObject.DELETE);
                message = super.showMessage(SUCC);
            }
            showAdd = true;
            showConfirm = false;
        } catch (Throwable ex) {
            message = "Error, please delete again";

        }
    }

    public void fillUp(ActionEvent e) {
        EMGAttr = (EmgHolidaysDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    public void updateEMGHolidays(ActionEvent e) throws Throwable {

        if (showAdd == false) {
            message = super.showMessage(CONF_FIRST);
        } else if (EMGAttr.getReason() == "" || EMGAttr.geteReason() == "") {
            message = super.showMessage(REQ_FIELD);
            if (EMGAttr.getReason() == "") {
                EMGAttr.setReason(reasonArabic);
            }
            if (EMGAttr.geteReason() == "") {
                EMGAttr.seteReason(reasonEnglish);
            }
        } else if (!EMGObject.checkDate(EMGAttr.getpFrom(), EMGAttr.getpTo())) {
            message = super.showMessage(CHECK_DATE);
            if (oldDate == null) {
                EMGAttr.setpTo(oldpDate);
            }
            if (oldpDate == null) {
                EMGAttr.setpFrom(oldDate);
            }
            if (oldDate != null && oldpDate != null) {
                EMGAttr.setpTo(oldpDate);
                EMGAttr.setpFrom(oldDate);
            }
        } else {
            if (oldDate == null) {
                oldDate = EMGAttr.getpFrom();
            }
            EMGObject.editeEmgHolidays(EMGAttr, oldDate, EMGObject.UPDATE);
            message = super.showMessage(SUCC);
        }

    }

    public void resetVars() {
        message = "";
    }

    public void AddEMGHolidays() {
        if (EMGList == null) {
            EMGList = new ArrayList<EmgHolidaysDTOInter>();
        }
        newEMG = DTOFactory.createEmgHolidaysDTO();
        EMGList.add(newEMG);
        showAdd = false;
        showConfirm = true;
        resetVars();

    }

    public void ConfirmBlockReason() {
        try {
            if (newEMG.getpFrom() == null || newEMG.getpTo() == null || newEMG.getReason() == ""
                    || newEMG.geteReason() == "") {
                message = super.showMessage(REQ_FIELD);
            } else if (!EMGObject.checkDate(newEMG.getpFrom(), newEMG.getpTo())) {
                message = super.showMessage(CHECK_DATE);
            } else {
                EMGObject.editeEmgHolidays(newEMG, oldDate, EMGObject.INSERT);
                message = super.showMessage(SUCC);
                showAdd = true;
                showConfirm = false;
            }
        } catch (Throwable ex) {
            // message = "Your record can't be added, please try again";
            message = super.showMessage(REC_EXIST);
        }
    }

    public void getOldFDate(ValueChangeEvent e) {
        oldDate = (Date) e.getOldValue();
    }

    public void getOldPDate(ValueChangeEvent e) {
        oldpDate = (Date) e.getOldValue();
    }

    public void getOldEnglish(ValueChangeEvent e) {
        reasonEnglish = (String) e.getOldValue();
    }

    public void getOldArabic(ValueChangeEvent e) {
        reasonArabic = (String) e.getOldValue();
    }

    public void returnOldValues(ActionEvent e) {
        if (oldDate != null) {
            EMGAttr.setpFrom(oldDate);
        }
        if (oldpDate != null) {
            EMGAttr.setpTo(oldpDate);
        }
        if (oldDate != null && oldpDate != null) {
            EMGAttr.setpTo(oldpDate);
            EMGAttr.setpFrom(oldDate);
        }
        if (reasonArabic != null) {
            EMGAttr.setReason(reasonArabic);
        }

        if (reasonEnglish != null) {
            EMGAttr.seteReason(reasonEnglish);
        }
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
