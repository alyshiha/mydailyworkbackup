/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.ProfileRestBOInter;
import com.ev.AtmBingo.bus.dto.ProfileDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author Administrator
 */
public class ProfileRestClient extends BaseBean  implements Serializable{

    private List<ProfileDTOInter> pDTOL;
    private ProfileDTOInter selectedProfile;
    private ProfileRestBOInter prBO;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProfileDTOInter> getpDTOL() {
        return pDTOL;
    }

    public void setpDTOL(List<ProfileDTOInter> pDTOL) {
        this.pDTOL = pDTOL;
    }

    public ProfileDTOInter getSelectedProfile() {
        return selectedProfile;
    }

    public void setSelectedProfile(ProfileDTOInter selectedProfile) {
        this.selectedProfile = selectedProfile;
    }

    /** Creates a new instance of ProfileRestClient */
    public ProfileRestClient() {
        super();
        super.GetAccessPage();
        try {
            prBO = BOFactory.createProfileRestBO(null);
            pDTOL = (List<ProfileDTOInter>) prBO.getProfiles(super.getRestrected());
                 FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        } catch (Throwable ex) {
            Logger.getLogger(ProfileRestClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void fillUp(ActionEvent e){
        selectedProfile = (ProfileDTOInter) e.getComponent().getAttributes().get("updateRow");
        message = "";
    }

    public void update(){
        try {
            prBO.update(selectedProfile);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            Logger.getLogger(ProfileRestClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
