/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.SettlementLogBOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.SettlementLogDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class SettelmentLogClient extends BaseBean  implements Serializable{

    private List<AtmMachineDTOInter> atmList;
    private Date fromDate, toDate;
    private Integer userId, setteldId, ATMID;
    private SettlementLogBOInter bingoBo;
    private List<UsersDTOInter> userList;
    private List<SettlementLogDTOInter> logList;
    private String message;
    private String cardNo;

    public String getCardno() {
        return cardNo;
    }

    public void setCardno(String cardNo) {
        this.cardNo = cardNo;
    }

    public Integer getATMID() {
        return ATMID;
    }

    public void setATMID(Integer ATMID) {
        this.ATMID = ATMID;
    }

    public List<AtmMachineDTOInter> getAtmList() {
        return atmList;
    }

    public void setAtmList(List<AtmMachineDTOInter> atmList) {
        this.atmList = atmList;
    }

    public Integer getSetteldId() {
        return setteldId;
    }

    public void setSetteldId(Integer setteldId) {
        this.setteldId = setteldId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public List<SettlementLogDTOInter> getLogList() {
        return logList;
    }

    public void setLogList(List<SettlementLogDTOInter> logList) {
        this.logList = logList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<UsersDTOInter> getUserList() {
        return userList;
    }

    public void setUserList(List<UsersDTOInter> userList) {
        this.userList = userList;
    }

    /**
     * Creates a new instance of SettelmentLogClient
     */
    public SettelmentLogClient() throws Throwable {
        super();
        super.GetAccessPage();
        FacesContext context = FacesContext.getCurrentInstance();
         

        Session sessionutil = new Session();
        UsersDTOInter userHolded = sessionutil.GetUserLogging();
        bingoBo = BOFactory.createSettlementLogBO(null);
        atmList = (List<AtmMachineDTOInter>) bingoBo.getAtmMachines(userHolded);
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        userList = (List<UsersDTOInter>) bingoBo.getUsers(super.getRestrected());
    }

    public void doSearch() {
        try {
            String[] inputs = {fromDate.toString(), toDate.toString(), "" + userId, "" + setteldId, cardNo, "" + ATMID};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace(":", "").replace("#", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
                logList = (List<SettlementLogDTOInter>) bingoBo.getSettlementLogged(fromDate, toDate, userId, setteldId, cardNo, ATMID);
                resetVars();
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
            message = "Please Insert Required Fields";
        }
    }

    public void resetVars() {
        message = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
