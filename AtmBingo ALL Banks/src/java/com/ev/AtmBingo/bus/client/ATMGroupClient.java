/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.AtmGroupBOInter;
import com.ev.AtmBingo.bus.bo.AtmGroupEditorBoInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UserAtmDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.TreeNode;

/**
 *
 * @author ISLAM
 */
public class ATMGroupClient extends BaseBean implements Serializable {

    private AtmGroupBOInter atmgObject;
    private AtmGroupDTOInter newAtmg, atmgDeleteAttr, atmgUpdateAttr;
    private TreeNode atmgList;
    private TreeNode selectedNode;
    private List<AtmGroupDTOInter> agDTOL;
    private Boolean showAdd, showConfirm, disabledSave;
    private String message;
    private AtmGroupEditorBoInter mcObject;
    private List<UserAtmDTOInter> uatmList;
    private List<String> targetList, sourceList;
    private List<AtmGroupDTOInter> userList;
    private DualListModel<String> pickValues;
    private Integer atmGInt, userInt;
    private String message1;

    public Integer getAtmGInt() {
        return atmGInt;
    }

    public void setAtmGInt(Integer atmGInt) {
        this.atmGInt = atmGInt;
    }

    public Boolean getDisabledSave() {
        return disabledSave;
    }

    public void setDisabledSave(Boolean disabledSave) {
        this.disabledSave = disabledSave;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public DualListModel<String> getPickValues() {
        return pickValues;
    }

    public void setPickValues(DualListModel<String> pickValues) {
        this.pickValues = pickValues;
    }

    public List<String> getSourceList() {
        return sourceList;
    }

    public void setSourceList(List<String> sourceList) {
        this.sourceList = sourceList;
    }

    public List<String> getTargetList() {
        return targetList;
    }

    public void setTargetList(List<String> targetList) {
        this.targetList = targetList;
    }

    public List<UserAtmDTOInter> getUatmList() {
        return uatmList;
    }

    public void setUatmList(List<UserAtmDTOInter> uatmList) {
        this.uatmList = uatmList;
    }

    public Integer getUserInt() {
        return userInt;
    }

    public void setUserInt(Integer userInt) {
        this.userInt = userInt;
    }

    public List<AtmGroupDTOInter> getUserList() {
        return userList;
    }

    public void setUserList(List<AtmGroupDTOInter> userList) {
        this.userList = userList;
    }

    public AtmGroupDTOInter getAtmgDeleteAttr() {
        return atmgDeleteAttr;
    }

    public void setAtmgDeleteAttr(AtmGroupDTOInter atmgDeleteAttr) {
        this.atmgDeleteAttr = atmgDeleteAttr;
    }

    public AtmGroupDTOInter getAtmgUpdateAttr() {
        return atmgUpdateAttr;
    }

    public void setAtmgUpdateAttr(AtmGroupDTOInter atmgUpdateAttr) {
        this.atmgUpdateAttr = atmgUpdateAttr;
    }

    public AtmGroupDTOInter getNewAtmg() {
        return newAtmg;
    }

    public void setNewAtmg(AtmGroupDTOInter newAtmg) {
        this.newAtmg = newAtmg;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public List<AtmGroupDTOInter> getAgDTOL() {
        return agDTOL;
    }

    public void setAgDTOL(List<AtmGroupDTOInter> agDTOL) {
        this.agDTOL = agDTOL;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public TreeNode getAtmgList() {
        return atmgList;
    }

    public void setAtmgList(TreeNode atmgList) {
        this.atmgList = atmgList;
    }
    private Session utillist = new Session();

    /**
     * Creates a new instance of ATMGroupClient
     */
    public ATMGroupClient() throws Throwable {
        super();
        super.GetAccessPage();
        atmgObject = BOFactory.createAtmGroupBO(null);
        atmgList = atmgObject.getRoot();
        showAdd = true;
        showConfirm = false;

        mcObject = BOFactory.AtmGroupEditorBoInter(null);
        userList = (List<AtmGroupDTOInter>) utillist.GetATMGroupList();
        targetList = new ArrayList<String>();
        sourceList = new ArrayList<String>();
        pickValues = new DualListModel<String>(targetList, sourceList);
        disabledSave = true;
        FacesContext context = FacesContext.getCurrentInstance();

        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void userChange(ValueChangeEvent e) throws Throwable {
        resetVars();
        userInt = (Integer) e.getNewValue();
        if (userInt == 0) {
            disabledSave = true;
            targetList.clear();
        } else {
            if (atmGInt == null || atmGInt == 0) {
                atmGInt = 0;
                disabledSave = true;
                targetList = (List<String>) mcObject.getAtmGroup(userInt);
                pickValues = new DualListModel<String>(targetList, sourceList);
            } else {
                if (userInt == atmGInt) {
                    message = "Select another group";
                    targetList.clear();
                    disabledSave = true;
                    userInt = 0;
                } else {
                    disabledSave = false;
                    targetList = (List<String>) mcObject.getAtmGroup(userInt);
                    pickValues = new DualListModel<String>(targetList, sourceList);
                }
            }

        }
    }

    public void userChange1(ValueChangeEvent e) throws Throwable {
        resetVars();
        atmGInt = (Integer) e.getNewValue();
        if (atmGInt == 0) {
            disabledSave = true;
            sourceList.clear();
        } else {
            if (userInt == null || userInt == 0) {
                userInt = 0;
                disabledSave = true;
                sourceList = (List<String>) mcObject.getAtmGroup(atmGInt);
                pickValues = new DualListModel<String>(targetList, sourceList);
            } else {
                if (userInt == atmGInt) {
                    message = "Select another group";
                    sourceList.clear();
                    disabledSave = true;
                    atmGInt = 0;
                } else {
                    disabledSave = false;
                    sourceList = (List<String>) mcObject.getAtmGroup(atmGInt);
                    pickValues = new DualListModel<String>(targetList, sourceList);
                }
            }
        }
    }

    public void saveChange() throws Throwable {
        targetList = pickValues.getSource();
        sourceList = pickValues.getTarget();
        mcObject.saveUserAtm(userInt, targetList);
        mcObject.saveUserAtm(atmGInt, sourceList);
        message1 = super.showMessage(SUCC);
    }

    public void selectNodeShow(NodeSelectEvent w) throws Throwable {
        String name = (String) w.getTreeNode().getData();
        agDTOL = (List<AtmGroupDTOInter>) atmgObject.getChildren(name);
        resetVars();
        showAdd = true;
        showConfirm = false;
    }

    public void addRecord() {
        try {
            newAtmg = DTOFactory.createAtmGroupDTO();
            agDTOL.add(newAtmg);
            showConfirm = true;
            showAdd = false;
            resetVars();
        } catch (Throwable ex) {
            message = "Select record";
        }
    }

    public String TestColName(String ColName) {
        String MSG = "";
        int j = 0;
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getName().equals(ColName)) {
                    return "Found";
            }
        }
        return "Not Found";
    }

    public void confirmRecord() {
        try {
            if (agDTOL == null) {
                agDTOL = new ArrayList<AtmGroupDTOInter>();
            }
            if (newAtmg.getName().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                if ("Not Found".equals(TestColName(newAtmg.getName()))) {
                    atmgObject.editeAtmGroup(newAtmg, (String) selectedNode.getData(), atmgObject.INSERT);
                    message = super.showMessage(SUCC);
                    showAdd = true;
                    showConfirm = false;
                    TreeNode t = new DefaultTreeNode(newAtmg.getName(), selectedNode);
                } else {
                    message = "Name Already Exists";
                }
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void fillDelete(ActionEvent e) {
        atmgDeleteAttr = (AtmGroupDTOInter) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void deleteRecord() {
        try {
            atmgObject.editeAtmGroup(atmgDeleteAttr, (String) selectedNode.getData(), atmgObject.DELETE);
            agDTOL.remove(atmgDeleteAttr);
            message = super.showMessage(SUCC);
            showAdd = true;
            showConfirm = false;
        } catch (Throwable ex) {
            message = "Delete failed, group has ATMs";
        }
    }

    public void fillUpdate(ActionEvent e) {
        atmgUpdateAttr = (AtmGroupDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    public void updateRecord() {
        try {
            if (atmgUpdateAttr.getName().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else if (showAdd == false) {
                message = super.showMessage(CONF_FIRST);
            } else {
                if ("Not Found".equals(TestColName(atmgUpdateAttr.getName()))) {
                    atmgObject.editeAtmGroup(atmgUpdateAttr, (String) selectedNode.getData(), atmgObject.UPDATE);
                    message = super.showMessage(SUCC);
                } else {
                    message = "Name Already Exists";
                }
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void resetVars() {
        message1 = "";
        message = "";
    }

    public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();

    }
}
