/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.AtmGroupEditorBoInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.DisputesBOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.bo.ReplanishmentDetailsBOInter;
import com.ev.AtmBingo.bus.bo.ReplanishmentReportBOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.DepositDTOInter;
import com.ev.AtmBingo.bus.dto.RepPopDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentDetailDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTO;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.repreportexcelDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author ISLAM
 */
public class ReplanishmentClient extends BaseBean implements Serializable {

    private TimeZone timeZone;
    private DisputesBOInter mcObject;
    private List<AtmGroupDTOInter> groupList;
    private AtmGroupEditorBoInter myObject;
    private Integer group2;
    private UsersDTOInter userHolded;
    private Session utillist = new Session();
    private ReplanishmentDetailsBOInter cdObject;
    private ReplanishmentReportBOInter repDetailsReport;
    private ReplanishmentMasterDTOInter selectedRep;
    private List<ReplanishmentMasterDTOInter> cdList;
    private List<ReplanishmentDetailDTOInter> cdDetailsList;
    private List<DepositDTOInter> depositDetailList;
    private List<AtmMachineDTOInter> atmList;
    private List<AtmMachineDTOInter> atmList2;
    private List<repreportexcelDTOInter> result;
    private List<CassetteDTOInter> cassetteList;
    private int remainingamounttotal;
    private int remainingtotal;
    private int casseteValuetotal;
    private int dispensedcounttotal;
    private int dispensedvaluetotal;
    private int cassettecounttotal;
    private int cassetteCountTotal, cassetteValueTotal2, retractCountTotal, retractValueTotal,
            recycledCountTotal, recycledValueTotal, depositCountTotal, depositValueTotal;
    private ReplanishmentMasterDTOInter timeShiftAttr;
    private ReplanishmentMasterDTOInter timeShiftAttr1;
    private ReplanishmentMasterDTOInter selectedMT;
    private ReplanishmentMasterDTOInter newRecord;
    private ReplanishmentDetailDTOInter newRecord1;
    private ReplanishmentDetailDTOInter timeShiftDetailsAttr, timeShiftDetailsAttr1;
    private Integer myInt, currentPage;
    private List<String> privelageList;
    private Boolean showAdd, showAdd1;
    private Boolean showConfirm, showConfirm1;
    private String message, userNAME, comment, messageDlg;
    private String message1, colorFrom;
    private Date fromDate, toDate, toCreate, fromCreate, toDate_ico, toDate_ico1, fromDate_ico, fromDate_ico1;
    private Integer atmInt, userInt, status, iconCheck;
    private List<UsersDTOInter> repDetailsUsers, createdByUser;
    DataTable repTable;
    private Boolean datetoFlag, datefromFlag, subdatetoFlag, subdatefromFlag, fromOverlay, toOverlay;

    public List<AtmMachineDTOInter> getAtmList2() {
        return atmList2;
    }

    public void setAtmList2(List<AtmMachineDTOInter> atmList2) {
        this.atmList2 = atmList2;
    }

    public List<repreportexcelDTOInter> getResult() {
        return result;
    }

    public void setResult(List<repreportexcelDTOInter> result) {
        this.result = result;
    }

    public String getColorFrom() {
        return colorFrom;
    }

    public List<DepositDTOInter> getDepositDetailList() {
        return depositDetailList;
    }

    public void setDepositDetailList(List<DepositDTOInter> depositDetailList) {
        this.depositDetailList = depositDetailList;
    }

    public List<String> getPrivelageList() {
        return privelageList;
    }

    public void setPrivelageList(List<String> privelageList) {
        this.privelageList = privelageList;
    }

    public void setColorFrom(String colorFrom) {
        this.colorFrom = colorFrom;
    }

    public Boolean getFromOverlay() {
        return fromOverlay;
    }

    public int getCassetteCountTotal() {
        return cassetteCountTotal;
    }

    public void setCassetteCountTotal(int cassetteCountTotal) {
        this.cassetteCountTotal = cassetteCountTotal;
    }

    public int getCassetteValueTotal2() {
        return cassetteValueTotal2;
    }

    public void setCassetteValueTotal2(int cassetteValueTotal2) {
        this.cassetteValueTotal2 = cassetteValueTotal2;
    }

  
    public int getRetractCountTotal() {
        return retractCountTotal;
    }

    public void setRetractCountTotal(int retractCountTotal) {
        this.retractCountTotal = retractCountTotal;
    }

    public int getRetractValueTotal() {
        return retractValueTotal;
    }

    public void setRetractValueTotal(int retractValueTotal) {
        this.retractValueTotal = retractValueTotal;
    }

    public int getRecycledCountTotal() {
        return recycledCountTotal;
    }

    public void setRecycledCountTotal(int recycledCountTotal) {
        this.recycledCountTotal = recycledCountTotal;
    }

    public int getRecycledValueTotal() {
        return recycledValueTotal;
    }

    public void setRecycledValueTotal(int recycledValueTotal) {
        this.recycledValueTotal = recycledValueTotal;
    }

    public int getDepositCountTotal() {
        return depositCountTotal;
    }

    public void setDepositCountTotal(int depositCountTotal) {
        this.depositCountTotal = depositCountTotal;
    }

    public int getDepositValueTotal() {
        return depositValueTotal;
    }

    public void setDepositValueTotal(int depositValueTotal) {
        this.depositValueTotal = depositValueTotal;
    }

    public void setFromOverlay(Boolean fromOverlay) {
        this.fromOverlay = fromOverlay;
    }

    public Boolean getToOverlay() {
        return toOverlay;
    }

    public void setToOverlay(Boolean toOverlay) {
        this.toOverlay = toOverlay;
    }

    public Boolean getDatefromFlag() {
        return datefromFlag;
    }

    public void setDatefromFlag(Boolean datefromFlag) {
        this.datefromFlag = datefromFlag;
    }

    public Boolean getDatetoFlag() {
        return datetoFlag;
    }

    public void setDatetoFlag(Boolean datetoFlag) {
        this.datetoFlag = datetoFlag;
    }

    public Boolean getSubdatefromFlag() {
        return subdatefromFlag;
    }

    public void setSubdatefromFlag(Boolean subdatefromFlag) {
        this.subdatefromFlag = subdatefromFlag;
    }

    public Boolean getSubdatetoFlag() {
        return subdatetoFlag;
    }

    public void setSubdatetoFlag(Boolean subdatetoFlag) {
        this.subdatetoFlag = subdatetoFlag;
    }

    public Integer getIconCheck() {
        return iconCheck;
    }

    public void setIconCheck(Integer iconCheck) {
        this.iconCheck = iconCheck;
    }

    public Date getToDate_ico() {
        return toDate_ico;
    }

    public void setToDate_ico(Date toDate_ico) {
        this.toDate_ico = toDate_ico;
    }

    public Date getFromDate_ico() {
        return fromDate_ico;
    }

    public void setFromDate_ico(Date fromDate_ico) {
        this.fromDate_ico = fromDate_ico;
    }

    public Date getFromDate_ico1() {
        return fromDate_ico1;
    }

    public void setFromDate_ico1(Date fromDate_ico1) {
        this.fromDate_ico1 = fromDate_ico1;
    }

    public Date getToDate_ico1() {
        return toDate_ico1;
    }

    public void setToDate_ico1(Date toDate_ico1) {
        this.toDate_ico1 = toDate_ico1;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessageDlg() {
        return messageDlg;
    }

    public void setMessageDlg(String messageDlg) {
        this.messageDlg = messageDlg;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<UsersDTOInter> getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(List<UsersDTOInter> createdByUser) {
        this.createdByUser = createdByUser;
    }

    public List<UsersDTOInter> getRepDetailsUsers() {
        return repDetailsUsers;
    }

    public void setRepDetailsUsers(List<UsersDTOInter> repDetailsUsers) {
        this.repDetailsUsers = repDetailsUsers;
    }

    public Integer getAtmInt() {
        return atmInt;
    }

    public void setAtmInt(Integer atmInt) {
        this.atmInt = atmInt;
    }

    public Date getFromCreate() {
        return fromCreate;
    }

    public void setFromCreate(Date fromCreate) {
        this.fromCreate = fromCreate;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToCreate() {
        return toCreate;
    }

    public void setToCreate(Date toCreate) {
        this.toCreate = toCreate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer getUserInt() {
        return userInt;
    }

    public void setUserInt(Integer userInt) {
        this.userInt = userInt;
    }

    public List<CassetteDTOInter> getCassetteList() {
        return cassetteList;
    }

    public void setCassetteList(List<CassetteDTOInter> cassetteList) {
        this.cassetteList = cassetteList;
    }

    public String getUserNAME() {
        return userNAME;
    }

    public void setUserNAME(String userNAME) {
        this.userNAME = userNAME;
    }

    public List<AtmMachineDTOInter> getAtmList() {
        return atmList;
    }

    public void setAtmList(List<AtmMachineDTOInter> atmList) {
        this.atmList = atmList;
    }

    public List<ReplanishmentDetailDTOInter> getCdDetailsList() {
        return cdDetailsList;
    }

    public void setCdDetailsList(List<ReplanishmentDetailDTOInter> cdDetailsList) {
        this.cdDetailsList = cdDetailsList;
    }

    public List<ReplanishmentMasterDTOInter> getCdList() {
        return cdList;
    }

    public void setCdList(List<ReplanishmentMasterDTOInter> cdList) {
        this.cdList = cdList;
    }

    public String getMessage() {
        return message;
    }

    public int getRemainingamounttotal() {
        return remainingamounttotal;
    }

    public void setRemainingamounttotal(int remainingamounttotal) {
        this.remainingamounttotal = remainingamounttotal;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public Integer getMyInt() {
        return myInt;
    }

    public void setMyInt(Integer myInt) {
        this.myInt = myInt;
    }

    public ReplanishmentMasterDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(ReplanishmentMasterDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public ReplanishmentDetailDTOInter getNewRecord1() {
        return newRecord1;
    }

    public void setNewRecord1(ReplanishmentDetailDTOInter newRecord1) {
        this.newRecord1 = newRecord1;
    }

    public ReplanishmentMasterDTOInter getSelectedMT() {
        return selectedMT;
    }

    public void setSelectedMT(ReplanishmentMasterDTOInter selectedMT) {
        this.selectedMT = selectedMT;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public ReplanishmentMasterDTOInter getTimeShiftAttr() {
        return timeShiftAttr;
    }

    public void setTimeShiftAttr(ReplanishmentMasterDTOInter timeShiftAttr) {
        this.timeShiftAttr = timeShiftAttr;
    }

    public ReplanishmentMasterDTOInter getTimeShiftAttr1() {
        return timeShiftAttr1;
    }

    public List<AtmGroupDTOInter> getGroupList() {
        return groupList;
    }

    public Integer getGroup2() {
        return group2;
    }

    public void setGroup2(Integer group2) {
        this.group2 = group2;
    }

    public void setGroupList(List<AtmGroupDTOInter> groupList) {
        this.groupList = groupList;
    }

    public void setTimeShiftAttr1(ReplanishmentMasterDTOInter timeShiftAttr1) {
        this.timeShiftAttr1 = timeShiftAttr1;
    }

    public ReplanishmentDetailDTOInter getTimeShiftDetailsAttr() {
        return timeShiftDetailsAttr;
    }

    public void setTimeShiftDetailsAttr(ReplanishmentDetailDTOInter timeShiftDetailsAttr) {
        this.timeShiftDetailsAttr = timeShiftDetailsAttr;
    }

    public ReplanishmentDetailDTOInter getTimeShiftDetailsAttr1() {
        return timeShiftDetailsAttr1;
    }

    public void setTimeShiftDetailsAttr1(ReplanishmentDetailDTOInter timeShiftDetailsAttr1) {
        this.timeShiftDetailsAttr1 = timeShiftDetailsAttr1;
    }
    /**
     * Creates a new instance of ReplanishmentClient
     */
    private String texttocomp;

    public List<String> complete(String query) {
        List<String> results = new ArrayList<String>();
        for (int i = 0; i < atmList2.size(); i++) {
            if (atmList2.get(i).getApplicationId().toString().startsWith(query)) {
                results.add(atmList2.get(i).getApplicationId().toString());
            }
        }
        return results;
    }

    public String gettexttocomp() {
        return texttocomp;
    }

    public void settexttocomp(String texttocomp) {
        this.texttocomp = texttocomp;
    }
    private Session sessionutil;

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public DisputesBOInter getMcObject() {
        return mcObject;
    }

    public void setMcObject(DisputesBOInter mcObject) {
        this.mcObject = mcObject;
    }

    public AtmGroupEditorBoInter getMyObject() {
        return myObject;
    }

    public void setMyObject(AtmGroupEditorBoInter myObject) {
        this.myObject = myObject;
    }

    public UsersDTOInter getUserHolded() {
        return userHolded;
    }

    public void setUserHolded(UsersDTOInter userHolded) {
        this.userHolded = userHolded;
    }

    public Session getUtillist() {
        return utillist;
    }

    public void setUtillist(Session utillist) {
        this.utillist = utillist;
    }

    public ReplanishmentDetailsBOInter getCdObject() {
        return cdObject;
    }

    public void setCdObject(ReplanishmentDetailsBOInter cdObject) {
        this.cdObject = cdObject;
    }

    public ReplanishmentReportBOInter getRepDetailsReport() {
        return repDetailsReport;
    }

    public void setRepDetailsReport(ReplanishmentReportBOInter repDetailsReport) {
        this.repDetailsReport = repDetailsReport;
    }

    public ReplanishmentMasterDTOInter getSelectedRep() {
        return selectedRep;
    }

    public void setSelectedRep(ReplanishmentMasterDTOInter selectedRep) {
        this.selectedRep = selectedRep;
    }

    public int getRemainingtotal() {
        return remainingtotal;
    }

    public void setRemainingtotal(int remainingtotal) {
        this.remainingtotal = remainingtotal;
    }

    public int getCasseteValuetotal() {
        return casseteValuetotal;
    }

    public void setCasseteValuetotal(int casseteValuetotal) {
        this.casseteValuetotal = casseteValuetotal;
    }

    public int getDispensedcounttotal() {
        return dispensedcounttotal;
    }

    public void setDispensedcounttotal(int dispensedcounttotal) {
        this.dispensedcounttotal = dispensedcounttotal;
    }

    public int getDispensedvaluetotal() {
        return dispensedvaluetotal;
    }

    public void setDispensedvaluetotal(int dispensedvaluetotal) {
        this.dispensedvaluetotal = dispensedvaluetotal;
    }

    public int getCassettecounttotal() {
        return cassettecounttotal;
    }

    public void setCassettecounttotal(int cassettecounttotal) {
        this.cassettecounttotal = cassettecounttotal;
    }

    public DataTable getRepTable() {
        return repTable;
    }

    public void setRepTable(DataTable repTable) {
        this.repTable = repTable;
    }

    public String getTexttocomp() {
        return texttocomp;
    }

    public void setTexttocomp(String texttocomp) {
        this.texttocomp = texttocomp;
    }

    public Session getSessionutil() {
        return sessionutil;
    }

    public void setSessionutil(Session sessionutil) {
        this.sessionutil = sessionutil;
    }

    public ReplanishmentClient() throws Throwable {
        //GetAccess();
        super.GetAccessPage();
        remainingtotal = 0;
        casseteValuetotal = 0;
        dispensedcounttotal = 0;
        dispensedvaluetotal = 0;
        cassettecounttotal = 0;
        remainingamounttotal = 0;
        cassetteCountTotal = 0;
        cassetteValueTotal2 = 0;
        retractCountTotal = 0;
        retractValueTotal = 0;
        recycledCountTotal = 0;
        recycledValueTotal = 0;
        depositCountTotal = 0;
        depositValueTotal = 0;
        sessionutil = new Session();
        mcObject = BOFactory.createDisputeBO(null);
        userHolded = sessionutil.GetUserLogging();
        repDetailsReport = BOFactory.createReplanishmentReportBO(null);
        newRecord = DTOFactory.createReplanishmentMasterDTO();
        fromOverlay = true;
        toOverlay = true;
        showAdd = true;
        showConfirm = false;
        datefromFlag = false;
        colorFrom = "white";
        datetoFlag = false;
        subdatefromFlag = false;
        subdatetoFlag = false;
        userNAME = sessionutil.GetUserLogging().getUserName();
        cdObject = BOFactory.createReplanishmentBO(null);
        atmList2 = (List<AtmMachineDTOInter>) mcObject.getAtmMachines3(sessionutil.GetUserLogging());
        atmList = (List<AtmMachineDTOInter>) utillist.GetAtmMachineList(sessionutil.GetUserLogging());
        cassetteList = (List<CassetteDTOInter>) utillist.GetCassetteList();
        cdList = new ArrayList<ReplanishmentMasterDTOInter>();
        repDetailsUsers = (List<UsersDTOInter>) utillist.GetUsersList();
        myObject = BOFactory.AtmGroupEditorBoInter(null);
        timeZone = TimeZone.getDefault();
        groupList = (List<AtmGroupDTOInter>) utillist.GetATMGroupAssignList(sessionutil.GetUserLogging().getUserId());
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        privelageList = super.findPrivelage(utillist.GetUserLogging().getUserId(), "Replanishment.xhtml");
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public Boolean exportPriv() {
        return super.findPrivelageOperation(privelageList, "EXCUTE");
    }

    public void exportExcel(ActionEvent e) throws Throwable {
        selectedRep = (ReplanishmentMasterDTO) e.getComponent().getAttributes().get("excelRow");
        result = (List<repreportexcelDTOInter>) repDetailsReport.printExcel(selectedRep, userNAME, sessionutil.Getclient());
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printReplanishment(ActionEvent e) {

        try {
            selectedRep = (ReplanishmentMasterDTO) e.getComponent().getAttributes().get("printRow");
            String path = (String) repDetailsReport.print(selectedRep, userNAME, sessionutil.Getclient());

            prepDownload(path);
        } catch (Throwable ex) {
            Logger.getLogger(ReplanishmentClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<AtmMachineDTOInter> autoComplete(AtmMachineDTOInter atmID) {
        return atmList;
    }

    private int totalremainning(List<ReplanishmentDetailDTOInter> recordslist) {
        int total = 0;
        for (ReplanishmentDetailDTOInter record : recordslist) {
            total = total + record.getRemaining();
        }
        return total;
    }

    private int totalcasseteValue(List<ReplanishmentDetailDTOInter> recordslist) {
        int total = 0;
        for (ReplanishmentDetailDTOInter record : recordslist) {
            total = total + record.getCasseteValue();
        }
        return total;
    }

    private int totalremainingamounttotal(List<ReplanishmentDetailDTOInter> recordslist) {
        int total = 0;
        for (ReplanishmentDetailDTOInter record : recordslist) {
            total = total + record.getRemainingAmount();
        }
        return total;
    }

    private int totaldispensedcount(List<ReplanishmentDetailDTOInter> recordslist) {
        int total = 0;
        for (ReplanishmentDetailDTOInter record : recordslist) {
            total = total + record.getDispensedcount();
        }
        return total;
    }

    private int totaldispensedvalue(List<ReplanishmentDetailDTOInter> recordslist) {
        int total = 0;
        for (ReplanishmentDetailDTOInter record : recordslist) {
            total = total + record.getDispensedvalue();
        }
        return total;
    }
//depositcounttotal,depositvaluetotal

    private int cassetteValueTotal(List<DepositDTOInter> recordslist) {
        int total = 0;
        for (DepositDTOInter record : recordslist) {
            total = total + record.getCassetteValue();
        }
        return total;
    }

    private int cassetteCountTotal(List<DepositDTOInter> recordslist) {
        int total = 0;
        for (DepositDTOInter record : recordslist) {
            total = total + record.getCassetteCount();
        }
        return total;
    }

    private int retractValueTotal(List<DepositDTOInter> recordslist) {
        int total = 0;
        for (DepositDTOInter record : recordslist) {
            total = total + record.getRetractValue();
        }
        return total;
    }

    private int retractCountTotal(List<DepositDTOInter> recordslist) {
        int total = 0;
        for (DepositDTOInter record : recordslist) {
            total = total + record.getRetractCount();
        }
        return total;
    }

    private int recycledValueTotal(List<DepositDTOInter> recordslist) {
        int total = 0;
        for (DepositDTOInter record : recordslist) {
            total = total + record.getRecycledValue();
        }
        return total;
    }

    private int recycledCountTotal(List<DepositDTOInter> recordslist) {
        int total = 0;
        for (DepositDTOInter record : recordslist) {
            total = total + record.getRecycledCount();
        }
        return total;
    }

    private int depositValueTotal(List<DepositDTOInter> recordslist) {
        int total = 0;
        for (DepositDTOInter record : recordslist) {
            total = total + record.getDepositValue();
        }
        return total;
    }

    private int depositCountTotal(List<DepositDTOInter> recordslist) {
        int total = 0;
        for (DepositDTOInter record : recordslist) {
            total = total + record.getDepositCount();
        }
        return total;
    }

    private int totalcassettecount(List<ReplanishmentDetailDTOInter> recordslist) {
        int total = 0;
        for (ReplanishmentDetailDTOInter record : recordslist) {
            total = total + record.getCassettecount();
        }
        return total;
    }

    public void onRowSelect1(SelectEvent e) throws Throwable {
        selectedMT = DTOFactory.createReplanishmentMasterDTO();
        selectedMT = (ReplanishmentMasterDTOInter) e.getObject();
        cdDetailsList = (List<ReplanishmentDetailDTOInter>) cdObject.getOldReplanishmentDetail(selectedMT.getId());
        depositDetailList = (List<DepositDTOInter>) cdObject.getReplanishmentDetailDeposit(selectedMT.getId());
        remainingtotal = totalremainning(cdDetailsList);
        
        casseteValuetotal = totalcasseteValue(cdDetailsList);
        dispensedcounttotal = totaldispensedcount(cdDetailsList);
        dispensedvaluetotal = totaldispensedvalue(cdDetailsList);
        cassettecounttotal = totalcassettecount(cdDetailsList);
        cassetteCountTotal = cassetteCountTotal(depositDetailList);
        cassetteValueTotal2 = cassetteValueTotal(depositDetailList); 
        retractCountTotal = retractCountTotal(depositDetailList);
        retractValueTotal = retractValueTotal(depositDetailList);
        recycledCountTotal = recycledCountTotal(depositDetailList);
        recycledValueTotal = recycledValueTotal(depositDetailList);
        depositCountTotal = depositCountTotal(depositDetailList);
        depositValueTotal = depositValueTotal(depositDetailList);
        
        remainingamounttotal = totalremainingamounttotal(cdDetailsList);
        message = super.showMessage(0);
    }

    public void onRowUnSelect1(UnselectEvent e) {
        cdDetailsList = null;
        message = super.showMessage(0);
    }

    public Boolean deletePriv() {
        return super.findPrivelageOperation(privelageList, "DELETE");
    }

    public void fillDelete(ActionEvent e) {
        timeShiftAttr = (ReplanishmentMasterDTOInter) e.getComponent().getAttributes().get("removedRow");
        message = super.showMessage(0);
    }

    public void deleteRecord() {
        try {
            cdList.remove(timeShiftAttr);
            cdObject.editeReplanishment(timeShiftAttr, cdDetailsList, cdObject.DELETE);
            message = super.showMessage(SUCC);
            cdDetailsList = null;
            showAdd = true;
            showConfirm = false;
        } catch (Throwable ex) {
        }
    }

    public Boolean updatePriv() {
        return super.findPrivelageOperation(privelageList, "UPDATE");
    }

    public void fillUpdate(ActionEvent e) throws Throwable {
        timeShiftAttr1 = (ReplanishmentMasterDTOInter) e.getComponent().getAttributes().get("updateRow");
        message = super.showMessage(0);
// message = "";
    }
    ReplanishmentMasterDTOInter containerDTO;

    public ReplanishmentMasterDTOInter getContainerDTO() {
        return containerDTO;
    }

    public void setContainerDTO(ReplanishmentMasterDTOInter containerDTO) {
        this.containerDTO = containerDTO;
    }

    public void fillrecalc(ActionEvent e) throws Throwable {
        containerDTO = (ReplanishmentMasterDTOInter) e.getComponent().getAttributes().get("updateRowAll");
        message = super.showMessage(0);
    }

    public void fillUpdaterecalc() throws Throwable {
        try {
            long tStart = System.currentTimeMillis();
            if (!showAdd) {
                message = super.showMessage(CONF_FIRST);
            } else {
                if (containerDTO.getDateFrom() == null || containerDTO.getDateTo() == null) {
                    message = super.showMessage(REQ_FIELD);
                } else if (containerDTO.getDateFrom().after(containerDTO.getDateTo())) {
                    message = super.showMessage(CHECK_DATE);
                } else {
                    UsersDTOInter u = super.getLoggedInUser();
                    containerDTO.setCreatedBy(u.getUserId());
                    containerDTO.setUname(u.getUserName());
                    containerDTO.setCreatedDate(new Date());
                    cdObject.editeReplanishmentnew(containerDTO, cdDetailsList, cdObject.UPDATE);
                    cdObject.recalcall(containerDTO);
                    long tEnd = System.currentTimeMillis();
                    message = convertMillis(tEnd - tStart);
                }
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    private RepPopDTOInter popUp;

    public RepPopDTOInter getPopUp() {
        return popUp;
    }

    public void setPopUp(RepPopDTOInter popUp) {
        this.popUp = popUp;
    }

    public void replPopUp(ActionEvent e) throws Throwable {
        try {
            ReplanishmentMasterDTOInter repmaster = (ReplanishmentMasterDTOInter) e.getComponent().getAttributes().get("reppop");
            popUp = DTOFactory.createRepPopDTO();
            popUp = (RepPopDTOInter) cdObject.replanishmentPopUp(repmaster);
        } catch (Throwable ex) {
            Logger.getLogger(ReplanishmentClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateRecord() {
        try {
            long tStart = System.currentTimeMillis();
            ReplanishmentMasterDTOInter containerDTO = DTOFactory.createReplanishmentMasterDTO();
            if (selectedMT == null) {
                containerDTO = timeShiftAttr1;
            } else {
                containerDTO = timeShiftAttr1;
            }
//            message = "";
//            timeShiftAttr1 = (ReplanishmentMasterDTOInter) e.getComponent().getAttributes().get("updateRow");
            if (!showAdd) {
                message = super.showMessage(CONF_FIRST);
            } else {
                if (containerDTO.getDateFrom() == null || containerDTO.getDateTo() == null) {
                    message = super.showMessage(REQ_FIELD);
                } else if (containerDTO.getDateFrom().after(containerDTO.getDateTo())) {
                    message = super.showMessage(CHECK_DATE);
                } else {
                    UsersDTOInter u = super.getLoggedInUser();
                    containerDTO.setCreatedBy(u.getUserId());
                    containerDTO.setUname(u.getUserName());
                    containerDTO.setCreatedDate(new Date());
                    cdObject.editeReplanishment(containerDTO, cdDetailsList, cdObject.UPDATE);
                    cdObject.RecalculateReplanishment(containerDTO);

                    long tEnd = System.currentTimeMillis();
                    message = convertMillis(tEnd - tStart);
                }
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public static String convertMillis(long milliseconds) {
        return "Done Successfully In Time: " + (new SimpleDateFormat("mm:ss:SS")).format(new Date(milliseconds));
    }

    public void SaveChanges() throws Throwable {
        cdObject.save(cdList);
        message = super.showMessage(SUCC);
    }

    public void resetVars() {
        newRecord = DTOFactory.createReplanishmentMasterDTO();
    }

    public Boolean insertPriv() {
        if (super.findPrivelageOperation(privelageList, "INSERT")) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    public void addRecord() {
        try {
            if (newRecord.getDateFrom() == null || newRecord.getDateTo() == null || newRecord.getAtmId() == 0) {
                message = super.showMessage(REQ_FIELD);
            } else if (newRecord.getDateFrom().after(newRecord.getDateTo())) {
                message = super.showMessage(CHECK_DATE);
            } else {
                repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:mat_typ_dt");
                UsersDTOInter u = super.getLoggedInUser();
                newRecord.setUname(u.getUserName());
                newRecord.setCreatedBy(u.getUserId());
                newRecord.setCreatedDate(new Date());
                newRecord.setCardtakenstate(true);
                cdDetailsList = (List<ReplanishmentDetailDTOInter>) cdObject.getNewReplanishmentDetail();
                cdObject.editeReplanishment(newRecord, cdDetailsList, cdObject.INSERT);
                cdList.add(0, newRecord);
                selectedMT = newRecord;
                repTable.setSelection(newRecord);
                cdDetailsList = (List<ReplanishmentDetailDTOInter>) cdObject.getOldReplanishmentDetail(selectedMT.getId());
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void confirmRecord() {
        try {
            if (newRecord.getDateFrom() == null || newRecord.getDateTo() == null) {
                message = super.showMessage(REQ_FIELD);
            } else if (newRecord.getDateFrom().after(newRecord.getDateTo())) {
                message = super.showMessage(CHECK_DATE);
            } else {
                UsersDTOInter u = super.getLoggedInUser();
                newRecord.setCreatedBy(u.getUserId());
                newRecord.setUname(u.getUserName());
                newRecord.setCreatedDate(new Date());
                cdObject.editeReplanishment(newRecord, cdDetailsList, cdObject.INSERT);
                showAdd = true;
                showConfirm = false;
                message = super.showMessage(SUCC);
                lastPage();
                repTable.setSelection(newRecord);
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void lastPage() {
        repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:mat_typ_dt");
        int lastPage = repTable.getRowCount() / repTable.getRows();
        int rowFirst = (lastPage * repTable.getRows());
        repTable.setFirst(rowFirst);

        // repTable.setLiveScroll(Boolean.TRUE);
        repTable.setRowIndex(repTable.getRowCount() - 2);
        message = "";
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public void doSearch() {
        try {
            message = "";
            atmInt = 0;
            atmInt = cdObject.findByAtmId(texttocomp);
            boolean val = false;
            if (fromDate_ico == null) {
                if (toDate_ico == null) {
                    val = true;
                }
            }
            if (fromDate_ico != null) {
                if (toDate_ico != null) {
                    val = true;
                }
            }
            if (val == true) {
                val = false;
                if (fromDate_ico1 == null) {
                    if (toDate_ico1 == null) {
                        val = true;
                    }
                }
                if (fromDate_ico1 != null) {
                    if (toDate_ico1 != null) {
                        val = true;
                    }
                }
            }
            if (val == true) {
                cdList = (List<ReplanishmentMasterDTOInter>) cdObject.getReplanishmentData(fromDate, toDate, atmInt, fromCreate, toCreate, userInt, group2, status, fromDate_ico, toDate_ico, fromDate_ico1, toDate_ico1);
            } else {
                messageDlg = "Insert Complete Date Range";
                return;
            }
            showAdd = true;
            showConfirm = false;
            cdDetailsList = null;
            // repTable.setRowIndex(repTable.getRowCount() - 1);

        } catch (Throwable ex) {
        }
    }

    public void updateDate(ValueChangeEvent ex) throws Throwable {
        Integer atmId = (Integer) ex.getNewValue();
        if (newRecord != null) {
            newRecord.setDateFrom(cdObject.getLastReplanishmentDateFor(atmId));
        }
        message = "";
    }

    public void calenderIconChange(ValueChangeEvent e) {
        if (toDate_ico != null || fromDate_ico != null) {
            iconCheck = 1;
        } else {
            iconCheck = 0;
        }
    }

    public void groupChange(ValueChangeEvent e) {
        try {
            int grpId = (Integer) e.getNewValue();
            atmList = (List<AtmMachineDTOInter>) cdObject.getAtmMachines(sessionutil.GetUserLogging(), grpId);
        } catch (Throwable ex) {
            Logger.getLogger(ReplanishmentClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void overlayCheck() {
        if (fromDate != null || toDate != null) {
            fromDate_ico = null;
            toDate_ico = null;
            fromDate_ico1 = null;
            toDate_ico1 = null;
            subdatefromFlag = true;
            subdatetoFlag = true;
            fromOverlay = false;
            toOverlay = false;
        } else if (fromDate_ico != null || toDate_ico != null) {
            datefromFlag = true;
            fromOverlay = true;
            datetoFlag = true;
            fromDate = null;
            toDate = null;
            fromDate_ico1 = null;
            subdatetoFlag = true;
            toDate_ico1 = null;
            toOverlay = false;
            colorFrom = "#93B73B";
        } else if (fromDate_ico1 != null || toDate_ico1 != null) {
            datefromFlag = true;
            fromOverlay = true;
            datetoFlag = true;
            toOverlay = true;
            subdatefromFlag = true;
            fromDate_ico = null;
            toDate_ico = null;
            fromDate = null;
            toDate = null;
            colorFrom = "#93B73B";
        } else {
            subdatefromFlag = false;
            datefromFlag = false;
            fromOverlay = true;
            subdatetoFlag = false;
            datetoFlag = false;
            toOverlay = true;
        }
    }

    public void SelectDselectProcess() {
        for (ReplanishmentMasterDTOInter record : cdList) {
            record.setRowstatus(Boolean.FALSE);
        }
    }

    public void SelectAllProcess() {
        for (ReplanishmentMasterDTOInter record : cdList) {
            record.setRowstatus(Boolean.TRUE);
        }
    }

    public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();

    }

    public Boolean datesPriv() {
        if (super.findPrivelageOperation(privelageList, "SEARCH_DATE")) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }
}
