/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.UserAuditRepBOInter;
import com.ev.AtmBingo.bus.bo.UsersBOInter;

import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.LangDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileDTOInter;
import com.ev.AtmBingo.bus.dto.TimeShiftDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.faces.event.ActionEvent;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class SystemUsersClient extends BaseBean implements Serializable {

    private List<UsersDTOInter> brList;
    private List<UsersDTOInter> tempList;
    private UsersDTOInter temp;
    private UserAuditRepBOInter userRepBo;
    private List<TimeShiftDTOInter> tsList;
    private List<LangDTOInter> langList;
    private List<ProfileDTOInter> profileList;
    private UsersBOInter brObject;
    private UsersDTOInter brAttr, brAttrUp;
    private UsersDTOInter newBlock;
    private String message, confirmPassword, password, eName, aName, uName, message_pop;
    private Integer shiftId, userLang, profileInt, listSize;
    private Boolean showAdd, pendingAtms;
    private Boolean showexport, showcorrect, showConfirm, showPassLabel, showPass, disputGraphB, holidayEnabledB, emrHolidayEnabledB, showNewAtmsB, notificationsB, errorsB, onlineuserB, correctiveB, exportB, userActivityB;
    private Integer Onlineuser, Errors, Notification, Pendingatms, ShowdisputGraph, showNewAtms;

    public Boolean getShowcorrect() {
        return showcorrect;
    }

    public Boolean getShowexport() {
        return showexport;
    }

    public void setShowexport(Boolean showexport) {
        this.showexport = showexport;
    }

    public void setShowcorrect(Boolean showcorrect) {
        this.showcorrect = showcorrect;
    }

    public Boolean getUserActivityB() {
        return userActivityB;
    }

    public void setUserActivityB(Boolean userActivityB) {
        this.userActivityB = userActivityB;
    }

    public Boolean getCorrectiveB() {
        return correctiveB;
    }

    public void setCorrectiveB(Boolean correctiveB) {
        this.correctiveB = correctiveB;
    }

    public Boolean getExportB() {
        return exportB;
    }

    public void setExportB(Boolean exportB) {
        this.exportB = exportB;
    }

    public Boolean getOnlineuserB() {
        return onlineuserB;
    }

    public void setOnlineuserB(Boolean onlineuserB) {
        this.onlineuserB = onlineuserB;
    }

    public Boolean getErrorsB() {
        return errorsB;
    }

    public void setErrorsB(Boolean errorsB) {
        this.errorsB = errorsB;
    }

    public Boolean getNotificationsB() {
        return notificationsB;
    }

    public void setNotificationsB(Boolean notificationsB) {
        this.notificationsB = notificationsB;
    }

    public Boolean getPendingAtms() {
        return pendingAtms;
    }

    public void setPendingAtms(Boolean pendingAtms) {
        this.pendingAtms = pendingAtms;
    }

    public Integer getErrors() {
        return Errors;
    }

    public void setErrors(Integer Errors) {
        this.Errors = Errors;
    }

    public Integer getNotification() {
        return Notification;
    }

    public void setNotification(Integer Notification) {
        this.Notification = Notification;
    }

    public Integer getOnlineuser() {
        return Onlineuser;
    }

    public void setOnlineuser(Integer Onlineuser) {
        this.Onlineuser = Onlineuser;
    }

    public Integer getPendingatms() {
        return Pendingatms;
    }

    public void setPendingatms(Integer Pendingatms) {
        this.Pendingatms = Pendingatms;
    }

    public Integer getShowdisputGraph() {
        return ShowdisputGraph;
    }

    public void setShowdisputGraph(Integer ShowdisputGraph) {
        this.ShowdisputGraph = ShowdisputGraph;
    }

    public UsersBOInter getBrObject() {
        return brObject;
    }

    public void setBrObject(UsersBOInter brObject) {
        this.brObject = brObject;
    }

    public Integer getShowNewAtms() {
        return showNewAtms;
    }

    public void setShowNewAtms(Integer showNewAtms) {
        this.showNewAtms = showNewAtms;
    }

    public UserAuditRepBOInter getUserRepBo() {
        return userRepBo;
    }

    public void setUserRepBo(UserAuditRepBOInter userRepBo) {
        this.userRepBo = userRepBo;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public Integer getProfileInt() {
        return profileInt;
    }

    public void setProfileInt(Integer profileInt) {
        this.profileInt = profileInt;
    }

    public List<ProfileDTOInter> getProfileList() {
        return profileList;
    }

    public void setProfileList(List<ProfileDTOInter> profileList) {
        this.profileList = profileList;
    }

    public String getMessage_pop() {
        return message_pop;
    }

    public void setMessage_pop(String message_pop) {
        this.message_pop = message_pop;
    }

    public Integer getShiftId() {
        return shiftId;
    }

    public void setShiftId(Integer shiftId) {
        this.shiftId = shiftId;
    }

    public Boolean getDisputGraphB() {
        return disputGraphB;
    }

    public void setDisputGraphB(Boolean disputGraphB) {
        this.disputGraphB = disputGraphB;
    }

    public Boolean getEmrHolidayEnabledB() {
        return emrHolidayEnabledB;
    }

    public void setEmrHolidayEnabledB(Boolean emrHolidayEnabledB) {
        this.emrHolidayEnabledB = emrHolidayEnabledB;
    }

    public Boolean getHolidayEnabledB() {
        return holidayEnabledB;
    }

    public void setHolidayEnabledB(Boolean holidayEnabledB) {
        this.holidayEnabledB = holidayEnabledB;
    }

    public Boolean getShowNewAtmsB() {
        return showNewAtmsB;
    }

    public void setShowNewAtmsB(Boolean showNewAtmsB) {
        this.showNewAtmsB = showNewAtmsB;
    }

    public Integer getUserLang() {
        return userLang;
    }

    public void setUserLang(Integer userLang) {
        this.userLang = userLang;
    }

    public String getaName() {
        return aName;
    }

    public void setaName(String aName) {
        this.aName = aName;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UsersDTOInter getBrAttrUp() {
        return brAttrUp;
    }

    public void setBrAttrUp(UsersDTOInter brAttrUp) {
        this.brAttrUp = brAttrUp;
    }

    public Boolean getShowPass() {
        return showPass;
    }

    public void setShowPass(Boolean showPass) {
        this.showPass = showPass;
    }

    public Boolean getShowPassLabel() {
        return showPassLabel;
    }

    public void setShowPassLabel(Boolean showPassLabel) {
        this.showPassLabel = showPassLabel;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public List<TimeShiftDTOInter> getTsList() {
        return tsList;
    }

    public void setTsList(List<TimeShiftDTOInter> tsList) {
        this.tsList = tsList;
    }

    public List<LangDTOInter> getLangList() {
        return langList;
    }

    public void setLangList(List<LangDTOInter> langList) {
        this.langList = langList;
    }

    public UsersDTOInter getBrAttr() {
        return brAttr;
    }

    public void setBrAttr(UsersDTOInter brAttr) {
        this.brAttr = brAttr;
    }

    public List<UsersDTOInter> getBrList() {
        return brList;
    }

    public void setBrList(List<UsersDTOInter> brList) {
        this.brList = brList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UsersDTOInter getNewBlock() {
        return newBlock;
    }

    public void setNewBlock(UsersDTOInter newBlock) {
        this.newBlock = newBlock;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    /**
     * Creates a new instance of SystemUsersClient
     */
    public SystemUsersClient() throws Throwable {
        super();
        super.GetAccessPage();
        brObject = BOFactory.createUserBO(null);
        userRepBo = BOFactory.createUserAuditBOInter(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        if (session.getAttribute("UserLogging") != null) {
            Session sessionutil = new Session();
            if (!brObject.getaccess(sessionutil.GetUserLogging())) {
                HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                response.sendRedirect("faces/Login.xhtml");
                session.invalidate();
                return;
            }
        } else {

            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect("faces/Login.xhtml");
            session.invalidate();
            return;
        }
        brList = (List<UsersDTOInter>) brObject.getUsers(super.getRestrected());
        tempList = (List<UsersDTOInter>) brObject.getUsers(super.getRestrected());

        errorsB = Boolean.FALSE;
        for (int i = 0; i < brList.size(); i++) {
            if (brList.get(i).getPendingatms() == 1) {
                brList.get(i).setPendingAtms(true);
            } else {
                brList.get(i).setPendingAtms(false);
            }

            if (brList.get(i).getOnlineuser() == 1) {
                brList.get(i).setOnlineuserB(true);
            } else {
                brList.get(i).setOnlineuserB(false);
            }

            if (brList.get(i).getShowNewAtms() == 1) {
                brList.get(i).setShowNewAtmsB(true);
            } else {
                brList.get(i).setShowNewAtmsB(false);
            }
            if (brList.get(i).getNotification() == 1) {
                brList.get(i).setNotificationB(true);
            } else {
                brList.get(i).setNotificationB(false);
            }
            if (brList.get(i).getErrors() == 1) {
                brList.get(i).setErrorsB(true);
            } else {
                brList.get(i).setErrorsB(false);
            }
        }
        showAdd = true;
        showConfirm = false;
        //langList = (List<LangDTOInter>) brObject.getlang();

        profileList = (List<ProfileDTOInter>) brObject.getPrifiles(super.getRestrected());
        profileInt = profileList.get(0).getProfileId();
        showPass = false;
        showPassLabel = true;
        listSize = brList.size();

        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    private UsersDTOInter getrecord() {
        for (UsersDTOInter record : tempList) {
            if (record.getUserId() == brAttrUp.getUserId()) {
                return record;
            }
        }
        return null;
    }

    public void fillUp(ActionEvent e) {
        brAttrUp = (UsersDTOInter) e.getComponent().getAttributes().get("updateRow");
        temp = getrecord();
        resetVars();
    }

    public void updateBlockReason(ActionEvent e) throws Throwable {
        try {
            ArrayList tee = CompareList.compareBeans(temp, brAttrUp);
            if (showAdd == false) {
                message = super.showMessage(CONF_FIRST);
            } else if (brAttrUp.getUserName().equals("") || brAttrUp.getUserAName().equals("")
                    || brAttrUp.getUserPassword().equals("") || brAttrUp.getLogonName().equals("")) {

                message = super.showMessage(REQ_FIELD);
            } else {
                if (brAttrUp.getPendingAtms()) {
                    brAttrUp.setPendingatms(1);
                } else {
                    brAttrUp.setPendingatms(2);
                }
                if (brAttrUp.getOnlineuserB()) {
                    brAttrUp.setOnlineuser(1);
                } else {
                    brAttrUp.setOnlineuser(2);
                }
                if (brAttrUp.getShowNewAtmsB()) {
                    brAttrUp.setShowNewAtms(1);
                } else {
                    brAttrUp.setShowNewAtms(2);
                }
                if (brAttrUp.getNotificationB()) {
                    brAttrUp.setNotification(1);
                } else {
                    brAttrUp.setNotification(2);
                }
                if (brAttrUp.getErrorsB()) {
                    brAttrUp.setErrors(1);
                } else {
                    brAttrUp.setErrors(2);
                }
                if (brAttrUp.getShowCorrect() == true) {
                    brAttrUp.setCorrectiveEntry(1);
                } else {
                    brAttrUp.setCorrectiveEntry(2);
                }
                if (brAttrUp.getShowexport() == true) {
                    brAttrUp.setExport(1);
                } else {
                    brAttrUp.setExport(2);
                }
                String[] inputs = {brAttrUp.getLogonName(), brAttrUp.getUserName()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    if ("Not Found".equals(TestColName(brAttrUp.getLogonName()))) {
                        if ("Not Found".equals(TestColNameUserName(brAttrUp.getUserName()))) {
                            brObject.editeUsers(brAttrUp, brObject.UPDATE);
                            message = super.showMessage(SUCC);
                        } else {
                            message = "Name Already Exists";
                        }
                    } else {
                        message = "User Name Already Exists";
                    }
                } else {
                    message = "Please Enter Valid Data";
                }

            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void resetVars() {
        message = "";
    }

    public void fillDel(ActionEvent e) {
        brAttr = (UsersDTOInter) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void deleteBlockReason() throws Throwable {
        try {
            String[] inputs = {brAttr.getLogonName(), brAttr.getUserName()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {

                brObject.editeUsers(brAttr, brObject.DELETE);
                brList.remove(brAttr);
                message = super.showMessage(SUCC);
                showAdd = true;
                showConfirm = false;
                showPass = false;
                showPassLabel = true;
            } else {
                message = "Please Enter Valid Data";
            }
        } catch (Throwable ex) {
            message = "User can't be deleted, there are ATMs assigned to him";
        }
    }

    public String TestColName(String ColName) {
        String MSG = "";
        int j = 0;
        for (int i = 0; i < brList.size(); i++) {
            if (brList.get(i).getLogonName().equals(ColName)) {
                j++;
                if (j == 2) {
                    return "Found";
                }
            }
        }
        return "Not Found";
    }

    public String TestColNameUserName(String ColName) {
        String MSG = "";
        int j = 0;
        for (int i = 0; i < brList.size(); i++) {
            if (brList.get(i).getUserName().toString().equals(ColName)) {
                j++;
                if (j == 2) {
                    return "Found";
                }
            }
        }
        return "Not Found";
    }

    public void confirmInsert() {
        try {
            newBlock = DTOFactory.createUsersDTO();
            message_pop = "";
            if (eName.equals("") || uName.equals("") || password.equals("") || confirmPassword.equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else if (!password.equals(confirmPassword)) {
                message = "Passwords don't match";
            } else {
                newBlock.getProfileDTO().setProfileId(profileInt);
                newBlock.setUserName(eName);
                newBlock.setLogonName(uName);
                newBlock.setUserAName(eName);
                newBlock.setConfirmPassword(confirmPassword);
                newBlock.setUserPassword(password);
                newBlock.setDisputGraphB(userActivityB);

                newBlock.setShowCorrect(showcorrect);
                newBlock.setShowexport(showexport);

                if (userActivityB == true) {
                    newBlock.setDisputeGraph(1);
                } else {
                    newBlock.setDisputeGraph(2);
                }
                if (pendingAtms == true) {
                    newBlock.setPendingatms(1);
                } else {
                    newBlock.setPendingatms(2);
                }
                if (showNewAtmsB == true) {
                    newBlock.setShowNewAtms(1);
                } else {
                    newBlock.setShowNewAtms(2);
                }

                newBlock.setNotification(1);

                if (errorsB == true) {
                    newBlock.setErrors(1);
                } else {
                    newBlock.setErrors(2);
                }
                if (onlineuserB == true) {
                    newBlock.setOnlineuser(1);
                } else {
                    newBlock.setOnlineuser(2);
                }
                String[] inputs = {newBlock.getLogonName(), newBlock.getUserName()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    if ("Not Found".equals(TestColName(newBlock.getLogonName()))) {
                        if ("Not Found".equals(TestColNameUserName(newBlock.getUserName()))) {
                            brObject.editeUsers(newBlock, brObject.INSERT);
                            brList.add(newBlock);
                            message = super.showMessage(SUCC);
                        } else {
                            message = "Name Already Exists";
                        }
                    } else {
                        message = "User Name Already Exists";
                    }
                } else {
                    message = "Please Enter Valid Data";
                }

            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
            //message = ex.getMessage();
        }
    }

    public void resetDialog() {
        eName = "";
        aName = "";
        uName = "";
        password = "";
        confirmPassword = "";
        userLang = 1;

        disputGraphB = false;

        showNewAtmsB = false;
        resetVars();
    }

    public void userPrint() {
        try {
            Session sessionutil = new Session();
            String path = (String) userRepBo.print(super.getLoggedInUser().getUserName(), sessionutil.Getclient());
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect(path);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();

    }
}
