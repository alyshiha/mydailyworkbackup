/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import static com.ev.AtmBingo.base.client.BaseBean.SUCC;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.UsersProfileBOInter;
import com.ev.AtmBingo.bus.dto.ProfileDTOInter;
import com.ev.AtmBingo.bus.dto.UserProfileDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.model.DualListModel;

/**
 *
 * @author ISLAM
 */
public class ProfileAssignClient extends BaseBean  implements Serializable{

    private UsersProfileBOInter profileBO;
    private List<ProfileDTOInter> profileList;
    private UserProfileDTOInter upDTO;
    private List<String> targetList, sourceList;
    private DualListModel<String> pickValues;
    private Integer profileInt;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getSourceList() {
        return sourceList;
    }

    public void setSourceList(List<String> sourceList) {
        this.sourceList = sourceList;
    }

    public List<String> getTargetList() {
        return targetList;
    }

    public void setTargetList(List<String> targetList) {
        this.targetList = targetList;
    }

    public DualListModel<String> getPickValues() {
        return pickValues;
    }

    public void setPickValues(DualListModel<String> pickValues) {
        this.pickValues = pickValues;
    }

    public Integer getProfileInt() {
        return profileInt;
    }

    public void setProfileInt(Integer profileInt) {
        this.profileInt = profileInt;
    }

    public List<ProfileDTOInter> getProfileList() {
        return profileList;
    }

    public void setProfileList(List<ProfileDTOInter> profileList) {
        this.profileList = profileList;
    }

    /**
     * Creates a new instance of ProfileAssignClient
     */
    public ProfileAssignClient() throws Throwable {
        super();
        super.GetAccessPage();
        profileBO = BOFactory.createUsersProfileBO(null);
        profileList = (List<ProfileDTOInter>) profileBO.getProfiles(super.getRestrected());
        pickValues = new DualListModel<String>();
        FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void changeValue(ValueChangeEvent e) throws Throwable {
        profileInt = (Integer) e.getNewValue();
        pickValues = (DualListModel<String>) profileBO.getPickList(profileInt, super.getRestrected());
        resetVars();
    }

    public void saveProfile() {

        message = super.showMessage(SUCC);
        try {
            int i = 0;
            String[] inputs = new String[pickValues.getTarget().size() + pickValues.getSource().size()];
            for (String item : pickValues.getTarget()) {
                inputs[i] = item;
                i++;
            }
            for (String item : pickValues.getSource()) {
                inputs[i] = item;
                i++;
            }
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace("(", "").replace(")", "").replace("#", "").replace(".", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
                profileBO.editUserProfile(pickValues.getTarget(), profileInt);
                message = super.showMessage(SUCC);
                
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
            Logger.getLogger(DefualtColumnClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void resetVars() {
        message = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
