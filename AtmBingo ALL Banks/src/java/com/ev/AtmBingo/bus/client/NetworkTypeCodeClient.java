
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.NetworkTypeCodeBOInter;
import com.ev.AtmBingo.bus.bo.NetworksBOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.NetworkTypeCodeDTOInter;
import com.ev.AtmBingo.bus.dto.NetworksDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Aly
 */
@ManagedBean(name = "networkTypeCodeView")
@ViewScoped
public class NetworkTypeCodeClient extends BaseBean implements Serializable {

    private NetworkTypeCodeDTOInter selectedRecord, newRecord;
    private List<NetworkTypeCodeDTOInter> recordsList;
    private List<NetworksDTOInter> networkrecordsList;
    private List<ColumnDTOInter> columnList;
    private List<String> privelageList;
    private int listcount;
    private String code;
    private int network;
    private NetworkTypeCodeBOInter engin;
    private NetworksBOInter networkengin;
    private Session sessionengin;

    public List<ColumnDTOInter> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<ColumnDTOInter> columnList) {
        this.columnList = columnList;
    }

    public Session getSessionengin() {
        return sessionengin;
    }

    public void setSessionengin(Session sessionengin) {
        this.sessionengin = sessionengin;
    }

    public NetworkTypeCodeDTOInter getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(NetworkTypeCodeDTOInter selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public NetworkTypeCodeDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(NetworkTypeCodeDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public List<NetworkTypeCodeDTOInter> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<NetworkTypeCodeDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public List<NetworksDTOInter> getNetworkrecordsList() {
        return networkrecordsList;
    }

    public void setNetworkrecordsList(List<NetworksDTOInter> networkrecordsList) {
        this.networkrecordsList = networkrecordsList;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getNetwork() {
        return network;
    }

    public void setNetwork(int network) {
        this.network = network;
    }

    public NetworkTypeCodeBOInter getEngin() {
        return engin;
    }

    public void setEngin(NetworkTypeCodeBOInter engin) {
        this.engin = engin;
    }

    public NetworksBOInter getNetworkengin() {
        return networkengin;
    }

    public void setNetworkengin(NetworksBOInter networkengin) {
        this.networkengin = networkengin;
    }

    @PostConstruct
    public void init() {
        try {
            code = "";
            network = 0;
            engin = BOFactory.createNetworkTypeCodeBO(null);
            networkengin = BOFactory.createNetworksBO(null);
            sessionengin = new Session();
            networkrecordsList = networkengin.getNetworks();
            recordsList = engin.getNetworkType();
            columnList = (List<ColumnDTOInter>) sessionengin.GetColumnsList();
            privelageList = super.findPrivelage(sessionengin.GetUserLogging().getUserId(), "NetworkTypeCode.xhtml");
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(NetworksClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public void searchrecords() {
        try {
            recordsList = engin.getNetworkTypeRecord(network, code);
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(NetworksClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean insertPriv() {
        return super.findPrivelageOperation(privelageList, "INSERT");
    }

    public void addRecord() {
        newRecord = DTOFactory.createNetworkTypeCodeDTO();
    }

    public void saveRecord() throws SQLException, Throwable {
        
        String message = engin.insertNetworkType(newRecord);
        recordsList.add(newRecord);
        recordsList = engin.getNetworkType();
        listcount = recordsList.size();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean updatePriv() {
        return super.findPrivelageOperation(privelageList, "UPDATE");
    }

    public void updateRecord() throws SQLException, Throwable {
        String message = engin.updateNetworkType(selectedRecord);
        recordsList = engin.getNetworkType();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean deletePriv() {
        return super.findPrivelageOperation(privelageList, "DELETE");
    }

    public void deleteRecord() throws SQLException {
        String message = engin.deleteNetworkType(selectedRecord);
        recordsList.remove(selectedRecord);
        listcount = recordsList.size();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public String getColumnName(Integer ID) {
        for (ColumnDTOInter column : columnList) {
            if (column.getId() == ID) {
                return column.getHeader();
            }
        }
        return "UnAssigned";
    }

    public String getNetworkName(Integer ID) {
        for (NetworksDTOInter networkrecord : networkrecordsList) {
            if (networkrecord.getId() == ID) {
                return networkrecord.getName();
            }
        }
        return "UnAssigned";
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printreportPDF() throws Exception, Throwable {

        String path = (String) engin.printPDFrep(sessionengin.Getclient(), super.getLoggedInUser().getUserName(), network, code, getNetworkName(network));
        prepDownload(path);
    }
}
