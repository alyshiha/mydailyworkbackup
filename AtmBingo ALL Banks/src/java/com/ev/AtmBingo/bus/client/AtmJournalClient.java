/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.bo.AtmJournalBOInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.RepStatReportBOInter;
import com.ev.AtmBingo.bus.bo.replanishmentCashManagementBOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmJournalDTO;
import com.ev.AtmBingo.bus.dto.AtmJournalDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.replanishmentCashManagementDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Aly
 */
@ManagedBean(name = "AtmJournalView")
@ViewScoped
public class AtmJournalClient extends BaseBean implements Serializable {

    private List<CurrencyMasterDTOInter> currencyList;
    private List<AtmJournalDTOInter> detailrecordsList;
    private List<replanishmentCashManagementDTOInter> recordsList;
    private replanishmentCashManagementDTOInter selectedRecord;
    private AtmJournalDTOInter[] detailselectedRecord;
    private RepStatReportBOInter repStatEngin;
    private List<AtmJournalDTOInter> operationselectedRecord;

    private List<AtmMachineDTOInter> atmrecordsList;
    private List<AtmGroupDTOInter> atmgroupList;
    private int listcount;
    private int detaillistcount;
    private Date datefrom;
    private Date dateto;
    private String atmid;
    private int group;
    private Boolean export, realesed;
    private UsersDTOInter user;
    private AtmJournalBOInter engin;
    private replanishmentCashManagementBOInter rcmEngin;
    private Session utillist = new Session();
    private List<String> privelageList;
    private String excelcoloumns[] = {"ID", "TRANSACTION.TYPE", "DEBIT.ACCT.NO", "DEBIT.CURRENCY", "DEBIT.AMOUNT", "DEBIT.VALUE.DATE", "CREDIT.VALUE.DATE", "DEBIT.THEIR.REF", "CREDIT.ACC.NO", "ORDERING.BANK", "PROFIT.CENTER.DEPT", "COMMISION.CODE", "COMPANY"
    };

    public List<AtmJournalDTOInter> getDetailrecordsList() {
        return detailrecordsList;
    }

    public void setDetailrecordsList(List<AtmJournalDTOInter> detailrecordsList) {
        this.detailrecordsList = detailrecordsList;
    }

    public AtmJournalDTOInter[] getDetailselectedRecord() {
        return detailselectedRecord;
    }

    public void setDetailselectedRecord(AtmJournalDTOInter[] detailselectedRecord) {
        this.detailselectedRecord = detailselectedRecord;
    }

    public List<String> getPrivelageList() {
        return privelageList;
    }

    public void setPrivelageList(List<String> privelageList) {
        this.privelageList = privelageList;
    }

    public List<replanishmentCashManagementDTOInter> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<replanishmentCashManagementDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public replanishmentCashManagementDTOInter getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(replanishmentCashManagementDTOInter selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public int getDetaillistcount() {
        return detaillistcount;
    }

    public void setDetaillistcount(int detaillistcount) {
        this.detaillistcount = detaillistcount;
    }

    public Date getDatefrom() {
        return datefrom;
    }

    public void setDatefrom(Date datefrom) {
        this.datefrom = datefrom;
    }

    public Date getDateto() {
        return dateto;
    }

    public void setDateto(Date dateto) {
        this.dateto = dateto;
    }

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public UsersDTOInter getUser() {
        return user;
    }

    public void setUser(UsersDTOInter user) {
        this.user = user;
    }

    public AtmJournalBOInter getEngin() {
        return engin;
    }

    public void setEngin(AtmJournalBOInter engin) {
        this.engin = engin;
    }

    public replanishmentCashManagementBOInter getRcmEngin() {
        return rcmEngin;
    }

    public void setRcmEngin(replanishmentCashManagementBOInter rcmEngin) {
        this.rcmEngin = rcmEngin;
    }

    public Session getUtillist() {
        return utillist;
    }

    public void setUtillist(Session utillist) {
        this.utillist = utillist;
    }

    public String[] getExcelcoloumns() {
        return excelcoloumns;
    }

    public void setExcelcoloumns(String[] excelcoloumns) {
        this.excelcoloumns = excelcoloumns;
    }

    public List<AtmMachineDTOInter> getAtmrecordsList() {
        return atmrecordsList;
    }

    public List<AtmGroupDTOInter> getAtmgroupList() {
        return atmgroupList;
    }

    public void setAtmgroupList(List<AtmGroupDTOInter> atmgroupList) {
        this.atmgroupList = atmgroupList;
    }

    public void setAtmrecordsList(List<AtmMachineDTOInter> atmrecordsList) {
        this.atmrecordsList = atmrecordsList;
    }

    public RepStatReportBOInter getRepStatEngin() {
        return repStatEngin;
    }

    public void setRepStatEngin(RepStatReportBOInter repStatEngin) {
        this.repStatEngin = repStatEngin;
    }

    public List<AtmJournalDTOInter> getOperationselectedRecord() {
        return operationselectedRecord;
    }

    public void setOperationselectedRecord(List<AtmJournalDTOInter> operationselectedRecord) {
        this.operationselectedRecord = operationselectedRecord;
    }

    public Boolean getRealesed() {
        return realesed;
    }

    public void setRealesed(Boolean realesed) {
        this.realesed = realesed;
    }

    public List<CurrencyMasterDTOInter> getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(List<CurrencyMasterDTOInter> currencyList) {
        this.currencyList = currencyList;
    }

    public Boolean getExport() {
        return export;
    }

    public void setExport(Boolean export) {
        this.export = export;
    }

    @PostConstruct
    public void init() {
        try {
            atmid = "0";
            group = 0;
            if (!isSunday()) {
                datefrom = super.getYesterdaytrans((String) "00:00:00");
                dateto = super.getTodaytrans((String) "00:00:00");
            } else {
                datefrom = super.getBeforYesterdaytrans((String) "00:00:00");
                dateto = super.getTodaytrans((String) "00:00:00");
            }
            user = utillist.GetUserLogging();
            realesed = false;
            atmrecordsList = (List<AtmMachineDTOInter>) utillist.GetAtmMachineList(user);
            operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
            atmgroupList = (List<AtmGroupDTOInter>) utillist.GetATMGroupAssignList(user.getUserId());
            currencyList = (List<CurrencyMasterDTOInter>) utillist.GetCurrencyMasterList();
            engin = BOFactory.createAtmJournalBO(null);
            if (user.getExport() == 2) {
                export = Boolean.FALSE;
            } else {
                export = Boolean.TRUE;
            }
            repStatEngin = BOFactory.createRepStatReportBO(null);
            rcmEngin = BOFactory.createreplanishmentCashManagementBO(null);
            recordsList = new ArrayList<replanishmentCashManagementDTOInter>();
            detailrecordsList = new ArrayList<AtmJournalDTOInter>();
            selectedRecord = DTOFactory.createreplanishmentCashManagementDTO();
            detailselectedRecord = null;
            detaillistcount = 0;
            privelageList = super.findPrivelage(utillist.GetUserLogging().getUserId(), "AtmJournal.xhtml");

            listcount = recordsList.size();
            searchrecords();

        } catch (Throwable ex) {
            Logger.getLogger(NetworksClient.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public void searchrecords() {
        try {
            recordsList = rcmEngin.runReportATMAmountexcel(DateFormatter.changeDateAndTimeFormat(datefrom), DateFormatter.changeDateAndTimeFormat(dateto), atmid, group, user.getUserId(), realesed);
            listcount = recordsList.size();
            detaillistcount = 0;
            detailselectedRecord = null;
            detailrecordsList = null;
        } catch (Throwable ex) {
            Logger.getLogger(NetworksClient.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean markPriv() {
        if (super.findPrivelageOperation(privelageList, "CHECK")) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    private void selectAllRecord() {
        DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:singleDT2");
        repTable.setSelection(detailrecordsList.toArray());
    }

    public void handleMasterChange() throws Throwable {
        if (selectedRecord != null) {
            if (user.getExport() == 2) {
                detailrecordsList = engin.getJournalrecord(getATMName(selectedRecord.getAtmid()), selectedRecord.getReplanishmentid(), 1, DateFormatter.changeDateAndTimeFormat(selectedRecord.getFromdate()), realesed);
            } else {
                detailrecordsList = engin.getJournalrecord(getATMName(selectedRecord.getAtmid()), selectedRecord.getReplanishmentid(), 2, DateFormatter.changeDateAndTimeFormat(selectedRecord.getFromdate()), realesed);
            }
            detailselectedRecord = null;
            selectAllRecord();
            detaillistcount = detailrecordsList.size();
        }
    }

    public Boolean confirmPriv() {
        return super.findPrivelageOperation(privelageList, "VALIDATE");
    }

    public void confirmTransaction() throws Throwable {
        Boolean validrecord = Boolean.FALSE;
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord != null) {
            if (detailselectedRecord.length != 0) {
                for (AtmJournalDTOInter record : detailselectedRecord) {
                    if (record.getExportflag() == 1) {
                        record.setExportflag(2);
                        operationselectedRecord.add(record);
                        validrecord = Boolean.TRUE;
                    }
                }
                engin.updateJournal(operationselectedRecord, "2");
                detailselectedRecord = null;

                detaillistcount = detailrecordsList.size();
                if (validrecord) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Has Been Confirmed Succesfully"));
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Can`t Be Validated Due To It`s Status"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Confirm"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Master Record"));
        }

    }

    public Boolean exportPriv() {
        return super.findPrivelageOperation(privelageList, "EXCUTE");
    }

    public Boolean reexportPriv() {
        return super.findPrivelageOperation(privelageList, "REEXCUTE");
    }

    public Boolean releasePriv() {
        return super.findPrivelageOperation(privelageList, "RELEASE");
    }

    public Boolean checkstat() {
        for (AtmJournalDTOInter record : detailselectedRecord) {
            if (record.getExportflag() != 2 && record.getExportflag() != 3) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    public Boolean datesPriv() {
        if (super.findPrivelageOperation(privelageList, "SEARCH_DATE")) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    public void exportCSV() throws Throwable {
        Boolean flag = Boolean.FALSE;
        boolean exportstat = false;
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord != null) {
            if (detailselectedRecord.length != 0) {

                for (AtmJournalDTOInter record : detailselectedRecord) {
                    if (record.getExportflag() == 2) {
                        record.setExportflag(3);
                        operationselectedRecord.add(record);
                        exportstat = true;
                    } else {
                        flag = Boolean.TRUE;
                    }
                }
                if (exportstat) {
                    engin.updateJournal(operationselectedRecord, "3");
                    String path = writeCSV(operationselectedRecord);
                    prepDownload(path);
                    detailselectedRecord = null;

                    detaillistcount = detailrecordsList.size();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Report Has Been Exported Succesfully"));
                    if (flag) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Confirm Record"));
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Validate Record First"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Export"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Master Record"));
        }
    }

    public void reexportCSV() throws Throwable {
        Boolean flag = Boolean.FALSE;
        boolean exportflag = false;
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord != null) {
            if (detailselectedRecord.length != 0) {

                for (AtmJournalDTOInter record : detailselectedRecord) {
                    if (record.getExportflag() == 3 || record.getExportflag() == 4) {
                        record.setExportflag(4);
                        operationselectedRecord.add(record);
                        exportflag = true;
                    } else {
                        flag = Boolean.TRUE;
                    }
                }
                if (exportflag) {
                    engin.updateJournal(operationselectedRecord, "4");
                    String path = writeCSV(operationselectedRecord);
                    prepDownload(path);
                    detailselectedRecord = null;

                    detaillistcount = detailrecordsList.size();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Report Has Been Exported Succesfully"));
                    if (flag) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Confirm Record"));
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Validate Record First"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Export"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Master Record"));
        }
    }

    public void exportexcel() throws Throwable {
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        List<AtmJournalDTOInter> exportRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord != null) {
            if (detailselectedRecord.length != 0) {
                for (AtmJournalDTOInter record : detailselectedRecord) {
                    operationselectedRecord.add(record);
                    exportRecord.add(record);
                }

                String PAth = exportexcel(excelcoloumns, exportRecord);
                prepDownload(PAth);
                detailselectedRecord = null;
                detaillistcount = detailrecordsList.size();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Report Has Been Exported Succesfully"));

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Export"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Master Record"));
        }
    }

    public String getATMName(Integer ID) {
        for (AtmMachineDTOInter atmrecord : atmrecordsList) {
            if (atmrecord.getId() == ID) {
                return atmrecord.getApplicationId();
            }
        }
        return "UnAssigned";
    }

    public String getCurrencyName(Integer ID) {
        for (CurrencyMasterDTOInter currencyrecord : currencyList) {
            if (currencyrecord.getId() == ID) {
                return currencyrecord.getSymbol();
            }
        }
        return "UnAssigned";
    }

    public String exportexcel(String[] coloumns, List<AtmJournalDTOInter> Records) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("ATM Journal");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        Object[] coloumnslist = new Object[coloumns.length];
        int index = 0;
        for (String coloumn : coloumns) {
            coloumnslist[index] = coloumn;
            sheet.setDefaultColumnWidth(20);
            index = index + 1;
        }
        data.put(1, coloumnslist);
        index = 2;
        for (AtmJournalDTOInter Record : Records) {
            Object[] coloumnsData = new Object[coloumns.length];
            for (int i = 0; i < coloumns.length; i++) {
                if ("ID".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getAtmid());
                }
                if ("TRANSACTION.TYPE".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf("AC");
                }
                if ("DEBIT.ACCT.NO".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getDebitaccount());
                }
                if ("DEBIT.CURRENCY".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(getCurrencyName(Record.getCurrency()));
                }
                if ("DEBIT.AMOUNT".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getAmount());
                }
                if ("DEBIT.VALUE.DATE".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getJournaldate());
                }
                if ("CREDIT.VALUE.DATE".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getJournaldate());
                }
                if ("DEBIT.THEIR.REF".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getJournalrefernce());
                }

                if ("CREDIT.ACC.NO".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getCreditaccount());
                }
                if ("ORDERING.BANK".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf("UNBE");
                }
                if ("PROFIT.CENTER.DEPT".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf("11100000");
                }
                if ("COMMISION.CODE".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf("WAIVE");
                }
                if ("COMPANY".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getBranch());
                }

            }
            data.put(index, coloumnsData);
            index = index + 1;
        }
        Set<Integer> keyset = data.keySet();
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            Calendar c = Calendar.getInstance();
            String uri = "ATM Journal" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".xls";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            pdfPath = pdfPath + uri;
            FileOutputStream out = new FileOutputStream(new File(pdfPath));
            workbook.write(out);
            out.close();
            return pdfPath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //write from duplicate array of courses to a CSV file
    public String writeCSV(List<AtmJournalDTOInter> Records) throws Exception {
        Calendar c = Calendar.getInstance();
        String uri = "\\AtmJournalatm" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".Csv";
        String pdfPath = utillist.getParameter("JOURNAL_EXPORT_PATH").toString();
        pdfPath = pdfPath + uri;
        //create a File class object and give the file the name 
        java.io.File courseCSV = new java.io.File(pdfPath);

        //Create a Printwriter text output stream and link it to the CSV File
        java.io.PrintWriter outfile = new java.io.PrintWriter(courseCSV);

        //Iterate the elements actually being used
        for (AtmJournalDTOInter Record : Records) {
            outfile.write(Record.toCSVString(","));
        }//end for

        outfile.close();
        return pdfPath;
    } //end writeCSV()

    public String getNowDate() {
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return sdf.format(date);
    }

    public Boolean isSunday() {
        // Get calendar set to current date and time
        Calendar c = Calendar.getInstance();
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public String GetStatusField(int status) {
        String statusString;
        switch (status) {
            case 1:
                statusString = "Pending";
                break;
            case 2:
                statusString = "Validated";
                break;
            case 3:
                statusString = "Executed";
                break;
            case 4:
                statusString = "REExecuted";
                break;
            case 8:
                statusString = "Released";
                break;
            default:
                statusString = "UnKnown";
                break;
        }
        return statusString;
    }

    public String thousandSeparator(int amount) {
        return String.format("%,d", amount);
    }

    public int calclateRs(int amount, int amount2, int amount3) {
        return amount - (amount2 + amount3);
    }

    public void confirmTrans(AtmJournalDTOInter[] Records) {
        Boolean flag = Boolean.FALSE;
        boolean exportstat = false;
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord != null) {
            if (detailselectedRecord.length != 0) {

                for (AtmJournalDTOInter record : detailselectedRecord) {
                    if (record.getExportflag() == 1) {
                        record.setExportflag(2);
                        operationselectedRecord.add(record);
                        exportstat = true;
                    } else {
                        flag = Boolean.TRUE;
                    }
                }
                if (exportstat) {
                    engin.updateJournal(operationselectedRecord, "2");

                    detailselectedRecord = null;

                    detaillistcount = detailrecordsList.size();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Report Has Been Confirmed Succesfully"));
                    if (flag) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Confirm Record"));
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Confirmed Record First"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Confirmed"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Master Record"));
        }
    }

    public void printreportpdf() {

        try {
            if (datefrom == null || dateto == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Date From & Date To Is A Required Fields"));
            } else {
                List<AtmJournalDTOInter> rectoprint = new ArrayList<AtmJournalDTOInter>();
                for (AtmJournalDTOInter rec : detailselectedRecord) {
                    rectoprint.add(rec);
                }

                String path = (String) repStatEngin.printrep9(super.getLoggedInUser().getUserName(), selectedRecord.getFromdate(), dateto, selectedRecord.getReplanishmentid(), utillist.Getclient(), atmid.toString(), getATMName(Integer.valueOf(atmid)), rectoprint,realesed);
                prepDownload(path);
            }
        } catch (Throwable ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", ex.getMessage()));
        }
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void recalc() {
        try {
            engin.recalc2();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Has Been Calculate Succesfully"));
        } catch (Throwable ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void realeseTransaction() throws Throwable {

        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord != null) {
            if (detailselectedRecord.length != 0) {

                for (AtmJournalDTOInter record : detailselectedRecord) {
                    record.setExportflag(8);
                    operationselectedRecord.add(record);
                }
                engin.updateJournal(operationselectedRecord, "8");
                detailselectedRecord = null;

                detaillistcount = detailrecordsList.size();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Report Has Been Released Succesfully"));

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Release"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Master Record"));
        }
    }

}
