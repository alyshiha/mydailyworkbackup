/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.AtmGroupEditorBoInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.UserAtmDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.model.DualListModel;

/**
 *
 * @author ISLAM
 */
public class AddGroupClient extends BaseBean  implements Serializable{

    private AtmGroupEditorBoInter mcObject;
    private List<UserAtmDTOInter> uatmList;
    private List<String> targetList, sourceList;
    private List<AtmGroupDTOInter> userList;
    private DualListModel<String> pickValues;
    private Integer atmGInt, userInt;
    private String message;
    private Boolean disabledSave;

    public Boolean getDisabledSave() {
        return disabledSave;
    }

    public void setDisabledSave(Boolean disabledSave) {
        this.disabledSave = disabledSave;
    }

    public Integer getAtmGInt() {
        return atmGInt;
    }

    public void setAtmGInt(Integer atmGInt) {
        this.atmGInt = atmGInt;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DualListModel<String> getPickValues() {
        return pickValues;
    }

    public void setPickValues(DualListModel<String> pickValues) {
        this.pickValues = pickValues;
    }

    public List<String> getSourceList() {
        return sourceList;
    }

    public void setSourceList(List<String> sourceList) {
        this.sourceList = sourceList;
    }

    public List<String> getTargetList() {
        return targetList;
    }

    public void setTargetList(List<String> targetList) {
        this.targetList = targetList;
    }

    public List<UserAtmDTOInter> getUatmList() {
        return uatmList;
    }

    public void setUatmList(List<UserAtmDTOInter> uatmList) {
        this.uatmList = uatmList;
    }

    public Integer getUserInt() {
        return userInt;
    }

    public void setUserInt(Integer userInt) {
        this.userInt = userInt;
    }

    public List<AtmGroupDTOInter> getUserList() {
        return userList;
    }

    public void setUserList(List<AtmGroupDTOInter> userList) {
        this.userList = userList;
    }

    /** Creates a new instance of AddGroupClient */
    public AddGroupClient() throws Throwable {
        super();
        super.GetAccessPage();
        mcObject = BOFactory.AtmGroupEditorBoInter(null);
        userList = (List<AtmGroupDTOInter>) mcObject.GetGroups();
        targetList = new ArrayList<String>();
        sourceList = new ArrayList<String>();
        pickValues = new DualListModel<String>(targetList, sourceList);
        disabledSave = true;
             FacesContext context = FacesContext.getCurrentInstance();
        
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void userChange(ValueChangeEvent e) throws Throwable {
        message = "";
        userInt = (Integer) e.getNewValue();
        if (userInt == 0) {
            disabledSave = true;
            targetList.clear();
        } else {
            if (atmGInt == null || atmGInt == 0) {
                atmGInt = 0;
                disabledSave = true;
                targetList = (List<String>) mcObject.getAtmGroup(userInt);
                pickValues = new DualListModel<String>(targetList, sourceList);
            } else {
                if (userInt == atmGInt) {
                    message = "Select another group";
                    targetList.clear();
                    disabledSave = true;
                    userInt = 0;
                } else {
                    disabledSave = false;
                    targetList = (List<String>) mcObject.getAtmGroup(userInt);
                    pickValues = new DualListModel<String>(targetList, sourceList);
                }
            }

        }
    }

    public void userChange1(ValueChangeEvent e) throws Throwable {
        message = "";
        atmGInt = (Integer) e.getNewValue();
        if (atmGInt == 0) {
            disabledSave = true;
            sourceList.clear();
        } else {
            if (userInt == null || userInt == 0) {
                userInt = 0;
                disabledSave = true;
                sourceList = (List<String>) mcObject.getAtmGroup(atmGInt);
                pickValues = new DualListModel<String>(targetList, sourceList);
            } else {
                if (userInt == atmGInt) {
                    message = "Select another group";
                    sourceList.clear();
                    disabledSave = true;
                    atmGInt = 0;
                } else {
                    disabledSave = false;
                    sourceList = (List<String>) mcObject.getAtmGroup(atmGInt);
                    pickValues = new DualListModel<String>(targetList, sourceList);
                }
            }
        }
    }

    public void saveChange() throws Throwable {
        targetList = pickValues.getSource();
        sourceList = pickValues.getTarget();
        mcObject.saveUserAtm(userInt, targetList);
        mcObject.saveUserAtm(atmGInt, sourceList);
        message = super.showMessage(SUCC);

    }
      public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }
}
