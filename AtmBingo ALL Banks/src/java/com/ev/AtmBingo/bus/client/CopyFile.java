package com.ev.AtmBingo.bus.client;
import java.io.*;

public class CopyFile  implements Serializable{
	public  void copyfile(String srFile, String dtFile){
		try{
			File f1 = new File(srFile);
			File f2 = new File(dtFile);
			InputStream in = new FileInputStream(f1);
			
			//For Append the file.
//			OutputStream out = new FileOutputStream(f2,true);

			//For Overwrite the file.
			OutputStream out = new FileOutputStream(f2);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0){
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
			
		}
		catch(FileNotFoundException ex){
			
			System.exit(0);
		}
		catch(IOException e){
			
		}
	}

}