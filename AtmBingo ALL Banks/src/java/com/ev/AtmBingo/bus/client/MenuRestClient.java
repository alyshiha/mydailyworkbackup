/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.MenuRestBOInter;
import com.ev.AtmBingo.bus.dto.MenuLabelDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.model.TreeNode;


/**
 *
 * @author Administrator
 */
public class MenuRestClient extends BaseBean  implements Serializable{

    private TreeNode tree;
    private MenuLabelDTOInter selectedMenu;
    private MenuRestBOInter mrBO;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public MenuLabelDTOInter getSelectedMenu() {
        return selectedMenu;
    }

    public void setSelectedMenu(MenuLabelDTOInter selectedMenu) {
        this.selectedMenu = selectedMenu;
    }

    public TreeNode getTree() {
        return tree;
    }

    public void setTree(TreeNode tree) {
        this.tree = tree;
    }
    
    /** Creates a new instance of MenuRestClient */
    public MenuRestClient() {
        super();
        super.GetAccessPage();
        try {
            mrBO = BOFactory.createMenuRestBO(null);
            tree = mrBO.getMenuTree(super.getRestrected());
                 FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        } catch (Throwable ex) {
            Logger.getLogger(MenuRestClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void fillMenuUp(ActionEvent e){
        selectedMenu = (MenuLabelDTOInter) e.getComponent().getAttributes().get("updateRowMenu");
        message ="";
    }

    public void updateAtt(){
        try {
            mrBO.updateMenu(selectedMenu);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            Logger.getLogger(MenuRestClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
