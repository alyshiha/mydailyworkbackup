/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.DefualtColumnsBOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Administrator
 */
public class DefualtColumnClient extends BaseBean  implements Serializable{

    private DualListModel<String> pick;
    private DefualtColumnsBOInter dcBO;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DualListModel<String> getPick() {
        return pick;
    }

    public void setPick(DualListModel<String> pick) {
        this.pick = pick;
    }

    /**
     * Creates a new instance of DefualtColumnClient
     */
    public DefualtColumnClient() {
        super();
        super.GetAccessPage();
        try {
            dcBO = BOFactory.createDefualtColumnsBO(null);
            pick = (DualListModel<String>) dcBO.getPickList();
                 FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        } catch (Throwable ex) {
            Logger.getLogger(DefualtColumnClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void save() {
        try {
            int i = 0;
            String[] inputs = new String[pick.getTarget().size() + pick.getSource().size()];
            for (String item : pick.getTarget()) {
                inputs[i] = item;
                i++;
            }
            for (String item : pick.getSource()) {
                inputs[i] = item;
                i++;
            }
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace("(", "").replace(")", "").replace("#", "").replace(".", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
                dcBO.save(pick.getTarget(), pick.getSource());
                message = super.showMessage(SUCC);
                message = super.showMessage(SUCC);
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
            Logger.getLogger(DefualtColumnClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void resetVars() {
        message = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
