/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.bo.TimeShiftsBOInter;
import com.ev.AtmBingo.bus.dto.TimeShiftDTOInter;
import com.ev.AtmBingo.bus.dto.TimeShiftDetailDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author ISLAM
 */
public class TimeShiftClient extends BaseBean implements Serializable {

    private TimeShiftsBOInter timeShiftObject;
    private List<TimeShiftDTOInter> timeShiftList;
    private List<TimeShiftDetailDTOInter> timeShiftDetailsList;
    private TimeShiftDTOInter timeShiftAttr;
    private TimeShiftDTOInter timeShiftAttr1;
    private TimeShiftDTOInter selectedMT;
    private TimeShiftDTOInter newRecord;
    private TimeShiftDetailDTOInter timeShiftDetailsAttr;
    private Integer myInt;
    private Boolean showAdd;
    private Boolean showConfirm;
    private String message;
    private String message1;

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public TimeShiftDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(TimeShiftDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public TimeShiftDTOInter getTimeShiftAttr1() {
        return timeShiftAttr1;
    }

    public void setTimeShiftAttr1(TimeShiftDTOInter timeShiftAttr1) {
        this.timeShiftAttr1 = timeShiftAttr1;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TimeShiftDTOInter getSelectedMT() {
        return selectedMT;
    }

    public void setSelectedMT(TimeShiftDTOInter selectedMT) {
        this.selectedMT = selectedMT;
    }

    public Integer getMyInt() {
        return myInt;
    }

    public void setMyInt(Integer myInt) {
        this.myInt = myInt;
    }

    public List<TimeShiftDTOInter> getTimeShiftList() {
        return timeShiftList;
    }

    public void setTimeShiftList(List<TimeShiftDTOInter> timeShiftList) {
        this.timeShiftList = timeShiftList;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public TimeShiftDTOInter getTimeShiftAttr() {
        return timeShiftAttr;
    }

    public void setTimeShiftAttr(TimeShiftDTOInter timeShiftAttr) {
        this.timeShiftAttr = timeShiftAttr;
    }

    public TimeShiftDetailDTOInter getTimeShiftDetailsAttr() {
        return timeShiftDetailsAttr;
    }

    public void setTimeShiftDetailsAttr(TimeShiftDetailDTOInter timeShiftDetailsAttr) {
        this.timeShiftDetailsAttr = timeShiftDetailsAttr;
    }

    public List<TimeShiftDetailDTOInter> getTimeShiftDetailsList() {
        return timeShiftDetailsList;
    }

    public void setTimeShiftDetailsList(List<TimeShiftDetailDTOInter> timeShiftDetailsList) {
        this.timeShiftDetailsList = timeShiftDetailsList;
    }

    /**
     * Creates a new instance of TimeShiftClient
     */
    public TimeShiftClient() throws Throwable {

        super();
        super.GetAccessPage();
        timeShiftObject = BOFactory.createTimeShiftBO(null);
        timeShiftList = (List<TimeShiftDTOInter>) timeShiftObject.getTimeShifts();
        showAdd = true;
        showConfirm = false;
        FacesContext context = FacesContext.getCurrentInstance();

        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void onRowSelect1(SelectEvent e) throws Throwable {
        selectedMT = DTOFactory.createTimeShiftDTO();
        selectedMT = (TimeShiftDTOInter) e.getObject();
        timeShiftDetailsList = selectedMT.getTimeShiftDetails();
        message = "";
    }

    public void onRowUnSelect1(UnselectEvent e) throws Throwable {
        timeShiftDetailsList = null;
        message = "";
    }

    public void fillUpdate(ActionEvent e) {
        timeShiftAttr = (TimeShiftDTOInter) e.getComponent().getAttributes().get("updateRow");
        message = "";
    }

    public void updateRecord() {
        try {
            String[] inputs = {timeShiftAttr.getShiftEName()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
                if (!showAdd) {
                    message = super.showMessage(CONF_FIRST);
                } else {
                    if (timeShiftAttr.getShiftAName().equals("") || timeShiftAttr.getShiftEName().equals("")) {
                        message = super.showMessage(REQ_FIELD);
                    } else {
                        timeShiftObject.editeTimeShits(timeShiftAttr, timeShiftObject.UPDATE);
                        message = super.showMessage(SUCC);
                    }
                }
                message = super.showMessage(SUCC);
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void fillDelete(ActionEvent e) {
        timeShiftAttr1 = (TimeShiftDTOInter) e.getComponent().getAttributes().get("removedRow");
        message = "";
    }

    public void deleteRecord() {
        try {
            timeShiftObject.editeTimeShits(timeShiftAttr1, timeShiftObject.DELETE);
            timeShiftList.remove(timeShiftAttr1);
            message = super.showMessage(SUCC);
            showAdd = true;
            showConfirm = false;
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void addTimeShift() {
        newRecord = DTOFactory.createTimeShiftDTO();
        timeShiftList.add(newRecord);
        showAdd = false;
        showConfirm = true;
        message = "";
    }

    public void confirmAdd() {
        try {
            String[] inputs = {newRecord.getShiftEName()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
                if (newRecord.getShiftEName().equals("")) {
                    message = super.showMessage(REQ_FIELD);
                } else {
                    timeShiftObject.editeTimeShits(newRecord, timeShiftObject.INSERT);
                    message = super.showMessage(SUCC);
                    showAdd = true;
                    showConfirm = false;
                }
                message = super.showMessage(SUCC);
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
            message = ex.getMessage();
            showAdd = true;
            showConfirm = false;
            timeShiftList.remove(newRecord);
        }
    }

    public void fillUpdate1(ActionEvent e) {
        timeShiftDetailsAttr = (TimeShiftDetailDTOInter) e.getComponent().getAttributes().get("updateRow1");
        message1 = "";
    }

    public void updateRecord1() {
        try {
            if (timeShiftDetailsAttr.getTimeFrom().after(timeShiftDetailsAttr.getTimeTo())) {
                message1 = super.showMessage(INV_TIME);
            } else {
                timeShiftObject.edteTimeShiftsDetail(timeShiftDetailsAttr, timeShiftDetailsAttr.getDayNo(), timeShiftObject.UPDATE);
                message1 = super.showMessage(SUCC);
            }

        } catch (Throwable ex) {
            message1 = ex.getMessage();
        }
    }

    public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();

    }
}
