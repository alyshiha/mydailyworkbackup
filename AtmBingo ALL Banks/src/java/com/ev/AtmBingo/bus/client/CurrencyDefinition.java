/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.bo.CurrencyDefinitionBOInter;
import com.ev.AtmBingo.bus.dto.CurrencyDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author ISLAM
 */
public class CurrencyDefinition extends BaseBean  implements Serializable{

    private CurrencyDefinitionBOInter cdObject;
    private List<CurrencyMasterDTOInter> cdList;
    private List<CurrencyDTOInter> cdDetailsList;
    private CurrencyMasterDTOInter timeShiftAttr;
    private CurrencyMasterDTOInter timeShiftAttr1;
    private CurrencyMasterDTOInter selectedMT;
    private CurrencyMasterDTOInter newRecord;
    private CurrencyDTOInter newRecord1;
    private CurrencyDTOInter timeShiftDetailsAttr, timeShiftDetailsAttr1;
    private Integer myInt;
    private Boolean showAdd, showAdd1;
    private Boolean showConfirm, showConfirm1;
    private String message;
    private String message1;

    public CurrencyDTOInter getNewRecord1() {
        return newRecord1;
    }

    public void setNewRecord1(CurrencyDTOInter newRecord1) {
        this.newRecord1 = newRecord1;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public CurrencyDTOInter getTimeShiftDetailsAttr1() {
        return timeShiftDetailsAttr1;
    }

    public void setTimeShiftDetailsAttr1(CurrencyDTOInter timeShiftDetailsAttr1) {
        this.timeShiftDetailsAttr1 = timeShiftDetailsAttr1;
    }

    public List<CurrencyDTOInter> getCdDetailsList() {
        return cdDetailsList;
    }

    public void setCdDetailsList(List<CurrencyDTOInter> cdDetailsList) {
        this.cdDetailsList = cdDetailsList;
    }

    public List<CurrencyMasterDTOInter> getCdList() {
        return cdList;
    }

    public void setCdList(List<CurrencyMasterDTOInter> cdList) {
        this.cdList = cdList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public Integer getMyInt() {
        return myInt;
    }

    public void setMyInt(Integer myInt) {
        this.myInt = myInt;
    }

    public CurrencyMasterDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(CurrencyMasterDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public CurrencyMasterDTOInter getSelectedMT() {
        return selectedMT;
    }

    public void setSelectedMT(CurrencyMasterDTOInter selectedMT) {
        this.selectedMT = selectedMT;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public CurrencyMasterDTOInter getTimeShiftAttr() {
        return timeShiftAttr;
    }

    public void setTimeShiftAttr(CurrencyMasterDTOInter timeShiftAttr) {
        this.timeShiftAttr = timeShiftAttr;
    }

    public CurrencyMasterDTOInter getTimeShiftAttr1() {
        return timeShiftAttr1;
    }

    public void setTimeShiftAttr1(CurrencyMasterDTOInter timeShiftAttr1) {
        this.timeShiftAttr1 = timeShiftAttr1;
    }

    public CurrencyDTOInter getTimeShiftDetailsAttr() {
        return timeShiftDetailsAttr;
    }

    public void setTimeShiftDetailsAttr(CurrencyDTOInter timeShiftDetailsAttr) {
        this.timeShiftDetailsAttr = timeShiftDetailsAttr;
    }

    /**
     * Creates a new instance of CurrencyDefinition
     */
    public CurrencyDefinition() throws Throwable {
        super();
        super.GetAccessPage();
        cdObject = BOFactory.createCurrencyDefBO(null);
        cdList = (List<CurrencyMasterDTOInter>) cdObject.getCurrencyMaster();
        showAdd = true;
        showConfirm = false;
        showAdd1 = false;
        showConfirm1 = false;
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void onRowSelect1(SelectEvent e) throws Throwable {
        selectedMT = DTOFactory.createCurrencyMasterDTO();
        selectedMT = (CurrencyMasterDTOInter) e.getObject();
        cdDetailsList = (List<CurrencyDTOInter>) cdObject.getCurrency(selectedMT);
        resetVars();
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void onRowUnSelect1(UnselectEvent e) throws Throwable {
        cdDetailsList = null;
        resetVars();
        showAdd1 = false;
        showConfirm1 = false;
    }

    public void fillUpdate(ActionEvent e) {
        timeShiftAttr = (CurrencyMasterDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    public void updateRecord() {
        try {
            if (!showAdd) {
                message = super.showMessage(CONF_FIRST);
            } else {
                if (timeShiftAttr.getName().equals("")) {
                    message = super.showMessage(REQ_FIELD);
                } else {
                    int count = cdObject.CountCurrencyMaster();
                    if (timeShiftAttr.getDefcurr() == true) {
                        count++;
                    }
                    if (count < 2) {
                        String[] inputs = {timeShiftAttr.getName(), timeShiftAttr.getSymbol(), "" + timeShiftAttr.getDefcurr(), "" + timeShiftAttr.getId()};
                        String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                        Boolean found = Boolean.FALSE;
                        for (String input : inputs) {
                            boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                            if (b == false) {
                                found = Boolean.TRUE;
                            }
                            if (!"".equals(input)) {
                                for (String validate1 : validate) {
                                    if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                        found = Boolean.TRUE;
                                    }
                                }
                            }
                        }
                        if (!found) {
                            cdObject.editeCurrencyMaster(timeShiftAttr, cdObject.UPDATE);
                            message = super.showMessage(SUCC);
                            if (cdDetailsList != null) {
                                cdDetailsList.clear();
                            }
                            message = super.showMessage(SUCC);
                            showAdd = true;
                            showConfirm = false;
                        } else {
                            message = "Please Enter Valid Data";
                        }

                    } else {
                        message = "Only one default currency";
                    }
                }
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void fillDelete(ActionEvent e) {
        timeShiftAttr1 = (CurrencyMasterDTOInter) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void deleteRecord() {
        try {
            if (cdDetailsList == null || cdDetailsList.size() == 0) {
                String[] inputs = {timeShiftAttr1.getName(), timeShiftAttr1.getSymbol(), "" + timeShiftAttr1.getDefcurr(), "" + timeShiftAttr1.getId()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    cdObject.editeCurrencyMaster(timeShiftAttr1, cdObject.DELETE);
                    cdList.remove(timeShiftAttr1);
                    if (cdDetailsList != null) {
                        cdDetailsList.clear();
                    }
                    message = super.showMessage(SUCC);
                    showAdd = true;
                    showConfirm = false;
                } else {
                    message = "Please Enter Valid Data";
                }

            } else {
                message = "Delete the abbreviation first";
            }
        } catch (Throwable ex) {
            message = super.showMessage(CNT_DEL);

        }
    }

    public void addTimeShift() {
        newRecord = DTOFactory.createCurrencyMasterDTO();
        cdList.add(newRecord);
        showAdd = false;
        showConfirm = true;
        resetVars();
    }

    public void confirmAdd() {
        try {
            if (newRecord.getName().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                int count = cdObject.CountCurrencyMaster();
                if (Boolean.TRUE == newRecord.getDefcurr()) {
                    count++;
                }
                if (count < 2) {
                    String[] inputs = {newRecord.getName(), newRecord.getSymbol(), "" + newRecord.getDefcurr(), "" + newRecord.getId()};
                    String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                    Boolean found = Boolean.FALSE;
                    for (String input : inputs) {
                        boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                        if (b == false) {
                            found = Boolean.TRUE;
                        }
                        if (!"".equals(input)) {
                            for (String validate1 : validate) {
                                if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                    found = Boolean.TRUE;
                                }
                            }
                        }
                    }
                    if (!found) {
                        cdObject.editeCurrencyMaster(newRecord, cdObject.INSERT);
                        message = super.showMessage(SUCC);
                        showAdd = true;
                        showConfirm = false;
                    } else {
                        message = "Please Enter Valid Data";
                    }

                } else {
                    message = "Only one default currency";
                }

            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void fillUpdate1(ActionEvent e) {
        timeShiftDetailsAttr = (CurrencyDTOInter) e.getComponent().getAttributes().get("updateRow1");
        resetVars();
    }

    public void updateRecord1() {
        try {
            if (!showAdd1) {
                message1 = super.showMessage(CONF_FIRST);
            } else if (timeShiftDetailsAttr.getAbbreviation().equals("")) {
                message1 = super.showMessage(REQ_FIELD);
            } else {
                int count = cdObject.CountCurrencydetail(timeShiftDetailsAttr.getMasterId());
                if (count < 2) {
                    String[] inputs = {"" + timeShiftDetailsAttr.getId(), "" + timeShiftDetailsAttr.getMasterId(), "" + timeShiftDetailsAttr.getDefcurr(), timeShiftDetailsAttr.getAbbreviation()};
                    String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                    Boolean found = Boolean.FALSE;
                    for (String input : inputs) {
                        boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                        if (b == false) {
                            found = Boolean.TRUE;
                        }
                        if (!"".equals(input)) {
                            for (String validate1 : validate) {
                                if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                    found = Boolean.TRUE;
                                }
                            }
                        }
                    }
                    if (!found) {
                        cdObject.editeCurrency(timeShiftDetailsAttr, cdObject.UPDATE);
                        message1 = super.showMessage(SUCC);
                    } else {
                        message = "Please Enter Valid Data";
                    }
                } else {
                    message1 = "Can't insert more than one default flag";
                }
            }

        } catch (Throwable ex) {
            message1 = super.showMessage(REC_EXIST);
        }
    }

    public void fillDelete1(ActionEvent e) {
        timeShiftDetailsAttr1 = (CurrencyDTOInter) e.getComponent().getAttributes().get("removedRow1");
        resetVars();
    }

    public void deleteRecord1() {
        try {
            String[] inputs = {"" + timeShiftDetailsAttr1.getId(), "" + timeShiftDetailsAttr1.getMasterId(), "" + timeShiftDetailsAttr1.getDefcurr(), timeShiftDetailsAttr1.getAbbreviation()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
                cdObject.editeCurrency(timeShiftDetailsAttr1, cdObject.DELETE);
                cdDetailsList.remove(timeShiftDetailsAttr1);
                message1 = super.showMessage(SUCC);
                showAdd1 = true;
                showConfirm1 = false;
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
            message1 = super.showMessage(CNT_DEL);
        }
    }

    public void addTimeShift1() {
        newRecord1 = DTOFactory.createCurrencyDTO();
        cdDetailsList.add(newRecord1);
        showAdd1 = false;
        showConfirm1 = true;
        resetVars();
    }

    public void confirmAdd1() {
        try {
            if (newRecord1.getAbbreviation().equals("")) {
                message1 = super.showMessage(REQ_FIELD);
            } else {

                newRecord1.setMasterId(selectedMT.getId());
                int count = cdObject.CountCurrencydetail(newRecord1.getMasterId());
                if (newRecord1.getDefcurr() == true) {
                    count++;
                }
                if (count < 2) {
                    String[] inputs = {"" + newRecord1.getId(), "" + newRecord1.getMasterId(), "" + newRecord1.getDefcurr(), newRecord1.getAbbreviation()};
                    String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                    Boolean found = Boolean.FALSE;
                    for (String input : inputs) {
                        boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                        if (b == false) {
                            found = Boolean.TRUE;
                        }
                        if (!"".equals(input)) {
                            for (String validate1 : validate) {
                                if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                    found = Boolean.TRUE;
                                }
                            }
                        }
                    }
                    if (!found) {
                        cdObject.editeCurrency(newRecord1, cdObject.INSERT);
                        message1 = super.showMessage(SUCC);
                        showAdd1 = true;
                        showConfirm1 = false;
                    } else {
                        message1 = "Please Enter Valid Data";
                    }

                } else {
                    message1 = "Can't insert more than one default flag";
                }
            }
        } catch (Throwable ex) {
            message1 = super.showMessage(REC_EXIST);
        }
    }

    public void resetVars() {
        message = "";
        message1 = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
