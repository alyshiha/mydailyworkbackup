
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.menulabelsBOInter;
import com.ev.AtmBingo.bus.bo.privilegeStatusNameBOInter;
import com.ev.AtmBingo.bus.bo.userPrivilegeBOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.menulabelsDTOInter;
import com.ev.AtmBingo.bus.dto.privilegeStatusNameDTOInter;
import com.ev.AtmBingo.bus.dto.userPrivilegeDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Aly
 */
@ManagedBean(name = "userPrivilegeView")
@ViewScoped
public class userPrivilegeClient extends BaseBean implements Serializable {

    private List<userPrivilegeDTOInter> detailrecordsList;
    private List<UsersDTOInter> recordsList;
    private UsersDTOInter selectedRecord;
    private userPrivilegeDTOInter detailselectedRecord;
    private userPrivilegeDTOInter newRecord;
    private int listcount;
    private int detaillistcount;
    private userPrivilegeBOInter engin;
    private Session utillist = new Session();
    private List<privilegeStatusNameDTOInter> privilegeList;
    private List<menulabelsDTOInter> menuLabelList;
    private menulabelsBOInter enginmenuLabel;
    private List<String> privelageList;
    private privilegeStatusNameBOInter enginprivilege;

    public List<userPrivilegeDTOInter> getDetailrecordsList() {
        return detailrecordsList;
    }

    public List<privilegeStatusNameDTOInter> getPrivilegeList() {
        return privilegeList;
    }

    public void setPrivilegeList(List<privilegeStatusNameDTOInter> privilegeList) {
        this.privilegeList = privilegeList;
    }

    public void setDetailrecordsList(List<userPrivilegeDTOInter> detailrecordsList) {
        this.detailrecordsList = detailrecordsList;
    }

    public List<UsersDTOInter> getRecordsList() {
        return recordsList;
    }

    public userPrivilegeDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(userPrivilegeDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public void setRecordsList(List<UsersDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public UsersDTOInter getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(UsersDTOInter selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public userPrivilegeDTOInter getDetailselectedRecord() {
        return detailselectedRecord;
    }

    public void setDetailselectedRecord(userPrivilegeDTOInter detailselectedRecord) {
        this.detailselectedRecord = detailselectedRecord;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public int getDetaillistcount() {
        return detaillistcount;
    }

    public void setDetaillistcount(int detaillistcount) {
        this.detaillistcount = detaillistcount;
    }

    public userPrivilegeBOInter getEngin() {
        return engin;
    }

    public void setEngin(userPrivilegeBOInter engin) {
        this.engin = engin;
    }

    public Session getUtillist() {
        return utillist;
    }

    public List<menulabelsDTOInter> getMenuLabelList() {
        return menuLabelList;
    }

    public void setMenuLabelList(List<menulabelsDTOInter> menuLabelList) {
        this.menuLabelList = menuLabelList;
    }

    public menulabelsBOInter getEnginmenuLabel() {
        return enginmenuLabel;
    }

    public void setEnginmenuLabel(menulabelsBOInter enginmenuLabel) {
        this.enginmenuLabel = enginmenuLabel;
    }

    public privilegeStatusNameBOInter getEnginprivilege() {
        return enginprivilege;
    }

    public void setEnginprivilege(privilegeStatusNameBOInter enginprivilege) {
        this.enginprivilege = enginprivilege;
    }

    public void setUtillist(Session utillist) {
        this.utillist = utillist;
    }

    @PostConstruct
    public void init() {
        try {
            engin = BOFactory.createuserPrivilegeBO(null);
            enginprivilege = BOFactory.createprivilegeStatusNameBO(null);
            enginmenuLabel = BOFactory.createmenulabelsBO();
            recordsList = utillist.GetUsersList();
            detailrecordsList = new ArrayList<userPrivilegeDTOInter>();
            privilegeList = enginprivilege.findAll();
            menuLabelList = (List<menulabelsDTOInter>) enginmenuLabel.GetAllRecords();
            selectedRecord = DTOFactory.createUsersDTO();
            privelageList = super.findPrivelage(utillist.GetUserLogging().getUserId(), "");
            listcount = recordsList.size();
            detaillistcount = 0;

        } catch (Throwable ex) {
            Logger.getLogger(NetworksClient.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void handleMasterChange() throws Throwable {
        if (selectedRecord != null) {
            detailrecordsList = engin.findRecord("" + selectedRecord.getUserId());
            detailselectedRecord = null;
            detaillistcount = detailrecordsList.size();
        }
    }

    public String getPageName(Integer ID) {
        for (menulabelsDTOInter record : menuLabelList) {
            if (record.getmenuid() == ID) {
                return record.getenglishlabel();
            }
        }
        return "UnKnown";
    }

    public String getprivilegeName(Integer ID) {
        for (privilegeStatusNameDTOInter record : privilegeList) {
            if (record.getPrev_id() == ID) {
                return record.getSymbol();
            }
        }
        return "UnKnown";
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printreportPDF() throws Exception, Throwable {
        String path = (String) engin.printPDFrep(utillist.Getclient(), super.getLoggedInUser().getUserName(), selectedRecord.getUserName(), selectedRecord.getUserId());
        prepDownload(path);
    }

    public Boolean updatePriv() {
        return super.findPrivelageOperation(privelageList, "UPDATE");
    }

    public void updateRecord() throws SQLException, Throwable {
        String message = "";
        if (!engin.findDuplicateRecord(selectedRecord.getUserId(), detailselectedRecord.getMenuid(), detailselectedRecord.getPrevid(), detailselectedRecord.getUserprevid())) {
            message = engin.updateRecord(detailselectedRecord);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("carDialog.hide();");
        } else {
            message = "Please Select Unique Page & Privlage";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean insertPriv() {
        return super.findPrivelageOperation(privelageList, "INSERT");
    }

    public void addRecord() {
        newRecord = DTOFactory.createuserPrivilegeDTO();
    }

    public Boolean deletePriv() {
        return super.findPrivelageOperation(privelageList, "DELETE");
    }

    public void deleteRecord() throws SQLException {
        String message = engin.deleteRecord(detailselectedRecord);
        detailrecordsList.remove(detailselectedRecord);
        detaillistcount = detailrecordsList.size();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public void saveRecord() throws SQLException, Throwable {
        String message = "";
        if (!engin.findDuplicateRecord(selectedRecord.getUserId(), newRecord.getMenuid(), newRecord.getPrevid())) {
            newRecord.setUserid(selectedRecord.getUserId());
            message = engin.insertRecord(newRecord);
            detailrecordsList.add(newRecord);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("carDialog2.hide();");
            detaillistcount = detailrecordsList.size();
        } else {
            message = "Please Select Unique Page & Privlage";
        }

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

}
