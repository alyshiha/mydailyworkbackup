/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.BlockReasonBOInter;

import com.ev.AtmBingo.bus.dto.BlockReasonDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.faces.event.ActionEvent;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class BlockReasonClient extends BaseBean  implements Serializable{

    private List<BlockReasonDTOInter> brList;
    private BlockReasonBOInter brObject;
    private BlockReasonDTOInter brAttr;
    private BlockReasonDTOInter newBlock;
    private String message;
    private Boolean showAdd;
    private Boolean showConfirm;
    private Integer listSize;

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

// <editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public List<BlockReasonDTOInter> getBrList() {
        return brList;
    }

    public void setBrList(List<BlockReasonDTOInter> brList) {
        this.brList = brList;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    // </editor-fold>
    /**
     * Creates a new instance of BlockReasonClient
     */
    public BlockReasonClient() throws Throwable {
        super();
        super.GetAccessPage();
        brObject = BOFactory.createBlockReasonBO(null);
        brList = (List<BlockReasonDTOInter>) brObject.getBlockReasons();
        showAdd = true;
        showConfirm = false;
        listSize = brList.size();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void fillUp(ActionEvent e) {
        brAttr = (BlockReasonDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    public void updateBlockReason(ActionEvent e) throws Throwable {
        try {
            if (showAdd == false) {
                message = super.showMessage(CONF_FIRST);
            } else if (brAttr.getReasonAName() == "" || brAttr.getReasonName() == "") {

                message = super.showMessage(REQ_FIELD);
            } else {
                String[] inputs = {brAttr.getReasonAName(), brAttr.getReasonName(), "" + brAttr.getReasonId()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9\\u0600-\\u06FF]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    brObject.editeBlockReason(brAttr, brObject.UPDATE);
                    message = super.showMessage(SUCC);
                } else {
                    message = "Please Enter Valid Data";
                }

            }
        } catch (Throwable ex) {
            message = "Error, please repeat your changes";
        }
    }

    public void resetVars() {
        message = "";
    }

    public void fillDel(ActionEvent e) {
        brAttr = (BlockReasonDTOInter) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void deleteBlockReason(ActionEvent e) throws Throwable {
        try {
            if (brAttr.getReasonAName() == "" || brAttr.getReasonName() == "") {
                message = super.showMessage(SUCC);
                brList.remove(brAttr);
            } else {
                String[] inputs = {brAttr.getReasonAName(), brAttr.getReasonName(), "" + brAttr.getReasonId()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9\\u0600-\\u06FF]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    brList.remove(brAttr);
                    brObject.editeBlockReason(brAttr, brObject.DELETE);
                    message = super.showMessage(SUCC);
                } else {
                    message = "Please Enter Valid Data";
                }

            }
            showAdd = true;
            showConfirm = false;
        } catch (Throwable ex) {
            message = "Error, please delete again";
        }
    }

    public void AddBlockReason() {
        resetVars();
        if (brList == null) {
            brList = new ArrayList<BlockReasonDTOInter>();
        }
        newBlock = DTOFactory.createBlockReasonDTO();
        brList.add(newBlock);
        showAdd = false;
        showConfirm = true;
    }

    public void ConfirmBlockReason() {
        try {
            if (newBlock.getReasonAName() == "" || newBlock.getReasonName() == "") {
                message = super.showMessage(REQ_FIELD);
            } else {
                String[] inputs = {newBlock.getReasonAName(), newBlock.getReasonName(), "" + newBlock.getReasonId()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9\\u0600-\\u06FF]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    brObject.editeBlockReason(newBlock, brObject.INSERT);
                    message = super.showMessage(SUCC);
                    showAdd = true;
                    showConfirm = false;
                } else {
                    message = "Please Enter Valid Data";
                }

            }
        } catch (Throwable ex) {
            message = "Your record can't be added, please try again";
        }
    }
      public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }
}
