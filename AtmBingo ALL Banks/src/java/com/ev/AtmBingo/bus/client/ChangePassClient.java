/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import static com.ev.AtmBingo.base.client.BaseBean.SUCC;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.ChangePassBOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.SystemTableDAOInter;
import com.ev.AtmBingo.bus.dto.SystemTableDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class ChangePassClient extends BaseBean implements Serializable {

    private ChangePassBOInter cpBO;
    private UsersDTOInter selectedUDTO;
    private List<UsersDTOInter> uDTOL;
    private String oldPass;
    private String confPass, newPass;
    private Integer userInt;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getUserInt() {
        return userInt;
    }

    public void setUserInt(Integer userInt) {
        this.userInt = userInt;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getConfPass() {
        return confPass;
    }

    public void setConfPass(String confPass) {
        this.confPass = confPass;
    }

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public UsersDTOInter getSelectedUDTO() {
        return selectedUDTO;
    }

    public void setSelectedUDTO(UsersDTOInter selectedUDTO) {
        this.selectedUDTO = selectedUDTO;
    }

    public List<UsersDTOInter> getuDTOL() {
        return uDTOL;
    }

    public void setuDTOL(List<UsersDTOInter> uDTOL) {
        this.uDTOL = uDTOL;
    }

    /**
     * Creates a new instance of ChangePassClient
     */
    public ChangePassClient() {
        super();
        try {
            super.GetAccessPage();
            userInt = 0;
            cpBO = BOFactory.createChangePassBO(null);
            uDTOL = (List<UsersDTOInter>) cpBO.getUsers(super.getRestrected());
            oldPass = "";
            FacesContext context = FacesContext.getCurrentInstance();

            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.addHeader("X-Frame-Options", "SAMEORIGIN");
        } catch (Throwable ex) {
            Logger.getLogger(ChangePassClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void changeUser(ValueChangeEvent e) {
        int id = (Integer) e.getNewValue();
        try {
            selectedUDTO = cpBO.getSelected(id);
        } catch (Throwable ex) {
            Logger.getLogger(ChangePassClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void save() {
        try {
            SystemTableDAOInter stDAO = DAOFactory.createSystemTableDAO(null);
            SystemTableDTOInter stDTO = null;
            try {
                stDTO = (SystemTableDTOInter) stDAO.find("PASSLENGTH");

            } catch (Throwable ex) {
                Logger.getLogger(MainClient.class.getName()).log(Level.SEVERE, null, ex);
            }

            String matchinfo = "(?=^.{@passlength,}$)(?=.*\\d)(?=.*\\W+)(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$";
            matchinfo = matchinfo.replace("@passlength", stDTO.getParameterValue());
            if (!newPass.equals(confPass)) {
                message = "New Password and Confirm Password should be the same";

            } else if (!newPass.matches(matchinfo)) {
                message = "The input password volates the policy, please make sure that your password follows the following rules:"
                        + "Password length is "
                        + stDTO.getParameterValue()
                        + "characters or more."
                        + "It contains one or more capital letter(s)."
                        + "It contains a combination of characters, numbers and special characters.";
            } else {
                stDTO = (SystemTableDTOInter) stDAO.find("WRONG PASS NUMBER");
                if (oldPass.equals(newPass)) {
                    message = "New Password must not be equal your last "
                            + "passwords";
                } else if (cpBO.validatePassword(newPass, selectedUDTO.getUserId())) {
                    message = "New Password must not be equal the last "
                            + stDTO.getParameterValue()
                            + "passwords";
                } else {
                    cpBO.updatePass(selectedUDTO, confPass, newPass);

                    message = "Done";
                }
            }

        } catch (Throwable ex) {
            message = ex.getMessage();
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void resetVars() {
        message = "";
    }

    public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();

    }
}
