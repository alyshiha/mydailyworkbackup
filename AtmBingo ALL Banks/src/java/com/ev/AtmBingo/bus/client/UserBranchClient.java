
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.branchcodeBoInter;
import com.ev.AtmBingo.bus.bo.usersbranchBOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.branchcodeDTOInter;
import com.ev.AtmBingo.bus.dto.usersbranchDTOInter;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class UserBranchClient extends BaseBean  implements Serializable{

    private branchcodeBoInter branchObject;
    private List<branchcodeDTOInter> branchList;
    private usersbranchBOInter brObject;
    private List<usersbranchDTOInter> brList;
    private Boolean showAdd, showConfirm;
    private Integer listSize;
    private String message, eName, uName, password, confirmPassword, branchid;
    private usersbranchDTOInter brAttr, brAttrUp, newBlock;

    public usersbranchBOInter getBrObject() {
        return brObject;
    }

    public branchcodeBoInter getBranchObject() {
        return branchObject;
    }

    public void setBranchObject(branchcodeBoInter branchObject) {
        this.branchObject = branchObject;
    }

    public List<branchcodeDTOInter> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<branchcodeDTOInter> branchList) {
        this.branchList = branchList;
    }

    public String getBranchid() {
        return branchid;
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }

    public void setBrObject(usersbranchBOInter brObject) {
        this.brObject = brObject;
    }

    public List<usersbranchDTOInter> getBrList() {
        return brList;
    }

    public void setBrList(List<usersbranchDTOInter> brList) {
        this.brList = brList;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public usersbranchDTOInter getBrAttr() {
        return brAttr;
    }

    public void setBrAttr(usersbranchDTOInter brAttr) {
        this.brAttr = brAttr;
    }

    public usersbranchDTOInter getBrAttrUp() {
        return brAttrUp;
    }

    public void setBrAttrUp(usersbranchDTOInter brAttrUp) {
        this.brAttrUp = brAttrUp;
    }

    public usersbranchDTOInter getNewBlock() {
        return newBlock;
    }

    public void setNewBlock(usersbranchDTOInter newBlock) {
        this.newBlock = newBlock;
    }

    public UserBranchClient() throws Throwable {
        super();
        super.GetAccessPage();
        brObject = BOFactory.createusersbranchBO();
        branchObject = BOFactory.createbranchcodeBO();
        branchList = (List<branchcodeDTOInter>) branchObject.GetAllRecords();
        brList = (List<usersbranchDTOInter>) brObject.GetAllRecords();
        showAdd = true;
        showConfirm = false;
        listSize = brList.size();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void fillDel(ActionEvent e) {
        brAttr = (usersbranchDTOInter) e.getComponent().getAttributes().get("removedRow");
    }

    public boolean FindUserName(String userName) {
        for (usersbranchDTOInter User : brList) {
            if (User.getlogonname().toUpperCase().equals(userName)) {
                return true;
            }
        }
        return false;
    }

    public void deleteBlockReason() throws Throwable {
        message = (String) brObject.deleterecord(brAttr);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
        if (message.contains("Record Has Been Deleted")) {
            brList.remove(brAttr);
            showAdd = true;
            showConfirm = false;
            brList = (List<usersbranchDTOInter>) brObject.GetAllRecords();
        }
    }

    public void resetVars() {
        eName = "";
        uName = "";
        password = "";
        confirmPassword = "";

    }

    public void saveAll() throws Throwable {
        message = (String) brObject.save(brList);
        message = "Records are Updated";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));

    }

    public void confirmInsert() throws Throwable {

        newBlock = DTOFactory.createusersbranchDTO();
        if (!password.equals(confirmPassword)) {
            message = "Passwords don't match";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } else if (FindUserName(uName.toUpperCase()) == true) {
            message = "User Name aleady Found";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } else {
            newBlock.setusername(eName);
            newBlock.setlogonname(uName);
            newBlock.setuserpassword(password);
            newBlock.setbranchid(Integer.valueOf(branchid));
            message = (String) brObject.insertrecord(newBlock);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
            if (message.contains("Record Has Been Inserted"));
            {
                brList.add(0, newBlock);
                eName = "";
                uName = "";
                password = "";
            }
        }
        brList = (List<usersbranchDTOInter>) brObject.GetAllRecords();
    }
}
