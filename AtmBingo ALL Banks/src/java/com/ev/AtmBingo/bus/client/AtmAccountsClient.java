/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.AccountNameBOInter;
import com.ev.AtmBingo.bus.bo.AtmAccountsBOInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.AccountNameDTOInter;
import com.ev.AtmBingo.bus.dto.AtmAccountsDTOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.NetworksDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Aly
 */
@ManagedBean(name = "atmAccountsClientView")
@ViewScoped
public class AtmAccountsClient extends BaseBean implements Serializable {

    private AtmAccountsDTOInter selectedRecord, newRecord;
    private List<AtmAccountsDTOInter> recordsList;
    private List<AtmMachineDTOInter> atmrecordsList;
    private List<AccountNameDTOInter> accountnameList;
    private AccountNameBOInter accountnameengin;
    private int listcount;
    private String accountid;
    private String branch;
    private int accountname;
    private int atmid;
    private AtmAccountsBOInter engin;
    private Session sessionutil;
    private int atmgroup;
    private List<AtmGroupDTOInter> atmGroupList;
    private List<String> branchList;
    private List<String> privelageList;

    public int getAtmgroup() {
        return atmgroup;
    }

    public void setAtmgroup(int atmgroup) {
        this.atmgroup = atmgroup;
    }

    public List<AtmGroupDTOInter> getAtmGroupList() {
        return atmGroupList;
    }

    public void setAtmGroupList(List<AtmGroupDTOInter> atmGroupList) {
        this.atmGroupList = atmGroupList;
    }

    public AtmAccountsDTOInter getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(AtmAccountsDTOInter selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public AtmAccountsDTOInter getNewRecord() {
        return newRecord;
    }

    public List<AccountNameDTOInter> getAccountnameList() {
        return accountnameList;
    }

    public void setAccountnameList(List<AccountNameDTOInter> accountnameList) {
        this.accountnameList = accountnameList;
    }

    public AccountNameBOInter getAccountnameengin() {
        return accountnameengin;
    }

    public void setAccountnameengin(AccountNameBOInter accountnameengin) {
        this.accountnameengin = accountnameengin;
    }

    public void setNewRecord(AtmAccountsDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public List<AtmAccountsDTOInter> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<AtmAccountsDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public List<AtmMachineDTOInter> getAtmrecordsList() {
        return atmrecordsList;
    }

    public void setAtmrecordsList(List<AtmMachineDTOInter> atmrecordsList) {
        this.atmrecordsList = atmrecordsList;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public String getAccountid() {
        return accountid;
    }

    public void setAccountid(String accountid) {
        this.accountid = accountid;
    }

    public int getAccountname() {
        return accountname;
    }

    public void setAccountname(int accountname) {
        this.accountname = accountname;
    }

    public int getAtmid() {
        return atmid;
    }

    public void setAtmid(int atmid) {
        this.atmid = atmid;
    }

    public AtmAccountsBOInter getEngin() {
        return engin;
    }

    public void setEngin(AtmAccountsBOInter engin) {
        this.engin = engin;
    }

    public Session getSessionutil() {
        return sessionutil;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public List<String> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<String> branchList) {
        this.branchList = branchList;
    }

    public void setSessionutil(Session sessionutil) {
        this.sessionutil = sessionutil;
    }

    @PostConstruct
    public void init() {
        try {
            branch = "All";
            accountid = "";
            accountname = 0;
            atmid = 0;
            engin = BOFactory.createAtmAccountsBO(null);
            accountnameengin = BOFactory.createAccountName(null);
            sessionutil = new Session();
            UsersDTOInter currentUser = sessionutil.GetUserLogging();
            atmgroup = 0;
            branchList = (List<String>) engin.getBranchs();
            atmGroupList = (List<AtmGroupDTOInter>) sessionutil.GetATMGroupAssignList(currentUser.getUserId());
            atmrecordsList = (List<AtmMachineDTOInter>) sessionutil.GetAtmMachineList(currentUser);
            accountnameList = (List<AccountNameDTOInter>) accountnameengin.getNetworks();
            recordsList = engin.getAtmAccount(""+currentUser.getUserId());
            listcount = recordsList.size();
            privelageList = super.findPrivelage(sessionutil.GetUserLogging().getUserId(), "AtmAccount.xhtml");
        } catch (Throwable ex) {
            Logger.getLogger(NetworksClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public void searchrecords() {
        try {
            recordsList = engin.getAtmAccountrecord(atmid, accountid, "" + accountname, atmgroup, branch);
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(NetworksClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean insertPriv() {
        return super.findPrivelageOperation(privelageList, "INSERT");
    }

    public void addRecord() {
        newRecord = DTOFactory.createAtmAccountsDTO();
    }

    public void saveRecord() throws SQLException, Throwable {
        String message = "";
        if (ValidateAccount(newRecord.getAccountid())) {

            if (!newRecord.getAccountnameid().equals(0)) {
                
                    newRecord.setAtmapplicationid(getATMName(newRecord.getAtmid()));
                
                message = engin.insertAtmAccount(newRecord);
                if (message.contains("Successfully")) {
                    recordsList.add(newRecord);
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.execute("carDialog2.hide();");
                    searchrecords();
                }
                listcount = recordsList.size();
            } else {
                message = "Please Select Account";
            }

        } else {
            message = "Invalid Account Pattern";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean updatePriv() {
        return super.findPrivelageOperation(privelageList, "UPDATE");
    }

    public void updateRecord() throws SQLException, Throwable {
        String message = "";
        if (ValidateAccount(selectedRecord.getAccountid())) {

            if (!selectedRecord.getAccountnameid().equals(0)) {
               
                selectedRecord.setAtmapplicationid(getATMName(selectedRecord.getAtmid()));
                message = engin.updateAtmAccount(selectedRecord);
                if (message.contains("Successfully")) {
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.execute("carDialog.hide();");
                    searchrecords();
                }
            } else {
                message = "Please Select Account";
            }

        } else {
            message = "Invalid Account Pattern";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public String getaccountnamebyid(Integer id) {
        for (AccountNameDTOInter record : accountnameList) {
            if (record.getId().equals(id)) {
                return record.getSymbol();
            }
        }
        return "UnAssigned";
    }

    public String getbranchid(String AccountName) {
        return "EG001" + AccountName.substring(AccountName.length() - 4);
    }

    public Boolean deletePriv() {
        return super.findPrivelageOperation(privelageList, "DELETE");
    }

    public void deleteRecord() throws SQLException {
        String message = engin.deleteAtmAccount(selectedRecord);
        if (message.contains("Successfully")) {
            recordsList.remove(selectedRecord);
            listcount = recordsList.size();
            searchrecords();
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public String getATMName(Integer ID) {
        for (AtmMachineDTOInter atmrecord : atmrecordsList) {
            if (atmrecord.getId() == ID) {
                return atmrecord.getApplicationId();
            }
        }
        return "UnAssigned";
    }

    public String getATMGroup(Integer ID) {
        for (AtmGroupDTOInter record : atmGroupList) {
            if (record.getId() == ID) {
                return record.getName();
            }
        }
        return "UnAssigned";
    }

    public Boolean ValidateAccount(String AccountNumber) {
        if (AccountNumber.trim().length() == 16) {
            if (AccountNumber.trim().substring(0, 3).matches("[a-zA-Z]*")) {
                if (AccountNumber.trim().substring(3, 16).matches("\\d+")) {
                    return Boolean.TRUE;
                }
            }
        }
        return Boolean.FALSE;
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printreportPDF() throws Exception, Throwable {

        String path = (String) engin.printPDFrep(sessionutil.Getclient(), super.getLoggedInUser().getUserName(), atmid, atmgroup, accountname, accountid, getATMName(atmid), getATMGroup(atmgroup), getaccountnamebyid(accountname), branch);
        prepDownload(path);
    }
}
