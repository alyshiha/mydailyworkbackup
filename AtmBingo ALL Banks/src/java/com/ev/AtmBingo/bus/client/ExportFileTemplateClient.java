/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.ColumnsDefinitionBOInter;
import com.ev.AtmBingo.bus.bo.DisputeRulesBOInter;
import com.ev.AtmBingo.bus.dto.EXPORTTEMPLETEDTOInter;
import com.ev.AtmBingo.bus.dto.EXPORTTEMPLETEDETAILSDTOInter;
import com.ev.AtmBingo.bus.bo.ExportTemplateBoInter;
import com.ev.AtmBingo.bus.bo.ExportTemplateDetailBoInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.FileColumnDefinitionDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Administrator
 */
public class ExportFileTemplateClient extends BaseBean  implements Serializable{

    private List<EXPORTTEMPLETEDTOInter> exportList;
    private List<EXPORTTEMPLETEDETAILSDTOInter> exportDetailList;
    private List<FileColumnDefinitionDTOInter> columList;
    private EXPORTTEMPLETEDTOInter updateAttr, deleteAttr, newRecord, selectedTemp;
    private DisputeRulesBOInter columnObject;
    private ColumnsDefinitionBOInter columnwKeda;
    private EXPORTTEMPLETEDETAILSDTOInter updateAttrDetails, deleteAttrDetails, newRecordDetails;
    private ExportTemplateBoInter myObject;
    private ExportTemplateDetailBoInter myObjectDetail;
    private String message, messageDetail;
    private Boolean showConfirm, showAdd, showConfirmDetail, showAddDetail, activeFlag;

    public EXPORTTEMPLETEDTOInter getSelectedTemp() {
        return selectedTemp;
    }

    public void setSelectedTemp(EXPORTTEMPLETEDTOInter selectedTemp) {
        this.selectedTemp = selectedTemp;
    }

    

    public Boolean getActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(Boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    public List<FileColumnDefinitionDTOInter> getColumList() {
        return columList;
    }

    public void setColumList(List<FileColumnDefinitionDTOInter> columList) {
        this.columList = columList;
    }

    public List<EXPORTTEMPLETEDETAILSDTOInter> getExportDetailList() {
        return exportDetailList;
    }

    public void setExportDetailList(List<EXPORTTEMPLETEDETAILSDTOInter> exportDetailList) {
        this.exportDetailList = exportDetailList;
    }

    public String getMessageDetail() {
        return messageDetail;
    }

    public void setMessageDetail(String messageDetail) {
        this.messageDetail = messageDetail;
    }

    public Boolean getShowAddDetail() {
        return showAddDetail;
    }

    public void setShowAddDetail(Boolean showAddDetail) {
        this.showAddDetail = showAddDetail;
    }

    public Boolean getShowConfirmDetail() {
        return showConfirmDetail;
    }

    public void setShowConfirmDetail(Boolean showConfirmDetail) {
        this.showConfirmDetail = showConfirmDetail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public List<EXPORTTEMPLETEDTOInter> getExportList() {
        return exportList;
    }

    public void setExportList(List<EXPORTTEMPLETEDTOInter> exportList) {
        this.exportList = exportList;
    }

    /** Creates a new instance of ExportFileTemplateClient */
    public ExportFileTemplateClient() throws Throwable {
        super();
        super.GetAccessPage();
        myObject = BOFactory.createExportTemplateBo(null);
        myObjectDetail = BOFactory.createExportTemplateDetailBo(null);
        updateAttr = DTOFactory.createEXPORTTEMPLETEDTO();
        deleteAttr = DTOFactory.createEXPORTTEMPLETEDTO();
        columnwKeda = BOFactory.createColumnDefinitionBO(null);
        columnObject = BOFactory.createDisputeRulesBO(null);
        if (exportList == null) {
            exportList = new ArrayList<EXPORTTEMPLETEDTOInter>();
        }
        exportList = (List<EXPORTTEMPLETEDTOInter>) myObject.GetAllTemplates();
        columList = (List<FileColumnDefinitionDTOInter>) columnwKeda.findAllColoumn();
        showAdd = true;
        showAddDetail = true;
        showConfirm = false;
        showConfirmDetail = false;
     FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void onRowSelect1(SelectEvent e) {
        try {
            exportDetailList = (List<EXPORTTEMPLETEDETAILSDTOInter>) myObjectDetail.GetDetail((EXPORTTEMPLETEDTOInter) e.getObject());
            resetVars();
        } catch (Throwable ex) {
        }
    }

    public void fillAttr(ActionEvent e) {
        deleteAttr = (EXPORTTEMPLETEDTOInter) e.getComponent().getAttributes().get("deleteRow");
        resetVars();

    }

    public void fillAttrUpdate(ActionEvent e) {
        updateAttr = (EXPORTTEMPLETEDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    public void deleteRecord() throws Throwable {
        myObject.DeleteTemplate(deleteAttr);
        exportList.remove(deleteAttr);
        message = super.showMessage(SUCC);
        showAdd = true;
        showConfirm = false;
    }

    public void updateRecord() {
        if (showConfirm) {
            message = super.showMessage(CONF_FIRST);
        } else if (updateAttr.getTemplatename().equals("") || updateAttr.getSeparator().equals("")) {
            message = super.showMessage(REQ_FIELD);
        } else {
            try {
                myObject.UpdateTemplate(updateAttr);
                message = super.showMessage(SUCC);
            } catch (Throwable ex) {
            }
        }
    }

    public void addRecord() {
        if (exportList == null) {
            exportList = new ArrayList<EXPORTTEMPLETEDTOInter>();
        }
        newRecord = DTOFactory.createEXPORTTEMPLETEDTO();
        exportList.add(newRecord);
        showAdd = false;
        showConfirm = true;
        resetVars();
    }

    public void confrimRecord() {
        try {
            if (newRecord.getTemplatename().equals("") || newRecord.getSeparator().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                myObject.InsertTemplate(newRecord);
                showAdd = true;
                showConfirm = false;
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    // Details
    public void fillAttrDetail(ActionEvent e) {
        deleteAttrDetails = (EXPORTTEMPLETEDETAILSDTOInter) e.getComponent().getAttributes().get("deleteRowDetail");
        resetVars();
    }

    public void fillAttrUpdateDetail(ActionEvent e) {
        updateAttrDetails = (EXPORTTEMPLETEDETAILSDTOInter) e.getComponent().getAttributes().get("updateRowDetail");
        resetVars();
    }

    public void deleteRecordDetail() throws Throwable {
        myObjectDetail.Delete(deleteAttrDetails);
        exportDetailList.remove(deleteAttrDetails);
        messageDetail = super.showMessage(SUCC);
        showAddDetail = true;
        showConfirmDetail = false;
    }

    public void updateRecordDetail() {
        if (showConfirmDetail) {
            messageDetail = super.showMessage(CONF_FIRST);
        } else if ((Integer) updateAttrDetails.getSequence() == null) {
            message = super.showMessage(REQ_FIELD);
        } else {
            try {
                myObjectDetail.Update(updateAttrDetails);
                messageDetail = super.showMessage(SUCC);
            } catch (Throwable ex) {
            }
        }
    }

    public void addRecordDetail() {
        if (exportDetailList == null) {
            exportDetailList = new ArrayList<EXPORTTEMPLETEDETAILSDTOInter>();
        }
        newRecordDetails = DTOFactory.createEXPORTTEMPLETEDETAILSDTO();
        exportDetailList.add(newRecordDetails);
        showAddDetail = false;
        showConfirmDetail = true;
        resetVars();
    }

    public void confrimRecordDetail() {
        try {
            if ( (Integer) newRecordDetails.getSequence() == null) {
                messageDetail = super.showMessage(REQ_FIELD);
            } else {
                newRecordDetails.setTemplateid(selectedTemp.getTemplateid());
                myObjectDetail.Insert(newRecordDetails);
                showAddDetail = true;
                showConfirmDetail = false;
                messageDetail = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            messageDetail = super.showMessage(REC_EXIST);
        }
    }
    public void resetVars() {
        message = "";
        messageDetail = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
