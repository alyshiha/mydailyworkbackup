/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.AtmAccountsDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface AtmAccountsBOInter {

    Object printPDFrep(String customer, String PrintedName, int atmid, int atmgroupid, int accnameid, String account, String atmidvalue, String atmgroupvalue, String accnamevalue,String branch) throws Throwable;

    List<String> getBranchs() throws Throwable;

    Boolean findDuplicate(int atmid, int accname, String name, int count) throws Throwable;

    List<AtmAccountsDTOInter> getAtmAccountrecord(int atmid, String accid, String accname, int group, String branch) throws Throwable;

    String deleteAtmAccount(AtmAccountsDTOInter record);

    List<AtmAccountsDTOInter> getAtmAccount(String user) throws Throwable;

    String insertAtmAccount(AtmAccountsDTOInter record);

    String updateAtmAccount(AtmAccountsDTOInter record);

}
