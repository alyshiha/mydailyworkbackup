/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.UserActivityDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.UserActivityDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class UserActivityBO extends BaseBO implements UserActivityBOInter,Serializable{

    protected UserActivityBO() {
       // super();
    }

    public Object getGraph()throws Throwable{
        //List<UserActivityDTOInter> uaDTOL = new ArrayList<UserActivityDTOInter>();
        UserActivityDAOInter uaDAO = DAOFactory.createUserActivityDAO(null);
        List<UserActivityDTOInter> uDTOL = (List<UserActivityDTOInter>)uaDAO.findForGraph();
       // for(UsersDTOInter userDTO:uDTOL){
         //   UserActivityDTO uaDTO = (UserActivityDTO)uaDAO.findForGraph(userDTO);
          //  uaDTOL.add(uaDTO);
        //}
        return uDTOL;
    }

  //  private List<UsersDTOInter> getUsers()throws Throwable{
        //UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
       // List<UsersDTOInter> uDTOL = (List<UsersDTOInter>)uDAO.findDisputeGraphUser();
       // return uDTOL;
   // }
}
