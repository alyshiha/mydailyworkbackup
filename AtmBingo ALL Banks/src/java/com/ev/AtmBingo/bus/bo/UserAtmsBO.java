/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.AtmGroupDAOInter;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.UserAtmDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UserAtmDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class UserAtmsBO extends BaseBO implements UserAtmsBOInter ,Serializable{

    protected UserAtmsBO() {
        super();
    }

    public Object getAtmGroup()throws Throwable{
        AtmGroupDAOInter agDAO = DAOFactory.createAtmGroupDAO(null);
        List<AtmGroupDTOInter> agDTOL = (List<AtmGroupDTOInter>)agDAO.findAll();
        return agDTOL;
    }

    public Object getUsers()throws Throwable{
        UsersDAOInter agDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> agDTOL = (List<UsersDTOInter>)agDAO.findComboBox();
        return agDTOL;
    }
    public Object getTargetList(int userId, int atmGroup) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<String> target = new ArrayList<String>();
        target = amDAO.findUserMachines(userId,atmGroup);
        return target;
    }

    public Object getSourceList(int userId, int atmGroup) throws Throwable {
      AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<String> target = new ArrayList<String>();
        target = amDAO.findNotUserMachines(userId,atmGroup);
        return target;
    }

    public Object saveUserAtm(int userId, String[] source,String[] target,int atmGInt) throws Throwable {
        UserAtmDAOInter UADI = DAOFactory.createUserAtmDAO(null);
           UADI.insertAtms(target, userId);
               UADI.DeleteAtms(source,userId);
               return "True";
}
}