/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface ReplanishmentReportBOInter {

    Date getLastReplanishmentDateFor(int atmId)throws Throwable;

    public Object getAtmMachines(UsersDTOInter u)throws Throwable;

    public Object getUsers()throws Throwable;

    Object getReplanishmentData(Date from, Date to, String atmId, Date createdF, Date createdT, int userId,int group2,int proc,Date From1,Date From2,Date To1,Date To2) throws Throwable;

    Object print(ReplanishmentMasterDTOInter[] selectedRec, String userName,String cust) throws Throwable;
    Object print(ReplanishmentMasterDTOInter selectedRec, String userName,String cust) throws Throwable;
    Object printExcel(ReplanishmentMasterDTOInter selectedRec, String userName, String cust) throws Throwable ;

}
