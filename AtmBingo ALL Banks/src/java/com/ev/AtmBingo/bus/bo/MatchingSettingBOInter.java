/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.presdto.AtmMachineTypePresDTOInter;
import com.ev.AtmBingo.bus.presdto.FileColumnDefinitionPresDTOInter;
import com.ev.AtmBingo.bus.presdto.MatchingTypePresDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface MatchingSettingBOInter extends BaseBOInter{

    Object RecreateIndex()throws Throwable;
    List<FileColumnDefinitionPresDTOInter> findAllFileColumnDefinition()throws Throwable;

    List<AtmMachineTypePresDTOInter> findAllAtmMachinesType()throws Throwable;

    Object editeMatchingSetting(Object obj,int oldColumnId,int oldPartSetting, int operationType) throws Throwable;

    Object editMatchingTypeMachines(Object obj, int oldMachineType, int operationType) throws Throwable;

    Object editMatchingType(Object obj,int operationType) throws Throwable;

    List<MatchingTypePresDTOInter> findAllMatchingType() throws Throwable;

}
