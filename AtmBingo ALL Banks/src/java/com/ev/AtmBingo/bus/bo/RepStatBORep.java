package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import java.util.Date;
import com.ev.AtmBingo.base.bo.BaseBO;

import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.bo.RepStatBORepInter;
import com.ev.AtmBingo.bus.dao.AVGDailyDispenseDTOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.MissingJournalsRepDAOInter;
import com.ev.AtmBingo.bus.dao.ReplanishmentReportDAOInter;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author Administrator
 */
public class RepStatBORep extends BaseBO implements RepStatBORepInter,Serializable {

    protected RepStatBORep() {
        super();
    }
   public List<AVGDailyDispenseDTOInter> runReportATMAmountexcel(String dateFrom, String dateTo, String user, String cust, String ATMID, String ATMGroup) throws Throwable {
        ReplanishmentReportDAOInter rrDAO = DAOFactory.createReplanishmentReportDAO(null);
        List<AVGDailyDispenseDTOInter> res = (List<AVGDailyDispenseDTOInter>) rrDAO.averageloadamountexcel(dateFrom, dateTo, ATMGroup, ATMID);
        return res;
    }

    public String runReportATMAmount(String dateFrom, String dateTo, String user, String cust, String ATMID, String ATMGroup) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        ReplanishmentReportDAOInter rrDAO = DAOFactory.createReplanishmentReportDAO(null);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("DateFrom", dateFrom);
        params.put("user", user);
        params.put("DateTo", dateTo);

        params.put("customer", cust);
        params.put("ATMID", ATMID);
        params.put("ATMGroup", ATMGroup);
        params.put("AVGATM", rrDAO.averageloadamount(dateFrom, dateTo, ATMGroup, ATMID));
       
        Connection conn = CoonectionHandler.getInstance().getConnection();
       
        try {
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/AVG_Load_amout.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri = "AVGLoadamout" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {

                String x = "../PDF/" + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;CoonectionHandler.getInstance().returnConnection(conn);
                return x;

            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();

            return "fail";
        }

    }

    public List<AVGDailyDispenseDTOInter> runReportaverageRepCycleexcel(String dateFrom, String dateTo, String user, String cust, String ATMID, String ATMGroup) throws Throwable {
        ReplanishmentReportDAOInter rrDAO = DAOFactory.createReplanishmentReportDAO(null);
        List<AVGDailyDispenseDTOInter> res = (List<AVGDailyDispenseDTOInter>) rrDAO.averageRepCycleexcel(ATMID, ATMGroup, dateFrom, dateTo);
        return res;
    }

    public String runReportaverageRepCycle(String dateFrom, String dateTo, String user, String cust, String ATMID, String ATMGroup) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        ReplanishmentReportDAOInter rrDAO = DAOFactory.createReplanishmentReportDAO(null);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("DateFrom", dateFrom);
        params.put("user", user);
        params.put("DateTo", dateTo);
        params.put("customer", cust);
        params.put("ATMID", ATMID);
        params.put("ATMGroup", ATMGroup);
        params.put("AVGATM", rrDAO.averageRepCycle(ATMID, ATMGroup, dateFrom, dateTo));
    
   Connection conn =CoonectionHandler.getInstance().getConnection();
        try {
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/AVG_average_replenishment_cycle.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri = "averageRepCycle" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {

                String x = "../PDF/" + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;CoonectionHandler.getInstance().returnConnection(conn);
                return x;

            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            
            return "fail";
        }

    }

    public List<AVGDailyDispenseDTOInter> runReportDailydispenseexcel(String dateFrom, String dateTo, String user, String cust, String ATMID, String ATMGroup) throws Throwable {
        ReplanishmentReportDAOInter rrDAO = DAOFactory.createReplanishmentReportDAO(null);
        List<AVGDailyDispenseDTOInter> res = (List<AVGDailyDispenseDTOInter>) rrDAO.Dailydispenseexcel(dateFrom, dateTo, ATMID, ATMGroup);
        return res;
    }

    public String runReportDailydispense(String dateFrom, String dateTo, String user, String cust, String ATMID, String ATMGroup) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        ReplanishmentReportDAOInter rrDAO = DAOFactory.createReplanishmentReportDAO(null);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("DateFrom", dateFrom);
        params.put("user", user);
        params.put("DateTo", dateTo);
        params.put("customer", cust);
        params.put("ATMID", ATMID);
        params.put("ATMGroup", ATMGroup);
        params.put("AVGATM", rrDAO.Dailydispense(dateFrom, dateTo, ATMID, ATMGroup));
         Connection conn =CoonectionHandler.getInstance().getConnection();
        try {
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/AVG_Daily_dispense.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri = "Dailydispense" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {

                String x = "../PDF/" + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;CoonectionHandler.getInstance().returnConnection(conn);
                return x;

            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            
            return "fail";
        }

    }

    public String runReportRepGaps(Date dateFrom, Date dateTo, String user, String cust, String ATMID, String ATMGroup) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        ReplanishmentReportDAOInter rrDAO = DAOFactory.createReplanishmentReportDAO(null);
        Integer Result = rrDAO.calcReplanishmentGaps(ATMID.toString(), ATMGroup, dateFrom, dateTo);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("DateFrom", dateFrom.toString());
        params.put("user", user);
        params.put("DateTo", dateTo.toString());
        params.put("customer", cust);
        // params.put("ATMID", ATMID.toString());
        //  params.put("ATMGroup", ATMGroup.toString());
        params.put("Seq", Result.toString());
    Connection conn =CoonectionHandler.getInstance().getConnection();
        try {
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/Rep_Gaps.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri = "RepGap" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {

                String x = "../PDF/" + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;CoonectionHandler.getInstance().returnConnection(conn);
                return x;

            } catch (Exception ex) {
                ex.printStackTrace();
                rrDAO.DeletReplanishmentGaps(Result);
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            rrDAO.DeletReplanishmentGaps(Result);
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            
            return "fail";
        }

    }
}