/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.NotePresentedDAOInter;
import com.ev.AtmBingo.bus.dto.NotePresentedDTOInter;
import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author AlyShiha
 */
public class NotePresentedBO extends BaseBO implements NotePresentedBOInter {

    protected NotePresentedBO() {
        super();
    }

    @Override
    public List<NotePresentedDTOInter> runReportExcel(String dateFrom, String dateTo, Integer atmGroup, int userID, String atmAppID,String operator) throws Throwable {
        NotePresentedDAOInter arrRepDAO = DAOFactory.createNotePresentedDAO();
        List<NotePresentedDTOInter> rs = (List<NotePresentedDTOInter>) arrRepDAO.findNotePresentedexcelReport(dateFrom, dateTo, atmGroup, userID, atmAppID,operator);
        return rs;
    }

    @Override
    public String runReport(String dateFrom, String dateTo, Integer atmGroup, int userID, String atmAppID, String UserName, String Customer,String operator) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        JRResultSetDataSource resultSetDataSource;
        NotePresentedDAOInter arrRepDAO = DAOFactory.createNotePresentedDAO();

        ResultSet rs = (ResultSet) arrRepDAO.findNotePresentedReport(dateFrom, dateTo, atmGroup, userID, atmAppID,operator);
        resultSetDataSource = new JRResultSetDataSource(rs);

        params.put("DateFrom", dateFrom);
        params.put("DateTo", dateTo);
        params.put("ATMGroup", atmGroup);
        params.put("USERID", userID);
        params.put("ATMAppID", atmAppID);
        params.put("user", UserName);
        params.put("customer", Customer);
        params.put("operatorflag", operator);

        try {

            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/NotePresented.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);

            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, resultSetDataSource);

            Calendar c = Calendar.getInstance();
            String uri = "NotePresented" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {

                String x = "../PDF/" + uri;

                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                resultSetDataSource = null;
                CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());
                rs.close();
                resultSetDataSource = null;
                //  IDataProvider prov = DataProviderFactory.createPoolProvider();
                // prov.CloseConn();
                return x;

            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();

            return "fail";
        }

    }

}
