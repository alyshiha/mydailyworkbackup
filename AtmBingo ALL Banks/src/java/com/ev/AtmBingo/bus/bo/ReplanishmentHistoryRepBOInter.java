/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface ReplanishmentHistoryRepBOInter {

    Object findReportexcel(Date dateF, Date dateT, String atmId, Integer atmGrp, String customer, String userName) throws Throwable;
Object findcardtakenexcel(Date dateF, Date dateT, String atmId, Integer atmGrp, String customer, String userName) throws Throwable;
    Object getGroups() throws Throwable;
Object findcardtakenReport(Date dateF, Date dateT, String atmId, Integer atmGrp, String customer, String userName) throws Throwable ;
    Object findReport(Date dateF, Date dateT, String atmId, Integer atmGrp, String customer, String userName) throws Throwable;

    Object getAtmMachines(UsersDTOInter uDTO, Integer grpId) throws Throwable;
}
