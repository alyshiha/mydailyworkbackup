/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface LoginBOInter {

    Object checkLogin(String userName, String pass) throws Throwable;

    public Object FirstTimeLogin(String userName) throws Throwable;

    Object findUser(String userName) throws Throwable;

    Object lockUser(String userName) throws Throwable;

    int userlic() throws Throwable;

    Boolean checkPassExpire(String userName) throws Throwable;

    String ValidateLog(String userName, String password, String version, Integer NumOfUsers, Date EndDate) throws Throwable;

}
