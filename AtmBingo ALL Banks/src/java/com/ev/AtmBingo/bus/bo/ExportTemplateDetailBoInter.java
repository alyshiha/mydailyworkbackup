/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.EXPORTTEMPLETEDETAILSDTOInter;
import com.ev.AtmBingo.bus.dto.EXPORTTEMPLETEDTOInter;

/**
 *
 * @author Administrator
 */
public interface ExportTemplateDetailBoInter {

    Object Delete(EXPORTTEMPLETEDETAILSDTOInter cfDTO) throws Throwable;

    Object GetAllDetail() throws Throwable;

    Object GetDetail(EXPORTTEMPLETEDTOInter template) throws Throwable;

    Object Insert(EXPORTTEMPLETEDETAILSDTOInter cfDTO) throws Throwable;

    Object Update(EXPORTTEMPLETEDETAILSDTOInter cfDTO) throws Throwable;

}
