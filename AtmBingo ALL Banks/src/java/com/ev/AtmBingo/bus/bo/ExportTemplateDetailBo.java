/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.ExportTemplateDetailDAOInter;
import com.ev.AtmBingo.bus.dto.EXPORTTEMPLETEDETAILSDTOInter;
import com.ev.AtmBingo.bus.dto.EXPORTTEMPLETEDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */

public class ExportTemplateDetailBo extends BaseBO implements ExportTemplateDetailBoInter,Serializable {

    protected ExportTemplateDetailBo() {
        super();
    }

    public Object GetDetail(EXPORTTEMPLETEDTOInter template) throws Throwable {
        ExportTemplateDetailDAOInter amDAO = DAOFactory.createExportTemplateDetailDAO();
        List<EXPORTTEMPLETEDETAILSDTOInter> amDTOL = (List<EXPORTTEMPLETEDETAILSDTOInter>) amDAO.find(template.getTemplateid());
        return amDTOL;
    }

    public Object GetAllDetail() throws Throwable {
       ExportTemplateDetailDAOInter amtDAO = DAOFactory.createExportTemplateDetailDAO();
        List<EXPORTTEMPLETEDETAILSDTOInter> amtDTOL = (List<EXPORTTEMPLETEDETAILSDTOInter>) amtDAO.findAll();
        return amtDTOL;
    }

    public Object Insert(EXPORTTEMPLETEDETAILSDTOInter cfDTO) throws Throwable {
        ExportTemplateDetailDAOInter cfDAO = DAOFactory.createExportTemplateDetailDAO();
        cfDAO.insert(cfDTO);
        return "Insert Done";
    }

    public Object Update(EXPORTTEMPLETEDETAILSDTOInter cfDTO) throws Throwable {
        ExportTemplateDetailDAOInter cfDAO = DAOFactory.createExportTemplateDetailDAO();
        cfDAO.update(cfDTO);
        return "Update Done";
    }

    public Object Delete(EXPORTTEMPLETEDETAILSDTOInter cfDTO) throws Throwable {
        ExportTemplateDetailDAOInter cfDAO = DAOFactory.createExportTemplateDetailDAO();
        cfDAO.delete(cfDTO);
        return "Delete Done";

    }
}
