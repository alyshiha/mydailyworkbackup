package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dao.AtmJournalDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.AtmJournalDTOInter;
import java.util.Date;
import java.util.List;

public class AtmJournalBO implements AtmJournalBOInter {

    @Override
    public void recalc() throws Throwable {
        AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
        engin.recalc();
    }
  @Override
    public void recalc123visa() throws Throwable {
        AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
        engin.recalc123visa();
    }

    @Override
    public void recalc2() throws Throwable {
        AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
        engin.recalc2();
    }

    @Override
    public List<AtmJournalDTOInter> getJournalrecord(String AppID, int repid, int export, String FromDate,Boolean released) throws Throwable {
        AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
        List<AtmJournalDTOInter> records = engin.findrecord(AppID, repid, export, FromDate,released);
        return records;
    }

    @Override
    public List<AtmJournalDTOInter> findjournalatm(String dateFrom, String dateTo, String NoOfAtms, int export, int group, String Reversal, int user,Boolean constractor,Boolean released) throws Throwable {
        AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
        List<AtmJournalDTOInter> records = engin.findrecordjournal(dateFrom, dateTo, NoOfAtms, export, group, Reversal, user,constractor, released);
        return records;
    }

    @Override
    public List<AtmJournalDTOInter> getJournal() throws Throwable {
        AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
        List<AtmJournalDTOInter> records = engin.findAll();
        return records;
    }

    @Override
    public String insertJournal(AtmJournalDTOInter record) {
        try {
            AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
            engin.insert(record);
            return "Record Has Been Succesfully Added";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String updateJournal(List<AtmJournalDTOInter> record, String flag) {
        try {
            AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
            engin.update(record, flag);
            return "Record Has Been Succesfully Updates";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String deleteJournal(AtmJournalDTOInter record) {
        try {
            AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
            engin.delete(record);
            return "Record Has Been Succesfully Deleted";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }
}
