/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.AtmJournalDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface AtmJournalBOInter {

    List<AtmJournalDTOInter> findjournalatm(String dateFrom, String dateTo, String NoOfAtms, int export, int group, String Reversal, int user, Boolean constractor, Boolean released) throws Throwable;

    String deleteJournal(AtmJournalDTOInter record);

    void recalc123visa() throws Throwable;

    List<AtmJournalDTOInter> getJournal() throws Throwable;

    void recalc() throws Throwable;

    void recalc2() throws Throwable;

    List<AtmJournalDTOInter> getJournalrecord(String AppID, int repid, int export, String FromDate, Boolean released) throws Throwable;

    String insertJournal(AtmJournalDTOInter record);

    String updateJournal(List<AtmJournalDTOInter> record, String flag);

}
