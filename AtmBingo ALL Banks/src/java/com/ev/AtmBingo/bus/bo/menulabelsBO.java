package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.AtmBingo.bus.dao.menulabelsDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.menulabelsDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.AtmBingo.bus.dto.menulabelsDTOInter;
import java.io.Serializable;
import java.text.SimpleDateFormat;

public class menulabelsBO extends BaseBO implements menulabelsBOInter ,Serializable{

    protected menulabelsBO() {
        super();
    }
  public Boolean ValidatePage(int userID, String page) throws Throwable {
        menulabelsDAOInter LDAO = DAOFactory.createmenulabelsDAO();
        return LDAO.findmenuitemcheck(userID, page);
    }

    public Object insertrecord(Object... obj) throws Throwable {
        menulabelsDTOInter RecordToInsert = (menulabelsDTOInter) obj[0];
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        engin.insertrecord(RecordToInsert);
        return "Insert Done";
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<menulabelsDTOInter> entities = (List<menulabelsDTOInter>) obj[0];
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        return engin.save(entities);
         
    }

    public Object updaterecord(Object... obj) throws Throwable {
        menulabelsDTOInter RecordToUpdate = (menulabelsDTOInter) obj[0];
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        menulabelsDTOInter RecordToDelete = (menulabelsDTOInter) obj[0];
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        return engin.deleterecord(RecordToDelete);
      
    }

    public Object GetPageListOfRecords(Object... obj) throws Throwable {
         menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        List<menulabelsDTOInter> AllRecords = (List<menulabelsDTOInter>) engin.findPageRecordsList();
        return AllRecords;
    }
    public Object GetParentListOfRecords(Object... obj) throws Throwable {
         menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        List<menulabelsDTOInter> AllRecords = (List<menulabelsDTOInter>) engin.findParentRecordsList();
        return AllRecords;
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<menulabelsDTOInter> SearchRecords = (List<menulabelsDTOInter>) obj[0];
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        List<menulabelsDTOInter> AllRecords = (List<menulabelsDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        List<menulabelsDTOInter> AllRecords = (List<menulabelsDTOInter>) engin.findRecordsAll();
        return AllRecords;
    } public Object GetAllRecords2(Object... obj) throws Throwable {
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        List<menulabelsDTOInter> AllRecords = (List<menulabelsDTOInter>) engin.findRecordsAll2();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        menulabelsDTOInter SearchParameter = (menulabelsDTOInter) obj[0];
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        menulabelsDTOInter Record = (menulabelsDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
