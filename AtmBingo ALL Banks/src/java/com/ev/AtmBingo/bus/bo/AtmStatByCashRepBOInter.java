/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.statbytransDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AtmStatByCashRepBOInter {

    Object getGrp() throws Throwable;

    Object getGrpassignuser(Integer userId) throws Throwable;

    String runReport(String dateFrom, String dateTo, int atmGroupInt, String user, int NoOfAtms, int tType, String cust
    ) throws Throwable;

    List<statbytransDTOInter> runReportExcel(String dateFrom, String dateTo, int atmGroupInt, String user, int NoOfAtms, int tType, String cust) throws Throwable;
}
