/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.data.SimpleProtector;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.MenuLabelDAOInter;
import com.ev.AtmBingo.bus.dao.ProfileMenuDAOInter;
import com.ev.AtmBingo.bus.dao.UserProfileDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.MenuLabelDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileMenuDTOInter;
import com.ev.AtmBingo.bus.dto.UserProfileDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class PrivilageBO extends BaseBO implements PrivilageBOInter, Serializable {

    private String dashURL;

    public String getDashURL() {
        return dashURL;
    }

    public void setDashURL(String dashURL) {
        this.dashURL = dashURL;
    }

    protected PrivilageBO() {
        // super();
    }
   public Object findAllPage(int userId)throws Throwable{
        MenuLabelDAOInter amtDAO = DAOFactory.createMenuLabelDAO(null);
        List<String> amtDTOL = (List<String>)amtDAO.findAllPage(userId);
        return amtDTOL;
    }
    public String generateMenu(int userId) throws Throwable {
        ProfileMenuDAOInter pmiDAO = DAOFactory.createProfileMenuDAO(null);
        MenuLabelDAOInter mlDAO = DAOFactory.createMenuLabelDAO(null);
        UserProfileDAOInter upDAO = DAOFactory.createUserProfileDAO(null);
        List<MenuLabelDTOInter> mlDTOL = new ArrayList<MenuLabelDTOInter>();

        UserProfileDTOInter upDTO = (UserProfileDTOInter) upDAO.findByUserId(userId);

        List<ProfileMenuDTOInter> pmiDTOL = (List<ProfileMenuDTOInter>) pmiDAO.findByProfileId(upDTO.getProfileId());
        List<MenuLabelDTOInter> Menu = (List<MenuLabelDTOInter>) mlDAO.findAll(userId);

        // for (ProfileMenuDTOInter pmi : pmiDTOL) {
        //   MenuLabelDTOInter mlDTO = (MenuLabelDTOInter) mlDAO.find(pmi.getMenuId());
        //   }
        String menuItems = "";

        List<MenuLabelDTOInter> mlParent = (List<MenuLabelDTOInter>) mlDAO.findParents(upDTO.getProfileId());
        Integer Parent = 0;
        for (MenuLabelDTOInter d : mlParent) {

            if (!d.getEnglishLabel().contains("DashBoard")) {
                //--------first level------------------------------------
                menuItems += "<li class=\"top\" ><a class=\"top_link\" href=\"#\" $page><span $spa>$label</span></a>";
                if (d.getPage() != null) {
                    if (d.getPage().toUpperCase().equals("SYSTEMRULES.XHTML?ID=1")) {
                        menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('SystemRules.xhtml?ID=1');\"");
                    } else if (d.getPage().toUpperCase().equals("SYSTEMRULES.XHTML?ID=2")) {
                        menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('SystemRules.xhtml?ID=2');\"");
                    } else {
                        menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('" + d.getPage() + "');\"");
                        menuItems = menuItems.replace("$spa", " ");
                    }
                } else {
                    menuItems = menuItems.replace("$page", " ");
                    menuItems = menuItems.replace("$spa", "class=\"down\"");
                }
                menuItems = menuItems.replace("$label", d.getEnglishLabel());
                //----------------------------------------------------------------
                Parent = d.getMenuId();
                if (d.getChildMenu(Menu, Parent).size() != 0) {
                    //----------second level--------------------------
                    menuItems += "<ul class=\"sub\">";
                    for (MenuLabelDTOInter chi : d.getChildMenu(Menu, Parent)) {
                        Parent = chi.getMenuId();
                        menuItems += "<li><a href=\"#\" $page><span>$childLabel</span></a>";
                        if (chi.getPage() != null) {
                            if (chi.getPage().toUpperCase().equals("SYSTEMRULES.XHTML?ID=1")) {
                                menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('SystemRules.xhtml?ID=1" + "');\"");
                            } else if (chi.getPage().toUpperCase().equals("SYSTEMRULES.XHTML?ID=2")) {
                                menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('SystemRules.xhtml?ID=2" + "');\"");
                            } else {
                                menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('" + chi.getPage() + "');\"");
                                menuItems = menuItems.replace("$spa", " ");
                            }

                        } else {
                            menuItems = menuItems.replace("$page", "class=\"fly\" ");
                        }

                        menuItems = menuItems.replace("$childLabel", chi.getEnglishLabel());
                        //------------------------------------------------
                        if (chi.getChildMenu(Menu, Parent).size() != 0) {
                            //-----------third and the last level--------------------

                            menuItems += "<ul>";
                            for (MenuLabelDTOInter chiOf : chi.getChildMenu(Menu, Parent)) {
                                Parent = chiOf.getMenuId();
                                menuItems += "<li><a href=\"#\" $page>$childLabel</a>";
                                if (chiOf.getPage() != null) {

                                    if (chiOf.getPage().toUpperCase().equals("SYSTEMRULES.XHTML?ID=1")) {
                                        menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('SystemRules.xhtml?ID=1" + "');\"");
                                    } else if (chiOf.getPage().toUpperCase().equals("SYSTEMRULES.XHTML?ID=2")) {
                                        menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('SystemRules.xhtml?ID=2" +  "');\"");
                                    } else {
                                        menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('" + chiOf.getPage() + "');\"");
                                        menuItems = menuItems.replace("$spa", " ");
                                    }
                                } else {
                                    menuItems = menuItems.replace("$page", "class=\"fly\" ");
                                }

                                menuItems = menuItems.replace("$childLabel", chiOf.getEnglishLabel());
                                //--------------------------------------------------------------------
                                if (chi.getChildMenu(Menu, Parent).size() != 0) {
                                    //-----------third and the last level--------------------
                                    menuItems += "<ul>";
                                    for (MenuLabelDTOInter chiFourth : chiOf.getChildMenu(Menu, Parent)) {
                                        Parent = chiFourth.getMenuId();
                                        menuItems += "<li><a href=\"#\" $page>$childLabel</a>";
                                        if (chiFourth.getPage() != null) {

                                            if (chiFourth.getPage().toUpperCase().equals("SYSTEMRULES.XHTML?ID=1")) {
                                                menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('SystemRules.xhtml?ID=1" + "');\"");
                                            } else if (chiFourth.getPage().toUpperCase().equals("SYSTEMRULES.XHTML?ID=2")) {
                                                menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('SystemRules.xhtml?ID=2" +  "');\"");
                                            } else {
                                                menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('" + chiFourth.getPage() + "');\"");
                                                menuItems = menuItems.replace("$spa", " ");
                                            }
                                        } else {
                                            menuItems = menuItems.replace("$page", " ");
                                        }

                                        menuItems = menuItems.replace("$childLabel", chiFourth.getEnglishLabel());

                                    }
                                    menuItems += "</ul></li>";
                                } else {
                                    menuItems += "</li>";
                                }
                                //---------------------------------------------------------------------

                            }
                            menuItems += "</ul></li>";
                        } else {
                            menuItems += "</li>";
                        }
                    }
                    menuItems += "</ul></li>";

                } else {
                    menuItems += "</li>";
                    //---------------------------------------
                }
            } else {
                dashURL = d.getPage();
            }
        }

        return menuItems;

    }

    public Object updateStatus(UsersDTOInter uDTO) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        uDAO.updateStatus(uDTO);
        return "done";
    }
}
