/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;

/**
 *
 * @author Administrator
 */
public interface HolidaysBOInter extends BaseBOInter{


    Object editeHolidays(Object obj,Integer oldMonth, Integer oldDay, int operation) throws Throwable;

    Object getHolidays() throws Throwable;

}
