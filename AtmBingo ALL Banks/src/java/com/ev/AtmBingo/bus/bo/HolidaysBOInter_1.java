/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.HolidaysDTOInter_1;
import java.util.List;

/**
 *
 * @author shi7a
 */
public interface HolidaysBOInter_1 {

    String deleteRecord(HolidaysDTOInter_1 record);

    List<HolidaysDTOInter_1> findAll() throws Throwable;

    List<HolidaysDTOInter_1> findRecord(String Type,String Name) throws Throwable;

    String insertRecord(HolidaysDTOInter_1 record);

    Object printPDFrep(String customer, String PrintedName, String type) throws Throwable;

    String updateRecord(HolidaysDTOInter_1 record);
    
}
