/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.BlockUsersDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;

/**
 *
 * @author Administrator
 */
public interface BlockUsersBOInter extends BaseBOInter{

    Object getBlockedUsers()throws Throwable;

    Object getBlockReasons()throws Throwable;

    Object blockHim(BlockUsersDTOInter buDTO) throws Throwable;

    Object getUsers(int rest) throws Throwable;
    
    Object getlockedUsers() throws Throwable;

    Object unBlockHim(BlockUsersDTOInter buDTO) throws Throwable;
    
    Object unlockuser(UsersDTOInter buDTO)throws Throwable;

}
