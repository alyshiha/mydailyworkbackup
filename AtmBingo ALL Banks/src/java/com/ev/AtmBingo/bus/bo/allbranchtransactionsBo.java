package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.AtmBingo.bus.dao.allbranchtransactionsDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.DisputesDTOInter;
import com.ev.AtmBingo.bus.dto.allbranchtransactionsDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.AtmBingo.bus.dto.allbranchtransactionsDTOInter;
import com.ev.AtmBingo.bus.dto.allbranchtransactionssearchDTOInter;
import java.io.Serializable;
import java.text.SimpleDateFormat;

public class allbranchtransactionsBo extends BaseBO implements allbranchtransactionsBOInter,Serializable {

    protected allbranchtransactionsBo() {
        super();
    }

  

    public Object insertrecord(Object... obj) throws Throwable {
        allbranchtransactionsDTOInter RecordToInsert = (allbranchtransactionsDTOInter) obj[0];
        allbranchtransactionsDAOInter engin = DAOFactory.createallbranchtransactionsDAO();
        engin.insertrecord(RecordToInsert);
        return "Insert Done";
    }

    public List<allbranchtransactionsDTOInter> SelectStat(allbranchtransactionssearchDTOInter searchfields, int user) throws Throwable {
        String WhereCondition = " where 1=1 and ATM_ID  in (select b.atmid from branch_atm b where b.userid = $userid)   ";
        WhereCondition = WhereCondition.replace("$userid", "" + user);
        String Searchcriertia = "";

        if ("Between".equals(searchfields.gettransactiondateoper())) {
            if (searchfields.gettransactiondatefrom() != null && searchfields.gettransactiondatefrom() != null && searchfields.gettransactiondateto() != null && searchfields.gettransactiondateto() != null) {
               WhereCondition += " and transaction_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss') "
                        + " and transaction_date <= to_date('$col2','dd.mm.yyyy hh24:mi:ss') ";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom()));
                WhereCondition = WhereCondition.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondateto()));
                Searchcriertia = Searchcriertia + " Transaction Time Between " + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom()) + " & " + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondateto());
            }
        } else if ("<>".equals(searchfields.gettransactiondateoper())) {
            if (searchfields.gettransactiondatefrom() != null && searchfields.gettransactiondatefrom() != null) {
                WhereCondition += " and transaction_date <> to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom()));
                Searchcriertia = Searchcriertia + " Transaction Time Not = " + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom());
            }
        } else if ("<".equals(searchfields.gettransactiondateoper())) {
            if (searchfields.gettransactiondatefrom() != null && searchfields.gettransactiondatefrom() != null) {
                WhereCondition += " and transaction_date < to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom()));
                Searchcriertia = Searchcriertia + " Transaction Time Smaller Than " + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom());
            }
        } else if (">".equals(searchfields.gettransactiondateoper())) {
            if (searchfields.gettransactiondatefrom() != null && searchfields.gettransactiondatefrom() != null) {
                WhereCondition += " and transaction_date > to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom()));
                Searchcriertia = Searchcriertia + " Transaction Time Greater Than " + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom());
            }
        } else if (">=".equals(searchfields.gettransactiondateoper())) {
            if (searchfields.gettransactiondatefrom() != null && searchfields.gettransactiondatefrom() != null) {
                WhereCondition += " and transaction_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom()));
                Searchcriertia = Searchcriertia + " Transaction Time > Or = " + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom());
            }
        } else if ("<=".equals(searchfields.gettransactiondateoper())) {
            if (searchfields.gettransactiondatefrom() != null && searchfields.gettransactiondatefrom() != null) {
                WhereCondition += " and transaction_date <= to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom()));
                Searchcriertia = Searchcriertia + " Transaction Time < Or = " + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom());
            }
        } else if ("=".equals(searchfields.gettransactiondateoper())) {
            if (searchfields.gettransactiondatefrom() != null && searchfields.gettransactiondatefrom() != null) {
                WhereCondition += " and transaction_date = to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom()));
                Searchcriertia = Searchcriertia + " Transaction Time = " + DateFormatter.changeDateAndTimeFormat(searchfields.gettransactiondatefrom());
            }
        }
        if ("Like".equals(searchfields.getcardop())) {
            if (searchfields.getcardno() != null && !searchfields.getcardno().equals("")) {
                WhereCondition += " and " + super.getcardcolumn() + " like '$col1'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getcardno());
                Searchcriertia = Searchcriertia + " Card Number Like " + searchfields.getcardno();
            }
        } else if ("Not Like".equals(searchfields.getcardop())) {
            if (searchfields.getcardno() != null && !searchfields.getcardno().equals("")) {
                WhereCondition += " and " + super.getcardcolumn() + " Not Like '$col1'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getcardno());
                Searchcriertia = Searchcriertia + " Card Number Not Like " + searchfields.getcardno();
            }
        } else if ("is Null".equals(searchfields.getcardop())) {
            WhereCondition += " and card_no is null";
            Searchcriertia = Searchcriertia + " Card Number Is Null";
        } else if ("is Not Null".equals(searchfields.getcardop())) {
            WhereCondition += " and card_no is not null";
            Searchcriertia = Searchcriertia + " Card Number Is Not Null";
        } else if ("<>".equals(searchfields.getcardop())) {
            if (searchfields.getcardno() != null && !searchfields.getcardno().equals("")) {
                WhereCondition += " and " + super.getcardcolumn() + " <> '$col1'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getcardno());
                Searchcriertia = Searchcriertia + " Card Number Not = " + searchfields.getcardno();
            }
        } else if ("=".equals(searchfields.getcardop())) {
            if (searchfields.getcardno() != null && !searchfields.getcardno().equals("")) {
                WhereCondition += " and " + super.getcardcolumn() + " = '$col1'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getcardno());
                Searchcriertia = Searchcriertia + " Card Number = " + searchfields.getcardno();
            }
        }

        if ("Like".equals(searchfields.getcustaccnumop())) {
            if (searchfields.getcustomeraccountnumber() != null && !searchfields.getcustomeraccountnumber().equals("")) {
                WhereCondition += " and CUSTOMER_ACCOUNT_NUMBER like '$col1'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getcustomeraccountnumber());
                Searchcriertia = Searchcriertia + " CUSTOMER ACCOUNT NUMBER Like " + searchfields.getcustomeraccountnumber();
            }
        } else if ("Not Like".equals(searchfields.getcustaccnumop())) {
            if (searchfields.getcustomeraccountnumber() != null && !searchfields.getcustomeraccountnumber().equals("")) {
                WhereCondition += " and CUSTOMER_ACCOUNT_NUMBER Not Like '$col1'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getcustomeraccountnumber());
                Searchcriertia = Searchcriertia + " CUSTOMER ACCOUNT NUMBER Not Like " + searchfields.getcustomeraccountnumber();
            }
        } else if ("is Null".equals(searchfields.getcustaccnumop())) {
            WhereCondition += " and CUSTOMER_ACCOUNT_NUMBER is null";
            Searchcriertia = Searchcriertia + " CUSTOMER ACCOUNT NUMBER Is Null ";
        } else if ("is Not Null".equals(searchfields.getcustaccnumop())) {
            WhereCondition += " and CUSTOMER_ACCOUNT_NUMBER is not null";
            Searchcriertia = Searchcriertia + " CUSTOMER ACCOUNT NUMBER Is Not Null ";
        } else if ("<>".equals(searchfields.getcustaccnumop())) {
            if (searchfields.getcustomeraccountnumber() != null && !searchfields.getcustomeraccountnumber().equals("")) {
                WhereCondition += " and CUSTOMER_ACCOUNT_NUMBER <> '$col1'";
                Searchcriertia = Searchcriertia + " CUSTOMER ACCOUNT NUMBER != " + searchfields.getcustomeraccountnumber();
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getcustomeraccountnumber());
            }
        } else if ("=".equals(searchfields.getcustaccnumop())) {
            if (searchfields.getcustomeraccountnumber() != null && !searchfields.getcustomeraccountnumber().equals("")) {
                WhereCondition += " and CUSTOMER_ACCOUNT_NUMBER = '$col1'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getcustomeraccountnumber());
                Searchcriertia = Searchcriertia + " CUSTOMER ACCOUNT NUMBER = " + searchfields.getcustomeraccountnumber();
            }
        }
        if (searchfields.getatmid() != 0) {
            WhereCondition += " and ATM_ID = $col1";
            WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getatmid());
            Searchcriertia = Searchcriertia + " ATM ID = " + searchfields.getatmid();
        }
        if (searchfields.getatmid() != 0) {
            WhereCondition += " and ATM_ID = $col1";
            WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getatmid());
            Searchcriertia = Searchcriertia + " ATM ID = " + searchfields.getatmid();
        }
        if (searchfields.gettransactiontypeid() != 0) {
            WhereCondition += " and transaction_type_id = $col1";
            WhereCondition = WhereCondition.replace("$col1", "" + searchfields.gettransactiontypeid());
            Searchcriertia = Searchcriertia + " Transaction Type ID = " + searchfields.gettransactiontypeid();
        }
        if (searchfields.getresponsecodeid() != 0) {
            WhereCondition += " and response_code_id = $col1";
            WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getresponsecodeid());
            Searchcriertia = Searchcriertia + " Response Code ID = " + searchfields.getresponsecodeid();
        }
        if (searchfields.getreasonid() != 0) {
            WhereCondition += " and is_colected_reason_ID(card_no) = $col1";
            WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getreasonid());
            Searchcriertia = Searchcriertia + " Reason = " + searchfields.getreasonid();
        }
        if (searchfields.getbranchid() != 0) {
            WhereCondition += " and ATM_ID IN (select t.atmid from BRANCH_ATM t where t.userid = $col1) ";
            WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getbranchid());
            Searchcriertia = Searchcriertia + " Branch = " + searchfields.getbranchid();
        }
        List<allbranchtransactionsDTOInter> result = (List<allbranchtransactionsDTOInter>) GetListOfRecords(WhereCondition);
        return result;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        allbranchtransactionsDTOInter RecordToUpdate = (allbranchtransactionsDTOInter) obj[0];
        allbranchtransactionsDAOInter engin = DAOFactory.createallbranchtransactionsDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        allbranchtransactionsDAOInter engin = DAOFactory.createallbranchtransactionsDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        allbranchtransactionsDTOInter RecordToDelete = (allbranchtransactionsDTOInter) obj[0];
        allbranchtransactionsDAOInter engin = DAOFactory.createallbranchtransactionsDAO();
        engin.deleterecord(RecordToDelete);
        return "record has been deleted";
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        String SearchRecords = (String) obj[0];
        allbranchtransactionsDAOInter engin = DAOFactory.createallbranchtransactionsDAO();
        List<allbranchtransactionsDTOInter> AllRecords = (List<allbranchtransactionsDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        allbranchtransactionsDAOInter engin = DAOFactory.createallbranchtransactionsDAO();
        List<allbranchtransactionsDTOInter> AllRecords = (List<allbranchtransactionsDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        allbranchtransactionsDTOInter SearchParameter = (allbranchtransactionsDTOInter) obj[0];
        allbranchtransactionsDAOInter engin = DAOFactory.createallbranchtransactionsDAO();
        allbranchtransactionsDTOInter Record = (allbranchtransactionsDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }

    public Object Saverecord(Object... obj) throws Throwable {
        allbranchtransactionsDTOInter[] SearchParameter = (allbranchtransactionsDTOInter[]) obj[0];
        Integer userid = (Integer) obj[1];
        String reason = (String) obj[2];
        allbranchtransactionsDAOInter engin = DAOFactory.createallbranchtransactionsDAO();
        engin.save(SearchParameter, userid, reason);
        return "";
    }
}
