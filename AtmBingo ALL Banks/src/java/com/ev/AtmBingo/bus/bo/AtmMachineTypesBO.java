/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.AtmMachineTypeDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmMachineTypesBO extends BaseBO implements AtmMachineTypesBOInter,Serializable{

    protected AtmMachineTypesBO() {
        super();
    }

    public Object getAtmMachineType()throws Throwable{
        AtmMachineTypeDAOInter amtDAO = DAOFactory.createAtmMachineTypeDAO(null);
        List<AtmMachineTypeDTOInter> amtDTOL = (List<AtmMachineTypeDTOInter>)amtDAO.findAll();
        return amtDTOL;
    }

    public Object editeAtmMachineType(AtmMachineTypeDTOInter amtDTO, int operation)throws Throwable{
        AtmMachineTypeDAOInter amtDAO = DAOFactory.createAtmMachineTypeDAO(null);
        switch(operation){
            case INSERT:
                amtDAO.insert(amtDTO);
                return "Insert Done";
            case UPDATE:
                amtDAO.update(amtDTO);
                return "Update Done";
            case DELETE:
                amtDAO.delete(amtDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }

}
