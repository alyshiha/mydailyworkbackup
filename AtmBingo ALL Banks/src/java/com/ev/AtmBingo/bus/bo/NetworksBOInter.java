/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.NetworksDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface NetworksBOInter {

    Boolean findDuplicateRecord(String Name) throws Throwable;
Object printPDFrep(String customer, String PrintedName, String network) throws Throwable;
    List<NetworksDTOInter> getRecord(String Name) throws Throwable;

    String deleteNetworks(NetworksDTOInter record);

    List<NetworksDTOInter> getNetworks() throws Throwable;

    String insertNetworks(NetworksDTOInter record);

    String updateNetworks(NetworksDTOInter record);

}
