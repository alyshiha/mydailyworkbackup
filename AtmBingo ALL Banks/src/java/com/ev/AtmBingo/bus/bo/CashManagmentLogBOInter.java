/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.CashManagmentLogDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface CashManagmentLogBOInter {

    Object printPDFrep(int user, Date dateF, Date dateT, String customer, String UserName, String PrintedName) throws Throwable;
    
    List<CashManagmentLogDTOInter> findRecord(Integer User, Date Fromdate, Date Todate) throws Throwable;

    void insertNetworks(String record) throws Throwable;
    
}
