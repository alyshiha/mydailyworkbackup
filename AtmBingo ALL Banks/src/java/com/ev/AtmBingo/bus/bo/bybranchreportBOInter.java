/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import java.util.Date;

/**
 *
 * @author Aly
 */
public interface bybranchreportBOInter {
Object getAllcards(Date datefrom, Date dateto) throws Throwable;
    Object getAll(int branch, Date datefrom, Date dateto) throws Throwable;
    String runReport(String dateFrom, String dateTo, String user, String cust) throws Throwable;
    String runReportcards(String dateFrom, String dateTo, String user, String cust) throws Throwable;
}
