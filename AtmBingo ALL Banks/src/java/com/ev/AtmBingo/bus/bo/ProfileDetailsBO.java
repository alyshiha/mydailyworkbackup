/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.ItemBingoDAOInter;
import com.ev.AtmBingo.bus.dao.MenuLabelDAOInter;
import com.ev.AtmBingo.bus.dao.ProfileDAOInter;
import com.ev.AtmBingo.bus.dao.ProfileMenuDAOInter;
import com.ev.AtmBingo.bus.dao.ProfileMenuItemDAOInter;
import com.ev.AtmBingo.bus.dto.ItemBingoDTOInter;
import com.ev.AtmBingo.bus.dto.MenuLabelDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileMenuDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileMenuItemDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author Administrator
 */
public class ProfileDetailsBO extends BaseBO implements ProfileDetailsBOInter ,Serializable{

    protected ProfileDetailsBO() {
        super();
    }

    public Object getProfiles(int rest) throws Throwable {
        ProfileDAOInter pDAO = DAOFactory.createProfileDAO(null);
        List<ProfileDTOInter> pDTOL = (List<ProfileDTOInter>) pDAO.findAll(rest);
        return pDTOL;
    }

    public TreeNode getMenuTree(int rest) throws Throwable {
        TreeNode ROOT = new DefaultTreeNode("Root", null);
        MenuLabelDAOInter mlDAO = DAOFactory.createMenuLabelDAO(null);
        List<MenuLabelDTOInter> mlDTOL = (List<MenuLabelDTOInter>) mlDAO.findsParents(rest);
        TreeNode[] roots = new TreeNode[mlDTOL.size()];
        int i = 0;
        for (MenuLabelDTOInter m : mlDTOL) {
            int j = 0;
            roots[i] = new DefaultTreeNode(m, ROOT);
            List<MenuLabelDTOInter> mlCH1DTOL = m.getChildMenu(rest,0);
            if (mlCH1DTOL != null) {
                TreeNode[] children1 = new TreeNode[mlCH1DTOL.size()];
                for (MenuLabelDTOInter mm : mlCH1DTOL) {
                    int z = 0;
                    children1[j] = new DefaultTreeNode(mm, roots[i]);
                    List<MenuLabelDTOInter> mlCH2DTOL = mm.getChildMenu( rest,0);
                    if (mlCH2DTOL != null) {
                        TreeNode[] children2 = new TreeNode[mlCH2DTOL.size()];
                        for (MenuLabelDTOInter mmm : mlCH2DTOL) {
                            int x = 0;
                            children2[z] = new DefaultTreeNode(mmm, children1[j]);
                            List<MenuLabelDTOInter> mlCH3DTOL = mmm.getChildMenu(rest,0);
                            if (mlCH3DTOL != null) {
                                TreeNode[] children3 = new TreeNode[mlCH3DTOL.size()];
                                for (MenuLabelDTOInter mmmm : mlCH3DTOL) {
                                    children3[x] = new DefaultTreeNode(mmmm, children2[z]);
                                    x++;
                                }
                            }
                            z++;
                        }
                    }
                    j++;
                }
            }
            i++;
        }
        return ROOT;
    }

    public Object findMenuLinks()throws Throwable{
        MenuLabelDAOInter mlDAO = DAOFactory.createMenuLabelDAO(null);
        List<MenuLabelDTOInter> mlDTOL = (List<MenuLabelDTOInter>)mlDAO.findLinks();
        return mlDTOL;
    }

    public Object getAllItemBingo()throws Throwable{
        ItemBingoDAOInter ibDAO = DAOFactory.createItemBingoDAO(null);
        List<ItemBingoDTOInter> ibDTOL = (List<ItemBingoDTOInter>)ibDAO.findAll();
        return ibDTOL;
    }

    public Object getItemBingo(int profileId,int menuId)throws Throwable{
        ProfileMenuItemDAOInter pmiDAO = DAOFactory.createProfileMenuItemDAO(null);
        List<ProfileMenuItemDTOInter> pmiDTOL = (List<ProfileMenuItemDTOInter>)pmiDAO.findByProfileIdAndMenuId(profileId, menuId);
        List<ItemBingoDTOInter> ibDTOL = (List<ItemBingoDTOInter>)getItemBingoDTOL(pmiDTOL);
        return ibDTOL;
    }

    public Object editProfile(ProfileDTOInter pDTO,int operation)throws Throwable{
        ProfileDAOInter pDAO = DAOFactory.createProfileDAO(null);
        switch(operation){
            case INSERT:
                pDAO.insert(pDTO);
                return "Insert Done";
            case UPDATE:
                pDAO.update(pDTO);
                return "Update Done";
            case DELETE:
                pDAO.delete(pDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }
public boolean  ProfileHasUser(ProfileDTOInter pDTO)throws Throwable{ ProfileDAOInter pDAO = DAOFactory.createProfileDAO(null);
 if(pDAO.ProfileHasUser(pDTO)){return true;}else{return false;}
}
    public Object editProfileMenu(ProfileMenuDTOInter pmDTO, int operation )throws Throwable{
        ProfileMenuDAOInter pmDAO = DAOFactory.createProfileMenuDAO(null);
        switch(operation){
            case INSERT:
                pmDAO.insert(pmDTO);
                return "Insert Done";
            case DELETE:
                pmDAO.deleteByMenuIdAndProfileId(pmDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }

    public Object editProfileMenuItem(ProfileMenuItemDTOInter pmDTO,int operation)throws Throwable{
        ProfileMenuItemDAOInter pmDAO = DAOFactory.createProfileMenuItemDAO(null);
        switch(operation){
            case INSERT:
                pmDAO.insert(pmDTO);
                return "Insert Done";
            case DELETE:
                pmDAO.deleteByAll(pmDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }

    private Object getItemBingoDTOL(List<ProfileMenuItemDTOInter> pmiDTOL)throws Throwable{
        ItemBingoDAOInter ibDAO = DAOFactory.createItemBingoDAO(null);
        List<ItemBingoDTOInter> ibDTOL = new ArrayList<ItemBingoDTOInter>();
        for(ProfileMenuItemDTOInter p : pmiDTOL){
            ItemBingoDTOInter ibDTO = (ItemBingoDTOInter)ibDAO.find(p.getItemId());
            ibDTOL.add(ibDTO);
        }
        return ibDTOL;
    }
}
