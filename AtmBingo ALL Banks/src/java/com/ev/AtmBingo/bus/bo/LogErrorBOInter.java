/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.ExceptionLoadingErrorsDTOInter;
import com.ev.AtmBingo.bus.dto.LoadingInsertErrorsDTOInter;
import com.ev.AtmBingo.bus.dto.matchingErrorsDataDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface LogErrorBOInter {

    public Object printmatchingErrorsData(String user, Date dateF, Date dateT, String customer) throws Throwable;

    List<matchingErrorsDataDTOInter> runReportmatchingErrorsDataexcel(String dateFrom, String dateTo) throws Throwable;

    Object printExceptionLoadingErrors(String user, Date dateF, Date dateT, String customer) throws Throwable;

    Object printLoadingInsertErrors(String user, Date dateF, Date dateT, String customer, int fileId) throws Throwable;

    List<ExceptionLoadingErrorsDTOInter> runReportExceptionLoadingErrorsexcel(String dateFrom, String dateTo) throws Throwable;

    List<LoadingInsertErrorsDTOInter> runReportLoadingInsertErrorsexcel(String dateFrom, String dateTo, int fileid) throws Throwable;

}
