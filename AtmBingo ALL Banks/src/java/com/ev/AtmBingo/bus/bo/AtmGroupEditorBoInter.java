/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AtmGroupEditorBoInter extends BaseBOInter {

    public Object GetGroups() throws Throwable;

    public Object getAtmGroup(int atmGroup) throws Throwable;

    public Object saveUserAtm(int GroupID, List<String> target) throws Throwable;
}
