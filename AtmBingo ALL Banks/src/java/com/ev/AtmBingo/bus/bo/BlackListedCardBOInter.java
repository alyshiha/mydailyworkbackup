/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.BlackListedCardDTOInter;

/**
 *
 * @author Administrator
 */
public interface BlackListedCardBOInter extends BaseBOInter{

    Object editeBlackListed(BlackListedCardDTOInter blcDTO, String oldCardNo, int operation) throws Throwable;

    Object getBlackListed() throws Throwable;

}
