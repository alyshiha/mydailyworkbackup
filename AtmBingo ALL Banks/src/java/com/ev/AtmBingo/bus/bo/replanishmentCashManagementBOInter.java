/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.replanishmentCashManagementDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface replanishmentCashManagementBOInter {

    List<replanishmentCashManagementDTOInter> runReportATMAmountexcel(String DateFrom, String DateTo, String ATMID, int ATMGroup, int user,Boolean release) throws Throwable;
    
}
