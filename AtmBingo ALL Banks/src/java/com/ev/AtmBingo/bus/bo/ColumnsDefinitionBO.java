/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.ColumnDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ColumnsDefinitionBO extends BaseBO implements ColumnsDefinitionBOInter,Serializable{

    protected ColumnsDefinitionBO() {
        super();
    }

    public Object editeColumnDefinition(Object obj, int operation)throws Throwable{
        ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);
        ColumnDTOInter cDTO = (ColumnDTOInter)obj;

       cDAO.updateNameOnly(cDTO);
       return "done" ; 
    }
    public Object AddCol()throws Throwable{
        ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);
       cDAO.AddCol();
       return "done" ; 
    }
    public Object getColumns()throws Throwable{
        ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);
        List<ColumnDTOInter> fcdDTOL = (List<ColumnDTOInter>)cDAO.findAll();
        return fcdDTOL;
    }
     public Object findAllColoumn()throws Throwable{
        ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);
        List<ColumnDTOInter> fcdDTOL = (List<ColumnDTOInter>)cDAO.findAllColoumn();
        return fcdDTOL;
    }
    //findAllColoumn

}
