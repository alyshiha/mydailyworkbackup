/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.AtmGroupDAOInter;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.AtmMachineTypeDAOInter;
import com.ev.AtmBingo.bus.dao.CassetteDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmDefinitionBO extends BaseBO implements AtmDefinitionBOInter,Serializable{

    protected AtmDefinitionBO() {
        super();
    }

    public Object editeAtmMachine(Object obj, int operation) throws Throwable{
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        AtmMachineDTOInter amDTO = (AtmMachineDTOInter)obj;

        switch(operation){
            case INSERT:
                amDAO.insert(amDTO);
                return "Insert Done";
            case UPDATE:
                amDAO.update(amDTO);
                return "Update Done";
            case DELETE:
                amDAO.delete(amDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }
 public Object getAtmMachinesOutLic() throws Throwable{
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>)amDAO.findRecordOutLic();
        return amDTOL;
    }
    public Object getAtmMachines(UsersDTOInter u) throws Throwable{
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>)amDAO.findRecordGroupAll(u);
        return amDTOL;
    }
     public Object getCassetteGroup() throws Throwable{
        CassetteDAOInter amDAO = DAOFactory.createCassetteDAO(null);
        List<CassetteDTOInter> amDTOL = (List<CassetteDTOInter>)amDAO.findCassetteGroup();
        return amDTOL;
    }

    public Object getAtmGroups() throws Throwable{
        AtmGroupDAOInter agDAO = DAOFactory.createAtmGroupDAO(null);
        List<AtmGroupDTOInter> agDTOL = (List<AtmGroupDTOInter>)agDAO.findAll();
        return agDTOL;
    }

     public Object getGroupCount() throws Throwable{
      AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        String Res = (String)amDAO.findATMMachineGroupCount();
        return Res;
    }

    public Object getAtmMachineType()throws Throwable{
        AtmMachineTypeDAOInter amtDAO = DAOFactory.createAtmMachineTypeDAO(null);
        List<AtmMachineTypeDTOInter> amtDTOL = (List<AtmMachineTypeDTOInter>)amtDAO.findAll();
        return amtDTOL;
    }
}
