/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.AtmJournalDTOInter;
import com.ev.AtmBingo.bus.dto.RepStat13DTOInter;
import com.ev.AtmBingo.bus.dto.RepStat2DTOInter;
import com.ev.AtmBingo.bus.dto.RepStat4DTOInter;
import com.ev.AtmBingo.bus.dto.RepStat56DTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface RepStatReportBOInter {

    List<RepStat4DTOInter> getreport5(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable;

    List<RepStat4DTOInter> getreport4(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable;

    List<RepStat13DTOInter> getreport1(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable;
Object printrep8(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName,Boolean realesed) throws Throwable ;
Object printrep9(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName,List<AtmJournalDTOInter> rectoprint,Boolean realesed) throws Throwable ;
    List<RepStat2DTOInter> getreport2(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable;
public Object printrep6(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName,String networktype,String status) throws Throwable;
public Object printrep7(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName,String networktype,String status) throws Throwable;
    List<RepStat56DTOInter> getreport7(String dateFrom, String dateTo, int atmGroup, String atmid, String network, String status) throws Throwable;
    List<RepStat56DTOInter> getreport6(String dateFrom, String dateTo, int atmGroup, String atmid, String network, String status) throws Throwable;
//switch detail
    List<RepStat13DTOInter> getreport3(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable;

    Object printrep1(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable;

    Object printrep2(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable;

    Object printrep3(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable;

    Object printrep4(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable;

    Object printrep5(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable;

    Object printrep1excel(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable;

    Object printrep2excel(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable;

    Object printrep3excel(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable;

    Object printrep4excel(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable;

    Object printrep5excel(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable;

}
