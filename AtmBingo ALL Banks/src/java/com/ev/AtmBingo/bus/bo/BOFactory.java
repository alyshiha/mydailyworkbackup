/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

/**
 *
 * @author Administrator
 */
public class BOFactory {

    public static statementBOInter createstatementBO(Object obj) {
        return new statementBO();
    }

    public static DepositTransactionBOInter createDepositTransactionBO(Object obj) {
        return new DepositTransactionBO();
    }

    public static invalidReplanishmentsBOInter createinvalidReplanishmentsBO(Object obj) {
        return new invalidReplanishmentsBO();
    }

    public static HolidaysBOInter_1 createHolidaysBO_1(Object... obj) {
        return new HolidaysBO_1();
    }

    public static CashManagmentLogBOInter createCashManagmentLogBO(Object obj) {
        return new CashManagmentLogBO();
    }

    public static userPrivilegeBOInter createuserPrivilegeBO(Object obj) {
        return new userPrivilegeBO();
    }

    public static privilegeStatusNameBOInter createprivilegeStatusNameBO(Object obj) {
        return new privilegeStatusNameBO();
    }

    public static AccountNameBOInter createAccountName(Object obj) {
        return new AccountNameBO();
    }

    public static LogErrorBOInter createLogErrorBO(Object obj) {
        return new LogErrorBO();
    }

    public static replanishmentCashManagementBOInter createreplanishmentCashManagementBO(Object obj) {
        return new replanishmentCashManagementBO();
    }

    public static AtmJournalBOInter createAtmJournalBO(Object obj) {
        return new AtmJournalBO();
    }

    public static AtmAccountsBOInter createAtmAccountsBO(Object obj) {
        return new AtmAccountsBO();
    }

    public static NetworkTypeCodeBOInter createNetworkTypeCodeBO(Object obj) {
        return new NetworkTypeCodeBO();
    }

    public static NetworksBOInter createNetworksBO(Object obj) {
        return new NetworksBO();
    }

    public static RepStatReportBOInter createRepStatReportBO(Object obj) {
        return new RepStatReportBO();
    }

    public static NotePresentedBOInter createNotePresentedBO(Object obj) {
        return new NotePresentedBO();
    }

    public static bybranchreportBOInter createbybranchreportBO(Object obj) {
        return new bybranchreportBO();
    }

    public static FileViewBOInter createFileViewBO(Object obj) {
        return new FileViewBO();
    }

    public static ExportTemplateDetailBoInter createExportTemplateDetailBo(Object obj) {
        return new ExportTemplateDetailBo();
    }

    public static BranchReasonBOInter createBranchReasonBO(Object obj) {
        return new BranchReasonBO();
    }

    public static allbrachtransactionssearchBOInter createallbrachtransactionssearchBO(Object... obj) {
        return new allbrachtransactionssearchBO();
    }

    public static branchcodeBoInter createbranchcodeBO(Object... obj) {
        return new branchcodeBo();
    }

    public static usersbranchBOInter createusersbranchBO(Object... obj) {
        return new usersbranchBo();
    }

    public static allbranchtransactionsBOInter createallbranchtransactionsBO(Object... obj) {
        return new allbranchtransactionsBo();
    }

    public static useratmbranchBOInter createuseratmbranchBO(Object... obj) {
        return new useratmbranchBo();
    }

    public static ExportTemplateBoInter createExportTemplateBo(Object obj) {
        return new ExportTemplateBo();
    }

    public static ExportBoInter createExportBo(Object obj) {
        return new ExportBo();
    }

    public static menulabelsBOInter createmenulabelsBO(Object... obj) {
        return new menulabelsBO();
    }

    public static ExportLogBOInter createExportLogBO(Object obj) {
        return new ExportLogBO();
    }

    public static CorrectiveEntryLogBOInter createCorrectiveEntryLogBO(Object obj) {
        return new CorrectiveEntryLogBO();
    }

    public static AutoRepAtmToGroupBoInter createAutoRepAtmToGroupBo(Object obj) {
        return new AutoRepAtmToGroupBO();
    }

    public static DeleteTransactionsDataBOInter createDeleteTransactionsDataBO(Object obj) {
        return new DeleteTransactionsDataBO();
    }

    public static PendingAtmsBOInter createPendingAtmsBo(Object obj) {
        return new PendingAtmsBO();
    }

    public static ATMMachineLICBOINTER createATMMachineLICBOINTER(Object obj) {
        return new ATMMachineLICBO();
    }

    public static AtmGroupEditorBoInter AtmGroupEditorBoInter(Object obj) {
        return new AtmGroupEditorBo();
    }

    public static HelpBOInter createHelpBOInter(Object obj) {
        return new HelpBO();
    }

    public static UserAuditRepBOInter createUserAuditBOInter(Object obj) {
        return new UserAuditRepBO();
    }

    public static RepStatBORepInter createLoadamoutRepInter(Object obj) {
        return new RepStatBORep();
    }

    public static RevenueRepBOInter createRevenueBOInter(Object obj) {
        return new RevenueRepBO();
    }

    public static SwGlDifferenceRepBOInter createSwGLInter(Object obj) {
        return new SwGlDifferenceRepBO();
    }

    public static NetworkBOInter createNetworkBOInter(Object obj) throws Throwable {
        return new NetworkBO();
    }

    public static ReplanishmentTempletesBOInter createReplanishmenTempletesBOInter(Object obj) throws Throwable {
        return new ReplanishmentTempletesBO();
    }

    public static ReplanishmentHistoryRepBOInter createReplanishmentHistoryRepBO(Object obj) throws Throwable {
        return new ReplanishmentHistoryRepBO();
    }

    public static DefualtColumnsBOInter createDefualtColumnsBO(Object obj) throws Throwable {
        return new DefualtColumnsBO();
    }

    public static UserRestBOInter createUserRestBO(Object obj) throws Throwable {
        return new UserRestBO();
    }

    public static ProfileRestBOInter createProfileRestBO(Object obj) throws Throwable {
        return new ProfileRestBO();
    }

    public static MenuRestBOInter createMenuRestBO(Object obj) throws Throwable {
        return new MenuRestBO();
    }

    public static ChangePassBOInter createChangePassBO(Object obj) throws Throwable {
        return new ChangePassBO();
    }

    public static ReplanishmentReportBOInter createReplanishmentReportBO(Object obj) throws Throwable {
        return new ReplanishmentReportBO();
    }

    public static PrivilageBOInter createPrivilageBO(Object obj) throws Throwable {
        return new PrivilageBO();
    }

    public static UsersProfileBOInter createUsersProfileBO(Object obj) throws Throwable {
        return new UsersProfileBO();
    }

    public static ProfileDetailsBOInter createProfileDetailsBO(Object obj) throws Throwable {
        return new ProfileDetailsBO();
    }

    public static DashBoardBOInter createDashBoardBO(Object obj) throws Throwable {
        return new DashBoardBO();
    }

    public static AtmsTotalRepBOInter createAtmsTotalRepBO(Object obj) throws Throwable {
        return new AtmsTotalRepBO();
    }

    public static BanksTotalRepBOInter createBanksTotalRepBO(Object obj) throws Throwable {
        return new BanksTotalRepBO();
    }

    public static CardNoRepBOInter createCardNoRepBO(Object obj) throws Throwable {
        return new CardNoRepBO();
    }

    public static AtmStatByTransRepBOInter createAtmStatByTransRepBO(Object obj) throws Throwable {
        return new AtmStatByTransRepBO();
    }

    public static AtmStatByCashRepBOInter createAtmStatByCashRepBO(Object obj) throws Throwable {
        return new AtmStatByCashRepBO();
    }

    public static MissingJournalsRepBOInter createMissingJournalsRepBO(Object obj) throws Throwable {
        return new MissingJournalsRepBO();
    }

    public static AtmRemainingRepBOInter createAtmRemainingRepBO(Object obj) throws Throwable {
        return new AtmRemainingRepBO();
    }

    public static JobMonitorBOInter createJobMonitorBO(Object obj) throws Throwable {
        return new JobMonitorBO();
    }

    public static TransactionTypeBOInter createTransactionTypeBO(Object obj) throws Throwable {
        return new TransactionTypeBO();
    }

    public static ValidationRulesBOInter createValidationRulesBO(Object obj) throws Throwable {
        return new ValidationRulesBO();
    }

    public static UserActivityBOInter createUserActivityBO(Object obj) throws Throwable {
        return new UserActivityBO();
    }

    public static ReplanishmentDetailsBOInter createReplanishmentBO(Object obj) throws Throwable {
        return new ReplanishmentDetailsBO();
    }

    public static RejectedBOInter createRejectedBO(Object obj) throws Throwable {
        return new RejectedBO();
    }

    public static DisputesBOInter createDisputeBO(Object obj) throws Throwable {
        return new DisputesBO();
    }

    public static UserAtmsBOInter createUserAtmBO(Object obj) throws Throwable {
        return new UserAtmsBO();
    }

    public static UsersBOInter createUserBO(Object obj) throws Throwable {
        return new UsersBO();
    }

    public static ResponseCodesDefinitionBOInter createResponseCodeDefBO(Object obj) throws Throwable {
        return new ResponseCodesDefinitionBO();
    }

    public static CurrencyDefinitionBOInter createCurrencyDefBO(Object obj) throws Throwable {
        return new CurrencyDefinitionBO();
    }

    public static CasseteBOInter createCasseteBO(Object obj) throws Throwable {
        return new CasseteBO();
    }

    public static BlackListedCardBOInter createBlackListedBO(Object obj) throws Throwable {
        return new BlackListedCardBO();
    }

    public static SystemParametersBOInter createSystemParaBO(Object obj) throws Throwable {
        return new SystemParametersBO();
    }

    public static AtmGroupBOInter createAtmGroupBO(Object obj) throws Throwable {
        return new AtmGroupBO();
    }

    public static AtmMachineTypesBOInter createAtmMachineTypesBO(Object obj) {
        return new AtmMachineTypesBO();
    }

    public static AtmFileTemplateBOInter createAtmFileTemplateBO(Object obj) {
        return new AtmFileTemplateBO();
    }

    public static CopyFileSetupBOInter createCopyingFileBO(Object obj) {
        return new CopyFileSetupBO();
    }

    public static WorkingTimesBOIntert createWorkingTimesBO(Object obj) {
        return new WorkingTimesBO();
    }

    public static TimeShiftsBOInter createTimeShiftBO(Object obj) {
        return new TimeShiftsBO();
    }

    public static TransactionStatusBOInter createTransactionStatusBO(Object obj) {
        return new TransactionStatusBO();
    }

    public static ColumnsDefinitionBOInter createColumnDefinitionBO(Object obj) {
        return new ColumnsDefinitionBO();
    }

    public static BlockUsersBOInter createBlockUsersBO(Object obj) {
        return new BlockUsersBO();
    }

    public static AtmFileLogBOInter createAtmFileLogBO(Object obj) {
        return new AtmFileLogBO();
    }

    public static AtmDefinitionBOInter createAtmDefinitionBO(Object obj) {
        return new AtmDefinitionBO();
    }

    public static DisputeRulesBOInter createDisputeRulesBO(Object obj) {
        return new DisputeRulesBO();
    }

    public static TransactionsBOInter createTransactionBO(Object obj) {
        return new TransactionsBO();
    }

    public static HolidaysBOInter createHolidaysBO(Object obj) {
        return new HolidaysBO();
    }

    public static EmergencyHolidaysBOInter createEmergencyHolidaysBO(Object obj) {
        return new EmergencyHolidaysBO();
    }

    public static BlockReasonBOInter createBlockReasonBO(Object obj) {
        return new BlockReasonBO();
    }

    public static MatchingSettingBOInter createMatchingSettingBO(Object obj) {
        return new MatchingSettingBO();
    }

    public static LoginBOInter createLoginBO(Object obj) {
        return new LoginBO();
    }

    public static BingoLogBOInter createBingoLogBO(Object obj) {
        return new BingoLogBO();
    }

    public static NotificationBOInter createNotificationBO(Object obj) {
        return new NotificationBO();
    }

    public static SettlementLogBOInter createSettlementLogBO(Object obj) {
        return new SettlementLogBO();
    }
}
