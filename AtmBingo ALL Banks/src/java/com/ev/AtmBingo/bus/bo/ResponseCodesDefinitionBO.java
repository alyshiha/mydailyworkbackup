/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.TransactionResponseCodeDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionResponseMasterDAOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseMasterDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ResponseCodesDefinitionBO extends BaseBO implements ResponseCodesDefinitionBOInter,Serializable {

    protected ResponseCodesDefinitionBO() {
        super();
    }

    public Object getResponseCode(TransactionResponseMasterDTOInter cmDTO) throws Throwable {
        TransactionResponseCodeDAOInter cDAO = DAOFactory.createTransactionResponseCodeDAO(null);
        List<TransactionResponseCodeDTOInter> cDTOL = (List<TransactionResponseCodeDTOInter>) cDAO.findByMaster(cmDTO);
        return cDTOL;
    }

    public int getDefFlagCount(int cmDTO,int id) throws Throwable {
        TransactionResponseCodeDAOInter cDAO = DAOFactory.createTransactionResponseCodeDAO(null);
        int cDTOL =  cDAO.getDefFlagCount(cmDTO,id);
        return cDTOL;
    }

    public Object editeResponseCode(TransactionResponseCodeDTOInter cDTO, int operation) throws Throwable {
        TransactionResponseCodeDAOInter cDAO = DAOFactory.createTransactionResponseCodeDAO(null);
        switch (operation) {
            case INSERT:
                cDAO.insert(cDTO);
                return "Insert Done";
            case UPDATE:
                cDAO.update(cDTO);
                return "Update Done";
            case DELETE:
                cDAO.delete(cDTO);
                return "Delete Done";
            default:
                return "Their Is No Operation";
        }
    }

    public Object getCurrencyMaster() throws Throwable {
        TransactionResponseMasterDAOInter cmDAO = DAOFactory.createTransactionResponseMasterDAO(null);
        List<TransactionResponseMasterDTOInter> cmDTOL = (List<TransactionResponseMasterDTOInter>) cmDAO.findAll();
        return cmDTOL;
    }

    public Object editeCurrencyMaster(TransactionResponseMasterDTOInter cmDTO, int operation) throws Throwable {
        TransactionResponseMasterDAOInter cmDAO = DAOFactory.createTransactionResponseMasterDAO(null);
        TransactionResponseCodeDAOInter cDAO = DAOFactory.createTransactionResponseCodeDAO(null);

        switch (operation) {
            case INSERT:
                cmDAO.insert(cmDTO);
                return "Insert Done";
            case UPDATE:

                cmDAO.update(cmDTO);
                return "Update Done";
            case DELETE:
                List<TransactionResponseCodeDTOInter> c = (List<TransactionResponseCodeDTOInter>) cDAO.findByMaster(cmDTO);
                if (c.size() != 0) {
                    for (TransactionResponseCodeDTOInter cc : c) {
                        cDAO.delete(cc);
                    }
                }
                cmDAO.delete(cmDTO);
                return "Delete Done";
            default:
                return "Their Is No Operation";
        }
    }
}
