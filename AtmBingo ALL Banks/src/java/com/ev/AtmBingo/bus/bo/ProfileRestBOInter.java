/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.ProfileDTOInter;

/**
 *
 * @author Administrator
 */
public interface ProfileRestBOInter {

    Object getProfiles(int rest) throws Throwable;

    Object update(ProfileDTOInter pDTO) throws Throwable;

}
