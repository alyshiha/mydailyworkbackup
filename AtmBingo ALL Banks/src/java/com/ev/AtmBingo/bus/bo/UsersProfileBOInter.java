/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import java.util.List;

/**
 *
 * @author Administrator
 */
public interface UsersProfileBOInter {

    Object editUserProfile(List<String> target, int profileId) throws Throwable;

    Object getPickList(int profileId,int rest) throws Throwable;

    Object getProfiles(int rest) throws Throwable;

}
