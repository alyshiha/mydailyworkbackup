/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.LicenseDAOInter;
import com.ev.AtmBingo.bus.dto.LicenseDTOInter;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class HelpBO extends BaseBO implements HelpBOInter,Serializable{

    protected HelpBO() {
        super();
    }

    public Object getLicense()throws Throwable{
        LicenseDAOInter lDAO = DAOFactory.createLicenseDAO();
        LicenseDTOInter lDTO = (LicenseDTOInter) lDAO.findLicense();
        return lDTO;
    }

}
