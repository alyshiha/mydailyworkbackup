/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.AtmFileDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.LogErrorDAOInter;
import com.ev.AtmBingo.bus.dao.matchingErrorsDataDAOInter;
import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import com.ev.AtmBingo.bus.dto.ExceptionLoadingErrorsDTOInter;
import com.ev.AtmBingo.bus.dto.LoadingInsertErrorsDTOInter;
import com.ev.AtmBingo.bus.dto.matchingErrorsDataDTOInter;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRReportFont;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignReportFont;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author AlyShiha
 */
public class LogErrorBO implements LogErrorBOInter {

    private String getFileName(int FileId) throws Throwable {
        AtmFileDAOInter engin = DAOFactory.createAtmFileDAO(null);
        AtmFileDTOInter file = (AtmFileDTOInter) engin.find(FileId);
        return file.getName();
    }

    @Override
    public List<matchingErrorsDataDTOInter> runReportmatchingErrorsDataexcel(String dateFrom, String dateTo) throws Throwable {
        matchingErrorsDataDAOInter engin = DAOFactory.creatematchingErrorsDataDAO();
        List<matchingErrorsDataDTOInter> result = (List<matchingErrorsDataDTOInter>) engin.findLoadingInsertErrors(dateFrom, dateTo);
        return result;
    }

    @Override
    public List<LoadingInsertErrorsDTOInter> runReportLoadingInsertErrorsexcel(String dateFrom, String dateTo, int fileid) throws Throwable {
        LogErrorDAOInter engin = DAOFactory.createLogErrorDAO();
        List<LoadingInsertErrorsDTOInter> result = (List<LoadingInsertErrorsDTOInter>) engin.findLoadingInsertErrors(dateFrom, dateTo, fileid);
        return result;
    }

    @Override
    public List<ExceptionLoadingErrorsDTOInter> runReportExceptionLoadingErrorsexcel(String dateFrom, String dateTo) throws Throwable {
        LogErrorDAOInter engin = DAOFactory.createLogErrorDAO();
        List<ExceptionLoadingErrorsDTOInter> result = (List<ExceptionLoadingErrorsDTOInter>) engin.findExceptionLoadingErrors(dateFrom, dateTo);
        return result;
    }

    @Override
    public Object printLoadingInsertErrors(String user, Date dateF, Date dateT, String customer, int fileId) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        String filename;
        params.put("user", user);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        params.put("customer", customer);
        params.put("FileID", fileId);
        if (fileId == 0) {
            filename = "All";
        } else {
            filename = getFileName(fileId);
        }
        params.put("FileName", filename);
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/LoadingInsertErrors.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            JRReportFont font = new JRDesignReportFont();
            font.setPdfFontName("c:/windows/fonts/arial.ttf");
            font.setPdfEncoding(com.lowagie.text.pdf.BaseFont.IDENTITY_H);
            font.setPdfEmbedded(true);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "LoadingInsertErrors" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Object printExceptionLoadingErrors(String user, Date dateF, Date dateT, String customer) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        params.put("customer", customer);
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/ExceptionLoadingErrors.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            JRReportFont font = new JRDesignReportFont();
            font.setPdfFontName("c:/windows/fonts/arial.ttf");
            font.setPdfEncoding(com.lowagie.text.pdf.BaseFont.IDENTITY_H);
            font.setPdfEmbedded(true);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "ExceptionLoadingErrors" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Object printmatchingErrorsData(String user, Date dateF, Date dateT, String customer) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        params.put("customer", customer);
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/MatchingErrorsData.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            JRReportFont font = new JRDesignReportFont();
            font.setPdfFontName("c:/windows/fonts/arial.ttf");
            font.setPdfEncoding(com.lowagie.text.pdf.BaseFont.IDENTITY_H);
            font.setPdfEmbedded(true);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "MatchingErrorsData" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

}
