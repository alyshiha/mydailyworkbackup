/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.replanishmentCashManagementDAOInter;
import com.ev.AtmBingo.bus.dto.replanishmentCashManagementDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class replanishmentCashManagementBO implements replanishmentCashManagementBOInter {

    @Override
    public List<replanishmentCashManagementDTOInter> runReportATMAmountexcel(String DateFrom, String DateTo, String ATMID, int ATMGroup, int user,Boolean release) throws Throwable {
        replanishmentCashManagementDAOInter rrDAO = DAOFactory.createreplanishmentCashManagementDAO();
        List<replanishmentCashManagementDTOInter> res = (List<replanishmentCashManagementDTOInter>) rrDAO.findRecord(ATMID, ATMGroup, DateFrom, DateTo,user, release);
        return res;
    }
}
