/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.DeleteTransactionsDataDAOInter;
import com.ev.AtmBingo.bus.dto.DeleteTransactionsDataDTO;
import com.ev.AtmBingo.bus.dto.DeleteTransactionsDataDTOInter;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */

public class DeleteTransactionsDataBO extends BaseBO implements DeleteTransactionsDataBOInter ,Serializable{

    protected DeleteTransactionsDataBO() {
        super();
    }

    public void  DeleteTransactionsData(DeleteTransactionsDataDTOInter DTDD) throws Throwable {
        DeleteTransactionsDataDAOInter DTD = DAOFactory.createDeleteTransactionsDataDAO();
        DTD.DeleteTransactionsData(DTDD);
        return;
    }

}
