
        
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.privilegeStatusNameDAOInter;
import com.ev.AtmBingo.bus.dto.privilegeStatusNameDTOInter;
import java.sql.Connection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author AlyShiha
 */
public class privilegeStatusNameBO implements privilegeStatusNameBOInter  {

       public Object printPDFrep(String customer, String PrintedName, String name) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        if ("null".equals(name) || name == null) {
            params.put("name", "%%");
        } else {
            params.put("name", "%" + name + "%");
        }
        params.put("customer", customer);
        params.put("userPrint", PrintedName);

        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/privStatus.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "privStatus" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public List<privilegeStatusNameDTOInter> findRecord(String Name) throws Throwable {
        privilegeStatusNameDAOInter engin = DAOFactory.createprivilegeStatusName();
        List<privilegeStatusNameDTOInter> records = engin.findRecord(Name);
        return records;
    }
    
     @Override
    public List<privilegeStatusNameDTOInter> findprivassigned(String Userid, String MenuID) throws Throwable {
        privilegeStatusNameDAOInter engin = DAOFactory.createprivilegeStatusName();
        List<privilegeStatusNameDTOInter> records = engin.findprivassigned(Userid,MenuID);
        return records;
    }

    
    @Override
    public List<privilegeStatusNameDTOInter> findAll() throws Throwable {
        privilegeStatusNameDAOInter engin = DAOFactory.createprivilegeStatusName();
        List<privilegeStatusNameDTOInter> records = engin.findAll();
        return records;
    }

    
    @Override
    public String insertRecord(privilegeStatusNameDTOInter record) {
        try {
            privilegeStatusNameDAOInter engin = DAOFactory.createprivilegeStatusName();
            engin.insert(record);
            return "Record Has Been Succesfully Added";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    
    @Override
    public String updateRecord(privilegeStatusNameDTOInter record) {
        try {
            privilegeStatusNameDAOInter engin = DAOFactory.createprivilegeStatusName();
            engin.update(record);
            return "Record Has Been Succesfully Updates";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }
 @Override
    public String insertpriv(String Userid, String MenuID, String PrevID) {
        try {
            privilegeStatusNameDAOInter engin = DAOFactory.createprivilegeStatusName();
            engin.insertpriv(Userid,MenuID,PrevID);
            return "Record Has Been Succesfully Saved";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }
     @Override
    public String deleteprivassigned(String Userid, String MenuID) {
        try {
            privilegeStatusNameDAOInter engin = DAOFactory.createprivilegeStatusName();
            engin.deleteprivassigned(Userid,MenuID);
            return "Record Has Been Succesfully Deleted";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }
    @Override
    public String deleteRecord(privilegeStatusNameDTOInter record) {
        try {
            privilegeStatusNameDAOInter engin = DAOFactory.createprivilegeStatusName();
            return engin.delete(record);
            
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }
}
