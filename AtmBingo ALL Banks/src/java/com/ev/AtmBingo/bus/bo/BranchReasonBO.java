 
         
         /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import static com.ev.AtmBingo.base.bo.BaseBOInter.DELETE;
import static com.ev.AtmBingo.base.bo.BaseBOInter.INSERT;
import static com.ev.AtmBingo.base.bo.BaseBOInter.UPDATE;
import com.ev.AtmBingo.bus.dao.BlockReasonDAOInter;
import com.ev.AtmBingo.bus.dao.BranchReasonDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.BlockReasonDTOInter;
import com.ev.AtmBingo.bus.dto.BranchReasonDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class BranchReasonBO extends BaseBO implements BranchReasonBOInter ,Serializable{

    protected BranchReasonBO() {
        super();
    }

    @Override
    public Object editeBlockReason(Object obj, int operation) throws Throwable{
        BranchReasonDAOInter brDAO = DAOFactory.createbranchreasonDAO();
        BranchReasonDTOInter brDTO = (BranchReasonDTOInter)obj;
        
        switch(operation){
            case INSERT:
                brDAO.insert(brDTO);
                return "Insert Done";
            case UPDATE:
                brDAO.update(brDTO);
                return "Update Done";
            case DELETE:
                brDAO.delete(brDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }

    @Override
    public Object getBlockReasons() throws Throwable{
        BranchReasonDAOInter brDAO = DAOFactory.createbranchreasonDAO();
        List<BranchReasonDTOInter> brDTOL = (List<BranchReasonDTOInter>)brDAO.findAll();
        return brDTOL;
    }

}
