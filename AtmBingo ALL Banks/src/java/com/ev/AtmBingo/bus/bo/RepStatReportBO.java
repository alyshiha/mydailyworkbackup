/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.AtmRemainingRepDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.RepStatDAOInter;
import com.ev.AtmBingo.bus.dto.AtmJournalDTOInter;
import com.ev.AtmBingo.bus.dto.RepStat13DTOInter;
import com.ev.AtmBingo.bus.dto.RepStat2DTOInter;
import com.ev.AtmBingo.bus.dto.RepStat4DTOInter;
import com.ev.AtmBingo.bus.dto.RepStat56DTOInter;
import com.ev.AtmBingo.bus.dto.atmremainingDTOInter;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRReportFont;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignReportFont;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author AlyShiha
 */
public class RepStatReportBO extends BaseBO implements RepStatReportBOInter {

    public RepStatReportBO() {
        super();
    }

    private String grpName(int grpId) throws Throwable {
        return super.getGrpName(grpId);
    }

    @Override
    public List<RepStat13DTOInter> getreport1(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable {
        RepStatDAOInter engin = DAOFactory.createRepStatDAO();
        List<RepStat13DTOInter> amDTOL = (List<RepStat13DTOInter>) engin.findReport1(dateFrom, dateTo, atmGroup, atmid);
        return amDTOL;
    }

    @Override
    public List<RepStat2DTOInter> getreport2(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable {
        RepStatDAOInter engin = DAOFactory.createRepStatDAO();
        List<RepStat2DTOInter> amDTOL = (List<RepStat2DTOInter>) engin.findReport2(dateFrom, dateTo, atmGroup, atmid);
        return amDTOL;
    }

    @Override
    public List<RepStat13DTOInter> getreport3(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable {
        RepStatDAOInter engin = DAOFactory.createRepStatDAO();
        List<RepStat13DTOInter> amDTOL = (List<RepStat13DTOInter>) engin.findReport3(dateFrom, dateTo, atmGroup, atmid);
        return amDTOL;
    }

    @Override
    public List<RepStat4DTOInter> getreport4(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable {
        RepStatDAOInter engin = DAOFactory.createRepStatDAO();
        List<RepStat4DTOInter> amDTOL = (List<RepStat4DTOInter>) engin.findReport4(dateFrom, dateTo, atmGroup, atmid);
        return amDTOL;
    }

    @Override
    public List<RepStat56DTOInter> getreport6(String dateFrom, String dateTo, int atmGroup, String atmid, String network, String status) throws Throwable {
        RepStatDAOInter engin = DAOFactory.createRepStatDAO();
        List<RepStat56DTOInter> amDTOL = (List<RepStat56DTOInter>) engin.findReport6(dateFrom, dateTo, atmGroup, atmid, network, status);
        return amDTOL;
    }

    @Override
    public List<RepStat56DTOInter> getreport7(String dateFrom, String dateTo, int atmGroup, String atmid, String network, String status) throws Throwable {
        RepStatDAOInter engin = DAOFactory.createRepStatDAO();
        List<RepStat56DTOInter> amDTOL = (List<RepStat56DTOInter>) engin.findReport7(dateFrom, dateTo, atmGroup, atmid, network, status);
        return amDTOL;
    }

    private void export(String file, JasperPrint jasperPrint) throws JRException {
        JRXlsExporter exporter = new JRXlsExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
        exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
        exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
        exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, file);
        exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
        exporter.exportReport();
    }

    @Override
    public List<RepStat4DTOInter> getreport5(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable {
        RepStatDAOInter engin = DAOFactory.createRepStatDAO();
        List<RepStat4DTOInter> amDTOL = (List<RepStat4DTOInter>) engin.findReport5(dateFrom, dateTo, atmGroup, atmid);
        return amDTOL;
    }

    @Override
    public Object printrep1(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }

        params.put("ATMNAME", AtmName);
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/rep_1.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);

            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);

            JRReportFont font = new JRDesignReportFont();
            font.setPdfFontName("c:/windows/fonts/arial.ttf");
            font.setPdfEncoding(com.lowagie.text.pdf.BaseFont.IDENTITY_H);
            font.setPdfEmbedded(true);

            Calendar c = Calendar.getInstance();
            String uri;
            uri = "ATM Reconcile Summary" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime());
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri + ".pdf");

            try {
                String x = pdfPath + uri + ".pdf";
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Object printrep2(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("ATMNAME", AtmName);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/rep_2.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "ATM Repl.Activity" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Object printrep3(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("ATMNAME", AtmName);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/rep_3.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "ATM Reconcile Detail" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Object printrep4(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("ATMNAME", AtmName);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/rep_4.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "Switch Reconciliation Summary Report" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Object printrep5(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("ATMNAME", AtmName);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/rep_5.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "Switch Reconciliation Detailed Report" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Object printrep6(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName, String networktype, String status) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("ATMNAME", AtmName);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("networktype", networktype);
        params.put("status", status);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/rep_6.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "Financial Journal Detailed" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    public Object printrep7(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName, String networktype, String status) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("ATMNAME", AtmName);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("networktype", networktype);
        params.put("status", status);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/rep_7.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "Statement Report" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Object printrep1excel(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }

        params.put("ATMNAME", AtmName);
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/excel_rep_1.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);

            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);

            JRReportFont font = new JRDesignReportFont();
            font.setPdfFontName("c:/windows/fonts/arial.ttf");
            font.setPdfEncoding(com.lowagie.text.pdf.BaseFont.IDENTITY_H);
            font.setPdfEmbedded(true);

            Calendar c = Calendar.getInstance();
            String uri;
            uri = "ATM Reconcile Summary" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".XLS";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            export(pdfPath + uri, jasperPrint);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Object printrep2excel(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("ATMNAME", AtmName);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/excel_rep_2.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "ATM Repl.Activity" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".XLS";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");

            export(pdfPath + uri, jasperPrint);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Object printrep3excel(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("ATMNAME", AtmName);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/excel_rep_3.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "ATM Reconcile Detail" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".XLS";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            export(pdfPath + uri, jasperPrint);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Object printrep4excel(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("ATMNAME", AtmName);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/excel_rep_4.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "Switch Reconciliation Summary Report" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".XLS";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");

            export(pdfPath + uri, jasperPrint);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Object printrep5excel(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("ATMNAME", AtmName);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/excel2_rep_5.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "Switch Reconciliation Detailed Report" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".XLS";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            export(pdfPath + uri, jasperPrint);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Object printrep8(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName, Boolean realesed) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }
        if ("All".equals(atmId)) {
            atmId = "999999";
        }

        params.put("ATMNAME", AtmName);
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        if (realesed) {
            params.put("release", "0");
        } else {
            params.put("release", "8");
        }

        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/rep_8.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);

            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);

            JRReportFont font = new JRDesignReportFont();
            font.setPdfFontName("c:/windows/fonts/arial.ttf");
            font.setPdfEncoding(com.lowagie.text.pdf.BaseFont.IDENTITY_H);
            font.setPdfEmbedded(true);

            Calendar c = Calendar.getInstance();
            String uri;
            uri = "Journal123Visa" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime());
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri + ".pdf");

            try {
                String x = pdfPath + uri + ".pdf";
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    public Object printrep9(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName, List<AtmJournalDTOInter> rectoprint, Boolean realesed) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        if (AtmName == "UnAssigned") {
            AtmName = "0";
        }

        params.put("ATMNAME", AtmName);
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", "");
        params.put("customer", customer);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        if (realesed) {
            params.put("release", "0");
        } else {
            params.put("release", "8");
        }

        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/rep_9.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);

            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);

            JRReportFont font = new JRDesignReportFont();
            font.setPdfFontName("c:/windows/fonts/arial.ttf");
            font.setPdfEncoding(com.lowagie.text.pdf.BaseFont.IDENTITY_H);
            font.setPdfEmbedded(true);

            Calendar c = Calendar.getInstance();
            String uri;
            uri = "ATM Journal" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime());
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri + ".pdf");

            try {
                String x = pdfPath + uri + ".pdf";
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

}
