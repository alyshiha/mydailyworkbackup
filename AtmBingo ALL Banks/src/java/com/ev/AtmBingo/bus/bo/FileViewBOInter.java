/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface FileViewBOInter extends BaseBOInter {

    Integer PrintLines(String mdDTO) throws Exception;

    String PrintLinesReport(String Seq) throws Exception;

    Object getAtmMachines(UsersDTOInter loggedIn) throws Throwable;

    int findByAtmId(String ATMID) throws Throwable;

    List<AtmFileDTOInter> dosearch(String atmInt, Date loadingFrom, Date loadingTo, String cardno, String sequence) throws Throwable;

    Object FindFiletransaction(AtmFileDTOInter mdDTO) throws Throwable;
}
