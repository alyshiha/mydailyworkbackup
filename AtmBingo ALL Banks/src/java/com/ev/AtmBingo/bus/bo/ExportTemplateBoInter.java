/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.EXPORTTEMPLETEDTOInter;

/**
 *
 * @author Administrator
 */
public interface ExportTemplateBoInter {

    Object DeleteTemplate(EXPORTTEMPLETEDTOInter cfDTO) throws Throwable;

    Object GetAllTemplates() throws Throwable;

    Object GetTemplate(EXPORTTEMPLETEDTOInter template) throws Throwable;

    Object InsertTemplate(EXPORTTEMPLETEDTOInter cfDTO) throws Throwable;

    Object UpdateTemplate(EXPORTTEMPLETEDTOInter cfDTO) throws Throwable;

}
