/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.NotePresentedDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface NotePresentedBOInter extends Serializable {

    String runReport(String dateFrom, String dateTo, Integer atmGroup, int userID, String atmAppID, String UserName, String Customer,String operator) throws Throwable;

    List<NotePresentedDTOInter> runReportExcel(String dateFrom, String dateTo, Integer atmGroup, int userID, String atmAppID,String operator) throws Throwable;
    
}
