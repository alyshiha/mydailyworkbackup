/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseMasterDTOInter;

/**
 *
 * @author Administrator
 */
public interface ResponseCodesDefinitionBOInter extends BaseBOInter{

    Object editeCurrencyMaster(TransactionResponseMasterDTOInter cmDTO, int operation) throws Throwable;

    Object editeResponseCode(TransactionResponseCodeDTOInter cDTO, int operation) throws Throwable;

    Object getCurrencyMaster() throws Throwable;

    Object getResponseCode(TransactionResponseMasterDTOInter cmDTO) throws Throwable;
  int getDefFlagCount(int cmDTO,int id) throws Throwable;
}
