/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.NetworkDetailsDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkGroupDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkMasterDTOInter;
import java.io.Serializable;

/**
 *
 * @author KhAiReE
 */
public interface NetworkBOInter extends Serializable , BaseBOInter {

    Object editeNetworkGroup(NetworkGroupDTOInter ngDTO, int operation) throws Throwable;

    Object getCurrency() throws Throwable;

    Object getNetworkGroup() throws Throwable;

    Object getNetworkMaster(int id) throws Throwable;

    Object getNetworkDetails(int id) throws Throwable;

    Object editeNetworkMasterSingle(NetworkMasterDTOInter nmDTO, int operation, Integer oldCurr, Integer oldTrans ) throws Throwable ;

    Object editeNetworkDetailsSingle(NetworkDetailsDTOInter nmDTO, int operation, Integer oldCurr, Integer oldTrans, String oldBin ) throws Throwable;

    Object getTransactionType() throws Throwable;

}
