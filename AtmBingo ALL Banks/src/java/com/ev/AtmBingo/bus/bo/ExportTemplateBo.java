/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.EXPORTTEMPLETEDAOInter;
import com.ev.AtmBingo.bus.dto.EXPORTTEMPLETEDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ExportTemplateBo extends BaseBO implements ExportTemplateBoInter,Serializable {

    protected ExportTemplateBo() {
        super();
    }

    public Object GetTemplate(EXPORTTEMPLETEDTOInter template) throws Throwable {
        EXPORTTEMPLETEDAOInter amDAO = DAOFactory.createEXPORTTEMPLETEDAO();
        EXPORTTEMPLETEDTOInter amDTOL = (EXPORTTEMPLETEDTOInter) amDAO.find(template);
        return amDTOL;
    }

    public Object GetAllTemplates() throws Throwable {
        EXPORTTEMPLETEDAOInter amtDAO = DAOFactory.createEXPORTTEMPLETEDAO();
        List<EXPORTTEMPLETEDTOInter> amtDTOL = (List<EXPORTTEMPLETEDTOInter>) amtDAO.findAll();
        return amtDTOL;
    }

    public Object InsertTemplate(EXPORTTEMPLETEDTOInter cfDTO) throws Throwable {
        EXPORTTEMPLETEDAOInter cfDAO = DAOFactory.createEXPORTTEMPLETEDAO();
        cfDAO.insert(cfDTO);
        return "Insert Done";
    }

    public Object UpdateTemplate(EXPORTTEMPLETEDTOInter cfDTO) throws Throwable {
        EXPORTTEMPLETEDAOInter cfDAO = DAOFactory.createEXPORTTEMPLETEDAO();
        cfDAO.update(cfDTO);
        return "Update Done";
    }

    public Object DeleteTemplate(EXPORTTEMPLETEDTOInter cfDTO) throws Throwable {
        EXPORTTEMPLETEDAOInter cfDAO = DAOFactory.createEXPORTTEMPLETEDAO();
        cfDAO.delete(cfDTO);
        return "Delete Done";

    }
}
