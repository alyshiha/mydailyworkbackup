/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.CopyFilesDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.FilesToMoveDAOInter;
import com.ev.AtmBingo.bus.dto.CopyFilesDTOInter;
import com.ev.AtmBingo.bus.dto.FilesToMoveDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class CopyFileSetupBO extends BaseBO implements CopyFileSetupBOInter,Serializable{

    protected CopyFileSetupBO() {
        super();
    }

    public Object getCopyFiles()throws Throwable{
        CopyFilesDAOInter cfDAO = DAOFactory.createCopyFilesDAO(null);
        List<CopyFilesDTOInter> cfDTOL = (List<CopyFilesDTOInter>)cfDAO.findAll();
        return cfDTOL;
    }
       public Object deletefile(List<FilesToMoveDTOInter> deleteitems)throws Throwable{
        FilesToMoveDAOInter cfDAO = DAOFactory.createFilesToMoveDAO(null);
        cfDAO.deletefile(deleteitems);
        return "Done";
    }
   public Object getAllFiles()throws Throwable{
        FilesToMoveDAOInter cfDAO = DAOFactory.createFilesToMoveDAO(null);
        List<FilesToMoveDTOInter> cfDTOL = (List<FilesToMoveDTOInter>)cfDAO.findAllFiles();
        return cfDTOL;
    }

    public Object editeCopyFile(CopyFilesDTOInter cfDTO,String oldSourceFolder,int operation)throws Throwable{
        CopyFilesDAOInter cfDAO = DAOFactory.createCopyFilesDAO(null);
        switch(operation){
            case INSERT:
                cfDAO.insert(cfDTO);
                return "Insert Done";
            case UPDATE:
                cfDAO.update(cfDTO,oldSourceFolder);
                return "Update Done";
            case DELETE:
                cfDAO.delete(cfDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }
}
