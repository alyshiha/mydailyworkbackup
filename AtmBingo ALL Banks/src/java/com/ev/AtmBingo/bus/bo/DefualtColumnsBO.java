/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.ColumnDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.DefualtColumnsDAOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DefualtColumnsDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Administrator
 */
public class DefualtColumnsBO extends BaseBO implements DefualtColumnsBOInter,Serializable {

    protected DefualtColumnsBO() {
        super();
    }

    public Object getPickList() throws Throwable {
        ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);
        List<ColumnDTOInter> cDTOLT = (List<ColumnDTOInter>) cDAO.findDefualt();
        List<ColumnDTOInter> cDTOLS = (List<ColumnDTOInter>) cDAO.findNotDefualt();

        DualListModel<String> pick = new DualListModel();

        List<String> source = new ArrayList<String>();
        for (ColumnDTOInter c : cDTOLS) {
            // if (c.getId() != 1) {
            source.add(c.getHeader());
            //}
        }

        List<String> target = new ArrayList<String>();
        for (ColumnDTOInter c : cDTOLT) {
            // if (c.getId() != 1) {
            target.add(c.getHeader());
            //}
        }

        pick.setSource(source);
        pick.setTarget(target);

        return pick;

    }

    public Object save(List<String> target, List<String> source) throws Throwable {
        DefualtColumnsDAOInter dcDAO = DAOFactory.createDefualtColumnsDAO(null);
        ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);

        List<ColumnDTOInter> cDTOL = new ArrayList<ColumnDTOInter>();
        List<ColumnDTOInter> DDTOL = new ArrayList<ColumnDTOInter>();
        if (target.size() > 0) {
            cDTOL = (List<ColumnDTOInter>) cDAO.searchByName(target);
        }
        if (source.size() > 0) {
            DDTOL = (List<ColumnDTOInter>) cDAO.searchByName(source);
        }

       // dcDAO.deleteAll();
        dcDAO.defaultreset();
        if (target.size() > 0) {
            dcDAO.updatedefault(cDTOL);
        }
        if (source.size() > 0) {
            dcDAO.updateundefault(DDTOL);
        }

        return "done";
    }
}
