/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.UsersDTOInter;

/**
 *
 * @author Administrator
 */
public interface UserRestBOInter {

    Object getUsers(int rest) throws Throwable;

    Object update(UsersDTOInter uDTO) throws Throwable;

}
