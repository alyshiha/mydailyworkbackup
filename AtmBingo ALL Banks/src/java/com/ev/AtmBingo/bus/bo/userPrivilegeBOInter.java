/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.userPrivilegeDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface userPrivilegeBOInter {

    Boolean findDuplicateRecord(int user, int menu, int prev, int id) throws Throwable;

    Boolean findDuplicateRecord(int user, int menu, int prev) throws Throwable;

    List<String> findPrivelage(Integer userid, String PageName) throws Throwable;

    String deleteRecord(userPrivilegeDTOInter record);

    List<userPrivilegeDTOInter> findAll() throws Throwable;

    List<userPrivilegeDTOInter> findRecord(String userid) throws Throwable;

    String insertRecord(userPrivilegeDTOInter record);
Object printPDFrep(String customer, String PrintedName, String name,int user) throws Throwable ;
    String updateRecord(userPrivilegeDTOInter record);

}
