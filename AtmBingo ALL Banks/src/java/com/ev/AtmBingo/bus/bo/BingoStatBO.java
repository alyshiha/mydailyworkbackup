/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dao.BingoStatDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.BingostatsearchDTOInter;
import com.ev.AtmBingo.bus.dto.DisputesDTOInter;
import com.ev.AtmBingo.bus.dto.MatchedDataDTOInter;
import com.ev.AtmBingo.bus.dto.TransStatDTOInter;
import com.ev.AtmBingo.bus.dto.disstatprintDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class BingoStatBO implements BingoStatBOInter,Serializable {

    @Override
    public Object GetTransactionDetailsbyatmexcel(Object... obj) throws Throwable {
        BingoStatDAOInter engin = DAOFactory.createBingoStatDAO();
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        if (Searchparm.getSearchtype().equals("Matched")) {
            List<disstatprintDTOInter> MatchRecords = (List<disstatprintDTOInter>) engin.GetTransactionStatDetailMatchbyatmexcel(Searchparm);
            return MatchRecords;
        } else {
            List<disstatprintDTOInter> DisRecords = (List<disstatprintDTOInter>) engin.GetTransactionStatDetailDispbyatmexcel(Searchparm);
            return DisRecords;
        }
    }
    @Override
    public Object GetTransactionDetailsbyatm(Object... obj) throws Throwable {
        BingoStatDAOInter engin = DAOFactory.createBingoStatDAO();
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        if (Searchparm.getSearchtype().equals("Matched")) {
            List<MatchedDataDTOInter> MatchRecords = (List<MatchedDataDTOInter>) engin.GetTransactionStatDetailMatchbyatm(Searchparm);
            return MatchRecords;
        } else {
            List<DisputesDTOInter> DisRecords = (List<DisputesDTOInter>) engin.GetTransactionStatDetailDispbyatm(Searchparm);
            return DisRecords;
        }
    }
    @Override
    public Object GetTransactionDetails(Object... obj) throws Throwable {
        BingoStatDAOInter engin = DAOFactory.createBingoStatDAO();
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        if (Searchparm.getSearchtype().equals("Matched")) {
            List<MatchedDataDTOInter> MatchRecords = (List<MatchedDataDTOInter>) engin.GetTransactionStatDetailMatch(Searchparm);
            return MatchRecords;
        } else {
            List<DisputesDTOInter> DisRecords = (List<DisputesDTOInter>) engin.GetTransactionStatDetailDisp(Searchparm);
            return DisRecords;
        }
    }
    @Override
    public Object GetTransactionDetailsexcel(Object... obj) throws Throwable {
        BingoStatDAOInter engin = DAOFactory.createBingoStatDAO();
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        if (Searchparm.getSearchtype().equals("Matched")) {
            List<disstatprintDTOInter> MatchRecords = (List<disstatprintDTOInter>) engin.GetTransactionStatDetailMatchexcel(Searchparm);
            return MatchRecords;
        } else {
            List<disstatprintDTOInter> DisRecords = (List<disstatprintDTOInter>) engin.GetTransactionStatDetailDispexcel(Searchparm);
            return DisRecords;
        }
    }
    @Override
    public Object GetTransactionStat(Object... obj) throws Throwable {
        BingoStatDAOInter engin = DAOFactory.createBingoStatDAO();
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        List<TransStatDTOInter> StatRecord = (List<TransStatDTOInter>) engin.findBingoStatAll(Searchparm);
        return StatRecord;
    }
    
     public Object GetTransactionStatcount(Object... obj) throws Throwable {
        BingoStatDAOInter engin = DAOFactory.createBingoStatDAO();
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        TransStatDTOInter StatRecord = (TransStatDTOInter) engin.findBingoStatAllCount(Searchparm);
        return StatRecord;
    }
}
