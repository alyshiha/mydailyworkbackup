/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.PendingAtmsDAOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.PendingAtmsDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */

  public class PendingAtmsBO extends BaseBO implements PendingAtmsBOInter ,Serializable{

      protected PendingAtmsBO() {
        super();
    }
   public Object getPendingAtms() throws Throwable {
        PendingAtmsDAOInter PDAO = DAOFactory.CreatePendingAtmsDAO();
        List<PendingAtmsDTOInter> Result = (List<PendingAtmsDTOInter>) PDAO.findAllPendingAtms();
        return Result ;
    }
     public Object getAtmMachine() throws Throwable {
          PendingAtmsDAOInter PDAO = DAOFactory.CreatePendingAtmsDAO();
        List<AtmMachineDTOInter> Result = (List<AtmMachineDTOInter>) PDAO.findAtmMachineType();
        return Result ;
    }
     
     public Object SavePendingAtm(PendingAtmsDTOInter Target) throws Throwable {
        PendingAtmsDAOInter PDAO = DAOFactory.CreatePendingAtmsDAO();
        PDAO.saveAtm(Target);
        return "Saved" ;
    }
     public Object SavePendingAtms(List<PendingAtmsDTOInter> Target) throws Throwable {
        PendingAtmsDAOInter PDAO = DAOFactory.CreatePendingAtmsDAO();
        PDAO.save(Target);
        return "Saved" ;
    }
}
