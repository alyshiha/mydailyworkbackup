/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.bo.BaseBO;

import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.DisputeReportDAOInter;
import com.ev.AtmBingo.bus.dao.FileViewDAOInter;
import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.sql.Clob;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author Administrator
 */
public class FileViewBO extends BaseBO implements FileViewBOInter ,Serializable{

    protected FileViewBO() {
        super();
    }

    public Object getAtmMachines(UsersDTOInter loggedIn) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>) amDAO.findRecordGroup(loggedIn);
        return amDTOL;
    }

    public int findByAtmId(String ATMID) throws Throwable {
        AtmMachineDAOInter ATMNAME = DAOFactory.createAtmMachineDAO(null);
        AtmMachineDTOInter atm = (AtmMachineDTOInter) ATMNAME.findByAtmId(ATMID);
        return atm.getId();
    }

    public List<AtmFileDTOInter> dosearch(String atmInt, Date loadingFrom, Date loadingTo, String cardno, String sequence) throws Throwable {
        FileViewDAOInter FVDAO = DAOFactory.createFileViewDAO();
        return FVDAO.dosearch(atmInt, loadingFrom, loadingTo, cardno, sequence);
    }

    public Object FindFiletransaction(AtmFileDTOInter mdDTO) throws Throwable {
        FileViewDAOInter FVDAO = DAOFactory.createFileViewDAO();
        Clob mddtoi = (Clob) FVDAO.FindFiletransaction(mdDTO);
        return mddtoi;
    }

    public Integer PrintLines(String mdDTO) throws Exception {
        DisputeReportDAOInter drDAO = DAOFactory.createDisputeReportDAO(null);
        return drDAO.PrintLines(mdDTO);
    }

    public String PrintLinesReport(String Seq) throws Exception {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("sequence",Integer.valueOf(Seq));
       
        Connection conn =CoonectionHandler.getInstance().getConnection();
   
        try {
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/Trans_Data_Report.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri = "TransactionDataReport" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {

                String x = "../PDF/" + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;CoonectionHandler.getInstance().returnConnection(conn);
                return x;

            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            
            return "fail";
        }

    }
}
