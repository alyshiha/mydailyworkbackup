/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.PendingAtmsDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
 public interface PendingAtmsBOInter extends Serializable{

      Object getPendingAtms() throws Throwable;
      Object SavePendingAtm(PendingAtmsDTOInter Target) throws Throwable;
      Object getAtmMachine() throws Throwable ;
      Object SavePendingAtms(List<PendingAtmsDTOInter> Target) throws Throwable;

}
