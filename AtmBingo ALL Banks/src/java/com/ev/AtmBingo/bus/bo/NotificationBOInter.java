/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface NotificationBOInter extends Serializable{

    Object getNotifications () throws Throwable;

    Object getErrors () throws Throwable;


}
