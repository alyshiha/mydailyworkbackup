/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dao.AtmGroupDAOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import org.primefaces.model.TreeNode;

/**
 *
 * @author Administrator
 */
public interface AtmGroupBOInter extends BaseBOInter {

    Object getChildren(String name) throws Throwable;

    Object editeAtmGroup(AtmGroupDTOInter agDTO, String parent, int operation) throws Throwable;

    TreeNode getRoot();

    TreeNode[] getSelectedNode();

    void setRoot(TreeNode root);

    Object getAllGroups() throws Throwable;

    void setSelectedNode(TreeNode[] allNodes);
}
