/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import DBCONN.Session;
import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.dao.CoolInsert;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.base.util.ReportingTool;
import com.ev.AtmBingo.bus.dao.AtmGroupDAOInter;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.ColumnDAOInter;
import com.ev.AtmBingo.bus.dao.CurrencyMasterDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.DisputeReportDAOInter;
import com.ev.AtmBingo.bus.dao.DisputesDAOInter;
import com.ev.AtmBingo.bus.dao.MatchingTypeDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionResponseCodeDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionStatusDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionTypeDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DisputeReportDTOInter;
import com.ev.AtmBingo.bus.dto.DisputesDTOInter;
import com.ev.AtmBingo.bus.dto.MatchingTypeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.primefaces.model.DualListModel;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author Administrator
 */
public class DisputesBO extends BaseBO implements DisputesBOInter, Serializable {

    private String searchParameter;
    private String whereCluase;
    private BigDecimal totalAmount;

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getWhereCluase() {
        return whereCluase;
    }

    public void setWhereCluase(String whereCluase) {
        this.whereCluase = whereCluase;
    }

    public String getSearchParameter() {
        return searchParameter;
    }

    public void setSearchParameter(String searchParameter) {
        this.searchParameter = searchParameter;
    }
 public Object getAtmMachines2(UsersDTOInter loggedinUser, Integer grpId) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>) amDAO.findByAtmGrp2(grpId, loggedinUser.getUserId());
        return amDTOL;
    }
  public Object getAtmMachines3(UsersDTOInter loggedinUser) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>) amDAO.findByAtmGrp3(loggedinUser.getUserId());
        return amDTOL;
    }
    public Object getAtmMachines(UsersDTOInter loggedinUser, Integer grpId) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>) amDAO.findByAtmGrp(grpId, loggedinUser.getUserId());
        return amDTOL;
    }

    public Object getTransactionResponseCode() throws Throwable {
        TransactionResponseCodeDAOInter trcDAO = DAOFactory.createTransactionResponseCodeDAO(null);
        List<TransactionResponseCodeDTOInter> trcDTOL = (List<TransactionResponseCodeDTOInter>) trcDAO.findAll();
        return trcDTOL;
    }

    public Object getTransactionType() throws Throwable {
        TransactionTypeDAOInter ttDAO = DAOFactory.createTransactionTypeDAO(null);
        List<TransactionTypeDTOInter> ttDTOL = (List<TransactionTypeDTOInter>) ttDAO.findAll();
        return ttDTOL;
    }

    public Object getTransactionStatus() throws Throwable {
        TransactionStatusDAOInter tsDAO = DAOFactory.createTransactionStatusDAO(null);
        List<TransactionStatusDTOInter> tsDTOL = (List<TransactionStatusDTOInter>) tsDAO.findAll();
        return tsDTOL;
    }

    public Object insertBlackListed(DisputesDTOInter REC, String User) throws Throwable {
        DisputesDAOInter cmDAO = DAOFactory.createDisputesDAO(null);
        cmDAO.insertBlackListed(REC, User);
        return "Done";
    }

    public Object getCurrencyMaster() throws Throwable {
        CurrencyMasterDAOInter cmDAO = DAOFactory.createCurrencyMasterDAO(null);
        List<CurrencyMasterDTOInter> cmDTOL = (List<CurrencyMasterDTOInter>) cmDAO.findAll();
        return cmDTOL;
    }

    public Object getFileColumnDefinition() throws Throwable {
        ColumnDAOInter fcdDAO = DAOFactory.createColumnDAO(null);
        List<ColumnDTOInter> fcdDTOL = (List<ColumnDTOInter>) fcdDAO.findAll();
        return fcdDTOL;
    }

    public List<ColumnDTOInter> getColumns(List columns) throws Throwable {
        ColumnDAOInter fcdDAO = DAOFactory.createColumnDAO(null);
        List<ColumnDTOInter> fcdDTOL = new ArrayList<ColumnDTOInter>();
        /*  ColumnDTOInter c = (ColumnDTOInter) fcdDAO.find(1);
         fcdDTOL.add(c);
         for (int i = 0; i < columns.size(); i++) {
        
         String columnName = (String) columns.get(i);
         ColumnDTOInter fcdDTO = (ColumnDTOInter) fcdDAO.searchByName(columnName);
         fcdDTOL.add(fcdDTO);
         }
         */
        fcdDTOL = (List<ColumnDTOInter>) fcdDAO.searchByName(columns);
        return fcdDTOL;
    }

    public Object getMatchingType() throws Throwable {
        MatchingTypeDAOInter mtDAO = DAOFactory.createMatchingTypeDAO(null);
        List<MatchingTypeDTOInter> mtDTOL = (List<MatchingTypeDTOInter>) mtDAO.findComboBox();
        return mtDTOL;
    }

    public Object getSettledUsers() throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> uDTOL = (List<UsersDTOInter>) uDAO.findComboBox();
        return uDTOL;
    }

    public Integer PrintLines(String mdDTO) throws Exception {
        DisputeReportDAOInter drDAO = DAOFactory.createDisputeReportDAO(null);
        return drDAO.PrintLines(mdDTO);

    }

   public String PrintLinesReport(Integer Seq) throws Exception {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("sequence", Seq);

        Connection conn = CoonectionHandler.getInstance().getConnection();

        try {
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/Trans_Data_Report.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri = "TransactionDataReport" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {

                String x = "../PDF/" + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;

            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();

            return "fail";
        }

    }

    public Object getAtmGroup() throws Throwable {
        AtmGroupDAOInter agDAo = DAOFactory.createAtmGroupDAO(null);
        List<AtmGroupDTOInter> agDTOL = (List<AtmGroupDTOInter>) agDAo.findTree();
        return agDTOL;
    }

    public Object findReport(String fields, String whereCluase, UsersDTOInter logedUser, List<ColumnDTOInter> c, DisputesDTOInter[] dDTOArr, String cust, Integer mtInt, Integer listSize) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        DisputeReportDAOInter drDAO = DAOFactory.createDisputeReportDAO(null);
        List<DisputeReportDTOInter> Statments = new ArrayList<DisputeReportDTOInter>();
        CoolInsert test = new CoolInsert();
        Integer counter = 0;
        for (DisputesDTOInter d : dDTOArr) {
            counter++;
            DisputeReportDTOInter drDTO = DTOFactory.createDisputeReportDTO();
            drDTO.setCorrectivedate(d.getCorrectivedate());
            drDTO.setExportdate(d.getExportdate());
            drDTO.setExportuser(d.getExportuser());
            drDTO.setCorrectiveuser(d.getCorrectiveuser());
            drDTO.setAmount(d.getamount());
            drDTO.setCorrectiveuser(d.getCorrectiveuser());
            drDTO.setCorrectivedate(d.getCorrectivedate());
            drDTO.setExportuser(d.getExportuser());
            drDTO.setExportdate(d.getExportdate());
            drDTO.setAtmApplicationId(d.getatmapplicationid());
            drDTO.setAtmId(d.getatmid());
            drDTO.setCardNo(d.getcardno());
            drDTO.setCardNoSuffix(d.getcardnosuffix());
            drDTO.setColumn1(d.getcolumn1());
            drDTO.setColumn2(d.getcolumn2());
            drDTO.setColumn3(d.getcolumn3());
            drDTO.setColumn4(d.getcolumn4());
            drDTO.setColumn5(d.getcolumn5());
            drDTO.setComments(d.getcomments());
            drDTO.setCurrency(d.getcurrency());
            drDTO.setCurrencyId(d.getcurrencyid());
            drDTO.setCustomerAccountNumber(d.getcustomeraccountnumber());
            drDTO.setDisputeBy(d.getdisputeby());
            drDTO.setDisputeDate(d.getdisputedate());
            drDTO.setDisputeKey(d.getdisputekey());
            drDTO.setDisputeReason(d.getdisputereason());
            drDTO.setDisputeWithRoles(d.getdisputewithroles());
            drDTO.setMatchingType(d.getmatchingtype());
            drDTO.setNotesPresented(d.getnotespresented());
            drDTO.setRecordType(d.getrecordtype());
            drDTO.setResponseCode(d.getresponsecode());
            drDTO.setResponseCodeId(d.getresponsecodeid());
            drDTO.setResponseCodeMaster(d.getresponsecodemaster());
            drDTO.setSettledDate(d.getsettleddate());
            drDTO.setSettledFlag(d.getsettledflag());
            drDTO.setSettledUser(d.getsettleduser());
            drDTO.setSettlementDate(d.getsettlementdate());
            drDTO.setTransactionDate(d.gettransactiondate());
            drDTO.setTransactionSeqeunce(d.gettransactionseqeunce());
            drDTO.setTransactionSequenceOrderBy(d.gettransactionsequenceorderby());
            drDTO.setTransactionStatus(d.gettransactionstatus());
            drDTO.setTransactionStatusId(d.gettransactionstatusid());
            drDTO.setTransactionTime(d.gettransactiontime());
            drDTO.setTransactionType(d.gettransactiontype());
            drDTO.setTransactionTypeId(d.gettransactiontypeid());
            drDTO.setTransactionTypeMaster(d.gettransactiontypemaster());
            drDTO.setUserId(logedUser.getUserId());
            Statments.add(drDTO);

            if (drDTO.getDisputeKey() != 0) {
                DisputesDTOInter dDTO;

                drDTO = DTOFactory.createDisputeReportDTO();
                dDTO = (DisputesDTOInter) mdDAO.findAnotherSide(d);
                if (dDTO.getdisputekey() != 0) {
                    drDTO.setCorrectivedate(dDTO.getCorrectivedate());
                    drDTO.setExportdate(dDTO.getExportdate());
                    drDTO.setExportuser(dDTO.getExportuser());
                    drDTO.setCorrectiveuser(dDTO.getCorrectiveuser());
                    drDTO.setAmount(dDTO.getamount());
                    drDTO.setAtmApplicationId(dDTO.getatmapplicationid());
                    drDTO.setAtmId(dDTO.getatmid());
                    drDTO.setCardNo(dDTO.getcardno());
                    drDTO.setCardNoSuffix(dDTO.getcardnosuffix());
                    drDTO.setColumn1(dDTO.getcolumn1());
                    drDTO.setColumn2(dDTO.getcolumn2());
                    drDTO.setCorrectiveuser(dDTO.getCorrectiveuser());
                    drDTO.setCorrectivedate(dDTO.getCorrectivedate());
                    drDTO.setExportuser(dDTO.getExportuser());
                    drDTO.setExportdate(dDTO.getExportdate());
                    drDTO.setColumn3(dDTO.getcolumn3());
                    drDTO.setColumn4(dDTO.getcolumn4());
                    drDTO.setColumn5(dDTO.getcolumn5());
                    drDTO.setComments(dDTO.getcomments());
                    drDTO.setCurrency(dDTO.getcurrency());
                    drDTO.setCurrencyId(dDTO.getcurrencyid());
                    drDTO.setCustomerAccountNumber(dDTO.getcustomeraccountnumber());
                    drDTO.setDisputeBy(dDTO.getdisputeby());
                    drDTO.setDisputeDate(dDTO.getdisputedate());
                    drDTO.setDisputeKey(dDTO.getdisputekey());
                    drDTO.setDisputeReason(dDTO.getdisputereason());
                    drDTO.setDisputeWithRoles(dDTO.getdisputewithroles());
                    drDTO.setMatchingType(dDTO.getmatchingtype());
                    drDTO.setNotesPresented(dDTO.getnotespresented());
                    drDTO.setRecordType(dDTO.getrecordtype());
                    drDTO.setResponseCode(dDTO.getresponsecode());
                    drDTO.setResponseCodeId(dDTO.getresponsecodeid());
                    drDTO.setResponseCodeMaster(dDTO.getresponsecodemaster());
                    drDTO.setSettledDate(dDTO.getsettleddate());
                    drDTO.setSettledFlag(dDTO.getsettledflag());
                    drDTO.setSettledUser(dDTO.getsettleduser());
                    drDTO.setSettlementDate(dDTO.getsettlementdate());
                    drDTO.setTransactionDate(dDTO.gettransactiondate());
                    drDTO.setTransactionSeqeunce(dDTO.gettransactionseqeunce());
                    drDTO.setTransactionSequenceOrderBy(dDTO.gettransactionsequenceorderby());
                    drDTO.setTransactionStatus(dDTO.gettransactionstatus());
                    drDTO.setTransactionStatusId(dDTO.gettransactionstatusid());
                    drDTO.setTransactionTime(dDTO.gettransactiontime());
                    drDTO.setTransactionType(dDTO.gettransactiontype());
                    drDTO.setTransactionTypeId(dDTO.gettransactiontypeid());
                    drDTO.setTransactionTypeMaster(dDTO.gettransactiontypemaster());
                    drDTO.setUserId(logedUser.getUserId());

                    Statments.add(drDTO);
                }
            }
        }

        test.save(Statments);

        ResultSet rs = (ResultSet) drDAO.findByUserId(logedUser.getUserId(), fields, mtInt);
        // ReportingTool rt = new ReportingTool(Statments, (ArrayList) c, false,fields);

        ReportingTool rt = new ReportingTool(rs, (ArrayList) c, false);
        try {
            String path = rt.generate("Dispute Transactions", logedUser.getUserName(), searchParameter, false, cust, totalAmount, counter.toString());
            drDAO.delete(logedUser.getUserId());
            CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());

            return path;
        } catch (Throwable ex) {
            drDAO.delete(logedUser.getUserId());
            return "Fail";
        }

    }

    public Object findReport(String fields, String whereCluase, UsersDTOInter logedUser, List<ColumnDTOInter> c, List<DisputesDTOInter> dDTOArr, String cust, Integer mtInt, Integer listSize) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        DisputeReportDAOInter drDAO = DAOFactory.createDisputeReportDAO(null);
        List<DisputeReportDTOInter> Statments = new ArrayList<DisputeReportDTOInter>();
        CoolInsert test = new CoolInsert();
        Integer counter = 0;
        for (DisputesDTOInter d : dDTOArr) {
            counter++;

            DisputeReportDTOInter drDTO = DTOFactory.createDisputeReportDTO();
            drDTO.setCorrectivedate(d.getCorrectivedate());
            drDTO.setExportdate(d.getExportdate());
            drDTO.setExportuser(d.getExportuser());
            drDTO.setCorrectiveuser(d.getCorrectiveuser());
            drDTO.setAmount(d.getamount());
            drDTO.setAtmApplicationId(d.getatmapplicationid());
            drDTO.setAtmId(d.getatmid());
            drDTO.setCardNo(d.getcardno());
            drDTO.setCardNoSuffix(d.getcardnosuffix() );
            drDTO.setColumn1(d.getcolumn1());
            drDTO.setColumn2(d.getcolumn2());
            drDTO.setColumn3(d.getcolumn3());
            drDTO.setColumn4(d.getcolumn4());
            drDTO.setColumn5(d.getcolumn5());
            drDTO.setComments(d.getcomments());
            drDTO.setCurrency(d.getcurrency());
            drDTO.setCurrencyId(d.getcurrencyid());
            drDTO.setCustomerAccountNumber(d.getcustomeraccountnumber());
            drDTO.setDisputeBy(d.getdisputeby());
            drDTO.setDisputeDate(d.getdisputedate());
            drDTO.setDisputeKey(d.getdisputekey());
            drDTO.setDisputeReason(d.getdisputereason());
            drDTO.setDisputeWithRoles(d.getdisputewithroles());
            drDTO.setMatchingType(d.getmatchingtype());
            drDTO.setNotesPresented(d.getnotespresented());
            drDTO.setRecordType(d.getrecordtype());
            drDTO.setResponseCode(d.getresponsecode());
            drDTO.setResponseCodeId(d.getresponsecodeid());
            drDTO.setResponseCodeMaster(d.getresponsecodemaster());
            drDTO.setSettledDate(d.getsettleddate());
            drDTO.setSettledFlag(d.getsettledflag());
            drDTO.setSettledUser(d.getsettleduser());
            drDTO.setSettlementDate(d.getsettlementdate());
            drDTO.setTransactionDate(d.gettransactiondate());
            drDTO.setTransactionSeqeunce(d.gettransactionseqeunce());
            drDTO.setTransactionSequenceOrderBy(d.gettransactionsequenceorderby());
            drDTO.setTransactionStatus(d.gettransactionstatus());
            drDTO.setTransactionStatusId(d.gettransactionstatusid());
            drDTO.setTransactionTime(d.gettransactiontime());
            drDTO.setTransactionType(d.gettransactiontype());
            drDTO.setTransactionTypeId(d.gettransactiontypeid());
            drDTO.setTransactionTypeMaster(d.gettransactiontypemaster());
            drDTO.setUserId(logedUser.getUserId());
            Statments.add(drDTO);

            if (drDTO.getDisputeKey() != 0) {
                DisputesDTOInter dDTO;

                drDTO = DTOFactory.createDisputeReportDTO();
                dDTO = (DisputesDTOInter) mdDAO.findAnotherSide(d);
                if (dDTO.getdisputekey() != 0) {
                    drDTO.setCorrectivedate(dDTO.getCorrectivedate());
                    drDTO.setExportdate(dDTO.getExportdate());
                    drDTO.setExportuser(dDTO.getExportuser());
                    drDTO.setCorrectiveuser(dDTO.getCorrectiveuser());
                    drDTO.setAmount(dDTO.getamount());
                    drDTO.setAtmApplicationId(dDTO.getatmapplicationid());
                    drDTO.setAtmId(dDTO.getatmid());
                    drDTO.setCardNo(dDTO.getcardno());
                    drDTO.setCardNoSuffix(dDTO.getcardnosuffix());
                    drDTO.setColumn1(dDTO.getcolumn1());
                    drDTO.setColumn2(dDTO.getcolumn2());
                    drDTO.setColumn3(dDTO.getcolumn3());
                    drDTO.setColumn4(dDTO.getcolumn4());
                    drDTO.setColumn5(dDTO.getcolumn5());
                    drDTO.setComments(dDTO.getcomments());
                    drDTO.setCurrency(dDTO.getcurrency());
                    drDTO.setCurrencyId(dDTO.getcurrencyid());
                    drDTO.setCustomerAccountNumber(dDTO.getcustomeraccountnumber());
                    drDTO.setDisputeBy(dDTO.getdisputeby());
                    drDTO.setDisputeDate(dDTO.getdisputedate());
                    drDTO.setDisputeKey(dDTO.getdisputekey());
                    drDTO.setDisputeReason(dDTO.getdisputereason());
                    drDTO.setDisputeWithRoles(dDTO.getdisputewithroles());
                    drDTO.setMatchingType(dDTO.getmatchingtype());
                    drDTO.setNotesPresented(dDTO.getnotespresented());
                    drDTO.setRecordType(dDTO.getrecordtype());
                    drDTO.setResponseCode(dDTO.getresponsecode());
                    drDTO.setResponseCodeId(dDTO.getresponsecodeid());
                    drDTO.setResponseCodeMaster(dDTO.getresponsecodemaster());
                    drDTO.setSettledDate(dDTO.getsettleddate());
                    drDTO.setSettledFlag(dDTO.getsettledflag());
                    drDTO.setSettledUser(dDTO.getsettleduser());
                    drDTO.setSettlementDate(dDTO.getsettlementdate());
                    drDTO.setTransactionDate(dDTO.gettransactiondate());
                    drDTO.setTransactionSeqeunce(dDTO.gettransactionseqeunce());
                    drDTO.setTransactionSequenceOrderBy(dDTO.gettransactionsequenceorderby());
                    drDTO.setTransactionStatus(dDTO.gettransactionstatus());
                    drDTO.setTransactionStatusId(dDTO.gettransactionstatusid());
                    drDTO.setTransactionTime(dDTO.gettransactiontime());
                    drDTO.setTransactionType(dDTO.gettransactiontype());
                    drDTO.setTransactionTypeId(dDTO.gettransactiontypeid());
                    drDTO.setTransactionTypeMaster(dDTO.gettransactiontypemaster());
                    drDTO.setUserId(logedUser.getUserId());

                    Statments.add(drDTO);
                }
            }
        }

        test.save(Statments);

        ResultSet rs = (ResultSet) drDAO.findByUserId(logedUser.getUserId(), fields, mtInt);
        // ReportingTool rt = new ReportingTool(Statments, (ArrayList) c, false,fields);
        ReportingTool rt = new ReportingTool(rs, (ArrayList) c, false);
        try {

            String path = rt.generate("Dispute Transactions", logedUser.getUserName(), searchParameter, false, cust, totalAmount, counter.toString());
            drDAO.delete(logedUser.getUserId());
            CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());

            return path;
        } catch (Throwable ex) {
            drDAO.delete(logedUser.getUserId());
            return "Fail";
        }

    }

    public Object searchMatchedData(Date transDateF, String opt1, Date transDateT, Date settleDateF, String opt2, Date settleDateT,
            Date loadDateF, String opt3, Date loadDateT, Integer amountF, String opt4, Integer amountT, Integer transSeqOrderByF,
            String opt5, Integer transSeqOrderByT, String cardNo, String opt6, String accountNo, String opt7, String notesPre,
            int transStatusCode, String transactionStatus, int transTypeCode, String transType, int responseCode,
            String response, int currencyCode, int atmId,
            String atmCode, int atmGroup, boolean blackList, int settled, int matchingTypeList, int masterRecord,
            int settledBy, int disputedBy, int missingSide, int reverseType,
            String sortBy, UsersDTOInter logedUser, String field, int CorrectiveExport, Integer ResponseVariable, Integer ResponseVariable2, String col1, String col2, String col3, String col4, String col5, String colop1, String colop2, String colop3, String colop4, String colop5, Boolean collected) throws Throwable {

        //ResourceBundle.clearCache(DisputesBO.class.getClassLoader());
        String whereClaus = "";
        searchParameter = "";
        int transactiondatebetween = 0;
        int transactiondat = 0;
        int transactiontypeidin = 0;
        int matchingtype = 0;
        int responsecodeid = 0;
        int atmidingroup = 0;
        int atmid = 0;
        int recordtype = 0;
        int atmvalid = 0;
        int atmidinassign = 0;
        int atmapplicationidlike = 0;
        int settledflag = 0;
        int other = 0;
        String index = "";
        if (matchingTypeList == 1) {
            searchParameter += "Matching Type JR-SW -";
        } else if (matchingTypeList == 2) {
            searchParameter += "Matching Type SW-GL -";
        }

        if (masterRecord == 1) {
            searchParameter += "Master Record JR -";
        } else if (masterRecord == 2) {
            searchParameter += "Master Record SW -";
        } else if (masterRecord == 3) {
            searchParameter += "Master Record GL -";
        }

        // <editor-fold defaultstate="collapsed" desc="Transaction_date where clause">
        if (opt1.equals("Between")) {
            if (transDateF != null && transDateT != null) {
                whereClaus += " and transaction_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss') "
                        + " and transaction_date <= to_date('$col2','dd.mm.yyyy hh24:mi:ss') ";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                whereClaus = whereClaus.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(transDateT));
                transactiondatebetween = 1;
                transactiondat = 1;
                searchParameter += "Transaction Date between $F and $T -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter = searchParameter.replace("$T", DateFormatter.changeDateAndTimeFormat(transDateT));
            }
        } else if (opt1.equals("<>")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date <> to_date('$col1','dd.mm.yyyy hh24:mi:ss') ";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date <> $F -";
                transactiondat = 1;
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals("<")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date < to_date('$col1','dd.mm.yyyy hh24:mi:ss') ";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date < $F -";
                transactiondat = 1;
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals("<=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date <= to_date('$col1','dd.mm.yyyy hh24:mi:ss') ";
                transactiondat = 1;
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date <= $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals(">")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date > to_date('$col1','dd.mm.yyyy hh24:mi:ss') ";
                transactiondat = 1;
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date > $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals(">=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss') ";
                transactiondat = 1;
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date >= $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals("=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date = to_date('$col1','dd.mm.yyyy hh24:mi:ss') ";
                transactiondat = 1;
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date = $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Settlement_date where clause">
        if (opt2.equals("Between")) {
            if (settleDateF != null && settleDateT != null) {
                whereClaus += " and settlement_date between to_date('$col1','dd.mm.yyyy hh24:mi:ss') and "
                        + " to_date('$col2','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                whereClaus = whereClaus.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(settleDateT));
                searchParameter += "Settlement Date Between $F and $T -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter = searchParameter.replace("$T", DateFormatter.changeDateAndTimeFormat(settleDateT));
                other = 1;
            }
        } else if (opt2.equals(">")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date > to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date > $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                other = 1;
            }
        } else if (opt2.equals(">=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss') ";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date >= $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                other = 1;
            }
        } else if (opt2.equals("<")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date < to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date < $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                other = 1;
            }
        } else if (opt2.equals("<=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date <= to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date <= $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                other = 1;
            }
        } else if (opt2.equals("<>")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date <> to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date <> $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                other = 1;
            }
        } else if (opt2.equals("=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date = to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date = $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                other = 1;
            }
        }// </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Amount where clause">
        if (opt4.equals("Between")) {
            if (amountF != null && amountT != null&& amountF !=0&& amountT !=0) {
                whereClaus += " and amount between nvl($col1,0) and nvl($col2,9999999999999999)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                whereClaus = whereClaus.replace("$col2", "" + amountT);
                searchParameter += "Amount Between $F and $T -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                searchParameter = searchParameter.replace("$T", "" + amountT);
                other = 1;
            }
        } else if (opt4.equals(">")) {
            if (amountF != null&& amountF !=0) {
                whereClaus += " and amount > nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount > $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                other = 1;
            }
        } else if (opt4.equals(">=")) {
            if (amountF != null&& amountF !=0) {
                whereClaus += " and amount >= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount >= $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                other = 1;
            }
        } else if (opt4.equals("<")) {
            if (amountF != null&& amountF !=0) {
                whereClaus += " and amount < nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount < $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                other = 1;
            }
        } else if (opt4.equals("<=")) {
            if (amountF != null&& amountF !=0) {
                whereClaus += " and amount <= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount <= $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                other = 1;
            }
        } else if (opt4.equals("<>")) {
            if (amountF != null&& amountF !=0) {
                whereClaus += " and amount <> nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount <> $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                other = 1;
            }
        } else if (opt4.equals("=")) {
            if (amountF != null && amountF !=0) {
                whereClaus += " and amount = nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount = $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                other = 1;
            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Transaction_sequence where clause">
        if (opt5.equals("Between")) {
            if (transSeqOrderByF != null && transSeqOrderByT != null&& transSeqOrderByF !=0&& transSeqOrderByT !=0) {
                whereClaus += " and transaction_sequence_order_by between nvl($col1,0) and nvl($col2,9999999999999999)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                whereClaus = whereClaus.replace("$col2", "" + transSeqOrderByT);
                searchParameter += "Sequence Between $F and $T -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                searchParameter = searchParameter.replace("$T", "" + transSeqOrderByT);
                other = 1;
            }
        } else if (opt5.equals(">")) {
            if (transSeqOrderByF != null&& transSeqOrderByF !=0) {
                whereClaus += " and transaction_sequence_order_by > nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence > $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                other = 1;
            }
        } else if (opt5.equals(">=")) {
            if (transSeqOrderByF != null&& transSeqOrderByF !=0) {
                whereClaus += " and transaction_sequence_order_by >= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence >= $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                other = 1;
            }
        } else if (opt5.equals("<")) {
            if (transSeqOrderByF != null&& transSeqOrderByF !=0) {
                whereClaus += " and transaction_sequence_order_by < nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence < $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                other = 1;
            }
        } else if (opt5.equals("<=")) {
            if (transSeqOrderByF != null&& transSeqOrderByF !=0) {
                whereClaus += " and transaction_sequence_order_by <= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence <= $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                other = 1;
            }
        } else if (opt5.equals("<>")) {
            if (transSeqOrderByF != null&& transSeqOrderByF !=0) {
                whereClaus += " and transaction_sequence_order_by <> nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence <> $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                other = 1;
            }
        } else if (opt5.equals("=")) {
            if (transSeqOrderByF != null && transSeqOrderByF !=0 ) {
                whereClaus += " and transaction_sequence_order_by = nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence = $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                other = 1;
            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Card_no where clause">
        if (opt6.equals("Like")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and " + super.getcardcolumn() + " like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo);
                searchParameter += "Card No. like $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo);
                other = 1;
            }
        } else if (opt6.equals("Not Like")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and " + super.getcardcolumn() + " not like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo);
                searchParameter += "Card No. not like $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo);
                other = 1;
            }
        } else if (opt6.equals("is null")) {
            whereClaus += " and " + super.getcardcolumn() + " is null ";
            whereClaus = whereClaus.replace("$col1", "" + cardNo);
            searchParameter += "Card No.  is null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
            other = 1;
        } else if (opt6.equals("is not null")) {
            whereClaus += " and " + super.getcardcolumn() + " is not null ";
            whereClaus = whereClaus.replace("$col1", "" + cardNo);
            searchParameter += "Card No. not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
            other = 1;
        } else if (opt6.equals("<>")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and " + super.getcardcolumn() + " <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo);
                searchParameter += "Card No. <> $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo);
                other = 1;
            }
        } else if (opt6.equals("=")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and " + super.getcardcolumn() + " = '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo);
                searchParameter += "Card No. equal $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo);
                other = 1;
            }
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Account_No where clause">
        if (opt7.equals("=")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER = '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo);
                searchParameter += "Account No. equal $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo);
                other = 1;
            }
        } else if (opt7.equals("<>")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo);
                searchParameter += "Account No. <> $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo);
                other = 1;
            }

        } else if (opt7.equals("is null")) {
            whereClaus += " and CUSTOMER_ACCOUNT_NUMBER is null";
            whereClaus = whereClaus.replace("$col1", "" + accountNo);
            searchParameter += "Account No. is null";
            searchParameter = searchParameter.replace("$F", "" + accountNo);
            other = 1;

        } else if (opt7.equals("is not null")) {
            whereClaus += " and CUSTOMER_ACCOUNT_NUMBER is not null";
            whereClaus = whereClaus.replace("$col1", "" + accountNo);
            searchParameter += "Account No. is not null -";
            searchParameter = searchParameter.replace("$F", "" + accountNo);
            other = 1;
        } else if (opt7.equals("Like")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER Like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo);
                searchParameter += "Account No. Like $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo);
                other = 1;
            }

        } else if (opt7.equals("Not Like")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER not Like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo);
                searchParameter += "Account No. not Like $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo);
                other = 1;
            }

        }
        // </editor-fold>
        if (notesPre != null && !notesPre.equals("")) {
            whereClaus += " and lower(notes_presented) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + notesPre);
            searchParameter += "Notes Presnted like $F -";
            searchParameter = searchParameter.replace("$F", "" + notesPre);
            other = 1;
        }
        if (collected) {
            whereClaus += " and CARD_NO IN (select t.CARD_NO from CARD_TAKEN t)";
            searchParameter += "Colleted Cards";
            other = 1;
        }
        if (transStatusCode != 0) {
            if (transStatusCode != -1) {
                whereClaus += " and transaction_status_id = $col1";
                whereClaus = whereClaus.replace("$col1", "" + transStatusCode);
                searchParameter += "Status: $F -";
                TransactionStatusDAOInter tsDAO = DAOFactory.createTransactionStatusDAO(null);
                TransactionStatusDTOInter tsDTO = (TransactionStatusDTOInter) tsDAO.find(transStatusCode);
                searchParameter = searchParameter.replace("$F", "" + tsDTO.getName());
                other = 1;
            } else {
                whereClaus += " and transaction_status_id in (select id from transaction_status) ";
                whereClaus = whereClaus.replace("$col1", "" + transStatusCode);
                searchParameter += "Status: $F -";
                searchParameter = searchParameter.replace("$F", " Has Error");
                other = 1;
            }
        }
        if (transactionStatus != null && !transactionStatus.equals("")) {
            whereClaus += " and lower(transaction_status) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + transactionStatus);
            searchParameter += "Status: $F -";
            searchParameter = searchParameter.replace("$F", "" + transactionStatus);
            other = 1;
        }
        if (transTypeCode != 0) {
            whereClaus += " and transaction_type_id = $col1";
            whereClaus = whereClaus.replace("$col1", "" + transTypeCode);
            transactiontypeidin = 1;
            searchParameter += "Trans. Type: $F -";
            TransactionTypeDAOInter tsDAO = DAOFactory.createTransactionTypeDAO(null);
            TransactionTypeDTOInter tsDTO = (TransactionTypeDTOInter) tsDAO.find(transTypeCode);
            searchParameter = searchParameter.replace("$F", "" + tsDTO.getName());
        }
        if (transType != null && !transType.equals("")) {
            whereClaus += " and lower(transaction_type) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + transType);
            searchParameter += "Trans. Type: $F -";
            searchParameter = searchParameter.replace("$F", "" + transType);
            other = 1;
        }
        if (responseCode != 0) {
            String Operator = "";
            if (ResponseVariable == 1) {
                Operator = " = ";
                responsecodeid = 1;
            }
            if (ResponseVariable == 2) {
                Operator = " <> ";
            }
            whereClaus += " and response_code_id " + Operator + " $col1";
            whereClaus = whereClaus.replace("$col1", "" + responseCode);
            searchParameter += "Response: $F -";
            TransactionResponseCodeDAOInter tsDAO = DAOFactory.createTransactionResponseCodeDAO(null);
            TransactionResponseCodeDTOInter tsDTO = (TransactionResponseCodeDTOInter) tsDAO.find(responseCode);
            searchParameter = searchParameter.replace("$F", "" + tsDTO.getName());
        }
        if (response != null && !response.equals("")) {
            String Operator = "";
            if (ResponseVariable2 == 1) {
                Operator = " = ";
            }
            if (ResponseVariable2 == 2) {
                Operator = " <> ";
            }

            whereClaus += " and lower(response_code) " + Operator + " lower('$col1')";
            whereClaus = whereClaus.replace("$col1", "" + response);
            searchParameter += "Response: $F -";
            searchParameter = searchParameter.replace("$F", "" + response);
            other = 1;
        }
        if (currencyCode != 0) {
            whereClaus += " and currency_id = $col1";
            whereClaus = whereClaus.replace("$col1", "" + currencyCode);
            searchParameter += "Currency: $F -";
            CurrencyMasterDAOInter tsDAO = DAOFactory.createCurrencyMasterDAO(null);
            CurrencyMasterDTOInter tsDTO = (CurrencyMasterDTOInter) tsDAO.find(currencyCode);
            searchParameter = searchParameter.replace("$F", "" + tsDTO.getSymbol());
            other = 1;
        }
        if (matchingTypeList != 0) {
            whereClaus += "  and record_type = $col2 and matching_type = $col1";
            whereClaus = whereClaus.replace("$col1", "" + matchingTypeList);
            whereClaus = whereClaus.replace("$col2", "" + masterRecord);
            matchingtype = 1;
            recordtype = 1;

        }
        if (atmId != 0) {
            whereClaus += " and ATM_ID Like '%$col1%'";
            atmid = 1;
            whereClaus = whereClaus.replace("$col1", "" + atmId);
            searchParameter += "ATM ID: $F -";
            AtmMachineDAOInter x = DAOFactory.createAtmMachineDAO(null);
            AtmMachineDTOInter x1 = (AtmMachineDTOInter) x.find(atmId);
            searchParameter = searchParameter.replace("$F", "" + x1.getApplicationId());
        }
        if (atmCode != null && !atmCode.equals("")) {
            whereClaus += " and lower(ATM_APPLICATION_ID) like lower('%$col1%')";

            atmapplicationidlike = 1;
            whereClaus = whereClaus.replace("$col1", "" + atmCode);
            searchParameter += "ATM name: $F -";
            searchParameter = searchParameter.replace("$F", "" + atmCode);
        }
        if (atmGroup != 0) {
            whereClaus += " and ATM_ID IN (select id from atm_machine where atm_group in (select id from atm_group where parent_id in (select id from atm_group where (parent_id = $col1 or id = $col1)) or id = $col1) and Exists (select 1 from user_atm s where s.user_id = $user and s.atm_id = disputes.ATM_ID))";
            atmid = 1;
            whereClaus = whereClaus.replace("$col1", "" + atmGroup);
            atmidingroup = 1;
            AtmGroupDAOInter agDAO = DAOFactory.createAtmGroupDAO(null);
            AtmGroupDTOInter agDTO = (AtmGroupDTOInter) agDAO.find(atmGroup);
            searchParameter += "ATM Group: $F -";
            searchParameter = searchParameter.replace("$F", "" + agDTO.getName());
        }
        if (blackList) {
            whereClaus += " and card_no in (select card_no from black_listed_card)";
            searchParameter += "Black Listed Card -";
            other = 1;
        }
        if (settled != 3) {
            whereClaus += " and NVL (settled_flag, 2) = $col1";
            settledflag = 1;
            whereClaus = whereClaus.replace("$col1", "" + settled);
        }

        if (settledBy != 0) {
            whereClaus += " and settled_user = $col1";
            whereClaus = whereClaus.replace("$col1", "" + settledBy);
            UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
            UsersDTOInter uDTO = (UsersDTOInter) uDAO.find(settledBy);
            searchParameter += "Settled By: $F -";
            searchParameter = searchParameter.replace("$F", uDTO.getUserAName());
other = 1;
        }
        if (disputedBy != 0) {
            whereClaus += " and match_by = $col1";
            whereClaus = whereClaus.replace("$col1", "" + disputedBy);
            UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
            UsersDTOInter uDTO = (UsersDTOInter) uDAO.find(disputedBy);
            searchParameter += "Matched By: $F -";
            searchParameter = searchParameter.replace("$F", uDTO.getUserAName());
            other = 1;
        }
        if (CorrectiveExport != 0) {
            if (CorrectiveExport == 4) {
                whereClaus += " and CORRECTIVE_ENTRY_FLAG is null";
                other = 1;
            } else {

                whereClaus += " and CORRECTIVE_ENTRY_FLAG  = $col1";
                other = 1;
                whereClaus = whereClaus.replace("$col1", "" + CorrectiveExport);
                searchParameter += " and  corrective state: $F";
                String state = "";
                if (CorrectiveExport == 1) {
                    state = "corrective";
                } else if (CorrectiveExport == 2) {
                    state = "Export";
                } else if (CorrectiveExport == 3) {
                    state = "Completed";
                } else if (CorrectiveExport == 4) {
                    state = "un processed";
                }
                searchParameter = searchParameter.replace("$F", state);
            }
        }
        if (missingSide == 1) {
            whereClaus += " and not exists (select 1 from disputes d where d.dispute_key = disputes.dispute_key)";
            other = 1;
        } else if (missingSide == 2) {
            whereClaus += " and exists (select 1 from disputes d where d.dispute_key = disputes.dispute_key)";
            other = 1;
        }

        if (reverseType == 2) {
            whereClaus += " and not exists (select 1 from disputes x where 1=1 " + whereClaus + " and x.record_type=disputes.record_type and x.MATCHING_TYPE = disputes.matching_type and x.TRANSACTION_DATE= disputes.transaction_date and x.ATM_ID= disputes.ATM_ID and x.CURRENCY_ID= disputes.currency_id and x.amount <> disputes.amount and x.transaction_sequence = disputes.transaction_sequence)";
            other = 1;
        } else if (reverseType == 1) {
            whereClaus += " and exists (select 1 from disputes x where 1=1 " + whereClaus + " and x.record_type=disputes.record_type and x.MATCHING_TYPE = disputes.matching_type and x.TRANSACTION_DATE= disputes.transaction_date and x.ATM_ID= disputes.ATM_ID and x.CURRENCY_ID= disputes.currency_id and x.amount <> disputes.amount and x.transaction_sequence = disputes.transaction_sequence)";
            other = 1;
        }
        whereClaus += " and atm_valid(atm_id) = 1  ";
        atmvalid = 1;
        if (atmidingroup == 0) {
            whereClaus += " and Exists (select 1 from user_atm s where s.user_id = $user and s.atm_id = disputes.ATM_ID)";
        }
        atmidinassign = 1;
        whereClaus = whereClaus.replace("$user", "" + logedUser.getUserId());

        // <editor-fold defaultstate="collapsed" desc="Card_no where clause">
        if (colop1.equals("Like")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                other = 1;
                searchParameter += "Column 1 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
            }
        } else if (colop1.equals("Not Like")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 not like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                other = 1;
                searchParameter += "Column 1 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
            }
        } else if (colop1.equals("is null")) {
            whereClaus += " and COLUMN1 is null ";
            whereClaus = whereClaus.replace("$col1", "" + col1);
            other = 1;
            searchParameter += "Column 1 is null -";
            searchParameter = searchParameter.replace("$F", "" + col1);
        } else if (colop1.equals("is not null")) {
            whereClaus += " and COLUMN1 is not null ";
            whereClaus = whereClaus.replace("$col1", "" + col1);
            other = 1;
            searchParameter += "Column 1 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop1.equals("<>")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                other = 1;
                searchParameter += "Column 1 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
            }
        } else if (colop1.equals("=")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 = '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                searchParameter += "Column 1 equal $F -";
                other = 1;
                searchParameter = searchParameter.replace("$F", "" + col1);
            }
        }
        if (colop2.equals("Like")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 like '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 like $F -";
                other = 1;
                searchParameter = searchParameter.replace("$F", "" + col2);
            }
        } else if (colop2.equals("Not Like")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 not like '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 not like $F -";
                other = 1;
                searchParameter = searchParameter.replace("$F", "" + col2);
            }
        } else if (colop2.equals("is null")) {
            whereClaus += " and COLUMN2 is null ";
            whereClaus = whereClaus.replace("$col2", "" + col2);
            searchParameter += "Column 2 is null -";
            other = 1;
            searchParameter = searchParameter.replace("$F", "" + col2);
        } else if (colop2.equals("is not null")) {
            whereClaus += " and COLUMN2 is not null ";
            whereClaus = whereClaus.replace("$col2", "" + col2);
            searchParameter += "Column 2 not null -";
            other = 1;
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop2.equals("<>")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 <> '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 <> $F -";
                other = 1;
                searchParameter = searchParameter.replace("$F", "" + col2);
            }
        } else if (colop2.equals("=")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 = '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                other = 1;
                searchParameter += "Column 2 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col2);
            }
        }
        if (colop3.equals("Like")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 like '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 like $F -";
                other = 1;
                searchParameter = searchParameter.replace("$F", "" + col3);
            }
        } else if (colop3.equals("Not Like")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 not like '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 not like $F -";
                other = 1;
                searchParameter = searchParameter.replace("$F", "" + col3);
            }
        } else if (colop3.equals("is null")) {
            whereClaus += " and COLUMN3 is null ";
            whereClaus = whereClaus.replace("$col3", "" + col3);
            other = 1;
            searchParameter += "Column 3 is null -";
            searchParameter = searchParameter.replace("$F", "" + col3);
        } else if (colop3.equals("is not null")) {
            whereClaus += " and COLUMN3 is not null ";
            whereClaus = whereClaus.replace("$col3", "" + col3);
            other = 1;
            searchParameter += "Column 3 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop3.equals("<>")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 <> '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                other = 1;
                searchParameter += "Column 3 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
            }
        } else if (colop3.equals("=")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 = '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                other = 1;
                searchParameter += "Column 3 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
            }
        }
        if (colop4.equals("Like")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 like '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                other = 1;
                searchParameter += "Column 4 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
            }
        } else if (colop4.equals("Not Like")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 not like '$col4'";
                other = 1;
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
            }
        } else if (colop4.equals("is null")) {
            whereClaus += " and COLUMN4 is null ";
            whereClaus = whereClaus.replace("$col4", "" + col4);
            searchParameter += "Column 4 is null -";
            other = 1;
            searchParameter = searchParameter.replace("$F", "" + col4);
        } else if (colop4.equals("is not null")) {
            whereClaus += " and COLUMN4 is not null ";
            whereClaus = whereClaus.replace("$col4", "" + col4);
            searchParameter += "Column 4 not null -";
            other = 1;
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop4.equals("<>")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 <> '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 <> $F -";
                other = 1;
                searchParameter = searchParameter.replace("$F", "" + col4);
            }
        } else if (colop4.equals("=")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 = '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 equal $F -";
                other = 1;
                searchParameter = searchParameter.replace("$F", "" + col4);
            }
        }
        if (colop5.equals("Like")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 like '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 like $F -";
                other = 1;
                searchParameter = searchParameter.replace("$F", "" + col5);
            }
        } else if (colop5.equals("Not Like")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 not like '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 not like $F -";
                other = 1;
                searchParameter = searchParameter.replace("$F", "" + col5);
            }
        } else if (colop5.equals("is null")) {
            whereClaus += " and COLUMN5 is null ";
            whereClaus = whereClaus.replace("$col5", "" + col5);
            searchParameter += "Column 5 is null -";
            other = 1;
            searchParameter = searchParameter.replace("$F", "" + col5);
        } else if (colop5.equals("is not null")) {
            whereClaus += " and COLUMN5 is not null ";
            whereClaus = whereClaus.replace("$col5", "" + col5);
            searchParameter += "Column 5 not null -";
            other = 1;
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop5.equals("<>")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 <> '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 <> $F -";
                other = 1;
                searchParameter = searchParameter.replace("$F", "" + col5);
            }
        } else if (colop5.equals("=")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 = '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 equal $F -";
                other = 1;
                searchParameter = searchParameter.replace("$F", "" + col5);
            }
        }
        // </editor-fold>
        if (sortBy != null && !sortBy.equals("")) {
            whereClaus += " order by ATM_APPLICATION_ID,transaction_date, transaction_sequence_order_by";
            whereClaus = whereClaus.replace("$col1", "" + sortBy);
        } else {
            whereClaus += " order by  ATM_APPLICATION_ID,transaction_date, transaction_sequence_order_by";
        }

        this.whereCluase = whereClaus;
        this.whereCluase = this.whereCluase.replaceAll("and record_type = [0-9]", "");
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        Object[] result = new Object[2];
        
        
        result = mdDAO.customSearch(whereClaus, field, index);
        List<DisputesDTOInter> mdDTOL = (List<DisputesDTOInter>) result[0];
        //for (DisputesDTOInter d : mdDTOL) {
        //  String s = d.getDisputeReason();
        // DisputeRulesDAOInter drDAO = DAOFactory.createDisputeRulesDAO(null);
        // List<DisputeRulesDTOInter> drDTOL = new ArrayList<DisputeRulesDTOInter>();
        // drDTOL = (List<DisputeRulesDTOInter>) drDAO.findDisputesReasons(d);
        // if (drDTOL.size() != 0) {
        //  for (DisputeRulesDTOInter dd : drDTOL) {
        //      s += dd.getName() + " - ";
        //  }
        //  }
        //  d.setReasonToDisp(s);

        // }
        System.gc();

        totalAmount = (BigDecimal) result[1];

        return mdDTOL;
    }

    public Object findTotalAmount(Date transDateF, String opt1, Date transDateT, Date settleDateF, String opt2, Date settleDateT,
            Date loadDateF, String opt3, Date loadDateT, Integer amountF, String opt4, Integer amountT, Integer transSeqOrderByF,
            String opt5, Integer transSeqOrderByT, String cardNo, String opt6, String accountNo, String opt7, String notesPre,
            int transStatusCode, String transactionStatus, int transTypeCode, String transType, int responseCode,
            String response, int currencyCode, int atmId,
            String atmCode, int atmGroup, boolean blackList, int settled, int matchingTypeList, int masterRecord,
            int settledBy, int disputedBy, int missingSide, int reverseType,
            String sortBy, UsersDTOInter logedUser, String field, int CorrectiveExport, Integer ResponseVariable, Integer ResponseVariable2, String col1, String col2, String col3, String col4, String col5, String colop1, String colop2, String colop3, String colop4, String colop5, Boolean collected) throws Throwable {

        String whereClaus = "";

        // <editor-fold defaultstate="collapsed" desc="Transaction_date where clause">
        if (opt1.equals("Between")) {
            if (transDateF != null && transDateT != null) {
                whereClaus += " and transaction_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss') "
                        + " and transaction_date <= to_date('$col2','dd.mm.yyyy hh24:mi:ss') ";

                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                whereClaus = whereClaus.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(transDateT));

            }
        } else if (opt1.equals("<>")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date <> to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));

            }
        } else if (opt1.equals("<")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date < to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));

            }
        } else if (opt1.equals("<=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date <= to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));

            }
        } else if (opt1.equals(">")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date > to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));

            }
        } else if (opt1.equals(">=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));

            }
        } else if (opt1.equals("=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date = to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));

            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Settlement_date where clause">
        if (opt2.equals("Between")) {
            if (settleDateF != null && settleDateT != null) {
                whereClaus += " and settlement_date between to_date('$col1','dd.mm.yyyy hh24:mi:ss')"
                        + " and "
                        + "to_date('$col2','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                whereClaus = whereClaus.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(settleDateT));
            }
        } else if (opt2.equals(">")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date > to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
            }
        } else if (opt2.equals(">=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss')"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
            }
        } else if (opt2.equals("<")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date < to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
            }
        } else if (opt2.equals("<=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date <= to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
            }
        } else if (opt2.equals("<>")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date <> to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
            }
        } else if (opt2.equals("=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date = to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
            }
        }// </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Amount where clause">
        if (opt4.equals("Between")) {
            if (amountF != null && amountT != null&& amountF != 0&& amountT != 0) {
                whereClaus += " and amount between nvl($col1,0) and nvl($col2,9999999999999999)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                whereClaus = whereClaus.replace("$col2", "" + amountT);
            }
        } else if (opt4.equals(">")) {
            if (amountF != null&& amountF != 0) {
                whereClaus += " and amount > nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
            }
        } else if (opt4.equals(">=")) {
            if (amountF != null&& amountF != 0) {
                whereClaus += " and amount >= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
            }
        } else if (opt4.equals("<")) {
            if (amountF != null&& amountF != 0) {
                whereClaus += " and amount < nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
            }
        } else if (opt4.equals("<=")) {
            if (amountF != null&& amountF != 0) {
                whereClaus += " and amount <= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
            }
        } else if (opt4.equals("<>") ) {
            if (amountF != null&& amountF != 0) {
                whereClaus += " and amount <> nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
            }
        } else if (opt4.equals("=")) {
            if (amountF != null && amountF != 0) {
                whereClaus += " and amount = nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Transaction_sequence where clause">
        if (opt5.equals("Between")) {
            if (transSeqOrderByF != null && transSeqOrderByT != null) {
                whereClaus += " and transaction_sequence_order_by between nvl($col1,0) and nvl($col2,9999999999999999)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                whereClaus = whereClaus.replace("$col2", "" + transSeqOrderByT);
            }
        } else if (opt5.equals(">")) {
            if (transSeqOrderByF != null) {
                whereClaus += " and transaction_sequence_order_by > nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
            }
        } else if (opt5.equals(">=")) {
            if (transSeqOrderByF != null) {
                whereClaus += " and transaction_sequence_order_by >= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
            }
        } else if (opt5.equals("<")) {
            if (transSeqOrderByF != null) {
                whereClaus += " and transaction_sequence_order_by < nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
            }
        } else if (opt5.equals("<=")) {
            if (transSeqOrderByF != null) {
                whereClaus += " and transaction_sequence_order_by <= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
            }
        } else if (opt5.equals("<>")) {
            if (transSeqOrderByF != null) {
                whereClaus += " and transaction_sequence_order_by <> nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
            }
        } else if (opt5.equals("=")) {
            if (transSeqOrderByF != null) {
                whereClaus += " and transaction_sequence_order_by = nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Card_no where clause">
        if (opt6.equals("=")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and " + super.getcardcolumn() + " like '%$col1%'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo);
            }
        } else if (opt6.equals("<>")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and " + super.getcardcolumn() + " <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo);
            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Account_No where clause">
        if (opt7.equals("=")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER like '%$col1%'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo);
            }
        } else if (opt7.equals("<>")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo);
            }
        }// </editor-fold>
        if (notesPre != null && !notesPre.equals("")) {
            whereClaus += " and lower(notes_presented) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + notesPre);
        }

        if (transStatusCode != 0) {
            if (transStatusCode != -1) {
                whereClaus += " and transaction_status_id = $col1";
                whereClaus = whereClaus.replace("$col1", "" + transStatusCode);
            } else {
                whereClaus += " and transaction_status_id in (select id from transaction_status) ";
                whereClaus = whereClaus.replace("$col1", "" + transStatusCode);
            }
        }
        if (collected) {
            whereClaus += " and CARD_NO IN (select t.CARD_NO from CARD_TAKEN t)";
            searchParameter += "Colleted Cards";
        }
        if (colop1.equals("Like")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                searchParameter += "Column 1 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
            }
        } else if (colop1.equals("Not Like")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 not like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                searchParameter += "Column 1 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
            }
        } else if (colop1.equals("is null")) {
            whereClaus += " and COLUMN1 is null ";
            whereClaus = whereClaus.replace("$col1", "" + col1);
            searchParameter += "Column 1 is null -";
            searchParameter = searchParameter.replace("$F", "" + col1);
        } else if (colop1.equals("is not null")) {
            whereClaus += " and COLUMN1 is not null ";
            whereClaus = whereClaus.replace("$col1", "" + col1);
            searchParameter += "Column 1 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop1.equals("<>")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                searchParameter += "Column 1 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
            }
        } else if (colop1.equals("=")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 = '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                searchParameter += "Column 1 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
            }
        }
        if (colop2.equals("Like")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 like '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col2);
            }
        } else if (colop2.equals("Not Like")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 not like '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col2);
            }
        } else if (colop2.equals("is null")) {
            whereClaus += " and COLUMN2 is null ";
            whereClaus = whereClaus.replace("$col2", "" + col2);
            searchParameter += "Column 2 is null -";
            searchParameter = searchParameter.replace("$F", "" + col2);
        } else if (colop2.equals("is not null")) {
            whereClaus += " and COLUMN2 is not null ";
            whereClaus = whereClaus.replace("$col2", "" + col2);
            searchParameter += "Column 2 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop2.equals("<>")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 <> '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col2);
            }
        } else if (colop2.equals("=")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 = '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col2);
            }
        }
        if (colop3.equals("Like")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 like '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
            }
        } else if (colop3.equals("Not Like")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 not like '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
            }
        } else if (colop3.equals("is null")) {
            whereClaus += " and COLUMN3 is null ";
            whereClaus = whereClaus.replace("$col3", "" + col3);
            searchParameter += "Column 3 is null -";
            searchParameter = searchParameter.replace("$F", "" + col3);
        } else if (colop3.equals("is not null")) {
            whereClaus += " and COLUMN3 is not null ";
            whereClaus = whereClaus.replace("$col3", "" + col3);
            searchParameter += "Column 3 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop3.equals("<>")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 <> '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
            }
        } else if (colop3.equals("=")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 = '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
            }
        }
        if (colop4.equals("Like")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 like '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
            }
        } else if (colop4.equals("Not Like")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 not like '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
            }
        } else if (colop4.equals("is null")) {
            whereClaus += " and COLUMN4 is null ";
            whereClaus = whereClaus.replace("$col4", "" + col4);
            searchParameter += "Column 4 is null -";
            searchParameter = searchParameter.replace("$F", "" + col4);
        } else if (colop4.equals("is not null")) {
            whereClaus += " and COLUMN4 is not null ";
            whereClaus = whereClaus.replace("$col4", "" + col4);
            searchParameter += "Column 4 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop4.equals("<>")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 <> '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
            }
        } else if (colop4.equals("=")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 = '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
            }
        }
        if (colop5.equals("Like")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 like '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col5);
            }
        } else if (colop5.equals("Not Like")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 not like '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col5);
            }
        } else if (colop5.equals("is null")) {
            whereClaus += " and COLUMN5 is null ";
            whereClaus = whereClaus.replace("$col5", "" + col5);
            searchParameter += "Column 5 is null -";
            searchParameter = searchParameter.replace("$F", "" + col5);
        } else if (colop5.equals("is not null")) {
            whereClaus += " and COLUMN5 is not null ";
            whereClaus = whereClaus.replace("$col5", "" + col5);
            searchParameter += "Column 5 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop5.equals("<>")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 <> '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col5);
            }
        } else if (colop5.equals("=")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 = '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col5);
            }
        }

        if (CorrectiveExport != 0) {
            if (CorrectiveExport == 4) {
                whereClaus += " and CORRECTIVE_ENTRY_FLAG is null";
            } else {
                whereClaus += " and CORRECTIVE_ENTRY_FLAG  = $col1";
                whereClaus = whereClaus.replace("$col1", "" + CorrectiveExport);
            }
        }
        if (transactionStatus
                != null && !transactionStatus.equals(
                        "")) {
            whereClaus += " and lower(transaction_status) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + transactionStatus);
        }
        if (transTypeCode
                != 0) {
            whereClaus += " and transaction_type_id = $col1";

            whereClaus = whereClaus.replace("$col1", "" + transTypeCode);
        }
        if (transType
                != null && !transType.equals(
                        "")) {
            whereClaus += " and lower(transaction_type) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + transType);
        }
        if (responseCode
                != 0) {
            String Operator = "";
            if (ResponseVariable == 1) {
                Operator = " = ";
            }
            if (ResponseVariable == 2) {
                Operator = " <> ";
            }

            whereClaus += " and response_code_id " + Operator + " $col1";
            whereClaus = whereClaus.replace("$col1", "" + responseCode);
        }
        if (response
                != null && !response.equals(
                        "")) {
            String Operator = "";
            if (ResponseVariable2 == 1) {
                Operator = " = ";
            }
            if (ResponseVariable2 == 2) {
                Operator = " <> ";
            }

            whereClaus += " and lower(response_code) " + Operator + " lower('$col1')";
            whereClaus = whereClaus.replace("$col1", "" + response);
        }
        if (currencyCode
                != 0) {
            whereClaus += " and currency_id = $col1";
            whereClaus = whereClaus.replace("$col1", "" + currencyCode);
        }

        if (atmId
                != 0) {
            whereClaus += " and ATM_ID = $col1";
            whereClaus = whereClaus.replace("$col1", "" + atmId);
        }

        if (atmCode
                != null && !atmCode.equals(
                        "")) {
            whereClaus += " and lower(ATM_APPLICATION_ID) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + atmCode);
        }

        if (atmGroup
                != 0) {
            whereClaus += " and ATM_ID IN (select id from atm_machine where atm_group in (select id from atm_group where parent_id = $col1 or id = $col1))";
            whereClaus = whereClaus.replace("$col1", "" + atmGroup);
        }
        if (blackList) {
            whereClaus += " and card_no in (select card_no from black_listed_card)";
        }
        if (settled
                != 3) {
            whereClaus += " and NVL (settled_flag, 2) = $col1";
            whereClaus = whereClaus.replace("$col1", "" + settled);
        }

        if (matchingTypeList
                != 0) {
            whereClaus += " and matching_type = $col1 and record_type = $col2";
            whereClaus = whereClaus.replace("$col1", "" + matchingTypeList);
            whereClaus = whereClaus.replace("$col2", "" + masterRecord);
        }

        if (settledBy
                != 0) {
            whereClaus += " and settled_user = $col1";
            whereClaus = whereClaus.replace("$col1", "" + settledBy);

        }
        if (disputedBy
                != 0) {
            whereClaus += " and match_by = $col1";
            whereClaus = whereClaus.replace("$col1", "" + disputedBy);
        }

        if (missingSide
                == 1) {
            whereClaus += " and not exists (select 1 from disputes d where d.dispute_key = disputes.dispute_key)";
        } else if (missingSide
                == 2) {
            whereClaus += " and exists (select 1 from disputes d where d.dispute_key = disputes.dispute_key)";
        }

        if (reverseType
                == 2) {
            whereClaus += " and not exists (select 1 from disputes x where 1=1 " + whereClaus + " and x.record_type=disputes.record_type and x.MATCHING_TYPE = disputes.matching_type and x.TRANSACTION_DATE= disputes.transaction_date and x.ATM_ID= disputes.ATM_ID and x.CURRENCY_ID= disputes.currency_id and x.amount <> disputes.amount  and x.transaction_sequence = disputes.transaction_sequence)";
        } else if (reverseType
                == 1) {
            whereClaus += " and exists (select 1 from disputes x where 1=1 " + whereClaus + " and x.record_type=disputes.record_type and x.MATCHING_TYPE = disputes.matching_type and x.TRANSACTION_DATE= disputes.transaction_date and x.ATM_ID= disputes.ATM_ID and x.CURRENCY_ID= disputes.currency_id and x.amount <> disputes.amount  and x.transaction_sequence = disputes.transaction_sequence)";
        }
        whereClaus += " and Exists (select 1 from user_atm s where s.user_id = $user and s.atm_id = ATM_ID)";
        whereClaus = whereClaus.replace("$user", "" + logedUser.getUserId());

        this.whereCluase = whereClaus;

        this.whereCluase = this.whereCluase.replaceAll("and record_type = [0-9]", "");
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);

        BigDecimal mdDTOL = (BigDecimal) mdDAO.findTotalAmount(whereClaus);

        totalAmount = mdDTOL;

        return mdDTOL;
    }

    public Object gettexttransaction(DisputesDTOInter mdDTO) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        String mddtoi = (String) mdDAO.Findtexttransaction(mdDTO);
        return mddtoi;
    }

    public Object gettexttransaction_other(DisputesDTOInter mdDTO) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        String mddtoi = (String) mdDAO.Findtexttransaction_other(mdDTO);
        return mddtoi;
    }

    public Object FindFiletransaction(DisputesDTOInter mdDTO) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        Clob mddtoi = (Clob) mdDAO.FindFiletransaction(mdDTO);
        return mddtoi;
    }

    public Object FindFiletransaction_other(DisputesDTOInter mdDTO) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        Clob mddtoi = (Clob) mdDAO.FindFiletransaction_other(mdDTO);
        return mddtoi;
    }

    public Object getAnotherSideOfMatchedData(DisputesDTOInter mdDTO) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        DisputesDTOInter mddtoi = (DisputesDTOInter) mdDAO.findAnotherSide(mdDTO);
        List<DisputesDTOInter> mdDTOL = new ArrayList<DisputesDTOInter>();
        if (mddtoi != null) {
            mdDTOL.add(mddtoi);
        }
        /*
         for (DisputesDTOInter d : mdDTOL) {
         String s = d.getDisputeReason();
         DisputeRulesDAOInter drDAO = DAOFactory.createDisputeRulesDAO(null);
         List<DisputeRulesDTOInter> drDTOL = new ArrayList<DisputeRulesDTOInter>();
         drDTOL = (List<DisputeRulesDTOInter>) drDAO.findDisputesReasons(d);
         if (drDTOL.size() != 0) {
         for (DisputeRulesDTOInter dd : drDTOL) {
         s += dd.getName() + " - ";
         }
         }
         d.setReasonToDisp(s);
         }*/
        return mdDTOL;
    }

    public Object MarkAsExport2(DisputesDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        for (DisputesDTOInter mdDTO : mdDTOA) {
            mdDAO.updateForExport2(mdDTO, logedUser);
        }
        return null;
    }

    public Object MarkAsExport(DisputesDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);

        mdDAO.updateForExport(mdDTOA, logedUser);

        return null;
    }

    public Object MarkAsCorrectiveEntryCheck(DisputesDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        mdDAO.updateForCorrectiveEntry(mdDTOA, logedUser);
        return null;
    }

    public Object MatchedMarkAsSettled(DisputesDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        for (DisputesDTOInter mdDTO : mdDTOA) {
            mdDAO.updateForDisputePage(mdDTO, logedUser);
        }
        return null;
    }

    public Object MatchedMarkAsNotSettled(DisputesDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        for (DisputesDTOInter mdDTO : mdDTOA) {
            mdDAO.updateForDisputePage2(mdDTO, logedUser);
        }
        return null;
    }

    public Object MatchedMarkAsMatched(DisputesDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);

        String res = "";
        for (DisputesDTOInter mdDTO : mdDTOA) {
            if (mdDTO.getdisputekey() != 0) {
                mdDAO.insertForDisputePage(mdDTO, logedUser);
                mdDAO.deleteForDisputePage(mdDTO);
            } else {
                res += mdDTO.gettransactionseqeunce() + "-";
            }
        }
        return res;
    }

    public Object updateComment(DisputesDTOInter dDTO) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        mdDAO.updateComments(dDTO);
        return null;
    }

    public Object getPickColumn() throws Throwable {
        Session utillist = new Session();
        DualListModel<String> pick = new DualListModel();
        ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);
        List<ColumnDTOInter> cDDTO = (List<ColumnDTOInter>) utillist.GetDefaultColumn();
        List<ColumnDTOInter> cNDDTO = (List<ColumnDTOInter>) utillist.GetNonDefaultColumn();

        List<String> source = new ArrayList<String>();
        List<String> target = new ArrayList<String>();

        for (ColumnDTOInter c : cDDTO) {
            //if (c.getId() != 1) {
            target.add(c.getHeader());
            // }
        }
        for (ColumnDTOInter c : cNDDTO) {
            // if (c.getId() != 1) {
            source.add(c.getHeader());
            // }
        }

        pick.setSource(source);
        pick.setTarget(target);

        return pick;

    }

    protected DisputesBO() {
        super();
    }

    public Object MarkAsCorrectiveEntryUncheck(DisputesDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        mdDAO.updateForCorrectiveEntry2(mdDTOA, logedUser);
        return null;
    }

    public Object updatecorrectiveamount(DisputesDTOInter mdDTOA, UsersDTOInter user) throws Throwable {
        DisputesDAOInter mdDAO = DAOFactory.createDisputesDAO(null);
        mdDAO.updatecorrectiveamount(mdDTOA, user);
        return null;
    }
}
