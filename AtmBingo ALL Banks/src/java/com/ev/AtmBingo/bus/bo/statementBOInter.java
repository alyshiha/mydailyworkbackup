/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.StatementDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Aly.Shiha
 */
public interface statementBOInter extends Serializable {

    Object PrintReport(String user, String customer, Date dateF, String AtmName) throws Throwable;

    List<StatementDTOInter> runReport(String dateFrom, String ATMID) throws Throwable;
    
}
