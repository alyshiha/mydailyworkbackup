
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.userPrivilegeDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.userPrivilegeDTOInter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author AlyShiha
 */
public class userPrivilegeBO implements userPrivilegeBOInter {

    public Object printPDFrep(String customer, String PrintedName, String name,int user) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        if ("null".equals(name) || name == null) {
            params.put("name", "");
        } else {
            params.put("name", "" + name + "");
        }
        params.put("customer", customer);
        params.put("userPrint", PrintedName);
        params.put("userid",new BigDecimal(user));

        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/privAssign.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "privAssign" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    public List<String> findPrivelage(Integer userid, String PageName) throws Throwable {
        userPrivilegeDAOInter engin = DAOFactory.createuserPrivilegeDAO();
        List<String> records = engin.findPrivelage(userid, PageName);
        return records;
    }

    public Boolean findDuplicateRecord(int user, int menu, int prev, int id) throws Throwable {
        userPrivilegeDAOInter engin = DAOFactory.createuserPrivilegeDAO();
        Boolean records = engin.findDuplicatesaveRecord(user, menu, prev, id);
        return records;
    }

    public Boolean findDuplicateRecord(int user, int menu, int prev) throws Throwable {
        userPrivilegeDAOInter engin = DAOFactory.createuserPrivilegeDAO();
        Boolean records = engin.findDuplicateRecord(user, menu, prev);
        return records;
    }

    @Override
    public List<userPrivilegeDTOInter> findRecord(String userid) throws Throwable {
        userPrivilegeDAOInter engin = DAOFactory.createuserPrivilegeDAO();
        List<userPrivilegeDTOInter> records = engin.findRecord(userid);
        return records;
    }

    @Override
    public List<userPrivilegeDTOInter> findAll() throws Throwable {
        userPrivilegeDAOInter engin = DAOFactory.createuserPrivilegeDAO();
        List<userPrivilegeDTOInter> records = engin.findAll();
        return records;
    }

    @Override
    public String insertRecord(userPrivilegeDTOInter record) {
        try {
            userPrivilegeDAOInter engin = DAOFactory.createuserPrivilegeDAO();
            engin.insert(record);
            return "Record Has Been Succesfully Added";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String updateRecord(userPrivilegeDTOInter record) {
        try {
            userPrivilegeDAOInter engin = DAOFactory.createuserPrivilegeDAO();
            engin.update(record);
            return "Record Has Been Succesfully Updates";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String deleteRecord(userPrivilegeDTOInter record) {
        try {
            userPrivilegeDAOInter engin = DAOFactory.createuserPrivilegeDAO();
            engin.delete(record);
            return "Record Has Been Succesfully Deleted";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }
}
