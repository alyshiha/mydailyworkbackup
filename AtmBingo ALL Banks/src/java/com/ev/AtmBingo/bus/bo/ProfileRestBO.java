/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.ProfileDAOInter;
import com.ev.AtmBingo.bus.dto.ProfileDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ProfileRestBO extends BaseBO implements ProfileRestBOInter,Serializable{

    protected ProfileRestBO() {
        super();
    }

    public Object getProfiles(int rest)throws Throwable{
        ProfileDAOInter pDAO = DAOFactory.createProfileDAO(null);
        List<ProfileDTOInter> pDTOL = (List<ProfileDTOInter>)pDAO.findAll(rest);
        return pDTOL;
    }

    public Object update(ProfileDTOInter pDTO)throws Throwable{
        ProfileDAOInter pDAO = DAOFactory.createProfileDAO(null);
        pDAO.update(pDTO);
        return "Done";
    }

}
