/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.CurrencyMasterDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.NetworkDetailsDAOInter;
import com.ev.AtmBingo.bus.dao.NetworkGroupDAOInter;
import com.ev.AtmBingo.bus.dao.NetworkMasterDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionTypeMasterDAOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkDetailsDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkGroupDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkMasterDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeMasterDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author KhAiReE
 */
public class NetworkBO extends BaseBO implements NetworkBOInter,Serializable{

    protected NetworkBO() {
        super();
    }

    public Object getNetworkGroup() throws Throwable {
        NetworkGroupDAOInter ngDAO = DAOFactory.createNetworkGroupDAO(null);
        List<NetworkGroupDTOInter> nmDTOL  = (List<NetworkGroupDTOInter>) ngDAO.findAll();
        return nmDTOL ;
    }

    public Object getNetworkMaster(int id) throws Throwable {
        NetworkMasterDAOInter nmDAO = DAOFactory.createNetworkMasterDAO(null);
        List<NetworkMasterDTOInter> nmDTOL  = (List<NetworkMasterDTOInter>) nmDAO.findByIdMaster(id);
        return nmDTOL ;
    }

    public Object getNetworkDetails(int id) throws Throwable {
        NetworkDetailsDAOInter nmDAO = DAOFactory.createNetworkDetailsDAO(null);
        List<NetworkDetailsDTOInter> nmDTOL  = (List<NetworkDetailsDTOInter>) nmDAO.findByIdMaster(id);
        return nmDTOL ;
    }

    public Object getCurrency() throws Throwable {
        CurrencyMasterDAOInter cDAO = DAOFactory.createCurrencyMasterDAO(null);
        List<CurrencyMasterDTOInter> cDTOL = (List<CurrencyMasterDTOInter>) cDAO.findAll();
        return cDTOL;
    }

    public Object getTransactionType() throws Throwable {
        TransactionTypeMasterDAOInter ttDAO = DAOFactory.createTransactionTypeMasterDAO(null);
        List<TransactionTypeMasterDTOInter> ttDTOL = (List<TransactionTypeMasterDTOInter>) ttDAO.findAll();
        return ttDTOL;
    }

    public Object editeNetworkGroup(NetworkGroupDTOInter ngDTO, int operation) throws Throwable {

        NetworkGroupDAOInter ngDAO = DAOFactory.createNetworkGroupDAO(null);

        switch (operation) {
            case INSERT:
                ngDAO.insert(ngDTO);
                return "Insert Done";
            case UPDATE:
                ngDAO.update(ngDTO);
                return "Update Done";
            case DELETE:
                ngDAO.delete(ngDTO);
                return "Delete Done";
            default:
                return "There Is No Operation";
        }
    }

    public Object editeNetworkMasterSingle(NetworkMasterDTOInter nmDTO, int operation, Integer oldCurr, Integer oldTrans ) throws Throwable {

       NetworkMasterDAOInter nmDAO = DAOFactory.createNetworkMasterDAO(null);

        switch (operation) {
            case INSERT:
               nmDAO.insert(nmDTO);
                return "Insert Done";
            case UPDATE:
                nmDAO.update(nmDTO, oldCurr,oldTrans);
                return "Update Done";
            case DELETE:
                nmDAO.delete(nmDTO);
                return "Delete Done";
            default:
                return "There Is No Operation";
        }
    }

    public Object editeNetworkDetailsSingle(NetworkDetailsDTOInter nmDTO, int operation, Integer oldCurr, Integer oldTrans, String oldBin ) throws Throwable {

       NetworkDetailsDAOInter nmDAO = DAOFactory.createNetworkDetailsDAO(null);

        switch (operation) {
            case INSERT:
               nmDAO.insert(nmDTO);
                return "Insert Done";
            case UPDATE:
                nmDAO.update(nmDTO, oldCurr,oldTrans,oldBin);
                return "Update Done";
            case DELETE:
                nmDAO.delete(nmDTO);
                return "Delete Done";
            default:
                return "There Is No Operation";
        }
    }
    
    public static void main (String[] args) throws Throwable{
   
    }

}
