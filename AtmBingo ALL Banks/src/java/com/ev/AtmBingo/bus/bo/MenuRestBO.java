/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.MenuLabelDAOInter;
import com.ev.AtmBingo.bus.dto.MenuLabelDTOInter;
import java.io.Serializable;
import java.util.List;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author Administrator
 */
public class MenuRestBO extends BaseBO implements MenuRestBOInter,Serializable{

    protected MenuRestBO() {
        super();
    }

    public TreeNode getMenuTree(int rest) throws Throwable {
        TreeNode ROOT = new DefaultTreeNode("Root", null);
        MenuLabelDAOInter mlDAO = DAOFactory.createMenuLabelDAO(null);
        List<MenuLabelDTOInter> mlDTOL = (List<MenuLabelDTOInter>) mlDAO.findsParents(rest);
        TreeNode[] roots = new TreeNode[mlDTOL.size()];
        int i = 0;
        for (MenuLabelDTOInter m : mlDTOL) {
            int j = 0;
            roots[i] = new DefaultTreeNode(m, ROOT);
            List<MenuLabelDTOInter> mlCH1DTOL = m.getChildMenu(rest,0);
            if (mlCH1DTOL != null) {
                TreeNode[] children1 = new TreeNode[mlCH1DTOL.size()];
                for (MenuLabelDTOInter mm : mlCH1DTOL) {
                    int z = 0;
                    children1[j] = new DefaultTreeNode(mm, roots[i]);
                    List<MenuLabelDTOInter> mlCH2DTOL = mm.getChildMenu( rest,0);
                    if (mlCH2DTOL != null) {
                        TreeNode[] children2 = new TreeNode[mlCH2DTOL.size()];
                        for (MenuLabelDTOInter mmm : mlCH2DTOL) {
                            int x = 0;
                            children2[z] = new DefaultTreeNode(mmm, children1[j]);
                            List<MenuLabelDTOInter> mlCH3DTOL = mmm.getChildMenu(rest,0);
                            if (mlCH3DTOL != null) {
                                TreeNode[] children3 = new TreeNode[mlCH3DTOL.size()];
                                for (MenuLabelDTOInter mmmm : mlCH3DTOL) {
                                    children3[x] = new DefaultTreeNode(mmmm, children2[z]);
                                    x++;
                                }
                            }
                            z++;
                        }
                    }
                    j++;
                }
            }
            i++;
        }
        return ROOT;
    }

    public Object updateMenu(MenuLabelDTOInter mlDTO)throws Throwable{
        MenuLabelDAOInter mlDAO = DAOFactory.createMenuLabelDAO(null);
        mlDAO.updateRest(mlDTO);
        return "Done";
    }

}
