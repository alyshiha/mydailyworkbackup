/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.MenuLabelDTOInter;
import org.primefaces.model.TreeNode;

/**
 *
 * @author Administrator
 */
public interface MenuRestBOInter {

    TreeNode getMenuTree(int rest) throws Throwable;

    Object updateMenu(MenuLabelDTOInter mlDTO) throws Throwable;

}
