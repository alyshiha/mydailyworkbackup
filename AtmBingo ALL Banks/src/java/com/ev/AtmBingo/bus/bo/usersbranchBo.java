package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.AtmBingo.bus.dao.usersbranchDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.usersbranchDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.AtmBingo.bus.dto.usersbranchDTOInter;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class usersbranchBo extends BaseBO implements usersbranchBOInter,Serializable {

    protected usersbranchBo() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        usersbranchDTOInter RecordToInsert = (usersbranchDTOInter) obj[0];
        usersbranchDAOInter engin = DAOFactory.createusersbranchDAO();
        engin.insertrecord(RecordToInsert);
        return "Record Has Been Inserted";
    }

    public Object updaterecord(Object... obj) throws Throwable {
        usersbranchDTOInter RecordToUpdate = (usersbranchDTOInter) obj[0];
        usersbranchDAOInter engin = DAOFactory.createusersbranchDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        usersbranchDAOInter engin = DAOFactory.createusersbranchDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        usersbranchDTOInter RecordToDelete = (usersbranchDTOInter) obj[0];
        usersbranchDAOInter engin = DAOFactory.createusersbranchDAO();
        engin.deleterecord(RecordToDelete);
        return "Record Has Been Deleted";
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<usersbranchDTOInter> SearchRecords = (List<usersbranchDTOInter>) obj[0];
        usersbranchDAOInter engin = DAOFactory.createusersbranchDAO();
        List<usersbranchDTOInter> AllRecords = (List<usersbranchDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        usersbranchDAOInter engin = DAOFactory.createusersbranchDAO();
        List<usersbranchDTOInter> AllRecords = (List<usersbranchDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }
//used
    public Object GetRecord(Object... obj) throws Throwable {
        String SearchParameter = (String) obj[0];
        usersbranchDAOInter engin = DAOFactory.createusersbranchDAO();
        usersbranchDTOInter Record = (usersbranchDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
//used
    public String ValidateLog(String userName, String password, String version, Integer NumOfUsers, Date EndDate) throws Throwable {
        usersbranchDAOInter uDAO = DAOFactory.createusersbranchDAO();
        String Msg = (String) uDAO.ValidateLog(userName, password, version, NumOfUsers, EndDate);
        return Msg;
    }
    
    //used
    public Object save(Object... obj) throws SQLException, Throwable {
List<usersbranchDTOInter> entities = (List<usersbranchDTOInter>) obj[0];
usersbranchDAOInter engin = DAOFactory.createusersbranchDAO();
engin.save(entities);
return "Insert Done";
}
}
