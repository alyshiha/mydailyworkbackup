/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.usersbranchDTOInter;

/**
 *
 * @author Administrator
 */
public interface ChangePassBOInter {

    public Boolean validatePass2(String pass, int id) throws Throwable;

    public Boolean validatePass(String pass, int id) throws Throwable;

    public Boolean validatePassword(String pass, int id) throws Throwable;
    public Boolean validatePasswordBranch(String pass, int id) throws Throwable;

    public UsersDTOInter getSelected(int id) throws Throwable;
Object updatePassBranch(usersbranchDTOInter loggedInUser, String oldPass, String confPass, String newPass, Boolean reset) throws Throwable ;
    public Object updatePass(UsersDTOInter loggedInUser, String oldPass, String confPass, String newPass,Boolean reset) throws Throwable;

    public Object updatePass(UsersDTOInter uDTO, String confPass, String newPass) throws Throwable;

    public Object Checkpass(UsersDTOInter uDTO, String confPass, String newPass) throws Throwable;

    public Object getUsers(int rest) throws Throwable;
}
