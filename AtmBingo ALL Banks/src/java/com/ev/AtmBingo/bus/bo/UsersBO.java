/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.LangDAOInter;
import com.ev.AtmBingo.bus.dao.ProfileDAOInter;
import com.ev.AtmBingo.bus.dao.TimeShiftDAOInter;
import com.ev.AtmBingo.bus.dao.UserProfileDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.LangDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileDTOInter;
import com.ev.AtmBingo.bus.dto.TimeShiftDTOInter;
import com.ev.AtmBingo.bus.dto.UserProfileDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class UsersBO extends BaseBO implements UsersBOInter,Serializable {

    protected UsersBO() {
        super();
    }

    public Object getTimeShifts() throws Throwable {
        TimeShiftDAOInter tsDAO = DAOFactory.createTimeShiftDAO(null);
        List<TimeShiftDTOInter> tsDTOL = (List<TimeShiftDTOInter>) tsDAO.findRecordGroup();
        return tsDTOL;
    }

    public Boolean getaccess(UsersDTOInter user) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        Boolean cmDTOL =  uDAO.getaccess(user);
        return cmDTOL;
    }
   public UsersDTOInter finduser(Integer user) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        UsersDTOInter cmDTOL = (UsersDTOInter)  uDAO.find(user);
        return cmDTOL;
    }
    public Object getlang() throws Throwable {
        LangDAOInter tsDAO = DAOFactory.createLangDAO(null);
        List<LangDTOInter> tsDTOL = (List<LangDTOInter>) tsDAO.findAll();
        return tsDTOL;
    }

    public Object getUsers(int reat) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> uDTOL = (List<UsersDTOInter>) uDAO.findALl(reat);
        setProfilesToUsers(uDTOL);
        return uDTOL;
    }

    public Object getPrifiles(int restricted) throws Throwable {
        ProfileDAOInter pDAO = DAOFactory.createProfileDAO(null);
        List<ProfileDTOInter> pDTOL = (List<ProfileDTOInter>) pDAO.findAll(restricted);
        return pDTOL;
    }

    public Object editeUsers(UsersDTOInter uDTO, int operation) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        UserProfileDAOInter upDAO = DAOFactory.createUserProfileDAO(null);
        UserProfileDTOInter upDTO = DTOFactory.createUserProfileDTO();
        switch (operation) {
            case INSERT:
                uDAO.insert(uDTO);
                upDTO.setProfileId(uDTO.getProfileDTO().getProfileId());
                upDTO.setUserId(uDTO.getUserId());
                upDAO.insert(upDTO);
                return "Insert Done";
            case UPDATE:
                uDAO.update(uDTO);
                upDAO.updateByUserId(uDTO.getUserId(), uDTO.getProfileDTO().getProfileId());

                return "Update Done";
            case DELETE:
                uDAO.delete(uDTO);
                return "Delete Done";
            default:
                return "Their Is No Operation";
        }

    }

    private void setProfilesToUsers(List<UsersDTOInter> uDTOL) throws Throwable {
        UserProfileDAOInter upDAO = DAOFactory.createUserProfileDAO(null);
        ProfileDAOInter pDAO = DAOFactory.createProfileDAO(null);

        for (UsersDTOInter u : uDTOL) {
            UserProfileDTOInter upDTO = (UserProfileDTOInter) upDAO.findByUserId(u.getUserId());
            ProfileDTOInter pDTO = (ProfileDTOInter) pDAO.find(upDTO.getProfileId());
            u.setProfileDTO(pDTO);
        }
    }
}
