/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.DeleteTransactionsDataDTO;
import com.ev.AtmBingo.bus.dto.DeleteTransactionsDataDTOInter;

/**
 *
 * @author Administrator
 */

public interface DeleteTransactionsDataBOInter extends BaseBOInter{

     void  DeleteTransactionsData(DeleteTransactionsDataDTOInter DTDD) throws Throwable ;
}
