/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.privilegeStatusNameDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface privilegeStatusNameBOInter {

    List<privilegeStatusNameDTOInter> findprivassigned(String Userid, String MenuID) throws Throwable;
    String deleteprivassigned(String Userid, String MenuID);
    String insertpriv(String Userid, String MenuID, String PrevID);
    String deleteRecord(privilegeStatusNameDTOInter record);

    List<privilegeStatusNameDTOInter> findAll() throws Throwable;
    
    Object printPDFrep(String customer, String PrintedName, String name) throws Throwable;

    List<privilegeStatusNameDTOInter> findRecord(String Name) throws Throwable;

    String insertRecord(privilegeStatusNameDTOInter record);

    String updateRecord(privilegeStatusNameDTOInter record);
    
}
