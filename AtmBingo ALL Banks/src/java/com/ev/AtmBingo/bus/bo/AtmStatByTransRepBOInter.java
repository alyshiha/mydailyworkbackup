/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.statbytransDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AtmStatByTransRepBOInter {

    Object getGrp()throws Throwable;

    String runReport(String dateFrom, String dateTo, int atmGroupInt, String user, int NoOfAtms,String cust) throws Throwable;
    List<statbytransDTOInter> runReportexcel(String dateFrom, String dateTo, int atmGroupInt, String user, int NoOfAtms,String cust) throws Throwable ;
}
