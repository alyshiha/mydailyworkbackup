/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.CurrencyDAOInter;
import com.ev.AtmBingo.bus.dao.CurrencyMasterDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.CurrencyDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class CurrencyDefinitionBO extends BaseBO implements CurrencyDefinitionBOInter ,Serializable{

    protected CurrencyDefinitionBO() {
        super();

    }

    public Object getCurrency(CurrencyMasterDTOInter cmDTO) throws Throwable {
        CurrencyDAOInter cDAO = DAOFactory.createCurrencyDAO(null);
        List<CurrencyDTOInter> cDTOL = (List<CurrencyDTOInter>) cDAO.findByMaster(cmDTO);
        return cDTOL;
    }
public int CountCurrencyMaster() throws Throwable{
CurrencyMasterDAOInter cDAO = DAOFactory.createCurrencyMasterDAO(null);
        int cDTOL = cDAO.CountCurrencyMaster();
        return cDTOL;
}
public int CountCurrencydetail(int cmDTO) throws Throwable{
 CurrencyDAOInter cDAO = DAOFactory.createCurrencyDAO(null);
        int cDTOL =  cDAO.CountCurrencydetail(cmDTO);
        return cDTOL;
}
    public Object editeCurrency(CurrencyDTOInter cDTO, int operation) throws Throwable {
        CurrencyDAOInter cDAO = DAOFactory.createCurrencyDAO(null);
        switch (operation) {
            case INSERT:
                cDAO.insert(cDTO);
                return "Insert Done";
            case UPDATE:
                cDAO.update(cDTO);
                return "Update Done";
            case DELETE:
                cDAO.delete(cDTO);
                return "Delete Done";
            default:
                return "Their Is No Operation";
        }
    }

    public Object getCurrencyMaster() throws Throwable {
        CurrencyMasterDAOInter cmDAO = DAOFactory.createCurrencyMasterDAO(null);
        List<CurrencyMasterDTOInter> cmDTOL = (List<CurrencyMasterDTOInter>) cmDAO.findAll();
        return cmDTOL;
    }

    public Object editeCurrencyMaster(CurrencyMasterDTOInter cmDTO, int operation) throws Throwable {
        CurrencyMasterDAOInter cmDAO = DAOFactory.createCurrencyMasterDAO(null);
        CurrencyDAOInter cDAO = DAOFactory.createCurrencyDAO(null);

        switch (operation) {
            case INSERT:
                cmDAO.insert(cmDTO);
                return "Insert Done";
            case UPDATE:

                cmDAO.update(cmDTO);
                return "Update Done";
            case DELETE:
//                List<CurrencyDTOInter> c = (List<CurrencyDTOInter>) cDAO.findByMaster(cmDTO);
//                if (c.size() != 0) {
//                    for (CurrencyDTOInter cc : c) {
//                        cDAO.delete(cc);
//                    }
//                }
                cmDAO.delete(cmDTO);
                return "Delete Done";
            default:
                return "Their Is No Operation";
        }
    }
}
