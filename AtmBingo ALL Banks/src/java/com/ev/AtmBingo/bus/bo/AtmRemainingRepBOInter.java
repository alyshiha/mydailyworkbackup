/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.atmremainingDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AtmRemainingRepBOInter {

    Object getGrp()throws Throwable;
    
    String runReport(String dateTo, int atmGroupInt, String user, String cust, int atmID) throws Throwable;
    List<atmremainingDTOInter> runReportExcel(String dateTo, int atmGroupInt, String user, String cust, int atmID) throws Throwable ;
}
