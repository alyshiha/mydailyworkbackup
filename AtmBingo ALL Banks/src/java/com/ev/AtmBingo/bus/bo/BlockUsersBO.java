/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.BlockReasonDAOInter;
import com.ev.AtmBingo.bus.dao.BlockUsersDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.BlockReasonDTOInter;
import com.ev.AtmBingo.bus.dto.BlockUsersDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class BlockUsersBO extends BaseBO implements BlockUsersBOInter,Serializable{

    protected BlockUsersBO() {
        super();
    }

    public Object getBlockedUsers()throws Throwable{
        BlockUsersDAOInter buDAO = DAOFactory.createBlockUsersDAO(null);
        List<BlockUsersDTOInter> buDTOL = (List<BlockUsersDTOInter>)buDAO.findAll();
        return buDTOL;
    }
public Object getlockedUsers() throws Throwable{
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> uDTOL = (List<UsersDTOInter>)uDAO.findUnlockUsers();
        return uDTOL;
    }
    public Object getUsers(int rest) throws Throwable{
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> uDTOL = (List<UsersDTOInter>)uDAO.findUnblockUsers(rest);
        return uDTOL;
    }

    public Object getBlockReasons()throws Throwable{
        BlockReasonDAOInter brDAO = DAOFactory.createBlockReasonDAO(null);
        List<BlockReasonDTOInter> brDTOL = (List<BlockReasonDTOInter>)brDAO.findAll();
        return brDTOL;
    }
    public Object blockHim(BlockUsersDTOInter buDTO)throws Throwable{
        BlockUsersDAOInter buDAO = DAOFactory.createBlockUsersDAO(null);
        buDAO.insert(buDTO);
        return "done";
    }
 public Object unlockuser(UsersDTOInter buDTO)throws Throwable{
        BlockUsersDAOInter buDAO = DAOFactory.createBlockUsersDAO(null);
        buDAO.unlockuser(buDTO);
        return "done";
    }
    public Object unBlockHim(BlockUsersDTOInter buDTO)throws Throwable{
        BlockUsersDAOInter buDAO = DAOFactory.createBlockUsersDAO(null);
        buDAO.delete(buDTO);
        return "done";
    }
}
