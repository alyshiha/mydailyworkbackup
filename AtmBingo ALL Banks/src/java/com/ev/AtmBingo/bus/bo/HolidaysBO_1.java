/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.HolidaysDAOInter_1;
import com.ev.AtmBingo.bus.dto.HolidaysDTOInter_1;
import java.sql.Connection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author shi7a
 */
public class HolidaysBO_1 implements HolidaysBOInter_1 {
    
    @Override
    public Object printPDFrep(String customer, String PrintedName, String type) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        if ("null".equals(type) || type == null) {
            params.put("holidaytype", "%%");
        } else {
            params.put("holidaytype", "%" + type + "%");
        }
        params.put("customer", customer);
        params.put("userPrint", PrintedName);

        try {
          Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/holidays.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "holidays" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);                
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public List<HolidaysDTOInter_1> findRecord(String Type,String Name) throws Throwable {
        HolidaysDAOInter_1 engin = DAOFactory.createHolidaysDAO_1();
        List<HolidaysDTOInter_1> records = engin.findRecord(Type,Name);
        return records;
    }

    @Override
    public List<HolidaysDTOInter_1> findAll() throws Throwable {
        HolidaysDAOInter_1 engin = DAOFactory.createHolidaysDAO_1();
        List<HolidaysDTOInter_1> records = engin.findAll();
        return records;
    }

    @Override
    public String insertRecord(HolidaysDTOInter_1 record) {
        try {
            HolidaysDAOInter_1 engin = DAOFactory.createHolidaysDAO_1();
            engin.insert(record);
            return "Record Has Been Succesfully Added";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }
    
    @Override
    public String updateRecord(HolidaysDTOInter_1 record) {
        try {
            HolidaysDAOInter_1 engin = DAOFactory.createHolidaysDAO_1();
            engin.update(record);
            return "Record Has Been Succesfully Updates";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String deleteRecord(HolidaysDTOInter_1 record) {
        try {
            HolidaysDAOInter_1 engin = DAOFactory.createHolidaysDAO_1();
            return engin.delete(record);

        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

}
