/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author KhAiReE
 */
public interface BingoLogBOInter extends Serializable {

    Object getBingoLogged(Date dateF, Date dateT, Integer user, Integer profile) throws Throwable;

    Object getProfiles(int rest) throws Throwable;

    Object getuserProfiles() throws Throwable;

    Object getUsers(int rest) throws Throwable;
}
