/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.AtmRemainingRepDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.atmremainingDTOInter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author Administrator
 */
public class AtmRemainingRepBO extends BaseBO implements AtmRemainingRepBOInter ,Serializable{

    protected AtmRemainingRepBO() {
        super();
    }

    public Object getGrp() throws Throwable {
        return super.getAtmGrp();
    }

    private String grpName(int grpId) throws Throwable {
        return super.getGrpName(grpId);
    }

    public List<atmremainingDTOInter> runReportExcel(String dateTo, int atmGroupInt, String user, String cust, int atmID)  throws Throwable {
        AtmRemainingRepDAOInter arrRepDAO = DAOFactory.createAtmRemainingRepDAO(null);
        List<atmremainingDTOInter> rs = (List<atmremainingDTOInter>) arrRepDAO.findremainingcashexcelReport(dateTo, atmGroupInt,atmID);
        return rs;
    }

    public String runReport(String dateTo, int atmGroupInt, String user, String cust, int atmID) throws Throwable {
        String atmGroup = "";
        if (atmGroupInt == 0) {
            atmGroup = "All";
        } else {
            atmGroup = grpName(atmGroupInt);
        }
         String atmname = "";
        if (atmID == 0) {
            atmname = "All";
        } else {
            atmname = super.getAtmName(atmID);
        }

        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        JRResultSetDataSource resultSetDataSource;
        AtmRemainingRepDAOInter arrRepDAO = DAOFactory.createAtmRemainingRepDAO(null);

        ResultSet rs = (ResultSet) arrRepDAO.findReport(dateTo, atmGroupInt,atmID);
        resultSetDataSource = new JRResultSetDataSource(rs);

        params.put("user", user);
        params.put("DateTo", dateTo);

        params.put("AtmGroupInt", atmGroupInt);

        params.put("AtmGroup", atmGroup);
        params.put("customer", cust);
        params.put("atmid", new BigDecimal(atmID));
        params.put("atmname", atmname);

        try {

            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/ATM Remaining.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);

            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, resultSetDataSource);

            Calendar c = Calendar.getInstance();
            String uri = "AtmRemaining" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {

                String x = pdfPath + uri;

                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                resultSetDataSource = null;

                CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());
                rs.close();

                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();

            return "fail";
        }

    }

    public static void main(String[] args) throws Throwable {
        AtmRemainingRepBOInter arrBo = BOFactory.createAtmRemainingRepBO(null);
//        arrBo.runReport("01.01.2007 00:00:00", "01.12.2011 00:00:00", 41, "Admin");
    }
}
