/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.TransactionTypeDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionTypeMasterDAOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeMasterDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TransactionTypeBO extends BaseBO implements TransactionTypeBOInter,Serializable{

    protected TransactionTypeBO() {
        super();
    }

    public Object getTranasactionType(TransactionTypeMasterDTOInter cmDTO) throws Throwable {
        TransactionTypeDAOInter cDAO = DAOFactory.createTransactionTypeDAO(null);
        List<TransactionTypeDTOInter> cDTOL = (List<TransactionTypeDTOInter>) cDAO.findByMaster(cmDTO);
        return cDTOL;
    }

    public Object editeTransactionType(TransactionTypeDTOInter cDTO, int operation) throws Throwable {
        TransactionTypeDAOInter cDAO = DAOFactory.createTransactionTypeDAO(null);
        switch (operation) {
            case INSERT:
                cDAO.insert(cDTO);
                return "Insert Done";
            case UPDATE:
                cDAO.update(cDTO);
                return "Update Done";
            case DELETE:
                cDAO.delete(cDTO);
                return "Delete Done";
            default:
                return "Their Is No Operation";
        }
    }

    public Object getTransactionTypeMaster() throws Throwable {
        TransactionTypeMasterDAOInter cmDAO = DAOFactory.createTransactionTypeMasterDAO(null);
        List<TransactionTypeMasterDTOInter> cmDTOL = (List<TransactionTypeMasterDTOInter>) cmDAO.findAll();
        return cmDTOL;
    }

    public Object editeTransactionTypeMaster(TransactionTypeMasterDTOInter cmDTO, int operation) throws Throwable {
        TransactionTypeMasterDAOInter cmDAO = DAOFactory.createTransactionTypeMasterDAO(null);
        TransactionTypeDAOInter cDAO = DAOFactory.createTransactionTypeDAO(null);

        switch (operation) {
            case INSERT:
                cmDAO.insert(cmDTO);
                return "Insert Done";
            case UPDATE:

                cmDAO.update(cmDTO);
                return "Update Done";
            case DELETE:
                List<TransactionTypeDTOInter> c = (List<TransactionTypeDTOInter>) cDAO.findByMaster(cmDTO);
                if (c.size() != 0) {
                    for (TransactionTypeDTOInter cc : c) {
                        cDAO.delete(cc);
                    }
                }
                cmDAO.delete(cmDTO);
                return "Delete Done";
            default:
                return "Their Is No Operation";
        }
    }
}
