/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.invalidReplanishmentsDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface invalidReplanishmentsBOInter {

    Object printinvalidReplanishmentsData(String user, Date dateF, Date dateT, String customer) throws Throwable;

    List<invalidReplanishmentsDTOInter> runReportinvalidReplanishmentsexcel(String dateFrom, String dateTo) throws Throwable;

}
