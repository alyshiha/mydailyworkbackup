/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.AutoRepDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AutoRepAtmToGroupBoInter extends BaseBOInter {
Object UpdateGroup(AutoRepDTOInter amtDTO)throws Throwable;
    Object saveGroup(Integer sourceList, String[] targetList,Integer selectedSource, String[] selectedTarget)throws Throwable;
    Object unsaveGroup(Integer sourceList, String[] targetList,Integer selectedSource, String[] selectedTarget)throws Throwable;
    Object getTargetList(int userId, int CassetteGroup) throws Throwable;
    Object getTargetList2(int userId, int CassetteGroup,int status,Integer repgroup) throws Throwable;
Object DeleteGroup(AutoRepDTOInter amtDTO)throws Throwable;
Object AddGroup(AutoRepDTOInter amtDTO) throws Throwable;
    Object getAll(Integer groupid) throws Throwable;

    Object getSourceList(int userId, int CassetteGroup) throws Throwable;

    Object getGroups() throws Throwable;

    Object getCurrencyMaster() throws Throwable;

    Object getCassetes() throws Throwable;

    Object editeAtmGroup(AutoRepDTOInter amtDTO, int operation) throws Throwable;
}
