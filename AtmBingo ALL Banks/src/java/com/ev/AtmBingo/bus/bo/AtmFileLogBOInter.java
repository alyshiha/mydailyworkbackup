/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface AtmFileLogBOInter extends BaseBOInter {

    Object editeAtmFile(Object obj, int operation) throws Throwable;

    public Object getAtmFileByloadingdate(Date loadingfrom,Date loadingto) throws Throwable;
    

    Object getAtmFile( Date loadingfrom,Date loadingto,int FileType) throws Throwable;

Object getDuplicationTransaction(AtmFileDTOInter afDTO) throws Throwable;

}
