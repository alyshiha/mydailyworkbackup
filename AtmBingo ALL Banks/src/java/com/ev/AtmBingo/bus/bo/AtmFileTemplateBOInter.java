/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.AtmFileHeaderDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDetailDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AtmFileTemplateBOInter {

    Object DeleteAlldetailTemplate(AtmFileTemplateDTOInter afhDTO, String TName) throws Throwable;

    Object DeleteAtmHeader(AtmFileHeaderDTOInter afhDTO, String TName) throws Throwable;

    Object DeleteAtmTemplate(AtmFileTemplateDTOInter afhDTO, String TName) throws Throwable;
Object SaveAllHeader(List<AtmFileHeaderDTOInter> filetemplateHeaderList, String TName, Integer templateid) throws Throwable ;
    Object InsertAtmHeader(AtmFileHeaderDTOInter afhDTO, String TName) throws Throwable;

    Object SaveAllDetail(List<AtmFileTemplateDetailDTOInter> filetemplatedetailList, String TName, Integer templateid) throws Throwable;

    Object UpdateAtmHeader(AtmFileHeaderDTOInter afhDTO, String TName) throws Throwable;

    Object UpdateAtmTemplate(AtmFileTemplateDTOInter afhDTO, String TName) throws Throwable;

    Object deleterecordAllHeader(AtmFileTemplateDTOInter afhDTO, String TName) throws Throwable;

    Object fetFileColumnDefinition() throws Throwable;

    Object DeleteDetail(AtmFileTemplateDetailDTOInter filetemplatedetail, String TName) throws Throwable ;

    Object getAtmFileTemplates(String Table) throws Throwable;

    Object getMachineTypes() throws Throwable;

    Object insertAtmTemplate(AtmFileTemplateDTOInter afhDTO, String TName) throws Throwable;

}
