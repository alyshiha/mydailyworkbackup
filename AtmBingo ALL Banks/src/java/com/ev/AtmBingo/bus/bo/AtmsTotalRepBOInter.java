/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.TotalWithDrawalsDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AtmsTotalRepBOInter {

    Object getGrp()throws Throwable;

    String runReport(String dateFrom, String dateTo, int atmGroupInt, String user,String cust) throws Throwable;
    List<TotalWithDrawalsDTOInter> runReportExcel(String dateFrom, String dateTo, int atmGroupInt) throws Throwable ;
}
