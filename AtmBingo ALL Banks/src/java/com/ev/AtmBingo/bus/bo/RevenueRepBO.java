/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.bo.BaseBO;

import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.NetworkGroupDAOInter;
import com.ev.AtmBingo.bus.dao.NetworkMasterDAOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkGroupDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkMasterDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author KhAiReE
 */
public class RevenueRepBO extends BaseBO implements RevenueRepBOInter ,Serializable{

    protected RevenueRepBO() {
        super();
    }

    public Object findNetworkGroup() throws Throwable {
        NetworkGroupDAOInter ngDAO = DAOFactory.createNetworkGroupDAO(null);
        List<NetworkGroupDTOInter> ngDTOL = (List<NetworkGroupDTOInter>) ngDAO.findAll();
        return ngDTOL;
    }

    public Object findExceptions(int id) throws Throwable {
        NetworkMasterDAOInter nmDAO = DAOFactory.createNetworkMasterDAO(null);
        List<NetworkMasterDTOInter> nmDTOL = (List<NetworkMasterDTOInter>) nmDAO.findByIdMaster(id);
        return nmDTOL;
    }

    public Object getAtmMachines(UsersDTOInter u) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>) amDAO.findRecordGroup(u);
        return amDTOL;
    }

    public Object print(String user, String customer, Date dateF, Date dateT, int networkId, int groupId, int currency, int atmId, int group, int type) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();


        params.put("user", user);
        params.put("networkId", networkId);
        params.put("atmId", atmId);
        params.put("customer", customer);
        params.put("groupId", groupId);
        params.put("currency", currency);
        params.put("dateF", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("dateT", DateFormatter.changeDateAndTimeFormat(dateT));


        try {

              Connection conn =CoonectionHandler.getInstance().getConnection();
            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design;
            if (type == 1) {
                if (group == 1) {
                    design = JRXmlLoader.load("c:/BingoReports/Revenue.jrxml"); 
                } else {
                    design = JRXmlLoader.load("c:/BingoReports/RevenueByATM.jrxml");
                }
            } else {
                if (group == 1) {
                    design = JRXmlLoader.load("c:/BingoReports/RevenueSumm1.jrxml");
                } else {
                    design = JRXmlLoader.load("c:/BingoReports/RevenueSumm2.jrxml");
                }
            }

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);


            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);

           

            Calendar c = Calendar.getInstance();
            String uri;
            if (group == 1) {
                uri = "Revenue" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            } else {
                uri = "RevenueByATM" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            }
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            

            try {

                String x = "../PDF/" + uri;

                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;

                 CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            
            return "fail";
        }
    }
}
