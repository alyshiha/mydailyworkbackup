/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.DepositTransactionDAOInter;
import com.ev.AtmBingo.bus.dto.DepositTransactionDTOInter;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRReportFont;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignReportFont;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author Aly.Shiha
 */
public class DepositTransactionBO extends BaseBO implements Serializable, DepositTransactionBOInter {

    @Override
    public List<DepositTransactionDTOInter> runReport(String dateFrom, String dateTo, String ATMID, String CardNumber) throws Throwable {
        DepositTransactionDAOInter engin = DAOFactory.createDepositTransactionDAO();
        List<DepositTransactionDTOInter> result = (List<DepositTransactionDTOInter>) engin.findAll(dateFrom, dateTo, ATMID, CardNumber);
        return result;
    }

    public Object PrintReport(String user, String customer, Date dateF, Date dateT, String AtmName, String CardNumber) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("customer", customer);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        params.put("ATMNAME", "%" + AtmName + "%");
        params.put("cardnumber", "%" + CardNumber + "%");

        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/Deposit.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);

            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);

            JRReportFont font = new JRDesignReportFont();
            font.setPdfFontName("c:/windows/fonts/arial.ttf");
            font.setPdfEncoding(com.lowagie.text.pdf.BaseFont.IDENTITY_H);
            font.setPdfEmbedded(true);

            Calendar c = Calendar.getInstance();
            String uri;
            uri = "Deposit" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime());
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri + ".pdf");

            try {
                String x = pdfPath + uri + ".pdf";
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }
}
