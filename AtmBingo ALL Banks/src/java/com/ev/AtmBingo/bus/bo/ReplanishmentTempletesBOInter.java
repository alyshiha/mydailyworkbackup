/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentTempletesDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentTempletesDetDTOInter;
import java.util.List;

/**
 *
 * @author KhAiReE
 */
public interface ReplanishmentTempletesBOInter extends BaseBOInter {

    Object editReplanishmentTempletesDetAll(List<ReplanishmentTempletesDetDTOInter> rtdDTOL, int operation) throws Throwable;

    Object editeReplanishmentTempletesDetSingle(ReplanishmentTempletesDetDTOInter rtmdDTO, int operation, int oldType, int oldTypeNo, int idMaster) throws Throwable;

    Object editeReplanishmentTempletesMaster(ReplanishmentTempletesDTOInter rtmDTO, int operation) throws Throwable;

    Object getAtmMachineType() throws Throwable;

    Object getCurrencyMaster() throws Throwable;
    
    Object getCassitte() throws Throwable ;

    Object findByIdMaster (int id) throws Throwable;

    Object getReplanishmentTempletesMaster() throws Throwable;

    Object getReplanishmentmTempletesDetails(int masterId) throws Throwable;

}
