/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.DepositTransactionDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Aly.Shiha
 */
public interface DepositTransactionBOInter {

    List<DepositTransactionDTOInter> runReport(String dateFrom, String dateTo, String ATMID, String CardNumber) throws Throwable;

    Object PrintReport(String user, String customer, Date dateF, Date dateT, String AtmName, String CardNumber) throws Throwable;
}
