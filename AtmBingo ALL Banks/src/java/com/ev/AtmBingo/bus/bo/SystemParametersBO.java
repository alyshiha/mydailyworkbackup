/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.SystemTableDAOInter;
import com.ev.AtmBingo.bus.dto.SystemTableDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class SystemParametersBO extends BaseBO implements SystemParametersBOInter,Serializable{

    protected SystemParametersBO() {
        super();
    }

    public Object getSystemTable()throws Throwable{
        SystemTableDAOInter stDAO = DAOFactory.createSystemTableDAO(null);
        List<SystemTableDTOInter> stDTOL = (List<SystemTableDTOInter>)stDAO.findAll();
        return stDTOL;
    }

    public Object edtiteSystemTable(SystemTableDTOInter stDTO,String oldParameterName, int operation)throws Throwable{
        SystemTableDAOInter stDAO = DAOFactory.createSystemTableDAO(null);
        switch(operation){
            case INSERT:
                stDAO.insert(stDTO);
                return "Insert Done";
            case UPDATE:
                stDAO.update(stDTO,oldParameterName);
                return "Update Done";
            case DELETE:
                stDAO.delete(stDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }

}
