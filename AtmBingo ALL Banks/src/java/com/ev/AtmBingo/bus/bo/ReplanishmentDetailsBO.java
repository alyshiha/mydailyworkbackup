/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.Session;
import com.ev.AtmBingo.base.bo.BaseBO;
//import com.ev.AtmBingo.base.data.DBCache;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.CassetteDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.ReplanishmentDetailDAOInter;
import com.ev.AtmBingo.bus.dao.ReplanishmentMasterDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DepositDTOInter;
import com.ev.AtmBingo.bus.dto.RepPopDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentDetailDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class ReplanishmentDetailsBO extends BaseBO implements ReplanishmentDetailsBOInter ,Serializable{

    protected ReplanishmentDetailsBO() {
        super();
    }

    public Object getReplanishmentData(Date from, Date to, int atmId, Date createdF, Date createdT, int userId, int group, int status, Date From1, Date From2, Date To1, Date To2) throws Throwable {
        String whereClaus = "";
        String dateF = "";
        String dateFD1 = "";
        String dateFD2 = "";
        String dateT = "";
        String dateTD1 = "";
        String dateTD2 = "";
        String creaF = "";
        String creaT = "";
        if (from == null) {
            dateF = "null";
        } else {
            dateF = "'";
            dateF += DateFormatter.changeDateAndTimeFormat(from);
            dateF += "'";
        }
        if (From1 == null) {
            dateTD1 = "null";
        } else {
            dateTD1 = "'";
            dateTD1 += DateFormatter.changeDateAndTimeFormat(From1);
            dateTD1 += "'";
        }
        if (From2 == null) {
            dateTD2 = "null";
        } else {
            dateTD2 = "'";
            dateTD2 += DateFormatter.changeDateAndTimeFormat(From2);
            dateTD2 += "'";
        }
        if (To1 == null) {
            dateFD1 = "null";
        } else {
            dateFD1 = "'";
            dateFD1 += DateFormatter.changeDateAndTimeFormat(To1);
            dateFD1 += "'";
        }
        if (To2 == null) {
            dateFD2 = "null";
        } else {
            dateFD2 = "'";
            dateFD2 += DateFormatter.changeDateAndTimeFormat(To2);
            dateFD2 += "'";
        }

        if (to == null) {
            dateT = "null";
        } else {
            dateT = "'";
            dateT += DateFormatter.changeDateAndTimeFormat(to);
            dateT += "'";
        }

        if (createdF == null) {
            creaF = "null";
        } else {
            creaF = "'";
            creaF += DateFormatter.changeDateAndTimeFormat(createdF);
            creaF += "'";
        }

        if (createdT == null) {
            creaT = "null";
        } else {
            creaT = "'";
            creaT += DateFormatter.changeDateAndTimeFormat(createdT);
            creaT += "'";
        }
        whereClaus = "1=1 ";
        if (dateF != "null") {
            whereClaus += " and (Date_from >=to_date($dateF,'dd.mm.yyyy hh24:mi:ss')) ";

            whereClaus = whereClaus.replace("$dateF", dateF);
        }
        if (dateT != "null") {
            whereClaus += " and (Date_to <=to_date($datT,'dd.mm.yyyy hh24:mi:ss')) ";
            whereClaus = whereClaus.replace("$datT", dateT);
        }

        if (dateFD1 != "null") {
            whereClaus += " and  (((Date_to >=to_date($dF1,'dd.mm.yyyy hh24:mi:ss'))";
            whereClaus = whereClaus.replace("$dF1", dateFD1);
        }
        if (dateFD2 != "null") {
            whereClaus += " and (Date_to <=to_date($dF2,'dd.mm.yyyy hh24:mi:ss')))) ";
            whereClaus = whereClaus.replace("$dF2", dateFD2);
        }

        if (dateTD1 != "null") {
            whereClaus += " and  (((Date_from >=to_date($dT1,'dd.mm.yyyy hh24:mi:ss'))";
            whereClaus = whereClaus.replace("$dT1", dateTD1);
        }
        if (dateTD2 != "null") {
            whereClaus += " and (Date_from <=to_date($dT2,'dd.mm.yyyy hh24:mi:ss')))) ";
            whereClaus = whereClaus.replace("$dT2", dateTD2);
        }
        if (creaF != "null") {
            whereClaus += " and (CREATED_DATE >=to_date($creaF,'dd.mm.yyyy hh24:mi:ss'))";
            whereClaus = whereClaus.replace("$creaF", creaF);
        }
        if (creaT != "null") {
            whereClaus += " and (CREATED_DATE <=to_date($creT,'dd.mm.yyyy hh24:mi:ss'))";
            whereClaus = whereClaus.replace("$creT", creaT);
        }
        if (atmId != 0) {
            whereClaus += " and atm_id = $atmId";
            whereClaus = whereClaus.replace("$atmId", "" + atmId);
        }
        if (status != 0) {
            whereClaus += " and STATUS  $status";
            if (status == 1) {
                whereClaus = whereClaus.replace("$status", " = 1");
            } else {
                whereClaus = whereClaus.replace("$status", " is null");
            }
        }
        if (group != 0) {
            whereClaus += " and ATM_ID IN (select id from atm_machine where atm_group in (select id from atm_group where parent_id in (select id from atm_group where (parent_id = $col1 or id = $col1)) or id = $col1))";
            whereClaus = whereClaus.replace("$col1", "" + group);
        }
        if (userId != 0) {
            if (userId != -1) {
                whereClaus += " and created_by = $userId";
                whereClaus = whereClaus.replace("$userId", "" + userId);
            } else {
                whereClaus += " and created_by is null";
            }
        }

        whereClaus = whereClaus.replace("$creaF", creaF);
        whereClaus = whereClaus.replace("$creT", creaT);

        whereClaus += " order by date_from,created_date";
        ReplanishmentMasterDAOInter rmDAO = DAOFactory.createReplanishmentMasterDAO(null);
       
        Session sessionutil = new Session();
        UsersDTOInter currentUser = sessionutil.GetUserLogging();
        List<ReplanishmentMasterDTOInter> rmDTOL = (List<ReplanishmentMasterDTOInter>) rmDAO.customSearch(whereClaus, currentUser.getUserId());

        return rmDTOL;

    }

    public Object getReplanishmentMaster() throws Throwable {
        ReplanishmentMasterDAOInter rmDAO = DAOFactory.createReplanishmentMasterDAO(null);
        List<ReplanishmentMasterDTOInter> rmDTOL = (List<ReplanishmentMasterDTOInter>) rmDAO.findAll();
        return rmDTOL;
    }

    public RepPopDTOInter replanishmentPopUp(ReplanishmentMasterDTOInter id) throws Throwable {
        ReplanishmentMasterDAOInter rmDAO = DAOFactory.createReplanishmentMasterDAO(null);
        RepPopDTOInter rmDTOL = (RepPopDTOInter) rmDAO.replanishmentPopUp(id);
        return rmDTOL;
    }

    public Object getCassetes() throws Throwable {
        CassetteDAOInter rmDAO = DAOFactory.createCassetteDAO(null);
        List<CassetteDTOInter> rmDTOL = (List<CassetteDTOInter>) rmDAO.findAll();
        return rmDTOL;
    }

    public Object getNewReplanishmentDetail() throws Throwable {
        List<ReplanishmentDetailDTOInter> rdDTOL = new ArrayList<ReplanishmentDetailDTOInter>();
        CassetteDAOInter cDAO = DAOFactory.createCassetteDAO(null);
        List<CassetteDTOInter> cDTOL = (List<CassetteDTOInter>) cDAO.findAll();
        for (CassetteDTOInter c : cDTOL) {
            ReplanishmentDetailDTOInter rdDTO = DTOFactory.createReplanishmentDetailDTO();
            rdDTO.setCassete(c.getId());
            rdDTO.setCurrency(c.getCurrency());
            rdDTOL.add(rdDTO);
        }
        return rdDTOL;
    }
    public Object getReplanishmentDetailDeposit(int masterId) throws Throwable {
        ReplanishmentDetailDAOInter rdDAO = DAOFactory.createReplanishmentDetailDAO(null);
        List<DepositDTOInter> rdDTOL = (List<DepositDTOInter>) rdDAO.findDipositById(masterId);
        return rdDTOL;
    }

    public Object getOldReplanishmentDetail(int masterId) throws Throwable {
        ReplanishmentDetailDAOInter rdDAO = DAOFactory.createReplanishmentDetailDAO(null);
        List<ReplanishmentDetailDTOInter> rdDTOL = (List<ReplanishmentDetailDTOInter>) rdDAO.findById(masterId);
        return rdDTOL;
    }

    public Object RecalculateReplanishment(ReplanishmentMasterDTOInter rmDTO) {
        try {
            ReplanishmentMasterDAOInter rmDAO = DAOFactory.createReplanishmentMasterDAO(null);

            rmDAO.recalc(rmDTO);
            return "Recalculate in progress";
        } catch (Throwable ex) {
            Logger.getLogger(ReplanishmentDetailsBO.class.getName()).log(Level.SEVERE, null, ex);
            return "Recalculate Fail";
        }
    }
      public Object recalcall(ReplanishmentMasterDTOInter rmDTO) {
        try {
            ReplanishmentMasterDAOInter rmDAO = DAOFactory.createReplanishmentMasterDAO(null);

            rmDAO.recalcall(rmDTO.getId());
            return "Recalculate in progress";
        } catch (Throwable ex) {
            Logger.getLogger(ReplanishmentDetailsBO.class.getName()).log(Level.SEVERE, null, ex);
            return "Recalculate Fail";
        }
    }

    public Object getAtmMachines(UsersDTOInter loggedIn, Integer grpId) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>) amDAO.findByAtmGrp(grpId, loggedIn.getUserId());

        /*       DBCache d1=new DBCache();
         List<AtmMachineDTOInter> amDTOL = new ArrayList<AtmMachineDTOInter>();
         amDTOL = (List<AtmMachineDTOInter>)d1.getDBData(
         "select * from atm_machine where id in "
         + "(select atm_id from user_atm where user_id = $user) and atm_valid(id) = 1 order by name");
         */
        return amDTOL;
    }

    public void save(List<ReplanishmentMasterDTOInter> entities) throws Throwable {
        ReplanishmentMasterDAOInter rmDAO = DAOFactory.createReplanishmentMasterDAO(null);
        rmDAO.save(entities);
    }
  public Object editeReplanishmentnew(ReplanishmentMasterDTOInter rmDTO, List<ReplanishmentDetailDTOInter> rdDTOL, int operation) throws Throwable {
        ReplanishmentDetailDAOInter rdDAO = DAOFactory.createReplanishmentDetailDAO(null);
        ReplanishmentMasterDAOInter rmDAO = DAOFactory.createReplanishmentMasterDAO(null);
        CassetteDAOInter cDAO = DAOFactory.createCassetteDAO(null);

        switch (operation) {
            case INSERT:
                rmDAO.insert(rmDTO);
                /* for (ReplanishmentDetailDTOInter rdDTO : rdDTOL) {
                 CassetteDTOInter cDTO = (CassetteDTOInter) cDAO.find(rdDTO.getCassete());
                 int amount = cDTO.getAmount();
                 int value = rdDTO.getCasseteValue();
                 int remaining = rdDTO.getRemaining();
                 int totalAmount = (value - remaining) * amount;
                 int remainingAmount = remaining * amount;
                 rdDTO.setTotalAmount(totalAmount);
                 rdDTO.setRemainingAmount(remainingAmount);
                 rdDTO.setId(rmDTO.getId());
                 rdDAO.update(rdDTO);
                 }*/
                rdDAO.UpdateRep(rdDTOL, rmDTO.getId());
                return "Insert Done";
            case UPDATE:
                rmDAO.update2(rmDTO);
                if (rdDTOL != null) {
                    /*   for (ReplanishmentDetailDTOInter rdDTO : rdDTOL) {
                     CassetteDTOInter cDTO = (CassetteDTOInter) cDAO.find(rdDTO.getCassete());
                     int amount = cDTO.getAmount();
                     int value = rdDTO.getCasseteValue();
                     int remaining = rdDTO.getRemaining();
                     int totalAmount = (value - remaining) * amount;
                     int remainingAmount = remaining * amount;
                     rdDTO.setTotalAmount(totalAmount);
                     rdDTO.setRemainingAmount(remainingAmount);
                     rdDTO.setId(rmDTO.getId());

                     rdDAO.update(rdDTO);
                     }*/
                    rdDAO.UpdateRep(rdDTOL, rmDTO.getId());
                }

                return "Update Done";
            case DELETE:
                rmDAO.delete(rmDTO);
                if (rdDTOL != null) {
                    /*  for (ReplanishmentDetailDTOInter rdDTO : rdDTOL) {
                     rdDAO.delete(rdDTO);
                     }*/
                    rdDAO.DeleteRep(rdDTOL);
                }
                return "Delete Done";
            default:
                return "Their Is No Operation";
        }
    }

    public Object editeReplanishment(ReplanishmentMasterDTOInter rmDTO, List<ReplanishmentDetailDTOInter> rdDTOL, int operation) throws Throwable {
        ReplanishmentDetailDAOInter rdDAO = DAOFactory.createReplanishmentDetailDAO(null);
        ReplanishmentMasterDAOInter rmDAO = DAOFactory.createReplanishmentMasterDAO(null);
        CassetteDAOInter cDAO = DAOFactory.createCassetteDAO(null);

        switch (operation) {
            case INSERT:
                rmDAO.insert(rmDTO);
                /* for (ReplanishmentDetailDTOInter rdDTO : rdDTOL) {
                 CassetteDTOInter cDTO = (CassetteDTOInter) cDAO.find(rdDTO.getCassete());
                 int amount = cDTO.getAmount();
                 int value = rdDTO.getCasseteValue();
                 int remaining = rdDTO.getRemaining();
                 int totalAmount = (value - remaining) * amount;
                 int remainingAmount = remaining * amount;
                 rdDTO.setTotalAmount(totalAmount);
                 rdDTO.setRemainingAmount(remainingAmount);
                 rdDTO.setId(rmDTO.getId());
                 rdDAO.update(rdDTO);
                 }*/
                rdDAO.UpdateRep(rdDTOL, rmDTO.getId());
                return "Insert Done";
            case UPDATE:
                rmDAO.update(rmDTO);
                if (rdDTOL != null) {
                    /*   for (ReplanishmentDetailDTOInter rdDTO : rdDTOL) {
                     CassetteDTOInter cDTO = (CassetteDTOInter) cDAO.find(rdDTO.getCassete());
                     int amount = cDTO.getAmount();
                     int value = rdDTO.getCasseteValue();
                     int remaining = rdDTO.getRemaining();
                     int totalAmount = (value - remaining) * amount;
                     int remainingAmount = remaining * amount;
                     rdDTO.setTotalAmount(totalAmount);
                     rdDTO.setRemainingAmount(remainingAmount);
                     rdDTO.setId(rmDTO.getId());

                     rdDAO.update(rdDTO);
                     }*/
                    rdDAO.UpdateRep(rdDTOL, rmDTO.getId());
                }

                return "Update Done";
            case DELETE:
                rmDAO.delete(rmDTO);
                if (rdDTOL != null) {
                    /*  for (ReplanishmentDetailDTOInter rdDTO : rdDTOL) {
                     rdDAO.delete(rdDTO);
                     }*/
                    rdDAO.DeleteRep(rdDTOL);
                }
                return "Delete Done";
            default:
                return "Their Is No Operation";
        }
    }

    public Boolean validateAtmName(List<AtmMachineDTOInter> amDTOL, String atmName) throws Throwable {
        boolean flag = false;
        for (AtmMachineDTOInter am : amDTOL) {
            if (am.getName().equals(atmName)) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    public Object getUsers() throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> uDTOL = (List<UsersDTOInter>) uDAO.findComboBox();
        return uDTOL;
    }

    public int findByAtmId(String ATMID) throws Throwable {
        AtmMachineDAOInter ATMNAME = DAOFactory.createAtmMachineDAO(null);
        AtmMachineDTOInter atm = (AtmMachineDTOInter) ATMNAME.findByAtmId(ATMID);
        return atm.getId();
    }

    public Date getLastReplanishmentDateFor(int atmId) throws Throwable {
        ReplanishmentMasterDAOInter rmDAO = DAOFactory.createReplanishmentMasterDAO(null);
        Date d = (Date) rmDAO.findLastDate(atmId);
        return d;
    }
}
