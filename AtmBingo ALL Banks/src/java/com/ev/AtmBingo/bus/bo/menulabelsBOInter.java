package com.ev.AtmBingo.bus.bo;
import java.sql.SQLException;
public interface menulabelsBOInter {

Object insertrecord(Object... obj) throws Throwable;
Object save(Object... obj) throws SQLException ,Throwable;
Object updaterecord(Object... obj) throws Throwable;
Object deleteallrecord(Object... obj) throws Throwable; 
Object deleterecord(Object... obj) throws Throwable;
Object GetListOfRecords(Object... obj) throws Throwable;
Object GetRecord(Object... obj) throws Throwable;
Object GetAllRecords(Object... obj) throws Throwable;
Object GetAllRecords2(Object... obj) throws Throwable;
Object GetParentListOfRecords(Object... obj) throws Throwable ;
Object GetPageListOfRecords(Object... obj) throws Throwable;
Boolean ValidatePage(int userID, String page) throws Throwable;
}