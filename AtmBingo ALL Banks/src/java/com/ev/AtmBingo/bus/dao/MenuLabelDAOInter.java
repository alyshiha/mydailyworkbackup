/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.MenuLabelDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface MenuLabelDAOInter {

    Object updateRest(MenuLabelDTOInter mlDTO)throws Throwable;
    Object findAll(int user) throws Throwable;
    
    Object findLinks()throws Throwable;
    
    Object findAllPage(int user) throws Throwable ;

    Object find(int id)throws Throwable;

    Object findByparentId(int parentId,int profileId) throws Throwable;

    Object findParents(int profileId) throws Throwable;

    Object findsParents(int rest) throws Throwable;

    Object findByparentId(int parentId, int rest , int x)throws Throwable;

}
