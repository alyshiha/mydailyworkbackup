/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DepositDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentDetailDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class ReplanishmentDetailDAO extends BaseDAO implements ReplanishmentDetailDAOInter {

    protected ReplanishmentDetailDAO() {
        super();
        super.setTableName("replanishment_detail");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ReplanishmentDetailDTOInter uDTO = (ReplanishmentDetailDTOInter) obj[0];
        String insertStat = "insert into $table values ($id, $cassete , $cassetValue, $totalAmount, $currency, $remaining, $remaininAmount)";

        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$cassete", "" + uDTO.getCassete());
        insertStat = insertStat.replace("$cassetValue", "" + uDTO.getCasseteValue());
        insertStat = insertStat.replace("$currency", "" + uDTO.getCurrency());
        insertStat = insertStat.replace("$remaining", "" + uDTO.getRemaining());
        insertStat = insertStat.replace("$remaininAmount", "" + uDTO.getRemainingAmount());
        insertStat = insertStat.replace("$totalAmount", "" + uDTO.getTotalAmount());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public void UpdateRep(List<ReplanishmentDetailDTOInter> entities, Integer id) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement("update replanishment_detail set cassete_value = ?, "
                    + "currency = ?, "
                    + "remaining = ?"
                    + ", remaining_amount = ?,"
                    + " total_amount = ?"
                    + " where id = ? and cassete = ?");
            CassetteDAOInter cDAO = DAOFactory.createCassetteDAO(null);
            for (int i = 0; i < entities.size(); i++) {
                try {
                    ReplanishmentDetailDTOInter entity = entities.get(i);
                    CassetteDTOInter cDTO;
                    cDTO = (CassetteDTOInter) cDAO.find(entity.getCassete());
                    int amount = cDTO.getAmount();
                    int value = entity.getCasseteValue();
                    int remaining = entity.getRemaining();
                    int totalAmount = (value - remaining) * amount;
                    int remainingAmount = remaining * amount;
                    entity.setTotalAmount(totalAmount);
                    entity.setRemainingAmount(remainingAmount);

                    statement.setInt(6, id);
                    statement.setInt(7, entity.getCassete());
                    statement.setInt(1, entity.getCassettecount());
                    statement.setInt(2, entity.getCurrency());
                    statement.setInt(3, entity.getRemaining());
                    statement.setInt(4, entity.getRemainingAmount());
                    statement.setInt(5, entity.getTotalAmount());

                    statement.addBatch();
                    if ((i + 1) % 1000 == 0) {
                        statement.executeBatch(); // Execute every 1000 items.

                    }
                } catch (Throwable ex) {
                    Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            statement.executeBatch();
            try {

            } catch (Throwable ex) {
                Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ReplanishmentDetailDTOInter uDTO = (ReplanishmentDetailDTOInter) obj[0];
        String updateStat = "update $table set cassete_value = $cassetValue, currency = $currency, remaining = $remaining"
                + ", remaining_amount = $remaininAmount, total_amount = $totalAmount"
                + " where id = $id and cassete = $cassete";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$cassete", "" + uDTO.getCassete());
        updateStat = updateStat.replace("$cassetValue", "" + uDTO.getCasseteValue());
        updateStat = updateStat.replace("$currency", "" + uDTO.getCurrency());
        updateStat = updateStat.replace("$remaining", "" + uDTO.getRemaining());
        updateStat = updateStat.replace("$remaininAmount", "" + uDTO.getRemainingAmount());
        updateStat = updateStat.replace("$totalAmount", "" + uDTO.getTotalAmount());
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ReplanishmentDetailDTOInter uDTO = (ReplanishmentDetailDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id and cassete = $cassete";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        deleteStat = deleteStat.replace("$cassete", "" + uDTO.getCassete());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public void DeleteRep(List<ReplanishmentDetailDTOInter> entities) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement("delete from replanishment_detail where id = ? and cassete = ?");
            for (int i = 0; i < entities.size(); i++) {
                ReplanishmentDetailDTOInter entity = entities.get(i);
                statement.setInt(1, entity.getId());
                statement.setInt(2, entity.getCassete());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            statement.executeBatch();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        ReplanishmentDetailDTOInter uDTO = DTOFactory.createReplanishmentDetailDTO();
        while (rs.next()) {
            uDTO.setCassete(rs.getInt("cassete"));
            uDTO.setCasseteValue(rs.getInt("cassete_value"));
            uDTO.setCurrency(rs.getInt("currency"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setRemaining(rs.getInt("remaining"));
            uDTO.setRemainingAmount(rs.getInt("remaining_amount"));
            uDTO.setTotalAmount(rs.getInt("total_amount"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<ReplanishmentDetailDTOInter> uDTOL = new ArrayList<ReplanishmentDetailDTOInter>();
        while (rs.next()) {
            ReplanishmentDetailDTOInter uDTO = DTOFactory.createReplanishmentDetailDTO();
            uDTO.setCassete(rs.getInt("cassete"));
            uDTO.setCasseteValue(rs.getInt("cassete_value"));
            uDTO.setCurrency(rs.getInt("currency"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setRemaining(rs.getInt("remaining"));
            uDTO.setRemainingAmount(rs.getInt("remaining_amount"));
            uDTO.setTotalAmount(rs.getInt("total_amount"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public List<DepositDTOInter> findDipositById(int id) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select r.CASSETTE_DEPOSIT_COUNT, r.CASSETTE_DEPOSIT_VALUE, r.RETRACT_DEPOSIT_COUNT,\n"
                + "r.RETRACT_DEPOSIT_VALUE, r.RECYCLED_DEPOSIT_COUNT, r.RECYCLED_DEPOSIT_VALUE,\n"
                + "(r.CASSETTE_DEPOSIT_COUNT+r.RETRACT_DEPOSIT_COUNT+r.RECYCLED_DEPOSIT_COUNT) depositcount,\n"
                + "(r.CASSETTE_DEPOSIT_VALUE+r.RETRACT_DEPOSIT_VALUE+r.RECYCLED_DEPOSIT_VALUE) depositvalue,\n"
                + "NVL (r.CASSETE_NAME, c.name) CName\n"
                + "FROM replanishment_detail r, cassette c\n"
                + "WHERE r.cassete = c.id AND c.name like 'D%' AND r.id = $id\n"
                + "ORDER BY c.seq";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        ResultSet rs = executeQuery(selectStat);
        List<DepositDTOInter> uDTOL = new ArrayList<DepositDTOInter>();
        while (rs.next()) {
            DepositDTOInter uDTO = DTOFactory.createDepositDTO();

            uDTO.setCassetteName(rs.getString("CName"));

            uDTO.setDepositCount(rs.getInt("depositcount"));
            uDTO.setDepositValue(rs.getInt("depositvalue"));

            uDTO.setRetractCount(rs.getInt("RETRACT_DEPOSIT_COUNT"));
            uDTO.setRetractValue(rs.getInt("RETRACT_DEPOSIT_VALUE"));

            uDTO.setCassetteCount(rs.getInt("CASSETTE_DEPOSIT_COUNT"));
            uDTO.setCassetteValue(rs.getInt("CASSETTE_DEPOSIT_VALUE"));

            uDTO.setRecycledCount(rs.getInt("RECYCLED_DEPOSIT_COUNT"));
            uDTO.setRecycledValue(rs.getInt("RECYCLED_DEPOSIT_VALUE"));

            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findById(int id) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "SELECT   cassete,\n"
                + "           cassete_value cassete_count,\n"
                + "           (cassete_value * NVL (CASSETE_amount, amount)) cassete_value,\n"
                + "           r.currency currency,\n"
                + "           r.id id,\n"
                + "           remaining ,\n"
                + "           remaining_amount ,\n"
                + "           (cassete_value - remaining) Dispensed_count,\n"
                + "           ( (cassete_value - remaining) * NVL (CASSETE_amount, amount))\n"
                + "              Dispensed_value,\n"
                + "           total_amount,\n"
                + "           NVL (CASSETE_NAME, cassette.name) CName\n"
                + "    FROM   replanishment_detail r, cassette\n"
                + "   WHERE   r.cassete = cassette.id AND cassette.name not like 'D%' AND r.id = $id\n"
                + "ORDER BY   cassette.seq";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        ResultSet rs = executeQuery(selectStat);
        List<ReplanishmentDetailDTOInter> uDTOL = new ArrayList<ReplanishmentDetailDTOInter>();
        while (rs.next()) {
            ReplanishmentDetailDTOInter uDTO = DTOFactory.createReplanishmentDetailDTO();
            uDTO.setCassetename(rs.getString("CName"));
            uDTO.setCassete(rs.getInt("cassete"));
            uDTO.setCasseteValue(rs.getInt("cassete_value"));
            uDTO.setCurrency(rs.getInt("currency"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setRemaining(rs.getInt("remaining"));
            uDTO.setRemainingAmount(rs.getInt("remaining_amount"));
            uDTO.setTotalAmount(rs.getInt("total_amount"));
            uDTO.setCassettecount(rs.getInt("cassete_count"));
            uDTO.setDispensedcount(rs.getInt("Dispensed_count"));
            uDTO.setDispensedvalue(rs.getInt("Dispensed_value"));

            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
