/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DepositTransactionDTOInter;
import java.io.Serializable;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Aly.Shiha
 */
public class DepositTransactionDAO extends BaseDAO implements Serializable, DepositTransactionDAOInter {

    @Override
    public List<DepositTransactionDTOInter> findAll(String from, String To, String ATMID,String CardNumber) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStatment = "select ROWNUM ROWINDEX,D.refunded, "
                + "to_date(to_char(D.TRANSACTIONDATE,'DD/MM/YYYY')||' '||to_char(D.TRANSACTIONTIME,'HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS') Transaction, "
                + "D.valuted, D.escrow, D.error,  D.atmid, D.rejected, D.cardnumber, D.escrowdetail, D.valuteddetail, D.refundeddetail, "
                + "D.rejecteddetail, D.refrence \n"
                + "from DEPOSIT D where (D.TRANSACTIONDATE >= TO_DATE ($P{DateFrom}, 'dd-mm-yyyy hh24:mi:ss') and D.TRANSACTIONDATE <= TO_DATE ($P{DateTo}, 'dd-mm-yyyy hh24:mi:ss'))"
                + " and (D.ATMID LIKE '%$P{ATMIDInt}%' ) and (D.cardnumber Like '%$P{cardnumber}%')  order by d.atmid,d.transactiondate,d.transactiontime";
        selectStatment = selectStatment.replace("$P{DateFrom}", "'" + from + "'");
        selectStatment = selectStatment.replace("$P{DateTo}", "'" + To + "'");
        selectStatment = selectStatment.replace("$P{ATMIDInt}", "" + ATMID);
        selectStatment = selectStatment.replace("$P{cardnumber}", "" + CardNumber);
        ResultSet rs = executeQuery(selectStatment);

        List<DepositTransactionDTOInter> records = new ArrayList<DepositTransactionDTOInter>();
        while (rs.next()) {
            DepositTransactionDTOInter record = DTOFactory.createDepositTransactionDTO();
            record.setRowid(rs.getInt("ROWINDEX"));
            record.setAtmid(rs.getString("atmid"));
            record.setTransactiondate(rs.getTimestamp("Transaction"));
            record.setEscrow(rs.getString("escrow"));
            record.setRefunded(rs.getString("refunded"));
            record.setRejected(rs.getString("rejected"));
            record.setValuted(rs.getString("valuted"));
            record.setError(rs.getString("error"));
            record.setCardnumber(rs.getString("cardnumber"));
            record.setEscrowdetail(rs.getString("escrowdetail"));
            record.setValuteddetail(rs.getString("valuteddetail"));
            record.setRefundeddetail(rs.getString("refundeddetail"));
            record.setRejecteddetail(rs.getString("rejecteddetail"));
            record.setRefrence(rs.getString("refrence"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }
}
