/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.AccountNameDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class AccountNameDAO extends BaseDAO implements AccountNameDAOInter {

    public Boolean findDuplicatesaveRecord(String Name, Integer id) throws Throwable {
        super.preSelect();
        String selectStat = "select id, symbol  from ATM_ACCOUNT_NAME where id <> $id and symbol  = '$name'";
        selectStat = selectStat.replace("$name", Name);
        selectStat = selectStat.replace("$id", "" + id);
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            super.postSelect(rs);
            return Boolean.TRUE;
        }
        super.postSelect(rs);
        return Boolean.FALSE;
    }

    public Boolean findDuplicateRecord(String Name) throws Throwable {
        super.preSelect();
        String selectStat = "select id, symbol  from ATM_ACCOUNT_NAME where symbol  = '$name'";
        selectStat = selectStat.replace("$name", Name);
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            super.postSelect(rs);
            return Boolean.TRUE;
        }
        super.postSelect(rs);
        return Boolean.FALSE;
    }

    @Override
    public List<AccountNameDTOInter> findRecord(String Name) throws Throwable {
        super.preSelect();
        String selectStat = "select id, ACCOUNT_NAME,symbol  from ATM_ACCOUNT_NAME where symbol  like '%$name%' order by ACCOUNT_NAME ";
        selectStat = selectStat.replace("$name", Name);
        ResultSet rs = executeQuery(selectStat);
        List<AccountNameDTOInter> records = new ArrayList<AccountNameDTOInter>();
        while (rs.next()) {
            AccountNameDTOInter record = DTOFactory.createAccountNameDTO();
            record.setId(rs.getInt("id"));
            record.setName(rs.getString("ACCOUNT_NAME"));
            record.setSymbol(rs.getString("symbol"));
            records.add(record);
        }
        super.postSelect(rs);

        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select record in ATM_ACCOUNT_NAME by name like: " + Name);
        return records;
    }

    @Override
    public List<AccountNameDTOInter> findAll() throws Throwable {
        super.preSelect();
        String selectStat = "select id, ACCOUNT_NAME,symbol  from ATM_ACCOUNT_NAME order by ACCOUNT_NAME ";
        ResultSet rs = executeQuery(selectStat);
        List<AccountNameDTOInter> records = new ArrayList<AccountNameDTOInter>();
        while (rs.next()) {
            AccountNameDTOInter record = DTOFactory.createAccountNameDTO();
            record.setId(rs.getInt("id"));
            record.setName(rs.getString("ACCOUNT_NAME"));
            record.setSymbol(rs.getString("symbol"));
            records.add(record);
        }
        super.postSelect(rs);

        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select all records in ATM_ACCOUNT_NAME");
        return records;
    }

    @Override
    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        AccountNameDTOInter record = (AccountNameDTOInter) obj[0];
        record.setId(super.generateSequence("ATM_ACCOUNT_NAME"));
        String insertStat = "insert into ATM_ACCOUNT_NAME   (id, ACCOUNT_NAME,symbol )\n"
                + "values \n"
                + "  ($id, '$symbol','$symbol')";
        insertStat = insertStat.replace("$id", "" + record.getId());
        
        insertStat = insertStat.replace("$symbol", "" + record.getSymbol());
        super.executeUpdate(insertStat);

        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("insert record in ATM_ACCOUNT_NAME with id: " + record.getId());
        return null;
    }

    @Override
    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        AccountNameDTOInter record = (AccountNameDTOInter) obj[0];
        String updateStat = "update ATM_ACCOUNT_NAME\n"
                + "   set symbol  = '$name'\n"
                + " where id = $id";
        updateStat = updateStat.replace("$name", "" + record.getName());
        updateStat = updateStat.replace("$id", "" + record.getId());
        super.executeUpdate(updateStat);

        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("update record in ATM_ACCOUNT_NAME with id: " + record.getId());
        return null;
    }

    @Override
    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        AccountNameDTOInter record = (AccountNameDTOInter) obj[0];
        String deleteStat = "delete ATM_ACCOUNT_NAME\n"
                + " where id = $id";
        deleteStat = deleteStat.replace("$id", "" + record.getId());
        super.executeUpdate(deleteStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("delete record in ATM_ACCOUNT_NAME");
        return null;
    }

}
