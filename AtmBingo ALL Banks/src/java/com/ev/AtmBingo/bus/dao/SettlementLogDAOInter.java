/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import java.io.Serializable;

/**
 *
 * @author KhAiReE
 */
public interface SettlementLogDAOInter extends Serializable {

    Object CustomSearch(Object... obj) throws Throwable;

    Object findAll(Object... obj) throws Throwable;

}
