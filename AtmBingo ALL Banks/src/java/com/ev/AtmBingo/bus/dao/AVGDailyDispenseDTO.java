/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Administrator
 */
public class AVGDailyDispenseDTO implements AVGDailyDispenseDTOInter {
    String atmid,avg;

    @Override
    public String getAtmid() {
        return atmid;
    }

    @Override
    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    @Override
    public String getAvg() {
        return avg;
    }

    @Override
    public void setAvg(String avg) {
        this.avg = avg;
    }
    
}
