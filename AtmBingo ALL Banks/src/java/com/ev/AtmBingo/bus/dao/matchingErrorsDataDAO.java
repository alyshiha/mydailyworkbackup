/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.matchingErrorsDataDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class matchingErrorsDataDAO extends BaseDAO implements matchingErrorsDataDAOInter {

    @Override
    public Object findLoadingInsertErrors(String dateFrom, String dateTo) throws Throwable {
        super.preSelect();
        String selectStatment = "select t.loading_date loading_date,t.atm_application_id atm_application_id,t.transaction_type transaction_type\n"
                + "       ,t.currency currency,t.transaction_status transaction_status,t.response_code response_code,\n"
                + "       t.transaction_date transaction_date,t.transaction_sequence transaction_sequence,t.card_no card_no,\n"
                + "       t.amount amount,t.settlement_date settlement_date,t.notes_presented notes_presented,\n"
                + "       t.customer_account_number customer_account_number,t.error_code error_code,\n"
                + "       t.error_message error_message,t.error_date error_date,to_char(t.transaction_data) transaction_data \n"
                + "from matching_errors_data t\n"
                + "where t.error_date >= to_date ('$P{DateFrom}', 'dd-mm-yyyy hh24:mi:ss')\n"
                + "and t.error_date <= to_date ('$P{DateTo}', 'dd-mm-yyyy hh24:mi:ss')";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DateTo}", dateTo);
        List<matchingErrorsDataDTOInter> Records = new ArrayList<matchingErrorsDataDTOInter>();
        ResultSet rs = executeQueryReport(selectStatment);
        while (rs.next()) {
            matchingErrorsDataDTOInter record = DTOFactory.creatematchingErrorsDataDTO();
            record.setLoadingDate(rs.getTimestamp("loading_date"));
            record.setAtmApplicationId(rs.getString("atm_application_id"));
            record.setTransactionType(rs.getString("transaction_type"));
            record.setCurrency(rs.getString("currency"));
            record.setTransactionStatus(rs.getString("transaction_status"));
            record.setResponseCode(rs.getString("response_code"));
            record.setTransactionDate(rs.getTimestamp("transaction_date"));
            record.setTransactionSequence(rs.getString("transaction_sequence"));
            record.setCardNo(rs.getString("card_no"));
            record.setAmount(rs.getInt("amount"));
            record.setSettlementDate(rs.getTimestamp("settlement_date"));
            record.setNotesPresented(rs.getString("notes_presented"));
            record.setCustomerAccountNumber(rs.getString("customer_account_number"));
            record.setErrorCode(rs.getInt("error_code"));
            record.setErrorMessage(rs.getString("error_message"));
            record.setErrorDate(rs.getTimestamp("error_date"));
            record.setTransactionData(rs.getString("transaction_data"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    
    @Override
    public Object findExceptionLoadingErrors(String dateFrom, String dateTo) throws Throwable {
        super.preSelect();
        String selectStatment = "select t.loading_date loading_date,t.atm_application_id atm_application_id,t.transaction_type transaction_type\n"
                + "       ,t.currency currency,t.transaction_status transaction_status,t.response_code response_code,\n"
                + "       t.transaction_date transaction_date,t.transaction_sequence transaction_sequence,t.card_no card_no,\n"
                + "       t.amount amount,t.settlement_date settlement_date,t.notes_presented notes_presented,\n"
                + "       t.customer_account_number customer_account_number,t.error_code error_code,\n"
                + "       t.error_message error_message,t.error_date error_date,to_char(t.transaction_data) transaction_data \n"
                + "from matching_errors_data t\n"
                + "where t.error_date >= to_date ('$P{DateFrom}', 'dd-mm-yyyy hh24:mi:ss')\n"
                + "and t.error_date <= to_date ('$P{DateTo}', 'dd-mm-yyyy hh24:mi:ss')";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DateTo}", dateTo);
        List<matchingErrorsDataDTOInter> Records = new ArrayList<matchingErrorsDataDTOInter>();
        ResultSet rs = executeQueryReport(selectStatment);
        while (rs.next()) {
            matchingErrorsDataDTOInter record = DTOFactory.creatematchingErrorsDataDTO();
            record.setLoadingDate(rs.getTimestamp("loading_date"));
            record.setAtmApplicationId(rs.getString("atm_application_id"));
            record.setTransactionType(rs.getString("transaction_type"));
            record.setCurrency(rs.getString("currency"));
            record.setTransactionStatus(rs.getString("transaction_status"));
            record.setResponseCode(rs.getString("response_code"));
            record.setTransactionDate(rs.getTimestamp("transaction_date"));
            record.setTransactionSequence(rs.getString("transaction_sequence"));
            record.setCardNo(rs.getString("card_no"));
            record.setAmount(rs.getInt("amount"));
            record.setSettlementDate(rs.getTimestamp("settlement_date"));
            record.setNotesPresented(rs.getString("notes_presented"));
            record.setCustomerAccountNumber(rs.getString("customer_account_number"));
            record.setErrorCode(rs.getInt("error_code"));
            record.setErrorMessage(rs.getString("error_message"));
            record.setErrorDate(rs.getTimestamp("error_date"));
            record.setTransactionData(rs.getString("transaction_data"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }
}
