/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.allbranchtransactionssearchDTOInter;

/**
 *
 * @author Aly
 */
public interface allbrachtransactionssearchDAOInter {


    Object deleterecord(Integer tempid) throws Throwable;

    Object findRecord(Integer tempid) throws Throwable;

    Object findRecordsList(Integer RecordToSelect) throws Throwable;

Object insertrecord(allbranchtransactionssearchDTOInter RecordToInsert,Integer userid) throws Throwable;

    Object updaterecord(Object... obj) throws Throwable;
    
}
