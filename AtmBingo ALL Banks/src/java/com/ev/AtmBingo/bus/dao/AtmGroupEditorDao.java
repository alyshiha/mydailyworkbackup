/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class AtmGroupEditorDao extends BaseDAO implements AtmGroupEditorDaoInter {

    protected AtmGroupEditorDao() {
        super();
        super.setTableName("atm_group");
    }

    public void UpdateAtmByGroupId(List<String> entities, Integer GroupID) throws SQLException {
Connection connection = null;
        PreparedStatement statement = null;
        try {
             connection = CoonectionHandler.getInstance().getConnection();

            statement = connection.prepareStatement("Update atm_machine set atm_group = ? where id = (Select id From atm_machine Where application_id = ?)");
            for (int i = 0; i < entities.size(); i++) {
                String entity = entities.get(i);
                String[] ATMID = entity.split(" --- ");
                statement.setInt(1, GroupID);
                statement.setString(2, ATMID[2].toString());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            statement.executeBatch();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AtmGroupEditorDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AtmGroupEditorDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(AtmGroupEditorDao.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(AtmGroupEditorDao.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
