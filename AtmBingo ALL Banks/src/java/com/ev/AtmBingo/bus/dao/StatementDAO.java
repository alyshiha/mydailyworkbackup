/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DepositTransactionDTOInter;
import com.ev.AtmBingo.bus.dto.StatementDTOInter;
import java.io.Serializable;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aly.Shiha
 */
public class StatementDAO extends BaseDAO implements Serializable, StatementDAOInter {

    @Override
    public List<StatementDTOInter> findAll(String from, String ATMID) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStatment = "SELECT   from_date,\n"
                + "         TO_DATE,\n"
                + "         OPENING_BALANCE,\n"
                + "         CLOSING_BALANCE,\n"
                + "         atm_id\n"
                + "  FROM   STATEMENT_CALCULATIONS a\n"
                + " WHERE  from_date > TO_DATE ('$P{DateFrom}', 'dd-mm-yyyy hh24:mi:ss')\n" +
"      AND atm_id like '%$P{ATMNAME}%' order by atm_id,TO_DATE,OPENING_BALANCE";
        selectStatment = selectStatment.replace("$P{DateFrom}", "" + from + "");
        selectStatment = selectStatment.replace("$P{ATMNAME}", "" + ATMID);
        ResultSet rs = executeQuery(selectStatment);

        List<StatementDTOInter> records = new ArrayList<StatementDTOInter>();
        while (rs.next()) {
            StatementDTOInter record = DTOFactory.createStatementDTO();
            record.setAtmid(rs.getString("atm_id"));
            record.setDatefrom(rs.getTimestamp("from_date"));
            record.setDateto(rs.getTimestamp("TO_DATE"));
            record.setOpeningbalance(rs.getBigDecimal("OPENING_BALANCE"));
            record.setClosingbalance(rs.getBigDecimal("CLOSING_BALANCE"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }
}
