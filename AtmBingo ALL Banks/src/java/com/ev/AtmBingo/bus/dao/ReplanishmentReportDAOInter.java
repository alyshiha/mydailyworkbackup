/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.DepositDTOInter;
import com.ev.AtmBingo.bus.dto.repreportcassetteexcelDTOInter;
import com.ev.AtmBingo.bus.dto.repreportexcelDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface ReplanishmentReportDAOInter {

    Object DeletReplanishmentGaps(Object... obj) throws Throwable;
    List<AVGDailyDispenseDTOInter> averageRepCycleexcel(String AtmID, String AtmGroup, String dateFrom, String dateTo) throws Throwable ;
    String averageRepCycle(String AtmID,String AtmGroup,String dateFrom,String dateTo) throws Throwable ;
List<AVGDailyDispenseDTOInter> Dailydispenseexcel(String dateFrom, String dateTo, String AtmID, String AtmGroup) throws Throwable ;
    String Dailydispense(String dateFrom, String dateTo, String ATMID,String ATMGroup) throws Throwable;
List<AVGDailyDispenseDTOInter> averageloadamountexcel(String dateFrom, String dateTo, String group, String atm)   throws Throwable;
    String averageloadamount(String dateFrom, String dateTo, String group, String atm)   throws Throwable;

    Object deletePrv(Object... obj) throws Throwable;

    Integer calcReplanishmentGaps(String AtmID, String AtmGroup, Date dateFrom, Date dateTo) throws Throwable;

    Object calcReplanishment(int id) throws Throwable;

    Object insertPrv(Object... obj) throws Throwable;

    int getSequence() throws Throwable;

    Object delete(Object... obj) throws Throwable;

    Object insert(Object... obj) throws Throwable;
    
     List<repreportexcelDTOInter> getrepreoprtexcel(Object... obj) throws Throwable ;
     
     List<repreportexcelDTOInter> getrepreoprtcassetexcel(Object... obj) throws Throwable ;

     List<repreportexcelDTOInter> findDipositById(String id) throws Throwable;
}
