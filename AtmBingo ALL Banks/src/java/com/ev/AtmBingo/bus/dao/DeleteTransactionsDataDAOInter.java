/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.DeleteTransactionsDataDTO;
import com.ev.AtmBingo.bus.dto.DeleteTransactionsDataDTOInter;

/**
 *
 * @author Administrator
 */
public interface DeleteTransactionsDataDAOInter {
     Object DeleteTransactionsData(DeleteTransactionsDataDTOInter DTDD) throws Throwable ;
}
