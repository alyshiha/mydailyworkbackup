/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.SwitchFileTemplateDTOInter;

/**
 *
 * @author Administrator
 */
public interface SwitchFileHeaderDAOInter {

    Object delete(Object... obj) throws Throwable;

    Object find(Object... obj) throws Throwable;

    Object findAll() throws Throwable;

    Object findByTemplate(SwitchFileTemplateDTOInter aftDTO) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
