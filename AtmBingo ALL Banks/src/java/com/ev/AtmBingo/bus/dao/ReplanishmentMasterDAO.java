/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.PrintcardtakenExcelInter;
import com.ev.AtmBingo.bus.dto.RepPopDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentDetailDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import com.ev.AtmBingo.bus.dto.replanishmentHistoryRepDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 * findLastDateForRep
 *
 * @author Administrator
 */
public class ReplanishmentMasterDAO extends BaseDAO implements ReplanishmentMasterDAOInter {

    protected ReplanishmentMasterDAO() {
        super();
        super.setTableName("replanishment_master");
    }

    public void save(List<ReplanishmentMasterDTOInter> entities) throws SQLException, Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement("update replanishment_master r set r.status = ? where r.id = ?");
            for (int i = 0; i < entities.size(); i++) {
                ReplanishmentMasterDTOInter uDTO = entities.get(i);
                String temp;
                if (uDTO.getRowstatus() == true) {
                    temp = "1";
                } else {
                    temp = "";
                }
                statement.setString(1, temp);
                statement.setInt(2, uDTO.getId());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ReplanishmentMasterDTOInter uDTO = (ReplanishmentMasterDTOInter) obj[0];
        uDTO.setId(super.generateSequence("replanishment"));
        String insertStat = "insert into $table (id, date_from, date_to, atm_id, created_by, created_date, comments,status,cards_taken) values ($id, to_date('$dateFrom','dd.MM.yyyy hh24:mi:ss')"
                + " , to_date('$datTo','dd.MM.yyyy hh24:mi:ss'), $atmId, $createdBy, sysdate,'$comment',$status,'$cardstaken')";
        Integer temp;
        if (uDTO.getRowstatus() == true) {
            temp = 1;
        } else {
            temp = null;
        }
        insertStat = insertStat.replace("$status", "" + temp);
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$atmId", "" + uDTO.getAtmId());
        insertStat = insertStat.replace("$createdBy", "" + uDTO.getCreatedBy());
        insertStat = insertStat.replace("$dateFrom", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getDateFrom()));
        insertStat = insertStat.replace("$datTo", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getDateTo()));
        insertStat = insertStat.replace("$comment", "" + uDTO.getComment());
        insertStat = insertStat.replace("$cardstaken", "" + uDTO.getCardstaken());

        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }
   public Object update2(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ReplanishmentMasterDTOInter uDTO = (ReplanishmentMasterDTOInter) obj[0];
        String updateStat = "update $table set cards_taken = '$cardstaken' ,date_from= to_date('$dateFrom','dd.MM.yyyy hh24:mi:ss'), date_to= to_date('$datTo','dd.MM.yyyy hh24:mi:ss'), atm_id = $atmId, created_by = $createdBy, "
                + "  created_date = to_date('$cTo','dd.MM.yyyy hh24:mi:ss'), comments='$comment'"
                + " , STATUS = $status , calc_flag = '' where id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$atmId", "" + uDTO.getAtmId());
        updateStat = updateStat.replace("$createdBy", "" + uDTO.getCreatedBy());
        updateStat = updateStat.replace("$dateFrom", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getDateFrom()));
        updateStat = updateStat.replace("$datTo", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getDateTo()));
        updateStat = updateStat.replace("$cTo", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getCreatedDate()));
        updateStat = updateStat.replace("$comment", "" + uDTO.getComment());
        updateStat = updateStat.replace("$cardstaken", "" + uDTO.getCardstaken());
        Integer temp;
        if (uDTO.getRowstatus() == true) {
            temp = 1;
        } else {
            temp = null;
        }
        updateStat = updateStat.replace("$status", "" + temp);
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);

        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ReplanishmentMasterDTOInter uDTO = (ReplanishmentMasterDTOInter) obj[0];
        String updateStat = "update $table set cards_taken = '$cardstaken' , atm_id = $atmId, created_by = $createdBy, "
                + "  created_date = to_date('$cTo','dd.MM.yyyy hh24:mi:ss'), comments='$comment'"
                + " , STATUS = $status , calc_flag = '' where id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$atmId", "" + uDTO.getAtmId());
        updateStat = updateStat.replace("$createdBy", "" + uDTO.getCreatedBy());
        updateStat = updateStat.replace("$dateFrom", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getDateFrom()));
        updateStat = updateStat.replace("$datTo", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getDateTo()));
        updateStat = updateStat.replace("$cTo", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getCreatedDate()));
        updateStat = updateStat.replace("$comment", "" + uDTO.getComment());
        updateStat = updateStat.replace("$cardstaken", "" + uDTO.getCardstaken());
        Integer temp;
        if (uDTO.getRowstatus() == true) {
            temp = 1;
        } else {
            temp = null;
        }
        updateStat = updateStat.replace("$status", "" + temp);
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);

        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ReplanishmentMasterDTOInter uDTO = (ReplanishmentMasterDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object recalc(ReplanishmentMasterDTOInter rep) throws Throwable {
        preCollable();
        CallableStatement stat = super.executeCallableStatment("{call replanishment.CLAC_PENDING_REPLANISHMENTS1(?,?,?)}");
        stat.setInt(1, rep.getId());
        stat.setString(2, "" + DateFormatter.changeDateAndTimeFormatprocedure(rep.getDateFrom()));
        stat.setString(3, "" + DateFormatter.changeDateAndTimeFormatprocedure(rep.getDateTo()));
        System.out.println(rep.getId() + " " + DateFormatter.changeDateAndTimeFormatprocedure(rep.getDateFrom()) + " " + DateFormatter.changeDateAndTimeFormat(rep.getDateTo()));
        stat.execute();
        postCollable(stat);
        return null;
    }

    public Object recalcall(int id) throws Throwable {
        preCollable();
        CallableStatement stat = super.executeCallableStatment("{call replanishment.UPDATE_REPLANISHMENT(?)}");
        stat.setInt(1, id);
        stat.execute();
        postCollable(stat);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        ReplanishmentMasterDTOInter uDTO = DTOFactory.createReplanishmentMasterDTO();
        while (rs.next()) {
            uDTO.setAtmId(rs.getInt("atm_id"));
            uDTO.setCreatedBy(rs.getInt("created_by"));
            uDTO.setCreatedDate(rs.getTimestamp("created_date"));
            uDTO.setDateFrom(rs.getTimestamp("date_from"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setDateTo(rs.getTimestamp("date_to"));
            uDTO.setComment(rs.getString("comments"));
            Integer temp = rs.getInt("status");
            if (temp == 1) {
                uDTO.setRowstatus(Boolean.TRUE);
            } else {
                uDTO.setRowstatus(Boolean.FALSE);
            }
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findLastDate(int atmId) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select to_date(to_char(max(date_to),'dd.mm.rrrr hh24:mi:ss'),'dd.mm.rrrr hh24:mi:ss') date_to from $table where atm_id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + atmId);
        ResultSet rs = executeQuery(selectStat);
        Date d = null;
        while (rs.next()) {

            d = rs.getTimestamp("date_to");

        }
        super.postSelect(rs);
        return d;
    }

    public Object findLastDateForRep(int atmId, Date dateF) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        ReplanishmentMasterDTOInter uDTO = DTOFactory.createReplanishmentMasterDTO();
        String selectStat = "select atm_id,date_from,id,date_to,comments"
                + " from("
                + " select m.id, m.date_from,m.date_to,m.atm_id,m.comments"
                + " from replanishment_master m "
                + " where atm_id = $id and date_from <to_date('$dateF','dd.mm.rrrr hh24:mi:ss')"
                + " order by  date_from desc) "
                + " where rownum = 1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + atmId);
        selectStat = selectStat.replace("$dateF", "" + DateFormatter.changeDateAndTimeFormat(dateF));
        ResultSet rs = executeQuery(selectStat);

        while (rs.next()) {
            uDTO.setAtmId(rs.getInt("atm_id"));
            uDTO.setDateFrom(rs.getTimestamp("date_from"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setDateTo(rs.getTimestamp("date_to"));
            uDTO.setComment(rs.getString("comments"));
            Integer temp = rs.getInt("status");
            if (temp == 1) {
                uDTO.setRowstatus(Boolean.TRUE);
            } else {
                uDTO.setRowstatus(Boolean.FALSE);
            }
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by created_date DESC";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<ReplanishmentMasterDTOInter> uDTOL = new ArrayList<ReplanishmentMasterDTOInter>();
        while (rs.next()) {
            ReplanishmentMasterDTOInter uDTO = DTOFactory.createReplanishmentMasterDTO();
            uDTO.setAtmId(rs.getInt("atm_id"));
            uDTO.setCreatedBy(rs.getInt("created_by"));
            uDTO.setCreatedDate(rs.getTimestamp("created_date"));
            uDTO.setDateFrom(rs.getTimestamp("date_from"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setDateTo(rs.getTimestamp("date_to"));
            uDTO.setComment(rs.getString("comments"));
            Integer temp = rs.getInt("status");
            if (temp == 1) {
                uDTO.setRowstatus(Boolean.TRUE);
            } else {
                uDTO.setRowstatus(Boolean.FALSE);
            }
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object customSearch(String whereClause, Integer uname) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select cards_taken,status,calc_flag,atm_id,created_by,created_date,date_from,id,date_to,comments, decode((select u.user_name from users u where u.user_id = created_by),null,'System',(select u.user_name from users u where u.user_id = created_by)) UName  from replanishment_master where  atm_id in (select a.atm_id from user_atm a where a.user_id = $username) and atm_valid(atm_id) = 1 and $whereClause";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$whereClause", "" + whereClause);
        selectStat = selectStat.replace("$username", "" + uname);
        ResultSet rs = executeQuery(selectStat);
        List<ReplanishmentMasterDTOInter> uDTOL = new ArrayList<ReplanishmentMasterDTOInter>();
        while (rs.next()) {
            ReplanishmentMasterDTOInter uDTO = DTOFactory.createReplanishmentMasterDTO();
            uDTO.setAtmId(rs.getInt("atm_id"));
            uDTO.setcalcflag(rs.getString("calc_flag"));
            uDTO.setUname(rs.getString("UName"));
            uDTO.setCreatedBy(rs.getInt("created_by"));
            uDTO.setCreatedDate(rs.getTimestamp("created_date"));
            uDTO.setDateFrom(rs.getTimestamp("date_from"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setCardstaken(rs.getInt("cards_taken"));

            if ("System".equals(uDTO.getUname())) {
                uDTO.setCardtakenstate(false);
            } else {
                uDTO.setCardtakenstate(true);
            }

            Integer temp = rs.getInt("status");
            if (temp == 1) {
                uDTO.setRowstatus(Boolean.TRUE);
            } else {
                uDTO.setRowstatus(Boolean.FALSE);
            }
            uDTO.setDateTo(rs.getTimestamp("date_to"));
            uDTO.setComment(rs.getString("comments"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object replanishmentHistoryRep(String whereClause) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select"
                + " (select 'ATM ID: '||a.application_id||'  -  '||' Name: '||a.name||'  - Unit : '||a.unit_number from atm_machine a where a.id = rm.atm_id) ATM,"
                + " rm.date_from,rm.date_to,"
                + " (select m.symbol from currency c,currency_master m where c.id =  rd.currency and m.id = c.id) currency,"
                + " sum(rd.total_amount) total_despense,sum(rd.remaining_amount) total_remaining,"
                + " sum(rd.cassete_value*(select c.amount from CASSETTE c where c.id = rd.cassete))  cash_feed,"
                + " (select"
                + /*+INDEX (matched_data REPLANISHMENT_INDEX)*/ " nvl(sum(amount * nvl(r.amount_type,0)),0)"
                + " from matched_data ,transaction_response_code r"
                + " where transaction_date between rm.date_from  and rm.date_to"
                + " and atm_id = rm.atm_id"
                + " and record_type = 2"
                + " and matching_type = (select id from matching_type where active = 1 and (type1 = 2 or type2=2) and rownum = 1)"
                + " and transaction_type_id in (select id from transaction_type where type_category = 1)"
                + " and currency_id = rd.currency"
                + " and response_code_id = r.id(+)) switch_amount,"
                + " (select"
                + " nvl(sum(m.amount * nvl(amount_type,0)),0)"
                + " from disputes m ,transaction_response_code r"
                + " where m.transaction_date between rm.date_from  and rm.date_to"
                + " and m.atm_id = rm.atm_id"
                + " and m.transaction_type_id in (select id from transaction_type where type_category = 1)"
                + " and record_type = 2"
                + " and matching_type = (select id from matching_type where active = 1 and (type1 = 2 or type2=2) and rownum = 1)"
                + " and currency_id = rd.currency"
                + " and m.response_code_id = r.id(+))dispute_amount"
                + " from replanishment_master rm,replanishment_detail rd"
                + " where rm.id = rd.id and"
                + " $whereClause"
                + " group by rm.date_from,rm.date_to,"
                + " rd.currency,rm.atm_id"
                + " order by atm,rm.date_from";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$whereClause", "" + whereClause);
        ResultSet rs = executeQueryReport(selectStat);
        super.postSelect();
        return rs;
    }

    public Object replanishmentCardTakedExcel(String whereClause) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select nvl(cards_taken,0) cardcount ,(select a.application_id from atm_machine a where a.id = atm_id) AtmApplicationID,date_from,Date_to from replanishment_master "
                + " where nvl(cards_taken,0) <> 0 and $whereClause";
        selectStat = selectStat.replace("$whereClause", "" + whereClause);
        ResultSet rs = executeQueryReport(selectStat);
        List<PrintcardtakenExcelInter> Records = new ArrayList<PrintcardtakenExcelInter>();
        while (rs.next()) {
            PrintcardtakenExcelInter record = DTOFactory.createprintcardtakenexcelinter();
            record.setAtmapplicationid(rs.getString("AtmApplicationID"));
            record.setCardCount(rs.getInt("cardcount"));
            record.setDatefrom(rs.getString("date_from"));
            record.setDateto(rs.getString("Date_to"));
            Records.add(record);
        }

        super.postSelect(rs);
        return Records;
    }

    public Object replanishmentCardTaked(String whereClause) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select nvl(cards_taken,0) cardcount ,(select a.application_id from atm_machine a where a.id = atm_id) AtmApplicationID,date_from ,Date_to from replanishment_master  "
                + " where nvl(cards_taken,0) <> 0 and $whereClause order by AtmApplicationID,date_from";
        selectStat = selectStat.replace("$whereClause", "" + whereClause);
        ResultSet rs = executeQueryReport(selectStat);
        super.postSelect();
        return rs;
    }

    public Object replanishmentHistoryRepexcel(String whereClause) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select "
                + " (select 'ATM ID: '||a.application_id||'  -  '||' Name: '||a.name||'  - Unit : '||a.unit_number from atm_machine a where a.id = rm.atm_id) ATM,"
                + " rm.date_from,rm.date_to,"
                + " (select m.symbol from currency c,currency_master m where c.id =  rd.currency and m.id = c.id) currency,"
                + " sum(rd.total_amount) total_despense,sum(rd.remaining_amount) total_remaining,"
                + " sum(rd.cassete_value*(select c.amount from CASSETTE c where c.id = rd.cassete))  cash_feed,"
                + " (select"
                + /*+INDEX (matched_data REPLANISHMENT_INDEX)*/ " nvl(sum(amount * nvl(r.amount_type,0)),0)"
                + " from matched_data ,transaction_response_code r"
                + " where transaction_date between rm.date_from  and rm.date_to"
                + " and atm_id = rm.atm_id"
                + " and record_type = 2"
                + " and matching_type = (select id from matching_type where active = 1 and (type1 = 2 or type2=2) and rownum = 1)"
                + " and transaction_type_id in (select id from transaction_type where type_category = 1)"
                + " and currency_id = rd.currency"
                + " and response_code_id = r.id(+)) switch_amount,"
                + " (select"
                + " nvl(sum(m.amount * nvl(amount_type,0)),0)"
                + " from disputes m ,transaction_response_code r"
                + " where m.transaction_date between rm.date_from  and rm.date_to"
                + " and m.atm_id = rm.atm_id"
                + " and m.transaction_type_id in (select id from transaction_type where type_category = 1)"
                + " and record_type = 2"
                + " and matching_type = (select id from matching_type where active = 1 and (type1 = 2 or type2=2) and rownum = 1)"
                + " and currency_id = rd.currency"
                + " and m.response_code_id = r.id(+))dispute_amount"
                + " from replanishment_master rm,replanishment_detail rd"
                + " where rm.id = rd.id and"
                + " $whereClause"
                + " group by rm.date_from,rm.date_to,"
                + " rd.currency,rm.atm_id"
                + " order by atm,rm.date_from";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$whereClause", "" + whereClause);
        ResultSet rs = executeQueryReport(selectStat);
        List<replanishmentHistoryRepDTOInter> Records = new ArrayList<replanishmentHistoryRepDTOInter>();
        while (rs.next()) {
            replanishmentHistoryRepDTOInter record = DTOFactory.createreplanishmentHistoryRepDTO();
            record.setAtm(rs.getString("ATM"));
            record.setFromdate(rs.getString("date_from"));
            record.setTodate(rs.getString("date_to"));
            record.setCurrency(rs.getString("currency"));
            record.setDispendedcash(rs.getString("total_despense"));
            record.setRemainingcash(rs.getString("total_remaining"));
            record.setCashfeed(rs.getString("cash_feed"));
            record.setSwitchcash(rs.getString("switch_amount"));
            record.setDisputecash(rs.getString("dispute_amount"));
            record.setDifferent(String.valueOf((Integer.valueOf(record.getSwitchcash()) + Integer.valueOf(record.getDisputecash())) - Integer.valueOf(record.getDispendedcash())));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findReport(int id) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where id in (select rep_id from replanishment_report where id = $id)";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        ResultSet rs = executeQuery(selectStat);
        super.postSelect();
        return rs;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public RepPopDTOInter replanishmentPopUp(ReplanishmentMasterDTOInter id) throws Throwable {
        super.preSelect();

        Integer rep2 = findRepid2(id);

        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select m.date_from,m.date_to,m.cassitte_value,m.switch,m.switch_diff,m.cassitte_diff,m.disputes,m.comments,\n"
                + "(select mm.date_from from replanishment_master mm where mm.id = $id )last_date_from,\n"
                + "(select mm.date_to from replanishment_master mm where mm.id =  $id )last_date_to,\n"
                + "(select r.cassitte_value from replanishment_master r where id =  $id ) cassitte_value2,\n"
                + "(select r.SWITCH from replanishment_master r where id = $id  ) switch_value2,\n"
                + "(select r.switch_diff from replanishment_master r where id =  $id  ) switch_diff2,\n"
                + "(select r.cassitte_diff from replanishment_master r where id = $id) cassitte_diff2,\n"
                + "(select r.disputes from replanishment_master r where id =  $id ) disputes2,\n"
                + "(select mm.comments from replanishment_master mm where mm.id =  $id )last_comment\n"
                + "from replanishment_master m\n"
                + "where m.id = $mid\n"
                + "order by (select application_id from atm_machine where id =m.atm_id),m.date_from";
        selectStat = selectStat.replace("$id", "" + rep2);
        selectStat = selectStat.replace("$mid", "" + id.getId());
        ResultSet rs = executeQueryReport(selectStat);

        RepPopDTOInter record = DTOFactory.createRepPopDTO();
        while (rs.next()) {
            record.setCurrentdatefrom(rs.getTimestamp("date_from"));
            record.setCurrentdateto(rs.getTimestamp("date_to"));
            record.setCurrentcassetvalue(rs.getLong("cassitte_value"));
            record.setCurrentcassetdiff(rs.getLong("cassitte_diff"));
            record.setCurrentswitchvalue(rs.getLong("switch"));
            record.setCurrentswitchdiff(rs.getLong("switch_diff"));
            record.setCurrentdispute(rs.getLong("disputes"));
            record.setCurrentcomment(rs.getString("comments"));

            record.setLastdatefrom(rs.getTimestamp("last_date_from"));
            record.setLastdateto(rs.getTimestamp("last_date_to"));
            record.setLastcassetvalue(rs.getLong("cassitte_value2"));
            record.setLastcassetdiff(rs.getLong("cassitte_diff2"));
            record.setLastswitchvalue(rs.getLong("switch_value2"));
            record.setLastswitchdiff(rs.getLong("switch_diff2"));
            record.setLastdispute(rs.getLong("disputes2"));
            record.setLastcomment(rs.getString("last_comment"));
        }

        super.postSelect(rs);
        return record;
    }

    public Integer findRepid2(ReplanishmentMasterDTOInter temp) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer result = 0;
        String selectStat = "select id\n"
                + "    from (select m.id\n"
                + "            from replanishment_master m\n"
                + "           where atm_id = $atmid\n"
                + "             and date_from < to_date('$datefrom','dd.MM.yyyy hh24:mi:ss')\n"
                + "           order by date_from desc)\n"
                + "   where rownum = 1";
        selectStat = selectStat.replace("$atmid", "" + temp.getAtmId());
        selectStat = selectStat.replace("$datefrom", "" + DateFormatter.changeDateAndTimeFormat(temp.getDateFrom()));
        ResultSet rs = executeQuery(selectStat);

        while (rs.next()) {
            result = rs.getInt("id");
        }
        super.postSelect(rs);
        return result;
    }

}
