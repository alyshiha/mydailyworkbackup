/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;

import com.ev.AtmBingo.bus.dto.AtmFileHeaderDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDetailDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DuplicateListDTOInter;
import com.ev.AtmBingo.bus.dto.FileColumnDefinitionDTOInter;
import com.ev.AtmBingo.bus.dto.FileColumnDisplayDTOInter;
import java.sql.PreparedStatement;
import java.util.HashSet;
import java.util.Set;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.swing.JPopupMenu.Separator;

/**
 *
 * @author Administrator
 */
public class AtmtemplateDAO extends BaseDAO implements AtmtemplateDAOInter {

    protected AtmtemplateDAO() {
        super();
    }

    public int findColidHeader(AtmFileHeaderDTOInter aftDTO, String TName) throws Throwable {
        super.preSelect();
        int ColId = 0000;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select id from $table where template = $template and column_id = $columnid";
        if (TName.toUpperCase().indexOf("_HEADER") == -1) {
            TName = TName + "_HEADER";
        }
        selectStat = selectStat.replace("$table", "" + TName);
        selectStat = selectStat.replace("$template", "" + aftDTO.getTemplate());
        selectStat = selectStat.replace("$columnid", "" + aftDTO.getColumnId());
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            ColId = rs.getInt("id");
        }
        super.postSelect(rs);
        return ColId;
    }

    public int findColid(AtmFileTemplateDetailDTOInter aftDTO, String TName) throws Throwable {
        super.preSelect();
        int ColId = 0000;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select id from $table where template = $template and column_id = $columnid";
        if (TName.toUpperCase().indexOf("_DETAIL") == -1) {
            TName = TName + "_DETAIL";
        }
        selectStat = selectStat.replace("$table", "" + TName);
        selectStat = selectStat.replace("$template", "" + aftDTO.getTwmplate());
        selectStat = selectStat.replace("$columnid", "" + aftDTO.getColumnId());
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            ColId = rs.getInt("id");
        }
        super.postSelect(rs);
        return ColId;
    }

    public Object findByTemplateDetail(AtmFileTemplateDTOInter aftDTO, String TName) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where template = $template";
        if (TName.toUpperCase().indexOf("_DETAIL") == -1) {
            TName = TName + "_DETAIL";
        }
        selectStat = selectStat.replace("$table", "" + TName);
        selectStat = selectStat.replace("$template", "" + aftDTO.getId());
        ResultSet rs = executeQuery(selectStat);
        List<AtmFileTemplateDetailDTOInter> uDTOL = new ArrayList<AtmFileTemplateDetailDTOInter>();
        while (rs.next()) {
            AtmFileTemplateDetailDTOInter uDTO = DTOFactory.createAtmFileTemplateDetailDTO();
            uDTO.setTagsorder(rs.getInt("tags_order"));
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getBigDecimal("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getBigDecimal("length"));
            uDTO.setLengthDir(rs.getBigDecimal("length_dir"));
            uDTO.setLineNumber(rs.getBigDecimal("line_number"));
            uDTO.setLoadWhenMapped(rs.getInt("load_when_mapped"));
            uDTO.setTagstring(rs.getString("TAG_STRING"));
            uDTO.setRemovestring(rs.getString("remove_string"));
            Integer temp = rs.getInt("MANDATORY");
            if (temp == 1) {
                uDTO.setMandatory(Boolean.TRUE);
            } else {
                uDTO.setMandatory(Boolean.FALSE);
            }
            Integer temp2 = rs.getInt("NEGATIVE_AMOUNT_FLAG");
            if (temp2 == 1) {
                uDTO.setNegativeflag(Boolean.TRUE);
            } else {
                uDTO.setNegativeflag(Boolean.FALSE);
            }
            uDTO.setPosition(rs.getBigDecimal("position"));
            uDTO.setTwmplate(rs.getInt("template"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));

            Integer temp3 = rs.getInt("ADD_DECIMAL");
            if (temp3 == 1) {
                uDTO.setDecimalpoint(Boolean.TRUE);
            } else {
                uDTO.setDecimalpoint(Boolean.FALSE);
            }
            uDTO.setDecimalpointposition(rs.getString("DECIMAL_POS"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByTemplate(AtmFileTemplateDTOInter aftDTO, String TName) throws Throwable {
        super.preSelect();
        String temp = TName.toUpperCase();
        if (temp.indexOf("_HEADER") == -1) {
            TName = TName + "_HEADER";
        }

        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select h.* from $table h,file_column_definition f where h.template = $template and f.id = h.column_id and upper(f.column_name) in ('ATM_ID','TRANSACTION_SEQUENCE','CURRENCY','TRANSACTION_STATUS','TRANSACTION_TYPE','RESPONSE_CODE','TRANSACTION_DATE','SETTLEMENT_DATE','CARD_NO','AMOUNT','CUSTOMER_ACCOUNT_NUMBER','CUSTOMER_ACCOUNT_NUMBER','TRANSACTION_TIME','COLUMN1','COLUMN2','COLUMN3','COLUMN4','COLUMN5','CARD_NO_SUFFIX')";

        selectStat = selectStat.replace("$table", "" + TName);
        selectStat = selectStat.replace("$template", "" + aftDTO.getId());
        ResultSet rs = executeQuery(selectStat);
        List<AtmFileHeaderDTOInter> uDTOL = new ArrayList<AtmFileHeaderDTOInter>();
        while (rs.next()) {
            AtmFileHeaderDTOInter uDTO = DTOFactory.createAtmFileHeaderDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getInt("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormattwo(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getString("length"));
            uDTO.setPosition(rs.getString("position"));
            uDTO.setTemplate(rs.getInt("template"));
            uDTO.setStatus("Update");
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findAllColDef() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from FILE_COLUMN_DEFINITION ";
        ResultSet result = executeQuery(selectStat);
        List<FileColumnDefinitionDTOInter> mtL = new ArrayList<FileColumnDefinitionDTOInter>();
        while (result.next()) {
            FileColumnDefinitionDTOInter mt = DTOFactory.createFileColumnDefinitionDTO();
            mt.setId(result.getInt("id"));
            mt.setColumnName(result.getString("column_name"));
            mt.setName(result.getString("name"));
            mt.setDataType(result.getInt("data_type"));
            //mt.setProperty(result.getString("property"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object findAllColumns() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from FILE_COLUMN_DEFINITION where upper(column_name) in ('ATM_ID','TRANSACTION_SEQUENCE','CURRENCY','TRANSACTION_STATUS','TRANSACTION_TYPE','RESPONSE_CODE','TRANSACTION_DATE','SETTLEMENT_DATE','CARD_NO','AMOUNT','CUSTOMER_ACCOUNT_NUMBER','CUSTOMER_ACCOUNT_NUMBER','TRANSACTION_TIME','COLUMN1','COLUMN2','COLUMN3','COLUMN4','COLUMN5','CARD_NO_SUFFIX')";
        ResultSet rs = executeQuery(selectStat);
        List<FileColumnDisplayDTOInter> uDTOL = new ArrayList<FileColumnDisplayDTOInter>();
        while (rs.next()) {
            FileColumnDisplayDTOInter uDTO = DTOFactory.createFileColumnDisplayDTO();
            FileColumnDefinitionDTOInter fcdDTO = DTOFactory.createFileColumnDefinitionDTO();
            fcdDTO.setId(rs.getInt("id"));
            uDTO.setColumnId(fcdDTO);
            uDTO.setId(rs.getInt("id"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findAllMachineType(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from atm_machine_type order by name";
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineTypeDTOInter> mtL = new ArrayList<AtmMachineTypeDTOInter>();
        while (result.next()) {
            AtmMachineTypeDTOInter mt = DTOFactory.createAtmMachineTypeDTO();
            mt.setId(result.getInt("id"));
            mt.setName(result.getString("name"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object findAllTemplates(String Table) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select $table.*,reset_tags_starting,decode($table.separator,chr(60),'TAB',$table.separator)separatornew from $table order by name";
        selectStat = selectStat.replace("$table", "" + Table);
        ResultSet rs = executeQuery(selectStat);
        List<AtmFileTemplateDTOInter> uDTOL = new ArrayList<AtmFileTemplateDTOInter>();
        while (rs.next()) {
            AtmFileTemplateDTOInter uDTO = DTOFactory.createAtmFileTemplateDTO();
            uDTO.setActive(rs.getInt("active"));
            uDTO.setResettagsstarting(rs.getInt("reset_tags_starting"));
            uDTO.setTagendingvalue(rs.getString("TAGS_ENDING_VALUE"));
            uDTO.setTagendingformat(rs.getString("TAGS_ENDING_FORMAT"));
            uDTO.setTagendingdatatype(rs.getInt("TAGS_ENDING_DATA_TYPE"));
            uDTO.setTagendingposition(rs.getInt("TAGS_ENDING_POSITION"));
            uDTO.setTagendinglength(rs.getInt("TAGS_ENDING_LENGTH"));
            uDTO.setBuckupFolder(rs.getString("backup_folder"));
            uDTO.setCopyFolder(rs.getString("copy_folder"));
            uDTO.setDateSeparator(rs.getString("date_separator"));
            uDTO.setDateSeparatorPos1(rs.getBigDecimal("date_separator_pos1"));
            uDTO.setDateSeparatorPos2(rs.getBigDecimal("date_separator_pos2"));
            uDTO.setHeaderDataType(rs.getBigDecimal("header_data_type"));
            uDTO.setHeaderDateSeparator(rs.getString("header_date_separator"));
            uDTO.setHeaderDateSeparatorPos1(rs.getBigDecimal("header_date_separator_pos1"));
            uDTO.setHeaderDateSeparatorPos2(rs.getBigDecimal("header_date_separator_pos2"));
            uDTO.setHeaderFormat(rs.getString("header_format"));
            uDTO.setHeaderLength(rs.getBigDecimal("header_length"));
            uDTO.setHeaderPosition(rs.getString("header_position"));
            uDTO.setHeaderString(rs.getString("header_string"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setIgnoredLines(rs.getBigDecimal("ignored_lines"));
            uDTO.setLoadingFolder(rs.getString("loading_folder"));
            uDTO.setName(rs.getString("name"));
            uDTO.setNumberOfLines(rs.getBigDecimal("number_of_lines"));
            uDTO.setProcessingType(rs.getBigDecimal("processing_type"));
            uDTO.setSeparator(rs.getString("separatornew"));
            uDTO.setServerFolder(rs.getString("server_folder"));
            uDTO.setStartingDataType(rs.getBigDecimal("starting_data_type"));
            uDTO.setStartingFormat(rs.getString("starting_format"));
            uDTO.setStartingLength(rs.getBigDecimal("starting_length"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));
            uDTO.setStartingValue(rs.getString("starting_value"));
            if (Table.equals("ATM_FILE_TEMPLATE")) {
                uDTO.setMachineType(rs.getBigDecimal("atm_machine_type"));

            }
            uDTO.setAtmadd(rs.getString("atm_add"));
            uDTO.setHeaderString(rs.getString("header_string"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public String ValidateNullDeleteTemplate(Object... obj) {
        AtmFileTemplateDTOInter RecordToInsert = (AtmFileTemplateDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getId() == 0) {
            Validate = Validate + " Feild id is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object deleterecordTemplate(Object... obj) throws Throwable {
        super.preUpdate();
        AtmFileTemplateDTOInter RecordToDelete = (AtmFileTemplateDTOInter) obj[0];
        String TName = (String) obj[1];
        String msg = ValidateNullDeleteTemplate(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  ID= $id";
            deleteStat = deleteStat.replace("$table", "" + TName);
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getId());
            msg = super.executeUpdate(deleteStat).toString();
            msg = msg + " Template Has Been deleted";
            super.postUpdate("Delete Record In Table " + TName, false);
        }
        return msg;
    }

    public String ValidateNullTemplate(Object... obj) {
        AtmFileTemplateDTOInter RecordToInsert = (AtmFileTemplateDTOInter) obj[0];
        String TName = (String) obj[1];
        String Validate = "Validate";
        if (RecordToInsert.getId() == 0) {
            Validate = Validate + " Feild id is a requiered field";
        }
        if (RecordToInsert.getName() == null || RecordToInsert.getName().equals("")) {
            Validate = Validate + " Feild name is a requiered field";
        }
        if (RecordToInsert.getProcessingType() == null) {
            Validate = Validate + " Feild processingtype is a requiered field";
        }
        if (RecordToInsert.getNumberOfLines() == null) {
            Validate = Validate + " Feild numberoflines is a requiered field";
        }
        if (RecordToInsert.getLoadingFolder() == null || RecordToInsert.getLoadingFolder().equals("")) {
            Validate = Validate + " Feild loadingfolder is a requiered field";
        }
        if (RecordToInsert.getBuckupFolder() == null || RecordToInsert.getBuckupFolder().equals("")) {
            Validate = Validate + " Feild backupfolder is a requiered field";
        }
        if (RecordToInsert.getCopyFolder() == null || RecordToInsert.getCopyFolder().equals("")) {
            Validate = Validate + " Feild copyfolder is a requiered field";
        }
        if (RecordToInsert.getIgnoredLines() == null) {
            Validate = Validate + " Feild ignoredlines is a requiered field";
        }
        if (RecordToInsert.getActive() == 0) {
            Validate = Validate + " Feild active is a requiered field";
        }
        if (TName.equals("ATM_FILE_TEMPLATE")) {
            if (RecordToInsert.getMachineType() == null) {
                Validate = Validate + " Feild atmmachinetype is a requiered field";
            }
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileTemplateDTOInter uDTO = (AtmFileTemplateDTOInter) obj[0];
        String TName = (String) obj[1];
        String msg = ValidateNullTemplate(uDTO, TName);
        String sepStr = "";
        if (msg.equals("Validate")) {
            String tab = "";
            if ("tab".equals(uDTO.getSeparator().toLowerCase())) {
                tab = "chr(60)";
                sepStr = tab;
            } else {
                sepStr = "'" + uDTO.getSeparator() + "'";
            }
            String updateStat = "update $table set  name = '$name', processing_type = '$processingType'"
                    + ", number_of_lines = '$numberOfLines', loading_folder = '$lodingFolder', starting_data_type = '$startingDataType'"
                    + ", starting_format = '$startingFormat', starting_position = '$startingPosition', backup_folder = '$buckupFolder'"
                    + ", copy_folder = '$copyFolder', ignored_lines = '$ignoredLines', starting_length = '$startingLength'"
                    + ", date_separator = '$dateSparator', date_separator_pos1 = '$dSeparatorPos1', date_separator_pos2 = '$dateSeparatorPos2'"
                    + ", server_folder = '$serverFolder', header_data_type = $headerDataType, header_format = '$headerFormat'"
                    + ", header_position = '$headerPosition', header_length = '$headerLength', header_date_separator = '$headerDateSparator'"
                    + ", header_date_separator_pos1 = '$hDateSeparatorPos1', header_date_separator_pos2 = '$headerDateSeparatorPos2'"
                    + ", starting_value = '$startingValue', active = $active, separator = $separator ,atm_add = '$atm_add',reset_tags_starting = '$resettagsstarting',";
            if (TName.equals("ATM_FILE_TEMPLATE")) {
                updateStat = updateStat + "atm_machine_type = $machineType, ";
            }
            updateStat = updateStat + "TAGS_ENDING_VALUE='$TAGSENDINGVALUE',TAGS_ENDING_LENGTH='$TAGSENDINGLENGTH',TAGS_ENDING_POSITION= '$TAGSENDINGPOSITION' "
                    + " ,TAGS_ENDING_FORMAT='$TAGSENDINGFORMAT',TAGS_ENDING_DATA_TYPE='$TAGSENDINGDATATYPE',header_string='$headerString'"
                    + " where id = $id";
            updateStat = updateStat.replace("$resettagsstarting", "" + uDTO.getResettagsstarting());
            updateStat = updateStat.replace("$table", "" + TName);
            updateStat = updateStat.replace("$id", "" + uDTO.getId());
            updateStat = updateStat.replace("$atm_add", "" + uDTO.getAtmadd());
            updateStat = updateStat.replace("$TAGSENDINGVALUE", "" + uDTO.getTagendingvalue());
            updateStat = updateStat.replace("$TAGSENDINGFORMAT", "" + uDTO.getTagendingformat());
            updateStat = updateStat.replace("$TAGSENDINGDATATYPE", "" + uDTO.getTagendingdatatype());
            updateStat = updateStat.replace("$TAGSENDINGLENGTH", "" + uDTO.getTagendinglength());
            updateStat = updateStat.replace("$TAGSENDINGPOSITION", "" + uDTO.getTagendingposition());
            updateStat = updateStat.replace("$name", "" + uDTO.getName());
            updateStat = updateStat.replace("$processingType", "" + uDTO.getProcessingType());
            updateStat = updateStat.replace("$numberOfLines", "" + uDTO.getNumberOfLines());
            updateStat = updateStat.replace("$lodingFolder", "" + uDTO.getLoadingFolder());
            updateStat = updateStat.replace("$startingDataType", "" + uDTO.getStartingDataType());
            updateStat = updateStat.replace("$headerString", "" + uDTO.getHeaderString());
            updateStat = updateStat.replace("$startingFormat", "" + uDTO.getStartingFormat());
            updateStat = updateStat.replace("$startingPosition", "" + uDTO.getStartingPosition());
            updateStat = updateStat.replace("$buckupFolder", "" + uDTO.getBuckupFolder());
            updateStat = updateStat.replace("$copyFolder", "" + uDTO.getCopyFolder());
            updateStat = updateStat.replace("$ignoredLines", "" + uDTO.getIgnoredLines());
            updateStat = updateStat.replace("$startingLength", "" + uDTO.getStartingLength());
            updateStat = updateStat.replace("$dateSparator", "" + uDTO.getDateSeparator());
            updateStat = updateStat.replace("$dSeparatorPos1", "" + uDTO.getDateSeparatorPos1());
            updateStat = updateStat.replace("$dateSeparatorPos2", "" + uDTO.getDateSeparatorPos2());
            updateStat = updateStat.replace("$serverFolder", "" + uDTO.getServerFolder());
            updateStat = updateStat.replace("$headerDataType", "" + uDTO.getHeaderDataType());
            updateStat = updateStat.replace("$headerFormat", "" + uDTO.getHeaderFormat());
            updateStat = updateStat.replace("$headerPosition", "" + uDTO.getHeaderPosition());
            updateStat = updateStat.replace("$headerLength", "" + uDTO.getHeaderLength());
            updateStat = updateStat.replace("$headerDateSparator", "" + uDTO.getHeaderDateSeparator());
            updateStat = updateStat.replace("$hDateSeparatorPos1", "" + uDTO.getHeaderDateSeparatorPos1());
            updateStat = updateStat.replace("$headerDateSeparatorPos2", "" + uDTO.getHeaderDateSeparatorPos2());
            updateStat = updateStat.replace("$startingValue", "" + uDTO.getStartingValue());
            updateStat = updateStat.replace("$active", "" + uDTO.getActive());
            updateStat = updateStat.replace("$separator", "" + sepStr);
            updateStat = updateStat.replace("$machineType", "" + uDTO.getMachineType());
            updateStat = updateStat.replace("null", "");

            msg = super.executeUpdate(updateStat).toString();
            msg = msg + " Template Has Been updated";
            super.postUpdate("Update Record In Table ATM_FILE_TEMPLATE_DETAIL", false);
        }
        ;
        return msg;
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileTemplateDTOInter uDTO = (AtmFileTemplateDTOInter) obj[0];
        String TName = (String) obj[1];
        uDTO.setId(super.generateSequence(TName));
        String msg = ValidateNullTemplate(uDTO, TName);
        String sepStr = "";
        if (msg.equals("Validate")) {

            String insertStat = "";
            String tab = "";
            if ("tab".equals(uDTO.getSeparator().toLowerCase())) {
                tab = "chr(60)";
                sepStr = tab;
            } else {
                sepStr = "'" + uDTO.getSeparator() + "'";
            }
            if (TName.equals("SWITCH_FILE_TEMPLATE")) {
                insertStat = "insert into switch_file_template"
                        + "(id, name, processing_type, number_of_lines, loading_folder, starting_data_type, starting_format, starting_position, backup_folder, copy_folder, ignored_lines, starting_length, date_separator, date_separator_pos1, date_separator_pos2, server_folder, header_data_type, header_format, header_position, header_length, header_date_separator, header_date_separator_pos1, header_date_separator_pos2, header_string, starting_value, active, separator, tags_ending_data_type, tags_ending_format, tags_ending_position, tags_ending_length, tags_ending_value, atm_add,reset_tags_starting)"
                        + "values"
                        + "('$id', '$name', '$processingType', '$numberOfLines', '$loadingFolder', '$sstartingDataType', '$startingFormat', '$strtingPosition', '$buckupFolder', '$copyFolder', '$ignoredLines', '$strtngLength', '$dateSeparator', '$dSsPos1', '$dSPos2', '$serverFolder', '$headerDataType', '$headerFormat', '$headerPosition', '$headerLength', '$headerDateSeparator', '$hDSPos1', '$hDSPs2', '$headerString', '$startingValue', '$active', $separator, '$TAGSENDINGDATATYPE', '$TAGSENDINGFORMAT', '$TAGSENDINGPOSITION', '$TAGSENDINGLENGTH', '$TAGSENDINGVALUE', '$atmadd','$resettagsstarting')";
            }
            if (TName.equals("HOST_FILE_TEMPLATE")) {
                insertStat = "insert into host_file_template"
                        + "(id, name, processing_type, number_of_lines, loading_folder, starting_data_type, starting_format, starting_position, backup_folder, copy_folder, ignored_lines, starting_length, date_separator, date_separator_pos1, date_separator_pos2, server_folder, header_data_type, header_format, header_position, header_length, header_date_separator, header_date_separator_pos1, header_date_separator_pos2, header_string, starting_value, active, separator, tags_ending_data_type, tags_ending_format, tags_ending_position, tags_ending_length, tags_ending_value, atm_add,reset_tags_starting)"
                        + "values"
                        + "('$id', '$name', '$processingType', '$numberOfLines', '$loadingFolder', '$sstartingDataType', '$startingFormat', '$strtingPosition', '$buckupFolder', '$copyFolder', '$ignoredLines', '$strtngLength', '$dateSeparator', '$dSsPos1', '$dSPos2', '$serverFolder', '$headerDataType', '$headerFormat', '$headerPosition', '$headerLength', '$headerDateSeparator', '$hDSPos1', '$hDSPs2', '$headerString', '$startingValue', '$active', $separator, '$TAGSENDINGDATATYPE', '$TAGSENDINGFORMAT', '$TAGSENDINGPOSITION', '$TAGSENDINGLENGTH', '$TAGSENDINGVALUE', '$atmadd','$resettagsstarting')";
            }
            if (TName.equals("ATM_FILE_TEMPLATE")) {
                insertStat = "insert into atm_file_template"
                        + "(id, name, processing_type, number_of_lines, loading_folder, starting_data_type, starting_format, starting_position, backup_folder, copy_folder, ignored_lines, starting_length, date_separator, date_separator_pos1, date_separator_pos2, server_folder, header_data_type, header_format, header_position, header_length, header_date_separator, header_date_separator_pos1, header_date_separator_pos2, header_string, starting_value, active, separator, atm_machine_type, atm_add, tags_ending_data_type, tags_ending_format, tags_ending_position, tags_ending_length, tags_ending_value,reset_tags_starting)"
                        + "values"
                        + "('$id', '$name', '$processingType', '$numberOfLines', '$loadingFolder', '$sstartingDataType', '$startingFormat', '$strtingPosition', '$buckupFolder', '$copyFolder', '$ignoredLines', '$strtngLength', '$dateSeparator', '$dSsPos1', '$dSPos2', '$serverFolder', '$headerDataType', '$headerFormat', '$headerPosition', '$headerLength', '$headerDateSeparator', '$hDSPos1', '$hDSPs2', '$headerString', '$startingValue', '$active', $separator,'$machineType','$atmadd', '$TAGSENDINGDATATYPE', '$TAGSENDINGFORMAT', '$TAGSENDINGPOSITION', '$TAGSENDINGLENGTH', '$TAGSENDINGVALUE','$resettagsstarting')";
            }

            insertStat = insertStat.replace("$resettagsstarting", "" + uDTO.getResettagsstarting());
            insertStat = insertStat.replace("$table", "" + TName);
            insertStat = insertStat.replace("$id", "" + uDTO.getId());
            insertStat = insertStat.replace("$atmadd", "" + uDTO.getAtmadd());
            insertStat = insertStat.replace("$TAGSENDINGVALUE", "" + uDTO.getTagendingvalue());
            insertStat = insertStat.replace("$TAGSENDINGFORMAT", "" + uDTO.getTagendingformat());
            insertStat = insertStat.replace("$TAGSENDINGDATATYPE", "" + uDTO.getTagendingdatatype());
            insertStat = insertStat.replace("$TAGSENDINGLENGTH", "" + uDTO.getTagendinglength());
            insertStat = insertStat.replace("$TAGSENDINGPOSITION", "" + uDTO.getTagendingposition());
            insertStat = insertStat.replace("$name", "" + uDTO.getName());
            insertStat = insertStat.replace("$processingType", "" + uDTO.getProcessingType());
            insertStat = insertStat.replace("$numberOfLines", "" + uDTO.getNumberOfLines());
            insertStat = insertStat.replace("$loadingFolder", "" + uDTO.getLoadingFolder());
            insertStat = insertStat.replace("$sstartingDataType", "" + uDTO.getStartingDataType());
            insertStat = insertStat.replace("$startingFormat", "" + uDTO.getStartingFormat());
            insertStat = insertStat.replace("$strtingPosition", "" + uDTO.getStartingPosition());
            insertStat = insertStat.replace("$buckupFolder", "" + uDTO.getBuckupFolder());
            insertStat = insertStat.replace("$copyFolder", "" + uDTO.getCopyFolder());
            insertStat = insertStat.replace("$ignoredLines", "" + uDTO.getIgnoredLines());
            insertStat = insertStat.replace("$strtngLength", "" + uDTO.getStartingLength());
            insertStat = insertStat.replace("$dateSeparator", "" + uDTO.getDateSeparator());
            insertStat = insertStat.replace("$dSsPos1", "" + uDTO.getDateSeparatorPos1());
            insertStat = insertStat.replace("$dSPos2", "" + uDTO.getDateSeparatorPos2());
            insertStat = insertStat.replace("$serverFolder", "" + uDTO.getServerFolder());
            insertStat = insertStat.replace("$headerDataType", "" + uDTO.getHeaderDataType());
            insertStat = insertStat.replace("$headerFormat", "" + uDTO.getHeaderFormat());
            insertStat = insertStat.replace("$headerPosition", "" + uDTO.getHeaderPosition());
            insertStat = insertStat.replace("$headerLength", "" + uDTO.getHeaderLength());
            insertStat = insertStat.replace("$headerDateSeparator", "" + uDTO.getHeaderDateSeparator());
            insertStat = insertStat.replace("$hDSPos1", "" + uDTO.getHeaderDateSeparatorPos1());
            insertStat = insertStat.replace("$hDSPs2", "" + uDTO.getHeaderDateSeparatorPos2());
            insertStat = insertStat.replace("$headerString", "" + uDTO.getHeaderString());
            insertStat = insertStat.replace("$startingValue", "" + uDTO.getStartingValue());
            insertStat = insertStat.replace("$active", "" + uDTO.getActive());
            insertStat = insertStat.replace("$separator", "" + sepStr);
            insertStat = insertStat.replace("$machineType", "" + uDTO.getMachineType());
            insertStat = insertStat.replace("null", "");
            msg = super.executeUpdate(insertStat).toString();
            msg = msg + " Template Has Been Inserted";
            super.postUpdate("Add New Record To " + TName, false);
        }
        return msg;
    }

    public String ValidateNullDeleteTemplateAllHeaderID(Object... obj) {
        AtmFileTemplateDTOInter RecordToInsert = (AtmFileTemplateDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getId() == 0) {
            Validate = Validate + " Feild Template id is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object deleterecordHeaderByTemplateID(Object... obj) throws Throwable {
        super.preUpdate();
        AtmFileTemplateDTOInter RecordToDelete = (AtmFileTemplateDTOInter) obj[0];
        String TName = (String) obj[1];
        String msg = ValidateNullDeleteTemplateAllHeaderID(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table_HEADER "
                    + "  where  template = $id";
            deleteStat = deleteStat.replace("$table", "" + TName);
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getId());
            msg = super.executeUpdate(deleteStat).toString();
            msg = msg + " Header Has Been deleted";
            super.postUpdate("Delete Record In Table " + TName + "_HEADER", false);
        }
        return msg;
    }

    public String ValidateNullDeleteHeaderVal(Object... obj) {
        AtmFileHeaderDTOInter RecordToInsert = (AtmFileHeaderDTOInter) obj[0];
        String Validate = "Validate";
        return Validate;
    }

    public Object deleterecordHeader(Object... obj) throws Throwable {
        AtmFileHeaderDTOInter RecordToDelete = (AtmFileHeaderDTOInter) obj[0];
        String TName = (String) obj[1];
        List<FileColumnDefinitionDTOInter> ColumnList = (List<FileColumnDefinitionDTOInter>) findAllColumnsList();
        List<DuplicateListDTOInter> DupList = (List<DuplicateListDTOInter>) CreateDuplicateList(ColumnList);
        int[] Keys = HaveDuplicate(DupList, RecordToDelete.getColumnId());
        int col = Keys[0];
        int dup = Keys[1];
        int dup2 = Keys[2];
        int DeleteTotal = 0;
        if (col != 0) {
            RecordToDelete.setColumnId(col);
            DeleteTotal = deleteheader(RecordToDelete, TName) + DeleteTotal;
        }
        if (col == 0) {
            DeleteTotal = deleteheader(RecordToDelete, TName) + DeleteTotal;
        }
        if (dup != 0) {
            RecordToDelete.setColumnId(dup);
            DeleteTotal = deleteheader(RecordToDelete, TName) + DeleteTotal;
        }
        if (dup2 != 0) {
            RecordToDelete.setColumnId(dup2);
            DeleteTotal = deleteheader(RecordToDelete, TName) + DeleteTotal;
        }
        return DeleteTotal + " Row Has Been deleted";
    }

    public int deleteheader(Object... obj) throws Throwable {
        super.preUpdate();
        AtmFileHeaderDTOInter RecordToDelete = (AtmFileHeaderDTOInter) obj[0];
        int msg = 0;
        if (ValidateNullDeleteHeaderval(RecordToDelete).equals("Validate")) {
            String TName = (String) obj[1];
            String deleteStat = "delete from $table_HEADER "
                    + "  where  column_id = $id and template = $template";
            deleteStat = deleteStat.replace("$table", "" + TName);
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getColumnId());
            deleteStat = deleteStat.replace("$template", "" + RecordToDelete.getTemplate());
            msg = super.executeUpdate(deleteStat);
            super.postUpdate("Delete Record In Table " + TName + "_HEADER", false);
            return msg;
        } else {
            return msg;
        }
    }

    public Object deleteExtrarecordHeader(Object... obj) throws Throwable {
        super.preUpdate();
        Integer TemplateID = (Integer) obj[0];
        String TName = (String) obj[1];
        String ColIDS = (String) obj[2];
        Integer msg;
        String deleteStat = "delete from $table_HEADER "
                + "  where  template = '$tempid' and column_id not in ( $colids )";
        deleteStat = deleteStat.replace("$table", "" + TName);
        deleteStat = deleteStat.replace("$tempid", "" + TemplateID);
        deleteStat = deleteStat.replace("$colids", "" + ColIDS);
        msg = super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table " + TName + "_HEADER", false);
        return msg;
    }

    public String ValidateNullUpdateHeaderVal(Object... obj) {
        AtmFileHeaderDTOInter RecordToInsert = (AtmFileHeaderDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getTemplate() == 0) {
            Validate = Validate + " Feild template is a requiered field";
        }
        if (RecordToInsert.getColumnId() == 0) {
            Validate = Validate + " Feild columnid is a requiered field";
        }
        RecordToInsert.setPosition(RecordToInsert.getPosition().replace(" ", ""));
        if ((RecordToInsert.getPosition() == null ? "" == null : RecordToInsert.getPosition().equals("")) || RecordToInsert.getPosition() == null) {
            Validate = Validate + " Feild position is a requiered field";
        }
        if ((RecordToInsert.getLength() == null ? "" == null : RecordToInsert.getLength().equals("")) || RecordToInsert.getLength() == null) {
            Validate = Validate + " Feild Length is a requiered field";
        }
        if (RecordToInsert.getDataType() == null || (RecordToInsert.getDataType().toString() == null ? "null" == null : RecordToInsert.getDataType().toString().equals("null"))) {
            Validate = Validate + " Feild datatype is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object updaterecordHeader(Object... obj) throws Throwable {
        super.preUpdate();
        AtmFileHeaderDTOInter RecordToUpdate = (AtmFileHeaderDTOInter) obj[0];
        String TName = (String) obj[1];
        Integer msg = 0;
        String UpdateStat = "update $table_HEADER set "
                + "TEMPLATE= $template,COLUMN_ID= $columnid,POSITION= '$position',FORMAT= '$format',FORMAT2= '$format2',DATA_TYPE= $datatype,LENGTH= '$length'"
                + "  where  ID= $id";
        UpdateStat = UpdateStat.replace("$table", "" + TName);
        UpdateStat = UpdateStat.replace("$id", "" + RecordToUpdate.getId());
        UpdateStat = UpdateStat.replace("$template", "" + RecordToUpdate.getTemplate());
        UpdateStat = UpdateStat.replace("$columnid", "" + RecordToUpdate.getColumnId());
        UpdateStat = UpdateStat.replace("$position", "" + RecordToUpdate.getPosition());
        UpdateStat = UpdateStat.replace("$format2", "" + RecordToUpdate.getFormattwo());
        UpdateStat = UpdateStat.replace("$format", "" + RecordToUpdate.getFormat());

        UpdateStat = UpdateStat.replace("$datatype", "" + RecordToUpdate.getDataType());
        UpdateStat = UpdateStat.replace("$length", "" + RecordToUpdate.getLength());

        msg = super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table " + TName + "_HEADER", false);

        return msg;
    }

    public Object insertrecordHeader(Object... obj) throws Throwable {
        super.preUpdate();
        AtmFileHeaderDTOInter RecordToInsert = (AtmFileHeaderDTOInter) obj[0];
        String TableName = (String) obj[1];
        TableName = TableName.toUpperCase();
        if (TableName.toUpperCase().equals("SWITCH_FILE_TEMPLATE")) {
            RecordToInsert.setId(super.generateSequence("SWITCH_FILE_TEMPLATE_HDR"));
        } else if (TableName.toUpperCase().equals("HOST_FILE_TEMPLATE")) {
            RecordToInsert.setId(super.generateSequence("HOST_FILE_TEMPLATE_HDR"));
        } else {
            RecordToInsert.setId(super.generateSequence(TableName + "_HEADER"));
        }

//RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table_HEADER"
                + " (ID,TEMPLATE,COLUMN_ID,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH) "
                + " values "
                + " ('$id','$template','$columnid','$position','$format','$format2',$datatype,'$length')";
        insertStat = insertStat.replace("$table", "" + TableName);
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getId());
        insertStat = insertStat.replace("$template", "" + RecordToInsert.getTemplate());
        insertStat = insertStat.replace("$columnid", "" + RecordToInsert.getColumnId());
        insertStat = insertStat.replace("$position", "" + RecordToInsert.getPosition());
        insertStat = insertStat.replace("$format2", "" + RecordToInsert.getFormattwo());
        insertStat = insertStat.replace("$format", "" + RecordToInsert.getFormat());

        insertStat = insertStat.replace("$datatype", "" + RecordToInsert.getDataType());
        insertStat = insertStat.replace("$length", "" + RecordToInsert.getLength());
        Integer msg = 0;
        msg = super.executeUpdate(insertStat);

        super.postUpdate("Add New Record To " + TableName + "_HEADER", false);

        return msg;
    }

    public String ValidateNullDetail(Object... obj) {
        AtmFileTemplateDetailDTOInter RecordToInsert = (AtmFileTemplateDetailDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getTwmplate() == 0) {
            Validate = Validate + " Feild template is a requiered field";
        }
        if (RecordToInsert.getColumnId() == 0) {
            Validate = Validate + " Feild columnid is a requiered field";
        }
        if (RecordToInsert.getPosition() == null) {
            Validate = Validate + " Feild position is a requiered field";
        }
        if (RecordToInsert.getDataType() == null) {
            Validate = Validate + " Feild datatype is a requiered field";
        }
        if (RecordToInsert.getLineNumber() == null) {
            Validate = Validate + " Feild linenumber is a requiered field";
        }
        if (RecordToInsert.getLoadWhenMapped() == 0) {
            Validate = Validate + " Feild loadwhenmapped is a requiered field";
        }
        if (RecordToInsert.getLengthDir() == null) {
            Validate = Validate + " Feild lengthdir is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public String CheckPresent(Object... obj) throws Throwable {

        AtmFileTemplateDetailDTOInter RecordToSelect = (AtmFileTemplateDetailDTOInter) obj[0];
        String TName = (String) obj[1];
        Connection Conn = (Connection) obj[2];
        String selectStat = "Select ID From " + TName + "  where  ID= ?";
        PreparedStatement prpStatementCheck = Conn.prepareStatement(selectStat);
        prpStatementCheck.setInt(1, RecordToSelect.getId());
        ResultSet rsCheck = prpStatementCheck.executeQuery();
        if (rsCheck.next()) {
            CoonectionHandler.getInstance().returnConnection(Conn);
            return "Update";
        } else {
            CoonectionHandler.getInstance().returnConnection(Conn);
            return "Insert";
        }
        
    }

    public String CheckPresentHeader(Object... obj) throws Throwable {

        AtmFileHeaderDTOInter RecordToSelect = (AtmFileHeaderDTOInter) obj[0];
        String TName = (String) obj[1];
        Connection Conn = (Connection) obj[2];
        TName = TName + "_Header";
        String selectStat = "Select ID From " + TName + "  where  id = ? and column_id = ?";
        PreparedStatement prpStatementCheck = Conn.prepareStatement(selectStat);
        prpStatementCheck.setInt(1, RecordToSelect.getTemplate());
        prpStatementCheck.setInt(2, RecordToSelect.getColumnId());
        ResultSet rsCheck = prpStatementCheck.executeQuery();
        if (rsCheck.next()) {
            return "Update";
        } else {
            return "Insert";
        }

    }

    public Object insertDetail(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileTemplateDetailDTOInter uDTO = (AtmFileTemplateDetailDTOInter) obj[0];
        String TName = (String) obj[1];
        if (TName.toUpperCase().equals("SWITCH_FILE_TEMPLATE_DETAIL")) {
            uDTO.setId(super.generateSequence("SWITCH_FILE_TEMPLATE_DTL"));
        } else if (TName.toUpperCase().equals("HOST_FILE_TEMPLATE_DETAIL")) {
            uDTO.setId(super.generateSequence("HOST_FILE_TEMPLATE_DTL"));
        } else {
            uDTO.setId(super.generateSequence(TName));
        }
        String insertStat = "insert into $table   (id, column_id, template, position, line_number, format, format2, data_type, length, load_when_mapped, length_dir, starting_position, tag_string, mandatory, negative_amount_flag, add_decimal, decimal_pos,tags_order,remove_string)"
                + "values ($id,   $columnId,$template, $position,$lineNumber, '$format', '$formt2',"
                + " $dataType, $length, $loadWhenMapped, $lngthDir, $startPos,'$tagstring',$mandatory,$negativeflag,$decimalpoint,'$decimalpointposition','$tagsorder','$removestring')";
        insertStat = insertStat.replace("$removestring", "" + uDTO.getRemovestring());
        insertStat = insertStat.replace("$table", "" + TName);
        insertStat = insertStat.replace("$tagsorder", "" + uDTO.getTagsorder());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$template", "" + uDTO.getTwmplate());
        insertStat = insertStat.replace("$columnId", "" + uDTO.getColumnId());
        insertStat = insertStat.replace("$position", "" + uDTO.getPosition());
        insertStat = insertStat.replace("$format", "" + uDTO.getFormat());
        insertStat = insertStat.replace("$tagstring", "" + uDTO.getTagstring());
        //$decimalpoint,'$decimalpointposition'
        Integer temp;
        if (uDTO.getDecimalpoint() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        insertStat = insertStat.replace("$decimalpointposition", "" + uDTO.getDecimalpointposition());
        insertStat = insertStat.replace("$decimalpoint", "" + temp);

        if (uDTO.getMandatory() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        insertStat = insertStat.replace("$mandatory", "" + temp);
        if (uDTO.getNegativeflag() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        insertStat = insertStat.replace("$negativeflag", "" + temp);
        insertStat = insertStat.replace("$formt2", "" + uDTO.getFormat2());
        insertStat = insertStat.replace("$dataType", "" + uDTO.getDataType());
        insertStat = insertStat.replace("$length", "" + uDTO.getLength());
        insertStat = insertStat.replace("$lineNumber", "" + uDTO.getLineNumber());
        insertStat = insertStat.replace("$loadWhenMapped", "" + uDTO.getLoadWhenMapped());
        insertStat = insertStat.replace("$lngthDir", "" + uDTO.getLengthDir());
        insertStat = insertStat.replace("$startPos", "" + uDTO.getStartingPosition());

        Integer msg = super.executeUpdate(insertStat);
        String action = "Add detail to " + TName;
        super.postUpdate(action, false);
        return msg;
    }

    public Object updateDetail(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileTemplateDetailDTOInter uDTO = (AtmFileTemplateDetailDTOInter) obj[0];
        String TName = (String) obj[1];
        String updateStat = "update $table set  column_id = $columnId, position = $position"
                + ", format = '$format', format2 = '$formt2', data_type = $dataType, length = $length, line_number = $lineNumber"
                + ", load_when_mapped = $loadWhenMapped, length_dir = $lngthDir, starting_position = $startPos"
                + " ,remove_string = '$remove_string',tags_order = '$resettagsstarting',TAG_STRING = '$tagstring',MANDATORY = $mandatory ,NEGATIVE_AMOUNT_FLAG = $negativeflag ,ADD_DECIMAL=$decimalpoint,DECIMAL_POS='$decimalpointposition'"
                + " where id = $id";
        updateStat = updateStat.replace("$table", "" + TName);
        updateStat = updateStat.replace("$tagstring", "" + uDTO.getTagstring());
        updateStat = updateStat.replace("$remove_string", "" + uDTO.getRemovestring());
        Integer temp;

        if (uDTO.getDecimalpoint() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        updateStat = updateStat.replace("$decimalpointposition", "" + uDTO.getDecimalpointposition());
        updateStat = updateStat.replace("$decimalpoint", "" + temp);

        if (uDTO.getMandatory() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        updateStat = updateStat.replace("$mandatory", "" + temp);
        if (uDTO.getNegativeflag() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        updateStat = updateStat.replace("$negativeflag", "" + temp);
        updateStat = updateStat.replace("$resettagsstarting", "" + uDTO.getTagsorder());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$template", "" + uDTO.getTwmplate());
        updateStat = updateStat.replace("$columnId", "" + uDTO.getColumnId());
        updateStat = updateStat.replace("$position", "" + uDTO.getPosition());
        updateStat = updateStat.replace("$format", "" + uDTO.getFormat());
        updateStat = updateStat.replace("$formt2", "" + uDTO.getFormat2());
        updateStat = updateStat.replace("$dataType", "" + uDTO.getDataType());
        updateStat = updateStat.replace("$length", "" + uDTO.getLength());
        updateStat = updateStat.replace("$lineNumber", "" + uDTO.getLineNumber());
        updateStat = updateStat.replace("$loadWhenMapped", "" + uDTO.getLoadWhenMapped());
        updateStat = updateStat.replace("$lngthDir", "" + uDTO.getLengthDir());
        updateStat = updateStat.replace("$startPos", "" + uDTO.getStartingPosition());

        Integer msg = super.executeUpdate(updateStat);
        //AtmFileTemplateDTOInter aftDTO = (AtmFileTemplateDTOInter) aftDAO.find(uDTO.getTwmplate(),TName);
        String action = "Update Record detail In " + TName;
        super.postUpdate(action, false);
        return msg;
    }

    public Object findAllColumnsList() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select t.id id,upper(t.column_name) columnname from FILE_COLUMN_DEFINITION t";
        ResultSet rs = executeQuery(selectStat);
        List<FileColumnDefinitionDTOInter> uDTOL = new ArrayList<FileColumnDefinitionDTOInter>();
        while (rs.next()) {
            FileColumnDefinitionDTOInter fcdDTO = DTOFactory.createFileColumnDefinitionDTO();
            fcdDTO.setId(rs.getInt("id"));
            fcdDTO.setColumnName(rs.getString("columnname"));
            uDTOL.add(fcdDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public int GetColumnId(List<FileColumnDefinitionDTOInter> ColumnList, String ColName) {
        for (FileColumnDefinitionDTOInter Column : ColumnList) {
            if (ColName.toUpperCase().equals(Column.getColumnName().toUpperCase())) {
                return Column.getId();
            }
        }
        return 0000;
    }

    public Object CreateDuplicateList(List<FileColumnDefinitionDTOInter> ColumnList) {
        List<DuplicateListDTOInter> DupList = new ArrayList<DuplicateListDTOInter>();
        DuplicateListDTOInter DupItem = DTOFactory.createDuplicateListDTO();
        DupItem.setColID(GetColumnId(ColumnList, "ATM_ID"));
        DupItem.setColName("ATM_ID");
        DupItem.setDupColID(GetColumnId(ColumnList, "ATM_APPLICATION_ID"));
        DupItem.setDupColName("ATM_APPLICATION_ID");
        DupList.add(DupItem);
        DuplicateListDTOInter DupItem2 = DTOFactory.createDuplicateListDTO();
        DupItem2.setColID(GetColumnId(ColumnList, "TRANSACTION_SEQUENCE"));
        DupItem2.setColName("TRANSACTION_SEQUENCE");
        DupItem2.setDupColID(GetColumnId(ColumnList, "TRANSACTION_SEQUENCE_ORDER_BY"));
        DupItem2.setDupColName("TRANSACTION_SEQUENCE_ORDER_BY");
        DupList.add(DupItem2);
        DuplicateListDTOInter DupItem3 = DTOFactory.createDuplicateListDTO();
        DupItem3.setColID(GetColumnId(ColumnList, "CURRENCY"));
        DupItem3.setColName("CURRENCY");
        DupItem3.setDupColID(GetColumnId(ColumnList, "CURRENCY_ID"));
        DupItem3.setDupColName("CURRENCY_ID");
        DupList.add(DupItem3);
        DuplicateListDTOInter DupItem4 = DTOFactory.createDuplicateListDTO();
        DupItem4.setColID(GetColumnId(ColumnList, "TRANSACTION_STATUS"));
        DupItem4.setColName("TRANSACTION_STATUS");
        DupItem4.setDupColID(GetColumnId(ColumnList, "TRANSACTION_STATUS_ID"));
        DupItem4.setDupColName("TRANSACTION_STATUS_ID");
        DupList.add(DupItem4);
        DuplicateListDTOInter DupItem5 = DTOFactory.createDuplicateListDTO();
        DupItem5.setColID(GetColumnId(ColumnList, "TRANSACTION_TYPE"));
        DupItem5.setColName("TRANSACTION_TYPE");
        DupItem5.setDupColID(GetColumnId(ColumnList, "TRANSACTION_TYPE_ID"));
        DupItem5.setDupColName("TRANSACTION_TYPE_ID");
        DupItem5.setDupCol2ID(GetColumnId(ColumnList, "TRANSACTION_TYPE_MASTER"));
        DupItem5.setDupCol2Name("TRANSACTION_TYPE_MASTER");
        DupList.add(DupItem5);
        DuplicateListDTOInter DupItem6 = DTOFactory.createDuplicateListDTO();
        DupItem6.setColID(GetColumnId(ColumnList, "RESPONSE_CODE"));
        DupItem6.setColName("RESPONSE_CODE");
        DupItem6.setDupColID(GetColumnId(ColumnList, "RESPONSE_CODE_ID"));
        DupItem6.setDupColName("RESPONSE_CODE_ID");
        DupItem6.setDupCol2ID(GetColumnId(ColumnList, "RESPONSE_CODE_MASTER"));
        DupItem6.setDupCol2Name("RESPONSE_CODE_MASTER");
        DupList.add(DupItem6);

        return DupList;
    }

    public int[] HaveDuplicate(List<DuplicateListDTOInter> DupList, int ColID) {
        int[] Keys = new int[10];
        Keys[0] = 0;
        Keys[1] = 0;
        Keys[2] = 0;
        for (DuplicateListDTOInter DupItem : DupList) {
            if (ColID == DupItem.getColID() || ColID == DupItem.getDupCol2ID() || ColID == DupItem.getDupColID()) {
                Keys[0] = DupItem.getColID();
                Keys[1] = DupItem.getDupColID();
                Keys[2] = DupItem.getDupCol2ID();
                return Keys;
            }
        }
        return Keys;
    }

    public int HaveDuplicate2(List<DuplicateListDTOInter> DupList, int ColID) {
        for (DuplicateListDTOInter DupItem : DupList) {
            if (ColID == DupItem.getColID()) {
                return DupItem.getDupCol2ID();
            }
        }
        return 0000;
    }

    public Object SaveAllDetail(List<AtmFileTemplateDetailDTOInter> filetemplatedetailList, String TName, Integer templateid) throws Throwable {
        String Message = "";
        List<FileColumnDefinitionDTOInter> ColumnList = (List<FileColumnDefinitionDTOInter>) findAllColumnsList();
        List<DuplicateListDTOInter> DupList = (List<DuplicateListDTOInter>) CreateDuplicateList(ColumnList);
        Integer UpdateTotal = 0, InsertTotal = 0, ValidationMsg = 0, Idel = 0;
        Connection Conn = CoonectionHandler.getInstance().getConnection();
        for (AtmFileTemplateDetailDTOInter Record : filetemplatedetailList) {
            AtmFileTemplateDetailDTOInter RecordTemp = DTOFactory.createAtmFileTemplateDetailDTO();
            RecordTemp = Record;
            Record.setTwmplate(templateid);
            if (ValidateNullDetail(Record).equals("Validate")) {
                if (CheckPresent(Record, TName, Conn).contains("Update")) {
                    int[] Keys = HaveDuplicate(DupList, Record.getColumnId());
                    int col = Keys[0];
                    int dup = Keys[1];
                    int dup2 = Keys[2];
                    if (col != 0) {
                        RecordTemp.setColumnId(col);
                        int recordcolid = findColid(RecordTemp, TName);
                        if (recordcolid != 0000) {
                            RecordTemp.setId(recordcolid);
                            UpdateTotal = (Integer) updateDetail(RecordTemp, TName) + UpdateTotal;
                        } else {
                            InsertTotal = (Integer) insertDetail(RecordTemp, TName) + InsertTotal;
                        }
                    }
                    if (col == 0) {
                        int recordcolid4 = findColid(RecordTemp, TName);
                        if (recordcolid4 != 0000) {
                            RecordTemp.setId(recordcolid4);
                            UpdateTotal = (Integer) updateDetail(RecordTemp, TName) + UpdateTotal;
                        } else {
                            InsertTotal = (Integer) insertDetail(RecordTemp, TName) + InsertTotal;
                        }
                    }
                    if (dup != 0) {
                        RecordTemp.setColumnId(dup);
                        int recordcolid2 = findColid(RecordTemp, TName);
                        if (recordcolid2 != 0000) {
                            RecordTemp.setId(recordcolid2);
                            UpdateTotal = (Integer) updateDetail(RecordTemp, TName) + UpdateTotal;
                        } else {
                            InsertTotal = (Integer) insertDetail(RecordTemp, TName) + InsertTotal;
                        }
                    }
                    if (dup2 != 0) {
                        RecordTemp.setColumnId(dup2);
                        int recordcolid3 = findColid(RecordTemp, TName);
                        if (recordcolid3 != 0000) {
                            RecordTemp.setId(recordcolid3);
                            UpdateTotal = (Integer) updateDetail(RecordTemp, TName) + UpdateTotal;
                        } else {
                            InsertTotal = (Integer) insertDetail(RecordTemp, TName) + InsertTotal;
                        }
                    }

                } else if (CheckPresent(Record, TName, Conn).contains("Insert")) {
                    int[] Keys = HaveDuplicate(DupList, Record.getColumnId());
                    int col = Keys[0];
                    int dup = Keys[1];
                    int dup2 = Keys[2];
                    if (col != 0) {
                        Record.setColumnId(col);
                        InsertTotal = (Integer) insertDetail(Record, TName) + InsertTotal;
                    }
                    if (col == 0) {
                        InsertTotal = (Integer) insertDetail(Record, TName) + InsertTotal;
                    }
                    if (dup != 0) {
                        RecordTemp.setColumnId(dup);
                        InsertTotal = (Integer) insertDetail(RecordTemp, TName) + InsertTotal;
                    }
                    if (dup2 != 0) {
                        RecordTemp.setColumnId(dup2);
                        InsertTotal = (Integer) insertDetail(RecordTemp, TName) + InsertTotal;
                    }

                }
            } else {
                if (ValidateNullDetail(Record).equals(" Feild position is a requiered field Feild linenumber is a requiered field")) {
                    Idel = Idel + 1;
                } else {
                    ValidationMsg = ValidationMsg + 1;
                }
            }
        }
        Message = CreateMessage(UpdateTotal, InsertTotal, ValidationMsg, Idel);
        return Message;
    }

    public static String findDuplicates(List<AtmFileHeaderDTOInter> arr) {
        Set set = new HashSet();
        String Msg = "0000";
        for (int i = 0; i < arr.size(); i++) {
            if (set.contains(arr.get(i).getColumnId())) {
                Msg = "Duplicate Column found";
            } else {
                set.add(arr.get(i).getColumnId());
            }
        }
        return Msg;
    }

    public Object SaveAllHeader(List<AtmFileHeaderDTOInter> filetemplateHeaderList, String TName, Integer templateid) throws Throwable {
        String Message = "";
        String Msg = findDuplicates(filetemplateHeaderList);
        if (Msg.equals("0000")) {
            List<FileColumnDefinitionDTOInter> ColumnList = (List<FileColumnDefinitionDTOInter>) findAllColumnsList();
            List<DuplicateListDTOInter> DupList = (List<DuplicateListDTOInter>) CreateDuplicateList(ColumnList);
            Integer UpdateTotal = 0, InsertTotal = 0, ValidationMsg = 0;

            List<Integer> ColIDS = new ArrayList<Integer>();
            int ColIDSIndex = 0;
            for (AtmFileHeaderDTOInter Record : filetemplateHeaderList) {
                Record.setTemplate(templateid);
                if (ValidateNullUpdateHeaderVal(Record).equals("Validate")) {
                    if (Record.getStatus().equals("Update")) {
                        int[] Keys = HaveDuplicate(DupList, Record.getColumnId());
                        int col = Keys[0];
                        int dup = Keys[1];
                        int dup2 = Keys[2];
                        if (col != 0) {
                            Record.setColumnId(col);
                            int recordcolid = findColidHeader(Record, TName);
                            if (recordcolid != 0000) {
                                Record.setId(recordcolid);
                                ColIDS.add(Record.getColumnId());
                                UpdateTotal = (Integer) updaterecordHeader(Record, TName) + UpdateTotal;
                            } else {
                                ColIDS.add(Record.getColumnId());
                                InsertTotal = (Integer) insertrecordHeader(Record, TName) + InsertTotal;
                                Record.setStatus("Update");
                            }
                        }
                        if (col == 0) {

                            int recordcolid4 = findColidHeader(Record, TName);
                            if (recordcolid4 != 0000) {
                                Record.setId(recordcolid4);
                                ColIDS.add(Record.getColumnId());
                                UpdateTotal = (Integer) updaterecordHeader(Record, TName) + UpdateTotal;
                            } else {
                                ColIDS.add(Record.getColumnId());
                                InsertTotal = (Integer) insertrecordHeader(Record, TName) + InsertTotal;
                                Record.setStatus("Update");
                            }
                        }
                        if (dup != 0) {

                            Record.setColumnId(dup);
                            int recordcolid2 = findColidHeader(Record, TName);
                            if (recordcolid2 != 0000) {
                                Record.setId(recordcolid2);
                                ColIDS.add(Record.getColumnId());
                                UpdateTotal = (Integer) updaterecordHeader(Record, TName) + UpdateTotal;
                            } else {
                                ColIDS.add(Record.getColumnId());
                                InsertTotal = (Integer) insertrecordHeader(Record, TName) + InsertTotal;
                                Record.setStatus("Update");
                            }
                        }
                        if (dup2 != 0) {

                            Record.setColumnId(dup2);
                            int recordcolid3 = findColidHeader(Record, TName);
                            if (recordcolid3 != 0000) {
                                Record.setId(recordcolid3);
                                ColIDS.add(Record.getColumnId());
                                UpdateTotal = (Integer) updaterecordHeader(Record, TName) + UpdateTotal;
                            } else {
                                ColIDS.add(Record.getColumnId());
                                InsertTotal = (Integer) insertrecordHeader(Record, TName) + InsertTotal;
                                Record.setStatus("Update");
                            }
                        }

                    } else if (Record.getStatus().contains("Insert")) {
                        int[] Keys = HaveDuplicate(DupList, Record.getColumnId());
                        int col = Keys[0];
                        int dup = Keys[1];
                        int dup2 = Keys[2];
                        if (col != 0) {

                            Record.setColumnId(col);
                            ColIDS.add(Record.getColumnId());
                            InsertTotal = (Integer) insertrecordHeader(Record, TName) + InsertTotal;
                            Record.setStatus("Update");
                        }
                        if (col == 0) {
                            ColIDS.add(Record.getColumnId());
                            InsertTotal = (Integer) insertrecordHeader(Record, TName) + InsertTotal;
                            Record.setStatus("Update");
                        }
                        if (dup != 0) {

                            Record.setColumnId(dup);
                            ColIDS.add(Record.getColumnId());
                            InsertTotal = (Integer) insertrecordHeader(Record, TName) + InsertTotal;
                            Record.setStatus("Update");
                        }
                        if (dup2 != 0) {

                            Record.setColumnId(dup2);
                            ColIDS.add(Record.getColumnId());
                            InsertTotal = (Integer) insertrecordHeader(Record, TName) + InsertTotal;
                            Record.setStatus("Update");
                        }

                    }
                } else {
                    ValidationMsg = ValidationMsg + 1;
                }
            }
            String DeleteExtraRecord = CreateDeleteExtraRecord(ColIDS, templateid, TName);
            if (!DeleteExtraRecord.equals("0000")) {
                int deleteCount = (Integer) deleteExtrarecordHeader(templateid, TName, DeleteExtraRecord);
                Message = CreateMessageHeader(UpdateTotal, InsertTotal, ValidationMsg, deleteCount);
            } else {
                Message = CreateMessageHeader(UpdateTotal, InsertTotal, ValidationMsg, 0);
            }
        } else {
            Message = Msg;
        }
        return Message;
    }

    public String CreateDeleteExtraRecord(List<Integer> ColIDS, Integer TemplateID, String TName) {
        String Stat = "0000";
        for (int i = 0; i < ColIDS.size(); i++) {
            if (!ColIDS.get(i).equals(0)) {
                Stat = Stat + ColIDS.get(i) + ",";
            } else {
                break;
            }
        }
        if (!Stat.equals("0000")) {
            Stat = Stat.replace("0000", "");
            Stat = Stat.substring(0, Stat.length() - 1);
        }
        return Stat;
    }

    public String CreateMessageHeader(Integer UpdateTotal, Integer InsertTotal, Integer ValidationMsg, Integer DeleteRecord) {
        String Msg = "No Record Found";
        if (UpdateTotal > 0) {
            Msg = Msg.replace("No Record Found", "");
            Msg = Msg + UpdateTotal.toString() + " Record Has Been Updated ";
        }
        if (InsertTotal > 0) {
            Msg = Msg.replace("No Record Found", "");
            Msg = Msg + InsertTotal.toString() + " Record Has Been Inserted ";
        }
        if (ValidationMsg > 0) {
            Msg = Msg.replace("No Record Found", "");
            Msg = Msg + ValidationMsg.toString() + " Record Is InValid,Please Insert Requierd Fields";
        }
        if (DeleteRecord > 0) {
            Msg = Msg.replace("No Record Found", "");
            Msg = Msg + DeleteRecord.toString() + " Record Has Been Deleted";
        }
        return Msg;
    }

    public String CreateMessage(Integer UpdateTotal, Integer InsertTotal, Integer ValidationMsg, Integer Idel) {
        String Msg = "No Record Found";
        if (UpdateTotal > 0) {
            Msg = Msg.replace("No Record Found", "");
            Msg = Msg + UpdateTotal.toString() + " Record Has Been Updated ";
        }
        if (InsertTotal > 0) {
            Msg = Msg.replace("No Record Found", "");
            Msg = Msg + InsertTotal.toString() + " Record Has Been Inserted ";
        }
        if (Idel > 0) {
            Msg = Msg.replace("No Record Found", "");
            Msg = Msg + Idel.toString() + " Idel Record";
        }
        if (ValidationMsg > 0) {
            Msg = Msg.replace("No Record Found", "");
            Msg = Msg + ValidationMsg.toString() + " Record Is InValid,Please Insert Requierd Fields";
        }
        return Msg;
    }

    public String ValidateNullDeleteTemplateAllHeaderval(Object... obj) {
        AtmFileTemplateDTOInter RecordToInsert = (AtmFileTemplateDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getId() == 0) {
            Validate = Validate + " Feild Template id is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public String ValidateNullDeleteHeaderval(Object... obj) {
        AtmFileHeaderDTOInter RecordToInsert = (AtmFileHeaderDTOInter) obj[0];
        String Validate = "Validate";
        if ((RecordToInsert.getPosition() == null ? "" == null : RecordToInsert.getPosition().equals("")) || RecordToInsert.getPosition() == null) {
            Validate = Validate + " Feild position id is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public String ValidateNullDeleteTemplateDetail(Object... obj) {
        AtmFileTemplateDetailDTOInter RecordToInsert = (AtmFileTemplateDetailDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getId() == 0) {
            Validate = Validate + " Feild Template Detail id is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object deleterecorddetailTemplate(Object... obj) throws Throwable {
        super.preUpdate();
        AtmFileTemplateDTOInter RecordToDelete = (AtmFileTemplateDTOInter) obj[0];
        String TName = (String) obj[1];
        String msg = ValidateNullDeleteTemplateAllHeaderval(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  template = $id";
            deleteStat = deleteStat.replace("$table", "" + TName + "_DETAIL");
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getId());
            msg = super.executeUpdate(deleteStat).toString();
            msg = msg + " Detail Record Has Been deleted";
            super.postUpdate("Delete Record In Table " + TName + "_DETAIL", false);
        }
        return msg;
    }

    public Object DeleteDetail(Object... obj) throws Throwable {
        AtmFileTemplateDetailDTOInter RecordToDelete = (AtmFileTemplateDetailDTOInter) obj[0];
        String TName = (String) obj[1];
        List<FileColumnDefinitionDTOInter> ColumnList = (List<FileColumnDefinitionDTOInter>) findAllColumnsList();
        List<DuplicateListDTOInter> DupList = (List<DuplicateListDTOInter>) CreateDuplicateList(ColumnList);
        int[] Keys = HaveDuplicate(DupList, RecordToDelete.getColumnId());
        int col = Keys[0];
        int dup = Keys[1];
        int dup2 = Keys[2];
        RecordToDelete.setColumnId(col);
        int deleteTotal = 0;
        deleteTotal = (Integer) DeleteDetailRecord(RecordToDelete, TName) + deleteTotal;

        if (col != 0) {
            RecordToDelete.setColumnId(col);
            int recordcolid3 = findColid(RecordToDelete, TName);
            RecordToDelete.setId(recordcolid3);
            deleteTotal = (Integer) DeleteDetailRecord(RecordToDelete, TName) + deleteTotal;
        }
        if (col == 0) {
            RecordToDelete.setColumnId(col);
            int recordcolid4 = findColid(RecordToDelete, TName);
            RecordToDelete.setId(recordcolid4);
            deleteTotal = (Integer) DeleteDetailRecord(RecordToDelete, TName) + deleteTotal;
        }
        if (dup != 0) {
            RecordToDelete.setColumnId(dup);
            int recordcolid = findColid(RecordToDelete, TName);
            RecordToDelete.setId(recordcolid);
            deleteTotal = (Integer) DeleteDetailRecord(RecordToDelete, TName) + deleteTotal;
        }
        if (dup2 != 0) {
            RecordToDelete.setColumnId(dup2);
            int recordcolid2 = findColid(RecordToDelete, TName);
            RecordToDelete.setId(recordcolid2);
            deleteTotal = (Integer) DeleteDetailRecord(RecordToDelete, TName) + deleteTotal;
        }

        return deleteTotal + " Detail Record Has Been deleted";
    }

    public Object DeleteDetailRecord(Object... obj) throws Throwable {
        super.preUpdate();
        AtmFileTemplateDetailDTOInter RecordToDelete = (AtmFileTemplateDetailDTOInter) obj[0];
        String TName = (String) obj[1];
        int msg = 0;
        String deleteStat = "delete from $table "
                + "  where  id = $id";
        deleteStat = deleteStat.replace("$table", "" + TName);
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getId());
        msg = super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table " + TName, false);
        return msg;
    }
}
