/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.BingostatsearchDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DisputesDTOInter;
import com.ev.AtmBingo.bus.dto.MatchedDataDTOInter;
import com.ev.AtmBingo.bus.dto.TransStatDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.disstatprintDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class BingoStatDAO extends BaseDAO implements BingoStatDAOInter {

    public Object GetTransactionStatDetailMatchbyatmexcel(Object... obj) throws Throwable {
        super.preSelect();
        String Condition = "";
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        if (Searchparm.getSearchtype().equals("Matched")) {
            Condition = " where  transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype "
                    + " and matching_type = $matchingtype  "
                    + " and atm_application_id = '$atmid' ";

        }
        String selectStat = "select (select name from atm_file where id = t.file_id) filename,t.loading_date loadingdate,t.atm_application_id atmapplicationid, (select name from atm_machine where id = t.atm_id)atmname,t.transaction_type transactiontype,t.currency currency,t.transaction_status transactionstatus,t.response_code responsecode,t.transaction_date transactiondate,"+super.getcardcolumntalias()+" cardnum,t.amount amount,t.settlement_date settlementdate,t.notes_presented notespresented,t.customer_account_number customeraccountnumber,t.transaction_sequence_order_by transactionsequence,decode(record_type,1,'J',2,'S',3,'H')recordtype,t.settled_date settleddate,t.comments comments,(select user_name from users where user_id = t.settled_user)settleduser, t.abs_amount absamount,t.column4 NotesPresented,t.column3 Autho,t.column2 GLACCOUNTATM,t.column1 Branch,"+super.gettransactiondatacolumntalias()+" transdata from $table t where $whereClause";
        selectStat = selectStat.replace("$table", " MATCHED_DATA ");

        selectStat = selectStat.replace("$whereClause", "" + Condition);
        selectStat = selectStat.replace("$atmid", "" + Searchparm.getAtmid());
        selectStat = selectStat.replace("$matchingtype", "" + Searchparm.getMatchingtype());
        selectStat = selectStat.replace("$user", "" + Searchparm.getUserid());
        selectStat = selectStat.replace("$recordtype", "" + Searchparm.getRecordtype());
        selectStat = selectStat.replace("$datefrom", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getFromdate()));
        selectStat = selectStat.replace("$dateto", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getTodate()));
        ResultSet rs = executeQuery(selectStat);
         List<disstatprintDTOInter> Record = new ArrayList<disstatprintDTOInter>();
        while (rs.next()) {
            disstatprintDTOInter Records = DTOFactory.createdisstatprintDTO();
            Records.setFilename(rs.getString("filename"));
            Records.setLoadingdate(rs.getString("loadingdate"));
            Records.setAtmapplicationid(rs.getString("atmapplicationid"));
            Records.setAtmname(rs.getString("atmname"));
            Records.setTransactiontype(rs.getString("transactiontype"));
            Records.setCurrency(rs.getString("currency"));
            Records.setTransactionstatus(rs.getString("transactionstatus"));
            Records.setResponsecode(rs.getString("responsecode"));
            Records.setTransactiondate(rs.getString("transactiondate"));
            Records.setCardnum(rs.getString("cardnum"));
            Records.setAmount(rs.getString("amount"));
            Records.setSettlementdate(rs.getString("settlementdate"));
            Records.setNotesPresented(rs.getString("notespresented"));
            Records.setCustomeraccountnumber(rs.getString("customeraccountnumber"));
            Records.setTransactionsequence(rs.getString("transactionsequence"));
            Records.setRecordtype(rs.getString("recordtype"));
            Records.setSettleddate(rs.getString("settleddate"));
            Records.setComments(rs.getString("comments"));
            Records.setSettleduser(rs.getString("settleduser"));
            Records.setAbsamount(rs.getString("absamount"));
            Records.setNotesPresented(rs.getString("NotesPresented"));
            Records.setAutho(rs.getString("Autho"));
            Records.setGLACCOUNTATM(rs.getString("GLACCOUNTATM"));
            Records.setBranch(rs.getString("Branch"));
            Records.setTransdata(rs.getString("transdata"));
            Record.add(Records);
        }
        super.postSelect(rs);
        return Record;
    }
    public Object GetTransactionStatDetailMatchexcel(Object... obj) throws Throwable {
        super.preSelect();
        String Condition = "";
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        if (Searchparm.getSearchtype().equals("Matched")) {
            Condition = "  transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype "
                    + " and matching_type = $matchingtype  ";
        }
        String selectStat = "select (select name from atm_file where id = t.file_id) filename,t.loading_date loadingdate,t.atm_application_id atmapplicationid, (select name from atm_machine where id = t.atm_id)atmname,t.transaction_type transactiontype,t.currency currency,t.transaction_status transactionstatus,t.response_code responsecode,t.transaction_date transactiondate,"+super.getcardcolumntalias()+" cardnum,t.amount amount,t.settlement_date settlementdate,t.notes_presented notespresented,t.customer_account_number customeraccountnumber,t.transaction_sequence_order_by transactionsequence,decode(record_type,1,'J',2,'S',3,'H')recordtype,t.settled_date settleddate,t.comments comments,(select user_name from users where user_id = t.settled_user)settleduser, t.abs_amount absamount,t.column4 NotesPresented,t.column3 Autho,t.column2 GLACCOUNTATM,t.column1 Branch,"+super.gettransactiondatacolumntalias()+" transdata from $table t where $whereClause";
        selectStat = selectStat.replace("$table", " MATCHED_DATA ");
        selectStat = selectStat.replace("$whereClause", "" + Condition);
        selectStat = selectStat.replace("$matchingtype", "" + Searchparm.getMatchingtype());
        selectStat = selectStat.replace("$user", "" + Searchparm.getUserid());
        selectStat = selectStat.replace("$recordtype", "" + Searchparm.getRecordtype());
        selectStat = selectStat.replace("$datefrom", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getFromdate()));
        selectStat = selectStat.replace("$dateto", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getTodate()));
          ResultSet rs = executeQuery(selectStat);
         List<disstatprintDTOInter> Record = new ArrayList<disstatprintDTOInter>();
        while (rs.next()) {
            disstatprintDTOInter Records = DTOFactory.createdisstatprintDTO();
            Records.setFilename(rs.getString("filename"));
            Records.setLoadingdate(rs.getString("loadingdate"));
            Records.setAtmapplicationid(rs.getString("atmapplicationid"));
            Records.setAtmname(rs.getString("atmname"));
            Records.setTransactiontype(rs.getString("transactiontype"));
            Records.setCurrency(rs.getString("currency"));
            Records.setTransactionstatus(rs.getString("transactionstatus"));
            Records.setResponsecode(rs.getString("responsecode"));
            Records.setTransactiondate(rs.getString("transactiondate"));
            Records.setCardnum(rs.getString("cardnum"));
            Records.setAmount(rs.getString("amount"));
            Records.setSettlementdate(rs.getString("settlementdate"));
            Records.setNotesPresented(rs.getString("notespresented"));
            Records.setCustomeraccountnumber(rs.getString("customeraccountnumber"));
            Records.setTransactionsequence(rs.getString("transactionsequence"));
            Records.setRecordtype(rs.getString("recordtype"));
            Records.setSettleddate(rs.getString("settleddate"));
            Records.setComments(rs.getString("comments"));
            Records.setSettleduser(rs.getString("settleduser"));
            Records.setAbsamount(rs.getString("absamount"));
            Records.setNotesPresented(rs.getString("NotesPresented"));
            Records.setAutho(rs.getString("Autho"));
            Records.setGLACCOUNTATM(rs.getString("GLACCOUNTATM"));
            Records.setBranch(rs.getString("Branch"));
            Records.setTransdata(rs.getString("transdata"));
            Record.add(Records);
        }
        super.postSelect(rs);
        return Record;
    }
     public Object GetTransactionStatDetailMatchbyatm(Object... obj) throws Throwable {
        super.preSelect();
        String Condition = "";
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        if (Searchparm.getSearchtype().equals("Matched")) {
            Condition = " where  transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype "
                    + " and matching_type = $matchingtype  "
                    + " and atm_application_id = '$atmid' ";

        }
        String selectStat = "select rownum elrow,(select l.name from atm_machine l where l.id = $table.atm_id) atmname,is_black_list("+super.getcardcolumntable()+") blacklist,$table.*,"+super.getcardcolumntable()+" card_no_decrypt from $table $whereClause";
        selectStat = selectStat.replace("$table", " MATCHED_DATA ");

        selectStat = selectStat.replace("$whereClause", "" + Condition);
        selectStat = selectStat.replace("$atmid", "" + Searchparm.getAtmid());
        selectStat = selectStat.replace("$matchingtype", "" + Searchparm.getMatchingtype());
        selectStat = selectStat.replace("$user", "" + Searchparm.getUserid());
        selectStat = selectStat.replace("$recordtype", "" + Searchparm.getRecordtype());
        selectStat = selectStat.replace("$datefrom", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getFromdate()));
        selectStat = selectStat.replace("$dateto", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getTodate()));
        ResultSet rs = executeQuery(selectStat);
        List<MatchedDataDTOInter> uDTOL = new ArrayList<MatchedDataDTOInter>();
        while (rs.next()) {
            MatchedDataDTOInter uDTO = DTOFactory.createMatchedDataDTO();
            uDTO.setamount(rs.getInt("amount"));
            uDTO.setBlacklist(rs.getInt("blacklist"));
            uDTO.setRownum(rs.getString("elrow"));
            uDTO.setatmapplicationid(rs.getString("atm_application_id"));
            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
            amDTO.setId(rs.getInt("atm_id"));
            uDTO.setatmid(amDTO);
            uDTO.setCardno(rs.getString("card_no_decrypt"));
            uDTO.setcardnosuffix(rs.getString("card_no_suffix"));
            uDTO.setatmname(rs.getString("atmname"));
            uDTO.setColumn1(rs.getString("column1"));
            uDTO.setColumn2(rs.getString("column2"));
            uDTO.setColumn3(rs.getString("column3"));
            uDTO.setColumn4(rs.getString("column4"));
            uDTO.setColumn5(rs.getString("column5"));
            uDTO.setComments(rs.getString("comments"));
            uDTO.setCurrency(rs.getString("currency"));
            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
            cmDTO.setId((rs.getInt("currency_id")));
            uDTO.setCurrencyid(cmDTO);
            uDTO.setCustomeraccountnumber(rs.getString("customer_account_number"));
            uDTO.setFileid(rs.getInt("file_id"));
            uDTO.setLoadingdate(rs.getTimestamp("loading_date"));
            uDTO.setMatchby(rs.getInt("match_by"));
            uDTO.setmatchdate(rs.getTimestamp("match_date"));
            uDTO.setMatchkey(rs.getInt("match_key"));
            uDTO.setMatchingtype(rs.getInt("matching_type"));
            uDTO.setNotespresented(rs.getString("notes_presented"));
            uDTO.setRecordtype(rs.getInt("record_type"));
            uDTO.setResponsecode(rs.getString("response_code"));
            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
            trcDTO.setId((rs.getInt("response_code_id")));
            uDTO.setResponsecodeid(trcDTO);
            uDTO.setResponsecodemaster(rs.getInt("response_code_master"));
            uDTO.setSettleddate(rs.getTimestamp("settled_date"));
            uDTO.setSettledflag(rs.getInt("settled_flag"));
            UsersDTOInter userDTO = DTOFactory.createUsersDTO();
            userDTO.setUserId((rs.getInt("settled_user")));
            uDTO.setSettleduser(userDTO);
            uDTO.setSettlementdate(rs.getTimestamp("settlement_date"));
            uDTO.setTransactiondate(rs.getTimestamp("transaction_date"));
            uDTO.setTransactionsequence(rs.getString("transaction_sequence"));
            uDTO.setTransactionsequenceorderby(rs.getInt("transaction_sequence_order_by"));
            uDTO.setTransactionstatus(rs.getString("transaction_status"));
            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
            tsDTO.setId((rs.getInt("transaction_status_id")));
            uDTO.setTransactionstatusid(tsDTO);
            uDTO.setTransactiontime(rs.getTimestamp("transaction_time"));
            uDTO.setTransactiontype(rs.getString("transaction_type"));
            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
            ttDTO.setId((rs.getInt("transaction_type_id")));
            uDTO.setTransactiontypeid(ttDTO);
            uDTO.setTransactiontypemaster(rs.getInt("transaction_type_master"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object GetTransactionStatDetailMatch(Object... obj) throws Throwable {
        super.preSelect();
        String Condition = "";
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        if (Searchparm.getSearchtype().equals("Matched")) {
            Condition = " where  transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype "
                    + " and matching_type = $matchingtype  ";
        }
        String selectStat = "select  rownum elrow,(select l.name from atm_machine l where l.id = $table.atm_id) atmname,is_black_list("+super.getcardcolumntable()+") blacklist,$table.*,"+super.getcardcolumntable()+" card_no_decrypt from $table $whereClause";
        selectStat = selectStat.replace("$table", " MATCHED_DATA ");
        selectStat = selectStat.replace("$whereClause", "" + Condition);
        selectStat = selectStat.replace("$matchingtype", "" + Searchparm.getMatchingtype());
        selectStat = selectStat.replace("$user", "" + Searchparm.getUserid());
        selectStat = selectStat.replace("$recordtype", "" + Searchparm.getRecordtype());
        selectStat = selectStat.replace("$datefrom", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getFromdate()));
        selectStat = selectStat.replace("$dateto", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getTodate()));
        ResultSet rs = executeQuery(selectStat);
        List<MatchedDataDTOInter> uDTOL = new ArrayList<MatchedDataDTOInter>();
        while (rs.next()) {
            MatchedDataDTOInter uDTO = DTOFactory.createMatchedDataDTO();
            uDTO.setamount(rs.getInt("amount"));
            uDTO.setBlacklist(rs.getInt("blacklist"));
            uDTO.setRownum(rs.getString("elrow"));
            uDTO.setatmapplicationid(rs.getString("atm_application_id"));
            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
            amDTO.setId(rs.getInt("atm_id"));
            uDTO.setatmid(amDTO);
            uDTO.setCardno(rs.getString("card_no_decrypt"));
            uDTO.setcardnosuffix(rs.getString("card_no_suffix"));
            uDTO.setatmname(rs.getString("atmname"));
            uDTO.setColumn1(rs.getString("column1"));
            uDTO.setColumn2(rs.getString("column2"));
            uDTO.setColumn3(rs.getString("column3"));
            uDTO.setColumn4(rs.getString("column4"));
            uDTO.setColumn5(rs.getString("column5"));
            uDTO.setComments(rs.getString("comments"));
            uDTO.setCurrency(rs.getString("currency"));
            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
            cmDTO.setId((rs.getInt("currency_id")));
            uDTO.setCurrencyid(cmDTO);
            uDTO.setCustomeraccountnumber(rs.getString("customer_account_number"));
            uDTO.setFileid(rs.getInt("file_id"));
            uDTO.setLoadingdate(rs.getTimestamp("loading_date"));
            uDTO.setMatchby(rs.getInt("match_by"));
            uDTO.setmatchdate(rs.getTimestamp("match_date"));
            uDTO.setMatchkey(rs.getInt("match_key"));
            uDTO.setMatchingtype(rs.getInt("matching_type"));
            uDTO.setNotespresented(rs.getString("notes_presented"));
            uDTO.setRecordtype(rs.getInt("record_type"));
            uDTO.setResponsecode(rs.getString("response_code"));
            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
            trcDTO.setId((rs.getInt("response_code_id")));
            uDTO.setResponsecodeid(trcDTO);
            uDTO.setResponsecodemaster(rs.getInt("response_code_master"));
            uDTO.setSettleddate(rs.getTimestamp("settled_date"));
            uDTO.setSettledflag(rs.getInt("settled_flag"));
            UsersDTOInter userDTO = DTOFactory.createUsersDTO();
            userDTO.setUserId((rs.getInt("settled_user")));
            uDTO.setSettleduser(userDTO);
            uDTO.setSettlementdate(rs.getTimestamp("settlement_date"));
            uDTO.setTransactiondate(rs.getTimestamp("transaction_date"));
            uDTO.setTransactionsequence(rs.getString("transaction_sequence"));
            uDTO.setTransactionsequenceorderby(rs.getInt("transaction_sequence_order_by"));
            uDTO.setTransactionstatus(rs.getString("transaction_status"));
            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
            tsDTO.setId((rs.getInt("transaction_status_id")));
            uDTO.setTransactionstatusid(tsDTO);
            uDTO.setTransactiontime(rs.getTimestamp("transaction_time"));
            uDTO.setTransactiontype(rs.getString("transaction_type"));
            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
            ttDTO.setId((rs.getInt("transaction_type_id")));
            uDTO.setTransactiontypeid(ttDTO);
            uDTO.setTransactiontypemaster(rs.getInt("transaction_type_master"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object GetTransactionStatDetailDisp(Object... obj) throws Throwable {
        super.preSelect();
        String Condition = "";
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        if (Searchparm.getSearchtype().equals("Disputes")) {
            Condition = " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype  "
                    + " and matching_type = $matchingtype  "
                    + " and dispute_key is not null and dispute_with_roles = 2";
        } else if (Searchparm.getSearchtype().equals("MissingSide")) {
            Condition = " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype  "
                    + " and matching_type = $matchingtype  "
                    + " and dispute_key is null and dispute_with_roles = 2 ";
        } else if (Searchparm.getSearchtype().equals("Errors")) {
            Condition = " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype "
                    + " and matching_type = $matchingtype "
                    + " and  dispute_with_roles = 1";
        }
        String selectStat = "select $table.*,"+super.getcardcolumntable()+" card_no_decrypt,get_disputes_reason(dispute_key_uk,dispute_reason) ReasonToDisp from $table where $cond";
        selectStat = selectStat.replace("$table", "Disputes");
        selectStat = selectStat.replace("$cond", " " + Condition);
        selectStat = selectStat.replace("$matchingtype", "" + Searchparm.getMatchingtype());
        selectStat = selectStat.replace("$user", "" + Searchparm.getUserid());
        selectStat = selectStat.replace("$recordtype", "" + Searchparm.getRecordtype());
        selectStat = selectStat.replace("$datefrom", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getFromdate()));
        selectStat = selectStat.replace("$dateto", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getTodate()));
        ResultSet rs = executeQueryReport(selectStat);
        List<DisputesDTOInter> Records = new ArrayList<DisputesDTOInter>();
        while (rs.next()) {
            DisputesDTOInter uDTO = DTOFactory.createDisputesDTO();
            uDTO.setamount(rs.getInt("amount"));
            uDTO.setatmapplicationid(rs.getString("atm_application_id"));
            uDTO.setdisputereason(rs.getString("ReasonToDisp"));
            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
            amDTO.setId(rs.getInt("atm_id"));
            uDTO.setatmid(amDTO);
            uDTO.setcardno(rs.getString("card_no_decrypt"));
            uDTO.setcardnosuffix(rs.getString("card_no_suffix"));
            uDTO.setcolumn1(rs.getString("column1"));
            uDTO.setcolumn2(rs.getString("column2"));
            uDTO.setcolumn3(rs.getString("column3"));
            uDTO.setcolumn4(rs.getString("column4"));
            uDTO.setcolumn5(rs.getString("column5"));
            uDTO.setcomments(rs.getString("comments"));
            uDTO.setcurrency(rs.getString("currency"));
            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
            cmDTO.setId((rs.getInt("currency_id")));
            uDTO.setcurrencyid(cmDTO);
            uDTO.setcustomeraccountnumber(rs.getString("customer_account_number"));
            uDTO.setfileid(rs.getInt("file_id"));
            uDTO.setloadingdate(rs.getTimestamp("loading_date"));
            uDTO.setdisputeby(rs.getInt("dispute_by"));
            uDTO.setdisputedate(rs.getTimestamp("dispute_date"));
            uDTO.setdisputekey(rs.getInt("dispute_key"));
            uDTO.setDisputekeyuk(rs.getString("dispute_key_uk"));
            uDTO.setmatchingtype(rs.getInt("matching_type"));
            uDTO.setnotespresented(rs.getString("notes_presented"));
            uDTO.setrecordtype(rs.getInt("record_type"));
            uDTO.setresponsecode(rs.getString("response_code"));
            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
            trcDTO.setId((rs.getInt("response_code_id")));
            uDTO.setresponsecodeid(trcDTO);
            uDTO.setresponsecodemaster(rs.getInt("response_code_master"));
            uDTO.setsettleddate(rs.getTimestamp("settled_date"));
            uDTO.setsettledflag(rs.getInt("settled_flag"));
            UsersDTOInter userDTO = DTOFactory.createUsersDTO();
            userDTO.setUserId((rs.getInt("settled_user")));
            uDTO.setsettleduser(userDTO);
            uDTO.setsettlementdate(rs.getTimestamp("settlement_date"));
            uDTO.settransactiondate(rs.getTimestamp("transaction_date"));
            uDTO.settransactionseqeunce(rs.getString("transaction_sequence"));
            uDTO.settransactionsequenceorderby(rs.getInt("transaction_sequence_order_by"));
            uDTO.settransactionstatus(rs.getString("transaction_status"));
            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
            tsDTO.setId((rs.getInt("transaction_status_id")));
            uDTO.settransactionstatusid(tsDTO);
            uDTO.settransactiontime(rs.getTimestamp("transaction_time"));
            uDTO.settransactiontype(rs.getString("transaction_type"));
            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
            ttDTO.setId((rs.getInt("transaction_type_id")));
            uDTO.settransactiontypeid(ttDTO);
            uDTO.settransactiontypemaster(rs.getInt("transaction_type_master"));
            uDTO.setdisputereason(rs.getString("dispute_reason"));
            uDTO.setreasontodisp(rs.getString("dispute_reason"));
            Records.add(uDTO);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object GetTransactionStatDetailDispbyatm(Object... obj) throws Throwable {
        super.preSelect();
        String Condition = "";
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        if (Searchparm.getSearchtype().equals("Disputes")) {
            Condition = " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype  "
                    + " and matching_type = $matchingtype  "
                    + " and atm_application_id = '$atmid' "
                    + " and dispute_key is not null and dispute_with_roles = 2";
        } else if (Searchparm.getSearchtype().equals("MissingSide")) {
            Condition = " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype  "
                    + " and matching_type = $matchingtype  "
                    + " and dispute_key is null and dispute_with_roles = 2"
                    + " and atm_application_id = '$atmid' ";
        } else if (Searchparm.getSearchtype().equals("Errors")) {
            Condition = " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype "
                    + " and matching_type = $matchingtype "
                    + " and transaction_status_id in (select id from transaction_status) "
                    + " and atm_application_id = '$atmid' "
                    + " and  dispute_with_roles = 1";
        }
        String selectStat = "select $table.*,"+super.getcardcolumntable()+" card_no_decrypt,get_disputes_reason(dispute_key_uk,dispute_reason) ReasonToDisp from $table where $cond";
        selectStat = selectStat.replace("$table", "Disputes");
        selectStat = selectStat.replace("$cond", " " + Condition);
        selectStat = selectStat.replace("$matchingtype", "" + Searchparm.getMatchingtype());
        selectStat = selectStat.replace("$user", "" + Searchparm.getUserid());
        selectStat = selectStat.replace("$recordtype", "" + Searchparm.getRecordtype());
        selectStat = selectStat.replace("$datefrom", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getFromdate()));
        selectStat = selectStat.replace("$dateto", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getTodate()));
        selectStat = selectStat.replace("$atmid", "" + Searchparm.getAtmid());
        ResultSet rs = executeQueryReport(selectStat);
        List<DisputesDTOInter> Records = new ArrayList<DisputesDTOInter>();
        while (rs.next()) {
            DisputesDTOInter uDTO = DTOFactory.createDisputesDTO();
            uDTO.setamount(rs.getInt("amount"));
            uDTO.setatmapplicationid(rs.getString("atm_application_id"));
            uDTO.setdisputereason(rs.getString("ReasonToDisp"));
            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
            amDTO.setId(rs.getInt("atm_id"));
            uDTO.setatmid(amDTO);
            uDTO.setcardno(rs.getString("card_no_decrypt"));
            uDTO.setcardnosuffix(rs.getString("card_no_suffix"));
            uDTO.setcolumn1(rs.getString("column1"));
            uDTO.setcolumn2(rs.getString("column2"));
            uDTO.setcolumn3(rs.getString("column3"));
            uDTO.setcolumn4(rs.getString("column4"));
            uDTO.setcolumn5(rs.getString("column5"));
            uDTO.setcomments(rs.getString("comments"));
            uDTO.setcurrency(rs.getString("currency"));
            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
            cmDTO.setId((rs.getInt("currency_id")));
            uDTO.setcurrencyid(cmDTO);
            uDTO.setcustomeraccountnumber(rs.getString("customer_account_number"));
            uDTO.setfileid(rs.getInt("file_id"));
            uDTO.setloadingdate(rs.getTimestamp("loading_date"));
            uDTO.setdisputeby(rs.getInt("dispute_by"));
            uDTO.setdisputedate(rs.getTimestamp("dispute_date"));
            uDTO.setdisputekey(rs.getInt("dispute_key"));
            uDTO.setDisputekeyuk(rs.getString("dispute_key_uk"));
            uDTO.setmatchingtype(rs.getInt("matching_type"));
            uDTO.setnotespresented(rs.getString("notes_presented"));
            uDTO.setrecordtype(rs.getInt("record_type"));
            uDTO.setresponsecode(rs.getString("response_code"));
            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
            trcDTO.setId((rs.getInt("response_code_id")));
            uDTO.setresponsecodeid(trcDTO);
            uDTO.setresponsecodemaster(rs.getInt("response_code_master"));
            uDTO.setsettleddate(rs.getTimestamp("settled_date"));
            uDTO.setsettledflag(rs.getInt("settled_flag"));
            UsersDTOInter userDTO = DTOFactory.createUsersDTO();
            userDTO.setUserId((rs.getInt("settled_user")));
            uDTO.setsettleduser(userDTO);
            uDTO.setsettlementdate(rs.getTimestamp("settlement_date"));
            uDTO.settransactiondate(rs.getTimestamp("transaction_date"));
            uDTO.settransactionseqeunce(rs.getString("transaction_sequence"));
            uDTO.settransactionsequenceorderby(rs.getInt("transaction_sequence_order_by"));
            uDTO.settransactionstatus(rs.getString("transaction_status"));
            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
            tsDTO.setId((rs.getInt("transaction_status_id")));
            uDTO.settransactionstatusid(tsDTO);
            uDTO.settransactiontime(rs.getTimestamp("transaction_time"));
            uDTO.settransactiontype(rs.getString("transaction_type"));
            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
            ttDTO.setId((rs.getInt("transaction_type_id")));
            uDTO.settransactiontypeid(ttDTO);
            uDTO.settransactiontypemaster(rs.getInt("transaction_type_master"));
            uDTO.setdisputereason(rs.getString("dispute_reason"));
            uDTO.setreasontodisp(rs.getString("dispute_reason"));
            Records.add(uDTO);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object GetTransactionStatDetailDispbyatmexcel(Object... obj) throws Throwable {
        super.preSelect();
        String Condition = "";
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        if (Searchparm.getSearchtype().equals("Disputes")) {
            Condition = " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype  "
                    + " and matching_type = $matchingtype  "
                    + " and atm_application_id = '$atmid' "
                    + " and dispute_key is not null and dispute_with_roles = 2";
        } else if (Searchparm.getSearchtype().equals("MissingSide")) {
            Condition = " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype  "
                    + " and matching_type = $matchingtype  "
                    + " and dispute_key is null and dispute_with_roles = 2"
                    + " and atm_application_id = '$atmid' ";
        } else if (Searchparm.getSearchtype().equals("Errors")) {
            Condition = " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype "
                    + " and matching_type = $matchingtype "
                    + " and transaction_status_id in (select id from transaction_status) "
                    + " and atm_application_id = '$atmid' "
                       + " and  dispute_with_roles = 1";
        }
        String selectStat = "select (select name from atm_file where id = t.file_id) filename,t.loading_date loadingdate,t.atm_application_id atmapplicationid, (select name from atm_machine where id = t.atm_id)atmname,t.transaction_type transactiontype,t.currency currency,t.transaction_status transactionstatus,t.response_code responsecode,t.transaction_date transactiondate,"+super.getcardcolumntalias()+" cardnum,t.amount amount,t.settlement_date settlementdate,t.notes_presented notespresented,t.customer_account_number customeraccountnumber,t.transaction_sequence_order_by transactionsequence,t.dispute_reason disputereason,decode(record_type,1,'J',2,'S',3,'H')recordtype,t.settled_date settleddate,t.comments comments,(select user_name from users where user_id = t.settled_user)settleduser,t.dispute_date disputedate, t.abs_amount absamount,t.column4 NotesPresented,t.column3 Autho,t.column2 GLACCOUNTATM,t.column1 Branch,"+super.gettransactiondatacolumntalias()+" transdata from $table t where $cond";
        selectStat = selectStat.replace("$table", "Disputes");
        selectStat = selectStat.replace("$cond", " " + Condition);
        selectStat = selectStat.replace("$matchingtype", "" + Searchparm.getMatchingtype());
        selectStat = selectStat.replace("$user", "" + Searchparm.getUserid());
        selectStat = selectStat.replace("$recordtype", "" + Searchparm.getRecordtype());
        selectStat = selectStat.replace("$datefrom", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getFromdate()));
        selectStat = selectStat.replace("$dateto", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getTodate()));
        selectStat = selectStat.replace("$atmid", "" + Searchparm.getAtmid());
        ResultSet rs = executeQueryReport(selectStat);
        List<disstatprintDTOInter> Record = new ArrayList<disstatprintDTOInter>();
        while (rs.next()) {
            disstatprintDTOInter Records = DTOFactory.createdisstatprintDTO();
            Records.setFilename(rs.getString("filename"));
            Records.setLoadingdate(rs.getString("loadingdate"));
            Records.setAtmapplicationid(rs.getString("atmapplicationid"));
            Records.setAtmname(rs.getString("atmname"));
            Records.setTransactiontype(rs.getString("transactiontype"));
            Records.setCurrency(rs.getString("currency"));
            Records.setTransactionstatus(rs.getString("transactionstatus"));
            Records.setResponsecode(rs.getString("responsecode"));
            Records.setTransactiondate(rs.getString("transactiondate"));
            Records.setCardnum(rs.getString("cardnum"));
            Records.setAmount(rs.getString("amount"));
            Records.setSettlementdate(rs.getString("settlementdate"));
            Records.setNotesPresented(rs.getString("notespresented"));
            Records.setCustomeraccountnumber(rs.getString("customeraccountnumber"));
            Records.setTransactionsequence(rs.getString("transactionsequence"));
            Records.setDisputereason(rs.getString("disputereason"));
            Records.setRecordtype(rs.getString("recordtype"));
            Records.setSettleddate(rs.getString("settleddate"));
            Records.setComments(rs.getString("comments"));
            Records.setSettleduser(rs.getString("settleduser"));
            Records.setDisputedate(rs.getString("disputedate"));
            Records.setAbsamount(rs.getString("absamount"));
            Records.setNotesPresented(rs.getString("NotesPresented"));
            Records.setAutho(rs.getString("Autho"));
            Records.setGLACCOUNTATM(rs.getString("GLACCOUNTATM"));
            Records.setBranch(rs.getString("Branch"));
            Records.setTransdata(rs.getString("transdata"));
            Record.add(Records);
        }
        super.postSelect(rs);
        return Record;
    }
  public Object GetTransactionStatDetailDispexcel(Object... obj) throws Throwable {
        super.preSelect();
        String Condition = "";
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        if (Searchparm.getSearchtype().equals("Disputes")) {
            Condition = " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype  "
                    + " and matching_type = $matchingtype  "
                    + " and dispute_key is not null and dispute_with_roles = 2";
        } else if (Searchparm.getSearchtype().equals("MissingSide")) {
            Condition = " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype  "
                    + " and matching_type = $matchingtype  "
                    + " and dispute_key is null and dispute_with_roles = 2 ";
        } else if (Searchparm.getSearchtype().equals("Errors")) {
            Condition = " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                    + " and record_type = $recordtype "
                    + " and matching_type = $matchingtype "
                       + " and  dispute_with_roles = 1";
        }
         String selectStat = "select (select name from atm_file where id = t.file_id) filename,t.loading_date loadingdate,t.atm_application_id atmapplicationid, (select name from atm_machine where id = t.atm_id)atmname,t.transaction_type transactiontype,t.currency currency,t.transaction_status transactionstatus,t.response_code responsecode,t.transaction_date transactiondate,"+super.getcardcolumntalias()+" cardnum,t.amount amount,t.settlement_date settlementdate,t.notes_presented notespresented,t.customer_account_number customeraccountnumber,t.transaction_sequence_order_by transactionsequence,t.dispute_reason disputereason,decode(record_type,1,'J',2,'S',3,'H')recordtype,t.settled_date settleddate,t.comments comments,(select user_name from users where user_id = t.settled_user)settleduser,t.dispute_date disputedate, t.abs_amount absamount,t.column4 NotesPresented,t.column3 Autho,t.column2 GLACCOUNTATM,t.column1 Branch,"+super.gettransactiondatacolumntalias()+" transdata from $table t where $cond";
        selectStat = selectStat.replace("$table", "Disputes");
        selectStat = selectStat.replace("$cond", " " + Condition);
        selectStat = selectStat.replace("$matchingtype", "" + Searchparm.getMatchingtype());
        selectStat = selectStat.replace("$user", "" + Searchparm.getUserid());
        selectStat = selectStat.replace("$recordtype", "" + Searchparm.getRecordtype());
        selectStat = selectStat.replace("$datefrom", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getFromdate()));
        selectStat = selectStat.replace("$dateto", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getTodate()));
        ResultSet rs = executeQueryReport(selectStat);
        List<disstatprintDTOInter> Record = new ArrayList<disstatprintDTOInter>();
        while (rs.next()) {
            disstatprintDTOInter Records = DTOFactory.createdisstatprintDTO();
            Records.setFilename(rs.getString("filename"));
            Records.setLoadingdate(rs.getString("loadingdate"));
            Records.setAtmapplicationid(rs.getString("atmapplicationid"));
            Records.setAtmname(rs.getString("atmname"));
            Records.setTransactiontype(rs.getString("transactiontype"));
            Records.setCurrency(rs.getString("currency"));
            Records.setTransactionstatus(rs.getString("transactionstatus"));
            Records.setResponsecode(rs.getString("responsecode"));
            Records.setTransactiondate(rs.getString("transactiondate"));
            Records.setCardnum(rs.getString("cardnum"));
            Records.setAmount(rs.getString("amount"));
            Records.setSettlementdate(rs.getString("settlementdate"));
            Records.setNotesPresented(rs.getString("notespresented"));
            Records.setCustomeraccountnumber(rs.getString("customeraccountnumber"));
            Records.setTransactionsequence(rs.getString("transactionsequence"));
            Records.setDisputereason(rs.getString("disputereason"));
            Records.setRecordtype(rs.getString("recordtype"));
            Records.setSettleddate(rs.getString("settleddate"));
            Records.setComments(rs.getString("comments"));
            Records.setSettleduser(rs.getString("settleduser"));
            Records.setDisputedate(rs.getString("disputedate"));
            Records.setAbsamount(rs.getString("absamount"));
            Records.setNotesPresented(rs.getString("NotesPresented"));
            Records.setAutho(rs.getString("Autho"));
            Records.setGLACCOUNTATM(rs.getString("GLACCOUNTATM"));
            Records.setBranch(rs.getString("Branch"));
            Records.setTransdata(rs.getString("transdata"));
            Record.add(Records);
        }
        super.postSelect(rs);
        return Record;
    }
    public Object findBingoStatAll(Object... obj) throws Throwable {
        super.preSelect();
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        String selectStat = " select s.application_id ,(select count(*)TOTAL "
                + " from matched_data  "
                + " where  transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                + " and record_type = $recordtype "
                + " and matching_type = $matchingtype  "
                + " and atm_id = s.id) Matched,(select COUNT(*)TOTAL from disputes  where   "
                + " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                + " and record_type = $recordtype  "
                + " and matching_type = $matchingtype  "
                + " and dispute_key is not null and dispute_with_roles = 2"
                + " and atm_id = s.id)Disputes,(select COUNT(*)TOTAL from disputes  where   "
                + " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                + " and record_type = $recordtype  "
                + " and matching_type = $matchingtype  "
                + " and dispute_key is null and dispute_with_roles = 2"
                + " and atm_id = s.id)MissingSide,(select COUNT(*)TOTAL from disputes  where   "
                + " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                + " and record_type = $recordtype "
                + " and matching_type = $matchingtype "
                + " and  dispute_with_roles = 1"
                + " and atm_id = s.id)Errors "
                + " from atm_machine s  "
                + " Where id in (select atm_id from user_atm where user_id = $user) "
                + " and atm_valid(id)= 1 "
                + " order by matched desc,disputes desc,MissingSide desc,errors desc ";
        selectStat = selectStat.replace("$matchingtype", "" + Searchparm.getMatchingtype());
        selectStat = selectStat.replace("$user", "" + Searchparm.getUserid());
        selectStat = selectStat.replace("$recordtype", "" + Searchparm.getRecordtype());
        selectStat = selectStat.replace("$datefrom", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getFromdate()));
        selectStat = selectStat.replace("$dateto", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getTodate()));
        ResultSet result = executeQuery(selectStat);
        List<TransStatDTOInter> Records = new ArrayList<TransStatDTOInter>();
        while (result.next()) {
            TransStatDTOInter record = DTOFactory.createTransStatDTO();
            record.setErrors(result.getString("Errors"));
            record.setMatched(result.getString("Matched"));
            record.setMissingside(result.getString("MissingSide"));
            record.setDisputes(result.getString("Disputes"));
            record.setApplicationid(result.getString("application_id"));
            Records.add(record);
        }
        super.postSelect(result);
        return Records;
    }

    public Object findBingoStatAllCount(Object... obj) throws Throwable {
        super.preSelect();
        BingostatsearchDTOInter Searchparm = (BingostatsearchDTOInter) obj[0];
        String selectStat = " select  (select count(atm_id) from user_atm where user_id = $user) atmcount ,(select count(*)TOTAL "
                + " from matched_data  "
                + " where  transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                + " and record_type = $recordtype "
                + " and matching_type = $matchingtype  "
                + " ) Matched,(select COUNT(*)TOTAL from disputes  where   "
                + " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                + " and record_type = $recordtype  "
                + " and matching_type = $matchingtype  "
                + " and dispute_key is not null and dispute_with_roles = 2"
                + " )Disputes,(select COUNT(*)TOTAL from disputes  where   "
                + " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                + " and record_type = $recordtype  "
                + " and matching_type = $matchingtype  "
                + " and dispute_key is null and dispute_with_roles = 2 "
                + " )MissingSide,(select COUNT(*)TOTAL from disputes  where   "
                + " transaction_date between to_date(to_char(nvl(to_date('$datefrom','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$dateto','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') "
                + " and record_type = $recordtype "
                + " and matching_type = $matchingtype "
                + " and  dispute_with_roles = 1"
                + " )Errors "
                + " from atm_machine s  "
                + " Where id in (select atm_id from user_atm where user_id = $user) "
                + " and atm_valid(id)= 1 "
                + " and rownum = 1";
        selectStat = selectStat.replace("$matchingtype", "" + Searchparm.getMatchingtype());
        selectStat = selectStat.replace("$user", "" + Searchparm.getUserid());
        selectStat = selectStat.replace("$recordtype", "" + Searchparm.getRecordtype());
        selectStat = selectStat.replace("$datefrom", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getFromdate()));
        selectStat = selectStat.replace("$dateto", "" + DateFormatter.changeDateAndTimeFormat(Searchparm.getTodate()));
        ResultSet result = executeQuery(selectStat);
        TransStatDTOInter record = DTOFactory.createTransStatDTO();
        while (result.next()) {
            record.setErrorscount(result.getString("Errors"));
            record.setMatchedcount(result.getString("Matched"));
            record.setMissingsidecount(result.getString("MissingSide"));
            record.setDisputescount(result.getString("Disputes"));
            record.setApplicationidcount(result.getString("atmcount"));
        }
        super.postSelect(result);
        return record;
    }
}
