/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.RepStat13DTOInter;
import com.ev.AtmBingo.bus.dto.RepStat2DTOInter;
import com.ev.AtmBingo.bus.dto.RepStat4DTOInter;
import com.ev.AtmBingo.bus.dto.RepStat56DTOInter;
import com.ev.AtmBingo.bus.dto.atmremainingDTOInter;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class RepStatDAO extends BaseDAO implements RepStatDAOInter {

    @Override
    public Object findReport1(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable {
        super.preSelect();
        String selectStatment = "  SELECT   datefrom,\n"
                + "           dateto,\n"
                + "           SUM (Cash_Added) Cash_Added,\n"
                + "           SUM (Remaining_Cash) Remaining_Cash,\n"
                + "           SUM (Despensed_Cash) Despensed_Cash,\n"
                + "           switch,\n"
                + "           DISPUTE,\n"
                + "           SUM (NOTES_RESENTED) NOTES_RESENTED,\n"
                + "           CASH_OVER,\n"
                + "           DECODE (SIGN (switch - journal), -1, ABS (switch - journal), 0)\n"
                + "              Switch_Pending,\n"
                + "           DECODE (SIGN (switch - journal), -1, 0, (switch - journal))\n"
                + "              Switch_Over,\n"
                + "           rep_id,\n"
                + "           (SELECT   t.application_id\n"
                + "              FROM   ATM_MACHINE t\n"
                + "             WHERE   t.id = atm_id)\n"
                + "              applicationid,\n"
                + "           (SELECT   AM.NAME\n"
                + "              FROM   ATM_MACHINE AM\n"
                + "             WHERE   AM.ID = ATM_ID)\n"
                + "              ATM_NAME,\n"
                + "           atm_id\n"
                + "    FROM   (SELECT   master.date_from datefrom,\n"
                + "                     master.date_to dateto,\n"
                + "                     (CASSETE_VALUE * CASSETE_AMOUNT) Cash_Added,\n"
                + "                     REMAINING_AMOUNT Remaining_Cash,\n"
                + "                     ( (CASSETE_VALUE * CASSETE_AMOUNT)\n"
                + "                      - NVL (REMAINING_AMOUNT, 0))\n"
                + "                        Despensed_Cash,\n"
                + "                     NVL (master.switch, 0) switch,\n"
                + "                     master.id rep_id,\n"
                + "                     MASTER.DISPUTES DISPUTE,\n"
                + "                     atm_id,\n"
                + "                     NVL (MASTER.JOURNAL, 0) JOURNAL,\n"
                + "                     NVL (master.cash_over, 0) cash_over,\n"
                + "                     nvl(NOTES_RESENTED,0)NOTES_RESENTED\n"
                + "              FROM   REPLANISHMENT_DETAIL detail, REPLANISHMENT_MASTER master\n"
                + "             WHERE       master.id = detail.id\n"
                + "                     AND (atm_id = $P{ATMIDInt} OR $P{ATMIDInt} = 999999)\n"
                + "                     AND atm_valid (atm_id) = 1\n"
                + "                                     AND (ATM_ID IN\n"
                + "                                (SELECT   id\n"
                + "                                   FROM   atm_machine\n"
                + "                                  WHERE   atm_group IN\n"
                + "                                                (SELECT   id\n"
                + "                                                   FROM   atm_group\n"
                + "                                                  WHERE   parent_id IN\n"
                + "                                                                (SELECT   id\n"
                + "                                                                   FROM   atm_group\n"
                + "                                                                  WHERE   (parent_id =\n"
                + "                                                                              $P{AtmGroupInt}\n"
                + "                                                                           OR id =\n"
                + "                                                                               $P{AtmGroupInt}))\n"
                + "                                                          OR id = $P{AtmGroupInt}))\n"
                + "                          OR $P{AtmGroupInt} = 0)\n"
                + "                     AND date_from >=\n"
                + "                           TO_DATE ($P{DateFrom}, 'dd-mm-yyyy hh24:mi:ss')\n"
                + "                     AND date_to <=\n"
                + "                           TO_DATE ($P{DateTo}, 'dd-mm-yyyy hh24:mi:ss'))\n"
                + "GROUP BY   rep_id,\n"
                + "           datefrom,\n"
                + "           dateto,\n"
                + "           switch,\n"
                + "           dispute,\n"
                + "           atm_id,\n"
                + "           JOURNAL,\n"
                + "           cash_over\n"
                + "ORDER BY   atm_Name, datefrom";
        selectStatment = selectStatment.replace("$P{DateFrom}", "'" + dateFrom + "'");
        selectStatment = selectStatment.replace("$P{DateTo}", "'" + dateTo + "'");
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroup);
        selectStatment = selectStatment.replace("$P{ATMIDInt}", "" + atmid);
        List<RepStat13DTOInter> Records = new ArrayList<RepStat13DTOInter>();
        ResultSet rs = executeQueryReport(selectStatment);
        while (rs.next()) {
            RepStat13DTOInter record = DTOFactory.createRepStat13DTO();
            record.setDatefrom(rs.getTimestamp("datefrom"));
            record.setDateto(rs.getTimestamp("dateto"));
            record.setCashadded(rs.getInt("Cash_Added"));
            record.setDispensedcash(rs.getInt("Despensed_Cash"));
            record.setRemainingcash(rs.getInt("Remaining_Cash"));
            record.setSwitchover(rs.getInt("Switch_Over"));
            record.setCashover(rs.getInt("CASH_OVER"));
            record.setCashshortage(rs.getInt("NOTES_RESENTED"));
            record.setSwitchpending(rs.getInt("Switch_Pending"));
            record.setSwitchtotal(rs.getInt("switch"));
            record.setDispute(rs.getInt("DISPUTE"));
            record.setAtmid(rs.getString("applicationid"));
            record.setAtmname(rs.getString("ATM_NAME"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findReport2(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable {
        super.preSelect();
        String selectStatment = "SELECT   TAB.ID id,\n"
                + "           TAB.DATE_FROM DATE_FROM,\n"
                + "           TAB.DATE_TO DATE_TO,\n"
                + "           TAB.SWITCH SWITCH,\n"
                + "           TAB.NOTES_PRESENTED NOTES_PRESENTED,\n"
                + "           dispensed_amount,\n"
                + "           cash_over,\n"
                + "           DECODE (SIGN (TAB.switch - NVL (TAB.NOTES_PRESENTED, 0)),\n"
                + "                   -1,\n"
                + "                   'NOTES > SWITCH',\n"
                + "                   1,\n"
                + "                   'switch > NOTES',\n"
                + "                   0,\n"
                + "                   'ZERO')\n"
                + "              DIFF_name,\n"
                + "           ABS (TAB.switch - NVL (TAB.NOTES_PRESENTED, 0)) DIFF_value,\n"
                + "           (SELECT   (SELECT   AM.NAME\n"
                + "                        FROM   ATM_MACHINE AM\n"
                + "                       WHERE   AM.ID = RM.ATM_ID)\n"
                + "                        ATM_NAME\n"
                + "              FROM   REPLANISHMENT_MASTER RM\n"
                + "             WHERE   RM.ID = TAB.ID)\n"
                + "              ATMNAME,\n"
                + "           (SELECT   (SELECT   AM.application_id\n"
                + "                        FROM   ATM_MACHINE AM\n"
                + "                       WHERE   AM.ID = RM.ATM_ID)\n"
                + "                        ATM_NAME\n"
                + "              FROM   REPLANISHMENT_MASTER RM\n"
                + "             WHERE   RM.ID = TAB.ID)\n"
                + "              ATMID\n"
                + "    FROM   (SELECT   id,\n"
                + "                     DATE_FROM,\n"
                + "                     DATE_TO,\n"
                + "                     SWITCH,\n"
                + "                     (SELECT   NVL (SUM (NOTES_RESENTED), 0) NOTES_RESENTED\n"
                + "                        FROM   REPLANISHMENT_DETAIL b\n"
                + "                       WHERE   rep.id = b.id)\n"
                + "                        NOTES_PRESENTED,\n"
                + "                     (SELECT   NVL (SUM (TOTAL_AMOUNT), 0)\n"
                + "                        FROM   REPLANISHMENT_DETAIL b\n"
                + "                       WHERE   rep.id = b.id)\n"
                + "                        dispensed_amount,\n"
                + "                     NVL (journal, 0) journal,\n"
                + "                     NVL (cash_over, 0) cash_over\n"
                + "              FROM   REPLANISHMENT_MASTER rep\n"
                + "             WHERE   atm_valid (atm_id) = 1\n"
                + "                     AND EXISTS\n"
                + "                           (SELECT   1\n"
                + "                              FROM   user_atm a\n"
                + "                             WHERE   a.atm_id = rep.atm_id AND a.user_id = 1)\n"
                + "                      AND (Date_from >=\n"
                + "                           TO_DATE ($P{DateFrom},\n"
                + "                                    'dd.mm.yyyy hh24:mi:ss'))\n"
                + "                   AND (Date_to <=\n"
                + "                           TO_DATE ($P{DateTo},\n"
                + "                                    'dd.mm.yyyy hh24:mi:ss'))\n"
                + "                   AND (atm_id = $P{ATMIDInt} OR $P{ATMIDInt} = 999999)\n"
                + "                   AND (ATM_ID IN\n"
                + "                              (SELECT   id\n"
                + "                                 FROM   atm_machine\n"
                + "                                WHERE   atm_group IN\n"
                + "                                              (SELECT   id\n"
                + "                                                 FROM   atm_group\n"
                + "                                                WHERE   parent_id IN\n"
                + "                                                              (SELECT   id\n"
                + "                                                                 FROM\n"
                + "      atm_group\n"
                + "                                                                WHERE   (\n"
                + "      parent_id =\n"
                + "                                                                            $P{AtmGroupInt} OR id= $P{AtmGroupInt})) OR id = $P{AtmGroupInt}))\n"
                + "                        OR $P{AtmGroupInt} = 0)) TAB\n"
                + "ORDER BY   ATMNAME, TAB.DATE_FROM";
        selectStatment = selectStatment.replace("$P{DateFrom}", "'" + dateFrom + "'");
        selectStatment = selectStatment.replace("$P{DateTo}", "'" + dateTo + "'");
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroup);
        selectStatment = selectStatment.replace("$P{ATMIDInt}", "" + atmid);
        List<RepStat2DTOInter> Records = new ArrayList<RepStat2DTOInter>();
        ResultSet rs = executeQueryReport(selectStatment);
        while (rs.next()) {
            RepStat2DTOInter record = DTOFactory.createRepStat2DTO();
            record.setDatefrom(rs.getTimestamp("DATE_FROM"));
            record.setDateto(rs.getTimestamp("DATE_TO"));
            record.setDifferentname(rs.getString("DIFF_name"));
            record.setDifferentvalue(rs.getInt("DIFF_value"));
            record.setNotepres(rs.getInt("NOTES_PRESENTED"));
            record.setCashover(rs.getInt("CASH_OVER"));
            record.setSwitchtotal(rs.getInt("SWITCH"));
            record.setAtmid(rs.getString("ATMNAME"));
            record.setAtmname(rs.getString("ATMID"));
            record.setId(rs.getString("id"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findReport3(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable {
        super.preSelect();
        String selectStatment = "SELECT   datefrom,\n"
                + "           dateto,\n"
                + "           Cash_Added,\n"
                + "           Remaining_Cash,\n"
                + "           Despensed_Cash,\n"
                + "           switch,\n"
                + "           DISPUTE,\n"
                + "           NOTES_RESENTED,\n"
                + "           CASH_OVER,\n"
                + "           Switch_Pending,\n"
                + "           Switch_Over,\n"
                + "           atm_name,\n"
                + "           atmid,\n"
                + "           rep_id\n"
                + "    FROM   (  SELECT   datefrom,\n"
                + "                       dateto,\n"
                + "                       SUM (Cash_Added) Cash_Added,\n"
                + "                       SUM (Remaining_Cash) Remaining_Cash,\n"
                + "                       SUM (Despensed_Cash) Despensed_Cash,\n"
                + "                       switch,\n"
                + "                       DISPUTE,\n"
                + "                       SUM (NOTES_RESENTED) NOTES_RESENTED,\n"
                + "                       CASH_OVER,\n"
                + "                       NVL (\n"
                + "                          DECODE (SIGN (switch - journal),\n"
                + "                                  -1, ABS (switch - journal),\n"
                + "                                  0),\n"
                + "                          0\n"
                + "                       )\n"
                + "                          Switch_Pending,\n"
                + "                       NVL (\n"
                + "                          DECODE (SIGN (switch - journal),\n"
                + "                                  -1, 0,\n"
                + "                                  (switch - journal)),\n"
                + "                          0\n"
                + "                       )\n"
                + "                          Switch_Over,\n"
                + "                       (SELECT   (SELECT   AM.NAME\n"
                + "                                    FROM   ATM_MACHINE AM\n"
                + "                                   WHERE   AM.ID = RM.ATM_ID)\n"
                + "                                    ATM_ID\n"
                + "                          FROM   REPLANISHMENT_MASTER RM\n"
                + "                         WHERE   RM.ID = rep_id)\n"
                + "                          atm_name,\n"
                + "                       (SELECT   (SELECT   t.application_id\n"
                + "                                    FROM   ATM_MACHINE t\n"
                + "                                   WHERE   t.ID = RM.ATM_ID)\n"
                + "                                    ATM\n"
                + "                          FROM   REPLANISHMENT_MASTER RM\n"
                + "                         WHERE   RM.ID = rep_id)\n"
                + "                          atmid,\n"
                + "                       rep_id\n"
                + "                FROM   (SELECT   master.date_from datefrom,\n"
                + "                                 master.date_to dateto,\n"
                + "                                 (CASSETE_VALUE * CASSETE_AMOUNT) Cash_Added,\n"
                + "                                 REMAINING_AMOUNT Remaining_Cash,\n"
                + "                                 ( (CASSETE_VALUE * CASSETE_AMOUNT)\n"
                + "                                  - NVL (REMAINING_AMOUNT, 0))\n"
                + "                                    Despensed_Cash,\n"
                + "                                 master.switch,\n"
                + "                                 master.id rep_id,\n"
                + "                                 MASTER.DISPUTES DISPUTE,\n"
                + "                                 NVL (MASTER.JOURNAL, 0) JOURNAL,\n"
                + "                                 NVL (master.cash_over, 0) cash_over,\n"
                + "                                 NVL (detail.NOTES_RESENTED, 0) NOTES_RESENTED\n"
                + "                          FROM   REPLANISHMENT_DETAIL detail,\n"
                + "                                 REPLANISHMENT_MASTER master\n"
                + "                           WHERE       master.id = detail.id\n"
                + "                               AND atm_valid (atm_id) = 1\n"
                + "                               AND (atm_id = $P{ATMIDInt} OR $P{ATMIDInt} = 999999)\n"
                + "                               AND (ATM_ID IN\n"
                + "                                          (SELECT   id\n"
                + "                                             FROM   atm_machine\n"
                + "                                            WHERE   atm_group IN\n"
                + "                                                          (SELECT   id\n"
                + "                                                             FROM   atm_group\n"
                + "                                                            WHERE   parent_id IN\n"
                + "                                                                          (SELECT   id\n"
                + "                                                                             FROM   atm_group\n"
                + "                                                                            WHERE   (parent_id =\n"
                + "                                                                                        $P{AtmGroupInt}\n"
                + "                                                                                     OR id =\n"
                + "                                                                                          $P{AtmGroupInt}))\n"
                + "                                                                    OR id = $P{AtmGroupInt}))\n"
                + "                                    OR $P{AtmGroupInt} = 0)\n"
                + "                               AND date_from >=\n"
                + "                                     TO_DATE ($P{DateFrom},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "                               AND date_to <=\n"
                + "                                     TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss'))\n"
                + "            GROUP BY   rep_id,\n"
                + "                       datefrom,\n"
                + "                       dateto,\n"
                + "                       switch,\n"
                + "                       dispute,\n"
                + "                       JOURNAL,\n"
                + "                       CASH_OVER\n"
                + "            ORDER BY   datefrom)\n"
                + "   WHERE   NOT EXISTS (SELECT   1\n"
                + "                         FROM   DUAL\n"
                + "                        WHERE   switch_pending = 0 AND switch_over = 0 AND CASH_OVER = 0)\n"
                + "ORDER BY   atm_name, datefrom";
        selectStatment = selectStatment.replace("$P{DateFrom}", "'" + dateFrom + "'");
        selectStatment = selectStatment.replace("$P{DateTo}", "'" + dateTo + "'");
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroup);
        selectStatment = selectStatment.replace("$P{ATMIDInt}", "" + atmid);
        List<RepStat13DTOInter> Records = new ArrayList<RepStat13DTOInter>();
        ResultSet rs = executeQueryReport(selectStatment);
        while (rs.next()) {
            RepStat13DTOInter record = DTOFactory.createRepStat13DTO();
            record.setDatefrom(rs.getTimestamp("datefrom"));
            record.setDateto(rs.getTimestamp("dateto"));
            record.setCashadded(rs.getInt("Cash_Added"));
            record.setRepid(rs.getBigDecimal("rep_id").toString());
            record.setDispensedcash(rs.getInt("Despensed_Cash"));
            record.setRemainingcash(rs.getInt("Remaining_Cash"));
            record.setSwitchover(rs.getInt("Switch_Over"));
            record.setCashover(rs.getInt("CASH_OVER"));
            record.setCashshortage(rs.getInt("NOTES_RESENTED"));
            record.setSwitchpending(rs.getInt("Switch_Pending"));
            record.setSwitchtotal(rs.getInt("switch"));
            record.setDispute(rs.getInt("DISPUTE"));
            record.setAtmid(rs.getString("atmid"));
            record.setAtmname(rs.getString("atm_name"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findReport4(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable {
        super.preSelect();
        String selectStatment = "SELECT   id,\n"
                + "           date_from,\n"
                + "           date_to,\n"
                + "           (SELECT   m.name\n"
                + "              FROM   atm_machine m\n"
                + "             WHERE   m.id = atm_id)\n"
                + "              atmname,\n"
                + "           (SELECT   t.application_id\n"
                + "              FROM   ATM_MACHINE t\n"
                + "             WHERE   t.id = atm_id)\n"
                + "              applicationid,\n"
                + "           NVL (SWITCH_HOST, 0) switch,\n"
                + "           NVL (HOST_ONUS, 0) HOST_ONUS,\n"
                + "           NVL (HOST_OFFUS, 0) HOST_OFFUS,\n"
                + "           NVL (\n"
                + "              DECODE (\n"
                + "                 SIGN(NVL (SWITCH_HOST, 0)\n"
                + "                      - (NVL (HOST_ONUS, 0) + NVL (HOST_OFFUS, 0))),\n"
                + "                 -1,\n"
                + "                 0,\n"
                + "                 (NVL (SWITCH_HOST, 0)\n"
                + "                  - (NVL (HOST_ONUS, 0) + NVL (HOST_OFFUS, 0)))\n"
                + "              ),\n"
                + "              0\n"
                + "           )\n"
                + "              switch_diff,\n"
                + "           NVL (\n"
                + "              DECODE (SIGN (NVL (SWITCH_HOST, 0) - NVL (HOST_ONUS, 0)),\n"
                + "                      -1, ABS (NVL (SWITCH_HOST, 0) - NVL (HOST_ONUS, 0)),\n"
                + "                      0),\n"
                + "              0\n"
                + "           )\n"
                + "              onus_diff,\n"
                + "           NVL (\n"
                + "              DECODE (SIGN (NVL (SWITCH_HOST, 0) - NVL (HOST_OFFUS, 0)),\n"
                + "                      -1, ABS (NVL (SWITCH_HOST, 0) - NVL (HOST_OFFUS, 0)),\n"
                + "                      0),\n"
                + "              0\n"
                + "           )\n"
                + "              offus_diff\n"
                + "\n"
                + "  FROM   replanishment_master\n"
                + "\n"
                + "WHERE   date_from >=\n"
                + "\n"
                + "            TO_DATE ($P{DateFrom}, 'dd-mm-yyyy hh24:mi:ss')\n"
                + "\n"
                + "         AND date_to <=\n"
                + "\n"
                + "               TO_DATE ($P{DateTo}, 'dd-mm-yyyy hh24:mi:ss')\n"
                + "\n"
                + "                AND (atm_valid($P{ATMIDInt}) = 1  OR $P{ATMIDInt} = 999999)\n"
                + "\n"
                + "         AND (atm_id = $P{ATMIDInt} OR $P{ATMIDInt} = 999999)\n"
                + "\n"
                + "         AND (ATM_ID IN\n"
                + "\n"
                + "                    (SELECT   id\n"
                + "\n"
                + "                       FROM   atm_machine\n"
                + "\n"
                + "                      WHERE   atm_group IN\n"
                + "\n"
                + "                                    (SELECT   id\n"
                + "\n"
                + "                                       FROM   atm_group\n"
                + "\n"
                + "                                      WHERE   parent_id IN\n"
                + "\n"
                + "                                                    (SELECT   id\n"
                + "\n"
                + "                                                       FROM   atm_group\n"
                + "\n"
                + "                                                      WHERE   (parent_id =\n"
                + "\n"
                + "                                                                  $P{AtmGroupInt}\n"
                + "\n"
                + "                                                               OR id =\n"
                + "\n"
                + "                                                                    $P{AtmGroupInt}))\n"
                + "\n"
                + "                                              OR id = $P{AtmGroupInt}))\n"
                + "\n"
                + "              OR $P{AtmGroupInt} = 0)\n"
                + "\n"
                + "order by atmname,Date_from";
        selectStatment = selectStatment.replace("$P{DateFrom}", "'" + dateFrom + "'");
        selectStatment = selectStatment.replace("$P{DateTo}", "'" + dateTo + "'");
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroup);
        selectStatment = selectStatment.replace("$P{ATMIDInt}", "" + atmid);
        List<RepStat4DTOInter> Records = new ArrayList<RepStat4DTOInter>();
        ResultSet rs = executeQueryReport(selectStatment);
        while (rs.next()) {
            RepStat4DTOInter record = DTOFactory.createRepStat4DTO();
            record.setDatefrom(rs.getTimestamp("date_from"));
            record.setDateto(rs.getTimestamp("date_to"));
            record.setAtm(rs.getString("atmname"));
            record.setAtmid(rs.getString("applicationid"));
            record.setHostamount(rs.getInt("HOST_ONUS"));
            record.setHostdifference(rs.getInt("onus_diff"));
            record.setOffsamount(rs.getInt("HOST_OFFUS"));
            record.setOffusdifference(rs.getInt("offus_diff"));
            record.setSwitchamount(rs.getInt("switch"));
            record.setSwitchdifference(rs.getInt("switch_diff"));
            record.setId(rs.getBigDecimal("id").toString());
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findReport5(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable {
        super.preSelect();
        String selectStatment = "SELECT   *\n"
                + "    FROM   (SELECT   id,\n"
                + "                     date_from,\n"
                + "                     date_to,\n"
                + "                     (SELECT   t.name\n"
                + "                        FROM   ATM_MACHINE t\n"
                + "                       WHERE   t.id = atm_id)\n"
                + "                        atmname,\n"
                + "                     (SELECT   t.application_id\n"
                + "                        FROM   ATM_MACHINE t\n"
                + "                       WHERE   t.id = atm_id)\n"
                + "                        applicationid,\n"
                + "                     NVL (SWITCH_HOST, 0) switch,\n"
                + "                     NVL (HOST_ONUS, 0) HOST_ONUS,\n"
                + "                     NVL (HOST_OFFUS, 0) HOST_OFFUS,\n"
                + "                     NVL (\n"
                + "                        DECODE (SIGN (nvl(SWITCH_HOST,0) - (nvl(HOST_ONUS,0) +\n"
                + "      nvl(HOST_OFFUS,0))),\n"
                + "                                -1, 0,\n"
                + "                                (nvl(SWITCH_HOST,0) - (nvl(HOST_ONUS,0) + nvl(\n"
                + "      HOST_OFFUS,0)))),\n"
                + "                        0\n"
                + "                     )\n"
                + "                        switch_diff,\n"
                + "                     NVL (\n"
                + "                        DECODE (SIGN (nvl(SWITCH_HOST,0) - nvl(HOST_ONUS,0)),\n"
                + "                                -1, ABS (nvl(SWITCH_HOST,0) - nvl(HOST_ONUS,0)\n"
                + "      ),\n"
                + "                                0),\n"
                + "                        0\n"
                + "                     )\n"
                + "                        onus_diff,\n"
                + "                     NVL (\n"
                + "                        DECODE (SIGN (nvl(SWITCH_HOST,0) - nvl(HOST_OFFUS,0)),\n"
                + "                                -1, ABS (nvl(SWITCH_HOST,0) - nvl(HOST_OFFUS,0\n"
                + "      )),\n"
                + "                                0),\n"
                + "                        0\n"
                + "                     )\n"
                + "                        offus_diff\n"
                + "\n"
                + "            FROM   replanishment_master\n"
                + "\n"
                + "           WHERE   date_from >=\n"
                + "\n"
                + "                      TO_DATE ($P{DateFrom},\n"
                + "\n"
                + "                               'dd-mm-yyyy hh24:mi:ss')\n"
                + "\n"
                + "                   AND date_to <=\n"
                + "\n"
                + "                         TO_DATE ($P{DateTo},\n"
                + "\n"
                + "                                  'dd-mm-yyyy hh24:mi:ss')\n"
                + "\n"
                + "                   AND (atm_id = $P{ATMIDInt} OR $P{ATMIDInt} = 999999)\n"
                + "\n"
                + "                   AND (atm_valid (ATM_ID) = 1 OR $P{ATMIDInt} = 999999)\n"
                + "\n"
                + "                   AND (ATM_ID IN\n"
                + "\n"
                + "                              (SELECT   id\n"
                + "\n"
                + "                                 FROM   atm_machine\n"
                + "\n"
                + "                                WHERE   atm_group IN\n"
                + "\n"
                + "                                              (SELECT   id\n"
                + "\n"
                + "                                                 FROM   atm_group\n"
                + "\n"
                + "                                                WHERE   parent_id IN\n"
                + "\n"
                + "                                                              (SELECT   id\n"
                + "\n"
                + "                                                                 FROM   atm_group\n"
                + "\n"
                + "                                                                WHERE   (parent_id =\n"
                + "\n"
                + "                                                                            $P{AtmGroupInt}\n"
                + "\n"
                + "                                                                         OR id =\n"
                + "\n"
                + "                                                                              $P{AtmGroupInt}))\n"
                + "\n"
                + "                                                        OR id = $P{AtmGroupInt}))\n"
                + "\n"
                + "                        OR 0 = $P{AtmGroupInt}))\n"
                + "\n"
                + "WHERE   NOT EXISTS\n"
                + "\n"
                + "            (SELECT   1\n"
                + "\n"
                + "               FROM   DUAL\n"
                + "\n"
                + "              WHERE   switch_diff = 0 AND onus_diff = 0 AND offus_diff = 0)\n"
                + "\n"
                + "order by atmname,date_from\n"
                + "\n"
                + " ";
        selectStatment = selectStatment.replace("$P{DateFrom}", "'" + dateFrom + "'");
        selectStatment = selectStatment.replace("$P{DateTo}", "'" + dateTo + "'");
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroup);
        selectStatment = selectStatment.replace("$P{ATMIDInt}", "" + atmid);
        List<RepStat4DTOInter> Records = new ArrayList<RepStat4DTOInter>();
        ResultSet rs = executeQueryReport(selectStatment);
        while (rs.next()) {
            RepStat4DTOInter record = DTOFactory.createRepStat4DTO();
            record.setDatefrom(rs.getTimestamp("date_from"));
            record.setDateto(rs.getTimestamp("date_to"));
            record.setAtm(rs.getString("atmname"));
            record.setAtmid(rs.getString("applicationid"));
            record.setHostamount(rs.getInt("HOST_ONUS"));
            record.setHostdifference(rs.getInt("onus_diff"));
            record.setOffsamount(rs.getInt("HOST_OFFUS"));
            record.setOffusdifference(rs.getInt("offus_diff"));
            record.setSwitchamount(rs.getInt("switch"));
            record.setSwitchdifference(rs.getInt("switch_diff"));
            record.setId(rs.getBigDecimal("id").toString());
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findReport6(String dateFrom, String dateTo, int atmGroup, String atmid, String network, String status) throws Throwable {
        super.preSelect();
        String selectStatment = "select atm_journal_id, transaction_type, debit_account, amount, currency, currency_rate, "
                + "credit_account, journal_date, journal_refernce, date_from, date_to, rep_id, operation, export_flag, atm_id, branch, indication \n"
                + "from atm_journal WHERE   TRUNC (JOURNAL_DATE) BETWEEN TO_DATE ($P{DateFrom}, 'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                  AND  TO_DATE ($P{DateTo}, 'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND (ATM_ID = $P{ATMIDInt} OR $P{ATMIDInt} = 999999)\n"
                + "         AND (ATM_ID IN\n"
                + "                    (SELECT   APPLICATION_ID\n"
                + "                       FROM   atm_machine\n"
                + "                      WHERE   atm_group IN\n"
                + "                                    (SELECT   id\n"
                + "                                       FROM   atm_group\n"
                + "                                      WHERE   parent_id IN\n"
                + "                                                    (SELECT   id\n"
                + "                                                       FROM   atm_group\n"
                + "                                                      WHERE   (parent_id =\n"
                + "                                                                  $P{AtmGroupInt}\n"
                + "                                                               OR id =\n"
                + "                                                                    $P{AtmGroupInt}))\n"
                + "                                              OR id = $P{AtmGroupInt}))\n"
                + "              OR $P{AtmGroupInt} = 0)\n"
                + "         AND ( (INDICATION LIKE '%123%' AND UPPER ($P{NETWORKTYPE}) LIKE '123')\n"
                + "              OR (INDICATION LIKE '%VISA%'\n"
                + "                  AND UPPER ($P{NETWORKTYPE}) LIKE 'VISA')\n"
                + "              OR (INDICATION LIKE '%MATCHED%'\n"
                + "                  AND UPPER ($P{NETWORKTYPE}) LIKE 'ALL'))\n"
                + "      AND (   (EXPORT_FLAG = 1 AND UPPER ($P{STATUS}) LIKE 'PENDING')\n"
                + "              OR (EXPORT_FLAG = 2 AND UPPER ($P{STATUS}) LIKE 'VALIDATED')\n"
                + "              OR (EXPORT_FLAG = 3 AND UPPER ($P{STATUS}) LIKE 'EXECUTED')\n"
                + "              OR (UPPER ($P{STATUS}) LIKE 'ALL')) order by atm_id,date_from" ;
        selectStatment = selectStatment.replace("$P{DateFrom}", "'" + dateFrom + "'");
        selectStatment = selectStatment.replace("$P{DateTo}", "'" + dateTo + "'");
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroup);
        selectStatment = selectStatment.replace("$P{ATMIDInt}", "" + atmid);
        selectStatment = selectStatment.replace("$P{NETWORKTYPE}", "'" + network + "'");
        selectStatment = selectStatment.replace("$P{STATUS}", "'" + status + "'");
        List<RepStat56DTOInter> Records = new ArrayList<RepStat56DTOInter>();
        ResultSet rs = executeQueryReport(selectStatment);
        while (rs.next()) {
            RepStat56DTOInter record = DTOFactory.createReportRepStat56DTO();
            record.setBranch(rs.getString("branch"));
            record.setCreditaccnumber(rs.getString("credit_account"));
            record.setCreditvaluedateto(rs.getTimestamp("journal_date"));
            record.setDebitaccountnumber(rs.getString("debit_account"));
            record.setDebitamount(rs.getBigDecimal("amount"));
            record.setDebitcurrency(rs.getInt("currency"));
            record.setDebittheirref(rs.getString("journal_refernce"));
            record.setDebitvaluedate(rs.getTimestamp("journal_date"));
            record.setIndication(rs.getString("indication"));
            record.setAtmid(rs.getString("atm_id"));
            record.setProfitcenterdep("11100000");
            record.setFromdate(rs.getTimestamp("date_from"));
            record.setTodate(rs.getTimestamp("date_to"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findReport7(String dateFrom, String dateTo, int atmGroup, String atmid, String network, String status) throws Throwable {
        super.preSelect();
        String selectStatment = "select atm_journal_id, transaction_type, debit_account, amount, currency, currency_rate, "
                + "credit_account, journal_date, journal_refernce, date_from, date_to, rep_id, operation, export_flag, atm_id, branch, indication \n"
                + "  FROM   ATM_JOURNAL\n"
                + " WHERE   TRUNC (JOURNAL_DATE) BETWEEN TO_DATE ($P{DateFrom}, 'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                  AND  TO_DATE ($P{DateTo}, 'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND (ATM_ID = $P{ATMIDInt} OR $P{ATMIDInt} = 999999)\n"
                + "         AND (ATM_ID IN\n"
                + "                    (SELECT   APPLICATION_ID\n"
                + "                       FROM   atm_machine\n"
                + "                      WHERE   atm_group IN\n"
                + "                                    (SELECT   id\n"
                + "                                       FROM   atm_group\n"
                + "                                      WHERE   parent_id IN\n"
                + "                                                    (SELECT   id\n"
                + "                                                       FROM   atm_group\n"
                + "                                                      WHERE   (parent_id =\n"
                + "                                                                  $P{AtmGroupInt}\n"
                + "                                                               OR id =\n"
                + "                                                                    $P{AtmGroupInt}))\n"
                + "                                              OR id = $P{AtmGroupInt}))\n"
                + "              OR $P{AtmGroupInt} = 0)\n"
                + "         AND ( (INDICATION LIKE '%MATCHED%'\n"
                + "                AND UPPER ($P{NETWORKTYPE}) LIKE 'NETWORK')\n"
                + "              OR (INDICATION NOT LIKE '%MATCHED%'\n"
                + "                  AND UPPER ($P{NETWORKTYPE}) LIKE 'REPLENISHMENT')\n"
                + "              OR UPPER ($P{NETWORKTYPE}) LIKE 'ALL')\n"
                + "         AND (   (EXPORT_FLAG = 1 AND UPPER ($P{STATUS}) LIKE 'PENDING')\n"
                + "              OR (EXPORT_FLAG = 2 AND UPPER ($P{STATUS}) LIKE 'VALIDATED')\n"
                + "              OR (EXPORT_FLAG = 3 AND UPPER ($P{STATUS}) LIKE 'EXECUTED')\n"
                + "              OR (UPPER ($P{STATUS}) LIKE 'ALL')) and EXPORT_FLAG <> 0 order by atm_id,date_from";
        selectStatment = selectStatment.replace("$P{DateFrom}", "'" + dateFrom + "'");
        selectStatment = selectStatment.replace("$P{DateTo}", "'" + dateTo + "'");
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroup);
        selectStatment = selectStatment.replace("$P{ATMIDInt}", "" + atmid);
        selectStatment = selectStatment.replace("$P{NETWORKTYPE}", "'" + network + "'");
        selectStatment = selectStatment.replace("$P{STATUS}", "'" + status + "'");
        List<RepStat56DTOInter> Records = new ArrayList<RepStat56DTOInter>();
        ResultSet rs = executeQueryReport(selectStatment);
        while (rs.next()) {
            RepStat56DTOInter record = DTOFactory.createReportRepStat56DTO();
            record.setBranch(rs.getString("branch"));
            record.setCreditaccnumber(rs.getString("credit_account"));
            record.setCreditvaluedateto(rs.getTimestamp("journal_date"));
            record.setDebitaccountnumber(rs.getString("debit_account"));
            record.setDebitamount(rs.getBigDecimal("amount"));
            record.setDebitcurrency(rs.getInt("currency"));
            record.setDebittheirref(rs.getString("journal_refernce"));
            record.setDebitvaluedate(rs.getTimestamp("journal_date"));
            record.setIndication(rs.getString("indication"));
            record.setAtmid(rs.getString("atm_id"));
            record.setProfitcenterdep("11100000");
            record.setFromdate(rs.getTimestamp("date_from"));
            record.setTodate(rs.getTimestamp("date_to"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

}
