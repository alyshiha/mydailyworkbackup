/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.DeleteTransactionsDataDTO;
import com.ev.AtmBingo.bus.dto.DeleteTransactionsDataDTOInter;
import java.sql.CallableStatement;

/**
 *
 * @author Administrator
 */
public class DeleteTransactionsDataDAO extends BaseDAO implements DeleteTransactionsDataDAOInter {

    protected DeleteTransactionsDataDAO() {
        super();
    }

 public Object DeleteTransactionsData(DeleteTransactionsDataDTOInter DTDD) throws Throwable {
        super.preCollable();
        CallableStatement statement = super.executeCallableStatment("{call insert_delete_options(?,?,to_date(?,'dd.mm.yyyy hh24:mi:ss'),to_date(?,'dd.mm.yyyy hh24:mi:ss'),?,to_date(?,'dd.mm.yyyy hh24:mi:ss'))}");
        if( DTDD.getATMid() != 0){
        statement.setInt(1,  DTDD.getATMid());
        }
        else{statement.setString(1,"");}
         if( DTDD.getFileid() != 0){
        statement.setInt(2,  DTDD.getFileid());
        }
        else{statement.setString(2,"");}
        
        statement.setString(3, DateFormatter.changeDateAndTimeFormat(DTDD.getSearchDateFrom()).toString());
        statement.setString(4, DateFormatter.changeDateAndTimeFormat(DTDD.getSearchDateTo()).toString());
        statement.setInt(5,  DTDD.getUserId());
        statement.setString(6, DateFormatter.changeDateAndTimeFormat(DTDD.getStartDate()).toString());
        //statement.setString(6, DateFormatter.changeDateAndTimeFormat(DTDD.getStartDate()).toString());

        statement.executeUpdate();
        super.postCollable(statement);
        return null;
    }
}
