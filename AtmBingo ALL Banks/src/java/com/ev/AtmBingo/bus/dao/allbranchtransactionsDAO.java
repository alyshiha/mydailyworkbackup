package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.allbranchtransactionsDTOInter;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class allbranchtransactionsDAO extends BaseDAO implements allbranchtransactionsDAOInter {

    protected allbranchtransactionsDAO() {
        super();
        super.setTableName("ALL_BRANCH_TRANSACTIONS");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        allbranchtransactionsDTOInter RecordToInsert = (allbranchtransactionsDTOInter) obj[0];
        //RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (FILE_ID,LOADING_DATE,ATM_APPLICATION_ID,ATM_ID,TRANSACTION_TYPE,TRANSACTION_TYPE_ID,CURRENCY,CURRENCY_ID,TRANSACTION_STATUS,TRANSACTION_STATUS_ID,RESPONSE_CODE,RESPONSE_CODE_ID,TRANSACTION_DATE,TRANSACTION_SEQUENCE,CARD_NO,AMOUNT,SETTLEMENT_DATE,NOTES_PRESENTED,CUSTOMER_ACCOUNT_NUMBER,TRANSACTION_SEQUENCE_ORDER_BY,TRANSACTION_TIME,MATCHING_TYPE,RECORD_TYPE,AMOUNT_TYPE,CARD_NO_SUFFIX,COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5,TRANSACTION_TYPE_MASTER) "
                + " values "
                + " ($fileid,'$loadingdate','$atmapplicationid',$atmid,'$transactiontype',$transactiontypeid,'$currency',$currencyid,'$transactionstatus',$transactionstatusid,'$responsecode',$responsecodeid,'$transactiondate','$transactionsequence','$cardno',$amount,'$settlementdate','$notespresented','$customeraccountnumber',$transactionsequenceorderby,'$transactiontime',$matchingtype,$recordtype,$amounttype,$cardnosuffix,'$column1','$column2','$column3','$column4','$column5',$transactiontypemaster)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$fileid", "" + RecordToInsert.getfileid());
        insertStat = insertStat.replace("$loadingdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.getloadingdate()));
        insertStat = insertStat.replace("$atmapplicationid", "" + RecordToInsert.getatmapplicationid());
        insertStat = insertStat.replace("$atmid", "" + RecordToInsert.getatmid());
        insertStat = insertStat.replace("$transactiontype", "" + RecordToInsert.gettransactiontype());
        insertStat = insertStat.replace("$transactiontypeid", "" + RecordToInsert.gettransactiontypeid());
        insertStat = insertStat.replace("$currency", "" + RecordToInsert.getcurrency());
        insertStat = insertStat.replace("$currencyid", "" + RecordToInsert.getcurrencyid());
        insertStat = insertStat.replace("$transactionstatus", "" + RecordToInsert.gettransactionstatus());
        insertStat = insertStat.replace("$transactionstatusid", "" + RecordToInsert.gettransactionstatusid());
        insertStat = insertStat.replace("$responsecode", "" + RecordToInsert.getresponsecode());
        insertStat = insertStat.replace("$responsecodeid", "" + RecordToInsert.getresponsecodeid());
        insertStat = insertStat.replace("$transactiondate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiondate()));
        insertStat = insertStat.replace("$transactionsequence", "" + RecordToInsert.gettransactionsequence());
        insertStat = insertStat.replace("$cardno", "" + RecordToInsert.getcardno());
        insertStat = insertStat.replace("$amount", "" + RecordToInsert.getamount());
        insertStat = insertStat.replace("$settlementdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.getsettlementdate()));
        insertStat = insertStat.replace("$notespresented", "" + RecordToInsert.getnotespresented());
        insertStat = insertStat.replace("$customeraccountnumber", "" + RecordToInsert.getcustomeraccountnumber());
        insertStat = insertStat.replace("$transactionsequenceorderby", "" + RecordToInsert.gettransactionsequenceorderby());
        insertStat = insertStat.replace("$transactiontime", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiontime()));
        insertStat = insertStat.replace("$matchingtype", "" + RecordToInsert.getmatchingtype());
        insertStat = insertStat.replace("$recordtype", "" + RecordToInsert.getrecordtype());
        insertStat = insertStat.replace("$amounttype", "" + RecordToInsert.getamounttype());
        insertStat = insertStat.replace("$cardnosuffix", "" + RecordToInsert.getcardnosuffix());
        insertStat = insertStat.replace("$column1", "" + RecordToInsert.getcolumn1());
        insertStat = insertStat.replace("$column2", "" + RecordToInsert.getcolumn2());
        insertStat = insertStat.replace("$column3", "" + RecordToInsert.getcolumn3());
        insertStat = insertStat.replace("$column4", "" + RecordToInsert.getcolumn4());
        insertStat = insertStat.replace("$column5", "" + RecordToInsert.getcolumn5());
        insertStat = insertStat.replace("$transactiontypemaster", "" + RecordToInsert.gettransactiontypemaster());
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To ALL_BRANCH_TRANSACTIONS", false);
        return null;
    }

    public Boolean ValidateNull(Object... obj) {
        allbranchtransactionsDTOInter RecordToInsert = (allbranchtransactionsDTOInter) obj[0];
        return Boolean.TRUE;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        allbranchtransactionsDTOInter RecordToUpdate = (allbranchtransactionsDTOInter) obj[0];
        String UpdateStat = "update $table set "
                + " (FILE_ID= $fileid,LOADING_DATE= '$loadingdate',ATM_APPLICATION_ID= '$atmapplicationid',ATM_ID= $atmid,TRANSACTION_TYPE= '$transactiontype',TRANSACTION_TYPE_ID= $transactiontypeid,CURRENCY= '$currency',CURRENCY_ID= $currencyid,TRANSACTION_STATUS= '$transactionstatus',TRANSACTION_STATUS_ID= $transactionstatusid,RESPONSE_CODE= '$responsecode',RESPONSE_CODE_ID= $responsecodeid,TRANSACTION_DATE= '$transactiondate',TRANSACTION_SEQUENCE= '$transactionsequence',CARD_NO= '$cardno',AMOUNT= $amount,SETTLEMENT_DATE= '$settlementdate',NOTES_PRESENTED= '$notespresented',CUSTOMER_ACCOUNT_NUMBER= '$customeraccountnumber',TRANSACTION_SEQUENCE_ORDER_BY= $transactionsequenceorderby,TRANSACTION_TIME= '$transactiontime',MATCHING_TYPE= $matchingtype,RECORD_TYPE= $recordtype,AMOUNT_TYPE= $amounttype,CARD_NO_SUFFIX= $cardnosuffix,COLUMN1= '$column1',COLUMN2= '$column2',COLUMN3= '$column3',COLUMN4= '$column4',COLUMN5= '$column5',TRANSACTION_TYPE_MASTER= $transactiontypemaster) "
                + "  where  ";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$fileid", "" + RecordToUpdate.getfileid());
        UpdateStat = UpdateStat.replace("$loadingdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToUpdate.getloadingdate()));
        UpdateStat = UpdateStat.replace("$atmapplicationid", "" + RecordToUpdate.getatmapplicationid());
        UpdateStat = UpdateStat.replace("$atmid", "" + RecordToUpdate.getatmid());
        UpdateStat = UpdateStat.replace("$transactiontype", "" + RecordToUpdate.gettransactiontype());
        UpdateStat = UpdateStat.replace("$transactiontypeid", "" + RecordToUpdate.gettransactiontypeid());
        UpdateStat = UpdateStat.replace("$currency", "" + RecordToUpdate.getcurrency());
        UpdateStat = UpdateStat.replace("$currencyid", "" + RecordToUpdate.getcurrencyid());
        UpdateStat = UpdateStat.replace("$transactionstatus", "" + RecordToUpdate.gettransactionstatus());
        UpdateStat = UpdateStat.replace("$transactionstatusid", "" + RecordToUpdate.gettransactionstatusid());
        UpdateStat = UpdateStat.replace("$responsecode", "" + RecordToUpdate.getresponsecode());
        UpdateStat = UpdateStat.replace("$responsecodeid", "" + RecordToUpdate.getresponsecodeid());
        UpdateStat = UpdateStat.replace("$transactiondate", "" + DateFormatter.changeDateAndTimeFormat(RecordToUpdate.gettransactiondate()));
        UpdateStat = UpdateStat.replace("$transactionsequence", "" + RecordToUpdate.gettransactionsequence());
        UpdateStat = UpdateStat.replace("$cardno", "" + RecordToUpdate.getcardno());
        UpdateStat = UpdateStat.replace("$amount", "" + RecordToUpdate.getamount());
        UpdateStat = UpdateStat.replace("$settlementdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToUpdate.getsettlementdate()));
        UpdateStat = UpdateStat.replace("$notespresented", "" + RecordToUpdate.getnotespresented());
        UpdateStat = UpdateStat.replace("$customeraccountnumber", "" + RecordToUpdate.getcustomeraccountnumber());
        UpdateStat = UpdateStat.replace("$transactionsequenceorderby", "" + RecordToUpdate.gettransactionsequenceorderby());
        UpdateStat = UpdateStat.replace("$transactiontime", "" + DateFormatter.changeDateAndTimeFormat(RecordToUpdate.gettransactiontime()));
        UpdateStat = UpdateStat.replace("$matchingtype", "" + RecordToUpdate.getmatchingtype());
        UpdateStat = UpdateStat.replace("$recordtype", "" + RecordToUpdate.getrecordtype());
        UpdateStat = UpdateStat.replace("$amounttype", "" + RecordToUpdate.getamounttype());
        UpdateStat = UpdateStat.replace("$cardnosuffix", "" + RecordToUpdate.getcardnosuffix());
        UpdateStat = UpdateStat.replace("$column1", "" + RecordToUpdate.getcolumn1());
        UpdateStat = UpdateStat.replace("$column2", "" + RecordToUpdate.getcolumn2());
        UpdateStat = UpdateStat.replace("$column3", "" + RecordToUpdate.getcolumn3());
        UpdateStat = UpdateStat.replace("$column4", "" + RecordToUpdate.getcolumn4());
        UpdateStat = UpdateStat.replace("$column5", "" + RecordToUpdate.getcolumn5());
        UpdateStat = UpdateStat.replace("$transactiontypemaster", "" + RecordToUpdate.gettransactiontypemaster());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table ALL_BRANCH_TRANSACTIONS", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        allbranchtransactionsDTOInter RecordToDelete = (allbranchtransactionsDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$fileid", "" + RecordToDelete.getfileid());
        deleteStat = deleteStat.replace("$loadingdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToDelete.getloadingdate()));
        deleteStat = deleteStat.replace("$atmapplicationid", "" + RecordToDelete.getatmapplicationid());
        deleteStat = deleteStat.replace("$atmid", "" + RecordToDelete.getatmid());
        deleteStat = deleteStat.replace("$transactiontype", "" + RecordToDelete.gettransactiontype());
        deleteStat = deleteStat.replace("$transactiontypeid", "" + RecordToDelete.gettransactiontypeid());
        deleteStat = deleteStat.replace("$currency", "" + RecordToDelete.getcurrency());
        deleteStat = deleteStat.replace("$currencyid", "" + RecordToDelete.getcurrencyid());
        deleteStat = deleteStat.replace("$transactionstatus", "" + RecordToDelete.gettransactionstatus());
        deleteStat = deleteStat.replace("$transactionstatusid", "" + RecordToDelete.gettransactionstatusid());
        deleteStat = deleteStat.replace("$responsecode", "" + RecordToDelete.getresponsecode());
        deleteStat = deleteStat.replace("$responsecodeid", "" + RecordToDelete.getresponsecodeid());
        deleteStat = deleteStat.replace("$transactiondate", "" + DateFormatter.changeDateAndTimeFormat(RecordToDelete.gettransactiondate()));
        deleteStat = deleteStat.replace("$transactionsequence", "" + RecordToDelete.gettransactionsequence());
        deleteStat = deleteStat.replace("$cardno", "" + RecordToDelete.getcardno());
        deleteStat = deleteStat.replace("$amount", "" + RecordToDelete.getamount());
        deleteStat = deleteStat.replace("$settlementdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToDelete.getsettlementdate()));
        deleteStat = deleteStat.replace("$notespresented", "" + RecordToDelete.getnotespresented());
        deleteStat = deleteStat.replace("$customeraccountnumber", "" + RecordToDelete.getcustomeraccountnumber());
        deleteStat = deleteStat.replace("$transactionsequenceorderby", "" + RecordToDelete.gettransactionsequenceorderby());
        deleteStat = deleteStat.replace("$transactiontime", "" + DateFormatter.changeDateAndTimeFormat(RecordToDelete.gettransactiontime()));
        deleteStat = deleteStat.replace("$matchingtype", "" + RecordToDelete.getmatchingtype());
        deleteStat = deleteStat.replace("$recordtype", "" + RecordToDelete.getrecordtype());
        deleteStat = deleteStat.replace("$amounttype", "" + RecordToDelete.getamounttype());
        deleteStat = deleteStat.replace("$cardnosuffix", "" + RecordToDelete.getcardnosuffix());
        deleteStat = deleteStat.replace("$column1", "" + RecordToDelete.getcolumn1());
        deleteStat = deleteStat.replace("$column2", "" + RecordToDelete.getcolumn2());
        deleteStat = deleteStat.replace("$column3", "" + RecordToDelete.getcolumn3());
        deleteStat = deleteStat.replace("$column4", "" + RecordToDelete.getcolumn4());
        deleteStat = deleteStat.replace("$column5", "" + RecordToDelete.getcolumn5());
        deleteStat = deleteStat.replace("$transactiontypemaster", "" + RecordToDelete.gettransactiontypemaster());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table ALL_BRANCH_TRANSACTIONS", false);
        return null;
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table ALL_BRANCH_TRANSACTIONS", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        allbranchtransactionsDTOInter RecordToSelect = (allbranchtransactionsDTOInter) obj[0];
        String selectStat = "Select FILE_ID,LOADING_DATE,ATM_APPLICATION_ID,ATM_ID,TRANSACTION_TYPE,TRANSACTION_TYPE_ID,CURRENCY,CURRENCY_ID,TRANSACTION_STATUS,TRANSACTION_STATUS_ID,RESPONSE_CODE,RESPONSE_CODE_ID,TRANSACTION_DATE,TRANSACTION_SEQUENCE,CARD_NO,AMOUNT,SETTLEMENT_DATE,NOTES_PRESENTED,CUSTOMER_ACCOUNT_NUMBER,TRANSACTION_SEQUENCE_ORDER_BY,TRANSACTION_TIME,MATCHING_TYPE,RECORD_TYPE,AMOUNT_TYPE,CARD_NO_SUFFIX,COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5,TRANSACTION_TYPE_MASTER From $table"
                + "  where  ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        allbranchtransactionsDTOInter SelectedRecord = DTOFactory.createallbranchtransactionsDTO();
        while (rs.next()) {
            SelectedRecord.setfileid(rs.getInt("FILE_ID"));
            SelectedRecord.setloadingdate(rs.getTimestamp("LOADING_DATE"));
            SelectedRecord.setatmapplicationid(rs.getString("ATM_APPLICATION_ID"));
            SelectedRecord.setatmid(rs.getInt("ATM_ID"));
            SelectedRecord.settransactiontype(rs.getString("TRANSACTION_TYPE"));
            SelectedRecord.settransactiontypeid(rs.getInt("TRANSACTION_TYPE_ID"));
            SelectedRecord.setcurrency(rs.getString("CURRENCY"));
            SelectedRecord.setcurrencyid(rs.getInt("CURRENCY_ID"));
            SelectedRecord.settransactionstatus(rs.getString("TRANSACTION_STATUS"));
            SelectedRecord.settransactionstatusid(rs.getInt("TRANSACTION_STATUS_ID"));
            SelectedRecord.setresponsecode(rs.getString("RESPONSE_CODE"));
            SelectedRecord.setresponsecodeid(rs.getInt("RESPONSE_CODE_ID"));
            SelectedRecord.settransactiondate(rs.getTimestamp("TRANSACTION_DATE"));
            SelectedRecord.settransactionsequence(rs.getString("TRANSACTION_SEQUENCE"));
            SelectedRecord.setcardno(rs.getString("CARD_NO"));
            SelectedRecord.setamount(rs.getInt("AMOUNT"));
            SelectedRecord.setsettlementdate(rs.getTimestamp("SETTLEMENT_DATE"));
            SelectedRecord.setnotespresented(rs.getString("NOTES_PRESENTED"));
            SelectedRecord.setcustomeraccountnumber(rs.getString("CUSTOMER_ACCOUNT_NUMBER"));
            SelectedRecord.settransactionsequenceorderby(rs.getInt("TRANSACTION_SEQUENCE_ORDER_BY"));
            SelectedRecord.settransactiontime(rs.getTimestamp("TRANSACTION_TIME"));
            SelectedRecord.setmatchingtype(rs.getInt("MATCHING_TYPE"));
            SelectedRecord.setrecordtype(rs.getInt("RECORD_TYPE"));
            SelectedRecord.setamounttype(rs.getInt("AMOUNT_TYPE"));
            SelectedRecord.setcardnosuffix(rs.getInt("CARD_NO_SUFFIX"));
            SelectedRecord.setcolumn1(rs.getString("COLUMN1"));
            SelectedRecord.setcolumn2(rs.getString("COLUMN2"));
            SelectedRecord.setcolumn3(rs.getString("COLUMN3"));
            SelectedRecord.setcolumn4(rs.getString("COLUMN4"));
            SelectedRecord.setcolumn5(rs.getString("COLUMN5"));
            SelectedRecord.settransactiontypemaster(rs.getInt("TRANSACTION_TYPE_MASTER"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        String RecordToSelect = (String) obj[0];
        String selectStat = "Select is_colected_time(CARD_NO) colectedtime,is_colected(CARD_NO) colected,nvl(is_colected_reason(CARD_NO),'Pending') reasoncolected,RID,TableType,FILE_ID,LOADING_DATE,ATM_APPLICATION_ID,ATM_ID,TRANSACTION_TYPE,TRANSACTION_TYPE_ID,CURRENCY,CURRENCY_ID,TRANSACTION_STATUS,TRANSACTION_STATUS_ID,RESPONSE_CODE,RESPONSE_CODE_ID,TRANSACTION_DATE,TRANSACTION_SEQUENCE, CARD_NO,AMOUNT,SETTLEMENT_DATE,NOTES_PRESENTED,CUSTOMER_ACCOUNT_NUMBER,TRANSACTION_SEQUENCE_ORDER_BY,TRANSACTION_TIME,AMOUNT_TYPE,CARD_NO_SUFFIX,COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5,TRANSACTION_TYPE_MASTER From $table"
                + "    ";
        selectStat = selectStat.replace("$table", "" + super.getTableName() + RecordToSelect);
        ResultSet rs = executeQuery(selectStat);
        List<allbranchtransactionsDTOInter> Records = new ArrayList<allbranchtransactionsDTOInter>();
        while (rs.next()) {
            allbranchtransactionsDTOInter SelectedRecord = DTOFactory.createallbranchtransactionsDTO();
            SelectedRecord.setiscollectedtime(rs.getInt("colectedtime"));
            SelectedRecord.setfileid(rs.getInt("FILE_ID"));
            SelectedRecord.setiscollected(rs.getInt("colected"));
            SelectedRecord.setloadingdate(rs.getTimestamp("LOADING_DATE"));
            SelectedRecord.setreason(rs.getString("reasoncolected"));
            SelectedRecord.setatmapplicationid(rs.getString("ATM_APPLICATION_ID"));
            SelectedRecord.setrowid(rs.getString("RID"));
            SelectedRecord.settabletype(rs.getString("TableType"));
            SelectedRecord.setatmid(rs.getInt("ATM_ID"));
            SelectedRecord.settransactiontype(rs.getString("TRANSACTION_TYPE"));
            SelectedRecord.settransactiontypeid(rs.getInt("TRANSACTION_TYPE_ID"));
            SelectedRecord.setcurrency(rs.getString("CURRENCY"));
            SelectedRecord.setcurrencyid(rs.getInt("CURRENCY_ID"));
            SelectedRecord.settransactionstatus(rs.getString("TRANSACTION_STATUS"));
            SelectedRecord.settransactionstatusid(rs.getInt("TRANSACTION_STATUS_ID"));
            SelectedRecord.setresponsecode(rs.getString("RESPONSE_CODE"));
            SelectedRecord.setresponsecodeid(rs.getInt("RESPONSE_CODE_ID"));
            SelectedRecord.settransactiondate(rs.getTimestamp("TRANSACTION_DATE"));
            SelectedRecord.settransactionsequence(rs.getString("TRANSACTION_SEQUENCE"));
            SelectedRecord.setcardno(rs.getString("CARD_NO"));
            SelectedRecord.setamount(rs.getInt("AMOUNT"));
            SelectedRecord.setsettlementdate(rs.getTimestamp("SETTLEMENT_DATE"));
            SelectedRecord.setnotespresented(rs.getString("NOTES_PRESENTED"));
            SelectedRecord.setcustomeraccountnumber(rs.getString("CUSTOMER_ACCOUNT_NUMBER"));
            SelectedRecord.settransactionsequenceorderby(rs.getInt("TRANSACTION_SEQUENCE_ORDER_BY"));
            SelectedRecord.settransactiontime(rs.getTimestamp("TRANSACTION_TIME"));
            SelectedRecord.setamounttype(rs.getInt("AMOUNT_TYPE"));
            SelectedRecord.setcardnosuffix(rs.getInt("CARD_NO_SUFFIX"));
            SelectedRecord.setcolumn1(rs.getString("COLUMN1"));
            SelectedRecord.setcolumn2(rs.getString("COLUMN2"));
            SelectedRecord.setcolumn3(rs.getString("COLUMN3"));
            SelectedRecord.setcolumn4(rs.getString("COLUMN4"));
            SelectedRecord.setcolumn5(rs.getString("COLUMN5"));
            SelectedRecord.settransactiontypemaster(rs.getInt("TRANSACTION_TYPE_MASTER"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select FILE_ID,LOADING_DATE,ATM_APPLICATION_ID,ATM_ID,TRANSACTION_TYPE,TRANSACTION_TYPE_ID,CURRENCY,CURRENCY_ID,TRANSACTION_STATUS,TRANSACTION_STATUS_ID,RESPONSE_CODE,RESPONSE_CODE_ID,TRANSACTION_DATE,TRANSACTION_SEQUENCE,CARD_NO,AMOUNT,SETTLEMENT_DATE,NOTES_PRESENTED,CUSTOMER_ACCOUNT_NUMBER,TRANSACTION_SEQUENCE_ORDER_BY,TRANSACTION_TIME,MATCHING_TYPE,RECORD_TYPE,AMOUNT_TYPE,CARD_NO_SUFFIX,COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5,TRANSACTION_TYPE_MASTER From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<allbranchtransactionsDTOInter> Records = new ArrayList<allbranchtransactionsDTOInter>();
        while (rs.next()) {
            allbranchtransactionsDTOInter SelectedRecord = DTOFactory.createallbranchtransactionsDTO();
            SelectedRecord.setfileid(rs.getInt("FILE_ID"));
            SelectedRecord.setloadingdate(rs.getTimestamp("LOADING_DATE"));
            SelectedRecord.setatmapplicationid(rs.getString("ATM_APPLICATION_ID"));
            SelectedRecord.setatmid(rs.getInt("ATM_ID"));
            SelectedRecord.settransactiontype(rs.getString("TRANSACTION_TYPE"));
            SelectedRecord.settransactiontypeid(rs.getInt("TRANSACTION_TYPE_ID"));
            SelectedRecord.setcurrency(rs.getString("CURRENCY"));
            SelectedRecord.setcurrencyid(rs.getInt("CURRENCY_ID"));
            SelectedRecord.settransactionstatus(rs.getString("TRANSACTION_STATUS"));
            SelectedRecord.settransactionstatusid(rs.getInt("TRANSACTION_STATUS_ID"));
            SelectedRecord.setresponsecode(rs.getString("RESPONSE_CODE"));
            SelectedRecord.setresponsecodeid(rs.getInt("RESPONSE_CODE_ID"));
            SelectedRecord.settransactiondate(rs.getTimestamp("TRANSACTION_DATE"));
            SelectedRecord.settransactionsequence(rs.getString("TRANSACTION_SEQUENCE"));
            SelectedRecord.setcardno(rs.getString("CARD_NO"));
            SelectedRecord.setamount(rs.getInt("AMOUNT"));
            SelectedRecord.setsettlementdate(rs.getTimestamp("SETTLEMENT_DATE"));
            SelectedRecord.setnotespresented(rs.getString("NOTES_PRESENTED"));
            SelectedRecord.setcustomeraccountnumber(rs.getString("CUSTOMER_ACCOUNT_NUMBER"));
            SelectedRecord.settransactionsequenceorderby(rs.getInt("TRANSACTION_SEQUENCE_ORDER_BY"));
            SelectedRecord.settransactiontime(rs.getTimestamp("TRANSACTION_TIME"));
            SelectedRecord.setmatchingtype(rs.getInt("MATCHING_TYPE"));
            SelectedRecord.setrecordtype(rs.getInt("RECORD_TYPE"));
            SelectedRecord.setamounttype(rs.getInt("AMOUNT_TYPE"));
            SelectedRecord.setcardnosuffix(rs.getInt("CARD_NO_SUFFIX"));
            SelectedRecord.setcolumn1(rs.getString("COLUMN1"));
            SelectedRecord.setcolumn2(rs.getString("COLUMN2"));
            SelectedRecord.setcolumn3(rs.getString("COLUMN3"));
            SelectedRecord.setcolumn4(rs.getString("COLUMN4"));
            SelectedRecord.setcolumn5(rs.getString("COLUMN5"));
            SelectedRecord.settransactiontypemaster(rs.getInt("TRANSACTION_TYPE_MASTER"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        allbranchtransactionsDTOInter[] entities = (allbranchtransactionsDTOInter[]) obj[0];
        Integer userid = (Integer) obj[1];
        String reason = (String) obj[2];
        String mess = "", mess2 = "";
        if (entities.length == 0) {
            mess = "No Users Found";
        } else {
            String validate = "Validate";
            if (validate.equals("Validate")) {
                String insertStat = "insert into card_taken\n"
                        + "  (card_no, userid, takendate,reason)\n"
                        + "values\n"
                        + "  ("+super.getencryptcardcolumnstatment()+", ?, sysdate,?)";
                Connection connection = null;
                PreparedStatement statement = null;
                try {
                  
     connection  = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.length; i++) {
                        if (entities[i].getiscollected() == 1) {
                            allbranchtransactionsDTOInter RecordtoInsert = entities[i];
                            statement.setString(1, RecordtoInsert.getcardno());
                            statement.setInt(2, userid);
                            statement.setInt(3, Integer.valueOf(reason));
                            statement.addBatch();
                            if ((i + 1) % 1000 == 0) {
                                statement.executeBatch();
                            }
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                            CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
