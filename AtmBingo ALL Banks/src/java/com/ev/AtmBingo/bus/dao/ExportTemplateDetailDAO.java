

         /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.EXPORTTEMPLETEDETAILSDTOInter;
import com.ev.AtmBingo.bus.dto.EXPORTTEMPLETEDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */

public class ExportTemplateDetailDAO extends BaseDAO implements EXPORTTEMPLETEDAOInter, ExportTemplateDetailDAOInter {

    protected ExportTemplateDetailDAO() {
        super();
        super.setTableName("EXPORT_TEMPLETE_DETAILS");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        EXPORTTEMPLETEDETAILSDTOInter uDTO = (EXPORTTEMPLETEDETAILSDTOInter) obj[0];
        String insertStat = "insert into export_templete_details (id, column_id, seq, format) values ($v_id, $v_column_id, $v_seq, '$v_format')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$v_format", "" + uDTO.getFormat());
        insertStat = insertStat.replace("$v_column_id", "" + uDTO.getColumnid());
        insertStat = insertStat.replace("$v_seq", "" + uDTO.getSequence());
        insertStat = insertStat.replace("$v_id", "" +uDTO.getTemplateid());
        super.executeUpdate(insertStat);
        super.postUpdate("Add " + uDTO.getColumnid() + "Added On File Template", false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        EXPORTTEMPLETEDETAILSDTOInter uDTO = (EXPORTTEMPLETEDETAILSDTOInter) obj[0];
        String updateStat = "update $table  set  seq = $v_seq, format = '$v_format' where id = $v_id and column_id = $v_column_id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$v_column_id", "" + uDTO.getColumnid());
        updateStat = updateStat.replace("$v_seq", "" + uDTO.getSequence());
        updateStat = updateStat.replace("$v_format", "" + uDTO.getFormat());
        updateStat = updateStat.replace("$v_id", "" + uDTO.getTemplateid());
        super.executeUpdate(updateStat);
        super.postUpdate("coloum " + uDTO.getColumnid() + "Updated On File Template", false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        EXPORTTEMPLETEDETAILSDTOInter uDTO = (EXPORTTEMPLETEDETAILSDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id and column_id = $v_column_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$v_column_id", "" + uDTO.getColumnid());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getTemplateid());
        super.executeUpdate(deleteStat);
        super.postUpdate("coloum " + uDTO.getColumnid() + "Deleted On File Template", false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select id, column_id, seq, format from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        List<EXPORTTEMPLETEDETAILSDTOInter> uDTOL = new ArrayList<EXPORTTEMPLETEDETAILSDTOInter>();
        while (rs.next()) {
            EXPORTTEMPLETEDETAILSDTOInter uDTO = DTOFactory.createEXPORTTEMPLETEDETAILSDTO();
            uDTO.setTemplateid(rs.getInt("id"));
            uDTO.setColumnid(rs.getInt("column_id"));
            uDTO.setSequence(rs.getInt("seq"));
            uDTO.setFormat(rs.getString("format"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select id, column_id, seq, format from $table  order by id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<EXPORTTEMPLETEDETAILSDTOInter> uDTOL = new ArrayList<EXPORTTEMPLETEDETAILSDTOInter>();
        while (rs.next()) {
            EXPORTTEMPLETEDETAILSDTOInter uDTO = DTOFactory.createEXPORTTEMPLETEDETAILSDTO();
            uDTO.setTemplateid(rs.getInt("id"));
            uDTO.setColumnid(rs.getInt("column_id"));
            uDTO.setSequence(rs.getInt("seq"));
            uDTO.setFormat(rs.getString("format"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }
}
