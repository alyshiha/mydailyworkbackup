/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.NetworksDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface NetworksDAOInter {

    Boolean findDuplicateRecord(String Name) throws Throwable;

    List<NetworksDTOInter> findRecord(String Name) throws Throwable;

    String delete(Object... obj);

    List<NetworksDTOInter> findAll() throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
