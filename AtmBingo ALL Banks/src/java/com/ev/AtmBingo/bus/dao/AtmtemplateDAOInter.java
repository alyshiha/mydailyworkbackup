/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.AtmFileHeaderDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDetailDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AtmtemplateDAOInter {

    Object findAllColDef() throws Throwable;

    Object DeleteDetail(Object... obj) throws Throwable;

    Object findByTemplate(AtmFileTemplateDTOInter aftDTO, String TName) throws Throwable;

    Object findByTemplateDetail(AtmFileTemplateDTOInter aftDTO, String TName) throws Throwable;

    Object SaveAllDetail(List<AtmFileTemplateDetailDTOInter> filetemplatedetailList, String TName, Integer templateid) throws Throwable;

    Object deleterecordHeader(Object... obj) throws Throwable;

    Object deleterecordHeaderByTemplateID(Object... obj) throws Throwable;

    Object deleterecordTemplate(Object... obj) throws Throwable;

    Object deleterecorddetailTemplate(Object... obj) throws Throwable;

    Object findAllColumns() throws Throwable;

    Object findAllMachineType(Object... obj) throws Throwable;

    Object findAllTemplates(String Table) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object insertrecordHeader(Object... obj) throws Throwable;
Object SaveAllHeader(List<AtmFileHeaderDTOInter> filetemplateHeaderList, String TName, Integer templateid) throws Throwable ;
    Object update(Object... obj) throws Throwable;

    Object updaterecordHeader(Object... obj) throws Throwable;
}
