/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Administrator
 */
public interface UserPassDAOInter {

    Object findBeforeLastPass(int id) throws Throwable;
    Object findBeforeLastPass( int id,String name) throws Throwable;
    Object findBeforeLastPassBranch( int id,String name) throws Throwable;

    Object findLastPass(int id) throws Throwable;

    Object findLastUpdate(int id) throws Throwable;

}
