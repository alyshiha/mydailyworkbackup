/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.HolidaysDTOInter_1;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author shi7a
 */
public class HolidaysDAO_1 extends BaseDAO implements HolidaysDAOInter_1 {

    @Override
    public List<HolidaysDTOInter_1> findRecord(String Type,String Name) throws Throwable {
        super.preSelect();
        String selectStat = "select holiday_id, holiday_date, holiday_name, holiday_type from holidays "
                + " where (holiday_type = '$Type' OR '$Type' = 'All') "
                + " and (holiday_name Like '%$Name%' OR '$Name' = '')"
                + " order by holiday_date";
        selectStat = selectStat.replace("$Type", Type);
        selectStat = selectStat.replace("$Name", Name);
        ResultSet rs = executeQuery(selectStat);
        List<HolidaysDTOInter_1> records = new ArrayList<HolidaysDTOInter_1>();
        while (rs.next()) {
            HolidaysDTOInter_1 record = DTOFactory.createHolidaysDTO_1();
            record.setHolidaydate(rs.getTimestamp("holiday_date"));
            record.setHolidayid(rs.getInt("holiday_id"));
            record.setHolidayname(rs.getString("holiday_name"));
            record.setHolidaytype(rs.getString("holiday_type"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("search records in holidays  with type equal: " + Type);
        return records;
    }

    @Override
    public List<HolidaysDTOInter_1> findAll() throws Throwable {
        super.preSelect();
        String selectStat = "select holiday_id, holiday_date, holiday_name, holiday_type from holidays order by holiday_date";
        ResultSet rs = executeQuery(selectStat);
        List<HolidaysDTOInter_1> records = new ArrayList<HolidaysDTOInter_1>();
        while (rs.next()) {
            HolidaysDTOInter_1 record = DTOFactory.createHolidaysDTO_1();
            record.setHolidaydate(rs.getTimestamp("holiday_date"));
            record.setHolidayid(rs.getInt("holiday_id"));
            record.setHolidayname(rs.getString("holiday_name"));
            record.setHolidaytype(rs.getString("holiday_type"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("search all records in holidays");
        return records;
    }

    @Override
    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        HolidaysDTOInter_1 record = (HolidaysDTOInter_1) obj[0];
        record.setHolidayid(super.generateSequence("holidays"));
        String insertStat = "insert into holidays\n"
                + "  (holiday_id, holiday_date, holiday_name, holiday_type)\n"
                + "values\n"
                + "  ($holiday_id, to_date('$holiday_date','dd.MM.yyyy'), '$holiday_name', '$holiday_type')";
        insertStat = insertStat.replace("$holiday_id", "" + record.getHolidayid());
        insertStat = insertStat.replace("$holiday_name", "" + record.getHolidayname());
        insertStat = insertStat.replace("$holiday_type", "" + record.getHolidaytype());
        insertStat = insertStat.replace("$holiday_date", "" + DateFormatter.changeDateFormat(record.getHolidaydate()));
        super.executeUpdate(insertStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("insert record in holidays with id= " + record.getHolidayid());
        return null;
    }

    @Override
    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        HolidaysDTOInter_1 record = (HolidaysDTOInter_1) obj[0];
        String updateStat = "update holidays\n"
                + "   set holiday_date = to_date('$holiday_date','dd.MM.yyyy'),\n"
                + "       holiday_name = '$holiday_name',\n"
                + "       holiday_type = '$holiday_type'\n"
                + " where holiday_id = $holiday_id";
        updateStat = updateStat.replace("$holiday_id", "" + record.getHolidayid());
        updateStat = updateStat.replace("$holiday_name", "" + record.getHolidayname());
        updateStat = updateStat.replace("$holiday_type", "" + record.getHolidaytype());
        updateStat = updateStat.replace("$holiday_date", "" + DateFormatter.changeDateFormat(record.getHolidaydate()));
        super.executeUpdate(updateStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("update record in holidays with id= " + record.getHolidayid());
        return null;
    }

    @Override
    public String delete(Object... obj) {
        try {
            super.preUpdate();
            HolidaysDTOInter_1 record = (HolidaysDTOInter_1) obj[0];
            String deleteStat = "delete holidays\n"
                    + " where holiday_id = $holiday_id";
            deleteStat = deleteStat.replace("$holiday_id", "" + record.getHolidayid());
            super.executeUpdate(deleteStat);
            CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
            logEngin.insert("delete record in holidays");
            return "Record Has Been Succesfully Deleted";
        } catch (Throwable ex) {
            Logger.getLogger(HolidaysDAO_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Record Has Been Succesfully Deleted";
    }

}
