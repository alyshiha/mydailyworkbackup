package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.AtmBingo.base.util.DateFormatter;
import java.util.ArrayList;
import java.util.List;
import com.ev.AtmBingo.bus.dto.usersbranchDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class usersbranchDAO extends BaseDAO implements usersbranchDAOInter {

    protected usersbranchDAO() {
        super();
        super.setTableName("USERS_BRANCH");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        usersbranchDTOInter RecordToInsert = (usersbranchDTOInter) obj[0];
        RecordToInsert.setuserid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (USER_ID,USER_NAME,LOGON_NAME,USER_PASSWORD,BRANCHNAME,STATUS) "
                + " values "
                + " ($userid,'$username','$logonname','$userpassword','$branchname',$status)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$userid", "" + RecordToInsert.getuserid());
        insertStat = insertStat.replace("$username", "" + RecordToInsert.getusername());
        insertStat = insertStat.replace("$logonname", "" + RecordToInsert.getlogonname());
        insertStat = insertStat.replace("$userpassword", "" + RecordToInsert.getuserpassword());
        insertStat = insertStat.replace("$branchname", "" + RecordToInsert.getbranchid());
        insertStat = insertStat.replace("$status", "" + RecordToInsert.getstatus());
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To USERS_BRANCH", false);
        return null;
    }

    public Boolean ValidateNull(Object... obj) {
        usersbranchDTOInter RecordToInsert = (usersbranchDTOInter) obj[0];
        if (RecordToInsert.getuserid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getusername() == null && RecordToInsert.getusername().equals("")) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getlogonname() == null && RecordToInsert.getlogonname().equals("")) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getstatus() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        usersbranchDTOInter RecordToUpdate = (usersbranchDTOInter) obj[0];
        String UpdateStat = "update $table set "
                + " (USER_ID= $userid,USER_NAME= '$username',LOGON_NAME= '$logonname',USER_PASSWORD= '$userpassword',BRANCHNAME= '$branchname',STATUS= $status) "
                + "  where  USER_ID= $userid";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$userid", "" + RecordToUpdate.getuserid());
        UpdateStat = UpdateStat.replace("$username", "" + RecordToUpdate.getusername());
        UpdateStat = UpdateStat.replace("$logonname", "" + RecordToUpdate.getlogonname());
        UpdateStat = UpdateStat.replace("$userpassword", "" + RecordToUpdate.getuserpassword());
        UpdateStat = UpdateStat.replace("$branchname", "" + RecordToUpdate.getbranchid());
        UpdateStat = UpdateStat.replace("$status", "" + RecordToUpdate.getstatus());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table USERS_BRANCH", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        usersbranchDTOInter RecordToDelete = (usersbranchDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  USER_ID= $userid";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$userid", "" + RecordToDelete.getuserid());
        deleteStat = deleteStat.replace("$username", "" + RecordToDelete.getusername());
        deleteStat = deleteStat.replace("$logonname", "" + RecordToDelete.getlogonname());
        deleteStat = deleteStat.replace("$userpassword", "" + RecordToDelete.getuserpassword());
        deleteStat = deleteStat.replace("$branchname", "" + RecordToDelete.getbranchid());
        deleteStat = deleteStat.replace("$status", "" + RecordToDelete.getstatus());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table USERS_BRANCH", false);
        return null;
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table USERS_BRANCH", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        String RecordToSelect = (String) obj[0];
        String selectStat = "Select USER_ID,USER_NAME,LOGON_NAME,USER_PASSWORD,(select id from branch_code where name = BRANCHNAME)BRANCH,STATUS From $table"
                + "  where  upper(LOGON_NAME)= upper('$user')";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$user", "" + RecordToSelect);
        ResultSet rs = executeQuery(selectStat);
        usersbranchDTOInter SelectedRecord = DTOFactory.createusersbranchDTO();
        while (rs.next()) {
            SelectedRecord.setuserid(rs.getInt("USER_ID"));
            SelectedRecord.setusername(rs.getString("USER_NAME"));
            SelectedRecord.setlogonname(rs.getString("LOGON_NAME"));
            SelectedRecord.setuserpassword(rs.getString("USER_PASSWORD"));
            SelectedRecord.setbranchname(rs.getString("BRANCH"));
            SelectedRecord.setstatus(rs.getInt("STATUS"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        usersbranchDTOInter RecordToSelect = (usersbranchDTOInter) obj[0];
        String selectStat = "Select USER_ID,USER_NAME,LOGON_NAME,USER_PASSWORD,(select name from branch_code where id = BRANCHNAME)BRANCH,STATUS From $table"
                + "  where  USER_ID= $userid";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$userid", "" + RecordToSelect.getuserid());
        ResultSet rs = executeQuery(selectStat);
        List<usersbranchDTOInter> Records = new ArrayList<usersbranchDTOInter>();
        while (rs.next()) {
            usersbranchDTOInter SelectedRecord = DTOFactory.createusersbranchDTO();
            SelectedRecord.setuserid(rs.getInt("USER_ID"));
            SelectedRecord.setusername(rs.getString("USER_NAME"));
            SelectedRecord.setlogonname(rs.getString("LOGON_NAME"));
            SelectedRecord.setuserpassword(rs.getString("USER_PASSWORD"));
            SelectedRecord.setbranchname(rs.getString("BRANCH"));
            SelectedRecord.setstatus(rs.getInt("STATUS"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select USER_ID,USER_NAME,LOGON_NAME,USER_PASSWORD,(select name from branch_code where id = BRANCHNAME)BRANCH,STATUS From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<usersbranchDTOInter> Records = new ArrayList<usersbranchDTOInter>();
        while (rs.next()) {
            usersbranchDTOInter SelectedRecord = DTOFactory.createusersbranchDTO();
            SelectedRecord.setuserid(rs.getInt("USER_ID"));
            SelectedRecord.setusername(rs.getString("USER_NAME"));
            SelectedRecord.setlogonname(rs.getString("LOGON_NAME"));
            SelectedRecord.setuserpassword(rs.getString("USER_PASSWORD"));
            SelectedRecord.setbranchname(rs.getString("BRANCH"));
            SelectedRecord.setstatus(rs.getInt("STATUS"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String ValidateLog(String userName, String password, String version, Integer NumOfUsers, Date EndDate) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select Branch_login_validation('$u_name','$u_pass','$Runningversion',$NumOfUsers,to_date('$enddate','YY mm dd')) from dual";
        selectStat = selectStat.replace("$u_name", "" + userName);
        selectStat = selectStat.replace("$u_pass", "" + password);
        selectStat = selectStat.replace("$Runningversion", "" + version);
        selectStat = selectStat.replace("$NumOfUsers", "" + NumOfUsers);
        selectStat = selectStat.replace("$enddate", "" + EndDate);
        ResultSet rs = executeQuery(selectStat);
        String Result = "";
        while (rs.next()) {
            Result = rs.getString(1);

        }
        super.postSelect(rs);
        return Result;
    }

    public void save(Object... obj) throws SQLException, Throwable {
        List<usersbranchDTOInter> entities = (List<usersbranchDTOInter>) obj[0];
        String insertStat = "Update USERS_BRANCH Set USER_NAME = ?,LOGON_NAME = ?,BRANCHNAME = ? where USER_ID = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.size(); i++) {
                usersbranchDTOInter RecordtoInsert = entities.get(i);
                statement.setString(1, RecordtoInsert.getusername());
                statement.setString(2, RecordtoInsert.getlogonname());
                statement.setInt(3, RecordtoInsert.getbranchid());
                statement.setInt(4, RecordtoInsert.getuserid());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    CoonectionHandler.getInstance().returnConnection(connection);
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);

                } catch (SQLException logOrIgnore) {
                }
            }
        }
    }
}
