/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.RejectedTransactionsDTOInter;
import java.sql.Clob;

/**
 *
 * @author Administrator
 */
public interface RejectedTransactionsDAOInter {

     String Findtexttransaction(Object obj) throws Throwable;
     Clob FindFiletransaction(Object obj) throws Throwable;
    Object customSearch(String whereClause, String field,String temp) throws Throwable;

   // Object findReport(String fields, String whereClause) throws Throwable;

    Object revalidateRecord(RejectedTransactionsDTOInter[] rtDTOArr) throws Throwable;

    Object search(Object... obj) throws Throwable;

}
