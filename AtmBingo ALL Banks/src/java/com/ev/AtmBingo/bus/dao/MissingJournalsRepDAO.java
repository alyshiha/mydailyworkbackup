/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.MissingJournalDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class MissingJournalsRepDAO extends BaseDAO implements MissingJournalsRepDAOInter {

    protected MissingJournalsRepDAO() {
        super();
    }

    public List<MissingJournalDTOInter> findMissingJournalsexcelReport(String dateFrom, String dateTo) throws Throwable {
        super.preSelect();
        String selectStatment = "SELECT   ATM_APPLICATION_ID ATM, UNIT_NUMBER UNIT, COUNT (LOADING_DATE) days FROM   (SELECT   DISTINCT TRUNC (LOADING_DATE) LOADING_DATE,ATM_APPLICATION_ID,UNIT_NUMBER FROM   MISSING_JOURNAL WHERE    TRUNC (LOADING_DATE) BETWEEN TO_DATE ('$P{DateFrom}','dd.mm.yyyy hh24:mi:ss') AND  TO_DATE ('$P{DateTo}','dd.mm.yyyy hh24:mi:ss')) GROUP BY   ATM_APPLICATION_ID, UNIT_NUMBER";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DateTo}", dateTo);
        ResultSet rs = executeQueryReport(selectStatment);
        List<MissingJournalDTOInter> Records = new ArrayList<MissingJournalDTOInter>();
        while (rs.next()) {
            MissingJournalDTOInter record = DTOFactory.createMissingJournalDTO();
            record.setApplicationid(rs.getString("ATM"));
            record.setTotal(rs.getInt("days"));
            record.setUnit(rs.getString("UNIT"));
            Records.add(record);
        }
        
        super.postSelect(rs);
        return Records;
    }

    public Object findReport(String dateFrom, String dateTo) throws Throwable {
        super.preSelect();
        String selectStatment = "SELECT   ATM_APPLICATION_ID ATM, UNIT_NUMBER UNIT, COUNT (LOADING_DATE) days FROM   (SELECT   DISTINCT TRUNC (LOADING_DATE) LOADING_DATE,ATM_APPLICATION_ID,UNIT_NUMBER FROM   MISSING_JOURNAL WHERE    TRUNC (LOADING_DATE) BETWEEN TO_DATE ('$P{DateFrom}','dd.mm.yyyy hh24:mi:ss') AND  TO_DATE ('$P{DateTo}','dd.mm.yyyy hh24:mi:ss')) GROUP BY   ATM_APPLICATION_ID, UNIT_NUMBER" ;
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DateTo}", dateTo);
        ResultSet rs = executeQueryReport(selectStatment);

        super.postSelect();
        return rs;
    }
}
