/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class UserPassDAO extends BaseDAO implements UserPassDAOInter {

    protected UserPassDAO() {
        super();
        super.setTableName("user_pass");
    }
 public Object findBeforeLastPassBranch(int id, String newPass) throws Throwable {
        super.preSelect();

        String selectStat = "select Count(*) counter from $table where USER_ID = $id and pass = conv.conv_string('$pass')";
        selectStat = selectStat.replace("$table", "USER_PASS_BRANCH" );
        selectStat = selectStat.replace("$id", "" + id);
        selectStat = selectStat.replace("$pass", "" + newPass);
        ResultSet rs = executeQuery(selectStat);
        int counter = 0;
        while (rs.next()) {
            counter = rs.getInt("counter");
        }
        if (counter > 0) {
            super.postSelect(rs);
            return true;
        } else {
            super.postSelect(rs);
            return false;
        }
    }

    public Object findBeforeLastPass(int id, String newPass) throws Throwable {
        super.preSelect();

        String selectStat = "select Count(*) counter from $table where USER_ID = $id and pass = conv.conv_string('$pass')";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        selectStat = selectStat.replace("$pass", "" + newPass);
        ResultSet rs = executeQuery(selectStat);
        int counter = 0;
        while (rs.next()) {
            counter = rs.getInt("counter");
        }
        if (counter > 0) {
            super.postSelect(rs);
            return true;
        } else {
            super.postSelect(rs);
            return false;
        }
    }

    public Object findLastUpdate(int id) throws Throwable {
        super.preSelect();

        String selectStat = "select max(CHANGE_DATE) from $table where USER_ID = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        ResultSet rs = executeQuery(selectStat);
        Date lastChange = null;
        while (rs.next()) {
            lastChange = rs.getTimestamp(1);
        }
        super.postSelect(rs);
        return lastChange;
    }

    public Object findBeforeLastUpdate(int id) throws Throwable {
        super.preSelect();
        Date last = (Date) findLastUpdate(id);
        String selectStat = "select max(CHANGE_DATE) count from $table where USER_ID = $id and change_date <> to_date('$date','dd.mm.yyyy hh24:mi:ss')";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        selectStat = selectStat.replace("$date", "" + DateFormatter.changeDateAndTimeFormat(last));
        ResultSet rs = executeQuery(selectStat);
        int count = 0;
        while (rs.next()) {
            count = rs.getInt("count");
        }
        if (count > 0) {
            super.postSelect(rs);
            return true;
        }
        super.postSelect(rs);
        return false;
    }

    public Object findLastPass(int id) throws Throwable {
        super.preSelect();

        Date lastDate = (Date) findLastUpdate(id);

        String selectStat = "select PASS from $table where CHANGE_DATE = to_date('$date','dd.mm.yyyy hh24:mi:ss')";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        selectStat = selectStat.replace("$date", "" + DateFormatter.changeDateAndTimeFormat(lastDate));
        ResultSet rs = executeQuery(selectStat);
        String lastChange = null;
        while (rs.next()) {
            lastChange = rs.getString(1);
        }
        super.postSelect(rs);
        return lastChange;
    }

    public Object findBeforeLastPass(int id) throws Throwable {
        super.preSelect();

        Date lastDate = (Date) findBeforeLastUpdate(id);

        String selectStat = "select PASS from $table where CHANGE_DATE = to_date('$date','dd.mm.yyyy hh24:mi:ss')";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        selectStat = selectStat.replace("$date", "" + DateFormatter.changeDateAndTimeFormat(lastDate));
        ResultSet rs = executeQuery(selectStat);
        String lastChange = null;
        while (rs.next()) {
            lastChange = rs.getString("pass");
        }
        super.postSelect(rs);
        return lastChange;
    }
}
