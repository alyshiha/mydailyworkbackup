/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.MenuLabelDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class MenuLabelDAO extends BaseDAO implements MenuLabelDAOInter {

    protected MenuLabelDAO() {
        super();
        super.setTableName("menu_labels");
    }

    public Boolean findmenuitemcheck(int user, String Page) throws Throwable {

        super.preSelect();
        String selectStat = "select m.page from menu_labels m,user_profile p,profile_menu s where p.user_id = '$UserID' and LOWER(m.page) = LOWER('$Page') and s.profile_id = p.profile_id and s.menu_id = m.menu_id and m.page is not null";
        selectStat = selectStat.replace("$UserID", "" + user);
        selectStat = selectStat.replace("$Page", "" + Page);
        ResultSet rs = executeQuery(selectStat);
        List<MenuLabelDTOInter> uDTOL = new ArrayList<MenuLabelDTOInter>();
        Boolean found = Boolean.FALSE;
        if (rs.next()) {
            found = Boolean.TRUE;
        }
        super.postSelect(rs);
        return found;
    }

    public Object updateRest(MenuLabelDTOInter mlDTO) throws Throwable {
        super.preUpdate();
        String updatStat = "update $table set restrected = $rest where menu_id = $id";
        updatStat = updatStat.replace("$table", "" + super.getTableName());
        updatStat = updatStat.replace("$rest", "" + mlDTO.getRestrected());
        updatStat = updatStat.replace("$id", "" + mlDTO.getMenuId());
        super.executeUpdate(updatStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object findAllPage(int user) throws Throwable {
        super.preSelect();
        String selectStat = "select REPLACE(REPLACE(t.page,'?id=2'),'?id=1') PageLink from (select m.* from  menu_labels  m, profile_menu p where visability in (1,3) and m.menu_id = p.menu_id and p.profile_id = (select profile_id from user_profile where user_id = $UserID)  order BY parent_id,seq nulls last ,english_label) t connect by prior  t.menu_id  = t.parent_id start with t.parent_id is null  ORDER SIBLINGS BY seq nulls last , english_label ";
        selectStat = selectStat.replace("$UserID", "" + user);
        ResultSet rs = executeQuery(selectStat);
        List<String> uDTOL = new ArrayList<String>();

        while (rs.next()) {

            String Value = rs.getString("PageLink") ;
            uDTOL.add(Value);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findAll(int user) throws Throwable {

        super.preSelect();
        String selectStat = "select t.menu_id MenuID, t.page PageLink,t.english_label EngLabel, t.parent_id  ParentID, level LevelID from (select m.* from  menu_labels  m, profile_menu p where visability in (1,3) and m.menu_id = p.menu_id and p.profile_id = (select profile_id from user_profile where user_id = $UserID)order BY parent_id,seq nulls last ,english_label) t connect by prior  t.menu_id  = t.parent_id start with t.parent_id is null ORDER SIBLINGS BY seq nulls last , english_label";
        selectStat = selectStat.replace("$UserID", "" + user);
        ResultSet rs = executeQuery(selectStat);
        List<MenuLabelDTOInter> uDTOL = new ArrayList<MenuLabelDTOInter>();

        while (rs.next()) {

            MenuLabelDTOInter uDTO = DTOFactory.createMenuLabelDTO();
            uDTO.setMenuId(rs.getInt("MenuID"));
            uDTO.setParentId(rs.getInt("ParentID"));
            uDTO.setPage(rs.getString("PageLink"));
            uDTO.setEnglishLabel(rs.getString("EngLabel"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findParents(int profileId) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table m where parent_id is null and VISABILITY in(1,3) and exists(select 1 from profile_menu where profile_id = $prof and menu_id = m.menu_id) order by seq";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$prof", "" + profileId);
        ResultSet rs = executeQuery(selectStat);
        List<MenuLabelDTOInter> uDTOL = new ArrayList<MenuLabelDTOInter>();
        while (rs.next()) {
            MenuLabelDTOInter uDTO = DTOFactory.createMenuLabelDTO();
            uDTO.setArabicLabel(rs.getString("arabic_label"));
            uDTO.setEnglishLabel(rs.getString("english_label"));
            uDTO.setMenuId(rs.getInt("menu_id"));
            uDTO.setParentId(rs.getInt("parent_id"));
            uDTO.setPage(rs.getString("page"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findsParents(int rest) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table m where parent_id is null and VISABILITY in(1,3) and ($rest = 1 or restrected = 2) order by seq";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$rest", "" + "" + rest);
        ResultSet rs = executeQuery(selectStat);
        List<MenuLabelDTOInter> uDTOL = new ArrayList<MenuLabelDTOInter>();
        while (rs.next()) {
            MenuLabelDTOInter uDTO = DTOFactory.createMenuLabelDTO();
            uDTO.setArabicLabel(rs.getString("arabic_label"));
            uDTO.setEnglishLabel(rs.getString("english_label"));
            uDTO.setMenuId(rs.getInt("menu_id"));
            uDTO.setParentId(rs.getInt("parent_id"));
            uDTO.setPage(rs.getString("page"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByparentId(int parentId, int profileId) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table m where parent_id = $parentId and VISABILITY in(1,3) "
                + "and exists(select 1 from profile_menu where profile_id = $prof and menu_id = m.menu_id) order by seq";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$parentId", "" + parentId);
        selectStat = selectStat.replace("$prof", "" + profileId);
        ResultSet rs = executeQuery(selectStat);
        List<MenuLabelDTOInter> uDTOL = new ArrayList<MenuLabelDTOInter>();
        while (rs.next()) {
            MenuLabelDTOInter uDTO = DTOFactory.createMenuLabelDTO();
            uDTO.setArabicLabel(rs.getString("arabic_label"));
            uDTO.setEnglishLabel(rs.getString("english_label"));
            uDTO.setMenuId(rs.getInt("menu_id"));
            uDTO.setParentId(rs.getInt("parent_id"));
            uDTO.setPage(rs.getString("page"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByparentId(int parentId, int rest, int x) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table m where parent_id = $parentId and VISABILITY in(1,3) and ($rest = 1 or restrected = 2) order by seq";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$parentId", "" + parentId);
        selectStat = selectStat.replace("$rest", "" + rest);
        ResultSet rs = executeQuery(selectStat);
        List<MenuLabelDTOInter> uDTOL = new ArrayList<MenuLabelDTOInter>();
        while (rs.next()) {
            MenuLabelDTOInter uDTO = DTOFactory.createMenuLabelDTO();
            uDTO.setArabicLabel(rs.getString("arabic_label"));
            uDTO.setEnglishLabel(rs.getString("english_label"));
            uDTO.setMenuId(rs.getInt("menu_id"));
            uDTO.setParentId(rs.getInt("parent_id"));
            uDTO.setPage(rs.getString("page"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object find(int id) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where VISABILITY in(1,3) and menu_id = $id order by seq";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        ResultSet rs = executeQuery(selectStat);
        MenuLabelDTOInter uDTO = DTOFactory.createMenuLabelDTO();
        while (rs.next()) {

            uDTO.setArabicLabel(rs.getString("arabic_label"));
            uDTO.setEnglishLabel(rs.getString("english_label"));
            uDTO.setMenuId(rs.getInt("menu_id"));
            uDTO.setParentId(rs.getInt("parent_id"));
            uDTO.setPage(rs.getString("page"));
            uDTO.setRestrected(rs.getInt("restrected"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findLinks() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where page is not null order by english_label";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<MenuLabelDTOInter> uDTOL = new ArrayList<MenuLabelDTOInter>();
        while (rs.next()) {
            MenuLabelDTOInter uDTO = DTOFactory.createMenuLabelDTO();
            uDTO.setArabicLabel(rs.getString("arabic_label"));
            uDTO.setEnglishLabel(rs.getString("english_label"));
            uDTO.setMenuId(rs.getInt("menu_id"));
            uDTO.setParentId(rs.getInt("parent_id"));
            uDTO.setPage(rs.getString("page"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }
}
