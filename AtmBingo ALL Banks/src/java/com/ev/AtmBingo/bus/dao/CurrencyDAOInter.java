/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;

/**
 *
 * @author Administrator
 */
public interface CurrencyDAOInter {

    Object findByMaster(CurrencyMasterDTOInter cmDTO)throws Throwable;
int CountCurrencydetail(int cmDTO)throws Throwable;
    Object delete(Object... obj) throws Throwable;

    Object find(Object... obj) throws Throwable;

    Object findAll() throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
