/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.HostFileTemplateDTOInter;

/**
 *
 * @author Administrator
 */
public interface HostFileTemplateDetailDAOInter {

    Object findByColumnId(Object... obj) throws Throwable;

    Object delete(Object... obj) throws Throwable;

    Object find(Object... obj) throws Throwable;

    Object findAll() throws Throwable;

    Object findByTemplate(HostFileTemplateDTOInter aftDTO) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

    Object findByColumnIdAndTemplate(Object... obj) throws Throwable;

}
