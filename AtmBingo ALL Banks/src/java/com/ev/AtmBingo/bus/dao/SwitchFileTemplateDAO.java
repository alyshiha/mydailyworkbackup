/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.SwitchFileTemplateDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class SwitchFileTemplateDAO extends BaseDAO implements SwitchFileTemplateDAOInter {

    protected SwitchFileTemplateDAO() {
        super();
        super.setTableName("switch_file_template");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SwitchFileTemplateDTOInter uDTO = (SwitchFileTemplateDTOInter) obj[0];
        uDTO.setId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ($id , '$name', $processingType, "
                + "$numberOfLines, '$loadingFolder', $startingDataType, '$startingFormat', "
                + "$startingPosition, '$buckupFolder', '$copyFolder', $ignoredLines, $startingLength, "
                + "'$dateSeparator', $dSPos1,$dSsPos2, '$serverFolder', $headerDataType, "
                + "'$headerFormat', '$headerPosition', $headerLength, '$headerDateSeparator', "
                + "$hDSPos1,$hDSPs2, '$headerString', '$startingValue', "
                + " $active, '$separator','$TAGSENDINGDATATYPE','$TAGSENDINGFORMAT','$TAGSENDINGPOSITION','$TAGSENDINGLENGTH','$TAGSENDINGVALUE')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$name", "" + uDTO.getName());
//insertStat = insertStat.replace("$atmadd", "" + uDTO.getAtmadd());
        insertStat = insertStat.replace("$TAGSENDINGDATATYPE", "" + uDTO.getTagenddatatype());
        insertStat = insertStat.replace("$TAGSENDINGFORMAT", "" + uDTO.getTagendformate());
        insertStat = insertStat.replace("$TAGSENDINGPOSITION", "" + uDTO.getTagendpostition());
        insertStat = insertStat.replace("$TAGSENDINGLENGTH", "" + uDTO.getTagendlength());
        insertStat = insertStat.replace("$TAGSENDINGVALUE", "" + uDTO.getTagendvalue());
        insertStat = insertStat.replace("$processingType", "" + uDTO.getProcessingType());
        insertStat = insertStat.replace("$numberOfLines", "" + uDTO.getNumberOfLines());
        insertStat = insertStat.replace("$loadingFolder", "" + uDTO.getLoadingFolder());
        insertStat = insertStat.replace("$startingDataType", "" + uDTO.getStartingDataType());
        insertStat = insertStat.replace("$startingFormat", "" + uDTO.getStartingFormat());
        insertStat = insertStat.replace("$startingPosition", "" + uDTO.getStartingPosition());
        insertStat = insertStat.replace("$buckupFolder", "" + uDTO.getBuckupFolder());
        insertStat = insertStat.replace("$copyFolder", "" + uDTO.getCopyFolder());
        insertStat = insertStat.replace("$ignoredLines", "" + uDTO.getIgnoredLines());
        insertStat = insertStat.replace("$startingLength", "" + uDTO.getStartingLength());
        insertStat = insertStat.replace("$dateSeparator", "" + uDTO.getDateSeparator());
        insertStat = insertStat.replace("$dSPos1", "" + uDTO.getDateSeparatorPos1());
        insertStat = insertStat.replace("$dSsPos2", "" + uDTO.getDateSeparatorPos2());
        insertStat = insertStat.replace("$serverFolder", "" + uDTO.getServerFolder());
        insertStat = insertStat.replace("$headerDataType", "" + uDTO.getHeaderDataType());
        insertStat = insertStat.replace("$headerFormat", "" + uDTO.getHeaderFormat());
        insertStat = insertStat.replace("$headerPosition", "" + uDTO.getHeaderPosition());
        insertStat = insertStat.replace("$headerLength", "" + uDTO.getHeaderLength());
        insertStat = insertStat.replace("$headerDateSeparator", "" + uDTO.getHeaderDateSeparator());
        insertStat = insertStat.replace("$hDSPos1", "" + uDTO.getHeaderDateSeparatorPos1());
        insertStat = insertStat.replace("$hDSPs2", "" + uDTO.getHeaderDateSeparatorPos2());
        insertStat = insertStat.replace("$headerString", "" + uDTO.getHeaderString());
        insertStat = insertStat.replace("$startingValue", "" + uDTO.getStartingValue());
        insertStat = insertStat.replace("$active", "" + uDTO.getActive());
        insertStat = insertStat.replace("$separator", "" + uDTO.getSeparator());
        super.executeUpdate(insertStat);
        String action = "Add a new Switch template with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SwitchFileTemplateDTOInter uDTO = (SwitchFileTemplateDTOInter) obj[0];
        String updateStat = "update $table set  name = '$name', processing_type = $processingType"
                + ", number_of_lines = $numberOfLines, loading_folder = '$lodingFolder', starting_data_type = $startingDataType"
                + ", starting_format = '$startingFormat', starting_position = $startingPosition, backup_folder = '$buckupFolder'"
                + ", copy_folder = '$copyFolder', ignored_lines = $ignoredLines, starting_length = $startingLength"
                + ", date_separator = '$dateSparator', date_separator_pos1 = $dSeparatorPos1, date_separator_pos2 = $dateSeparatorPos2"
                + ", server_folder = '$serverFolder', header_data_type = $headerDataType, header_format = '$headerFormat'"
                + ", header_position = '$headerPosition', header_length = $headerLength, header_date_separator = '$headerDateSparator'"
                + ", header_date_separator_pos1 = $hDateSeparatorPos1, header_date_separator_pos2 = $headerDateSeparatorPos2"
                + ", starting_value = '$startingValue', active = $active, separator = '$separator'"
                + ",TAGS_ENDING_DATA_TYPE ='$TAGSENDINGDATATYPE' , TAGS_ENDING_FORMAT= '$TAGSENDINGFORMAT',TAGS_ENDING_POSITION= '$TAGSENDINGPOSITION'"
                + ", TAGS_ENDING_LENGTH='$TAGSENDINGLENGTH',TAGS_ENDING_VALUE ='$TAGSENDINGVALUE',header_string='$headerString'"
                + " where id = $id";
        //updateStat = updateStat.replace("$atmadd", "" + uDTO.getAtmadd());
        updateStat = updateStat.replace("$TAGSENDINGDATATYPE", "" + uDTO.getTagenddatatype());
        updateStat = updateStat.replace("$TAGSENDINGFORMAT", "" + uDTO.getTagendformate());
        updateStat = updateStat.replace("$TAGSENDINGPOSITION", "" + uDTO.getTagendpostition());
        updateStat = updateStat.replace("$headerString", "" + uDTO.getHeaderString());
        updateStat = updateStat.replace("$TAGSENDINGLENGTH", "" + uDTO.getTagendlength());
        updateStat = updateStat.replace("$TAGSENDINGVALUE", "" + uDTO.getTagendvalue());
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$name", "" + uDTO.getName());
        updateStat = updateStat.replace("$processingType", "" + uDTO.getProcessingType());
        updateStat = updateStat.replace("$numberOfLines", "" + uDTO.getNumberOfLines());
        updateStat = updateStat.replace("$lodingFolder", "" + uDTO.getLoadingFolder());
        updateStat = updateStat.replace("$startingDataType", "" + uDTO.getStartingDataType());
        updateStat = updateStat.replace("$startingFormat", "" + uDTO.getStartingFormat());
        updateStat = updateStat.replace("$startingPosition", "" + uDTO.getStartingPosition());
        updateStat = updateStat.replace("$buckupFolder", "" + uDTO.getBuckupFolder());
        updateStat = updateStat.replace("$copyFolder", "" + uDTO.getCopyFolder());
        updateStat = updateStat.replace("$ignoredLines", "" + uDTO.getIgnoredLines());
        updateStat = updateStat.replace("$startingLength", "" + uDTO.getStartingLength());
        updateStat = updateStat.replace("$dateSparator", "" + uDTO.getDateSeparator());
        updateStat = updateStat.replace("$dSeparatorPos1", "" + uDTO.getDateSeparatorPos1());
        updateStat = updateStat.replace("$dateSeparatorPos2", "" + uDTO.getDateSeparatorPos2());
        updateStat = updateStat.replace("$serverFolder", "" + uDTO.getServerFolder());
        updateStat = updateStat.replace("$headerDataType", "" + uDTO.getHeaderDataType());
        updateStat = updateStat.replace("$headerFormat", "" + uDTO.getHeaderFormat());
        updateStat = updateStat.replace("$headerPosition", "" + uDTO.getHeaderPosition());
        updateStat = updateStat.replace("$headerLength", "" + uDTO.getHeaderLength());
        updateStat = updateStat.replace("$headerDateSparator", "" + uDTO.getHeaderDateSeparator());
        updateStat = updateStat.replace("$hDateSeparatorPos1", "" + uDTO.getHeaderDateSeparatorPos1());
        updateStat = updateStat.replace("$headerDateSeparatorPos2", "" + uDTO.getHeaderDateSeparatorPos2());
        updateStat = updateStat.replace("$startingValue", "" + uDTO.getStartingValue());
        updateStat = updateStat.replace("$active", "" + uDTO.getActive());
        updateStat = updateStat.replace("$separator", "" + uDTO.getSeparator());

        super.executeUpdate(updateStat);
        String action = "Update Switch template with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SwitchFileTemplateDTOInter uDTO = (SwitchFileTemplateDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        String action = "Delete Switch template with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer userId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + userId);
        ResultSet rs = executeQuery(selectStat);
        SwitchFileTemplateDTOInter uDTO = DTOFactory.createSwitchFileTemplateDTO();
        while (rs.next()) {
            uDTO.setTagenddatatype(rs.getInt("tags_ending_data_type"));
            uDTO.setTagendpostition(rs.getInt("tags_ending_position"));
            uDTO.setTagendlength(rs.getInt("tags_ending_length"));
            uDTO.setTagendformate(rs.getString("tags_ending_format"));
            //uDTO.setAtmadd(rs.getString("atm_add"));
            uDTO.setTagendvalue(rs.getString("tags_ending_value"));
            uDTO.setActive(rs.getInt("active"));
            uDTO.setBuckupFolder(rs.getString("backup_folder"));
            uDTO.setCopyFolder(rs.getString("copy_folder"));
            uDTO.setDateSeparator(rs.getString("date_separator"));
            uDTO.setDateSeparatorPos1(rs.getBigDecimal("date_separator_pos1"));
            uDTO.setDateSeparatorPos2(rs.getBigDecimal("date_separator_pos2"));
            uDTO.setHeaderDataType(rs.getBigDecimal("header_data_type"));
            uDTO.setHeaderDateSeparator(rs.getString("header_date_separator"));
            uDTO.setHeaderDateSeparatorPos1(rs.getBigDecimal("header_date_separator_pos1"));
            uDTO.setHeaderDateSeparatorPos2(rs.getBigDecimal("header_date_separator_pos2"));
            uDTO.setHeaderFormat(rs.getString("header_format"));
            uDTO.setHeaderLength(rs.getBigDecimal("header_length"));
            uDTO.setHeaderPosition(rs.getString("header_position"));
            uDTO.setHeaderString(rs.getString("header_string"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setIgnoredLines(rs.getBigDecimal("ignored_lines"));
            uDTO.setLoadingFolder(rs.getString("loading_folder"));
            uDTO.setName(rs.getString("name"));
            uDTO.setNumberOfLines(rs.getBigDecimal("number_of_lines"));
            uDTO.setProcessingType(rs.getBigDecimal("processing_type"));
            uDTO.setSeparator(rs.getString("separator"));
            uDTO.setServerFolder(rs.getString("server_folder"));
            uDTO.setStartingDataType(rs.getBigDecimal("starting_data_type"));
            uDTO.setStartingFormat(rs.getString("starting_format"));
            uDTO.setStartingLength(rs.getBigDecimal("starting_length"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));
            uDTO.setStartingValue(rs.getString("starting_value"));
 uDTO.setHeaderString(rs.getString("header_string"));

        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<SwitchFileTemplateDTOInter> uDTOL = new ArrayList<SwitchFileTemplateDTOInter>();
        while (rs.next()) {
            SwitchFileTemplateDTOInter uDTO = DTOFactory.createSwitchFileTemplateDTO();
            uDTO.setTagenddatatype(rs.getInt("tags_ending_data_type"));
            uDTO.setTagendpostition(rs.getInt("tags_ending_position"));
            uDTO.setTagendlength(rs.getInt("tags_ending_length"));
            uDTO.setTagendformate(rs.getString("tags_ending_format"));
            uDTO.setTagendvalue(rs.getString("tags_ending_value"));
           // uDTO.setAtmadd(rs.getString("atm_add"));
            uDTO.setActive(rs.getInt("active"));
            uDTO.setBuckupFolder(rs.getString("backup_folder"));
            uDTO.setCopyFolder(rs.getString("copy_folder"));
            uDTO.setDateSeparator(rs.getString("date_separator"));
            uDTO.setDateSeparatorPos1(rs.getBigDecimal("date_separator_pos1"));
            uDTO.setDateSeparatorPos2(rs.getBigDecimal("date_separator_pos2"));
            uDTO.setHeaderDataType(rs.getBigDecimal("header_data_type"));
            uDTO.setHeaderDateSeparator(rs.getString("header_date_separator"));
            uDTO.setHeaderDateSeparatorPos1(rs.getBigDecimal("header_date_separator_pos1"));
            uDTO.setHeaderDateSeparatorPos2(rs.getBigDecimal("header_date_separator_pos2"));
            uDTO.setHeaderFormat(rs.getString("header_format"));
            uDTO.setHeaderLength(rs.getBigDecimal("header_length"));
            uDTO.setHeaderPosition(rs.getString("header_position"));
            uDTO.setHeaderString(rs.getString("header_string"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setIgnoredLines(rs.getBigDecimal("ignored_lines"));
            uDTO.setLoadingFolder(rs.getString("loading_folder"));
            uDTO.setName(rs.getString("name"));
            uDTO.setNumberOfLines(rs.getBigDecimal("number_of_lines"));
            uDTO.setProcessingType(rs.getBigDecimal("processing_type"));
            uDTO.setSeparator(rs.getString("separator"));
            uDTO.setServerFolder(rs.getString("server_folder"));
            uDTO.setStartingDataType(rs.getBigDecimal("starting_data_type"));
            uDTO.setStartingFormat(rs.getString("starting_format"));
            uDTO.setStartingLength(rs.getBigDecimal("starting_length"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));
            uDTO.setStartingValue(rs.getString("starting_value"));
             uDTO.setHeaderString(rs.getString("header_string"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public static void main(String[] args) throws Throwable {
    }
}
