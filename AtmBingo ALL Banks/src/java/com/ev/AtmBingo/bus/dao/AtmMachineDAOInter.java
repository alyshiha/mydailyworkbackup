/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AtmMachineDAOInter {

    List<String> findUserMachines(Integer loggedinUser, Integer group) throws Throwable;

    List<String> findNotUserMachines(Integer loggedinUser, Integer group) throws Throwable;

    List<String> findUserCassetteMachines(Integer loggedinUser, Integer Cassettegroup) throws Throwable;


    List<String> findUserUnAssignMachines(Integer loggedinUser, Integer Cassettegroup) throws Throwable;

    List<String> findUserUnAssign(Integer loggedinUser, Integer Cassettegroup,int status,Integer repgroup) throws Throwable;

    List<String> findNotUserCassetteMachines(Integer loggedinUser, Integer Cassettegroup) throws Throwable;
Object findRecordOutLic(Object... obj) throws Throwable;
    Object findRecordGroupAll(Object... obj) throws Throwable;

    Object findByAtmId(Object... obj) throws Throwable;

    Object findMachinesBelongUser(Object... obj) throws Throwable;

    Object findMachinesNotBelongUser(Object... obj) throws Throwable;

    Object delete(Object... obj) throws Throwable;

    Object find(Object... obj) throws Throwable;

    Object findAll(Object... obj) throws Throwable;

    List<String> findByGrp(Object... obj) throws Throwable;
Object findByAtmGrp2(Object... obj) throws Throwable;
Object findByAtmGrp3(Object... obj) throws Throwable;
    Object findByAtmGrp(Object... obj) throws Throwable;
    Object findByAtmbranch(int logedUser) throws Throwable;

    Object deleteNewAtms(int logedUser) throws Throwable;

    Object findNewAtms(int logedinUser) throws Throwable;

    Object findRecordGroup(Object... obj) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

    Object findATMMachineGroupCount(Object... obj) throws Throwable;
}
