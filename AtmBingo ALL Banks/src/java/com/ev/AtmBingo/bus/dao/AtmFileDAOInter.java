/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface AtmFileDAOInter {
public Object findByDate(Date loadingfrom,Date loadingto) throws Throwable;
    Object delete(Object... obj) throws Throwable;

    Object find(Object... obj) throws Throwable;

    Object findAll( Date loadingfrom,Date loadingto,int FileType) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
