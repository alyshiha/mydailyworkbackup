/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.MissingJournalDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface MissingJournalsRepDAOInter {

    Object findReport(String dateFrom, String dateTo) throws Throwable;
    
    List<MissingJournalDTOInter> findMissingJournalsexcelReport(String dateFrom, String dateTo) throws Throwable ;

}
