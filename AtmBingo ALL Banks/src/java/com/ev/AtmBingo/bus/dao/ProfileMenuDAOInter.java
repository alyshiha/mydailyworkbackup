/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Administrator
 */
public interface ProfileMenuDAOInter {

    Object deleteByMenuIdAndProfileId(Object... obj) throws Throwable;

    Object findByProfileId(int profileId) throws Throwable;

    Object deleteByMenuId(Object... obj) throws Throwable;

    Object deleteByProfileId(Object... obj) throws Throwable;

    Object findAll(Object... obj) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

}
