/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.HolidaysDTOInter_1;
import java.util.List;

/**
 *
 * @author shi7a
 */
public interface HolidaysDAOInter_1 {

    String delete(Object... obj);

    List<HolidaysDTOInter_1> findAll() throws Throwable;

    List<HolidaysDTOInter_1> findRecord(String Type,String Name) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;
    
}
