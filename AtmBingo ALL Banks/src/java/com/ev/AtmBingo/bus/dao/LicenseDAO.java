/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.LicenseDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Administrator
 */
public class LicenseDAO extends BaseDAO implements LicenseDAOInter {

    protected LicenseDAO() {
        super();
        super.setTableName("license");
    }

    public Object findLicense() throws Throwable {
        preSelect();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        LicenseDTOInter lDTO = DTOFactory.createLicenseDTO();
        while (rs.next()) {
            lDTO.setAtmNO(rs.getInt("atm_no"));
            lDTO.setCountry(rs.getString("country"));
            lDTO.setCreatedDate(rs.getDate("created_date"));
            lDTO.setEndDate(rs.getDate("end_date"));
            lDTO.setLicenseKey(rs.getString("license_key"));
            lDTO.setLicenseTo(rs.getString("license_to"));
            lDTO.setNoOfUser(rs.getInt("no_of_users"));
            lDTO.setVersion(rs.getString("version"));
            
        }
        postSelect(rs);
        return lDTO;
    }

    public Boolean checkLic() throws Throwable {
        preCollable();
        String callStat = "select check_lic from dual";
        ResultSet rs = executeQuery(callStat);
        int valid = 2;
        while (rs.next()) {
            valid = rs.getInt(1);
        }
        
CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());
        return valid == 1 ? true : false;
    }
}
