/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.RejectedReportDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Administrator
 */
public class RejectedReportDAO extends BaseDAO implements RejectedReportDAOInter {

    protected RejectedReportDAO() {
        super();
        super.setTableName("rejected_report");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        RejectedReportDTOInter uDTO = (RejectedReportDTOInter) obj[0];
        String insertStat = "insert into $table values ('$atm_application_id', $atm_id"
                + ", '$transaction_type', $trnsaction_type_id, '$currency', $crrency_id, '$transaction_status', $transactin_status_id"
                + ", '$response_code', $respons_code_id, to_date('$transaction_date','dd.MM.yyyy hh24:mi:ss'), '$transaction_sequence', "+super.getencryptcardcolumn("$card_no")
                + ", $amount, to_date('$settlement_date','dd.MM.yyyy hh24:mi:ss'), '$notes_presented', '$customer_account_number', $trans_sequence_order_by"
                + ", to_date('$transaction_time','dd.MM.yyyy hh24:mi:ss'), $record_type,"
                + "'$column1', '$column2', '$column3', '$column4', '$column5',$user_id)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());


        insertStat = insertStat.replace("$atm_application_id", "" + uDTO.getAtmApplicationId());

        insertStat = insertStat.replace("$atm_id", "" + uDTO.getAtmId().getId());


        insertStat = insertStat.replace("$transaction_type", "" + uDTO.getTransactionType());

        insertStat = insertStat.replace("$trnsaction_type_id", "" + uDTO.getTransactionTypeId().getId());
        insertStat = insertStat.replace("$currency", "" + uDTO.getCurrency());
        insertStat = insertStat.replace("$crrency_id", "" + uDTO.getCurrencyId().getId());


        insertStat = insertStat.replace("$transaction_status", "" + uDTO.getTransactionStatus());

        insertStat = insertStat.replace("$transactin_status_id", "" + uDTO.getTransactionStatusId().getId());


        insertStat = insertStat.replace("$response_code", "" + uDTO.getResponseCode());

        insertStat = insertStat.replace("$respons_code_id", "" + uDTO.getResponseCodeId().getId());
        insertStat = insertStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactionDate()));


        insertStat = insertStat.replace("$transaction_sequence", "" + uDTO.getTransactionSeqeunce());

        insertStat = insertStat.replace("$card_no", "" + uDTO.getCardNo());

        insertStat = insertStat.replace("$amount", "" + uDTO.getAmount());
        insertStat = insertStat.replace("$settlement_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getSettlementDate()));
        insertStat = insertStat.replace("$notes_presented", "" + uDTO.getNotesPresented());

        if (uDTO.getCustomerAccountNumber() != null) {
            insertStat = insertStat.replace("$customer_account_number", "" + uDTO.getCustomerAccountNumber());
        } else {
            insertStat = insertStat.replace("'$customer_account_number'", "null");
        }

        insertStat = insertStat.replace("$trans_sequence_order_by", "" + uDTO.getTransactionSequenceOrderBy());
        insertStat = insertStat.replace("$transaction_time", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactionTime()));
        insertStat = insertStat.replace("$record_type", "" + uDTO.getRecordType());

        if (uDTO.getColumn1() != null) {
            insertStat = insertStat.replace("$column1", "" + uDTO.getColumn1());
        } else {
            insertStat = insertStat.replace("'$column1'", "" + uDTO.getColumn1());
        }

        if (uDTO.getColumn2() != null) {
            insertStat = insertStat.replace("$column2", "" + uDTO.getColumn2());
        } else {
            insertStat = insertStat.replace("'$column2'", "" + uDTO.getColumn2());
        }

        if (uDTO.getColumn3() != null) {
            insertStat = insertStat.replace("$column3", "" + uDTO.getColumn3());
        } else {
            insertStat = insertStat.replace("'$column3'", "" + uDTO.getColumn3());
        }

        if (uDTO.getColumn4() != null) {
            insertStat = insertStat.replace("$column4", "" + uDTO.getColumn4());
        } else {
            insertStat = insertStat.replace("'$column4'", "" + uDTO.getColumn4());
        }

        if (uDTO.getColumn5() != null) {
            insertStat = insertStat.replace("$column5", "" + uDTO.getColumn5());
        } else {
            insertStat = insertStat.replace("'$column5'", "" + uDTO.getColumn5());
        }

        insertStat = insertStat.replace("$user_id", "" + uDTO.getUserId());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(int userId) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String deleteStat = "delete from $table where user_id = $userId";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$userId", "" + userId);

        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object findByUserId(int userId, String fields) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        fields = fields.replace(",CURRENCY", "");
        fields = fields.replace("CURRENCY,", "");
        fields = fields.replace("CURRENCY", "");
        fields = fields.replace(", ,", ",");
        fields = fields.replace(",  ,", ",");
        fields = fields.replace(",   ,", ",");
        fields = fields.replace(",,", ",");
        String selectStat = "select decode(record_type,1,'JR',2,'SW',3,'GL')record_type,"
                + "  $field ,"
                + " atm_application_id,"
                + " currency,"
                + " (select unit_number from atm_machine m where m.application_id = atm_application_id)unit_id,"
                + " (select name from atm_machine m where m.application_id = atm_application_id)name"
                + " from $table where user_id = $userId order by atm_application_id,TRANSACTION_DATE";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$userId", "" + userId);
        selectStat = selectStat.replace("$field", "" + fields);
        selectStat = selectStat.replace("card_no", "" + super.getcardcolumn() + " card_no");
        ResultSet rs = executeQueryReport(selectStat);

        super.postSelect();
        return rs;
    }
}
