/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.SwitchFileHeaderDTOInter;
import com.ev.AtmBingo.bus.dto.SwitchFileTemplateDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class SwitchFileHeaderDAO extends BaseDAO implements SwitchFileHeaderDAOInter {

    protected SwitchFileHeaderDAO() {
        super();
        super.setTableName("switch_file_template_header");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SwitchFileHeaderDTOInter uDTO = (SwitchFileHeaderDTOInter) obj[0];
        uDTO.setId(super.generateSequence("switch_file_template_hdr"));
        String insertStat = "insert into $table values ($id,$columnId, $template  , $position, '$format', '$formt2',"
                + " $dataType, $length)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$template", "" + uDTO.getTemplate());
        insertStat = insertStat.replace("$columnId", "" + uDTO.getColumnId());
        insertStat = insertStat.replace("$position", "" + uDTO.getPosition());
        insertStat = insertStat.replace("$format", "" + uDTO.getFormat());
        insertStat = insertStat.replace("$formt2", "" + uDTO.getFormat2());
        insertStat = insertStat.replace("$dataType", "" + uDTO.getDataType());
        insertStat = insertStat.replace("$length", "" + uDTO.getLength());
        super.executeUpdate(insertStat);
        SwitchFileTemplateDAOInter sftDAO = DAOFactory.createSwitchFileTemplateDAO(null);
        SwitchFileTemplateDTOInter sftDTO = (SwitchFileTemplateDTOInter) sftDAO.find(uDTO.getTemplate());
        String action = "Add a new header template to Switch template's with name " + sftDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SwitchFileHeaderDTOInter uDTO = (SwitchFileHeaderDTOInter) obj[0];
        String updateStat = "update $table set template = $template, column_id = $columnId, position = $position"
                + ", format = '$format', format2 = '$formt2', data_type = $dataType, length = $length"
                + "where id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$template", "" + uDTO.getTemplate());
        updateStat = updateStat.replace("$columnId", "" + uDTO.getColumnId());
        updateStat = updateStat.replace("$position", "" + uDTO.getPosition());
        updateStat = updateStat.replace("$format", "" + uDTO.getFormat());
        updateStat = updateStat.replace("$formt2", "" + uDTO.getFormat2());
        updateStat = updateStat.replace("$dataType", "" + uDTO.getDataType());
        updateStat = updateStat.replace("$length", "" + uDTO.getLength());
        super.executeUpdate(updateStat);
        SwitchFileTemplateDAOInter sftDAO = DAOFactory.createSwitchFileTemplateDAO(null);
        SwitchFileTemplateDTOInter sftDTO = (SwitchFileTemplateDTOInter) sftDAO.find(uDTO.getTemplate());
        String action = "Update header template for Switch template's with name " + sftDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SwitchFileHeaderDTOInter uDTO = (SwitchFileHeaderDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        SwitchFileTemplateDAOInter sftDAO = DAOFactory.createSwitchFileTemplateDAO(null);
        SwitchFileTemplateDTOInter sftDTO = (SwitchFileTemplateDTOInter) sftDAO.find(uDTO.getTemplate());
        String action = "Delete header template for Switch template's with name " + sftDTO.getName();
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        SwitchFileHeaderDTOInter uDTO = DTOFactory.createSwitchFileHeaderDTO();
        while (rs.next()) {
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getInt("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getInt("length"));
            uDTO.setPosition(rs.getInt("position"));
            uDTO.setTemplate(rs.getInt("template"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<SwitchFileHeaderDTOInter> uDTOL = new ArrayList<SwitchFileHeaderDTOInter>();
        while (rs.next()) {
            SwitchFileHeaderDTOInter uDTO = DTOFactory.createSwitchFileHeaderDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getInt("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getInt("length"));
            uDTO.setPosition(rs.getInt("position"));
            uDTO.setTemplate(rs.getInt("template"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByTemplate(SwitchFileTemplateDTOInter aftDTO) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where template = $template";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$template", "" + aftDTO.getId());
        ResultSet rs = executeQuery(selectStat);
        List<SwitchFileHeaderDTOInter> uDTOL = new ArrayList<SwitchFileHeaderDTOInter>();
        while (rs.next()) {
            SwitchFileHeaderDTOInter uDTO = DTOFactory.createSwitchFileHeaderDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getInt("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getInt("length"));
            uDTO.setPosition(rs.getInt("position"));
            uDTO.setTemplate(rs.getInt("template"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
