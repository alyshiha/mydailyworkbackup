
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.AtmJournalDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AlyShiha
 */
public class AtmJournalDAO extends BaseDAO implements AtmJournalDAOInter {

    public void recalc123visa() throws Throwable {
        preCollable();
        CallableStatement stat = super.executeCallableStatment("{call ATM.CASH_MANAGEMENT.RUN_NETWORK_JOURNAL(((trunc(to_date(sysdate,'dd-mm-yyyy')) -2)+ get_number_parameter ('START_END_TIME_123JOB')),((trunc(to_date(sysdate,'dd-mm-yyyy')) -1)+ get_number_parameter ('START_END_TIME_123JOB')), 1, 1 )}");
        stat.execute();
        postCollable(stat);
        preCollable();
        CallableStatement stat2 = super.executeCallableStatment("{call ATM.CASH_MANAGEMENT.RUN_NETWORK_JOURNAL((trunc(to_date(sysdate,'dd-mm-yyyy')) -2),(trunc(to_date(sysdate,'dd-mm-yyyy')) -1), 2, 1 )}");
        stat2.execute();
        postCollable(stat2);
    }

    public void recalc() throws Throwable {
        preCollable();
        CallableStatement stat = super.executeCallableStatment("{call ATM.CASH_MANAGEMENT.CHECK_DIFF_NETWORKJOURNAL()}");
        stat.execute();
        postCollable(stat);
        CoonectionHandler.getInstance().returnConnection(stat.getConnection());
    }

    public void recalc2() throws Throwable {
        preCollable();
        CallableStatement stat = super.executeCallableStatment("{call ATM.CASH_MANAGEMENT.FILL_REPL_CASH_MANGEMENT()}");
        stat.execute();
        postCollable(stat);
        CoonectionHandler.getInstance().returnConnection(stat.getConnection());
    }

    @Override
    public List<AtmJournalDTOInter> findrecordjournal(String dateFrom, String dateTo, String NoOfAtms, int export, int group, String Reversal, int user, Boolean constractor, Boolean released) throws Throwable {
        super.preSelect();
        String Temp = "";
        if ("1".equals(Reversal)) {
            Temp = "AND OPERATION = '-1'";
        }
        if ("2".equals(Reversal)) {
            Temp = "AND OPERATION <> '-1'";
        }

        String selectStatment = "select atm_journal_id, rep_id, operation, export_flag, transaction_type,\n"
                + "       debit_account, amount, currency, currency_rate, credit_account, journal_date,\n"
                + "       journal_refernce,branch,indication, date_from, date_to, atm_id \n"
                + "from atm_journal\n"
                + "where REP_ID IS NULL\n"
                + "AND ( (journal_date >= TO_DATE ($P{DateFrom}, 'dd.MM.yyyy hh24:mi:ss')\n"
                + "                  AND journal_date <= TO_DATE ($P{DateTo}, 'dd.MM.yyyy hh24:mi:ss'))\n"
                + "                AND (EXPORT_FLAG IN ($constflag $releasedflag)) )\n"
                + "AND (atm_id = '$P{NoOfAtms}' OR '$P{NoOfAtms}' = 'All')\n"
                + "$reverse\n"
                + "AND (ATM_ID IN (SELECT   application_id\n"
                + "FROM   atm_machine\n"
                + "WHERE   atm_group IN\n"
                + "(SELECT   id FROM   atm_group WHERE   parent_id IN\n"
                + "(SELECT   id FROM   atm_group\n"
                + "WHERE   (parent_id = $atmgroup OR id = $atmgroup))\n"
                + "OR id = $atmgroup)) OR $atmgroup = 0)\n"
                + "AND ATM_ID in (select atm_machine.application_id \n"
                + "			   from user_atm,atm_machine \n"
                + "			   where user_atm.user_id = $user \n"
                + "			   and atm_machine.id = user_atm.atm_id) \n"
                + "order by ATM_ID,journal_date asc";
        selectStatment = selectStatment.replace("$P{DateFrom}", "'" + dateFrom + "'");
        selectStatment = selectStatment.replace("$P{DateTo}", "'" + dateTo + "'");
        selectStatment = selectStatment.replace("$P{NoOfAtms}", "" + NoOfAtms);
        selectStatment = selectStatment.replace("$export", "" + export);
        selectStatment = selectStatment.replace("$atmgroup", "" + group);
        selectStatment = selectStatment.replace("$reverse", "" + Temp);
        selectStatment = selectStatment.replace("$user", "" + user);
        if (constractor) {
            selectStatment = selectStatment.replace("$constflag", "2");
        } else {
            selectStatment = selectStatment.replace("$constflag", "1,2,3,4");
        }

        if (released) {
            selectStatment = selectStatment.replace("$releasedflag", ",8");
        } else {
            selectStatment = selectStatment.replace("$releasedflag", "");
        }
        ResultSet rs = executeQuery(selectStatment);
        List<AtmJournalDTOInter> records = new ArrayList<AtmJournalDTOInter>();
        while (rs.next()) {
            AtmJournalDTOInter record = DTOFactory.createAtmJournalDTO();
            record.setAtmid(rs.getString("atm_id"));
            record.setAmount(rs.getInt("amount"));
            record.setAtmjournalid(rs.getInt("atm_journal_id"));
            record.setCreditaccount(rs.getString("credit_account"));
            record.setCurrency(rs.getInt("currency"));
            record.setCurrencyrate(rs.getInt("currency_rate"));
            record.setDatefrom(rs.getTimestamp("date_from"));
            record.setDateto(rs.getTimestamp("date_to"));
            record.setDebitaccount(rs.getString("debit_account"));
            record.setJournaldate(rs.getTimestamp("journal_date"));
            record.setJournalrefernce(rs.getString("journal_refernce"));
            record.setTransactiontype(rs.getString("transaction_type"));
            record.setBranch(rs.getString("branch"));
            record.setIndication(rs.getString("indication"));

            record.setRepid(rs.getInt("rep_id"));
            record.setExportflag(rs.getInt("export_flag"));
            record.setOperation(rs.getInt("operation"));
            records.add(record);
        }

        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select record in atm Journal with datefrom: " + dateFrom + " dateto: " + dateTo + " atm id: " + NoOfAtms + " atm group: " + group);
        return records;
    }

    @Override
    public List<AtmJournalDTOInter> findrecord(String AppID, int repid, int export, String FromDate, Boolean released) throws Throwable {
        super.preSelect();
        String selectStat = "select atm_journal_id, rep_id, operation, export_flag, transaction_type, "
                + "debit_account, amount, currency, currency_rate, credit_account, journal_date, "
                + "journal_refernce, date_from,branch,indication, date_to,atm_id from atm_journal "
                + "where (( export_flag <> 0 and REP_ID = $REPID and "
                + " journal_date = to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') )"
                + " or ( export_flag <> 0 and REP_ID <> $REPID  "
                + " and  journal_date = to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') )) "
                + " and atm_id = '$AppID' order by date_from";
        selectStat = selectStat.replace("$REPID", "" + repid);
        selectStat = selectStat.replace("$export", "" + export);
        selectStat = selectStat.replace("$P{DateFrom}", "" + FromDate);
        selectStat = selectStat.replace("$AppID", "" + AppID);
        ResultSet rs = executeQuery(selectStat);
        List<AtmJournalDTOInter> records = new ArrayList<AtmJournalDTOInter>();
        while (rs.next()) {
            AtmJournalDTOInter record = DTOFactory.createAtmJournalDTO();
            record.setBranch(rs.getString("branch"));
            record.setIndication(rs.getString("indication"));
            record.setAmount(rs.getInt("amount"));
            record.setAtmjournalid(rs.getInt("atm_journal_id"));
            record.setCreditaccount(rs.getString("credit_account"));
            record.setAtmid(rs.getString("atm_id"));
            record.setCurrency(rs.getInt("currency"));
            record.setCurrencyrate(rs.getInt("currency_rate"));
            record.setDatefrom(rs.getTimestamp("date_from"));
            record.setDateto(rs.getTimestamp("date_to"));
            record.setDebitaccount(rs.getString("debit_account"));
            record.setJournaldate(rs.getTimestamp("journal_date"));
            record.setJournalrefernce(rs.getString("journal_refernce"));
            record.setTransactiontype(rs.getString("transaction_type"));
            record.setRepid(rs.getInt("rep_id"));
            record.setExportflag(rs.getInt("export_flag"));
            record.setOperation(rs.getInt("operation"));
            if (record.getExportflag() == 8) {
                if (released) {
                    records.add(record);
                }
            } else {
                records.add(record);
            }
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("found record in atm Journal with id: " + repid);
        return records;
    }

    @Override
    public List<AtmJournalDTOInter> findAll() throws Throwable {
        super.preSelect();
        String selectStat = "select atm_journal_id, rep_id, operation, export_flag, transaction_type, debit_account, amount, currency, currency_rate, credit_account, journal_date, "
                + "journal_refernce, date_from,branch,indication, date_to from atm_journal order by date_from";
        ResultSet rs = executeQuery(selectStat);
        List<AtmJournalDTOInter> records = new ArrayList<AtmJournalDTOInter>();
        while (rs.next()) {
            AtmJournalDTOInter record = DTOFactory.createAtmJournalDTO();
            record.setAmount(rs.getInt("amount"));
            record.setBranch(rs.getString("branch"));
            record.setIndication(rs.getString("indication"));
            record.setAtmjournalid(rs.getInt("atm_journal_id"));
            record.setCreditaccount(rs.getString("credit_account"));
            record.setCurrency(rs.getInt("currency"));
            record.setCurrencyrate(rs.getInt("currency_rate"));
            record.setDatefrom(rs.getTimestamp("date_from"));
            record.setDateto(rs.getTimestamp("date_to"));
            record.setDebitaccount(rs.getString("debit_account"));
            record.setJournaldate(rs.getTimestamp("journal_date"));
            record.setJournalrefernce(rs.getString("journal_refernce"));
            record.setTransactiontype(rs.getString("transaction_type"));
            record.setRepid(rs.getInt("rep_id"));
            record.setExportflag(rs.getInt("export_flag"));
            record.setOperation(rs.getInt("operation"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("get all records in atm Journal ");
        return records;
    }

    @Override
    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        AtmJournalDTOInter record = (AtmJournalDTOInter) obj[0];
        record.setAtmjournalid(super.generateSequence("ATM_JOURNAL"));
        String insertStat = "insert into atm_journal\n"
                + "  (atm_journal_id, rep_id, export_flag, operation, transaction_type, debit_account, amount, currency, currency_rate, credit_account, journal_date, journal_refernce, date_from, date_to)\n"
                + "values\n"
                + "  ($atmjournalid, $repid, $exportflag, $operation, '$transactiontype', '$debitaccount', $amount, $currency, $currencyrate, '$creditaccount', "
                + "to_date('$journaldate','dd.MM.yyyy hh24:mi:ss'), '$journalrefernce', to_date('$datefrom','dd.MM.yyyy hh24:mi:ss'), to_date('$dateto','dd.MM.yyyy hh24:mi:ss'))";
        insertStat = insertStat.replace("$transactiontype", "" + record.getTransactiontype());
        insertStat = insertStat.replace("$debitaccount", "" + record.getDebitaccount());
        insertStat = insertStat.replace("$amount", "" + record.getAmount());
        insertStat = insertStat.replace("$currency", "" + record.getCurrency());
        insertStat = insertStat.replace("$currencyrate", "" + record.getCurrencyrate());
        insertStat = insertStat.replace("$creditaccount", "" + record.getCreditaccount());
        insertStat = insertStat.replace("$journaldate", "" + DateFormatter.changeDateAndTimeFormat(record.getJournaldate()));
        insertStat = insertStat.replace("$journalrefernce", "" + record.getJournalrefernce());
        insertStat = insertStat.replace("$datefrom", "" + DateFormatter.changeDateAndTimeFormat(record.getDatefrom()));
        insertStat = insertStat.replace("$dateto", "" + DateFormatter.changeDateAndTimeFormat(record.getDateto()));
        insertStat = insertStat.replace("$repid", "" + record.getRepid());
        insertStat = insertStat.replace("$exportflag", "" + record.getExportflag());
        insertStat = insertStat.replace("$operation", "" + record.getOperation());
        super.executeUpdate(insertStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("insert new record in atm Journal with id: " + record.getAtmjournalid());
        return null;
    }

    @Override
    public Object update(Object... obj) throws Throwable {

        List<AtmJournalDTOInter> records = (List<AtmJournalDTOInter>) obj[0];
        System.out.println("update " + records.size());
        String flag = (String) obj[1];
        PreparedStatement statement = null;
        try {
            int i = 0;
            Connection conn = CoonectionHandler.getInstance().getConnection();

            statement = conn.prepareStatement("update atm_journal\n"
                    + "   set export_flag = ?\n"
                    + " where atm_journal_id = ?");
            for (AtmJournalDTOInter record : records) {
                i = i + 1;
                statement.setString(1, flag);
                statement.setInt(2, record.getAtmjournalid());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            statement.executeBatch();
            CoonectionHandler.getInstance().returnConnection(conn);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ATMMAchineLICDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ATMMAchineLICDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }

        }
        return null;
    }

    @Override
    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        AtmJournalDTOInter record = (AtmJournalDTOInter) obj[0];
        String deleteStat = "delete atm_journal\n"
                + " where atm_journal_id = $atmjournalid";
        deleteStat = deleteStat.replace("$id", "" + record.getAtmjournalid());
        super.executeUpdate(deleteStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("delete record in atm Journal with id: " + record.getAtmjournalid());
        return null;
    }

}
