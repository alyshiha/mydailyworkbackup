/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Administrator
 */
public interface UserProfileDAOInter {

    Object updateByUserId(Object... obj) throws Throwable;

    Object findByUserId(int userId) throws Throwable;

    Object deleteByProfileId(Object... obj) throws Throwable;

    Object deleteByUserId(Object... obj) throws Throwable;

    Object findAll(Object... obj) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

}
