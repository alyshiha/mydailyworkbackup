/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.AtmFileTemplateDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDetailDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AtmFileTemplateDetailDAOInter {
Object SaveAllDetail(List<AtmFileTemplateDetailDTOInter> filetemplatedetailList, String TName,Integer templateid) throws Throwable ;
    Object findByColumnId(Object... obj) throws Throwable ;
Object deleterecorddetailTemplate(Object... obj) throws Throwable ;
    Object findByTemplate(AtmFileTemplateDTOInter aftDTO,String TName)throws Throwable;

    Object delete(Object... obj) throws Throwable;

    Object find(Object... obj) throws Throwable;

    Object findAll() throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

    Object findByTemplateAndColumnId(Object... obj) throws Throwable;

}
