/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.AtmJournalDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface AtmJournalDAOInter {

    List<AtmJournalDTOInter> findrecordjournal(String dateFrom, String dateTo, String NoOfAtms, int export, int group, String Reversal, int user, Boolean constractor, Boolean realesed) throws Throwable;

    void recalc123visa() throws Throwable;

    Object delete(Object... obj) throws Throwable;

    void recalc() throws Throwable;

    void recalc2() throws Throwable;

    List<AtmJournalDTOInter> findAll() throws Throwable;

    List<AtmJournalDTOInter> findrecord(String AppID, int repid, int export, String FromDate, Boolean released) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
