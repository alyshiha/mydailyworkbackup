/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.replanishmentCashManagementDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface replanishmentCashManagementDAOInter {

    List<replanishmentCashManagementDTOInter> findRecord(String ATMID, Integer ATMGroup, String DateFrom, String DateTo, int user,Boolean release) throws Throwable;
    
}
