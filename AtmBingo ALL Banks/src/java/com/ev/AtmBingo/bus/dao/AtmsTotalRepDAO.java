/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.TotalWithDrawalsDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmsTotalRepDAO extends BaseDAO implements AtmsTotalRepDAOInter {

    protected AtmsTotalRepDAO() {
        super();
    }

    public Object findTotalswithdrawalsexcelReport(String dateFrom, String dateTo, int atmGroupInt) throws Throwable {
        super.preSelect();

        String selectStatment = "  SELECT   SUM (transaction) transaction,\n"
                + "           atm_application_id,\n"
                + "           atm_id,\n"
                + "           currency\n"
                + "    FROM   (SELECT           /*+parallel index(disputes DISPUTES_SUM_INDEX) */\n"
                + "                  DISTINCT\n"
                + "                     atm_id,\n"
                + "                     SUM (amount) OVER (PARTITION BY atm_id) transaction,\n"
                + "                     atm_application_id,\n"
                + "                     currency\n"
                + "              FROM   disputes\n"
                + "             WHERE   transaction_date BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                                       'dd.MM.yyyy hh24:mi:ss')\n"
                + "                                          AND  TO_DATE (\n"
                + "                                                  '$P{DatTo}',\n"
                + "                                                  'dd.MM.yyyy hh24:mi:ss'\n"
                + "                                               )\n"
                + "                     AND record_type = 2\n"
                + "                     AND matching_type = 1\n"
                + "                     AND EXISTS\n"
                + "                           (SELECT   1\n"
                + "                              FROM   TRANSACTION_TYPE_MASTER\n"
                + "                             WHERE   UPPER (name) LIKE UPPER ('%with%')\n"
                + "                                     AND TRANSACTION_TYPE_MASTER.id =\n"
                + "                                           DISPUTES.TRANSACTION_TYPE_MASTER)\n"
                + "                     AND ATM_ID IN\n"
                + "                              (SELECT   id\n"
                + "                                 FROM   atm_machine\n"
                + "                                WHERE   atm_group IN\n"
                + "                                              (SELECT   id\n"
                + "                                                 FROM   atm_group\n"
                + "                                                WHERE   parent_id IN\n"
                + "                                                              (SELECT   id\n"
                + "                                                                 FROM   atm_group\n"
                + "                                                                WHERE   (parent_id =\n"
                + "                                                                            $P{AtmGroupInt}\n"
                + "                                                                         OR id =\n"
                + "                                                                              $P{AtmGroupInt}))\n"
                + "                                                        OR 0 = $P{AtmGroupInt}))\n"
                + "            UNION\n"
                + "            SELECT /*+ parallel index (matched_data MATCHED_DATA_GRAPH_INDEX2) */\n"
                + "                  DISTINCT\n"
                + "                     atm_id,\n"
                + "                     SUM (amount) OVER (PARTITION BY atm_id) transaction,\n"
                + "                     atm_application_id,\n"
                + "                     currency\n"
                + "              FROM   matched_data\n"
                + "             WHERE   transaction_date BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                                       'dd.MM.yyyy hh24:mi:ss')\n"
                + "                                          AND  TO_DATE (\n"
                + "                                                  '$P{DatTo}',\n"
                + "                                                  'dd.MM.yyyy hh24:mi:ss'\n"
                + "                                               )\n"
                + "                     AND record_type = 2\n"
                + "                     AND matching_type = 1\n"
                + "                     AND EXISTS\n"
                + "                           (SELECT   1\n"
                + "                              FROM   TRANSACTION_TYPE_MASTER\n"
                + "                             WHERE   UPPER (name) LIKE UPPER ('%with%')\n"
                + "                                     AND TRANSACTION_TYPE_MASTER.id =\n"
                + "                                           matched_data.TRANSACTION_TYPE_MASTER)\n"
                + "                     AND ATM_ID IN\n"
                + "                              (SELECT   id\n"
                + "                                 FROM   atm_machine\n"
                + "                                WHERE   atm_group IN\n"
                + "                                              (SELECT   id\n"
                + "                                                 FROM   atm_group\n"
                + "                                                WHERE   parent_id IN\n"
                + "                                                              (SELECT   id\n"
                + "                                                                 FROM   atm_group\n"
                + "                                                                WHERE   (parent_id =\n"
                + "                                                                            $P{AtmGroupInt}\n"
                + "                                                                         OR id =\n"
                + "                                                                              $P{AtmGroupInt}))\n"
                + "                                                        OR 0 = $P{AtmGroupInt}))\n"
                + "            ORDER BY   atm_id) fulltable\n"
                + "   WHERE   atm_valid (atm_id) = 1\n"
                + "GROUP BY   atm_application_id, atm_id, currency";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DatTo}", dateTo);
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        ResultSet rs = executeQueryReport(selectStatment);
        List<TotalWithDrawalsDTOInter> Records = new ArrayList<TotalWithDrawalsDTOInter>();
        while (rs.next()) {
            TotalWithDrawalsDTOInter record = DTOFactory.createTotalWithDrawalsDTO();
            record.setAtm_application_id(rs.getString("atm_application_id"));
            record.setAtm_id(rs.getInt("atm_id"));
            record.setCurrency(rs.getString("currency"));
            record.setTransaction(rs.getInt("transaction"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findReport(String dateFrom, String dateTo, int atmGroupInt) throws Throwable {
        super.preSelect();
        String selectStatment = "  SELECT   SUM (transaction) transaction,\n"
                + "           atm_application_id,\n"
                + "           atm_id,\n"
                + "           currency\n"
                + "    FROM   (SELECT           /*+parallel index(disputes DISPUTES_SUM_INDEX) */\n"
                + "                  DISTINCT\n"
                + "                     atm_id,\n"
                + "                     SUM (amount) OVER (PARTITION BY atm_id) transaction,\n"
                + "                     atm_application_id,\n"
                + "                     currency\n"
                + "              FROM   disputes\n"
                + "             WHERE   transaction_date BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                                       'dd.MM.yyyy hh24:mi:ss')\n"
                + "                                          AND  TO_DATE (\n"
                + "                                                  '$P{DatTo}',\n"
                + "                                                  'dd.MM.yyyy hh24:mi:ss'\n"
                + "                                               )\n"
                + "                     AND record_type = 2\n"
                + "                     AND matching_type = 1\n"
                + "                     AND EXISTS\n"
                + "                           (SELECT   1\n"
                + "                              FROM   TRANSACTION_TYPE_MASTER\n"
                + "                             WHERE   UPPER (name) LIKE UPPER ('%with%')\n"
                + "                                     AND TRANSACTION_TYPE_MASTER.id =\n"
                + "                                           DISPUTES.TRANSACTION_TYPE_MASTER)\n"
                + "                     AND ATM_ID IN\n"
                + "                              (SELECT   id\n"
                + "                                 FROM   atm_machine\n"
                + "                                WHERE   atm_group IN\n"
                + "                                              (SELECT   id\n"
                + "                                                 FROM   atm_group\n"
                + "                                                WHERE   parent_id IN\n"
                + "                                                              (SELECT   id\n"
                + "                                                                 FROM   atm_group\n"
                + "                                                                WHERE   (parent_id =\n"
                + "                                                                            $P{AtmGroupInt}\n"
                + "                                                                         OR id =\n"
                + "                                                                              $P{AtmGroupInt}))\n"
                + "                                                        OR 0 = $P{AtmGroupInt}))\n"
                + "            UNION\n"
                + "            SELECT /*+ parallel index (matched_data MATCHED_DATA_GRAPH_INDEX2) */\n"
                + "                  DISTINCT\n"
                + "                     atm_id,\n"
                + "                     SUM (amount) OVER (PARTITION BY atm_id) transaction,\n"
                + "                     atm_application_id,\n"
                + "                     currency\n"
                + "              FROM   matched_data\n"
                + "             WHERE   transaction_date BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                                       'dd.MM.yyyy hh24:mi:ss')\n"
                + "                                          AND  TO_DATE (\n"
                + "                                                  '$P{DatTo}',\n"
                + "                                                  'dd.MM.yyyy hh24:mi:ss'\n"
                + "                                               )\n"
                + "                     AND record_type = 2\n"
                + "                     AND matching_type = 1\n"
                + "                     AND EXISTS\n"
                + "                           (SELECT   1\n"
                + "                              FROM   TRANSACTION_TYPE_MASTER\n"
                + "                             WHERE   UPPER (name) LIKE UPPER ('%with%')\n"
                + "                                     AND TRANSACTION_TYPE_MASTER.id =\n"
                + "                                           matched_data.TRANSACTION_TYPE_MASTER)\n"
                + "                     AND ATM_ID IN\n"
                + "                              (SELECT   id\n"
                + "                                 FROM   atm_machine\n"
                + "                                WHERE   atm_group IN\n"
                + "                                              (SELECT   id\n"
                + "                                                 FROM   atm_group\n"
                + "                                                WHERE   parent_id IN\n"
                + "                                                              (SELECT   id\n"
                + "                                                                 FROM   atm_group\n"
                + "                                                                WHERE   (parent_id =\n"
                + "                                                                            $P{AtmGroupInt}\n"
                + "                                                                         OR id =\n"
                + "                                                                              $P{AtmGroupInt}))\n"
                + "                                                        OR 0 = $P{AtmGroupInt}))\n"
                + "            ORDER BY   atm_id) fulltable\n"
                + "   WHERE   atm_valid (atm_id) = 1\n"
                + "GROUP BY   atm_application_id, atm_id, currency";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DatTo}", dateTo);
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        ResultSet rs = executeQueryReport(selectStatment);
        super.postSelect();
        return rs;
    }
}
