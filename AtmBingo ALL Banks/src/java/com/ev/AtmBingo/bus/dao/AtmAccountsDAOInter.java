/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.AtmAccountsDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface AtmAccountsDAOInter {
    List<String> findAllBranchs() throws Throwable;

    Boolean findDuplicate(int atmid, int accname, String name, int count) throws Throwable;

    List<AtmAccountsDTOInter> findrecord(int atmid, String accid, String accname,int group,String branch) throws Throwable;

    String delete(Object... obj) throws Throwable;

    List<AtmAccountsDTOInter> findAll(String user) throws Throwable;

    String insert(Object... obj) throws Throwable;

    String update(Object... obj) throws Throwable;

}
