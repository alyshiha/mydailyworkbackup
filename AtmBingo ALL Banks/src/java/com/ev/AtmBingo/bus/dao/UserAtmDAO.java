/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UserAtmDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class UserAtmDAO extends BaseDAO implements UserAtmDAOInter {

    protected UserAtmDAO() {
        super();
        super.setTableName("user_atm");
    }

public void DeleteByUserATMID(String user,Integer atmid)throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String deleteStat = "delete from $table where user_id = $userid and atm_id = $atmid";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$userid", "" + user);
        deleteStat = deleteStat.replace("$atmid", "" + atmid);
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
       // return;
}
public boolean  insertAtms(String[] entities,Integer UserID) throws SQLException {
    Connection connection = null;
    PreparedStatement statement = null;
    try {
        
     connection  = CoonectionHandler.getInstance().getConnection();
        statement = connection.prepareStatement("insert into user_atm (atm_id,user_id) Select id,? From atm_machine Where  trim(application_id) = ?");
        
        for (int i = 0; i < entities.length; i++) {
            String entity = entities[i].toString();
            String[] ATMID = entity.split(" --- ");
                
            statement.setInt(1, UserID);
            statement.setString(2,ATMID[2].toString().trim());

            statement.addBatch();
            if ((i + 1) % 1000 == 0) {
                statement.executeBatch(); // Execute every 1000 items.
            }

        }

        statement.executeBatch();      
    }
    finally {
        if (statement != null) try { statement.close(); } catch (SQLException logOrIgnore) {}
        if (connection != null) try { CoonectionHandler.getInstance().returnConnection(connection); } catch (SQLException logOrIgnore) {} catch (ClassNotFoundException ex) {
            Logger.getLogger(UserAtmDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(UserAtmDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
}
public boolean  DeleteAtms(String[] entities,Integer UserID) throws SQLException {
    Connection connection = null;
    PreparedStatement statement = null;
    try {
      
     connection  = CoonectionHandler.getInstance().getConnection();

        statement = connection.prepareStatement("delete from user_atm where user_id = ? and atm_id in (Select id From atm_machine Where  trim(application_id) = ?)");

        for (int i = 0; i < entities.length; i++) {
            String entity = entities[i];
            String[] ATMID = entity.split(" --- ");

            statement.setInt(1, UserID);
            statement.setString(2,ATMID[2].toString().trim());
            statement.addBatch();
            if ((i + 1) % 1000 == 0) {
                statement.executeBatch(); // Execute every 1000 items.
            }

        }

        statement.executeBatch();
    }
    finally {
        if (statement != null) try { statement.close(); } catch (SQLException logOrIgnore) {}
        if (connection != null) try { CoonectionHandler.getInstance().returnConnection(connection); } catch (SQLException logOrIgnore) {} catch (ClassNotFoundException ex) {
            Logger.getLogger(UserAtmDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(UserAtmDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
}

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        UserAtmDTOInter uDTO = (UserAtmDTOInter) obj[0];
        String insertStat = "insert into $table values ($userid, $atmid)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$userid", "" + uDTO.getUserId());
        insertStat = insertStat.replace("$atmid", "" + uDTO.getAtmId());
        super.executeUpdate(insertStat);
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);

        AtmMachineDTOInter amDTO = (AtmMachineDTOInter) amDAO.find(uDTO.getAtmId());
        UsersDTOInter uuDTO = (UsersDTOInter) uDAO.find(uDTO.getUserId());
        String action = "Assign ATM ID" + amDTO.getApplicationId() + " to " + uuDTO.getUserName();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
//        super.preUpdate();
//        // databse insert statement usinf conn pool getConn().createStatement();
//        UserAtmDTOInter uDTO = (UserAtmDTOInter) obj[0];
//        String updateStat = "update $table set name = '$name' where id = $id";
//        updateStat = updateStat.replace("$table", "" + super.getTableName());
//        updateStat = updateStat.replace("$id", "" + uDTO.getId());
//        updateStat = updateStat.replace("$name", "" + uDTO.getName());
//        super.executeUpdate(updateStat);
//        super.postUpdate();
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        UserAtmDTOInter uDTO = (UserAtmDTOInter) obj[0];
        String deleteStat = "delete from $table where user_id = $userid and atm_id = $atmid";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$userid", "" + uDTO.getUserId());
        deleteStat = deleteStat.replace("$atmid", "" + uDTO.getAtmId());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object deleteByUserId(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        int uDTO = (Integer) obj[0];
        int atmGInt = (Integer) obj[1];
        String deleteStat = "";
        if(atmGInt != 0){
         deleteStat = "delete from $table u where u.user_id = $userid and u.atm_id in (select a.id from atm_machine a where a.id = u.atm_id and a.atm_group = $group)";}
        else{deleteStat = "delete from $table u where u.user_id = $userid";}
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$userid", "" + uDTO);
        deleteStat = deleteStat.replace("$group", "" + atmGInt);
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object findByUserId(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where user_id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        List<UserAtmDTOInter> uDTOL = new ArrayList<UserAtmDTOInter>();
        while (rs.next()) {
            UserAtmDTOInter uDTO = DTOFactory.createUserAtmDTO();
            uDTO.setAtmId(rs.getInt("atm_id"));
            uDTO.setUserId(rs.getInt("user_id"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByAtmId(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where atm_id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        List<UserAtmDTOInter> uDTOL = new ArrayList<UserAtmDTOInter>();
        while (rs.next()) {
            UserAtmDTOInter uDTO = DTOFactory.createUserAtmDTO();
            uDTO.setAtmId(rs.getInt("atm_id"));
            uDTO.setUserId(rs.getInt("user_id"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
