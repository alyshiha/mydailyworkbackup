/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Administrator
 */
public interface NetworkDetailsDAOInter {

    Object delete(Object... obj) throws Throwable;

    Object find(int id, int curr, int trans, String bin) throws Throwable;

    Object findAll(Object... obj) throws Throwable;

    Object findByIdMaster(int id) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
