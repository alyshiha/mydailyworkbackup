/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UserActivityDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class UserActivityDAO extends BaseDAO implements UserActivityDAOInter {
public int d =0;
    protected UserActivityDAO() {
        super();
        try {
            super.setTableName("disputes");
            d = getNumberOfDays();
        } catch (Throwable ex) {
            Logger.getLogger(UserActivityDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Object findForGraph() throws Throwable {
        super.preSelect();
        List<UserActivityDTOInter> countDis = new ArrayList<UserActivityDTOInter>();
        List<UserActivityDTOInter> countSett = new ArrayList<UserActivityDTOInter>();
        List<UsersDTOInter> UserData = new ArrayList<UsersDTOInter>();
        countDis = getCountDis();
        countSett = getCountSett();
        UserData = getUserData();
        List<UserActivityDTOInter> uDTO = new ArrayList<UserActivityDTOInter>();
        Integer temp =0;


        for(UsersDTOInter UDI:UserData)
        {
            UserActivityDTOInter UADTO = DTOFactory.createUserActivityDTO();
            for(UserActivityDTOInter UADI:countSett)
            {
                if(UADI.getUser()==UDI.getUserId())
                {
                    UADTO.setCountSett(UADI.getCountSett());
                    
                }
                 if(temp<UADI.getCountSett())
                    {
                     UADI.setMaxcountSett(UADI.getCountSett());

                    }
            }
            for(UserActivityDTOInter UADID:countDis)
            {
                if(UADID.getUser()==UDI.getUserId())
                {
                    UADTO.setCountDis(UADID.getCountDis());
                    
                }
                 if(temp<UADID.getCountDis())
                    {
                     UADID.setMaxcountDis(UADID.getCountDis());
                    }
            }
            UADTO.setuDTO(UDI);
            uDTO.add(UADTO);
        }

        super.postSelect();
        return uDTO;
    }


    private int getNumberOfDays() throws Throwable {
        super.preCollable();

        CallableStatement stat = super.executeCallableStatment("{call ? := get_number_parameter('DISPUTE_GRAPH_DAYS')}");
        int done = 0;
        stat.registerOutParameter(1, Types.NUMERIC);
        stat.execute();
        done = stat.getInt(1);
        super.postCollable(stat);
        return done;
    }
   private List<UsersDTOInter> getUserData() throws Throwable {
        super.preSelect();

        List<UsersDTOInter> uaDTOL = new ArrayList<UsersDTOInter>();
        
        String selectStat = "select k.user_id user_id,K.user_name user_name from users K where K.user_id in( select /*+INDEX (d user_activity_index)*/ a.user_id user_id from disputes d, user_atm a where d.transaction_date > sysdate-$d and d.settled_flag = 2 and d.atm_id = a.atm_id group by a.user_id)";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
       // selectStat = selectStat.replace("$userId", "" + userDTO.getUserId());
        selectStat = selectStat.replace("$d", "" + d);
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            UsersDTOInter mt = DTOFactory.createUsersDTO();
            mt.setUserName(rs.getString("user_name"));
            mt.setUserId(rs.getInt("user_id"));
            uaDTOL.add(mt);
        }
        super.postSelect(rs);
        return uaDTOL;
    }

    private List<UserActivityDTOInter> getCountDis() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        

        // databse insert statement usinf conn pool getConn().createStatement();
     
       
        List<UserActivityDTOInter> uaDTOL = new ArrayList<UserActivityDTOInter>();
        String selectStat = "select /*+INDEX (d user_activity_index)*/ count(*) countdis  , a.user_id userid from $table d, user_atm a where d.transaction_date > sysdate-$d and d.settled_flag = 2 and d.atm_id = a.atm_id group by a.user_id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
       // selectStat = selectStat.replace("$userId", "" + userDTO.getUserId());
        selectStat = selectStat.replace("$d", "" + d);
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            UserActivityDTOInter mt = DTOFactory.createUserActivityDTO();
             
            mt.setUser(rs.getInt(2));
           
            mt.setCountDis(rs.getInt(1));
            uaDTOL.add(mt);
        }
        super.postSelect(rs);
        return uaDTOL;
    }

    private List<UserActivityDTOInter> getCountSett() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
       
        
        List<UserActivityDTOInter> uaDTOL = new ArrayList<UserActivityDTOInter>();
        String selectStat = "select /*+INDEX (d user_activity_index)*/ count(*) countdis  , a.user_id userid from disputes d, user_atm a where d.transaction_date > sysdate-$d and d.settled_flag = 1 and d.settled_user = a.user_id and d.atm_id = a.atm_id group by a.user_id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
       // selectStat = selectStat.replace("$userId", "" + userDTO.getUserId());
        selectStat = selectStat.replace("$d", "" + d);
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            UserActivityDTOInter mt = DTOFactory.createUserActivityDTO();
            
            mt.setUser(rs.getInt(2));
           
            mt.setCountSett(rs.getInt(1));
            uaDTOL.add(mt);
        }
        super.postSelect(rs);
        return uaDTOL;
    }
}
