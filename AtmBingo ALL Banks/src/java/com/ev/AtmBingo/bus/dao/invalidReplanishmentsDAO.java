
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.invalidReplanishmentsDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class invalidReplanishmentsDAO extends BaseDAO implements invalidReplanishmentsDAOInter {

    @Override
    public Object findinvalidReplanishments(String dateFrom, String dateTo) throws Throwable {
        super.preSelect();
        String selectStatment = "select DBMS_LOB.SUBSTR(text, 4000, 1) text, file_name, atm_id, loading_date, reason from invalid_replanishments\n"
                + "where loading_date >= to_date ('$P{DateFrom}', 'dd-mm-yyyy hh24:mi:ss')\n"
                + "and loading_date <= to_date ('$P{DateTo}', 'dd-mm-yyyy hh24:mi:ss')";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DateTo}", dateTo);
        List<invalidReplanishmentsDTOInter> Records = new ArrayList<invalidReplanishmentsDTOInter>();
        ResultSet rs = executeQueryReport(selectStatment);
        while (rs.next()) {
            invalidReplanishmentsDTOInter record = DTOFactory.createinvalidReplanishmentsDTO();
            record.setLoadingDate(rs.getTimestamp("loading_date"));
            record.setReason(rs.getString("reason"));
            record.setAtmID(rs.getString("atm_id"));
            record.setFileName(rs.getString("file_name"));
            record.setTransactionData(rs.getString("text"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }
}
