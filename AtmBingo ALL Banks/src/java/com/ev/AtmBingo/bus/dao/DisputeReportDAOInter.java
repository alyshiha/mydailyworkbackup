/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Administrator
 */
public interface DisputeReportDAOInter {

    Object findByUserId(int userId, String fields, Integer mtInt)throws Throwable;

    Object delete(int userId) throws Throwable ;

    Object insert(Object... obj) throws Throwable;

    Integer PrintLines(String mdDTO) throws Exception;

}
