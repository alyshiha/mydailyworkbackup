package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.util.Date;

public interface UsersDAOInter {

    String ValidateLog(String userName,String password,String version,Integer NumOfUsers,Date EndDate)throws Throwable;

    Object findUnblockUsers(Object... obj)throws Throwable;
Object findUnlockUsers(Object... obj) throws Throwable ;
    Object findOffline()throws Throwable;
Boolean getaccess(UsersDTOInter user) throws Throwable ;
    Object findOnline()throws Throwable;

    Object findHasNotProfile()throws Throwable;

    Object findHasProfile(int profileId, int rest)throws Throwable;

    Object findComboBox() throws Throwable;

    Object checkUserPass(String userName,String pass)throws Throwable;

    Object findByLogonName(String logonName) throws Throwable;

    public Object insert(Object... obj)  throws Throwable ;

    Object updateStatus(Object... obj)  throws Throwable;

    Object updateRest(Object... obj)  throws Throwable;
public Object FirstTimeLogin(String UserName) throws Throwable;
    Object updatePass(Object... obj)  throws Throwable ;
    Object updatePassBranch(Object... obj) throws Throwable;
    Boolean Checkpass(UsersDTOInter uDTO,String confPass, String newPass)  throws Throwable ;
    Object updatePass2(Object... obj)  throws Throwable ;
Object findBranch(Object... obj) throws Throwable;
     Object encrUserPass(String pass)throws Throwable;
    
    public Object update(Object... obj) throws Throwable ;
    public Object Lockuser(String userName) throws Throwable ;
    int userlic() throws Throwable;
    public Object delete(Object... obj) throws Throwable ;
    
    public Object find(Object... obj) throws Throwable ;
    
    public Object search(Object... obj) throws Throwable ;

    Object findALl(int rest)throws Throwable;


    Object findDisputeGraphUser() throws Throwable;

}
