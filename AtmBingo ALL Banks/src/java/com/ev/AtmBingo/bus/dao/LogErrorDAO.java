/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ExceptionLoadingErrorsDTOInter;
import com.ev.AtmBingo.bus.dto.LoadingInsertErrorsDTOInter;
import com.ev.AtmBingo.bus.dto.RepStat13DTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class LogErrorDAO extends BaseDAO implements LogErrorDAOInter {

    @Override
    public Object findLoadingInsertErrors(String dateFrom, String dateTo, int fileid) throws Throwable {
        super.preSelect();
        String selectStatment = "SELECT FILE_ID, ERROR_DATE, ERROR_CODE, ERROR_TEXT, FILE_NAME, to_char(TRANSACTION_DATA) TRANSACTION_DATA"
                + " FROM LOADING_INSERT_ERRORS"
                + " WHERE (FILE_ID = $P{FILE_ID} OR $P{FILE_ID} = 0)"
                + " AND ERROR_DATE >= TO_DATE ('$P{DateFrom}', 'dd-mm-yyyy hh24:mi:ss')"
                + " AND ERROR_DATE <= TO_DATE ('$P{DateTo}', 'dd-mm-yyyy hh24:mi:ss')";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DateTo}", dateTo);
        selectStatment = selectStatment.replace("$P{FILE_ID}", "" + fileid);
        List<LoadingInsertErrorsDTOInter> Records = new ArrayList<LoadingInsertErrorsDTOInter>();
        ResultSet rs = executeQueryReport(selectStatment);
        while (rs.next()) {
            LoadingInsertErrorsDTOInter record = DTOFactory.createLoadingInsertErrorsDTO();
            record.setFileID(rs.getInt("FILE_ID"));
            record.setErrorDate(rs.getTimestamp("ERROR_DATE"));
            record.setErrorCode(rs.getInt("ERROR_CODE"));
            record.setErrorText(rs.getString("ERROR_TEXT"));
            record.setFileName(rs.getString("FILE_NAME"));
            record.setTransactionData(rs.getString("TRANSACTION_DATA"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findExceptionLoadingErrors(String dateFrom, String dateTo) throws Throwable {
        super.preSelect();
        String selectStatment = "SELECT FUNCTION_NAME, ERROR_CODE, ERROR_NAME, ERROR_DATE, ERROR_CONDITION"
                + " FROM EXCEPTION_LOADING_ERRORS"
                + " WHERE ERROR_DATE >= TO_DATE ('$P{DateFrom}', 'dd-mm-yyyy hh24:mi:ss')"
                + " AND ERROR_DATE <= TO_DATE ('$P{DateTo}', 'dd-mm-yyyy hh24:mi:ss')";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DateTo}", dateTo);
        List<ExceptionLoadingErrorsDTOInter> Records = new ArrayList<ExceptionLoadingErrorsDTOInter>();
        ResultSet rs = executeQueryReport(selectStatment);
        while (rs.next()) {
            ExceptionLoadingErrorsDTOInter record = DTOFactory.createExceptionLoadingErrorsDTO();
            record.setFunctionName(rs.getString("FUNCTION_NAME"));
            record.setErrorCode(rs.getInt("ERROR_CODE"));
            record.setErrorName(rs.getString("ERROR_NAME"));
            record.setErrorDate(rs.getTimestamp("ERROR_DATE"));
            record.setErrorCondition(rs.getString("ERROR_CONDITION"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }
}
