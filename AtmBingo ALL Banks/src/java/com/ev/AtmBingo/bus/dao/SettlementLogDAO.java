/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.SettlementLogDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author KhAiReE
 */
public class SettlementLogDAO extends BaseDAO implements SettlementLogDAOInter {

    protected SettlementLogDAO() {
        super();
        super.setTableName("Settlement_Log");

    }

    public Object findAll(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select " + super.getTableName() + ".*," + super.getcardcolumn() + " card_no_Decrypt from " + super.getTableName() + " order by settlement_date desc ";
        ResultSet result = executeQuery(selectStat);
        List<SettlementLogDTOInter> slL = new ArrayList<SettlementLogDTOInter>();
        while (result.next()) {
            SettlementLogDTOInter sl = DTOFactory.createSettlementLogDTO();
            sl.setAmount(result.getInt("amount"));
            sl.setAtmApplicationId(result.getString("atm_application_id"));
            sl.setCardNo(result.getString("card_no_Decrypt"));
            sl.setCurrency(result.getString("currency"));
            sl.setMatchingType(result.getInt("matching_type"));
            sl.setRecordType(result.getInt("record_type"));
            sl.setResponseCode(result.getString("response_code"));
            sl.setSettledFlag(result.getInt("settled_flag"));
            sl.setSettledUser(result.getInt("settled_user"));
            sl.setSettlementDate(result.getDate("settlement_date"));
            sl.setTransactionDate(result.getDate("transaction_date"));
            sl.setTransactionSequence(result.getString("transaction_sequence"));
            slL.add(sl);
        }
        super.postSelect(result);
        return slL;
    }

    public Object CustomSearch(Object... obj) throws Throwable {
        super.preSelect();
        String whereCluase = (String) obj[0];
        String selectStat = "select  " + super.getTableName() + ".*,decode(record_type,1,'Journal',2,'Switch',3,'Host','Other') recordtext,(select s.user_name from users s where s.user_id=settled_user) usertext,(select t.name from matching_type t where t.id=matching_type)mttext , " + super.getcardcolumn() + " card_no_Decrypt from " + super.getTableName() + " where 1=1 $whereCluase order by settlement_date desc";
        selectStat = selectStat.replace("$whereCluase", "" + whereCluase);
        List<SettlementLogDTOInter> slL = new ArrayList<SettlementLogDTOInter>();
        ResultSet result = executeQuery(selectStat);
        while (result.next()) {
            SettlementLogDTOInter sl = DTOFactory.createSettlementLogDTO();
            sl.setAmount(result.getInt("amount"));
            sl.setSettledUsertext(result.getString("usertext"));
            sl.setMatchingTypetext(result.getString("mttext"));
            sl.setRecordTypetext(result.getString("recordtext"));
            sl.setAtmApplicationId(result.getString("atm_application_id"));
            sl.setCardNo(result.getString("card_no_Decrypt"));
            sl.setCurrency(result.getString("currency"));
            sl.setMatchingType(result.getInt("matching_type"));
            sl.setRecordType(result.getInt("record_type"));
            sl.setResponseCode(result.getString("response_code"));
            sl.setSettledFlag(result.getInt("settled_flag"));
            sl.setSettledUser(result.getInt("settled_user"));
            sl.setSettlementDate(result.getTimestamp("settlement_date"));
            sl.setTransactionDate(result.getTimestamp("transaction_date"));
            sl.setTransactionSequence(result.getString("transaction_sequence"));
            slL.add(sl);
        }
        super.postSelect(result);
        return slL;
    }
}
