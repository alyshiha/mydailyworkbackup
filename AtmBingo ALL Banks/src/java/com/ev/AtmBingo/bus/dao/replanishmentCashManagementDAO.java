/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.replanishmentCashManagementDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class replanishmentCashManagementDAO extends BaseDAO implements replanishmentCashManagementDAOInter {

    @Override
    public List<replanishmentCashManagementDTOInter> findRecord(String ATMID, Integer ATMGroup, String DateFrom, String DateTo, int user, Boolean release) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT   REPLANISHMENT_ID,\n"
                + "\n"
                + "        FROM_DATE,\n"
                + "\n"
                + "        TO_DATE,\n"
                + "\n"
                + "        ATM_ID,\n"
                + "\n"
                + "        ADDED_CASH,\n"
                + "\n"
                + "        REMAINING_CASH,\n"
                + "\n"
                + "        DISPENSED_CASH,\n"
                + "\n"
                + "        WITHDRWAL_JOURNAL_ONUS,\n"
                + "\n"
                + "        WITHDRWAL_JOURNAL_OFFUS,\n"
                + "\n"
                + "        WITHDRWAL_SWITCH_ONUS,\n"
                + "\n"
                + "        WITHDRWAL_SWITCH_OFFUS,\n"
                + "\n"
                + "        (DISPENSED_CASH - (WITHDRWAL_JOURNAL_ONUS + WITHDRWAL_JOURNAL_OFFUS))\n"
                + "\n"
                + "           DIFF_DISPENSED_JOURNAL,\n"
                + "\n"
                + "        ( (WITHDRWAL_JOURNAL_ONUS + WITHDRWAL_JOURNAL_OFFUS)\n"
                + "\n"
                + "         - (WITHDRWAL_SWITCH_ONUS + WITHDRWAL_SWITCH_OFFUS))\n"
                + "\n"
                + "           DIFF_JOURNAL_SWITCH\n"
                + "\n"
                + "FROM   REPLANISHMENT_CASH_MANGEMENT\n"
                + "\n"
                + "WHERE  ( (FROM_DATE >= TO_DATE ('$DateFrom', 'DD-MM-YYYY HH24:MI:SS')\n"
                + "\n"
                + "          )\n"
                + "\n"
                + "        AND (FROM_DATE <= TO_DATE ('$DateTo', 'DD-MM-YYYY HH24:MI:SS')\n"
                + "\n"
                + "             )\n"
                + "             OR EXISTS\n"
                + "                 (SELECT   1\n"
                + "                    FROM   ATM_JOURNAL\n"
                + "                   WHERE    EXPORT_FLAG in (1,2$release)  AND JOURNAL_DATE =REPLANISHMENT_CASH_MANGEMENT.FROM_DATE"
                + " AND ATM_JOURNAL.atm_id =\n"
                + "                                 CASH_MANAGEMENT.GET_APPLICATION_ID (\n"
                + "                                    REPLANISHMENT_CASH_MANGEMENT.atm_id\n"
                + "                                 )))\n"
                + "\n"
                + "        AND (ATM_ID = '$ATMID' OR '$ATMID' = '0')\n"
                + "\n"
                + "        AND (ATM_ID IN\n"
                + "\n"
                + "                   (SELECT   id\n"
                + "\n"
                + "                      FROM   atm_machine\n"
                + "\n"
                + "                     WHERE   atm_group IN\n"
                + "\n"
                + "                                   (SELECT   id\n"
                + "\n"
                + "                                      FROM   atm_group\n"
                + "\n"
                + "                                     WHERE   parent_id IN\n"
                + "\n"
                + "                                                   (SELECT   id\n"
                + "\n"
                + "                                                      FROM   atm_group\n"
                + "\n"
                + "                                                     WHERE   (parent_id =\n"
                + "\n"
                + "                                                                 $atmgroup\n"
                + "\n"
                + "                                                              OR id =\n"
                + "\n"
                + "                                                                   $atmgroup))\n"
                + "\n"
                + "                                             OR id = $atmgroup))\n"
                + "\n"
                + "OR $atmgroup = 0) and ATM_ID in (select atm_id from user_atm where user_id = $user) and atm_valid(ATM_ID) = 1 order by ATM_ID,FROM_DATE asc";
        selectStat = selectStat.replace("$atmgroup", "" + ATMGroup);
        selectStat = selectStat.replace("$ATMID", "" + ATMID);
        selectStat = selectStat.replace("$DateFrom", DateFrom);
        selectStat = selectStat.replace("$DateTo", DateTo);
        selectStat = selectStat.replace("$user", "" + user);
        if (release) {
            selectStat = selectStat.replace("$release", ",8");
        } else {
            selectStat = selectStat.replace("$release", "");
        }
        ResultSet rs = executeQuery(selectStat);
        List<replanishmentCashManagementDTOInter> records = new ArrayList<replanishmentCashManagementDTOInter>();
        while (rs.next()) {
            replanishmentCashManagementDTOInter record = DTOFactory.createreplanishmentCashManagementDTO();
            record.setReplanishmentid(rs.getInt("REPLANISHMENT_ID"));
            record.setFromdate(rs.getTimestamp("FROM_DATE"));
            record.setTodate(rs.getTimestamp("TO_DATE"));
            record.setAtmid(rs.getInt("ATM_ID"));
            record.setAddedcash(rs.getInt("ADDED_CASH"));
            record.setRemainingcash(rs.getInt("REMAINING_CASH"));
            record.setDispensedcash(rs.getInt("DISPENSED_CASH"));
            record.setWithdrwaljournalonus(rs.getInt("WITHDRWAL_JOURNAL_ONUS"));
            record.setWithdrwaljournaloffus(rs.getInt("WITHDRWAL_JOURNAL_OFFUS"));
            record.setWithdrwalswitchonus(rs.getInt("WITHDRWAL_SWITCH_ONUS"));
            record.setWithdrwalswitchoffus(rs.getInt("WITHDRWAL_SWITCH_OFFUS"));
            record.setDiffdispensedjornal(rs.getInt("DIFF_DISPENSED_JOURNAL"));
            record.setDiffjournalswitch(rs.getInt("DIFF_JOURNAL_SWITCH"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }
}
