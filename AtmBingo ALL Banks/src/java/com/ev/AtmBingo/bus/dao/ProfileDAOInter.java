/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.ProfileDTOInter;

/**
 *
 * @author Administrator
 */
public interface ProfileDAOInter {

    Object find(int id) throws Throwable;
 void addlog(String log) throws Throwable;
    Object delete(Object... obj) throws Throwable;
 boolean  ProfileHasUser(ProfileDTOInter pDTO)throws Throwable;
    Object findAlluserprofile() throws Throwable;

    Object findAll(Object... obj) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;
}
