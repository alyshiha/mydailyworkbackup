/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.atmremainingDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmRemainingRepDAO extends BaseDAO implements AtmRemainingRepDAOInter {

    protected AtmRemainingRepDAO() {
        super();
    }

    public Object findremainingcashexcelReport(String dateTo, int atmGroupInt,  int atmID) throws Throwable {
        super.preSelect();
        String selectStatment = "SELECT   r.REMAINING_ID,\n"
                + "         r.ATM_ID,\n"
                + "         (select am.application_id from atm_machine am where am.id = r.ATM_ID) ATM_NAME,\n"
                + "         r.LAST_REP_DATE,\n"
                + "         r.REMAINING_VALUE REMAINING,\n"
                + "         r.DATE_TO\n"
                + "  FROM   REMAINING_CASH_ATM r\n"
                + " WHERE   r.DATE_TO <= TO_DATE ($P{DateTo}, 'dd.MM.yyyy hh24:mi:ss')\n"
                + "         AND (r.ATM_ID = $P{atmid} OR $P{atmid} = 0)\n"
                + "         AND (r.ATM_ID IN\n"
                + "                    (SELECT   m.id\n"
                + "                       FROM   atm_machine m\n"
                + "                      WHERE   m.atm_group IN\n"
                + "                                    (SELECT   g.id\n"
                + "                                       FROM   atm_group g\n"
                + "                                      WHERE   g.parent_id IN\n"
                + "                                                    (SELECT   ag.id\n"
                + "                                                       FROM   atm_group ag\n"
                + "                                                      WHERE   (ag.parent_id =\n"
                + "                                                                  $P{AtmGroupInt}\n"
                + "                                                               OR ag.id =\n"
                + "                                                                    $P{AtmGroupInt}))\n"
                + "                                              OR g.id = $P{AtmGroupInt}))\n"
                + "              OR $P{AtmGroupInt} = 0)";
        selectStatment = selectStatment.replace("$P{DateTo}", "'" + dateTo + "'");
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        selectStatment = selectStatment.replace("$P{atmid}", "" + atmID);
        ResultSet rs = executeQueryReport(selectStatment);
        List<atmremainingDTOInter> Records = new ArrayList<atmremainingDTOInter>();
        while (rs.next()) {
            atmremainingDTOInter record = DTOFactory.createatmremainingDTO();
            record.setAtmapplicationid(rs.getString("ATM_NAME"));
            record.setFromdate(rs.getTimestamp("LAST_REP_DATE"));
            record.setTodate(rs.getTimestamp("DATE_TO"));
            record.setRemaining(rs.getBigDecimal("REMAINING"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findReport( String dateTo, int atmGroupInt, int atmID) throws Throwable {
        super.preSelect();
        String selectStatment = "SELECT   r.REMAINING_ID,\n"
                + "         r.ATM_ID,\n"
                + "         (select am.application_id from atm_machine am where am.id = r.ATM_ID) ATM_NAME,\n"
                + "         r.LAST_REP_DATE,\n"
                + "         r.REMAINING_VALUE REMAINING,\n"
                + "         r.DATE_TO\n"
                + "  FROM   REMAINING_CASH_ATM r\n"
                + " WHERE   r.DATE_TO <= TO_DATE ($P{DateTo}, 'dd.MM.yyyy hh24:mi:ss')\n"
                + "         AND (r.ATM_ID = $P{atmid} OR $P{atmid} = 0)\n"
                + "         AND (r.ATM_ID IN\n"
                + "                    (SELECT   m.id\n"
                + "                       FROM   atm_machine m\n"
                + "                      WHERE   m.atm_group IN\n"
                + "                                    (SELECT   g.id\n"
                + "                                       FROM   atm_group g\n"
                + "                                      WHERE   g.parent_id IN\n"
                + "                                                    (SELECT   ag.id\n"
                + "                                                       FROM   atm_group ag\n"
                + "                                                      WHERE   (ag.parent_id =\n"
                + "                                                                  $P{AtmGroupInt}\n"
                + "                                                               OR ag.id =\n"
                + "                                                                    $P{AtmGroupInt}))\n"
                + "                                              OR g.id = $P{AtmGroupInt}))\n"
                + "              OR $P{AtmGroupInt} = 0)";
        selectStatment = selectStatment.replace("$P{DateTo}", "'" + dateTo + "'");
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        selectStatment = selectStatment.replace("$P{atmid}", "" + atmID);
        ResultSet rs = executeQueryReport(selectStatment);
        super.postSelect();
        return rs;
    }
}
