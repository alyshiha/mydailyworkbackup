/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.NotePresentedDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface NotePresentedDAOInter {

    Object findNotePresentedReport(String dateFrom, String dateTo, Integer atmGroup, int userID, String atmAppID,String operator) throws Throwable;

    List<NotePresentedDTOInter> findNotePresentedexcelReport(String dateFrom, String dateTo, Integer atmGroup, int userID, String atmAppID,String operator) throws Throwable;
    
}
