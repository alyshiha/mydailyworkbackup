/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.DepositTransactionDTOInter;
import java.util.List;

/**
 *
 * @author Aly.Shiha
 */
public interface DepositTransactionDAOInter {

    List<DepositTransactionDTOInter> findAll(String from, String To, String ATMID,String CardNumber) throws Throwable;
    
}
