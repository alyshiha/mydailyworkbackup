/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface NotificationDAOInter extends Serializable{

    Object findAllError(Object... obj) throws Throwable;

    Object findAllNotification(Object... obj) throws Throwable;

}
