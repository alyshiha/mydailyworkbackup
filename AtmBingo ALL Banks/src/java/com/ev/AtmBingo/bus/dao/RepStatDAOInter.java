/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author AlyShiha
 */
public interface RepStatDAOInter {

    Object findReport1(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable;

    Object findReport2(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable;

    Object findReport3(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable;
    
    Object findReport4(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable ;
    Object findReport5(String dateFrom, String dateTo, int atmGroup, int atmid) throws Throwable ;
    Object findReport6(String dateFrom, String dateTo, int atmGroup, String atmid, String network, String status) throws Throwable;
     Object findReport7(String dateFrom, String dateTo, int atmGroup, String atmid, String network, String status) throws Throwable;
}
