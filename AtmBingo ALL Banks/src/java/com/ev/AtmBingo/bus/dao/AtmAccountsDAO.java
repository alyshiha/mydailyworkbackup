/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.AtmAccountsDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class AtmAccountsDAO extends BaseDAO implements AtmAccountsDAOInter {

    @Override
    public Boolean findDuplicate(int atmid, int accname, String name, int count) throws Throwable {
        super.preSelect();
        String selectStat = "select Count(t.id) resultcount from ATM_ACCOUNTS t where ( t.atm_id = $atmid and t.account_name_id = $atmaccountname ) or ( t.atm_id = $atmid and Lower(t.atm_account_id) = Lower('$name') ) or ( Lower(t.atm_account_id) = Lower('$name') )";
        selectStat = selectStat.replace("$atmid", "" + atmid);
        selectStat = selectStat.replace("$atmaccountname", "" + accname);
        selectStat = selectStat.replace("$name", "" + name);
        ResultSet rs = executeQuery(selectStat);
        Boolean result = Boolean.FALSE;
        while (rs.next()) {
            if (rs.getInt("resultcount") > count) {
                result = Boolean.TRUE;
            }
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select records From ATM Accounts with atm id = " + atmid + " account name: " + accname + " Name: " + name);
        return result;
    }

    @Override
    public List<AtmAccountsDTOInter> findrecord(int atmid, String accid, String accname, int group, String branch) throws Throwable {
        super.preSelect();
        String selectStat = "select c.id,(select gt.id from atm_group gt where gt.id in (\n"
                + "select d.atm_group from atm_machine d where d.id = c.atm_id ))groupid ,c.atm_id,(select g.name from atm_group g where g.id in (\n"
                + "select m.atm_group from atm_machine m where m.id = c.atm_id ))groupname, c.atm_describtion, c.atm_account_id,\n"
                + "(select n.symbol  from atm_account_name n where n.id = c.ACCOUNT_NAME_ID) ACCOUNTNAME , \n"
                + "concat('EG001',substr(c.atm_account_id,-4)) branch,\n"
                + "c.ACCOUNT_NAME_ID, c.atm_application_id  "
                + "from atm_accounts c where (c.atm_id = $atmid or $atmid = 0) "
                + "and c.atm_account_id like '%$atmaccountid%' and ('$branch' = 'All' Or concat('EG001',substr(c.atm_account_id,-4)) = '$branch') "
                + "and (c.ACCOUNT_NAME_ID = $atmaccountname or $atmaccountname = 0) AND (c.ATM_ID IN (SELECT   am.id FROM   atm_machine am WHERE   am.atm_group IN (SELECT   ag.id FROM   atm_group ag WHERE   ag.parent_id IN\n"
                + "(SELECT   gg.id FROM   atm_group gg WHERE   (gg.parent_id = $atmgroup OR gg.id = $atmgroup)) OR ag.id = $atmgroup)) OR $atmgroup = 0)"
                + "order by c.atm_application_id";
        selectStat = selectStat.replace("$atmid", "" + atmid);
        selectStat = selectStat.replace("$atmaccountid", accid);
        selectStat = selectStat.replace("$atmaccountname", accname);
        selectStat = selectStat.replace("$branch", branch);
        selectStat = selectStat.replace("$atmgroup", "" + group);
        ResultSet rs = executeQuery(selectStat);
        List<AtmAccountsDTOInter> records = new ArrayList<AtmAccountsDTOInter>();
        while (rs.next()) {
            AtmAccountsDTOInter record = DTOFactory.createAtmAccountsDTO();
            record.setId(rs.getInt("id"));
            record.setGroupid(rs.getInt("groupid"));
            record.setGroupvalue(rs.getString("groupname"));
            record.setBranch(rs.getString("branch"));
            record.setAccountname(rs.getString("ACCOUNTNAME"));
            record.setAtmapplicationid(rs.getString("atm_application_id"));
            record.setAccountid(rs.getString("atm_account_id"));
            record.setAccountnameid(rs.getInt("ACCOUNT_NAME_ID"));
            record.setAtmid(rs.getInt("atm_id"));
            record.setDescription(rs.getString("atm_describtion"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select records From ATM Accounts with atm id = " + atmid + " account name: " + accname + " branch: " + branch + " group: " + group);
        return records;
    }

    @Override
    public List<String> findAllBranchs() throws Throwable {
        super.preSelect();
        String selectStat = "select Distinct concat('EG001',substr(atm_account_id,-4)) BranchID from atm_accounts order by 1";
        ResultSet rs = executeQuery(selectStat);
        List<String> records = new ArrayList<String>();
        while (rs.next()) {
            records.add(rs.getString("BranchID"));
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select All Branches to search in ATM Accounts");
        return records;
    }

    @Override
    public List<AtmAccountsDTOInter> findAll(String user) throws Throwable {
        super.preSelect();
        String selectStat = "select c.id, (select gt.id from atm_group gt where gt.id in (\n"
                + "select d.atm_group from atm_machine d where d.id = c.atm_id ))groupid,c.atm_id, (select g.name from atm_group g where g.id in (\n"
                + "select m.atm_group from atm_machine m where m.id = c.atm_id ))groupname,c.atm_describtion, c.atm_account_id,\n"
                + "(select n.symbol  from atm_account_name n where n.id = c.ACCOUNT_NAME_ID) ACCOUNTNAME , \n"
                + "concat('EG001',substr(c.atm_account_id,-4)) branch,\n"
                + "c.ACCOUNT_NAME_ID, c.atm_application_id  from atm_accounts c where c.atm_id in (select p.atm_id from user_atm p where p.user_id = " + user + ") order by c.atm_application_id";
        ResultSet rs = executeQuery(selectStat);
        List<AtmAccountsDTOInter> records = new ArrayList<AtmAccountsDTOInter>();
        while (rs.next()) {
            AtmAccountsDTOInter record = DTOFactory.createAtmAccountsDTO();
            record.setGroupid(rs.getInt("groupid"));
            record.setGroupvalue(rs.getString("groupname"));
            record.setId(rs.getInt("id"));
            record.setBranch(rs.getString("branch"));
            record.setAccountname(rs.getString("ACCOUNTNAME"));
            record.setAtmapplicationid(rs.getString("atm_application_id"));
            record.setAccountid(rs.getString("atm_account_id"));
            record.setAccountnameid(rs.getInt("ACCOUNT_NAME_ID"));
            record.setAtmid(rs.getInt("atm_id"));
            record.setDescription(rs.getString("atm_describtion"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select All records From ATM Accounts");
        return records;
    }

    public String update(Object... obj) throws Throwable {
        AtmAccountsDTOInter record = (AtmAccountsDTOInter) obj[0];
        super.preCollable();
        String temp = "";
        CallableStatement stat = super.executeCallableStatment("{call ? := CASH_MANAGEMENT.update_atm_account(?,?,?,?,?,?,?)}");
        stat.registerOutParameter(1, Types.VARCHAR);
        stat.registerOutParameter(8, Types.VARCHAR);
        stat.setInt(2, record.getId());
        stat.setInt(3, record.getAtmid());
        stat.setString(4, record.getDescription());
        stat.setString(5, record.getAccountid());
        stat.setInt(6, record.getGroupid());
        stat.setInt(7, record.getAccountnameid());
        stat.executeUpdate();
        String Result = stat.getString(1);
        if ("FALSE".equals(Result)) {
            Result = stat.getString(8);
        } else {
            Result = "Record Has Been Updated Successfully";
        }
        super.postCollable(stat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("update record with id " + record.getId() + " in ATM Accounts");
        return Result;
    }

    public String insert(Object... obj) throws Throwable {
        AtmAccountsDTOInter record = (AtmAccountsDTOInter) obj[0];
        super.preCollable();
        String temp = "";
        record.setId(super.generateSequence("atm_accounts"));
        CallableStatement stat = super.executeCallableStatment("{call ? := CASH_MANAGEMENT.add_atm_account(?,?,?,?,?,?,?)}");
        stat.registerOutParameter(1, Types.VARCHAR);
        stat.registerOutParameter(8, Types.VARCHAR);
        stat.setInt(2, record.getId());
        stat.setInt(3, record.getAtmid());
        stat.setString(4, record.getDescription());
        stat.setString(5, record.getAccountid());
        stat.setInt(6, record.getGroupid());
        stat.setInt(7, record.getAccountnameid());

        stat.executeUpdate();
        String Result = stat.getString(1);
        if ("FALSE".equals(Result)) {
            Result = stat.getString(8);
        } else {
            Result = "Record Has Been inserted Successfully";
        }
        super.postCollable(stat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("insert record with id " + record.getId() + " in ATM Accounts");
        return Result;
    }

    public String delete(Object... obj) throws Throwable {
        AtmAccountsDTOInter record = (AtmAccountsDTOInter) obj[0];
        super.preCollable();
        CallableStatement stat = super.executeCallableStatment("{call ? := CASH_MANAGEMENT.delete_atm_account(?,?)}");
        stat.registerOutParameter(1, Types.VARCHAR);
        stat.registerOutParameter(3, Types.VARCHAR);
        stat.setInt(2, record.getId());
        stat.executeUpdate();
        String Result = stat.getString(1);
        if ("FALSE".equals(Result)) {
            Result = stat.getString(3);
        } else {
            Result = "Record Has Been Deleted Successfully";
        }
        super.postCollable(stat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("delete record with id " + record.getId() + " in ATM Accounts");
        return Result;
    }
}
