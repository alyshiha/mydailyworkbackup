/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.MatchingTypeDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class MatchingTypeDAO extends BaseDAO implements MatchingTypeDAOInter {

    protected MatchingTypeDAO() {
        super();
        super.setTableName("matching_type");

    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchingTypeDTOInter matchingType = (MatchingTypeDTOInter) obj[0];
        String insertStat = "insert into " + super.getTableName() + " values(" + matchingType.getId() + ","
                + matchingType.getType1() + "," + matchingType.getType2() + "," + matchingType.getActive() + ",'" + matchingType.getName() + "')";
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchingTypeDTOInter matchingType = (MatchingTypeDTOInter) obj[0];
        String updateStat = "update " + super.getTableName() + " set type1 = " + matchingType.getType1() + ",type2 = " + matchingType.getType2()
                + ", active = " + matchingType.getActive() + ",name = '" + matchingType.getName() + "'"
                + "where id = " + matchingType.getId();
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchingTypeDTOInter matchingType = (MatchingTypeDTOInter) obj[0];
        String deleteStat = "delete from " + super.getTableName() + " where id = " + matchingType.getId();
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchingTypeDTOInter matchingType = (MatchingTypeDTOInter) obj[0];
        String selectStat = "select * from " + super.getTableName() + " where id = " + matchingType.getId();
        ResultSet result = executeQuery(selectStat);
        List<MatchingTypeDTOInter> mtL = new ArrayList<MatchingTypeDTOInter>();
        while (result.next()) {
            MatchingTypeDTOInter mt = DTOFactory.createMatchingTypeDTO();
            mt.setActive(result.getInt("active"));
            mt.setId(result.getInt("id"));
            mt.setName(result.getString("name"));
            mt.setType1(result.getInt("type1"));
            mt.setType2(result.getInt("type2"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object findAll(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from " + super.getTableName() + " order by name";
        ResultSet result = executeQuery(selectStat);
        List<MatchingTypeDTOInter> mtL = new ArrayList<MatchingTypeDTOInter>();
        while (result.next()) {
            MatchingTypeDTOInter mt = DTOFactory.createMatchingTypeDTO();
            mt.setActive(result.getInt("active"));
            mt.setId(result.getInt("id"));
            mt.setName(result.getString("name"));
            mt.setType1(result.getInt("type1"));
            mt.setType2(result.getInt("type2"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object findComboBox(Object... obj) throws Throwable {

        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from " + super.getTableName() + " m where m.active = 1 order by m.id";
        ResultSet result = executeQuery(selectStat);
        List<MatchingTypeDTOInter> mtL = new ArrayList<MatchingTypeDTOInter>();
        while (result.next()) {
            MatchingTypeDTOInter mt = DTOFactory.createMatchingTypeDTO();
            mt.setActive(result.getInt("active"));
            mt.setId(result.getInt("id"));
            mt.setName(result.getString("name"));
            mt.setType1(result.getInt("type1"));
            mt.setType2(result.getInt("type2"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();

        super.postSelect();
        return null;
    }
}
