 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.NetworksDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AlyShiha
 */
public class NetworksDAO extends BaseDAO implements NetworksDAOInter {

    public Boolean findDuplicateRecord(String Name) throws Throwable {
        super.preSelect();
        String selectStat = "select id, name from networks where name = '$name'";
        selectStat = selectStat.replace("$name", Name);
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            super.postSelect(rs);
            return Boolean.TRUE;
        }
        super.postSelect(rs);
        return Boolean.FALSE;
    }

    @Override
    public List<NetworksDTOInter> findRecord(String Name) throws Throwable {
        super.preSelect();
        String selectStat = "select id, name from networks where name like '%$name%' order by name";
        selectStat = selectStat.replace("$name", Name);
        ResultSet rs = executeQuery(selectStat);
        List<NetworksDTOInter> records = new ArrayList<NetworksDTOInter>();
        while (rs.next()) {
            NetworksDTOInter record = DTOFactory.createNetworksDTO();
            record.setId(rs.getInt("id"));
            record.setName(rs.getString("name"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select record in networks with name like "+Name);
        
        return records;
    }

    @Override
    public List<NetworksDTOInter> findAll() throws Throwable {
        super.preSelect();
        String selectStat = "select id, name from networks order by name";
        ResultSet rs = executeQuery(selectStat);
        List<NetworksDTOInter> records = new ArrayList<NetworksDTOInter>();
        while (rs.next()) {
            NetworksDTOInter record = DTOFactory.createNetworksDTO();
            record.setId(rs.getInt("id"));
            record.setName(rs.getString("name"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select all records in networks");
        
        return records;
    }

    @Override
    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        NetworksDTOInter record = (NetworksDTOInter) obj[0];
        record.setId(super.generateSequence("NETWORKS"));
        String insertStat = "insert into networks   (id, name)\n"
                + "values \n"
                + "  ($id, '$name')";
        insertStat = insertStat.replace("$id", "" + record.getId());
        insertStat = insertStat.replace("$name", "" + record.getName());
        super.executeUpdate(insertStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("insert record in networks");
        
        return null;
    }

    @Override
    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        NetworksDTOInter record = (NetworksDTOInter) obj[0];
        String updateStat = "update networks\n"
                + "   set name = '$name'\n"
                + " where id = $id";
        updateStat = updateStat.replace("$name", "" + record.getName());
        updateStat = updateStat.replace("$id", "" + record.getId());
        super.executeUpdate(updateStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("update record in networks");
        
        return null;
    }

    @Override
    public String delete(Object... obj)  {
        try {
            super.preUpdate();
            NetworksDTOInter record = (NetworksDTOInter) obj[0];
            String deleteStat = "delete networks\n"
                    + " where id = $id";
            deleteStat = deleteStat.replace("$id", "" + record.getId());
            super.executeUpdate(deleteStat);
            
            
            CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
            logEngin.insert("delete record in networks");
            return "Record Has Been Succesfully Deleted";
        } catch (Throwable ex) {
            return "Record Can`t be Deleted";
        }
    }

}
