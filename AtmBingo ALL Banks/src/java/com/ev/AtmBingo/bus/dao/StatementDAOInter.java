/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.StatementDTOInter;
import java.util.List;

/**
 *
 * @author Aly.Shiha
 */
public interface StatementDAOInter {

    List<StatementDTOInter> findAll(String from, String ATMID) throws Throwable;
    
}
