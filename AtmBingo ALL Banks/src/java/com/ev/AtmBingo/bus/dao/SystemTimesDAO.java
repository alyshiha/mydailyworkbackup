/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.SystemTimesDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class SystemTimesDAO extends BaseDAO implements SystemTimesDAOInter {

    protected SystemTimesDAO() {
        super();
        super.setTableName("system_times");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SystemTimesDTOInter uDTO = (SystemTimesDTOInter) obj[0];
        String insertStat = "insert into $table values ($day_no , $seq, '$arabic_name', '$english_name'"
                + ", to_date('$start_time','hh24:mi'),to_date('$end_time','hh24:mi'), $working )";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$day_no", "" + uDTO.getDayNo());
        insertStat = insertStat.replace("$seq", "" + uDTO.getSeq());
        insertStat = insertStat.replace("$arabic_name", "" + uDTO.getArabicName());
        insertStat = insertStat.replace("$english_name", "" + uDTO.getEnglishName());
        insertStat = insertStat.replace("$start_time", "" + DateFormatter.changeTimeFormat(uDTO.getStartTime()));
        insertStat = insertStat.replace("$end_time", "" + DateFormatter.changeTimeFormat(uDTO.getEndTime()));
        insertStat = insertStat.replace("$working", "" + uDTO.getWorking());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SystemTimesDTOInter uDTO = (SystemTimesDTOInter) obj[0];
        String updateStat = "update $table set  arabic_name = '$arabic_name'"
                + ", english_name = '$english_name' , start_time = to_date('$start_date','hh24:mi')"
                + ", end_time = to_date('$end_date','hh24:mi'), working = $working"
                + " where day_no = $day_no";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$arabic_name", "" + uDTO.getArabicName());
        updateStat = updateStat.replace("$english_name", "" + uDTO.getEnglishName());
        updateStat = updateStat.replace("$start_date", "" + DateFormatter.changeTimeWithoutSecFormat(uDTO.getStartTime()));
        updateStat = updateStat.replace("$end_date", "" + DateFormatter.changeTimeWithoutSecFormat(uDTO.getEndTime()));
        updateStat = updateStat.replace("$working", "" + uDTO.getWorking());
        updateStat = updateStat.replace("$day_no", "" + uDTO.getDayNo());
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SystemTimesDTOInter uDTO = (SystemTimesDTOInter) obj[0];
        String deleteStat = "delete from $table where day_no = $day_no";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$day_no", "" + uDTO.getDayNo());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer dayNo = (Integer) obj[0];
        String selectStat = "select * from $table where day_no = $day_no";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$day_no", "" + dayNo);
        ResultSet rs = executeQuery(selectStat);
        SystemTimesDTOInter uDTO = DTOFactory.createSystemTimesDTO();
        while (rs.next()) {
            uDTO.setDayNo(rs.getInt("day_no"));
            uDTO.setArabicName(rs.getString("arabic_name"));
            uDTO.setEndTime(rs.getTime("end_time"));
            uDTO.setStartTime(rs.getTime("start_time"));
            uDTO.setWorking(rs.getInt("working"));
            uDTO.setSeq(rs.getInt("seq"));
            uDTO.setEnglishName(rs.getString("english_name"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<SystemTimesDTOInter> uDTOL = new ArrayList<SystemTimesDTOInter>();
        while (rs.next()) {
            SystemTimesDTOInter uDTO = DTOFactory.createSystemTimesDTO();
            uDTO.setDayNo(rs.getInt("day_no"));
            uDTO.setArabicName(rs.getString("arabic_name"));
            uDTO.setEndTime(rs.getTime("end_time"));
            uDTO.setStartTime(rs.getTime("start_time"));
            uDTO.setWorking(rs.getInt("working"));
            uDTO.setSeq(rs.getInt("seq"));
            uDTO.setEnglishName(rs.getString("english_name"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public static void main(String[] args) throws Throwable {
    }
}
