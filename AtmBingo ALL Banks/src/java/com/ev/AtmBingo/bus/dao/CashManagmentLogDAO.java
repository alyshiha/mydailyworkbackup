/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.CashManagmentLogDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author AlyShiha
 */
public class CashManagmentLogDAO extends BaseDAO implements CashManagmentLogDAOInter {

    @Override
    public List<CashManagmentLogDTOInter> findRecord(Integer User, Date Fromdate, Date Todate) throws Throwable {
        super.preSelect();
        String selectStat = "select c.id id, c.action_date action_date, (select s.user_name from users s where s.user_id = c.user_id) userName,"
                + "c.user_id user_id, c.ip_address ip_address, c.action action"
                + " from cash_managment_log c "
                + "where c.action_date between to_date('$datefrom','dd.MM.yyyy hh24:mi:ss') "
                + "and to_date('$dateto','dd.MM.yyyy hh24:mi:ss') "
                + "and (c.user_id = $user Or $user = 0) "
                + "order by c.action_date";
        selectStat = selectStat.replace("$user", "" + User);
        selectStat = selectStat.replace("$datefrom", "" + DateFormatter.changeDateAndTimeFormat(Fromdate));
        selectStat = selectStat.replace("$dateto", "" + DateFormatter.changeDateAndTimeFormat(Todate));

        ResultSet rs = executeQuery(selectStat);
        List<CashManagmentLogDTOInter> records = new ArrayList<CashManagmentLogDTOInter>();
        while (rs.next()) {
            CashManagmentLogDTOInter record = DTOFactory.createCashManagmentLogDTO();
            record.setId(rs.getInt("id"));
            record.setActionDate(rs.getTimestamp("action_date"));
            record.setUserId(rs.getInt("user_id"));
            record.setIpAddress(rs.getString("ip_address"));
            record.setAction(rs.getString("action"));
            record.setUserName(rs.getString("userName"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public Object insert(String Action) throws Throwable {
        super.preUpdate();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        String ip = req.getRemoteAddr();

        Integer id = super.generateSequence("cash_managment_log");
        String insertStat = "insert into cash_managment_log\n"
                + "  (id, action_date, user_id, ip_address, action) values\n"
                + "  ($id, sysdate, $userid, '$ipaddress', '$action')";
        insertStat = insertStat.replace("$id", "" + id);
        insertStat = insertStat.replace("$userid", "" + super.getLoggedInUser());
        insertStat = insertStat.replace("$action", "" + Action);
        insertStat = insertStat.replace("$ipaddress", "" + ip);
        //System.out.println(insertStat);
        super.executeUpdate(insertStat);
        return null;
    }

}
