/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmMachineTypeDAO extends BaseDAO implements AtmMachineTypeDAOInter {

    protected AtmMachineTypeDAO() {
        super();
        super.setTableName("atm_machine_type");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmMachineTypeDTOInter machineType = (AtmMachineTypeDTOInter) obj[0];
        machineType.setId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into " + super.getTableName() + " values(" + machineType.getId() + ","
                + "'" + machineType.getName() + "')";
        super.executeUpdate(insertStat);
        super.postUpdate("Add " + machineType.getName() + " Machine Type", false);
        return null;

    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmMachineTypeDTOInter machineType = (AtmMachineTypeDTOInter) obj[0];
        String updateStat = "update " + super.getTableName() + " set name = '" + machineType.getName() + "'"
                + " where id = " + machineType.getId();
        super.executeUpdate(updateStat);
        super.postUpdate("Update " + machineType.getName() + " Machine Type", false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmMachineTypeDTOInter machineType = (AtmMachineTypeDTOInter) obj[0];
        String deleteStat = "delete from " + super.getTableName()
                + " where id = " + machineType.getId();
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete " + machineType.getName() + " Machine Type", false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer matchingType = (Integer) obj[0];
        String selectStat = "select * from " + super.getTableName()
                + " where id = " + matchingType.intValue();
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineTypeDTOInter> mtL = new ArrayList<AtmMachineTypeDTOInter>();
        while (result.next()) {
            AtmMachineTypeDTOInter mt = DTOFactory.createAtmMachineTypeDTO();
            mt.setId(result.getInt("id"));
            mt.setName(result.getString("name"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL.get(0);
    }

    public Object findAll(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from " + super.getTableName() + " order by name";
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineTypeDTOInter> mtL = new ArrayList<AtmMachineTypeDTOInter>();
        while (result.next()) {
            AtmMachineTypeDTOInter mt = DTOFactory.createAtmMachineTypeDTO();
            mt.setId(result.getInt("id"));
            mt.setName(result.getString("name"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String name = (String) obj[0];
        String selectStat = "select * from " + super.getTableName() + " where name = '" + name + "' order by name";
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineTypeDTOInter> mtL = new ArrayList<AtmMachineTypeDTOInter>();
        while (result.next()) {
            AtmMachineTypeDTOInter mt = DTOFactory.createAtmMachineTypeDTO();
            mt.setId(result.getInt("id"));
            mt.setName(result.getString("name"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL.get(0);
    }
}
