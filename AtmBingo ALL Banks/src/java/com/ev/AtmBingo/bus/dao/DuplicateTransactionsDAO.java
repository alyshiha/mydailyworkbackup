/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DuplicateTransactionsDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class DuplicateTransactionsDAO extends BaseDAO implements DuplicateTransactionsDAOInter {

    protected DuplicateTransactionsDAO() {
        super();
        super.setTableName("duplicate_transactions");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DuplicateTransactionsDTOInter uDTO = (DuplicateTransactionsDTOInter) obj[0];
        String insertStat = "insert into $table values ($file_id, '$loading_date' , '$atm_application_id', $atm_id"
                + ", '$transaction_type', $transaction_type_id, '$currency', $currency_id, '$transaction_status', $transaction_status_id"
                + ", '$response_code', $response_code_id, '$transaction_date', '$transaction_sequence', " + super.getencryptcardcolumn("$card_no")
                + ", $amount, '$settlement_date', '$notes_presented', '$customer_account_number', $transaction_sequence_order_by"
                + ", '$transaction_time', $record_type"
                + ", '$column1', '$column2', '$column3', '$column4', '$column5', $transaction_type_master"
                + ", $response_code_master, $card_no_suffix)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$file_id", "" + uDTO.getFileId());
        insertStat = insertStat.replace("$loading_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getLoadingDate()));
        insertStat = insertStat.replace("$atm_application_id", "" + uDTO.getAtmApplicationId());
        insertStat = insertStat.replace("$atm_id", "" + uDTO.getAtmId());
        insertStat = insertStat.replace("$transaction_type", "" + uDTO.getTransactionType());
        insertStat = insertStat.replace("$transaction_type_id", "" + uDTO.getTransactionTypeId());
        insertStat = insertStat.replace("$currency", "" + uDTO.getCurrency());
        insertStat = insertStat.replace("$currency_id", "" + uDTO.getCurrencyId());
        insertStat = insertStat.replace("$transaction_status", "" + uDTO.getTransactionStatus());
        insertStat = insertStat.replace("$transaction_status_id", "" + uDTO.getTransactionStatusId());
        insertStat = insertStat.replace("$response_code", "" + uDTO.getResponseCode());
        insertStat = insertStat.replace("$response_code_id", "" + uDTO.getResponseCodeId());
        insertStat = insertStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactionDate()));
        insertStat = insertStat.replace("$transaction_sequence", "" + uDTO.getTransactionSeqeunce());
        insertStat = insertStat.replace("$card_no", "" + uDTO.getCardNo());
        insertStat = insertStat.replace("$amount", "" + uDTO.getAmount());
        insertStat = insertStat.replace("$settlement_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getSettlementDate()));
        insertStat = insertStat.replace("$notes_presented", "" + uDTO.getNotesPresented());
        insertStat = insertStat.replace("$customer_account_number", "" + uDTO.getCustomerAccountNumber());
        insertStat = insertStat.replace("$transaction_sequence_order_by", "" + uDTO.getTransactionSequenceOrderBy());
        insertStat = insertStat.replace("$transaction_time", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactionTime()));
        insertStat = insertStat.replace("$record_type", "" + uDTO.getRecordType());
        insertStat = insertStat.replace("$column1", "" + uDTO.getColumn1());
        insertStat = insertStat.replace("$column2", "" + uDTO.getColumn2());
        insertStat = insertStat.replace("$column3", "" + uDTO.getColumn3());
        insertStat = insertStat.replace("$column4", "" + uDTO.getColumn4());
        insertStat = insertStat.replace("$column5", "" + uDTO.getColumn5());
        insertStat = insertStat.replace("$transaction_type_master", "" + uDTO.getTransactionTypeMaster());
        insertStat = insertStat.replace("$response_code_master", "" + uDTO.getResponseCodeMaster());
        insertStat = insertStat.replace("$card_no_suffix", "" + uDTO.getCardNoSuffix());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DuplicateTransactionsDTOInter uDTO = (DuplicateTransactionsDTOInter) obj[0];
        String updateStat = "update $table set file_id = $file_id, atm_id = $atm_id, transaction_type = '$transaction_type'"
                + ", transaction_type_id = $transaction_type_id, currency_id = $currency_id, transaction_status = '$transaction_status'"
                + ", transaction_status_id = $transaction_status_id, response_code_id = $response_code_id, transaction_sequence = '$transaction_sequence'"
                + ", settlement_date = '$settlement_date', notes_presented = '$notes_presented', customer_account_number = '$customer_account_number'"
                + ", transaction_sequence_order_by = $transaction_sequence_order_by, transaction_time = '$transaction_time'"
                + ", column1 = '$column1'"
                + ", column2 = '$column2', column3 = '$column3', column4 = '$column4', column5 = '$column5', transaction_type_master = $transaction_type_master"
                + ", response_code_master = $response_code_master, card_no_suffix = $card_no_suffix"
                + " where transaction_date = '$transaction_date' and atm_application_id = '$atm_application_id' "
                + "and " + super.getcardcolumn() + " = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = '$response_code' "
                + "and record_type = $record_type";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$file_id", "" + uDTO.getFileId());
        updateStat = updateStat.replace("$loading_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getLoadingDate()));
        updateStat = updateStat.replace("$atm_application_id", "" + uDTO.getAtmApplicationId());
        updateStat = updateStat.replace("$atm_id", "" + uDTO.getAtmId().getId());
        updateStat = updateStat.replace("$transaction_type", "" + uDTO.getTransactionType());
        updateStat = updateStat.replace("$transaction_type_id", "" + uDTO.getTransactionTypeId().getId());
        updateStat = updateStat.replace("$currency", "" + uDTO.getCurrency());
        updateStat = updateStat.replace("$currency_id", "" + uDTO.getCurrencyId().getId());
        updateStat = updateStat.replace("$transaction_status", "" + uDTO.getTransactionStatus());
        updateStat = updateStat.replace("$transaction_status_id", "" + uDTO.getTransactionStatusId().getId());
        updateStat = updateStat.replace("$response_code", "" + uDTO.getResponseCode());
        updateStat = updateStat.replace("$response_code_id", "" + uDTO.getResponseCodeId().getId());
        updateStat = updateStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactionDate()));
        updateStat = updateStat.replace("$transaction_sequence", "" + uDTO.getTransactionSeqeunce());
        updateStat = updateStat.replace("$card_no", "" + uDTO.getCardNo());
        updateStat = updateStat.replace("$amount", "" + uDTO.getAmount());
        updateStat = updateStat.replace("$settlement_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getSettlementDate()));
        updateStat = updateStat.replace("$notes_presented", "" + uDTO.getNotesPresented());
        updateStat = updateStat.replace("$customer_account_number", "" + uDTO.getCustomerAccountNumber());
        updateStat = updateStat.replace("$transaction_sequence_order_by", "" + uDTO.getTransactionSequenceOrderBy());
        updateStat = updateStat.replace("$transaction_time", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactionTime()));
        updateStat = updateStat.replace("$record_type", "" + uDTO.getRecordType());
        updateStat = updateStat.replace("$column1", "" + uDTO.getColumn1());
        updateStat = updateStat.replace("$column2", "" + uDTO.getColumn2());
        updateStat = updateStat.replace("$column3", "" + uDTO.getColumn3());
        updateStat = updateStat.replace("$column4", "" + uDTO.getColumn4());
        updateStat = updateStat.replace("$column5", "" + uDTO.getColumn5());
        updateStat = updateStat.replace("$transaction_type_master", "" + uDTO.getTransactionTypeMaster());
        updateStat = updateStat.replace("$response_code_master", "" + uDTO.getResponseCodeMaster());
        updateStat = updateStat.replace("$card_no_suffix", "" + uDTO.getCardNoSuffix());
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DuplicateTransactionsDTOInter uDTO = (DuplicateTransactionsDTOInter) obj[0];
        String deleteStat = "delete from $table where transaction_date = '$transaction_date' and atm_application_id = '$atm_application_id' "
                + "and " + super.getcardcolumn() + " = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = '$response_code' "
                + "and record_type = $record_type";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$atm_application_id", "" + uDTO.getAtmApplicationId());
        deleteStat = deleteStat.replace("$currency", "" + uDTO.getCurrency());
        deleteStat = deleteStat.replace("$response_code", "" + uDTO.getResponseCode());
        deleteStat = deleteStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactionDate()));
        deleteStat = deleteStat.replace("$card_no", "" + uDTO.getCardNo());
        deleteStat = deleteStat.replace("$amount", "" + uDTO.getAmount());
        deleteStat = deleteStat.replace("$record_type", "" + uDTO.getRecordType());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Date transactionDate, String atmApplicationId, String cardNo, int amount, String currency, String responseCode, int matchingType, int recordType) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where transaction_date = '$transaction_date' and atm_application_id = '$atm_application_id' "
                + "and " + super.getcardcolumn() + " = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = '$response_code' "
                + "and record_type = $record_type";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$atm_application_id", "" + atmApplicationId);
        selectStat = selectStat.replace("$currency", "" + currency);
        selectStat = selectStat.replace("$response_code", "" + responseCode);
        selectStat = selectStat.replace("$transaction_date", "" + transactionDate);
        selectStat = selectStat.replace("$card_no", "" + cardNo);
        selectStat = selectStat.replace("$amount", "" + amount);
        selectStat = selectStat.replace("$record_type", "" + recordType);
        ResultSet rs = executeQuery(selectStat);
        DuplicateTransactionsDTOInter uDTO = DTOFactory.createDuplicateTransactionsDTO();
        while (rs.next()) {
            uDTO.setAmount(rs.getInt("amount"));
            uDTO.setAtmApplicationId(rs.getString("atm_application_id"));

            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
            amDTO.setId(rs.getInt("atm_id"));
            uDTO.setAtmId(amDTO);

            uDTO.setCardNo(rs.getString("card_no"));
            uDTO.setCardNoSuffix(rs.getInt("card_no_suffix"));
            uDTO.setColumn1(rs.getString("column1"));
            uDTO.setColumn2(rs.getString("column2"));
            uDTO.setColumn3(rs.getString("column3"));
            uDTO.setColumn4(rs.getString("column4"));
            uDTO.setColumn5(rs.getString("column5"));
            uDTO.setCurrency(rs.getString("currency"));

            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
            cmDTO.setId((rs.getInt("currency_id")));
            uDTO.setCurrencyId(cmDTO);

            uDTO.setCustomerAccountNumber(rs.getString("customer_account_number"));
            uDTO.setFileId(rs.getInt("file_id"));
            uDTO.setLoadingDate(rs.getDate("loading_date"));
            uDTO.setNotesPresented(rs.getString("notes_presented"));
            uDTO.setRecordType(rs.getInt("record_type"));
            uDTO.setResponseCode(rs.getString("response_code"));

            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
            trcDTO.setId((rs.getInt("response_code_id")));
            uDTO.setResponseCodeId(trcDTO);

            uDTO.setResponseCodeMaster(rs.getInt("response_code_master"));
            uDTO.setSettlementDate(rs.getDate("settlement_date"));
            uDTO.setTransactionDate(rs.getDate("transaction_date"));
            uDTO.setTransactionSeqeunce(rs.getString("transaction_sequence"));
            uDTO.setTransactionSequenceOrderBy(rs.getInt("transaction_sequence_order_by"));
            uDTO.setTransactionStatus(rs.getString("transaction_status"));

            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
            tsDTO.setId((rs.getInt("transaction_status_id")));
            uDTO.setTransactionStatusId(tsDTO);

            uDTO.setTransactionTime(rs.getDate("transaction_time"));
            uDTO.setTransactionType(rs.getString("transaction_type"));

            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
            ttDTO.setId((rs.getInt("transaction_type_id")));
            uDTO.setTransactionTypeId(ttDTO);

            uDTO.setTransactionTypeMaster(rs.getInt("transaction_type_master"));

        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll(AtmFileDTOInter fileId) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select $table.*," + super.getcardcolumntable() + " card_no_decrypt from $table where file_id = $fileId order by atm_application_id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$fileId", "" + fileId.getId());
        ResultSet rs = executeQuery(selectStat);
        List<DuplicateTransactionsDTOInter> uDTOL = new ArrayList<DuplicateTransactionsDTOInter>();
        DuplicateTransactionsDTOInter uDTO = DTOFactory.createDuplicateTransactionsDTO();
        while (rs.next()) {
            uDTO.setAmount(rs.getInt("amount"));
            uDTO.setAtmApplicationId(rs.getString("atm_application_id"));

            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
            amDTO.setId(rs.getInt("atm_id"));
            uDTO.setAtmId(amDTO);

            uDTO.setCardNo(rs.getString("card_no_decrypt"));
            uDTO.setCardNoSuffix(rs.getInt("card_no_suffix"));
            uDTO.setColumn1(rs.getString("column1"));
            uDTO.setColumn2(rs.getString("column2"));
            uDTO.setColumn3(rs.getString("column3"));
            uDTO.setColumn4(rs.getString("column4"));
            uDTO.setColumn5(rs.getString("column5"));
            uDTO.setCurrency(rs.getString("currency"));

            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
            cmDTO.setId((rs.getInt("currency_id")));
            uDTO.setCurrencyId(cmDTO);

            uDTO.setCustomerAccountNumber(rs.getString("customer_account_number"));
            uDTO.setFileId(rs.getInt("file_id"));
            uDTO.setLoadingDate(rs.getDate("loading_date"));
            uDTO.setNotesPresented(rs.getString("notes_presented"));
            uDTO.setRecordType(rs.getInt("record_type"));
            uDTO.setResponseCode(rs.getString("response_code"));

            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
            trcDTO.setId((rs.getInt("response_code_id")));
            uDTO.setResponseCodeId(trcDTO);

            uDTO.setResponseCodeMaster(rs.getInt("response_code_master"));

            uDTO.setSettlementDate(rs.getDate("settlement_date"));
            uDTO.setTransactionDate(rs.getDate("transaction_date"));
            uDTO.setTransactionSeqeunce(rs.getString("transaction_sequence"));
            uDTO.setTransactionSequenceOrderBy(rs.getInt("transaction_sequence_order_by"));
            uDTO.setTransactionStatus(rs.getString("transaction_status"));

            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
            tsDTO.setId((rs.getInt("transaction_status_id")));
            uDTO.setTransactionStatusId(tsDTO);

            uDTO.setTransactionTime(rs.getDate("transaction_time"));
            uDTO.setTransactionType(rs.getString("transaction_type"));

            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
            ttDTO.setId((rs.getInt("transaction_type_id")));
            uDTO.setTransactionTypeId(ttDTO);

            uDTO.setTransactionTypeMaster(rs.getInt("transaction_type_master"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public static void main(String[] args) throws Throwable {
    }
}
