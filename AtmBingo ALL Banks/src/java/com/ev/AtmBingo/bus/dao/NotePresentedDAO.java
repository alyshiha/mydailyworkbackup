/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.NotePresentedDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class NotePresentedDAO extends BaseDAO implements NotePresentedDAOInter {

    protected NotePresentedDAO() {
        super();
    }

    @Override
    public List<NotePresentedDTOInter> findNotePresentedexcelReport(String dateFrom, String dateTo, Integer atmGroup, int userID, String atmAppID, String operator) throws Throwable {
        super.preSelect();
        String selectStatment = "SELECT   ATM_APPLICATION_ID,\n"
                + "         atm_id,\n"
                + "         CURRENCY,\n"
                + "         RESPONSE_CODE,\n"
                + "         TRANSACTION_DATE,\n"
                + "         TRANSACTION_SEQUENCE,\n"
                + "         CARD_NO,\n"
                + "         AMOUNT,\n"
                + "         MATCHING_TYPE,\n"
                + "         RECORD_TYPE,\n"
                + "         NOTES_PRESENTED,\n"
                + "         get_notes_amount (NOTES_PRESENTED, atm_id) notes_amount\n"
                + "  FROM   disputes\n"
                + " WHERE   transaction_date BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ('$P{DateTo}',\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND ATM_ID IN\n"
                + "                  (SELECT   id\n"
                + "                     FROM   atm_machine\n"
                + "                    WHERE   ((atm_group IN\n"
                + "                                  (SELECT   id\n"
                + "                                     FROM   atm_group\n"
                + "                                    WHERE   parent_id IN\n"
                + "                                                  (SELECT   id\n"
                + "                                                     FROM   atm_group\n"
                + "                                                    WHERE   (parent_id =\n"
                + "                                                                $P{ATMGroup}\n"
                + "                                                             OR id =\n"
                + "                                                                  $P{ATMGroup}))\n"
                + "                                            OR id = $P{ATMGroup})) or (0 = $P{ATMGroup}))\n"
                + "                            AND EXISTS\n"
                + "                                  (SELECT   1\n"
                + "                                     FROM   user_atm\n"
                + "                                    WHERE   atm_id = atm_machine.id\n"
                + "                                            AND user_id = $P{UserID}))\n"
                + "         AND atm_application_id LIKE '%$P{ATMID}%'\n"
                + "         AND ( (get_notes_amount (NOTES_PRESENTED, atm_id) <> amount\n"
                + "                AND 1 = $P{OperatorFlag})\n"
                + "              OR (get_notes_amount (NOTES_PRESENTED, atm_id) = amount\n"
                + "                  AND 2 = $P{OperatorFlag})\n"
                + "              OR 3 = $P{OperatorFlag})";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DateTo}", dateTo);
        selectStatment = selectStatment.replace("$P{ATMGroup}", "" + atmGroup);
        selectStatment = selectStatment.replace("$P{UserID}", "" + userID);
        selectStatment = selectStatment.replace("$P{ATMID}", "" + atmAppID);
        selectStatment = selectStatment.replace("$P{OperatorFlag}", "" + operator);

        ResultSet rs = executeQueryReport(selectStatment);
        List<NotePresentedDTOInter> Records = new ArrayList<NotePresentedDTOInter>();
        while (rs.next()) {
            NotePresentedDTOInter record = DTOFactory.createNotePresentedDTO();
            record.setAmount(rs.getBigDecimal("AMOUNT"));
            record.setAtmapplicationid(rs.getString("ATM_APPLICATION_ID"));
            record.setAtmid(rs.getInt("atm_id"));
            record.setCardno(rs.getString("CARD_NO"));
            record.setCurrency(rs.getString("CURRENCY"));
            record.setMatchtype(rs.getString("MATCHING_TYPE"));
            record.setMatchtypeid(0);
            record.setNotepres(rs.getString("NOTES_PRESENTED"));
            record.setNotepresamount(rs.getBigDecimal("notes_amount"));
            record.setRecordtype(rs.getString("RECORD_TYPE"));
            record.setRecordtypeid(0);
            record.setResponsecode(rs.getString("RESPONSE_CODE"));
            record.setTransactiondate(rs.getDate("TRANSACTION_DATE"));
            record.setTransactionseqeunce(rs.getString("TRANSACTION_SEQUENCE"));
            Records.add(record);
        }

        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findNotePresentedReport(String dateFrom, String dateTo, Integer atmGroup, int userID, String atmAppID, String operator) throws Throwable {
        super.preSelect();
        String selectStatment = "SELECT   ATM_APPLICATION_ID,\n"
                + "         atm_id,\n"
                + "         CURRENCY,\n"
                + "         RESPONSE_CODE,\n"
                + "         TRANSACTION_DATE,\n"
                + "         TRANSACTION_SEQUENCE,\n"
                + "         CARD_NO,\n"
                + "         AMOUNT,\n"
                + "         MATCHING_TYPE,\n"
                + "         RECORD_TYPE,\n"
                + "         NOTES_PRESENTED,\n"
                + "         get_notes_amount (NOTES_PRESENTED, atm_id) notes_amount\n"
                + "  FROM   disputes\n"
                + " WHERE   transaction_date BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ('$P{DateTo}',\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND ATM_ID IN\n"
                + "                  (SELECT   id\n"
                + "                     FROM   atm_machine\n"
                + "                    WHERE   ((atm_group IN\n"
                + "                                  (SELECT   id\n"
                + "                                     FROM   atm_group\n"
                + "                                    WHERE   parent_id IN\n"
                + "                                                  (SELECT   id\n"
                + "                                                     FROM   atm_group\n"
                + "                                                    WHERE   (parent_id =\n"
                + "                                                                $P{ATMGroup}\n"
                + "                                                             OR id =\n"
                + "                                                                  $P{ATMGroup}))\n"
                + "                                            OR id = $P{ATMGroup})) or (0 = $P{ATMGroup}))\n"
                + "                            AND EXISTS\n"
                + "                                  (SELECT   1\n"
                + "                                     FROM   user_atm\n"
                + "                                    WHERE   atm_id = atm_machine.id\n"
                + "                                            AND user_id = $P{UserID}))\n"
                + "         AND atm_application_id LIKE '%$P{ATMID}%'\n"
                + "         AND ( (get_notes_amount (NOTES_PRESENTED, atm_id) <> amount\n"
                + "                AND 1 = $P{OperatorFlag})\n"
                + "              OR (get_notes_amount (NOTES_PRESENTED, atm_id) = amount\n"
                + "                  AND 2 = $P{OperatorFlag})\n"
                + "              OR 3 = $P{OperatorFlag})";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DateTo}", dateTo);
        selectStatment = selectStatment.replace("$P{ATMGroup}", "" + atmGroup);
        selectStatment = selectStatment.replace("$P{UserID}", "" + userID);
        selectStatment = selectStatment.replace("$P{ATMID}", "" + atmAppID.replace("null", ""));
        selectStatment = selectStatment.replace("$P{OperatorFlag}", "" + operator);
        ResultSet rs = executeQueryReport(selectStatment);
        super.postSelect();
        return rs;
    }
}
