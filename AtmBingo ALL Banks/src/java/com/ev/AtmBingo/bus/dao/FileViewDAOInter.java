/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import java.sql.Clob;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface FileViewDAOInter {

    List<AtmFileDTOInter> dosearch(String atmInt, Date loadingFrom, Date loadingTo, String cardno, String sequence) throws Throwable;

    Clob FindFiletransaction(Object obj) throws Throwable;
}
