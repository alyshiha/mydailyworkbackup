/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.AtmFileTemplateDTOInter;

/**
 *
 * @author Administrator
 */
public interface AtmFileHeaderDAOInter {

    Object findByTemplate(AtmFileTemplateDTOInter aftDTO,String TName) throws Throwable;

    Object delete(Object... obj) throws Throwable;
    Object deleterecordHeader(Object... obj) throws Throwable ;
    Object updaterecordHeader(Object... obj) throws Throwable ;
    Object insertrecordHeader(Object... obj) throws Throwable ;
Object deleterecordHeaderByTemplateID(Object... obj) throws Throwable ;
    Object find(Object... obj) throws Throwable;

    Object findAll() throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
