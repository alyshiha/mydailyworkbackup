package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.AtmBingo.base.util.DateFormatter;
import java.util.ArrayList;
import com.ev.AtmBingo.bus.dto.allbranchtransactionssearchDTOInter;
import java.util.List;

public class allbrachtransactionssearchDAO extends BaseDAO implements allbrachtransactionssearchDAOInter {

    protected allbrachtransactionssearchDAO() {
        super();
        super.setTableName("branch_trans_search");
    }

    public Object insertrecord(allbranchtransactionssearchDTOInter RecordToInsert,Integer userid) throws Throwable {
        super.preUpdate();
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into branch_trans_search\n"
                + "  (DEFAULT_TRANS,id,Temp_Name, user_id, atm_id, branch_id, reason_id, transaction_type_id, response_code_id, responsecode_id_op, transaction_date_from, transaction_date_to, transaction_date_oper, card_no, customer_account_number, card_op, cust_acc_num_op)\n"
                + "values\n"
                + "  (2,v_id,'V_Temp_Name', v_user_id, v_atm_id, v_branch_id, v_reason_id, v_transaction_type_id, v_response_code_id, v_responsecode_id_op,"
                + "  to_date('v_transaction_date_from','dd.mm.yyyy hh24:mi:ss'), to_date('v_transaction_date_to','dd.mm.yyyy hh24:mi:ss'), 'v_transaction_date_oper', 'v_card_no', 'v_customer_account_number',"
                + " 'v_card_op', 'v_cust_acc_num_op')";
        insertStat = insertStat.replace("v_id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("V_Temp_Name", "" + RecordToInsert.gettempname());
        insertStat = insertStat.replace("v_user_id", "" + userid);
        insertStat = insertStat.replace("v_atm_id", "" + RecordToInsert.getatmid());
        insertStat = insertStat.replace("v_branch_id", "" + RecordToInsert.getbranchid());
        insertStat = insertStat.replace("v_reason_id", "" + RecordToInsert.getreasonid());
        insertStat = insertStat.replace("v_transaction_type_id", "" + RecordToInsert.gettransactiontypeid());
        insertStat = insertStat.replace("v_response_code_id", "" + RecordToInsert.getresponsecodeid());
        insertStat = insertStat.replace("v_responsecode_id_op", "" + RecordToInsert.getresponsecodeidop());
        insertStat = insertStat.replace("v_transaction_date_from", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiondatefrom()));
        insertStat = insertStat.replace("v_transaction_date_to", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiondateto()));
        insertStat = insertStat.replace("v_transaction_date_oper", "" + RecordToInsert.gettransactiondateoper());
        insertStat = insertStat.replace("v_card_no", "" + RecordToInsert.getcardno());
        insertStat = insertStat.replace("v_customer_account_number", "" + RecordToInsert.getcustomeraccountnumber());
        insertStat = insertStat.replace("v_card_op", "" + RecordToInsert.getcardop());
        insertStat = insertStat.replace("v_cust_acc_num_op", "" + RecordToInsert.getcustaccnumop());
        insertStat = insertStat.replace("null", "");
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To branch_trans_search", false);
        return null;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        allbranchtransactionssearchDTOInter RecordToInsert = (allbranchtransactionssearchDTOInter) obj[0];
        String insertStat = "update branch_trans_search\n"
                + "       set DEFAULT_TRANS = v_DEFAULT_TRANS,"
                + "       atm_id = v_atm_id,\n"
                + "       Temp_Name = 'V_Temp_Name',\n"
                + "       branch_id = v_branch_id,\n"
                + "       reason_id = v_reason_id,\n"
                + "       transaction_type_id = v_transaction_type_id,\n"
                + "       response_code_id = v_response_code_id,\n"
                + "       responsecode_id_op = v_responsecode_id_op,\n"
                + "       transaction_date_from = to_date('v_transaction_date_from','dd.mm.yyyy hh24:mi:ss'),\n"
                + "       transaction_date_to = to_date('v_transaction_date_to','dd.mm.yyyy hh24:mi:ss'),\n"
                + "       transaction_date_oper = 'v_transaction_date_oper',\n"
                + "       card_no = 'v_card_no',\n"
                + "       customer_account_number = 'v_customer_account_number',\n"
                + "       card_op = 'v_card_op',\n"
                + "       cust_acc_num_op = 'v_cust_acc_num_op'\n"
                + " where id = v_id";
        insertStat = insertStat.replace("v_id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("V_Temp_Name", "" + RecordToInsert.gettempname());
        insertStat = insertStat.replace("v_user_id", "" + RecordToInsert.getuserid());
        insertStat = insertStat.replace("v_atm_id", "" + RecordToInsert.getatmid());
        insertStat = insertStat.replace("v_branch_id", "" + RecordToInsert.getbranchid());
        insertStat = insertStat.replace("v_reason_id", "" + RecordToInsert.getreasonid());
        insertStat = insertStat.replace("v_transaction_type_id", "" + RecordToInsert.gettransactiontypeid());
        insertStat = insertStat.replace("v_response_code_id", "" + RecordToInsert.getresponsecodeid());
        insertStat = insertStat.replace("v_responsecode_id_op", "" + RecordToInsert.getresponsecodeidop());
        insertStat = insertStat.replace("v_transaction_date_from", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiondatefrom()));
        insertStat = insertStat.replace("v_transaction_date_to", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiondateto()));
        insertStat = insertStat.replace("v_transaction_date_oper", "" + RecordToInsert.gettransactiondateoper());
        insertStat = insertStat.replace("v_card_no", "" + RecordToInsert.getcardno());
        insertStat = insertStat.replace("v_customer_account_number", "" + RecordToInsert.getcustomeraccountnumber());
        insertStat = insertStat.replace("v_card_op", "" + RecordToInsert.getcardop());
        insertStat = insertStat.replace("v_cust_acc_num_op", "" + RecordToInsert.getcustaccnumop());
        if (RecordToInsert.getdefaulttransbol() == Boolean.FALSE) {
            insertStat = insertStat.replace("v_DEFAULT_TRANS", "" + 2);
        } else if (RecordToInsert.getdefaulttransbol() == Boolean.TRUE) {
            insertStat = insertStat.replace("v_DEFAULT_TRANS", "" + 1);
        }
        insertStat = insertStat.replace("null", "");
        super.executeUpdate(insertStat);
        super.postUpdate("Update Record In Table branch_trans_search", false);
        return null;
    }

    public Object deleterecord(Integer tempid) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from branch_trans_search "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$id", "" + tempid);
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table branch_trans_search", false);
        return "";
    }

    public Object findRecord(Integer tempid) throws Throwable {
        super.preSelect();
        String selectStat = "select id, user_id, atm_id,Temp_Name, branch_id, reason_id, transaction_type_id, response_code_id, responsecode_id_op, "
                + "transaction_date_from, transaction_date_to, transaction_date_oper, card_no, customer_account_number, card_op, "
                + "cust_acc_num_op,DEFAULT_TRANS from branch_trans_search where id = $id";
        selectStat = selectStat.replace("$id", "" + tempid);
        ResultSet rs = executeQuery(selectStat);
        allbranchtransactionssearchDTOInter SelectedRecord = DTOFactory.createallbranchtransactionssearchDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("id"));
            SelectedRecord.setuserid(rs.getInt("user_id"));
                    SelectedRecord.settempname(rs.getString("Temp_Name"));
            SelectedRecord.setatmid(rs.getInt("atm_id"));
            SelectedRecord.setbranchid(rs.getInt("branch_id"));
            SelectedRecord.setreasonid(rs.getInt("reason_id"));
            SelectedRecord.settransactiontypeid(rs.getInt("transaction_type_id"));
            SelectedRecord.setresponsecodeid(rs.getInt("response_code_id"));
            SelectedRecord.setresponsecodeidop(rs.getInt("responsecode_id_op"));
            SelectedRecord.settransactiondatefrom(rs.getTimestamp("transaction_date_from"));
            SelectedRecord.settransactiondateto(rs.getTimestamp("transaction_date_to"));
            SelectedRecord.settransactiondateoper(rs.getString("transaction_date_oper"));
            SelectedRecord.setcardno(rs.getString("card_no"));
            SelectedRecord.setcustomeraccountnumber(rs.getString("customer_account_number"));
            SelectedRecord.setcardop(rs.getString("card_op"));
            SelectedRecord.setcustaccnumop(rs.getString("cust_acc_num_op"));
            SelectedRecord.setdefaulttrans(rs.getInt("DEFAULT_TRANS"));
            if (SelectedRecord.getdefaulttrans() == 1) {
                SelectedRecord.setdefaulttransbol(Boolean.TRUE);
            } else if (SelectedRecord.getdefaulttrans() == 2) {
                SelectedRecord.setdefaulttransbol(Boolean.FALSE);
            }

        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Integer RecordToSelect) throws Throwable {
        super.preSelect();
        String selectStat = "select id, user_id, atm_id, branch_id, reason_id, transaction_type_id, response_code_id, responsecode_id_op, "
                + "transaction_date_from, transaction_date_to, transaction_date_oper, card_no, customer_account_number, card_op, "
                + "cust_acc_num_op,Temp_Name,DEFAULT_TRANS from branch_trans_search where user_id = $user_id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$user_id", "" + RecordToSelect);
        ResultSet rs = executeQuery(selectStat);
        List<allbranchtransactionssearchDTOInter> Records = new ArrayList<allbranchtransactionssearchDTOInter>();
        while (rs.next()) {
            allbranchtransactionssearchDTOInter SelectedRecord = DTOFactory.createallbranchtransactionssearchDTO();
            SelectedRecord.setid(rs.getInt("id"));
            SelectedRecord.setuserid(rs.getInt("user_id"));
            SelectedRecord.settempname(rs.getString("Temp_Name"));
            SelectedRecord.setatmid(rs.getInt("atm_id"));
            SelectedRecord.setbranchid(rs.getInt("branch_id"));
            SelectedRecord.setreasonid(rs.getInt("reason_id"));
            SelectedRecord.settransactiontypeid(rs.getInt("transaction_type_id"));
            SelectedRecord.setresponsecodeid(rs.getInt("response_code_id"));
            SelectedRecord.setresponsecodeidop(rs.getInt("responsecode_id_op"));
            SelectedRecord.settransactiondatefrom(rs.getTimestamp("transaction_date_from"));
            SelectedRecord.settransactiondateto(rs.getTimestamp("transaction_date_to"));
            SelectedRecord.settransactiondateoper(rs.getString("transaction_date_oper"));
            SelectedRecord.setcardno(rs.getString("card_no"));
            SelectedRecord.setcustomeraccountnumber(rs.getString("customer_account_number"));
            SelectedRecord.setcardop(rs.getString("card_op"));
            SelectedRecord.setcustaccnumop(rs.getString("cust_acc_num_op"));
            SelectedRecord.setdefaulttrans(rs.getInt("DEFAULT_TRANS"));
            if (SelectedRecord.getdefaulttrans() == 1) {
                SelectedRecord.setdefaulttransbol(Boolean.TRUE);
            } else if (SelectedRecord.getdefaulttrans() == 2) {
                SelectedRecord.setdefaulttransbol(Boolean.FALSE);
            }
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

}
