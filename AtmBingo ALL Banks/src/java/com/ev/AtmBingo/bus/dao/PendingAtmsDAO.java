/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.NotificationDTOInter;
import com.ev.AtmBingo.bus.dto.PendingAtmsDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class PendingAtmsDAO extends BaseDAO implements PendingAtmsDAOInter {

    protected PendingAtmsDAO() {
        super();
    }

    public void saveAtm(PendingAtmsDTOInter entitie) throws SQLException {

        super.preUpdate();
        String updateStat = "update pending_atms set done = 1,machine_type = $machine_type where atm_id = $atm_id";
        updateStat = updateStat.replace("$machine_type", "" + entitie.getAtmName());
        updateStat = updateStat.replace("$done", "" + entitie.getDone());
        updateStat = updateStat.replace("$atm_id", "" + entitie.getAtmId());
     
        try {
            super.executeUpdate(updateStat);
        } catch (Exception ex) {
            Logger.getLogger(PendingAtmsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
     
            super.postUpdate(null, true);
   
    }

    public void save(List<PendingAtmsDTOInter> entities) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
          
     connection  = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement("update pending_atms set done = ?,machine_type = ? where atm_id = ?");
            for (int i = 0; i < entities.size(); i++) {
                PendingAtmsDTOInter entity = entities.get(i);
                statement.setInt(2, entity.getAtmName());
                statement.setInt(3, entity.getAtmId());
                statement.setInt(1, 1);
                // ...
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            statement.executeBatch();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(PendingAtmsDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PendingAtmsDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                  CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(PendingAtmsDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(PendingAtmsDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

       public Object findAtmMachineType(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "select id,name from ATM_MACHINE_TYPE";
        ResultSet result = executeQuery(selectStat);

        List<AtmMachineDTOInter> Res = new ArrayList<AtmMachineDTOInter>();
        while (result.next()) {
            AtmMachineDTOInter R = DTOFactory.createAtmMachineDTO();
            R.setId(result.getInt("id"));
            R.setName(result.getString("name"));
            Res.add(R);
        }
        super.postSelect(result);
        return Res;
    }

    public Object findAllPendingAtms(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select p.atm_id, p.machine_type, p.done,m.name from pending_atms p, atm_machine m where m.id = p.atm_id and   p.Done = 2";
        ResultSet result = executeQuery(selectStat);
        List<PendingAtmsDTOInter> PendingAtmsList = new ArrayList<PendingAtmsDTOInter>();
        while (result.next()) {
            PendingAtmsDTOInter PendingAtms = DTOFactory.createPendingAtmsDTO();
            PendingAtms.setAtmName(result.getInt(2));
            PendingAtms.setAtmId(result.getInt(1));
            PendingAtms.setDone(result.getInt(3));
            PendingAtms.setAppName(result.getString(4));
            PendingAtmsList.add(PendingAtms);
        }
        super.postSelect(result);
        return PendingAtmsList;
    }
}
