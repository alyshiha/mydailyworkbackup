/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ProfileMenuItemDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ProfileMenuItemDAO extends BaseDAO implements ProfileMenuItemDAOInter {

    protected ProfileMenuItemDAO() {
        super();
        super.setTableName("profile_menu_item");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        ProfileMenuItemDTOInter pDTO = (ProfileMenuItemDTOInter) obj[0];
        String insertStat = "insert into $table values ($profile_id , $menu_id , $item_id)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$profile_id", "" + pDTO.getProfileId());
        insertStat = insertStat.replace("$menu_id", "" + pDTO.getMenuId());
        insertStat = insertStat.replace("$item_id", "" + pDTO.getItemId());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object deleteByProfileId(Object... obj) throws Throwable {
        super.preUpdate();
        ProfileMenuItemDTOInter pDTO = (ProfileMenuItemDTOInter) obj[0];
        String deleteStat = "delete from $table where profile_id = $profile_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$profile_id", "" + pDTO.getProfileId());
        super.postUpdate(null, true);
        return null;
    }

    public Object deleteByMenuId(Object... obj) throws Throwable {
        super.preUpdate();
        ProfileMenuItemDTOInter pDTO = (ProfileMenuItemDTOInter) obj[0];
        String deleteStat = "delete from $table where menu_id = $menu_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$menu_id", "" + pDTO.getMenuId());
        super.postUpdate(null, true);
        return null;
    }

    public Object deleteByItemId(Object... obj) throws Throwable {
        super.preUpdate();
        ProfileMenuItemDTOInter pDTO = (ProfileMenuItemDTOInter) obj[0];
        String deleteStat = "delete from $table where item_id = $item_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$item_id", "" + pDTO.getItemId());
        super.postUpdate(null, true);
        return null;
    }

    public Object deleteByAll(Object... obj) throws Throwable {
        super.preUpdate();
        ProfileMenuItemDTOInter pDTO = (ProfileMenuItemDTOInter) obj[0];
        String deleteStat = "delete from $table where item_id = $item_id and profile_id = $profileId and menu_id = $menuId";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$item_id", "" + pDTO.getItemId());
        deleteStat = deleteStat.replace("$menuId", "" + pDTO.getMenuId());
        deleteStat = deleteStat.replace("$profileId", "" + pDTO.getProfileId());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object findByProfileIdAndMenuId(int profileId, int menuId) throws Throwable {
        preSelect();
        String selectStat = "select * from $table where profile_id = $profileId and menu_id = $menuId ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$profileId", "" + profileId);
        selectStat = selectStat.replace("$menuId", "" + menuId);
        ResultSet rs = executeQuery(selectStat);
        List<ProfileMenuItemDTOInter> pmiDTOL = new ArrayList<ProfileMenuItemDTOInter>();
        while (rs.next()) {
            ProfileMenuItemDTOInter pmiDTO = DTOFactory.createProfileMenuItemDTO();
            pmiDTO.setItemId(rs.getInt("item_id"));
            pmiDTO.setMenuId(rs.getInt("menu_id"));
            pmiDTO.setProfileId(rs.getInt("profile_id"));
            pmiDTOL.add(pmiDTO);
        }
        postSelect(rs);
        return pmiDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        Object obj1 = super.preSelect();
        super.postSelect(obj1);
        return null;
    }

    public Object findAll(Object... obj) throws Throwable {
        preSelect();
        String selectStat = "select * from $table  ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<ProfileMenuItemDTOInter> pmiDTOL = new ArrayList<ProfileMenuItemDTOInter>();
        while (rs.next()) {
            ProfileMenuItemDTOInter pmiDTO = DTOFactory.createProfileMenuItemDTO();
            pmiDTO.setItemId(rs.getInt("item_id"));
            pmiDTO.setMenuId(rs.getInt("menu_id"));
            pmiDTO.setProfileId(rs.getInt("profile_id"));
            pmiDTOL.add(pmiDTO);
        }
        postSelect(rs);
        return pmiDTOL;
    }

    public Object findByProfileId(int profileId) throws Throwable {
        preSelect();
        String selectStat = "select * from $table where profile_id = $profileId ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$profileId", "" + profileId);
        ResultSet rs = executeQuery(selectStat);
        List<ProfileMenuItemDTOInter> pmiDTOL = new ArrayList<ProfileMenuItemDTOInter>();
        while (rs.next()) {
            ProfileMenuItemDTOInter pmiDTO = DTOFactory.createProfileMenuItemDTO();
            pmiDTO.setItemId(rs.getInt("item_id"));
            pmiDTO.setMenuId(rs.getInt("menu_id"));
            pmiDTO.setProfileId(rs.getInt("profile_id"));
            pmiDTOL.add(pmiDTO);
        }
        postSelect(rs);
        return pmiDTOL;
    }
}
