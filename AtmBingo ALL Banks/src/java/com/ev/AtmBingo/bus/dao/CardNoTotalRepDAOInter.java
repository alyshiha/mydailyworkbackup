/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Administrator
 */
public interface CardNoTotalRepDAOInter {

    Object findReport(String dateFrom, String dateTo, int atmGroupInt, int side, int ord, int topCard, int tType) throws Throwable;
     Object findTotalbycardexcel(String dateFrom, String dateTo, int atmGroupInt, int side, int ord, int topCard, int tType) throws Throwable ;

}
