/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Administrator
 */
public interface ValidationRulesDAOInter {

    Boolean checkFormula(String formula,Integer Type)throws Throwable;

    Object findByColumnId(int columnId) throws Throwable;

    Object delete(Object... obj) throws Throwable;

    Object findAll() throws Throwable;

    Object findByValidationType(int validationType) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
