/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Administrator
 */
public interface BingoStatDAOInter {

    Object findBingoStatAll(Object... obj) throws Throwable;
    Object GetTransactionStatDetailMatch(Object... obj) throws Throwable ;
    Object findBingoStatAllCount(Object... obj) throws Throwable ;
    Object GetTransactionStatDetailDisp(Object... obj) throws Throwable ;
    Object GetTransactionStatDetailDispbyatm(Object... obj) throws Throwable ;
    Object GetTransactionStatDetailDispbyatmexcel(Object... obj) throws Throwable;
    Object GetTransactionStatDetailMatchbyatmexcel(Object... obj) throws Throwable;
    Object GetTransactionStatDetailMatchexcel(Object... obj) throws Throwable ;
    public Object GetTransactionStatDetailDispexcel(Object... obj) throws Throwable;
    Object GetTransactionStatDetailMatchbyatm(Object... obj) throws Throwable ;
}
