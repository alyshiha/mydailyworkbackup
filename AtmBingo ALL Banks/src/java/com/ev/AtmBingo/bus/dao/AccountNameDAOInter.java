/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.AccountNameDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface AccountNameDAOInter {

    Boolean findDuplicatesaveRecord(String Name, Integer id) throws Throwable;
            
    Boolean findDuplicateRecord(String Name) throws Throwable;

    List<AccountNameDTOInter> findRecord(String Name) throws Throwable;

    Object delete(Object... obj) throws Throwable;

    List<AccountNameDTOInter> findAll() throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;
}
