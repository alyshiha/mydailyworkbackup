/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.MatchedDataDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class MatchedDataDAO extends BaseDAO implements MatchedDataDAOInter {

    protected MatchedDataDAO() {
        super();
        super.setTableName("matched_data");
    }

    public Clob FindFiletransaction(Object obj) throws Throwable {
        super.preSelect();
        MatchedDataDTOInter mdDTO = (MatchedDataDTOInter) obj;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select decrypt_blob(t.id) filetext from ATM_FILE t where t.id in "
                + "(select d.file_id from matched_data d where "
                + "match_key = $match_key and record_type = 1)";
        Clob elmsg = null;
        selectStat = selectStat.replace("$match_key", "" + mdDTO.getMatchkey());
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            elmsg = rs.getClob("filetext");
        }
        super.postSelect(rs);
        return elmsg;
    }

    public String Findtexttransaction(Object... obj) throws Throwable {
        super.preSelect();
        MatchedDataDTOInter mdDTO = (MatchedDataDTOInter) obj[0];
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select "+super.gettransactiondata()+" transaction_data  from matched_data where  matched_data.match_key = $match_key ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$record_Type", "" + mdDTO.getRecordtype());
        String elmsg = null;
        selectStat = selectStat.replace("$match_key", "" + mdDTO.getMatchkey());
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            // File data = new File("C:\\a.txt");
            elmsg = rs.getString("transaction_data");
            // clobToString( elmsg );
  /*      Reader reader = rs.getCharacterStream("transaction_data");
             FileWriter writer = new FileWriter(data);
             char[] buffer = new char[1];
             while (reader.read(buffer) > 0) {
             writer.write(buffer);
             }
             writer.close();*/
        }
        super.postSelect(rs);
        return elmsg;
    }

    public String Findtexttransaction_other(Object... obj) throws Throwable {
        super.preSelect();
        MatchedDataDTOInter mdDTO = (MatchedDataDTOInter) obj[0];
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select "+super.gettransactiondata()+" transaction_data  from matched_data where  matched_data.match_key = '$match_key' and record_type = $record_Type";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$match_key", "" + mdDTO.getMatchkey());
        selectStat = selectStat.replace("$record_Type", "" + mdDTO.getRecordtype());
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        String elmsg = null;
        selectStat = selectStat.replace("$match_key", "" + mdDTO.getMatchkey());
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            // File data = new File("C:\\a.txt");
            elmsg = rs.getString("transaction_data");
            // clobToString( elmsg );
  /*      Reader reader = rs.getCharacterStream("transaction_data");
             FileWriter writer = new FileWriter(data);
             char[] buffer = new char[1];
             while (reader.read(buffer) > 0) {
             writer.write(buffer);
             }
             writer.close();*/
        }
        super.postSelect(rs);
        return elmsg;
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchedDataDTOInter uDTO = (MatchedDataDTOInter) obj[0];
        String insertStat = "insert into $table values ($file_id, '$loading_date' , '$atm_application_id', $atm_id"
                + ", '$transaction_type', $transaction_type_id, '$currency', $currency_id, '$transaction_status', $transaction_status_id"
                + ", '$response_code', $response_code_id, '$transaction_date', '$transaction_sequence', "+super.getencryptcardcolumn("$card_no")+""
                + ", $amount, '$settlement_date', '$notes_presented', '$customer_account_number', $transaction_sequence_order_by"
                + ", '$transaction_time', $matching_type, $record_type, $match_key, $settled_flag, '$settled_date', $settled_user"
                + ", '$comments', $match_by, '$match_date', '$column1', '$column2', '$column3', '$column4', '$column5', $transaction_type_master"
                + ", $response_code_master, $card_no_suffix)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$file_id", "" + uDTO.getFileid());
        insertStat = insertStat.replace("$loading_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getLoadingdate()));
        insertStat = insertStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        insertStat = insertStat.replace("$atm_id", "" + uDTO.getatmid());
        insertStat = insertStat.replace("$transaction_type", "" + uDTO.getTransactiontype());
        insertStat = insertStat.replace("$transaction_type_id", "" + uDTO.getTransactiontypeid());
        insertStat = insertStat.replace("$currency", "" + uDTO.getCurrency());
        insertStat = insertStat.replace("$currency_id", "" + uDTO.getCurrencyid());
        insertStat = insertStat.replace("$transaction_status", "" + uDTO.getTransactionstatus());
        insertStat = insertStat.replace("$transaction_status_id", "" + uDTO.getTransactionstatusid());
        insertStat = insertStat.replace("$response_code", "" + uDTO.getResponsecode());
        insertStat = insertStat.replace("$response_code_id", "" + uDTO.getResponsecodeid());
        insertStat = insertStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactiondate()));
        insertStat = insertStat.replace("$transaction_sequence", "" + uDTO.getTransactionsequence());
        insertStat = insertStat.replace("$card_no", "" + uDTO.getCardno());
        insertStat = insertStat.replace("$amount", "" + uDTO.getamount());
        insertStat = insertStat.replace("$settlement_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getSettlementdate()));
        insertStat = insertStat.replace("$notes_presented", "" + uDTO.getNotespresented());
        insertStat = insertStat.replace("$customer_account_number", "" + uDTO.getCustomeraccountnumber());
        insertStat = insertStat.replace("$transaction_sequence_order_by", "" + uDTO.getTransactionsequenceorderby());
        insertStat = insertStat.replace("$transaction_time", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactiontime()));
        insertStat = insertStat.replace("$matching_type", "" + uDTO.getMatchingtype());
        insertStat = insertStat.replace("$record_type", "" + uDTO.getRecordtype());
        insertStat = insertStat.replace("$match_key", "" + uDTO.getMatchkey());
        insertStat = insertStat.replace("$settled_flag", "" + uDTO.getSettledflag());
        insertStat = insertStat.replace("$settled_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getSettleddate()));
        insertStat = insertStat.replace("$settled_user", "" + uDTO.getSettleduser());
        insertStat = insertStat.replace("$comments", "" + uDTO.getComments());
        insertStat = insertStat.replace("$match_by", "" + uDTO.getMatchby());
        insertStat = insertStat.replace("$match_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getmatchdate()));
        insertStat = insertStat.replace("$column1", "" + uDTO.getColumn1());
        insertStat = insertStat.replace("$column2", "" + uDTO.getColumn2());
        insertStat = insertStat.replace("$column3", "" + uDTO.getColumn3());
        insertStat = insertStat.replace("$column4", "" + uDTO.getColumn4());
        insertStat = insertStat.replace("$column5", "" + uDTO.getColumn5());
        insertStat = insertStat.replace("$transaction_type_master", "" + uDTO.getTransactiontypemaster());
        insertStat = insertStat.replace("$response_code_master", "" + uDTO.getResponsecodemaster());
        insertStat = insertStat.replace("$card_no_suffix", "" + uDTO.getcardnosuffix());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object insertForMatchedPage(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchedDataDTOInter uDTO = (MatchedDataDTOInter) obj[0];
        UsersDTOInter logedUser = (UsersDTOInter) obj[1];
        int s = super.generateSequence(super.getTableName());
        String insertStat = "insert into disputes ( file_id, loading_date, atm_application_id, atm_id, transaction_type, "
                + "transaction_type_id, currency, currency_id, transaction_status, transaction_status_id, response_code, "
                + "response_code_id, transaction_date, transaction_sequence, card_no, amount, settlement_date, "
                + "notes_presented, customer_account_number, transaction_sequence_order_by, transaction_time, "
                + "matching_type, record_type, dispute_key,comments,dispute_by,dispute_key_uk,ABS_AMOUNT, column1, column2, column3, column4, column5, transaction_type_master, response_code_master, card_no_suffix) "
                + "select  file_id, loading_date, atm_application_id, atm_id, transaction_type, transaction_type_id, "
                + "currency, currency_id, transaction_status, transaction_status_id, response_code, response_code_id, "
                + "transaction_date, transaction_sequence, card_no, amount, settlement_date, notes_presented, "
                + "customer_account_number, transaction_sequence_order_by, transaction_time, matching_type, "
                + "record_type, $s, comments,$user,dispute_key_uk_seq.nextval,abs(amount),column1, column2, column3, column4, column5, transaction_type_master, response_code_master, card_no_suffix "
                + "from $table where MATCH_key = $match_key";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$user", "" + logedUser.getUserId());
        insertStat = insertStat.replace("$match_key", "" + uDTO.getMatchkey());
        insertStat = insertStat.replace("$s", "" + s);

        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchedDataDTOInter uDTO = (MatchedDataDTOInter) obj[0];
        String updateStat = "update $table set file_id = $file_id, atm_id = $atm_id, transaction_type = '$transaction_type'"
                + ", transaction_type_id = $transaction_type_id, currency_id = $currency_id, transaction_status = '$transaction_status'"
                + ", transaction_status_id = $transaction_status_id, response_code_id = $response_code_id, transaction_sequence = '$transaction_sequence'"
                + ", settlement_date = '$settlement_date', notes_presented = '$notes_presented', customer_account_number = '$customer_account_number'"
                + ", transaction_sequence_order_by = $transaction_sequence_order_by, transaction_time = '$transaction_time'"
                + ", match_key = $match_key, settled_flag = $settled_flag, settled_date = '$settled_date', settled_user = $settled_user"
                + ", comments = '$comments', match_by = $match_by, match_date = '$match_date', column1 = '$column1'"
                + ", column2 = '$column2', column3 = '$column3', column4 = '$column4', column5 = '$column5', transaction_type_master = $transaction_type_master"
                + ", response_code_master = $response_code_master, card_no_suffix = $card_no_suffix"
                + " where transaction_date = '$transaction_date' and atm_application_id = '$atm_application_id' "
                + "and " + super.getcardcolumn() + " = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = '$response_code' "
                + "and record_type = $record_type and matching_type = $matching_type";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$file_id", "" + uDTO.getFileid());
        updateStat = updateStat.replace("$loading_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getLoadingdate()));
        updateStat = updateStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        updateStat = updateStat.replace("$atm_id", "" + uDTO.getatmid().getId());
        updateStat = updateStat.replace("$transaction_type", "" + uDTO.getTransactiontype());
        updateStat = updateStat.replace("$transaction_type_id", "" + uDTO.getTransactiontypeid().getId());
        updateStat = updateStat.replace("$currency", "" + uDTO.getCurrency());
        updateStat = updateStat.replace("$currency_id", "" + uDTO.getCurrencyid().getId());
        updateStat = updateStat.replace("$transaction_status", "" + uDTO.getTransactionstatus());
        updateStat = updateStat.replace("$transaction_status_id", "" + uDTO.getTransactionstatusid().getId());
        updateStat = updateStat.replace("$response_code", "" + uDTO.getResponsecode());
        updateStat = updateStat.replace("$response_code_id", "" + uDTO.getResponsecodeid().getId());
        updateStat = updateStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactiondate()));
        updateStat = updateStat.replace("$transaction_sequence", "" + uDTO.getTransactionsequence());
        updateStat = updateStat.replace("$card_no", "" + uDTO.getCardno());
        updateStat = updateStat.replace("$amount", "" + uDTO.getamount());
        updateStat = updateStat.replace("$settlement_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getSettlementdate()));
        updateStat = updateStat.replace("$notes_presented", "" + uDTO.getNotespresented());
        updateStat = updateStat.replace("$customer_account_number", "" + uDTO.getCustomeraccountnumber());
        updateStat = updateStat.replace("$transaction_sequence_order_by", "" + uDTO.getTransactionsequenceorderby());
        updateStat = updateStat.replace("$transaction_time", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactiontime()));
        updateStat = updateStat.replace("$matching_type", "" + uDTO.getMatchingtype());
        updateStat = updateStat.replace("$record_type", "" + uDTO.getRecordtype());
        updateStat = updateStat.replace("$match_key", "" + uDTO.getMatchkey());
        updateStat = updateStat.replace("$settled_flag", "" + uDTO.getSettledflag());
        updateStat = updateStat.replace("$settled_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getSettleddate()));
        updateStat = updateStat.replace("$settled_user", "" + uDTO.getSettleduser().getUserId());
        updateStat = updateStat.replace("$comments", "" + uDTO.getComments());
        updateStat = updateStat.replace("$match_by", "" + uDTO.getMatchby());
        updateStat = updateStat.replace("$match_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getmatchdate()));
        updateStat = updateStat.replace("$column1", "" + uDTO.getColumn1());
        updateStat = updateStat.replace("$column2", "" + uDTO.getColumn2());
        updateStat = updateStat.replace("$column3", "" + uDTO.getColumn3());
        updateStat = updateStat.replace("$column4", "" + uDTO.getColumn4());
        updateStat = updateStat.replace("$column5", "" + uDTO.getColumn5());
        updateStat = updateStat.replace("$transaction_type_master", "" + uDTO.getTransactiontypemaster());
        updateStat = updateStat.replace("$response_code_master", "" + uDTO.getResponsecodemaster());
        updateStat = updateStat.replace("$card_no_suffix", "" + uDTO.getcardnosuffix());
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object updateForMatchedPage(MatchedDataDTOInter uDTO, UsersDTOInter logedUser) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String updateStat = "update $table set settled_flag=1 ,settled_date=sysdate ,settled_user=$user "
                + "where MATCH_key = $match_key";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$user", "" + logedUser.getUserId());
        updateStat = updateStat.replace("$match_key", "" + uDTO.getMatchkey());

        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object deleteForMatchedPage(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchedDataDTOInter uDTO = (MatchedDataDTOInter) obj[0];
        String deleteStat = "delete from $table where MATCH_key = $match_key";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$match_key", "" + uDTO.getMatchkey());

        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchedDataDTOInter uDTO = (MatchedDataDTOInter) obj[0];
        String deleteStat = "delete from $table where transaction_date = '$transaction_date' and atm_application_id = '$atm_application_id' "
                + "and " + super.getcardcolumn() + " = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = '$response_code' "
                + "and record_type = $record_type and matching_type = $matching_type";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        deleteStat = deleteStat.replace("$currency", "" + uDTO.getCurrency());
        deleteStat = deleteStat.replace("$response_code", "" + uDTO.getResponsecode());
        deleteStat = deleteStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactiondate()));
        deleteStat = deleteStat.replace("$card_no", "" + uDTO.getCardno());
        deleteStat = deleteStat.replace("$amount", "" + uDTO.getamount());
        deleteStat = deleteStat.replace("$matching_type", "" + uDTO.getMatchingtype());
        deleteStat = deleteStat.replace("$record_type", "" + uDTO.getRecordtype());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Date transactionDate, String atmApplicationId, String cardNo, int amount, String currency, String responseCode, int matchingType, int recordType) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select *,is_black_list("+super.getcardcolumntable()+") blacklist from $table where transaction_date = '$transaction_date' and atm_application_id = '$atm_application_id' "
                + "and " + super.getcardcolumn() + " = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = '$response_code' "
                + "and record_type = $record_type and matching_type = $matching_type";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$atm_application_id", "" + atmApplicationId);
        selectStat = selectStat.replace("$currency", "" + currency);
        selectStat = selectStat.replace("$response_code", "" + responseCode);
        selectStat = selectStat.replace("$transaction_date", "" + transactionDate);
        selectStat = selectStat.replace("$card_no", "" + cardNo);
        selectStat = selectStat.replace("$amount", "" + amount);
        selectStat = selectStat.replace("$matching_type", "" + matchingType);
        selectStat = selectStat.replace("$record_type", "" + recordType);

        ResultSet rs = executeQuery(selectStat);
        MatchedDataDTOInter uDTO = DTOFactory.createMatchedDataDTO();
        while (rs.next()) {
            uDTO.setamount(rs.getInt("amount"));
            uDTO.setBlacklist(rs.getInt("blacklist"));
            uDTO.setatmapplicationid(rs.getString("atm_application_id"));

            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
            amDTO.setId(rs.getInt("atm_id"));
            uDTO.setatmid(amDTO);

            uDTO.setCardno(rs.getString("card_no"));
            uDTO.setcardnosuffix(rs.getString("card_no_suffix"));
            uDTO.setColumn1(rs.getString("column1"));
            uDTO.setColumn2(rs.getString("column2"));
            uDTO.setColumn3(rs.getString("column3"));
            uDTO.setColumn4(rs.getString("column4"));
            uDTO.setColumn5(rs.getString("column5"));
            uDTO.setComments(rs.getString("comments"));
            uDTO.setCurrency(rs.getString("currency"));

            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
            cmDTO.setId((rs.getInt("currency_id")));
            uDTO.setCurrencyid(cmDTO);

            uDTO.setCustomeraccountnumber(rs.getString("customer_account_number"));
            uDTO.setFileid(rs.getInt("file_id"));
            uDTO.setLoadingdate(rs.getTimestamp("loading_date"));
            uDTO.setMatchby(rs.getInt("match_by"));
            uDTO.setmatchdate(rs.getTimestamp("match_date"));
            uDTO.setMatchkey(rs.getInt("match_key"));
            uDTO.setMatchingtype(rs.getInt("matching_type"));
            uDTO.setNotespresented(rs.getString("notes_presented"));
            uDTO.setRecordtype(rs.getInt("record_type"));
            uDTO.setResponsecode(rs.getString("response_code"));

            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
            trcDTO.setId((rs.getInt("response_code_id")));
            uDTO.setResponsecodeid(trcDTO);

            uDTO.setResponsecodemaster(rs.getInt("response_code_master"));
            uDTO.setSettleddate(rs.getTimestamp("settled_date"));
            uDTO.setSettledflag(rs.getInt("settled_flag"));

            UsersDTOInter userDTO = DTOFactory.createUsersDTO();
            userDTO.setUserId((rs.getInt("settled_user")));
            uDTO.setSettleduser(userDTO);

            uDTO.setSettlementdate(rs.getTimestamp("settlement_date"));
            uDTO.setTransactiondate(rs.getTimestamp("transaction_date"));
            uDTO.setTransactionsequence(rs.getString("transaction_sequence"));
            uDTO.setTransactionsequenceorderby(rs.getInt("transaction_sequence_order_by"));
            uDTO.setTransactionstatus(rs.getString("transaction_status"));

            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
            tsDTO.setId((rs.getInt("transaction_status_id")));
            uDTO.setTransactionstatusid(tsDTO);

            uDTO.setTransactiontime(rs.getTimestamp("transaction_time"));
            uDTO.setTransactiontype(rs.getString("transaction_type"));

            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
            ttDTO.setId((rs.getInt("transaction_type_id")));
            uDTO.setTransactiontypeid(ttDTO);

            uDTO.setTransactiontypemaster(rs.getInt("transaction_type_master"));

        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select $table.*,"+super.getcardcolumntable()+" card_no_decrypt from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<MatchedDataDTOInter> uDTOL = new ArrayList<MatchedDataDTOInter>();
        MatchedDataDTOInter uDTO = DTOFactory.createMatchedDataDTO();
        while (rs.next()) {
            uDTO.setamount(rs.getInt("amount"));
            uDTO.setatmapplicationid(rs.getString("atm_application_id"));

            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
            amDTO.setId(rs.getInt("atm_id"));
            uDTO.setatmid(amDTO);

            uDTO.setCardno(rs.getString("card_no_decrypt"));
            uDTO.setcardnosuffix(rs.getString("card_no_suffix"));
            uDTO.setColumn1(rs.getString("column1"));
            uDTO.setColumn2(rs.getString("column2"));
            uDTO.setColumn3(rs.getString("column3"));
            uDTO.setColumn4(rs.getString("column4"));
            uDTO.setColumn5(rs.getString("column5"));
            uDTO.setComments(rs.getString("comments"));
            uDTO.setCurrency(rs.getString("currency"));

            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
            cmDTO.setId((rs.getInt("currency_id")));
            uDTO.setCurrencyid(cmDTO);

            uDTO.setCustomeraccountnumber(rs.getString("customer_account_number"));
            uDTO.setFileid(rs.getInt("file_id"));
            uDTO.setLoadingdate(rs.getTimestamp("loading_date"));
            uDTO.setMatchby(rs.getInt("match_by"));
            uDTO.setmatchdate(rs.getTimestamp("match_date"));
            uDTO.setMatchkey(rs.getInt("match_key"));
            uDTO.setMatchingtype(rs.getInt("matching_type"));
            uDTO.setNotespresented(rs.getString("notes_presented"));
            uDTO.setRecordtype(rs.getInt("record_type"));
            uDTO.setResponsecode(rs.getString("response_code"));

            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
            trcDTO.setId((rs.getInt("response_code_id")));
            uDTO.setResponsecodeid(trcDTO);

            uDTO.setResponsecodemaster(rs.getInt("response_code_master"));
            uDTO.setSettleddate(rs.getTimestamp("settled_date"));
            uDTO.setSettledflag(rs.getInt("settled_flag"));

            UsersDTOInter userDTO = DTOFactory.createUsersDTO();
            userDTO.setUserId((rs.getInt("settled_user")));
            uDTO.setSettleduser(userDTO);

            uDTO.setSettlementdate(rs.getTimestamp("settlement_date"));
            uDTO.setTransactiondate(rs.getTimestamp("transaction_date"));
            uDTO.setTransactionsequence(rs.getString("transaction_sequence"));
            uDTO.setTransactionsequenceorderby(rs.getInt("transaction_sequence_order_by"));
            uDTO.setTransactionstatus(rs.getString("transaction_status"));

            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
            tsDTO.setId((rs.getInt("transaction_status_id")));
            uDTO.setTransactionstatusid(tsDTO);

            uDTO.setTransactiontime(rs.getTimestamp("transaction_time"));
            uDTO.setTransactiontype(rs.getString("transaction_type"));

            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
            ttDTO.setId((rs.getInt("transaction_type_id")));
            uDTO.setTransactiontypeid(ttDTO);

            uDTO.setTransactiontypemaster(rs.getInt("transaction_type_master"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findAnotherSide(Object obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchedDataDTOInter mdDTO = (MatchedDataDTOInter) obj;
        String selectStat = "select $table.*,"+super.getcardcolumntable()+" card_no_decrypt from $table where match_key = $match_key and record_type <> $record_Type";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$match_key", "" + mdDTO.getMatchkey());
        selectStat = selectStat.replace("$record_Type", "" + mdDTO.getRecordtype());
        ResultSet rs = executeQuery(selectStat);
        MatchedDataDTOInter uDTO = DTOFactory.createMatchedDataDTO();
        while (rs.next()) {
            uDTO.setamount(rs.getInt("amount"));
            uDTO.setatmapplicationid(rs.getString("atm_application_id"));

            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
            amDTO.setId(rs.getInt("atm_id"));
            uDTO.setatmid(amDTO);

            uDTO.setCardno(rs.getString("card_no_decrypt"));
            uDTO.setcardnosuffix(rs.getString("card_no_suffix"));
            uDTO.setColumn1(rs.getString("column1"));
            uDTO.setColumn2(rs.getString("column2"));
            uDTO.setColumn3(rs.getString("column3"));
            uDTO.setColumn4(rs.getString("column4"));
            uDTO.setColumn5(rs.getString("column5"));
            uDTO.setComments(rs.getString("comments"));
            uDTO.setCurrency(rs.getString("currency"));

            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
            cmDTO.setId((rs.getInt("currency_id")));
            uDTO.setCurrencyid(cmDTO);

            uDTO.setCustomeraccountnumber(rs.getString("customer_account_number"));
            uDTO.setFileid(rs.getInt("file_id"));
            uDTO.setLoadingdate(rs.getTimestamp("loading_date"));
            uDTO.setMatchby(rs.getInt("match_by"));
            uDTO.setmatchdate(rs.getTimestamp("match_date"));
            uDTO.setMatchkey(rs.getInt("match_key"));
            uDTO.setMatchingtype(rs.getInt("matching_type"));
            uDTO.setNotespresented(rs.getString("notes_presented"));
            uDTO.setRecordtype(rs.getInt("record_type"));
            uDTO.setResponsecode(rs.getString("response_code"));

            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
            trcDTO.setId((rs.getInt("response_code_id")));
            uDTO.setResponsecodeid(trcDTO);

            uDTO.setResponsecodemaster(rs.getInt("response_code_master"));
            uDTO.setSettleddate(rs.getTimestamp("settled_date"));
            uDTO.setSettledflag(rs.getInt("settled_flag"));

            UsersDTOInter userDTO = DTOFactory.createUsersDTO();
            userDTO.setUserId((rs.getInt("settled_user")));
            uDTO.setSettleduser(userDTO);

            uDTO.setSettlementdate(rs.getTimestamp("settlement_date"));
            uDTO.setTransactiondate(rs.getTimestamp("transaction_date"));
            uDTO.setTransactionsequence(rs.getString("transaction_sequence"));
            uDTO.setTransactionsequenceorderby(rs.getInt("transaction_sequence_order_by"));
            uDTO.setTransactionstatus(rs.getString("transaction_status"));

            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
            tsDTO.setId((rs.getInt("transaction_status_id")));
            uDTO.setTransactionstatusid(tsDTO);

            uDTO.setTransactiontime(rs.getTimestamp("transaction_time"));
            uDTO.setTransactiontype(rs.getString("transaction_type"));

            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
            ttDTO.setId((rs.getInt("transaction_type_id")));
            uDTO.setTransactiontypeid(ttDTO);

            uDTO.setTransactiontypemaster(rs.getInt("transaction_type_master"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public void save(MatchedDataDTOInter[] entities, Integer userId) throws SQLException, ClassNotFoundException, Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement("insert into matched_data_report values(?,?)");
            for (int i = 0; i < entities.length; i++) {
                statement.setInt(1, userId);
                statement.setInt(2, entities[i].getMatchkey());
                // ...
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
    }

    public Object findReport(String fields, String whereClause, MatchedDataDTOInter[] mdDTOArr, Integer userId) throws Throwable {
        super.preSelect();
        save(mdDTOArr, userId);
        /* for (int i = 0; i < mdDTOArr.length; i++) {
         String insertStat = "insert into matched_data_report values($user,$matchedKey)";
         insertStat = insertStat.replace("$user", "" + userId);
         insertStat = insertStat.replace("$matchedKey", "" + mdDTOArr[i].getMatchKey());
         super.executeUpdateReport(insertStat);
         }*/
        // databse insert statement usinf conn pool getConn().createStatement();

        fields = fields.replace("CARD_NO_SUFFIX", "card_no_suffix");
        fields = fields.replace("CARD_NO", super.getcardcolumnmatchedalias()+" CARD_NO,");
        fields = fields.replace("CURRENCY", "");
        fields = fields.replace("corrective_date", "");
        fields = fields.replace("export_date", "");
        fields = fields.replace("export_user", "");
        fields = fields.replace("corrective_user", "");
        fields = fields.replace(", ,", ",");
        fields = fields.replace(",,", ",");
        String selectStat = "select decode(record_type,1,'JR',2,'SW',3,'GL')record_type,"
                + "decode(settled_flag,1,'Y',2,'N')settled_flag,"
                + "  $field "
                + "comments,"
                + "atm_application_id,"
                + "currency,"
                + "(select unit_number from atm_machine m where m.application_id = atm_application_id)unit_id,"
                + " (select name from atm_machine m where m.application_id = atm_application_id)name,"
                + "(decode(record_type,1,0,2,1,3,0)*amount)amnt"
                + " from $table where 1=1 and match_key in (select matched_key from matched_data_report where user_id = $user) order by  atm_application_id,transaction_date,match_key";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$user", "" + userId);
        selectStat = selectStat.replace("$field", "" + fields);
        
        ResultSet rs = executeQueryReport(selectStat);

//        String deleteStat = "delete from matched_data_report where user_id = $user";
//        deleteStat = deleteStat.replace("$user", ""+userId);
//        super.executeUpdate(deleteStat);
        super.postSelect();
        return rs;
    }

    public void deleteFromMatchedReport(int userId) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from matched_data_report where user_id = $user";
        deleteStat = deleteStat.replace("$user", "" + userId);
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
    }
    private BigDecimal fullamount = null;

    public Object[] customSearch(String whereClause, String field, String temp,String index) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select $index rownum elrow,(select l.name from atm_machine l where l.id = $table.atm_id) atmname,is_black_list("+super.getcardcolumntable()+") blacklist,$table.*,"+super.getcardcolumntable()+" card_no_decrypt from $table where $temp $whereClause";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$whereClause", "" + whereClause);
        selectStat = selectStat.replace("$temp", "" + temp);
        selectStat = selectStat.replace("$index", "" + index);
        //selectStat = selectStat.replace("$field", "" + field);
        ResultSet rs = executeQuery(selectStat);
        List<MatchedDataDTOInter> uDTOL = new ArrayList<MatchedDataDTOInter>();
        fullamount = new BigDecimal(BigInteger.ZERO);
        while (rs.next()) {
            MatchedDataDTOInter uDTO = DTOFactory.createMatchedDataDTO();
            uDTO.setamount(rs.getInt("amount"));
            fullamount = fullamount.add(new BigDecimal(uDTO.getamount()));
            uDTO.setBlacklist(rs.getInt("blacklist"));
            uDTO.setRownum(rs.getString("elrow"));
            uDTO.setatmapplicationid(rs.getString("atm_application_id"));

            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
            amDTO.setId(rs.getInt("atm_id"));
            uDTO.setatmid(amDTO);

            uDTO.setCardno(rs.getString("card_no_decrypt"));
            uDTO.setcardnosuffix(rs.getString("card_no_suffix"));
            uDTO.setatmname(rs.getString("atmname"));
            uDTO.setColumn1(rs.getString("column1"));
            uDTO.setColumn2(rs.getString("column2"));
            uDTO.setColumn3(rs.getString("column3"));
            uDTO.setColumn4(rs.getString("column4"));
            uDTO.setColumn5(rs.getString("column5"));
            uDTO.setComments(rs.getString("comments"));
            uDTO.setCurrency(rs.getString("currency"));

            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
            cmDTO.setId((rs.getInt("currency_id")));
            uDTO.setCurrencyid(cmDTO);

            uDTO.setCustomeraccountnumber(rs.getString("customer_account_number"));
            uDTO.setFileid(rs.getInt("file_id"));
            uDTO.setLoadingdate(rs.getTimestamp("loading_date"));
            uDTO.setMatchby(rs.getInt("match_by"));
            uDTO.setmatchdate(rs.getTimestamp("match_date"));
            uDTO.setMatchkey(rs.getInt("match_key"));
            uDTO.setMatchingtype(rs.getInt("matching_type"));
            uDTO.setNotespresented(rs.getString("notes_presented"));
            uDTO.setRecordtype(rs.getInt("record_type"));
            uDTO.setResponsecode(rs.getString("response_code"));

            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
            trcDTO.setId((rs.getInt("response_code_id")));
            uDTO.setResponsecodeid(trcDTO);

            uDTO.setResponsecodemaster(rs.getInt("response_code_master"));
            uDTO.setSettleddate(rs.getTimestamp("settled_date"));
            uDTO.setSettledflag(rs.getInt("settled_flag"));

            UsersDTOInter userDTO = DTOFactory.createUsersDTO();
            userDTO.setUserId((rs.getInt("settled_user")));
            uDTO.setSettleduser(userDTO);

            uDTO.setSettlementdate(rs.getTimestamp("settlement_date"));
            uDTO.setTransactiondate(rs.getTimestamp("transaction_date"));
            uDTO.setTransactionsequence(rs.getString("transaction_sequence"));
            uDTO.setTransactionsequenceorderby(rs.getInt("transaction_sequence_order_by"));
            uDTO.setTransactionstatus(rs.getString("transaction_status"));

            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
            tsDTO.setId((rs.getInt("transaction_status_id")));
            uDTO.setTransactionstatusid(tsDTO);

            uDTO.setTransactiontime(rs.getTimestamp("transaction_time"));
            uDTO.setTransactiontype(rs.getString("transaction_type"));

            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
            ttDTO.setId((rs.getInt("transaction_type_id")));
            uDTO.setTransactiontypeid(ttDTO);

            uDTO.setTransactiontypemaster(rs.getInt("transaction_type_master"));

            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        Object[] results = new Object[2];
        results[0] = uDTOL;
        results[1] = fullamount;
        return results;
    }

    public Object findTotalAmount(String whereClause, String temp) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select  sum(disputes.amount) from $table disputes where $temp $whereClause";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$whereClause", "" + whereClause);
        selectStat = selectStat.replace("$temp", "" + temp);
        //selectStat = selectStat.replace("$field", "" + field);
        ResultSet rs = executeQuery(selectStat);
        BigDecimal amount = BigDecimal.valueOf(0);

        while (rs.next()) {
            amount = rs.getBigDecimal(1);
        }
        super.postSelect(rs);
        return amount;
    }

    public Object countMatched(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        int userId = (Integer) obj[0];
        String selectStat = "select count(*)from matched_data m"
                + " where m.transaction_date between trunc(sysdate) - 1 and sysdate"
                + " and m.atm_id in (select a.atm_id from user_atm a where a.user_id = $user)";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$user", "" + userId);
        ResultSet rs = executeQuery(selectStat);
        int count = 0;
        while (rs.next()) {
            count = rs.getInt(1);
        }
        super.postSelect(rs);
        return count;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public static void main(String[] args) throws Throwable {
        MatchedDataDAOInter mDAO = DAOFactory.createMatchedDataDAO(null);
        int mDTOL = (Integer) mDAO.countMatched(1);

    }
}
