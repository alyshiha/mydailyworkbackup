/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.RepPopDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentDetailDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface ReplanishmentMasterDAOInter {

    Object replanishmentCardTaked(String whereClause) throws Throwable;

    Object replanishmentCardTakedExcel(String whereClause) throws Throwable;

    Object replanishmentHistoryRepexcel(String whereClause) throws Throwable;

    void save(List<ReplanishmentMasterDTOInter> entities) throws SQLException, Throwable;

    Object replanishmentHistoryRep(String whereClause) throws Throwable;

    Object findReport(int id) throws Throwable;

    Object customSearch(String whereClause, Integer uname) throws Throwable;
Object recalcall(int id) throws Throwable;
    Object recalc(ReplanishmentMasterDTOInter id) throws Throwable;

    Object delete(Object... obj) throws Throwable;

    Object find(Object... obj) throws Throwable;

    Object findAll() throws Throwable;

    RepPopDTOInter replanishmentPopUp(ReplanishmentMasterDTOInter id) throws Throwable;

    Object findLastDate(int atmId) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;
    
    Object update2(Object... obj) throws Throwable;

    Object findLastDateForRep(int atmId, Date dateF) throws Throwable;

}
