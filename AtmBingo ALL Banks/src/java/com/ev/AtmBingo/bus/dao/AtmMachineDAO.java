/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmMachineDAO extends BaseDAO implements AtmMachineDAOInter {

    protected AtmMachineDAO() {
        super();
        super.setTableName("atm_machine");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmMachineDTOInter machine = (AtmMachineDTOInter) obj[0];
        machine.setId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into " + super.getTableName() + " values($id, $atm_group, '$application_id',"
                + " '$name', '$location', $machine_type, '$unit_number', '$serial_no', '$application_id2')";
        insertStat = insertStat.replace("$id", "" + machine.getId());

        insertStat = insertStat.replace("$atm_group", "" + machine.getAtmGroup().getId());
        insertStat = insertStat.replace("$application_id", "" + machine.getApplicationId());
        insertStat = insertStat.replace("$name", "" + machine.getName());
        insertStat = insertStat.replace("$location", "" + machine.getLocation());
        insertStat = insertStat.replace("$machine_type", "" + machine.getMachineType());
        insertStat = insertStat.replace("$unit_number", "" + machine.getUnitNumber());
        insertStat = insertStat.replace("$serial_no", "" + machine.getSerialNo());
        insertStat = insertStat.replace("$application_id2", "" + machine.getApplicationId2());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;

    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmMachineDTOInter machine = (AtmMachineDTOInter) obj[0];
        String updateStat = "update " + super.getTableName() + " set atm_group = $atm_group, application_id = '$application_id', "
                + "name = '$name', location = '$location', machine_type = $machine_type, unit_number = '$unit_number', "
                + "serial_no = '$serial_no', application_id2 = '$app_id2' where id = $id ";
        updateStat = updateStat.replace("$id", "" + machine.getId());
        updateStat = updateStat.replace("$atm_group", "" + machine.getAtmGroup().getId());
        updateStat = updateStat.replace("$application_id", "" + machine.getApplicationId());
        updateStat = updateStat.replace("$name", "" + machine.getName());
        updateStat = updateStat.replace("$location", "" + machine.getLocation());
        updateStat = updateStat.replace("$machine_type", "" + machine.getMachineType());
        updateStat = updateStat.replace("$unit_number", "" + machine.getUnitNumber());
        updateStat = updateStat.replace("$serial_no", "" + machine.getSerialNo());
        updateStat = updateStat.replace("$app_id2", "" + machine.getApplicationId2());
        super.executeUpdate(updateStat);
        super.postUpdate("Update " + machine.getApplicationId() + " ATM", false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmMachineDTOInter machineType = (AtmMachineDTOInter) obj[0];
        String deleteStat = "delete from " + super.getTableName()
                + "where id = " + machineType.getId();
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer matchingType = (Integer) obj[0];
        String selectStat = "select * from " + super.getTableName()
                + " where id = " + matchingType.intValue() + " and  atm_valid(id)= 1";
        ResultSet result = executeQuery(selectStat);
        AtmMachineDTOInter mt = DTOFactory.createAtmMachineDTO();
        while (result.next()) {
            mt.setId(result.getInt("id"));
            mt.setApplicationId(result.getString("application_id"));
            mt.setApplicationId2(result.getString("application_id2"));
            AtmGroupDTOInter agDTO = DTOFactory.createAtmGroupDTO();
            agDTO.setId(result.getInt("atm_group"));
            mt.setAtmGroup(agDTO);
            mt.setLocation(result.getString("location"));
            mt.setMachineType(result.getInt("machine_type"));
            mt.setName(result.getString("name"));
            mt.setSerialNo(result.getString("serial_no"));
            mt.setUnitNumber(result.getString("unit_number"));
        }
        super.postSelect(result);
        return mt;
    }

    public Object findByAtmId(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String matchingType = (String) obj[0];
        String selectStat = "select * from " + super.getTableName()
                + " where application_id like '" + matchingType + "' and atm_valid(id) = 1 order by application_id";
        ResultSet result = executeQuery(selectStat);
        AtmMachineDTOInter mt = DTOFactory.createAtmMachineDTO();
        while (result.next()) {
            mt.setId(result.getInt("id"));
            mt.setApplicationId(result.getString("application_id"));
            mt.setApplicationId2(result.getString("application_id2"));
            AtmGroupDTOInter agDTO = DTOFactory.createAtmGroupDTO();
            agDTO.setId(result.getInt("atm_group"));
            mt.setAtmGroup(agDTO);
            mt.setLocation(result.getString("location"));
            mt.setMachineType(result.getInt("machine_type"));
            mt.setName(result.getString("name"));
            mt.setSerialNo(result.getString("serial_no"));
            mt.setUnitNumber(result.getString("unit_number"));
        }
        super.postSelect(result);
        return mt;
    }
     public Object findByAtmGrp3(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer userId = (Integer) obj[0];
        String selectStat = "";
        
            selectStat = "select * from " + super.getTableName()
                    + " where id in (select atm_id from user_atm where user_id = " + userId + ") and atm_valid(id) = 1"
                    + " order by application_id";
        
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineDTOInter> mtL = new ArrayList<AtmMachineDTOInter>();
        while (result.next()) {
            AtmMachineDTOInter mt = DTOFactory.createAtmMachineDTO();
            mt.setId(result.getInt("id"));
            mt.setApplicationId(result.getString("application_id"));
            mt.setApplicationId2(result.getString("application_id2"));
            AtmGroupDTOInter agDTO = DTOFactory.createAtmGroupDTO();
            agDTO.setId(result.getInt("atm_group"));
            mt.setAtmGroup(agDTO);
            mt.setLocation(result.getString("location"));
            mt.setMachineType(result.getInt("machine_type"));
            mt.setName(result.getString("name"));
            mt.setSerialNo(result.getString("serial_no"));
            mt.setUnitNumber(result.getString("unit_number"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

  public Object findByAtmGrp2(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer grpId = (Integer) obj[0];
        Integer userId = (Integer) obj[1];
        String selectStat = "";
        if (grpId != 0) {
            selectStat = "select * from " + super.getTableName()
                    + " where id IN (select id from atm_machine where atm_group in (select id from atm_group where parent_id in (select id from atm_group where (parent_id = " + grpId  + " or id = " + grpId  + ")) or id = " + grpId  + "))"
                    + " and id in (select atm_id from user_atm where user_id = " + userId + ") and atm_valid(id) = 1 and cassette_group is not null"
                    + " order by application_id";
        } else {
            selectStat = "select * from " + super.getTableName()
                    + " where id in (select atm_id from user_atm where user_id = " + userId + ") and atm_valid(id) = 1 and cassette_group is not null"
                    + " order by application_id";
        }
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineDTOInter> mtL = new ArrayList<AtmMachineDTOInter>();
        while (result.next()) {
            AtmMachineDTOInter mt = DTOFactory.createAtmMachineDTO();
            mt.setId(result.getInt("id"));
            mt.setApplicationId(result.getString("application_id"));
            mt.setApplicationId2(result.getString("application_id2"));
            AtmGroupDTOInter agDTO = DTOFactory.createAtmGroupDTO();
            agDTO.setId(result.getInt("atm_group"));
            mt.setAtmGroup(agDTO);
            mt.setLocation(result.getString("location"));
            mt.setMachineType(result.getInt("machine_type"));
            mt.setName(result.getString("name"));
            mt.setSerialNo(result.getString("serial_no"));
            mt.setUnitNumber(result.getString("unit_number"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object findByAtmGrp(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer grpId = (Integer) obj[0];
        Integer userId = (Integer) obj[1];
        String selectStat = "";
        if (grpId != 0) {
            selectStat = "select * from " + super.getTableName()
                    + " where id IN (select id from atm_machine where atm_group in (select id from atm_group where parent_id in (select id from atm_group where (parent_id = " + grpId  + " or id = " + grpId  + ")) or id = " + grpId  + "))"
                    + " and id in (select atm_id from user_atm where user_id = " + userId + ") and atm_valid(id) = 1"
                    + " order by application_id";
        } else {
            selectStat = "select * from " + super.getTableName()
                    + " where id in (select atm_id from user_atm where user_id = " + userId + ") and atm_valid(id) = 1"
                    + " order by application_id";
        }
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineDTOInter> mtL = new ArrayList<AtmMachineDTOInter>();
        while (result.next()) {
            AtmMachineDTOInter mt = DTOFactory.createAtmMachineDTO();
            mt.setId(result.getInt("id"));
            mt.setApplicationId(result.getString("application_id"));
            mt.setApplicationId2(result.getString("application_id2"));
            AtmGroupDTOInter agDTO = DTOFactory.createAtmGroupDTO();
            agDTO.setId(result.getInt("atm_group"));
            mt.setAtmGroup(agDTO);
            mt.setLocation(result.getString("location"));
            mt.setMachineType(result.getInt("machine_type"));
            mt.setName(result.getString("name"));
            mt.setSerialNo(result.getString("serial_no"));
            mt.setUnitNumber(result.getString("unit_number"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object findByAtmbranch(int logedUser) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        
        String selectStat = "";
      
            selectStat = "select * from " + super.getTableName()
                    + " where id in (select b.atmid from branch_atm b where b.userid = $userid) and atm_valid(id) = 1"
                    + " order by application_id";
        selectStat = selectStat.replace("$userid", ""+logedUser);
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineDTOInter> mtL = new ArrayList<AtmMachineDTOInter>();
        while (result.next()) {
            AtmMachineDTOInter mt = DTOFactory.createAtmMachineDTO();
            mt.setId(result.getInt("id"));
            mt.setApplicationId(result.getString("application_id"));
            mt.setApplicationId2(result.getString("application_id2"));
            AtmGroupDTOInter agDTO = DTOFactory.createAtmGroupDTO();
            agDTO.setId(result.getInt("atm_group"));
            mt.setAtmGroup(agDTO);
            mt.setLocation(result.getString("location"));
            mt.setMachineType(result.getInt("machine_type"));
            mt.setName(result.getString("name"));
            mt.setSerialNo(result.getString("serial_no"));
            mt.setUnitNumber(result.getString("unit_number"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object findAll(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from " + super.getTableName() + " and atm_valid(id) = 1 order by application_id";
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineDTOInter> mtL = new ArrayList<AtmMachineDTOInter>();
        while (result.next()) {
            AtmMachineDTOInter mt = DTOFactory.createAtmMachineDTO();
            mt.setId(result.getInt("id"));
            mt.setApplicationId(result.getString("application_id"));
            mt.setApplicationId2(result.getString("application_id2"));
            AtmGroupDTOInter agDTO = DTOFactory.createAtmGroupDTO();
            agDTO.setId(result.getInt("atm_group"));
            mt.setAtmGroup(agDTO);
            mt.setLocation(result.getString("location"));
            mt.setMachineType(result.getInt("machine_type"));
            mt.setName(result.getString("name"));
            mt.setSerialNo(result.getString("serial_no"));
            mt.setUnitNumber(result.getString("unit_number"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object deleteNewAtms(int logedUser) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String deleteStat = "delete from ATM_USER_NOTIFICATION"
                + " where user_id = $userId";
        deleteStat = deleteStat.replace("$userId", "" + logedUser);
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object findNewAtms(int logedinUser) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select m.name||'-'|| m.application_id  from ATM_USER_NOTIFICATION n,atm_machine m  "
                + "where user_id = $userId  and m.id = n.atm_id order by m.application_id";
        selectStat = selectStat.replace("$userId", "" + logedinUser);
        ResultSet result = executeQuery(selectStat);
        List<String> mtL = new ArrayList<String>();
        while (result.next()) {
            String mt = "";
            mt = result.getString(1);
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object findRecordGroup(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        UsersDTOInter loggedinUser = (UsersDTOInter) obj[0];
        String selectStat = "select * from " + super.getTableName() + " where id in "
                + "(select atm_id from user_atm where user_id = $user) and atm_valid(id) = 1 order by name";
        selectStat = selectStat.replace("$user", "" + loggedinUser.getUserId());
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineDTOInter> mtL = new ArrayList<AtmMachineDTOInter>();
        while (result.next()) {
            AtmMachineDTOInter mt = DTOFactory.createAtmMachineDTO();
            mt.setId(result.getInt("id"));
            mt.setApplicationId(result.getString("application_id"));
            mt.setApplicationId2(result.getString("application_id2"));
            AtmGroupDTOInter agDTO = DTOFactory.createAtmGroupDTO();
            agDTO.setId(result.getInt("atm_group"));
            mt.setAtmGroup(agDTO);
            mt.setLocation(result.getString("location"));
            mt.setMachineType(result.getInt("machine_type"));
            mt.setName(result.getString("name"));
            mt.setSerialNo(result.getString("serial_no"));
            mt.setUnitNumber(result.getString("unit_number"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }
  public Object findRecordOutLic(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from " + super.getTableName()
                + "  order by name";
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineDTOInter> mtL = new ArrayList<AtmMachineDTOInter>();
        while (result.next()) {
            AtmMachineDTOInter mt = DTOFactory.createAtmMachineDTO();
            mt.setId(result.getInt("id"));
            mt.setApplicationId(result.getString("application_id"));
            mt.setApplicationId2(result.getString("application_id2"));
            AtmGroupDTOInter agDTO = DTOFactory.createAtmGroupDTO();
            agDTO.setId(result.getInt("atm_group"));
            mt.setAtmGroup(agDTO);
            mt.setLocation(result.getString("location"));
            mt.setMachineType(result.getInt("machine_type"));
            mt.setName(result.getString("name"));
            mt.setSerialNo(result.getString("serial_no"));
            mt.setUnitNumber(result.getString("unit_number"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }
    public Object findRecordGroupAll(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        UsersDTOInter loggedinUser = (UsersDTOInter) obj[0];
        String selectStat = "select * from " + super.getTableName() + " where "
                + " atm_valid(id) = 1 order by name";
        selectStat = selectStat.replace("$user", "" + loggedinUser.getUserId());
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineDTOInter> mtL = new ArrayList<AtmMachineDTOInter>();
        while (result.next()) {
            AtmMachineDTOInter mt = DTOFactory.createAtmMachineDTO();
            mt.setId(result.getInt("id"));
            mt.setApplicationId(result.getString("application_id"));
            mt.setApplicationId2(result.getString("application_id2"));
            AtmGroupDTOInter agDTO = DTOFactory.createAtmGroupDTO();
            agDTO.setId(result.getInt("atm_group"));
            mt.setAtmGroup(agDTO);
            mt.setLocation(result.getString("location"));
            mt.setMachineType(result.getInt("machine_type"));
            mt.setName(result.getString("name"));
            mt.setSerialNo(result.getString("serial_no"));
            mt.setUnitNumber(result.getString("unit_number"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

     public Object findATMMachineGroupCount (Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "select Count(t.ID) as count,g.name as name from atm_machine t,atm_group g where g.id = t.atm_group and  atm_valid(t.id)= 1  group by g.name";
        ResultSet result = executeQuery(selectStat);
        String Res = "";
        while (result.next()) {
            Res = Res + result.getString("name").toString() + ":" + result.getString("count").toString() + ",";
        }
        super.postSelect(result);
        return Res;
    }

       public List<String> findUserUnAssignMachines(Integer loggedinUser,Integer Cassettegroup) throws Throwable {
        super.preSelect();
        String selectStat = "select m.name || ' --- ' || a.name || ' --- ' || m.application_id as Value from atm_machine m,atm_group a where m.id in(select atm_id from user_atm where user_id = $user) and atm_valid(m.id) = 1 and a.id = m.atm_group   and m.cassette_group is null  order by  m.atm_group,m.application_id";
        selectStat = selectStat.replace("$user", "" + loggedinUser);
        ResultSet result = executeQuery(selectStat);
        List<String> Values = new ArrayList<String>();
        while (result.next()) {
            Values.add(result.getString("Value"));
        }
        super.postSelect(result);
        return Values;
    }

   public List<String> findUserUnAssign(Integer loggedinUser,Integer Cassettegroup,int status,Integer repgroup) throws Throwable {
        super.preSelect();
        String selectStat = "";
        if(status == 1){
         selectStat = "select m.name || ' --- ' || a.name || ' --- ' || m.application_id as Value from atm_machine m,atm_group a where m.id in(select atm_id from user_atm where user_id = $user) and atm_valid(m.id) = 1 and a.id = m.atm_group $casseteGroup  $repgroup   and m.cassette_group is null  order by  m.atm_group,m.application_id";}
        if(status == 0){
         selectStat = "select m.name || ' --- ' || a.name || ' --- ' || m.application_id as Value from atm_machine m,atm_group a where m.id in(select atm_id from user_atm where user_id = $user) and atm_valid(m.id) = 1 and a.id = m.atm_group $casseteGroup  $repgroup   and m.cassette_group is not null  order by  m.atm_group,m.application_id";}
        selectStat = selectStat.replace("$user", "" + loggedinUser);
        if(Cassettegroup != 0){
            if(Cassettegroup != -1){
        selectStat = selectStat.replace("$casseteGroup", "" + "  AND m.ID IN\n" +
"                    (SELECT   id\n" +
"                       FROM   atm_machine\n" +
"                      WHERE   atm_group IN\n" +
"                                    (SELECT   id\n" +
"                                       FROM   atm_group\n" +
"                                      WHERE   parent_id IN\n" +
"                                                    (SELECT   id\n" +
"                                                       FROM   atm_group\n" +
"                                                      WHERE   (parent_id = "+Cassettegroup+" \n" +
"                                                               OR id = "+Cassettegroup+" ))\n" +
"                                              OR id = "+Cassettegroup+") )" );}else
        {selectStat = selectStat.replace("$casseteGroup", "");}}
        else
        {selectStat = selectStat.replace("$casseteGroup", "");}
             if(repgroup != 0){
        selectStat = selectStat.replace("$repgroup", "" + " and m.cassette_group = '"+repgroup+"' " );}
        else
        {selectStat = selectStat.replace("$repgroup", "");}

        ResultSet result = executeQuery(selectStat);
        List<String> Values = new ArrayList<String>();
        while (result.next()) {
            Values.add(result.getString("Value"));
        }
        super.postSelect(result);
        return Values;
    }
  public List<String> findUserCassetteMachines(Integer loggedinUser,Integer Cassettegroup) throws Throwable {
        super.preSelect();
        String selectStat = "select m.name || ' --- ' || a.name || ' --- ' || m.application_id as Value from atm_machine m,atm_group a where m.id in(select atm_id from user_atm where user_id = $user) and atm_valid(m.id) = 1 and a.id = m.atm_group $groupCond  order by  m.atm_group,m.application_id";
        if (Cassettegroup > 0) {
            selectStat = selectStat.replace("$groupCond", "and m.cassette_group = " + Cassettegroup);
        } else {
            selectStat = selectStat.replace("$groupCond", "");
        }
        selectStat = selectStat.replace("$user", "" + loggedinUser);
        ResultSet result = executeQuery(selectStat);
        List<String> Values = new ArrayList<String>();
        while (result.next()) {
            Values.add(result.getString("Value"));
        }
        super.postSelect(result);
        return Values;
    }

         public List<String> findNotUserCassetteMachines(Integer loggedinUser,Integer Cassettegroup) throws Throwable {
        super.preSelect();
        String selectStat = "select m.name || ' --- ' || a.name || ' --- ' || m.application_id as Value from atm_machine m,atm_group a where m.id not in(select atm_id from user_atm where user_id = $user) and atm_valid(m.id) = 1 and a.id = m.atm_group $groupCond order by m.atm_group,m.application_id";
        if (Cassettegroup > 0) {
            selectStat = selectStat.replace("$groupCond", "and m.cassette_group = " + Cassettegroup);
        } else {
            selectStat = selectStat.replace("$groupCond", "");
        }
        selectStat = selectStat.replace("$user", "" + loggedinUser);
        ResultSet result = executeQuery(selectStat);
        List<String> Values = new ArrayList<String>();
        while (result.next()) {
            Values.add(result.getString("Value"));
        }
        super.postSelect(result);
        return Values;
    }



       public List<String> findUserMachines(Integer loggedinUser,Integer group) throws Throwable {
        super.preSelect();
        String selectStat = "select m.name || ' --- ' || a.name || ' --- ' || m.application_id as Value from atm_machine m,atm_group a where m.id in(select atm_id from user_atm where user_id = $user) and atm_valid(m.id) = 1 and a.id = m.atm_group $groupCond  order by  m.atm_group,m.application_id";
        if (group > 0) {
             selectStat = selectStat.replace("$groupCond", " and m.id in (select id from atm_machine where atm_group in"
                    + "(select id from  (select * from atm_group l order BY l.parent_id,name) "
                    + "start with id = "+group+" connect by prior id = parent_id)) " );
        } else {
            selectStat = selectStat.replace("$groupCond", "");
        }
        selectStat = selectStat.replace("$user", "" + loggedinUser);
        ResultSet result = executeQuery(selectStat);
        List<String> Values = new ArrayList<String>();
        while (result.next()) {
            Values.add(result.getString("Value"));
        }
        super.postSelect(result);
        return Values;
    }
         public List<String> findNotUserMachines(Integer loggedinUser,Integer group) throws Throwable {
        super.preSelect();
        String selectStat = "select m.name || ' --- ' || a.name || ' --- ' || m.application_id as Value from atm_machine m,atm_group a where m.id not in(select atm_id from user_atm where user_id = $user) and atm_valid(m.id) = 1 and a.id = m.atm_group $groupCond order by m.atm_group,m.application_id";
        if (group > 0) {
             selectStat = selectStat.replace("$groupCond", " and m.id in (select id from atm_machine where atm_group in"
                    + "(select id from  (select * from atm_group l order BY l.parent_id,name) "
                    + "start with id = "+group+" connect by prior id = parent_id)) " );
        } else {
            selectStat = selectStat.replace("$groupCond", "");
        }
        selectStat = selectStat.replace("$user", "" + loggedinUser);
        ResultSet result = executeQuery(selectStat);
        List<String> Values = new ArrayList<String>();
        while (result.next()) {
            Values.add(result.getString("Value"));
        }
        super.postSelect(result);
        return Values;
    }

         
           public List<String> findByGrp(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer grpId = (Integer) obj[0];
        String selectStat = "select m.name || ' --- ' || a.name || ' --- ' || m.application_id as Value from atm_machine m,atm_group a where   atm_valid(m.id) = 1 and a.id = m.atm_group $groupCond order by m.atm_group,m.application_id ";
        if (grpId > 0) {
            selectStat = selectStat.replace("$groupCond", " and m.id in (select id from atm_machine where atm_group in"
                    + "(select id from  (select * from atm_group l order BY l.parent_id,name) "
                    + "start with id = "+grpId+" connect by prior id = parent_id)) " );
        } else {
            selectStat = selectStat.replace("$groupCond", "");
        }
        ResultSet result = executeQuery(selectStat);
        List<String> Values = new ArrayList<String>();
        while (result.next()) {
             Values.add(result.getString("Value"));
        }
        super.postSelect(result);
        return Values;
    }


    public Object findMachinesBelongUser(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        int loggedinUser = (Integer) obj[0];
        int group = (Integer) obj[1];
        String selectStat = "select * from $table where id in(select atm_id from user_atm where user_id = $user) and atm_valid(id) = 1 $groupCond"
                + "  order by atm_group,application_id ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$user", "" + loggedinUser);
        if (group > 0) {
             selectStat = selectStat.replace("$groupCond", " and id in (select id from atm_machine where atm_group in"
                    + "(select id from  (select * from atm_group l order BY l.parent_id,name) "
                    + "start with id = "+group+" connect by prior id = parent_id)) " );
        } else {
            selectStat = selectStat.replace("$groupCond", "");
        }
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineDTOInter> mtL = new ArrayList<AtmMachineDTOInter>();
        while (result.next()) {
            AtmMachineDTOInter mt = DTOFactory.createAtmMachineDTO();
            mt.setId(result.getInt("id"));
            mt.setApplicationId(result.getString("application_id"));
            mt.setApplicationId2(result.getString("application_id2"));

            AtmGroupDTOInter agDTO = DTOFactory.createAtmGroupDTO();
            agDTO.setId(result.getInt("atm_group"));

            mt.setAtmGroup(agDTO);
            mt.setLocation(result.getString("location"));
            mt.setMachineType(result.getInt("machine_type"));
            mt.setName(result.getString("name"));
            mt.setSerialNo(result.getString("serial_no"));
            mt.setUnitNumber(result.getString("unit_number"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object findMachinesNotBelongUser(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        int loggedinUser = (Integer) obj[0];
        int group = (Integer) obj[1];
        String selectStat = "select * from $table where id not in(select atm_id from user_atm where user_id = $user) and atm_valid(id) = 1"
                + " $groupCond order by atm_group,application_id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$user", "" + loggedinUser);
        if (group > 0) {
             selectStat = selectStat.replace("$groupCond", " and id in (select id from atm_machine where atm_group in"
                    + "(select id from  (select * from atm_group l order BY l.parent_id,name) "
                    + "start with id = "+group+" connect by prior id = parent_id)) " );
        } else {
            selectStat = selectStat.replace("$groupCond", "");
        }
        ResultSet result = executeQuery(selectStat);
        List<AtmMachineDTOInter> mtL = new ArrayList<AtmMachineDTOInter>();
        while (result.next()) {
            AtmMachineDTOInter mt = DTOFactory.createAtmMachineDTO();
            mt.setId(result.getInt("id"));
            mt.setApplicationId(result.getString("application_id"));
            mt.setApplicationId2(result.getString("application_id2"));

            AtmGroupDTOInter agDTO = DTOFactory.createAtmGroupDTO();
            agDTO.setId(result.getInt("atm_group"));
            mt.setAtmGroup(agDTO);
            mt.setLocation(result.getString("location"));
            mt.setMachineType(result.getInt("machine_type"));
            mt.setName(result.getString("name"));
            mt.setSerialNo(result.getString("serial_no"));
            mt.setUnitNumber(result.getString("unit_number"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }
}
