/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.BingoLogDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.NotificationDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Administrator
 */
public class NotificationDAO extends BaseDAO implements NotificationDAOInter{

    protected NotificationDAO() {
        super();
        super.setTableName("NOTIFICATIONS");
    }

    public Object findAllNotification(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select id, text, created_date, user_id, type from " + super.getTableName() + " Where type = 2 AND CREATED_DATE > (SYSDATE - 7)";
        ResultSet result = executeQuery(selectStat);
        List<NotificationDTOInter> Notifications = new ArrayList<NotificationDTOInter>();
        while (result.next()) {
            NotificationDTOInter Notification = DTOFactory.createNotificationDTO();
            Notification.setAction(result.getString("text"));
            Notification.setId(result.getInt("id"));
            Notification.setActionDate(result.getTimestamp("created_date"));
            Notification.setType(result.getInt("type"));
            Notification.setUserId(result.getInt("user_id"));
            Notifications.add(Notification);
        }
        super.postSelect(result);
        return Notifications;
    }

    public Object findAllError(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select id, text, created_date, user_id, type from " + super.getTableName() + " Where type = 1";
        ResultSet result = executeQuery(selectStat);
        List<NotificationDTOInter> Notifications = new ArrayList<NotificationDTOInter>();
        while (result.next()) {
            NotificationDTOInter Notification = DTOFactory.createNotificationDTO();
            Notification.setAction(result.getString("text"));
            Notification.setId(result.getInt("id"));
            Notification.setActionDate(result.getTimestamp("created_date"));
            Notification.setType(result.getInt("type"));
            Notification.setUserId(result.getInt("user_id"));
            Notifications.add(Notification);
        }
        super.postSelect(result);
        return Notifications;
    }
}
