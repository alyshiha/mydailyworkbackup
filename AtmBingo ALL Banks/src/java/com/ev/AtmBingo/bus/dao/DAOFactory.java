/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

/**
 * createuseratmbranchDAO
 *
 * @author Administrator
 */
public class DAOFactory {

    public static StatementDAOInter createStatementDAO(Object... obj) {
        return new StatementDAO();
    }

    public static DepositTransactionDAOInter createDepositTransactionDAO(Object... obj) {
        return new DepositTransactionDAO();
    }

    public static HolidaysDAOInter_1 createHolidaysDAO_1(Object... obj) {
        return new HolidaysDAO_1();
    }

    public static invalidReplanishmentsDAOInter createinvalidReplanishmentsDAO(Object... obj) {
        return new invalidReplanishmentsDAO();
    }

    public static CashManagmentLogDAOInter createCashManagmentLogDAO(Object... obj) {
        return new CashManagmentLogDAO();
    }

    public static userPrivilegeDAOInter createuserPrivilegeDAO(Object... obj) {
        return new userPrivilegeDAO();
    }

    public static privilegeStatusNameDAOInter createprivilegeStatusName(Object... obj) {
        return new privilegeStatusNameDAO();
    }

    public static AccountNameDAOInter createAccountNameDAO(Object... obj) {
        return new AccountNameDAO();
    }

    public static matchingErrorsDataDAOInter creatematchingErrorsDataDAO(Object... obj) {
        return new matchingErrorsDataDAO();
    }

    public static LogErrorDAOInter createLogErrorDAO(Object... obj) {
        return new LogErrorDAO();
    }

    public static replanishmentCashManagementDAOInter createreplanishmentCashManagementDAO(Object... obj) {
        return new replanishmentCashManagementDAO();
    }

    public static AtmJournalDAOInter createAtmJournalDAO(Object... obj) {
        return new AtmJournalDAO();
    }

    public static AtmAccountsDAOInter createAtmAccountsDAO(Object... obj) {
        return new AtmAccountsDAO();
    }

    public static RepStatDAOInter createRepStatDAO(Object... obj) {
        return new RepStatDAO();
    }

    public static NetworkTypeCodeDAOInter createNetworkTypeCodeDAO(Object... obj) {
        return new NetworkTypeCodeDAO();
    }

    public static NetworksDAOInter createNetworksDAO(Object... obj) {
        return new NetworksDAO();
    }
//

    public static bybranchreportDAOInter createbybranchreportDAO(Object... obj) {
        return new bybranchreportDAO();
    }

    public static NotePresentedDAOInter createNotePresentedDAO(Object... obj) {
        return new NotePresentedDAO();
    }

    public static allbrachtransactionssearchDAOInter createallbrachtransactionssearchDAO(Object... obj) {
        return new allbrachtransactionssearchDAO();
    }

    public static BranchReasonDAOInter createbranchreasonDAO(Object... obj) {
        return new BranchReasonDAO();
    }

    public static branchcodeDAOInter createbranchcodeDAO(Object... obj) {
        return new branchcodeDAO();
    }

    public static allbranchtransactionsDAOInter createallbranchtransactionsDAO(Object... obj) {
        return new allbranchtransactionsDAO();
    }

    public static useratmbranchDAOInter createuseratmbranchDAO(Object... obj) {
        return new useratmbranchDAO();
    }

    public static usersbranchDAOInter createusersbranchDAO(Object... obj) {
        return new usersbranchDAO();
    }

    public static BingoStatDAOInter createBingoStatDAO(Object... obj) {
        return new BingoStatDAO();
    }

    public static menulabelsDAOInter createmenulabelsDAO(Object... obj) {
        return new menulabelsDAO();
    }

    public static AtmtemplateDAOInter createAtmtemplateDAO(Object... obj) {
        return new AtmtemplateDAO();
    }

    public static FileViewDAOInter createFileViewDAO(Object... obj) {
        return new FileViewDAO();
    }

    public static EXPORTTEMPLETEDAOInter createEXPORTTEMPLETEDAO(Object... obj) {
        return new EXPORTTEMPLETEDAO();
    }

    public static ExportTemplateDetailDAOInter createExportTemplateDetailDAO(Object... obj) {
        return new ExportTemplateDetailDAO();
    }

    public static ExportLogDAOInter createExportLogDAO(Object... obj) {
        return new ExportLogDAO();
    }

    public static CorrectiveEntryLogDAOInter createCorrectiveEntryLogDAO(Object... obj) {
        return new CorrectiveEntryLogDAO();
    }

    public static AutoRepAtmToGroupDAOInter createAutoRepAtmToGroupDAO(Object... obj) {
        return new AutoRepAtmToGroupDAO();
    }

    public static DeleteTransactionsDataDAOInter createDeleteTransactionsDataDAO(Object... obj) {
        return new DeleteTransactionsDataDAO();
    }

    public static PendingAtmsDAOInter CreatePendingAtmsDAO(Object... obj) {
        return new PendingAtmsDAO();
    }

    public static ATMMAchineLICDAOInter CreateATMMAchineLICDAO(Object... obj) {
        return new ATMMAchineLICDAO();
    }

    public static AtmGroupEditorDaoInter CreateAtmGroupEditorDao(Object... obj) {
        return new AtmGroupEditorDao();
    }

    public static LicenseDAOInter createLicenseDAO(Object... obj) {
        return new LicenseDAO();
    }

    public static SettlementLogDAOInter createSettlementLogDAO(Object... obj) {
        return new SettlementLogDAO();
    }

    public static BingoLogDAOInter createBingoLogDAO(Object... obj) {
        return new BingoLogDAO();
    }

    public static NotificationDAOInter createNotificationDAO(Object... obj) {
        return new NotificationDAO();
    }

    public static UserPassDAOInter createUserPassDAO(Object... obj) {
        return new UserPassDAO();
    }

    public static ReplanishmentTempletesDetDAOInter createReplanishmentTempletesDetDAO(Object... obj) {
        return new ReplanishmentTempletesDetDAO();
    }

    public static ReplanishmentTempletesDAOInter createReplanishmentTempletesDAO(Object... obj) {
        return new ReplanishmentTempletesDAO();
    }

    public static DefualtColumnsDAOInter createDefualtColumnsDAO(Object obj) {
        return new DefualtColumnsDAO();
    }

    public static ReplanishmentReportDAOInter createReplanishmentReportDAO(Object obj) {
        return new ReplanishmentReportDAO();
    }

    public static ProfileDAOInter createProfileDAO(Object obj) {
        return new ProfileDAO();
    }

    public static UserProfileDAOInter createUserProfileDAO(Object obj) {
        return new UserProfileDAO();
    }

    public static ProfileMenuDAOInter createProfileMenuDAO(Object obj) {
        return new ProfileMenuDAO();
    }

    public static ItemBingoDAOInter createItemBingoDAO(Object obj) {
        return new ItemBingoDAO();
    }

    public static ProfileMenuItemDAOInter createProfileMenuItemDAO(Object obj) {
        return new ProfileMenuItemDAO();
    }

    public static MissingJournalsRepDAOInter createMissingJournalsRepDAO(Object obj) {
        return new MissingJournalsRepDAO();
    }

    public static CardNoTotalRepDAOInter createCardNoTotalRepDAO(Object obj) {
        return new CardNoTotalRepDAO();
    }

    public static BanksTotalRepDAOInter createBanksTotalRepDAO(Object obj) {
        return new BanksTotalRepDAO();
    }

    public static AtmsTotalRepDAOInter createAtmsTotalRepDAO(Object obj) {
        return new AtmsTotalRepDAO();
    }

    public static AtmStatByTransRepDAOInter createAtmStatByTransRepDAO(Object obj) {
        return new AtmStatByTransRepDAO();
    }

    public static AtmStatByCashRepDAOInter createAtmStatByCashRepDAO(Object obj) {
        return new AtmStatByCashRepDAO();
    }

    public static AtmRemainingRepDAOInter createAtmRemainingRepDAO(Object obj) {
        return new AtmRemainingRepDAO();
    }

    public static RejectedReportDAOInter createRejectedReportDAO(Object obj) {
        return new RejectedReportDAO();
    }

    public static DisputeReportDAOInter createDisputeReportDAO(Object obj) {
        return new DisputeReportDAO();
    }

    public static JobsStatusDAOInter createJobsStatusDAO(Object obj) {
        return new JobsStatusDAO();
    }

    public static FilesToMoveDAOInter createFilesToMoveDAO(Object obj) {
        return new FilesToMoveDAO();
    }

    public static UserActivityDAOInter createUserActivityDAO(Object obj) {
        return new UserActivityDAO();
    }

    public static MenuLabelDAOInter createMenuLabelDAO(Object obj) {
        return new MenuLabelDAO();
    }

    public static ReplanishmentDetailDAOInter createReplanishmentDetailDAO(Object obj) {
        return new ReplanishmentDetailDAO();
    }

    public static ReplanishmentMasterDAOInter createReplanishmentMasterDAO(Object obj) {
        return new ReplanishmentMasterDAO();
    }

    public static ValidationRulesDAOInter createValidationRulesDAO(Object obj) {
        return new ValidationRulesDAO();
    }

    public static RejectedTransactionsDAOInter createRejectedTransactionsDAO(Object obj) {
        return new RejectedTransactionsDAO();
    }

    public static DisputesDAOInter createDisputesDAO(Object obj) {
        return new DisputesDAO();
    }

    public static ColumnDAOInter createColumnDAO(Object obj) {
        return new ColumnDAO();
    }

    public static LangDAOInter createLangDAO(Object obj) {
        return new LangDAO();
    }

    public static TransactionTypeMasterDAOInter createTransactionTypeMasterDAO(Object obj) {
        return new TransactionTypeMasterDAO();
    }

    public static UserAtmDAOInter createUserAtmDAO(Object obj) {
        return new UserAtmDAO();
    }

    public static TransactionResponseMasterDAOInter createTransactionResponseMasterDAO(Object obj) {
        return new TransactionResponseMasterDAO();
    }

    public static SwitchFileTemplateDAOInter createSwitchFileTemplateDAO(Object obj) {
        return new SwitchFileTemplateDAO();
    }

    public static SwitchFileTemplateDetailDAOInter createSwitchFileTemplateDetailDAO(Object obj) {
        return new SwitchFileTemplateDetailDAO();
    }

    public static SwitchFileHeaderDAOInter createSwitchFileHeaderDAO(Object obj) {
        return new SwitchFileHeaderDAO();
    }

    public static HostFileTemplateDAOInter createHostFileTemplateDAO(Object obj) {
        return new HostFileTemplateDAO();
    }

    public static HostFileHeaderDAOInter createHostFileHeaderDAO(Object obj) {
        return new HostFileHeaderDAO();
    }

    public static HostFileTemplateDetailDAOInter createHostFileTemplateDetailDAO(Object obj) {
        return new HostFileTemplateDetailDAO();
    }

    public static FileColumnDisplayDetailsDAOInter createFileColumnDisplayDetailsDAO(Object obj) {
        return new FileColumnDisplayDetailsDAO();
    }

    public static FileColumnDisplayDAOInter createFileColumnDisplayDAO(Object obj) {
        return new FileColumnDisplayDAO();
    }

    public static CassetteDAOInter createCassetteDAO(Object obj) {
        return new CassetteDAO();
    }

    public static SystemTableDAOInter createSystemTableDAO(Object obj) {
        return new SystemTableDAO();
    }

    public static BlackListedCardDAOInter createBlackListedCardDAO(Object obj) {
        return new BlackListedCardDAO();
    }

    public static DuplicateTransactionsDAOInter createDuplicateTransactionsDAO(Object obj) {
        return new DuplicateTransactionsDAO();
    }

    public static AtmFileDAOInter createAtmFileDAO(Object obj) {
        return new AtmFileDAO();
    }

    public static AtmFileHeaderDAOInter createAtmFileHeaderDAO(Object obj) {
        return new AtmFileHeaderDAO();
    }

    public static AtmFileTemplateDetailDAOInter createAtmFileTemplateDetailDAO(Object obj) {
        return new AtmFileTemplateDetailDAO();
    }

    public static AtmFileTemplateDAOInter createAtmFileTemplateDAO(Object obj) {
        return new AtmFileTemplateDAO();
    }

    public static CopyFilesDAOInter createCopyFilesDAO(Object obj) {
        return new CopyFilesDAO();
    }

    public static BlockUsersDAOInter createBlockUsersDAO(Object obj) {
        return new BlockUsersDAO();
    }

    public static SystemTimesDAOInter createSystemTimesDAO(Object obj) {
        return new SystemTimesDAO();
    }

    public static TimeShiftDAOInter createTimeShiftDAO(Object obj) {
        return new TimeShiftDAO();
    }

    public static DisputeRulesDAOInter createDisputeRulesDAO(Object obj) {
        return new DisputeRulesDAO();
    }

    public static AtmGroupDAOInter createAtmGroupDAO(Object obj) {
        return new AtmGroupDAO();
    }

    public static CurrencyMasterDAOInter createCurrencyMasterDAO(Object obj) {
        return new CurrencyMasterDAO();
    }

    public static MatchedDataDAOInter createMatchedDataDAO(Object obj) {
        return new MatchedDataDAO();
    }

    public static CurrencyDAOInter createCurrencyDAO(Object obj) {
        return new CurrencyDAO();
    }

    public static TransactionStatusDAOInter createTransactionStatusDAO(Object obj) {
        return new TransactionStatusDAO();
    }

    public static TransactionTypeDAOInter createTransactionTypeDAO(Object obj) {
        return new TransactionTypeDAO();
    }

    public static TransactionResponseCodeDAOInter createTransactionResponseCodeDAO(Object obj) {
        return new TransactionResponseCodeDAO();
    }

    public static AtmMachineDAOInter createAtmMachineDAO(Object obj) {
        return new AtmMachineDAO();
    }

    public static BlockReasonDAOInter createBlockReasonDAO(Object obj) {
        return new BlockReasonDAO();
    }

    public static EmgHolidaysDAOInter createEmgHolidaysDAO(Object obj) {
        return new EmgHolidaysDAO();
    }

    public static HolidaysDAOInter createHolidaysDAO(Object obj) {
        return new HolidaysDAO();
    }

    public static TimeShiftDetailDAOInter createTimeShiftDetailDAO(Object obj) {
        return new TimeShiftDetailDAO();
    }

    public static UsersDAOInter createUsersDAO(Object obj) {
        return new UsersDAO();
    }

    public static MatchingTypeDAOInter createMatchingTypeDAO(Object obj) {
        return new MatchingTypeDAO();
    }

    public static MatchingTypeMachineDAOInter createMatchingTypeMachineDAO(Object obj) {
        return new MatchingTypeMachineDAO();
    }

    public static NetworkMasterDAOInter createNetworkMasterDAO(Object obj) {
        return new NetworkMasterDAO();
    }

    public static AtmMachineTypeDAOInter createAtmMachineTypeDAO(Object obj) {
        return new AtmMachineTypeDAO();
    }

    public static MatchingTypeSettingDAOInter createMatchingTypeSettingDAO(Object obj) {
        return new MatchingTypeSettingDAO();
    }

    public static FileColumnDefinitionDAOInter createFileColumnDefinitionDAO(Object obj) {
        return new FileColumnDefinitionDAO();
    }

    public static NetworkGroupDAOInter createNetworkGroupDAO(Object obj) {
        return new NetworkGroupDAO();
    }

    public static NetworkDetailsDAOInter createNetworkDetailsDAO(Object obj) {
        return new NetworkDetailsDAO();
    }
}
