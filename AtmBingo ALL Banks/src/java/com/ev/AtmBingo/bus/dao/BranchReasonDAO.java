
        
        /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.BlockReasonDTOInter;
import com.ev.AtmBingo.bus.dto.BranchReasonDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class BranchReasonDAO extends BaseDAO implements BranchReasonDAOInter  {

    protected BranchReasonDAO() {
        super();
        super.setTableName("branch_reason");
    }

    @Override
    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        BranchReasonDTOInter uDTO = (BranchReasonDTOInter) obj[0];
        uDTO.setReasonId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ($reason_id, '$reason_name' )";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$reason_id", "" + uDTO.getReasonId());
        insertStat = insertStat.replace("$reason_name", "" + uDTO.getReasonName());
        super.executeUpdate(insertStat);
        super.postUpdate("Add " + uDTO.getReasonName() + " to branch reasons", false);
        return null;
    }

    @Override
    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        BranchReasonDTOInter uDTO = (BranchReasonDTOInter) obj[0];
        String updateStat = "update $table set name = '$reason_name'"
                + " where id = $reason_id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$reason_id", "" + uDTO.getReasonId());
        updateStat = updateStat.replace("$reason_name", "" + uDTO.getReasonName());
        super.executeUpdate(updateStat);
        super.postUpdate("Update " + uDTO.getReasonName() + " in branch reasons", false);
        return null;
    }

    @Override
    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        BranchReasonDTOInter uDTO = (BranchReasonDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $reason_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$reason_id", "" + uDTO.getReasonId());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete " + uDTO.getReasonName() + " from branch reasons", false);
        return null;
    }

    @Override
    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $reason_id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$reason_id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        BranchReasonDTOInter uDTO = DTOFactory.createbranchreasonDTO();
        while (rs.next()) {
            uDTO.setReasonId(rs.getInt("id"));
            uDTO.setReasonName(rs.getString("name"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    @Override
    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<BranchReasonDTOInter> uDTOL = new ArrayList<BranchReasonDTOInter>();
        while (rs.next()) {
            BranchReasonDTOInter uDTO = DTOFactory.createbranchreasonDTO();
            uDTO.setReasonId(rs.getInt("id"));
            uDTO.setReasonName(rs.getString("name"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    @Override
    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
