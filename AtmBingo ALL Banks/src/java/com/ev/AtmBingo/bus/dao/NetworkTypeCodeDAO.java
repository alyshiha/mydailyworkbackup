
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.AtmAccountsDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.NetworkTypeCodeDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class NetworkTypeCodeDAO extends BaseDAO implements NetworkTypeCodeDAOInter {

    @Override
    public List<NetworkTypeCodeDTOInter> findRecord(int network, String code) throws Throwable {
        super.preSelect();
        String selectStat = "select n.id, (select name from networks k where k.id = n.network_id) networkvalue , n.network_id, n.code, n.start_position,  n.length, (select f.name from file_column_definition f where f.id = n.network_col_id) networkcolcalue ,n.NETWORK_COL_ID from network_type_codes n where (n.network_id = $network or $network = 0) and  n.code like '%$code%' order by n.id";
        selectStat = selectStat.replace("$code", code);
        selectStat = selectStat.replace("$network", "" + network);
        ResultSet rs = executeQuery(selectStat);
        List<NetworkTypeCodeDTOInter> records = new ArrayList<NetworkTypeCodeDTOInter>();
        while (rs.next()) {
            NetworkTypeCodeDTOInter record = DTOFactory.createNetworkTypeCodeDTO();
            record.setId(rs.getInt("id"));
            record.setNetworkid(rs.getInt("network_id"));
            record.setCode(rs.getString("code"));
            record.setStartposition(rs.getInt("start_position"));
            record.setLength(rs.getInt("length"));
            record.setColumnid(rs.getInt("NETWORK_COL_ID"));
            record.setNetworkvalue(rs.getString("networkvalue"));
            record.setColumnvalue(rs.getString("networkcolcalue"));

            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("search records in network type code by code like: " + code + " and network like: " + network);
        return records;
    }

    @Override
    public List<NetworkTypeCodeDTOInter> findAll() throws Throwable {
        super.preSelect();
        String selectStat = "select n.id, (select name from networks k where k.id = n.network_id) networkvalue , n.network_id, n.code, n.start_position,  n.length, (select f.name from file_column_definition f where f.id = n.network_col_id) networkcolcalue ,n.NETWORK_COL_ID from network_type_codes n order by n.id";
        ResultSet rs = executeQuery(selectStat);
        List<NetworkTypeCodeDTOInter> records = new ArrayList<NetworkTypeCodeDTOInter>();
        while (rs.next()) {
            NetworkTypeCodeDTOInter record = DTOFactory.createNetworkTypeCodeDTO();
            record.setId(rs.getInt("id"));
            record.setNetworkid(rs.getInt("network_id"));
            record.setCode(rs.getString("code"));
            record.setStartposition(rs.getInt("start_position"));
            record.setLength(rs.getInt("length"));
            record.setColumnid(rs.getInt("NETWORK_COL_ID"));
            record.setNetworkvalue(rs.getString("networkvalue"));
            record.setColumnvalue(rs.getString("networkcolcalue"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("search all records in network type ");
        return records;
    }

    @Override
    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        NetworkTypeCodeDTOInter record = (NetworkTypeCodeDTOInter) obj[0];
        record.setId(super.generateSequence("NETWORK_TYPE_CODES"));
        String insertStat = "insert into network_type_codes\n"
                + "  (id, network_id, code, start_position, length, NETWORK_COL_ID)\n"
                + "values\n"
                + "  ($id, $networkid, '$code', $start_position, $length, $column_id)";
        insertStat = insertStat.replace("$id", "" + record.getId());
        insertStat = insertStat.replace("$networkid", "" + record.getNetworkid());
        insertStat = insertStat.replace("$code", "" + record.getCode());
        insertStat = insertStat.replace("$start_position", "" + record.getStartposition());
        insertStat = insertStat.replace("$length", "" + record.getLength());
        insertStat = insertStat.replace("$column_id", "" + record.getColumnid());

        super.executeUpdate(insertStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("insert new record in network type code by id: " + record.getId());
        return null;
    }

    @Override
    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        NetworkTypeCodeDTOInter record = (NetworkTypeCodeDTOInter) obj[0];
        String updateStat = "update network_type_codes\n"
                + "   set network_id = $networkid,\n"
                + "       code = '$code',\n"
                + "       start_position = $startposition,\n"
                + "       length = $length,\n"
                + "       NETWORK_COL_ID = $columnid\n"
                + " where id = $id";
        updateStat = updateStat.replace("$networkid", "" + record.getNetworkid());
        updateStat = updateStat.replace("$code", "" + record.getCode());
        updateStat = updateStat.replace("$startposition", "" + record.getStartposition());
        updateStat = updateStat.replace("$length", "" + record.getLength());
        updateStat = updateStat.replace("$columnid", "" + record.getColumnid());
        updateStat = updateStat.replace("$id", "" + record.getId());

        
        super.executeUpdate(updateStat);

        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("update record in network type code by id: " + record.getId());
        return null;
    }

    @Override
    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        NetworkTypeCodeDTOInter record = (NetworkTypeCodeDTOInter) obj[0];
        String deleteStat = "delete network_type_codes\n"
                + " where id = $id";
        deleteStat = deleteStat.replace("$id", "" + record.getId());
        super.executeUpdate(deleteStat);

        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("delete record in network type code");

        return null;
    }

}
