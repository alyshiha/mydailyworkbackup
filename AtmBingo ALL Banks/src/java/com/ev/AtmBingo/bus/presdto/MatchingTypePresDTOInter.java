/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.presdto;

import com.ev.AtmBingo.bus.dto.MatchingTypeDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface MatchingTypePresDTOInter extends MatchingTypeDTOInter {

    String getTyp1();

    String getTyp2();

    boolean isActiv();

    List<MatchingTypeMachinesPresDTOInter> getMatchingTypeMachinesL();

    void setMatchingTypeMachinesL(List<MatchingTypeMachinesPresDTOInter> matchingTypeMachinesL);

    void setActiv(boolean activ);

    void setTyp1(String typ1);

    void setTyp2(String typ2);

//    int getActive();
//
//    int getId();
//
//    String getName();
//
//    int getType1();
//
//    int getType2();
//
//    void setActive(int active);
//
//    void setId(int id);
//
//    void setName(String name);
//
//    void setType1(int type1);
//
//    void setType2(int type2);

}
