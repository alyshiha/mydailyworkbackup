/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.presdto;

import com.ev.AtmBingo.base.presdto.BasePresDTO;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class MatchingTypePresDTO extends BasePresDTO implements MatchingTypePresDTOInter, Serializable {

    private String typ1;
    private String typ2;
    private boolean activ;
    private List<MatchingTypeMachinesPresDTOInter> matchingTypeMachinesL;

    private int id;
    private int type1;
    private int type2;
    private int active;
    private String name;

    protected MatchingTypePresDTO() {
        super();
    }

    public List<MatchingTypeMachinesPresDTOInter> getMatchingTypeMachinesL() {
        return matchingTypeMachinesL;
    }

    public void setMatchingTypeMachinesL(List<MatchingTypeMachinesPresDTOInter> matchingTypeMachinesL) {
        this.matchingTypeMachinesL = matchingTypeMachinesL;
    }

    public boolean isActiv() {
        setActiv(getActive() == 1);
        return activ;
    }

    public void setActiv(boolean activ) {
        if(activ == true){
            setActive(1);
        }
        else{
            setActive(2);
        }
        this.activ = activ;

    }

    public String getTyp1() {
        if(getType1() == 1){
            setTyp1("Journal");
        }
        else if(getType1() == 2){
            setTyp1("Switch");
        }
        else if(getType1() == 3){
            setTyp1("Host");
        }
        return typ1;
    }

    public void setTyp1(String typ1) {
        if(typ1.equals("Journal")){
            setType1(1);
        }
        else if(typ1.equals("Switch")){
            setType1(2);
        }
        else if(typ1.equals("Host")){
            setType1(3);
        }
        this.typ1 = typ1;
    }

    public String getTyp2() {
        if(getType2() == 1){
            setTyp2("Journal");
        }
        else if(getType2() == 2){
            setTyp2("Switch");
        }
        else if(getType2() == 3){
            setTyp2("Host");
        }
        return typ2;
    }

    public void setTyp2(String typ2) {
        if(typ2.equals("Journal")){
            setType2(1);
        }
        else if(typ2.equals("Switch")){
            setType2(2);
        }
        else if(typ2.equals("Host")){
            setType2(3);
        }
        this.typ2 = typ2;
    }

    public int getActive() {
         return this.active;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public int getType1() {
        return this.type1;
    }

    public int getType2() {
        return this.type2;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType1(int type1) {
        this.type1 = type1;
    }

    public void setType2(int type2) {
        this.type2 = type2;
    }
}
