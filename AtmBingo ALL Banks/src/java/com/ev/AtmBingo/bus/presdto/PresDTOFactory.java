/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.presdto;

/**
 *
 * @author Administrator
 */
public class PresDTOFactory {

    public static MatchingTypePresDTOInter creatMatchingTypePresDTO(){
        return new MatchingTypePresDTO();
    }
    public static MatchingTypeMachinesPresDTOInter creatMatchingTypeMachinesPresDTO(){
        return new MatchingTypeMachinesPresDTO();
    }
    public static MatchingTypeSettingPresDTOInter creatMatchingTypeSettingPresDTO(){
        return new MatchingTypeSettingPresDTO();
    }
    public static AtmMachineTypePresDTOInter creatAtmMachineTypePresDTOPresDTO(){
        return new AtmMachineTypePresDTO();
    }
    public static FileColumnDefinitionPresDTOInter creatFileColumnDefinitionPresDTOPresDTO(){
        return new FileColumnDefinitionPresDTO();
    }
}
