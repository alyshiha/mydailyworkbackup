/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.presdto;

import com.ev.AtmBingo.base.presdto.BasePresDTO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.FileColumnDefinitionDAOInter;
import com.ev.AtmBingo.bus.dto.FileColumnDefinitionDTOInter;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Administrator
 */
public class MatchingTypeSettingPresDTO extends BasePresDTO implements MatchingTypeSettingPresDTOInter, Serializable {

    private String colName;


    private int id;
    private int columnId;
    private int partSetting;
    private BigDecimal partLength;
    private int matchingType;

    protected  MatchingTypeSettingPresDTO() {
        super();
    }

    public String getColName() throws Throwable {
        FileColumnDefinitionDAOInter fcdDAO = DAOFactory.createFileColumnDefinitionDAO(null);
        FileColumnDefinitionDTOInter fcdDTO = (FileColumnDefinitionDTOInter) fcdDAO.find(getColumnId());
        setColName(fcdDTO.getName());
        return colName;
    }

    public void setColName(String colName) throws Throwable {
        FileColumnDefinitionDAOInter fcdDAO = DAOFactory.createFileColumnDefinitionDAO(null);
        FileColumnDefinitionDTOInter fcdDTO = (FileColumnDefinitionDTOInter) fcdDAO.search(colName);
        setColumnId(fcdDTO.getId());
        this.colName = colName;
    }


    public int getColumnId() {
        return this.columnId;
    }

    public int getId() {
        return this.id;
    }

    public int getMatchingType() {
        return this.matchingType;
    }

    public BigDecimal getPartLength() {
        return this.partLength;
    }

    public int getPartSetting() {
        return this.partSetting;
    }

    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMatchingType(int matchingType) {
        this.matchingType = matchingType;
    }

    public void setPartLength(BigDecimal partLength) {
        this.partLength = partLength;
    }

    public void setPartSetting(int partSetting) {
        this.partSetting = partSetting;
    }


}
