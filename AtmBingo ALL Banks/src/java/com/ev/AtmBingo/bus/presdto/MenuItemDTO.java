/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.presdto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class MenuItemDTO extends BaseDTO implements MenuItemDTOInter{

    private String title;
    private String itemId;
    List<MenuItemDTOInter> subItem;

    public MenuItemDTO() {
        super();
    }

    public List<MenuItemDTOInter> getSubItem() {
        return subItem;
    }

    public void setSubItem(List<MenuItemDTOInter> subItem) {
        this.subItem = subItem;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
