/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.presdto;

import com.ev.AtmBingo.base.presdto.BasePresDTO;
import com.ev.AtmBingo.bus.dao.AtmMachineTypeDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class MatchingTypeMachinesPresDTO extends BasePresDTO implements  MatchingTypeMachinesPresDTOInter, Serializable {

    private String machType;
    private boolean defaultMatchSet;
    private List<MatchingTypeSettingPresDTOInter> matchingTypeSettingL;

    private int matchingType;
    private int machineType;
    private int defaultMatchingSetting;

    public int getDefaultMatchingSetting() {
        return defaultMatchingSetting;
    }

    public int getMachineType() {
        return machineType;
    }

    public int getMatchingType() {
        return matchingType;
    }

    public void setDefaultMatchingSetting(int defaultMatchingSetting) {
        this.defaultMatchingSetting = defaultMatchingSetting;
    }

    public void setMachineType(int machineType) {
        this.machineType = machineType;
    }

    public void setMatchingType(int matchingType) {
        this.matchingType = matchingType;
    }

    protected MatchingTypeMachinesPresDTO() {
        super();
    }

    public List<MatchingTypeSettingPresDTOInter> getMatchingTypeSettingL() {
        return matchingTypeSettingL;
    }

    public void setMatchingTypeSettingL(List<MatchingTypeSettingPresDTOInter> matchingTypeSettingL) {
        this.matchingTypeSettingL = matchingTypeSettingL;
    }

    public boolean isDefaultMatchSet() {
        setDefaultMatchSet(getDefaultMatchingSetting() == 1);
        return defaultMatchSet;
    }

    public void setDefaultMatchSet(boolean defaultMatchSet) {
        if(defaultMatchSet == true){
            setDefaultMatchingSetting(1);
        }
        else{
            setDefaultMatchingSetting(2);
        }
        this.defaultMatchSet = defaultMatchSet;
    }

    public String getMachType() throws Throwable {
        AtmMachineTypeDAOInter amtDAO = DAOFactory.createAtmMachineTypeDAO(null);
        AtmMachineTypeDTOInter amtDTO = (AtmMachineTypeDTOInter) amtDAO.find(getMachineType());
        setMachType(amtDTO.getName());
        return this.machType;
    }

    public void setMachType(String machType) throws Throwable {
        AtmMachineTypeDAOInter amtDAO = DAOFactory.createAtmMachineTypeDAO(null);
        AtmMachineTypeDTOInter amtDTO = (AtmMachineTypeDTOInter) amtDAO.search(machType);
        setMachineType(amtDTO.getId());
        this.machType = machType;
    }
}
