package com.ev.AtmBingo;


import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.ReplanishmentHistoryRepBOInter;
import java.io.Serializable;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.*;

/**
 *
 * @author Administrator
 */
public class SimpleProtector  implements Serializable{

    private static final String ALGORITHM = "AES";
    // private static final byte[] keyValue = new byte[]{'T', 'h', 'i', 's', 'I', 's', 'A', 'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y'};

    public static String encrypt(String valueToEnc, String keyValue) throws Exception {
        byte[] keyVal = keyValue.getBytes();
        Key key = generateKey(keyVal);
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encValue = c.doFinal(valueToEnc.getBytes());
        String encryptedValue = new BASE64Encoder().encode(encValue);
        return encryptedValue;
    }

    public static String decrypt(String encryptedValue, String keyValue) throws Exception {
        byte[] keyVal = keyValue.getBytes();
        Key key = generateKey(keyVal);
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedValue);
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }

    private static Key generateKey(byte[] keyValue) throws Exception {
        Key key = new SecretKeySpec(keyValue, ALGORITHM); // SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM); // key = keyFactory.generateSecret(new DESKeySpec(keyValue)); return key; } - See more at: http://www.digizol.com/2009/10/java-encrypt-decrypt-jce-salt.html#sthash.7iP1001l.dpuf
        return key;
    }

    public static Date getYesterdaytrans(String time) {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        cal.add(Calendar.DATE, Integer.valueOf(1));
        String[] sArr = time.split(":");
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sArr[0]));
        cal.set(Calendar.MINUTE, Integer.parseInt(sArr[1]));
        cal.set(Calendar.SECOND, Integer.parseInt(sArr[2]));
        String s = dateFormat.format(cal.getTime());
        Date d = dateFormat.getCalendar().getTime();
        return d;
    }

    public static Date getTodaytrans(String time) {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String[] sArr = time.split(":");
        cal.add(Calendar.DATE, Integer.valueOf(-0));
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sArr[0]));
        cal.set(Calendar.MINUTE, Integer.parseInt(sArr[1]));
        cal.set(Calendar.SECOND, Integer.parseInt(sArr[2]) - 1);

        String s = dateFormat.format(cal.getTime());
        Date d = dateFormat.getCalendar().getTime();
        return d;

    }

  
    public static void main(String[] args) throws Exception, Throwable {
        /*String password = "atm";
         String key = "testtesttesttest";
         String passwordEnc = SimpleProtector.encrypt(password,key);
         String passwordDec = SimpleProtector.decrypt(passwordEnc,key);
         System.out.println("Plain Text : " + password);
         // System.out.println("Encrypted : " + passwordEnc);
         System.out.println("Decrypted : " + passwordDec);*/
        ReplanishmentHistoryRepBOInter repHistoryRep;
        repHistoryRep = BOFactory.createReplanishmentHistoryRepBO(null);
        
        String path = (String) repHistoryRep.findReport(getTodaytrans("00:00:00"), getYesterdaytrans("00:00:00"), null, 0, "Evision", "Administrator");
    }
}
