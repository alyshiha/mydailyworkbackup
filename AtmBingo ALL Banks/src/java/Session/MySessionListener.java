/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * When the user session timedout, ({@link #sessionDestroyed(HttpSessionEvent)}) method will be invoked.
 * This method will make necessary cleanups (logging out user, updating db and audit logs, etc...)
 * As a result; after this method, we will be in a clear and stable state. So nothing left to think about
 * because session expired, user can do nothing after this point.
 * 
 * @author hturksoy
 * 
 */
public class MySessionListener implements HttpSessionListener {
 
 /**
  * our apps security service bean responsible from doing necessary operations at
  * login/logout progress...
  */
 private String user;

 public MySessionListener() {
 }

 @Override
 public void sessionCreated(HttpSessionEvent event) {
   System.out.println("session created : " + event.getSession().getId());
 }

 @Override
 public void sessionDestroyed(HttpSessionEvent event) {
  // get the destroying session...
  HttpSession session = event.getSession();
   System.out.println("session destroyed :" + session.getId() + " Logging out user...");

  /*
   * nobody can reach user data after this point because session is invalidated already.
   * So, get the user data from session and save its logout information 
   * before losing it.
   * User's redirection to the timeout page will be handled by the SessionTimeoutFilter.
   */
  try {
   prepareLogoutInfoAndLogoutActiveUser(session);
  } catch(Exception e) {
    System.out.println("error while logging out at session destroyed : " + e.getMessage());
  }
 }
 
 /**
  * Gets the logged in user data from Acegi SecurityContext and makes necessary logout operations. 
     * @param httpSession
  */
 public void prepareLogoutInfoAndLogoutActiveUser(HttpSession httpSession) {
     System.out.println("Clear Session Att");
 }

}