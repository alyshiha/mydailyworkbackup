/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CAsh;


import java.io.Serializable;

/**
 *
 * @author AlyShiha
 */
public class CreateCash implements Serializable {
     private static CrunchifyInMemoryCache instance = null;

    public static synchronized CrunchifyInMemoryCache getInstance() {
        if (instance == null) {
            instance = new CrunchifyInMemoryCache<String, Object>(200, 0, 6);
        }
        return instance;
        
    }
}
