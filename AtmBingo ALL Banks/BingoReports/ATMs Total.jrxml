<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="e.visionTemp" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="802" leftMargin="10" rightMargin="10" topMargin="10" bottomMargin="10">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Column header"/>
	<style name="Title"/>
	<parameter name="user" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateFrom" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="AtmGroup" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="AtmGroupInt" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="customer" class="java.lang.String"/>
	<queryString>
		<![CDATA[  SELECT   SUM (transaction) transaction,
           atm_application_id,
           atm_id,
           currency
    FROM   (SELECT           /*+parallel index(disputes DISPUTES_SUM_INDEX) */
                  DISTINCT
                     atm_id,
                     SUM (amount) OVER (PARTITION BY atm_id) transaction,
                     atm_application_id,
                     currency
              FROM   disputes
             WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},
                                                       'dd.MM.yyyy hh24:mi:ss')
                                          AND  TO_DATE (
                                                  $P{DateTo},
                                                  'dd.MM.yyyy hh24:mi:ss'
                                               )
                     AND record_type = 2
                     AND matching_type = 1
                     AND EXISTS
                           (SELECT   1
                              FROM   TRANSACTION_TYPE_MASTER
                             WHERE   UPPER (name) LIKE UPPER ('%with%')
                                     AND TRANSACTION_TYPE_MASTER.id =
                                           DISPUTES.TRANSACTION_TYPE_MASTER)
                     AND ATM_ID IN
                              (SELECT   id
                                 FROM   atm_machine
                                WHERE   atm_group IN
                                              (SELECT   id
                                                 FROM   atm_group
                                                WHERE   parent_id IN
                                                              (SELECT   id
                                                                 FROM   atm_group
                                                                WHERE   (parent_id =
                                                                            $P{AtmGroupInt}
                                                                         OR id =
                                                                              $P{AtmGroupInt}))
                                                        OR 0 = $P{AtmGroupInt}))
            UNION
            SELECT /*+ parallel index (matched_data MATCHED_DATA_GRAPH_INDEX2) */
                  DISTINCT
                     atm_id,
                     SUM (amount) OVER (PARTITION BY atm_id) transaction,
                     atm_application_id,
                     currency
              FROM   matched_data
             WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},
                                                       'dd.MM.yyyy hh24:mi:ss')
                                          AND  TO_DATE (
                                                  $P{DateTo},
                                                  'dd.MM.yyyy hh24:mi:ss'
                                               )
                     AND record_type = 2
                     AND matching_type = 1
                     AND EXISTS
                           (SELECT   1
                              FROM   TRANSACTION_TYPE_MASTER
                             WHERE   UPPER (name) LIKE UPPER ('%with%')
                                     AND TRANSACTION_TYPE_MASTER.id =
                                           matched_data.TRANSACTION_TYPE_MASTER)
                     AND ATM_ID IN
                              (SELECT   id
                                 FROM   atm_machine
                                WHERE   atm_group IN
                                              (SELECT   id
                                                 FROM   atm_group
                                                WHERE   parent_id IN
                                                              (SELECT   id
                                                                 FROM   atm_group
                                                                WHERE   (parent_id =
                                                                            $P{AtmGroupInt}
                                                                         OR id =
                                                                              $P{AtmGroupInt}))
                                                        OR 0 = $P{AtmGroupInt}))
            ORDER BY   atm_id) fulltable
   WHERE   atm_valid (atm_id) = 1
GROUP BY   atm_application_id, atm_id, currency]]>
	</queryString>
	<field name="TRANSACTION" class="java.math.BigDecimal"/>
	<field name="ATM_APPLICATION_ID" class="java.lang.String"/>
	<field name="ATM_ID" class="java.math.BigDecimal"/>
	<field name="CURRENCY" class="java.lang.String"/>
	<variable name="CURRENCY_1" class="java.lang.Integer" resetType="Column" calculation="Count">
		<variableExpression><![CDATA[$F{CURRENCY}]]></variableExpression>
	</variable>
	<variable name="TRANSACTION#_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{TRANSACTION}]]></variableExpression>
	</variable>
	<pageHeader>
		<band height="68">
			<textField>
				<reportElement x="721" y="21" width="100" height="20"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{user}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy HH:mm">
				<reportElement style="Column header" x="665" y="1" width="156" height="20"/>
				<textElement textAlignment="Right">
					<font size="8" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="711" y="21" width="57" height="20"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Printed by :]]></text>
			</staticText>
			<line>
				<reportElement x="154" y="1" width="1" height="61"/>
				<graphicElement>
					<pen lineWidth="3.0" lineColor="#666666"/>
				</graphicElement>
			</line>
			<image>
				<reportElement x="37" y="14" width="96" height="41"/>
				<imageExpression class="java.lang.String"><![CDATA["c:\\BingoReports\\ATM_logo_report.png"]]></imageExpression>
			</image>
			<textField pattern="dd.MM.yyyy HH:mm:ss" isBlankWhenNull="true">
				<reportElement x="235" y="24" width="136" height="20"/>
				<textElement textAlignment="Center">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{DateFrom}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="371" y="24" width="14" height="20"/>
				<textElement textAlignment="Center">
					<font size="12"/>
				</textElement>
				<text><![CDATA[to :]]></text>
			</staticText>
			<staticText>
				<reportElement x="163" y="24" width="72" height="20"/>
				<textElement>
					<font size="12"/>
				</textElement>
				<text><![CDATA[Date from :]]></text>
			</staticText>
			<textField pattern="dd.MM.yyyy HH:mm:ss" isBlankWhenNull="true">
				<reportElement x="385" y="24" width="132" height="20"/>
				<textElement textAlignment="Center">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{DateTo}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="163" y="3" width="233" height="19" forecolor="#999999"/>
				<textElement>
					<font size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[ATM Total Withdraw]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Transparent" x="163" y="44" width="41" height="19" forecolor="#000000" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="12" isBold="false"/>
				</textElement>
				<text><![CDATA[Group :]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="204" y="44" width="100" height="19"/>
				<textElement verticalAlignment="Middle">
					<font size="12"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{AtmGroup}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="718" y="41" width="103" height="17"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$P{customer}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="707" y="41" width="57" height="17"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Licensed to :]]></text>
			</staticText>
			<staticText>
				<reportElement x="706" y="1" width="54" height="20"/>
				<textElement>
					<font fontName="Arial" size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Report date :]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="41">
			<staticText>
				<reportElement x="210" y="0" width="100" height="40" forecolor="#339900"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ATM ID]]></text>
			</staticText>
			<staticText>
				<reportElement x="310" y="0" width="117" height="40" forecolor="#339900"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ATM Name]]></text>
			</staticText>
			<staticText>
				<reportElement x="427" y="0" width="132" height="40" forecolor="#339900"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Transactions total amount]]></text>
			</staticText>
			<staticText>
				<reportElement x="559" y="0" width="100" height="40" forecolor="#339900"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Currency]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="18">
			<line>
				<reportElement x="210" y="17" width="449" height="1"/>
			</line>
			<textField isBlankWhenNull="true">
				<reportElement x="209" y="-1" width="101" height="18"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{ATM_APPLICATION_ID}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="310" y="0" width="117" height="17"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{ATM_APPLICATION_ID}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
				<reportElement x="427" y="0" width="132" height="17"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{TRANSACTION}]]></textFieldExpression>
			</textField>
			<textField pattern=" #,##0.00" isBlankWhenNull="true">
				<reportElement x="559" y="0" width="100" height="17"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{CURRENCY}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="28">
			<line>
				<reportElement x="736" y="2" width="85" height="1"/>
				<graphicElement>
					<pen lineWidth="4.0" lineColor="#FF0000"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="0" y="2" width="736" height="1"/>
				<graphicElement>
					<pen lineWidth="4.0" lineColor="#669900"/>
				</graphicElement>
			</line>
			<rectangle>
				<reportElement x="0" y="5" width="821" height="22" backcolor="#666666"/>
				<graphicElement>
					<pen lineWidth="1.0"/>
				</graphicElement>
			</rectangle>
			<textField evaluationTime="Report">
				<reportElement style="Column header" x="773" y="8" width="40" height="17" forecolor="#FFFFFF"/>
				<textElement>
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Column header" x="693" y="8" width="80" height="17" forecolor="#FFFFFF"/>
				<textElement textAlignment="Right">
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="217" y="6" width="342" height="20" forecolor="#FFFFFF"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Copyright  2016 eVision Financial Compliance Unit]]></text>
			</staticText>
		</band>
	</pageFooter>
	<summary>
		<band height="17">
			<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Opaque" x="427" y="1" width="132" height="16" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{TRANSACTION#_1}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Opaque" x="310" y="1" width="117" height="16" forecolor="#339900" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total amount :]]></text>
			</staticText>
		</band>
	</summary>
</jasperReport>
