/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evision.service;

import com.evision.model.JSchUserInfo;
import com.evision.model.propertiesDTO;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author Aly-Shiha
 */
public class SFTPservice {

    private static propertiesDTO Prop;
    private String username;
    private byte[] passphrase;
    private byte[] privatekey = null;
    private JSchUserInfo userinfo = null;
    private String hostname;
    private Session session = null;
    private ChannelSftp sftp = null;
    private static final Logger _logger = Logger.getLogger(SFTPservice.class);

    public SFTPservice(String hostname, String username, String password,
            String passphrase, File privateKeyfile) throws IOException {
        this.hostname = hostname;
        this.username = username;
        this.passphrase = this.str2byte(passphrase);
        this.privatekey = FileUtils.readFileToByteArray(privateKeyfile);
        this.userinfo = new JSchUserInfo(password, passphrase);

    }

    public void connect() throws JSchException, SftpException {
        JSch jSch = new JSch();
        jSch.addIdentity(this.username, // String userName
                this.privatekey, // byte[] privateKey
                null, // byte[] publicKey
                this.passphrase // byte[] passPhrase
        );

        this.session = jSch.getSession(this.username, this.hostname, 22);
        this.session.setUserInfo(this.userinfo);
        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);

        this.session.connect();
        Channel channel = session.openChannel("sftp");
        this.sftp = (ChannelSftp) channel;
        this.sftp.connect();
        _logger.info("Connected to Server via SFTP...");
    }

    public void disconnect() {
        if (this.sftp != null) {
            this.sftp.disconnect();
        }
        if (this.session != null) {
            this.session.disconnect();
        }
        _logger.info("Disconnected from Server.");
    }

    public void putFile(String file, String target) throws SftpException {
        _logger.debug("Copying [" + file + "] to [" + target + "]");
        this.sftp.put(file, target);
    }

    private byte[] str2byte(String str, String encoding) {
        if (str == null) {
            return null;
        }
        try {
            return str.getBytes(encoding);
        } catch (java.io.UnsupportedEncodingException e) {
            return str.getBytes();
        }
    }

    private byte[] str2byte(String str) {
        return str2byte(str, "UTF-8");
    }
}
