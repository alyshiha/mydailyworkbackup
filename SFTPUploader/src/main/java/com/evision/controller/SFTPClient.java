/*
 * This file is licensed under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.evision.controller;

import com.evision.model.propertiesDTO;
import com.evision.service.SFTPservice;
import java.io.File;
import java.io.IOException;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.apache.log4j.Logger;

/**
 * A SFTP client program that provides easy methods to -file put and delete
 * -creation of remote folders -deep creation of remote folders (such as
 * /parent/child1/child1.1 )
 */
public class SFTPClient {

    private static final Logger _logger = Logger.getLogger(SFTPClient.class);

    public static void engin(propertiesDTO Prop) {
        SFTPservice client = null;
        try {
            client = new SFTPservice(
                    Prop.getHostname(),
                    Prop.getUsername(),
                    "",
                    "",
                    new File(Prop.getKeyfile()));
            client.connect();
            File[] files = new File(Prop.getSource()).listFiles();
//If this pathname does not denote a directory, then listFiles() returns null. 
            for (File file : files) {
                if (file.isFile()) {
                    client.putFile(file.getAbsolutePath(), "\\");
                }
            }

            client.disconnect();
        } catch (IOException | SftpException | JSchException ex) {
            _logger.error(ex.getMessage());
        } finally {
            try {
                if (client != null) {
                    client.disconnect();
                }
            } catch (Exception e) {
                _logger.error(e.getMessage());
            }
        }
    }
}
