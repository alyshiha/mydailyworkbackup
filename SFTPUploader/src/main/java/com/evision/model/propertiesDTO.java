/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evision.model;

import java.io.Serializable;

/**
 *
 * @author Aly-Shiha
 */
public class propertiesDTO implements Serializable {

    private String Hostname;
    private String Username;
    private String Keyfile;
    private String Source;

    public String getHostname() {
        return Hostname;
    }

    public void setHostname(String Hostname) {
        this.Hostname = Hostname;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getKeyfile() {
        return Keyfile;
    }

    public void setKeyfile(String Keyfile) {
        this.Keyfile = Keyfile;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String Source) {
        this.Source = Source;
    }

}
