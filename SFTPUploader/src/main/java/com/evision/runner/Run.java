/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evision.runner;

import com.evision.controller.SFTPClient;
import com.evision.controller.XStreamTranslator;
import com.evision.model.propertiesDTO;
import java.io.FileNotFoundException;

/**
 *
 * @author Aly-Shiha
 */
public class Run {

    private static final org.apache.log4j.Logger _logger = org.apache.log4j.Logger.getLogger(Run.class);

    public static void main(String[] args) {
        try {
            //Load config Files ./prop.xml

            propertiesDTO Prop = (propertiesDTO) XStreamTranslator.getInstance().toObject(args[0]);
            SFTPClient.engin(Prop);
        } catch (FileNotFoundException ex) {
            _logger.error(ex.getMessage());
        }
    }
}
