<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="EBC_1" language="groovy" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="DateFrom" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="DateTo" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT   'Matched' Title,
         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,
                           1,
                           NVL (AMOUNT, 0),
                           2,
                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)
            AMOUNT,
         COUNT (1) NO,
         1 ordering
  FROM   MATCHED_DATA
 WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},
                                           'dd-mm-yyyy hh24:mi:ss')
                              AND  TO_DATE ($P{DateTo},
                                            'dd-mm-yyyy hh24:mi:ss')
         AND record_type = 1
         AND network_id = 1
         AND CARD_NO NOT LIKE '411739%'
UNION
SELECT   'Unmatched Switch' Title,
         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,
                           1,
                           NVL (AMOUNT, 0),
                           2,
                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)
            AMOUNT,
         COUNT (1) NO,
         2 ordering
  FROM   disputes
 WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},
                                           'dd-mm-yyyy hh24:mi:ss')
                              AND  TO_DATE ($P{DateTo},
                                            'dd-mm-yyyy hh24:mi:ss')
         AND record_type = 2
         AND network_id = 1
         AND CARD_NO NOT LIKE '411739%'
UNION
SELECT   'Unmatched Network' Title,
         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,
                           1,
                           NVL (AMOUNT, 0),
                           2,
                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)
            AMOUNT,
         COUNT (1) NO,
         3 ordering
  FROM   disputes
 WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},
                                           'dd-mm-yyyy hh24:mi:ss')
                              AND  TO_DATE ($P{DateTo},
                                            'dd-mm-yyyy hh24:mi:ss')
         AND record_type = 1
         AND network_id = 1
         AND CARD_NO NOT LIKE '411739%'
UNION
SELECT   'Pre-paid Matched' Title,
         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,
                           1,
                           NVL (AMOUNT, 0),
                           2,
                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)
            AMOUNT,
         COUNT (1) NO,
         4 ordering
  FROM   matched_data
 WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},
                                           'dd-mm-yyyy hh24:mi:ss')
                              AND  TO_DATE ($P{DateTo},
                                            'dd-mm-yyyy hh24:mi:ss')
         AND record_type = 1
         AND network_id = 1
         AND CARD_NO LIKE '411739%'
UNION
SELECT   'Pre-paid Unmatched Switch' Title,
         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,
                           1,
                           NVL (AMOUNT, 0),
                           2,
                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)
            AMOUNT,
         COUNT (1) NO,
         5 ordering
  FROM   disputes
 WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},
                                           'dd-mm-yyyy hh24:mi:ss')
                              AND  TO_DATE ($P{DateTo},
                                            'dd-mm-yyyy hh24:mi:ss')
         AND record_type = 2
         AND network_id = 1
         AND CARD_NO LIKE '411739%'
UNION
SELECT   'Pre-paid Unmatched Network' Title,
         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,
                           1,
                           NVL (AMOUNT, 0),
                           2,
                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)
            AMOUNT,
         COUNT (1) NO,
         6 ordering
  FROM   disputes
 WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},
                                           'dd-mm-yyyy hh24:mi:ss')
                              AND  TO_DATE ($P{DateTo},
                                            'dd-mm-yyyy hh24:mi:ss')
         AND record_type = 1
         AND network_id = 1
         AND CARD_NO LIKE '411739%'
ORDER BY   ordering]]>
	</queryString>
	<field name="TITLE" class="java.lang.String"/>
	<field name="AMOUNT" class="java.math.BigDecimal"/>
	<field name="NO" class="java.math.BigDecimal"/>
	<field name="ORDERING" class="java.math.BigDecimal"/>
	<variable name="totalamount" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{AMOUNT}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<columnHeader>
		<band height="21">
			<rectangle>
				<reportElement x="0" y="1" width="822" height="19" backcolor="#666666"/>
				<graphicElement>
					<pen lineWidth="1.0"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement x="2" y="2" width="130" height="18" forecolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Title]]></text>
			</staticText>
			<staticText>
				<reportElement x="140" y="2" width="130" height="18" forecolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[AMOUNT]]></text>
			</staticText>
			<line>
				<reportElement x="134" y="1" width="1" height="19"/>
				<graphicElement>
					<pen lineWidth="1.0" lineColor="#000000"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="273" y="1" width="1" height="19"/>
				<graphicElement>
					<pen lineWidth="1.0" lineColor="#000000"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement x="274" y="2" width="130" height="17" forecolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Count]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="18" splitType="Stretch">
			<rectangle>
				<reportElement positionType="Float" x="2" y="1" width="822" height="17" backcolor="#CCCCCC">
					<printWhenExpression><![CDATA[$V{REPORT_COUNT}%2==0]]></printWhenExpression>
				</reportElement>
				<graphicElement>
					<pen lineWidth="0.0"/>
				</graphicElement>
			</rectangle>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="5" y="0" width="129" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{TITLE}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="142" y="0" width="133" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{AMOUNT}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0;-#,##0" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="280" y="0" width="133" height="17" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{NO}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="20">
			<rectangle>
				<reportElement x="0" y="0" width="822" height="19" backcolor="#666666"/>
				<graphicElement>
					<pen lineWidth="1.0"/>
				</graphicElement>
			</rectangle>
			<staticText>
				<reportElement x="2" y="0" width="130" height="19" forecolor="#FFFFFF"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[--------Total--------]]></text>
			</staticText>
			<textField pattern="#,##0.00;-#,##0.00" isBlankWhenNull="true">
				<reportElement x="135" y="0" width="139" height="19" forecolor="#FFFFFF"/>
				<textElement>
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{totalamount}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
