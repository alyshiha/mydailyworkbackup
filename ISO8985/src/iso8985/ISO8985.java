/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iso8985;

import com.tplus.transform.runtime.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author shi7a
 */
public class ISO8985 {

    static MessageFlow messageFlow;

    public static MessageFlow getMessageFlow(String messageFlowName) throws javax.naming.NamingException {
        // look up message flow and cache
        if (messageFlow == null) {
            // Get the lookup context for the current environment
            LookupContext lcxt = LookupContextFactory.getLookupContext();

            // Lookup message flow (defined in the cartridge)
            messageFlow = lcxt.lookupMessageFlow(messageFlowName);
        }
        return messageFlow;
    }
    /*
     Input: ISOASCIIBatched
     Output:CSV
     */

    public static void processISOASCIIBatchedCSV(byte[] rawIn, String FileOut) throws FileNotFoundException {
        try {
            messageFlow = getMessageFlow("ISOASCIIBatchedCSVFlow");

            // Create a TransformContext. We have no special properties to set in the context.
            TransformContext cxt = new TransformContextImpl();

            Object[] messageFlowArgs = new Object[]{rawIn,};

            // Execute the message flow.
            Object[] output = messageFlow.run(messageFlowArgs, cxt);

            // Use output
            RawMessage fileErroneousObjs = (RawMessage) output[0];
            if (fileErroneousObjs != null && fileErroneousObjs.toString().length() > 0) {
                System.out.println("***fileErroneousObjs:\r\n" + fileErroneousObjs.toString());
            }
            RawMessage rawOut = (RawMessage) output[1];
            if (rawOut != null) {
                try (PrintWriter out = new PrintWriter(FileOut)) {
                    out.println(rawOut.toString());
                }
            }
        } catch (TransformException e) {
            //  System.err.println(e.toXMLString());
        } catch (javax.naming.NamingException e) {
            e.printStackTrace();
        } catch (java.rmi.RemoteException e) {
            e.printStackTrace();
        }
    }
    /*
     Input: ISOEBCDICBatched
     Output:Excel (.xlsx or xlx);
     */

    public static void processISOEBCDICBatchedCSV(byte[] rawIn, String FileOut) throws FileNotFoundException {
        try {
            messageFlow = getMessageFlow("ISOEBCDICBatchedCSVFlow");

            // Create a TransformContext. We have no special properties to set in the context.
            TransformContext cxt = new TransformContextImpl();

            Object[] messageFlowArgs = new Object[]{rawIn,};

            // Execute the message flow.
            Object[] output = messageFlow.run(messageFlowArgs, cxt);

            // Use output
            RawMessage fileErroneousObjs = (RawMessage) output[0];
            if (fileErroneousObjs != null && fileErroneousObjs.toString().length() > 0) {
                System.out.println("***fileErroneousObjs:\r\n" + fileErroneousObjs.toString());
            }

            RawMessage rawOut = (RawMessage) output[1];
            if (rawOut != null) {
                try (PrintWriter out = new PrintWriter(FileOut)) {
                    out.println(rawOut.toString());
                }
            }
        } catch (TransformException e) {
            System.err.println(e.toXMLString());
        } catch (javax.naming.NamingException e) {
            e.printStackTrace();
        } catch (java.rmi.RemoteException e) {
            e.printStackTrace();
        }
    }
    static PropDTO Properties = ReadPropertiesXmlFile.readProperties();
    static FilesCollection FC = new FilesCollection();

    public static void main(String[] args) throws IOException, TransformException {
        com.tplus.transform.util.LoggingUtil.enableLogging("log.xml");

        for (final File fileEntry : new File(Properties.getFromFolder()).listFiles()) {
            RawMessage rawIn = new FileInputSource(fileEntry.getPath());
            processISOASCIIBatchedCSV(rawIn.getAsBytes(), Properties.getToFolder() +"\\"+ fileEntry.getName());
            FC.CreateDirectory(Properties.getBackUpFolder());
            FC.MoveFile(fileEntry.getAbsolutePath());
        }
    }

} // isosample
