package evision;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import static java.lang.String.format;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import javax.crypto.Cipher;
import static javax.crypto.Cipher.ENCRYPT_MODE;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import static javax.xml.bind.DatatypeConverter.printBase64Binary;

public class Main {

    private static final String ALGORITHM = "AES";
    private static final int KEY_SIZE = 128;
    private static final String MODE = "ECB";
    private static final String PADDING = "PKCS5PADDING";
    private static String CLEAR_PASSWORD;

    public static byte[] generateKeyBytes(String algorithm, int keySize) throws NoSuchAlgorithmException {
        KeyGenerator generator = KeyGenerator.getInstance(algorithm);
        generator.init(keySize);
        SecretKey key = generator.generateKey();

        return key.getEncoded();
    }

    public static byte[] encryptPassword(String password, String algorithm, String mode, String padding, File keyFile) throws Exception {
        byte[] keyBytes = Files.readAllBytes(keyFile.toPath());

        SecretKeySpec keySpec = new SecretKeySpec(keyBytes, algorithm);

        Cipher cipher = Cipher.getInstance(format("%s/%s/%s", algorithm, mode, padding));
        cipher.init(ENCRYPT_MODE, keySpec);
        byte[] cipherText = cipher.doFinal(password.getBytes());

        return cipherText;
    }

    public static void main(String[] args) throws Exception {
        //DES/CBC/PKCS5Padding (56)
        if (args.length > 0) {
            CLEAR_PASSWORD = args[0];
        } else {
            System.err.println("please insert password");
            Scanner sc = new Scanner(System.in);
            if (sc.hasNextLine()) {
                CLEAR_PASSWORD = sc.nextLine();
            }
        }
        System.out.println("Generating key...");
        byte[] keyBytes = generateKeyBytes(ALGORITHM, KEY_SIZE);

        File keyFile = new File(format("./Result/%s-%d.key", ALGORITHM, KEY_SIZE));
        System.out.println("Writing new key to file: " + keyFile.getCanonicalPath());
        Files.write(keyFile.toPath(), keyBytes);

        System.out.println("Encrypting password...");
        byte[] cipherBytes = encryptPassword(CLEAR_PASSWORD, ALGORITHM, MODE, PADDING, keyFile);

        File passwordFile = new File(format("./Result/%s-%d.password", ALGORITHM, KEY_SIZE));
        System.out.println("Writing encrypted password to file: " + passwordFile.getCanonicalPath());
        try (BufferedWriter writer = Files.newBufferedWriter(passwordFile.toPath(), Charset.forName("UTF-8"))) {
            writer.write(printBase64Binary(cipherBytes));
        } catch (IOException ex) {
            System.out.println("Unable to create file:" + ex.getMessage());
            System.out.println("Cipher-text password (Base64): " + printBase64Binary(cipherBytes));
        }

    }

}
