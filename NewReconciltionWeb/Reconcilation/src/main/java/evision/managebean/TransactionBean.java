package evision.managebean;

import com.googlecode.cqengine.ConcurrentIndexedCollection;
import com.googlecode.cqengine.IndexedCollection;
import com.googlecode.cqengine.index.hash.HashIndex;
import com.googlecode.cqengine.query.Query;
import com.googlecode.cqengine.query.QueryFactory;
import static com.googlecode.cqengine.query.QueryFactory.*;
import evision.datamodel.EBCEntity;
import evision.datamodel.HostEntity;
import evision.datamodel.SearchingCriteria;
import evision.datamodel.SearchOptions;
import evision.datamodel.SwitchEntity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
import oracle.jdbc.pool.OracleDataSource;
import oracle.net.aso.i;
import org.primefaces.event.ItemSelectEvent;
import static org.springframework.core.convert.TypeDescriptor.collection;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

@ManagedBean(name = "transBean")
@ViewScoped
public class TransactionBean implements Serializable {

    @PostConstruct
    public void init() {
        try {
            System.out.println("const");
            jdbcTemplate = new JdbcTemplate(dataSource());
            transSearchOptions = new SearchOptions();
            transSearchCriteria = new SearchingCriteria();

            acquirerSwitchStatus = Boolean.FALSE;
            transactionamountSwitchStatus = Boolean.FALSE;
            accountOperatorSwitchStatus = Boolean.FALSE;
            txdatetimeSwitchStatus = Boolean.FALSE;
            SettelmentdateSwitchStatus = Boolean.FALSE;
            authSwitchStatus = Boolean.FALSE;
            AcquireramountSwitchStatus = Boolean.FALSE;
            SettelmentamountSwitchStatus = Boolean.FALSE;
            TerminalamountSwitchStatus = Boolean.FALSE;
            txdatetimeHostStatus = Boolean.FALSE;
            SettelmentdateHostStatus = Boolean.FALSE;
            AuthHostStatus = Boolean.FALSE;
            TransactionamountHostStatus = Boolean.FALSE;
            txdatetimeEBCStatus = Boolean.FALSE;
            AuthEBCStatus = Boolean.FALSE;
            TransactionamountEBCStatus = Boolean.FALSE;
            FeesamountEBCStatus = Boolean.FALSE;
            TerminalSwitchStatus = Boolean.FALSE;
            BankCodeSwitchStatus = Boolean.FALSE;
            TranstypeSwitchStatus = Boolean.FALSE;
            PanSwitchStatus = Boolean.FALSE;
            rrnSwitchStatus = Boolean.FALSE;
            TranstypeHostStatus = Boolean.FALSE;
            NetworktypeHostStatus = Boolean.FALSE;
            TerminalHostStatus = Boolean.FALSE;
            BankCodeHostStatus = Boolean.FALSE;
            PanHostStatus = Boolean.FALSE;
            terminalEBCStatus = Boolean.FALSE;
            PanEBCStatus = Boolean.FALSE;
            rrnEBCStatus = Boolean.FALSE;
            uniquereferencenumberEBCStatus = Boolean.FALSE;
            ledgereffectEBCStatus = Boolean.FALSE;
            actioncodeEBCStatus = Boolean.FALSE;
            toPanEBCStatus = Boolean.FALSE;
            OctrrnEBCStatus = Boolean.FALSE;
            cardacceptorterminalidEBCStatus = Boolean.FALSE;
            CardacceptoridEBCStatus = Boolean.FALSE;

            setFilterVisibltyebc("hidden");
            setTransVisibltyebc("hidden");
            setFilterDisplayebc("none");
            setTransDisplayebc("none");

            setFilterVisibltySwitch("hidden");
            setTransVisibltySwitch("hidden");
            setFilterDisplaySwitch("none");
            setTransDisplaySwitch("none");

            setFilterVisibltyHost("hidden");
            setTransVisibltyHost("hidden");
            setFilterDisplayHost("none");
            setTransDisplayHost("none");

            setCharVisiblty("visible");
            setCharDisplay("inline");
            setChartSelection("EBC");
            transSearchCriteria.setAuthnumberFrom(0);
            transSearchCriteria.setAuthnumberTo(0);
            transSearchCriteria.setTracefrom(0);
            transSearchCriteria.setTraceto(0);

            acquirerSwitchList = new ArrayList<Integer>();
            responsecodeEBCList = new ArrayList<Integer>();
            terminalEBCList = new ArrayList<String>();
            actioncodeEBCList = new ArrayList<String>();
            ledgereffectEBCList = new ArrayList<String>();
            responsecodeSwitchList = new ArrayList<Integer>();
            terminalSwitchList = new ArrayList<String>();
            bankcodeSwitchList = new ArrayList<String>();
            transtypeSwitchList = new ArrayList<String>();
            currencycodeHostList = new ArrayList<Integer>();
            bankcodeHostList = new ArrayList<String>();
            terminalHostList = new ArrayList<String>();
            transtypeHostList = new ArrayList<String>();
            networktypeHostList = new ArrayList<String>();

            pendingEBC = new ArrayList<EBCEntity>();
            pendingSwitch = new ArrayList<SwitchEntity>();
            pendingHost = new ArrayList<HostEntity>();
        } catch (SQLException ex) {
            Logger.getLogger(TransactionBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

// <editor-fold defaultstate="collapsed" desc="Variables">
    private Hashtable<String, IndexedCollection<EBCEntity>> AllSidesEBC;
    private Hashtable<String, IndexedCollection<SwitchEntity>> AllSidesSwitch;
    private Hashtable<String, IndexedCollection<HostEntity>> AllSidesHost;
    private JdbcTemplate jdbcTemplate;
    private SearchingCriteria transSearchCriteria;
    private SearchOptions transSearchOptions;
    private Boolean txdatetimeSwitchStatus, SettelmentdateSwitchStatus, authSwitchStatus,
            AcquireramountSwitchStatus, SettelmentamountSwitchStatus, TerminalamountSwitchStatus,
            txdatetimeHostStatus, SettelmentdateHostStatus, AuthHostStatus,
            TransactionamountHostStatus, txdatetimeEBCStatus, AuthEBCStatus, TransactionamountEBCStatus,
            FeesamountEBCStatus, TerminalSwitchStatus, BankCodeSwitchStatus, TranstypeSwitchStatus,
            PanSwitchStatus, rrnSwitchStatus, TranstypeHostStatus, NetworktypeHostStatus, TerminalHostStatus,
            BankCodeHostStatus, PanHostStatus, terminalEBCStatus, PanEBCStatus, rrnEBCStatus,
            uniquereferencenumberEBCStatus, ledgereffectEBCStatus, actioncodeEBCStatus, toPanEBCStatus,
            OctrrnEBCStatus, cardacceptorterminalidEBCStatus, CardacceptoridEBCStatus, accountOperatorHostStatus,
            transactionamountSwitchStatus, accountOperatorSwitchStatus, networktypeSwitchStatus, acquirerSwitchStatus;
    private List<EBCEntity> pendingEBC;
    private List<SwitchEntity> pendingSwitch;
    private List<HostEntity> pendingHost;
    private List<String> terminalEBCList, actioncodeEBCList, ledgereffectEBCList,
            terminalSwitchList, bankcodeSwitchList, transtypeSwitchList, bankcodeHostList, terminalHostList, transtypeHostList,
            networktypeHostList, networktypeSwitchList;
    private List<Integer> responsecodeEBCList, responsecodeSwitchList, currencycodeHostList, acquirerSwitchList;

    private String filterVisibltyebc, transVisibltyebc,
            filterVisibltySwitch, transVisibltySwitch,
            filterVisibltyHost, transVisibltyHost, charVisiblty;
    private String filterDisplayebc, transDisplayebc,
            filterDisplaySwitch, transDisplaySwitch,
            filterDisplayHost, transDisplayHost, charDisplay;
    private String chartSelection;
    private String MSG = "";
    private List<Query<EBCEntity>> userQueriesEBC;
    private List<Query<SwitchEntity>> userQueriesSwitch;
    private List<Query<HostEntity>> userQueriesHost;
    private Boolean pendingtab, disputetab, matchedtab, rejectedtab;
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Getter&Setter">
    public void setRejectedtab(Boolean rejectedtab) {
        this.rejectedtab = rejectedtab;
    }

    public Boolean getAccountOperatorHostStatus() {
        return accountOperatorHostStatus;
    }

    public void setAccountOperatorHostStatus(Boolean accountOperatorHostStatus) {
        this.accountOperatorHostStatus = accountOperatorHostStatus;
    }

    public Hashtable<String, IndexedCollection<EBCEntity>> getAllSidesEBC() {
        return AllSidesEBC;
    }

    public String getFilterVisibltySwitch() {
        return filterVisibltySwitch;
    }

    public void setFilterVisibltySwitch(String filterVisibltySwitch) {
        this.filterVisibltySwitch = filterVisibltySwitch;
    }

    public String getTransVisibltySwitch() {
        return transVisibltySwitch;
    }

    public void setTransVisibltySwitch(String transVisibltySwitch) {
        this.transVisibltySwitch = transVisibltySwitch;
    }

    public String getFilterVisibltyHost() {
        return filterVisibltyHost;
    }

    public void setFilterVisibltyHost(String filterVisibltyHost) {
        this.filterVisibltyHost = filterVisibltyHost;
    }

    public String getTransVisibltyHost() {
        return transVisibltyHost;
    }

    public void setTransVisibltyHost(String transVisibltyHost) {
        this.transVisibltyHost = transVisibltyHost;
    }

    public String getFilterDisplaySwitch() {
        return filterDisplaySwitch;
    }

    public void setFilterDisplaySwitch(String filterDisplaySwitch) {
        this.filterDisplaySwitch = filterDisplaySwitch;
    }

    public String getTransDisplaySwitch() {
        return transDisplaySwitch;
    }

    public void setTransDisplaySwitch(String transDisplaySwitch) {
        this.transDisplaySwitch = transDisplaySwitch;
    }

    public String getFilterDisplayHost() {
        return filterDisplayHost;
    }

    public void setFilterDisplayHost(String filterDisplayHost) {
        this.filterDisplayHost = filterDisplayHost;
    }

    public String getTransDisplayHost() {
        return transDisplayHost;
    }

    public void setTransDisplayHost(String transDisplayHost) {
        this.transDisplayHost = transDisplayHost;
    }

    public List<String> getTerminalSwitchList() {
        return terminalSwitchList;
    }

    public Boolean getTransactionamountSwitchStatus() {
        return transactionamountSwitchStatus;
    }

    public void setTransactionamountSwitchStatus(Boolean transactionamountSwitchStatus) {
        this.transactionamountSwitchStatus = transactionamountSwitchStatus;
    }

    public void setTerminalSwitchList(List<String> terminalSwitchList) {
        this.terminalSwitchList = terminalSwitchList;
    }

    public Boolean getAcquirerSwitchStatus() {
        return acquirerSwitchStatus;
    }

    public void setAcquirerSwitchStatus(Boolean acquirerSwitchStatus) {
        this.acquirerSwitchStatus = acquirerSwitchStatus;
    }

    public List<String> getBankcodeSwitchList() {
        return bankcodeSwitchList;
    }

    public void setBankcodeSwitchList(List<String> bankcodeSwitchList) {
        this.bankcodeSwitchList = bankcodeSwitchList;
    }

    public Boolean getAccountOperatorSwitchStatus() {
        return accountOperatorSwitchStatus;
    }

    public void setAccountOperatorSwitchStatus(Boolean accountOperatorSwitchStatus) {
        this.accountOperatorSwitchStatus = accountOperatorSwitchStatus;
    }

    public List<String> getTranstypeSwitchList() {
        return transtypeSwitchList;
    }

    public void setTranstypeSwitchList(List<String> transtypeSwitchList) {
        this.transtypeSwitchList = transtypeSwitchList;
    }

    public List<String> getBankcodeHostList() {
        return bankcodeHostList;
    }

    public void setBankcodeHostList(List<String> bankcodeHostList) {
        this.bankcodeHostList = bankcodeHostList;
    }

    public List<String> getTerminalHostList() {
        return terminalHostList;
    }

    public void setTerminalHostList(List<String> terminalHostList) {
        this.terminalHostList = terminalHostList;
    }

    public List<String> getTranstypeHostList() {
        return transtypeHostList;
    }

    public void setTranstypeHostList(List<String> transtypeHostList) {
        this.transtypeHostList = transtypeHostList;
    }

    public List<String> getNetworktypeHostList() {
        return networktypeHostList;
    }

    public void setNetworktypeHostList(List<String> networktypeHostList) {
        this.networktypeHostList = networktypeHostList;
    }

    public List<Integer> getResponsecodeSwitchList() {
        return responsecodeSwitchList;
    }

    public void setResponsecodeSwitchList(List<Integer> responsecodeSwitchList) {
        this.responsecodeSwitchList = responsecodeSwitchList;
    }

    public List<Integer> getCurrencycodeHostList() {
        return currencycodeHostList;
    }

    public Boolean getNetworktypeSwitchStatus() {
        return networktypeSwitchStatus;
    }

    public void setNetworktypeSwitchStatus(Boolean networktypeSwitchStatus) {
        this.networktypeSwitchStatus = networktypeSwitchStatus;
    }

    public void setCurrencycodeHostList(List<Integer> currencycodeHostList) {
        this.currencycodeHostList = currencycodeHostList;
    }

    public void setAllSidesEBC(Hashtable<String, IndexedCollection<EBCEntity>> AllSidesEBC) {
        this.AllSidesEBC = AllSidesEBC;
    }

    public Hashtable<String, IndexedCollection<SwitchEntity>> getAllSidesSwitch() {
        return AllSidesSwitch;
    }

    public void setAllSidesSwitch(Hashtable<String, IndexedCollection<SwitchEntity>> AllSidesSwitch) {
        this.AllSidesSwitch = AllSidesSwitch;
    }

    public Hashtable<String, IndexedCollection<HostEntity>> getAllSidesHost() {
        return AllSidesHost;
    }

    public void setAllSidesHost(Hashtable<String, IndexedCollection<HostEntity>> AllSidesHost) {
        this.AllSidesHost = AllSidesHost;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public SearchingCriteria getTransSearchCriteria() {
        return transSearchCriteria;
    }

    public void setTransSearchCriteria(SearchingCriteria transSearchCriteria) {
        this.transSearchCriteria = transSearchCriteria;
    }

    public SearchOptions getTransSearchOptions() {
        return transSearchOptions;
    }

    public void setTransSearchOptions(SearchOptions transSearchOptions) {
        this.transSearchOptions = transSearchOptions;
    }

    public Boolean getTxdatetimeSwitchStatus() {
        return txdatetimeSwitchStatus;
    }

    public void setTxdatetimeSwitchStatus(Boolean txdatetimeSwitchStatus) {
        this.txdatetimeSwitchStatus = txdatetimeSwitchStatus;
    }

    public Boolean getSettelmentdateSwitchStatus() {
        return SettelmentdateSwitchStatus;
    }

    public void setSettelmentdateSwitchStatus(Boolean SettelmentdateSwitchStatus) {
        this.SettelmentdateSwitchStatus = SettelmentdateSwitchStatus;
    }

    public Boolean getAuthSwitchStatus() {
        return authSwitchStatus;
    }

    public void setAuthSwitchStatus(Boolean authSwitchStatus) {
        this.authSwitchStatus = authSwitchStatus;
    }

    public Boolean getAcquireramountSwitchStatus() {
        return AcquireramountSwitchStatus;
    }

    public void setAcquireramountSwitchStatus(Boolean AcquireramountSwitchStatus) {
        this.AcquireramountSwitchStatus = AcquireramountSwitchStatus;
    }

    public Boolean getSettelmentamountSwitchStatus() {
        return SettelmentamountSwitchStatus;
    }

    public void setSettelmentamountSwitchStatus(Boolean SettelmentamountSwitchStatus) {
        this.SettelmentamountSwitchStatus = SettelmentamountSwitchStatus;
    }

    public Boolean getTerminalamountSwitchStatus() {
        return TerminalamountSwitchStatus;
    }

    public void setTerminalamountSwitchStatus(Boolean TerminalamountSwitchStatus) {
        this.TerminalamountSwitchStatus = TerminalamountSwitchStatus;
    }

    public Boolean getTxdatetimeHostStatus() {
        return txdatetimeHostStatus;
    }

    public void setTxdatetimeHostStatus(Boolean txdatetimeHostStatus) {
        this.txdatetimeHostStatus = txdatetimeHostStatus;
    }

    public Boolean getSettelmentdateHostStatus() {
        return SettelmentdateHostStatus;
    }

    public void setSettelmentdateHostStatus(Boolean SettelmentdateHostStatus) {
        this.SettelmentdateHostStatus = SettelmentdateHostStatus;
    }

    public Boolean getAuthHostStatus() {
        return AuthHostStatus;
    }

    public void setAuthHostStatus(Boolean AuthHostStatus) {
        this.AuthHostStatus = AuthHostStatus;
    }

    public Boolean getTransactionamountHostStatus() {
        return TransactionamountHostStatus;
    }

    public void setTransactionamountHostStatus(Boolean TransactionamountHostStatus) {
        this.TransactionamountHostStatus = TransactionamountHostStatus;
    }

    public Boolean getTxdatetimeEBCStatus() {
        return txdatetimeEBCStatus;
    }

    public void setTxdatetimeEBCStatus(Boolean txdatetimeEBCStatus) {
        this.txdatetimeEBCStatus = txdatetimeEBCStatus;
    }

    public Boolean getAuthEBCStatus() {
        return AuthEBCStatus;
    }

    public void setAuthEBCStatus(Boolean AuthEBCStatus) {
        this.AuthEBCStatus = AuthEBCStatus;
    }

    public Boolean getTransactionamountEBCStatus() {
        return TransactionamountEBCStatus;
    }

    public void setTransactionamountEBCStatus(Boolean TransactionamountEBCStatus) {
        this.TransactionamountEBCStatus = TransactionamountEBCStatus;
    }

    public Boolean getFeesamountEBCStatus() {
        return FeesamountEBCStatus;
    }

    public void setFeesamountEBCStatus(Boolean FeesamountEBCStatus) {
        this.FeesamountEBCStatus = FeesamountEBCStatus;
    }

    public Boolean getTerminalSwitchStatus() {
        return TerminalSwitchStatus;
    }

    public void setTerminalSwitchStatus(Boolean TerminalSwitchStatus) {
        this.TerminalSwitchStatus = TerminalSwitchStatus;
    }

    public Boolean getBankCodeSwitchStatus() {
        return BankCodeSwitchStatus;
    }

    public void setBankCodeSwitchStatus(Boolean BankCodeSwitchStatus) {
        this.BankCodeSwitchStatus = BankCodeSwitchStatus;
    }

    public Boolean getTranstypeSwitchStatus() {
        return TranstypeSwitchStatus;
    }

    public void setTranstypeSwitchStatus(Boolean TranstypeSwitchStatus) {
        this.TranstypeSwitchStatus = TranstypeSwitchStatus;
    }

    public Boolean getPanSwitchStatus() {
        return PanSwitchStatus;
    }

    public void setPanSwitchStatus(Boolean PanSwitchStatus) {
        this.PanSwitchStatus = PanSwitchStatus;
    }

    public Boolean getRrnSwitchStatus() {
        return rrnSwitchStatus;
    }

    public void setRrnSwitchStatus(Boolean rrnSwitchStatus) {
        this.rrnSwitchStatus = rrnSwitchStatus;
    }

    public Boolean getTranstypeHostStatus() {
        return TranstypeHostStatus;
    }

    public void setTranstypeHostStatus(Boolean TranstypeHostStatus) {
        this.TranstypeHostStatus = TranstypeHostStatus;
    }

    public Boolean getNetworktypeHostStatus() {
        return NetworktypeHostStatus;
    }

    public void setNetworktypeHostStatus(Boolean NetworktypeHostStatus) {
        this.NetworktypeHostStatus = NetworktypeHostStatus;
    }

    public Boolean getTerminalHostStatus() {
        return TerminalHostStatus;
    }

    public void setTerminalHostStatus(Boolean TerminalHostStatus) {
        this.TerminalHostStatus = TerminalHostStatus;
    }

    public Boolean getBankCodeHostStatus() {
        return BankCodeHostStatus;
    }

    public void setBankCodeHostStatus(Boolean BankCodeHostStatus) {
        this.BankCodeHostStatus = BankCodeHostStatus;
    }

    public Boolean getPanHostStatus() {
        return PanHostStatus;
    }

    public void setPanHostStatus(Boolean PanHostStatus) {
        this.PanHostStatus = PanHostStatus;
    }

    public Boolean getTerminalEBCStatus() {
        return terminalEBCStatus;
    }

    public void setTerminalEBCStatus(Boolean terminalEBCStatus) {
        this.terminalEBCStatus = terminalEBCStatus;
    }

    public Boolean getPanEBCStatus() {
        return PanEBCStatus;
    }

    public void setPanEBCStatus(Boolean PanEBCStatus) {
        this.PanEBCStatus = PanEBCStatus;
    }

    public Boolean getRrnEBCStatus() {
        return rrnEBCStatus;
    }

    public void setRrnEBCStatus(Boolean rrnEBCStatus) {
        this.rrnEBCStatus = rrnEBCStatus;
    }

    public Boolean getUniquereferencenumberEBCStatus() {
        return uniquereferencenumberEBCStatus;
    }

    public void setUniquereferencenumberEBCStatus(Boolean uniquereferencenumberEBCStatus) {
        this.uniquereferencenumberEBCStatus = uniquereferencenumberEBCStatus;
    }

    public List<Integer> getAcquirerSwitchList() {
        return acquirerSwitchList;
    }

    public void setAcquirerSwitchList(List<Integer> acquirerSwitchList) {
        this.acquirerSwitchList = acquirerSwitchList;
    }

    public Boolean getLedgereffectEBCStatus() {
        return ledgereffectEBCStatus;
    }

    public void setLedgereffectEBCStatus(Boolean ledgereffectEBCStatus) {
        this.ledgereffectEBCStatus = ledgereffectEBCStatus;
    }

    public Boolean getActioncodeEBCStatus() {
        return actioncodeEBCStatus;
    }

    public void setActioncodeEBCStatus(Boolean actioncodeEBCStatus) {
        this.actioncodeEBCStatus = actioncodeEBCStatus;
    }

    public Boolean getToPanEBCStatus() {
        return toPanEBCStatus;
    }

    public void setToPanEBCStatus(Boolean toPanEBCStatus) {
        this.toPanEBCStatus = toPanEBCStatus;
    }

    public Boolean getOctrrnEBCStatus() {
        return OctrrnEBCStatus;
    }

    public void setOctrrnEBCStatus(Boolean OctrrnEBCStatus) {
        this.OctrrnEBCStatus = OctrrnEBCStatus;
    }

    public Boolean getCardacceptorterminalidEBCStatus() {
        return cardacceptorterminalidEBCStatus;
    }

    public void setCardacceptorterminalidEBCStatus(Boolean cardacceptorterminalidEBCStatus) {
        this.cardacceptorterminalidEBCStatus = cardacceptorterminalidEBCStatus;
    }

    public Boolean getCardacceptoridEBCStatus() {
        return CardacceptoridEBCStatus;
    }

    public List<String> getNetworktypeSwitchList() {
        return networktypeSwitchList;
    }

    public void setNetworktypeSwitchList(List<String> networktypeSwitchList) {
        this.networktypeSwitchList = networktypeSwitchList;
    }

    public void setCardacceptoridEBCStatus(Boolean CardacceptoridEBCStatus) {
        this.CardacceptoridEBCStatus = CardacceptoridEBCStatus;
    }

    public List<EBCEntity> getPendingEBC() {
        return pendingEBC;
    }

    public void setPendingEBC(List<EBCEntity> pendingEBC) {
        this.pendingEBC = pendingEBC;
    }

    public List<SwitchEntity> getPendingSwitch() {
        return pendingSwitch;
    }

    public void setPendingSwitch(List<SwitchEntity> pendingSwitch) {
        this.pendingSwitch = pendingSwitch;
    }

    public List<HostEntity> getPendingHost() {
        return pendingHost;
    }

    public void setPendingHost(List<HostEntity> pendingHost) {
        this.pendingHost = pendingHost;
    }

    public List<String> getTerminalEBCList() {
        return terminalEBCList;
    }

    public void setTerminalEBCList(List<String> terminalEBCList) {
        this.terminalEBCList = terminalEBCList;
    }

    public List<String> getActioncodeEBCList() {
        return actioncodeEBCList;
    }

    public void setActioncodeEBCList(List<String> actioncodeEBCList) {
        this.actioncodeEBCList = actioncodeEBCList;
    }

    public List<String> getLedgereffectEBCList() {
        return ledgereffectEBCList;
    }

    public void setLedgereffectEBCList(List<String> ledgereffectEBCList) {
        this.ledgereffectEBCList = ledgereffectEBCList;
    }

    public List<Integer> getResponsecodeEBCList() {
        return responsecodeEBCList;
    }

    public void setResponsecodeEBCList(List<Integer> responsecodeEBCList) {
        this.responsecodeEBCList = responsecodeEBCList;
    }

    public String getCharVisiblty() {
        return charVisiblty;
    }

    public void setCharVisiblty(String charVisiblty) {
        this.charVisiblty = charVisiblty;
    }

    public String getFilterVisibltyebc() {
        return filterVisibltyebc;
    }

    public void setFilterVisibltyebc(String filterVisibltyebc) {
        this.filterVisibltyebc = filterVisibltyebc;
    }

    public String getTransVisibltyebc() {
        return transVisibltyebc;
    }

    public void setTransVisibltyebc(String transVisibltyebc) {
        this.transVisibltyebc = transVisibltyebc;
    }

    public String getFilterDisplayebc() {
        return filterDisplayebc;
    }

    public void setFilterDisplayebc(String filterDisplayebc) {
        this.filterDisplayebc = filterDisplayebc;
    }

    public String getTransDisplayebc() {
        return transDisplayebc;
    }

    public void setTransDisplayebc(String transDisplayebc) {
        this.transDisplayebc = transDisplayebc;
    }

    public String getCharDisplay() {
        return charDisplay;
    }

    public void setCharDisplay(String charDisplay) {
        this.charDisplay = charDisplay;
    }

    public String getChartSelection() {
        return chartSelection;
    }

    public void setChartSelection(String chartSelection) {
        this.chartSelection = chartSelection;
    }

    public String getMSG() {
        return MSG;
    }

    public void setMSG(String MSG) {
        this.MSG = MSG;
    }

    public List<Query<EBCEntity>> getUserQueriesEBC() {
        return userQueriesEBC;
    }

    public void setUserQueriesEBC(List<Query<EBCEntity>> userQueriesEBC) {
        this.userQueriesEBC = userQueriesEBC;
    }

    public List<Query<SwitchEntity>> getUserQueriesSwitch() {
        return userQueriesSwitch;
    }

    public void setUserQueriesSwitch(List<Query<SwitchEntity>> userQueriesSwitch) {
        this.userQueriesSwitch = userQueriesSwitch;
    }

    public List<Query<HostEntity>> getUserQueriesHost() {
        return userQueriesHost;
    }

    public void setUserQueriesHost(List<Query<HostEntity>> userQueriesHost) {
        this.userQueriesHost = userQueriesHost;
    }

    public Boolean getPendingtab() {
        return pendingtab;
    }

    public void setPendingtab(Boolean pendingtab) {
        this.pendingtab = pendingtab;
    }

    public Boolean getDisputetab() {
        return disputetab;
    }

    public void setDisputetab(Boolean disputetab) {
        this.disputetab = disputetab;
    }

    public Boolean getMatchedtab() {
        return matchedtab;
    }

    public void setMatchedtab(Boolean matchedtab) {
        this.matchedtab = matchedtab;
    }

    public Boolean getRejectedtab() {
        return rejectedtab;
    }
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Operators Listner">
    public void txdatetimeSwitchOperatorChange(ValueChangeEvent e) {
        if (!"between".equals(e.getNewValue().toString())) {
            txdatetimeSwitchStatus = Boolean.TRUE;
        } else {
            txdatetimeSwitchStatus = Boolean.FALSE;
        }
    }

    public void settelmentdateSwitchOperatorChange(ValueChangeEvent e) {
        if (!"between".equals(e.getNewValue().toString())) {
            SettelmentdateSwitchStatus = Boolean.TRUE;
        } else {
            SettelmentdateSwitchStatus = Boolean.FALSE;
        }
    }

    public void txdatetimeHostOperatorChange(ValueChangeEvent e) {
        if (!"between".equals(e.getNewValue().toString())) {
            txdatetimeHostStatus = Boolean.TRUE;
        } else {
            txdatetimeHostStatus = Boolean.FALSE;
        }
    }

    public void settelmentdateHostOperatorChange(ValueChangeEvent e) {
        if (!"between".equals(e.getNewValue().toString())) {
            SettelmentdateHostStatus = Boolean.TRUE;
        } else {
            SettelmentdateHostStatus = Boolean.FALSE;
        }
    }

    public void txdatetimeEBCOperatorChange(ValueChangeEvent e) {
        if (!"between".equals(e.getNewValue().toString())) {
            txdatetimeEBCStatus = Boolean.TRUE;
        } else {
            txdatetimeEBCStatus = Boolean.FALSE;
        }
    }

    public void accountOperatorSwitchOperatorChange() {
        if (transSearchCriteria.getAccountOperator().contains("is")) {
            accountOperatorSwitchStatus = Boolean.TRUE;
        } else {
            accountOperatorSwitchStatus = Boolean.FALSE;
        }
    }

    public void networktypeSwitchOperatorChange() {
        if (transSearchCriteria.getNetworktypeOperator().contains("is")) {
            networktypeSwitchStatus = Boolean.TRUE;
        } else {
            networktypeSwitchStatus = Boolean.FALSE;
        }
    }

    public void acquireramountSwitchOperatorChange() {
        if (!"between".equals(transSearchCriteria.getAcquireramountOperator())) {
            AcquireramountSwitchStatus = Boolean.TRUE;
        } else {
            AcquireramountSwitchStatus = Boolean.FALSE;
        }
    }

    public void settelmentamountSwitchOperatorChange() {
        if (!"between".equals(transSearchCriteria.getSettelmentamountOperator())) {
            SettelmentamountSwitchStatus = Boolean.TRUE;
        } else {
            SettelmentamountSwitchStatus = Boolean.FALSE;
        }
    }

    public void transactionamountSwitchOperatorChange() {
        if (!"between".equals(transSearchCriteria.getTransactionamountOperator())) {
            transactionamountSwitchStatus = Boolean.TRUE;
        } else {
            transactionamountSwitchStatus = Boolean.FALSE;
        }
    }

    public void terminalamountSwitchOperatorChange() {
        if (!"between".equals(transSearchCriteria.getTerminalOperator())) {
            TerminalamountSwitchStatus = Boolean.TRUE;
        } else {
            TerminalamountSwitchStatus = Boolean.FALSE;
        }
    }

    public void transactionamountHostOperatorChange() {
        if (!"between".equals(transSearchCriteria.getTransactionamountOperator())) {
            TransactionamountHostStatus = Boolean.TRUE;
        } else {
            TransactionamountHostStatus = Boolean.FALSE;
        }
    }

    public void bankCodeHostOperatorChange() {
        if (transSearchCriteria.getBankcodeOperator().contains("is")) {
            BankCodeHostStatus = Boolean.TRUE;
        } else {
            BankCodeHostStatus = Boolean.FALSE;
        }
    }

    public void transactionamountEBCOperatorChange() {
        if (!"between".equals(transSearchCriteria.getTransactionamountOperator())) {
            TransactionamountEBCStatus = Boolean.TRUE;
        } else {
            TransactionamountEBCStatus = Boolean.FALSE;
        }
    }

    public void feesamountEBCOperatorChange() {
        if (!"between".equals(transSearchCriteria.getFeesamountOperator())) {
            FeesamountEBCStatus = Boolean.TRUE;
        } else {
            FeesamountEBCStatus = Boolean.FALSE;
        }
    }

    public void authSwitchOperatorChange() {
        if (!"between".equals(transSearchCriteria.getAuthOperator())) {
            authSwitchStatus = Boolean.TRUE;
        } else {
            authSwitchStatus = Boolean.FALSE;
        }
    }

    public void authHostOperatorChange() {
        if (!"between".equals(transSearchCriteria.getAuthOperator())) {
            AuthHostStatus = Boolean.TRUE;
        } else {
            AuthHostStatus = Boolean.FALSE;
        }
    }

    public void authEBCOperatorChange() {
        if (!"between".equals(transSearchCriteria.getAuthOperator())) {
            AuthEBCStatus = Boolean.TRUE;
        } else {
            AuthEBCStatus = Boolean.FALSE;
        }
    }

    public void terminalSwitchOperatorChange() {
        if (transSearchCriteria.getTerminalOperator().contains("is")) {
            TerminalSwitchStatus = Boolean.TRUE;
        } else {
            TerminalSwitchStatus = Boolean.FALSE;
        }
    }

    public void bankCodeSwitchOperatorChange() {
        if (transSearchCriteria.getBankcodeOperator().contains("is")) {
            BankCodeSwitchStatus = Boolean.TRUE;
        } else {
            BankCodeSwitchStatus = Boolean.FALSE;
        }
    }

    public void transtypeSwitchOperatorChange() {
        if (transSearchCriteria.getTranstypeOperator().contains("is")) {
            TranstypeSwitchStatus = Boolean.TRUE;
        } else {
            TranstypeSwitchStatus = Boolean.FALSE;
        }
    }

    public void panSwitchOperatorChange() {
        if (transSearchCriteria.getPanOperator().contains("is")) {
            PanSwitchStatus = Boolean.TRUE;
        } else {
            PanSwitchStatus = Boolean.FALSE;
        }
    }

    public void acquirerSwitchOperatorChange() {
        if (transSearchCriteria.getAcquirerOperator().contains("is")) {
            acquirerSwitchStatus = Boolean.TRUE;
        } else {
            acquirerSwitchStatus = Boolean.FALSE;
        }
    }

    public void accountOperatorHostOperatorChange() {
        if (transSearchCriteria.getAccountOperator().contains("is")) {
            accountOperatorHostStatus = Boolean.TRUE;
        } else {
            accountOperatorHostStatus = Boolean.FALSE;
        }
    }

    public void rrnSwitchOperatorChange() {
        if (transSearchCriteria.getRrnOperator().contains("is")) {
            rrnSwitchStatus = Boolean.TRUE;
        } else {
            rrnSwitchStatus = Boolean.FALSE;
        }
    }

    public void transtypeHostOperatorChange() {
        if (transSearchCriteria.getTranstypeOperator().contains("is")) {
            TranstypeHostStatus = Boolean.TRUE;
        } else {
            TranstypeHostStatus = Boolean.FALSE;
        }
    }

    public void networktypeHostOperatorChange() {
        if (transSearchCriteria.getNetworktypeOperator().contains("is")) {
            NetworktypeHostStatus = Boolean.TRUE;
        } else {
            NetworktypeHostStatus = Boolean.FALSE;
        }
    }

    public void terminalHostOperatorChange() {
        if (transSearchCriteria.getTerminalOperator().contains("is")) {
            TerminalHostStatus = Boolean.TRUE;
        } else {
            TerminalHostStatus = Boolean.FALSE;
        }
    }

    public void panHostOperatorChange() {
        if (transSearchCriteria.getPanOperator().contains("is")) {
            PanHostStatus = Boolean.TRUE;
        } else {
            PanHostStatus = Boolean.FALSE;
        }
    }

    public void terminalEBCOperatorChange() {
        if (transSearchCriteria.getTerminalOperator().contains("is")) {
            terminalEBCStatus = Boolean.TRUE;
        } else {
            terminalEBCStatus = Boolean.FALSE;
        }
    }

    public void panEBCOperatorChange() {
        if (transSearchCriteria.getPanOperator().contains("is")) {
            PanEBCStatus = Boolean.TRUE;
        } else {
            PanEBCStatus = Boolean.FALSE;
        }
    }

    public void rrnEBCOperatorChange() {
        if (transSearchCriteria.getRrnOperator().contains("is")) {
            rrnEBCStatus = Boolean.TRUE;
        } else {
            rrnEBCStatus = Boolean.FALSE;
        }
    }

    public void uniquereferencenumberEBCOperatorChange() {
        if (transSearchCriteria.getUniquereferencenumberOperator().contains("is")) {
            uniquereferencenumberEBCStatus = Boolean.TRUE;
        } else {
            uniquereferencenumberEBCStatus = Boolean.FALSE;
        }
    }

    public void ledgereffectEBCOperatorChange() {
        if (transSearchCriteria.getLedgereffectOperator().contains("is")) {
            ledgereffectEBCStatus = Boolean.TRUE;
        } else {
            ledgereffectEBCStatus = Boolean.FALSE;
        }
    }

    public void actioncodeEBCOperatorChange() {
        if (transSearchCriteria.getActioncodeOperator().contains("is")) {
            actioncodeEBCStatus = Boolean.TRUE;
        } else {
            actioncodeEBCStatus = Boolean.FALSE;
        }
    }

    public void toPanEBCOperatorChange() {
        if (transSearchCriteria.getTopanOperator().contains("is")) {
            toPanEBCStatus = Boolean.TRUE;
        } else {
            toPanEBCStatus = Boolean.FALSE;
        }
    }

    public void octrrnEBCOperatorChange() {
        if (transSearchCriteria.getOctrrnOperator().contains("is")) {
            OctrrnEBCStatus = Boolean.TRUE;
        } else {
            OctrrnEBCStatus = Boolean.FALSE;
        }
    }

    public void cardacceptorterminalidEBCOperatorChange() {
        if (transSearchCriteria.getCardacceptorterminalidOperator().contains("is")) {
            cardacceptorterminalidEBCStatus = Boolean.TRUE;
        } else {
            cardacceptorterminalidEBCStatus = Boolean.FALSE;
        }
    }

    public void cardacceptoridEBCOperatorChange() {
        if (transSearchCriteria.getCardacceptoridcodeOperator().contains("is")) {
            CardacceptoridEBCStatus = Boolean.TRUE;
        } else {
            CardacceptoridEBCStatus = Boolean.FALSE;
        }
    }
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Lookps">
    private void getEBCLookups() {
        LedgereffectEBCDistinct();
        actioncodeEBCDistinct();
        terminalEBCDistinct();
        responsecodeEBCDistinct();
    }

    private void getSwitchLookups() {
        terminalSwitchDistinct();
        bankcodeSwitchDistinct();
        transtypeSwitchDistinct();
        responsecodeSwitchDistinct();
        networktypeSwitchDistinct();
        acquirerSwitchDistinct();
    }

    private void getHostLookups() {
        bankcodeHostDistinct();
        terminalHostDistinct();
        transtypeHostDistinct();
        networktypeHostDistinct();
        currencycodeHostDistinct();
    }

    private void acquirerSwitchDistinct() {
        acquirerSwitchList = new ArrayList(pendingSwitch.stream().map(SwitchEntity::getAcquirer)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void LedgereffectEBCDistinct() {
        ledgereffectEBCList = new ArrayList(pendingEBC.stream().map(EBCEntity::getLedgereffect)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void actioncodeEBCDistinct() {
        actioncodeEBCList = new ArrayList(pendingEBC.stream().map(EBCEntity::getActioncode)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void terminalEBCDistinct() {
        terminalEBCList = new ArrayList(pendingEBC.stream().map(EBCEntity::getTerminal)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void responsecodeEBCDistinct() {
        responsecodeEBCList = new ArrayList(pendingEBC.stream().map(EBCEntity::getResponsecode)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void terminalSwitchDistinct() {
        terminalSwitchList = new ArrayList(pendingSwitch.stream().map(SwitchEntity::getTerminal)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void bankcodeSwitchDistinct() {
        bankcodeSwitchList = new ArrayList(pendingSwitch.stream().map(SwitchEntity::getBankcode)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void transtypeSwitchDistinct() {
        transtypeSwitchList = new ArrayList(pendingSwitch.stream().map(SwitchEntity::getTranstype)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void networktypeSwitchDistinct() {
        networktypeSwitchList = new ArrayList(pendingHost.stream().map(HostEntity::getNetworktype)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void responsecodeSwitchDistinct() {
        responsecodeSwitchList = new ArrayList(pendingSwitch.stream().map(SwitchEntity::getResponsecode)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void bankcodeHostDistinct() {
        bankcodeHostList = new ArrayList(pendingHost.stream().map(HostEntity::getBankcode)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void terminalHostDistinct() {
        terminalHostList = new ArrayList(pendingHost.stream().map(HostEntity::getTerminal)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void transtypeHostDistinct() {
        transtypeHostList = new ArrayList(pendingHost.stream().map(HostEntity::getTranstype)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void networktypeHostDistinct() {
        networktypeHostList = new ArrayList(pendingHost.stream().map(HostEntity::getNetworktype)
                .distinct()
                .collect(Collectors.toSet()));
    }

    private void currencycodeHostDistinct() {
        currencycodeHostList = new ArrayList(pendingHost.stream().map(HostEntity::getCurrencycode)
                .distinct()
                .collect(Collectors.toSet()));
    }
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Filters">
    private void ApplyFiltersHost() {
        AddAuthFilterHost();
        AddPanFilterHost();
        AddTransactionamountFilterHOST();
        AddSettelmentdateFilterHost();
        AddtxdatetimeFilterHost();
        AddBankCodeFilterHost();
        AddTerminalFilterHost();
        AddAccountnumberFilterHost();
        AddNetworktypeFilterHost();
        AddTranstypeFilterHost();
        AddCurrencycodeFilterHost();
        AddTraceFilterHost();
    }

    private void ApplyFiltersEBC() {
        AddresponsecodeFilterEBC();
        AddterminalFilterEBC();
        AddpanFilterEBC();
        AddAuthFilterEBC();
        AddtxdatetimeFilterEBC();
        AddTransactionamountFilterEBC();
        AddrrnFilterEBC();
        AdduniquereferencenumberFilterEBC();
        AddledgereffectFilterEBC();
        AddactioncodeFilterEBC();
        AddtoPanFilterEBC();
        AddOctrrnFilterEBC();
        AddcardacceptorterminalidFilterEBC();
        AddCardacceptoridFilterEBC();
        AddFeesamountFilterEBC();
    }

    private void ApplyFiltersSwitch() {
        AddAcquireramountFilterSwitch();
        AddSettelmentamountFilterSwitch();
        AddtxdatetimeFilterSwitch();
        AddTerminalFilterSwitch();
        AddTerminalamountFilterSwitch();
        AddBankCodeFilterSwitch();
        AddAcquirercodeFilterSwitch();
        AddAcquirerFilterSwitch();
        AddSettelmentcodeFilterSwitch();
        AddTranstypeFilterSwitch();
        AddresponseTypeFilterSwitch();
        AddPanFilterSwitch();
        AddTraceFilterSwitch();
        AddrrnFilterSwitch();
        AddSettelmentdateFilterSwitch();
        AddAuthFilterSwitch();
        AddAccountnumberFilterSwitch();
    }

    private void AddAcquireramountFilterSwitch() {
        if (transSearchCriteria.getAcquireramountOperator() != null) {
            switch (transSearchCriteria.getAcquireramountOperator().toLowerCase()) {
                case "between":
                    if (transSearchCriteria.getAcquireramountFrom() != null && transSearchCriteria.getAcquireramountTo() != null) {
                        userQueriesSwitch.add(between(SwitchEntity.ACQUIRERAMOUNT, transSearchCriteria.getAcquireramountFrom(), transSearchCriteria.getAcquireramountTo()));
                    }
                    break;
                case "not equal":
                    if (transSearchCriteria.getAcquireramountFrom() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.ACQUIRERAMOUNT, transSearchCriteria.getAcquireramountFrom())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getAcquireramountFrom() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.ACQUIRERAMOUNT, transSearchCriteria.getAcquireramountFrom()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getAcquireramountFrom() != null) {
                        userQueriesSwitch.add(greaterThan(SwitchEntity.ACQUIRERAMOUNT, transSearchCriteria.getAcquireramountFrom()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getAcquireramountFrom() != null) {
                        userQueriesSwitch.add(lessThan(SwitchEntity.ACQUIRERAMOUNT, transSearchCriteria.getAcquireramountFrom()));
                    }
                    break;
            }
        }
    }

    private void AddSettelmentamountFilterSwitch() {
        if (transSearchCriteria.getSettelmentamountOperator() != null) {
            switch (transSearchCriteria.getSettelmentamountOperator().toLowerCase()) {
                case "between":
                    if (transSearchCriteria.getSettelmentamountFrom() != null && transSearchCriteria.getSettelmentamountTo() != null) {
                        userQueriesSwitch.add(between(SwitchEntity.SETTELMENTAMOUNT, transSearchCriteria.getSettelmentamountFrom(), transSearchCriteria.getSettelmentamountTo()));
                    }
                    break;
                case "not equal":
                    if (transSearchCriteria.getSettelmentamountFrom() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.SETTELMENTAMOUNT, transSearchCriteria.getSettelmentamountFrom())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getSettelmentamountFrom() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.SETTELMENTAMOUNT, transSearchCriteria.getSettelmentamountFrom()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getSettelmentamountFrom() != null) {
                        userQueriesSwitch.add(greaterThan(SwitchEntity.SETTELMENTAMOUNT, transSearchCriteria.getSettelmentamountFrom()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getSettelmentamountFrom() != null) {
                        userQueriesSwitch.add(lessThan(SwitchEntity.SETTELMENTAMOUNT, transSearchCriteria.getSettelmentamountFrom()));
                    }
                    break;
            }
        }
    }

    private void AddtxdatetimeFilterSwitch() {
        if (transSearchCriteria.getTxdatetimeOperator() != null) {
            switch (transSearchCriteria.getTxdatetimeOperator().toLowerCase()) {
                case "between":
                    if (transSearchCriteria.getTxdatetimeFrom() != null && transSearchCriteria.getTxdatetimeTo() != null) {
                        userQueriesSwitch.add(between(SwitchEntity.TXDATETIME, transSearchCriteria.getTxdatetimeFrom(), transSearchCriteria.getTxdatetimeTo()));
                    }
                    break;
                case "not equal":
                    if (transSearchCriteria.getTxdatetimeFrom() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.TXDATETIME, transSearchCriteria.getTxdatetimeFrom())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTxdatetimeFrom() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.TXDATETIME, transSearchCriteria.getTxdatetimeFrom()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getTxdatetimeFrom() != null) {
                        userQueriesSwitch.add(greaterThan(SwitchEntity.TXDATETIME, transSearchCriteria.getTxdatetimeFrom()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getTxdatetimeFrom() != null) {
                        userQueriesSwitch.add(lessThan(SwitchEntity.TXDATETIME, transSearchCriteria.getTxdatetimeFrom()));
                    }
                    break;
            }
        }
    }

    private void AddTerminalFilterSwitch() {
        if (transSearchCriteria.getTerminalOperator() != null) {
            switch (transSearchCriteria.getTerminalOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getTerminal() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.TERMINAL, transSearchCriteria.getTerminal())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTerminal() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.TERMINAL, transSearchCriteria.getTerminal()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getTerminal() != null) {
                        userQueriesSwitch.add(contains(SwitchEntity.TERMINAL, transSearchCriteria.getTerminal()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getTerminal() != null) {
                        userQueriesSwitch.add(not(contains(SwitchEntity.TERMINAL, transSearchCriteria.getTerminal())));
                    }
                    break;
                case "isnull":
                    userQueriesSwitch.add(has(SwitchEntity.TERMINAL));
                    break;
                case "isnotnull":
                    userQueriesSwitch.add(not(has(SwitchEntity.TERMINAL)));
                    break;
            }
        }
    }

    private void AddTerminalamountFilterSwitch() {
        if (transSearchCriteria.getTransactionamountOperator() != null) {
            switch (transSearchCriteria.getTransactionamountOperator().toLowerCase()) {
                case "between":
                    if (transSearchCriteria.getTransactionamountFrom() != null && transSearchCriteria.getTransactionamountTo() != null) {
                        userQueriesSwitch.add(between(SwitchEntity.TERMINALAMOUNT, transSearchCriteria.getTransactionamountFrom(), transSearchCriteria.getTransactionamountTo()));
                    }
                    break;
                case "not equal":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.TERMINALAMOUNT, transSearchCriteria.getTransactionamountFrom())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.TERMINALAMOUNT, transSearchCriteria.getTransactionamountFrom()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesSwitch.add(greaterThan(SwitchEntity.TERMINALAMOUNT, transSearchCriteria.getTransactionamountFrom()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesSwitch.add(lessThan(SwitchEntity.TERMINALAMOUNT, transSearchCriteria.getTransactionamountFrom()));
                    }
                    break;
            }
        }
    }

    private void AddBankCodeFilterSwitch() {
        if (transSearchCriteria.getBankcodeOperator() != null) {
            switch (transSearchCriteria.getBankcodeOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getBankcode() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.BANKCODE, transSearchCriteria.getBankcode())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getBankcode() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.BANKCODE, transSearchCriteria.getBankcode()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getBankcode() != null) {
                        userQueriesSwitch.add(contains(SwitchEntity.BANKCODE, transSearchCriteria.getBankcode()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getBankcode() != null) {
                        userQueriesSwitch.add(not(contains(SwitchEntity.BANKCODE, transSearchCriteria.getBankcode())));
                    }
                    break;
                case "isnull":
                    userQueriesSwitch.add(has(SwitchEntity.BANKCODE));
                    break;
                case "isnotnull":
                    userQueriesSwitch.add(not(has(SwitchEntity.BANKCODE)));
                    break;
            }
        }
    }

    private void AddAcquirercodeFilterSwitch() {
        if (transSearchCriteria.getAcquirercodeOperator() != null) {
            switch (transSearchCriteria.getAcquirercodeOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getAcquirercode() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.ACQUIRERCODE, transSearchCriteria.getAcquirercode())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getAcquirercode() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.ACQUIRERCODE, transSearchCriteria.getAcquirercode()));
                    }
                    break;
            }
        }
    }

    private void AddAcquirerFilterSwitch() {
        if (transSearchCriteria.getAcquirerOperator() != null) {
            switch (transSearchCriteria.getAcquirerOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getAcquirer() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.ACQUIRER, transSearchCriteria.getAcquirer())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getAcquirer() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.ACQUIRER, transSearchCriteria.getAcquirer()));
                    }
                    break;
            }
        }
    }

    private void AddSettelmentcodeFilterSwitch() {
        if (transSearchCriteria.getSettelmentcodeOperator() != null) {
            switch (transSearchCriteria.getSettelmentcodeOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getSettelmentcode() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.SETTELMENTCODE, transSearchCriteria.getSettelmentcode())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getSettelmentcode() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.SETTELMENTCODE, transSearchCriteria.getSettelmentcode()));
                    }
                    break;
            }
        }
    }

    private void AddTranstypeFilterSwitch() {
        if (transSearchCriteria.getTranstypeOperator() != null) {
            switch (transSearchCriteria.getTranstypeOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getTranstype() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.TRANSTYPE, transSearchCriteria.getTranstype())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTranstype() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.TRANSTYPE, transSearchCriteria.getTranstype()));
                    }
                    break;
                case "isnull":
                    userQueriesSwitch.add(has(SwitchEntity.TRANSTYPE));
                    break;
                case "isnotnull":
                    userQueriesSwitch.add(not(has(SwitchEntity.TRANSTYPE)));
                    break;
            }
        }
    }

    private void AddresponseTypeFilterSwitch() {
        if (transSearchCriteria.getResponsecodeOperator() != null) {
            switch (transSearchCriteria.getResponsecodeOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getResponsecode() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.RESPONCETYPE, transSearchCriteria.getResponsecode())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getResponsecode() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.RESPONCETYPE, transSearchCriteria.getResponsecode()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getResponsecode() != null) {
                        userQueriesSwitch.add(greaterThan(SwitchEntity.RESPONCETYPE, transSearchCriteria.getResponsecode()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getResponsecode() != null) {
                        userQueriesSwitch.add(lessThan(SwitchEntity.RESPONCETYPE, transSearchCriteria.getResponsecode()));
                    }
                    break;
            }
        }
    }

    private void AddPanFilterSwitch() {
        if (transSearchCriteria.getTopanOperator() != null) {
            switch (transSearchCriteria.getTopanOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getTopan() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.PAN, transSearchCriteria.getTopan())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTopan() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.PAN, transSearchCriteria.getTopan()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getTopan() != null) {
                        userQueriesSwitch.add(contains(SwitchEntity.PAN, transSearchCriteria.getTopan()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getTopan() != null) {
                        userQueriesSwitch.add(not(contains(SwitchEntity.PAN, transSearchCriteria.getTopan())));
                    }
                    break;
                case "isnull":
                    userQueriesSwitch.add(has(SwitchEntity.PAN));
                    break;
                case "isnotnull":
                    userQueriesSwitch.add(not(has(SwitchEntity.PAN)));
                    break;
            }
        }
    }

    private void AddTraceFilterSwitch() {
        if (transSearchCriteria.getTracefrom() != 0 || transSearchCriteria.getTraceto() != 0) {
            userQueriesHost.add(between(HostEntity.TRACE, transSearchCriteria.getTracefrom(), transSearchCriteria.getTraceto()));
        }
    }

    private void AddrrnFilterSwitch() {
        if (transSearchCriteria.getRrnOperator() != null) {
            switch (transSearchCriteria.getRrnOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getRrn() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.RRN, transSearchCriteria.getRrn())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getRrn() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.RRN, transSearchCriteria.getRrn()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getRrn() != null) {
                        userQueriesSwitch.add(contains(SwitchEntity.RRN, transSearchCriteria.getRrn()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getRrn() != null) {
                        userQueriesSwitch.add(not(contains(SwitchEntity.RRN, transSearchCriteria.getRrn())));
                    }
                    break;
                case "isnull":
                    userQueriesSwitch.add(has(SwitchEntity.RRN));
                    break;
                case "isnotnull":
                    userQueriesSwitch.add(not(has(SwitchEntity.RRN)));
                    break;
            }
        }
    }

    private void AddSettelmentdateFilterSwitch() {
        if (transSearchCriteria.getSettelmentdateOperator() != null) {
            switch (transSearchCriteria.getSettelmentdateOperator().toLowerCase()) {
                case "between":
                    if (transSearchCriteria.getSettelmentdateFrom() != null && transSearchCriteria.getSettelmentdateTo() != null) {
                        userQueriesSwitch.add(between(SwitchEntity.SETTELMENTDATE, transSearchCriteria.getSettelmentdateFrom(), transSearchCriteria.getSettelmentdateTo()));
                    }
                    break;
                case "not equal":
                    if (transSearchCriteria.getSettelmentdateFrom() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.SETTELMENTDATE, transSearchCriteria.getSettelmentdateFrom())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getSettelmentdateFrom() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.SETTELMENTDATE, transSearchCriteria.getSettelmentdateFrom()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getSettelmentdateFrom() != null) {
                        userQueriesSwitch.add(greaterThan(SwitchEntity.SETTELMENTDATE, transSearchCriteria.getSettelmentdateFrom()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getSettelmentdateFrom() != null) {
                        userQueriesSwitch.add(lessThan(SwitchEntity.SETTELMENTDATE, transSearchCriteria.getSettelmentdateFrom()));
                    }
                    break;
            }
        }
    }

    private void AddAuthFilterSwitch() {
        if (transSearchCriteria.getAuthOperator() != null) {
            switch (transSearchCriteria.getAuthOperator().toLowerCase()) {
                case "between":
                    if (transSearchCriteria.getAuthnumberFrom() != null && transSearchCriteria.getAuthnumberTo() != null) {
                        userQueriesSwitch.add(between(SwitchEntity.AUTHNUMBER, transSearchCriteria.getAuthnumberFrom(), transSearchCriteria.getAuthnumberTo()));
                    }
                    break;
                case "not equal":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesSwitch.add(not(equal(SwitchEntity.AUTHNUMBER, transSearchCriteria.getAuthnumberFrom())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesSwitch.add(equal(SwitchEntity.AUTHNUMBER, transSearchCriteria.getAuthnumberFrom()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesSwitch.add(greaterThan(SwitchEntity.AUTHNUMBER, transSearchCriteria.getAuthnumberFrom()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesSwitch.add(lessThan(SwitchEntity.AUTHNUMBER, transSearchCriteria.getAuthnumberFrom()));
                    }
                    break;
            }
        }
    }

    private void AddAccountnumberFilterSwitch() {
        if (transSearchCriteria.getAccountnumber() != null) {
            userQueriesSwitch.add(equal(SwitchEntity.ACCOUNTNUMBER, transSearchCriteria.getAccountnumber()));
        }
    }

    private void AddTraceFilterHost() {
        if (transSearchCriteria.getTracefrom() != 0 || transSearchCriteria.getTraceto() != 0) {
            userQueriesHost.add(between(HostEntity.TRACE, transSearchCriteria.getTracefrom(), transSearchCriteria.getTraceto()));
        }

    }

    private void AddCurrencycodeFilterHost() {
        if (transSearchCriteria.getCurrencycodeOperator() != null) {
            switch (transSearchCriteria.getCurrencycodeOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getCurrencycode() != null) {
                        userQueriesHost.add(not(equal(HostEntity.CURRENCYCODE, transSearchCriteria.getCurrencycode())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getCurrencycode() != null) {
                        userQueriesHost.add(equal(HostEntity.CURRENCYCODE, transSearchCriteria.getCurrencycode()));
                    }
                    break;
            }
        }
    }

    private void AddTranstypeFilterHost() {
        if (transSearchCriteria.getTranstypeOperator() != null) {
            switch (transSearchCriteria.getTranstypeOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getTranstype() != null) {
                        userQueriesHost.add(not(equal(HostEntity.TRANSTYPE, transSearchCriteria.getTranstype())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTranstype() != null) {
                        userQueriesHost.add(equal(HostEntity.TRANSTYPE, transSearchCriteria.getTranstype()));
                    }
                    break;
                case "isnull":
                    userQueriesHost.add(has(HostEntity.TRANSTYPE));
                    break;
                case "isnotnull":
                    userQueriesHost.add(not(has(HostEntity.TRANSTYPE)));
                    break;
            }
        }
    }

    private void AddNetworktypeFilterHost() {
        if (transSearchCriteria.getNetworktypeOperator() != null) {
            switch (transSearchCriteria.getNetworktypeOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getNetworktype() != null) {
                        userQueriesHost.add(not(equal(HostEntity.NETWORKTYPE, transSearchCriteria.getNetworktype())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getNetworktype() != null) {
                        userQueriesHost.add(equal(HostEntity.NETWORKTYPE, transSearchCriteria.getNetworktype()));
                    }
                    break;
                case "isnull":
                    userQueriesHost.add(has(HostEntity.NETWORKTYPE));
                    break;
                case "isnotnull":
                    userQueriesHost.add(not(has(HostEntity.NETWORKTYPE)));
                    break;
            }
        }
    }

    private void AddAccountnumberFilterHost() {
        if (transSearchCriteria.getAccountnumber() != null) {
            userQueriesHost.add(equal(HostEntity.ACCOUNTNUMBER, transSearchCriteria.getAccountnumber()));
        }
    }

    private void AddTerminalFilterHost() {
        if (transSearchCriteria.getTerminalOperator() != null) {
            switch (transSearchCriteria.getTerminalOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getTerminal() != null) {
                        userQueriesHost.add(not(equal(HostEntity.TERMINAL, transSearchCriteria.getTerminal())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTerminal() != null) {
                        userQueriesHost.add(equal(HostEntity.TERMINAL, transSearchCriteria.getTerminal()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getTerminal() != null) {
                        userQueriesHost.add(contains(HostEntity.TERMINAL, transSearchCriteria.getTerminal()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getTerminal() != null) {
                        userQueriesHost.add(not(contains(HostEntity.TERMINAL, transSearchCriteria.getTerminal())));
                    }
                    break;
                case "isnull":
                    userQueriesHost.add(has(HostEntity.TERMINAL));
                    break;
                case "isnotnull":
                    userQueriesHost.add(not(has(HostEntity.TERMINAL)));
                    break;
            }
        }
    }

    private void AddBankCodeFilterHost() {
        if (transSearchCriteria.getBankcodeOperator() != null) {
            switch (transSearchCriteria.getBankcodeOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getBankcode() != null) {
                        userQueriesHost.add(not(equal(HostEntity.BANKCODE, transSearchCriteria.getBankcode())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getBankcode() != null) {
                        userQueriesHost.add(equal(HostEntity.BANKCODE, transSearchCriteria.getBankcode()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getBankcode() != null) {
                        userQueriesHost.add(contains(HostEntity.BANKCODE, transSearchCriteria.getBankcode()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getBankcode() != null) {
                        userQueriesHost.add(not(contains(HostEntity.BANKCODE, transSearchCriteria.getBankcode())));
                    }
                    break;
                case "isnull":
                    userQueriesHost.add(has(HostEntity.BANKCODE));
                    break;
                case "isnotnull":
                    userQueriesHost.add(not(has(HostEntity.BANKCODE)));
                    break;
            }
        }
    }

    private void AddtxdatetimeFilterHost() {
        if (transSearchCriteria.getTxdatetimeOperator() != null) {
            switch (transSearchCriteria.getTxdatetimeOperator().toLowerCase()) {
                case "between":
                    if (transSearchCriteria.getTxdatetimeFrom() != null && transSearchCriteria.getTxdatetimeTo() != null) {
                        userQueriesHost.add(between(HostEntity.TXDATE, transSearchCriteria.getTxdatetimeFrom(), transSearchCriteria.getTxdatetimeTo()));
                    }
                    break;
                case "not equal":
                    if (transSearchCriteria.getTxdatetimeFrom() != null) {
                        userQueriesHost.add(not(equal(HostEntity.TXDATE, transSearchCriteria.getTxdatetimeFrom())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTxdatetimeFrom() != null) {
                        userQueriesHost.add(equal(HostEntity.TXDATE, transSearchCriteria.getTxdatetimeFrom()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getTxdatetimeFrom() != null) {
                        userQueriesHost.add(greaterThan(HostEntity.TXDATE, transSearchCriteria.getTxdatetimeFrom()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getTxdatetimeFrom() != null) {
                        userQueriesHost.add(lessThan(HostEntity.TXDATE, transSearchCriteria.getTxdatetimeFrom()));
                    }
                    break;
            }
        }
    }

    private void AddSettelmentdateFilterHost() {
        if (transSearchCriteria.getSettelmentdateOperator() != null) {
            switch (transSearchCriteria.getSettelmentdateOperator().toLowerCase()) {
                case "between":
                    if (transSearchCriteria.getSettelmentdateFrom() != null && transSearchCriteria.getSettelmentdateTo() != null) {
                        userQueriesHost.add(between(HostEntity.SETTELMENTDATE, transSearchCriteria.getSettelmentdateFrom(), transSearchCriteria.getSettelmentdateTo()));
                    }
                    break;
                case "not equal":
                    if (transSearchCriteria.getSettelmentdateFrom() != null) {
                        userQueriesHost.add(not(equal(HostEntity.SETTELMENTDATE, transSearchCriteria.getSettelmentdateFrom())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getSettelmentdateFrom() != null) {
                        userQueriesHost.add(equal(HostEntity.SETTELMENTDATE, transSearchCriteria.getSettelmentdateFrom()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getSettelmentdateFrom() != null) {
                        userQueriesHost.add(greaterThan(HostEntity.SETTELMENTDATE, transSearchCriteria.getSettelmentdateFrom()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getSettelmentdateFrom() != null) {
                        userQueriesHost.add(lessThan(HostEntity.SETTELMENTDATE, transSearchCriteria.getSettelmentdateFrom()));
                    }
                    break;
            }
        }
    }

    private void AddTransactionamountFilterHOST() {
        if (transSearchCriteria.getTransactionamountOperator() != null) {
            switch (transSearchCriteria.getTransactionamountOperator().toLowerCase()) {
                case "between":
                    if (transSearchCriteria.getTransactionamountFrom() != null && transSearchCriteria.getTransactionamountTo() != null) {
                        userQueriesHost.add(between(HostEntity.TRANSACTIONAMOUNT, transSearchCriteria.getTransactionamountFrom(), transSearchCriteria.getTransactionamountTo()));
                    }
                    break;
                case "not equal":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesHost.add(not(equal(HostEntity.TRANSACTIONAMOUNT, transSearchCriteria.getTransactionamountFrom())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesHost.add(equal(HostEntity.TRANSACTIONAMOUNT, transSearchCriteria.getTransactionamountFrom()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesHost.add(greaterThan(HostEntity.TRANSACTIONAMOUNT, transSearchCriteria.getTransactionamountFrom()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesHost.add(lessThan(HostEntity.TRANSACTIONAMOUNT, transSearchCriteria.getTransactionamountFrom()));
                    }
                    break;
            }
        }
    }

    private void AddPanFilterHost() {
        if (transSearchCriteria.getTopanOperator() != null) {
            switch (transSearchCriteria.getTopanOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getTopan() != null) {
                        userQueriesHost.add(not(equal(HostEntity.PAN, transSearchCriteria.getTopan())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTopan() != null) {
                        userQueriesHost.add(equal(HostEntity.PAN, transSearchCriteria.getTopan()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getTopan() != null) {
                        userQueriesHost.add(contains(HostEntity.PAN, transSearchCriteria.getTopan()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getTopan() != null) {
                        userQueriesHost.add(not(contains(HostEntity.PAN, transSearchCriteria.getTopan())));
                    }
                    break;
                case "isnull":
                    userQueriesHost.add(has(HostEntity.PAN));
                    break;
                case "isnotnull":
                    userQueriesHost.add(not(has(HostEntity.PAN)));
                    break;
            }
        }
    }

    private void AddAuthFilterHost() {
        if (transSearchCriteria.getAuthOperator() != null) {
            switch (transSearchCriteria.getAuthOperator().toLowerCase()) {
                case "between":
                    if (transSearchCriteria.getAuthnumberFrom() != null && transSearchCriteria.getAuthnumberTo() != null) {
                        userQueriesHost.add(between(HostEntity.AUTHNUMBER, transSearchCriteria.getAuthnumberFrom(), transSearchCriteria.getAuthnumberTo()));
                    }
                    break;
                case "not equal":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesHost.add(not(equal(HostEntity.AUTHNUMBER, transSearchCriteria.getAuthnumberFrom())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesHost.add(equal(HostEntity.AUTHNUMBER, transSearchCriteria.getAuthnumberFrom()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesHost.add(greaterThan(HostEntity.AUTHNUMBER, transSearchCriteria.getAuthnumberFrom()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesHost.add(lessThan(HostEntity.AUTHNUMBER, transSearchCriteria.getAuthnumberFrom()));
                    }
                    break;
            }
        }
    }

    private void AddresponsecodeFilterEBC() {
        if (transSearchCriteria.getResponsecodeOperator() != null) {
            switch (transSearchCriteria.getResponsecodeOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getResponsecode() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.RESPONCECODE, transSearchCriteria.getResponsecode())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getResponsecode() != null) {
                        userQueriesEBC.add(equal(EBCEntity.RESPONCECODE, transSearchCriteria.getResponsecode()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getResponsecode() != null) {
                        userQueriesEBC.add(greaterThan(EBCEntity.RESPONCECODE, transSearchCriteria.getResponsecode()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getResponsecode() != null) {
                        userQueriesEBC.add(lessThan(EBCEntity.RESPONCECODE, transSearchCriteria.getResponsecode()));
                    }
                    break;
            }
        }
    }

    private void AddterminalFilterEBC() {
        if (transSearchCriteria.getTerminalOperator() != null) {
            switch (transSearchCriteria.getTerminalOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getTerminal() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.TERMINAL, transSearchCriteria.getTerminal())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTerminal() != null) {
                        userQueriesEBC.add(equal(EBCEntity.TERMINAL, transSearchCriteria.getTerminal()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getTerminal() != null) {
                        userQueriesEBC.add(contains(EBCEntity.TERMINAL, transSearchCriteria.getTerminal()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getTerminal() != null) {
                        userQueriesEBC.add(not(contains(EBCEntity.TERMINAL, transSearchCriteria.getTerminal())));
                    }
                    break;
                case "isnull":
                    userQueriesEBC.add(has(EBCEntity.TERMINAL));
                    break;
                case "isnotnull":
                    userQueriesEBC.add(not(has(EBCEntity.TERMINAL)));
                    break;
            }
        }
    }

    private void AddpanFilterEBC() {
        if (transSearchCriteria.getPanOperator() != null) {
            switch (transSearchCriteria.getPanOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getPan() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.PAN, transSearchCriteria.getPan())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getPan() != null) {
                        userQueriesEBC.add(equal(EBCEntity.PAN, transSearchCriteria.getPan()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getPan() != null) {
                        userQueriesEBC.add(contains(EBCEntity.PAN, transSearchCriteria.getPan()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getPan() != null) {
                        userQueriesEBC.add(not(contains(EBCEntity.PAN, transSearchCriteria.getPan())));
                    }
                    break;
                case "isnull":
                    userQueriesEBC.add(has(EBCEntity.PAN));
                    break;
                case "isnotnull":
                    userQueriesEBC.add(not(has(EBCEntity.PAN)));
                    break;
            }
        }
    }

    private void AddAuthFilterEBC() {
        if (transSearchCriteria.getAuthnumberFrom() != 0 || transSearchCriteria.getAuthnumberTo() != 0) {
            userQueriesEBC.add(between(EBCEntity.AUTHNUMBER, transSearchCriteria.getAuthnumberFrom(), transSearchCriteria.getAuthnumberTo()));
        }
    }

    private void AddtxdatetimeFilterEBC() {
        if (transSearchCriteria.getTxdatetimeOperator() != null) {
            switch (transSearchCriteria.getTxdatetimeOperator().toLowerCase()) {
                case "between":
                    if (transSearchCriteria.getTxdatetimeFrom() != null && transSearchCriteria.getTxdatetimeTo() != null) {
                        userQueriesEBC.add(between(EBCEntity.TXDATETIME, transSearchCriteria.getTxdatetimeFrom(), transSearchCriteria.getTxdatetimeTo()));
                    }
                    break;
                case "not equal":
                    if (transSearchCriteria.getTxdatetimeFrom() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.TXDATETIME, transSearchCriteria.getTxdatetimeFrom())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTxdatetimeFrom() != null) {
                        userQueriesEBC.add(equal(EBCEntity.TXDATETIME, transSearchCriteria.getTxdatetimeFrom()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getTxdatetimeFrom() != null) {
                        userQueriesEBC.add(greaterThan(EBCEntity.TXDATETIME, transSearchCriteria.getTxdatetimeFrom()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getTxdatetimeFrom() != null) {
                        userQueriesEBC.add(lessThan(EBCEntity.TXDATETIME, transSearchCriteria.getTxdatetimeFrom()));
                    }
                    break;
            }
        }
    }

    private void AddTransactionamountFilterEBC() {
        if (transSearchCriteria.getTransactionamountOperator() != null) {
            switch (transSearchCriteria.getTransactionamountOperator().toLowerCase()) {
                case "between":
                    if (transSearchCriteria.getTransactionamountFrom() != null && transSearchCriteria.getTransactionamountTo() != null) {
                        userQueriesEBC.add(between(EBCEntity.TRANSACTIONAMOUNT, transSearchCriteria.getTransactionamountFrom(), transSearchCriteria.getTransactionamountTo()));
                    }
                    break;
                case "not equal":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.TRANSACTIONAMOUNT, transSearchCriteria.getTransactionamountFrom())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesEBC.add(equal(EBCEntity.TRANSACTIONAMOUNT, transSearchCriteria.getTransactionamountFrom()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesEBC.add(greaterThan(EBCEntity.TRANSACTIONAMOUNT, transSearchCriteria.getTransactionamountFrom()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getTransactionamountFrom() != null) {
                        userQueriesEBC.add(lessThan(EBCEntity.TRANSACTIONAMOUNT, transSearchCriteria.getTransactionamountFrom()));
                    }
                    break;
            }
        }
    }

    private void AddrrnFilterEBC() {
        if (transSearchCriteria.getRrnOperator() != null) {
            switch (transSearchCriteria.getRrnOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getRrn() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.RRN, transSearchCriteria.getRrn())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getRrn() != null) {
                        userQueriesEBC.add(equal(EBCEntity.RRN, transSearchCriteria.getRrn()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getRrn() != null) {
                        userQueriesEBC.add(contains(EBCEntity.RRN, transSearchCriteria.getRrn()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getRrn() != null) {
                        userQueriesEBC.add(not(contains(EBCEntity.RRN, transSearchCriteria.getRrn())));
                    }
                    break;
                case "isnull":
                    userQueriesEBC.add(has(EBCEntity.RRN));
                    break;
                case "isnotnull":
                    userQueriesEBC.add(not(has(EBCEntity.RRN)));
                    break;
            }
        }
    }

    private void AdduniquereferencenumberFilterEBC() {
        if (transSearchCriteria.getUniquereferencenumberOperator() != null) {
            switch (transSearchCriteria.getUniquereferencenumberOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getUniquereferencenumber() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.UNIQUEREFERENCENUMBER, transSearchCriteria.getUniquereferencenumber())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getUniquereferencenumber() != null) {
                        userQueriesEBC.add(equal(EBCEntity.UNIQUEREFERENCENUMBER, transSearchCriteria.getUniquereferencenumber()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getUniquereferencenumber() != null) {
                        userQueriesEBC.add(contains(EBCEntity.UNIQUEREFERENCENUMBER, transSearchCriteria.getUniquereferencenumber()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getUniquereferencenumber() != null) {
                        userQueriesEBC.add(not(contains(EBCEntity.UNIQUEREFERENCENUMBER, transSearchCriteria.getUniquereferencenumber())));
                    }
                    break;
                case "isnull":
                    userQueriesEBC.add(has(EBCEntity.UNIQUEREFERENCENUMBER));
                    break;
                case "isnotnull":
                    userQueriesEBC.add(not(has(EBCEntity.UNIQUEREFERENCENUMBER)));
                    break;
            }
        }
    }

    private void AddledgereffectFilterEBC() {
        if (transSearchCriteria.getLedgereffectOperator() != null) {
            switch (transSearchCriteria.getLedgereffectOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getLedgereffect() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.LEDGEREFFECT, transSearchCriteria.getLedgereffect())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getLedgereffect() != null) {
                        userQueriesEBC.add(equal(EBCEntity.LEDGEREFFECT, transSearchCriteria.getLedgereffect()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getLedgereffect() != null) {
                        userQueriesEBC.add(contains(EBCEntity.LEDGEREFFECT, transSearchCriteria.getLedgereffect()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getLedgereffect() != null) {
                        userQueriesEBC.add(not(contains(EBCEntity.LEDGEREFFECT, transSearchCriteria.getLedgereffect())));
                    }
                    break;
                case "isnull":
                    userQueriesEBC.add(has(EBCEntity.LEDGEREFFECT));
                    break;
                case "isnotnull":
                    userQueriesEBC.add(not(has(EBCEntity.LEDGEREFFECT)));
                    break;
            }
        }
    }

    private void AddactioncodeFilterEBC() {
        if (transSearchCriteria.getActioncodeOperator() != null) {
            switch (transSearchCriteria.getActioncodeOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getActioncode() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.ACTIONCODE, transSearchCriteria.getActioncode())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getActioncode() != null) {
                        userQueriesEBC.add(equal(EBCEntity.ACTIONCODE, transSearchCriteria.getActioncode()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getActioncode() != null) {
                        userQueriesEBC.add(contains(EBCEntity.ACTIONCODE, transSearchCriteria.getActioncode()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getActioncode() != null) {
                        userQueriesEBC.add(not(contains(EBCEntity.ACTIONCODE, transSearchCriteria.getActioncode())));
                    }
                    break;
                case "isnull":
                    userQueriesEBC.add(has(EBCEntity.ACTIONCODE));
                    break;
                case "isnotnull":
                    userQueriesEBC.add(not(has(EBCEntity.ACTIONCODE)));
                    break;
            }
        }
    }

    private void AddtoPanFilterEBC() {
        if (transSearchCriteria.getTopanOperator() != null) {
            switch (transSearchCriteria.getTopanOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getTopan() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.TOPAN, transSearchCriteria.getTopan())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getTopan() != null) {
                        userQueriesEBC.add(equal(EBCEntity.TOPAN, transSearchCriteria.getTopan()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getTopan() != null) {
                        userQueriesEBC.add(contains(EBCEntity.TOPAN, transSearchCriteria.getTopan()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getTopan() != null) {
                        userQueriesEBC.add(not(contains(EBCEntity.TOPAN, transSearchCriteria.getTopan())));
                    }
                    break;
                case "isnull":
                    userQueriesEBC.add(has(EBCEntity.TOPAN));
                    break;
                case "isnotnull":
                    userQueriesEBC.add(not(has(EBCEntity.TOPAN)));
                    break;
            }
        }
    }

    private void AddOctrrnFilterEBC() {
        if (transSearchCriteria.getOctrrnOperator() != null) {
            switch (transSearchCriteria.getOctrrnOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getOctrrn() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.OCTRRN, transSearchCriteria.getOctrrn())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getOctrrn() != null) {
                        userQueriesEBC.add(equal(EBCEntity.OCTRRN, transSearchCriteria.getOctrrn()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getOctrrn() != null) {
                        userQueriesEBC.add(contains(EBCEntity.OCTRRN, transSearchCriteria.getOctrrn()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getOctrrn() != null) {
                        userQueriesEBC.add(not(contains(EBCEntity.OCTRRN, transSearchCriteria.getOctrrn())));
                    }
                    break;
                case "isnull":
                    userQueriesEBC.add(has(EBCEntity.OCTRRN));
                    break;
                case "isnotnull":
                    userQueriesEBC.add(not(has(EBCEntity.OCTRRN)));
                    break;
            }
        }
    }

    private void AddcardacceptorterminalidFilterEBC() {
        if (transSearchCriteria.getCardacceptorterminalidOperator() != null) {
            switch (transSearchCriteria.getCardacceptorterminalidOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getCardacceptorterminalid() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.CARDACCEPTORTERMINALID, transSearchCriteria.getCardacceptorterminalid())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getCardacceptorterminalid() != null) {
                        userQueriesEBC.add(equal(EBCEntity.CARDACCEPTORTERMINALID, transSearchCriteria.getCardacceptorterminalid()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getCardacceptorterminalid() != null) {
                        userQueriesEBC.add(contains(EBCEntity.CARDACCEPTORTERMINALID, transSearchCriteria.getCardacceptorterminalid()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getCardacceptorterminalid() != null) {
                        userQueriesEBC.add(not(contains(EBCEntity.CARDACCEPTORTERMINALID, transSearchCriteria.getCardacceptorterminalid())));
                    }
                    break;
                case "isnull":
                    userQueriesEBC.add(has(EBCEntity.CARDACCEPTORTERMINALID));
                    break;
                case "isnotnull":
                    userQueriesEBC.add(not(has(EBCEntity.CARDACCEPTORTERMINALID)));
                    break;
            }
        }
    }

    private void AddCardacceptoridFilterEBC() {
        if (transSearchCriteria.getCardacceptoridcodeOperator() != null) {
            switch (transSearchCriteria.getCardacceptoridcodeOperator().toLowerCase()) {
                case "not equal":
                    if (transSearchCriteria.getCardacceptoridcode() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.CARDACCEPTORIDCODE, transSearchCriteria.getCardacceptoridcode())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getCardacceptorterminalid() != null) {
                        userQueriesEBC.add(equal(EBCEntity.CARDACCEPTORIDCODE, transSearchCriteria.getCardacceptoridcode()));
                    }
                    break;
                case "like":
                    if (transSearchCriteria.getCardacceptorterminalid() != null) {
                        userQueriesEBC.add(contains(EBCEntity.CARDACCEPTORIDCODE, transSearchCriteria.getCardacceptoridcode()));
                    }
                    break;
                case "not like":
                    if (transSearchCriteria.getCardacceptorterminalid() != null) {
                        userQueriesEBC.add(not(contains(EBCEntity.CARDACCEPTORIDCODE, transSearchCriteria.getCardacceptoridcode())));
                    }
                    break;
                case "isnull":
                    userQueriesEBC.add(has(EBCEntity.CARDACCEPTORIDCODE));
                    break;
                case "isnotnull":
                    userQueriesEBC.add(not(has(EBCEntity.CARDACCEPTORIDCODE)));
                    break;
            }
        }
    }

    private void AddFeesamountFilterEBC() {
        if (transSearchCriteria.getFeesamountOperator() != null) {
            switch (transSearchCriteria.getFeesamountOperator().toLowerCase()) {
                case "between":
                    if (transSearchCriteria.getFeesamountFrom() != null && transSearchCriteria.getFeesamountTo() != null) {
                        userQueriesEBC.add(between(EBCEntity.FEESAMOUNT, transSearchCriteria.getFeesamountFrom(), transSearchCriteria.getFeesamountTo()));
                    }
                    break;
                case "not equal":
                    if (transSearchCriteria.getFeesamountFrom() != null) {
                        userQueriesEBC.add(not(equal(EBCEntity.FEESAMOUNT, transSearchCriteria.getFeesamountFrom())));
                    }
                    break;
                case "equal":
                    if (transSearchCriteria.getFeesamountFrom() != null) {
                        userQueriesEBC.add(equal(EBCEntity.FEESAMOUNT, transSearchCriteria.getFeesamountFrom()));
                    }
                    break;
                case "greater":
                    if (transSearchCriteria.getFeesamountFrom() != null) {
                        userQueriesEBC.add(greaterThan(EBCEntity.FEESAMOUNT, transSearchCriteria.getFeesamountFrom()));
                    }
                    break;
                case "smaller":
                    if (transSearchCriteria.getFeesamountFrom() != null) {
                        userQueriesEBC.add(lessThan(EBCEntity.FEESAMOUNT, transSearchCriteria.getFeesamountFrom()));
                    }
                    break;
            }
        }
    }
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="DB">
    private OracleDataSource dataSource() throws SQLException {
        OracleDataSource dataSource = new OracleDataSource();
        dataSource.setUser("atm");
        dataSource.setPassword("atm");
        dataSource.setURL("jdbc:oracle:thin:@192.168.87.139:1521/db");
        dataSource.setDriverType("oracle.jdbc.driver.OracleDriver");
        dataSource.setImplicitCachingEnabled(true);
        dataSource.setFastConnectionFailoverEnabled(true);
        return dataSource;
    }

    private void loadValuesEbC(String[] SidesToShow) {
        for (final String Side : SidesToShow) {
            jdbcTemplate.query("select uniquereferencenumber,pan,ledgereffect,actioncode,\n"
                    + "topan,transactionamount,feesamount,txdatetime,\n"
                    + "rrn,responsecode,authnumber,octrrn,cardacceptorterminalid,\n"
                    + "cardacceptoridcode,terminal from EbC", new ResultSetExtractor() {
                        @Override
                        public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                            while (rs.next()) {
                                ((IndexedCollection<EBCEntity>) (AllSidesEBC.get(Side))).add(new EBCEntity(rs.getString("uniquereferencenumber"), rs.getString("pan"), rs.getString("ledgereffect"), rs.getString("actioncode"),
                                                rs.getString("topan"), rs.getBigDecimal("transactionamount"), rs.getBigDecimal("feesamount"), rs.getDate("txdatetime"),
                                                rs.getString("rrn"), rs.getInt("responsecode"), rs.getInt("authnumber"), rs.getString("octrrn"), rs.getString("cardacceptorterminalid"),
                                                rs.getString("cardacceptoridcode"), rs.getString("terminal")
                                        ));
                            }
                            return null;
                        }
                    });
        }
    }

    private void loadValuesSwitch(String[] SidesToShow) {
        for (final String Side : SidesToShow) {
            jdbcTemplate.query("select txdatetime,terminal,transactionamount,bankcode,\n"
                    + "transtype,responsecode,pan,trace,settelmentamount,\n"
                    + "settelmentcode,acquirer,rrn,settelmentdate,authnumber,\n"
                    + "acquireramount,acquirercode,accountnumber from Switch", new ResultSetExtractor() {
                        @Override
                        public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                            while (rs.next()) {
                                ((IndexedCollection<SwitchEntity>) (AllSidesSwitch.get(Side))).add(new SwitchEntity(rs.getDate("txdatetime"), rs.getString("terminal"), rs.getBigDecimal("transactionamount"), rs.getString("bankcode"),
                                                rs.getString("transtype"), rs.getInt("responsecode"), rs.getString("pan"), rs.getInt("trace"), rs.getBigDecimal("settelmentamount"),
                                                rs.getInt("settelmentcode"), rs.getInt("acquirer"), rs.getString("rrn"), rs.getDate("settelmentdate"), rs.getInt("authnumber"),
                                                rs.getBigDecimal("acquireramount"), rs.getInt("acquirercode"), rs.getString("accountnumber")));
                            }
                            return null;
                        }
                    });
        }
    }

    private void loadValuesHost(String[] SidesToShow) {
        for (final String Side : SidesToShow) {
            jdbcTemplate.query("select bankcode,terminal,pan,accountnumber,txdate,\n"
                    + "txtime,transactionamount,currencycode,trace,transtype,\n"
                    + "settelmentdate,authnumber,networktype from Host", new ResultSetExtractor() {
                        @Override
                        public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                            while (rs.next()) {
                                ((IndexedCollection<HostEntity>) (AllSidesHost.get(Side))).add(new HostEntity(rs.getString("bankcode"), rs.getString("terminal"), rs.getString("pan"), rs.getString("accountnumber"), rs.getDate("txdate"),
                                                rs.getDate("txtime"), rs.getBigDecimal("transactionamount"), rs.getInt("currencycode"), rs.getInt("trace"), rs.getString("transtype"),
                                                rs.getDate("settelmentdate"), rs.getInt("authnumber"), rs.getString("networktype")));
                            }
                            return null;
                        }
                    });
        }
    }

    private void buildIndexedCollectionEBC(String[] Sides) throws Exception {
        AllSidesEBC = new Hashtable<String, IndexedCollection<EBCEntity>>();
        for (String side : Sides) {
            AllSidesEBC.put(side, new ConcurrentIndexedCollection<EBCEntity>());
        }
    }

    private void buildIndexedCollectionSwitch(String[] Sides) throws Exception {
        AllSidesSwitch = new Hashtable<String, IndexedCollection<SwitchEntity>>();
        for (String side : Sides) {
            AllSidesSwitch.put(side, new ConcurrentIndexedCollection<SwitchEntity>());
        }
    }

    private void buildIndexedCollectionHost(String[] Sides) throws Exception {
        AllSidesHost = new Hashtable<String, IndexedCollection<HostEntity>>();
        for (String side : Sides) {
            AllSidesHost.put(side, new ConcurrentIndexedCollection<HostEntity>());
        }
    }
// </editor-fold>

    public void search() throws Exception {
        if (transSearchOptions.getMasterType().equals("E")) {
            pendingEBC.clear();
            buildIndexedCollectionEBC(transSearchOptions.getTransCategories());
            loadValuesEbC(transSearchOptions.getTransCategories());
            pendingEBC.addAll(AllSidesEBC.get("P"));
            getEBCLookups();

            setFilterVisibltyHost("hidden");
            setTransVisibltyHost("hidden");
            setFilterDisplayHost("none");
            setTransDisplayHost("none");
            setFilterVisibltySwitch("hidden");
            setTransVisibltySwitch("hidden");
            setFilterDisplaySwitch("none");
            setTransDisplaySwitch("none");
            filterVisibltyebc = "visible";
            transVisibltyebc = "visible";
            filterDisplayebc = "inline";
            transDisplayebc = "inline";
        } else if (transSearchOptions.getMasterType().equals("S")) {
            pendingSwitch.clear();
            buildIndexedCollectionSwitch(transSearchOptions.getTransCategories());
            loadValuesSwitch(transSearchOptions.getTransCategories());
            pendingSwitch.addAll(AllSidesSwitch.get("P"));
            getSwitchLookups();

            setFilterVisibltyHost("hidden");
            setTransVisibltyHost("hidden");
            setFilterDisplayHost("none");
            setTransDisplayHost("none");
            setFilterVisibltyebc("hidden");
            setTransVisibltyebc("hidden");
            setFilterDisplayebc("none");
            setTransDisplayebc("none");
            filterVisibltySwitch = "visible";
            transVisibltySwitch = "visible";
            filterDisplaySwitch = "inline";
            transDisplaySwitch = "inline";

        } else if (transSearchOptions.getMasterType().equals("H")) {
            pendingHost.clear();
            buildIndexedCollectionHost(transSearchOptions.getTransCategories());
            loadValuesHost(transSearchOptions.getTransCategories());
            pendingHost.addAll(AllSidesHost.get("P"));
            getHostLookups();

            setFilterVisibltySwitch("hidden");
            setTransVisibltySwitch("hidden");
            setFilterDisplaySwitch("none");
            setTransDisplaySwitch("none");
            setFilterVisibltyebc("hidden");
            setTransVisibltyebc("hidden");
            setFilterDisplayebc("none");
            setTransDisplayebc("none");
            filterVisibltyHost = "visible";
            transVisibltyHost = "visible";
            filterDisplayHost = "inline";
            transDisplayHost = "inline";
        }

        charVisiblty = "hidden";
        charDisplay = "none";
        setTabsVisability(transSearchOptions.getTransCategories());

    }

    public void filter() {
        if (transSearchOptions.getMasterType().equals("E")) {
            pendingEBC.clear();
            pendingEBC = getTransactionsEBC((IndexedCollection<EBCEntity>) (AllSidesEBC.get("P")), transSearchCriteria);
        } else if (transSearchOptions.getMasterType().equals("S")) {
            pendingSwitch.clear();
            pendingSwitch = getTransactionsSwitch((IndexedCollection<SwitchEntity>) (AllSidesSwitch.get("P")), transSearchCriteria);
        } else if (transSearchOptions.getMasterType().equals("H")) {
            pendingHost.clear();
            pendingHost = getTransactionsHost((IndexedCollection<HostEntity>) (AllSidesHost.get("P")), transSearchCriteria);
        }

    }

    public List<EBCEntity> getTransactionsEBC(IndexedCollection<EBCEntity> Result, SearchingCriteria criteria) {

        userQueriesEBC = new ArrayList<Query<EBCEntity>>();
        ApplyFiltersEBC();
        Query<EBCEntity> finalQuery = null;
        if (userQueriesEBC.size() == 1) {
            finalQuery = userQueriesEBC.get(0);
        } else if (userQueriesEBC.size() == 2) {
            finalQuery = QueryFactory.and(userQueriesEBC.get(0), userQueriesEBC.get(1));
        } else if (userQueriesEBC.size() > 2) {
            finalQuery = QueryFactory.and(userQueriesEBC.get(0), userQueriesEBC.get(1), userQueriesEBC.subList(2, userQueriesEBC.size()));
        }
        List<EBCEntity> FilteredRecords = new ArrayList<EBCEntity>();
        for (Object obj : Result.retrieve(finalQuery)) {
            FilteredRecords.add((EBCEntity) obj);
        }
        return FilteredRecords;
    }

    public List<SwitchEntity> getTransactionsSwitch(IndexedCollection<SwitchEntity> Result, SearchingCriteria criteria) {

        userQueriesSwitch = new ArrayList<Query<SwitchEntity>>();
        ApplyFiltersSwitch();
        Query<SwitchEntity> finalQuery = null;
        if (userQueriesSwitch.size() == 1) {
            finalQuery = userQueriesSwitch.get(0);
        } else if (userQueriesSwitch.size() == 2) {
            finalQuery = QueryFactory.and(userQueriesSwitch.get(0), userQueriesSwitch.get(1));
        } else if (userQueriesSwitch.size() > 2) {
            finalQuery = QueryFactory.and(userQueriesSwitch.get(0), userQueriesSwitch.get(1), userQueriesSwitch.subList(2, userQueriesSwitch.size()));
        }
        List<SwitchEntity> FilteredRecords = new ArrayList<SwitchEntity>();
        for (Object obj : Result.retrieve(finalQuery)) {
            FilteredRecords.add((SwitchEntity) obj);
        }
        return FilteredRecords;
    }

    public List<HostEntity> getTransactionsHost(IndexedCollection<HostEntity> Result, SearchingCriteria criteria) {

        userQueriesHost = new ArrayList<Query<HostEntity>>();
        ApplyFiltersHost();
        Query<HostEntity> finalQuery = null;
        if (userQueriesHost.size() == 1) {
            finalQuery = userQueriesHost.get(0);
        } else if (userQueriesHost.size() == 2) {
            finalQuery = QueryFactory.and(userQueriesHost.get(0), userQueriesHost.get(1));
        } else if (userQueriesSwitch.size() > 2) {
            finalQuery = QueryFactory.and(userQueriesHost.get(0), userQueriesHost.get(1), userQueriesHost.subList(2, userQueriesHost.size()));
        }
        List<HostEntity> FilteredRecords = new ArrayList<HostEntity>();
        for (Object obj : Result.retrieve(finalQuery)) {
            FilteredRecords.add((HostEntity) obj);
        }
        return FilteredRecords;
    }

    public void itemSelect(ItemSelectEvent event) throws Exception {
        String msgText = "Item Index: " + event.getItemIndex() + ", Series Index:" + event.getSeriesIndex();
        transSearchOptions.setMasterType("E");
        transSearchOptions.setTransCategories(new String[]{"P"});
        search();
        System.out.println(msgText);
    }

    private void setTabsVisability(String[] SelectedTabes) {
        pendingtab = Boolean.FALSE;
        disputetab = Boolean.FALSE;
        matchedtab = Boolean.FALSE;
        rejectedtab = Boolean.FALSE;
        for (String SelectedTabe : SelectedTabes) {
            if (SelectedTabe.equals("P")) {
                pendingtab = Boolean.TRUE;
            } else if (SelectedTabe.equals("D")) {
                disputetab = Boolean.TRUE;
            } else if (SelectedTabe.equals("M")) {
                matchedtab = Boolean.TRUE;
            } else if (SelectedTabe.equals("R")) {
                rejectedtab = Boolean.TRUE;
            }
        }
    }
}
