/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.managebean;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

@ManagedBean(name = "chartBean")
@ViewScoped
public class ChartBean implements Serializable {

    private BarChartModel barModel;

    @PostConstruct
    public void init() {
        createBarModels();
    }

    public BarChartModel getBarModel() {
        return barModel;
    }

    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();

        ChartSeries Pending = new ChartSeries();
        Pending.setLabel("Pending Transactions");
        Pending.set("01 Mar 2017 ", 120);
        Pending.set("02 Mar 2017", 100);
        Pending.set("03 Mar 2017", 44);
        Pending.set("04 Mar 2017", 150);
        Pending.set("05 Mar 2017", 25);

        ChartSeries Dispute = new ChartSeries();
        Dispute.setLabel("Dispute Transactions");
        Dispute.set("01 Mar 2017", 89);
        Dispute.set("02 Mar 2017", 44);
        Dispute.set("03 Mar 2017", 68);
        Dispute.set("04 Mar 2017", 78);
        Dispute.set("05 Mar 2017", 45);

        ChartSeries Matched = new ChartSeries();
        Matched.setLabel("Matched Transactions");
        Matched.set("01 Mar 2017", 52);
        Matched.set("02 Mar 2017", 60);
        Matched.set("03 Mar 2017", 110);
        Matched.set("04 Mar 2017", 135);
        Matched.set("05 Mar 2017", 120);

        ChartSeries Rejected = new ChartSeries();
        Rejected.setLabel("Rejected Transactions");
        Rejected.set("01 Mar 2017", 96);
        Rejected.set("02 Mar 2017", 48);
        Rejected.set("03 Mar 2017", 78);
        Rejected.set("04 Mar 2017", 13);
        Rejected.set("05 Mar 2017", 10);

        model.addSeries(Pending);
        model.addSeries(Matched);
        model.addSeries(Dispute);
        model.addSeries(Rejected);

        return model;
    }

    private void createBarModels() {
        createBarModel();
    }

    private void createBarModel() {
        barModel = initBarModel();
        //barModel.setZoom(true);
        barModel.setTitle("Last Week Statistics");
        barModel.setLegendPosition("ne");
        //barModel.setStacked(true);
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Dates");
        xAxis.setTickAngle(-50);

        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Counts");

    }
}
