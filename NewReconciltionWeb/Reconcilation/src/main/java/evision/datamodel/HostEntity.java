/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.datamodel;

import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.attribute.SimpleAttribute;
import com.googlecode.cqengine.query.option.QueryOptions;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author shi7a
 */
public class HostEntity {

    private String bankcode;
    private String terminal;
    private String pan;
    private String accountnumber;
    private Date txdate;
    private Date txtime;
    private BigDecimal transactionamount;
    private Integer currencycode;
    private Integer trace;
    private String transtype;
    private Date settelmentdate;
    private Integer authnumber;
    private String networktype;

    public HostEntity(String bankcode, String terminal, String pan, String accountnumber, Date txdate, Date txtime, BigDecimal transactionamount, Integer currencycode, Integer trace, String transtype, Date settelmentdate, Integer authnumber, String networktype) {
        this.bankcode = bankcode;
        this.terminal = terminal;
        this.pan = pan;
        this.accountnumber = accountnumber;
        this.txdate = txdate;
        this.txtime = txtime;
        this.transactionamount = transactionamount;
        this.currencycode = currencycode;
        this.trace = trace;
        this.transtype = transtype;
        this.settelmentdate = settelmentdate;
        this.authnumber = authnumber;
        this.networktype = networktype;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }

    public Date getTxdate() {
        return txdate;
    }

    public void setTxdate(Date txdate) {
        this.txdate = txdate;
    }

    public Date getTxtime() {
        return txtime;
    }

    public void setTxtime(Date txtime) {
        this.txtime = txtime;
    }

    public BigDecimal getTransactionamount() {
        return transactionamount;
    }

    public void setTransactionamount(BigDecimal transactionamount) {
        this.transactionamount = transactionamount;
    }

    public Integer getCurrencycode() {
        return currencycode;
    }

    public void setCurrencycode(Integer currencycode) {
        this.currencycode = currencycode;
    }

    public Integer getTrace() {
        return trace;
    }

    public void setTrace(Integer trace) {
        this.trace = trace;
    }

    public String getTranstype() {
        return transtype;
    }

    public void setTranstype(String transtype) {
        this.transtype = transtype;
    }

    public Date getSettelmentdate() {
        return settelmentdate;
    }

    public void setSettelmentdate(Date settelmentdate) {
        this.settelmentdate = settelmentdate;
    }

    public Integer getAuthnumber() {
        return authnumber;
    }

    public void setAuthnumber(Integer authnumber) {
        this.authnumber = authnumber;
    }

    public String getNetworktype() {
        return networktype;
    }

    public void setNetworktype(String networktype) {
        this.networktype = networktype;
    }
    public static final Attribute<HostEntity, String> BANKCODE = new SimpleAttribute<HostEntity, String>("bankcode") {
        @Override
        public String getValue(HostEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.bankcode;
        }
    };
    public static final Attribute<HostEntity, String> TERMINAL = new SimpleAttribute<HostEntity, String>("terminal") {
        @Override
        public String getValue(HostEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.terminal;
        }
    };
    public static final Attribute<HostEntity, String> PAN = new SimpleAttribute<HostEntity, String>("pan") {
        @Override
        public String getValue(HostEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.pan;
        }
    };
    public static final Attribute<HostEntity, String> ACCOUNTNUMBER = new SimpleAttribute<HostEntity, String>("accountnumber") {
        @Override
        public String getValue(HostEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.accountnumber;
        }
    };
    public static final Attribute<HostEntity, Date> TXDATE = new SimpleAttribute<HostEntity, Date>("txdate") {
        @Override
        public Date getValue(HostEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.txdate;
        }
    };
    public static final Attribute<HostEntity, Date> TXTIME = new SimpleAttribute<HostEntity, Date>("txtime") {
        @Override
        public Date getValue(HostEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.txtime;
        }
    };
    public static final Attribute<HostEntity, BigDecimal> TRANSACTIONAMOUNT = new SimpleAttribute<HostEntity, BigDecimal>("transactionamount") {
        @Override
        public BigDecimal getValue(HostEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.transactionamount;
        }
    };
    public static final Attribute<HostEntity, Integer> CURRENCYCODE = new SimpleAttribute<HostEntity, Integer>("currencycode") {
        @Override
        public Integer getValue(HostEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.currencycode;
        }
    };
    public static final Attribute<HostEntity, Integer> TRACE = new SimpleAttribute<HostEntity, Integer>("trace") {
        @Override
        public Integer getValue(HostEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.trace;
        }
    };
    public static final Attribute<HostEntity, String> TRANSTYPE = new SimpleAttribute<HostEntity, String>("transtype") {
        @Override
        public String getValue(HostEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.transtype;
        }
    };
    public static final Attribute<HostEntity, Date> SETTELMENTDATE = new SimpleAttribute<HostEntity, Date>("settelmentdate") {
        @Override
        public Date getValue(HostEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.settelmentdate;
        }
    };
    public static final Attribute<HostEntity, Integer> AUTHNUMBER = new SimpleAttribute<HostEntity, Integer>("authnumber") {
        @Override
        public Integer getValue(HostEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.authnumber;
        }
    };
    public static final Attribute<HostEntity, String> NETWORKTYPE = new SimpleAttribute<HostEntity, String>("networktype") {
        @Override
        public String getValue(HostEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.networktype;
        }
    };
}
