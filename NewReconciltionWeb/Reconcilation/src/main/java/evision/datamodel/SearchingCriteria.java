/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.datamodel;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author shi7a
 */
public class SearchingCriteria {
        //COMMON

    private String accountOperator;
    private String accountnumber;

    private String responsecodeOperator;
    private Integer responsecode;

    private String terminalOperator;
    private String terminal;

    private String panOperator;
    private String pan;

    private String authOperator;
    private Integer authnumberFrom;
    private Integer authnumberTo;

    private String txdatetimeOperator;
    private Date txdatetimeFrom;
    private Date txdatetimeTo;

    private String settelmentdateOperator;
    private Date settelmentdateFrom;
    private Date settelmentdateTo;

    private String transactionamountOperator;
    private BigDecimal transactionamountFrom;
    private BigDecimal transactionamountTo;

    private String transtypeOperator;
    private String transtype;

    private String traceOperator;
    private Integer tracefrom;
    private Integer traceto;

    private String rrnOperator;
    private String rrn;

    private String bankcodeOperator;
    private String bankcode;
//EBC
    private String uniquereferencenumberOperator;
    private String uniquereferencenumber;

    private String ledgereffectOperator;
    private String ledgereffect;

    private String actioncodeOperator;
    private String actioncode;

    private String topanOperator;
    private String topan;

    private String feesamountOperator;
    private BigDecimal feesamountFrom;
    private BigDecimal feesamountTo;

    private String octrrnOperator;
    private String octrrn;

    private String cardacceptorterminalidOperator;
    private String cardacceptorterminalid;

    private String cardacceptoridcodeOperator;
    private String cardacceptoridcode;
//HOST
    private String currencycodeOperator;
    private Integer currencycode;

    private String networktypeOperator;
    private String networktype;
//SWITCH
    private String settelmentamountOperator;
    private BigDecimal settelmentamountFrom;
    private BigDecimal settelmentamountTo;

    private String settelmentcodeOperator;
    private Integer settelmentcode;

    private String acquirerOperator;
    private Integer acquirer;

    private String acquireramountOperator;
    private BigDecimal acquireramountFrom;
    private BigDecimal acquireramountTo;

    private String acquirercodeOperator;
    private Integer acquirercode;

    public SearchingCriteria() {
    }

    public String getAccountOperator() {
        return accountOperator;
    }

    public void setAccountOperator(String accountOperator) {
        this.accountOperator = accountOperator;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }

    public String getResponsecodeOperator() {
        return responsecodeOperator;
    }

    public void setResponsecodeOperator(String responsecodeOperator) {
        this.responsecodeOperator = responsecodeOperator;
    }

    public Integer getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(Integer responsecode) {
        this.responsecode = responsecode;
    }

    public String getTerminalOperator() {
        return terminalOperator;
    }

    public void setTerminalOperator(String terminalOperator) {
        this.terminalOperator = terminalOperator;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getPanOperator() {
        return panOperator;
    }

    public void setPanOperator(String panOperator) {
        this.panOperator = panOperator;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getAuthOperator() {
        return authOperator;
    }

    public void setAuthOperator(String authOperator) {
        this.authOperator = authOperator;
    }

    public Integer getAuthnumberFrom() {
        return authnumberFrom;
    }

    public void setAuthnumberFrom(Integer authnumberFrom) {
        this.authnumberFrom = authnumberFrom;
    }

    public Integer getAuthnumberTo() {
        return authnumberTo;
    }

    public void setAuthnumberTo(Integer authnumberTo) {
        this.authnumberTo = authnumberTo;
    }

    public String getTxdatetimeOperator() {
        return txdatetimeOperator;
    }

    public void setTxdatetimeOperator(String txdatetimeOperator) {
        this.txdatetimeOperator = txdatetimeOperator;
    }

    public Date getTxdatetimeFrom() {
        return txdatetimeFrom;
    }

    public void setTxdatetimeFrom(Date txdatetimeFrom) {
        this.txdatetimeFrom = txdatetimeFrom;
    }

    public Date getTxdatetimeTo() {
        return txdatetimeTo;
    }

    public void setTxdatetimeTo(Date txdatetimeTo) {
        this.txdatetimeTo = txdatetimeTo;
    }

    public String getSettelmentdateOperator() {
        return settelmentdateOperator;
    }

    public void setSettelmentdateOperator(String settelmentdateOperator) {
        this.settelmentdateOperator = settelmentdateOperator;
    }

    public Date getSettelmentdateFrom() {
        return settelmentdateFrom;
    }

    public void setSettelmentdateFrom(Date settelmentdateFrom) {
        this.settelmentdateFrom = settelmentdateFrom;
    }

    public Date getSettelmentdateTo() {
        return settelmentdateTo;
    }

    public void setSettelmentdateTo(Date settelmentdateTo) {
        this.settelmentdateTo = settelmentdateTo;
    }

    public String getTransactionamountOperator() {
        return transactionamountOperator;
    }

    public void setTransactionamountOperator(String transactionamountOperator) {
        this.transactionamountOperator = transactionamountOperator;
    }

    public BigDecimal getTransactionamountFrom() {
        return transactionamountFrom;
    }

    public void setTransactionamountFrom(BigDecimal transactionamountFrom) {
        this.transactionamountFrom = transactionamountFrom;
    }

    public BigDecimal getTransactionamountTo() {
        return transactionamountTo;
    }

    public void setTransactionamountTo(BigDecimal transactionamountTo) {
        this.transactionamountTo = transactionamountTo;
    }

    public String getTranstypeOperator() {
        return transtypeOperator;
    }

    public void setTranstypeOperator(String transtypeOperator) {
        this.transtypeOperator = transtypeOperator;
    }

    public String getTranstype() {
        return transtype;
    }

    public void setTranstype(String transtype) {
        this.transtype = transtype;
    }

    public String getTraceOperator() {
        return traceOperator;
    }

    public void setTraceOperator(String traceOperator) {
        this.traceOperator = traceOperator;
    }

    public Integer getTracefrom() {
        return tracefrom;
    }

    public void setTracefrom(Integer tracefrom) {
        this.tracefrom = tracefrom;
    }

    public Integer getTraceto() {
        return traceto;
    }

    public void setTraceto(Integer traceto) {
        this.traceto = traceto;
    }


    public String getRrnOperator() {
        return rrnOperator;
    }

    public void setRrnOperator(String rrnOperator) {
        this.rrnOperator = rrnOperator;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getBankcodeOperator() {
        return bankcodeOperator;
    }

    public void setBankcodeOperator(String bankcodeOperator) {
        this.bankcodeOperator = bankcodeOperator;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getUniquereferencenumberOperator() {
        return uniquereferencenumberOperator;
    }

    public void setUniquereferencenumberOperator(String uniquereferencenumberOperator) {
        this.uniquereferencenumberOperator = uniquereferencenumberOperator;
    }

    public String getUniquereferencenumber() {
        return uniquereferencenumber;
    }

    public void setUniquereferencenumber(String uniquereferencenumber) {
        this.uniquereferencenumber = uniquereferencenumber;
    }

    public String getLedgereffectOperator() {
        return ledgereffectOperator;
    }

    public void setLedgereffectOperator(String ledgereffectOperator) {
        this.ledgereffectOperator = ledgereffectOperator;
    }

    public String getLedgereffect() {
        return ledgereffect;
    }

    public void setLedgereffect(String ledgereffect) {
        this.ledgereffect = ledgereffect;
    }

    public String getActioncodeOperator() {
        return actioncodeOperator;
    }

    public void setActioncodeOperator(String actioncodeOperator) {
        this.actioncodeOperator = actioncodeOperator;
    }

    public String getActioncode() {
        return actioncode;
    }

    public void setActioncode(String actioncode) {
        this.actioncode = actioncode;
    }

    public String getTopanOperator() {
        return topanOperator;
    }

    public void setTopanOperator(String topanOperator) {
        this.topanOperator = topanOperator;
    }

    public String getTopan() {
        return topan;
    }

    public void setTopan(String topan) {
        this.topan = topan;
    }

    public String getFeesamountOperator() {
        return feesamountOperator;
    }

    public void setFeesamountOperator(String feesamountOperator) {
        this.feesamountOperator = feesamountOperator;
    }

    public BigDecimal getFeesamountFrom() {
        return feesamountFrom;
    }

    public void setFeesamountFrom(BigDecimal feesamountFrom) {
        this.feesamountFrom = feesamountFrom;
    }

    public BigDecimal getFeesamountTo() {
        return feesamountTo;
    }

    public void setFeesamountTo(BigDecimal feesamountTo) {
        this.feesamountTo = feesamountTo;
    }

    public String getOctrrnOperator() {
        return octrrnOperator;
    }

    public void setOctrrnOperator(String octrrnOperator) {
        this.octrrnOperator = octrrnOperator;
    }

    public String getOctrrn() {
        return octrrn;
    }

    public void setOctrrn(String octrrn) {
        this.octrrn = octrrn;
    }

    public String getCardacceptorterminalidOperator() {
        return cardacceptorterminalidOperator;
    }

    public void setCardacceptorterminalidOperator(String cardacceptorterminalidOperator) {
        this.cardacceptorterminalidOperator = cardacceptorterminalidOperator;
    }

    public String getCardacceptorterminalid() {
        return cardacceptorterminalid;
    }

    public void setCardacceptorterminalid(String cardacceptorterminalid) {
        this.cardacceptorterminalid = cardacceptorterminalid;
    }

    public String getCardacceptoridcodeOperator() {
        return cardacceptoridcodeOperator;
    }

    public void setCardacceptoridcodeOperator(String cardacceptoridcodeOperator) {
        this.cardacceptoridcodeOperator = cardacceptoridcodeOperator;
    }

    public String getCardacceptoridcode() {
        return cardacceptoridcode;
    }

    public void setCardacceptoridcode(String cardacceptoridcode) {
        this.cardacceptoridcode = cardacceptoridcode;
    }

    public String getCurrencycodeOperator() {
        return currencycodeOperator;
    }

    public void setCurrencycodeOperator(String currencycodeOperator) {
        this.currencycodeOperator = currencycodeOperator;
    }

    public Integer getCurrencycode() {
        return currencycode;
    }

    public void setCurrencycode(Integer currencycode) {
        this.currencycode = currencycode;
    }

    public String getNetworktypeOperator() {
        return networktypeOperator;
    }

    public void setNetworktypeOperator(String networktypeOperator) {
        this.networktypeOperator = networktypeOperator;
    }

    public String getNetworktype() {
        return networktype;
    }

    public void setNetworktype(String networktype) {
        this.networktype = networktype;
    }

    public String getSettelmentamountOperator() {
        return settelmentamountOperator;
    }

    public void setSettelmentamountOperator(String settelmentamountOperator) {
        this.settelmentamountOperator = settelmentamountOperator;
    }

    public BigDecimal getSettelmentamountFrom() {
        return settelmentamountFrom;
    }

    public void setSettelmentamountFrom(BigDecimal settelmentamountFrom) {
        this.settelmentamountFrom = settelmentamountFrom;
    }

    public BigDecimal getSettelmentamountTo() {
        return settelmentamountTo;
    }

    public void setSettelmentamountTo(BigDecimal settelmentamountTo) {
        this.settelmentamountTo = settelmentamountTo;
    }

    public String getSettelmentcodeOperator() {
        return settelmentcodeOperator;
    }

    public void setSettelmentcodeOperator(String settelmentcodeOperator) {
        this.settelmentcodeOperator = settelmentcodeOperator;
    }

    public Integer getSettelmentcode() {
        return settelmentcode;
    }

    public void setSettelmentcode(Integer settelmentcode) {
        this.settelmentcode = settelmentcode;
    }

    public String getAcquirerOperator() {
        return acquirerOperator;
    }

    public void setAcquirerOperator(String acquirerOperator) {
        this.acquirerOperator = acquirerOperator;
    }

    public Integer getAcquirer() {
        return acquirer;
    }

    public void setAcquirer(Integer acquirer) {
        this.acquirer = acquirer;
    }

    public String getAcquireramountOperator() {
        return acquireramountOperator;
    }

    public void setAcquireramountOperator(String acquireramountOperator) {
        this.acquireramountOperator = acquireramountOperator;
    }

    public BigDecimal getAcquireramountFrom() {
        return acquireramountFrom;
    }

    public void setAcquireramountFrom(BigDecimal acquireramountFrom) {
        this.acquireramountFrom = acquireramountFrom;
    }

    public BigDecimal getAcquireramountTo() {
        return acquireramountTo;
    }

    public void setAcquireramountTo(BigDecimal acquireramountTo) {
        this.acquireramountTo = acquireramountTo;
    }

    public String getAcquirercodeOperator() {
        return acquirercodeOperator;
    }

    public void setAcquirercodeOperator(String acquirercodeOperator) {
        this.acquirercodeOperator = acquirercodeOperator;
    }

    public Integer getAcquirercode() {
        return acquirercode;
    }

    public void setAcquirercode(Integer acquirercode) {
        this.acquirercode = acquirercode;
    }
    
    
}
