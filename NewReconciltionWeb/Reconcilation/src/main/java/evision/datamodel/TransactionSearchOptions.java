package evision.datamodel;

import java.util.Date;

/**
 *
 * @author shi7a
 */
public class TransactionSearchOptions {
private String[] transCategories;
private String masterType;
private Date transactionDateFrom;
private Date transactionDateTo;
private Boolean totalAmount;
private Boolean totalTrans;

    public String[] getTransCategories() {
        return transCategories;
    }
    public void setTransCategories(String[] transCategories) {
        this.transCategories = transCategories;
    }
    public String getMasterType() {
        return masterType;
    }
    public void setMasterType(String masterType) {
        this.masterType = masterType;
    }
    public Date getTransactionDateFrom() {
        return transactionDateFrom;
    }
    public void setTransactionDateFrom(Date transactionDateFrom) {
        this.transactionDateFrom = transactionDateFrom;
    }
    public Date getTransactionDateTo() {
        return transactionDateTo;
    }
    public void setTransactionDateTo(Date transactionDateTo) {
        this.transactionDateTo = transactionDateTo;
    }
    public Boolean getTotalAmount() {
        return totalAmount;
    }
    public void setTotalAmount(Boolean totalAmount) {
        this.totalAmount = totalAmount;
    }
    public Boolean getTotalTrans() {
        return totalTrans;
    }
    public void setTotalTrans(Boolean totalTrans) {
        this.totalTrans = totalTrans;
    }

}
