package evision.datamodel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Administrator
 */
import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.attribute.SimpleAttribute;
import com.googlecode.cqengine.query.option.QueryOptions;

public class MatchingTemplate {

    private String matchingkey;
    private String transactionid;
    private String matchsidecount;

    public MatchingTemplate(String matchingkey, String transactionid, String matchsidecount) {
        this.matchingkey = matchingkey;
        this.transactionid = transactionid;
        this.matchsidecount = matchsidecount;
    }

    public String getMatchingkey() {
        return matchingkey;
    }

    public void setMatchingkey(String matchingkey) {
        this.matchingkey = matchingkey;
    }

    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public String getMatchsidecount() {
        return matchsidecount;
    }

    public void setMatchsidecount(String matchsidecount) {
        this.matchsidecount = matchsidecount;
    }

    public static final Attribute<MatchingTemplate, String> MATCHING_KEY = new SimpleAttribute<MatchingTemplate, String>("matchingkey") {
        @Override
        public String getValue(MatchingTemplate matchingtemplate, QueryOptions qo) {
            return matchingtemplate.matchingkey;
        }
    };

}
