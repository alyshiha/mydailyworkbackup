/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.datamodel;

import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.attribute.SimpleAttribute;
import com.googlecode.cqengine.query.option.QueryOptions;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author shi7a
 */
public class SwitchEntity {

    private String bankcode;
    private String terminal;
    private String pan;
    private String accountnumber;
    
    private Integer trace;
    private String transtype;
    private String rrn;
    private Integer acquirer;
    
    private Integer authnumber;
    
    private BigDecimal transactionamount;
    private BigDecimal settelmentamount;
    private BigDecimal acquireramount;
    
    private Date txdatetime;
    private Date settelmentdate;
    
    private Integer responsecode;
    private Integer settelmentcode;
    private Integer acquirercode;
    
    public SwitchEntity(Date txdatetime, String terminal, BigDecimal transactionamount, String bankcode, String transtype, Integer responsecode, String pan, Integer trace, BigDecimal settelmentamount, Integer settelmentcode, Integer acquirer, String rrn, Date settelmentdate, Integer authnumber, BigDecimal acquireramount, Integer acquirercode, String accountnumber) {
        this.txdatetime = txdatetime;
        this.terminal = terminal;
        this.transactionamount = transactionamount;
        this.bankcode = bankcode;
        this.transtype = transtype;
        this.responsecode = responsecode;
        this.pan = pan;
        this.trace = trace;
        this.settelmentamount = settelmentamount;
        this.settelmentcode = settelmentcode;
        this.acquirer = acquirer;
        this.rrn = rrn;
        this.settelmentdate = settelmentdate;
        this.authnumber = authnumber;
        this.acquireramount = acquireramount;
        this.acquirercode = acquirercode;
        this.accountnumber = accountnumber;
    }

    public Date getTxdatetime() {
        return txdatetime;
    }

    public void setTxdatetime(Date txdatetime) {
        this.txdatetime = txdatetime;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public BigDecimal getTransactionamount() {
        return transactionamount;
    }

    public void setTransactionamount(BigDecimal transactionamount) {
        this.transactionamount = transactionamount;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getTranstype() {
        return transtype;
    }

    public void setTranstype(String transtype) {
        this.transtype = transtype;
    }

    public Integer getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(Integer responsecode) {
        this.responsecode = responsecode;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public Integer getTrace() {
        return trace;
    }

    public void setTrace(Integer trace) {
        this.trace = trace;
    }

    public BigDecimal getSettelmentamount() {
        return settelmentamount;
    }

    public void setSettelmentamount(BigDecimal settelmentamount) {
        this.settelmentamount = settelmentamount;
    }

    public Integer getSettelmentcode() {
        return settelmentcode;
    }

    public void setSettelmentcode(Integer settelmentcode) {
        this.settelmentcode = settelmentcode;
    }

    public Integer getAcquirer() {
        return acquirer;
    }

    public void setAcquirer(Integer acquirer) {
        this.acquirer = acquirer;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public Date getSettelmentdate() {
        return settelmentdate;
    }

    public void setSettelmentdate(Date settelmentdate) {
        this.settelmentdate = settelmentdate;
    }

    public Integer getAuthnumber() {
        return authnumber;
    }

    public void setAuthnumber(Integer authnumber) {
        this.authnumber = authnumber;
    }

    public BigDecimal getAcquireramount() {
        return acquireramount;
    }

    public void setAcquireramount(BigDecimal acquireramount) {
        this.acquireramount = acquireramount;
    }

    public Integer getAcquirercode() {
        return acquirercode;
    }

    public void setAcquirercode(Integer acquirercode) {
        this.acquirercode = acquirercode;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }
    public static final Attribute<SwitchEntity, Date> TXDATETIME = new SimpleAttribute<SwitchEntity, Date>("txdatetime") {
        @Override
        public Date getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.txdatetime;
        }
    };
    public static final Attribute<SwitchEntity, String> TERMINAL = new SimpleAttribute<SwitchEntity, String>("terminal") {
        @Override
        public String getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.terminal;
        }
    };
    public static final Attribute<SwitchEntity, BigDecimal> TERMINALAMOUNT = new SimpleAttribute<SwitchEntity, BigDecimal>("transactionamount") {
        @Override
        public BigDecimal getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.transactionamount;
        }
    };
    public static final Attribute<SwitchEntity, String> BANKCODE = new SimpleAttribute<SwitchEntity, String>("bankcode") {
        @Override
        public String getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.bankcode;
        }
    };
    public static final Attribute<SwitchEntity, String> TRANSTYPE = new SimpleAttribute<SwitchEntity, String>("transtype") {
        @Override
        public String getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.transtype;
        }
    };
    public static final Attribute<SwitchEntity, Integer> RESPONCETYPE = new SimpleAttribute<SwitchEntity, Integer>("responsecode") {
        @Override
        public Integer getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.responsecode;
        }
    };
    public static final Attribute<SwitchEntity, String> PAN = new SimpleAttribute<SwitchEntity, String>("pan") {
        @Override
        public String getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.pan;
        }
    };
    public static final Attribute<SwitchEntity, Integer> TRACE = new SimpleAttribute<SwitchEntity, Integer>("trace") {
        @Override
        public Integer getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.trace;
        }
    };
    public static final Attribute<SwitchEntity, BigDecimal> SETTELMENTAMOUNT = new SimpleAttribute<SwitchEntity, BigDecimal>("settelmentamount") {
        @Override
        public BigDecimal getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.settelmentamount;
        }
    };
    public static final Attribute<SwitchEntity, Integer> SETTELMENTCODE = new SimpleAttribute<SwitchEntity, Integer>("settelmentcode") {
        @Override
        public Integer getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.settelmentcode;
        }
    };
    public static final Attribute<SwitchEntity, Integer> ACQUIRER = new SimpleAttribute<SwitchEntity, Integer>("acquirer") {
        @Override
        public Integer getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.acquirer;
        }
    };
    public static final Attribute<SwitchEntity, String> RRN = new SimpleAttribute<SwitchEntity, String>("rrn") {
        @Override
        public String getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.rrn;
        }
    };
    public static final Attribute<SwitchEntity, Date> SETTELMENTDATE = new SimpleAttribute<SwitchEntity, Date>("settelmentdate") {
        @Override
        public Date getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.settelmentdate;
        }
    };
    public static final Attribute<SwitchEntity, Integer> AUTHNUMBER = new SimpleAttribute<SwitchEntity, Integer>("authnumber") {
        @Override
        public Integer getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.authnumber;
        }
    };
    public static final Attribute<SwitchEntity, BigDecimal> ACQUIRERAMOUNT = new SimpleAttribute<SwitchEntity, BigDecimal>("acquireramount") {
        @Override
        public BigDecimal getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.acquireramount;
        }
    };
    public static final Attribute<SwitchEntity, Integer> ACQUIRERCODE = new SimpleAttribute<SwitchEntity, Integer>("acquirercode") {
        @Override
        public Integer getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.acquirercode;
        }
    };
    public static final Attribute<SwitchEntity, String> ACCOUNTNUMBER = new SimpleAttribute<SwitchEntity, String>("accountnumber") {
        @Override
        public String getValue(SwitchEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.accountnumber;
        }
    };
}
