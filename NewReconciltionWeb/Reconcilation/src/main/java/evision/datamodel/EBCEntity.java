/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.datamodel;

import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.attribute.SimpleAttribute;
import com.googlecode.cqengine.query.option.QueryOptions;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author shi7a
 */
public class EBCEntity {

    private String uniquereferencenumber;
    private String pan;
    private String ledgereffect;
    private String actioncode;
    private String topan;
    private BigDecimal transactionamount;
    private BigDecimal feesamount;
    private Date txdatetime;
    private String rrn;
    private Integer responsecode;
    private Integer authnumber;
    private String octrrn;
    private String cardacceptorterminalid;
    private String cardacceptoridcode;
    private String terminal;

    public EBCEntity(String uniquereferencenumber, String pan, String ledgereffect, String actioncode, String topan, BigDecimal transactionamount, BigDecimal feesamount, Date txdatetime, String rrn, Integer responsecode, Integer authnumber, String octrrn, String cardacceptorterminalid, String cardacceptoridcode, String terminal) {
        this.uniquereferencenumber = uniquereferencenumber;
        this.pan = pan;
        this.ledgereffect = ledgereffect;
        this.actioncode = actioncode;
        this.topan = topan;
        this.transactionamount = transactionamount;
        this.feesamount = feesamount;
        this.txdatetime = txdatetime;
        this.rrn = rrn;
        this.responsecode = responsecode;
        this.authnumber = authnumber;
        this.octrrn = octrrn;
        this.cardacceptorterminalid = cardacceptorterminalid;
        this.cardacceptoridcode = cardacceptoridcode;
        this.terminal = terminal;
    }

    public String getUniquereferencenumber() {
        return uniquereferencenumber;
    }

    public void setUniquereferencenumber(String uniquereferencenumber) {
        this.uniquereferencenumber = uniquereferencenumber;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getLedgereffect() {
        return ledgereffect;
    }

    public void setLedgereffect(String ledgereffect) {
        this.ledgereffect = ledgereffect;
    }

    public String getActioncode() {
        return actioncode;
    }

    public void setActioncode(String actioncode) {
        this.actioncode = actioncode;
    }

    public String getTopan() {
        return topan;
    }

    public void setTopan(String topan) {
        this.topan = topan;
    }

    public BigDecimal getTransactionamount() {
        return transactionamount;
    }

    public void setTransactionamount(BigDecimal transactionamount) {
        this.transactionamount = transactionamount;
    }

    public BigDecimal getFeesamount() {
        return feesamount;
    }

    public void setFeesamount(BigDecimal feesamount) {
        this.feesamount = feesamount;
    }

    public Date getTxdatetime() {
        return txdatetime;
    }

    public void setTxdatetime(Date txdatetime) {
        this.txdatetime = txdatetime;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public Integer getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(Integer responsecode) {
        this.responsecode = responsecode;
    }

    public Integer getAuthnumber() {
        return authnumber;
    }

    public void setAuthnumber(Integer authnumber) {
        this.authnumber = authnumber;
    }

    public String getOctrrn() {
        return octrrn;
    }

    public void setOctrrn(String octrrn) {
        this.octrrn = octrrn;
    }

    public String getCardacceptorterminalid() {
        return cardacceptorterminalid;
    }

    public void setCardacceptorterminalid(String cardacceptorterminalid) {
        this.cardacceptorterminalid = cardacceptorterminalid;
    }

    public String getCardacceptoridcode() {
        return cardacceptoridcode;
    }

    public void setCardacceptoridcode(String cardacceptoridcode) {
        this.cardacceptoridcode = cardacceptoridcode;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }
    public static final Attribute<EBCEntity, String> UNIQUEREFERENCENUMBER = new SimpleAttribute<EBCEntity, String>("uniquereferencenumber") {
        @Override
        public String getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.uniquereferencenumber;
        }
    };
    public static final Attribute<EBCEntity, String> PAN = new SimpleAttribute<EBCEntity, String>("pan") {
        @Override
        public String getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.pan;
        }
    };
    public static final Attribute<EBCEntity, String> LEDGEREFFECT = new SimpleAttribute<EBCEntity, String>("ledgereffect") {
        @Override
        public String getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.ledgereffect;
        }
    };
    public static final Attribute<EBCEntity, String> ACTIONCODE = new SimpleAttribute<EBCEntity, String>("actioncode") {
        @Override
        public String getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.actioncode;
        }
    };
    public static final Attribute<EBCEntity, String> TOPAN = new SimpleAttribute<EBCEntity, String>("topan") {
        @Override
        public String getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.topan;
        }
    };
    public static final Attribute<EBCEntity, BigDecimal> TRANSACTIONAMOUNT = new SimpleAttribute<EBCEntity, BigDecimal>("transactionamount") {
        @Override
        public BigDecimal getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.transactionamount;
        }
    };
    public static final Attribute<EBCEntity, BigDecimal> FEESAMOUNT = new SimpleAttribute<EBCEntity, BigDecimal>("feesamount") {
        @Override
        public BigDecimal getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.feesamount;
        }
    };
    public static final Attribute<EBCEntity, Date> TXDATETIME = new SimpleAttribute<EBCEntity, Date>("txdatetime") {
        @Override
        public Date getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.txdatetime;
        }
    };
    public static final Attribute<EBCEntity, String> RRN = new SimpleAttribute<EBCEntity, String>("rrn") {
        @Override
        public String getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.rrn;
        }
    };
    public static final Attribute<EBCEntity, Integer> RESPONCECODE = new SimpleAttribute<EBCEntity, Integer>("responsecode") {
        @Override
        public Integer getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.responsecode;
        }
    };
    public static final Attribute<EBCEntity, Integer> AUTHNUMBER = new SimpleAttribute<EBCEntity, Integer>("authnumber") {
        @Override
        public Integer getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.authnumber;
        }
    };
    public static final Attribute<EBCEntity, String> OCTRRN = new SimpleAttribute<EBCEntity, String>("octrrn") {
        @Override
        public String getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.octrrn;
        }
    };
    public static final Attribute<EBCEntity, String> CARDACCEPTORTERMINALID = new SimpleAttribute<EBCEntity, String>("cardacceptorterminalid") {
        @Override
        public String getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.cardacceptorterminalid;
        }
    };
    public static final Attribute<EBCEntity, String> CARDACCEPTORIDCODE = new SimpleAttribute<EBCEntity, String>("cardacceptoridcode") {
        @Override
        public String getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.cardacceptoridcode;
        }
    };
    public static final Attribute<EBCEntity, String> TERMINAL = new SimpleAttribute<EBCEntity, String>("terminal") {
        @Override
        public String getValue(EBCEntity ebctransaction, QueryOptions qo) {
            return ebctransaction.terminal;
        }
    };
}
