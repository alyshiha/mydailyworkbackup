/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.datamodel;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author shi7a
 */
public class TransactionSearchCriteria implements Serializable{

    private String transactionDateOperator;
    private Date transactionDateFrom;
    private Date transactionDateTo;

    private String settelmentDateOperator;
    private Date settelmentDateFrom;
    private Date settelmentDateTo;

    private String loadingDateOperator;
    private Date loadingDateFrom;
    private Date loadingDateTo;

    private String actualAmountOperator;
    private String actualAmountFrom;
    private String actualAmountTo;

    private String settelmentAmountOperator;
    private String settelmentAmountFrom;
    private String settelmentAmountTo;

    private String acquireAmountOperator;
    private String acquireAmountFrom;
    private String acquireAmountTo;

    private String baseCurrency;
    private String settelmentCurrency;
    private String acquireCurrency;

    private String cardNumberOperator;
    private String cardNumber;

    private String accountNumberOperator;
    private String accountNumber;

    private Boolean blacklisted;

    private String transactionTypeOperator;
    private String transactionType;

    private String errorOperator;
    private String error;

    private String responceOperator;
    private String responce;

    private String sequenceOperator;
    private String sequenceFrom;
    private String sequenceTo;

    private String disputeby;
    private String setteledby;

    private String view;
    private String reverse;
    private String setteledstate;
    private String disputestate;

    public String getTransactionDateOperator() {
        return transactionDateOperator;
    }
    public void setTransactionDateOperator(String transactionDateOperator) {
        this.transactionDateOperator = transactionDateOperator;
    }
    public Date getTransactionDateFrom() {
        return transactionDateFrom;
    }
    public void setTransactionDateFrom(Date transactionDateFrom) {
        this.transactionDateFrom = transactionDateFrom;
    }
    public Date getTransactionDateTo() {
        return transactionDateTo;
    }
    public void setTransactionDateTo(Date transactionDateTo) {
        this.transactionDateTo = transactionDateTo;
    }
    public String getSettelmentDateOperator() {
        return settelmentDateOperator;
    }
    public void setSettelmentDateOperator(String settelmentDateOperator) {
        this.settelmentDateOperator = settelmentDateOperator;
    }
    public Date getSettelmentDateFrom() {
        return settelmentDateFrom;
    }
    public void setSettelmentDateFrom(Date settelmentDateFrom) {
        this.settelmentDateFrom = settelmentDateFrom;
    }
    public Date getSettelmentDateTo() {
        return settelmentDateTo;
    }
    public void setSettelmentDateTo(Date settelmentDateTo) {
        this.settelmentDateTo = settelmentDateTo;
    }
    public String getLoadingDateOperator() {
        return loadingDateOperator;
    }
    public void setLoadingDateOperator(String loadingDateOperator) {
        this.loadingDateOperator = loadingDateOperator;
    }
    public Date getLoadingDateFrom() {
        return loadingDateFrom;
    }
    public void setLoadingDateFrom(Date loadingDateFrom) {
        this.loadingDateFrom = loadingDateFrom;
    }
    public Date getLoadingDateTo() {
        return loadingDateTo;
    }
    public void setLoadingDateTo(Date loadingDateTo) {
        this.loadingDateTo = loadingDateTo;
    }
    public String getActualAmountOperator() {
        return actualAmountOperator;
    }
    public void setActualAmountOperator(String actualAmountOperator) {
        this.actualAmountOperator = actualAmountOperator;
    }
    public String getActualAmountFrom() {
        return actualAmountFrom;
    }
    public void setActualAmountFrom(String actualAmountFrom) {
        this.actualAmountFrom = actualAmountFrom;
    }
    public String getActualAmountTo() {
        return actualAmountTo;
    }
    public void setActualAmountTo(String actualAmountTo) {
        this.actualAmountTo = actualAmountTo;
    }
    public String getSettelmentAmountOperator() {
        return settelmentAmountOperator;
    }
    public void setSettelmentAmountOperator(String settelmentAmountOperator) {
        this.settelmentAmountOperator = settelmentAmountOperator;
    }
    public String getSettelmentAmountFrom() {
        return settelmentAmountFrom;
    }
    public void setSettelmentAmountFrom(String settelmentAmountFrom) {
        this.settelmentAmountFrom = settelmentAmountFrom;
    }
    public String getSettelmentAmountTo() {
        return settelmentAmountTo;
    }
    public void setSettelmentAmountTo(String settelmentAmountTo) {
        this.settelmentAmountTo = settelmentAmountTo;
    }
    public String getAcquireAmountOperator() {
        return acquireAmountOperator;
    }
    public void setAcquireAmountOperator(String acquireAmountOperator) {
        this.acquireAmountOperator = acquireAmountOperator;
    }
    public String getAcquireAmountFrom() {
        return acquireAmountFrom;
    }
    public void setAcquireAmountFrom(String acquireAmountFrom) {
        this.acquireAmountFrom = acquireAmountFrom;
    }
    public String getAcquireAmountTo() {
        return acquireAmountTo;
    }
    public void setAcquireAmountTo(String acquireAmountTo) {
        this.acquireAmountTo = acquireAmountTo;
    }
    public String getBaseCurrency() {
        return baseCurrency;
    }
    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }
    public String getSettelmentCurrency() {
        return settelmentCurrency;
    }
    public void setSettelmentCurrency(String settelmentCurrency) {
        this.settelmentCurrency = settelmentCurrency;
    }
    public String getAcquireCurrency() {
        return acquireCurrency;
    }
    public void setAcquireCurrency(String acquireCurrency) {
        this.acquireCurrency = acquireCurrency;
    }
    public String getCardNumberOperator() {
        return cardNumberOperator;
    }
    public void setCardNumberOperator(String cardNumberOperator) {
        this.cardNumberOperator = cardNumberOperator;
    }
    public String getCardNumber() {
        return cardNumber;
    }
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
    public String getAccountNumberOperator() {
        return accountNumberOperator;
    }
    public void setAccountNumberOperator(String accountNumberOperator) {
        this.accountNumberOperator = accountNumberOperator;
    }
    public String getAccountNumber() {
        return accountNumber;
    }
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public Boolean getBlacklisted() {
        return blacklisted;
    }
    public void setBlacklisted(Boolean blacklisted) {
        this.blacklisted = blacklisted;
    }
    public String getTransactionTypeOperator() {
        return transactionTypeOperator;
    }
    public void setTransactionTypeOperator(String transactionTypeOperator) {
        this.transactionTypeOperator = transactionTypeOperator;
    }
    public String getTransactionType() {
        return transactionType;
    }
    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
    public String getErrorOperator() {
        return errorOperator;
    }
    public void setErrorOperator(String errorOperator) {
        this.errorOperator = errorOperator;
    }
    public String getError() {
        return error;
    }
    public void setError(String error) {
        this.error = error;
    }
    public String getResponceOperator() {
        return responceOperator;
    }
    public void setResponceOperator(String responceOperator) {
        this.responceOperator = responceOperator;
    }
    public String getResponce() {
        return responce;
    }
    public void setResponce(String responce) {
        this.responce = responce;
    }
    public String getSequenceOperator() {
        return sequenceOperator;
    }
    public void setSequenceOperator(String sequenceOperator) {
        this.sequenceOperator = sequenceOperator;
    }
    public String getSequenceFrom() {
        return sequenceFrom;
    }
    public void setSequenceFrom(String sequenceFrom) {
        this.sequenceFrom = sequenceFrom;
    }
    public String getSequenceTo() {
        return sequenceTo;
    }
    public void setSequenceTo(String sequenceTo) {
        this.sequenceTo = sequenceTo;
    }
    public String getDisputeby() {
        return disputeby;
    }
    public void setDisputeby(String disputeby) {
        this.disputeby = disputeby;
    }
    public String getSetteledby() {
        return setteledby;
    }
    public void setSetteledby(String setteledby) {
        this.setteledby = setteledby;
    }
    public String getView() {
        return view;
    }
    public void setView(String view) {
        this.view = view;
    }
    public String getReverse() {
        return reverse;
    }
    public void setReverse(String reverse) {
        this.reverse = reverse;
    }
    public String getSetteledstate() {
        return setteledstate;
    }
    public void setSetteledstate(String setteledstate) {
        this.setteledstate = setteledstate;
    }
    public String getDisputestate() {
        return disputestate;
    }
    public void setDisputestate(String disputestate) {
        this.disputestate = disputestate;
    }
}
