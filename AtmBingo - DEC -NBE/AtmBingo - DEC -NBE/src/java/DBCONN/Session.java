/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBCONN;

import com.ev.AtmBingo.bus.bo.AtmStatByCashRepBOInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.DisputesBOInter;
import com.ev.AtmBingo.bus.bo.NotificationBOInter;
import com.ev.AtmBingo.bus.bo.PendingAtmsBOInter;
import com.ev.AtmBingo.bus.bo.RejectedBOInter;
import com.ev.AtmBingo.bus.bo.ReplanishmentDetailsBOInter;
import com.ev.AtmBingo.bus.bo.ReplanishmentReportBOInter;
import com.ev.AtmBingo.bus.bo.ResponseCodesDefinitionBOInter;
import com.ev.AtmBingo.bus.bo.RevenueRepBOInter;
import com.ev.AtmBingo.bus.dao.ColumnDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.LicenseDAOInter;
import com.ev.AtmBingo.bus.dao.SystemTableDAOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.LicenseDTOInter;
import com.ev.AtmBingo.bus.dto.MatchingTypeDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkGroupDTOInter;
import com.ev.AtmBingo.bus.dto.NotificationDTOInter;
import com.ev.AtmBingo.bus.dto.PendingAtmsDTOInter;
import com.ev.AtmBingo.bus.dto.SystemTableDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseMasterDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.ValidationRulesDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author AlyShiha
 */
public class Session implements Serializable {

    public synchronized void GenerateSession(UsersDTOInter logedinUser) {
        try {
            //System.out.println("Before");
            // getmemo();
            GetNotificationsList();
            GetErrorsList();
            GetPendingAtmsList();
            GetTimeFrom();
            GetTimeTo();
            GetNetworkGroupList();
            GetATMGroupList();
            GetUsersList();
            GetCassetteList();
            GetColumnsList();
            GetCurrencyMasterList();
            GetMatchingTypeList();
            GetTransactionResponseCodeList();
            GetSettledUsersList();
            GetTransactionTypeList();
            GetTransactionStatusList();
            GetValidationRulesList();
            GetTransTime();
            GetDefaultColumn();
            GetNonDefaultColumn();
            GetAtmMachineList(logedinUser);
            GetTransactionResponseMasterList();
            GetLicenseSessions();
            SetIP();
            //System.out.println("After");
//            getmemo();

        } catch (Throwable ex) {
            Logger.getLogger(Session.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public synchronized List<String> GetUserPages() throws Exception, Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        return (List<String>) session.getAttribute("UserPages");
    }

    public synchronized void SetUserLogging(UsersDTOInter user) {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.setAttribute("UserLogging", user);
    }

    public synchronized void SetIP() throws Throwable {
        String ip = "";
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if (req.getHeader("x-forwarded-for") == null) {
            ip = req.getRemoteAddr();
        } else {
            ip = req.getHeader("x-forwarded-for");
        }
        session.setAttribute("ip", ip);
    }

    public synchronized void GetLicenseSessions() throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        LicenseDAOInter lDAO = DAOFactory.createLicenseDAO();
        LicenseDTOInter LDTO = (LicenseDTOInter) lDAO.findLicense();
        if (session.getAttribute("client") == null) {
            session.setAttribute("client", LDTO.getLicenseTo() + " - " + LDTO.getCountry());
        }
        if (session.getAttribute("version") == null) {
            session.setAttribute("version", LDTO.getVersion());
        }
        if (session.getAttribute("UserCount") == null) {
            session.setAttribute("UserCount", LDTO.getNoOfUser());
        }
        if (session.getAttribute("EndDate") == null) {
            session.setAttribute("EndDate", LDTO.getEndDate());
        }

    }
    private String client;

    public synchronized String Getclient() throws Throwable {

        LicenseDAOInter lDAO = DAOFactory.createLicenseDAO();
        LicenseDTOInter LDTO = (LicenseDTOInter) lDAO.findLicense();
        if (client == null) {
            client = LDTO.getLicenseTo() + " - " + LDTO.getCountry();
        }
        return (String) client;
    }

    public synchronized UsersDTOInter GetUserLogging() throws Exception, Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        return (UsersDTOInter) session.getAttribute("UserLogging");
    }
    private List<TransactionResponseMasterDTOInter> TransactionResponseMasterList;

    public synchronized List<TransactionResponseMasterDTOInter> GetTransactionResponseMasterList() throws Throwable {
        ResponseCodesDefinitionBOInter cdObject = BOFactory.createResponseCodeDefBO(null);
        if (TransactionResponseMasterList == null) {
            TransactionResponseMasterList = (List<TransactionResponseMasterDTOInter>) cdObject.getCurrencyMaster();
        }
        return TransactionResponseMasterList;
    }

    public synchronized List<AtmMachineDTOInter> GetAtmMachineList(UsersDTOInter user) throws Throwable {
        DisputesBOInter mcObject = BOFactory.createDisputeBO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("AtmMachineList") == null) {
            session.setAttribute("AtmMachineList", (List<AtmMachineDTOInter>) mcObject.getAtmMachines(user, 0));
        }
        return (List<AtmMachineDTOInter>) session.getAttribute("AtmMachineList");
    }

    private List<ColumnDTOInter> DefaultColumn;

    public synchronized List<ColumnDTOInter> GetDefaultColumn() throws Throwable {
        ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);

        if (DefaultColumn == null) {
            DefaultColumn = (List<ColumnDTOInter>) cDAO.findDefualt();
        }
        return DefaultColumn;
    }

    private List<ColumnDTOInter> NonDefaultColumn;

    public synchronized List<ColumnDTOInter> GetNonDefaultColumn() throws Throwable {
        ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (NonDefaultColumn == null) {
            NonDefaultColumn = (List<ColumnDTOInter>) cDAO.findNotDefualt();
        }
        return NonDefaultColumn;
    }
    private List<NotificationDTOInter> NotificationsList;

    public synchronized List<NotificationDTOInter> GetNotificationsList() throws Throwable {
        NotificationBOInter NotificationBO;
        NotificationBO = BOFactory.createNotificationBO(null);
        if (NotificationsList == null) {
            NotificationsList = (List<NotificationDTOInter>) NotificationBO.getNotifications();
        }
        return NotificationsList;
    }

    private List<NotificationDTOInter> ErrorsList;

    public synchronized List<NotificationDTOInter> GetErrorsList() throws Throwable {
        NotificationBOInter NotificationBO;
        NotificationBO = BOFactory.createNotificationBO(null);

        if (ErrorsList == null) {
            ErrorsList = (List<NotificationDTOInter>) NotificationBO.getErrors();
        }
        return ErrorsList;
    }

    private List<PendingAtmsDTOInter> PendingAtmsList;

    public synchronized List<PendingAtmsDTOInter> GetPendingAtmsList() throws Throwable {
        PendingAtmsBOInter PendingAtmsBO;
        PendingAtmsBO = BOFactory.createPendingAtmsBo(null);

        if (PendingAtmsList == null) {
            PendingAtmsList = (List<PendingAtmsDTOInter>) PendingAtmsBO.getPendingAtms();
        }
        return PendingAtmsList;
    }

    private String DateFromVariable = "";

    public synchronized String GetTimeFrom() {

        if (DateFromVariable == "") {
            DateFromVariable = (String) getParameter("TIMETO");
        }
        return DateFromVariable;
    }

    private String DateToVariable = "";

    public synchronized String GetTimeTo() {
        if (DateToVariable == "") {
            DateToVariable = (String) getParameter("TIMEFROM");
        }
        return DateToVariable;
    }

    private List<NetworkGroupDTOInter> NetworkGroupList;

    public synchronized List<NetworkGroupDTOInter> GetNetworkGroupList() throws Throwable {
        RevenueRepBOInter revenueBO;
        revenueBO = BOFactory.createRevenueBOInter(null);

        if (NetworkGroupList == null) {
            NetworkGroupList = (List<NetworkGroupDTOInter>) revenueBO.findNetworkGroup();
        }
        return NetworkGroupList;
    }

    private List<AtmGroupDTOInter> ATMGroupList;

    public synchronized List<AtmGroupDTOInter> GetATMGroupList() throws Throwable {
        AtmStatByCashRepBOInter atmByCashRep;
        atmByCashRep = BOFactory.createAtmStatByCashRepBO(null);

        if (ATMGroupList == null) {
            ATMGroupList = (List<AtmGroupDTOInter>) atmByCashRep.getGrp();
        }
        return ATMGroupList;
    }

    private List<UsersDTOInter> UsersList;

    public synchronized List<UsersDTOInter> GetUsersList() throws Throwable {
        ReplanishmentReportBOInter repDetailsReport;
        repDetailsReport = BOFactory.createReplanishmentReportBO(null);

        if (UsersList == null) {
            UsersList = (List<UsersDTOInter>) repDetailsReport.getUsers();
        }
        return UsersList;
    }

    private List<CassetteDTOInter> CassettesList;

    public synchronized List<CassetteDTOInter> GetCassetteList() throws Throwable {
        ReplanishmentDetailsBOInter cdObject;
        cdObject = BOFactory.createReplanishmentBO(null);
        if (CassettesList == null) {
            CassettesList = (List<CassetteDTOInter>) cdObject.getCassetes();
        }
        return CassettesList;
    }

    private List<ColumnDTOInter> ColumnsList;

    public synchronized List<ColumnDTOInter> GetColumnsList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);
        if (ColumnsList == null) {
            ColumnsList = (List<ColumnDTOInter>) mcObject.getFileColumnDefinition();
        }
        return ColumnsList;
    }

    private List<CurrencyMasterDTOInter> CurrencyMasterList;

    public synchronized List<CurrencyMasterDTOInter> GetCurrencyMasterList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);

        if (CurrencyMasterList == null) {
            CurrencyMasterList = (List<CurrencyMasterDTOInter>) mcObject.getCurrencyMaster();
        }
        return CurrencyMasterList;
    }

    private List<MatchingTypeDTOInter> MatchingTypeList;

    public synchronized List<MatchingTypeDTOInter> GetMatchingTypeList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);

        if (MatchingTypeList == null) {
            MatchingTypeList = (List<MatchingTypeDTOInter>) mcObject.getMatchingType();
        }
        return MatchingTypeList;
    }

    private List<TransactionResponseCodeDTOInter> TransactionResponseCodeList;

    public synchronized List<TransactionResponseCodeDTOInter> GetTransactionResponseCodeList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);
        if (TransactionResponseCodeList == null) {
            TransactionResponseCodeList = (List<TransactionResponseCodeDTOInter>) mcObject.getTransactionResponseCode();
        }
        return TransactionResponseCodeList;
    }

    private List<UsersDTOInter> SettledUsersList;

    public synchronized List<UsersDTOInter> GetSettledUsersList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);
        if (SettledUsersList == null) {
            SettledUsersList = (List<UsersDTOInter>) mcObject.getSettledUsers();
        }
        return SettledUsersList;
    }

    private List<TransactionTypeDTOInter> TransactionTypeList;

    public synchronized List<TransactionTypeDTOInter> GetTransactionTypeList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);
        if (TransactionTypeList == null) {
            TransactionTypeList = (List<TransactionTypeDTOInter>) mcObject.getTransactionType();
        }
        return TransactionTypeList;
    }

    private List<TransactionStatusDTOInter> TransactionStatusList;

    public synchronized List<TransactionStatusDTOInter> GetTransactionStatusList() throws Throwable {
        DisputesBOInter mcObject;
        mcObject = BOFactory.createDisputeBO(null);

        if (TransactionStatusList == null) {
            TransactionStatusList = (List<TransactionStatusDTOInter>) mcObject.getTransactionStatus();
        }
        return TransactionStatusList;
    }

    private List<ValidationRulesDTOInter> ValidationRulesList;

    public synchronized List<ValidationRulesDTOInter> GetValidationRulesList() throws Throwable {
        RejectedBOInter mcObject;
        mcObject = BOFactory.createRejectedBO(null);

        if (ValidationRulesList == null) {
            ValidationRulesList = (List<ValidationRulesDTOInter>) mcObject.getValidationRules(1);
        }
        return ValidationRulesList;
    }

    private SystemTableDTOInter TRANSACTIONTIME;

    public  synchronized SystemTableDTOInter GetTransTime() throws Throwable {
        SystemTableDAOInter stDAO = DAOFactory.createSystemTableDAO(null);
        if (TRANSACTIONTIME == null) {
            TRANSACTIONTIME = (SystemTableDTOInter) stDAO.find("TRANSACTION TIME");
        }
        return TRANSACTIONTIME;
    }

    public synchronized Object getParameter(String paramName) {
        SystemTableDAOInter stDAO = DAOFactory.createSystemTableDAO(null);
        SystemTableDTOInter stDTO = null;
        try {
            stDTO = (SystemTableDTOInter) stDAO.find(paramName);
            return stDTO.getParameterValue();
        } catch (Throwable ex) {
            return null;
        }
    }

    public synchronized void clearsessionattributes() throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session != null) {
            Enumeration attributeNames = session.getAttributeNames();
            while (attributeNames.hasMoreElements()) {
                String sAttribute = attributeNames.nextElement().toString();
                session.removeAttribute(sAttribute);
            }
        }
    }
}
