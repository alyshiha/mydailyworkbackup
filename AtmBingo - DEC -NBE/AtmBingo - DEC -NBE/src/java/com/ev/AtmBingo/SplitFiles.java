/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;

/**
 *
 * @author Aly
 */
public class SplitFiles  implements Serializable{

    public static void ConvertFile(String sourceFileName, String directory,String filename) throws Exception {
        String destinationFileName = directory + "\\"+filename+"-Part-{0}.txt";
        FileInputStream fstream = new FileInputStream(sourceFileName);
        BufferedReader sourceFile = new BufferedReader(new InputStreamReader(fstream));
        Integer linesPerFile = 1000;

        Integer fileCounter = 0;
        FileWriter destinationFile = new FileWriter(String.format(destinationFileName, fileCounter + 1));

        try {
            int lineCounter = 0;

            String line;
            while ((line = sourceFile.readLine()) != null) {
                // Did we reach the maximum number of lines for the file?
                if (lineCounter >= linesPerFile) {
                    // Yep... Time to close this one and
                    // switch to another file
                    lineCounter = 0;
                    fileCounter++;
                    destinationFile.close();
                    destinationFile = new FileWriter(destinationFileName.replace("{0}", fileCounter.toString()));
                }

                destinationFile.write(line + "\n");
                lineCounter++;
            }

        } finally {
            sourceFile.close();
            fstream.close();
            destinationFile.close();
        }

    }

    public static void getList(String directory)
            throws Exception {
        File[] foldersep = new File(directory).listFiles();
        for (int i = 0; i < foldersep.length; i++) {
            ConvertFile(foldersep[i].getPath(), directory,foldersep[i].getName());
            Path path = Paths.get(foldersep[i].getPath());
            Files.delete(path);

        }
        File[] folder = new File(directory).listFiles();
        String element;
        long size;
        for (int i = 0; i < folder.length; i++) {
            if (folder[i].isFile()) {
                element = folder[i].getName();
                size = folder[i].length() / 1024;
            }
        }
    }

    public static void main(String[] args) throws Exception {

        SplitFiles.getList("E:\\Folder");
    }
}
