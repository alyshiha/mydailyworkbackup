/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
/**
 *
 * @author Aly
 */
public class TextEncoding  implements Serializable{
private static char[] l720to1256 = new char[256];
  
  static
  {
    l720to1256['ÿ'] = ' ';
    l720to1256[''] = '£';
    l720to1256[''] = '¤';
    l720to1256['®'] = '«';
    l720to1256['ø'] = '°';
    l720to1256['ý'] = '²';
    l720to1256['æ'] = 'µ';
    l720to1256['ú'] = '·';
    l720to1256['¯'] = '»';
    l720to1256[''] = 'à';
    l720to1256[''] = 'â';
    l720to1256[''] = 'ç';
    l720to1256[''] = 'è';
    l720to1256[''] = 'é';
    l720to1256[''] = 'ê';
    l720to1256[''] = 'ë';
    l720to1256[''] = 'î';
    l720to1256[''] = 'ï';
    l720to1256[''] = 'ô';
    l720to1256[''] = 'û';
    l720to1256[''] = 'Á';
    l720to1256[''] = 'Â';
    l720to1256[''] = 'Ã';
    l720to1256[''] = 'Ä';
    l720to1256[''] = 'Å';
    l720to1256[''] = 'Æ';
    l720to1256[''] = 'Ç';
    l720to1256[' '] = 'È';
    l720to1256['¡'] = 'É';
    l720to1256['¢'] = 'Ê';
    l720to1256['£'] = 'Ë';
    l720to1256['¤'] = 'Ì';
    l720to1256['¥'] = 'Í';
    l720to1256['¦'] = 'Î';
    l720to1256['§'] = 'Ï';
    l720to1256['¨'] = 'Ð';
    l720to1256['©'] = 'Ñ';
    l720to1256['ª'] = 'Ò';
    l720to1256['«'] = 'Ó';
    l720to1256['¬'] = 'Ô';
    l720to1256['­'] = 'Õ';
    l720to1256['à'] = 'Ö';
    l720to1256['á'] = 'Ø';
    l720to1256['â'] = 'Ù';
    l720to1256['ã'] = 'Ú';
    l720to1256['ä'] = 'Û';
    l720to1256[''] = 'Ü';
    l720to1256['å'] = 'Ý';
    l720to1256['ç'] = 'Þ';
    l720to1256['è'] = 'ß';
    l720to1256['é'] = 'á';
    l720to1256['ê'] = 'ã';
    l720to1256['ë'] = 'ä';
    l720to1256['ì'] = 'å';
    l720to1256['í'] = 'æ';
    l720to1256['î'] = 'ì';
    l720to1256['ï'] = 'í';
    l720to1256['ñ'] = 'ð';
    l720to1256['ò'] = 'ñ';
    l720to1256['ó'] = 'ò';
    l720to1256['ô'] = 'ó';
    l720to1256['õ'] = 'õ';
    l720to1256['ö'] = 'ö';
    l720to1256[''] = 'ø';
    l720to1256[''] = 'ú';
    l720to1256['ÿ'] = ' ';
  }
  
  public static final byte[] encodeUTF8(String txt, String from)
    throws UnsupportedEncodingException
  {
    String asString = new String(txt.getBytes(), from);
    byte[] newBytes = asString.getBytes("UTF8");
    
    return newBytes;
  }
  
  public static final synchronized String encode720toUTF8(String txt)
    throws UnsupportedEncodingException
  {
    char[] newBytes = txt.toCharArray();
    char[] chs = new char[newBytes.length];
    String newtxt = "";
    for (int i = 0; i < newBytes.length; i++)
    {
      try
      {
        if (newBytes[i] == 65533) {
          chs[i] = ' ';
        } else if (newBytes[i] == 'Ÿ') {
          chs[i] = 'Ç';
        } else if ((newBytes[i] >= '') && (newBytes[i] <= 'ÿ')) {
          chs[i] = l720to1256[newBytes[i]];
        } else {
          chs[i] = newBytes[i];
        }
      }
      catch (Exception e)
      {
        chs[i] = newBytes[i];
      }
      newtxt = newtxt + chs[i];
    }
    return newtxt;
  }
    public static void main(String[] args) throws Exception {
   byte[] newBytes = TextEncoding.encodeUTF8("أ","Cp1256");
    //System.out.println("d");
    }
}
