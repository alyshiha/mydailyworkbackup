/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;

import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DefualtColumnsDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class DefualtColumnsDAO extends BaseDAO implements DefualtColumnsDAOInter {

    protected DefualtColumnsDAO() {
        super();
        super.setTableName("defualt_columns");
    }

    public Object updatedefault(Object... obj) throws Throwable {
        List<ColumnDTOInter> uDTO = (List<ColumnDTOInter>) obj[0];
        Connection connection = null;
        PreparedStatement statement = null;
        try {

     connection  = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            statement = connection.prepareStatement("update FILE_COLUMN_DEFINITION set display_in_transaction = 1,COLUMN_ORDER_BY = ? where id in (?)");
            for (int i = 0; i < uDTO.size(); i++) {
                ColumnDTOInter Col = uDTO.get(i);
                //  statement.setString(1, "");
                statement.setInt(2, Col.getId());
                statement.setInt(1, i);
                statement.addBatch();


                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
        return null;

    }

    public Object updateundefault(Object... obj) throws Throwable {
        List<ColumnDTOInter> uDTO = (List<ColumnDTOInter>) obj[0];
        Connection connection = null;
        PreparedStatement statement = null;
        try {

           
     connection  = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            statement = connection.prepareStatement("update FILE_COLUMN_DEFINITION set display_in_transaction = 3 where id in (?)");
            for (int i = 0; i < uDTO.size(); i++) {
                ColumnDTOInter Col = uDTO.get(i);
                statement.setInt(1, Col.getId());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
         CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
        return null;

    }

    public Object defaultreset(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String deleteStat = "update FILE_COLUMN_DEFINITION set COLUMN_ORDER_BY = null";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate(" ", false);
        return null;
    }

    public Object deleteAll(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String deleteStat = "delete from $table";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DefualtColumnsDTOInter uDTO = (DefualtColumnsDTOInter) obj[0];
        String deleteStat = "delete from $table where column_id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getColumnId());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where column_id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        DefualtColumnsDTOInter uDTO = DTOFactory.createDefualtColumnsDTO();
        while (rs.next()) {

            uDTO.setColumnId(rs.getInt("column_id"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by column_id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<DefualtColumnsDTOInter> uDTOL = new ArrayList<DefualtColumnsDTOInter>();
        while (rs.next()) {
            DefualtColumnsDTOInter uDTO = DTOFactory.createDefualtColumnsDTO();

            uDTO.setColumnId(rs.getInt("column_id"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }
}
