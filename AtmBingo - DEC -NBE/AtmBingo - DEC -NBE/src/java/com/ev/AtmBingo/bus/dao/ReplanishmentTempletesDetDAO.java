/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ReplanishmentTempletesDetDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ReplanishmentTempletesDetDAO extends BaseDAO implements ReplanishmentTempletesDetDAOInter {

    protected ReplanishmentTempletesDetDAO() {
        super();
        super.setTableName("replanishment_templetes_det");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        ReplanishmentTempletesDetDTOInter uDTO = (ReplanishmentTempletesDetDTOInter) obj[0];
        String insertStat = "insert into $table values ($id , '$starting_string' , $position , $lenght , $type , $line , $typno , $currency)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
       // insertStat = insertStat.replace("$cassitte_id", "" + uDTO.getCassitteId());
        insertStat = insertStat.replace("$starting_string", "" + uDTO.getStartingString());
        insertStat = insertStat.replace("$position", "" + uDTO.getPosition());
        insertStat = insertStat.replace("$lenght", "" + uDTO.getLenght());
        insertStat = insertStat.replace("$type", "" + uDTO.getType());
        insertStat = insertStat.replace("$line", "" + uDTO.getLine());
        insertStat = insertStat.replace("$typno", "" + uDTO.getTypeNo());
        insertStat = insertStat.replace("$currency", "" + uDTO.getCurrency());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        ReplanishmentTempletesDetDTOInter uDTO = (ReplanishmentTempletesDetDTOInter) obj[0];
        Integer id = (Integer) obj[1];
        Integer oldType = (Integer) obj[2];
        Integer oldTypeNo = (Integer) obj[3];
        String updateStat = "update $table set starting_string = '$starting_string' , position = $position , length = $lenght "
                + ",type = $tp , line = $line ,  type_no = $tno, currency = $currency where id = $id and type = $oldType and type_no = $otn ";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        //updateStat = updateStat.replace("$cassitte_id", "" + uDTO.getCassitteId());
        updateStat = updateStat.replace("$starting_string", "" + uDTO.getStartingString());
        updateStat = updateStat.replace("$position", "" + uDTO.getPosition());
        updateStat = updateStat.replace("$lenght", "" + uDTO.getLenght());
        updateStat = updateStat.replace("$tp", "" + uDTO.getType());
        updateStat = updateStat.replace("$tno", "" + uDTO.getTypeNo());
        updateStat = updateStat.replace("$currency", "" + uDTO.getCurrency());
        updateStat = updateStat.replace("$oldType", "" + oldType);
        updateStat = updateStat.replace("$otn", "" + oldTypeNo);
        updateStat = updateStat.replace("$line", "" + uDTO.getLine());
        updateStat = updateStat.replace("$id", "" + id);
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        ReplanishmentTempletesDetDTOInter uDTO = (ReplanishmentTempletesDetDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id and type = $type and type_no = $typNo";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        deleteStat = deleteStat.replace("$type", "" + uDTO.getType());
        deleteStat = deleteStat.replace("$typNo", "" + uDTO.getTypeNo());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object findAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<ReplanishmentTempletesDetDTOInter> uDTOL = new ArrayList<ReplanishmentTempletesDetDTOInter>();
        while (rs.next()) {
            ReplanishmentTempletesDetDTOInter uDTO = DTOFactory.createReplanishmentTempletesDetDTO();
            uDTO.setId(rs.getInt("id"));
           // uDTO.setCassitteId(rs.getInt("cassitte_id"));
            uDTO.setCurrency(rs.getInt("currency"));
            uDTO.setLenght(rs.getInt("lenght"));
            uDTO.setLine(rs.getInt("line"));
            uDTO.setPosition(rs.getInt("position"));
            uDTO.setStartingString(rs.getString("starting_string"));
            uDTO.setType(rs.getInt("type"));
            uDTO.setTypeNo(rs.getInt("type_no"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public Object findByMasterId(int id) throws Throwable {
        super.preSelect();

        String selectStat = "select * from $table where id = $id ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        ResultSet rs = executeQuery(selectStat);
        List<ReplanishmentTempletesDetDTOInter> uDTOL = new ArrayList<ReplanishmentTempletesDetDTOInter>();
        while (rs.next()) {
            ReplanishmentTempletesDetDTOInter uDTO = DTOFactory.createReplanishmentTempletesDetDTO();

           // uDTO.setCassitteId(rs.getInt("cassitte_id"));
            uDTO.setCurrency(rs.getInt("currency"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLenght(rs.getInt("length"));
            uDTO.setLine(rs.getInt("line"));
            uDTO.setPosition(rs.getInt("position"));
            uDTO.setStartingString(rs.getString("starting_string"));
            uDTO.setType(rs.getInt("type"));
            uDTO.setTypeNo(rs.getInt("type_no"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }
}
