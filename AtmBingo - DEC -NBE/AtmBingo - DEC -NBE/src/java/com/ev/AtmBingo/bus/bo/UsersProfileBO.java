/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.ProfileDAOInter;
import com.ev.AtmBingo.bus.dao.UserProfileDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ProfileDTOInter;
import com.ev.AtmBingo.bus.dto.UserProfileDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Administrator
 */
public class UsersProfileBO extends BaseBO implements UsersProfileBOInter ,Serializable{

    protected UsersProfileBO() {
        super();
    }

    public Object getProfiles(int rest) throws Throwable {
        ProfileDAOInter pDAO = DAOFactory.createProfileDAO(null);
        List<ProfileDTOInter> pDTOL = (List<ProfileDTOInter>) pDAO.findAll(rest);
        return pDTOL;
    }

    public Object getPickList(int profileId, int rest) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> hasProfile = (List<UsersDTOInter>) uDAO.findHasProfile(profileId, rest);
        List<UsersDTOInter> hasNotProfile = (List<UsersDTOInter>) uDAO.findHasNotProfile();

        DualListModel pickList = new DualListModel();
        List<String> source = new ArrayList<String>();
        List<String> target = new ArrayList<String>();

        for (UsersDTOInter u : hasProfile) {
            target.add(u.getLogonName());
        }
        for (UsersDTOInter u : hasNotProfile) {
            source.add(u.getLogonName());
        }

        pickList.setSource(source);
        pickList.setTarget(target);

        return pickList;
    }

    public Object editUserProfile(List<String> target, int profileId) throws Throwable {
        
        String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
        Boolean found = Boolean.FALSE;
        for (String input : target) {
            boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
            if (b == false) {
                found = Boolean.TRUE;
            }
            if (!"".equals(input)) {
                for (String validate1 : validate) {
                    if (input.toUpperCase().contains(validate1.toUpperCase())) {
                        found = Boolean.TRUE;
                    }
                }
            }
        }
        if (!found) {

            UserProfileDAOInter upDAO = DAOFactory.createUserProfileDAO(null);
            UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
            List<UserProfileDTOInter> upDTOL = new ArrayList<UserProfileDTOInter>();

            for (String s : target) {
                UsersDTOInter uDTO = (UsersDTOInter) uDAO.findByLogonName(s);
                UserProfileDTOInter upDTO = DTOFactory.createUserProfileDTO();
                upDTO.setProfileId(profileId);
                upDTO.setUserId(uDTO.getUserId());
                upDTOL.add(upDTO);
            }
            upDAO.deleteByProfileId(profileId);

            if (target.size() != 0) {
                for (UserProfileDTOInter up : upDTOL) {
                    upDAO.insert(up);
                }
            }

            return "Done";
        } else {
            return "Please Enter Valid Data";
        }
    }
}
