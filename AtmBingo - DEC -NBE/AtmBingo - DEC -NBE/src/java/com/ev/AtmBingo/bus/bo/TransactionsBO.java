
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import DBCONN.Session;
import com.ev.AtmBingo.base.bo.BaseBO;

import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.base.util.ReportingTool;
import com.ev.AtmBingo.bus.dao.AtmGroupDAOInter;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.ColumnDAOInter;
import com.ev.AtmBingo.bus.dao.CurrencyMasterDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.DisputeReportDAOInter;
import com.ev.AtmBingo.bus.dao.MatchedDataDAOInter;
import com.ev.AtmBingo.bus.dao.MatchingTypeDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionResponseCodeDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionStatusDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionTypeDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.MatchedDataDTOInter;
import com.ev.AtmBingo.bus.dto.MatchingTypeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.primefaces.model.DualListModel;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author Administrator
 */
public class TransactionsBO extends BaseBO implements TransactionsBOInter,Serializable {

    private String searchParameter;
    private String whereCluase;
    private BigDecimal totalAmount;

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getWhereCluase() {
        return whereCluase;
    }

    public void setWhereCluase(String whereCluase) {
        this.whereCluase = whereCluase;
    }

    public String getSearchParameter() {
        return searchParameter;
    }

    public void setSearchParameter(String searchParameter) {
        this.searchParameter = searchParameter;
    }

    protected TransactionsBO() {
        super();
    }

    public Object findByAtmbranch(UsersDTOInter loggedinUser) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>) amDAO.findByAtmbranch(loggedinUser.getUserId());
        return amDTOL;
    }

    public Object getAtmMachines(UsersDTOInter loggedinUser, Integer grpId) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>) amDAO.findByAtmGrp(grpId, loggedinUser.getUserId());
        return amDTOL;
    }

    public Object getTransactionResponseCode() throws Throwable {
        TransactionResponseCodeDAOInter trcDAO = DAOFactory.createTransactionResponseCodeDAO(null);
        List<TransactionResponseCodeDTOInter> trcDTOL = (List<TransactionResponseCodeDTOInter>) trcDAO.findAll();
        return trcDTOL;
    }

    public Object getTransactionType() throws Throwable {
        TransactionTypeDAOInter ttDAO = DAOFactory.createTransactionTypeDAO(null);
        List<TransactionTypeDTOInter> ttDTOL = (List<TransactionTypeDTOInter>) ttDAO.findAll();
        return ttDTOL;
    }

    public Object getTransactionStatus() throws Throwable {
        TransactionStatusDAOInter tsDAO = DAOFactory.createTransactionStatusDAO(null);
        List<TransactionStatusDTOInter> tsDTOL = (List<TransactionStatusDTOInter>) tsDAO.findAll();
        return tsDTOL;
    }

    public Object getCurrencyMaster() throws Throwable {
        CurrencyMasterDAOInter cmDAO = DAOFactory.createCurrencyMasterDAO(null);
        List<CurrencyMasterDTOInter> cmDTOL = (List<CurrencyMasterDTOInter>) cmDAO.findAll();
        return cmDTOL;
    }

    public Object getFileColumnDefinition() throws Throwable {
        ColumnDAOInter fcdDAO = DAOFactory.createColumnDAO(null);
        List<ColumnDTOInter> fcdDTOL = (List<ColumnDTOInter>) fcdDAO.findAll();
        return fcdDTOL;
    }

    public List<ColumnDTOInter> getColumns(List columns) throws Throwable {
        ColumnDAOInter fcdDAO = DAOFactory.createColumnDAO(null);
        List<ColumnDTOInter> fcdDTOL = new ArrayList<ColumnDTOInter>();
        fcdDTOL = (List<ColumnDTOInter>) fcdDAO.searchByName(columns);
        return fcdDTOL;
    }

    public Integer PrintLines(String mdDTO) throws Exception {
        DisputeReportDAOInter drDAO = DAOFactory.createDisputeReportDAO(null);
        return drDAO.PrintLines(mdDTO);

    }

    public String PrintLinesReport(String Seq) throws Exception {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("sequence", Integer.valueOf(Seq));

        Connection conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
        try {
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/Trans_Data_Report.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri = "TransactionDataReport" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {

                String x = "../PDF/" + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;

            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();

            return "fail";
        }

    }

    public Object getMatchingType() throws Throwable {
        MatchingTypeDAOInter mtDAO = DAOFactory.createMatchingTypeDAO(null);
        List<MatchingTypeDTOInter> mtDTOL = (List<MatchingTypeDTOInter>) mtDAO.findComboBox();
        return mtDTOL;
    }

    public Object getSettledUsers() throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> uDTOL = (List<UsersDTOInter>) uDAO.findComboBox();
        return uDTOL;
    }

    public Object getAtmGroup() throws Throwable {
        AtmGroupDAOInter agDAo = DAOFactory.createAtmGroupDAO(null);
        List<AtmGroupDTOInter> agDTOL = (List<AtmGroupDTOInter>) agDAo.findTree();
        return agDTOL;
    }

    public Object findReport(String fields, String whereCluase, UsersDTOInter logedUser, List<ColumnDTOInter> c, MatchedDataDTOInter[] mdDTOArr, String cust) throws Throwable {
        MatchedDataDAOInter mdDAO = DAOFactory.createMatchedDataDAO(null);
        ResultSet rs = (ResultSet) mdDAO.findReport(fields, whereCluase, mdDTOArr, logedUser.getUserId());
        ReportingTool rt = new ReportingTool(rs, (ArrayList) c, false);
        Integer size = mdDTOArr.length;
        String path = rt.generate("Matched Transactions", logedUser.getUserName(), searchParameter, false, cust, totalAmount, size.toString());
        mdDAO.deleteFromMatchedReport(logedUser.getUserId());
        CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());

        return path;
    }

    public Object gettexttransaction(MatchedDataDTOInter mdDTO) throws Throwable {
        MatchedDataDAOInter mdDAO = DAOFactory.createMatchedDataDAO(null);
        String mddtoi = mdDAO.Findtexttransaction(mdDTO).toString();
        return mddtoi;
    }

    public Object gettexttransaction_other(MatchedDataDTOInter mdDTO) throws Throwable {
        MatchedDataDAOInter mdDAO = DAOFactory.createMatchedDataDAO(null);
        String mddtoi = mdDAO.Findtexttransaction_other(mdDTO).toString();
        return mddtoi;
    }

    public Object FindFiletransaction(MatchedDataDTOInter mdDTO) throws Throwable {
        MatchedDataDAOInter mdDAO = DAOFactory.createMatchedDataDAO(null);
        Clob mddtoi = (Clob) mdDAO.FindFiletransaction(mdDTO);
        return mddtoi;
    }

    public Object searchMatchedData(Date transDateF, String opt1, Date transDateT, Date settleDateF, String opt2, Date settleDateT,
            Date loadDateF, String opt3, Date loadDateT, Integer amountF, String opt4, Integer amountT, Integer transSeqOrderByF,
            String opt5, Integer transSeqOrderByT, String cardNo, String opt6, String accountNo, String opt7, String notesPre,
            Integer transStatusCode, Integer transTypeCode, Integer responseCode, Integer currencyCode, Integer atmId,
            String atmCode, Integer atmGroup, boolean blackList, Integer settled, Integer matchingTypeList, Integer masterRecord,
            Integer settledBy, Integer matchedBy, String sortBy, UsersDTOInter logedUser, String field, String col1, String col2, String col3, String col4, String col5, String colop1, String colop2, String colop3, String colop4, String colop5) throws Throwable {
String index = "";
        String whereClaus = "";
        searchParameter = "";
        int CriteriaMatchingType = 0;
        int CriteriaRecordType = 0;
        int CriteriaTransactionDateBetween = 0;
        int CriteriaTransactionDateNotEqual = 0;
        int CriteriaTransactionDateSmallerThan = 0;
        int CriteriaTransactionDateSmallerThanOrEqual = 0;
        int CriteriaTransactionDateGreateThan = 0;
        int CriteriaTransactionDateGreateThanOrEqual = 0;
        int CriteriaTransactionDateEqual = 0;
        int CriteriaSettlmentDateBetween = 0;
        int CriteriaSettlmentDateGreaterThan = 0;
        int CriteriaSettlmentDateGreaterThanOrEqual = 0;
        int CriteriaSettlmentDateSmallerThan = 0;
        int CriteriaSettlmentDateSmallerThanOrEqual = 0;
        int CriteriaSettlmentDateNotEqual = 0;
        int CriteriaSettlmentDateEqual = 0;
        int CriteriaAmountBetween = 0;
        int CriteriaAmountGreaterThan = 0;
        int CriteriaAmountGreaterThanOrEqual = 0;
        int CriteriaAmountSmallerThan = 0;
        int CriteriaAmountSmallerThanOrEqual = 0;
        int CriteriaAmountNotEqual = 0;
        int CriteriaAmountEqual = 0;
        int CriteriaTransSeqBetween = 0;
        int CriteriaTransSeqGreater = 0;
        int CriteriaTransSeqGreaterOrEqual = 0;
        int CriteriaTransSeqSmaller = 0;
        int CriteriaTransSeqSmallerOrEqual = 0;
        int CriteriaTransSeqNotEqual = 0;
        int CriteriaTransSeqEqual = 0;
        int CriteriaCardNumberEqual = 0;
        int CriteriaCardNumberNotEqual = 0;
        int CriteriaCardNumberLike = 0;
        int CriteriaCardNumberNotLike = 0;
        int CriteriaCardNumberIsNull = 0;
        int CriteriaCardNumberIsNotNull = 0;
        int CriteriaAccontNumberEqual = 0;
        int CriteriaAccontNumberNotEqual = 0;
        int CriteriaAccontNumberIsNull = 0;
        int CriteriaAccontNumberIsNotNull = 0;
        int CriteriaAccontNumberLike = 0;
        int CriteriaAccontNumberNotLike = 0;
        int CriteriaColumnOneLike = 0;
        int CriteriaColumnOneNotLike = 0;
        int CriteriaColumnOneIsNull = 0;
        int CriteriaColumnOneIsNotNull = 0;
        int CriteriaColumnOneNotEqual = 0;
        int CriteriaColumnOneEqual = 0;
        int CriteriaColumnTwoLike = 0;
        int CriteriaColumnTwoNotLike = 0;
        int CriteriaColumnTwoIsNull = 0;
        int CriteriaColumnTwoIsNotNull = 0;
        int CriteriaColumnTwoNotEqual = 0;
        int CriteriaColumnTwoEqual = 0;
        int CriteriaColumnThreeLike = 0;
        int CriteriaColumnThreeNotLike = 0;
        int CriteriaColumnThreeIsNull = 0;
        int CriteriaColumnThreeIsNotNull = 0;
        int CriteriaColumnThreeNotEqual = 0;
        int CriteriaColumnThreeEqual = 0;
        int CriteriaColumnFourLike = 0;
        int CriteriaColumnFourNotLike = 0;
        int CriteriaColumnFourIsNull = 0;
        int CriteriaColumnFourIsNotNull = 0;
        int CriteriaColumnFourNotEqual = 0;
        int CriteriaColumnFourEqual = 0;
        int CriteriaColumnFiveLike = 0;
        int CriteriaColumnFiveNotLike = 0;
        int CriteriaColumnFiveIsNull = 0;
        int CriteriaColumnFiveIsNotNull = 0;
        int CriteriaColumnFiveEqual = 0;
        int CriteriaColumnFiveNotEqual = 0;
        int CriteriaNotesPresLike = 0;
        int CriteriaTranStatusEqual = 0;
        int CriteriaTranStatusIn = 0;
        int CriteriaTranTypeIDEqual = 0;
        int CriteriaResponceCodeIDEqual = 0;
        int CriteriaCurrencyIDEqual = 0;
        int CriteriaATMIDEqual = 0;
        int CriteriaATMApplicationIDLikeLower = 0;
        int CriteriaATMIDIn = 0;
        int CriteriaCardNumInBlackListed = 0;
        int CriteriaSettledFlagEqual = 0;
        int CriteriaSettledUserEqual = 0;
        int CriteriaMatchByEqual = 0;
        int CriteriaAtmValidEqual = 0;
        int CriteriaExistUserAtm = 0;
        if (matchingTypeList == 1) {
            searchParameter += "Matching Type JR-SW -";
            CriteriaMatchingType = 1;
        } else if (matchingTypeList == 2) {
            searchParameter += "Matching Type SW-GL -";
            CriteriaMatchingType = 1;
        }

        if (masterRecord == 1) {
            searchParameter += "Master Record JR -";
            CriteriaRecordType = 1;
        } else if (masterRecord == 2) {
            CriteriaRecordType = 1;
            searchParameter += "Master Record SW -";
        } else if (masterRecord == 3) {
            CriteriaRecordType = 1;
            searchParameter += "Master Record GL -";
        }

        String WhereColoumnArray = "";

        // <editor-fold defaultstate="collapsed" desc="Transaction_date where clause">
        if (opt1.equals("Between")) {
            if (transDateF != null && transDateT != null) {
                whereClaus += " and transaction_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss') "
                        + " and transaction_date <= to_date('$col2','dd.mm.yyyy hh24:mi:ss') ";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                whereClaus = whereClaus.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(transDateT));
                searchParameter += "Transaction Date between $F and $T -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter = searchParameter.replace("$T", DateFormatter.changeDateAndTimeFormat(transDateT));
                CriteriaTransactionDateBetween = 1;
                WhereColoumnArray = WhereColoumnArray + ",transaction_date";
            }
        } else if (opt1.equals("<>")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date <> to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date <> $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
                CriteriaTransactionDateNotEqual = 1;
                WhereColoumnArray = WhereColoumnArray + ",transaction_date";
            }
        } else if (opt1.equals("<")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date < to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date < $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
                WhereColoumnArray = WhereColoumnArray + ",transaction_date";
                CriteriaTransactionDateSmallerThan = 1;
            }
        } else if (opt1.equals("<=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date <= to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date <= $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
                WhereColoumnArray = WhereColoumnArray + ",transaction_date";
                CriteriaTransactionDateSmallerThanOrEqual = 1;
            }
        } else if (opt1.equals(">")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date > to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date > $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
                WhereColoumnArray = WhereColoumnArray + ",transaction_date";
                CriteriaTransactionDateGreateThan = 1;
            }
        } else if (opt1.equals(">=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date >= to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date >= $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
                WhereColoumnArray = WhereColoumnArray + ",transaction_date";
                CriteriaTransactionDateGreateThanOrEqual = 1;
            }
        } else if (opt1.equals("=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date = to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date = $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
                WhereColoumnArray = WhereColoumnArray + ",transaction_date";
                CriteriaTransactionDateEqual = 1;
            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Settlement_date where clause">
        if (opt2.equals("Between")) {
            if (settleDateF != null && settleDateT != null) {
                whereClaus += " and settlement_date between to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and "
                        + "to_date(to_char(nvl(to_date('$col2','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                whereClaus = whereClaus.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(settleDateT));
                searchParameter += "Settlement Date Between $F and $T -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter = searchParameter.replace("$T", DateFormatter.changeDateAndTimeFormat(settleDateT));
                WhereColoumnArray = WhereColoumnArray + ",settlement_date";
                CriteriaSettlmentDateBetween = 1;
            }
        } else if (opt2.equals(">")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date > to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date > $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                WhereColoumnArray = WhereColoumnArray + ",settlement_date";
                CriteriaSettlmentDateGreaterThan = 1;
            }
        } else if (opt2.equals(">=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date >= to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date >= $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                WhereColoumnArray = WhereColoumnArray + ",settlement_date";
                CriteriaSettlmentDateGreaterThanOrEqual = 1;
            }
        } else if (opt2.equals("<")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date < to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date < $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                WhereColoumnArray = WhereColoumnArray + ",settlement_date";
                CriteriaSettlmentDateSmallerThan = 1;
            }
        } else if (opt2.equals("<=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date <= to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date <= $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                WhereColoumnArray = WhereColoumnArray + ",settlement_date";
                CriteriaSettlmentDateSmallerThanOrEqual = 1;
            }
        } else if (opt2.equals("<>")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date <> to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date <> $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                WhereColoumnArray = WhereColoumnArray + ",settlement_date";
                CriteriaSettlmentDateNotEqual = 1;
            }
        } else if (opt2.equals("=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date = to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date = $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                WhereColoumnArray = WhereColoumnArray + ",settlement_date";
                CriteriaSettlmentDateEqual = 1;
            }
        }// </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Amount where clause">
        if (opt4.equals("Between")) {
            if (amountF != 0 && amountT != 0 && amountF != null && amountT != null) {
                whereClaus += " and amount between nvl($col1,0) and nvl($col2,9999999999999999)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                whereClaus = whereClaus.replace("$col2", "" + amountT);
                searchParameter += "Amount Between $F and $T -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                searchParameter = searchParameter.replace("$T", "" + amountT);
                WhereColoumnArray = WhereColoumnArray + ",amount";
                CriteriaAmountBetween = 1;
            }
        } else if (opt4.equals(">")) {
            if (amountF != null && amountF != 0) {
                whereClaus += " and amount > nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount > $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                WhereColoumnArray = WhereColoumnArray + ",amount";
                CriteriaAmountGreaterThan = 1;
            }
        } else if (opt4.equals(">=")) {
            if (amountF != null && amountF != 0) {
                whereClaus += " and amount >= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount >= $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                WhereColoumnArray = WhereColoumnArray + ",amount";
                CriteriaAmountGreaterThanOrEqual = 1;
            }
        } else if (opt4.equals("<")) {
            if (amountF != null && amountF != 0) {
                whereClaus += " and amount < nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount < $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                WhereColoumnArray = WhereColoumnArray + ",amount";
                CriteriaAmountSmallerThan = 1;
            }
        } else if (opt4.equals("<=")) {
            if (amountF != null && amountF != 0) {
                whereClaus += " and amount <= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount <= $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                WhereColoumnArray = WhereColoumnArray + ",amount";
                CriteriaAmountSmallerThanOrEqual = 1;
            }
        } else if (opt4.equals("<>")) {
            if (amountF != null && amountF != 0) {
                whereClaus += " and amount <> nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount <> $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                WhereColoumnArray = WhereColoumnArray + ",amount";
                CriteriaAmountNotEqual = 1;
            }
        } else if (opt4.equals("=")) {
            if (amountF != null && amountF != 0) {
                whereClaus += " and amount = nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount = $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                WhereColoumnArray = WhereColoumnArray + ",amount";
                CriteriaAmountEqual = 1;
            }
        }// </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Transaction_sequence where clause">
        if (opt5.equals("Between")) {
            if (transSeqOrderByF != null && transSeqOrderByT != null && transSeqOrderByF != 0 && transSeqOrderByT != 0) {
                whereClaus += " and transaction_sequence_order_by between nvl($col1,0) and nvl($col2,9999999999999999)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                whereClaus = whereClaus.replace("$col2", "" + transSeqOrderByT);
                searchParameter += "Sequence Between $F and $T -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                searchParameter = searchParameter.replace("$T", "" + transSeqOrderByT);
                WhereColoumnArray = WhereColoumnArray + ",transaction_sequence_order_by";
                CriteriaTransSeqBetween = 1;
            }
        } else if (opt5.equals(">")) {
            if (transSeqOrderByF != null && transSeqOrderByF != 0) {
                whereClaus += " and transaction_sequence_order_by > nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence > $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                WhereColoumnArray = WhereColoumnArray + ",transaction_sequence_order_by";
                CriteriaTransSeqGreater = 1;
            }
        } else if (opt5.equals(">=")) {
            if (transSeqOrderByF != null && transSeqOrderByF != 0) {
                whereClaus += " and transaction_sequence_order_by >= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence >= $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                WhereColoumnArray = WhereColoumnArray + ",transaction_sequence_order_by";
                CriteriaTransSeqGreaterOrEqual = 1;
            }
        } else if (opt5.equals("<")) {
            if (transSeqOrderByF != null && transSeqOrderByF != 0) {
                whereClaus += " and transaction_sequence_order_by < nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence < $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                WhereColoumnArray = WhereColoumnArray + ",transaction_sequence_order_by";
                CriteriaTransSeqSmaller = 1;
            }
        } else if (opt5.equals("<=")) {
            if (transSeqOrderByF != null && transSeqOrderByF != 0) {
                whereClaus += " and transaction_sequence_order_by <= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence <= $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                WhereColoumnArray = WhereColoumnArray + ",transaction_sequence_order_by";
                CriteriaTransSeqSmallerOrEqual = 1;
            }
        } else if (opt5.equals("<>")) {
            if (transSeqOrderByF != null && transSeqOrderByF != 0) {
                whereClaus += " and transaction_sequence_order_by <> nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence <> $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                WhereColoumnArray = WhereColoumnArray + ",transaction_sequence_order_by";
                CriteriaTransSeqNotEqual = 1;
            }
        } else if (opt5.equals("=")) {
            if (transSeqOrderByF != null && transSeqOrderByF != 0) {
                whereClaus += " and transaction_sequence_order_by = nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence = $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                WhereColoumnArray = WhereColoumnArray + ",transaction_sequence_order_by";
                CriteriaTransSeqEqual = 1;
            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Card_no where clause">
        if (opt6.equals("=")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and card_no like '%$col1%'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo.trim());
                searchParameter += "Card No. like $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo.trim());
                WhereColoumnArray = WhereColoumnArray + ",card_no";
                CriteriaCardNumberEqual = 1;
            }
        } else if (opt6.equals("<>")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and card_no <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo.trim());
                searchParameter += "Card No. <> $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo.trim());
                WhereColoumnArray = WhereColoumnArray + ",card_no";
                CriteriaCardNumberNotEqual = 1;
            }
        } else if (opt6.equals("Like")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and card_no like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo.trim());
                searchParameter += "Card No. like $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo.trim());
                CriteriaCardNumberLike = 1;
            }
        } else if (opt6.equals("Not Like")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and card_no not like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo.trim());
                searchParameter += "Card No. not like $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo.trim());
                CriteriaCardNumberNotLike = 1;
            }
        } else if (opt6.equals("is null")) {
            whereClaus += " and card_no is null ";
            whereClaus = whereClaus.replace("$col1", "" + cardNo);
            searchParameter += "Card No.  is null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo.trim());
            CriteriaCardNumberIsNull = 1;
        } else if (opt6.equals("is not null")) {
            whereClaus += " and card_no is not null ";
            whereClaus = whereClaus.replace("$col1", "" + cardNo.trim());
            searchParameter += "Card No. not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo.trim());
            CriteriaCardNumberIsNotNull = 1;
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Account_No where clause">
        if (opt7.equals("=")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER like '%$col1%'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo.trim());
                searchParameter += "Account No. like $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo.trim());
                WhereColoumnArray = WhereColoumnArray + ",CUSTOMER_ACCOUNT_NUMBER";
                CriteriaAccontNumberEqual = 1;
            }
        } else if (opt7.equals("<>")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo.trim());
                searchParameter += "Account No. <> $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo.trim());
                WhereColoumnArray = WhereColoumnArray + ",CUSTOMER_ACCOUNT_NUMBER";
                CriteriaAccontNumberNotEqual = 1;
            }
        } else if (opt7.equals("is null")) {
            whereClaus += " and CUSTOMER_ACCOUNT_NUMBER is null";
            whereClaus = whereClaus.replace("$col1", "" + accountNo.trim());
            searchParameter += "Account No. is null";
            searchParameter = searchParameter.replace("$F", "" + accountNo.trim());
            CriteriaAccontNumberIsNull = 1;

        } else if (opt7.equals("is not null")) {
            whereClaus += " and CUSTOMER_ACCOUNT_NUMBER is not null";
            whereClaus = whereClaus.replace("$col1", "" + accountNo.trim());
            searchParameter += "Account No. is not null -";
            searchParameter = searchParameter.replace("$F", "" + accountNo.trim());
            CriteriaAccontNumberIsNotNull = 1;
        } else if (opt7.equals("Like")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER Like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo.trim());
                searchParameter += "Account No. Like $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo.trim());
                CriteriaAccontNumberLike = 1;
            }

        } else if (opt7.equals("Not Like")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER not Like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo.trim());
                searchParameter += "Account No. not Like $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo.trim());
                CriteriaAccontNumberNotLike = 1;
            }

        }
        // </editor-fold>
        if (colop1.equals("Like")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                searchParameter += "Column 1 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
                CriteriaColumnOneLike = 1;
            }
        } else if (colop1.equals("Not Like")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 not like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                searchParameter += "Column 1 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
                CriteriaColumnOneNotLike = 1;
            }
        } else if (colop1.equals("is null")) {
            whereClaus += " and COLUMN1 is null ";
            whereClaus = whereClaus.replace("$col1", "" + col1);
            searchParameter += "Column 1 is null -";
            searchParameter = searchParameter.replace("$F", "" + col1);
            CriteriaColumnOneIsNull = 1;
        } else if (colop1.equals("is not null")) {
            whereClaus += " and COLUMN1 is not null ";
            whereClaus = whereClaus.replace("$col1", "" + col1);
            searchParameter += "Column 1 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
            CriteriaColumnOneIsNotNull = 1;
        } else if (colop1.equals("<>")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                searchParameter += "Column 1 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
                CriteriaColumnOneNotEqual = 1;
            }
        } else if (colop1.equals("=")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 = '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                searchParameter += "Column 1 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
                CriteriaColumnOneEqual = 1;
            }
        }
        if (colop2.equals("Like")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 like '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col2);
                CriteriaColumnTwoLike = 1;
            }
        } else if (colop2.equals("Not Like")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 not like '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col2);
                CriteriaColumnTwoNotLike = 1;
            }
        } else if (colop2.equals("is null")) {
            whereClaus += " and COLUMN2 is null ";
            whereClaus = whereClaus.replace("$col2", "" + col2);
            searchParameter += "Column 2 is null -";
            searchParameter = searchParameter.replace("$F", "" + col2);
            CriteriaColumnTwoIsNull = 1;
        } else if (colop2.equals("is not null")) {
            whereClaus += " and COLUMN2 is not null ";
            whereClaus = whereClaus.replace("$col2", "" + col2);
            searchParameter += "Column 2 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
            CriteriaColumnTwoIsNotNull = 1;
        } else if (colop2.equals("<>")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 <> '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col2);
                CriteriaColumnTwoNotEqual = 1;
            }
        } else if (colop2.equals("=")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 = '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col2);
                CriteriaColumnTwoEqual = 1;
            }
        }
        if (colop3.equals("Like")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 like '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
                CriteriaColumnThreeLike = 1;
            }
        } else if (colop3.equals("Not Like")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 not like '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
                CriteriaColumnThreeNotLike = 1;
            }
        } else if (colop3.equals("is null")) {
            whereClaus += " and COLUMN3 is null ";
            whereClaus = whereClaus.replace("$col3", "" + col3);
            searchParameter += "Column 3 is null -";
            searchParameter = searchParameter.replace("$F", "" + col3);
            CriteriaColumnThreeIsNull = 1;
        } else if (colop3.equals("is not null")) {
            whereClaus += " and COLUMN3 is not null ";
            whereClaus = whereClaus.replace("$col3", "" + col3);
            searchParameter += "Column 3 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
            CriteriaColumnThreeIsNotNull = 1;
        } else if (colop3.equals("<>")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 <> '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
                CriteriaColumnThreeNotEqual = 1;
            }
        } else if (colop3.equals("=")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 = '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
                CriteriaColumnThreeEqual = 1;
            }
        }
        if (colop4.equals("Like")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 like '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
                CriteriaColumnFourLike = 1;
            }
        } else if (colop4.equals("Not Like")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 not like '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
                CriteriaColumnFourNotLike = 1;
            }
        } else if (colop4.equals("is null")) {
            whereClaus += " and COLUMN4 is null ";
            whereClaus = whereClaus.replace("$col4", "" + col4);
            searchParameter += "Column 4 is null -";
            searchParameter = searchParameter.replace("$F", "" + col4);
            CriteriaColumnFourIsNull = 1;
        } else if (colop4.equals("is not null")) {
            whereClaus += " and COLUMN4 is not null ";
            whereClaus = whereClaus.replace("$col4", "" + col4);
            searchParameter += "Column 4 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
            CriteriaColumnFourIsNotNull = 1;
        } else if (colop4.equals("<>")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 <> '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
                CriteriaColumnFourNotEqual = 1;
            }
        } else if (colop4.equals("=")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 = '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
                CriteriaColumnFourEqual = 1;
            }
        }
        if (colop5.equals("Like")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 like '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col5);
                CriteriaColumnFiveLike = 1;
            }
        } else if (colop5.equals("Not Like")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 not like '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col5);
                CriteriaColumnFiveNotLike = 1;
            }
        } else if (colop5.equals("is null")) {
            whereClaus += " and COLUMN5 is null ";
            whereClaus = whereClaus.replace("$col5", "" + col5);
            searchParameter += "Column 5 is null -";
            searchParameter = searchParameter.replace("$F", "" + col5);
            CriteriaColumnFiveIsNull = 1;
        } else if (colop5.equals("is not null")) {
            whereClaus += " and COLUMN5 is not null ";
            whereClaus = whereClaus.replace("$col5", "" + col5);
            searchParameter += "Column 5 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
            CriteriaColumnFiveIsNotNull = 1;
        } else if (colop5.equals("<>")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 <> '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col5);
                CriteriaColumnFiveNotEqual = 1;
            }
        } else if (colop5.equals("=")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 = '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col5);
                CriteriaColumnFiveEqual = 1;
            }
        }
        if (notesPre != null && !notesPre.equals("")) {
            whereClaus += " and lower(notes_presented) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + notesPre);
            searchParameter += "Notes Presnted like $F -";
            searchParameter = searchParameter.replace("$F", "" + notesPre);
            WhereColoumnArray = WhereColoumnArray + ",notes_presented";
            CriteriaNotesPresLike = 1;
        }
        if (transStatusCode != 0) {
            if (transStatusCode != -1) {
                whereClaus += " and transaction_status_id = $col1";
                whereClaus = whereClaus.replace("$col1", "" + transStatusCode);
                searchParameter += "Status: $F -";
                TransactionStatusDAOInter tsDAO = DAOFactory.createTransactionStatusDAO(null);
                TransactionStatusDTOInter tsDTO = (TransactionStatusDTOInter) tsDAO.find(transStatusCode);
                searchParameter = searchParameter.replace("$F", "" + tsDTO.getName());
                WhereColoumnArray = WhereColoumnArray + ",transaction_status_id";
                CriteriaTranStatusEqual = 1;
            } else {
                whereClaus += " and transaction_status_id in (select id from transaction_status) ";
                whereClaus = whereClaus.replace("$col1", "" + transStatusCode);
                searchParameter += "Status: $F -";
                searchParameter = searchParameter.replace("$F", " Has Error");
                WhereColoumnArray = WhereColoumnArray + ",transaction_status_id";
                CriteriaTranStatusIn = 1;
            }
        }
        if (transTypeCode != 0) {
            whereClaus += " and transaction_type_id = $col1";
            whereClaus = whereClaus.replace("$col1", "" + transTypeCode);
            searchParameter += "Trans. Type: $F -";
            TransactionTypeDAOInter tsDAO = DAOFactory.createTransactionTypeDAO(null);
            TransactionTypeDTOInter tsDTO = (TransactionTypeDTOInter) tsDAO.find(transTypeCode);
            searchParameter = searchParameter.replace("$F", "" + tsDTO.getName());
            WhereColoumnArray = WhereColoumnArray + ",transaction_type_id";
            CriteriaTranTypeIDEqual = 1;
        }
        if (responseCode != 0) {
            whereClaus += " and response_code_id = $col1";
            whereClaus = whereClaus.replace("$col1", "" + responseCode);
            searchParameter += "Response: $F -";
            TransactionResponseCodeDAOInter tsDAO = DAOFactory.createTransactionResponseCodeDAO(null);
            TransactionResponseCodeDTOInter tsDTO = (TransactionResponseCodeDTOInter) tsDAO.find(responseCode);
            searchParameter = searchParameter.replace("$F", "" + tsDTO.getName());
            WhereColoumnArray = WhereColoumnArray + ",response_code_id";
            CriteriaResponceCodeIDEqual = 1;
        }
        if (currencyCode != 0) {
            whereClaus += " and currency_id = $col1";
            whereClaus = whereClaus.replace("$col1", "" + currencyCode);
            searchParameter += "Currency: $F -";
            CurrencyMasterDAOInter tsDAO = DAOFactory.createCurrencyMasterDAO(null);
            CurrencyMasterDTOInter tsDTO = (CurrencyMasterDTOInter) tsDAO.find(currencyCode);
            searchParameter = searchParameter.replace("$F", "" + tsDTO.getSymbol());
            WhereColoumnArray = WhereColoumnArray + ",currency_id";
            CriteriaCurrencyIDEqual = 1;
        }
        if (atmId != 0) {
            whereClaus += " and ATM_ID = $col1";
            whereClaus = whereClaus.replace("$col1", "" + atmId);
            searchParameter += "ATM ID: $F -";
            AtmMachineDAOInter x = DAOFactory.createAtmMachineDAO(null);
            AtmMachineDTOInter x1 = (AtmMachineDTOInter) x.find(atmId);
            searchParameter = searchParameter.replace("$F", "" + x1.getApplicationId());
            WhereColoumnArray = WhereColoumnArray + ",ATM_ID";
            CriteriaATMIDEqual = 1;
        }
        if (atmCode != null && !atmCode.equals("")) {
            whereClaus += " and lower(ATM_APPLICATION_ID) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + atmCode);
            searchParameter += "ATM name: $F -";
            searchParameter = searchParameter.replace("$F", "" + atmCode);
            WhereColoumnArray = WhereColoumnArray + ",ATM_APPLICATION_ID";
            CriteriaATMApplicationIDLikeLower = 1;
        }
        if (atmGroup != 0) {
            whereClaus += " and ATM_ID IN (select id from atm_machine where atm_group in (select id from atm_group where parent_id in (select id from atm_group where (parent_id = $col1 or id = $col1)) or id = $col1))";
            whereClaus = whereClaus.replace("$col1", "" + atmGroup);
            AtmGroupDAOInter agDAO = DAOFactory.createAtmGroupDAO(null);
            AtmGroupDTOInter agDTO = (AtmGroupDTOInter) agDAO.find(atmGroup);
            searchParameter += "ATM Group: $F -";
            searchParameter = searchParameter.replace("$F", "" + agDTO.getName());
            WhereColoumnArray = WhereColoumnArray + ",ATM_ID";
            CriteriaATMIDIn = 1;
        }
        if (blackList) {
            whereClaus += " and card_no in (select card_no from black_listed_card)";
            searchParameter += "Black Listed Card -";
            WhereColoumnArray = WhereColoumnArray + ",card_no";
            CriteriaCardNumInBlackListed = 1;
        }
        if (settled != 3) {
            whereClaus += " and settled_flag = $col1";
            whereClaus = whereClaus.replace("$col1", "" + settled);
            WhereColoumnArray = WhereColoumnArray + ",settled_flag";
            CriteriaSettledFlagEqual = 1;
        }
        String temp = "";
        if (matchingTypeList != 0) {
            temp += "  matching_type = $col1 and record_type = $col2";
            temp = temp.replace("$col1", "" + matchingTypeList);
            temp = temp.replace("$col2", "" + masterRecord);
            WhereColoumnArray = WhereColoumnArray + ",matching_type";
            WhereColoumnArray = WhereColoumnArray + ",record_type";

        }

        if (settledBy != 0) {
            whereClaus += " and settled_user = $col1";
            whereClaus = whereClaus.replace("$col1", "" + settledBy);
            UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
            UsersDTOInter uDTO = (UsersDTOInter) uDAO.find(settledBy);
            searchParameter += "Settled By: $F -";
            searchParameter = searchParameter.replace("$F", uDTO.getUserName());
            WhereColoumnArray = WhereColoumnArray + ",settled_user";
            CriteriaSettledUserEqual = 1;
        }
        if (matchedBy != 0) {
            whereClaus += " and match_by = $col1";
            whereClaus = whereClaus.replace("$col1", "" + matchedBy);
            UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
            UsersDTOInter uDTO = (UsersDTOInter) uDAO.find(matchedBy);
            searchParameter += "Matched By: $F -";
            searchParameter = searchParameter.replace("$F", uDTO.getUserName());
            WhereColoumnArray = WhereColoumnArray + ",match_by";
            CriteriaMatchByEqual = 1;
        }

        whereClaus += " and atm_valid(atm_id) = 1 ";
        CriteriaAtmValidEqual = 1;
        whereClaus += " and Exists (select 1 from user_atm s where s.user_id = $user and s.atm_id = matched_data.ATM_ID)";
        whereClaus = whereClaus.replace("$user", "" + logedUser.getUserId());
        WhereColoumnArray = WhereColoumnArray + ",atm_id";
        CriteriaExistUserAtm = 1;

        if (sortBy != null && !sortBy.equals("")) {
            whereClaus += " order by ATM_APPLICATION_ID,transaction_date, transaction_sequence_order_by";
            whereClaus = whereClaus.replace("$col1", "" + sortBy);
            WhereColoumnArray = WhereColoumnArray + ",transaction_date,ATM_APPLICATION_ID, transaction_sequence_order_by";
        } else {
            whereClaus += " order by ATM_APPLICATION_ID,transaction_date, transaction_sequence_order_by";
            WhereColoumnArray = WhereColoumnArray + " ,transaction_date,ATM_APPLICATION_ID, card_no";
        }
        if (CriteriaMatchingType == 1
                && CriteriaRecordType == 1
                && CriteriaTransactionDateBetween == 1
                && CriteriaTransactionDateNotEqual == 0
                && CriteriaTransactionDateSmallerThan == 0
                && CriteriaTransactionDateSmallerThanOrEqual == 0
                && CriteriaTransactionDateGreateThan == 0
                && CriteriaTransactionDateGreateThanOrEqual == 0
                && CriteriaTransactionDateEqual == 0
                && CriteriaSettlmentDateBetween == 0
                && CriteriaSettlmentDateGreaterThan == 0
                && CriteriaSettlmentDateGreaterThanOrEqual == 0
                && CriteriaSettlmentDateSmallerThan == 0
                && CriteriaSettlmentDateSmallerThanOrEqual == 0
                && CriteriaSettlmentDateNotEqual == 0
                && CriteriaSettlmentDateEqual == 0
                && CriteriaAmountBetween == 0
                && CriteriaAmountGreaterThan == 0
                && CriteriaAmountGreaterThanOrEqual == 0
                && CriteriaAmountSmallerThan == 0
                && CriteriaAmountSmallerThanOrEqual == 0
                && CriteriaAmountNotEqual == 0
                && CriteriaAmountEqual == 0
                && CriteriaTransSeqBetween == 0
                && CriteriaTransSeqGreater == 0
                && CriteriaTransSeqGreaterOrEqual == 0
                && CriteriaTransSeqSmaller == 0
                && CriteriaTransSeqSmallerOrEqual == 0
                && CriteriaTransSeqNotEqual == 0
                && CriteriaTransSeqEqual == 0
                && CriteriaCardNumberEqual == 0
                && CriteriaCardNumberNotEqual == 0
                && CriteriaCardNumberLike == 0
                && CriteriaCardNumberNotLike == 0
                && CriteriaCardNumberIsNull == 0
                && CriteriaCardNumberIsNotNull == 0
                && CriteriaAccontNumberEqual == 0
                && CriteriaAccontNumberNotEqual == 0
                && CriteriaAccontNumberIsNull == 0
                && CriteriaAccontNumberIsNotNull == 0
                && CriteriaAccontNumberLike == 0
                && CriteriaAccontNumberNotLike == 0
                && CriteriaColumnOneLike == 0
                && CriteriaColumnOneNotLike == 0
                && CriteriaColumnOneIsNull == 0
                && CriteriaColumnOneIsNotNull == 0
                && CriteriaColumnOneNotEqual == 0
                && CriteriaColumnOneEqual == 0
                && CriteriaColumnTwoLike == 0
                && CriteriaColumnTwoNotLike == 0
                && CriteriaColumnTwoIsNull == 0
                && CriteriaColumnTwoIsNotNull == 0
                && CriteriaColumnTwoNotEqual == 0
                && CriteriaColumnTwoEqual == 0
                && CriteriaColumnThreeLike == 0
                && CriteriaColumnThreeNotLike == 0
                && CriteriaColumnThreeIsNull == 0
                && CriteriaColumnThreeIsNotNull == 0
                && CriteriaColumnThreeNotEqual == 0
                && CriteriaColumnThreeEqual == 0
                && CriteriaColumnFourLike == 0
                && CriteriaColumnFourNotLike == 0
                && CriteriaColumnFourIsNull == 0
                && CriteriaColumnFourIsNotNull == 0
                && CriteriaColumnFourNotEqual == 0
                && CriteriaColumnFourEqual == 0
                && CriteriaColumnFiveLike == 0
                && CriteriaColumnFiveNotLike == 0
                && CriteriaColumnFiveIsNull == 0
                && CriteriaColumnFiveIsNotNull == 0
                && CriteriaColumnFiveEqual == 0
                && CriteriaColumnFiveNotEqual == 0
                && CriteriaNotesPresLike == 0
                && CriteriaTranStatusEqual == 0
                && CriteriaTranStatusIn == 0
                && CriteriaTranTypeIDEqual == 0
                && CriteriaResponceCodeIDEqual == 0
                && CriteriaCurrencyIDEqual == 0
                && CriteriaATMIDEqual == 0
                && CriteriaATMApplicationIDLikeLower == 1
                && CriteriaATMIDIn == 0
                && CriteriaCardNumInBlackListed == 0
                && CriteriaSettledFlagEqual == 0
                && CriteriaSettledUserEqual == 0
                && CriteriaMatchByEqual == 0
                && CriteriaAtmValidEqual == 1
                && CriteriaExistUserAtm == 1) {

            whereClaus = "  transaction_date >=\n"
                    + "                 TO_DATE ('$col1', 'dd.mm.yyyy hh24:mi:ss')\n"
                    + "           AND transaction_date <=\n"
                    + "                 TO_DATE ('$col2', 'dd.mm.yyyy hh24:mi:ss')\n"
                    + "          AND LOWER (ATM_APPLICATION_ID) LIKE LOWER ('%$col3%')\n"
                    + "           AND matching_type = $col4\n"
                    + "           AND record_type = $col5\n"
                    + "           AND atm_valid (atm_id) = 1\n"
                    + "           AND EXISTS\n"
                    + "                 (SELECT   1\n"
                    + "                    FROM   user_atm s\n"
                    + "                   WHERE   s.user_id = $col6 AND s.atm_id = matched_data.ATM_ID)\n"
                    + " ORDER BY   ATM_APPLICATION_ID,\n"
                    + "           transaction_date,\n"
                    + "           transaction_sequence_order_by ";
            whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
            whereClaus = whereClaus.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(transDateT));
            whereClaus = whereClaus.replace("$col3", "" + atmCode);
            whereClaus = whereClaus.replace("$col4", "" + matchingTypeList);
            whereClaus = whereClaus.replace("$col5", "" + masterRecord);
            whereClaus = whereClaus.replace("$col6", "" + logedUser.getUserId());
            index = " /*+index (matched_data PK_MATCHED_DATA)  */ ";
            temp = "";
        }
        WhereColoumnArray = WhereColoumnArray.toUpperCase();
        String[] result = removedop(WhereColoumnArray.split(","));
        this.whereCluase = whereClaus;
        this.whereCluase = this.whereCluase.replaceAll("and record_type = [0-9]", "");
        MatchedDataDAOInter mdDAO = DAOFactory.createMatchedDataDAO(null);
        Object[] finalresult = new Object[2];
        finalresult = mdDAO.customSearch(whereClaus, field, temp,index);

        List<MatchedDataDTOInter> mdDTOL = (List<MatchedDataDTOInter>) finalresult[0];
        totalAmount = (BigDecimal) finalresult[1];
        // findTotalAmount(transDateF, opt1, transDateT, settleDateF, opt2, settleDateT, loadDateF, opt3, loadDateT, amountF, opt4, amountT, transSeqOrderByF, opt5, transSeqOrderByT, cardNo, opt6, accountNo, opt7, notesPre, transStatusCode, transTypeCode, responseCode, currencyCode, atmId, atmCode, atmGroup, blackList, settled, matchingTypeList, masterRecord, settledBy, matchedBy, sortBy, logedUser, field, temp, col1, col2, col3, col4, col5, colop1, colop2, colop3, colop4, colop5);

        return mdDTOL;

    }

    public Object findTotalAmount(Date transDateF, String opt1, Date transDateT, Date settleDateF, String opt2, Date settleDateT,
            Date loadDateF, String opt3, Date loadDateT, Integer amountF, String opt4, Integer amountT, Integer transSeqOrderByF,
            String opt5, Integer transSeqOrderByT, String cardNo, String opt6, String accountNo, String opt7, String notesPre,
            Integer transStatusCode, Integer transTypeCode, Integer responseCode, Integer currencyCode, Integer atmId,
            String atmCode, Integer atmGroup, boolean blackList, Integer settled, Integer matchingTypeList, Integer masterRecord,
            Integer settledBy, Integer matchedBy, String sortBy, UsersDTOInter logedUser, String field, String temp, String col1, String col2, String col3, String col4, String col5, String colop1, String colop2, String colop3, String colop4, String colop5) throws Throwable {

        String whereClaus = "";

        // <editor-fold defaultstate="collapsed" desc="Transaction_date where clause">
        if (opt1.equals("Between")) {
            if (transDateF != null && transDateT != null) {
                whereClaus += " and transaction_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss') "
                        + " and transaction_date <= to_date('$col2','dd.mm.yyyy hh24:mi:ss') ";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                whereClaus = whereClaus.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(transDateT));
            }
        } else if (opt1.equals("<>")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date <> to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals("<")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date < to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals("<=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date <= to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals(">")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date > to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals(">=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date >= to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals("=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date = to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Settlement_date where clause">
        if (opt2.equals("Between")) {
            if (settleDateF != null && settleDateT != null) {
                whereClaus += " and settlement_date between to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and "
                        + "to_date(to_char(nvl(to_date('$col2','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                whereClaus = whereClaus.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(settleDateT));

            }
        } else if (opt2.equals(">")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date > to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));

            }
        } else if (opt2.equals(">=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date >= to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));

            }
        } else if (opt2.equals("<")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date < to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));

            }
        } else if (opt2.equals("<=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date <= to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));

            }
        } else if (opt2.equals("<>")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date <> to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));

            }
        } else if (opt2.equals("=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date = to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));

            }
        }// </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Amount where clause">
        if (opt4.equals("Between")) {
            if (amountF != null && amountT != null) {
                whereClaus += " and amount between nvl($col1,0) and nvl($col2,9999999999999999)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                whereClaus = whereClaus.replace("$col2", "" + amountT);

            }
        } else if (opt4.equals(">")) {
            if (amountF != null) {
                whereClaus += " and amount > nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
            }
        } else if (opt4.equals(">=")) {
            if (amountF != null) {
                whereClaus += " and amount >= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
            }
        } else if (opt4.equals("<")) {
            if (amountF != null) {
                whereClaus += " and amount < nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
            }
        } else if (opt4.equals("<=")) {
            if (amountF != null) {
                whereClaus += " and amount <= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
            }
        } else if (opt4.equals("<>")) {
            if (amountF != null) {
                whereClaus += " and amount <> nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
            }
        } else if (opt4.equals("=")) {
            if (amountF != null) {
                whereClaus += " and amount = nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Transaction_sequence where clause">
        if (opt5.equals("Between")) {
            if (transSeqOrderByF != null && transSeqOrderByT != null) {
                whereClaus += " and transaction_sequence_order_by between nvl($col1,0) and nvl($col2,9999999999999999)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                whereClaus = whereClaus.replace("$col2", "" + transSeqOrderByT);

            }
        } else if (opt5.equals(">")) {
            if (transSeqOrderByF != null) {
                whereClaus += " and transaction_sequence_order_by > nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);

            }
        } else if (opt5.equals(">=")) {
            if (transSeqOrderByF != null) {
                whereClaus += " and transaction_sequence_order_by >= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);

            }
        } else if (opt5.equals("<")) {
            if (transSeqOrderByF != null) {
                whereClaus += " and transaction_sequence_order_by < nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);

            }
        } else if (opt5.equals("<=")) {
            if (transSeqOrderByF != null) {
                whereClaus += " and transaction_sequence_order_by <= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);

            }
        } else if (opt5.equals("<>")) {
            if (transSeqOrderByF != null) {
                whereClaus += " and transaction_sequence_order_by <> nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);

            }
        } else if (opt5.equals("=")) {
            if (transSeqOrderByF != null) {
                whereClaus += " and transaction_sequence_order_by = nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);

            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Card_no where clause">
        if (opt6.equals("Like")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and card_no like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo);
                searchParameter += "Card No. like $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo);
            }
        } else if (opt6.equals("Not Like")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and card_no not like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo);
                searchParameter += "Card No. not like $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo);
            }
        } else if (opt6.equals("is null")) {
            whereClaus += " and card_no is null ";
            whereClaus = whereClaus.replace("$col1", "" + cardNo);
            searchParameter += "Card No.  is null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (opt6.equals("is not null")) {
            whereClaus += " and card_no is not null ";
            whereClaus = whereClaus.replace("$col1", "" + cardNo);
            searchParameter += "Card No. not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (opt6.equals("<>")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and card_no <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo);
                searchParameter += "Card No. <> $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo);
            }
        } else if (opt6.equals("=")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and card_no = '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo);
                searchParameter += "Card No. equal $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo);
            }
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Account_No where clause">
        if (opt7.equals("=")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER = '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo);
                searchParameter += "Account No. equal $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo);
            }
        } else if (opt7.equals("<>")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo);
                searchParameter += "Account No. <> $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo);
            }

        } else if (opt7.equals("is null")) {
            whereClaus += " and CUSTOMER_ACCOUNT_NUMBER is null";
            whereClaus = whereClaus.replace("$col1", "" + accountNo);
            searchParameter += "Account No. is null";
            searchParameter = searchParameter.replace("$F", "" + accountNo);

        } else if (opt7.equals("is not null")) {
            whereClaus += " and CUSTOMER_ACCOUNT_NUMBER is not null";
            whereClaus = whereClaus.replace("$col1", "" + accountNo);
            searchParameter += "Account No. is not null -";
            searchParameter = searchParameter.replace("$F", "" + accountNo);
        } else if (opt7.equals("Like")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER Like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo);
                searchParameter += "Account No. Like $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo);
            }

        } else if (opt7.equals("Not Like")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER not Like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo);
                searchParameter += "Account No. not Like $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo);
            }

        }
        // </editor-fold>
        if (notesPre != null && !notesPre.equals("")) {
            whereClaus += " and lower(notes_presented) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + notesPre);
        }
        if (transStatusCode != 0) {
            if (transStatusCode != -1) {
                whereClaus += " and transaction_status_id = $col1";
                whereClaus = whereClaus.replace("$col1", "" + transStatusCode);
            } else {
                whereClaus += " and transaction_status_id in (select id from transaction_status) ";
            }
        }
        if (colop1.equals("Like")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                searchParameter += "Column 1 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
            }
        } else if (colop1.equals("Not Like")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 not like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                searchParameter += "Column 1 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
            }
        } else if (colop1.equals("is null")) {
            whereClaus += " and COLUMN1 is null ";
            whereClaus = whereClaus.replace("$col1", "" + col1);
            searchParameter += "Column 1 is null -";
            searchParameter = searchParameter.replace("$F", "" + col1);
        } else if (colop1.equals("is not null")) {
            whereClaus += " and COLUMN1 is not null ";
            whereClaus = whereClaus.replace("$col1", "" + col1);
            searchParameter += "Column 1 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop1.equals("<>")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                searchParameter += "Column 1 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
            }
        } else if (colop1.equals("=")) {
            if (col1 != null && !col1.equals("")) {
                whereClaus += " and COLUMN1 = '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + col1);
                searchParameter += "Column 1 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col1);
            }
        }
        if (colop2.equals("Like")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 like '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col2);
            }
        } else if (colop2.equals("Not Like")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 not like '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col2);
            }
        } else if (colop2.equals("is null")) {
            whereClaus += " and COLUMN2 is null ";
            whereClaus = whereClaus.replace("$col2", "" + col2);
            searchParameter += "Column 2 is null -";
            searchParameter = searchParameter.replace("$F", "" + col2);
        } else if (colop2.equals("is not null")) {
            whereClaus += " and COLUMN2 is not null ";
            whereClaus = whereClaus.replace("$col2", "" + col2);
            searchParameter += "Column 2 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop2.equals("<>")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 <> '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col2);
            }
        } else if (colop2.equals("=")) {
            if (col2 != null && !col2.equals("")) {
                whereClaus += " and COLUMN2 = '$col2'";
                whereClaus = whereClaus.replace("$col2", "" + col2);
                searchParameter += "Column 2 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col2);
            }
        }
        if (colop3.equals("Like")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 like '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
            }
        } else if (colop3.equals("Not Like")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 not like '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
            }
        } else if (colop3.equals("is null")) {
            whereClaus += " and COLUMN3 is null ";
            whereClaus = whereClaus.replace("$col3", "" + col3);
            searchParameter += "Column 3 is null -";
            searchParameter = searchParameter.replace("$F", "" + col3);
        } else if (colop3.equals("is not null")) {
            whereClaus += " and COLUMN3 is not null ";
            whereClaus = whereClaus.replace("$col3", "" + col3);
            searchParameter += "Column 3 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop3.equals("<>")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 <> '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
            }
        } else if (colop3.equals("=")) {
            if (col3 != null && !col3.equals("")) {
                whereClaus += " and COLUMN3 = '$col3'";
                whereClaus = whereClaus.replace("$col3", "" + col3);
                searchParameter += "Column 3 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col3);
            }
        }
        if (colop4.equals("Like")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 like '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
            }
        } else if (colop4.equals("Not Like")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 not like '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
            }
        } else if (colop4.equals("is null")) {
            whereClaus += " and COLUMN4 is null ";
            whereClaus = whereClaus.replace("$col4", "" + col4);
            searchParameter += "Column 4 is null -";
            searchParameter = searchParameter.replace("$F", "" + col4);
        } else if (colop4.equals("is not null")) {
            whereClaus += " and COLUMN4 is not null ";
            whereClaus = whereClaus.replace("$col4", "" + col4);
            searchParameter += "Column 4 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop4.equals("<>")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 <> '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
            }
        } else if (colop4.equals("=")) {
            if (col4 != null && !col4.equals("")) {
                whereClaus += " and COLUMN4 = '$col4'";
                whereClaus = whereClaus.replace("$col4", "" + col4);
                searchParameter += "Column 4 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col4);
            }
        }
        if (colop5.equals("Like")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 like '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 like $F -";
                searchParameter = searchParameter.replace("$F", "" + col5);
            }
        } else if (colop5.equals("Not Like")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 not like '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 not like $F -";
                searchParameter = searchParameter.replace("$F", "" + col5);
            }
        } else if (colop5.equals("is null")) {
            whereClaus += " and COLUMN5 is null ";
            whereClaus = whereClaus.replace("$col5", "" + col5);
            searchParameter += "Column 5 is null -";
            searchParameter = searchParameter.replace("$F", "" + col5);
        } else if (colop5.equals("is not null")) {
            whereClaus += " and COLUMN5 is not null ";
            whereClaus = whereClaus.replace("$col5", "" + col5);
            searchParameter += "Column 5 not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo);
        } else if (colop5.equals("<>")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 <> '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 <> $F -";
                searchParameter = searchParameter.replace("$F", "" + col5);
            }
        } else if (colop5.equals("=")) {
            if (col5 != null && !col5.equals("")) {
                whereClaus += " and COLUMN5 = '$col5'";
                whereClaus = whereClaus.replace("$col5", "" + col5);
                searchParameter += "Column 5 equal $F -";
                searchParameter = searchParameter.replace("$F", "" + col5);
            }
        }
        if (transTypeCode != 0) {
            whereClaus += " and transaction_type_id = $col1";
            whereClaus = whereClaus.replace("$col1", "" + transTypeCode);

        }
        if (responseCode != 0) {
            whereClaus += " and response_code_id = $col1";
            whereClaus = whereClaus.replace("$col1", "" + responseCode);

        }
        if (currencyCode != 0) {
            whereClaus += " and currency_id = $col1";
            whereClaus = whereClaus.replace("$col1", "" + currencyCode);

        }
        if (atmId != 0) {
            whereClaus += " and ATM_ID = $col1";
            whereClaus = whereClaus.replace("$col1", "" + atmId);

        }
        if (atmCode != null && !atmCode.equals("")) {
            whereClaus += " and lower(ATM_APPLICATION_ID) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + atmCode);

        }
        if (atmGroup != 0) {
            whereClaus += " and ATM_ID IN (select id from atm_machine where atm_group in (select id from atm_group where parent_id = $col1 or id = $col1))";
            whereClaus = whereClaus.replace("$col1", "" + atmGroup);

        }
        if (blackList) {
            whereClaus += " and card_no in (select card_no from black_listed_card)";

        }
        if (settled != 3) {
            whereClaus += " and settled_flag = $col1";
            whereClaus = whereClaus.replace("$col1", "" + settled);
        }

        if (settledBy != 0) {
            whereClaus += " and settled_user = $col1";
            whereClaus = whereClaus.replace("$col1", "" + settledBy);

        }
        if (matchedBy != 0) {
            whereClaus += " and match_by = $col1";
            whereClaus = whereClaus.replace("$col1", "" + matchedBy);

        }

        whereClaus += " and Exists (select 1 from user_atm s where s.user_id = $user and s.atm_id = ATM_ID)";
        whereClaus = whereClaus.replace("$user", "" + logedUser.getUserId());

        if (sortBy != null && !sortBy.equals("")) {
            whereClaus += " order by transaction_date, match_key";
            whereClaus = whereClaus.replace("$col1", "" + sortBy);
        } else {
            whereClaus += " order by transaction_date, match_key";
        }

        this.whereCluase = whereClaus;
        this.whereCluase = this.whereCluase.replaceAll("and record_type = [0-9]", "");
        MatchedDataDAOInter mdDAO = DAOFactory.createMatchedDataDAO(null);

        BigDecimal amount = (BigDecimal) mdDAO.findTotalAmount(whereClaus, temp);
        totalAmount = amount;
        return amount;
    }

    public String[] removedop(String[] data) throws Throwable {
        List<String> list = Arrays.asList(data);
        Set<String> set = new HashSet<String>(list);
        String[] result = new String[set.size()];
        set.toArray(result);
        return result;
    }

    public Object getAnotherSideOfMatchedData(MatchedDataDTOInter mdDTO) throws Throwable {
        MatchedDataDAOInter mdDAO = DAOFactory.createMatchedDataDAO(null);
        MatchedDataDTOInter mddtoi = (MatchedDataDTOInter) mdDAO.findAnotherSide(mdDTO);
        List<MatchedDataDTOInter> mdDTOL = new ArrayList<MatchedDataDTOInter>();
        mdDTOL.add(mddtoi);
        return mdDTOL;
    }

    public Object MatchedMarkAsSettled(MatchedDataDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable {
        MatchedDataDAOInter mdDAO = DAOFactory.createMatchedDataDAO(null);
        for (MatchedDataDTOInter mdDTO : mdDTOA) {
            mdDAO.updateForMatchedPage(mdDTO, logedUser);
        }
        return null;
    }

    public Object MatchedMarkAsDisputes(MatchedDataDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable {
        MatchedDataDAOInter mdDAO = DAOFactory.createMatchedDataDAO(null);
        for (MatchedDataDTOInter mdDTO : mdDTOA) {
            mdDAO.insertForMatchedPage(mdDTO, logedUser);
            mdDAO.deleteForMatchedPage(mdDTO);
        }
        return null;
    }

    public Object getPickColumn() throws Throwable {
        Session utillist = new Session();
        DualListModel<String> pick = new DualListModel();
        ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);
        List<ColumnDTOInter> cDDTO = (List<ColumnDTOInter>) utillist.GetDefaultColumn();
        List<ColumnDTOInter> cNDDTO = (List<ColumnDTOInter>) utillist.GetNonDefaultColumn();

        List<String> source = new ArrayList<String>();
        List<String> target = new ArrayList<String>();

        for (ColumnDTOInter c : cDDTO) {
            //   if (c.getId() != 1) {
            target.add(c.getHeader());
            // }
        }
        for (ColumnDTOInter c : cNDDTO) {
            // if (c.getId() != 1) {
            source.add(c.getHeader());
            // }
        }

        pick.setSource(source);
        pick.setTarget(target);

        return pick;

    }
}
