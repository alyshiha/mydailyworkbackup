/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import DBCONN.Session;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.bybranchreportDAOInter;
import com.ev.AtmBingo.bus.dto.CardsTakenReportDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.bybranchreportDTOInter;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author Aly
 */
public class bybranchreportBO implements bybranchreportBOInter, Serializable {

    @Override
    public Object getAll(int branch, Date datefrom, Date dateto) throws Throwable {
        bybranchreportDAOInter agDAO = DAOFactory.createbybranchreportDAO();
        List<bybranchreportDTOInter> agDTOL = (List<bybranchreportDTOInter>) agDAO.findAll(branch, datefrom, dateto);
        return agDTOL;
    }

    @Override
    public Object getAllcards(Date datefrom, Date dateto) throws Throwable {
        bybranchreportDAOInter agDAO = DAOFactory.createbybranchreportDAO();
        List<CardsTakenReportDTOInter> agDTOL = (List<CardsTakenReportDTOInter>) agDAO.findAllcards(datefrom, dateto);
        return agDTOL;
    }

    public String runReport(String dateFrom, String dateTo, String user, String cust) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("dateF", dateFrom);
        params.put("user", user);
        params.put("dateT", dateTo);
        params.put("customer", cust);

        Connection conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
        try {
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/branchstatus.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri = "branchstatus" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {

                String x = "../PDF/" + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;

            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();

            return "fail";
        }

    }

    public Integer getLoggedInUser() {
        try {
            Session sessionutil = new Session();
            UsersDTOInter currentUser = sessionutil.GetUserLogging();
            Integer uDTO = currentUser.getUserId();
            return uDTO;
        } catch (Exception ex) {
            return 0;
        } catch (Throwable ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String runReportcards(String dateFrom, String dateTo, String user, String cust) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("dateF", dateFrom);
        params.put("user", user);
        params.put("dateT", dateTo);
        params.put("customer", cust);

        Connection conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
        try {
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/branchcollect.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri = "branchcollect" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {

                String x = "../PDF/" + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;

            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();

            return "fail";
        }

    }

}
