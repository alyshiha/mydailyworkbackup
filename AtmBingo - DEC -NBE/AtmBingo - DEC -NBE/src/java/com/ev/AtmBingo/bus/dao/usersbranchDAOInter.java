package com.ev.AtmBingo.bus.dao;
import java.util.Date;
import java.util.List;
import java.sql.SQLException;
public interface usersbranchDAOInter {

Object insertrecord(Object... obj) throws Throwable ;
Object updaterecord(Object... obj) throws Throwable;
Object deleterecord(Object... obj) throws Throwable;
Object deleteallrecord(Object... obj) throws Throwable;
Object findRecord(Object... obj) throws Throwable ;
Object findRecordsList(Object... obj) throws Throwable;
Object findRecordsAll(Object... obj) throws Throwable ;
void save(Object... obj) throws SQLException, Throwable;
String ValidateLog(String userName, String password, String version, Integer NumOfUsers, Date EndDate) throws Throwable ;
}