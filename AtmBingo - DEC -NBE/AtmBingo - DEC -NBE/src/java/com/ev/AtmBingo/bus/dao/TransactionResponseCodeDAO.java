/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseMasterDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TransactionResponseCodeDAO extends BaseDAO implements TransactionResponseCodeDAOInter {

    protected TransactionResponseCodeDAO() {
        super();
        super.setTableName("transaction_response_code");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TransactionResponseCodeDTOInter uDTO = (TransactionResponseCodeDTOInter) obj[0];
        uDTO.setId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ($id, '$name' , '$description', $amount_type, $default_flag, $master_code)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$name", "" + uDTO.getName());
        insertStat = insertStat.replace("$description", "" + uDTO.getDescription());
        insertStat = insertStat.replace("$amount_type", "" + uDTO.getAmountType());
        insertStat = insertStat.replace("$default_flag", "" + uDTO.getDefaultFlag());
        insertStat = insertStat.replace("$master_code", "" + uDTO.getMasterCode());
        super.executeUpdate(insertStat);
        String action = "Add a new response detail with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TransactionResponseCodeDTOInter uDTO = (TransactionResponseCodeDTOInter) obj[0];
        String updateStat = "update $table set description = '$description', name = '$name', "
                + " amount_type = $amount_type, default_flag = $default_flag, master_code = $master_code"
                + " where id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$name", "" + uDTO.getName());
        updateStat = updateStat.replace("$description", "" + uDTO.getDescription());
        updateStat = updateStat.replace("$amount_type", "" + uDTO.getAmountType());
        updateStat = updateStat.replace("$default_flag", "" + uDTO.getDefaultFlag());
        updateStat = updateStat.replace("$master_code", "" + uDTO.getMasterCode());
        super.executeUpdate(updateStat);
        String action = "update response detail with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }
public int getDefFlagCount(int trmDTO,int id)throws Throwable
{
    super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where master_code = $master and id <> $ID and DEFAULT_FLAG = 1 order by 1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$master", "" + trmDTO);
        selectStat = selectStat.replace("$ID", "" + id);
        ResultSet rs = executeQuery(selectStat);
        int count =0;
        while (rs.next()) {
            count++;
        }
        super.postSelect(rs);
        return count;
    }
    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TransactionResponseCodeDTOInter uDTO = (TransactionResponseCodeDTOInter) obj[0];
        String deleteStat = "delete from $table where  id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        String action = "Delete response detail with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        TransactionResponseCodeDTOInter uDTO = DTOFactory.createTransactionResponseCodeDTO();
        while (rs.next()) {
            uDTO.setAmountType(rs.getInt("amount_type"));
            uDTO.setDefaultFlag(rs.getInt("default_flag"));
            uDTO.setDescription(rs.getString("description"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setMasterCode(rs.getInt("master_code"));
            uDTO.setName(rs.getString("name"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by 1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<TransactionResponseCodeDTOInter> uDTOL = new ArrayList<TransactionResponseCodeDTOInter>();
        while (rs.next()) {
            TransactionResponseCodeDTOInter uDTO = DTOFactory.createTransactionResponseCodeDTO();
            uDTO.setAmountType(rs.getInt("amount_type"));
            uDTO.setDefaultFlag(rs.getInt("default_flag"));
            uDTO.setDescription(rs.getString("description"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setMasterCode(rs.getInt("master_code"));
            uDTO.setName(rs.getString("name"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByMaster(TransactionResponseMasterDTOInter trmDTO) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where master_code = $master order by 1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$master", "" + trmDTO.getId());
        ResultSet rs = executeQuery(selectStat);
        List<TransactionResponseCodeDTOInter> uDTOL = new ArrayList<TransactionResponseCodeDTOInter>();
        while (rs.next()) {
            TransactionResponseCodeDTOInter uDTO = DTOFactory.createTransactionResponseCodeDTO();
            uDTO.setAmountType(rs.getInt("amount_type"));
            uDTO.setDefaultFlag(rs.getInt("default_flag"));
            uDTO.setDescription(rs.getString("description"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setMasterCode(rs.getInt("master_code"));
            uDTO.setName(rs.getString("name"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
