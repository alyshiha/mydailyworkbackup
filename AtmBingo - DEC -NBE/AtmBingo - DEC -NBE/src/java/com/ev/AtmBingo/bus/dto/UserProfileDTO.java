/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class UserProfileDTO extends BaseDTO implements Serializable , UserProfileDTOInter {

    protected UserProfileDTO() {
        super();
    }
    private int UserId;
    private int ProfileId;

    public int getProfileId() {
        return ProfileId;
    }

    public void setProfileId(int ProfileId) {
        this.ProfileId = ProfileId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

}
