/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author Administrator
 */
public class ColumnDAO extends BaseDAO implements ColumnDAOInter {

    protected ColumnDAO() {
        super();
        super.setTableName("FILE_COLUMN_DEFINITION");
    }

    public Object AddCol() throws Throwable {
        super.preUpdate();
        String updateStat = "insert into file_column_definition\n"
                + "  (id, name, column_name, data_type, display_in_loading, display_in_matching, duplicate_no, display_in_transaction, width, column_order_by)\n"
                + "values\n"
                + "  (file_column_definition_seq.nextval, 'Cor.Amount', 'CORRECTIVE_AMOUNT', 2, 1, 1, '', 1, 40, 20)";
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object updateNameOnly(Object... obj) throws Throwable {
        super.preUpdate();
        ColumnDTOInter uDTO = (ColumnDTOInter) obj[0];
        String updateStat = "update $table set name =  '$name',duplicate_no = '$duplicatenumber',width = $width,display_in_transaction = $displaytransaction,display_in_loading=$displayloading,display_in_matching=$displaymatching where id = $id ";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$name", "" + uDTO.getHeader());
        updateStat = updateStat.replace("$width", "" + uDTO.getWidth());
        updateStat = updateStat.replace("$duplicatenumber", "" + uDTO.getDuplicate_no());
        int temp = 0;
        if (uDTO.getDisplayinmatching() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        updateStat = updateStat.replace("$displaymatching", "" + temp);
        if (uDTO.getDisplayinloading() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        updateStat = updateStat.replace("$displayloading", "" + temp);
        if (uDTO.getDisplayintransaction() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        updateStat = updateStat.replace("$displaytransaction", "" + temp);
        super.executeUpdate(updateStat);
        super.postUpdate("Update " + uDTO.getHeader() + " column header", false);
        return null;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where display_in_transaction in  (1,3) order by id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<ColumnDTOInter> uDTOL = new ArrayList<ColumnDTOInter>();
        while (rs.next()) {
            ColumnDTOInter uDTO = DTOFactory.createColumnDTO();
            uDTO.setDbName(rs.getString("column_name"));
            uDTO.setHeader(rs.getString("name"));
            uDTO.setProperty(getproperty(rs.getString("column_name")));
            uDTO.setId(rs.getInt("id"));
            uDTO.setWidth(rs.getInt("width"));
            Integer temp2 = rs.getInt("display_in_matching");
            if (temp2 == 1) {
                uDTO.setDisplayinmatching(Boolean.TRUE);
            } else {
                uDTO.setDisplayinmatching(Boolean.FALSE);
            }
            temp2 = rs.getInt("display_in_loading");
            if (temp2 == 1) {
                uDTO.setDisplayinloading(Boolean.TRUE);
            } else {
                uDTO.setDisplayinloading(Boolean.FALSE);
            }
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findAllColoumn() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table  order by id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<ColumnDTOInter> uDTOL = new ArrayList<ColumnDTOInter>();
        while (rs.next()) {
            ColumnDTOInter uDTO = DTOFactory.createColumnDTO();
            uDTO.setDbName(rs.getString("column_name"));
            uDTO.setHeader(rs.getString("name"));
            uDTO.setProperty(getproperty(rs.getString("column_name")));
            uDTO.setId(rs.getInt("id"));
            uDTO.setWidth(rs.getInt("width"));
            Integer temp = rs.getInt("duplicate_no");
            uDTO.setDuplicate_no(temp.toString());
            Integer temp2 = rs.getInt("display_in_matching");
            if (temp2 == 1) {
                uDTO.setDisplayinmatching(Boolean.TRUE);
            } else {
                uDTO.setDisplayinmatching(Boolean.FALSE);
            }
            temp2 = rs.getInt("display_in_loading");
            if (temp2 == 1) {
                uDTO.setDisplayinloading(Boolean.TRUE);
            } else {
                uDTO.setDisplayinloading(Boolean.FALSE);
            }
            temp2 = rs.getInt("display_in_transaction");
            if (temp2 == 1) {
                uDTO.setDisplayintransaction(Boolean.TRUE);
            } else {
                uDTO.setDisplayintransaction(Boolean.FALSE);
            }
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public String getproperty(String colname) {

        String name = colname.replace("_", "");
        name = name.toLowerCase();
        return name;

    }

    public Object searchByName(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        List columns = new ArrayList();
        columns = (List) obj[0];
        String columnName = "";
        for (int i = 0; i < columns.size(); i++) {
            if (i == 0) {
                columnName = "'" + (String) columns.get(i) + "'";
            }
            columnName = columnName + ",'" + (String) columns.get(i) + "'";
        }
        String[] inputs = {columnName};
        String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
        Boolean found = Boolean.FALSE;
        for (String input : inputs) {
            boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace("'", "").replace(",", "").replace("#", "").replace(".", "").replace("(", "").replace(")", ""));
            if (b == false) {
                found = Boolean.TRUE;
            }
            if (!"".equals(input)) {
                for (String validate1 : validate) {
                    if (input.toUpperCase().contains(validate1.toUpperCase())) {
                        found = Boolean.TRUE;
                    }
                }
            }
        }
        List<ColumnDTOInter> ColResult = new ArrayList<ColumnDTOInter>();
        ResultSet result = null;
        if (!found) {
            // databse insert statement usinf conn pool getConn().createStatement();
            //String colName = (String) obj[0];
            String selectStat = "select * from " + super.getTableName() + " where name in (" + columnName + ")";
            result = executeQuery(selectStat);
            List<ColumnDTOInter> mtL = new ArrayList<ColumnDTOInter>();
            while (result.next()) {
                ColumnDTOInter mt = DTOFactory.createColumnDTO();
                mt.setDbName(result.getString("column_name"));
                mt.setHeader(result.getString("name"));
                mt.setId(result.getInt("id"));
                mt.setProperty(getproperty(result.getString("column_name")));
                mt.setWidth(result.getInt("width"));
                mtL.add(mt);
            }

            for (int j = 0; j < columns.size(); j++) {
                for (int i = 0; i < mtL.size(); i++) {
                    if (mtL.get(i).getHeader().toLowerCase().equals(columns.get(j).toString().toLowerCase())) {
                        ColResult.add(mtL.get(i));
                    }
                }
            }
        }
        super.postSelect(result);
        return ColResult;

    }

    public Object find(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        int colName = (Integer) obj[0];
        String selectStat = "select * from " + super.getTableName() + " where id = " + colName;
        ResultSet result = executeQuery(selectStat);
        List<ColumnDTOInter> mtL = new ArrayList<ColumnDTOInter>();
        while (result.next()) {
            ColumnDTOInter mt = DTOFactory.createColumnDTO();
            mt.setDbName(result.getString("column_name"));
            mt.setHeader(result.getString("name"));
            mt.setId(result.getInt("id"));
            mt.setProperty(getproperty(result.getString("column_name")));
            mt.setWidth(result.getInt("width"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL.get(0);
    }

    public Object searchByColumnName(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String colName = (String) obj[0];
        String selectStat = "select * from " + super.getTableName() + " where column_name = '" + colName + "'";
        ResultSet result = executeQuery(selectStat);
        List<ColumnDTOInter> mtL = new ArrayList<ColumnDTOInter>();
        while (result.next()) {
            ColumnDTOInter mt = DTOFactory.createColumnDTO();
            mt.setDbName(result.getString("column_name"));
            mt.setHeader(result.getString("name"));
            mt.setId(result.getInt("id"));
            mt.setProperty(getproperty(result.getString("column_name")));
            mt.setWidth(result.getInt("width"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL.get(0);
    }

    public Object findDefualt() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where  display_in_transaction = 1 order by column_order_by";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<ColumnDTOInter> uDTOL = new ArrayList<ColumnDTOInter>();
        while (rs.next()) {
            ColumnDTOInter uDTO = DTOFactory.createColumnDTO();
            uDTO.setDbName(rs.getString("column_name"));
            uDTO.setHeader(rs.getString("name"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setProperty(getproperty(rs.getString("column_name")));
            uDTO.setWidth(rs.getInt("width"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findNotDefualt() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where  display_in_transaction = 3 order by column_order_by";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<ColumnDTOInter> uDTOL = new ArrayList<ColumnDTOInter>();
        while (rs.next()) {
            ColumnDTOInter uDTO = DTOFactory.createColumnDTO();
            uDTO.setDbName(rs.getString("column_name"));
            uDTO.setHeader(rs.getString("name"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setProperty(getproperty(rs.getString("column_name")));
            uDTO.setWidth(rs.getInt("width"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }
}
