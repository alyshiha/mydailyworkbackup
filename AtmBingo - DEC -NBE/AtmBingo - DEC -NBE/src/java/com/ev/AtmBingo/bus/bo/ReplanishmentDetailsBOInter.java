/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.RepPopDTOInter;
import com.ev.AtmBingo.bus.dto.RepTransDetailDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentDetailDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface ReplanishmentDetailsBOInter extends BaseBOInter {

    List<RepTransDetailDTOInter> findTransDetail(String ATM, Date FromDate, Date ToDate) throws Throwable;

    void save(List<ReplanishmentMasterDTOInter> entities) throws Throwable;

    Object recalcall(ReplanishmentMasterDTOInter rmDTO);

    RepPopDTOInter replanishmentPopUp(ReplanishmentMasterDTOInter id) throws Throwable;

    Date getLastReplanishmentDateFor(int atmId) throws Throwable;

    Object getUsers() throws Throwable;

    Boolean validateAtmName(List<AtmMachineDTOInter> amDTOL, String atmName) throws Throwable;

    Object getCassetes() throws Throwable;

    int findByAtmId(String ATMID) throws Throwable;

    Object editeReplanishment(ReplanishmentMasterDTOInter rmDTO, List<ReplanishmentDetailDTOInter> rdDTOL, int operation) throws Throwable;

    Object editeReplanishmentnew(ReplanishmentMasterDTOInter rmDTO, List<ReplanishmentDetailDTOInter> rdDTOL, int operation) throws Throwable;

    Object getAtmMachines(UsersDTOInter loggedIn, Integer grpId) throws Throwable;

    Object getNewReplanishmentDetail() throws Throwable;

    Object getOldReplanishmentDetail(int masterId) throws Throwable;

    Object getReplanishmentMaster() throws Throwable;

    Object RecalculateReplanishment(ReplanishmentMasterDTOInter rmDTO);

    Object getReplanishmentData(Date dailysheet,Date from, Date to, int atmId, Date createdF, Date createdT, int userId, int group2, int status, Date From1, Date From2, Date To1, Date To2) throws Throwable;
}
