/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.BlockUsersDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.EmgHolidaysDAOInter;
import com.ev.AtmBingo.bus.dao.HolidaysDAOInter;
import com.ev.AtmBingo.bus.dao.SystemTableDAOInter;
import com.ev.AtmBingo.bus.dao.TimeShiftDetailDAOInter;
import com.ev.AtmBingo.bus.dao.UserPassDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.BlockUsersDTOInter;
import com.ev.AtmBingo.bus.dto.EmgHolidaysDTOInter;
import com.ev.AtmBingo.bus.dto.HolidaysDTOInter;
import com.ev.AtmBingo.bus.dto.SystemTableDTOInter;
import com.ev.AtmBingo.bus.dto.TimeShiftDetailDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.print.DocFlavor.STRING;

/**
 *
 * @author Administrator
 */
public class LoginBO extends BaseBO implements LoginBOInter, Serializable {

    protected LoginBO() {
        super();
    }

    public Object checkLogin(String userName, String pass) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        TimeShiftDetailDAOInter tsdDAO = DAOFactory.createTimeShiftDetailDAO(null);
        HolidaysDAOInter hDAO = DAOFactory.createHolidaysDAO(null);
        EmgHolidaysDAOInter ehDAO = DAOFactory.createEmgHolidaysDAO(null);
        BlockUsersDAOInter buDAO = DAOFactory.createBlockUsersDAO(null);

        UsersDTOInter uDTO = (UsersDTOInter) uDAO.findByLogonName(userName);
        TimeShiftDetailDTOInter tsdDTO;
        HolidaysDTOInter hDTO;
        EmgHolidaysDTOInter ehDTO;
        BlockUsersDTOInter buDTO = (BlockUsersDTOInter) buDAO.find(uDTO.getUserId());
        if (uDTO.getLogonName() == null) {
            return "Missing user name";
        } else {
            //Check blocked user
            //ask ayman
            //-----------------
            if (!(Boolean) uDAO.checkUserPass(userName, pass)) {
                return "Wrong Password";
            }

            if (uDTO.getShiftId() == 0) {
//                return "Missing Shift";
//            } else {
//                tsdDTO = (TimeShiftDetailDTOInter) tsdDAO.findShiftDay(uDTO.getShiftId());
//                if (tsdDTO.getWorking() != 1) {
//                    return "Not working in this shift in this day";
//                }
//
//
//                if (!tsdDTO.getTimeFrom().before(super.getTime()) ) {
//
//                    return "This time of shift is from "+tsdDTO.getTimeFrom()+" to "+tsdDTO.getTimeTo();
//                }else if(!tsdDTO.getTimeTo().after(super.getTime())){
//                    return "This time of shift is from "+tsdDTO.getTimeFrom()+" to "+tsdDTO.getTimeTo();
//                }

            }
            if ((Boolean) hDAO.findNowIsHoliday() && (Boolean) ehDAO.findNowIsHoliday() && uDTO.getHolidayEnabled() == 1) {
                // return "Access denied due to holiday";
            }

            if (buDTO.getUserId().getUserName() == null) {
                // return "Blocked User";
            } else {
                return "Access denied due to user blockage";
            }
            return "Success";
        }

    }

    
    public String ValidateLog(String userName, String password, String version, Integer NumOfUsers, Date EndDate) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        String Msg = (String) uDAO.ValidateLog(userName, password, version, NumOfUsers, EndDate);
        return Msg;
    }

    public Object findUser(String userName) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        UsersDTOInter logedinUser = (UsersDTOInter) uDAO.findByLogonName(userName);
        return logedinUser;
    }

    public Object FirstTimeLogin(String userName) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        String FirstTime = (String) uDAO.FirstTimeLogin(userName);
        return FirstTime;
    }

    public Object lockUser(String userName) throws Throwable {

        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        uDAO.Lockuser(userName);
        return "Done";
    }

    public int userlic() throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        return uDAO.userlic();
    }

    public Boolean checkPassExpire(String userName) throws Throwable {
        UserPassDAOInter upDAO = DAOFactory.createUserPassDAO();
        SystemTableDAOInter stDAO = DAOFactory.createSystemTableDAO(null);
        Integer days = Integer.valueOf(((SystemTableDTOInter) stDAO.find("PASSWORD CHANGE")).getParameterValue());

        Date now = Calendar.getInstance().getTime();

        UsersDTOInter uDTO = (UsersDTOInter) findUser(userName);
        Date lastChangeDate = (Date) upDAO.findLastUpdate(uDTO.getUserId());

        if (lastChangeDate == null) {
            return false;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(lastChangeDate);
        c.set(c.DATE, days);

        lastChangeDate = c.getTime();

        if (lastChangeDate.before(now)) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) throws Throwable {
        LoginBOInter lbo = BOFactory.createLoginBO(null);
        String msg = (String) lbo.checkLogin("Akram Samir", "admin");

    }
}
