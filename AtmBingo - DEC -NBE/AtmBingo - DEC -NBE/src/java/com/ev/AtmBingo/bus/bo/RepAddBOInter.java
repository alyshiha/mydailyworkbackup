/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.RepAddDTOInter;
import com.ev.AtmBingo.bus.dto.RepDetailTempDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface RepAddBOInter extends Serializable {

    List<RepAddDTOInter> GetAllRecords() throws Throwable;

    List<ReplanishmentMasterDTOInter> GetRecord(Date todate, int atmid) throws Throwable;

    String deleterecord(RepAddDTOInter RecordToDelete) throws Throwable;

    int insertrecord(RepAddDTOInter RecordToInsert) throws Throwable;

    String updaterecord(RepAddDTOInter RecordToUpdate) throws Throwable;

    int findInTemp(int ID) throws Throwable;

    List<RepDetailTempDTOInter> GetCassette(int RepID) throws Throwable;

    String savecassette(List<RepDetailTempDTOInter> entities) throws Throwable;

    String Excute(RepAddDTOInter record) throws Throwable;
}
