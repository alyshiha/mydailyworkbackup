/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import java.util.Date;

/**
 *
 * @author Aly
 */
public interface bybranchreportDAOInter {

    Object findAll(int branch, Date datefrom, Date dateto) throws Throwable;
    Object findAllcards(Date datefrom, Date dateto) throws Throwable;
}
