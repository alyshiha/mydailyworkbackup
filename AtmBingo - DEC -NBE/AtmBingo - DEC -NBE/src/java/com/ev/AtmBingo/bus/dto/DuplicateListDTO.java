/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class DuplicateListDTO extends BaseDTO implements DuplicateListDTOInter , Serializable{

    String  ColName, DupColName,DupCol2Name;
    int ColID, DupColID,DupCol2ID;

    public int getColID() {
        return ColID;
    }

    public void setColID(int ColID) {
        this.ColID = ColID;
    }

    public String getColName() {
        return ColName;
    }

    public void setColName(String ColName) {
        this.ColName = ColName;
    }

    public int getDupColID() {
        return DupColID;
    }

    public void setDupColID(int DupColID) {
        this.DupColID = DupColID;
    }

    public String getDupColName() {
        return DupColName;
    }

    public void setDupColName(String DupColName) {
        this.DupColName = DupColName;
    }

        public int getDupCol2ID() {
        return DupCol2ID;
    }

    public void setDupCol2ID(int DupCol2ID) {
        this.DupCol2ID = DupCol2ID;
    }

    public String getDupCol2Name() {
        return DupCol2Name;
    }

    public void setDupCol2Name(String DupCol2Name) {
        this.DupCol2Name = DupCol2Name;
    }
}
