/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.BlockReasonDTOInter;
import com.ev.AtmBingo.bus.dto.BlockUsersDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class BlockUsersDAO extends BaseDAO implements BlockUsersDAOInter {

    protected BlockUsersDAO() {
        super();
        super.setTableName("blocked_users");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        BlockUsersDTOInter uDTO = (BlockUsersDTOInter) obj[0];
        String insertStat = "insert into $table values ($user_id , to_date(to_char(sysdate,'dd.MM.yyyy hh24:mi:ss'),'dd.MM.yyyy hh24:mi:ss'), $reason_id)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$user_id", "" + uDTO.getUserId().getUserId());
        insertStat = insertStat.replace("$reason_id", "" + uDTO.getReasonId().getReasonId());
        super.executeUpdate(insertStat);
        super.postUpdate("Block " + uDTO.getUserId().getUserName() + " for the reason " + uDTO.getReasonId().getReasonName(), false);
        return null;
    }
  public Object unlockuser(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        UsersDTOInter uDTO = (UsersDTOInter) obj[0];
        String updateStat = "update users set  locked = 2"
                + " where user_id = $user_id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$user_id", "" + uDTO.getUserId());
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }
    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        BlockUsersDTOInter uDTO = (BlockUsersDTOInter) obj[0];
        String updateStat = "update $table set  reason_id = $reason_id"
                + ", block_time = to_date(to_char(sysdate,'dd.MM.yyyy hh24:mi:ss'),'dd.MM.yyyy hh24:mi:ss')"
                + " where user_id = $user_id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$reason_id", "" + uDTO.getReasonId().getReasonId());
        updateStat = updateStat.replace("$user_id", "" + uDTO.getUserId().getUserId());

        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        BlockUsersDTOInter uDTO = (BlockUsersDTOInter) obj[0];
        String deleteStat = "delete from $table where user_id = $user_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$user_id", "" + uDTO.getUserId().getUserId());
        super.executeUpdate(deleteStat);
        super.postUpdate("Unblock " + uDTO.getUserId().getUserName(), false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer userId = (Integer) obj[0];
        String selectStat = "select * from $table where user_id = $user_id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$user_id", "" + userId);
        ResultSet rs = executeQuery(selectStat);
        BlockUsersDTOInter uDTO = DTOFactory.createBlockUsersDTO();
        while (rs.next()) {
            uDTO.setBlockTime(rs.getDate("block_time"));

            BlockReasonDTOInter brDTO = DTOFactory.createBlockReasonDTO();
            UsersDTOInter userDTO = DTOFactory.createUsersDTO();

            brDTO.setReasonId(rs.getInt("reason_id"));
            uDTO.setReasonId(brDTO);
            userDTO.setUserId(rs.getInt("user_id"));
            uDTO.setUserId(userDTO);

        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by block_time";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<BlockUsersDTOInter> uDTOL = new ArrayList<BlockUsersDTOInter>();

        while (rs.next()) {

            BlockReasonDTOInter brDTO = DTOFactory.createBlockReasonDTO();
            UsersDTOInter u = DTOFactory.createUsersDTO();


            BlockUsersDTOInter uDTO = DTOFactory.createBlockUsersDTO();


            uDTO.setBlockTime(rs.getDate("block_time"));


            brDTO.setReasonId(rs.getInt("reason_id"));
            uDTO.setReasonId(brDTO);
            u.setUserId(rs.getInt("user_id"));
            uDTO.setUserId(u);






            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public static void main(String[] args) throws Throwable {
    }
}
