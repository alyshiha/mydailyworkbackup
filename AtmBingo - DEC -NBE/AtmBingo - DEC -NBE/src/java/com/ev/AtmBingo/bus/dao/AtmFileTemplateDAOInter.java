/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Administrator
 */
public interface AtmFileTemplateDAOInter {

    Object delete(Object... obj) throws Throwable;
Object deleterecordTemplate(Object... obj) throws Throwable ;
    Object find(Object... obj) throws Throwable;

    Object findAll(String Table) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
