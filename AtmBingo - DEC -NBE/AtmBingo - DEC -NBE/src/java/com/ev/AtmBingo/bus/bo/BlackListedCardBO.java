/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.BlackListedCardDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.BlackListedCardDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class BlackListedCardBO extends BaseBO implements BlackListedCardBOInter,Serializable{

    protected BlackListedCardBO() {
        super();
    }

    public Object getBlackListed()throws Throwable{
        BlackListedCardDAOInter blcDAO = DAOFactory.createBlackListedCardDAO(null);
        List<BlackListedCardDTOInter> blcDTOL = (List<BlackListedCardDTOInter>)blcDAO.findAll();
        return blcDTOL;
    }

    public Object editeBlackListed(BlackListedCardDTOInter blcDTO,String oldCardNo,int operation)throws Throwable{
        BlackListedCardDAOInter blcDAO = DAOFactory.createBlackListedCardDAO(null);
        switch(operation){
            case INSERT:
                blcDAO.insert(blcDTO);
                return "Insert Done";
            case UPDATE:
                blcDAO.update(blcDTO,oldCardNo);
                return "Update Done";
            case DELETE:
                blcDAO.delete(blcDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }


}
