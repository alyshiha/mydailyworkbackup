/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.CassetteDAOInter;
import com.ev.AtmBingo.bus.dao.CurrencyDAOInter;
import com.ev.AtmBingo.bus.dao.CurrencyMasterDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class CasseteBO extends BaseBO implements CasseteBOInter,Serializable{

    protected CasseteBO() {
        super();
        
    }

    public Object getCurrency()throws Throwable{
        CurrencyMasterDAOInter cmDAO = DAOFactory.createCurrencyMasterDAO(null);
        List<CurrencyMasterDTOInter> cmDTOL = (List<CurrencyMasterDTOInter>)cmDAO.findAll();
        return cmDTOL;
    }
public Object getCurrencys() throws Throwable {
        CurrencyDAOInter cDAO = DAOFactory.createCurrencyDAO(null);
        List<CurrencyDTOInter> cDTOL = (List<CurrencyDTOInter>) cDAO.findAll();
        return cDTOL;
    }
    public Object getCassetes()throws Throwable{
        CassetteDAOInter cDAO = DAOFactory.createCassetteDAO(null);
        List<CassetteDTOInter> cDTOL = (List<CassetteDTOInter>)cDAO.findAll();
        return cDTOL;
    }

    public Object editeCassette(CassetteDTOInter cDTO, int operation)throws Throwable{
        CassetteDAOInter cDAO = DAOFactory.createCassetteDAO(null);
        switch(operation){
            case INSERT:
                cDAO.insert(cDTO);
                return "Insert Done";
            case UPDATE:
                cDAO.update(cDTO);
                return "Update Done";
            case DELETE:
                cDAO.delete(cDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }
}
