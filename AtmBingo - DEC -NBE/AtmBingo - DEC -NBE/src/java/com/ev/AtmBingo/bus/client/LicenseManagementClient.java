/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.bus.bo.ATMMachineLICBOINTER;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.ATMMAchineLIC;
import com.ev.AtmBingo.bus.dto.ATMMAchineLICInter;
import com.ev.AtmBingo.bus.dto.LincManagmentDetailsDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;

/**
 *
 * @author Administrator
 */
public class LicenseManagementClient  implements Serializable{

    private ATMMachineLICBOINTER myObject;
    private LincManagmentDetailsDTOInter dd;
    private ATMMAchineLICInter myDTO;
    private List<ATMMAchineLICInter> myList, selectedList,tempList;
    private String message, bakcolor, filterATM, defineatms, liclimit, selectedatms, remain, outlic;
    private static Integer counter, checked;



    public String getOutlic() {
        return outlic;
    }

    public void setOutlic(String outlic) {
        this.outlic = outlic;
    }

    public String getRemain() {
        return remain;
    }

    public void setRemain(String remain) {
        this.remain = remain;
    }

    public String getDefineatms() {
        return defineatms;
    }

    public void setDefineatms(String defineatms) {
        this.defineatms = defineatms;
    }

    public String getLiclimit() {
        return liclimit;
    }

    public void setLiclimit(String liclimit) {
        this.liclimit = liclimit;
    }

    public String getSelectedatms() {
        return selectedatms;
    }

    public void setSelectedatms(String selectedatms) {
        this.selectedatms = selectedatms;
    }

    public String getFilterATM() {
        return filterATM;
    }

    public void setFilterATM(String filterATM) {
        this.filterATM = filterATM;
    }

    public String getBakcolor() {
        return bakcolor;
    }

    public void setBakcolor(String bakcolor) {
        this.bakcolor = bakcolor;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ATMMAchineLICInter getMyDTO() {
        return myDTO;
    }

    public void setMyDTO(ATMMAchineLICInter myDTO) {
        this.myDTO = myDTO;
    }

    public List<ATMMAchineLICInter> getTempList() {
        return tempList;
    }

    public void setTempList(List<ATMMAchineLICInter> tempList) {
        this.tempList = tempList;
    }

    public List<ATMMAchineLICInter> getMyList() {
        return myList;
    }

    public void setMyList(List<ATMMAchineLICInter> myList) {
        this.myList = myList;
    }

    /** Creates a new instance of LicenseManagementClient */
    public LicenseManagementClient() throws Throwable {
        myObject = BOFactory.createATMMachineLICBOINTER(null);
        dd = (LincManagmentDetailsDTOInter) myObject.LicMangDetail();
        selectedatms = dd.getInlic();
        defineatms = dd.getAllmachines();
        liclimit = dd.getLic();
        remain = dd.getRemaininlic();
        outlic = dd.getOutlic();
        myList = new ArrayList<ATMMAchineLICInter>();
        selectedList = new ArrayList<ATMMAchineLICInter>();
        myList = (List<ATMMAchineLICInter>) myObject.getAtmMachine();
        tempList = myList;
        counter = 0;
        checked = 0;
        for (int i = 0; i < myList.size(); i++) {
            if (myList.get(i).getLIC()) {
                checked++;
            }
        }
        counter += checked;
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void filterATMList() {
        try {
            myList = (List<ATMMAchineLICInter>) myObject.FilterAtmMachine(tempList,filterATM);
        } catch (Throwable ex) {
            Logger.getLogger(LicenseManagementClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void changeValue(ValueChangeEvent e) {
        Boolean holder = (Boolean) e.getNewValue();
        if (holder == true) {
            counter++;
        } else {
            counter--;
        }
    }

    public void changeValueDelete(ATMMAchineLICInter holder ) {
        try {
           
            if (holder.getLIC() == true) {
                holder.setLIC(false);
            } else if (selectedatms.equals(liclimit)) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "license limit Exceeded", ""));
            } else {
                holder.setLIC(true);
            }
            myObject.SAVE(myList);
            dd = (LincManagmentDetailsDTOInter) myObject.LicMangDetail();
             selectedatms = dd.getInlic();
            defineatms = dd.getAllmachines();
            liclimit = dd.getLic();
            remain = dd.getRemaininlic();
            outlic = dd.getOutlic();
        } catch (Throwable ex) {
        }
    }

    public void deleteLic() {
        try {
            myObject.Delete(selectedList);
            myList = (List<ATMMAchineLICInter>) myObject.getAtmMachine();
            message = "Done Successfully";
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void saveList() {
        try {
            myObject.SAVE(myList);
            message = "Done Successfully";
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void resetVars() {
        message = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
