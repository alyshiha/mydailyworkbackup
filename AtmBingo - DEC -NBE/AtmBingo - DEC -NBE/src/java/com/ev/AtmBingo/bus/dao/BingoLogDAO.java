/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.BingoLogDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author KhAiReE
 */
public class BingoLogDAO extends BaseDAO implements BingoLogDAOInter {

    protected BingoLogDAO() {
        super();
        super.setTableName("bingo_log");
    }

    public Object findAll(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from " + super.getTableName() + " order by action_date desc";
        ResultSet result = executeQuery(selectStat);
        List<BingoLogDTOInter> blL = new ArrayList<BingoLogDTOInter>();
        while (result.next()) {
            BingoLogDTOInter bl = DTOFactory.createBingoLogDTO();
            bl.setAction(result.getString("action"));
            bl.setId(result.getInt("id"));
            bl.setActionDate(result.getTimestamp("action_date"));
            bl.setIpAddress(result.getString("ip_address"));
            bl.setUserId(result.getInt("user_id"));
            blL.add(bl);
        }
        super.postSelect(result);
        return blL;
    }

    public Object CustomSearch(Object... obj) throws Throwable {
        super.preSelect();
        String whereCluase = (String) obj[0];
        String selectStat = "select * from " + super.getTableName() + " where 1=1 $whereCluase order by action_date desc";
        selectStat = selectStat.replace("$whereCluase", "" + whereCluase);
        List<BingoLogDTOInter> blL = new ArrayList<BingoLogDTOInter>();
        ResultSet result = executeQuery(selectStat);
        while (result.next()) {
            BingoLogDTOInter bl = DTOFactory.createBingoLogDTO();
            bl.setAction(result.getString("action"));
            bl.setActionDate(result.getTimestamp("action_date"));
            bl.setId(result.getInt("id"));
            bl.setIpAddress(result.getString("ip_address"));
            bl.setUserId(result.getInt("user_id"));
            blL.add(bl);
        }
        super.postSelect(result);
        return blL;
    }
}
