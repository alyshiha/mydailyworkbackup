/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Aly
 */
public interface bybranchreportDTOInter  extends Serializable {

    String getBranch();

    String getCardnum();

    Date getCollecteddate();

    String getStatus();

    void setBranch(String branch);

    void setCardnum(String cardnum);

    void setCollecteddate(Date collecteddate);

    void setStatus(String status);
    
}
