/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.presdto;

import com.ev.AtmBingo.base.presdto.BasePresDTO;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class AtmMachineTypePresDTO extends BasePresDTO implements AtmMachineTypePresDTOInter, Serializable {


    private int id;
    private String name;

    protected AtmMachineTypePresDTO() {
        super();
    }



    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

}
