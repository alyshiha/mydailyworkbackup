/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import DBCONN.Session;
import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.ReportingTool;
import com.ev.AtmBingo.bus.dao.AtmGroupDAOInter;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.ColumnDAOInter;
import com.ev.AtmBingo.bus.dao.CurrencyMasterDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.MatchingTypeDAOInter;
import com.ev.AtmBingo.bus.dao.RejectedReportDAOInter;
import com.ev.AtmBingo.bus.dao.RejectedTransactionsDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionResponseCodeDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionStatusDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionTypeDAOInter;
import com.ev.AtmBingo.bus.dao.ValidationRulesDAOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.MatchingTypeDTOInter;
import com.ev.AtmBingo.bus.dto.RejectedReportDTOInter;
import com.ev.AtmBingo.bus.dto.RejectedTransactionsDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.ValidationRulesDTOInter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Administrator
 */
public class RejectedBO extends BaseBO implements RejectedBOInter ,Serializable{

    protected RejectedBO() {
        super();
    }
    private String searchParameter;
    private String whereCluase;

    public String getWhereCluase() {
        return whereCluase;
    }

    public void setWhereCluase(String whereCluase) {
        this.whereCluase = whereCluase;
    }

    public String getSearchParameter() {
        return searchParameter;
    }

    public void setSearchParameter(String searchParameter) {
        this.searchParameter = searchParameter;
    }

    public Object getAtmMachines(UsersDTOInter loggedinUser, Integer grpId) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>) amDAO.findByAtmGrp(grpId, loggedinUser.getUserId());
        return amDTOL;
    }

    public Object getTransactionResponseCode() throws Throwable {
        TransactionResponseCodeDAOInter trcDAO = DAOFactory.createTransactionResponseCodeDAO(null);
        List<TransactionResponseCodeDTOInter> trcDTOL = (List<TransactionResponseCodeDTOInter>) trcDAO.findAll();
        return trcDTOL;
    }

    public Object getTransactionType() throws Throwable {
        TransactionTypeDAOInter ttDAO = DAOFactory.createTransactionTypeDAO(null);
        List<TransactionTypeDTOInter> ttDTOL = (List<TransactionTypeDTOInter>) ttDAO.findAll();
        return ttDTOL;
    }

    public Object getTransactionStatus() throws Throwable {
        TransactionStatusDAOInter tsDAO = DAOFactory.createTransactionStatusDAO(null);
        List<TransactionStatusDTOInter> tsDTOL = (List<TransactionStatusDTOInter>) tsDAO.findAll();
        return tsDTOL;
    }

    public Object gettexttransaction(RejectedTransactionsDTOInter mdDTO) throws Throwable {
        RejectedTransactionsDAOInter mdDAO = DAOFactory.createRejectedTransactionsDAO(null);
        String mddtoi = (String) mdDAO.Findtexttransaction(mdDTO);
        return mddtoi;
    }

    public Object FindFiletransaction(RejectedTransactionsDTOInter mdDTO) throws Throwable {
        RejectedTransactionsDAOInter mdDAO = DAOFactory.createRejectedTransactionsDAO(null);
        Clob mddtoi = (Clob) mdDAO.FindFiletransaction(mdDTO);
        return mddtoi;
    }

    public Object getCurrencyMaster() throws Throwable {
        CurrencyMasterDAOInter cmDAO = DAOFactory.createCurrencyMasterDAO(null);
        List<CurrencyMasterDTOInter> cmDTOL = (List<CurrencyMasterDTOInter>) cmDAO.findAll();
        return cmDTOL;
    }

    public Object getFileColumnDefinition() throws Throwable {
        ColumnDAOInter fcdDAO = DAOFactory.createColumnDAO(null);
        List<ColumnDTOInter> fcdDTOL = (List<ColumnDTOInter>) fcdDAO.findAll();
        return fcdDTOL;
    }

    public List<ColumnDTOInter> getColumns(List columns) throws Throwable {
        ColumnDAOInter fcdDAO = DAOFactory.createColumnDAO(null);
        List<ColumnDTOInter> fcdDTOL = new ArrayList<ColumnDTOInter>();
        /* ColumnDTOInter c = (ColumnDTOInter) fcdDAO.find(1);
         fcdDTOL.add(c);
         for (int i = 0; i < columns.size(); i++) {
        
         String columnName = (String) columns.get(i);
         ColumnDTOInter fcdDTO = (ColumnDTOInter) fcdDAO.searchByName(columnName);
         fcdDTOL.add(fcdDTO);
         }*/
        fcdDTOL = (List<ColumnDTOInter>) fcdDAO.searchByName(columns);
        return fcdDTOL;
    }

    public Object getMatchingType() throws Throwable {
        MatchingTypeDAOInter mtDAO = DAOFactory.createMatchingTypeDAO(null);
        List<MatchingTypeDTOInter> mtDTOL = (List<MatchingTypeDTOInter>) mtDAO.findComboBox();
        return mtDTOL;
    }

    public Object getAtmGroup() throws Throwable {
        AtmGroupDAOInter agDAo = DAOFactory.createAtmGroupDAO(null);
        List<AtmGroupDTOInter> agDTOL = (List<AtmGroupDTOInter>) agDAo.findTree();
        return agDTOL;
    }

    public Object getValidationRules(int validationType) throws Throwable {
        ValidationRulesDAOInter agDAo = DAOFactory.createValidationRulesDAO(null);
        List<ValidationRulesDTOInter> agDTOL = (List<ValidationRulesDTOInter>) agDAo.findByValidationType(validationType);
        return agDTOL;
    }

    public Object findReport(String fields, String whereCluase, UsersDTOInter logedUser, List<ColumnDTOInter> c, RejectedTransactionsDTOInter[] dDTOArr, String cust) throws Throwable {
        RejectedTransactionsDAOInter mdDAO = DAOFactory.createRejectedTransactionsDAO(null);
        RejectedReportDAOInter drDAO = DAOFactory.createRejectedReportDAO(null);

        for (RejectedTransactionsDTOInter d : dDTOArr) {
            RejectedReportDTOInter drDTO = DTOFactory.createRejectedReportDTO();
            drDTO.setAmount(d.getamount());
            drDTO.setAtmApplicationId(d.getatmapplicationid());
            drDTO.setAtmId(d.getatmid());
            drDTO.setCardNo(d.getcardno());
            drDTO.setColumn1(d.getcolumn1());
            drDTO.setColumn2(d.getcolumn2());
            drDTO.setColumn3(d.getcolumn3());
            drDTO.setColumn4(d.getcolumn4());
            drDTO.setColumn5(d.getcolumn5());
            drDTO.setCurrency(d.getcurrency());
            drDTO.setCurrencyId(d.getcurrencyid());
            drDTO.setCustomerAccountNumber(d.getcustomeraccountnumber());
            drDTO.setNotesPresented(d.getnotespresented());
            drDTO.setRecordType(d.getrecordtype());
            drDTO.setResponseCode(d.getresponsecode());
            drDTO.setResponseCodeId(d.getresponsecodeid());
            drDTO.setSettlementDate(d.getsettlementdate());
            drDTO.setTransactionDate(d.gettransactiondate());
            drDTO.setTransactionSeqeunce(d.gettransactionsequence());
            drDTO.setTransactionSequenceOrderBy(d.gettransactionsequenceorderby());
            drDTO.setTransactionStatus(d.gettransactionstatus());
            drDTO.setTransactionStatusId(d.gettransactionstatusid());
            drDTO.setTransactionTime(d.gettransactiontime());
            drDTO.setTransactionType(d.gettransactiontype());
            drDTO.setTransactionTypeId(d.gettransactiontypeid());
            drDTO.setUserId(logedUser.getUserId());

            drDAO.insert(drDTO);

        }
        ResultSet rs = (ResultSet) drDAO.findByUserId(logedUser.getUserId(), fields);
        ReportingTool rt = new ReportingTool(rs, (ArrayList) c, true);
        String path = rt.generate("Rejected Transactions", logedUser.getUserName(), searchParameter, true, cust, BigDecimal.ONE, "0");
        drDAO.delete(logedUser.getUserId());
          CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());
                rs.close();
        return path;
    }

    public Object searchMatchedData(Date transDateF, String opt1, Date transDateT, Date settleDateF, String opt2, Date settleDateT,
            Date loadDateF, String opt3, Date loadDateT, Integer amountF, String opt4, Integer amountT, Integer transSeqOrderByF,
            String opt5, Integer transSeqOrderByT, String cardNo, String opt6, String accountNo, String opt7, String notesPre,
            Integer transStatusCode, String transactionStatus, Integer transTypeCode, String transType, Integer responseCode,
            String response, Integer currencyCode, Integer atmId,
            String atmCode, Integer atmGroup, boolean blackList, Integer validationRule, Integer recordType,
            String sortBy, UsersDTOInter logedUser, String field, String col1, String col2, String col3, String col4, String col5, String colop1, String colop2, String colop3, String colop4, String colop5) throws Throwable {

        String whereClaus = "";
        searchParameter = "";

        if (recordType == 1) {
            searchParameter += "Master Record JR -";
        } else if (recordType == 2) {
            searchParameter += "Master Record SW -";
        } else if (recordType == 3) {
            searchParameter += "Master Record GL -";
        }

        // <editor-fold defaultstate="collapsed" desc="Transaction_date where clause">
        if (opt1.equals("Between")) {
            if (transDateF != null && transDateT != null) {
               whereClaus += " and transaction_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss') "
                        + " and transaction_date <= to_date('$col2','dd.mm.yyyy hh24:mi:ss') ";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                whereClaus = whereClaus.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(transDateT));
                searchParameter += "Transaction Date between $F and $T -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter = searchParameter.replace("$T", DateFormatter.changeDateAndTimeFormat(transDateT));
            }
        } else if (opt1.equals("<>")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date <> to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date <> $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals("<")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date < to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date < $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals("<=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date <= to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date <= $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals(">")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date > to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date > $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals(">=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date >= to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date >= $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        } else if (opt1.equals("=")) {
            if (transDateF != null) {
                whereClaus += " and transaction_date = to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(transDateF));
                searchParameter += "Transaction Date = $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(transDateF));
            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Settlement_date where clause">
        if (opt2.equals("Between")) {
            if (settleDateF != null && settleDateT != null) {
                whereClaus += " and settlement_date between to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and "
                        + "to_date(to_char(nvl(to_date('$col2','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                whereClaus = whereClaus.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(settleDateT));
                searchParameter += "Settlement Date Between $F and $T -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter = searchParameter.replace("$T", DateFormatter.changeDateAndTimeFormat(settleDateT));
            }
        } else if (opt2.equals(">")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date > to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date > $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
            }
        } else if (opt2.equals(">=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date >= to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date >= $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
            }
        } else if (opt2.equals("<")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date < to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date < $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
            }
        } else if (opt2.equals("<=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date <= to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date <= $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
            }
        } else if (opt2.equals("<>")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date <> to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date <> $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
            }
        } else if (opt2.equals("=")) {
            if (settleDateF != null) {
                whereClaus += " and settlement_date = to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                whereClaus = whereClaus.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(settleDateF));
                searchParameter += "Settlement Date = $F -";
                searchParameter = searchParameter.replace("$F", DateFormatter.changeDateAndTimeFormat(settleDateF));
            }
        }// </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Amount where clause">
        if (opt4.equals("Between")) {
            if (amountF != null && amountT != null&& amountF !=0&& amountT !=0) {
                whereClaus += " and amount between nvl($col1,0) and nvl($col2,9999999999999999)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                whereClaus = whereClaus.replace("$col2", "" + amountT);
                searchParameter += "Amount Between $F and $T -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
                searchParameter = searchParameter.replace("$T", "" + amountT);
            }
        } else if (opt4.equals(">")) {
            if (amountF != null&& amountF !=0) {
                whereClaus += " and amount > nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount > $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
            }
        } else if (opt4.equals(">=")) {
            if (amountF != null&& amountF !=0) {
                whereClaus += " and amount >= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount  >= $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
            }
        } else if (opt4.equals("<")) {
            if (amountF != null&& amountF !=0) {
                whereClaus += " and amount < nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount < $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
            }
        } else if (opt4.equals("<=")) {
            if (amountF != null&& amountF !=0) {
                whereClaus += " and amount <= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount <= $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
            }
        } else if (opt4.equals("<>")) {
            if (amountF != null&& amountF !=0) {
                whereClaus += " and amount <> nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount <> $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
            }
        } else if (opt4.equals("=")) {
            if (amountF != null&& amountF !=0) {
                whereClaus += " and amount = nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + amountF);
                searchParameter += "Amount = $F -";
                searchParameter = searchParameter.replace("$F", "" + amountF);
            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Transaction_sequence where clause">
        if (opt5.equals("Between")) {
            if (transSeqOrderByF != null && transSeqOrderByT != null&& transSeqOrderByF !=0&& transSeqOrderByT !=0) {
                whereClaus += " and transaction_sequence_order_by between nvl($col1,0) and nvl($col2,9999999999999999)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                whereClaus = whereClaus.replace("$col2", "" + transSeqOrderByT);
                searchParameter += "Sequence Between $F and $T -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
                searchParameter = searchParameter.replace("$T", "" + transSeqOrderByT);
            }
        } else if (opt5.equals(">")) {
            if (transSeqOrderByF != null&& transSeqOrderByF !=0) {
                whereClaus += " and transaction_sequence_order_by > nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence > $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
            }
        } else if (opt5.equals(">=")) {
            if (transSeqOrderByF != null&& transSeqOrderByF !=0) {
                whereClaus += " and transaction_sequence_order_by >= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence >= $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
            }
        } else if (opt5.equals("<")) {
            if (transSeqOrderByF != null&& transSeqOrderByF !=0) {
                whereClaus += " and transaction_sequence_order_by < nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence < $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
            }
        } else if (opt5.equals("<=")) {
            if (transSeqOrderByF != null&& transSeqOrderByF !=0) {
                whereClaus += " and transaction_sequence_order_by <= nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence <= $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
            }
        } else if (opt5.equals("<>")) {
            if (transSeqOrderByF != null&& transSeqOrderByF !=0) {
                whereClaus += " and transaction_sequence_order_by <> nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence <> $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
            }
        } else if (opt5.equals("=")) {
            if (transSeqOrderByF != null&& transSeqOrderByF !=0) {
                whereClaus += " and transaction_sequence_order_by = nvl($col1,0)";
                whereClaus = whereClaus.replace("$col1", "" + transSeqOrderByF);
                searchParameter += "Sequence = $F -";
                searchParameter = searchParameter.replace("$F", "" + transSeqOrderByF);
            }
        }// </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Card_no where clause">
        if (opt6.equals("=")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and card_no like '%$col1%'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo.trim());
                searchParameter += "Card No. like $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo.trim());
            }
        } else if (opt6.equals("<>")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and card_no <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo.trim());
                searchParameter += "Card No. <> $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo.trim());
            }
        } else if (opt6.equals("Like")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and card_no like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo.trim());
                searchParameter += "Card No. like $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo.trim());
            }
        } else if (opt6.equals("Not Like")) {
            if (cardNo != null && !cardNo.equals("")) {
                whereClaus += " and card_no not like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + cardNo.trim());
                searchParameter += "Card No. not like $F -";
                searchParameter = searchParameter.replace("$F", "" + cardNo.trim());
            }
        } else if (opt6.equals("is null")) {
            whereClaus += " and card_no is null ";
            whereClaus = whereClaus.replace("$col1", "" + cardNo);
            searchParameter += "Card No.  is null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo.trim());
        } else if (opt6.equals("is not null")) {
            whereClaus += " and card_no is not null ";
            whereClaus = whereClaus.replace("$col1", "" + cardNo.trim());
            searchParameter += "Card No. not null -";
            searchParameter = searchParameter.replace("$F", "" + cardNo.trim());
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Account_No where clause">
        if (opt7.equals("=")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER like '%$col1%'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo.trim());
                searchParameter += "Account No. like $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo.trim());

            }
        } else if (opt7.equals("<>")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER <> '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo.trim());
                searchParameter += "Account No. <> $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo.trim());

            }
        } else if (opt7.equals("is null")) {
            whereClaus += " and CUSTOMER_ACCOUNT_NUMBER is null";
            whereClaus = whereClaus.replace("$col1", "" + accountNo.trim());
            searchParameter += "Account No. is null";
            searchParameter = searchParameter.replace("$F", "" + accountNo.trim());

        } else if (opt7.equals("is not null")) {
            whereClaus += " and CUSTOMER_ACCOUNT_NUMBER is not null";
            whereClaus = whereClaus.replace("$col1", "" + accountNo.trim());
            searchParameter += "Account No. is not null -";
            searchParameter = searchParameter.replace("$F", "" + accountNo.trim());
        } else if (opt7.equals("Like")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER Like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo.trim());
                searchParameter += "Account No. Like $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo.trim());
            }

        } else if (opt7.equals("Not Like")) {
            if (accountNo != null && !accountNo.equals("")) {
                whereClaus += " and CUSTOMER_ACCOUNT_NUMBER not Like '$col1'";
                whereClaus = whereClaus.replace("$col1", "" + accountNo.trim());
                searchParameter += "Account No. not Like $F -";
                searchParameter = searchParameter.replace("$F", "" + accountNo.trim());
            }

        }
        // </editor-fold>
        
        String temp = "";
        temp += "  record_type = $col";
        temp = temp.replace("$col", "" + recordType);

        if (notesPre != null && !notesPre.equals("")) {
            whereClaus += " and lower(notes_presented) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + notesPre);
            searchParameter += "Notes Presnted like $F -";
            searchParameter = searchParameter.replace("$F", "" + notesPre);
        }
        if (transStatusCode != 0) {
            if (transStatusCode != -1) {
                whereClaus += " and transaction_status_id = $col1";
                whereClaus = whereClaus.replace("$col1", "" + transStatusCode);
                searchParameter += "Status: $F -";
                TransactionStatusDAOInter tsDAO = DAOFactory.createTransactionStatusDAO(null);
                TransactionStatusDTOInter tsDTO = (TransactionStatusDTOInter) tsDAO.find(transStatusCode);
                searchParameter = searchParameter.replace("$F", "" + tsDTO.getName());
            } else {
                whereClaus += " and transaction_status_id in (select id from transaction_status) ";
                whereClaus = whereClaus.replace("$col1", "" + transStatusCode);
                searchParameter += "Status: $F -";
                searchParameter = searchParameter.replace("$F", " Has Error");
            }
        }
        if (transactionStatus != null && !transactionStatus.equals("")) {
            whereClaus += " and lower(transaction_status) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + transactionStatus);
            searchParameter += "Status: $F -";
            searchParameter = searchParameter.replace("$F", "" + transactionStatus);
        }
        if (transTypeCode != 0) {
            whereClaus += " and transaction_type_id = $col1";
            whereClaus = whereClaus.replace("$col1", "" + transTypeCode);
            searchParameter += "Trans. Type: $F -";
            TransactionTypeDAOInter tsDAO = DAOFactory.createTransactionTypeDAO(null);
            TransactionTypeDTOInter tsDTO = (TransactionTypeDTOInter) tsDAO.find(transTypeCode);
            searchParameter = searchParameter.replace("$F", "" + tsDTO.getName());
        }
        if (transType != null && !transType.equals("")) {
            whereClaus += " and lower(transaction_type) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + transType);
            searchParameter += "Trans. Type: $F -";
            searchParameter = searchParameter.replace("$F", "" + transType);
        }
        if (responseCode != 0) {
            whereClaus += " and response_code_id = $col1";
            whereClaus = whereClaus.replace("$col1", "" + responseCode);
            searchParameter += "Response: $F -";
            TransactionResponseCodeDAOInter tsDAO = DAOFactory.createTransactionResponseCodeDAO(null);
            TransactionResponseCodeDTOInter tsDTO = (TransactionResponseCodeDTOInter) tsDAO.find(responseCode);
            searchParameter = searchParameter.replace("$F", "" + tsDTO.getName());
        }
        if (response != null && !response.equals("")) {
            whereClaus += " and lower(response_code) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + response);
            searchParameter += "Response: $F -";
            searchParameter = searchParameter.replace("$F", "" + response);
        }
        if (currencyCode != 0) {
            whereClaus += " and currency_id = $col1";
            whereClaus = whereClaus.replace("$col1", "" + currencyCode);
            searchParameter += "Currency: $F -";
            CurrencyMasterDAOInter tsDAO = DAOFactory.createCurrencyMasterDAO(null);
            CurrencyMasterDTOInter tsDTO = (CurrencyMasterDTOInter) tsDAO.find(currencyCode);
            searchParameter = searchParameter.replace("$F", "" + tsDTO.getSymbol());
        }
        if (atmId != null&& atmId != 0) {
            whereClaus += " and ATM_ID = $col1";
            whereClaus = whereClaus.replace("$col1", "" + atmId);
            searchParameter += "ATM ID: $F -";
            AtmMachineDAOInter x = DAOFactory.createAtmMachineDAO(null);
            AtmMachineDTOInter x1 = (AtmMachineDTOInter) x.find(atmId);
            searchParameter = searchParameter.replace("$F", "" + x1.getApplicationId());
        }
        if (atmCode != null && !atmCode.equals("")) {
            whereClaus += " and lower(ATM_APPLICATION_ID) like lower('%$col1%')";
            whereClaus = whereClaus.replace("$col1", "" + atmCode);
            searchParameter += "ATM name: $F -";
            searchParameter = searchParameter.replace("$F", "" + atmCode);
        }
        if (atmGroup != 0) {
           whereClaus += " and ATM_ID IN (select id from atm_machine where atm_group in (select id from atm_group where parent_id in (select id from atm_group where (parent_id = $col1 or id = $col1)) or id = $col1))";
            whereClaus = whereClaus.replace("$col1", "" + atmGroup);
            AtmGroupDAOInter agDAO = DAOFactory.createAtmGroupDAO(null);
            AtmGroupDTOInter agDTO = (AtmGroupDTOInter) agDAO.find(atmGroup);
            searchParameter += "ATM Group: $F -";
            searchParameter = searchParameter.replace("$F", "" + agDTO.getName());
        }
        if (blackList) {
            whereClaus += " and card_no in (select card_no from black_listed_card)";
            searchParameter += "Black Listed Card -";
        }
        if (validationRule != 0) {
            if (recordType == 1) {
                whereClaus += " and exists (select 1 from rejected_journal_rules where rejected_journal_rules.transaction_date = rejected_transactions.transaction_date and rejected_journal_rules.rejected_journal_key = rejected_transactions.rejected_key and rejected_journal_rules.rule_id = $rule) ";
                whereClaus = whereClaus.replace("$rule", "" + validationRule);
            } else if (recordType == 2) {

                whereClaus += " and exists (select 1 from rejected_switch_rules where rejected_switch_rules.transaction_date = rejected_transactions.transaction_date and rejected_switch_rules.rejected_switch_key = rejected_transactions.rejected_key and rejected_switch_rules.rule_id = $rule) ";
                whereClaus = whereClaus.replace("$rule", "" + validationRule);

            } else if (recordType == 3) {
                whereClaus += " and exists (select 1 from rejected_host_rules where rejected_host_rules.transaction_date = rejected_transactions.transaction_date and rejected_host_rules.rejected_host_key = rejected_transactions.rejected_key and rejected_host_rules.rule_id = $rule)  ";
                whereClaus = whereClaus.replace("$rule", "" + validationRule);
            }
        }

        whereClaus += " and atm_valid(atm_id) = 1 ";
        whereClaus += " and Exists (select 1 from user_atm s where s.user_id = $user and s.atm_id = ATM_ID)";
        whereClaus = whereClaus.replace("$user", "" + logedUser.getUserId());

        if (sortBy != null && !sortBy.equals("")) {
            whereClaus += " order by ATM_APPLICATION_ID,transaction_date, transaction_sequence_order_by";
            whereClaus = whereClaus.replace("$col1", "" + sortBy);
        } else {
            whereClaus += " order by ATM_APPLICATION_ID,transaction_date, transaction_sequence_order_by";
        }

        this.whereCluase = whereClaus;
        this.whereCluase = this.whereCluase.replaceAll("and record_type = [0-9]", "");
        RejectedTransactionsDAOInter mdDAO = DAOFactory.createRejectedTransactionsDAO(null);

        List<RejectedTransactionsDTOInter> mdDTOL = (List<RejectedTransactionsDTOInter>) mdDAO.customSearch(whereClaus, field, temp);

        return mdDTOL;
    }

    public Object markasdisp(RejectedTransactionsDTOInter[] rtDTOArr,String RecordType) throws Throwable {
        RejectedTransactionsDAOInter rtDAO = DAOFactory.createRejectedTransactionsDAO(null);
        rtDAO.markasdisp(rtDTOArr,RecordType);
        return "Done";
    }
    public Object revalidat(RejectedTransactionsDTOInter[] rtDTOArr) throws Throwable {
        RejectedTransactionsDAOInter rtDAO = DAOFactory.createRejectedTransactionsDAO(null);
        rtDAO.revalidateRecord(rtDTOArr);
        return "Done";
    }

    public Object getPickColumn() throws Throwable {
        Session utillist = new Session();
        DualListModel<String> pick = new DualListModel();
        ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);
        List<ColumnDTOInter> cDDTO = (List<ColumnDTOInter>) utillist.GetDefaultColumn();
        List<ColumnDTOInter> cNDDTO = (List<ColumnDTOInter>) utillist.GetNonDefaultColumn();

        List<String> source = new ArrayList<String>();
        List<String> target = new ArrayList<String>();

        for (ColumnDTOInter c : cDDTO) {
            // if (c.getId() != 1) {
            target.add(c.getHeader());
            // }
        }
        for (ColumnDTOInter c : cNDDTO) {
            // if (c.getId() != 1) {
            source.add(c.getHeader());
            // }
        }

        pick.setSource(source);
        pick.setTarget(target);

        return pick;

    }
}
