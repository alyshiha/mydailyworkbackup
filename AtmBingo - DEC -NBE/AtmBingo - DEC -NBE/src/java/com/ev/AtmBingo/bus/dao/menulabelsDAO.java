package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.ev.AtmBingo.bus.dto.menulabelsDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

public class menulabelsDAO extends BaseDAO implements menulabelsDAOInter {

    protected menulabelsDAO() {
        super();
        super.setTableName("MENU_LABELS");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        menulabelsDTOInter RecordToInsert = (menulabelsDTOInter) obj[0];

        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
//RecordToInsert.setid(super.generateSequence(super.getTableName()));
            String insertStat = "insert into $table"
                    + " (MENU_ID,ARABIC_LABEL,ENGLISH_LABEL,TYPE,PARENT_ID,FORM_ID,REPORT_ID,SEQ,VISABILITY,PAGE,RESTRECTED) "
                    + " values "
                    + " ($menuid,'$arabiclabel','$englishlabel',$type,$parentid,$formid,$reportid,$seq,$visability,'$page',$restrected)";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$menuid", "" + RecordToInsert.getmenuid());
            insertStat = insertStat.replace("$arabiclabel", "" + RecordToInsert.getarabiclabel());
            insertStat = insertStat.replace("$englishlabel", "" + RecordToInsert.getenglishlabel());
            insertStat = insertStat.replace("$type", "" + RecordToInsert.gettype());
            insertStat = insertStat.replace("$parentid", "" + RecordToInsert.getparentid());
            insertStat = insertStat.replace("$formid", "" + RecordToInsert.getformid());
            insertStat = insertStat.replace("$reportid", "" + RecordToInsert.getreportid());
            insertStat = insertStat.replace("$seq", "" + RecordToInsert.getseq());
            insertStat = insertStat.replace("$visability", "" + RecordToInsert.getvisability());
            insertStat = insertStat.replace("$page", "" + RecordToInsert.getpage());
            insertStat = insertStat.replace("$restrected", "" + RecordToInsert.getrestrected());
            msg = super.executeUpdate(insertStat).toString();
            msg = msg + " Row Has Been Inserted";
            super.postUpdate("Add New Record To MENU_LABELS", false);
        }
        return msg;
    }

    public String ValidateNull(Object... obj) {
        menulabelsDTOInter RecordToInsert = (menulabelsDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getmenuid() == 0) {
            Validate = Validate + "   +menuid  ";
        }
        if (RecordToInsert.getarabiclabel() == null && RecordToInsert.getarabiclabel().equals("")) {
            Validate = Validate + "   +arabiclabel  ";
        }
        if (RecordToInsert.getenglishlabel() == null && RecordToInsert.getenglishlabel().equals("")) {
            Validate = Validate + "   +englishlabel  ";
        }
        if (RecordToInsert.gettype() == 0) {
            Validate = Validate + "   +type  ";
        }
        if (RecordToInsert.getvisability() == 0) {
            Validate = Validate + "   +visability  ";
        }
        if (RecordToInsert.getrestrected() == 0) {
            Validate = Validate + "   +restrected  ";
        }
        if (!Validate.equals("Validate")) {
            Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        menulabelsDTOInter RecordToUpdate = (menulabelsDTOInter) obj[0];
        String msg = ValidateNull(RecordToUpdate);
        if (msg.equals("Validate")) {
            String UpdateStat = "update $table set "
                    + " (MENU_ID= $menuid,ARABIC_LABEL= '$arabiclabel',ENGLISH_LABEL= '$englishlabel',TYPE= $type,PARENT_ID= $parentid,FORM_ID= $formid,REPORT_ID= $reportid,SEQ= $seq,VISABILITY= $visability,PAGE= '$page',RESTRECTED= $restrected) "
                    + "  where  MENU_ID= $menuid";
            UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
            UpdateStat = UpdateStat.replace("$menuid", "" + RecordToUpdate.getmenuid());
            UpdateStat = UpdateStat.replace("$arabiclabel", "" + RecordToUpdate.getarabiclabel());
            UpdateStat = UpdateStat.replace("$englishlabel", "" + RecordToUpdate.getenglishlabel());
            UpdateStat = UpdateStat.replace("$type", "" + RecordToUpdate.gettype());
            UpdateStat = UpdateStat.replace("$parentid", "" + RecordToUpdate.getparentid());
            UpdateStat = UpdateStat.replace("$formid", "" + RecordToUpdate.getformid());
            UpdateStat = UpdateStat.replace("$reportid", "" + RecordToUpdate.getreportid());
            UpdateStat = UpdateStat.replace("$seq", "" + RecordToUpdate.getseq());
            UpdateStat = UpdateStat.replace("$visability", "" + RecordToUpdate.getvisability());
            UpdateStat = UpdateStat.replace("$page", "" + RecordToUpdate.getpage());
            UpdateStat = UpdateStat.replace("$restrected", "" + RecordToUpdate.getrestrected());
            msg =  super.executeUpdate(UpdateStat).toString();
            super.postUpdate("Update Record In Table MENU_LABELS", false);
        }
        ;
        return msg;
    }
   public Boolean findmenuitemcheck(int user, String Page) throws Throwable {

        super.preSelect();
        String selectStat = "select m.page from menu_labels m,user_profile p,profile_menu s where p.user_id = '$UserID' and LOWER(m.page) = LOWER('$Page') and s.profile_id = p.profile_id and s.menu_id = m.menu_id and m.page is not null";
        selectStat = selectStat.replace("$UserID", "" + user);
        selectStat = selectStat.replace("$Page", "" + Page);
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> uDTOL = new ArrayList<menulabelsDTOInter>();
        Boolean found = Boolean.FALSE;
        if (rs.next()) {
            found = Boolean.TRUE;
        }
        super.postSelect(rs);
        return found;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        menulabelsDTOInter RecordToDelete = (menulabelsDTOInter) obj[0];
        String msg = ValidateNull(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  MENU_ID= $menuid";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$menuid", "" + RecordToDelete.getmenuid());

            msg = super.executeUpdate(deleteStat).toString();
            super.postUpdate("Delete Record In Table MENU_LABELS", false);
        }
        return msg + " Record Has Been Deleted";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table MENU_LABELS", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        Integer RecordToSelect = (Integer) obj[0];
        String selectStat = "Select MENU_ID,ARABIC_LABEL,ENGLISH_LABEL,TYPE,PARENT_ID,FORM_ID,REPORT_ID,SEQ,VISABILITY,PAGE,RESTRECTED From $table"
                + "  where  MENU_ID= $menuid ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$menuid", "" + RecordToSelect);
        ResultSet rs = executeQuery(selectStat);
        menulabelsDTOInter SelectedRecord = DTOFactory.createmenulabelsDTO();
        while (rs.next()) {
            SelectedRecord.setmenuid(rs.getInt("MENU_ID"));
            SelectedRecord.setarabiclabel(rs.getString("ARABIC_LABEL"));
            SelectedRecord.setenglishlabel(rs.getString("ENGLISH_LABEL"));
            SelectedRecord.settype(rs.getInt("TYPE"));
            SelectedRecord.setparentid(rs.getInt("PARENT_ID"));
            SelectedRecord.setformid(rs.getInt("FORM_ID"));
            SelectedRecord.setreportid(rs.getInt("REPORT_ID"));
            SelectedRecord.setseq(rs.getInt("SEQ"));
            SelectedRecord.setvisability(rs.getInt("VISABILITY"));
            SelectedRecord.setpage(rs.getString("PAGE"));
            SelectedRecord.setrestrected(rs.getInt("RESTRECTED"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findParentRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select MENU_ID,PAGE,ENGLISH_LABEL From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> Records = new ArrayList<menulabelsDTOInter>();
        while (rs.next()) {
            menulabelsDTOInter SelectedRecord = DTOFactory.createmenulabelsDTO();
            SelectedRecord.setmenuid(rs.getInt("MENU_ID"));
            SelectedRecord.setpage(rs.getString("PAGE"));
            SelectedRecord.setenglishlabel(rs.getString("ENGLISH_LABEL"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findPageRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select MENU_ID,PAGE,ENGLISH_LABEL From $table where PAGE is not null";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> Records = new ArrayList<menulabelsDTOInter>();
        while (rs.next()) {
            menulabelsDTOInter SelectedRecord = DTOFactory.createmenulabelsDTO();
            SelectedRecord.setmenuid(rs.getInt("MENU_ID"));
            SelectedRecord.setpage(rs.getString("PAGE"));
            SelectedRecord.setenglishlabel(rs.getString("ENGLISH_LABEL"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        Integer RecordToSelect = (Integer) obj[0];
        String selectStat = "Select MENU_ID,ARABIC_LABEL,ENGLISH_LABEL,TYPE,PARENT_ID,FORM_ID,REPORT_ID,SEQ,VISABILITY,PAGE,RESTRECTED From $table  where  MENU_ID in (Select MENU_ID From PROFILE_MENU  where  PROFILE_ID= $menuid)";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$menuid", "" + RecordToSelect);
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> Records = new ArrayList<menulabelsDTOInter>();
        while (rs.next()) {
            menulabelsDTOInter SelectedRecord = DTOFactory.createmenulabelsDTO();
            SelectedRecord.setmenuid(rs.getInt("MENU_ID"));
            SelectedRecord.setarabiclabel(rs.getString("ARABIC_LABEL"));
            SelectedRecord.setenglishlabel(rs.getString("ENGLISH_LABEL"));
            SelectedRecord.settype(rs.getInt("TYPE"));
            SelectedRecord.setparentid(rs.getInt("PARENT_ID"));
            SelectedRecord.setformid(rs.getInt("FORM_ID"));
            SelectedRecord.setreportid(rs.getInt("REPORT_ID"));
            SelectedRecord.setseq(rs.getInt("SEQ"));
            SelectedRecord.setvisability(rs.getInt("VISABILITY"));
            SelectedRecord.setpage(rs.getString("PAGE"));
            SelectedRecord.setrestrected(rs.getInt("RESTRECTED"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select MENU_ID,ARABIC_LABEL,ENGLISH_LABEL,TYPE,PARENT_ID,FORM_ID,REPORT_ID,SEQ,VISABILITY,PAGE,RESTRECTED From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> Records = new ArrayList<menulabelsDTOInter>();
        while (rs.next()) {
            menulabelsDTOInter SelectedRecord = DTOFactory.createmenulabelsDTO();
            SelectedRecord.setmenuid(rs.getInt("MENU_ID"));
            SelectedRecord.setarabiclabel(rs.getString("ARABIC_LABEL"));
            SelectedRecord.setenglishlabel(rs.getString("ENGLISH_LABEL"));
            SelectedRecord.settype(rs.getInt("TYPE"));
            SelectedRecord.setparentid(rs.getInt("PARENT_ID"));
            SelectedRecord.setformid(rs.getInt("FORM_ID"));
            SelectedRecord.setreportid(rs.getInt("REPORT_ID"));
            SelectedRecord.setseq(rs.getInt("SEQ"));
            SelectedRecord.setvisability(rs.getInt("VISABILITY"));
            SelectedRecord.setpage(rs.getString("PAGE"));
            SelectedRecord.setrestrected(rs.getInt("RESTRECTED"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findmenu(int user) throws Throwable {

        super.preSelect();
        String selectStat = "select t.menu_id MenuID, t.page PageLink,t.english_label EngLabel, t.parent_id  ParentID, level LevelID from (select m.* from  menu_labels  m, profile_menu p where visability in (1,3) and m.menu_id = p.menu_id and p.profile_id = (select profile_id from user_profile where user_id = $UserID)order BY parent_id,seq nulls last ,english_label) t connect by prior  t.menu_id  = t.parent_id start with t.parent_id is null ORDER SIBLINGS BY seq nulls last , english_label";
        selectStat = selectStat.replace("$UserID", "" + user);
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> uDTOL = new ArrayList<menulabelsDTOInter>();

        while (rs.next()) {

            menulabelsDTOInter uDTO = DTOFactory.createmenulabelsDTO();
            uDTO.setmenuid(rs.getInt("MenuID"));
            uDTO.setparentid(rs.getInt("ParentID"));
            uDTO.setpage(rs.getString("PageLink"));
            uDTO.setenglishlabel(rs.getString("EngLabel"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findParents() throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table m where parent_id is null and VISABILITY in(1,3) and exists(select 1 from profile_menu where  menu_id = m.menu_id) order by seq";
        selectStat = selectStat.replace("$table", "" + super.getTableName());

        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> uDTOL = new ArrayList<menulabelsDTOInter>();
        while (rs.next()) {
            menulabelsDTOInter uDTO = DTOFactory.createmenulabelsDTO();
            uDTO.setarabiclabel(rs.getString("arabic_label"));
            uDTO.setenglishlabel(rs.getString("english_label"));
            uDTO.setmenuid(rs.getInt("menu_id"));
            uDTO.setparentid(rs.getInt("parent_id"));
            uDTO.setpage(rs.getString("page"));
            uDTO.setrestrected(rs.getInt("restrected"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findParents(int profileId) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table m where parent_id is null and VISABILITY in(1,3) and exists(select 1 from profile_menu where profile_id = $prof and menu_id = m.menu_id) order by seq";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$prof", "" + profileId);
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> uDTOL = new ArrayList<menulabelsDTOInter>();
        while (rs.next()) {
            menulabelsDTOInter uDTO = DTOFactory.createmenulabelsDTO();
            uDTO.setarabiclabel(rs.getString("arabic_label"));
            uDTO.setenglishlabel(rs.getString("english_label"));
            uDTO.setmenuid(rs.getInt("menu_id"));
            uDTO.setparentid(rs.getInt("parent_id"));
            uDTO.setpage(rs.getString("page"));
            uDTO.setrestrected(rs.getInt("restrected"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<menulabelsDTOInter> entities = (List<menulabelsDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Users Found";
        } else {
            String validate = "Validate";
            if (validate.equals("Validate")) {
                String insertStat = "update menu_labels set arabic_label = ?, english_label = ?, "
                        + "type = ?,form_id = ?,report_id = ?,seq = ?, visability = ?, page = ?, "
                        + "restrected = ? ,parent_id = ? where menu_id = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
     connection  = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        menulabelsDTOInter RecordtoInsert = entities.get(i);

                        statement.setString(1, RecordtoInsert.getenglishlabel());
                        statement.setString(2, RecordtoInsert.getenglishlabel());
                        statement.setInt(3, RecordtoInsert.gettype());
                        statement.setInt(4, RecordtoInsert.getformid());
                        statement.setInt(5, RecordtoInsert.getreportid());
                        statement.setInt(6, RecordtoInsert.getseq());
                        statement.setInt(7, RecordtoInsert.getvisability());
                        statement.setString(8, RecordtoInsert.getpage());
                        statement.setInt(9, RecordtoInsert.getrestrected());
                        if (RecordtoInsert.getparentid() != 0) {
                            statement.setInt(10, RecordtoInsert.getparentid());
                        } else {
                            statement.setString(10,"" );
                        }
                        statement.setInt(11, RecordtoInsert.getmenuid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }

                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                            CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
