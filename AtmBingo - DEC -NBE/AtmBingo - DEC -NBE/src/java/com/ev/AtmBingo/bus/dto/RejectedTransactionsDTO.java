/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author administrator
 */
public class RejectedTransactionsDTO extends BaseDTO implements RejectedTransactionsDTOInter  , Serializable{

  


    private int fileid;
    private Date loadingdate;
    private String atmapplicationid;
    private AtmMachineDTOInter atmid;
    private String transactiontype;
    private TransactionTypeDTOInter transactiontypeid;
    private String currency;
    private CurrencyMasterDTOInter currencyid;
    private String transactionstatus;
    private TransactionStatusDTOInter transactionstatusid;
    private String responsecode;
    private TransactionResponseCodeDTOInter responsecodeid;
    private Date transactiondate;
    private String transactionsequence;
    private String cardno;
    private int amount;
    private int blacklist;
    private Date settlementdate;
    private String notespresented;
    private String customeraccountnumber;
    private int transactionsequenceorderby;
    private Date transactiontime;
    private int recordtype;
    private String column1;
    private String column2;
    private String column3;
    private String column4;
    private String column5;
    private String rownum;
private String rejectedreason;
private String rejkey,atmname;
 public String getatmname() {
        return atmname;
    }
 private int exportuser;
private int correctiveuser;
    private Date correctivedate;
    private Date exportdate;
    public Date getCorrectivedate() {
        return correctivedate;
    }
    public int getExportuser() {
        return exportuser;
    }

    public void setExportuser(int exportuser) {
        this.exportuser = exportuser;
    }

    public int getCorrectiveuser() {
        return correctiveuser;
    }

    public void setCorrectiveuser(int correctiveuser) {
        this.correctiveuser = correctiveuser;
    }
    
    public void setCorrectivedate(Date correctivedate) {
        this.correctivedate = correctivedate;
    }

    public Date getExportdate() {
        return exportdate;
    }
    public void setExportdate(Date exportdate) {
        this.exportdate = exportdate;
    }
    public void setatmname(String atmname) {
        this.atmname = atmname;
    }
    public String getRejkey() {
        return rejkey;
    }

    public void setRejkey(String rejkey) {
        this.rejkey = rejkey;
    }

    public int getBlacklist() {
        return blacklist;
    }

    public void setBlacklist(int blacklist) {
        this.blacklist = blacklist;
    }

    public int getamount() {
        return amount;
    }

    public void setamount(int amount) {
        this.amount = amount;
    }

    public String getcolumn1() {
        return column1;
    }

    public void setcolumn1(String column1) {
        this.column1 = column1;
    }

    public String getcolumn2() {
        return column2;
    }

    public void setcolumn2(String column2) {
        this.column2 = column2;
    }

    public String getcolumn3() {
        return column3;
    }

    public void setcolumn3(String column3) {
        this.column3 = column3;
    }

    public String getcolumn4() {
        return column4;
    }

    public void setcolumn4(String column4) {
        this.column4 = column4;
    }

    public String getcolumn5() {
        return column5;
    }

    public void setcolumn5(String column5) {
        this.column5 = column5;
    }

    public String getcurrency() {
        return currency;
    }

    public void setcurrency(String currency) {
        this.currency = currency;
    }

    public String getrejectedreason() {
        return rejectedreason;
    }

    public void setrejectedreason(String rejectedreason) {
        this.rejectedreason = rejectedreason;
    }

    public String getrownum() {
        return rownum;
    }

    public void setrownum(String rownum) {
        this.rownum = rownum;
    }

    public Date getsettlementdate() {
        return settlementdate;
    }

    public void setsettlementdate(Date settlementdate) {
        this.settlementdate = settlementdate;
    }

 
    protected RejectedTransactionsDTO() {
        super();
    }

    public String getatmapplicationid() {
        return atmapplicationid;
    }

    public void setatmapplicationid(String atmapplicationid) {
        this.atmapplicationid = atmapplicationid;
    }

    public AtmMachineDTOInter getatmid() {
        return atmid;
    }

    public void setatmid(AtmMachineDTOInter atmid) {
        this.atmid = atmid;
    }

    public String getcardno() {
        return cardno;
    }

    public void setcardno(String cardno) {
        this.cardno = cardno;
    }

    public CurrencyMasterDTOInter getcurrencyid() {
        return currencyid;
    }

    public void setcurrencyid(CurrencyMasterDTOInter currencyid) {
        this.currencyid = currencyid;
    }

    public String getcustomeraccountnumber() {
        return customeraccountnumber;
    }

    public void setcustomeraccountnumber(String customeraccountnumber) {
        this.customeraccountnumber = customeraccountnumber;
    }

    public int getfileid() {
        return fileid;
    }

    public void setfileid(int fileid) {
        this.fileid = fileid;
    }

    public Date getloadingdate() {
        return loadingdate;
    }

    public void setloadingdate(Date loadingdate) {
        this.loadingdate = loadingdate;
    }

    public String getnotespresented() {
        return notespresented;
    }

    public void setnotespresented(String notespresented) {
        this.notespresented = notespresented;
    }

    public int getrecordtype() {
        return recordtype;
    }

    public void setrecordtype(int recordtype) {
        this.recordtype = recordtype;
    }

    public String getresponsecode() {
        return responsecode;
    }

    public void setresponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

    public TransactionResponseCodeDTOInter getresponsecodeid() {
        return responsecodeid;
    }

    public void setresponsecodeid(TransactionResponseCodeDTOInter responsecodeid) {
        this.responsecodeid = responsecodeid;
    }

    public Date gettransactiondate() {
        return transactiondate;
    }

    public void settransactiondate(Date transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String gettransactionsequence() {
        return transactionsequence;
    }

    public void settransactionsequence(String transactionsequence) {
        this.transactionsequence = transactionsequence;
    }

   

    public int gettransactionsequenceorderby() {
        return transactionsequenceorderby;
    }

    public void settransactionsequenceorderby(int transactionsequenceorderby) {
        this.transactionsequenceorderby = transactionsequenceorderby;
    }

    public String gettransactionstatus() {
        return transactionstatus;
    }

    public void settransactionstatus(String transactionstatus) {
        this.transactionstatus = transactionstatus;
    }

    public TransactionStatusDTOInter gettransactionstatusid() {
        return transactionstatusid;
    }

    public void settransactionstatusid(TransactionStatusDTOInter transactionstatusid) {
        this.transactionstatusid = transactionstatusid;
    }

    public Date gettransactiontime() {
        return transactiontime;
    }

    public void settransactiontime(Date transactiontime) {
        this.transactiontime = transactiontime;
    }

    public String gettransactiontype() {
        return transactiontype;
    }

    public void settransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public TransactionTypeDTOInter gettransactiontypeid() {
        return transactiontypeid;
    }

    public void settransactiontypeid(TransactionTypeDTOInter transactiontypeid) {
        this.transactiontypeid = transactiontypeid;
    }

}
