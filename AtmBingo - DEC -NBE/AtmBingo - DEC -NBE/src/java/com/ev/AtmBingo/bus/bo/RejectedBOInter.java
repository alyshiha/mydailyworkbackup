/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.RejectedTransactionsDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface RejectedBOInter extends BaseBOInter {

    Object findReport(String fields, String whereCluase, UsersDTOInter logedUser, List<ColumnDTOInter> c,RejectedTransactionsDTOInter[] dDTOArr,String cust) throws Throwable ;

    Object getAtmGroup() throws Throwable;
Object gettexttransaction(RejectedTransactionsDTOInter mdDTO) throws Throwable;
  Object FindFiletransaction(RejectedTransactionsDTOInter mdDTO) throws Throwable ;
    Object getAtmMachines(UsersDTOInter loggedinUser,Integer grpId) throws Throwable;

    List<ColumnDTOInter> getColumns(List columns) throws Throwable;

    Object getCurrencyMaster() throws Throwable;

    Object getFileColumnDefinition() throws Throwable;

    Object getMatchingType() throws Throwable;

    String getSearchParameter();

    Object getTransactionResponseCode() throws Throwable;

    Object getTransactionStatus() throws Throwable;

    Object getTransactionType() throws Throwable;

    Object getValidationRules(int validationType) throws Throwable;

    String getWhereCluase();

    Object revalidat(RejectedTransactionsDTOInter[] rtDTOArr) throws Throwable;
    Object markasdisp(RejectedTransactionsDTOInter[] rtDTOArr,String RecordType) throws Throwable;

    Object searchMatchedData(Date transDateF, String opt1, Date transDateT, Date settleDateF, String opt2, Date settleDateT,
            Date loadDateF, String opt3, Date loadDateT, Integer amountF, String opt4, Integer amountT, Integer transSeqOrderByF,
            String opt5, Integer transSeqOrderByT, String cardNo, String opt6, String accountNo, String opt7, String notesPre,
            Integer transStatusCode, String transactionStatus, Integer transTypeCode, String transType, Integer responseCode,
            String response, Integer currencyCode, Integer atmId,
            String atmCode, Integer atmGroup, boolean blackList, Integer validationRule, Integer recordType,
            String sortBy, UsersDTOInter logedUser, String field,String col1,String col2,String col3,String col4,String col5,String colop1,String colop2,String colop3,String colop4,String colop5) throws Throwable;

    void setSearchParameter(String searchParameter);

    Object getPickColumn()throws Throwable;

    void setWhereCluase(String whereCluase);

}
