/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class ValidationRulesDTO extends BaseDTO implements ValidationRulesDTOInter, Serializable{

    private int id;
    private int validationType;
    private int columnId;
    private String name;
    private String formula;
    private String formulaCode;
    private int active;
    private Boolean activeB;

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public Boolean getActiveB() {
        activeB = active == 1;
        return activeB;
    }

    public void setActiveB(Boolean activeB) {
        if(activeB)
            active = 1;
        else
            active = 2;
        this.activeB = activeB;
    }

    public int getColumnId() {
        return columnId;
    }

    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public String getFormulaCode() {
        return formulaCode;
    }

    public void setFormulaCode(String formulaCode) {
        this.formulaCode = formulaCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValidationType() {
        return validationType;
    }

    public void setValidationType(int validationType) {
        this.validationType = validationType;
    }

    protected ValidationRulesDTO() {
        super();
    }
}
