/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author KhAiReE
 */
public interface SwGlDifferenceRepBOInter extends Serializable {

    Object print(String user, String customer, Date dateF, Date dateT) throws Throwable;
Object printExcel(String dateF , String dateT) throws Throwable ;

}
