/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.RepTransDetailDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentDetailDTOInter;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface ReplanishmentDetailDAOInter {

    List<RepTransDetailDTOInter> findTransDetail(String ATM, Date FromDate, Date ToDate) throws Throwable;

    Object findById(int id) throws Throwable;

    void DeleteRep(List<ReplanishmentDetailDTOInter> entities) throws SQLException;

    Object delete(Object... obj) throws Throwable;

    Object find(Object... obj) throws Throwable;

    Object findAll() throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

    void UpdateRep(List<ReplanishmentDetailDTOInter> entities, Integer id) throws SQLException;

}
