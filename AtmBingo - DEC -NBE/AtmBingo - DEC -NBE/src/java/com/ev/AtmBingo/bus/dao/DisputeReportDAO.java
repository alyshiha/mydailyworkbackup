/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;

import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.DisputeReportDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class DisputeReportDAO extends BaseDAO implements DisputeReportDAOInter {

    protected DisputeReportDAO() {
        super();
        super.setTableName("disputes_report");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DisputeReportDTOInter uDTO = (DisputeReportDTOInter) obj[0];
        String insertStat = "insert into $table values ('$atm_application_id', $atm_id"
                + ", '$transaction_type', $trnsaction_type_id, '$currency', $crrency_id, '$transaction_status', $transactin_status_id"
                + ", '$response_code', $respons_code_id, to_date('$transaction_date','dd.MM.yyyy hh24:mi:ss'), '$transaction_sequence', '$card_no'"
                + ", $amount, to_date('$settlement_date','dd.MM.yyyy hh24:mi:ss'), '$notes_presented', '$customer_account_number', $trans_sequence_order_by"
                + ", to_date('$transaction_time','dd.MM.yyyy hh24:mi:ss'), $matching_type, $record_type, $dspute_key, '$dispte_reason' ,$settle_flag, to_date('$settled_date','dd.MM.yyyy hh24:mi:ss'), $sttled_user"
                + ", '$comments', $dipute_by, to_date('$dispue_date','dd.MM.yyyy hh24:mi:ss'), $dispute_with_roles,'$column1', '$column2', '$column3', '$column4', '$column5', $t_type_master"
                + ", $r_code_master, $card_n_suffix,$user_id)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$dispute_with_roles", "" + uDTO.getDisputeWithRoles());

        insertStat = insertStat.replace("$atm_application_id", "" + uDTO.getAtmApplicationId());

        insertStat = insertStat.replace("$atm_id", "" + uDTO.getAtmId().getId());

        insertStat = insertStat.replace("$trnsaction_type_id", "" + uDTO.getTransactionTypeId().getId());
        insertStat = insertStat.replace("$transaction_type", "" + uDTO.getTransactionType());

        insertStat = insertStat.replace("$currency", "" + uDTO.getCurrency());
        insertStat = insertStat.replace("$crrency_id", "" + uDTO.getCurrencyId().getId());

        insertStat = insertStat.replace("$transactin_status_id", "" + uDTO.getTransactionStatusId().getId());
        insertStat = insertStat.replace("$transaction_status", "" + uDTO.getTransactionStatus());

        insertStat = insertStat.replace("$response_code", "" + uDTO.getResponseCode());

        insertStat = insertStat.replace("$respons_code_id", "" + uDTO.getResponseCodeId().getId());
        insertStat = insertStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactionDate()));

        insertStat = insertStat.replace("$transaction_sequence", "" + uDTO.getTransactionSeqeunce());

        insertStat = insertStat.replace("$card_no", "" + uDTO.getCardNo());

        insertStat = insertStat.replace("$amount", "" + uDTO.getAmount());
        insertStat = insertStat.replace("$settlement_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getSettlementDate()));
        insertStat = insertStat.replace("$notes_presented", "" + uDTO.getNotesPresented());

        if (uDTO.getCustomerAccountNumber() != null) {
            insertStat = insertStat.replace("$customer_account_number", "" + uDTO.getCustomerAccountNumber());
        } else {
            insertStat = insertStat.replace("'$customer_account_number'", "null");
        }

        insertStat = insertStat.replace("$trans_sequence_order_by", "" + uDTO.getTransactionSequenceOrderBy());
        insertStat = insertStat.replace("$transaction_time", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactionTime()));
        insertStat = insertStat.replace("$matching_type", "" + uDTO.getMatchingType());
        insertStat = insertStat.replace("$record_type", "" + uDTO.getRecordType());
        insertStat = insertStat.replace("$dspute_key", "" + uDTO.getDisputeKey());
        insertStat = insertStat.replace("$dispte_reason", "" + uDTO.getDisputeReason());
        insertStat = insertStat.replace("$settle_flag", "" + uDTO.getSettledFlag());
        insertStat = insertStat.replace("$settled_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getSettledDate()));
        insertStat = insertStat.replace("$sttled_user", "" + uDTO.getSettledUser().getUserId());

        if (uDTO.getComments() != null) {
            insertStat = insertStat.replace("$comments", "" + uDTO.getComments());
        } else {
            insertStat = insertStat.replace("'$comments'", "null");
        }
        insertStat = insertStat.replace("$dipute_by", "" + uDTO.getDisputeBy());
        insertStat = insertStat.replace("$dispue_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getDisputeDate()));

        if (uDTO.getColumn1() != null) {
            insertStat = insertStat.replace("$column1", "" + uDTO.getColumn1());
        } else {
            insertStat = insertStat.replace("'$column1'", "" + uDTO.getColumn1());
        }

        if (uDTO.getColumn2() != null) {
            insertStat = insertStat.replace("$column2", "" + uDTO.getColumn2());
        } else {
            insertStat = insertStat.replace("'$column2'", "" + uDTO.getColumn2());
        }

        if (uDTO.getColumn3() != null) {
            insertStat = insertStat.replace("$column3", "" + uDTO.getColumn3());
        } else {
            insertStat = insertStat.replace("'$column3'", "" + uDTO.getColumn3());
        }

        if (uDTO.getColumn4() != null) {
            insertStat = insertStat.replace("$column4", "" + uDTO.getColumn4());
        } else {
            insertStat = insertStat.replace("'$column4'", "" + uDTO.getColumn4());
        }

        if (uDTO.getColumn5() != null) {
            insertStat = insertStat.replace("$column5", "" + uDTO.getColumn5());
        } else {
            insertStat = insertStat.replace("'$column5'", "" + uDTO.getColumn5());
        }

        insertStat = insertStat.replace("$t_type_master", "" + uDTO.getTransactionTypeMaster());
        insertStat = insertStat.replace("$r_code_master", "" + uDTO.getResponseCodeMaster());
        insertStat = insertStat.replace("$card_n_suffix", "" + uDTO.getCardNoSuffix());
        insertStat = insertStat.replace("$user_id", "" + uDTO.getUserId());

        super.executeUpdateReport(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(int userId) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String deleteStat = "delete from $table where user_id = $userId";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$userId", "" + userId);

        super.executeUpdateReport(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object findByUserId(int userId, String fields, Integer mtInt) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        fields = fields.replace("CURRENCY", "");
        fields = fields.replace(", ,", ",");
        fields = fields.replace(",,", ",");
        fields = fields.replace("CARD_NO", "CARD_NO CARD_NO");
        String selectStat = "select decode(record_type,1,'JR',2,'SW',3,'GL')record_type,"
                + "decode(settled_flag,1,'Y',2,'N')settled_flag,"
                + " $field"
                + ",decode(comments,'',' ','null',' ',comments)comments,"
                + " atm_application_id,"
                + "  currency,"
                + " (select unit_number from atm_machine m where m.application_id = atm_application_id)unit_id,"
                + " (select name from atm_machine m where m.application_id = atm_application_id)name,"
                + "(decode(record_type,1,$J,2,$S,3,$H)*amount)amnt"
                + " from $table where user_id = $userId order by ATM_APPLICATION_ID,transaction_date,dispute_key,record_type";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$userId", "" + userId);
        selectStat = selectStat.replace(",,", ",");
        selectStat = selectStat.replace(", ,", ",");
        selectStat = selectStat.replace("$field", "" + fields);
        selectStat = selectStat.replace(", ,", ",");
        selectStat = selectStat.replace(",,", ",");
        switch (mtInt) {
            case 1:
                selectStat = selectStat.replace("$J", "1");
                selectStat = selectStat.replace("$S", "0");
                selectStat = selectStat.replace("$H", "0");
            case 2:
                selectStat = selectStat.replace("$J", "0");
                selectStat = selectStat.replace("$S", "1");
                selectStat = selectStat.replace("$H", "0");

            case 3:
                selectStat = selectStat.replace("$J", "0");
                selectStat = selectStat.replace("$S", "0");
                selectStat = selectStat.replace("$H", "1");

            default:
                selectStat = selectStat.replace("$J", "0");
                selectStat = selectStat.replace("$S", "1");
                selectStat = selectStat.replace("$H", "0");
        }
        selectStat = selectStat.replace("TRANSACTION_DATA,", "");

        ResultSet rs = executeQueryReport(selectStat);
        super.postSelect();
        return rs;
    }

    public Integer PrintLines(String entities) throws Exception {
        int Seq = super.generateSequence("Transaction_Data_Report");
        Connection connection = null;
        PreparedStatement statement = null;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            statement = connection.prepareStatement("insert into transaction_data_report(sequence, linenumber, transdata)values(?, ?,?)");

            statement.setInt(1, Seq);
            statement.setInt(2, 0);
            statement.setString(3, entities);
            statement.addBatch();
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }

        }
        return Seq;
    }
}
