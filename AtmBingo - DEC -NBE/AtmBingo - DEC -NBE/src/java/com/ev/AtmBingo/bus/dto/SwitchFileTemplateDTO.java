/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.SwitchFileHeaderDAOInter;
import com.ev.AtmBingo.bus.dao.SwitchFileTemplateDetailDAOInter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class SwitchFileTemplateDTO extends BaseDTO implements SwitchFileTemplateDTOInter, Serializable{

    private int id;
    private String name;
    private BigDecimal processingType;
    private BigDecimal numberOfLines;
    private String loadingFolder;
    private BigDecimal startingDataType;
    private String startingFormat;
    private BigDecimal startingPosition;
    private String buckupFolder;
    private String copyFolder;
    private BigDecimal ignoredLines;
    private BigDecimal startingLength;
    private String dateSeparator;
    private BigDecimal dateSeparatorPos1;
    private BigDecimal dateSeparatorPos2;
    private String serverFolder;
    private BigDecimal headerDataType;
    private String headerFormat;
    private String headerPosition;
    private BigDecimal headerLength;
    private String headerDateSeparator;
    private BigDecimal headerDateSeparatorPos1;
    private BigDecimal headerDateSeparatorPos2;
    private String headerString;
    private String startingValue;
    private int active;
    private Boolean activeB;
    private String separator;
private String tagendformate,tagendvalue,atmadd;
private Integer  tagenddatatype,tagendpostition,tagendlength;

    public String getAtmadd() {
        return atmadd;
    }

    public void setAtmadd(String atmadd) {
        this.atmadd = atmadd;
    }




    public Integer getTagenddatatype() {
        return tagenddatatype;
    }

    public void setTagenddatatype(Integer tagenddatatype) {
        this.tagenddatatype = tagenddatatype;
    }

    public String getTagendformate() {
        return tagendformate;
    }

    public void setTagendformate(String tagendformate) {
        this.tagendformate = tagendformate;
    }

    public Integer getTagendlength() {
        return tagendlength;
    }

    public void setTagendlength(Integer tagendlength) {
        this.tagendlength = tagendlength;
    }

    public Integer getTagendpostition() {
        return tagendpostition;
    }

    public void setTagendpostition(Integer tagendpostition) {
        this.tagendpostition = tagendpostition;
    }

    public String getTagendvalue() {
        return tagendvalue;
    }

    public void setTagendvalue(String tagendvalue) {
        this.tagendvalue = tagendvalue;
    }

    public Boolean getActiveB() {
        activeB = active == 1;
        return activeB;
    }

    public void setActiveB(Boolean activeB) {
        if(activeB)
            active = 1;
        else
            active = 2;
        this.activeB = activeB;
    }
    private List<SwitchFileHeaderDTOInter> switchFileHeaderDTOL;
    private List<SwitchFileTemplateDetailDTOInter> switchFileTemplateDetailsDTOL;

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public List<SwitchFileHeaderDTOInter> getSwitchFileHeaderDTOL() throws Throwable {
        SwitchFileHeaderDAOInter afhDAO = DAOFactory.createSwitchFileHeaderDAO(null);
        switchFileHeaderDTOL = (List<SwitchFileHeaderDTOInter>)afhDAO.findByTemplate(this);
        return switchFileHeaderDTOL;
    }

    public void setSwitchFileHeaderDTOL(List<SwitchFileHeaderDTOInter> switchFileHeaderDTOL) {
        this.switchFileHeaderDTOL = switchFileHeaderDTOL;
    }

    public List<SwitchFileTemplateDetailDTOInter> getSwitchFileTemplateDetailsDTOL() throws Throwable {
        SwitchFileTemplateDetailDAOInter aftdDAO = DAOFactory.createSwitchFileTemplateDetailDAO(null);
        switchFileTemplateDetailsDTOL = (List<SwitchFileTemplateDetailDTOInter>)aftdDAO.findByTemplate(this);
        return switchFileTemplateDetailsDTOL;
    }

    public void setSwitchFileTemplateDetailsDTOL(List<SwitchFileTemplateDetailDTOInter> switchFileTemplateDetailsDTOL) {
        this.switchFileTemplateDetailsDTOL = switchFileTemplateDetailsDTOL;
    }


    public String getBuckupFolder() {
        return buckupFolder;
    }

    public void setBuckupFolder(String buckupFolder) {
        this.buckupFolder = buckupFolder;
    }

    public String getCopyFolder() {
        return copyFolder;
    }

    public void setCopyFolder(String copyFolder) {
        this.copyFolder = copyFolder;
    }

    public String getDateSeparator() {
        return dateSeparator;
    }

    public void setDateSeparator(String dateSeparator) {
        this.dateSeparator = dateSeparator;
    }

    public BigDecimal getDateSeparatorPos1() {
        return dateSeparatorPos1;
    }

    public void setDateSeparatorPos1(BigDecimal dateSeparatorPos1) {
        this.dateSeparatorPos1 = dateSeparatorPos1;
    }

    public BigDecimal getDateSeparatorPos2() {
        return dateSeparatorPos2;
    }

    public void setDateSeparatorPos2(BigDecimal dateSeparatorPos2) {
        this.dateSeparatorPos2 = dateSeparatorPos2;
    }

    public BigDecimal getHeaderDataType() {
        return headerDataType;
    }

    public void setHeaderDataType(BigDecimal headerDataType) {
        this.headerDataType = headerDataType;
    }

    public String getHeaderDateSeparator() {
        return headerDateSeparator;
    }

    public void setHeaderDateSeparator(String headerDateSeparator) {
        this.headerDateSeparator = headerDateSeparator;
    }

    public BigDecimal getHeaderDateSeparatorPos1() {
        return headerDateSeparatorPos1;
    }

    public void setHeaderDateSeparatorPos1(BigDecimal headerDateSeparatorPos1) {
        this.headerDateSeparatorPos1 = headerDateSeparatorPos1;
    }

    public BigDecimal getHeaderDateSeparatorPos2() {
        return headerDateSeparatorPos2;
    }

    public void setHeaderDateSeparatorPos2(BigDecimal headerDateSeparatorPos2) {
        this.headerDateSeparatorPos2 = headerDateSeparatorPos2;
    }

    public String getHeaderFormat() {
        return headerFormat;
    }

    public void setHeaderFormat(String headerFormat) {
        this.headerFormat = headerFormat;
    }

    public BigDecimal getHeaderLength() {
        return headerLength;
    }

    public void setHeaderLength(BigDecimal headerLength) {
        this.headerLength = headerLength;
    }

    public String getHeaderPosition() {
        return headerPosition;
    }

    public void setHeaderPosition(String headerPosition) {
        this.headerPosition = headerPosition;
    }

    public String getHeaderString() {
        return headerString;
    }

    public void setHeaderString(String headerString) {
        this.headerString = headerString;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getIgnoredLines() {
        return ignoredLines;
    }

    public void setIgnoredLines(BigDecimal ignoredLines) {
        this.ignoredLines = ignoredLines;
    }

    public String getLoadingFolder() {
        return loadingFolder;
    }

    public void setLoadingFolder(String loadingFolder) {
        this.loadingFolder = loadingFolder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getNumberOfLines() {
        return numberOfLines;
    }

    public void setNumberOfLines(BigDecimal numberOfLines) {
        this.numberOfLines = numberOfLines;
    }

    public BigDecimal getProcessingType() {
        return processingType;
    }

    public void setProcessingType(BigDecimal processingType) {
        this.processingType = processingType;
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public String getServerFolder() {
        return serverFolder;
    }

    public void setServerFolder(String serverFolder) {
        this.serverFolder = serverFolder;
    }

    public BigDecimal getStartingDataType() {
        return startingDataType;
    }

    public void setStartingDataType(BigDecimal startingDataType) {
        this.startingDataType = startingDataType;
    }

    public String getStartingFormat() {
        return startingFormat;
    }

    public void setStartingFormat(String startingFormat) {
        this.startingFormat = startingFormat;
    }

    public BigDecimal getStartingLength() {
        return startingLength;
    }

    public void setStartingLength(BigDecimal startingLength) {
        this.startingLength = startingLength;
    }

    public BigDecimal getStartingPosition() {
        return startingPosition;
    }

    public void setStartingPosition(BigDecimal startingPosition) {
        this.startingPosition = startingPosition;
    }

    public String getStartingValue() {
        return startingValue;
    }

    public void setStartingValue(String startingValue) {
        this.startingValue = startingValue;
    }
    

    protected SwitchFileTemplateDTO() {
        super();
    }

}
