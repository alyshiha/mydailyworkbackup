/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.AtmFileTemplateBOInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.AtmFileHeaderDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDetailDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.FileColumnDisplayDTOInter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.faces.event.ActionEvent;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JFileChooser;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author ISLAM
 */
public class ATMFileTemplateClient extends BaseBean  implements Serializable{

    private List<AtmFileTemplateDTOInter> templatenameList;
    private AtmFileTemplateDTOInter selectedtemplate, Deletetemplate, updatetemplate, newrecord;
    private AtmFileHeaderDTOInter DeletetemplateHeader, updatetemplateHeader, newrecordHeader;
    private AtmFileTemplateBOInter templateBO;
    private String ChosenItem, message, message2;
    private JFileChooser chooser;
    private List<FileColumnDisplayDTOInter> columnlist, tempcolumnlist;
    private List<AtmMachineTypeDTOInter> atmTypeList;
    private Boolean showAdd, showConfirm, showAdd1, showConfirm1;
    private List<AtmFileHeaderDTOInter> headertemplateList;
    private List<AtmFileTemplateDetailDTOInter> filetemplatedetailList;
    private AtmFileTemplateDetailDTOInter DeletetemplateDetail;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage2() {
        return message2;
    }

    public void setMessage2(String message2) {
        this.message2 = message2;
    }

    public AtmFileTemplateDetailDTOInter getDeletetemplateDetail() {
        return DeletetemplateDetail;
    }

    public void setDeletetemplateDetail(AtmFileTemplateDetailDTOInter DeletetemplateDetail) {
        this.DeletetemplateDetail = DeletetemplateDetail;
    }

    public AtmFileHeaderDTOInter getDeletetemplateHeader() {
        return DeletetemplateHeader;
    }

    public void setDeletetemplateHeader(AtmFileHeaderDTOInter DeletetemplateHeader) {
        this.DeletetemplateHeader = DeletetemplateHeader;
    }

    public List<AtmFileTemplateDetailDTOInter> getFiletemplatedetailList() {
        return filetemplatedetailList;
    }

    public void setFiletemplatedetailList(List<AtmFileTemplateDetailDTOInter> filetemplatedetailList) {
        this.filetemplatedetailList = filetemplatedetailList;
    }

    public AtmFileTemplateDTOInter getNewrecord() {
        return newrecord;
    }

    public void setNewrecord(AtmFileTemplateDTOInter newrecord) {
        this.newrecord = newrecord;
    }

    public AtmFileHeaderDTOInter getNewrecordHeader() {
        return newrecordHeader;
    }

    public void setNewrecordHeader(AtmFileHeaderDTOInter newrecordHeader) {
        this.newrecordHeader = newrecordHeader;
    }

    public AtmFileTemplateBOInter getTemplateBO() {
        return templateBO;
    }

    public void setTemplateBO(AtmFileTemplateBOInter templateBO) {
        this.templateBO = templateBO;
    }

    public AtmFileHeaderDTOInter getUpdatetemplateHeader() {
        return updatetemplateHeader;
    }

    public void setUpdatetemplateHeader(AtmFileHeaderDTOInter updatetemplateHeader) {
        this.updatetemplateHeader = updatetemplateHeader;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public List<AtmFileHeaderDTOInter> getHeadertemplateList() {
        return headertemplateList;
    }

    public void setHeadertemplateList(List<AtmFileHeaderDTOInter> headertemplateList) {
        this.headertemplateList = headertemplateList;
    }

    public List<FileColumnDisplayDTOInter> getColumnlist() {
        return columnlist;
    }

    public void setColumnlist(List<FileColumnDisplayDTOInter> columnlist) {
        this.columnlist = columnlist;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public AtmFileTemplateDTOInter getUpdatetemplate() {
        return updatetemplate;
    }

    public void setUpdatetemplate(AtmFileTemplateDTOInter updatetemplate) {
        this.updatetemplate = updatetemplate;
    }

 
    public AtmFileTemplateDTOInter getSelectedtemplate() {
        return selectedtemplate;
    }

    public void setSelectedtemplate(AtmFileTemplateDTOInter selectedtemplate) {
        this.selectedtemplate = selectedtemplate;
    }

    public AtmFileTemplateDTOInter getDeletetemplate() {
        return Deletetemplate;
    }

    public void setDeletetemplate(AtmFileTemplateDTOInter Deletetemplate) {
        this.Deletetemplate = Deletetemplate;
    }

    public List<AtmMachineTypeDTOInter> getAtmTypeList() {
        return atmTypeList;
    }

    public void setAtmTypeList(List<AtmMachineTypeDTOInter> atmTypeList) {
        this.atmTypeList = atmTypeList;
    }

    public String getChosenItem() {
        return ChosenItem;
    }

    public void setChosenItem(String ChosenItem) {
        this.ChosenItem = ChosenItem;
    }

    public List<AtmFileTemplateDTOInter> getTemplatenameList() {
        return templatenameList;
    }

    public void setTemplatenameList(List<AtmFileTemplateDTOInter> templatenameList) {
        this.templatenameList = templatenameList;
    }

    public void fetTable() {
        if (filetemplatedetailList.size() == 0) {
            for (FileColumnDisplayDTOInter f : columnlist) {
                AtmFileTemplateDetailDTOInter a = DTOFactory.createAtmFileTemplateDetailDTO();
                a.setColumnId(f.getColumnId().getId());
                filetemplatedetailList.add(a);
            }
        } else {
            List<AtmFileTemplateDetailDTOInter> l = new ArrayList<AtmFileTemplateDetailDTOInter>();
            for (FileColumnDisplayDTOInter f : columnlist) {
                AtmFileTemplateDetailDTOInter a = DTOFactory.createAtmFileTemplateDetailDTO();
                a.setColumnId(f.getColumnId().getId());
                l.add(a);
            }
            for (int i = 0; i < filetemplatedetailList.size(); i++) {
                AtmFileTemplateDetailDTOInter aa = null;
                for (int j = 0; j < l.size(); j++) {
                    int fid = l.get(j).getColumnId();
                    int aid = filetemplatedetailList.get(i).getColumnId();
                    if (fid == aid) {
                        l.get(j).setColumnId(filetemplatedetailList.get(i).getColumnId());
                        l.get(j).setDataType(filetemplatedetailList.get(i).getDataType());
                        l.get(j).setFormat(filetemplatedetailList.get(i).getFormat());
                        l.get(j).setFormat2(filetemplatedetailList.get(i).getFormat2());
                        l.get(j).setId(filetemplatedetailList.get(i).getId());
                        l.get(j).setLength(filetemplatedetailList.get(i).getLength());
                        l.get(j).setLengthDir(filetemplatedetailList.get(i).getLengthDir());
                        l.get(j).setLineNumber(filetemplatedetailList.get(i).getLineNumber());
                        l.get(j).setLoadWhenMappedB(filetemplatedetailList.get(i).getLoadWhenMappedB());
                        l.get(j).setPosition(filetemplatedetailList.get(i).getPosition());
                        l.get(j).setTwmplate(filetemplatedetailList.get(i).getTwmplate());
                        l.get(j).setStartingPosition(filetemplatedetailList.get(i).getStartingPosition());
                        l.get(j).setMandatory(filetemplatedetailList.get(i).getMandatory());
                        l.get(j).setNegativeflag(filetemplatedetailList.get(i).getNegativeflag());
                        l.get(j).setTagstring(filetemplatedetailList.get(i).getTagstring());
                        l.get(j).setDecimalpoint(filetemplatedetailList.get(i).getDecimalpoint());
                        l.get(j).setDecimalpointposition(filetemplatedetailList.get(i).getDecimalpointposition());
                         l.get(j).setTagsorder(filetemplatedetailList.get(i).getTagsorder());
                          l.get(j).setRemovestring(filetemplatedetailList.get(i).getRemovestring());

                    }
                }
            }
            filetemplatedetailList.clear();
            filetemplatedetailList = l;
        }
    }

    public ATMFileTemplateClient() throws Throwable {
        templatenameList = new ArrayList<AtmFileTemplateDTOInter>();
        templateBO = BOFactory.createAtmFileTemplateBO(null);
        atmTypeList = (List<AtmMachineTypeDTOInter>) templateBO.getMachineTypes();
        selectedtemplate = DTOFactory.createAtmFileTemplateDTO();
        columnlist = new ArrayList<FileColumnDisplayDTOInter>();
        headertemplateList = new ArrayList<AtmFileHeaderDTOInter>();
        columnlist = (List<FileColumnDisplayDTOInter>) templateBO.fetFileColumnDefinition();
        ChosenItem = "Journal";
        templatenameList = (List<AtmFileTemplateDTOInter>) templateBO.getAtmFileTemplates(FindTableName(ChosenItem));
        showConfirm = false;
        showAdd = true;
        showConfirm1 = false;
        showAdd1 = true;
        tempcolumnlist = columnlist;
        message = "";
        message2 = "";
             FacesContext context = FacesContext.getCurrentInstance();
      
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public String FindTableName(String TabName) {
        String TableName = "";
        if (TabName.equals("Journal")) {
            TableName = "ATM_FILE_TEMPLATE";
        } else if (TabName.equals("Switch")) {
            TableName = "SWITCH_FILE_TEMPLATE";
        } else if (TabName.equals("GL")) {
            TableName = "HOST_FILE_TEMPLATE";
        }
        return TableName;
    }

    public void recordtypeChange(TabChangeEvent e) throws Throwable {
        ChosenItem = e.getTab().getTitle().toString();
        templatenameList = (List<AtmFileTemplateDTOInter>) templateBO.getAtmFileTemplates(FindTableName(ChosenItem));
        selectedtemplate = DTOFactory.createAtmFileTemplateDTO();
        headertemplateList = new ArrayList<AtmFileHeaderDTOInter>();
        filetemplatedetailList = new ArrayList<AtmFileTemplateDetailDTOInter>();
        message = "";
        message2 = "";
    }

    public void templatenameSelect(SelectEvent e) throws Throwable {
        selectedtemplate = (AtmFileTemplateDTOInter) e.getObject();
       
        headertemplateList = selectedtemplate.getAtmFileHeaderDTOL(selectedtemplate, FindTableName(ChosenItem));
        filetemplatedetailList = selectedtemplate.getAtmFileTemplateDetailsDTOL(selectedtemplate, FindTableName(ChosenItem));
        fetTable();
        message = "";
        message2 = "";
    }

    public void fillDeleteTemplate(ActionEvent e) {
        Deletetemplate = (AtmFileTemplateDTOInter) e.getComponent().getAttributes().get("removedRow");
    }

    public void fillupdateTemplate(ActionEvent e) {
        updatetemplate = (AtmFileTemplateDTOInter) e.getComponent().getAttributes().get("updateRow");
    }

    public void deleteRecordTemplate() throws Throwable {
        message = (String) templateBO.DeleteAlldetailTemplate(Deletetemplate, FindTableName(ChosenItem));
        if (message.contains("Detail Record Has Been deleted")) {
            message = message + " " + (String) templateBO.deleterecordAllHeader(Deletetemplate, FindTableName(ChosenItem));
            if (message.contains("Header Has Been deleted")) {
                message = message + " " + (String) templateBO.DeleteAtmTemplate(Deletetemplate, FindTableName(ChosenItem));
                if (message.contains("Template Has Been deleted")) {
                    filetemplatedetailList = null;
                    headertemplateList = null;
                    templatenameList.remove(Deletetemplate);
                    message2 = "";
                }
            }
        }
    }

    public void updateRecordTemplate() throws Throwable {
        message = (String) templateBO.UpdateAtmTemplate(updatetemplate, FindTableName(ChosenItem));
        message2 = "";
    }

    public void addNewTemplate() throws Throwable {
        newrecord = DTOFactory.createAtmFileTemplateDTO();
        templatenameList.add(newrecord);
        showAdd = false;
        showConfirm = true;
        message = "";
        message2 = "";
    }

    public void confirmAddTemplate() throws Throwable {
        message = "";
        message2 = "";
        message = (String) templateBO.insertAtmTemplate(newrecord, FindTableName(ChosenItem));
        if (message.contains("Template Has Been Inserted")) {
            showConfirm = false;
            showAdd = true;
        }
        else{
        message = "Please Enter Required Fields";
        }
    }

    public void fillDeleteTemplateHeader(ActionEvent e) {
        DeletetemplateHeader = (AtmFileHeaderDTOInter) e.getComponent().getAttributes().get("removedRow1");
    }

    public void fillupdateTemplateHeader(ActionEvent e) {
        updatetemplateHeader = (AtmFileHeaderDTOInter) e.getComponent().getAttributes().get("updateRow1");
    }

    public void deleteRecordTemplateHeader() throws Throwable {
        message = "";
        message2 = "";
        message = (String) templateBO.DeleteAtmHeader(DeletetemplateHeader, FindTableName(ChosenItem));
        if (message.contains("Row Has Been deleted")) {
            headertemplateList.remove(DeletetemplateHeader);
        }
    }

    public void updateRecordTemplateHeader() throws Throwable {
        message = (String) templateBO.UpdateAtmHeader(updatetemplateHeader, FindTableName(ChosenItem));
    }

    public void addNewTemplateHeader() throws Throwable {
        newrecordHeader = DTOFactory.createAtmFileHeaderDTO();
        newrecordHeader.setTemplate(selectedtemplate.getId());
        newrecordHeader.setStatus("Insert");
        headertemplateList.add(newrecordHeader);
        message = "";
        message2 = "";
    }

    public void confirmAddTemplateHeader() throws Throwable {
        message = "";
        message2 = "";
        message = (String) templateBO.InsertAtmHeader(newrecordHeader, FindTableName(ChosenItem));
        if (message.contains("Row Has Been Inserted")) {
            showConfirm1 = false;
            showAdd1 = true;
        }
    }

    public void fillDeleteTemplateDetail(ActionEvent e) {
        DeletetemplateDetail = (AtmFileTemplateDetailDTOInter) e.getComponent().getAttributes().get("removedRow3");
    }

    public void deleteRecordTemplateDetail() throws Throwable {
        message = "";
        message2 = "";
        message2 = (String) templateBO.DeleteDetail(DeletetemplateDetail, FindTableName(ChosenItem) + "_DETAIL");
        if (message2.contains("Detail Record Has Been deleted")) {
            DeletetemplateDetail.setDataType(BigDecimal.ONE);
            DeletetemplateDetail.setFormat(null);
            DeletetemplateDetail.setFormat2(null);
            DeletetemplateDetail.setLength(null);
            DeletetemplateDetail.setLengthDir(null);
            DeletetemplateDetail.setLineNumber(null);
            DeletetemplateDetail.setPosition(null);
            DeletetemplateDetail.setLoadWhenMappedB(false);
            DeletetemplateDetail.setStartingPosition(null);
            DeletetemplateDetail.setTagstring(null);
            DeletetemplateDetail.setTagsorder(null);
            DeletetemplateDetail.setNegativeflag(false);
            DeletetemplateDetail.setRemovestring(null);
        }
    }

    public void SaveChanges() throws Throwable {
        message = "";
        message2 = (String) templateBO.SaveAllDetail(filetemplatedetailList, FindTableName(ChosenItem) + "_DETAIL", selectedtemplate.getId());
    }

    public void SaveChangesHeader() throws Throwable {
        message2 = "";
        message = (String) templateBO.SaveAllHeader(headertemplateList, FindTableName(ChosenItem), selectedtemplate.getId());

    }

    public void browseFolder() {
        chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(null);
        if (chooser.getSelectedFile().toString() != null) {
            selectedtemplate.setLoadingFolder(chooser.getSelectedFile().toString());
        }
    }

    public void browseFolder1() {
        chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(null);
        if (chooser.getSelectedFile().toString() != null) {
            selectedtemplate.setBuckupFolder(chooser.getSelectedFile().toString());
        }
    }

    public void browseFolder2() {
        chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(null);
        if (chooser.getSelectedFile().toString() != null) {
            selectedtemplate.setCopyFolder(chooser.getSelectedFile().toString());
        }
    }

    public void browseFolder3() {
        chooser = new JFileChooser();
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(null);
        if (chooser.getSelectedFile().toString() != null) {
            selectedtemplate.setServerFolder(chooser.getSelectedFile().toString());
        }
    }
    
      public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }
}
