/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.PendingAtmsBOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.PendingAtmsDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class PendingATMsClient extends BaseBean  implements Serializable{

    private PendingAtmsBOInter myObject;
    private PendingAtmsDTOInter pendingDTO;
    private List<PendingAtmsDTOInter> pendingList;
    private List<AtmMachineDTOInter> machineList;
    private String message;
    private int listSize;

    public List<AtmMachineDTOInter> getMachineList() {
        return machineList;
    }

    public void setMachineList(List<AtmMachineDTOInter> machineList) {
        this.machineList = machineList;
    }

    public int getListSize() {
        return listSize;
    }

    public void setListSize(int listSize) {
        this.listSize = listSize;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PendingAtmsDTOInter getPendingDTO() {
        return pendingDTO;
    }

    public void setPendingDTO(PendingAtmsDTOInter pendingDTO) {
        this.pendingDTO = pendingDTO;
    }

    public List<PendingAtmsDTOInter> getPendingList() {
        return pendingList;
    }

    public void setPendingList(List<PendingAtmsDTOInter> pendingList) {
        this.pendingList = pendingList;
    }

    /**
     * Creates a new instance of PendingATMsClient
     */
    public PendingATMsClient() throws Throwable {
        super();
        super.GetAccess();
        myObject = BOFactory.createPendingAtmsBo(null);
        pendingDTO = DTOFactory.createPendingAtmsDTO();
        pendingList = new ArrayList<PendingAtmsDTOInter>();
        machineList = new ArrayList<AtmMachineDTOInter>();
        pendingList = (List<PendingAtmsDTOInter>) myObject.getPendingAtms();
        listSize = pendingList.size();
        machineList = (List<AtmMachineDTOInter>) myObject.getAtmMachine();
        FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void fillRow(ActionEvent e) {
        pendingDTO = (PendingAtmsDTOInter) e.getComponent().getAttributes().get("removedRow");
        message = "";
    }

    public void approveRecord() {
        try {
            myObject.SavePendingAtm(pendingDTO);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
        }
    }

    public void approveAll() {
        try {
            myObject.SavePendingAtms(pendingList);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
        }
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
