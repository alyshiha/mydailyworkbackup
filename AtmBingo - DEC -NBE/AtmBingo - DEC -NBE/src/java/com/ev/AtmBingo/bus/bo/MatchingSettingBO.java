/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.AtmMachineTypeDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.FileColumnDefinitionDAOInter;
import com.ev.AtmBingo.bus.dao.MatchingTypeDAOInter;
import com.ev.AtmBingo.bus.dao.MatchingTypeMachineDAOInter;
import com.ev.AtmBingo.bus.dao.MatchingTypeSettingDAOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.FileColumnDefinitionDTOInter;
import com.ev.AtmBingo.bus.dto.MatchingTypeDTOInter;
import com.ev.AtmBingo.bus.dto.MatchingTypeMachinesDTOInter;
import com.ev.AtmBingo.bus.dto.MatchingTypeSettingDTOInter;
import com.ev.AtmBingo.bus.presdto.AtmMachineTypePresDTOInter;
import com.ev.AtmBingo.bus.presdto.FileColumnDefinitionPresDTOInter;
import com.ev.AtmBingo.bus.presdto.MatchingTypeMachinesPresDTOInter;
import com.ev.AtmBingo.bus.presdto.MatchingTypePresDTOInter;
import com.ev.AtmBingo.bus.presdto.MatchingTypeSettingPresDTOInter;
import com.ev.AtmBingo.bus.presdto.PresDTOFactory;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class MatchingSettingBO extends BaseBO implements MatchingSettingBOInter, Serializable {


      public Object RecreateIndex()throws Throwable{
        MatchingTypeSettingDAOInter mlDAO = DAOFactory.createMatchingTypeSettingDAO(null);
        mlDAO.RecreateIndex();
        return "Done";
    }

    public Object editMatchingType(Object obj, int operationType) throws Throwable {
        MatchingTypePresDTOInter mtPresDTO = (MatchingTypePresDTOInter) obj;
        MatchingTypeDAOInter mtDAO = DAOFactory.createMatchingTypeDAO(null);
        MatchingTypeDTOInter mtDTO = DTOFactory.createMatchingTypeDTO();

        mtDTO.setActive(mtPresDTO.getActive());
        mtDTO.setId(mtPresDTO.getId());
        mtDTO.setName(mtPresDTO.getName());
        mtDTO.setType1(mtPresDTO.getType1());
        mtDTO.setType2(mtPresDTO.getType2());

        if (operationType == UPDATE) {
            mtDAO.update(mtDTO);
        } else if (operationType == DELETE) {
            mtDAO.delete(mtDTO);
        } else if (operationType == INSERT) {
            mtDAO.insert(mtDTO);
        }

        return true;
    }

    public Object editMatchingTypeMachines(Object obj, int oldMachineType, int operationType) throws Throwable {
        MatchingTypeMachinesPresDTOInter mtmPresDTO = (MatchingTypeMachinesPresDTOInter) obj;
        MatchingTypeMachineDAOInter mtmDAO = DAOFactory.createMatchingTypeMachineDAO(null);
        MatchingTypeMachinesDTOInter mtmDTO = DTOFactory.createMatchingTypeMachinesDTO();

        mtmDTO.setDefaultMatchingSetting(mtmPresDTO.getDefaultMatchingSetting());
        mtmDTO.setMachineType(mtmPresDTO.getMachineType());
        mtmDTO.setMatchingType(mtmPresDTO.getMatchingType());

        if (operationType == UPDATE) {
            mtmDAO.update(mtmDTO, oldMachineType);
        } else if (operationType == DELETE) {
            if (mtmPresDTO.getMatchingTypeSettingL() != null) {
                for (MatchingTypeSettingPresDTOInter mtsPresDTO : mtmPresDTO.getMatchingTypeSettingL()) {
                    editeMatchingSetting(mtsPresDTO, 0, 0, DELETE);
                }
            }
            mtmDAO.delete(mtmDTO);
        } else if (operationType == INSERT) {
            mtmDAO.insert(mtmDTO);
        }

        return true;
    }

    public Object editeMatchingSetting(Object obj, int oldColumnId, int oldPartSetting, int operationType) throws Throwable {
        MatchingTypeSettingPresDTOInter mtsPresDTO = (MatchingTypeSettingPresDTOInter) obj;
        MatchingTypeSettingDAOInter mtsDAO = DAOFactory.createMatchingTypeSettingDAO(null);
        MatchingTypeSettingDTOInter mtsDTO = DTOFactory.createMatchingTypeSettingDTO();

        mtsDTO.setColumnId(mtsPresDTO.getColumnId());
        mtsDTO.setId(mtsPresDTO.getId());
        mtsDTO.setMatchingType(mtsPresDTO.getMatchingType());
        mtsDTO.setPartLength(mtsPresDTO.getPartLength());
        mtsDTO.setPartSetting(mtsPresDTO.getPartSetting());

        if (operationType == UPDATE) {
            mtsDAO.update(mtsDTO, oldColumnId, oldPartSetting);
        } else if (operationType == DELETE) {
            mtsDAO.delete(mtsDTO);
        } else if (operationType == INSERT) {
            mtsDAO.insert(mtsDTO);
        }

        return true;
    }

    public List<MatchingTypePresDTOInter> findAllMatchingType() throws Throwable {
        MatchingTypeDAOInter mtDAO = DAOFactory.createMatchingTypeDAO(null);
        List<MatchingTypeDTOInter> mtDTOL = (List<MatchingTypeDTOInter>) mtDAO.findAll();
        List<MatchingTypePresDTOInter> mtPresDTOL = new ArrayList<MatchingTypePresDTOInter>();

        for (MatchingTypeDTOInter mtDTO : mtDTOL) {
            MatchingTypePresDTOInter mtPresDTO = PresDTOFactory.creatMatchingTypePresDTO();
            mtPresDTO.setActive(mtDTO.getActive());
            mtPresDTO.setId(mtDTO.getId());
            mtPresDTO.setType1(mtDTO.getType1());
            mtPresDTO.setType2(mtDTO.getType2());
            mtPresDTO.setName(mtDTO.getName());
            mtPresDTO.setMatchingTypeMachinesL(findMatchingTypeMachinesPresDTO(mtDTO.getId()));
            mtPresDTOL.add(mtPresDTO);
        }

        return mtPresDTOL;
    }

    public List<AtmMachineTypePresDTOInter> findAllAtmMachinesType() throws Throwable {
        AtmMachineTypeDAOInter amtDAO = DAOFactory.createAtmMachineTypeDAO(null);
        List<AtmMachineTypeDTOInter> amtDTOL = (List<AtmMachineTypeDTOInter>) amtDAO.findAll();
        List<AtmMachineTypePresDTOInter> amtPresDTOL = new ArrayList<AtmMachineTypePresDTOInter>();
        for (AtmMachineTypeDTOInter amtDTO : amtDTOL) {
            AtmMachineTypePresDTOInter amtPresDTO = PresDTOFactory.creatAtmMachineTypePresDTOPresDTO();
            amtPresDTO.setId(amtDTO.getId());
            amtPresDTO.setName(amtDTO.getName());
            amtPresDTOL.add(amtPresDTO);
        }
        return amtPresDTOL;
    }

    public List<FileColumnDefinitionPresDTOInter> findAllFileColumnDefinition() throws Throwable {
        FileColumnDefinitionDAOInter fcdDAO = DAOFactory.createFileColumnDefinitionDAO(null);
        List<FileColumnDefinitionDTOInter> fcdDTOL = (List<FileColumnDefinitionDTOInter>) fcdDAO.findAllmatching();
        List<FileColumnDefinitionPresDTOInter> fcdPresDTOL = new ArrayList<FileColumnDefinitionPresDTOInter>();
        for (FileColumnDefinitionDTOInter fcdDTO : fcdDTOL) {
            FileColumnDefinitionPresDTOInter fcdPresDTO = PresDTOFactory.creatFileColumnDefinitionPresDTOPresDTO();
            fcdPresDTO.setColumnName(fcdDTO.getColumnName());
            fcdPresDTO.setDataType(fcdDTO.getDataType());
            fcdPresDTO.setId(fcdDTO.getId());
            fcdPresDTO.setName(fcdDTO.getName());
            fcdPresDTOL.add(fcdPresDTO);
        }
        return fcdPresDTOL;
    }

    // <editor-fold defaultstate="collapsed" desc="private method to fill lists">
    private List<MatchingTypeMachinesPresDTOInter> findMatchingTypeMachinesPresDTO(Object matchingId) throws Throwable {
        MatchingTypeMachineDAOInter mtmDAO = DAOFactory.createMatchingTypeMachineDAO(null);
        List<MatchingTypeMachinesDTOInter> mtmDTOL = (List<MatchingTypeMachinesDTOInter>) mtmDAO.find(matchingId);
        List<MatchingTypeMachinesPresDTOInter> mtmPresDTOL = new ArrayList<MatchingTypeMachinesPresDTOInter>();

        for (MatchingTypeMachinesDTOInter mtmDTO : mtmDTOL) {
            MatchingTypeMachinesPresDTOInter mtmPresDTO = PresDTOFactory.creatMatchingTypeMachinesPresDTO();

            mtmPresDTO.setDefaultMatchingSetting(mtmDTO.getDefaultMatchingSetting());
            mtmPresDTO.setMachineType(mtmDTO.getMachineType());
            mtmPresDTO.setMatchingType(mtmDTO.getMatchingType());
            mtmPresDTO.setMatchingTypeSettingL(findMatchingTypeSettingPresDTO(mtmDTO.getMatchingType(), mtmDTO.getMachineType()));

            mtmPresDTOL.add(mtmPresDTO);
        }
        return mtmPresDTOL;
    }

    private List<MatchingTypeSettingPresDTOInter> findMatchingTypeSettingPresDTO(Object matchingId, Object machineId) throws Throwable {
        MatchingTypeSettingDAOInter mtmDAO = DAOFactory.createMatchingTypeSettingDAO(null);
        List<MatchingTypeSettingDTOInter> mtmDTOL = (List<MatchingTypeSettingDTOInter>) mtmDAO.find(matchingId, machineId);
        List<MatchingTypeSettingPresDTOInter> mtmPresDTOL = new ArrayList<MatchingTypeSettingPresDTOInter>();

        for (MatchingTypeSettingDTOInter mtmDTO : mtmDTOL) {
            MatchingTypeSettingPresDTOInter mtmPresDTO = PresDTOFactory.creatMatchingTypeSettingPresDTO();

            mtmPresDTO.setColumnId(mtmDTO.getColumnId());
            mtmPresDTO.setId(mtmDTO.getId());
            mtmPresDTO.setMatchingType(mtmDTO.getMatchingType());
            if (mtmDTO.getPartLength() == null) {
                mtmPresDTO.setPartLength(null);
                
            } else {
                mtmPresDTO.setPartLength(mtmDTO.getPartLength());
                
            }
            mtmPresDTO.setPartSetting(mtmDTO.getPartSetting());

            mtmPresDTOL.add(mtmPresDTO);
        }
        return mtmPresDTOL;
    }// </editor-fold>
}

