/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;

/**
 *
 * @author Administrator
 */
public interface AtmGroupDAOInter extends BaseBOInter{

    Object findChildren(AtmGroupDTOInter parent) throws Throwable;

    Object findByName(String name)throws Throwable;

    Object findTree() throws Throwable;

    Object delete(Object... obj) throws Throwable;

    Object find(Object... obj) throws Throwable;

    Object findAll() throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
