/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class ATMMAchineLIC extends BaseDTO implements ATMMAchineLICInter, Serializable {

    private ATMMAchineLICInter atm;
    private String applicationId;
    private Boolean  LICStatus;
private  Integer MachineID;
private  Boolean Deleted;
    protected ATMMAchineLIC() {
          super();
    }

     public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

  public Boolean getDel() {
        return Deleted;
    }

    public void setDel(Boolean  Del) {
        this.Deleted = Del;
    }

    public Boolean getLIC() {
        return LICStatus;
    }

    public void setLIC(Boolean  LIC) {
        this.LICStatus = LIC;
    }
      public Integer getID() {
        return MachineID;
    }

    public void setID(Integer  MachineID) {
        this.MachineID = MachineID;
    }

}
