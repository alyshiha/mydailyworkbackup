/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.presdto;

import com.ev.AtmBingo.bus.dto.MatchingTypeMachinesDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface MatchingTypeMachinesPresDTOInter extends MatchingTypeMachinesDTOInter{

    String getMachType() throws Throwable;

    List<MatchingTypeSettingPresDTOInter> getMatchingTypeSettingL();

    void setMatchingTypeSettingL(List<MatchingTypeSettingPresDTOInter> matchingTypeSettingL);

    boolean isDefaultMatchSet();

    void setDefaultMatchSet(boolean defaultMatchSet);

    void setMachType(String machType) throws Throwable;

//    int getDefaultMatchingSetting();
//
//    int getMachineType();
//
//    int getMatchingType();
//
//    void setDefaultMatchingSetting(int defaultMatchingSetting);
//
//    void setMachineType(int machineType);
//
//    void setMatchingType(int matchingType);

}
