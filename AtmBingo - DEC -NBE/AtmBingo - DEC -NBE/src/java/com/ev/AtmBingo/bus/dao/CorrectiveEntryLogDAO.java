/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.CorrectiveEntryLogDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.SettlementLogDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Administrator
 */
public class CorrectiveEntryLogDAO extends BaseDAO implements CorrectiveEntryLogDAOInter
{
  protected CorrectiveEntryLogDAO() {
        super();
        super.setTableName("corrective_log");

    }
    public Object findAll(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select " + super.getTableName() + ".*," + super.getTableName() + ".card_no card_no_Decrypt from " + super.getTableName() + " order by corrective_date desc ";
        ResultSet result = executeQuery(selectStat);
        List<CorrectiveEntryLogDTOInter> slL = new ArrayList<CorrectiveEntryLogDTOInter>();
        while (result.next()) {
            CorrectiveEntryLogDTOInter sl = DTOFactory.createCorrectiveEntryLogDTO();
            sl.setAmount(result.getInt("amount"));
            sl.setAtmApplicationId(result.getString("atm_application_id"));
            sl.setCardNo(result.getString("card_no_Decrypt"));
            sl.setCurrency(result.getString("currency"));
            sl.setMatchingType(result.getInt("matching_type"));
            sl.setRecordType(result.getInt("record_type"));
            sl.setResponseCode(result.getString("response_code"));
            sl.setCorrectiveUser(result.getInt("corrective_user"));
            sl.setTransactionDate(result.getDate("transaction_date"));
             sl.setCorrective_date(result.getDate("corrective_date"));
            sl.setTransactionSequence(result.getString("transaction_sequence"));
            sl.setCorrectiveState(result.getInt("CORRECTIVE_STATE"));
            slL.add(sl);
        }
        super.postSelect(result);
        return slL;
    }

    public Object CustomSearch(Object... obj) throws Throwable {
        super.preSelect();
        String whereCluase = (String) obj[0];
        String selectStat = "select  " + super.getTableName() + ".*," + super.getTableName() + ".card_no card_no_Decrypt from " + super.getTableName() + " where 1=1 $whereCluase order by corrective_date desc";
        selectStat = selectStat.replace("$whereCluase", "" + whereCluase);
        List<CorrectiveEntryLogDTOInter> slL = new ArrayList<CorrectiveEntryLogDTOInter>();
        ResultSet result = executeQuery(selectStat);
        while (result.next()) {
            CorrectiveEntryLogDTOInter sl = DTOFactory.createCorrectiveEntryLogDTO();
            sl.setAmount(result.getInt("amount"));
            sl.setAtmApplicationId(result.getString("atm_application_id"));
            sl.setCardNo(result.getString("card_no_Decrypt"));
            sl.setCurrency(result.getString("currency"));
            sl.setMatchingType(result.getInt("matching_type"));
            sl.setRecordType(result.getInt("record_type"));
            sl.setResponseCode(result.getString("response_code"));
            sl.setCorrectiveUser(result.getInt("corrective_user"));
            sl.setTransactionDate(result.getDate("transaction_date"));
            sl.setCorrective_date(result.getDate("corrective_date"));
            sl.setTransactionSequence(result.getString("transaction_sequence"));
            sl.setCorrectiveState(result.getInt("CORRECTIVE_STATE"));
            slL.add(sl);
        }
        super.postSelect(result);
        return slL;
    }
}
