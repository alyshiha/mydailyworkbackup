/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ReplainshmentLogDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aly-Shiha
 */
public class ReplainshmentLogDAO extends BaseDAO implements ReplainshmentLogDAOInter {

    protected ReplainshmentLogDAO() {
        super();
    }

    @Override
    public Object findDataTable(String dailysheetFrom, String dailysheetTo, String dateFrom, String dateTo, int user) throws Throwable {
        super.preSelect();
        String selectStatment = "select (select application_id from atm_machine where id = atm_id)atm,created_date,date_from,date_to,comments,\n"
                + "       (select u.user_name from users u where u.user_id = created_by) UName,dailysheet\n"
                + "from replanishment_master\n"
                + "where atm_valid(atm_id) = 1 \n"
                + "and ((date_from <= to_date('$P{DateTo}','dd.mm.yyyy hh24:mi:ss')) or '$P{DateTo}' = '')\n"
                + "and ((date_from >= to_date('$P{DateFrom}','dd.mm.yyyy hh24:mi:ss'))or '$P{DateFrom}' = '')\n"
                + "and ((dailysheet >= to_date('$P{DailyFrom}','dd.mm.yyyy')) or '$P{DailyFrom}' = '')\n"
                + "and ((dailysheet <= to_date('$P{DailyTo}','dd.mm.yyyy')) or '$P{DailyTo}' = '')\n"
                + "and ((created_by = $P{user}) or '$P{user}' = '0')\n"
                + "and  created_by is not null\n"
                + "order by created_by,created_date";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DateTo}", dateTo);
        selectStatment = selectStatment.replace("$P{DailyFrom}", dailysheetFrom);
        selectStatment = selectStatment.replace("$P{DailyTo}", dailysheetTo);
        selectStatment = selectStatment.replace("$P{user}", "" + user);
        selectStatment = selectStatment.replace("'' = ''", "'1' = '1'");
        ResultSet rs = executeQueryReport(selectStatment);
        List<ReplainshmentLogDTOInter> Records = new ArrayList<ReplainshmentLogDTOInter>();
        while (rs.next()) {
            ReplainshmentLogDTOInter record = DTOFactory.createReplainshmentLogDTO();
            record.setComments(rs.getString("comments"));
            record.setUser(rs.getString("UName"));
            record.setAtmID(rs.getString("atm"));
            record.setCreatedBy(rs.getTimestamp("created_date"));
            record.setDateFrom(rs.getTimestamp("date_from"));
            record.setDateTo(rs.getTimestamp("date_to"));
            record.setDailySheet(rs.getTimestamp("dailysheet"));

            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findReport(String dailysheetFrom, String dailysheetTo, String dateFrom, String dateTo, int user) throws Throwable {
        super.preSelect();
        String selectStatment = "select (select application_id from atm_machine where id = atm_id)atm,created_date,date_from,date_to,comments,\n"
                + "       (select u.user_name from users u where u.user_id = created_by) UName,dailysheet\n"
                + "from replanishment_master\n"
                + "where atm_valid(atm_id) = 1 \n"
                + "and ((date_from <= to_date('$P{DateTo}','dd.mm.yyyy hh24:mi:ss')) or '$P{DateTo}' = '')\n"
                + "and ((date_from >= to_date('$P{DateFrom}','dd.mm.yyyy hh24:mi:ss'))or '$P{DateFrom}' = '')\n"
                + "and ((dailysheet >= to_date('$P{DailyFrom}','dd.mm.yyyy')) or '$P{DailyFrom}' = '')\n"
                + "and ((dailysheet <= to_date('$P{DailyTo}','dd.mm.yyyy')) or '$P{DailyTo}' = '')\n"
                + "and ((created_by = $P{user}) or '$P{user}' = '0')\n"
                + "and  created_by is not null\n"
                + "order by created_by,created_date";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DateTo}", dateTo);
        selectStatment = selectStatment.replace("$P{DailyFrom}", dailysheetFrom);
        selectStatment = selectStatment.replace("$P{DailyTo}", dailysheetTo);
        selectStatment = selectStatment.replace("$P{user}", "" + user);
        selectStatment = selectStatment.replace("'' = ''", "'1' = '1'");
        ResultSet rs = executeQueryReport(selectStatment);
        super.postSelect();
        return rs;
    }
}
