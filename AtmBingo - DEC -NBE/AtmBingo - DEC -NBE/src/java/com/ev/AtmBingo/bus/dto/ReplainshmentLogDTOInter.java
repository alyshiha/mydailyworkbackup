/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author Aly-Shiha
 */
public interface ReplainshmentLogDTOInter {

    Date getDailySheet();

    void setDailySheet(Date dailySheet);

    String getAtmID();

    String getComments();

    Date getCreatedBy();

    Date getDateFrom();

    Date getDateTo();

    String getUser();

    void setAtmID(String atmID);

    void setComments(String comments);

    void setCreatedBy(Date createdBy);

    void setDateFrom(Date dateFrom);

    void setDateTo(Date dateTo);

    void setUser(String user);

}
