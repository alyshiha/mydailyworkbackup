/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.MatchedDataDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TransactionsBOInter extends BaseBOInter {
 Integer PrintLines(String mdDTO) throws Exception;
String PrintLinesReport(String Seq) throws Exception ;
    public BigDecimal getTotalAmount();

    public void setTotalAmount(BigDecimal totalAmount);

    String getWhereCluase();

    void setWhereCluase(String whereCluase);

    Object findReport(String fields, String whereCluase, UsersDTOInter logedUser, List<ColumnDTOInter> c, MatchedDataDTOInter[] mdDTOArr, String cust) throws Throwable;

    Object gettexttransaction(MatchedDataDTOInter mdDTO) throws Throwable;

    Object FindFiletransaction(MatchedDataDTOInter mdDTO) throws Throwable;

    String getSearchParameter();

    void setSearchParameter(String searchParameter);

    List<ColumnDTOInter> getColumns(List columns) throws Throwable;

    Object MatchedMarkAsDisputes(MatchedDataDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable;

    Object MatchedMarkAsSettled(MatchedDataDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable;
 Object gettexttransaction_other(MatchedDataDTOInter mdDTO) throws Throwable ;
    Object getAnotherSideOfMatchedData(MatchedDataDTOInter mdDTO) throws Throwable;

    Object getAtmGroup() throws Throwable;

    Object getAtmMachines(UsersDTOInter loggedinUser, Integer grpId) throws Throwable;
    Object findByAtmbranch(UsersDTOInter loggedinUser) throws Throwable;

    Object getCurrencyMaster() throws Throwable;

    Object getFileColumnDefinition() throws Throwable;

    Object getMatchingType() throws Throwable;

    Object getSettledUsers() throws Throwable;

    Object getTransactionResponseCode() throws Throwable;

    Object getTransactionStatus() throws Throwable;

    Object getTransactionType() throws Throwable;

    Object searchMatchedData(Date transDateF, String opt1, Date transDateT, Date settleDateF, String opt2, Date settleDateT,
            Date loadDateF, String opt3, Date loadDateT, Integer amountF, String opt4, Integer amountT, Integer transSeqOrderByF,
            String opt5, Integer transSeqOrderByT, String cardNo, String opt6, String accountNo, String opt7, String notesPre,
            Integer transStatusCode, Integer transTypeCode, Integer responseCode, Integer currencyCode, Integer atmId,
            String atmCode, Integer atmGroup, boolean blackList, Integer settled, Integer matchingTypeList, Integer masterRecord,
            Integer settledBy, Integer matchedBy, String sortBy, UsersDTOInter logedUser, String field,String col1,String col2,String col3,String col4,String col5,String colop1,String colop2,String colop3,String colop4,String colop5) throws Throwable;

    Object getPickColumn() throws Throwable;

    Object findTotalAmount(Date transDateF, String opt1, Date transDateT, Date settleDateF, String opt2, Date settleDateT,
            Date loadDateF, String opt3, Date loadDateT, Integer amountF, String opt4, Integer amountT, Integer transSeqOrderByF,
            String opt5, Integer transSeqOrderByT, String cardNo, String opt6, String accountNo, String opt7, String notesPre,
            Integer transStatusCode, Integer transTypeCode, Integer responseCode, Integer currencyCode, Integer atmId,
            String atmCode, Integer atmGroup, boolean blackList, Integer settled, Integer matchingTypeList, Integer masterRecord,
            Integer settledBy, Integer matchedBy, String sortBy, UsersDTOInter logedUser, String field,String temp, String col1, String col2, String col3, String col4, String col5, String colop1, String colop2, String colop3, String colop4, String colop5) throws Throwable;
}
