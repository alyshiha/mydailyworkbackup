/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.AtmMachineTypesBOInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class ATMMachineTypesClient extends BaseBean implements Serializable {

    private AtmMachineTypesBOInter atmObject;
    private List<AtmMachineTypeDTOInter> atmList;
    private AtmMachineTypeDTOInter newAtm;
    private AtmMachineTypeDTOInter deleteAttr, updateAttr;
    private String message;
    private Boolean showAdd, showConfirm;
    private Integer listSize;

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public AtmMachineTypeDTOInter getNewAtm() {
        return newAtm;
    }

    public void setNewAtm(AtmMachineTypeDTOInter newAtm) {
        this.newAtm = newAtm;
    }

    public List<AtmMachineTypeDTOInter> getAtmList() {
        return atmList;
    }

    public void setAtmList(List<AtmMachineTypeDTOInter> atmList) {
        this.atmList = atmList;
    }

    public AtmMachineTypeDTOInter getDeleteAttr() {
        return deleteAttr;
    }

    public void setDeleteAttr(AtmMachineTypeDTOInter deleteAttr) {
        this.deleteAttr = deleteAttr;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public AtmMachineTypeDTOInter getUpdateAttr() {
        return updateAttr;
    }

    public void setUpdateAttr(AtmMachineTypeDTOInter updateAttr) {
        this.updateAttr = updateAttr;
    }

    /**
     * Creates a new instance of ATMMachineTypesClient
     */
    public ATMMachineTypesClient() throws Throwable {
        super();
        GetAccess();
        atmObject = BOFactory.createAtmMachineTypesBO(null);
        atmList = (List<AtmMachineTypeDTOInter>) atmObject.getAtmMachineType();
        showAdd = true;
        showConfirm = false;
        listSize = atmList.size();
        FacesContext context = FacesContext.getCurrentInstance();

        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void fillDelete(ActionEvent e) {
        deleteAttr = (AtmMachineTypeDTOInter) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void deleteRecord() throws Throwable {
        atmObject.editeAtmMachineType(deleteAttr, atmObject.DELETE);
        atmList.remove(deleteAttr);
        message = super.showMessage(SUCC);
        showAdd = true;
        showConfirm = false;
    }

    public void fillUpdate(ActionEvent e) {
        updateAttr = (AtmMachineTypeDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    public void updateRecord() {
        try {
            if (!showAdd) {
                message = super.showMessage(CONF_FIRST);
            } else if (updateAttr.getName().equalsIgnoreCase("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                String[] inputs = {updateAttr.getName()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    if (input.length() > 100) {
                        message = "value too long";
                        return;
                    }
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    atmObject.editeAtmMachineType(updateAttr, atmObject.UPDATE);
                    message = super.showMessage(SUCC);
                } else {
                    message = "Please Enter Valid Data";
                }

            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void addRecord() {
        newAtm = DTOFactory.createAtmMachineTypeDTO();
        atmList.add(newAtm);
        showAdd = false;
        showConfirm = true;
        resetVars();
    }

    public void confrimRecord() {
        try {
            if (newAtm.getName().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                String[] inputs = {newAtm.getName()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    if (input.length() > 100) {
                        message = "value too long";
                        return;
                    }
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {

                    atmObject.editeAtmMachineType(newAtm, atmObject.INSERT);
                    message = super.showMessage(SUCC);
                    showAdd = true;
                    showConfirm = false;
                } else {
                    message = "Please Enter Valid Data";
                }
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);

        }
    }

    public void resetVars() {
        message = "";
    }

    public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();

    }
}
