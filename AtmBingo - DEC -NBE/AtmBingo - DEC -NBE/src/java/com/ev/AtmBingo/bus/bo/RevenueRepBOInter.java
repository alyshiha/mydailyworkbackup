/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author KhAiReE
 */
public interface RevenueRepBOInter extends Serializable {

    Object findExceptions(int id) throws Throwable;

    Object findNetworkGroup() throws Throwable;

    Object getAtmMachines(UsersDTOInter u) throws Throwable;

    Object print(String user, String customer, Date dateF, Date dateT, int networkId, int groupId, int currency, int atmId, int group, int type) throws Throwable ;

}
