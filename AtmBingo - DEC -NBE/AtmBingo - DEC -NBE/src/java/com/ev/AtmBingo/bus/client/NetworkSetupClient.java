/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.NetworkBOInter;
import com.ev.AtmBingo.bus.dto.CurrencyDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.NetworkDetailsDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkGroupDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkMasterDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author ISLAM
 */
public class NetworkSetupClient extends BaseBean  implements Serializable{

    private NetworkBOInter netBo;
    private List<NetworkGroupDTOInter> netList;
    private List<NetworkMasterDTOInter> netDetailList;
    private List<NetworkDetailsDTOInter> netDetailedList;
    private List<CurrencyMasterDTOInter> currList;
    private List<TransactionTypeDTOInter> transList;
    private NetworkDetailsDTOInter newDetailNet, updateDetailNet, deleteDetailNet;
    private NetworkMasterDTOInter newNet, updateNet, deleteNet;
    private NetworkGroupDTOInter newmasternet, updateMasterNet, deleteMasterNet, selectedNet;
    private Boolean showAdd, showConfirm, showAddDetail, showConfirmDetail, showAddDetailed, showConfirmDetailed;
    private String message, messageDetails, messageDetailed, oldBin,groupName,networkName;
    private Integer oldCurrency, oldTransaction, oldCurrency1, oldTransaction1;

    public NetworkGroupDTOInter getNewmasternet() {
        return newmasternet;
    }

    public void setNewmasternet(NetworkGroupDTOInter newmasternet) {
        this.newmasternet = newmasternet;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    

    public String getOldBin() {
        return oldBin;
    }

    public void setOldBin(String oldBin) {
        this.oldBin = oldBin;
    }

    public Integer getOldCurrency1() {
        return oldCurrency1;
    }

    public void setOldCurrency1(Integer oldCurrency1) {
        this.oldCurrency1 = oldCurrency1;
    }

    public Integer getOldTransaction1() {
        return oldTransaction1;
    }

    public void setOldTransaction1(Integer oldTransaction1) {
        this.oldTransaction1 = oldTransaction1;
    }

    public Integer getOldCurrency() {
        return oldCurrency;
    }

    public void setOldCurrency(Integer oldCurrency) {
        this.oldCurrency = oldCurrency;
    }

    public Integer getOldTransaction() {
        return oldTransaction;
    }

    public void setOldTransaction(Integer oldTransaction) {
        this.oldTransaction = oldTransaction;
    }

    public NetworkDetailsDTOInter getDeleteDetailNet() {
        return deleteDetailNet;
    }

    public void setDeleteDetailNet(NetworkDetailsDTOInter deleteDetailNet) {
        this.deleteDetailNet = deleteDetailNet;
    }

    public String getMessageDetailed() {
        return messageDetailed;
    }

    public void setMessageDetailed(String messageDetailed) {
        this.messageDetailed = messageDetailed;
    }

    public List<NetworkDetailsDTOInter> getNetDetailedList() {
        return netDetailedList;
    }

    public void setNetDetailedList(List<NetworkDetailsDTOInter> netDetailedList) {
        this.netDetailedList = netDetailedList;
    }

    public NetworkDetailsDTOInter getNewDetailNet() {
        return newDetailNet;
    }

    public void setNewDetailNet(NetworkDetailsDTOInter newDetailNet) {
        this.newDetailNet = newDetailNet;
    }

    public Boolean getShowAddDetailed() {
        return showAddDetailed;
    }

    public void setShowAddDetailed(Boolean showAddDetailed) {
        this.showAddDetailed = showAddDetailed;
    }

    public Boolean getShowConfirmDetailed() {
        return showConfirmDetailed;
    }

    public void setShowConfirmDetailed(Boolean showConfirmDetailed) {
        this.showConfirmDetailed = showConfirmDetailed;
    }

    public NetworkDetailsDTOInter getUpdateDetailNet() {
        return updateDetailNet;
    }

    public void setUpdateDetailNet(NetworkDetailsDTOInter updateDetailNet) {
        this.updateDetailNet = updateDetailNet;
    }

    public List<CurrencyMasterDTOInter> getCurrList() {
        return currList;
    }

    public void setCurrList(List<CurrencyMasterDTOInter> currList) {
        this.currList = currList;
    }

    public List<TransactionTypeDTOInter> getTransList() {
        return transList;
    }

    public void setTransList(List<TransactionTypeDTOInter> transList) {
        this.transList = transList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageDetails() {
        return messageDetails;
    }

    public void setMessageDetails(String messageDetails) {
        this.messageDetails = messageDetails;
    }

    public NetworkGroupDTOInter getDeleteMasterNet() {
        return deleteMasterNet;
    }

    public void setDeleteMasterNet(NetworkGroupDTOInter deleteMasterNet) {
        this.deleteMasterNet = deleteMasterNet;
    }

   

    public NetworkGroupDTOInter getSelectedNet() {
        return selectedNet;
    }

    public void setSelectedNet(NetworkGroupDTOInter selectedNet) {
        this.selectedNet = selectedNet;
    }

    public NetworkGroupDTOInter getUpdateMasterNet() {
        return updateMasterNet;
    }

    public void setUpdateMasterNet(NetworkGroupDTOInter updateMasterNet) {
        this.updateMasterNet = updateMasterNet;
    }

    public List<NetworkMasterDTOInter> getNetDetailList() {
        return netDetailList;
    }

    public void setNetDetailList(List<NetworkMasterDTOInter> netDetailList) {
        this.netDetailList = netDetailList;
    }

    public List<NetworkGroupDTOInter> getNetList() {
        return netList;
    }

    public void setNetList(List<NetworkGroupDTOInter> netList) {
        this.netList = netList;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowAddDetail() {
        return showAddDetail;
    }

    public void setShowAddDetail(Boolean showAddDetail) {
        this.showAddDetail = showAddDetail;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowConfirmDetail() {
        return showConfirmDetail;
    }

    public void setShowConfirmDetail(Boolean showConfirmDetail) {
        this.showConfirmDetail = showConfirmDetail;
    }

    public NetworkMasterDTOInter getDeleteNet() {
        return deleteNet;
    }

    public void setDeleteNet(NetworkMasterDTOInter deleteNet) {
        this.deleteNet = deleteNet;
    }

    public NetworkMasterDTOInter getNewNet() {
        return newNet;
    }

    public void setNewNet(NetworkMasterDTOInter newNet) {
        this.newNet = newNet;
    }

    public NetworkMasterDTOInter getUpdateNet() {
        return updateNet;
    }

    public void setUpdateNet(NetworkMasterDTOInter updateNet) {
        this.updateNet = updateNet;
    }

    /** Creates a new instance of NetworkSetupClient */
    public NetworkSetupClient() throws Throwable {
        super();
        super.GetAccess();
        netBo = BOFactory.createNetworkBOInter(null);
        netList = (List<NetworkGroupDTOInter>) netBo.getNetworkGroup();
        transList = (List<TransactionTypeDTOInter>) netBo.getTransactionType();
        currList = (List<CurrencyMasterDTOInter>) netBo.getCurrency();
        showAdd = true;
        showAddDetail = true;
        showAddDetailed = true;
        showConfirm = false;
        showConfirmDetail = false;
        showConfirmDetailed = false;
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void onRowSelect1(SelectEvent e) {
        try {
            selectedNet = DTOFactory.createNetworkGroupDTO();
            selectedNet = (NetworkGroupDTOInter) e.getObject();
            netDetailList = (List<NetworkMasterDTOInter>) netBo.getNetworkMaster(selectedNet.getGroupId());
            netDetailedList = (List<NetworkDetailsDTOInter>) netBo.getNetworkDetails(selectedNet.getGroupId());
            resetVars();
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void onRowUnSelect1(UnselectEvent e) {
        if (netDetailList.size() != 0) {
            netDetailList.clear();
        }
        if (netDetailedList.size() != 0) {
            netDetailedList.clear();
        }
        resetVars();
        showAddDetail = true;
        showConfirmDetail = false;
        showAddDetailed = true;
        showConfirmDetailed = false;
    }

    public void fillDelete(ActionEvent e) {
        deleteMasterNet = (NetworkGroupDTOInter) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void deleteRecord() {
        try {
            netBo.editeNetworkGroup(deleteMasterNet, netBo.DELETE);
            netList.remove(deleteMasterNet);
            if (netDetailList != null) {
                netDetailList.clear();
            }
            if (netDetailedList != null) {
                netDetailedList.clear();
            }
            showAdd = true;
            showConfirm = false;
            showAddDetail = true;
            showConfirmDetail = false;
            showAddDetailed = true;
            showConfirmDetailed = false;
            message = super.showMessage(SUCC);
            messageDetailed = "";
            messageDetails = "";
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void fillUpdate(ActionEvent e) {
        updateMasterNet = (NetworkGroupDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    public void updateRecord() {
        try {
            if (showAdd == false) {
                message = super.showMessage(CONF_FIRST);
            } else if (updateMasterNet.getGroupName().equals("") || updateMasterNet.getNetwork().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                netBo.editeNetworkGroup(updateMasterNet, netBo.UPDATE);
                message = super.showMessage(SUCC);
                messageDetailed = "";
                messageDetails = "";
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void addRecord() {
        if (netList == null) {
            netList = new ArrayList<NetworkGroupDTOInter>();
        }
        newmasternet = DTOFactory.createNetworkGroupDTO();
        netList.add(newmasternet);
        if (netDetailList != null) {
            netDetailList.clear();
        }
        if (netDetailedList != null) {
            netDetailedList.clear();
        }
        showAdd = false;
        showConfirm = true;
        resetVars();
    }

    public void confrimRecord() {
        try {
            if (groupName.equals("") || networkName.equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                newmasternet.setGroupName(groupName);
                newmasternet.setNetwork(networkName);
                netBo.editeNetworkGroup(newmasternet, netBo.INSERT);
                message = super.showMessage(SUCC);
                showAdd = true;
                showConfirm = false;
                messageDetails = "";
                messageDetailed = "";
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void changeCurrency(ValueChangeEvent e) {
        oldCurrency = (Integer) e.getOldValue();
        resetVars();

    }

    public void changeTransaction(ValueChangeEvent e) {
        oldTransaction = (Integer) e.getOldValue();
        resetVars();
    }

    public void fillDelete1(ActionEvent e) {
        deleteNet = (NetworkMasterDTOInter) e.getComponent().getAttributes().get("removedRow1");
        resetVars();
    }

    public void fillUpdate1(ActionEvent e) {
        updateNet = (NetworkMasterDTOInter) e.getComponent().getAttributes().get("updateRow1");
        resetVars();
    }

    public void deleteRecord1() throws Throwable {
        netDetailList.remove(deleteNet);
        netBo.editeNetworkMasterSingle(deleteNet, netBo.DELETE, 0, 0);
        message = "";
        messageDetailed = "";
        messageDetails = super.showMessage(SUCC);
        showAdd = true;
        showAddDetail = true;
        showConfirm = false;
        showConfirmDetail = false;
    }

    public void updateRecord1() {
        try {
            if (showAddDetail == false) {
                messageDetails = super.showMessage(CONF_FIRST);
            } else if (updateNet.getRate() == null) {
                messageDetails = super.showMessage(REQ_FIELD);
            } else {
                if (oldCurrency == null) {
                    oldCurrency = updateNet.getCurrency();
                }
                if (oldTransaction == null) {
                    oldTransaction = updateNet.getTransactionType();
                }
                if (updateNet.getAmountFrom() == null) {
                    updateNet.setAmountFrom(0);
                }
                if (updateNet.getAmountTo() == null) {
                    updateNet.setAmountTo(99999);
                }
                netBo.editeNetworkMasterSingle(updateNet, netBo.UPDATE, oldCurrency, oldTransaction);
                messageDetails = super.showMessage(SUCC);
                message = "";
                messageDetailed = "";
            }
        } catch (Throwable ex) {
            messageDetails = super.showMessage(REC_EXIST);
        }
    }

    public void addDetailRecord() {
        if (netDetailList == null) {
            netDetailList = new ArrayList<NetworkMasterDTOInter>();
        }
        if (selectedNet == null) {
            messageDetails = "Select network first";
        } else if (showConfirm) {
            messageDetails = "Confirm the network";
        } else {
            newNet = DTOFactory.createNetworkMasterDTO();
            netDetailList.add(newNet);
            message = "";
            messageDetails = "";
            messageDetailed = "";
            showAdd = false;
            showAddDetail = false;
            showConfirm = true;
            showConfirmDetail = true;
        }
    }

    public void confirmDetailRecord() {
        try {
            if (newNet.getRate() == null) {
                messageDetails = super.showMessage(REQ_FIELD);
            } else {
                if (newNet.getAmountFrom() == null) {
                    newNet.setAmountFrom(0);
                }
                if (newNet.getAmountTo() == null) {
                    newNet.setAmountTo(99999);
                }
                newNet.setGroupId(selectedNet.getGroupId());
                netBo.editeNetworkMasterSingle(newNet, netBo.INSERT, 0, 0);
                messageDetails = super.showMessage(SUCC);
                message = "";
                messageDetailed = "";
                showAdd = true;
                showAddDetail = true;
                showConfirm = false;
                showConfirmDetail = false;
            }
        } catch (Throwable ex) {
            messageDetails = super.showMessage(REC_EXIST);
        }
    }

    public void changeCurrency1(ValueChangeEvent e) {
        oldCurrency1 = (Integer) e.getOldValue();

    }

    public void changeTransaction1(ValueChangeEvent e) {
        oldTransaction1 = (Integer) e.getOldValue();
    }

    public void changeBin1(ValueChangeEvent e) {
        oldBin = (String) e.getOldValue();
    }

    public void fillDelete2(ActionEvent e) {
        deleteDetailNet = (NetworkDetailsDTOInter) e.getComponent().getAttributes().get("removedRow2");
        messageDetails = "";
        message = "";
        messageDetailed = "";
    }

    public void fillUpdate2(ActionEvent e) {
        updateDetailNet = (NetworkDetailsDTOInter) e.getComponent().getAttributes().get("updateRow2");
        messageDetails = "";
        message = "";
        messageDetailed = "";
    }

    public void deleteRecord2() throws Throwable {
        netDetailedList.remove(deleteDetailNet);
        netBo.editeNetworkDetailsSingle(deleteDetailNet, netBo.DELETE, 0, 0, null);
        message = "";
        messageDetails = "";
        messageDetailed = super.showMessage(SUCC);
        showAdd = true;
        showAddDetail = true;
        showAddDetailed = true;
        showConfirm = false;
        showConfirmDetail = false;
        showConfirmDetailed = false;
    }

    public void updateRecord2() {
        try {
            if (showAddDetailed == false) {
                messageDetailed = super.showMessage(CONF_FIRST);
            } else if (updateDetailNet.getBin().equals("") || updateDetailNet.getRate() == null) {
                messageDetailed = super.showMessage(REQ_FIELD);
            } else {
                if (oldCurrency1 == null) {
                    oldCurrency1 = updateDetailNet.getCurrency();
                }
                if (oldTransaction1 == null) {
                    oldTransaction1 = updateDetailNet.getTransactionType();
                }
                if (oldBin == null) {
                    oldBin = updateDetailNet.getBin();
                }
                if (updateDetailNet.getAmountFrom() == null) {
                    updateDetailNet.setAmountFrom(0);
                }
                if (updateDetailNet.getAmountTo() == null) {
                    updateDetailNet.setAmountTo(99999);
                }
                netBo.editeNetworkDetailsSingle(updateDetailNet, netBo.UPDATE, oldCurrency1, oldTransaction1, oldBin);
                messageDetailed = super.showMessage(SUCC);
                message = "";
                messageDetails = "";
            }
        } catch (Throwable ex) {
            messageDetailed = super.showMessage(REC_EXIST);
        }
    }

    public void addDetailRecord1() {
        if (netDetailedList == null) {
            netDetailedList = new ArrayList<NetworkDetailsDTOInter>();
        }
        if (selectedNet == null) {
            messageDetailed = "Select network first";
        } else if (showConfirm) {
            messageDetailed = "Confirm the network";
        } else {
            newDetailNet = DTOFactory.createNetworkDetailsDTO();
            netDetailedList.add(newDetailNet);
            message = "";
            messageDetails = "";
            messageDetailed = "";
            showAdd = false;
            showAddDetail = false;
            showAddDetailed = false;
            showConfirm = true;
            showConfirmDetail = true;
            showConfirmDetailed = true;
        }
    }

    public void confirmDetailRecord1() {
        try {
            if (newDetailNet.getBin().equals("") || newDetailNet.getRate() == null) {
                messageDetailed = super.showMessage(REQ_FIELD);
            } else {
                if (newDetailNet.getAmountFrom() == null) {
                    newDetailNet.setAmountFrom(0);
                }
                if (newDetailNet.getAmountTo() == null) {
                    newDetailNet.setAmountTo(99999);
                }
                newDetailNet.setGroupId(selectedNet.getGroupId());
                netBo.editeNetworkDetailsSingle(newDetailNet, netBo.INSERT, 0, 0, null);
                messageDetailed = super.showMessage(SUCC);
                message = "";
                messageDetails = "";
                showAdd = true;
                showAddDetail = true;
                showAddDetailed = true;
                showConfirm = false;
                showConfirmDetail = false;
                showConfirmDetailed = false;
            }
        } catch (Throwable ex) {
            messageDetailed = super.showMessage(REC_EXIST);
        }
    }
    public void resetVars() {
        message = "";
        messageDetails = "";
        messageDetailed = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
