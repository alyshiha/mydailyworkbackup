/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DisputeRepDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aly-Shiha
 */
public class DisputeRepDAO extends BaseDAO implements DisputeRepDAOInter {

    protected DisputeRepDAO() {
        super();
    }

    @Override
    public Object findDataTable(String dateFrom, String dateTo, String atmID, Integer uservalue, Integer group) throws Throwable {
        super.preSelect();

        String selectStatment = " select atm_id, \n"
                + " (\n"
                + " select application_id\n"
                + " from atm_machine\n"
                + " where id = rm.atm_id\n"
                + " ) Atm_Full_Name\n"
                + " ,rm.date_from,rm.date_to,\n"
                + " (SELECT     listagg('C:' || NVL (CASSETE_NAME, cassette.name) || '/A:' ||\n"
                + " cassete_value || '/R:' ||\n"
                + " remaining,'  ;  ') within group(order by cassette.name) Cassettegroup\n"
                + " FROM   replanishment_detail r, cassette\n"
                + " WHERE   r.cassete = cassette.id AND r.id = rm.id AND cassette.name not like 'D%') Cassette_Value\n"
                + " from replanishment_master rm\n"
                + " where rm.switch_diff=0 and rm.cassitte_diff=0 and rm.disputes = 0 \n"
                + " and rm.date_from > to_date('$DateFrom','dd.MM.yyyy hh24:mi:ss')\n"
                + " and rm.date_to < to_date('$DateTo','dd.MM.yyyy hh24:mi:ss')\n"
                + " and (rm.atm_id = '$P{AtmID}' or '$P{AtmID}' = '1')\n"
                + " and (rm.created_by = '$P{createdby}' or '$P{createdby}' = '0')\n"
                + " and ((rm.atm_id in (select a.id from atm_machine a where a.id = rm.atm_id and a.atm_group = '$ATMGroup')) or '$ATMGroup' = '0')\n"
                + " order by (select application_id from atm_machine where id =rm.atm_id),rm.date_from";
        selectStatment = selectStatment.replace("$DateFrom", dateFrom);
        selectStatment = selectStatment.replace("$DateTo", dateTo);
        if (atmID == null || atmID == "") {
            selectStatment = selectStatment.replace("$P{AtmID}", "1");
        } else {
            selectStatment = selectStatment.replace("$P{AtmID}", "" + atmID);
        }
        selectStatment = selectStatment.replace("$P{createdby}", "" + uservalue);
        selectStatment = selectStatment.replace("$ATMGroup", "" + group);
        System.out.println(selectStatment);
        ResultSet rs = executeQueryReport(selectStatment);
        List<DisputeRepDTOInter> Records = new ArrayList<DisputeRepDTOInter>();
        while (rs.next()) {
            DisputeRepDTOInter record = DTOFactory.createDisputeRepDTO();

            record.setAtmAppID(rs.getString("atm_id"));
            record.setAtmFullName(rs.getString("Atm_Full_Name"));
            record.setCassetteValue(rs.getString("Cassette_Value"));
            record.setDateFrom(rs.getTimestamp("date_from"));
            record.setDateTo(rs.getTimestamp("date_to"));

            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findReport(String dateFrom, String dateTo, String atmID, Integer uservalue, Integer group) throws Throwable {
        super.preSelect();
        String selectStatment = " select atm_id, \n"
                + " (\n"
                + " select application_id\n"
                + " from atm_machine\n"
                + " where id = rm.atm_id\n"
                + " ) Atm_Full_Name\n"
                + " ,rm.date_from,rm.date_to,\n"
                + " (SELECT     listagg('C:' || NVL (CASSETE_NAME, cassette.name) || '/A:' ||\n"
                + " cassete_value || '/R:' ||\n"
                + " remaining,'  ;  ') within group(order by cassette.name) Cassettegroup\n"
                + " FROM   replanishment_detail r, cassette\n"
                + " WHERE   r.cassete = cassette.id AND r.id = rm.id AND cassette.name not like 'D%') Cassette_Value\n"
                + " from replanishment_master rm\n"
                + " where rm.switch_diff=0 and rm.cassitte_diff=0 and rm.disputes = 0 \n"
                + " and rm.date_from > to_date('$DateFrom','dd.MM.yyyy hh24:mi:ss')\n"
                + " and rm.date_to < to_date('$DateTo','dd.MM.yyyy hh24:mi:ss')\n"
                + " and (rm.atm_id = '$P{AtmID}' or '$P{AtmID}' = '1')\n"
                + " and (rm.created_by = '$P{createdby}' or '$P{createdby}' = '0')\n"
                + " and ((rm.atm_id in (select a.id from atm_machine a where a.id = rm.atm_id and a.atm_group = '$ATMGroup')) or '$ATMGroup' = '0')\n"
                + " order by (select application_id from atm_machine where id =rm.atm_id),rm.date_from";
        selectStatment = selectStatment.replace("$DateFrom", dateFrom);
        selectStatment = selectStatment.replace("$DateTo", dateTo);
        if (atmID == null || atmID == "") {
            selectStatment = selectStatment.replace("$P{AtmID}", "1");
        } else {
            selectStatment = selectStatment.replace("$P{AtmID}", "" + atmID);
        }
        selectStatment = selectStatment.replace("$P{createdby}", "" + uservalue);
        selectStatment = selectStatment.replace("$ATMGroup", "" + group);
        ResultSet rs = executeQueryReport(selectStatment);
        super.postSelect();
        return rs;
    }
}
