/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class TransStatDTO implements TransStatDTOInter , Serializable{

    private String matched, disputes, missingside, errors;
    private String applicationid;
    private String matchedcount, disputescount, missingsidecount, errorscount, applicationidcount;

    public String getApplicationidcount() {
        return applicationidcount;
    }

    public void setApplicationidcount(String applicationidcount) {
        this.applicationidcount = applicationidcount;
    }

    public String getDisputescount() {
        return disputescount;
    }

    public void setDisputescount(String disputescount) {
        this.disputescount = disputescount;
    }

    public String getErrorscount() {
        return errorscount;
    }

    public void setErrorscount(String errorscount) {
        this.errorscount = errorscount;
    }

    public String getMatchedcount() {
        return matchedcount;
    }

    public void setMatchedcount(String matchedcount) {
        this.matchedcount = matchedcount;
    }

    public String getMissingsidecount() {
        return missingsidecount;
    }

    public void setMissingsidecount(String missingsidecount) {
        this.missingsidecount = missingsidecount;
    }

    @Override
    public String getApplicationid() {
        return applicationid;
    }

    @Override
    public void setApplicationid(String applicationid) {
        this.applicationid = applicationid;
    }

    @Override
    public String getDisputes() {
        return disputes;
    }

    @Override
    public void setDisputes(String disputes) {
        this.disputes = disputes;
    }

    @Override
    public String getErrors() {
        return errors;
    }

    @Override
    public void setErrors(String errors) {
        this.errors = errors;
    }

    @Override
    public String getMatched() {
        return matched;
    }

    @Override
    public void setMatched(String matched) {
        this.matched = matched;
    }

    @Override
    public String getMissingside() {
        return missingside;
    }

    @Override
    public void setMissingside(String missingside) {
        this.missingside = missingside;
    }
}
