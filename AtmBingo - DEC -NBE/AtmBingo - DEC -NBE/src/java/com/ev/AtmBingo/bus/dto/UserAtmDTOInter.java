/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface UserAtmDTOInter extends Serializable {

    int getAtmId();

    int getUserId();

    void setAtmId(int atmId);

    void setUserId(int userId);

}
