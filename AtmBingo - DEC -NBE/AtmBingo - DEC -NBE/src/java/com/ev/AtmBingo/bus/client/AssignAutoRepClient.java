/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.AutoRepAtmToGroupBoInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.AutoRepDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author Administrator
 */
public class AssignAutoRepClient extends BaseBean  implements Serializable{
    private List<AutoRepDTOInter> groupList;
    private List<AtmMachineDTOInter> atmList;
    private AutoRepAtmToGroupBoInter uatmObject;

    public List<AtmMachineDTOInter> getAtmList() {
        return atmList;
    }

    public void setAtmList(List<AtmMachineDTOInter> atmList) {
        this.atmList = atmList;
    }

    public List<AutoRepDTOInter> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<AutoRepDTOInter> groupList) {
        this.groupList = groupList;
    }

    

    /** Creates a new instance of AssignAutoRepClient */
    public AssignAutoRepClient() throws Throwable {
        super();
        super.GetAccess();
        uatmObject = BOFactory.createAutoRepAtmToGroupBo(null);
        groupList = (List<AutoRepDTOInter>) uatmObject.getGroups();
        atmList = (List<AtmMachineDTOInter>) uatmObject.getSourceList(super.getLoggedInUser().getUserId(), 0);
             FacesContext context = FacesContext.getCurrentInstance();
        
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }
}
