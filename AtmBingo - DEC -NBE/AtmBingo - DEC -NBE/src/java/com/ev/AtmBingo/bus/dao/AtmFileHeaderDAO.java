/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.AtmFileHeaderDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmFileHeaderDAO extends BaseDAO implements AtmFileHeaderDAOInter {

    protected AtmFileHeaderDAO() {
        super();
        super.setTableName("atm_file_template_header");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileHeaderDTOInter uDTO = (AtmFileHeaderDTOInter) obj[0];
        String TName = (String) obj[1];
        if (TName.toUpperCase().equals("SWITCH_FILE_TEMPLATE_HEADER")) {
            uDTO.setId(super.generateSequence("SWITCH_FILE_TEMPLATE_HDR"));
        } else if (TName.toUpperCase().equals("HOST_FILE_TEMPLATE_HEADER")) {
            uDTO.setId(super.generateSequence("HOST_FILE_TEMPLATE_HDR"));
        } else {
            uDTO.setId(super.generateSequence(TName));
        }
        String insertStat = "insert into $table  (id, template, column_id, position, format, format2, data_type, length) values ($id, $template , $columnId, $position, '$format', '$formt2',"
                + " $dataType, $length)";
        insertStat = insertStat.replace("$table", "" + TName);
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$template", "" + uDTO.getTemplate());
        insertStat = insertStat.replace("$columnId", "" + uDTO.getColumnId());
        insertStat = insertStat.replace("$position", "" + uDTO.getPosition());
        insertStat = insertStat.replace("$formt2", "" + uDTO.getFormattwo());
        insertStat = insertStat.replace("$format", "" + uDTO.getFormat());
        
        insertStat = insertStat.replace("$dataType", "" + uDTO.getDataType());
        insertStat = insertStat.replace("$length", "" + uDTO.getLength());
        super.executeUpdate(insertStat);
        AtmFileTemplateDAOInter aftDAO = DAOFactory.createAtmFileTemplateDAO(null);
        AtmFileTemplateDTOInter aftDTO = (AtmFileTemplateDTOInter) aftDAO.find(uDTO.getTemplate(), TName);
        String action = "Add to " + aftDTO.getName() + " template a header template";
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileHeaderDTOInter uDTO = (AtmFileHeaderDTOInter) obj[0];
        String TName = (String) obj[1];
        String updateStat = "update $table set template = $template, column_id = $columnId, position = $position"
                + ", format = '$format', format2 = '$formt2', data_type = $dataType, length = $length"
                + "where id = $id";
        updateStat = updateStat.replace("$table", "" + TName);
        // updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$template", "" + uDTO.getTemplate());
        updateStat = updateStat.replace("$columnId", "" + uDTO.getColumnId());
        updateStat = updateStat.replace("$position", "" + uDTO.getPosition());
        updateStat = updateStat.replace("$formt2", "" + uDTO.getFormattwo());
        updateStat = updateStat.replace("$format", "" + uDTO.getFormat());
        
        updateStat = updateStat.replace("$dataType", "" + uDTO.getDataType());
        updateStat = updateStat.replace("$length", "" + uDTO.getLength());
        super.executeUpdate(updateStat);
        AtmFileTemplateDAOInter aftDAO = DAOFactory.createAtmFileTemplateDAO(null);
        AtmFileTemplateDTOInter aftDTO = (AtmFileTemplateDTOInter) aftDAO.find(uDTO.getTemplate(), TName);
        String action = "Update " + aftDTO.getName() + " template's header template";
        super.postUpdate(action, false);
        return null;
    }

    public Object deleterecordHeader(Object... obj) throws Throwable {
        super.preUpdate();
        AtmFileHeaderDTOInter RecordToDelete = (AtmFileHeaderDTOInter) obj[0];
        String TName = (String) obj[1];
        String msg = ValidateNullDeleteHeader(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table_HEADER "
                    + "  where  ID= $id";
            deleteStat = deleteStat.replace("$table", "" + TName);
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getId());
            msg = super.executeUpdate(deleteStat).toString();
            msg = msg + " Row Has Been deleted";
            super.postUpdate("Delete Record In Table " + TName + "_HEADER", false);
        }
        return msg;
    }
    public Object deleterecordHeaderByTemplateID(Object... obj) throws Throwable {
        super.preUpdate();
        AtmFileTemplateDTOInter RecordToDelete = (AtmFileTemplateDTOInter) obj[0];
        String TName = (String) obj[1];
        String msg = ValidateNullDeleteTemplateAllHeader(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table_HEADER "
                    + "  where  template = $id";
            deleteStat = deleteStat.replace("$table", "" + TName);
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getId());
            msg = super.executeUpdate(deleteStat).toString();
            msg = msg + " Header Has Been deleted";
            super.postUpdate("Delete Record In Table " + TName + "_HEADER", false);
        }
        return msg;
    }

    public String ValidateNullUpdateHeader(Object... obj) {
        AtmFileHeaderDTOInter RecordToInsert = (AtmFileHeaderDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getId() == 0) {
            Validate = Validate + " Feild id is a requiered field";
        }
        if (RecordToInsert.getTemplate() == 0) {
            Validate = Validate + " Feild template is a requiered field";
        }
        if (RecordToInsert.getColumnId() == 0) {
            Validate = Validate + " Feild columnid is a requiered field";
        }
        RecordToInsert.setPosition(RecordToInsert.getPosition().replace(" ", ""));
        if (RecordToInsert.getPosition() == null ? "" == null : RecordToInsert.getPosition().equals("")) {
            Validate = Validate + " Feild position is a requiered field";
        }
        if (RecordToInsert.getDataType() == null || (RecordToInsert.getDataType().toString() == null ? "null" == null : RecordToInsert.getDataType().toString().equals("null"))) {
            Validate = Validate + " Feild datatype is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object insertrecordHeader(Object... obj) throws Throwable {
        super.preUpdate();
        AtmFileHeaderDTOInter RecordToInsert = (AtmFileHeaderDTOInter) obj[0];
        String TableName = (String) obj[1];
        RecordToInsert.setId(super.generateSequence(TableName + "_HEADER"));
        String msg = ValidateNullUpdateHeader(RecordToInsert);
        if (msg.equals("Validate")) {
//RecordToInsert.setid(super.generateSequence(super.getTableName()));
            String insertStat = "insert into $table_HEADER"
                    + " (ID,TEMPLATE,COLUMN_ID,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH) "
                    + " values "
                    + " ($id,$template,$columnid,'$position','$format','$format2',$datatype,'$length')";
            insertStat = insertStat.replace("$table", "" + TableName);
            insertStat = insertStat.replace("$id", "" + RecordToInsert.getId());
            insertStat = insertStat.replace("$template", "" + RecordToInsert.getTemplate());
            insertStat = insertStat.replace("$columnid", "" + RecordToInsert.getColumnId());
            insertStat = insertStat.replace("$position", "" + RecordToInsert.getPosition());
            insertStat = insertStat.replace("$format2", "" + RecordToInsert.getFormattwo());
            insertStat = insertStat.replace("$format", "" + RecordToInsert.getFormat());
            
            insertStat = insertStat.replace("$datatype", "" + RecordToInsert.getDataType());
            insertStat = insertStat.replace("$length", "" + RecordToInsert.getLength());
            msg = super.executeUpdate(insertStat).toString();
            msg = msg + " Row Has Been Inserted";
            super.postUpdate("Add New Record To " + TableName + "_HEADER", false);
        }
        return msg;
    }

    public Object updaterecordHeader(Object... obj) throws Throwable {
        super.preUpdate();
        AtmFileHeaderDTOInter RecordToUpdate = (AtmFileHeaderDTOInter) obj[0];
        String TName = (String) obj[1];
        String msg = ValidateNullUpdateHeader(RecordToUpdate);
        if (msg.equals("Validate")) {
            String UpdateStat = "update $table_HEADER set "
                    + "TEMPLATE= $template,COLUMN_ID= $columnid,POSITION= '$position',FORMAT= '$format',FORMAT2= '$format2',DATA_TYPE= $datatype,LENGTH= '$length'"
                    + "  where  ID= $id";
            UpdateStat = UpdateStat.replace("$table", "" + TName);
            UpdateStat = UpdateStat.replace("$id", "" + RecordToUpdate.getId());
            UpdateStat = UpdateStat.replace("$template", "" + RecordToUpdate.getTemplate());
            UpdateStat = UpdateStat.replace("$columnid", "" + RecordToUpdate.getColumnId());
            UpdateStat = UpdateStat.replace("$position", "" + RecordToUpdate.getPosition());
            UpdateStat = UpdateStat.replace("$format2", "" + RecordToUpdate.getFormattwo());
            UpdateStat = UpdateStat.replace("$format", "" + RecordToUpdate.getFormat());
            
            UpdateStat = UpdateStat.replace("$datatype", "" + RecordToUpdate.getDataType());
            UpdateStat = UpdateStat.replace("$length", "" + RecordToUpdate.getLength());
            msg = super.executeUpdate(UpdateStat).toString();
            msg = msg + " Row Has Been updated";
            super.postUpdate("Update Record In Table " + TName + "_HEADER", false);
        }
        
        return msg;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileHeaderDTOInter uDTO = (AtmFileHeaderDTOInter) obj[0];
        String TName = (String) obj[1];
        String deleteStat = "delete from $table where id = $id  ";
        deleteStat = deleteStat.replace("$table", "" + TName);
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        AtmFileTemplateDAOInter aftDAO = DAOFactory.createAtmFileTemplateDAO(null);
        AtmFileTemplateDTOInter aftDTO = (AtmFileTemplateDTOInter) aftDAO.find(uDTO.getTemplate(), TName);
        String action = "Delete " + aftDTO.getName() + " template's header template";
        super.postUpdate(action, false);
        super.postUpdate(null, true);
        return null;
    }
public String ValidateNullDeleteTemplateAllHeader(Object... obj) {
        AtmFileTemplateDTOInter RecordToInsert = (AtmFileTemplateDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getId() == 0) {
            Validate = Validate + " Feild Template id is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }
    public String ValidateNullDeleteHeader(Object... obj) {
        AtmFileHeaderDTOInter RecordToInsert = (AtmFileHeaderDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getId() == 0) {
            Validate = Validate + " Feild id is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);

        ResultSet rs = executeQuery(selectStat);
        AtmFileHeaderDTOInter uDTO = DTOFactory.createAtmFileHeaderDTO();
        while (rs.next()) {
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getInt("data_type"));
            uDTO.setFormattwo(rs.getString("format2"));
            uDTO.setFormat(rs.getString("format"));
            
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getString("length"));
            uDTO.setPosition(rs.getString("position"));
            uDTO.setTemplate(rs.getInt("template"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<AtmFileHeaderDTOInter> uDTOL = new ArrayList<AtmFileHeaderDTOInter>();
        while (rs.next()) {
            AtmFileHeaderDTOInter uDTO = DTOFactory.createAtmFileHeaderDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getInt("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormattwo(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getString("length"));
            uDTO.setPosition(rs.getString("position"));
            uDTO.setTemplate(rs.getInt("template"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByTemplate(AtmFileTemplateDTOInter aftDTO, String TName) throws Throwable {
        super.preSelect();
        String temp = TName.toUpperCase();
        if (temp.indexOf("_HEADER") == -1) {
            TName = TName + "_HEADER";
        }

        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where template = $template";

        selectStat = selectStat.replace("$table", "" + TName);
        selectStat = selectStat.replace("$template", "" + aftDTO.getId());
        ResultSet rs = executeQuery(selectStat);
        List<AtmFileHeaderDTOInter> uDTOL = new ArrayList<AtmFileHeaderDTOInter>();
        while (rs.next()) {
            AtmFileHeaderDTOInter uDTO = DTOFactory.createAtmFileHeaderDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getInt("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormattwo(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getString("length"));
            uDTO.setPosition(rs.getString("position"));
            uDTO.setTemplate(rs.getInt("template"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
