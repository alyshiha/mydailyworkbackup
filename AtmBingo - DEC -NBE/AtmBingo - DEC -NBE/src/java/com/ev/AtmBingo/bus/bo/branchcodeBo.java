package com.ev.AtmBingo.bus.bo;
import com.ev.AtmBingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.AtmBingo.bus.dao.branchcodeDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.branchcodeDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.AtmBingo.bus.dto.branchcodeDTOInter;
import java.io.Serializable;
import java.text.SimpleDateFormat;
public class branchcodeBo extends BaseBO implements branchcodeBoInter,Serializable {
 protected branchcodeBo() {
 super();
}
 public Object insertrecord(Object... obj) throws Throwable {
branchcodeDTOInter RecordToInsert = (branchcodeDTOInter) obj[0];
branchcodeDAOInter engin = DAOFactory.createbranchcodeDAO();
engin.insertrecord(RecordToInsert);
return "Record Has Been Inserted";
}
public Object save(Object... obj) throws SQLException, Throwable {
List<branchcodeDTOInter> entities = (List<branchcodeDTOInter>) obj[0];
branchcodeDAOInter engin = DAOFactory.createbranchcodeDAO();
engin.save(entities);
return "Insert Done";
}
 public Object updaterecord(Object... obj) throws Throwable {
branchcodeDTOInter RecordToUpdate = (branchcodeDTOInter) obj[0];
branchcodeDAOInter engin = DAOFactory.createbranchcodeDAO();
engin.updaterecord(RecordToUpdate);
return "Update Done";
}
 public Object deleteallrecord(Object... obj) throws Throwable {
branchcodeDAOInter engin = DAOFactory.createbranchcodeDAO();
engin.deleteallrecord();
return "Delete Complete";
}
 public Object deleterecord(Object... obj) throws Throwable {
branchcodeDTOInter RecordToDelete = (branchcodeDTOInter) obj[0];
branchcodeDAOInter engin = DAOFactory.createbranchcodeDAO();
engin.deleterecord(RecordToDelete);
return "Record Has Been Deleted";
}
 public Object GetListOfRecords(Object... obj) throws Throwable {
List<branchcodeDTOInter> SearchRecords = (List<branchcodeDTOInter>) obj[0];
branchcodeDAOInter engin = DAOFactory.createbranchcodeDAO();
List<branchcodeDTOInter> AllRecords = (List<branchcodeDTOInter>) engin.findRecordsList(SearchRecords);
return AllRecords;
}
 public Object GetAllRecords(Object... obj) throws Throwable {
branchcodeDAOInter engin = DAOFactory.createbranchcodeDAO();
List<branchcodeDTOInter> AllRecords = (List<branchcodeDTOInter>) engin.findRecordsAll();
return AllRecords;
}
 public Object GetRecord(Object... obj) throws Throwable {
branchcodeDTOInter SearchParameter = (branchcodeDTOInter) obj[0];
branchcodeDAOInter engin = DAOFactory.createbranchcodeDAO();
branchcodeDTOInter Record = (branchcodeDTOInter) engin.findRecord(SearchParameter);
return Record;
}
}
