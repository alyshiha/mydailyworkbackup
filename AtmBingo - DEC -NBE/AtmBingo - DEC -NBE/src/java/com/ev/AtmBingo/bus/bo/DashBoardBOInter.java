/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.UserWorkDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface DashBoardBOInter {

    Object getOnlineUser(int rest) throws Throwable;

    Object getOfflineUser() throws Throwable;

    Object getNewAtms(int logedinUser) throws Throwable;

    List<UserWorkDTOInter> getUserWorkGraph(int userId, String from, String to) throws Throwable;
}
