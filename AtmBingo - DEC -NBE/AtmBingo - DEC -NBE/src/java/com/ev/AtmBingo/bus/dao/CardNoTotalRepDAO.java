/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.bycardDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class CardNoTotalRepDAO extends BaseDAO implements CardNoTotalRepDAOInter {

    protected CardNoTotalRepDAO() {
        super();
    }

    public Object findTotalbycardexcel(String dateFrom, String dateTo, int atmGroupInt, int side, int ord, int topCard, int tType) throws Throwable {
        super.preSelect();
        String selectStatment = "select *"
                + " from"
                + " ("
                + " select sum(get_number_type(m.amount))  transactions_count,sum(m.amount*m.AMOUNT_TYPE) total_amount,(select symbol from currency_master where id =m.currency_id) currency,card_no card_no"
                + " from all_transactions m"
                + " where m.transaction_date between to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and m.record_type = $P{side}"
                + " and transaction_type_id in (select id from transaction_type where TYPE_CATEGORY = $P{Ttype})"
                + " and ((m.atm_id in (select id from atm_machine where atm_group in (select id from  (select * from atm_group l order BY l.parent_id,name) start with id = $P{AtmGroupInt}  connect by prior id = parent_id))) or $P{AtmGroupInt} = 0)"
                + " group by m.currency_id,card_no order by $ord desc)"
                + " where (rownum <= $P{TopCard} or $P{TopCard} = 0)";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DatTo}", dateTo);
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        selectStatment = selectStatment.replace("$P{side}", "" + side);
        selectStatment = selectStatment.replace("$ord", "" + ord);
        selectStatment = selectStatment.replace("$P{Ttype}", "" + tType);
        selectStatment = selectStatment.replace("$P{TopCard}", "" + topCard);
        ResultSet rs = executeQueryReport(selectStatment);
        List<bycardDTOInter> Records = new ArrayList<bycardDTOInter>();
        while (rs.next()) {
            bycardDTOInter record = DTOFactory.createbycardDTO();
            record.setTransactionscount(rs.getString("transactions_count"));
            record.setTotalamount(rs.getInt("total_amount"));
            record.setCardno(rs.getString("card_no"));
            record.setCurrency(rs.getString("currency"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }
    public Object findReport(String dateFrom, String dateTo, int atmGroupInt, int side, int ord, int topCard, int tType) throws Throwable {
        super.preSelect();
        String selectStatment = "select *"
                + " from"
                + " ("
                + " select sum(get_number_type(m.amount))  transactions_count,sum(m.amount*m.AMOUNT_TYPE) total_amount,(select symbol from currency_master where id =m.currency_id) currency,card_no card_no"
                + " from all_transactions m"
                + " where m.transaction_date between to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and m.record_type = $P{side}"
                + " and transaction_type_id in (select id from transaction_type where TYPE_CATEGORY = $P{Ttype})"
                + " and ((m.atm_id in (select id from atm_machine where atm_group in (select id from  (select * from atm_group l order BY l.parent_id,name) start with id = $P{AtmGroupInt}  connect by prior id = parent_id))) or $P{AtmGroupInt} = 0)"
                + " group by m.currency_id,card_no order by $ord desc)"
                + " where (rownum <= $P{TopCard} or $P{TopCard} = 0)";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DatTo}", dateTo);
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        selectStatment = selectStatment.replace("$P{side}", "" + side);
        selectStatment = selectStatment.replace("$ord", "" + ord);
        selectStatment = selectStatment.replace("$P{Ttype}", "" + tType);
        selectStatment = selectStatment.replace("$P{TopCard}", "" + topCard);
 
        ResultSet rs = executeQueryReport(selectStatment);
        super.postSelect();
        return rs;
    }
}
