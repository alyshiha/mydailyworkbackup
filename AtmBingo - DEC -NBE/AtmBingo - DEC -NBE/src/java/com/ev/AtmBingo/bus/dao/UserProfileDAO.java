/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UserProfileDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class UserProfileDAO extends BaseDAO implements UserProfileDAOInter {

    protected UserProfileDAO() {
        super();
        super.setTableName("user_profile");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        UserProfileDTOInter uDTO = (UserProfileDTOInter) obj[0];
        String insertStat = "insert into $table values ($user_id, $profile_id)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$profile_id", "" + uDTO.getProfileId());
        insertStat = insertStat.replace("$user_id", "" + uDTO.getUserId());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object deleteByProfileId(Object... obj) throws Throwable {
        super.preUpdate();
        int uDTO = (Integer) obj[0];
        String deleteStat = "delete from $table where profile_id = $profile_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$profile_id", "" + uDTO);
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object updateByUserId(Object... obj) throws Throwable {
        super.preUpdate();
        int uDTO = (Integer) obj[0];
        int profile = (Integer) obj[1];
        String deleteStat = "update $table set profile_id = $profileId where user_id = $userId";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$userId", "" + uDTO);
        deleteStat = deleteStat.replace("$profileId", "" + profile);
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object deleteByUserId(Object... obj) throws Throwable {
        super.preUpdate();
        UserProfileDTOInter uDTO = (UserProfileDTOInter) obj[0];
        String deleteStat = "delete from $table where user_id = $user_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$user_id", "" + uDTO.getUserId());
        super.postUpdate(null, true);
        return null;
    }

    public Object findByUserId(int userId) throws Throwable {
        preSelect();
        String selectStat = "select * from $table where user_id = $userId";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$userId", "" + userId);
        ResultSet rs = executeQuery(selectStat);
        UserProfileDTOInter upDTO = DTOFactory.createUserProfileDTO();
        while (rs.next()) {
            upDTO.setProfileId(rs.getInt("profile_id"));
            upDTO.setUserId(rs.getInt("user_id"));
        }
        postSelect(rs);
        return upDTO;
    }

    public Object search(Object... obj) throws Throwable {
        Object obj1 = super.preSelect();
        super.postSelect(obj1);
        return null;
    }

    public Object findAll(Object... obj) throws Throwable {
        preSelect();
        String selectStat = "select * from $table ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<UserProfileDTOInter> all = new ArrayList<UserProfileDTOInter>();
        while (rs.next()) {
            UserProfileDTOInter upDTO = DTOFactory.createUserProfileDTO();
            upDTO.setProfileId(rs.getInt("profile_id"));
            upDTO.setUserId(rs.getInt("user_id"));
            all.add(upDTO);
        }
        postSelect(rs);
        return all;
    }
}
