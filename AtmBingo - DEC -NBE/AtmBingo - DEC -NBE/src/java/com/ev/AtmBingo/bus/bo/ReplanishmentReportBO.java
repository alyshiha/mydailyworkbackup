/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import DBCONN.Session;
import com.ev.AtmBingo.base.bo.BaseBO;

import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.ReplanishmentMasterDAOInter;
import com.ev.AtmBingo.bus.dao.ReplanishmentReportDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentReportDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.repreportexcelDTOInter;
import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRReportFont;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignReportFont;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author Administrator
 */
public class ReplanishmentReportBO extends BaseBO implements ReplanishmentReportBOInter,Serializable {

    protected ReplanishmentReportBO() {
        super();
    }

    public Object getReplanishmentData(Date DailySheet,Date from, Date to, String atmId, Date createdF, Date createdT, int userId, int group, int proc, Date From1, Date From2, Date To1, Date To2) throws Throwable {
        String whereClaus = " 1=1 ";
        String dateF = "";
        String dateT = "";
        String creaF = "";
        String creaT = "";
        String dateFD1 = "";
        String dateFD2 = "";
        String dateTD1 = "";
        String dateTD2 = "";
        String dailyS = "";

        if (proc != 0) {
            if (proc == 1) {
                whereClaus += " and status = 1 ";
            }
            if (proc == 2) {
                whereClaus += " and status is null ";
            }
        }
        if (From1 == null) {
            dateTD1 = "null";
        } else {
            dateTD1 = "'";
            dateTD1 += DateFormatter.changeDateAndTimeFormat(From1);
            dateTD1 += "'";
            whereClaus += " and (Date_from >=to_date($dateF,'dd.mm.yyyy hh24:mi:ss'))";
            whereClaus = whereClaus.replace("$dateF", dateTD1);
        }
        if (DailySheet == null) {
            dailyS = "null";
        } else {
            dailyS = "'";
            dailyS += DateFormatter.changeDateFormat(DailySheet);
            dailyS += "'";
            whereClaus += " and (dailysheet = to_date($daily,'dd.mm.yyyy'))";
            whereClaus = whereClaus.replace("$daily", dailyS);
        }
        if (From2 == null) {
            dateTD2 = "null";
        } else {
            dateTD2 = "'";
            dateTD2 += DateFormatter.changeDateAndTimeFormat(From2);
            dateTD2 += "'";
            whereClaus += " and (Date_from <=to_date($dateF,'dd.mm.yyyy hh24:mi:ss'))";
            whereClaus = whereClaus.replace("$dateF", dateTD2);
        }
        if (To1 == null) {
            dateFD1 = "null";
        } else {
            dateFD1 = "'";
            dateFD1 += DateFormatter.changeDateAndTimeFormat(To1);
            dateFD1 += "'";
            whereClaus += " and (Date_to >=to_date($dateF,'dd.mm.yyyy hh24:mi:ss'))";
            whereClaus = whereClaus.replace("$dateF", dateFD1);
        }
        if (To2 == null) {
            dateFD2 = "null";
        } else {
            dateFD2 = "'";
            dateFD2 += DateFormatter.changeDateAndTimeFormat(To2);
            dateFD2 += "'";
            whereClaus += " and (Date_to <=to_date($dateF,'dd.mm.yyyy hh24:mi:ss'))";
            whereClaus = whereClaus.replace("$dateF", dateFD2);
        }

        if (from == null) {
            dateF = "null";
        } else {
            dateF = "'";
            dateF += DateFormatter.changeDateAndTimeFormat(from);
            dateF += "'";
            whereClaus += " and (Date_from >=to_date($dateF,'dd.mm.yyyy hh24:mi:ss'))";
            whereClaus = whereClaus.replace("$dateF", dateF);
        }

        if (to == null) {
            dateT = "null";
        } else {
            dateT = "'";
            dateT += DateFormatter.changeDateAndTimeFormat(to);
            dateT += "'";
            whereClaus += " and (Date_to <=to_date($datT,'dd.mm.yyyy hh24:mi:ss')) ";
            whereClaus = whereClaus.replace("$datT", dateT);
        }

        if (createdF == null) {
            creaF = "null";
        } else {
            creaF = "'";
            creaF += DateFormatter.changeDateAndTimeFormat(createdF);
            creaF += "'";
            whereClaus += " and (CREATED_DATE >=to_date($creaF,'dd.mm.yyyy hh24:mi:ss'))";
            whereClaus = whereClaus.replace("$creaF", creaF);
        }

        if (createdT == null) {
            creaT = "null";
        } else {
            creaT = "'";
            creaT += DateFormatter.changeDateAndTimeFormat(createdT);
            creaT += "'";
            whereClaus += " and (CREATED_DATE <=to_date($creT,'dd.mm.yyyy hh24:mi:ss'))";
            whereClaus = whereClaus.replace("$creT", creaT);
        }
        /*  if (atmId != null) {
        AutoComplete AC = new AutoComplete();
        whereClaus += " and atm_id = $atmId";
        whereClaus = whereClaus.replace("$atmId", "" + AC.findByAtmId(atmId));
        }*/
        if (userId != 0) {
            if (userId != -1) {
                whereClaus += " and created_by = $userId";
                whereClaus = whereClaus.replace("$userId", "" + userId);
            } else {
                whereClaus += " and created_by is null";
            }
        }
        if (group != 0) {
            whereClaus += " and ATM_ID IN (select id from atm_machine where atm_group in (select id from atm_group where parent_id in (select id from atm_group where (parent_id = $col1 or id = $col1)) or id = $col1))";
            whereClaus = whereClaus.replace("$col1", "" + group);
        }





        whereClaus += " order by date_from,created_date DESC";
        ReplanishmentMasterDAOInter rmDAO = DAOFactory.createReplanishmentMasterDAO(null);
      
        Session sessionutil = new Session();
        UsersDTOInter currentUser = sessionutil.GetUserLogging();
        List<ReplanishmentMasterDTOInter> rmDTOL = (List<ReplanishmentMasterDTOInter>) rmDAO.customSearch(whereClaus, currentUser.getUserId());

        return rmDTOL;

    }

    public Object printExcel(ReplanishmentMasterDTOInter selectedRec, String userName, String cust) throws Throwable {
        ReplanishmentReportDAOInter rrDAO = DAOFactory.createReplanishmentReportDAO(null);
        int id = rrDAO.getSequence();
        ReplanishmentReportDTOInter rrDTO = DTOFactory.createReplanishmentReportDTO();
        rrDTO.setId(id);
        rrDTO.setRepId(selectedRec.getId());
        rrDAO.insert(rrDTO);
        List<repreportexcelDTOInter> result = new ArrayList<repreportexcelDTOInter>();
        List<repreportexcelDTOInter> result2 = new ArrayList<repreportexcelDTOInter>();
        result = (List<repreportexcelDTOInter>) rrDAO.getrepreoprtexcel(id);
        if(result.get(2).getColoumn9() != null){
        result2 = (List<repreportexcelDTOInter>) rrDAO.getrepreoprtcassetexcel(result.get(2).getColoumn9());
        rrDAO.delete(id);
        result.addAll(result2);}
        return result;
    }

    public Object print(ReplanishmentMasterDTOInter[] selectedRec, String userName, String cust) throws Throwable {
        ReplanishmentReportDAOInter rrDAO = DAOFactory.createReplanishmentReportDAO(null);
        int id = rrDAO.getSequence();
        // int id2 = rrDAO.getSequence();

        for (ReplanishmentMasterDTOInter r : selectedRec) {
            ReplanishmentReportDTOInter rrDTO = DTOFactory.createReplanishmentReportDTO();
            rrDTO.setId(id);
            rrDTO.setRepId(r.getId());
            rrDAO.insert(rrDTO);
        }

        //    rrDAO.calcReplanishment(id);

        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();


        params.put("user", userName);
        params.put("rep_id", id);
        //  params.put("rep_id2", id2);
        params.put("customer", cust);


        try {

             Connection conn =CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/Replanishment.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);


            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);



            JRReportFont font = new JRDesignReportFont();
            font.setPdfFontName("c:/windows/fonts/arial.ttf");
            font.setPdfEncoding(com.lowagie.text.pdf.BaseFont.IDENTITY_H);
            font.setPdfEmbedded(true);

            jasperPrint.setDefaultFont(font);
            Calendar c = Calendar.getInstance();
            String uri = "Replanishment" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";

            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);


            try {

                String x = "../PDF/" + uri;

                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                rrDAO.delete(id);
                //  rrDAO.deletePrv(id);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                rrDAO.delete(id);
                //  rrDAO.deletePrv(id);
                return "failPDF";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            rrDAO.delete(id);
            //  rrDAO.deletePrv(id);
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();

            return "failReport";
        }
    }

    public Object print(ReplanishmentMasterDTOInter selectedRec, String userName, String cust) throws Throwable {
        ReplanishmentReportDAOInter rrDAO = DAOFactory.createReplanishmentReportDAO(null);
        ReplanishmentMasterDAOInter rmDAO = DAOFactory.createReplanishmentMasterDAO(null);
        int id = rrDAO.getSequence();
        ReplanishmentReportDTOInter rrDTO = DTOFactory.createReplanishmentReportDTO();
        rrDTO.setId(id);
        rrDTO.setRepId(selectedRec.getId());
        rrDAO.insert(rrDTO);

        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();


        params.put("user", userName);
        params.put("rep_id", id);
        params.put("customer", cust);


        try {
   Connection conn =CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/Replanishment.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);


            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);



            JRReportFont font = new JRDesignReportFont();
            font.setPdfFontName("c:/windows/fonts/arial.ttf");
            font.setPdfEncoding(com.lowagie.text.pdf.BaseFont.IDENTITY_H);
            font.setPdfEmbedded(true);

            jasperPrint.setDefaultFont(font);
            Calendar c = Calendar.getInstance();
            String uri = "Replanishment" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";

            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);


            try {

                String x = pdfPath + uri;

                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
               CoonectionHandler.getInstance().returnConnection(conn);
                rrDAO.delete(id);
                //  rrDAO.deletePrv(id);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                rrDAO.delete(id);
                //  rrDAO.deletePrv(id);
                return "failPDF";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            rrDAO.delete(id);
            // rrDAO.deletePrv(id);
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();

            return "failReport";
        }
    }

    public Object getAtmMachines(UsersDTOInter u) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>) amDAO.findRecordGroup(u);
        return amDTOL;
    }

    public Object getUsers() throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> uDTOL = (List<UsersDTOInter>) uDAO.findComboBox();
        return uDTOL;
    }

    public Date getLastReplanishmentDateFor(int atmId) throws Throwable {
        ReplanishmentMasterDAOInter rmDAO = DAOFactory.createReplanishmentMasterDAO(null);
        Date d = (Date) rmDAO.findLastDate(atmId);
        return d;
    }
}
