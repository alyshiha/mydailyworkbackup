/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface cardissuerDTOInter  extends Serializable {

    String getCurrency();

    String getName();

    Integer getTotalamount();

    Integer getTotalcount();

    void setCurrency(String currency);

    void setName(String name);

    void setTotalamount(Integer totalamount);

    void setTotalcount(Integer totalcount);
    
}
