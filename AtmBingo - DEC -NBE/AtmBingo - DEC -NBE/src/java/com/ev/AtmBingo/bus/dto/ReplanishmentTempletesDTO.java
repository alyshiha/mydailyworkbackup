/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class ReplanishmentTempletesDTO extends BaseDTO implements ReplanishmentTempletesDTOInter , Serializable{

    private int id ;
    private String name ;
    private String startingString ;
    private Integer startingPosition ;
    private String endingString ;
    private Integer endingPosition ;
    private String atmIdString ;
    private Integer atmIdPosition ;
    private Integer atmIdLenght ;
    private String addingDateStartingString ;
    private Integer addingDateLine ;
    private Integer addingDatePosition ;
    private Integer addingDateLenght ;
    private String remainingDateStartingString ;
    private Integer remainingDateLine ;
    private Integer remainingDatePosition ;
    private Integer remainingDateLenght ;
    private String remainingDateFormat ;
    private Integer atmStringPosition ;
    private Integer atmMachineType ;
    private String addingDateFormat;
    private String cardtakenstartingstring ;
    private Integer cardtakenline ;
    private Integer cardtakenposition ;
    private Integer cardtakenlength ;
   
    public Integer getCardtakenlength() {
        return cardtakenlength;
    }

    public void setCardtakenlength(Integer cardtakenlength) {
        this.cardtakenlength = cardtakenlength;
    }

    public Integer getCardtakenline() {
        return cardtakenline;
    }

    public void setCardtakenline(Integer cardtakenline) {
        this.cardtakenline = cardtakenline;
    }

    public Integer getCardtakenposition() {
        return cardtakenposition;
    }
     public void setCardtakenposition(Integer cardtakenposition) {
        this.cardtakenposition = cardtakenposition;
    }


    public String getCardtakenstartingstring() {
        return cardtakenstartingstring;
    }

    public void setCardtakenstartingstring(String cardtakenstartingstring) {
        this.cardtakenstartingstring = cardtakenstartingstring;
    }
    
    public String getAddingDateFormat() {
        return addingDateFormat;
    }

    public void setAddingDateFormat(String addingDateFormat) {
        this.addingDateFormat = addingDateFormat;
    }
    
    public Integer getAddingDateLenght() {
        return addingDateLenght;
    }

    public void setAddingDateLenght(Integer addingDateLenght) {
        this.addingDateLenght = addingDateLenght;
    }

    public Integer getAddingDateLine() {
        return addingDateLine;
    }

    public void setAddingDateLine(Integer addingDateLine) {
        this.addingDateLine = addingDateLine;
    }

    public Integer getAddingDatePosition() {
        return addingDatePosition;
    }

    public void setAddingDatePosition(Integer addingDatePosition) {
        this.addingDatePosition = addingDatePosition;
    }

    public String getAddingDateStartingString() {
        return addingDateStartingString;
    }

    public void setAddingDateStartingString(String addingDateStartingString) {
        this.addingDateStartingString = addingDateStartingString;
    }

    public Integer getAtmIdLenght() {
        return atmIdLenght;
    }

    public void setAtmIdLenght(Integer atmIdLenght) {
        this.atmIdLenght = atmIdLenght;
    }

    public Integer getAtmIdPosition() {
        return atmIdPosition;
    }

    public void setAtmIdPosition(Integer atmIdPosition) {
        this.atmIdPosition = atmIdPosition;
    }

    public String getAtmIdString() {
        return atmIdString;
    }

    public void setAtmIdString(String atmIdString) {
        this.atmIdString = atmIdString;
    }

    public Integer getAtmMachineType() {
        return atmMachineType;
    }

    public void setAtmMachineType(Integer atmMachineType) {
        this.atmMachineType = atmMachineType;
    }

    public Integer getAtmStringPosition() {
        return atmStringPosition;
    }

    public void setAtmStringPosition(Integer atmStringPosition) {
        this.atmStringPosition = atmStringPosition;
    }

    public Integer getEndingPosition() {
        return endingPosition;
    }

    public void setEndingPosition(Integer endingPosition) {
        this.endingPosition = endingPosition;
    }

    public String getEndingString() {
        return endingString;
    }

    public void setEndingString(String endingString) {
        this.endingString = endingString;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemainingDateFormat() {
        return remainingDateFormat;
    }

    public void setRemainingDateFormat(String remainingDateFormat) {
        this.remainingDateFormat = remainingDateFormat;
    }

    public Integer getRemainingDateLenght() {
        return remainingDateLenght;
    }

    public void setRemainingDateLenght(Integer remainingDateLenght) {
        this.remainingDateLenght = remainingDateLenght;
    }

    public Integer getRemainingDateLine() {
        return remainingDateLine;
    }

    public void setRemainingDateLine(Integer remainingDateLine) {
        this.remainingDateLine = remainingDateLine;
    }

    public Integer getRemainingDatePosition() {
        return remainingDatePosition;
    }

    public void setRemainingDatePosition(Integer remainingDatePosition) {
        this.remainingDatePosition = remainingDatePosition;
    }

    public String getRemainingDateStartingString() {
        return remainingDateStartingString;
    }

    public void setRemainingDateStartingString(String remainingDateStartingString) {
        this.remainingDateStartingString = remainingDateStartingString;
    }

    public Integer getStartingPosition() {
        return startingPosition;
    }

    public void setStartingPosition(Integer startingPosition) {
        this.startingPosition = startingPosition;
    }

    public String getStartingString() {
        return startingString;
    }

    public void setStartingString(String startingString) {
        this.startingString = startingString;
    }


}
