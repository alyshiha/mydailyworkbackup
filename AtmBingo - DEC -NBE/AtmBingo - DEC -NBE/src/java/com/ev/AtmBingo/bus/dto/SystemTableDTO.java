/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class SystemTableDTO extends BaseDTO implements Serializable, SystemTableDTOInter{

    private String parameterName;
    private String parameterValue;
    private int parameterDataType;

    public int getParameterDataType() {
        return parameterDataType;
    }

    public void setParameterDataType(int parameterDataType) {
        this.parameterDataType = parameterDataType;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }
    
    protected SystemTableDTO() {
        super();
    }




}
