/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public interface RepDetailTempDTOInter {

    Integer getCassetteid();

    String getCassettename();

    Integer getRemaining();

    Integer getRepid();

    Integer getTotalamount();

    void setCassetteid(Integer cassetteid);

    void setCassettename(String cassettename);

    void setRemaining(Integer remaining);

    void setRepid(Integer repid);

    void setTotalamount(Integer totalamount);
    
}
