/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface CurrencyDTOInter extends Serializable{

    String getAbbreviation();
 Boolean getDefcurr();
  void setDefcurr(Boolean defcurr);
    int getId();

    int getMasterId();

    void setAbbreviation(String abbreviation);

    void setId(int id);

    void setMasterId(int masterId);

}
