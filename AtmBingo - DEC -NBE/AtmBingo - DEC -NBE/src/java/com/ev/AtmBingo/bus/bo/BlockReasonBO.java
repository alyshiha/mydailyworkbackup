/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.BlockReasonDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.BlockReasonDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class BlockReasonBO extends BaseBO implements BlockReasonBOInter,Serializable{

    protected BlockReasonBO() {
        super();
    }

    public Object editeBlockReason(Object obj, int operation) throws Throwable{
        BlockReasonDAOInter brDAO = DAOFactory.createBlockReasonDAO(null);
        BlockReasonDTOInter brDTO = (BlockReasonDTOInter)obj;
        
        switch(operation){
            case INSERT:
                brDAO.insert(brDTO);
                return "Insert Done";
            case UPDATE:
                brDAO.update(brDTO);
                return "Update Done";
            case DELETE:
                brDAO.delete(brDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }

    public Object getBlockReasons() throws Throwable{
        BlockReasonDAOInter brDAO = DAOFactory.createBlockReasonDAO(null);
        List<BlockReasonDTOInter> brDTOL = (List<BlockReasonDTOInter>)brDAO.findAll();
        return brDTOL;
    }

    public static void main(String[] args) throws Throwable{
        BlockReasonBOInter bOInter = BOFactory.createBlockReasonBO(null);
        for(BlockReasonDTOInter b :(List<BlockReasonDTOInter>) bOInter.getBlockReasons()){
            
        }
    }
}
