/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class CurrencyMasterDAO extends BaseDAO implements CurrencyMasterDAOInter {

    protected CurrencyMasterDAO() {
        super();
        super.setTableName("currency_master");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        CurrencyMasterDTOInter uDTO = (CurrencyMasterDTOInter) obj[0];
        uDTO.setId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ($id, '$name' , '$symbol',$DefCur)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$name", "" + uDTO.getName());
        insertStat = insertStat.replace("$symbol", "" + uDTO.getSymbol());


               Integer temp;
        if(uDTO.getDefcurr() == true){temp=1;}else{temp=2;}
        insertStat = insertStat.replace("$DefCur", "" + temp);

        super.executeUpdate(insertStat);
        String action = "Add a new currency master with name" + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        CurrencyMasterDTOInter uDTO = (CurrencyMasterDTOInter) obj[0];
        String updateStat = "update $table  set DEFAULT_CURRENCY = $DefCur, name = '$name', symbol = '$symbol' where id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$name", "" + uDTO.getName());
        updateStat = updateStat.replace("$symbol", "" + uDTO.getSymbol());
                Integer temp;
        if(uDTO.getDefcurr() == true){temp=1;}else{temp=2;}
        updateStat = updateStat.replace("$DefCur", "" + temp);

        super.executeUpdate(updateStat);
        String action = "Update currency master with name" + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        CurrencyMasterDTOInter uDTO = (CurrencyMasterDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        String action = "Delete currency master with name" + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        CurrencyMasterDTOInter uDTO = DTOFactory.createCurrencyMasterDTO();
        while (rs.next()) {
            uDTO.setName(rs.getString("name"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setSymbol(rs.getString("symbol"));
     Integer temp = rs.getInt("DEFAULT_CURRENCY");
            if(temp == 1){uDTO.setDefcurr(Boolean.TRUE);}
            else{uDTO.setDefcurr(Boolean.FALSE);}
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<CurrencyMasterDTOInter> uDTOL = new ArrayList<CurrencyMasterDTOInter>();
        while (rs.next()) {
            CurrencyMasterDTOInter uDTO = DTOFactory.createCurrencyMasterDTO();
            uDTO.setName(rs.getString("name"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setSymbol(rs.getString("symbol"));
     Integer temp = rs.getInt("DEFAULT_CURRENCY");
            if(temp == 1){uDTO.setDefcurr(Boolean.TRUE);}
            else{uDTO.setDefcurr(Boolean.FALSE);}
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
       public int CountCurrencyMaster()throws Throwable{
   super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
       // Integer Id = cmDTO.getId();
        String selectStat = "select count(*) elnum from $table where  DEFAULT_CURRENCY = 1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        Integer temp = 0;
        while (rs.next()) {
              temp = rs.getInt("elnum");
        }
        super.postSelect(rs);
        return temp;
    }
}
