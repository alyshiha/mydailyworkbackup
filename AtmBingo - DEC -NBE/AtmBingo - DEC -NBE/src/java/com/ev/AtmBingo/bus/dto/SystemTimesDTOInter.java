/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface SystemTimesDTOInter  extends Serializable{

    Boolean getWorkingB();

    void setWorkingB(Boolean workingB);

    String getArabicName();

    int getDayNo();

    Date getEndTime();

    String getEnglishName();

    int getSeq();

    Date getStartTime();

    int getWorking();

    void setArabicName(String arabicName);

    void setDayNo(int dayNo);

    void setEndTime(Date endTime);

    void setEnglishName(String englishName);

    void setSeq(int seq);

    void setStartTime(Date startTime);

    void setWorking(int working);

}
