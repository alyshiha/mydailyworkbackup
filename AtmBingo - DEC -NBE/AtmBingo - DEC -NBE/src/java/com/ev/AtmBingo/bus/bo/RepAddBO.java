/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.RepAddDAOInter;
import com.ev.AtmBingo.bus.dto.RepAddDTOInter;
import com.ev.AtmBingo.bus.dto.RepDetailTempDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import java.util.List;
import java.io.Serializable;
import java.util.Date;

public class RepAddBO extends BaseBO implements RepAddBOInter {

    protected RepAddBO() {
        super();
    }

    @Override
    public int insertrecord(RepAddDTOInter RecordToInsert) throws Throwable {
        RepAddDAOInter engin = DAOFactory.createRepAddDAO();
        return engin.insert(RecordToInsert);

    }

    @Override
    public String Excute(RepAddDTOInter record) throws Throwable {
        RepAddDAOInter engin = DAOFactory.createRepAddDAO();
        return engin.Excute(record);
    }

    @Override
    public String updaterecord(RepAddDTOInter RecordToUpdate) throws Throwable {
        RepAddDAOInter engin = DAOFactory.createRepAddDAO();
        engin.update(RecordToUpdate);
        return "Update Done";
    }

    @Override
    public String deleterecord(RepAddDTOInter RecordToDelete) throws Throwable {
        RepAddDAOInter engin = DAOFactory.createRepAddDAO();
        engin.delete(RecordToDelete);
        return "Record Has Been Deleted";
    }

    @Override
    public String savecassette(List<RepDetailTempDTOInter> entities) throws Throwable {
        RepAddDAOInter engin = DAOFactory.createRepAddDAO();
        engin.save(entities);
        return "Records Has Been Saved";
    }

    @Override
    public List<RepAddDTOInter> GetAllRecords() throws Throwable {
        RepAddDAOInter engin = DAOFactory.createRepAddDAO();
        List<RepAddDTOInter> AllRecords = (List<RepAddDTOInter>) engin.findAll();
        return AllRecords;
    }

    @Override
    public List<ReplanishmentMasterDTOInter> GetRecord(Date todate, int atmid) throws Throwable {
        RepAddDAOInter engin = DAOFactory.createRepAddDAO();
        List<ReplanishmentMasterDTOInter> AllRecords = (List<ReplanishmentMasterDTOInter>) engin.find(todate, atmid);
        return AllRecords;
    }

    @Override
    public List<RepDetailTempDTOInter> GetCassette(int RepID) throws Throwable {
        RepAddDAOInter engin = DAOFactory.createRepAddDAO();
        List<RepDetailTempDTOInter> AllRecords = (List<RepDetailTempDTOInter>) engin.findAllCassette(RepID);
        return AllRecords;
    }

    @Override
    public int findInTemp(int ID) throws Throwable {
        RepAddDAOInter engin = DAOFactory.createRepAddDAO();
        return engin.findInTemp(ID);
    }
}
