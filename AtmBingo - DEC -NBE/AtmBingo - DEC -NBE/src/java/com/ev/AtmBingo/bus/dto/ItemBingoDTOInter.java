/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface ItemBingoDTOInter extends Serializable {

    int getItemId();

    String getItemName();

    void setItemId(int ItemId);

    void setItemName(String ItemName);

}
