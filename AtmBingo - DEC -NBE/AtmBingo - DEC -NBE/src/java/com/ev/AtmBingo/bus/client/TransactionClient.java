/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import java.math.*;
import java.text.*;
import java.util.*;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.TransactionsBOInter;
import com.ev.AtmBingo.bus.dto.FileColumnDefinitionDTOInter;
import java.util.List;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.ColumnsDefinitionBOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.MatchedDataDTOInter;
import com.ev.AtmBingo.bus.dto.MatchingTypeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.DualListModel;

/**
 *
 * @author ISLAM
 */
public class TransactionClient extends BaseBean implements Serializable {

    private List<ColumnDTOInter> mcList;
    private List<FileColumnDefinitionDTOInter> mcDefaultList;
    private List<MatchedDataDTOInter> mDataList;
    private TransactionsBOInter mcObject;
    private Session utillist = new Session();
    private List<String> transFill;
    private List<String> transFill_op;
    private String sType1;
    private String sType2;
    private Boolean toShow;
    private Boolean toShow_to;
    private Boolean toShow1;
    private Boolean toShow1_to, cardFlag, cardFlag1, accountFlag, accountFlag1;
    private Boolean toShow2;
    private Boolean toShow3;
    private Boolean toShow3_to, fileFlag1, fileFlag;
    private Boolean toShow4;
    private Boolean toShow4_to;
    private Boolean showSelection;
    private Boolean settledFlag;
    private Date tranDateF;
    private Date tranDateT;
    private Date settledDateF;
    private Date settledDateT;
    private Date loadDateF;
    private Date loadDateT;
    private Integer amountF, autoComp;
    private Integer listSize;
    private Integer amountT;
    private String totalAmount;
    private Integer seqF;
    private Integer seqT;
    private String cardSt;
    private String col1_op, col2_op, col3_op, col4_op, col5_op;
    private String col1St, col2St, col3St, col4St, col5St;
    private String message;
    private String accountSt;
    private Integer atmcString;
    private Boolean blBool;
    private Session sessionutil;
    private MatchingTypeDTOInter masterRecord;
    private TransactionsBOInter coloumDefinition;
    private List<AtmMachineDTOInter> atmList;
    private List<TransactionTypeDTOInter> ttList;
    private List<TransactionStatusDTOInter> tsList;
    private List<CurrencyMasterDTOInter> cmList;
    private List<MatchingTypeDTOInter> mtList;
    private List<MatchingTypeDTOInter> mrList;
    private List<UsersDTOInter> uSettledList;
    private List<TransactionResponseCodeDTOInter> rcList;
    private List<AtmGroupDTOInter> atmgList;
    private List<String> settled;
    private String tDate;
    private String masterNew;
    private Integer teDate;
    private String sDate;
    private String showSide, showSide_1;
    private String lDate;
    private String sortBy;
    private String sortBy1;
    private String sortBy2;
    private String amount;
    private String sequance;
    private String settledState, result;
    private String account_op;
    private String card_op;
    private int tsInt;
    private int cmInt;
    private String atmInt;
    private Integer newMaster;
    private int ttInt;
    private int rcInt;
    private Boolean deselectFlag, selectFlag, printCheck;
    private Integer mtInt;
    private Integer mtInt1;
    private Integer usInt;
    private Integer usInt1;
    private Integer atmgInt;
    private String comboValue;
    private String matchingChange;
    private String nameCollapse;
    private Boolean collapseField;
    private DualListModel<String> fcdPickList;
    private List<ColumnDTOInter> mcTargetList;
    private Boolean col1Flag, col2Flag, col3Flag, col4Flag, col5Flag;
    private List<MatchedDataDTOInter> otherSide;
    private MatchedDataDTOInter[] selectedItems;
    private List<String> colHolder;
    private List<ColumnDTOInter> column;
    private List<ColumnDTOInter> column1;
    private String searchParameter;
    private UsersDTOInter logedinUser;
    private Boolean matchFlag, disFlag, rejFlag, dlgFlag;
    private Integer selectedPage;
    private String searchMessage, col1name, col2name, col3name, col4name, col5name;
    private ColumnsDefinitionBOInter colObject;
    private List<ColumnDTOInter> colDefList;
    private String textfile, transfile, hiddenText, hiddenText1;
    private String textfile_other, transfile_other, hiddenText_other, hiddenText1_other;
    private Clob sa;

    public ColumnsDefinitionBOInter getColObject() {
        return colObject;
    }

    public void setColObject(ColumnsDefinitionBOInter colObject) {
        this.colObject = colObject;
    }

    public List<ColumnDTOInter> getColDefList() {
        return colDefList;
    }

    public void setColDefList(List<ColumnDTOInter> colDefList) {
        this.colDefList = colDefList;
    }

    public String getCol1name() {
        return col1name;
    }

    public void setCol1name(String col1name) {
        this.col1name = col1name;
    }

    public String getCol2name() {
        return col2name;
    }

    public void setCol2name(String col2name) {
        this.col2name = col2name;
    }

    public String getCol3name() {
        return col3name;
    }

    public void setCol3name(String col3name) {
        this.col3name = col3name;
    }

    public String getCol4name() {
        return col4name;
    }

    public void setCol4name(String col4name) {
        this.col4name = col4name;
    }

    public String getCol5name() {
        return col5name;
    }

    public void setCol5name(String col5name) {
        this.col5name = col5name;
    }

    public Boolean getCol1Flag() {
        return col1Flag;
    }

    public void setCol1Flag(Boolean col1Flag) {
        this.col1Flag = col1Flag;
    }

    public Boolean getCol2Flag() {
        return col2Flag;
    }

    public void setCol2Flag(Boolean col2Flag) {
        this.col2Flag = col2Flag;
    }

    public Boolean getCol3Flag() {
        return col3Flag;
    }

    public void setCol3Flag(Boolean col3Flag) {
        this.col3Flag = col3Flag;
    }

    public Boolean getCol4Flag() {
        return col4Flag;
    }

    public void setCol4Flag(Boolean col4Flag) {
        this.col4Flag = col4Flag;
    }

    public Boolean getCol5Flag() {
        return col5Flag;
    }

    public void setCol5Flag(Boolean col5Flag) {
        this.col5Flag = col5Flag;
    }

    public Boolean getFileFlag() {
        return fileFlag;
    }

    public void setFileFlag(Boolean fileFlag) {
        this.fileFlag = fileFlag;
    }

    public Boolean getFileFlag1() {
        return fileFlag1;
    }

    public void setFileFlag1(Boolean fileFlag1) {
        this.fileFlag1 = fileFlag1;
    }

    public Boolean getDlgFlag() {
        return dlgFlag;
    }

    public void setDlgFlag(Boolean dlgFlag) {
        this.dlgFlag = dlgFlag;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getAutoComp() {
        return autoComp;
    }

    public void setAutoComp(Integer autoComp) {
        this.autoComp = autoComp;
    }

    public String getShowSide_1() {
        return showSide_1;
    }

    public void setShowSide_1(String showSide_1) {
        this.showSide_1 = showSide_1;
    }

    public Boolean getAccountFlag() {
        return accountFlag;
    }

    public void setAccountFlag(Boolean accountFlag) {
        this.accountFlag = accountFlag;
    }

    public Boolean getAccountFlag1() {
        return accountFlag1;
    }

    public void setAccountFlag1(Boolean accountFlag1) {
        this.accountFlag1 = accountFlag1;
    }

    public Boolean getCardFlag() {
        return cardFlag;
    }

    public void setCardFlag(Boolean cardFlag) {
        this.cardFlag = cardFlag;
    }

    public Boolean getCardFlag1() {
        return cardFlag1;
    }

    public void setCardFlag1(Boolean cardFlag1) {
        this.cardFlag1 = cardFlag1;
    }

    public String getHiddenText1_other() {
        return hiddenText1_other;
    }

    public void setHiddenText1_other(String hiddenText1_other) {
        this.hiddenText1_other = hiddenText1_other;
    }

    public String getHiddenText_other() {
        return hiddenText_other;
    }

    public void setHiddenText_other(String hiddenText_other) {
        this.hiddenText_other = hiddenText_other;
    }

    public String getTextfile_other() {
        return textfile_other;

    }

    public void setTextfile_other(String textfile_other) {
        this.textfile_other = textfile_other;
    }

    public String getTransfile_other() {
        return transfile_other;
    }

    public void setTransfile_other(String transfile_other) {
        this.transfile_other = transfile_other;
    }

    public String getHiddenText() {
        return hiddenText;
    }

    public void setHiddenText(String hiddenText) {
        this.hiddenText = hiddenText;
    }

    public String getHiddenText1() {
        return hiddenText1;
    }

    public void setHiddenText1(String hiddenText1) {
        this.hiddenText1 = hiddenText1;
    }

    public Clob getSa() {
        return sa;
    }

    public void setSa(Clob sa) {
        this.sa = sa;
    }

    public String getTextfile() {
        return textfile;
    }

    public void setTextfile(String textfile) {
        this.textfile = textfile;
    }

    public String getTransfile() {
        return transfile;
    }

    public void setTransfile(String transfile) {
        this.transfile = transfile;
    }

    public String getSearchMessage() {
        return searchMessage;
    }

    public void setSearchMessage(String searchMessage) {
        this.searchMessage = searchMessage;
    }

    public Boolean getDeselectFlag() {
        return deselectFlag;
    }

    public void setDeselectFlag(Boolean deselectFlag) {
        this.deselectFlag = deselectFlag;
    }

    public Boolean getSelectFlag() {
        return selectFlag;
    }

    public void setSelectFlag(Boolean selectFlag) {
        this.selectFlag = selectFlag;
    }

    public Boolean getPrintCheck() {
        return printCheck;
    }

    public TransactionsBOInter getMcObject() {
        return mcObject;
    }

    public void setMcObject(TransactionsBOInter mcObject) {
        this.mcObject = mcObject;
    }

    public String getCol1_op() {
        return col1_op;
    }

    public void setCol1_op(String col1_op) {
        this.col1_op = col1_op;
    }

    public String getCol2_op() {
        return col2_op;
    }

    public void setCol2_op(String col2_op) {
        this.col2_op = col2_op;
    }

    public String getCol3_op() {
        return col3_op;
    }

    public void setCol3_op(String col3_op) {
        this.col3_op = col3_op;
    }

    public String getCol4_op() {
        return col4_op;
    }

    public void setCol4_op(String col4_op) {
        this.col4_op = col4_op;
    }

    public String getCol5_op() {
        return col5_op;
    }

    public void setCol5_op(String col5_op) {
        this.col5_op = col5_op;
    }

    public String getCol1St() {
        return col1St;
    }

    public void setCol1St(String col1St) {
        this.col1St = col1St;
    }

    public String getCol2St() {
        return col2St;
    }

    public void setCol2St(String col2St) {
        this.col2St = col2St;
    }

    public String getCol3St() {
        return col3St;
    }

    public void setCol3St(String col3St) {
        this.col3St = col3St;
    }

    public String getCol4St() {
        return col4St;
    }

    public void setCol4St(String col4St) {
        this.col4St = col4St;
    }

    public String getCol5St() {
        return col5St;
    }

    public void setCol5St(String col5St) {
        this.col5St = col5St;
    }

    public UsersDTOInter getLogedinUser() {
        return logedinUser;
    }

    public void setLogedinUser(UsersDTOInter logedinUser) {
        this.logedinUser = logedinUser;
    }

    public void setPrintCheck(Boolean printCheck) {
        this.printCheck = printCheck;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    // <editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public Boolean getRejFlag() {
        return rejFlag;
    }

    public void setRejFlag(Boolean rejFlag) {
        this.rejFlag = rejFlag;
    }

    public List<ColumnDTOInter> getColumn1() {
        return column1;
    }

    public void setColumn1(List<ColumnDTOInter> column1) {
        this.column1 = column1;
    }

    public Integer getSelectedPage() {
        return selectedPage;
    }

    public void setSelectedPage(Integer selectedPage) {
        this.selectedPage = selectedPage;
    }

    public Boolean getDisFlag() {
        return disFlag;
    }

    public void setDisFlag(Boolean disFlag) {
        this.disFlag = disFlag;
    }

    public Boolean getMatchFlag() {
        return matchFlag;
    }

    public void setMatchFlag(Boolean matchFlag) {
        this.matchFlag = matchFlag;
    }

    public String getMasterNew() {
        return masterNew;
    }

    public void setMasterNew(String masterNew) {
        this.masterNew = masterNew;
    }

    public String getShowSide() {
        return showSide;
    }

    public void setShowSide(String showSide) {
        this.showSide = showSide;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNameCollapse() {
        return nameCollapse;
    }

    public void setNameCollapse(String nameCollapse) {
        this.nameCollapse = nameCollapse;
    }

    public Boolean getToShow_to() {
        return toShow_to;
    }

    public void setToShow_to(Boolean toShow_to) {
        this.toShow_to = toShow_to;
    }

    public Boolean getToShow1_to() {
        return toShow1_to;
    }

    public void setToShow1_to(Boolean toShow1_to) {
        this.toShow1_to = toShow1_to;
    }

    public Boolean getSettledFlag() {
        return settledFlag;
    }

    public void setSettledFlag(Boolean settledFlag) {
        this.settledFlag = settledFlag;
    }

    public Boolean getToShow3_to() {
        return toShow3_to;
    }

    public void setToShow3_to(Boolean toShow3_to) {
        this.toShow3_to = toShow3_to;
    }

    public Boolean getToShow4_to() {
        return toShow4_to;
    }

    public void setToShow4_to(Boolean toShow4_to) {
        this.toShow4_to = toShow4_to;
    }

    public String getSettledState() {
        return settledState;
    }

    public void setSettledState(String settledState) {
        this.settledState = settledState;
    }

    public String getSearchParameter() {
        return searchParameter;
    }

    public void setSearchParameter(String searchParameter) {
        this.searchParameter = searchParameter;
    }

    public Boolean getCollapseField() {
        return collapseField;
    }

    public void setCollapseField(Boolean collapseField) {
        this.collapseField = collapseField;
    }

    public Integer getNewMaster() {
        return newMaster;
    }

    public void setNewMaster(Integer newMaster) {
        this.newMaster = newMaster;
    }

    public List<ColumnDTOInter> getColumn() {
        return column;
    }

    public List<MatchedDataDTOInter> getOtherSide() {
        return otherSide;
    }

    public void setOtherSide(List<MatchedDataDTOInter> otherSide) {
        this.otherSide = otherSide;
    }

    public Boolean getShowSelection() {
        return showSelection;
    }

    public void setShowSelection(Boolean showSelection) {
        this.showSelection = showSelection;
    }

    public void setColumn(List<ColumnDTOInter> column) {
        this.column = column;
    }

    public String getsType1() {
        return sType1;

    }

    public List<String> getColHolder() {
        return colHolder;
    }

    public void setColHolder(List<String> colHolder) {
        this.colHolder = colHolder;
    }

    public DualListModel<String> getFcdPickList() {
        return fcdPickList;
    }

    public MatchedDataDTOInter[] getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(MatchedDataDTOInter[] selectedItems) {
        this.selectedItems = selectedItems;
    }

    public String getAccount_op() {
        return account_op;
    }

    public void setAccount_op(String account_op) {
        this.account_op = account_op;
    }

    public String getCard_op() {
        return card_op;
    }

    public void setCard_op(String card_op) {
        this.card_op = card_op;
    }

    public List<String> getTransFill_op() {
        return transFill_op;
    }

    public void setTransFill_op(List<String> transFill_op) {
        this.transFill_op = transFill_op;
    }

    public Integer getAtmcString() {
        return atmcString;
    }

    public void setAtmcString(Integer atmcString) {
        this.atmcString = atmcString;
    }

    public Boolean getBlBool() {
        return blBool;
    }

    public void setBlBool(Boolean blBool) {
        this.blBool = blBool;
    }

    public void setFcdPickList(DualListModel<String> fcdPickList) {
        this.fcdPickList = fcdPickList;
    }

    public List<MatchedDataDTOInter> getmDataList() {
        return mDataList;
    }

    public void setmDataList(List<MatchedDataDTOInter> mDataList) {
        this.mDataList = mDataList;
    }

    public void setsType1(String sType1) {
        this.sType1 = sType1;
    }

    public String getSortBy1() {
        return sortBy1;
    }

    public void setSortBy1(String sortBy1) {
        this.sortBy1 = sortBy1;
    }

    public String getSortBy2() {
        return sortBy2;
    }

    public void setSortBy2(String sortBy2) {
        this.sortBy2 = sortBy2;
    }

    public String getsType2() {
        return sType2;
    }

    public void setsType2(String sType2) {
        this.sType2 = sType2;
    }

    public TransactionsBOInter getColoumDefinition() {
        return coloumDefinition;
    }

    public void setColoumDefinition(TransactionsBOInter coloumDefinition) {
        this.coloumDefinition = coloumDefinition;
    }

    public String getMatchingChange() {
        return matchingChange;
    }

    public void setMatchingChange(String matchingChange) {
        this.matchingChange = matchingChange;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public List<AtmMachineDTOInter> getAtmList() {
        return atmList;
    }

    public Integer getAtmgInt() {
        return atmgInt;
    }

    public MatchingTypeDTOInter getMasterRecord() {
        return masterRecord;
    }

    public void setMasterRecord(MatchingTypeDTOInter masterRecord) {
        this.masterRecord = masterRecord;
    }

    public void setAtmgInt(Integer atmgInt) {
        this.atmgInt = atmgInt;
    }

    public List<MatchingTypeDTOInter> getMrList() {
        return mrList;
    }

    public void setMrList(List<MatchingTypeDTOInter> mrList) {
        this.mrList = mrList;
    }

    public void setAtmList(List<AtmMachineDTOInter> atmList) {
        this.atmList = atmList;
    }

    public Integer getTeDate() {
        return teDate;
    }

    public void setTeDate(Integer teDate) {
        this.teDate = teDate;
    }

    public String getComboValue() {
        return comboValue;
    }

    public void setComboValue(String comboValue) {
        this.comboValue = comboValue;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getlDate() {
        return lDate;
    }

    public void setlDate(String lDate) {
        this.lDate = lDate;
    }

    public String getsDate() {
        return sDate;
    }

    public void setsDate(String sDate) {
        this.sDate = sDate;
    }

    public List<String> getSettled() {
        return settled;
    }

    public void setSettled(List<String> settled) {
        this.settled = settled;
    }

    public String getSequance() {
        return sequance;
    }

    public void setSequance(String sequance) {
        this.sequance = sequance;
    }

    public String gettDate() {
        return tDate;
    }

    public void settDate(String tDate) {
        this.tDate = tDate;
    }

    public String getAtmInt() {
        return atmInt;
    }

    public void setAtmInt(String atmInt) {
        this.atmInt = atmInt;
    }

    public int getCmInt() {
        return cmInt;
    }

    public void setCmInt(int cmInt) {
        this.cmInt = cmInt;
    }

    public Integer getMtInt() {
        return mtInt;
    }

    public void setMtInt(Integer mtInt) {
        this.mtInt = mtInt;
    }

    public Integer getMtInt1() {
        return mtInt1;
    }

    public void setMtInt1(Integer mtInt1) {
        this.mtInt1 = mtInt1;
    }

    public int getRcInt() {
        return rcInt;
    }

    public void setRcInt(int rcInt) {
        this.rcInt = rcInt;
    }

    public int getTsInt() {
        return tsInt;
    }

    public void setTsInt(int tsInt) {
        this.tsInt = tsInt;
    }

    public int getTtInt() {
        return ttInt;
    }

    public void setTtInt(int ttInt) {
        this.ttInt = ttInt;
    }

    public Integer getUsInt() {
        return usInt;
    }

    public void setUsInt(Integer usInt) {
        this.usInt = usInt;
    }

    public Integer getUsInt1() {
        return usInt1;
    }

    public void setUsInt1(Integer usInt1) {
        this.usInt1 = usInt1;
    }

    public List<CurrencyMasterDTOInter> getCmList() {
        return cmList;
    }

    public void setCmList(List<CurrencyMasterDTOInter> cmList) {
        this.cmList = cmList;
    }

    public List<MatchingTypeDTOInter> getMtList() {
        return mtList;
    }

    public void setMtList(List<MatchingTypeDTOInter> mtList) {
        this.mtList = mtList;
    }

    public List<TransactionResponseCodeDTOInter> getRcList() {
        return rcList;
    }

    public void setRcList(List<TransactionResponseCodeDTOInter> rcList) {
        this.rcList = rcList;
    }

    public List<TransactionStatusDTOInter> getTsList() {
        return tsList;
    }

    public void setTsList(List<TransactionStatusDTOInter> tsList) {
        this.tsList = tsList;
    }

    public List<AtmGroupDTOInter> getAtmgList() {
        return atmgList;
    }

    public void setAtmgList(List<AtmGroupDTOInter> atmgList) {
        this.atmgList = atmgList;
    }

    public List<TransactionTypeDTOInter> getTtList() {
        return ttList;
    }

    public void setTtList(List<TransactionTypeDTOInter> ttList) {
        this.ttList = ttList;
    }

    public List<UsersDTOInter> getuSettledList() {
        return uSettledList;
    }

    public void setuSettledList(List<UsersDTOInter> uSettledList) {
        this.uSettledList = uSettledList;
    }

    public List<FileColumnDefinitionDTOInter> getMcDefaultList() {
        return mcDefaultList;
    }

    public void setMcDefaultList(List<FileColumnDefinitionDTOInter> mcDefaultList) {
        this.mcDefaultList = mcDefaultList;
    }

    public Boolean getToShow() {
        return toShow;
    }

    public void setToShow(Boolean toShow) {
        this.toShow = toShow;
    }

    public List<String> getTransFill() {
        return transFill;
    }

    public void setTransFill(List<String> transFill) {
        this.transFill = transFill;
    }

    public List<ColumnDTOInter> getMcList() {
        return mcList;
    }

    public void setMcList(List<ColumnDTOInter> mcList) {
        this.mcList = mcList;
    }

    public Boolean getToShow1() {
        return toShow1;
    }

    public void setToShow1(Boolean toShow1) {
        this.toShow1 = toShow1;
    }

    public Boolean getToShow2() {
        return toShow2;
    }

    public void setToShow2(Boolean toShow2) {
        this.toShow2 = toShow2;
    }

    public Boolean getToShow3() {
        return toShow3;
    }

    public void setToShow3(Boolean toShow3) {
        this.toShow3 = toShow3;
    }

    public Boolean getToShow4() {
        return toShow4;
    }

    public List<ColumnDTOInter> getMcTargetList() {
        return mcTargetList;
    }

    public Integer getAmountF() {

        return amountF;
    }

    public void setAmountF(Integer amountF) {
        this.amountF = amountF;
    }

    public Integer getAmountT() {

        return amountT;
    }

    public void setAmountT(Integer amountT) {
        this.amountT = amountT;
    }

    public Date getLoadDateF() {
        return loadDateF;
    }

    public void setLoadDateF(Date loadDateF) {
        this.loadDateF = loadDateF;
    }

    public Date getLoadDateT() {
        return loadDateT;
    }

    public void setLoadDateT(Date loadDateT) {
        this.loadDateT = loadDateT;
    }

    public Integer getSeqF() {

        return seqF;
    }

    public void setSeqF(Integer seqF) {
        this.seqF = seqF;
    }

    public Integer getSeqT() {

        return seqT;
    }

    public void setSeqT(Integer seqT) {
        this.seqT = seqT;
    }

    public Date getSettledDateF() {
        return settledDateF;
    }

    public void setSettledDateF(Date settledDateF) {
        this.settledDateF = settledDateF;
    }

    public Date getSettledDateT() {
        return settledDateT;
    }

    public void setSettledDateT(Date settledDateT) {
        this.settledDateT = settledDateT;
    }

    public String getAccountSt() {
        return accountSt;
    }

    public void setAccountSt(String accountSt) {
        this.accountSt = accountSt;
    }

    public String getCardSt() {
        return cardSt;
    }

    public void setCardSt(String cardSt) {
        this.cardSt = cardSt;
    }

    public Date getTranDateF() {
        return tranDateF;
    }

    public void setTranDateF(Date tranDateF) {
        this.tranDateF = tranDateF;
    }

    public Date getTranDateT() {
        return tranDateT;
    }

    public void setTranDateT(Date tranDateT) {
        this.tranDateT = tranDateT;
    }

    public void setMcTargetList(List<ColumnDTOInter> mcTargetList) {
        this.mcTargetList = mcTargetList;
    }

    public void setToShow4(Boolean toShow4) {
        this.toShow4 = toShow4;
    }
    // </editor-fold>

    /**
     * Creates a new instance of TransactionClient
     */
    public TransactionClient() throws Throwable {
        super();
        super.GetAccess();
        selectFlag = true;
        deselectFlag = false;
        selectedPage = 2;
        fileFlag = true;
        fileFlag1 = true;
        disFlag = true;
        autoComp = 0;
        matchFlag = false;
        atmgInt = 0;
        rejFlag = false;
        settledState = "Mark as settled";
        showSelection = false;
        column = new ArrayList<ColumnDTOInter>();
        column1 = new ArrayList<ColumnDTOInter>();
        mDataList = new ArrayList<MatchedDataDTOInter>();
        otherSide = new ArrayList<MatchedDataDTOInter>();
        mcObject = BOFactory.createTransactionBO(null);
        mcList = (List<ColumnDTOInter>) utillist.GetColumnsList();
        nameCollapse = "Close";
        toShow = true;
        toShow_to = false;
        toShow1 = false;
        toShow1_to = true;
        toShow3_to = true;
        toShow4_to = true;
        toShow2 = true;
        toShow3 = false;
        toShow4 = false;
        accountFlag = true;
        atmInt = null;
        cardFlag = true;
        dlgFlag = false;
        accountFlag1 = false;
        cardFlag1 = false;
        tDate = "Between";
        tranDateF = super.getYesterdaytrans((String) utillist.GetTransTime().getParameterValue());
        tranDateT = super.getTodaytrans((String) utillist.GetTransTime().getParameterValue());
        collapseField = true;
        settledFlag = false;
        seqT = null;
        mcTargetList = new ArrayList<ColumnDTOInter>();
        fcdPickList = (DualListModel<String>) mcObject.getPickColumn();
        FacesContext context = FacesContext.getCurrentInstance();

        sessionutil = new Session();
        logedinUser = sessionutil.GetUserLogging();
        masterRecord = DTOFactory.createMatchingTypeDTO();
        masterRecord.setType1(1);
        masterRecord.setType2(2);
        fillCombos();
        getcolname();

        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");

    }

    public void getcolname() throws Throwable {
        col5Flag = true;
        col4Flag = true;
        col3Flag = true;
        col2Flag = true;
        col1Flag = true;
        colObject = BOFactory.createColumnDefinitionBO(null);
        colDefList = (List<ColumnDTOInter>) utillist.GetColumnsList();
        for (ColumnDTOInter record : colDefList) {
            if (record.getDbName().toUpperCase().trim().equals("COLUMN1")) {
                col1name = record.getHeader();
            }
            if (record.getDbName().toUpperCase().trim().equals("COLUMN2")) {
                col2name = record.getHeader();
            }
            if (record.getDbName().toUpperCase().trim().equals("COLUMN3")) {
                col3name = record.getHeader();
            }
            if (record.getDbName().toUpperCase().trim().equals("COLUMN4")) {
                col4name = record.getHeader();
            }
            if (record.getDbName().toUpperCase().trim().equals("COLUMN5")) {
                col5name = record.getHeader();
            }
        }

    }

    public void fillCombos() throws Throwable {

        transFill_op = super.getTrans_op();
        transFill = super.getTrans();
        mcList = (List<ColumnDTOInter>) utillist.GetColumnsList();
        atmList = (List<AtmMachineDTOInter>) utillist.GetAtmMachineList(sessionutil.GetUserLogging());
        cmList = (List<CurrencyMasterDTOInter>) utillist.GetCurrencyMasterList();
        mtList = (List<MatchingTypeDTOInter>) utillist.GetMatchingTypeList();
        masterRecord = mtList.get(masterRecord.getId());
        if (masterRecord.getType1() == 1) {
            sType1 = "Journal";

        } else {
            sType1 = "Switch";

        }

        if (masterRecord.getType2() == 2) {
            sType2 = "Switch";

        } else {
            sType2 = "Host";
        }
        rcList = (List<TransactionResponseCodeDTOInter>) utillist.GetTransactionResponseCodeList();
        uSettledList = (List<UsersDTOInter>) utillist.GetSettledUsersList();
        ttList = (List<TransactionTypeDTOInter>) utillist.GetTransactionTypeList();
        tsList = (List<TransactionStatusDTOInter>) utillist.GetTransactionStatusList();
        atmgList = (List<AtmGroupDTOInter>) utillist.GetATMGroupList();
        mcTargetList = new ArrayList<ColumnDTOInter>();
        settled = super.getSettledList();

        mtInt1 = 1;
        mtInt = 1;

    }

    public void valueChangeEffect(ValueChangeEvent e) {
        selectedPage = (Integer) e.getNewValue();
        if (selectedPage == 1) {
            matchFlag = true;
            disFlag = false;
            rejFlag = false;
        } else if (selectedPage == 2) {
            matchFlag = false;
            disFlag = true;
            rejFlag = false;
        } else {
            matchFlag = false;
            disFlag = false;
            rejFlag = true;
        }
        nameCollapse = "Close";
    }

    public void groupChange(ValueChangeEvent e) {
        int grpId = (Integer) e.getNewValue();
        try {
            atmList = (List<AtmMachineDTOInter>) mcObject.getAtmMachines(sessionutil.GetUserLogging(), grpId);
        } catch (Throwable ex) {
            Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void selectingCombo(ValueChangeEvent e) {
        comboValue = e.getNewValue().toString();
        if (comboValue.equals("Between")) {
            toShow = true;
            toShow_to = false;
        } else {
            toShow = false;
            toShow_to = true;
        }
    }

    public void selectingCombo1(ValueChangeEvent e) {
        comboValue = e.getNewValue().toString();
        if (comboValue.equals("Between")) {
            toShow1 = true;
            toShow1_to = false;
        } else {
            toShow1 = false;
            toShow1_to = true;
        }
    }

    public void selectingCombo2(ValueChangeEvent e) {
        comboValue = e.getNewValue().toString();
        if (comboValue.equals("Between")) {
            toShow2 = false;
        } else {
            toShow2 = true;
        }
    }

    public void resetVars() {
        toShow = false;
        toShow1 = false;
        toShow2 = false;
        toShow3 = false;
        toShow4 = false;

    }

    public void selectingCombo3(ValueChangeEvent e) {
        comboValue = e.getNewValue().toString();
        if (comboValue.equals("Between")) {
            toShow3 = true;
            toShow3_to = false;

        } else {
            toShow3 = false;
            toShow3_to = true;
        }
    }

    public void selectingCombo4(ValueChangeEvent e) {
        comboValue = e.getNewValue().toString();
        if (comboValue.equals("Between")) {
            toShow4 = true;
            toShow4_to = false;

        } else {
            toShow4 = false;
            toShow4_to = true;
        }
    }

    public void matchChange(ValueChangeEvent e) {
        newMaster = (Integer) e.getNewValue();
        for (MatchingTypeDTOInter masRec : mtList) {
            if ((Integer) e.getNewValue() == masRec.getId()) {
                masterRecord = masRec;
            }
        }
        if (masterRecord.getType1() == 1) {
            sType1 = "Journal";
        } else {
            sType1 = "Switch";
        }

        if (masterRecord.getType2() == 2) {
            sType2 = "Switch";
        } else {
            sType2 = "Host";
        }
    }

    public void col5Change(ValueChangeEvent e) {
        String newValue = e.getNewValue().toString();
        if (newValue.equals("is null") || newValue.equals("is not null")) {
            col5Flag = true;
        } else {
            col5Flag = true;
        }
    }

    public void col4Change(ValueChangeEvent e) {
        String newValue = e.getNewValue().toString();
        if (newValue.equals("is null") || newValue.equals("is not null")) {
            col4Flag = true;
        } else {
            col4Flag = true;
        }
    }

    public void col3Change(ValueChangeEvent e) {
        String newValue = e.getNewValue().toString();
        if (newValue.equals("is null") || newValue.equals("is not null")) {
            col3Flag = true;
        } else {
            col3Flag = true;
        }
    }

    public void col2Change(ValueChangeEvent e) {
        String newValue = e.getNewValue().toString();
        if (newValue.equals("is null") || newValue.equals("is not null")) {
            col2Flag = true;
        } else {
            col2Flag = true;
        }
    }

    public void col1Change(ValueChangeEvent e) {
        String newValue = e.getNewValue().toString();
        if (newValue.equals("is null") || newValue.equals("is not null")) {
            col1Flag = true;
        } else {
            col1Flag = true;
        }
    }

    public void doSearch() throws Throwable {
        dlgFlag = false;
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:trans"))).setFirst(0);
        if (fcdPickList.getTarget().size() == 0) {
            nameCollapse = "Search by";
        } else {
            List<String> target = fcdPickList.getTarget();
            column = mcObject.getColumns(target);
            column1 = column;
            //column1.remove(0);
            String srtb = "";
            for (int i = 0; i < 3; i++) {
                srtb += column.get(i).getDbName();
                if (i != 2) {
                    srtb += ",";
                }
            }
            String fields = "";
            for (int i = 0; i < column1.size(); i++) {
                fields += column1.get(i).getDbName();
                if (i != column1.size() - 1) {
                    fields += ",";
                }

            }
            List<MatchedDataDTOInter> mTempList;// = new ArrayList<MatchedDataDTOInter>();

            mTempList = (List<MatchedDataDTOInter>) mcObject.searchMatchedData(tranDateF, tDate, tranDateT, settledDateF, sDate, settledDateT, null, null, null,
                    amountF, amount, amountT, seqF, sequance, seqT, cardSt, card_op,
                    accountSt, account_op.replace(" ", ""), null, tsInt, ttInt, rcInt, cmInt,
                    atmcString, atmInt, atmgInt, blBool, 3, mtInt1, mtInt, 0, usInt, srtb, sessionutil.GetUserLogging(), fields, col1St, col2St, col3St, col4St, col5St, col1_op, col2_op, col3_op, col4_op, col5_op);

            otherSide = null;

//            if (teDate == 1) {
//                settledState = "Mark as not settled";
//            } else if (teDate == 2) {
//                settledState = "Mark as settled";
//            } else {
//                settledFlag = true;
//            }
            if (mtInt1 == 1) {
                if (mtInt == 1) {
                    showSide = "JR";
                    showSide_1 = "SW";
                } else {
                    showSide = "SW";
                    showSide_1 = "JR";
                }
            } else {
                if (mtInt == 2) {
                    showSide = "SW";
                    showSide_1 = "GL";
                } else {
                    showSide = "GL";
                    showSide_1 = "SW";
                }
            }
            column = mcObject.getColumns(target);
            column1 = column;
            //column1.remove(0);
            searchParameter = mcObject.getSearchParameter();
            BigDecimal someNumber = mcObject.getTotalAmount();
            NumberFormat nf = NumberFormat.getInstance();
            if (someNumber != null) {
                totalAmount = nf.format(someNumber);
            } else {
                totalAmount = "0";
            }
            mDataList = mTempList;
            showSelection = true;
            collapseField = true;
        }
        nameCollapse = "Search by";
        listSize = mDataList.size();
        printCheck = false;
        deselectFlag = false;
        selectFlag = true;
    }

    public void changeName() {
        if (nameCollapse.equals("Close")) {
            nameCollapse = "Search by";
        } else {
            nameCollapse = "Close";
        }
    }

    public void getFiletransaction(ActionEvent e) throws Throwable {
        MatchedDataDTOInter m = (MatchedDataDTOInter) e.getComponent().getAttributes().get("textfile");
        sa = (Clob) mcObject.FindFiletransaction(m);
        transfile = clobToString(sa);
        hiddenText = "";
        searchMessage = "";
        message = "";
    }

    public void gettexttransaction(ActionEvent e) throws Throwable {
        MatchedDataDTOInter m = (MatchedDataDTOInter) e.getComponent().getAttributes().get("texttrans");
        textfile = mcObject.gettexttransaction(m).toString();
        hiddenText = "";
        searchMessage = "";
        message = "";
    }

    public void getFiletransaction_other(ActionEvent e) throws Throwable {
        MatchedDataDTOInter m = (MatchedDataDTOInter) e.getComponent().getAttributes().get("textfile_other_mat");
        sa = (Clob) mcObject.FindFiletransaction(m);
        transfile = clobToString(sa);
        hiddenText1 = "";
        searchMessage = "";
        message = "";
    }

    public void gettexttransaction_other(ActionEvent e) throws Throwable {
        MatchedDataDTOInter m = (MatchedDataDTOInter) e.getComponent().getAttributes().get("texttrans_other");
        textfile_other = mcObject.gettexttransaction_other(m).toString();
        hiddenText_other = "";
        searchMessage = "";
        message = "";
    }

    public List<String> complete(String query) {
        List<String> results = new ArrayList<String>();
        for (int i = 0; i < atmList.size(); i++) {
            if (atmList.get(i).getApplicationId().toString().startsWith(query)) {
                results.add(atmList.get(i).getApplicationId().toString());
            }
        }
        return results;
    }

    public void printFile() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            Integer Seq = mcObject.PrintLines(hiddenText1);
            String path = (String) mcObject.PrintLinesReport(Seq.toString());
            response.sendRedirect(path);

        } catch (Exception ex) {
            Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void printFile_other() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            Integer Seq = mcObject.PrintLines(hiddenText1_other);
            String path = (String) mcObject.PrintLinesReport(Seq.toString());
            response.sendRedirect(path);

        } catch (Exception ex) {
            Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String TestAtmID(String atmidInt) {
        String MSG = "";
        for (int i = 0; i < atmList.size(); i++) {
            if (atmList.get(i).getApplicationId().toString().equals(atmidInt)) {
                return "Found";
            }
        }
        return "ATM " + atmidInt + " is not assigned for this user";
    }

    public void printOrg_other() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            Integer Seq = mcObject.PrintLines(textfile_other);
            String path = (String) mcObject.PrintLinesReport(Seq.toString());
            response.sendRedirect(path);

        } catch (Exception ex) {
            Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void printOrg() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            Integer Seq = mcObject.PrintLines(textfile);
            String path = (String) mcObject.PrintLinesReport(Seq.toString());
            response.sendRedirect(path);

        } catch (Exception ex) {
            Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static byte[] stringToBytesASCII(String str) {
        char[] buffer = str.toCharArray();
        byte[] b = new byte[buffer.length];
        for (int i = 0; i < b.length; i++) {
            b[i] = (byte) buffer[i];
        }
        return b;
    }

    public byte[] CreateArray(byte[] a, byte[] b) {
        byte[] result = new byte[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    public String clobToString(Clob key) {
        if (key != null) {
            Object value = null;
            {
                java.io.InputStream is = null;
                try {
                    is = key.getAsciiStream();
                    byte[] buffer = new byte[4096];
                    byte[] breakpoint = new byte[4096];
                    breakpoint = stringToBytesASCII("braaa");
                    java.io.OutputStream outputStream = new java.io.ByteArrayOutputStream();
                    while (true) {
                        int read = is.read(buffer);
                        if (read == -1) {
                            break;
                        }
                        outputStream.write(CreateArray(buffer, breakpoint), 0, read);

                    }
                    outputStream.close();
                    is.close();

                    value = outputStream.toString();
                } catch (SQLException ex) {
                    Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
                } catch (java.io.IOException io_ex) {
                } finally {
                    try {
                        is.close();
                    } catch (IOException ex) {
                        Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            return value.toString();
        } else {
            return "";
        }
    }

    public void print() throws Throwable {
        // column1.remove(0);
        if (mDataList.size() != 0 && column1.size() != 0) {
            String fields = "";
            for (int i = 0; i < column1.size(); i++) {
                if (column1.get(i).getId() == 1) {
                    column1.remove(i);
                }
                if ("ATM_NAME".equals(column1.get(i).getDbName().toUpperCase())) {
                    column1.remove(i);
                }
                if ("export_date".equals(column1.get(i).getDbName().toLowerCase())) {
                    column1.remove(i);
                }
                if ("export_user".equals(column1.get(i).getDbName().toLowerCase())) {
                    column1.remove(i);
                }
                if ("corrective_user".equals(column1.get(i).getDbName().toLowerCase())) {
                    column1.remove(i);
                }
                if ("corrective_date".equals(column1.get(i).getDbName().toLowerCase())) {
                    column1.remove(i);
                }
            }
            for (int i = 0; i < column1.size(); i++) {

                fields += column1.get(i).getDbName();
                if (i != column1.size() - 1) {
                    fields += ",";
                }
            }
            String path = (String) mcObject.findReport(fields, mcObject.getWhereCluase(), sessionutil.GetUserLogging(), column1, selectedItems, sessionutil.Getclient());

            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect(path);
        }
    }

    public void markAsDispute() throws Throwable {
        mcObject.MatchedMarkAsDisputes(selectedItems, sessionutil.GetUserLogging());
        for (MatchedDataDTOInter m : selectedItems) {
            mDataList.remove(m);
        }
        searchMessage = "";
        message = "";
    }

    public void markAsSettled() throws Throwable {
        mcObject.MatchedMarkAsSettled(selectedItems, sessionutil.GetUserLogging());
        searchMessage = "";
        message = "";
    }

    public void showOtherSide(ActionEvent e) throws Throwable {
        MatchedDataDTOInter m = (MatchedDataDTOInter) e.getComponent().getAttributes().get("otherOther");
        otherSide = (List<MatchedDataDTOInter>) mcObject.getAnotherSideOfMatchedData(m);
        searchMessage = "";
        message = "";
    }

    public void selectAllRows() {
        DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:trans");
        repTable.setSelection(mDataList.toArray());
        printCheck = true;
        deselectFlag = true;
        selectFlag = false;
        searchMessage = "";
        message = "";
    }

    public void deselectAllRows() {
        printCheck = false;
        deselectFlag = false;
        selectFlag = true;
        searchMessage = "";
        message = "";
    }

    public void accountChange(ValueChangeEvent e) {
        String newValue = e.getNewValue().toString();
        if (newValue.equals("is null") || newValue.equals("is not null")) {
            accountFlag = false;
            accountFlag1 = true;
        } else {
            accountFlag = true;
            accountFlag1 = false;
        }
    }

    public void cardChange(ValueChangeEvent e) {
        String newValue = e.getNewValue().toString();
        if (newValue.equals("is null") || newValue.equals("is not null")) {
            cardFlag = false;
            cardFlag1 = true;
        } else {
            cardFlag = true;
            cardFlag1 = false;
        }
    }

    public void masterRecordChange(ValueChangeEvent e) {
        Integer newV = (Integer) e.getNewValue();
        if (newV == 1) {
            fileFlag = true;
            fileFlag1 = false;
        } else {
            fileFlag = false;
            fileFlag1 = true;
        }
    }

    public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Calendar calobj = Calendar.getInstance();
        System.out.println("Dialog: " + df.format(calobj.getTime()));

        session.invalidate();

    }
}
