/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.ExportBoInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.ExportLogDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class ExportClient extends BaseBean  implements Serializable{

    private List<AtmMachineDTOInter> atmList;
    private Date fromDate, toDate;
    private Integer userId, ATMID, state;
    private ExportBoInter bingoBo;
    private List<UsersDTOInter> userList;
    private List<ExportLogDTOInter> logList;
    private String message;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getATMID() {
        return ATMID;
    }

    public void setATMID(Integer ATMID) {
        this.ATMID = ATMID;
    }

    public List<AtmMachineDTOInter> getAtmList() {
        return atmList;
    }

    public void setAtmList(List<AtmMachineDTOInter> atmList) {
        this.atmList = atmList;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public List<ExportLogDTOInter> getLogList() {
        return logList;
    }

    public void setLogList(List<ExportLogDTOInter> logList) {
        this.logList = logList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<UsersDTOInter> getUserList() {
        return userList;
    }

    public void setUserList(List<UsersDTOInter> userList) {
        this.userList = userList;
    }

    /** Creates a new instance of SettelmentLogClient */
    public ExportClient() throws Throwable {
        super();
        super.GetAccess();
        FacesContext context = FacesContext.getCurrentInstance();
         
        
        
         Session sessionutil = new Session();
        UsersDTOInter userHolded = sessionutil.GetUserLogging();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        bingoBo = BOFactory.createExportBo(null);
        atmList = (List<AtmMachineDTOInter>) bingoBo.getAtmMachines(userHolded);
        userList = (List<UsersDTOInter>) bingoBo.getUsers(super.getRestrected());
    }

    public void doSearch() {
        try {

            logList = (List<ExportLogDTOInter>) bingoBo.getExportLogged(fromDate, toDate, userId, ATMID, state);

        } catch (Throwable ex) {
        }
    }

    public void resetVars() {
        message = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
