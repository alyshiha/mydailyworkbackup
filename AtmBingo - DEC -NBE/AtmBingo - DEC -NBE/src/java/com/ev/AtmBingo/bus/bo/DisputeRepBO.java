/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.DisputeRepDAOInter;
import com.ev.AtmBingo.bus.dto.DisputeRepDTOInter;
import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class DisputeRepBO extends BaseBO implements DisputeRepBOInter {

    protected DisputeRepBO() {
        super();
    }

    @Override
    public List<DisputeRepDTOInter> runReportrxcel(String dateFrom, String dateTo, String atmID, Integer uservalue, Integer group) throws Throwable {
        DisputeRepDAOInter arrRepDAO = DAOFactory.createDisputeRepDAO();
        List<DisputeRepDTOInter> rs = (List<DisputeRepDTOInter>) arrRepDAO.findDataTable(dateFrom, dateTo, atmID, uservalue, group);
        return rs;
    }

    @Override
    public String runReport(String dateFrom, String dateTo, String atmID, String user, String cust, Integer uservalue, Integer group) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        JRResultSetDataSource resultSetDataSource;
        DisputeRepDAOInter arrRepDAO = DAOFactory.createDisputeRepDAO();

        ResultSet rs = (ResultSet) arrRepDAO.findReport(dateFrom, dateTo, atmID, uservalue, group);
        resultSetDataSource = new JRResultSetDataSource(rs);

        params.put("DateFrom", dateFrom);
        params.put("user", user);
        params.put("DateTo", dateTo);
        params.put("AtmID", "" + atmID);
        params.put("customer", cust);
        params.put("createdby", "" + uservalue);
        params.put("ATMGroup", "" + group);

        try {

            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/DisputeRep.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);

            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, resultSetDataSource);

            Calendar c = Calendar.getInstance();
            String uri = "DisputeRep" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {

                String x = pdfPath + uri;

                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                resultSetDataSource = null;
                CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());
                rs.close();

                return x;

            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();

            return "fail";
        }

    }

}
