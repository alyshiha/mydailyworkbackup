/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.presdto;

import com.ev.AtmBingo.base.presdto.BasePresDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class FileColumnDefinitionPresDTO extends BasePresDTO implements FileColumnDefinitionPresDTOInter, Serializable {

    private int id,DISPLAYINLOADING,DUPLICATENO;
    private String name;
    private String columnName,cname;
    private int dataType;
    private String property;

  public int getDISPLAYINLOADING() {
        return DISPLAYINLOADING;
    }

    public void setDISPLAYINLOADING(int DISPLAYINLOADING) {
        this.DISPLAYINLOADING = DISPLAYINLOADING;
    }
  public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }
    public int getDUPLICATENO() {
        return DUPLICATENO;
    }

    public void setDUPLICATENO(int DUPLICATENO) {
        this.DUPLICATENO = DUPLICATENO;
    }

    protected FileColumnDefinitionPresDTO() {
    }



    public String getColumnName() {
        return this.columnName;
    }

    public int getDataType() {
        return this.dataType;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProperty() {
        return this.property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

}
