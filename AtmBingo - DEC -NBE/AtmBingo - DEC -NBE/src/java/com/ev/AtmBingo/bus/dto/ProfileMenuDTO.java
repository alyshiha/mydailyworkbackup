/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class ProfileMenuDTO extends BaseDTO implements Serializable , ProfileMenuDTOInter{

    protected ProfileMenuDTO() {
        super();
    }
    private int ProfileId;
    private int MenuId;

    public int getMenuId() {
        return MenuId;
    }

    public void setMenuId(int MenuId) {
        this.MenuId = MenuId;
    }

    public int getProfileId() {
        return ProfileId;
    }

    public void setProfileId(int ProfileId) {
        this.ProfileId = ProfileId;
    }

}
