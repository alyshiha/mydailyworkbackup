/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Administrator
 */
public class MatchingTypeSettingDTO extends BaseDTO implements MatchingTypeSettingDTOInter, Serializable {

    private int id;
    private int columnId;
    private int partSetting;
    private BigDecimal partLength;
    private int matchingType;

    protected MatchingTypeSettingDTO() {
    }

    public int getColumnId() {
        return columnId;
    }

    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatchingType() {
        return matchingType;
    }

    public void setMatchingType(int matchingType) {
        this.matchingType = matchingType;
    }

    public BigDecimal getPartLength() {
        return partLength;
    }

    public void setPartLength(BigDecimal partLength) {
        this.partLength = partLength;
    }

    public int getPartSetting() {
        return partSetting;
    }

    public void setPartSetting(int partSetting) {
        this.partSetting = partSetting;
    }
    
}
