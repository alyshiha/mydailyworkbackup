/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.ATMMAchineLICDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.ATMMAchineLICInter;
import com.ev.AtmBingo.bus.dto.LincManagmentDetailsDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ATMMachineLICBO extends BaseBO implements ATMMachineLICBOINTER,Serializable {

    protected ATMMachineLICBO() {
    }

    public Object getAtmMachine() throws Throwable {
        ATMMAchineLICDAOInter amtDAO = DAOFactory.CreateATMMAchineLICDAO();
        List<ATMMAchineLICInter> res = new ArrayList<ATMMAchineLICInter>();
        res = (List<ATMMAchineLICInter>) amtDAO.find();
        return res;
    }

    public Object FilterAtmMachine(List<ATMMAchineLICInter> AtmList,String Like) throws Throwable {
        ATMMAchineLICDAOInter amtDAO = DAOFactory.CreateATMMAchineLICDAO();
        List<ATMMAchineLICInter> res = new ArrayList<ATMMAchineLICInter>();
        res = (List<ATMMAchineLICInter>) amtDAO.findfilter(AtmList,Like);
        return res;
    }

    public Object LicMangDetail() throws Throwable {
        ATMMAchineLICDAOInter amtDAO = DAOFactory.CreateATMMAchineLICDAO();
        LincManagmentDetailsDTOInter LIC = (LincManagmentDetailsDTOInter) amtDAO.LicMangDetail();
        return LIC;
    }

    public Object GetLIC() throws Throwable {
        ATMMAchineLICDAOInter amtDAO = DAOFactory.CreateATMMAchineLICDAO();
        Integer LIC = (Integer) amtDAO.GetLIC();
        return LIC;
    }

    public void Delete(List<ATMMAchineLICInter> List) throws Throwable {
        ATMMAchineLICDAOInter amtDAO = DAOFactory.CreateATMMAchineLICDAO();
        amtDAO.Delete(List);
        return;
    }

    public void SAVE(List<ATMMAchineLICInter> List) throws Throwable {
        ATMMAchineLICDAOInter amtDAO = DAOFactory.CreateATMMAchineLICDAO();
        amtDAO.save(List, "Del");
        return;
    }
}
