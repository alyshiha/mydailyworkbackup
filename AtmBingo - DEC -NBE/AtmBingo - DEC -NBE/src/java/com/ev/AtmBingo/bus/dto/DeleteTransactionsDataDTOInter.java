/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface DeleteTransactionsDataDTOInter extends Serializable {

    Date getSearchDateTo();
    int getATMid();

    int getFileid();

    Date getSearchDateFrom();

    Date getStartDate();

    int getUserId();

    void setATMid(int ATMid);

    void setFileid(int Fileid);

    void setSearchDateFrom(Date SearchDateFrom);

    void setSearchDateTo(Date SearchDateTo);

    void setStartDate(Date StartDate);

    void setUserId(int userId);

}
