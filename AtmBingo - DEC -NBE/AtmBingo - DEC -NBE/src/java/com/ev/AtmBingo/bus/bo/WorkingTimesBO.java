/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.SystemTimesDAOInter;
import com.ev.AtmBingo.bus.dto.SystemTimesDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class WorkingTimesBO extends BaseBO implements WorkingTimesBOIntert,Serializable{

    protected WorkingTimesBO() {
        super();
    }

    public Object getSystemTime()throws Throwable{
        SystemTimesDAOInter stDAO = DAOFactory.createSystemTimesDAO(null);
        List<SystemTimesDTOInter> stDTOL = (List<SystemTimesDTOInter>)stDAO.findAll();
        return stDTOL;
    }

    public Object editeSystemTimes(SystemTimesDTOInter stDTO, int operation)throws Throwable{
        SystemTimesDAOInter stDAO = DAOFactory.createSystemTimesDAO(null);
        switch(operation){
            case INSERT:
                stDAO.insert(stDTO);
                return "Insert Done";
            case UPDATE:
                stDAO.update(stDTO);
                return "Update Done";
            case DELETE:
                stDAO.delete(stDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }

}
