/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class RepAddDTO implements RepAddDTOInter {

    private int id;
    private Date dateFrom;
    private Date dateTo;
    private int atmId;
    private String atmapplicationid;
    private String createdby;
    private int createdbyid;
    private Date createddate;
    private int repid;

    public int getCreatedbyid() {
        return createdbyid;
    }

    public void setCreatedbyid(int createdbyid) {
        this.createdbyid = createdbyid;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Date getDateFrom() {
        return dateFrom;
    }

    @Override
    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Override
    public Date getDateTo() {
        return dateTo;
    }

    @Override
    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    @Override
    public int getAtmId() {
        return atmId;
    }

    @Override
    public void setAtmId(int atmId) {
        this.atmId = atmId;
    }

    @Override
    public String getAtmapplicationid() {
        return atmapplicationid;
    }

    @Override
    public void setAtmapplicationid(String atmapplicationid) {
        this.atmapplicationid = atmapplicationid;
    }

    @Override
    public String getCreatedby() {
        return createdby;
    }

    @Override
    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    @Override
    public Date getCreateddate() {
        return createddate;
    }

    @Override
    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    @Override
    public int getRepid() {
        return repid;
    }

    @Override
    public void setRepid(int repid) {
        this.repid = repid;
    }
    
   
}
