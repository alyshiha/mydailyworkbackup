/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import java.io.Serializable;

/**
 *
 * @author KhAiReE
 */
public interface UserAuditRepBOInter extends Serializable {

    Object print(String user, String customer) throws Throwable;

}
