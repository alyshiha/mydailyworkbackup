/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class RepTransDetailDTO implements RepTransDetailDTOInter {

    private Integer disputekey;
    private Integer recordtypevalue;
    private Integer matchingtype;
    private String atmapplicationid;
    private String currency;
    private String recordtype;
    private String settledflag;
    private String corrflag;
    private Date transactiondate;
    private String transactionsequence;
    private Integer amount;
    private String transactiontype;
    private String cardno;
    private String customeraccountnumber;
    private String responsecode;
    private String transactionstatus;
    private String amt;
    private String iss;
    private String ref;
    private String notespresented;
    private Date settleddate;
    private String comments;
    private Integer blacklist;
    private Integer rowidnumber;

    public RepTransDetailDTO() {
    }

    public RepTransDetailDTO(String atmapplicationid, String currency, String recordtype, String settledflag, Date transactiondate, String transactionsequence, Integer amount, String transactiontype, String cardno, String customeraccountnumber, String responsecode, String transactionstatus, String amt, String iss, String ref, String notespresented, Date settleddate, String comments) {
        this.atmapplicationid = atmapplicationid;
        this.currency = currency;
        this.recordtype = recordtype;
        this.settledflag = settledflag;
        this.transactiondate = transactiondate;
        this.transactionsequence = transactionsequence;
        this.amount = amount;
        this.transactiontype = transactiontype;
        this.cardno = cardno;
        this.customeraccountnumber = customeraccountnumber;
        this.responsecode = responsecode;
        this.transactionstatus = transactionstatus;
        this.amt = amt;
        this.iss = iss;
        this.ref = ref;
        this.notespresented = notespresented;
        this.settleddate = settleddate;
        this.comments = comments;
    }

    public Integer getDisputekey() {
        return disputekey;
    }

    public void setDisputekey(Integer disputekey) {
        this.disputekey = disputekey;
    }

    public Integer getRecordtypevalue() {
        return recordtypevalue;
    }

    public void setRecordtypevalue(Integer recordtypevalue) {
        this.recordtypevalue = recordtypevalue;
    }

    public String getCorrflag() {
        return corrflag;
    }

    public void setCorrflag(String corrflag) {
        this.corrflag = corrflag;
    }

    public Integer getMatchingtype() {
        return matchingtype;
    }

    public void setMatchingtype(Integer matchingtype) {
        this.matchingtype = matchingtype;
    }

    @Override
    public Integer getBlacklist() {
        return blacklist;
    }

    @Override
    public void setBlacklist(Integer blacklist) {
        this.blacklist = blacklist;
    }

    public Integer getRowidnumber() {
        return rowidnumber;
    }

    public void setRowidnumber(Integer rowidnumber) {
        this.rowidnumber = rowidnumber;
    }

    @Override
    public String getAtmapplicationid() {
        return atmapplicationid;
    }

    @Override
    public void setAtmapplicationid(String atmapplicationid) {
        this.atmapplicationid = atmapplicationid;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String getRecordtype() {
        return recordtype;
    }

    @Override
    public void setRecordtype(String recordtype) {
        this.recordtype = recordtype;
    }

    @Override
    public String getSettledflag() {
        return settledflag;
    }

    @Override
    public void setSettledflag(String settledflag) {
        this.settledflag = settledflag;
    }

    @Override
    public Date getTransactiondate() {
        return transactiondate;
    }

    @Override
    public void setTransactiondate(Date transactiondate) {
        this.transactiondate = transactiondate;
    }

    @Override
    public String getTransactionsequence() {
        return transactionsequence;
    }

    @Override
    public void setTransactionsequence(String transactionsequence) {
        this.transactionsequence = transactionsequence;
    }

    @Override
    public Integer getAmount() {
        return amount;
    }

    @Override
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String getTransactiontype() {
        return transactiontype;
    }

    @Override
    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    @Override
    public String getCardno() {
        return cardno;
    }

    @Override
    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    @Override
    public String getCustomeraccountnumber() {
        return customeraccountnumber;
    }

    @Override
    public void setCustomeraccountnumber(String customeraccountnumber) {
        this.customeraccountnumber = customeraccountnumber;
    }

    @Override
    public String getResponsecode() {
        return responsecode;
    }

    @Override
    public void setResponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

    @Override
    public String getTransactionstatus() {
        return transactionstatus;
    }

    @Override
    public void setTransactionstatus(String transactionstatus) {
        this.transactionstatus = transactionstatus;
    }

    @Override
    public String getAmt() {
        return amt;
    }

    @Override
    public void setAmt(String amt) {
        this.amt = amt;
    }

    @Override
    public String getIss() {
        return iss;
    }

    @Override
    public void setIss(String iss) {
        this.iss = iss;
    }

    @Override
    public String getRef() {
        return ref;
    }

    @Override
    public void setRef(String ref) {
        this.ref = ref;
    }

    @Override
    public String getNotespresented() {
        return notespresented;
    }

    @Override
    public void setNotespresented(String notespresented) {
        this.notespresented = notespresented;
    }

    @Override
    public Date getSettleddate() {
        return settleddate;
    }

    @Override
    public void setSettleddate(Date settleddate) {
        this.settleddate = settleddate;
    }

    @Override
    public String getComments() {
        return comments;
    }

    @Override
    public void setComments(String comments) {
        this.comments = comments;
    }

}
