/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.UserActivityBOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UserActivityDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

/**
 *
 * @author ISLAM
 */
public class UserActivityClient extends BaseBean implements Serializable {

    /** Creates a new instance of UserActivityClient */
    private UserActivityBOInter uaObject;
    private UserActivityDTOInter userADTO;
    private List<UserActivityDTOInter> uaList;
    private CartesianChartModel userModel;
    private CartesianChartModel disputeModel;
    private int maxSett, maxDis;

    public int getMaxDis() {
        return maxDis;
    }

    public void setMaxDis(int maxDis) {
        this.maxDis = maxDis;
    }

    public int getMaxSett() {
        return maxSett;
    }

    public void setMaxSett(int maxSett) {
        this.maxSett = maxSett;
    }

    public CartesianChartModel getDisputeModel() {
        return disputeModel;
    }

    public void setDisputeModel(CartesianChartModel disputeModel) {
        this.disputeModel = disputeModel;
    }

    public CartesianChartModel getUserModel() {
        return userModel;
    }

    public void setUserModel(CartesianChartModel userModel) {
        this.userModel = userModel;
    }

    public List<UserActivityDTOInter> getUaList() throws Throwable {
        return uaList;
    }

    public void setUaList(List<UserActivityDTOInter> uaList) {
        this.uaList = uaList;
    }

    public UserActivityClient() throws Throwable {
//        super();
        uaObject = BOFactory.createUserActivityBO(null);
        uaList = (List<UserActivityDTOInter>) uaObject.getGraph();
        userADTO = DTOFactory.createUserActivityDTO();
        userModel = new CartesianChartModel();
        disputeModel = new CartesianChartModel();
        ChartSeries settledSeries = new ChartSeries();
        ChartSeries disputeSeries = new ChartSeries();
        disputeSeries.setLabel("Dispute");
        settledSeries.setLabel("Settled");
        int dismax =0,setmax =0;
        for (int i = 0; i < uaList.size(); i++) {
            disputeSeries.set(uaList.get(i).getuDTO().getUserName(), uaList.get(i).getCountDis());
            settledSeries.set(uaList.get(i).getuDTO().getUserName(), uaList.get(i).getCountSett());
            if(dismax <  uaList.get(i).getCountDis()){dismax = uaList.get(i).getCountDis();}
            if(setmax <  uaList.get(i).getCountSett()){setmax = uaList.get(i).getCountSett();}
        }

        maxDis =  dismax;
        maxSett = setmax;

        if (uaList.size() > 0) {
//            maxDis = uaList.get(uaList.size() - 1).getCountDis();
//            maxDis = ((maxDis / 100) * 25) + uaList.get(uaList.size() - 1).getCountDis();
            maxDis += 0.1 * maxDis;
//            maxSett = uaList.get(uaList.size() - 1).getCountSett();
//            maxSett = Math.round((float) (((maxSett / 100) * 25) + uaList.get(uaList.size() - 1).getCountSett()));
            maxSett += 0.1 * maxSett;
        }
        userModel.addSeries(settledSeries);
        disputeModel.addSeries(disputeSeries);
     FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
