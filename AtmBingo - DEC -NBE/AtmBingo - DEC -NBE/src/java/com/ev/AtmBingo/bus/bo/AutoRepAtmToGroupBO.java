/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.AutoRepAtmToGroupDAOInter;
import com.ev.AtmBingo.bus.dao.CassetteDAOInter;
import com.ev.AtmBingo.bus.dao.CurrencyMasterDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.AutoRepDTOInter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 *
 */
public class AutoRepAtmToGroupBO extends BaseBO implements AutoRepAtmToGroupBoInter ,Serializable{
public Object AddGroup(AutoRepDTOInter amtDTO) throws Throwable{
    AutoRepAtmToGroupDAOInter cDAO = DAOFactory.createAutoRepAtmToGroupDAO();
        cDAO.AddGroup(amtDTO);
        return "Insert Done";}

     public Object DeleteGroup(AutoRepDTOInter amtDTO)throws Throwable{
      AutoRepAtmToGroupDAOInter cDAO = DAOFactory.createAutoRepAtmToGroupDAO();
        cDAO.DeleteGroup(amtDTO);
        return "Insert Done";
  }

    public Object UpdateGroup(AutoRepDTOInter amtDTO)throws Throwable{
      AutoRepAtmToGroupDAOInter cDAO = DAOFactory.createAutoRepAtmToGroupDAO();
        cDAO.UpdateGroup(amtDTO);
        return "Insert Done";
  }
  public Object saveGroup(Integer sourceList, String[] targetList,Integer selectedSource, String[] selectedTarget)throws Throwable{
      AutoRepAtmToGroupDAOInter cDAO = DAOFactory.createAutoRepAtmToGroupDAO();
        cDAO.saveGroup(sourceList, targetList, selectedSource, selectedTarget);
        return "Insert Done";
  }
  public Object unsaveGroup(Integer sourceList, String[] targetList,Integer selectedSource, String[] selectedTarget)throws Throwable{
      AutoRepAtmToGroupDAOInter cDAO = DAOFactory.createAutoRepAtmToGroupDAO();
        cDAO.unsaveGroup(sourceList, targetList, selectedSource, selectedTarget);
        return "Insert Done";
  }


    public Object getTargetList(int userId, int CassetteGroup) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<String> target = new ArrayList<String>();
        target = amDAO.findUserCassetteMachines(userId, CassetteGroup);
        return target;
    }
     public Object getTargetList2(int userId, int CassetteGroup,int status,Integer repgroup) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<String> target = new ArrayList<String>();
        target = amDAO.findUserUnAssign(userId, CassetteGroup,status,repgroup);
        return target;
    }

    public Object getSourceList(int userId, int CassetteGroup) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<String> target = new ArrayList<String>();
        target = amDAO.findNotUserCassetteMachines(userId, CassetteGroup);
        return target;
    }

    public Object getCurrencyMaster() throws Throwable {
        CurrencyMasterDAOInter cmDAO = DAOFactory.createCurrencyMasterDAO(null);
        List<CurrencyMasterDTOInter> cmDTOL = (List<CurrencyMasterDTOInter>) cmDAO.findAll();
        return cmDTOL;
    }

    public Object getCassetes() throws Throwable {
        CassetteDAOInter cDAO = DAOFactory.createCassetteDAO(null);
        List<CassetteDTOInter> cDTOL = (List<CassetteDTOInter>) cDAO.findAll();
        return cDTOL;
    }
    public Object getGroups() throws Throwable {
        AutoRepAtmToGroupDAOInter cDAO = DAOFactory.createAutoRepAtmToGroupDAO();
        List<AutoRepDTOInter> cDTOL = (List<AutoRepDTOInter>) cDAO.findAllcassettegroup();
        return cDTOL;
    }
  public Object getAll(Integer groupid) throws Throwable {
        AutoRepAtmToGroupDAOInter cDAO = DAOFactory.createAutoRepAtmToGroupDAO();
        List<AutoRepDTOInter> cDTOL = (List<AutoRepDTOInter>) cDAO.findAll(groupid);
        return cDTOL;
    }
     public Object editeAtmGroup(AutoRepDTOInter amtDTO, int operation)throws Throwable{
        AutoRepAtmToGroupDAOInter amtDAO = DAOFactory.createAutoRepAtmToGroupDAO();
        switch(operation){
            case INSERT:
                 amtDAO.InsertAtmGroup(amtDTO);
                return "Insert Done";
            case UPDATE:
               amtDAO.AtmGroup(amtDTO);
                return "Update Done";
            case DELETE:
                amtDAO.deleteAtmGroup(amtDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }
}

