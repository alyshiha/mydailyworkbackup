/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.MatchingSettingBOInter;
import com.ev.AtmBingo.bus.presdto.AtmMachineTypePresDTOInter;
import com.ev.AtmBingo.bus.presdto.FileColumnDefinitionPresDTOInter;
import com.ev.AtmBingo.bus.presdto.MatchingTypeMachinesPresDTOInter;
import com.ev.AtmBingo.bus.presdto.MatchingTypePresDTOInter;
import com.ev.AtmBingo.bus.presdto.MatchingTypeSettingPresDTOInter;
import com.ev.AtmBingo.bus.presdto.PresDTOFactory;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author Administrator
 */
public class MatchingSettingClient extends BaseBean implements Serializable {

    /** Creates a new instance of MatchingSettingClient */
    private List<MatchingTypePresDTOInter> matchingMTL;
    private List<MatchingTypeMachinesPresDTOInter> matchingMTML;
    private List<MatchingTypeSettingPresDTOInter> matchingMTSL;
    private List<AtmMachineTypePresDTOInter> amtL;
    private List<FileColumnDefinitionPresDTOInter> fcdL;
    private MatchingSettingBOInter myObject;
    private MatchingTypePresDTOInter selectedMT;
    private MatchingTypeMachinesPresDTOInter selectedATM;
    private MatchingTypeMachinesPresDTOInter newMatching;
    private MatchingTypeSettingPresDTOInter newSetting;
    private MatchingTypeMachinesPresDTOInter myAttr;
    private MatchingTypeMachinesPresDTOInter myAttr1;
    private MatchingTypeSettingPresDTOInter myAttr2;
    private MatchingTypePresDTOInter myAttr_mt;
    private Boolean disableAdd;
    private Boolean disableAddMatching;
    private Boolean showAdd;
    private Boolean showConfirm;
    private Boolean dialogShow;
    private Boolean showAddMatching;
    private Boolean showConfirmMatching;
    private Integer oldMT;
    private Integer oldCID;
    private Integer oldPS;
    private BigDecimal oldPL;
    private Boolean mtActive;
    private Boolean mtDefault;
    private Boolean dialogShowMatching;
    private String message;
    private String message_mt;
    private String message_mtms;

    public MatchingTypeMachinesPresDTOInter getSelectedATM() {
        return selectedATM;
    }

    public void setSelectedATM(MatchingTypeMachinesPresDTOInter selectedATM) {
        this.selectedATM = selectedATM;
    }

    

// <editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public String getMessage_mtms() {
        return message_mtms;
    }

    public void setMessage_mtms(String message_mtms) {
        this.message_mtms = message_mtms;
    }

    public BigDecimal getOldPL() {
        return oldPL;
    }

    public void setOldPL(BigDecimal oldPL) {
        this.oldPL = oldPL;
    }

    public Boolean getMtDefault() {
        return mtDefault;
    }

    public void setMtDefault(Boolean mtDefault) {
        this.mtDefault = mtDefault;
    }

    public String getMessage_mt() {
        return message_mt;
    }

    public void setMessage_mt(String message_mt) {
        this.message_mt = message_mt;
    }

    public Boolean getDialogShowMatching() {
        return dialogShowMatching;
    }

    public Boolean getDisableAddMatching() {
        return disableAddMatching;
    }

    public void setDisableAddMatching(Boolean disableAddMatching) {
        this.disableAddMatching = disableAddMatching;
    }

    public void setDialogShowMatching(Boolean dialogShowMatching) {
        this.dialogShowMatching = dialogShowMatching;
    }

    public Boolean getShowAddMatching() {
        return showAddMatching;
    }

    public void setShowAddMatching(Boolean showAddMatching) {
        this.showAddMatching = showAddMatching;
    }

    public Boolean getShowConfirmMatching() {
        return showConfirmMatching;
    }

    public void setShowConfirmMatching(Boolean showConfirmMatching) {
        this.showConfirmMatching = showConfirmMatching;
    }

    public Boolean getDialogShow() {
        return dialogShow;
    }

    public void setDialogShow(Boolean dialogShow) {
        this.dialogShow = dialogShow;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getDisableAdd() {
        return disableAdd;
    }

    public void setDisableAdd(Boolean disableAdd) {
        this.disableAdd = disableAdd;
    }

    public List<FileColumnDefinitionPresDTOInter> getFcdL() {
        return fcdL;
    }

    public void setFcdL(List<FileColumnDefinitionPresDTOInter> fcdL) {
        this.fcdL = fcdL;
    }

    public List<AtmMachineTypePresDTOInter> getAmtL() {
        return amtL;
    }

    public void setAmtL(List<AtmMachineTypePresDTOInter> amtL) {
        this.amtL = amtL;
    }

    public List<MatchingTypePresDTOInter> getMatchingMTL() {
        return matchingMTL;
    }

    public void setMatchingMTL(List<MatchingTypePresDTOInter> matchingMTL) {
        this.matchingMTL = matchingMTL;
    }

    public List<MatchingTypeMachinesPresDTOInter> getMatchingMTML() {
        return matchingMTML;
    }

    public void setMatchingMTML(List<MatchingTypeMachinesPresDTOInter> matchingMTML) {
        this.matchingMTML = matchingMTML;
    }

    public Integer getOldCID() {
        return oldCID;
    }

    public void setOldCID(Integer oldCID) {
        this.oldCID = oldCID;
    }

    public Integer getOldMT() {
        return oldMT;
    }

    public void setOldMT(Integer oldMT) {
        this.oldMT = oldMT;
    }

    public Integer getOldPS() {
        return oldPS;
    }

    public void setOldPS(Integer oldPS) {
        this.oldPS = oldPS;
    }

    public Boolean getMtActive() {
        return mtActive;
    }

    public void setMtActive(Boolean mtActive) {
        this.mtActive = mtActive;
    }

    public List<MatchingTypeSettingPresDTOInter> getMatchingMTSL() {
        return matchingMTSL;
    }

    public void setMatchingMTSL(List<MatchingTypeSettingPresDTOInter> matchingMTSL) {
        this.matchingMTSL = matchingMTSL;
    }// </editor-fold>

    public MatchingSettingClient() throws Throwable {
        super();
        super.GetAccess();
        myObject = BOFactory.createMatchingSettingBO(null);
        matchingMTL = myObject.findAllMatchingType();
        selectedMT = PresDTOFactory.creatMatchingTypePresDTO();
        showAdd = true;
        disableAdd = true;
        disableAddMatching = true;
        showConfirm = false;
        showAddMatching = true;
        showConfirmMatching = false;
        dialogShow = false;
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void resetVars() {
        message_mt = "";
        message = "";
        message_mtms = "";
    }

// <editor-fold defaultstate="collapsed" desc="Matching Tyoe">
    public void fillMatch(ActionEvent e) {
        myAttr_mt = (MatchingTypePresDTOInter) e.getComponent().getAttributes().get("updateMatchingType");
        resetVars();
    }

    public void updateMatchingType(ActionEvent e) throws Throwable {
        try {
            myObject.editMatchingType(myAttr_mt, myObject.UPDATE);
            message_mt = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message_mt = "Error, please repeat your changes";
        }
    }

    public void mtActive(ValueChangeEvent e) {
        mtActive = (Boolean) e.getOldValue();
    }

    public void setOldActive(ActionEvent e) {
        myAttr_mt.setActiv(mtActive);
    }
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Selecting Rows">
    public void onRowSelect1(SelectEvent e) throws Throwable {

        selectedMT = (MatchingTypePresDTOInter) e.getObject();
        matchingMTML = selectedMT.getMatchingTypeMachinesL();
        amtL = myObject.findAllAtmMachinesType();
        matchingMTSL = null;
        selectedATM = null;
        resetVars();
        disableAdd = false;
    }

    public void onRowUnSelect1(UnselectEvent e) {
        matchingMTSL = null;
        matchingMTML = null;
        resetVars();
        selectedATM = null;
        disableAddMatching = true;
        disableAdd = true;

    }

    public void onRowSelect2(SelectEvent e) throws Throwable {
        disableAddMatching = false;
        selectedATM = (MatchingTypeMachinesPresDTOInter) e.getObject();
        matchingMTSL = selectedATM.getMatchingTypeSettingL();
        resetVars();
        fcdL = myObject.findAllFileColumnDefinition();
        if (matchingMTSL == null) {
            matchingMTSL = new ArrayList<MatchingTypeSettingPresDTOInter>();
        }
    }

    public void onRowUnSelect2(UnselectEvent e) {
        matchingMTSL = null;
        disableAddMatching = true;
        resetVars();
    }// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Machine methods">
    public void getOldMachineType(ValueChangeEvent e) {
        oldMT = (Integer) e.getOldValue();
    }

    public void defaultOld(ValueChangeEvent e) {
        mtDefault = (Boolean) e.getOldValue();
    }

    public void ConfirmedAdd() {
        try {
            myObject.editMatchingTypeMachines(newMatching, 0, myObject.INSERT);
            message = super.showMessage(SUCC);
            //save(message);
            showAdd = true;
            showConfirm = false;
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void addMachineType() {

        if (matchingMTML == null) {
            matchingMTML = new ArrayList<MatchingTypeMachinesPresDTOInter>();
        }
        newMatching = PresDTOFactory.creatMatchingTypeMachinesPresDTO();


        if (amtL.size() == matchingMTML.size()) {

            message = super.showMessage(MACH_TYPE);

            //save(message);
        } else {
            matchingMTML.add(newMatching);
            newMatching.setMatchingType(selectedMT.getId());
            showAdd = false;
            showConfirm = true;
            resetVars();

        }
    }

    public void fillAttr(ActionEvent e) {
        myAttr = (MatchingTypeMachinesPresDTOInter) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void deleteMachineType(ActionEvent e) throws Throwable {

        try {
            showAdd = true;
            showConfirm = false;
            matchingMTML.remove(myAttr);
            myObject.editMatchingTypeMachines(myAttr, 0, myObject.DELETE);
            message = super.showMessage(SUCC);
            showAdd = true;
            showConfirm = false;
        } catch (Throwable ex) {
            message = super.showMessage(SUCC);
        }
    }

    public void updateMachineType(ActionEvent e) throws Throwable {
        try {
            if (showAdd == false) {
                message = super.showMessage(CONF_FIRST);
            } else if (oldMT == null) {
                oldMT = myAttr1.getMachineType();
                myObject.editMatchingTypeMachines(myAttr1, oldMT, myObject.UPDATE);
                message = super.showMessage(SUCC);
            } else {
                myObject.editMatchingTypeMachines(myAttr1, oldMT, myObject.UPDATE);
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
            myAttr1.setMachineType(oldMT);
            myAttr1.setDefaultMatchSet(mtDefault);
        }
    }

    public void fillAttrUpdate(ActionEvent e) {
        myAttr1 = (MatchingTypeMachinesPresDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    public void setOldValues(ActionEvent e) {
        if (mtDefault != null) {
            myAttr1.setDefaultMatchSet(mtDefault);
        }
        if (oldMT != null) {
            myAttr1.setMachineType(oldMT);
        }
    }
    // </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Matching Settings Methods">
    public void oldPartLenght(ValueChangeEvent e) {
        oldPL = (BigDecimal) e.getOldValue();
    }

    public void getOldColoumnID(ValueChangeEvent e) {
        oldCID = (Integer) e.getOldValue();
    }

    public void getOldPartSetting(ValueChangeEvent e) {
        oldPS = (Integer) e.getOldValue();
    }

    public void fillAttrdel(ActionEvent e) {
        myAttr2 = (MatchingTypeSettingPresDTOInter) e.getComponent().getAttributes().get("removedMatching");
        resetVars();
    }

    public void deleteMatchingSetting(ActionEvent e) throws Throwable {
        try {

            matchingMTSL.remove(myAttr2);
            myObject.editeMatchingSetting(myAttr2, 0, 0, myObject.DELETE);
            message_mtms = super.showMessage(SUCC);
            showAddMatching = true;
            showConfirmMatching = false;
        } catch (Throwable ex) {
            message_mtms = "Error, please delete again";
        }

    }

    public void fillAttrUp(ActionEvent e) {
        myAttr2 = (MatchingTypeSettingPresDTOInter) e.getComponent().getAttributes().get("updateMatching");
        resetVars();
    }

    public void updateMatchingSetting(ActionEvent e) throws Throwable {
        try {
            if (showAddMatching == false) {
                message_mtms = super.showMessage(CONF_FIRST);
            } else if (oldCID == null || oldMT == null || oldPS == null) {
                if (oldCID == null) {
                    oldCID = myAttr2.getColumnId();
                }
                if (oldMT == null) {
                    oldMT = myAttr2.getMatchingType();
                }
                if (oldPS == null) {
                    oldPS = myAttr2.getPartSetting();
                }
                if ((Boolean) myObject.editeMatchingSetting(myAttr2, oldCID, oldPS, myObject.UPDATE)) {
                    myObject.editeMatchingSetting(myAttr2, oldCID, oldPS, myObject.UPDATE);
                    message_mtms = super.showMessage(SUCC);
                } else {
                    message_mtms = super.showMessage(REC_EXIST);
                    matchingMTSL = selectedATM.getMatchingTypeSettingL();
                    myAttr2.setPartLength(oldPL);
                    myAttr2.setColumnId(oldCID);
                    myAttr2.setPartSetting(oldPS);
                }

            } else {
                myObject.editeMatchingSetting(myAttr2, oldCID, oldPS, myObject.UPDATE);
                message_mtms = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message_mtms = super.showMessage(REC_EXIST);
            myAttr2.setColumnId(oldCID);
            myAttr2.setPartSetting(oldPS);
            myAttr2.setPartLength(oldPL);
        }
    }

    public void setOldValuesMS(ActionEvent e) {
        myAttr2.setColumnId(oldCID);
        myAttr2.setPartSetting(oldPS);
        myAttr2.setPartLength(oldPL);
    }

    public void addMatchingSetting() throws Throwable {
        resetVars();
        if (matchingMTSL == null) {
            matchingMTSL = new ArrayList<MatchingTypeSettingPresDTOInter>();
        }

        newSetting = PresDTOFactory.creatMatchingTypeSettingPresDTO();
        newSetting.setPartSetting(1);
        newSetting.setColumnId(2);
        newSetting.setPartLength(null);
        newSetting.setId(selectedATM.getMatchingType());
        newSetting.setMatchingType(selectedATM.getMachineType());
        matchingMTSL.add(newSetting);
        selectedATM.setMatchingTypeSettingL(matchingMTSL);
        showAddMatching = false;
        showConfirmMatching = true;
    }

    public void confirmMatchingSetting() throws Throwable {
        try {
            myObject.editeMatchingSetting(newSetting, 0, 0, myObject.INSERT);
            showAddMatching = true;
            showConfirmMatching = false;
            message_mtms = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message_mtms = super.showMessage(REC_EXIST);
        }
    }
    // </editor-fold>

    public void RecreateIndex() throws Throwable {
        try {
            myObject.RecreateIndex();
            message_mtms = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message_mtms = super.showMessage(REC_EXIST);
        }
    }
  public void onIdle() {
        //System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
