/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.RepAddDTOInter;
import com.ev.AtmBingo.bus.dto.RepDetailTempDTOInter;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface RepAddDAOInter {

    Object delete(Object... obj) throws Throwable;

    Object find(Date todate, int atmid) throws Throwable;

    Object findAll() throws Throwable;

    String Excute(RepAddDTOInter record) throws Throwable;

    int insert(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

    int findInTemp(int ID) throws Throwable;

    Object findAllCassette(int RepID) throws Throwable;

    String save(List<RepDetailTempDTOInter> entities) throws SQLException, Throwable;
}
