/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class replanishmentHistoryRepDTO implements replanishmentHistoryRepDTOInter , Serializable{
    String fromdate,todate,cashfeed,remainingcash,dispendedcash,switchcash,disputecash,different,atm,currency;
@Override
    public String getCurrency() {
        return currency;
    }
@Override
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    
@Override
    public String getAtm() {
        return atm;
    }
@Override
    public void setAtm(String atm) {
        this.atm = atm;
    }

    @Override
    public String getCashfeed() {
        return cashfeed;
    }

    @Override
    public void setCashfeed(String cashfeed) {
        this.cashfeed = cashfeed;
    }

    @Override
    public String getDifferent() {
        return different;
    }

    @Override
    public void setDifferent(String different) {
        this.different = different;
    }

    @Override
    public String getDispendedcash() {
        return dispendedcash;
    }

    @Override
    public void setDispendedcash(String dispendedcash) {
        this.dispendedcash = dispendedcash;
    }

    @Override
    public String getDisputecash() {
        return disputecash;
    }

    @Override
    public void setDisputecash(String disputecash) {
        this.disputecash = disputecash;
    }

    @Override
    public String getFromdate() {
        return fromdate;
    }

    @Override
    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    @Override
    public String getRemainingcash() {
        return remainingcash;
    }

    @Override
    public void setRemainingcash(String remainingcash) {
        this.remainingcash = remainingcash;
    }

    @Override
    public String getSwitchcash() {
        return switchcash;
    }

    @Override
    public void setSwitchcash(String switchcash) {
        this.switchcash = switchcash;
    }

    @Override
    public String getTodate() {
        return todate;
    }

    @Override
    public void setTodate(String todate) {
        this.todate = todate;
    }
    
}
