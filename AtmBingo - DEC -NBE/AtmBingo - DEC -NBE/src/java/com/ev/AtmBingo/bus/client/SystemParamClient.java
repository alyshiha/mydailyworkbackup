/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.SystemParametersBOInter;
import com.ev.AtmBingo.bus.dto.SystemTableDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.io.Serializable;
import java.util.Date;
import java.util.Enumeration;
import javax.faces.event.ActionEvent;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class SystemParamClient extends BaseBean  implements Serializable{

    private List<SystemTableDTOInter> spList;
    private SystemParametersBOInter spObject;
    private SystemTableDTOInter spAttr;
    private SystemTableDTOInter spAttrUp;
    private SystemTableDTOInter newSP;
    private String message, oldValue;
    private Boolean showAdd;
    private Boolean showConfirm;
    private Integer listSize;

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }
    private Date[] dates;

    public Date[] getDates() {
        return dates;
    }

    public void setDates(Date[] dates) {
        this.dates = dates;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public SystemTableDTOInter getSpAttrUp() {
        return spAttrUp;
    }

    public void setSpAttrUp(SystemTableDTOInter spAttrUp) {
        this.spAttrUp = spAttrUp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SystemTableDTOInter getNewSP() {
        return newSP;
    }

    public void setNewSP(SystemTableDTOInter newSP) {
        this.newSP = newSP;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public SystemTableDTOInter getSpAttr() {
        return spAttr;
    }

    public void setSpAttr(SystemTableDTOInter spAttr) {
        this.spAttr = spAttr;
    }

    public List<SystemTableDTOInter> getSpList() {
        return spList;
    }

    public void setSpList(List<SystemTableDTOInter> spList) {
        this.spList = spList;
    }

    /** Creates a new instance of SystemParamClient */
    public SystemParamClient() throws Throwable {
        super();
        super.GetAccess();
        spObject = BOFactory.createSystemParaBO(null);
        spList = (List<SystemTableDTOInter>) spObject.getSystemTable();
        showAdd = true;
        showConfirm = false;
        listSize = spList.size();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void fillDelete(ActionEvent e) {
        spAttr = (SystemTableDTOInter) e.getComponent().getAttributes().get("removedRow");
        message = "";
    }

    public void deleteRecord() {
        try {
            spObject.edtiteSystemTable(spAttr, null, spObject.DELETE);
            spList.remove(spAttr);
            message = super.showMessage(SUCC);
            showAdd = true;
            showConfirm = false;
        } catch (Throwable ex) {
            message = ex.getMessage();
        }

    }

    public void fillUpdate(ActionEvent e) {
        spAttrUp = (SystemTableDTOInter) e.getComponent().getAttributes().get("updateRow");
        message = "";
    }

    public void updateRecord() {
        try {
            if (!showAdd) {
                message = super.showMessage(CONF_FIRST);
            } else if (spAttrUp.getParameterName().equals("") || spAttrUp.getParameterValue().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                if (oldValue == null) {
                    oldValue = spAttrUp.getParameterName();
                }
                spObject.edtiteSystemTable(spAttrUp, oldValue, spObject.UPDATE);
                message = super.showMessage(SUCC);
                showAdd = true;
                showConfirm = false;
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
            spAttrUp.setParameterName(oldValue);
        }
    }

    public void addRecord() {
        newSP = DTOFactory.createSystemTableDTO();
        spList.add(newSP);
        showAdd = false;
        showConfirm = true;
        resetVars();
    }

    public void confirmRecord() {
        try {
            if (newSP.getParameterName().equals("") || newSP.getParameterValue().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                spObject.edtiteSystemTable(newSP, null, spObject.INSERT);
                message = super.showMessage(SUCC);
                showAdd = true;
                showConfirm = false;
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void oldValue(ValueChangeEvent e) {
        oldValue = (String) e.getOldValue();
        resetVars();
    }

    public void resetVars() {
        message = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
