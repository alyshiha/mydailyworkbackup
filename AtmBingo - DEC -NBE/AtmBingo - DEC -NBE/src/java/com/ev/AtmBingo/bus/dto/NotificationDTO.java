/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class NotificationDTO extends BaseDTO implements NotificationDTOInter, Serializable{
    
    protected  NotificationDTO() {
        super();
    }
    
    Integer id ;
    String action ;
    Date actionDate ;
    Integer userId ;
    Integer type ;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

      public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
