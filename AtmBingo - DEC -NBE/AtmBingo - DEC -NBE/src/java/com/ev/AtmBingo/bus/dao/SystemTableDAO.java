/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.SystemTableDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class SystemTableDAO extends BaseDAO implements SystemTableDAOInter {

    protected SystemTableDAO() {
        super();
        super.setTableName("system_table");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SystemTableDTOInter uDTO = (SystemTableDTOInter) obj[0];
        String insertStat = "insert into $table values ('$pn', '$pv' , $pdt)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$pn", "" + uDTO.getParameterName());
        insertStat = insertStat.replace("$pv", "" + uDTO.getParameterValue());
        insertStat = insertStat.replace("$pdt", "" + uDTO.getParameterDataType());
        super.executeUpdate(insertStat);
        String action = "Add a new system parameter with name " + uDTO.getParameterName();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SystemTableDTOInter uDTO = (SystemTableDTOInter) obj[0];
        String oldName = (String) obj[1];
        String updateStat = "update $table set parameter_name = '$pn', parameter_value = '$pv', parameter_data_type = $pdt"
                + " where parameter_name = '$oldpn'";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$pn", "" + uDTO.getParameterName());
        updateStat = updateStat.replace("$pv", "" + uDTO.getParameterValue());
        updateStat = updateStat.replace("$pdt", "" + uDTO.getParameterDataType());
        updateStat = updateStat.replace("$oldpn", "" + oldName);
        super.executeUpdate(updateStat);
        String action = "Update system parameter with name " + uDTO.getParameterName();
        super.postUpdate(action, false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SystemTableDTOInter uDTO = (SystemTableDTOInter) obj[0];
        String deleteStat = "delete from $table where parameter_name = '$pn'";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$pn", "" + uDTO.getParameterName());
        super.executeUpdate(deleteStat);
        String action = "Delete system parameter with name " + uDTO.getParameterName();
        super.postUpdate(action, false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String reasonId = (String) obj[0];
        String selectStat = "select * from $table where parameter_name = '$pn'";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$pn", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        SystemTableDTOInter uDTO = DTOFactory.createSystemTableDTO();
        while (rs.next()) {
            uDTO.setParameterDataType(rs.getInt("parameter_data_type"));
            uDTO.setParameterName(rs.getString("parameter_name"));
            uDTO.setParameterValue(rs.getString("parameter_value"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by parameter_name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<SystemTableDTOInter> uDTOL = new ArrayList<SystemTableDTOInter>();
        while (rs.next()) {
            SystemTableDTOInter uDTO = DTOFactory.createSystemTableDTO();
            uDTO.setParameterDataType(rs.getInt("parameter_data_type"));
            uDTO.setParameterName(rs.getString("parameter_name"));
            uDTO.setParameterValue(rs.getString("parameter_value"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public static void main(String[] args) throws Throwable {
    }
}
