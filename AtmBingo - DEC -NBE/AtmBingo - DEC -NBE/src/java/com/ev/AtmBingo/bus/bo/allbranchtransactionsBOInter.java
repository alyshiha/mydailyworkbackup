package com.ev.AtmBingo.bus.bo;
import com.ev.AtmBingo.bus.dto.allbranchtransactionsDTOInter;
import com.ev.AtmBingo.bus.dto.allbranchtransactionssearchDTOInter;
import java.sql.SQLException;
import java.util.List;
public interface allbranchtransactionsBOInter {

Object insertrecord(Object... obj) throws Throwable;
List<allbranchtransactionsDTOInter> SelectStat(allbranchtransactionssearchDTOInter searchfields, int user) throws Throwable ;
Object updaterecord(Object... obj) throws Throwable;
Object deleteallrecord(Object... obj) throws Throwable; 
Object deleterecord(Object... obj) throws Throwable;
Object GetListOfRecords(Object... obj) throws Throwable;
Object GetRecord(Object... obj) throws Throwable;
Object GetAllRecords(Object... obj) throws Throwable;
Object Saverecord(Object... obj) throws Throwable;
}