/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface BingostatsearchDTOInter  extends Serializable {

    void setSearchtype(String searchtype);

    String getSearchtype();

    void setAtmid(String atmid);

    String getAtmid();

    Date getFromdate();

    int getMatchingtype();

    int getRecordtype();

    Date getTodate();

    int getUserid();

    void setFromdate(Date fromdate);

    void setMatchingtype(int matchingtype);

    void setRecordtype(int recordtype);

    void setTodate(Date todate);

    void setUserid(int userid);
}
