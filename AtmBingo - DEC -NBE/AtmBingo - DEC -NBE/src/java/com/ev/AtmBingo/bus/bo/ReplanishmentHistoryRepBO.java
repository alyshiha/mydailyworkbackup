/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.client.AutoComplete;

import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.AtmGroupDAOInter;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.ReplanishmentMasterDAOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.PrintcardtakenExcelInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.replanishmentHistoryRepDTOInter;
import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author Administrator
 */
public class ReplanishmentHistoryRepBO extends BaseBO implements ReplanishmentHistoryRepBOInter, Serializable {

    protected ReplanishmentHistoryRepBO() {
        super();
    }

    public Object getAtmMachines(UsersDTOInter uDTO, Integer grpId) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>) amDAO.findByAtmGrp(grpId, uDTO.getUserId());
        return amDTOL;
    }

    private String atmName(int grpId) throws Throwable {
        return super.getAtmName(grpId);
    }

    public Object getGroups() throws Throwable {
        AtmGroupDAOInter agDAO = DAOFactory.createAtmGroupDAO(null);
        List<AtmGroupDTOInter> agDTOL = (List<AtmGroupDTOInter>) agDAO.findAll();
        return agDTOL;
    }

    public Object findReportexcel(Date dateF, Date dateT, String atmId, Integer atmGrp, String customer, String userName) throws Throwable {
        String whereClause = "";
        String atm = atmId;
        String grp = "";
        Integer temp = 0;
        AutoComplete AC = new AutoComplete();
        if (atmId == null) {
            atm = "All";
        } else {
            temp = AC.findByAtmId(atmId);
            atm = temp.toString();
        }
        if (atmGrp == 0) {
            grp = "All";
        } else {
            grp = super.getGrpName(atmGrp);
        }
        String param = "Date From $dateF and Date To $datT\nGroup $grp\nATM ID $atm";
        param = param.replace("$dateF", DateFormatter.changeDateAndTimeFormat(dateF));
        param = param.replace("$datT", DateFormatter.changeDateAndTimeFormat(dateT));
        param = param.replace("$atm", atm);
        param = param.replace("$grp", grp);
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        JRResultSetDataSource resultSetDataSource;
        ReplanishmentMasterDAOInter arrRepDAO = DAOFactory.createReplanishmentMasterDAO(null);
        whereClause += "Date_from >=to_date(nvl('$dateF',to_CHAR(sysdate-3000,'dd.mm.yyyy hh24:mi:ss')),'dd.mm.yyyy hh24:mi:ss')";
        whereClause += " and Date_to <=to_date(nvl('$datT',to_CHAR(sysdate+1,'dd.mm.yyyy hh24:mi:ss')),'dd.mm.yyyy hh24:mi:ss')";
        if (atmGrp != 0) {
            if (atmId != null) {
                whereClause += " and (atm_id = $atmId or 0 = $atmId )";
                whereClause = whereClause.replace("$atmId", "" + atmId);
            } else {
                whereClause += " and atm_id in(SELECT   id\n"
                        + "                       FROM   atm_machine\n"
                        + "                      WHERE   atm_group IN\n"
                        + "                                    (SELECT   id\n"
                        + "                                       FROM   atm_group\n"
                        + "                                      WHERE   parent_id IN\n"
                        + "                                                    (SELECT   id\n"
                        + "                                                       FROM   atm_group\n"
                        + "                                                      WHERE   (parent_id = $grpId\n"
                        + "                                                               OR id = $grpId))\n"
                        + "                                              OR id = $grpId))";
                whereClause = whereClause.replace("$grpId", "" + atmGrp);
            }
        } else {
            if (atmId != null) {
                whereClause += " and (atm_id = $atmId or 0 = $atmId )";
                whereClause = whereClause.replace("$atmId", "" + atm);
            }
        }
        whereClause = whereClause.replace("$dateF", DateFormatter.changeDateAndTimeFormat(dateF));
        whereClause = whereClause.replace("$datT", DateFormatter.changeDateAndTimeFormat(dateT));
        List<replanishmentHistoryRepDTOInter> rs = (List<replanishmentHistoryRepDTOInter>) arrRepDAO.replanishmentHistoryRepexcel(whereClause);
        return rs;
    }

    public Object findcardtakenexcel(Date dateF, Date dateT, String atmId, Integer atmGrp, String customer, String userName) throws Throwable {
        String whereClause = "";
        String atm = atmId;
        String grp = "";
        Integer temp = 0;
        AutoComplete AC = new AutoComplete();
        if (atmId == null) {
            atm = "All";
        } else {
            temp = AC.findByAtmId(atmId);
            atm = temp.toString();
        }
        if (atmGrp == 0) {
            grp = "All";
        } else {
            grp = super.getGrpName(atmGrp);
        }
        String param = "Date From $dateF and Date To $datT\nGroup $grp\nATM ID $atm";
        param = param.replace("$dateF", DateFormatter.changeDateAndTimeFormat(dateF));
        param = param.replace("$datT", DateFormatter.changeDateAndTimeFormat(dateT));
        param = param.replace("$atm", atm);
        param = param.replace("$grp", grp);
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        JRResultSetDataSource resultSetDataSource;
        ReplanishmentMasterDAOInter arrRepDAO = DAOFactory.createReplanishmentMasterDAO(null);
        whereClause += "Date_from >=to_date(nvl('$dateF',to_CHAR(sysdate-3000,'dd.mm.yyyy hh24:mi:ss')),'dd.mm.yyyy hh24:mi:ss')";
        whereClause += " and Date_to <=to_date(nvl('$datT',to_CHAR(sysdate+1,'dd.mm.yyyy hh24:mi:ss')),'dd.mm.yyyy hh24:mi:ss')";
        if (atmGrp != 0) {
            if (atmId == null ? "" != null : !atmId.equals("")) {
                whereClause += " and (atm_id = $atmId or 0 = $atmId )";
                whereClause = whereClause.replace("$atmId", "" + atmId);
            } else {
                whereClause += " and atm_id in(SELECT   id\n"
                        + "                       FROM   atm_machine\n"
                        + "                      WHERE   atm_group IN\n"
                        + "                                    (SELECT   id\n"
                        + "                                       FROM   atm_group\n"
                        + "                                      WHERE   parent_id IN\n"
                        + "                                                    (SELECT   id\n"
                        + "                                                       FROM   atm_group\n"
                        + "                                                      WHERE   (parent_id = $grpId\n"
                        + "                                                               OR id = $grpId))\n"
                        + "                                              OR id = $grpId))";
                whereClause = whereClause.replace("$grpId", "" + atmGrp);
            }
        } else {
            if (atmId != null) {
                whereClause += " and (atm_id = $atmId or 0 = $atmId )";
                whereClause = whereClause.replace("$atmId", "" + atm);
            }
        }
        whereClause = whereClause + " order by AtmApplicationID,Date_from ";
        whereClause = whereClause.replace("$dateF", DateFormatter.changeDateAndTimeFormat(dateF));
        whereClause = whereClause.replace("$datT", DateFormatter.changeDateAndTimeFormat(dateT));
        List<PrintcardtakenExcelInter> rs = (List<PrintcardtakenExcelInter>) arrRepDAO.replanishmentCardTakedExcel(whereClause);
        return rs;
    }

    public Object findReport(Date dateF, Date dateT, String atmId, Integer atmGrp, String customer, String userName) throws Throwable {

        String whereClause = "";
        String atm = atmId;
        String grp = "";
        Integer temp = 0;
        AutoComplete AC = new AutoComplete();
        if (atmId == null) {
            atm = "All";
        } else {
            temp = AC.findByAtmId(atmId);
            atm = temp.toString();
        }

        if (atmGrp == 0) {
            grp = "All";
        } else {
            grp = super.getGrpName(atmGrp);
        }

        String param = "Date From $dateF and Date To $datT\nGroup $grp\nATM ID $atm";
        param = param.replace("$dateF", DateFormatter.changeDateAndTimeFormat(dateF));
        param = param.replace("$datT", DateFormatter.changeDateAndTimeFormat(dateT));
        param = param.replace("$atm", atm);
        param = param.replace("$grp", grp);

        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        JRResultSetDataSource resultSetDataSource;

        ReplanishmentMasterDAOInter arrRepDAO = DAOFactory.createReplanishmentMasterDAO(null);

        whereClause += "Date_from >=to_date(nvl('$dateF',to_CHAR(sysdate-3000,'dd.mm.yyyy hh24:mi:ss')),'dd.mm.yyyy hh24:mi:ss')";
        whereClause += " and Date_to <=to_date(nvl('$datT',to_CHAR(sysdate+1,'dd.mm.yyyy hh24:mi:ss')),'dd.mm.yyyy hh24:mi:ss')";

        if (atmGrp != 0) {
            if (atmId == null ? "" != null : !atmId.equals("")) {
                whereClause += " and (atm_id = $atmId or 0 = $atmId )";
                whereClause = whereClause.replace("$atmId", "" + atmId);
            } else {
                whereClause += " and atm_id in(select id from atm_machine m where atm_group =  $grpId)";
                whereClause = whereClause.replace("$grpId", "" + atmGrp);
            }
        } else {
            if (atmId != null) {
                whereClause += " and (atm_id = $atmId or 0 = $atmId )";
                whereClause = whereClause.replace("$atmId", "" + atm);
            }
        }

        whereClause = whereClause.replace("$dateF", DateFormatter.changeDateAndTimeFormat(dateF));
        whereClause = whereClause.replace("$datT", DateFormatter.changeDateAndTimeFormat(dateT));

        ResultSet rs = (ResultSet) arrRepDAO.replanishmentHistoryRep(whereClause);
        resultSetDataSource = new JRResultSetDataSource(rs);

        params.put("user", userName);
        params.put("customer", customer);
        params.put("param", param);
        try {
            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/rephistory.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, resultSetDataSource);
            Calendar c = Calendar.getInstance();
            String uri = "ReplanishmentHist" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {

                String x = "../PDF/" + uri;

                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                resultSetDataSource = null;
                CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());
                rs.close();
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();

            return "fail";
        }
    }

    public Object findcardtakenReport(Date dateF, Date dateT, String atmId, Integer atmGrp, String customer, String userName) throws Throwable {

        String whereClause = "";
        String atm = atmId;
        String grp = "";
        Integer temp = 0;
        AutoComplete AC = new AutoComplete();
        if (atmId == null) {
            atm = "All";
        } else {
            temp = AC.findByAtmId(atmId);
            atm = temp.toString();
        }

        if (atmGrp == 0) {
            grp = "All";
        } else {
            grp = super.getGrpName(atmGrp);
        }

        String param = "Date From $dateF and Date To $datT\nGroup $grp\nATM ID $atm";
        param = param.replace("$dateF", DateFormatter.changeDateAndTimeFormat(dateF));
        param = param.replace("$datT", DateFormatter.changeDateAndTimeFormat(dateT));
        param = param.replace("$atm", atm);
        param = param.replace("$grp", grp);

        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        JRResultSetDataSource resultSetDataSource;

        ReplanishmentMasterDAOInter arrRepDAO = DAOFactory.createReplanishmentMasterDAO(null);

        whereClause += "Date_from >=to_date(nvl('$dateF',to_CHAR(sysdate-3000,'dd.mm.yyyy hh24:mi:ss')),'dd.mm.yyyy hh24:mi:ss')";
        whereClause += " and Date_to <=to_date(nvl('$datT',to_CHAR(sysdate+1,'dd.mm.yyyy hh24:mi:ss')),'dd.mm.yyyy hh24:mi:ss')";

        if (atmGrp != 0) {
            if (atmId == null ? "" != null : !atmId.equals("")) {
                whereClause += " and atm_id = $atmId";
                whereClause = whereClause.replace("$atmId", "" + atmId);
            } else {
                whereClause += " and atm_id in(select id from atm_machine m where atm_group =  $grpId)";
                whereClause = whereClause.replace("$grpId", "" + atmGrp);
            }
        } else {
            if (atmId != null) {
                whereClause += " and atm_id = $atmId";
                whereClause = whereClause.replace("$atmId", "" + atm);
            }
        }

        whereClause = whereClause.replace("$dateF", DateFormatter.changeDateAndTimeFormat(dateF));
        whereClause = whereClause.replace("$datT", DateFormatter.changeDateAndTimeFormat(dateT));

        ResultSet rs = (ResultSet) arrRepDAO.replanishmentCardTaked(whereClause);
        resultSetDataSource = new JRResultSetDataSource(rs);

        params.put("user", userName);
        params.put("customer", customer);
        params.put("param", param);
        try {
            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/repcardtaken.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, resultSetDataSource);
            Calendar c = Calendar.getInstance();
            String uri = "repcard" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {

                String x = "../PDF/" + uri;

                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                resultSetDataSource = null;
                CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());
                rs.close();
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();

            return "fail";
        }
    }
}
