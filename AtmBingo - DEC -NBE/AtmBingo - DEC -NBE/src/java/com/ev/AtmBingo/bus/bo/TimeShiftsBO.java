/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.TimeShiftDAOInter;
import com.ev.AtmBingo.bus.dao.TimeShiftDetailDAOInter;
import com.ev.AtmBingo.bus.dto.TimeShiftDTOInter;
import com.ev.AtmBingo.bus.dto.TimeShiftDetailDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TimeShiftsBO extends BaseBO implements TimeShiftsBOInter,Serializable{

    protected TimeShiftsBO() {
        super();
    }

    public Object getTimeShifts()throws Throwable{
        TimeShiftDAOInter tsDAO = DAOFactory.createTimeShiftDAO(null);
        List<TimeShiftDTOInter> tsDTOL = (List<TimeShiftDTOInter>)tsDAO.findAll();
        for(TimeShiftDTOInter t : tsDTOL){
            t.getTimeShiftDetails();
        }
        return tsDTOL;
    }

    public Object editeTimeShits(TimeShiftDTOInter tsDTO, int operation)throws Throwable{
        TimeShiftDAOInter tsDAO = DAOFactory.createTimeShiftDAO(null);
        switch(operation){
            case INSERT:
                tsDAO.insert(tsDTO);
                return "Insert Done";
            case UPDATE:
                tsDAO.update(tsDTO);
                return "Update Done";
            case DELETE:
                tsDAO.delete(tsDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }

    public Object edteTimeShiftsDetail(TimeShiftDetailDTOInter tsdDTO,int oldDay,int operation)throws Throwable{
        TimeShiftDetailDAOInter tsdDAO = DAOFactory.createTimeShiftDetailDAO(null);
        switch(operation){
            case INSERT:
                tsdDAO.insert(tsdDTO);
                return "Insert Done";
            case UPDATE:
                tsdDAO.update(tsdDTO,oldDay);
                return "Update Done";
            case DELETE:
                tsdDAO.delete(tsdDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }

}
