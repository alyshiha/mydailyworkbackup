/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.UserPassDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.usersbranchDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ChangePassBO extends BaseBO implements ChangePassBOInter ,Serializable{

    protected ChangePassBO() {
        super();
    }

    public Object getUsers(int rest) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> uDTOL = (List<UsersDTOInter>) uDAO.findALl(rest);
        return uDTOL;
    }

    public Object Checkpass(UsersDTOInter uDTO, String confPass, String newPass) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);

        if (newPass.equals(confPass)) {
            uDTO.setUserPassword(newPass);
            if (!uDAO.Checkpass(uDTO, confPass, newPass)) {
                return "Done";
            } else {
                return "Password can`t be late";
            }
        } else {
            return "Passwords Don't match";
        }
    }

    public Object updatePass(UsersDTOInter uDTO, String confPass, String newPass) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);

        if (newPass.equals(confPass)) {
            uDTO.setUserPassword(newPass);
            uDAO.updatePass(uDTO);
            return "Done";
        } else {
            return "Passwords Don't match";
        }

    }
   public Object updatePassBranch(usersbranchDTOInter loggedInUser, String oldPass, String confPass, String newPass, Boolean reset) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        String p;
        usersbranchDTOInter u;
        if (reset != Boolean.TRUE) {
             p = (String) uDAO.encrUserPass(oldPass);
             u = (usersbranchDTOInter) uDAO.findBranch(loggedInUser.getuserid());
        }else{
        u = loggedInUser;
        p = u.getuserpassword();
        }
        if (u.getuserpassword().equals(p)) {
            if (newPass.equals(confPass)) {
                loggedInUser.setuserpassword(newPass);
                uDAO.updatePassBranch(loggedInUser);
                return "Done Successfully";
            } else {
                return "Password Confirmation must equal New Password";
            }
        } else {
            return "Wrong Old Password";
        }


    }

    public Object updatePass(UsersDTOInter loggedInUser, String oldPass, String confPass, String newPass, Boolean reset) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        String p;
        UsersDTOInter u;
        if (reset != Boolean.TRUE) {
             p = (String) uDAO.encrUserPass(oldPass);
             u = (UsersDTOInter) uDAO.find(loggedInUser.getUserId());
        }else{
        u = loggedInUser;
        p = u.getUserPassword();
        }
        if (u.getUserPassword().equals(p)) {
            if (newPass.equals(confPass)) {
                loggedInUser.setUserPassword(newPass);
                uDAO.updatePass(loggedInUser);
                return "Done Successfully";
            } else {
                return "Password Confirmation must equal New Password";
            }
        } else {
            return "Wrong Old Password";
        }


    }

    public UsersDTOInter getSelected(int id) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        UsersDTOInter uDTO = (UsersDTOInter) uDAO.find(id);
        return uDTO;
    }
  public Boolean validatePasswordBranch(String pass, int id) throws Throwable {
        UserPassDAOInter upDAO = DAOFactory.createUserPassDAO();
        return (Boolean) upDAO.findBeforeLastPassBranch(id, pass);
    }
    public Boolean validatePassword(String pass, int id) throws Throwable {
        UserPassDAOInter upDAO = DAOFactory.createUserPassDAO();
        return (Boolean) upDAO.findBeforeLastPass(id, pass);
    }

    public Boolean validatePass(String pass, int id) throws Throwable {
        UserPassDAOInter upDAO = DAOFactory.createUserPassDAO();
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);

        String newPass = (String) uDAO.encrUserPass(pass);
        String oldPass = (String) upDAO.findBeforeLastPass(id);

        if (oldPass == null) {
            return false;
        }
        if (oldPass.equals(newPass)) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean validatePass2(String pass, int id) throws Throwable {
        UserPassDAOInter upDAO = DAOFactory.createUserPassDAO();
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);

        String newPass = (String) uDAO.encrUserPass(pass);
        String oldPass = (String) upDAO.findLastPass(id);

        if (oldPass == null) {
            return false;
        }
        if (oldPass.equals(newPass)) {
            return true;
        } else {
            return false;
        }
    }
}
