/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.presdto;

import com.ev.AtmBingo.bus.dto.MatchingTypeSettingDTOInter;

/**
 *
 * @author Administrator
 */
public interface MatchingTypeSettingPresDTOInter extends MatchingTypeSettingDTOInter{

    String getColName() throws Throwable;

    void setColName(String colName) throws Throwable;

//
//    int getColumnId();
//
//    int getId();
//
//    int getMatchingType();
//
//    int getPartLength();
//
//    int getPartSetting();
//
//    void setColumnId(int columnId);
//
//    void setId(int id);
//
//    void setMatchingType(int matchingType);
//
//    void setPartLength(int partLength);
//
//    void setPartSetting(int partSetting);

}
