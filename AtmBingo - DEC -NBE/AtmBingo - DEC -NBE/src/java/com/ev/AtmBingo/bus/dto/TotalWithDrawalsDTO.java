/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class TotalWithDrawalsDTO implements TotalWithDrawalsDTOInter, Serializable {

    private String atm_application_id, currency;
    private Integer transaction, atm_id;

    @Override
    public String getAtm_application_id() {
        return atm_application_id;
    }

    @Override
    public void setAtm_application_id(String atm_application_id) {
        this.atm_application_id = atm_application_id;
    }

    @Override
    public Integer getAtm_id() {
        return atm_id;
    }

    @Override
    public void setAtm_id(Integer atm_id) {
        this.atm_id = atm_id;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public Integer getTransaction() {
        return transaction;
    }

    @Override
    public void setTransaction(Integer transaction) {
        this.transaction = transaction;
    }
    
}
