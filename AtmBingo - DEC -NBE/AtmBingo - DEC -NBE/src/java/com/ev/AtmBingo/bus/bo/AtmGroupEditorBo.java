/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.AtmGroupDAOInter;
import com.ev.AtmBingo.bus.dao.AtmGroupEditorDaoInter;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmGroupEditorBo extends BaseBO implements AtmGroupEditorBoInter,Serializable {

    public Object GetGroups()throws Throwable{
        AtmGroupDAOInter agDAO = DAOFactory.createAtmGroupDAO(null);
        List<AtmGroupDTOInter> agDTOL = (List<AtmGroupDTOInter>)agDAO.findAll();
        return agDTOL;
    }


      public Object getAtmGroup(int atmGroup) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
         
        List<String> target =new ArrayList<String>(amDAO.findByGrp(atmGroup));
        return target;
    }

 public Object saveUserAtm(int GroupID, List<String> target) throws Throwable {

        AtmGroupEditorDaoInter UADI = DAOFactory.CreateAtmGroupEditorDao();
        if (target.size() != 0)
        {
            UADI.UpdateAtmByGroupId(target, GroupID);
        }
         return "saved";
}
}
