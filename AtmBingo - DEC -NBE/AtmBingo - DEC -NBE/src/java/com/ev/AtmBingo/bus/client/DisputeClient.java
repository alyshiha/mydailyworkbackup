/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import java.math.*;
import java.text.*;
import java.util.*;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.base.client.ExportExcel;
import com.ev.AtmBingo.base.client.HashContainer;
import com.ev.AtmBingo.bus.dto.FileColumnDefinitionDTOInter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.ColumnsDefinitionBOInter;
import com.ev.AtmBingo.bus.bo.DisputesBOInter;

import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DisputesDTOInter;
import com.ev.AtmBingo.bus.dto.MatchingTypeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.branchcodeDTOInter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Clob;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static mondrian.olap.fun.vba.Vba.string;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.event.data.PageEvent;
import org.primefaces.model.DualListModel;

/**
 *
 * @author ISLAM
 */
public class DisputeClient extends BaseBean implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="Getter and setter and attributes">
    private HashContainer holderObject = new HashContainer();
    private HashContainer CorrectiveholderObject = new HashContainer();
    private List<ColumnDTOInter> mcList;
    private List<branchcodeDTOInter> branchList;
    private Session utillist = new Session();
    private Integer responseOp, responseOp1;
    private List<FileColumnDefinitionDTOInter> mcDefaultList;
    private List<DisputesDTOInter> mDataList;
    private DisputesBOInter mcObject;
    private List<String> transFill;
    private List<String> transFill_op;
    private Boolean deselectFlag, selectFlag, blackFlag, printCheck, exportFlag, correctiveFlag;
    private String sType1, hiddenText, hiddenText1;
    private String totalAmount, corramount;

    private String sType2, correctiveText;
    private Boolean toShow, correctiveDisable, accountFlag, accountFlag1, cardFlag1;
    private Boolean toShow_to;
    private Boolean toShow1;
    private Boolean toShow1_to;
    private Boolean collectedcard;
    private Boolean toShow2;
    private Boolean toShow3;
    private Boolean toShow3_to;
    private Boolean toShow4;
    private Boolean toShow4_to;
    private Boolean showSelection;
    private Boolean settledFlag;
    private Date tranDateF;
    private Date tranDateT;
    private Date settledDateF;
    private Date settledDateT;
    private Date loadDateF;
    private Date loadDateT;
    private Integer amountF;
    private Integer listSize;
    private Integer amountT;
    private Integer seqF;
    private Integer seqT;
    private String cardSt, col1St, col2St, col3St, col4St, col5St, branch;
    private String message;
    private String accountSt;
    private Integer atmcString;
    private String selectString, col1name, col2name, col3name, col4name, col5name;
    private Boolean blBool, commentFlag, unCommentFlag, cardFlag, col1Flag, col2Flag, col3Flag, col4Flag, col5Flag;
    private MatchingTypeDTOInter masterRecord;
    private DisputesBOInter coloumDefinition;
    private List<AtmMachineDTOInter> atmList;
    private List<TransactionTypeDTOInter> ttList;
    private List<TransactionStatusDTOInter> tsList;
    private List<CurrencyMasterDTOInter> cmList;
    private List<MatchingTypeDTOInter> mtList;
    private List<MatchingTypeDTOInter> mrList;
    private List<UsersDTOInter> uSettledList;
    private List<TransactionResponseCodeDTOInter> rcList;
    private List<AtmGroupDTOInter> atmgList;
    private List<String> settled;
    private String tDate;
    private String masterNew;
    private Integer teDate;
    private String sDate;
    private String showSide, showSide_1;
    private String statusCode;
    private String transTCode;
    private String responseCode;
    private String lDate;
    private String sortBy, result;
    private String sortBy1;
    private String sortBy2;
    private String amount;
    private String sequance;
    private String settledState;
    private String account_op;
    private String card_op, col1op, col2op, col3op, col4op, col5op;
    private int tsInt;
    private int cmInt;
    private String atmidInt;
    private Integer newMaster;
    private int ttInt;
    private int rcInt, disputeStatusInt;
    private Integer mtInt;
    private Integer mtInt1;
    private Integer usInt;
    private Integer usInt1, autoComp;
    private Integer atmgroupInt;
    private String comboValue;
    private String matchingChange;
    private String nameCollapse;
    private String validATM = "";
    private Boolean collapseField, exportDisabled, idFlag, nameFlag, dlgFlag, fileFlag, fileFlag1;
    private DualListModel<String> fcdPickList;
    private List<ColumnDTOInter> mcTargetList;
    private List<DisputesDTOInter> otherSide;
    private DisputesDTOInter[] selectedItems;
    private List<DisputesDTOInter> unSelectedItems;
    private List<String> colHolder;
    private List<ColumnDTOInter> column;
    private List<ColumnDTOInter> column1;
    private String searchParameter;
    private UsersDTOInter logedinUser;
    private Integer reverseInt, recordInt;
    private DisputesDTOInter dAtt;
    private String comment, searchMessage;
    private List<DisputesDTOInter> PrintAll;
    private String textfile, transfile, transfile_other, textfile_other, hiddenText1_other;
    private Clob sa;
    private ColumnsDefinitionBOInter colObject;
    private List<ColumnDTOInter> colDefList;

    public ColumnsDefinitionBOInter getColObject() {
        return colObject;
    }

    public void setColObject(ColumnsDefinitionBOInter colObject) {
        this.colObject = colObject;
    }

    public List<ColumnDTOInter> getColDefList() {
        return colDefList;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public void setColDefList(List<ColumnDTOInter> colDefList) {
        this.colDefList = colDefList;
    }

    public String getCol1St() {
        return col1St;
    }

    public String getCol1name() {
        return col1name;
    }

    public void setCol1name(String col1name) {
        this.col1name = col1name;
    }

    public String getCol2name() {
        return col2name;
    }

    public void setCol2name(String col2name) {
        this.col2name = col2name;
    }

    public List<branchcodeDTOInter> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<branchcodeDTOInter> branchList) {
        this.branchList = branchList;
    }

    public Boolean getCollectedcard() {
        return collectedcard;
    }

    public void setCollectedcard(Boolean collectedcard) {
        this.collectedcard = collectedcard;
    }

    public String getCol3name() {
        return col3name;
    }

    public void setCol3name(String col3name) {
        this.col3name = col3name;
    }

    public String getCol4name() {
        return col4name;
    }

    public void setCol4name(String col4name) {
        this.col4name = col4name;
    }

    public String getCol5name() {
        return col5name;
    }

    public void setCol5name(String col5name) {
        this.col5name = col5name;
    }

    public void setCol1St(String col1St) {
        this.col1St = col1St;
    }

    public String getCol2St() {
        return col2St;
    }

    public void setCol2St(String col2St) {
        this.col2St = col2St;
    }

    public String getCol3St() {
        return col3St;
    }

    public void setCol3St(String col3St) {
        this.col3St = col3St;
    }

    public String getCol4St() {
        return col4St;
    }

    public void setCol4St(String col4St) {
        this.col4St = col4St;
    }

    public String getCol5St() {
        return col5St;
    }

    public void setCol5St(String col5St) {
        this.col5St = col5St;
    }

    public Boolean getCol1Flag() {
        return col1Flag;
    }

    public void setCol1Flag(Boolean col1Flag) {
        this.col1Flag = col1Flag;
    }

    public Boolean getCol2Flag() {
        return col2Flag;
    }

    public void setCol2Flag(Boolean col2Flag) {
        this.col2Flag = col2Flag;
    }

    public Boolean getCol3Flag() {
        return col3Flag;
    }

    public void setCol3Flag(Boolean col3Flag) {
        this.col3Flag = col3Flag;
    }

    public Boolean getCol4Flag() {
        return col4Flag;
    }

    public void setCol4Flag(Boolean col4Flag) {
        this.col4Flag = col4Flag;
    }

    public Boolean getCol5Flag() {
        return col5Flag;
    }

    public void setCol5Flag(Boolean col5Flag) {
        this.col5Flag = col5Flag;
    }

    public Boolean getFileFlag1() {
        return fileFlag1;
    }

    public String getCorramount() {
        return corramount;
    }

    public void setCorramount(String corramount) {
        this.corramount = corramount;
    }

    public void setFileFlag1(Boolean fileFlag1) {
        this.fileFlag1 = fileFlag1;
    }

    public Boolean getFileFlag() {
        return fileFlag;
    }

    public void setFileFlag(Boolean fileFlag) {
        this.fileFlag = fileFlag;
    }

    public Boolean getAccountFlag() {
        return accountFlag;
    }

    public String getValidATM() {
        return validATM;
    }

    public void setValidATM(String validATM) {
        this.validATM = validATM;
    }

    public void setAccountFlag(Boolean accountFlag) {
        this.accountFlag = accountFlag;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Boolean getDlgFlag() {
        return dlgFlag;
    }

    public void setDlgFlag(Boolean dlgFlag) {
        this.dlgFlag = dlgFlag;
    }

    public Integer getResponseOp() {
        return responseOp;
    }

    public Integer getAutoComp() {
        return autoComp;
    }

    public void setAutoComp(Integer autoComp) {
        this.autoComp = autoComp;
    }

    public void setResponseOp(Integer responseOp) {
        this.responseOp = responseOp;
    }

    public Integer getResponseOp1() {
        return responseOp1;
    }

    public void setResponseOp1(Integer responseOp1) {
        this.responseOp1 = responseOp1;
    }

    public Boolean getCardFlag() {
        return cardFlag;
    }

    public void setCardFlag(Boolean cardFlag) {
        this.cardFlag = cardFlag;
    }

    public Boolean getIdFlag() {
        return idFlag;
    }

    public void setIdFlag(Boolean idFlag) {
        this.idFlag = idFlag;
    }

    public Boolean getNameFlag() {
        return nameFlag;
    }

    public void setNameFlag(Boolean nameFlag) {
        this.nameFlag = nameFlag;
    }

    public String getHiddenText1_other() {
        return hiddenText1_other;
    }

    public void setHiddenText1_other(String hiddenText1_other) {
        this.hiddenText1_other = hiddenText1_other;
    }

    public String getTextfile_other() {
        return textfile_other;
    }

    public void setTextfile_other(String textfile_other) {
        this.textfile_other = textfile_other;
    }

    public String getTransfile_other() {
        return transfile_other;
    }

    public void setTransfile_other(String transfile_other) {
        this.transfile_other = transfile_other;
    }

    public String getHiddenText1() {
        return hiddenText1;
    }

    public void setHiddenText1(String hiddenText1) {
        this.hiddenText1 = hiddenText1;
    }

    public String getHiddenText() {
        return hiddenText;
    }

    public void setHiddenText(String hiddenText) {
        this.hiddenText = hiddenText;
    }

    public Clob getSa() {
        return sa;
    }

    public void setSa(Clob sa) {
        this.sa = sa;
    }

    public String getTransfile() {
        return transfile;
    }

    public void setTransfile(String transfile) {
        this.transfile = transfile;
    }

    public String getTextfile() {
        return textfile;
    }

    public void setTextfile(String textfile) {
        this.textfile = textfile;
    }

    public String getShowSide_1() {
        return showSide_1;
    }

    public void setShowSide_1(String showSide_1) {
        this.showSide_1 = showSide_1;
    }

    public String getSearchMessage() {
        return searchMessage;
    }

    public void setSearchMessage(String searchMessage) {
        this.searchMessage = searchMessage;
    }

    public Boolean getExportDisabled() {
        return exportDisabled;
    }

    public Boolean getAccountFlag1() {
        return accountFlag1;
    }

    public void setAccountFlag1(Boolean accountFlag1) {
        this.accountFlag1 = accountFlag1;
    }

    public Boolean getCardFlag1() {
        return cardFlag1;
    }

    public void setCardFlag1(Boolean cardFlag1) {
        this.cardFlag1 = cardFlag1;
    }

    public void setExportDisabled(Boolean exportDisabled) {
        this.exportDisabled = exportDisabled;
    }

    public Boolean getCorrectiveDisable() {
        return correctiveDisable;
    }

    public void setCorrectiveDisable(Boolean correctiveDisable) {
        this.correctiveDisable = correctiveDisable;
    }

    public String getCorrectiveText() {
        return correctiveText;
    }

    public void setCorrectiveText(String correctiveText) {
        this.correctiveText = correctiveText;
    }

    public Boolean getCorrectiveFlag() {
        return correctiveFlag;
    }

    public void setCorrectiveFlag(Boolean correctiveFlag) {
        this.correctiveFlag = correctiveFlag;
    }

    public Boolean getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(Boolean exportFlag) {
        this.exportFlag = exportFlag;
    }

    public int getDisputeStatusInt() {
        return disputeStatusInt;
    }

    public void setDisputeStatusInt(int disputeStatusInt) {
        this.disputeStatusInt = disputeStatusInt;
    }

    public Boolean getDeselectFlag() {
        return deselectFlag;
    }

    public void setDeselectFlag(Boolean deselectFlag) {
        this.deselectFlag = deselectFlag;
    }

    public Boolean getSelectFlag() {
        return selectFlag;
    }

    public void setSelectFlag(Boolean selectFlag) {
        this.selectFlag = selectFlag;
    }

    public Boolean getPrintCheck() {
        return printCheck;
    }

    public void setPrintCheck(Boolean printCheck) {
        this.printCheck = printCheck;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getCommentFlag() {
        return commentFlag;
    }

    public void setCommentFlag(Boolean commentFlag) {
        this.commentFlag = commentFlag;
    }

    public Boolean getUnCommentFlag() {
        return unCommentFlag;
    }

    public void setUnCommentFlag(Boolean unCommentFlag) {
        this.unCommentFlag = unCommentFlag;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public Integer getRecordInt() {
        return recordInt;
    }

    public void setRecordInt(Integer recordInt) {
        this.recordInt = recordInt;
    }

    public Integer getReverseInt() {
        return reverseInt;
    }

    public void setReverseInt(Integer reverseInt) {
        this.reverseInt = reverseInt;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getTransTCode() {
        return transTCode;
    }

    public void setTransTCode(String transTCode) {
        this.transTCode = transTCode;
    }

    public String getAccountSt() {
        return accountSt;
    }

    public void setAccountSt(String accountSt) {
        this.accountSt = accountSt;
    }

    public String getAccount_op() {
        return account_op;
    }

    public void setAccount_op(String account_op) {
        this.account_op = account_op;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Integer getAmountF() {
        return amountF;
    }

    public void setAmountF(Integer amountF) {
        this.amountF = amountF;
    }

    public Integer getAmountT() {
        return amountT;
    }

    public void setAmountT(Integer amountT) {
        this.amountT = amountT;
    }

    public String getAtmidInt() {
        return atmidInt;
    }

    public void setAtmidInt(String atmidInt) {

        this.atmidInt = atmidInt;
    }

    public List<AtmMachineDTOInter> getAtmList() {
        return atmList;
    }

    public void setAtmList(List<AtmMachineDTOInter> atmList) {
        this.atmList = atmList;
    }

    public Integer getAtmcString() {
        return atmcString;
    }

    public void setAtmcString(Integer atmcString) {
        this.atmcString = atmcString;
    }

    public Integer getAtmgroupInt() {
        return atmgroupInt;
    }

    public void setAtmgroupInt(Integer atmgroupInt) {
        this.atmgroupInt = atmgroupInt;
    }

    public List<AtmGroupDTOInter> getAtmgList() {
        return atmgList;
    }

    public void setAtmgList(List<AtmGroupDTOInter> atmgList) {
        this.atmgList = atmgList;
    }

    public String getSelectString() {
        return selectString;
    }

    public void setSelectString(String selectString) {
        this.selectString = selectString;
    }

    public Boolean getBlBool() {
        return blBool;
    }

    public void setBlBool(Boolean blBool) {
        this.blBool = blBool;
    }

    public String getCardSt() {
        return cardSt;
    }

    public void setCardSt(String cardSt) {
        this.cardSt = cardSt;
    }

    public String getCard_op() {
        return card_op;
    }

    public void setCard_op(String card_op) {
        this.card_op = card_op;
    }

    public int getCmInt() {
        return cmInt;
    }

    public void setCmInt(int cmInt) {
        this.cmInt = cmInt;
    }

    public List<CurrencyMasterDTOInter> getCmList() {
        return cmList;
    }

    public void setCmList(List<CurrencyMasterDTOInter> cmList) {
        this.cmList = cmList;
    }

    public List<String> getColHolder() {
        return colHolder;
    }

    public void setColHolder(List<String> colHolder) {
        this.colHolder = colHolder;
    }

    public Boolean getCollapseField() {
        return collapseField;
    }

    public void setCollapseField(Boolean collapseField) {
        this.collapseField = collapseField;
    }

    public List<ColumnDTOInter> getColumn() {
        return column;
    }

    public void setColumn(List<ColumnDTOInter> column) {
        this.column = column;
    }

    public List<ColumnDTOInter> getColumn1() {
        return column1;
    }

    public void setColumn1(List<ColumnDTOInter> column1) {
        this.column1 = column1;
    }

    public String getComboValue() {
        return comboValue;
    }

    public String getCol1op() {
        return col1op;
    }

    public void setCol1op(String col1op) {
        this.col1op = col1op;
    }

    public String getCol2op() {
        return col2op;
    }

    public void setCol2op(String col2op) {
        this.col2op = col2op;
    }

    public String getCol3op() {
        return col3op;
    }

    public void setCol3op(String col3op) {
        this.col3op = col3op;
    }

    public String getCol4op() {
        return col4op;
    }

    public void setCol4op(String col4op) {
        this.col4op = col4op;
    }

    public String getCol5op() {
        return col5op;
    }

    public void setCol5op(String col5op) {
        this.col5op = col5op;
    }

    public void setComboValue(String comboValue) {
        this.comboValue = comboValue;
    }

    public DualListModel<String> getFcdPickList() {
        return fcdPickList;
    }

    public void setFcdPickList(DualListModel<String> fcdPickList) {
        this.fcdPickList = fcdPickList;
    }

    public String getlDate() {
        return lDate;
    }

    public void setlDate(String lDate) {
        this.lDate = lDate;
    }

    public Date getLoadDateF() {
        return loadDateF;
    }

    public void setLoadDateF(Date loadDateF) {
        this.loadDateF = loadDateF;
    }

    public Date getLoadDateT() {
        return loadDateT;
    }

    public void setLoadDateT(Date loadDateT) {
        this.loadDateT = loadDateT;
    }

    public UsersDTOInter getLogedinUser() {
        return logedinUser;
    }

    public void setLogedinUser(UsersDTOInter logedinUser) {
        this.logedinUser = logedinUser;
    }

    public List<DisputesDTOInter> getmDataList() {
        return mDataList;
    }

    public void setmDataList(List<DisputesDTOInter> mDataList) {
        this.mDataList = mDataList;
    }

    public String getMasterNew() {
        return masterNew;
    }

    public void setMasterNew(String masterNew) {
        this.masterNew = masterNew;
    }

    public MatchingTypeDTOInter getMasterRecord() {
        return masterRecord;
    }

    public void setMasterRecord(MatchingTypeDTOInter masterRecord) {
        this.masterRecord = masterRecord;
    }

    public HashContainer getHolderObject() {
        return holderObject;
    }

    public void setHolderObject(HashContainer holderObject) {
        this.holderObject = holderObject;
    }

    public HashContainer getCorrectiveholderObject() {
        return CorrectiveholderObject;
    }

    public void setCorrectiveholderObject(HashContainer CorrectiveholderObject) {
        this.CorrectiveholderObject = CorrectiveholderObject;
    }

    public DisputesBOInter getMcObject() {
        return mcObject;
    }

    public void setMcObject(DisputesBOInter mcObject) {
        this.mcObject = mcObject;
    }

    public Boolean getBlackFlag() {
        return blackFlag;
    }

    public void setBlackFlag(Boolean blackFlag) {
        this.blackFlag = blackFlag;
    }

    public DisputesBOInter getColoumDefinition() {
        return coloumDefinition;
    }

    public void setColoumDefinition(DisputesBOInter coloumDefinition) {
        this.coloumDefinition = coloumDefinition;
    }

    public List<DisputesDTOInter> getUnSelectedItems() {
        return unSelectedItems;
    }

    public void setUnSelectedItems(List<DisputesDTOInter> unSelectedItems) {
        this.unSelectedItems = unSelectedItems;
    }

    public List<DisputesDTOInter> getPrintAll() {
        return PrintAll;
    }

    public void setPrintAll(List<DisputesDTOInter> PrintAll) {
        this.PrintAll = PrintAll;
    }

    public Integer getStatus2() {
        return status2;
    }

    public void setStatus2(Integer status2) {
        this.status2 = status2;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getMatchingChange() {
        return matchingChange;
    }

    public void setMatchingChange(String matchingChange) {
        this.matchingChange = matchingChange;
    }

    public List<FileColumnDefinitionDTOInter> getMcDefaultList() {
        return mcDefaultList;
    }

    public void setMcDefaultList(List<FileColumnDefinitionDTOInter> mcDefaultList) {
        this.mcDefaultList = mcDefaultList;
    }

    public List<ColumnDTOInter> getMcList() {
        return mcList;
    }

    public void setMcList(List<ColumnDTOInter> mcList) {
        this.mcList = mcList;
    }

    public List<ColumnDTOInter> getMcTargetList() {
        return mcTargetList;
    }

    public void setMcTargetList(List<ColumnDTOInter> mcTargetList) {
        this.mcTargetList = mcTargetList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MatchingTypeDTOInter> getMrList() {
        return mrList;
    }

    public void setMrList(List<MatchingTypeDTOInter> mrList) {
        this.mrList = mrList;
    }

    public Integer getMtInt() {
        return mtInt;
    }

    public void setMtInt(Integer mtInt) {
        this.mtInt = mtInt;
    }

    public Integer getMtInt1() {
        return mtInt1;
    }

    public void setMtInt1(Integer mtInt1) {
        this.mtInt1 = mtInt1;
    }

    public List<MatchingTypeDTOInter> getMtList() {
        return mtList;
    }

    public void setMtList(List<MatchingTypeDTOInter> mtList) {
        this.mtList = mtList;
    }

    public String getNameCollapse() {
        return nameCollapse;
    }

    public void setNameCollapse(String nameCollapse) {
        this.nameCollapse = nameCollapse;
    }

    public Integer getNewMaster() {
        return newMaster;
    }

    public void setNewMaster(Integer newMaster) {
        this.newMaster = newMaster;
    }

    public List<DisputesDTOInter> getOtherSide() {
        return otherSide;
    }

    public void setOtherSide(List<DisputesDTOInter> otherSide) {
        this.otherSide = otherSide;
    }

    public int getRcInt() {
        return rcInt;
    }

    public void setRcInt(int rcInt) {
        this.rcInt = rcInt;
    }

    public List<TransactionResponseCodeDTOInter> getRcList() {
        return rcList;
    }

    public void setRcList(List<TransactionResponseCodeDTOInter> rcList) {
        this.rcList = rcList;
    }

    public String getsDate() {
        return sDate;
    }

    public void setsDate(String sDate) {
        this.sDate = sDate;
    }

    public String getsType1() {
        return sType1;
    }

    public void setsType1(String sType1) {
        this.sType1 = sType1;
    }

    public String getsType2() {
        return sType2;
    }

    public void setsType2(String sType2) {
        this.sType2 = sType2;
    }

    public String getSearchParameter() {
        return searchParameter;
    }

    public void setSearchParameter(String searchParameter) {
        this.searchParameter = searchParameter;
    }

    public DisputesDTOInter[] getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(DisputesDTOInter[] selectedItems) {
        this.selectedItems = selectedItems;
    }

    public Integer getSeqF() {
        return seqF;
    }

    public void setSeqF(Integer seqF) {
        this.seqF = seqF;
    }

    public Integer getSeqT() {
        return seqT;
    }

    public void setSeqT(Integer seqT) {
        this.seqT = seqT;
    }

    public String getSequance() {
        return sequance;
    }

    public void setSequance(String sequance) {
        this.sequance = sequance;
    }

    public List<String> getSettled() {
        return settled;
    }

    public void setSettled(List<String> settled) {
        this.settled = settled;
    }

    public Date getSettledDateF() {
        return settledDateF;
    }

    public void setSettledDateF(Date settledDateF) {
        this.settledDateF = settledDateF;
    }

    public Date getSettledDateT() {
        return settledDateT;
    }

    public void setSettledDateT(Date settledDateT) {
        this.settledDateT = settledDateT;
    }

    public Boolean getSettledFlag() {
        return settledFlag;
    }

    public void setSettledFlag(Boolean settledFlag) {
        this.settledFlag = settledFlag;
    }

    public String getSettledState() {
        return settledState;
    }

    public void setSettledState(String settledState) {
        this.settledState = settledState;
    }

    public Boolean getShowSelection() {
        return showSelection;
    }

    public void setShowSelection(Boolean showSelection) {
        this.showSelection = showSelection;
    }

    public String getShowSide() {
        return showSide;
    }

    public void setShowSide(String showSide) {
        this.showSide = showSide;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getSortBy1() {
        return sortBy1;
    }

    public void setSortBy1(String sortBy1) {
        this.sortBy1 = sortBy1;
    }

    public String getSortBy2() {
        return sortBy2;
    }

    public void setSortBy2(String sortBy2) {
        this.sortBy2 = sortBy2;
    }

    public String gettDate() {
        return tDate;
    }

    public void settDate(String tDate) {
        this.tDate = tDate;
    }

    public Integer getTeDate() {
        return teDate;
    }

    public void setTeDate(Integer teDate) {
        this.teDate = teDate;
    }

    public Boolean getToShow() {
        return toShow;
    }

    public void setToShow(Boolean toShow) {
        this.toShow = toShow;
    }

    public Boolean getToShow1() {
        return toShow1;
    }

    public void setToShow1(Boolean toShow1) {
        this.toShow1 = toShow1;
    }

    public Boolean getToShow1_to() {
        return toShow1_to;
    }

    public void setToShow1_to(Boolean toShow1_to) {
        this.toShow1_to = toShow1_to;
    }

    public Boolean getToShow2() {
        return toShow2;
    }

    public void setToShow2(Boolean toShow2) {
        this.toShow2 = toShow2;
    }

    public Boolean getToShow3() {
        return toShow3;
    }

    public void setToShow3(Boolean toShow3) {
        this.toShow3 = toShow3;
    }

    public Boolean getToShow3_to() {
        return toShow3_to;
    }

    public void setToShow3_to(Boolean toShow3_to) {
        this.toShow3_to = toShow3_to;
    }

    public Boolean getToShow4() {
        return toShow4;
    }

    public void setToShow4(Boolean toShow4) {
        this.toShow4 = toShow4;
    }

    public Boolean getToShow4_to() {
        return toShow4_to;
    }

    public void setToShow4_to(Boolean toShow4_to) {
        this.toShow4_to = toShow4_to;
    }

    public Boolean getToShow_to() {
        return toShow_to;
    }

    public void setToShow_to(Boolean toShow_to) {
        this.toShow_to = toShow_to;
    }

    public Date getTranDateF() {
        return tranDateF;
    }

    public void setTranDateF(Date tranDateF) {
        this.tranDateF = tranDateF;
    }

    public Date getTranDateT() {
        return tranDateT;
    }

    public void setTranDateT(Date tranDateT) {
        this.tranDateT = tranDateT;
    }

    public List<String> getTransFill() {
        return transFill;
    }

    public void setTransFill(List<String> transFill) {
        this.transFill = transFill;
    }

    public List<String> getTransFill_op() {
        return transFill_op;
    }

    public void setTransFill_op(List<String> transFill_op) {
        this.transFill_op = transFill_op;
    }

    public int getTsInt() {
        return tsInt;
    }

    public void setTsInt(int tsInt) {
        this.tsInt = tsInt;
    }

    public List<TransactionStatusDTOInter> getTsList() {
        return tsList;
    }

    public void setTsList(List<TransactionStatusDTOInter> tsList) {
        this.tsList = tsList;
    }

    public int getTtInt() {
        return ttInt;
    }

    public void setTtInt(int ttInt) {
        this.ttInt = ttInt;
    }

    public List<TransactionTypeDTOInter> getTtList() {
        return ttList;
    }

    public void setTtList(List<TransactionTypeDTOInter> ttList) {
        this.ttList = ttList;
    }

    public List<UsersDTOInter> getuSettledList() {
        return uSettledList;
    }

    public void setuSettledList(List<UsersDTOInter> uSettledList) {
        this.uSettledList = uSettledList;
    }

    public Integer getUsInt() {
        return usInt;
    }

    public void setUsInt(Integer usInt) {
        this.usInt = usInt;
    }

    public Integer getUsInt1() {
        return usInt1;
    }

    public void setUsInt1(Integer usInt1) {
        this.usInt1 = usInt1;
    }// </editor-fold>
    private Session sessionutil;

    /**
     * Creates a new instance of DisputeClient
     */
    public DisputeClient() throws Throwable {

        super();
        super.GetAccess();
        FacesContext context2 = FacesContext.getCurrentInstance();
        HttpSession session2 = (HttpSession) context2.getExternalContext().getSession(true);
        HttpServletResponse response2 = (HttpServletResponse) context2.getExternalContext().getResponse();
        response2.addHeader("X-Frame-Options", "SAMEORIGIN");
        selectFlag = true;
        deselectFlag = false;
        blackFlag = true;
        fileFlag = false;
        fileFlag1 = true;
        exportDisabled = true;
        hiddenText = "";
        hiddenText1 = "";
        dlgFlag = false;
        atmidInt = null;
//List<DisputesDTOInter> mTempList = ArrayList<DisputesDTOInter>();;        repTabl
        //DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:disputeTable");
        // repTable.setLazy(true);
        if (!checkSettPrivilage()) {
            settledState = "Mark as settled";
        } else {
            settledState = "unauthorized User";
        }

        showSelection = false;
        column = new ArrayList<ColumnDTOInter>();
        column1 = new ArrayList<ColumnDTOInter>();
        mDataList = new ArrayList<DisputesDTOInter>();
        // List<DisputesDTOInter> PrintAll = new ArrayList<DisputesDTOInter>();
        otherSide = new ArrayList<DisputesDTOInter>();
        mcObject = BOFactory.createDisputeBO(null);
        mcList = (List<ColumnDTOInter>) utillist.GetColumnsList();
        nameCollapse = "Close";
        printCheck = false;
        toShow = true;
        idFlag = false;
        nameFlag = false;
        autoComp = 0;
        toShow_to = false;
        toShow1 = false;
        toShow1_to = true;
        accountFlag = true;
        cardFlag = true;
        accountFlag1 = false;
        cardFlag1 = false;
        toShow3_to = true;
        toShow4_to = true;
        toShow2 = true;
        toShow3 = false;
        toShow4 = false;
        correctiveDisable = true;
        correctiveFlag = true;
        correctiveText = "Corrective Entry";
        tDate = "Between";
        setAtmidInt(null);
        setAtmgroupInt(0);
        //amountF = null;
        //amountT = null;

        tranDateF = super.getYesterdaytrans((String) utillist.GetTransTime().getParameterValue());
        tranDateT = super.getTodaytrans((String) utillist.GetTransTime().getParameterValue());
        collapseField = true;
        settledFlag = false;
        //seqT = null;
        mcTargetList = new ArrayList<ColumnDTOInter>();
        fcdPickList = (DualListModel<String>) mcObject.getPickColumn();

        // FacesContext context = FacesContext.getCurrentInstance();
        // 
        if (super.getLoggedInUser().getCorrectiveEntry() == 1) {
            correctiveFlag = true;
        } else {
            correctiveFlag = false;
        }
        if (super.getLoggedInUser().getExport() == 1) {
            exportFlag = true;
        } else {
            exportFlag = false;
        }
        collectedcard = false;
        branch = "0";
        getcolname();

        sessionutil = new Session();
        logedinUser = sessionutil.GetUserLogging();
//        if (logedinUser.getCorrectiveEntry() == 1) {
//            correctiveFlag = true;
//        } else {
//            correctiveFlag = false;
//        }
//        if (logedinUser.getExport() == 1) {
//            exportFlag = true;
//        } else {
//            exportFlag = false;
//        }
        masterRecord = DTOFactory.createMatchingTypeDTO();
        masterRecord.setType1(1);
        masterRecord.setType2(2);

        fillCombos();

    }

    public void getcolname() throws Throwable {
        col5Flag = true;
        col4Flag = true;
        col3Flag = true;
        col2Flag = true;
        col1Flag = true;
        colObject = BOFactory.createColumnDefinitionBO(null);
        colDefList = (List<ColumnDTOInter>) utillist.GetColumnsList();
        for (ColumnDTOInter record : colDefList) {
            if (record.getDbName().toUpperCase().trim().equals("COLUMN1")) {
                col1name = record.getHeader();
            }
            if (record.getDbName().toUpperCase().trim().equals("COLUMN2")) {
                col2name = record.getHeader();
            }
            if (record.getDbName().toUpperCase().trim().equals("COLUMN3")) {
                col3name = record.getHeader();
            }
            if (record.getDbName().toUpperCase().trim().equals("COLUMN4")) {
                col4name = record.getHeader();
            }
            if (record.getDbName().toUpperCase().trim().equals("COLUMN5")) {
                col5name = record.getHeader();
            }
        }

    }

    private Boolean checkSettPrivilage() throws Throwable {
        if (super.getLoggedInUser().getSettlement() == 2) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    private Boolean checkUnSettPrivilage() throws Throwable {
        if (super.getLoggedInUser().getUnsettlement() == 2) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public void fillCombos() throws Throwable {

        transFill_op = super.getTrans_op();
        transFill = super.getTrans();
        mcList = (List<ColumnDTOInter>) utillist.GetColumnsList();
        atmList = (List<AtmMachineDTOInter>) utillist.GetAtmMachineList(sessionutil.GetUserLogging());
        cmList = (List<CurrencyMasterDTOInter>) utillist.GetCurrencyMasterList();
        mtList = (List<MatchingTypeDTOInter>) utillist.GetMatchingTypeList();
        masterRecord = mtList.get(masterRecord.getId());
        if (masterRecord.getType1() == 1) {
            sType1 = "Journal";

        } else {
            sType1 = "Switch";

        }

        if (masterRecord.getType2() == 2) {
            sType2 = "Switch";

        } else {
            sType2 = "Host";
        }
        rcList = (List<TransactionResponseCodeDTOInter>) utillist.GetTransactionResponseCodeList();
        uSettledList = (List<UsersDTOInter>) utillist.GetSettledUsersList();
        ttList = (List<TransactionTypeDTOInter>) utillist.GetTransactionTypeList();
        tsList = (List<TransactionStatusDTOInter>) utillist.GetTransactionStatusList();
        atmgList = (List<AtmGroupDTOInter>) utillist.GetATMGroupList();
        mcTargetList = new ArrayList<ColumnDTOInter>();
        settled = super.getSettledList();

        mtInt1 = 1;
        mtInt = 2;
    }

    public void selectingCombo(ValueChangeEvent e) {
        comboValue = e.getNewValue().toString();
        if (comboValue.equals("Between")) {
            toShow = true;
            toShow_to = false;
        } else {
            toShow = false;
            toShow_to = true;
        }
    }

    public void groupChange(ValueChangeEvent e) {
        int grpId = (Integer) e.getNewValue();
        try {
            atmList = (List<AtmMachineDTOInter>) mcObject.getAtmMachines(sessionutil.GetUserLogging(), grpId);
        } catch (Throwable ex) {
            Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void selectingCombo1(ValueChangeEvent e) {
        comboValue = e.getNewValue().toString();
        if (comboValue.equals("Between")) {
            toShow1 = true;
            toShow1_to = false;
        } else {
            toShow1 = false;
            toShow1_to = true;
        }
    }

    public void selectingCombo2(ValueChangeEvent e) {
        comboValue = e.getNewValue().toString();
        if (comboValue.equals("Between")) {
            toShow2 = false;
        } else {
            toShow2 = true;
        }
    }

    public void resetVars() {
        message = "";

    }

    public void selectingCombo3(ValueChangeEvent e) {
        comboValue = e.getNewValue().toString();
        if (comboValue.equals("Between")) {
            toShow3 = true;
            toShow3_to = false;

        } else {
            toShow3 = false;
            toShow3_to = true;
        }
    }

    public void selectingCombo4(ValueChangeEvent e) {
        comboValue = e.getNewValue().toString();
        if (comboValue.equals("Between")) {
            toShow4 = true;
            toShow4_to = false;

        } else {
            toShow4 = false;
            toShow4_to = true;
        }
    }

    public void matchChange(ValueChangeEvent e) {
        newMaster = (Integer) e.getNewValue();
        for (MatchingTypeDTOInter masRec : mtList) {
            if ((Integer) e.getNewValue() == masRec.getId()) {
                masterRecord = masRec;
            }
        }

        if (masterRecord.getType1() == 1) {
            sType1 = "Journal";
        } else {
            sType1 = "Switch";
        }

        if (masterRecord.getType2() == 2) {
            sType2 = "Switch";
        } else {
            sType2 = "Host";
        }
        searchMessage = "";
        message = "";
    }

    public void doSearch() throws Throwable {

        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:disputeTable"))).setFirst(0);
        if (mtInt1 == 2) {
            fileFlag1 = false;
        } else {
            fileFlag1 = true;
        }
        dlgFlag = false;
        searchMessage = "";
        if (autoComp == 0) {
            if (atmidInt != null && !"".equals(atmidInt)) {
                result = TestAtmID(atmidInt);
                if (!result.equals("Found")) {
                    dlgFlag = true;
                    return;
                }
            }

        }
        if (searchMessage.length() == 0) {
            holderObject.reset();
            CorrectiveholderObject.reset();
            if (fcdPickList.getTarget().size() == 0) {
                nameCollapse = "Search by";
            } else {
                List<String> target = fcdPickList.getTarget();
                column = mcObject.getColumns(target);
                column1 = column;
                //column1.remove(0);
                String srtb = "";
                String fields = "";
                for (int i = 0; i < column1.size(); i++) {
                    fields += column1.get(i).getDbName();
                    if (i != column1.size() - 1) {
                        fields += ",";
                    }
                }
                List<DisputesDTOInter> mTempList;
                // = new ArrayList<MatchedDataDTOInter>();
                FacesContext context = FacesContext.getCurrentInstance();

                // session.setAttribute("selectStatment", null);
                String AtmValue = "", Seq = "";
                if (atmcString != null) {
                    AtmValue = atmcString.toString();
                }
                if (atmidInt != null) {
                    AtmValue = atmidInt;
                }
//                session.setAttribute("AtmValue", AtmValue);
                //              session.setAttribute("Cardno", cardSt);
                if (seqF != null) {
                    Seq = seqF.toString();
                }
                if (seqT != null) {
                    Seq = seqT.toString();
                }
//                session.setAttribute("Seq", Seq);
//                if (tranDateF != null && tranDateT != null) {
//                    session.setAttribute("FromDate", tranDateF);
//                    session.setAttribute("ToDate", tranDateT);
//                }
                mTempList = (List<DisputesDTOInter>) mcObject.searchMatchedData(tranDateF, tDate, tranDateT, settledDateF, sDate, settledDateT, null, null, null,
                        amountF, amount, amountT, seqF, sequance, seqT, cardSt, card_op,
                        accountSt, account_op.replace(" ", ""), null, tsInt, statusCode, ttInt, transTCode, rcInt, responseCode, cmInt,
                        atmcString, atmidInt, atmgroupInt, blBool, teDate, mtInt1, mtInt, usInt1, usInt, recordInt, reverseInt, srtb, sessionutil.GetUserLogging(), fields, disputeStatusInt, responseOp, responseOp1, col1St, col2St, col3St, col4St, col5St, col1op, col2op, col3op, col4op, col5op, collectedcard);

                //session.setAttribute("selectStatment", (List<DisputesDTOInter>) mTempList);
                otherSide = null;

                if (teDate == 1) {
                    if (!checkUnSettPrivilage()) {
                        settledState = "Mark as unsettled";
                    } else {
                        settledState = "unauthorized User";
                    }
                    settledFlag = false;
                } else if (teDate == 2) {
                    if (!checkSettPrivilage()) {
                        settledState = "Mark as settled";
                    } else {
                        settledState = "unauthorized User";
                    }
                    settledFlag = false;
                } else {
                    settledFlag = true;
                }

                if (mtInt1 == 1) {
                    if (mtInt == 1) {
                        showSide = "JR";
                        showSide_1 = "SW";
                    } else {
                        showSide = "SW";
                        showSide_1 = "JR";
                    }
                } else {
                    if (mtInt == 2) {
                        showSide = "SW";
                        showSide_1 = "GL";
                    } else {
                        showSide = "GL";
                        showSide_1 = "SW";
                    }
                }
                column = mcObject.getColumns(target);
                column1 = column;
                //column1.remove(0);
                searchParameter = mcObject.getSearchParameter();
                mDataList = mTempList;
                BigDecimal someNumber = mcObject.getTotalAmount();
                NumberFormat nf = NumberFormat.getInstance();
                if (someNumber != null) {
                    totalAmount = nf.format(someNumber);
                } else {
                    totalAmount = "0";
                }
                listSize = mDataList.size();
                PrintAll = mTempList;
                showSelection = true;
                collapseField = true;
            }
            nameCollapse = "Search by";
            printCheck = false;
            deselectFlag = false;
            selectFlag = true;
        }
        searchMessage = "";
        message = "";
    }

    public void changeName() {
        if (nameCollapse.equals("Close")) {
            nameCollapse = "Search by";
        } else {
            nameCollapse = "Close";
        }
        message = "";
        searchMessage = "";
    }

    public void print() throws Throwable {
        //column1.remove(0);
        List<String> target = fcdPickList.getTarget();
        column = mcObject.getColumns(target);
        column1 = column;

        if (mDataList.size() != 0 && !column1.isEmpty()) {
            for (int i = 0; i < column1.size(); i++) {
                if (column1.get(i).getId() == 1) {
                    column1.remove(i);
                }
                if ("ATM_NAME".equals(column1.get(i).getDbName().toUpperCase())) {
                    column1.remove(i);
                }
            }
            String fields = "";
            for (int i = 0; i < column1.size(); i++) {
                fields += column1.get(i).getDbName();
                if (i != column1.size() - 1) {
                    fields += ",";
                }
            }
            String path = "";
            FacesContext context = FacesContext.getCurrentInstance();

            path = (String) mcObject.findReport(fields, mcObject.getWhereCluase(), sessionutil.GetUserLogging(), column1, selectedItems, sessionutil.Getclient(), mtInt, listSize);
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect(path);
            column = mcObject.getColumns(target);
            column1 = column;

        }
    }

    public void markAsCorrectiveEntry() throws Throwable {
        try {
            if (correctiveText.equals("Corrective Entry")) {

                mcObject.MarkAsCorrectiveEntryCheck(selectedItems, sessionutil.GetUserLogging());

                for (int i = 0; i < selectedItems.length; i++) {
                    DisputesDTOInter entity = selectedItems[i];
                    entity.setcorrectivestatus(1);
                    entity.setsettledflag(1);
                }
                selectedItems = null;
                // doSearch();
            } else {
                mcObject.MarkAsCorrectiveEntryUncheck(selectedItems, sessionutil.GetUserLogging());
                for (int i = 0; i < selectedItems.length; i++) {
                    DisputesDTOInter entity = selectedItems[i];
                    entity.setcorrectivestatus(0);
                    entity.setsettledflag(2);
                }
                selectedItems = null;

                // doSearch();
            }
            BigDecimal someNumber = mcObject.getTotalAmount();
            NumberFormat nf = NumberFormat.getInstance();
            totalAmount = nf.format(someNumber);
            listSize = mDataList.size();
        } catch (Throwable ex) {
        }
    }

    public void exportData() {
        try {
            mcObject.MarkAsExport(selectedItems, sessionutil.GetUserLogging());
            doSearch();
            BigDecimal someNumber = mcObject.getTotalAmount();
            NumberFormat nf = NumberFormat.getInstance();
            totalAmount = nf.format(someNumber);
            listSize = mDataList.size();
        } catch (Throwable ex) {
        }
    }

    public void markAsDispute() {
        try {
            String backMsg = (String) mcObject.MatchedMarkAsMatched(selectedItems, sessionutil.GetUserLogging());
            if (!backMsg.equals("")) {
                message = "Records with sequence " + backMsg + " can't be matched";
            }
            for (DisputesDTOInter m : selectedItems) {
                if (m.getdisputekey() != 0) {
                    mDataList.remove(m);
                }
            }
            BigDecimal someNumber = mcObject.getTotalAmount();
            NumberFormat nf = NumberFormat.getInstance();
            totalAmount = nf.format(someNumber);
            listSize = mDataList.size();
        } catch (Throwable ex) {
        }
    }
    public Integer status2 = 4, status = 4, flag = 4;

    public void onRowUnSelect(UnselectEvent e) {
        searchMessage = "";
        message = "";
        DisputesDTOInter holder = (DisputesDTOInter) e.getObject();
        if (holder.getBlacklist() == 1) {
            blackFlag = true;
        } else {
            blackFlag = false;
        }
        status = holderObject.remove(holder.getrownum());
        if (status != 3) {
            if (status.equals(1)) {
                try {
                    if (!checkUnSettPrivilage()) {
                        settledState = "Mark as not settled";
                    } else {
                        settledState = "unauthorized User";
                    }

                    settledFlag = false;
                } catch (Throwable ex) {
                    Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (status.equals(2)) {
                try {
                    if (!checkSettPrivilage()) {
                        settledState = "Mark as settled";
                    } else {
                        settledState = "unauthorized User";
                    }
                    settledFlag = false;
                } catch (Throwable ex) {
                    Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            settledFlag = true;
        }

        status2 = CorrectiveholderObject.remove(holder.getrownum());

        if (status2 != 3) {
            if (status2.equals(0)) {
                correctiveText = "Corrective Entry";
                correctiveDisable = false;
                exportDisabled = true;
            } else if (status2.equals(1)) {
                correctiveText = "unselect Corrective";
                correctiveDisable = false;
                exportDisabled = false;
            } else {
                correctiveDisable = true;
                exportDisabled = true;
            }
        } else {
            correctiveDisable = true;
            exportDisabled = true;
        }
        searchMessage = "";
        message = "";
//correctiveText
    }

    public void printUpdate(ValueChangeEvent e) {
        printCheck = (Boolean) e.getNewValue();
        if (printCheck == true) {
            // DataTable holdTable =(DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:disputeTable");
            selectedItems = null;
        }
    }

    public void onRowSelect(SelectEvent e) {
        searchMessage = "";
        message = "";
        printCheck = false;
        DisputesDTOInter holder = (DisputesDTOInter) e.getObject();
        status = holderObject.add(holder.getrownum(), holder.getsettledflag());
        if (holder.getBlacklist() == 1) {
            blackFlag = true;
        } else {
            blackFlag = false;
        }
        //System.out.println(selectedItems.length);
        if (holder.getsettledflag() != 3) {
            if (status != 3) {
                //flag = holder.getSettledFlag();
                if (status.equals(1)) {
                    try {
                        if (!checkUnSettPrivilage()) {
                            settledState = "Mark as not settled";
                        } else {
                            settledState = "unauthorized User";
                        }

                        settledFlag = false;
                    } catch (Throwable ex) {
                        Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (status.equals(2)) {
                    try {
                        if (!checkSettPrivilage()) {
                            settledState = "Mark as settled";
                        } else {
                            settledState = "unauthorized User";
                        }

                        settledFlag = false;
                    } catch (Throwable ex) {
                        Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                settledFlag = true;
            }

            status2 = CorrectiveholderObject.add(holder.getrownum(), holder.getcorrectivestatus());

            if (status2 != 3) {
                if (status2.equals(0)) {
                    correctiveText = "Corrective Entry";
                    correctiveDisable = false;
                    exportDisabled = true;
                } else if (status2.equals(1)) {
                    correctiveText = "unselect Corrective";
                    correctiveDisable = false;
                    exportDisabled = false;
                } else if (status2.equals(2)) {
                    correctiveText = "unselect Corrective";
                    correctiveDisable = false;
                    exportDisabled = true;
                } else {
                    correctiveDisable = true;
                    exportDisabled = true;
                }
            } else {
                correctiveDisable = true;
                exportDisabled = true;
            }
        } else {
            settledFlag = true;
            correctiveDisable = true;
            exportDisabled = true;
        }
    }

    public void markAsBlackListed() throws Throwable {
        for (DisputesDTOInter record : selectedItems) {
            if (record.getBlacklist() != 1) {
                mcObject.insertBlackListed(record, sessionutil.GetUserLogging().getLogonName());
                for (DisputesDTOInter rec : selectedItems) {
                    if (rec.getcardno().equals(record.getcardno())) {
                        rec.setBlacklist(1);
                    }
                }
            }
        }
    }

    public void markAsSettled() throws Throwable {
        if (settledState.equals("Mark as settled")) {
            mcObject.MatchedMarkAsSettled(selectedItems, sessionutil.GetUserLogging());
            holderObject.reset();
            for (DisputesDTOInter m : selectedItems) {
                m.setsettledflag(1);
            }
            selectedItems = null;
        }
        if (settledState.equals("Mark as not settled")) {
            mcObject.MatchedMarkAsNotSettled(selectedItems, sessionutil.GetUserLogging());
            holderObject.reset();
            for (DisputesDTOInter m : selectedItems) {
                m.setsettledflag(2);
            }
            selectedItems = null;
        }
        if (teDate != 3) {
            for (DisputesDTOInter m : selectedItems) {
                mDataList.remove(m);
            }
            selectedItems = null;
        }

        searchMessage = "";
        message = "";
        BigDecimal someNumber = mcObject.getTotalAmount();
        NumberFormat nf = NumberFormat.getInstance();
        totalAmount = nf.format(someNumber);
        listSize = mDataList.size();
    }
//selectedItems

    public void showOtherSide(ActionEvent e) throws Throwable {
        DisputesDTOInter m = (DisputesDTOInter) e.getComponent().getAttributes().get("otherOther_dis");
        otherSide = (List<DisputesDTOInter>) mcObject.getAnotherSideOfMatchedData(m);
        searchMessage = "";
        message = "";
    }

    public void gettexttransaction_other(ActionEvent e) throws Throwable {
        DisputesDTOInter m = (DisputesDTOInter) e.getComponent().getAttributes().get("texttrans_other");
        textfile_other = (String) mcObject.gettexttransaction_other(m).toString();
    }

    public void getFiletransaction_other(ActionEvent e) throws Throwable {
        DisputesDTOInter m = (DisputesDTOInter) e.getComponent().getAttributes().get("textfile_other");
        sa = (Clob) mcObject.FindFiletransaction_other(m);
        transfile_other = clobToString(sa);

    }

    public void gettexttransaction(ActionEvent e) throws Throwable {
        DisputesDTOInter m = (DisputesDTOInter) e.getComponent().getAttributes().get("texttrans");
        textfile = mcObject.gettexttransaction(m).toString();
    }

    public void getFiletransaction(ActionEvent e) throws Throwable {
        DisputesDTOInter m = (DisputesDTOInter) e.getComponent().getAttributes().get("textfile");
        sa = (Clob) mcObject.FindFiletransaction(m);
        textfile = clobToString(sa);
    }

    public String TestAtmID(String atmidInt) {
        String MSG = "";
        for (int i = 0; i < atmList.size(); i++) {
            if (atmList.get(i).getApplicationId().toString().equals(atmidInt)) {
                return "Found";
            }
        }
        return "ATM " + atmidInt + " is not assigned for this user";
    }

    public List<String> complete(String query) {
        List<String> results = new ArrayList<String>();
        for (int i = 0; i < atmList.size(); i++) {
            if (atmList.get(i).getApplicationId().toString().startsWith(query)) {
                results.add(atmList.get(i).getApplicationId().toString());
            }
        }
        return results;
    }

    public void printFile() {
        try {

            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            Integer Seq = mcObject.PrintLines(hiddenText1);
            String path = (String) mcObject.PrintLinesReport(Seq);
            response.sendRedirect(path);

        } catch (Exception ex) {
            Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void printFile_other() {
        try {

            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            Integer Seq = mcObject.PrintLines(hiddenText1_other);
            String path = (String) mcObject.PrintLinesReport(Seq);
            response.sendRedirect(path);

        } catch (Exception ex) {
            Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//
//    public String[] SplitString(String Text) throws Exception {
//        String Text2 = Text.replace("\n", "").replaceAll("(\\r|\\n|\\t)", "");
//        return Text2.split("(?<=\\G.{80})");
//    }
//hiddenText1_other,textfile_other

    public void printOrg_other() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            Integer Seq = mcObject.PrintLines(textfile);
            String path = (String) mcObject.PrintLinesReport(Seq);
            response.sendRedirect(path);

        } catch (Exception ex) {
            Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void printOrg() {
        try {

            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            Integer Seq = mcObject.PrintLines(textfile);
            String path = (String) mcObject.PrintLinesReport(Seq);
            response.sendRedirect(path);

        } catch (Exception ex) {
            Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static byte[] stringToBytesASCII(String str) {
        char[] buffer = str.toCharArray();
        byte[] b = new byte[buffer.length];
        for (int i = 0; i < b.length; i++) {
            b[i] = (byte) buffer[i];
        }
        return b;
    }

    public byte[] CreateArray(byte[] a, byte[] b) {
        byte[] result = new byte[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    public String clobToString(Clob key) {
        if (key != null) {
            Object value = null;
            {
                java.io.InputStream is = null;
                try {
                    is = key.getAsciiStream();
                    byte[] buffer = new byte[4096];
                    byte[] breakpoint = new byte[4096];
                    breakpoint = stringToBytesASCII("braaa");
                    java.io.OutputStream outputStream = new java.io.ByteArrayOutputStream();
                    while (true) {
                        int read = is.read(buffer);
                        if (read == -1) {
                            break;
                        }
                        outputStream.write(CreateArray(buffer, breakpoint), 0, read);

                    }
                    outputStream.close();
                    is.close();

                    value = outputStream.toString();
                } catch (SQLException ex) {
                    Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
                } catch (java.io.IOException io_ex) {
                } finally {
                    try {
                        if (is.available() > 0) {
                            is.close();
                        }

                    } catch (IOException ex) {
                        Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            return value.toString();
        } else {
            return "";
        }
    }

    public DisputesDTOInter getdAtt() {
        return dAtt;
    }

    public void setdAtt(DisputesDTOInter dAtt) {
        this.dAtt = dAtt;
    }

    public void showCorrectiveAmount(ActionEvent e) throws Throwable {
        DisputesDTOInter m = (DisputesDTOInter) e.getComponent().getAttributes().get("coramount_dis");
        mcObject.updatecorrectiveamount(m, sessionutil.GetUserLogging());
        searchMessage = "";
        message = "";
    }

    public void updateCorramountPop(ActionEvent e) {
        dAtt = (DisputesDTOInter) e.getComponent().getAttributes().get("coramount_dis");
        corramount = dAtt.getCorrectiveamount();
    }

    public void updateCommentPop(ActionEvent e) {
        dAtt = (DisputesDTOInter) e.getComponent().getAttributes().get("commentAttr");
        if (dAtt.getcomments() == null) {
            comment = "";
        } else {
            comment = dAtt.getcomments();
        }
    }

    public void updateCorr() {
        try {
            dAtt.setCorrectiveamount(corramount);
            mcObject.updatecorrectiveamount(dAtt, sessionutil.GetUserLogging());
        } catch (Throwable ex) {
            Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateComment() {
        try {
            dAtt.setcomments(comment);
            mcObject.updateComment(dAtt);
        } catch (Throwable ex) {
            Logger.getLogger(DisputeClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void selectAllRows() {
        DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:disputeTable");
        repTable.setSelection(mDataList.toArray());
        blackFlag = true;
        int temp = 4, result = 4;
        for (int i = 0; i < mDataList.size(); i++) {
            temp = mDataList.get(i).getcorrectivestatus();
            if (result != 4) {
                if (result != temp) {
                    result = 3;
                }
            } else {
                result = temp;
            }
        }
        if (result == temp) {
            if (result != 3) {
                if (result == 0) {
                    correctiveText = "Corrective Entry";
                    correctiveDisable = false;
                    exportDisabled = true;
                } else if (result == 1) {
                    correctiveText = "unselect Corrective";
                    correctiveDisable = false;
                    exportDisabled = false;
                }
            } else {
                correctiveDisable = true;
                exportDisabled = true;
            }
        } else {
            correctiveDisable = true;
            exportDisabled = true;
        }
        printCheck = true;
        deselectFlag = true;
        selectFlag = false;
        searchMessage = "";
        message = "";
    }

    public void deselectAllRows() {
        printCheck = false;
        deselectFlag = false;
        selectFlag = true;
        blackFlag = true;
        searchMessage = "";
        message = "";
    }

    public void renewText(String id) {
        textfile = id;
        textfile = id;
    }

    public void checkName(ValueChangeEvent e) {
        atmcString = (Integer) e.getNewValue();
        if (atmidInt != null) {
            atmcString = 0;
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Warning, Clear ATM ID first", "Clear ATM ID first"));
        }
    }

    public void checkId(ValueChangeEvent e) {
        atmidInt = e.getNewValue().toString();

    }

    public void accountChange(ValueChangeEvent e) {
        String newValue = e.getNewValue().toString();
        if (newValue.equals("is null") || newValue.equals("is not null")) {
            accountFlag = false;
            accountFlag1 = true;
        } else {
            accountFlag = true;
            accountFlag1 = false;
        }
    }

    public void cardChange(ValueChangeEvent e) {
        String newValue = e.getNewValue().toString();
        if (newValue.equals("is null") || newValue.equals("is not null")) {
            cardFlag = false;
            cardFlag1 = true;
        } else {
            cardFlag = true;
            cardFlag1 = false;
        }
    }

    public void masterRecordChange(ValueChangeEvent e) {
        Integer newV = (Integer) e.getNewValue();
        if (newV == 1) {
            fileFlag = true;
            fileFlag1 = false;
        } else {
            fileFlag = false;
            fileFlag1 = true;
        }
    }

    public void exportexcel() throws Throwable {
        ExportExcel sm = new ExportExcel();
        sm.exportexcel(column1, selectedItems);
    }

    public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();

    }

    public void onPaging(PageEvent e) {
        System.out.println(e.getPage());
    }
}
