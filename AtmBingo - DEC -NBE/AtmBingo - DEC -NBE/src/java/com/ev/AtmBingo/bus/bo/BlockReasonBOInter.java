/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;

/**
 *
 * @author Administrator
 */
public interface BlockReasonBOInter extends BaseBOInter{

  

    Object editeBlockReason(Object obj, int operation)throws Throwable;

    Object getBlockReasons() throws Throwable;

}
