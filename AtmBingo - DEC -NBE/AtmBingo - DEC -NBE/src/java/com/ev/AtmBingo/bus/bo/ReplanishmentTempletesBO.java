/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.AtmMachineTypeDAOInter;
import com.ev.AtmBingo.bus.dao.CassetteDAOInter;
import com.ev.AtmBingo.bus.dao.CurrencyMasterDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.ReplanishmentTempletesDAO;
import com.ev.AtmBingo.bus.dao.ReplanishmentTempletesDAOInter;
//import com.ev.AtmBingo.bus.dao.ReplanishmentTempletesDetDAOInter;
import com.ev.AtmBingo.bus.dao.ReplanishmentTempletesDetDAOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ReplanishmentTempletesDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentTempletesDetDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ReplanishmentTempletesBO extends BaseBO implements ReplanishmentTempletesBOInter ,Serializable{

    protected ReplanishmentTempletesBO() {
        super();
    }

    public Object getReplanishmentTempletesMaster() throws Throwable {
        ReplanishmentTempletesDAOInter rtDAO = DAOFactory.createReplanishmentTempletesDAO();
        List<ReplanishmentTempletesDTOInter> rtDTOL = (List<ReplanishmentTempletesDTOInter>) rtDAO.findAll();

        return rtDTOL;
    }
        public Object getReplanishmentmTempletesDetails(int masterId) throws Throwable {
        ReplanishmentTempletesDetDAOInter rtdDAO = DAOFactory.createReplanishmentTempletesDetDAO();
        List<ReplanishmentTempletesDetDTOInter> rtDTOL = (List<ReplanishmentTempletesDetDTOInter>) rtdDAO.findByMasterId(masterId );

        return rtDTOL;
    }

    public Object getAtmMachineType() throws Throwable {
        AtmMachineTypeDAOInter amtDAO = DAOFactory.createAtmMachineTypeDAO(null);
        List<AtmMachineTypeDTOInter> amtDTOL = (List<AtmMachineTypeDTOInter>) amtDAO.findAll();
        return amtDTOL;
    }

    public Object editeReplanishmentTempletesMaster(ReplanishmentTempletesDTOInter rtmDTO, int operation) throws Throwable {

        ReplanishmentTempletesDAOInter rtmDAO = DAOFactory.createReplanishmentTempletesDAO();

        switch (operation) {
            case INSERT:
                rtmDAO.insert(rtmDTO);
                return "Insert Done";
            case UPDATE:
                rtmDAO.update(rtmDTO);
                return "Update Done";
            case DELETE:
                rtmDAO.delete(rtmDTO);
                return "Delete Done";
            default:
                return "There Is No Operation";
        }
    }

    public Object editReplanishmentTempletesDetAll(List<ReplanishmentTempletesDetDTOInter> rtdDTOL, int operation) throws Throwable {
        ReplanishmentTempletesDetDAOInter rtdDAO = DAOFactory.createReplanishmentTempletesDetDAO();
        switch (operation) {
            case INSERT:
                for (ReplanishmentTempletesDetDTOInter r : rtdDTOL) {
                    rtdDAO.insert(r);
                }
                return "Insert Done";
            case UPDATE:
                for (ReplanishmentTempletesDetDTOInter r : rtdDTOL) {
                    rtdDAO.update(r);
                }
                return "Update Done";
            case DELETE:
                for (ReplanishmentTempletesDetDTOInter r : rtdDTOL) {
                    rtdDAO.delete(r);
                }
                return "Delete Done";
            default:
                return "There Is No Operation";
        }
    }

    public Object editeReplanishmentTempletesDetSingle(ReplanishmentTempletesDetDTOInter rtmdDTO, int operation , int oldType , int oldTypeNo, int idMaster) throws Throwable {

        ReplanishmentTempletesDetDAOInter rtmdDAO = DAOFactory.createReplanishmentTempletesDetDAO();

        switch (operation) {
            case INSERT:
                rtmdDAO.insert(rtmdDTO);
                return "Insert Done";
            case UPDATE:
                rtmdDAO.update(rtmdDTO,idMaster,oldType,oldTypeNo);
                return "Update Done";
            case DELETE:
                rtmdDAO.delete(rtmdDTO);
                return "Delete Done";
            default:
                return "There Is No Operation";
        }
    }

    public Object getCurrencyMaster() throws Throwable {
        CurrencyMasterDAOInter cmDAO = DAOFactory.createCurrencyMasterDAO(null);
        List<CurrencyMasterDTOInter> cmDTOL = (List<CurrencyMasterDTOInter>) cmDAO.findAll();
        return cmDTOL;
    }

    public Object getCassitte() throws Throwable {
        CassetteDAOInter cmDAO = DAOFactory.createCassetteDAO(null);
        List<CassetteDTOInter> cmDTOL = (List<CassetteDTOInter>) cmDAO.findAll();
        return cmDTOL;
    }
    public Object findByIdMaster (int id) throws Throwable {
        ReplanishmentTempletesDAOInter rtDAO = DAOFactory.createReplanishmentTempletesDAO();
        ReplanishmentTempletesDTOInter rpDTO = (ReplanishmentTempletesDTOInter) rtDAO.find(id);
        return rpDTO ;
    }
    public static void main (String[]args) throws Throwable{
        ReplanishmentTempletesBOInter rtb = BOFactory.createReplanishmenTempletesBOInter(null);
        ReplanishmentTempletesDetDTOInter rtmDTO = DTOFactory.createReplanishmentTempletesDetDTO();
        List<ReplanishmentTempletesDetDTOInter> rtmDTOL = (List<ReplanishmentTempletesDetDTOInter>)rtb.getReplanishmentmTempletesDetails(7518);
        rtmDTO =rtmDTOL.get(0);
        int oldType= rtmDTO.getType();
        int oldTypeNo = rtmDTO.getTypeNo();
        rtmDTO.setType(8);
        rtmDTO.setTypeNo(10);
        rtmDTO.setStartingString("Ahmex");
        rtb.editeReplanishmentTempletesDetSingle(rtmDTO, UPDATE, oldType,oldTypeNo,rtmDTO.getId());
    }
}
