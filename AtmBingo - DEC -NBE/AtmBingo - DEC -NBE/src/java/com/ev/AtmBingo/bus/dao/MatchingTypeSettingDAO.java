/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.MatchingTypeSettingDTOInter;
import java.sql.CallableStatement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class MatchingTypeSettingDAO extends BaseDAO implements MatchingTypeSettingDAOInter {

    protected MatchingTypeSettingDAO() {
        super();
        super.setTableName("matching_type_setting");

    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchingTypeSettingDTOInter matchingTypeSetting = (MatchingTypeSettingDTOInter) obj[0];
        String insertStat = "insert into " + super.getTableName() + " values(" + matchingTypeSetting.getId() + ","
                + matchingTypeSetting.getColumnId() + "," + matchingTypeSetting.getPartSetting() + "," + matchingTypeSetting.getPartLength() + "," + matchingTypeSetting.getMatchingType() + ")";
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchingTypeSettingDTOInter matchingTypeSetting = (MatchingTypeSettingDTOInter) obj[0];
        Integer oldColumnId = (Integer) obj[1];
        Integer oldPartSetting = (Integer) obj[2];
        String updateStat = "update " + super.getTableName() + " set column_id = " + matchingTypeSetting.getColumnId() + ",part_setting = " + matchingTypeSetting.getPartSetting()
                + ", part_length = " + matchingTypeSetting.getPartLength()
                + " where id = " + matchingTypeSetting.getId() + " and column_id = " + oldColumnId + " and part_setting = " + oldPartSetting;
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchingTypeSettingDTOInter matchingTypeSetting = (MatchingTypeSettingDTOInter) obj[0];
        String deleteStat = "delete from " + super.getTableName() + " where id = " + matchingTypeSetting.getId() + " and column_id = " + matchingTypeSetting.getColumnId() + " and part_setting = " + matchingTypeSetting.getPartSetting();
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }


    public Object RecreateIndex(Object... obj) throws Throwable {
        super.preCollable();
        CallableStatement stat = super.executeCallableStatment("{call  create_matching_indexes}");
        stat.execute();
        super.postCollable(stat);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = null;
        Integer matchingTypeId = (Integer) obj[0];
        if (obj.length == 2) {
            Integer machineTypeId = (Integer) obj[1];
            selectStat = "select * from " + super.getTableName() + " where id = " + matchingTypeId + " and machine_type = " + machineTypeId;

            selectStat = "select * from " + super.getTableName() + " where id = $1 $cond1 $cond2 ";

            selectStat = selectStat.replace("$1", "" + matchingTypeId);
            selectStat = selectStat.replace("$cond1", "and machine_type = " + machineTypeId);
            selectStat = selectStat.replace("$cond2", "");

        } else {
            selectStat = "select * from " + super.getTableName() + " where id = " + matchingTypeId;
        }
        ResultSet result = executeQuery(selectStat);
        List<MatchingTypeSettingDTOInter> mtL = new ArrayList<MatchingTypeSettingDTOInter>();
        while (result.next()) {
            MatchingTypeSettingDTOInter mt = DTOFactory.createMatchingTypeSettingDTO();
            mt.setId(result.getInt("id"));
            mt.setColumnId(result.getInt("column_id"));
            mt.setMatchingType(result.getInt("machine_type"));
            mt.setPartLength(result.getBigDecimal("part_length"));
            mt.setPartSetting(result.getInt("part_setting"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();

        super.postSelect();
        return null;
    }
}
