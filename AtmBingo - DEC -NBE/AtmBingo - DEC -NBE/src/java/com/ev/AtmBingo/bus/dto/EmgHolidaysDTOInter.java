/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface EmgHolidaysDTOInter extends Serializable {

    String getReason();

    String geteReason();

    Date getpFrom();

    Date getpTo();

    void setReason(String reason);

    void seteReason(String eReason);

    void setpFrom(Date pFrom);

    void setpTo(Date pTo);

}
