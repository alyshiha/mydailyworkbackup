/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.NotificationDAOInter;
import java.io.Serializable;
import java.util.List;
/**
 *
 * @author Administrator
 */
public class NotificationBO extends BaseBO implements NotificationBOInter,Serializable {

      protected NotificationBO() {
        super();
    }

       public Object getNotifications () throws Throwable {
        NotificationDAOInter NDAO = DAOFactory.createNotificationDAO();
        List<NotificationDAOInter> Result = (List<NotificationDAOInter>) NDAO.findAllNotification();
        return Result ;
    }
      public Object getErrors () throws Throwable {
        NotificationDAOInter NDAO = DAOFactory.createNotificationDAO();
        List<NotificationDAOInter> Result = (List<NotificationDAOInter>) NDAO.findAllError();
        return Result ;
    }

}
