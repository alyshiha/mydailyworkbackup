/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.SystemTimesDTOInter;

/**
 *
 * @author Administrator
 */
public interface WorkingTimesBOIntert extends BaseBOInter{

    Object editeSystemTimes(SystemTimesDTOInter stDTO, int operation) throws Throwable;

    Object getSystemTime() throws Throwable;

}
