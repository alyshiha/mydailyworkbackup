/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author KhAiReE
 */
public interface NetworkGroupDTOInter extends Serializable {

    Integer getRowid();
    void setRowid(Integer rowid);
    Integer getGroupId();

    String getGroupName();

    void setGroupId(Integer groupId);

    void setGroupName(String groupName);

    public String getNetwork() ;

    public void setNetwork(String network);

}
