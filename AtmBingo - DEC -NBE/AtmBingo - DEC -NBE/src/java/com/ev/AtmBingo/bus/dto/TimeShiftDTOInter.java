/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TimeShiftDTOInter extends Serializable {

    void setTimeShiftDetails(List<TimeShiftDetailDTOInter> timeShiftDetails);

    List<TimeShiftDetailDTOInter> getTimeShiftDetails()throws Throwable;

    String getShiftAName();

    String getShiftEName();

    int getShiftId();

    void setShiftAName(String shiftAName);

    void setShiftEName(String shiftEName);

    void setShiftId(int shiftId);

}
