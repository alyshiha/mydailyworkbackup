/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Aly
 */
public interface CardsTakenReportDTOInter extends Serializable {

    String getAtmaplication();

    String getCardonfile();

    String getCaronatm();

    void setAtmaplication(String atmaplication);

    void setCardonfile(String cardonfile);

    void setCaronatm(String caronatm);
    
}
