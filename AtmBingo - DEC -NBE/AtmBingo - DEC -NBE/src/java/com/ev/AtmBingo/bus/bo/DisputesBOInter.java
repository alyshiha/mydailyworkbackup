/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.DisputesDTOInter;
import com.ev.AtmBingo.bus.dto.RepTransDetailDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface DisputesBOInter extends BaseBOInter {

    Object insertBlackListed(DisputesDTOInter REC, String User) throws Throwable;

    Object MarkAsExport(DisputesDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable;

    Object MarkAsCorrectiveEntryUncheckrep(RepTransDetailDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable;

    Object MarkAsCorrectiveEntryUncheck(DisputesDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable;

    Object MarkAsCorrectiveEntryCheckrep(RepTransDetailDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable;

    Object MarkAsCorrectiveEntryCheck(DisputesDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable;

    Object MatchedMarkAsNotSettled(DisputesDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable;

    Object gettexttransaction(DisputesDTOInter mdDTO) throws Throwable;

    Object FindFiletransaction(DisputesDTOInter mdDTO) throws Throwable;

    Object getPickColumn() throws Throwable;

    Object updateComment(DisputesDTOInter dDTO) throws Throwable;

    Object updateComment(RepTransDetailDTOInter dDTO) throws Throwable;

    Object MatchedMarkAsMatched(DisputesDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable;

    Object MatchedMarkAsSettled(DisputesDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable;

    Object MatchedMarkAsSettled(RepTransDetailDTOInter[] mdDTOA, UsersDTOInter logedUser) throws Throwable;

    Integer PrintLines(String mdDTO) throws Exception;

    String PrintLinesReport(Integer Seq) throws Exception;

    Object findReport(String fields, String whereCluase, UsersDTOInter logedUser, List<ColumnDTOInter> c, DisputesDTOInter[] dDTOArr, String cust, Integer mtInt, Integer listSize) throws Throwable;

    Object findReport(String fields, String whereCluase, UsersDTOInter logedUser, List<ColumnDTOInter> c, List<DisputesDTOInter> dDTOArr, String cust, Integer mtInt, Integer listSize) throws Throwable;

    Object getAnotherSideOfMatchedData(DisputesDTOInter mdDTO) throws Throwable;

    Object gettexttransaction_other(DisputesDTOInter mdDTO) throws Throwable;

    Object FindFiletransaction_other(DisputesDTOInter mdDTO) throws Throwable;

    Object findTotalAmount(Date transDateF, String opt1, Date transDateT, Date settleDateF, String opt2, Date settleDateT,
            Date loadDateF, String opt3, Date loadDateT, Integer amountF, String opt4, Integer amountT, Integer transSeqOrderByF,
            String opt5, Integer transSeqOrderByT, String cardNo, String opt6, String accountNo, String opt7, String notesPre,
            int transStatusCode, String transactionStatus, int transTypeCode, String transType, int responseCode,
            String response, int currencyCode, int atmId,
            String atmCode, int atmGroup, boolean blackList, int settled, int matchingTypeList, int masterRecord,
            int settledBy, int disputedBy, int missingSide, int reverseType,
            String sortBy, UsersDTOInter logedUser, String field, int correctiveentry, Integer ResponseVariable, Integer ResponseVariable2, String col1, String col2, String col3, String col4, String col5, String colop1, String colop2, String colop3, String colop4, String colop5, Boolean collected) throws Throwable;

    Object getAtmGroup() throws Throwable;

    Object getAtmMachines(UsersDTOInter loggedinUser, Integer grpId) throws Throwable;

    List<ColumnDTOInter> getColumns(List columns) throws Throwable;

    Object getCurrencyMaster() throws Throwable;

    Object getFileColumnDefinition() throws Throwable;

    Object getMatchingType() throws Throwable;

    String getSearchParameter();

    Object getSettledUsers() throws Throwable;

    Object getTransactionResponseCode() throws Throwable;

    Object getTransactionStatus() throws Throwable;

    Object getTransactionType() throws Throwable;

    String getWhereCluase();

    Object searchMatchedData(Date transDateF, String opt1, Date transDateT, Date settleDateF, String opt2, Date settleDateT, Date loadDateF, String opt3, Date loadDateT, Integer amountF, String opt4, Integer amountT, Integer transSeqOrderByF, String opt5, Integer transSeqOrderByT, String cardNo, String opt6, String accountNo, String opt7, String notesPre, int transStatusCode, String transactionStatus, int transTypeCode, String transType, int responseCode, String response, int currencyCode, int atmId, String atmCode, int atmGroup, boolean blackList, int settled, int matchingTypeList, int masterRecord, int settledBy, int disputedBy, int missingSide, int reverseType, String sortBy, UsersDTOInter logedUser, String field, int CorrectiveExport, Integer ResponseVariable, Integer ResponseVariable2, String col1, String col2, String col3, String col4, String col5, String colop1, String colop2, String colop3, String colop4, String colop5, Boolean collected) throws Throwable;

    void setSearchParameter(String searchParameter);

    void setWhereCluase(String whereCluase);

    public BigDecimal getTotalAmount();

    public void setTotalAmount(BigDecimal totalAmount);

    Object updatecorrectiveamount(DisputesDTOInter mdDTOA, UsersDTOInter user) throws Throwable;
}
