/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.TransactionTypeMasterDTOInter;
import java.sql.Clob;

/**
 *
 * @author Administrator
 */
public interface TransactionTypeDAOInter {


    Object findByMaster(TransactionTypeMasterDTOInter ttmDTO) throws Throwable;

    Object delete(Object... obj) throws Throwable;

    Object find(Object... obj) throws Throwable;

    Object findAll() throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;
}
