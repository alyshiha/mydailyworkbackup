/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.cardissuerDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface BanksTotalRepBOInter {

    Object getGrp()throws Throwable;

    String runReport(String dateFrom, String dateTo, int atmGroupInt, String user, int side,String cust) throws Throwable;
    List<cardissuerDTOInter> runReportrxcel(String dateFrom, String dateTo, int atmGroupInt, String user, int side, String cust) throws Throwable ;
}
