/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.ProfileDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileMenuDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileMenuItemDTOInter;
import org.primefaces.model.TreeNode;

/**
 *
 * @author Administrator
 */
public interface ProfileDetailsBOInter extends BaseBOInter {

    Object findMenuLinks()throws Throwable;

    Object getItemBingo(int profileId,int menuId)throws Throwable;

    Object editProfileMenuItem(ProfileMenuItemDTOInter pmDTO,int operation)throws Throwable;

    Object editProfileMenu(ProfileMenuDTOInter pmDTO, int operation )throws Throwable;

    Object editProfile(ProfileDTOInter pDTO, int operation)throws Throwable;
boolean ProfileHasUser(ProfileDTOInter pDTO)throws Throwable;
    Object getAllItemBingo() throws Throwable;

    TreeNode getMenuTree(int rest) throws Throwable;

    Object getProfiles(int rest) throws Throwable;

}
