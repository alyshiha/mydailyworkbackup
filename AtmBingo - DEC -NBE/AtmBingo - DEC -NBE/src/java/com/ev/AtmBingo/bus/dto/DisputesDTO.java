/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.DisputeRulesDAOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import oracle.sql.CLOB;

/**
 *
 * @author Administrator
 */
public class DisputesDTO extends BaseDTO implements DisputesDTOInter , Serializable{

    private Date transactiondate;
    private String transactionseqeunce;
    private String transactiontype;

    private int fileid;
    private Date loadingdate;
    private String atmapplicationid, atmname;
    private AtmMachineDTOInter atmid;
    private String currency;
    private String responsecode;
    private String transactionstatus;

    private TransactionTypeDTOInter transactiontypeid;
    private CurrencyMasterDTOInter currencyid;
    private TransactionStatusDTOInter transactionstatusid;
    private TransactionResponseCodeDTOInter responsecodeid;
    private String cardno, disputekeyuk;
    private Integer correctivestatus;
    private Integer blacklist;
    private Integer collected;
    private Boolean isjournal;

    public Boolean getIsjournal() {
        return isjournal;
    }

    public void setIsjournal(Boolean isjournal) {
        this.isjournal = isjournal;
    }

    public Integer getcollected() {
        return collected;
    }

    public void setcollected(Integer collected) {
        this.collected = collected;
    }

    public String getatmname() {
        return atmname;
    }

    public void setatmname(String atmname) {
        this.atmname = atmname;
    }

    public Integer getBlacklist() {
        return blacklist;
    }

    public void setBlacklist(Integer blacklist) {
        this.blacklist = blacklist;
    }

    public String getDisputekeyuk() {
        return disputekeyuk;
    }

    public void setDisputekeyuk(String disputekeyuk) {
        this.disputekeyuk = disputekeyuk;
    }

    public String getCorrectiveamount() {
        return correctiveamount;
    }

    public void setCorrectiveamount(String correctiveamount) {
        this.correctiveamount = correctiveamount;
    }

    private int amount;
    private Date settlementdate;
    private String notespresented, correctiveamount;
    private String customeraccountnumber;
    private int transactionsequenceorderby;
    private Date transactiontime;

    private int matchingtype;
    private int recordtype;
    private int disputekey;
    private String disputereason;
    private int settledflag;
    private Date settleddate;
    private UsersDTOInter settleduser;
    private String comments;
    private int disputeby;
    private Date disputedate;
    private int disputewithroles;
    private String column1;
    private String column2;
    private String column3;
    private String column4;
    private String column5;
    private int transactiontypemaster;
    private int responsecodemaster;
    private int cardnosuffix;
    private String reasontodisp;
    private String rownum;
    private CLOB transactiondata;
    private String exportuser;
    private String correctiveuser;
    private Date correctivedate;
    private Date exportdate;

    public String getExportuser() {
        return exportuser;
    }

    public void setExportuser(String exportuser) {
        this.exportuser = exportuser;
    }

    public String getCorrectiveuser() {
        return correctiveuser;
    }

    public void setCorrectiveuser(String correctiveuser) {
        this.correctiveuser = correctiveuser;
    }

    public Date getCorrectivedate() {
        return correctivedate;
    }

    public void setCorrectivedate(Date correctivedate) {
        this.correctivedate = correctivedate;
    }

    public Date getExportdate() {
        return exportdate;
    }

    public void setExportdate(Date exportdate) {
        this.exportdate = exportdate;
    }

    public CLOB gettransactiondata() {
        return transactiondata;
    }

    public void settransactiondata(CLOB transactiondata) {
        this.transactiondata = transactiondata;
    }

    public String gettransactionsequence() {
        return transactionseqeunce;
    }

    public void settransactionsequence(String transactionsequence) {
        this.transactionseqeunce = transactionsequence;
    }

    public Integer getcorrectivestatus() {
        return correctivestatus;
    }

    public void setcorrectivestatus(Integer Correctivestatus) {
        this.correctivestatus = Correctivestatus;
    }

    public String getrownum() {
        return rownum;
    }

    public void setrownum(String rownum) {
        this.rownum = rownum;
    }

    public String getreasontodisp() {

        return reasontodisp;
    }

    public void setreasontodisp(String reasonToDisp) {
        this.reasontodisp = reasonToDisp;
    }

    public int getamount() {
        return amount;
    }

    public void setamount(int amount) {
        this.amount = amount;
    }

    public String getatmapplicationid() {
        return atmapplicationid;
    }

    public void setatmapplicationid(String atmApplicationId) {
        this.atmapplicationid = atmApplicationId;
    }

    public AtmMachineDTOInter getatmid() {
        return atmid;
    }

    public void setatmid(AtmMachineDTOInter atmId) {
        this.atmid = atmId;
    }

    public String getcardno() {
        return cardno;
    }

    public void setcardno(String cardNo) {
        this.cardno = cardNo;
    }

    public int getcardnosuffix() {
        return cardnosuffix;
    }

    public void setcardnosuffix(int cardNoSuffix) {
        this.cardnosuffix = cardNoSuffix;
    }

    public String getcolumn1() {
        return column1;
    }

    public void setcolumn1(String column1) {
        this.column1 = column1;
    }

    public String getcolumn2() {
        return column2;
    }

    public void setcolumn2(String column2) {
        this.column2 = column2;
    }

    public String getcolumn3() {
        return column3;
    }

    public void setcolumn3(String column3) {
        this.column3 = column3;
    }

    public String getcolumn4() {
        return column4;
    }

    public void setcolumn4(String column4) {
        this.column4 = column4;
    }

    public String getcolumn5() {
        return column5;
    }

    public void setcolumn5(String column5) {
        this.column5 = column5;
    }

    public String getcomments() {
        return comments;
    }

    public void setcomments(String comments) {
        this.comments = comments;
    }

    public String getcurrency() {
        return currency;
    }

    public void setcurrency(String currency) {
        this.currency = currency;
    }

    public CurrencyMasterDTOInter getcurrencyid() {
        return currencyid;
    }

    public void setcurrencyid(CurrencyMasterDTOInter currencyId) {
        this.currencyid = currencyId;
    }

    public String getcustomeraccountnumber() {
        return customeraccountnumber;
    }

    public void setcustomeraccountnumber(String customerAccountNumber) {
        this.customeraccountnumber = customerAccountNumber;
    }

    public int getdisputeby() {
        return disputeby;
    }

    public void setdisputeby(int disputeBy) {
        this.disputeby = disputeBy;
    }

    public Date getdisputedate() {
        return disputedate;
    }

    public void setdisputedate(Date disputeDate) {
        this.disputedate = disputeDate;
    }

    public int getdisputekey() {
        return disputekey;
    }

    public void setdisputekey(int disputeKey) {
        this.disputekey = disputeKey;
    }

    public String getdisputereason() {
        return disputereason;
    }

    public void setdisputereason(String disputeReason) {
        this.disputereason = disputeReason;
    }

    public int getdisputewithroles() {
        return disputewithroles;
    }

    public void setdisputewithroles(int disputeWithRoles) {
        this.disputewithroles = disputeWithRoles;
    }

    public int getfileid() {
        return fileid;
    }

    public void setfileid(int fileId) {
        this.fileid = fileId;
    }

    public Date getloadingdate() {
        return loadingdate;
    }

    public void setloadingdate(Date loadingDate) {
        this.loadingdate = loadingDate;
    }

    public int getmatchingtype() {
        return matchingtype;
    }

    public void setmatchingtype(int matchingType) {
        this.matchingtype = matchingType;
    }

    public String getnotespresented() {
        return notespresented;
    }

    public void setnotespresented(String notesPresented) {
        this.notespresented = notesPresented;
    }

    public int getrecordtype() {
        return recordtype;
    }

    public void setrecordtype(int recordType) {
        if (recordType == 1) {
            setIsjournal(Boolean.TRUE);
        } else {
            setIsjournal(Boolean.FALSE);
        }
        this.recordtype = recordType;
    }

    public String getresponsecode() {
        return responsecode;
    }

    public void setresponsecode(String responseCode) {
        this.responsecode = responseCode;
    }

    public TransactionResponseCodeDTOInter getresponsecodeid() {
        return responsecodeid;
    }

    public void setresponsecodeid(TransactionResponseCodeDTOInter responseCodeId) {
        this.responsecodeid = responseCodeId;
    }

    public int getresponsecodemaster() {
        return responsecodemaster;
    }

    public void setresponsecodemaster(int responseCodeMaster) {
        this.responsecodemaster = responseCodeMaster;
    }

    public Date getsettleddate() {
        return settleddate;
    }

    public void setsettleddate(Date settledDate) {
        this.settleddate = settledDate;
    }

    public int getsettledflag() {
        return settledflag;
    }

    public void setsettledflag(int settledFlag) {
        this.settledflag = settledFlag;
    }

    public UsersDTOInter getsettleduser() {
        return settleduser;
    }

    public void setsettleduser(UsersDTOInter settledUser) {
        this.settleduser = settledUser;
    }

    public Date getsettlementdate() {
        return settlementdate;
    }

    public void setsettlementdate(Date settlementDate) {
        this.settlementdate = settlementDate;
    }

    public Date gettransactiondate() {
        return transactiondate;
    }

    public void settransactiondate(Date transactionDate) {
        this.transactiondate = transactionDate;
    }

    public String gettransactionseqeunce() {
        return transactionseqeunce;
    }

    public void settransactionseqeunce(String transactionSeqeunce) {
        this.transactionseqeunce = transactionSeqeunce;
    }

    public int gettransactionsequenceorderby() {
        return transactionsequenceorderby;
    }

    public void settransactionsequenceorderby(int transactionSequenceOrderBy) {
        this.transactionsequenceorderby = transactionSequenceOrderBy;
    }

    public String gettransactionstatus() {
        return transactionstatus;
    }

    public void settransactionstatus(String transactionStatus) {
        this.transactionstatus = transactionStatus;
    }

    public TransactionStatusDTOInter gettransactionstatusid() {
        return transactionstatusid;
    }

    public void settransactionstatusid(TransactionStatusDTOInter transactionStatusId) {
        this.transactionstatusid = transactionStatusId;
    }

    public Date gettransactiontime() {
        return transactiontime;
    }

    public void settransactiontime(Date transactionTime) {
        this.transactiontime = transactionTime;
    }

    public String gettransactiontype() {
        return transactiontype;
    }

    public void settransactiontype(String transactionType) {
        this.transactiontype = transactionType;
    }

    public TransactionTypeDTOInter gettransactiontypeid() {
        return transactiontypeid;
    }

    public void settransactiontypeid(TransactionTypeDTOInter transactionTypeId) {
        this.transactiontypeid = transactionTypeId;
    }

    public int gettransactiontypemaster() {
        return transactiontypemaster;
    }

    public void settransactiontypemaster(int transactionTypeMaster) {
        this.transactiontypemaster = transactionTypeMaster;
    }

    protected DisputesDTO() {
        super();
    }
}
