package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.UserProfileDTOInter;
import com.ev.AtmBingo.bus.dto.useratmbranchDTOInter;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class useratmbranchDAO extends BaseDAO implements useratmbranchDAOInter {

    protected useratmbranchDAO() {
        super();
        super.setTableName("USER_ATM_BRANCH");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        useratmbranchDTOInter RecordToInsert = (useratmbranchDTOInter) obj[0];
//RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (USER_ID,ATM_ID) "
                + " values "
                + " ($userid,$atmid)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$userid", "" + RecordToInsert.getuserid());
        insertStat = insertStat.replace("$atmid", "" + RecordToInsert.getatmid());
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To USER_ATM_BRANCH", false);
        return null;
    }

    public Boolean ValidateNull(Object... obj) {
        useratmbranchDTOInter RecordToInsert = (useratmbranchDTOInter) obj[0];
        if (RecordToInsert.getuserid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getatmid() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        useratmbranchDTOInter RecordToUpdate = (useratmbranchDTOInter) obj[0];
        String UpdateStat = "update $table set "
                + " (USER_ID= $userid,ATM_ID= $atmid) "
                + "  where  USER_ID= $userid,ATM_ID= $atmid";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$userid", "" + RecordToUpdate.getuserid());
        UpdateStat = UpdateStat.replace("$atmid", "" + RecordToUpdate.getatmid());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table USER_ATM_BRANCH", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        useratmbranchDTOInter RecordToDelete = (useratmbranchDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  USER_ID= $userid,ATM_ID= $atmid";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$userid", "" + RecordToDelete.getuserid());
        deleteStat = deleteStat.replace("$atmid", "" + RecordToDelete.getatmid());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table USER_ATM_BRANCH", false);
        return null;
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table USER_ATM_BRANCH", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        useratmbranchDTOInter RecordToSelect = (useratmbranchDTOInter) obj[0];
        String selectStat = "Select USER_ID,ATM_ID From $table"
                + "  where  USER_ID= $userid,ATM_ID= $atmid";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$userid", "" + RecordToSelect.getuserid());
        selectStat = selectStat.replace("$atmid", "" + RecordToSelect.getatmid());
        ResultSet rs = executeQuery(selectStat);
        useratmbranchDTOInter SelectedRecord = DTOFactory.createuseratmbranchDTO();
        while (rs.next()) {
            SelectedRecord.setuserid(rs.getInt("USER_ID"));
            SelectedRecord.setatmid(rs.getInt("ATM_ID"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        useratmbranchDTOInter RecordToSelect = (useratmbranchDTOInter) obj[0];
        String selectStat = "Select USER_ID,ATM_ID From $table"
                + "  where  USER_ID= $userid,ATM_ID= $atmid";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$userid", "" + RecordToSelect.getuserid());
        selectStat = selectStat.replace("$atmid", "" + RecordToSelect.getatmid());
        ResultSet rs = executeQuery(selectStat);
        List<useratmbranchDTOInter> Records = new ArrayList<useratmbranchDTOInter>();
        while (rs.next()) {
            useratmbranchDTOInter SelectedRecord = DTOFactory.createuseratmbranchDTO();
            SelectedRecord.setuserid(rs.getInt("USER_ID"));
            SelectedRecord.setatmid(rs.getInt("ATM_ID"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select USER_ID,ATM_ID From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<useratmbranchDTOInter> Records = new ArrayList<useratmbranchDTOInter>();
        while (rs.next()) {
            useratmbranchDTOInter SelectedRecord = DTOFactory.createuseratmbranchDTO();
            SelectedRecord.setuserid(rs.getInt("USER_ID"));
            SelectedRecord.setatmid(rs.getInt("ATM_ID"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findassign(int userId,String targetlikevalue) throws Throwable {
        super.preSelect();
        String selectStat = "select m.id,m.application_id from atm_machine m \n"
                + "where encrypt(m.id) = m.atm_inc \n"
                + "and m.id in (select b.atmid from branch_atm b where b.userid = $userid) and m.application_id like '%$likevalue%'";
        selectStat = selectStat.replace("$userid", "" + userId);
        selectStat = selectStat.replace("$likevalue", "" + targetlikevalue);
        ResultSet rs = executeQuery(selectStat);
        List<AtmMachineDTOInter> Records = new ArrayList<AtmMachineDTOInter>();
        while (rs.next()) {
            AtmMachineDTOInter SelectedRecord = DTOFactory.createAtmMachineDTO();
            SelectedRecord.setId(rs.getInt(1));
            SelectedRecord.setApplicationId(rs.getString(2));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findnotassign(int userId,String likevalue) throws Throwable {
        super.preSelect();
        String selectStat = "select m.id,m.application_id from atm_machine m \n"
                + "where encrypt(m.id) = m.atm_inc \n"
                + "and m.id not in (select b.atmid from branch_atm b where b.userid = $userid) and m.application_id like '%$likevalue%'";
        selectStat = selectStat.replace("$userid", "" + userId);
        selectStat = selectStat.replace("$likevalue", "" + likevalue);
        ResultSet rs = executeQuery(selectStat);
        List<AtmMachineDTOInter> Records = new ArrayList<AtmMachineDTOInter>();
        while (rs.next()) {
            AtmMachineDTOInter SelectedRecord = DTOFactory.createAtmMachineDTO();
            SelectedRecord.setId(rs.getInt(1));
            SelectedRecord.setApplicationId(rs.getString(2));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordGroupAll(String app) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();        
        String selectStat = "select id from atm_machine where "
                + " atm_valid(id) = 1 and application_id = '$application_id'";
        selectStat = selectStat.replace("$application_id", "" + app);
        ResultSet result = executeQuery(selectStat);
        AtmMachineDTOInter mt = DTOFactory.createAtmMachineDTO();
        while (result.next()) {
            mt.setId(result.getInt("id"));
        }
        super.postSelect(result);
        return mt;
    }

    public Object deletealluserrecord(int user) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from branch_atm where userid = '$userid'";
        deleteStat = deleteStat.replace("$userid", "" + user);
        super.executeUpdate(deleteStat);
        return null;
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<UserProfileDTOInter> entities = (List<UserProfileDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Files Found";
        } else {
            String validate = "Validate";
            if (validate.equals("Validate")) {
                String insertStat = "insert into branch_atm\n"
                        + " (userid, atmid)\n"
                        + " values\n"
                        + " (?, ?)";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
     connection  = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        UserProfileDTOInter RecordtoInsert = entities.get(i);
                        statement.setInt(1, RecordtoInsert.getUserId());
                        statement.setInt(2, RecordtoInsert.getProfileId());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " Record has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                           CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
