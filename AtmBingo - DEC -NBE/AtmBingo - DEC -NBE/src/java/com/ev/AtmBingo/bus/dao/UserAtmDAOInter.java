/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;


import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface UserAtmDAOInter {

    Object deleteByUserId(Object... obj) throws Throwable;
 boolean  DeleteAtms(String[] entities,Integer UserID) throws SQLException ;
    Object delete(Object... obj) throws Throwable;

    Object findByAtmId(Object... obj) throws Throwable;

    Object findByUserId(Object... obj) throws Throwable;

    Object insert(Object... obj) throws Throwable;
    boolean insertAtms(String[] Entity,Integer UserID) throws SQLException;

    void DeleteByUserATMID(String User,Integer ATM) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
