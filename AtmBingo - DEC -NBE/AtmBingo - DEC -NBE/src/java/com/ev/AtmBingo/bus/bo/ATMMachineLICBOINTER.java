/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.ATMMAchineLICInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface  ATMMachineLICBOINTER extends BaseBOInter{
     public Object getAtmMachine()throws Throwable;
     public Object FilterAtmMachine(List<ATMMAchineLICInter> AtmList,String Like)throws Throwable;
     public Object LicMangDetail()throws Throwable;
public Object GetLIC()throws Throwable;
public void SAVE(List<ATMMAchineLICInter> List)throws Throwable;
   public void Delete(List<ATMMAchineLICInter> List)throws Throwable;
}
