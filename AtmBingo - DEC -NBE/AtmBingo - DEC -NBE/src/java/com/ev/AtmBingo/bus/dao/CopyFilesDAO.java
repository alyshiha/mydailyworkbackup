/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.CopyFilesDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class CopyFilesDAO extends BaseDAO implements CopyFilesDAOInter {

    protected CopyFilesDAO() {
        super();
        super.setTableName("copy_files");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        CopyFilesDTOInter uDTO = (CopyFilesDTOInter) obj[0];
        String insertStat = "insert into $table values ('$source_folder' , '$folder1', '$folder2')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$source_folder", "" + uDTO.getSourceFolder());
        insertStat = insertStat.replace("$folder1", "" + uDTO.getFolder1());
        insertStat = insertStat.replace("$folder2", "" + uDTO.getFolder2());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        CopyFilesDTOInter uDTO = (CopyFilesDTOInter) obj[0];
        String oldSourceFolder = (String) obj[1];
        String updateStat = "update $table set  source_folder = '$source_folder'"
                + ", folder1 = '$folder1', folder2 = '$folder2'"
                + " where source_folder = '$oldSourceFolder'";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$source_folder", "" + uDTO.getSourceFolder());
        updateStat = updateStat.replace("$folder1", "" + uDTO.getFolder1());
        updateStat = updateStat.replace("$folder2", "" + uDTO.getFolder2());
        updateStat = updateStat.replace("$oldSourceFolder", "" + oldSourceFolder);

        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        CopyFilesDTOInter uDTO = (CopyFilesDTOInter) obj[0];
        String deleteStat = "delete from $table where source_folder = '$source_folder'";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$source_folder", "" + uDTO.getSourceFolder());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        return null;
//        super.preSelect();
//        // databse insert statement usinf conn pool getConn().createStatement();
//        Integer userId = (Integer)obj[0];
//        String selectStat = "select * from $table where user_id = $user_id"
        //        selectStat = selectStat.replace("$table", "" + super.getTableName());
//        selectStat = selectStat.replace("$user_id", "" + userId);
//        ResultSet rs = super.executeQuery(selectStat);
//        BlockUsersDTOInter uDTO = DTOFactory.createBlockUsersDTO();
//        while (rs.next()) {
//            uDTO.setBlockTime(rs.getDate("block_time"));
//            uDTO.setReasonId(rs.getInt("reason_id"));
//            uDTO.setUserId(rs.getInt("user_id"));
//
//        }
//        super.postSelect();
//        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by source_folder";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);

        List<CopyFilesDTOInter> uDTOL = new ArrayList<CopyFilesDTOInter>();
        while (rs.next()) {
            CopyFilesDTOInter uDTO = DTOFactory.createCopyFilesDTO();
            uDTO.setFolder1(rs.getString("folder1"));
            uDTO.setFolder2(rs.getString("folder2"));
            uDTO.setSourceFolder(rs.getString("source_folder"));
            uDTOL.add(uDTO);

        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public static void main(String[] args) throws Throwable {
    }
}
