/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface atmremainingDTOInter  extends Serializable {

    String getAtmapplicationid();

    String getGroup();

    String getName();

    Integer getRemaining();

    void setAtmapplicationid(String atmapplicationid);

    void setGroup(String group);

    void setName(String name);

    void setRemaining(Integer remaining);
    
}
