/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface RepTransDetailDTOInter {

    String getCorrflag();

    void setCorrflag(String corrflag);

    Integer getDisputekey();

    void setDisputekey(Integer disputekey);

    Integer getRecordtypevalue();

    void setRecordtypevalue(Integer recordtypevalue);

    Integer getMatchingtype();

    void setMatchingtype(Integer matchingtype);

    Integer getBlacklist();

    void setBlacklist(Integer blacklist);

    void setRowidnumber(Integer rowidnumber);

    Integer getRowidnumber();

    Integer getAmount();

    String getAmt();

    String getAtmapplicationid();

    String getCardno();

    String getComments();

    String getCurrency();

    String getCustomeraccountnumber();

    String getIss();

    String getNotespresented();

    String getRecordtype();

    String getRef();

    String getResponsecode();

    Date getSettleddate();

    String getSettledflag();

    Date getTransactiondate();

    String getTransactionsequence();

    String getTransactionstatus();

    String getTransactiontype();

    void setAmount(Integer amount);

    void setAmt(String amt);

    void setAtmapplicationid(String atmapplicationid);

    void setCardno(String cardno);

    void setComments(String comments);

    void setCurrency(String currency);

    void setCustomeraccountnumber(String customeraccountnumber);

    void setIss(String iss);

    void setNotespresented(String notespresented);

    void setRecordtype(String recordtype);

    void setRef(String ref);

    void setResponsecode(String responsecode);

    void setSettleddate(Date settleddate);

    void setSettledflag(String settledflag);

    void setTransactiondate(Date transactiondate);

    void setTransactionsequence(String transactionsequence);

    void setTransactionstatus(String transactionstatus);

    void setTransactiontype(String transactiontype);

}
