/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import com.ev.AtmBingo.bus.dao.AtmtemplateDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.FileColumnDefinitionDAOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class FileColumnDisplayDTO extends BaseDTO implements Serializable, FileColumnDisplayDTOInter{

    private int id;
    private FileColumnDefinitionDTOInter columnId;
    private List<FileColumnDisplayDetailsDTOInter> fileColumnDisplayDetailsDTOL;

    public List<FileColumnDisplayDetailsDTOInter> getFileColumnDisplayDetailsDTOL()throws Throwable {
        return fileColumnDisplayDetailsDTOL;
    }

    public void setFileColumnDisplayDetailsDTOL(List<FileColumnDisplayDetailsDTOInter> fileColumnDisplayDetailsDTOL) {
        this.fileColumnDisplayDetailsDTOL = fileColumnDisplayDetailsDTOL;
    }

    public FileColumnDefinitionDTOInter getColumnId() {
        try {
        
        AtmtemplateDAOInter fcdDAO = DAOFactory.createAtmtemplateDAO();
        List<FileColumnDefinitionDTOInter> Result = new ArrayList<FileColumnDefinitionDTOInter>();
        Result=(List<FileColumnDefinitionDTOInter>)fcdDAO.findAllColDef();
        for(FileColumnDefinitionDTOInter Res :Result)
        {
            if(Res.getId()==columnId.getId())
            {
                return Res;
            }
        }
        } catch (Throwable ex) {
            Logger.getLogger(FileColumnDisplayDTO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return columnId;
    }

    public void setColumnId(FileColumnDefinitionDTOInter columnId) {
        
        this.columnId = columnId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
   
    protected FileColumnDisplayDTO() {
        super();
        columnId = DTOFactory.createFileColumnDefinitionDTO();
    }


}
