/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface SystemTableDTOInter  extends Serializable {

    int getParameterDataType();

    String getParameterName();

    String getParameterValue();

    void setParameterDataType(int parameterDataType);

    void setParameterName(String parameterName);

    void setParameterValue(String parameterValue);

}
