package com.ev.AtmBingo.bus.dto;
import java.io.Serializable;
import java.util.Date;
public interface usersbranchDTOInter extends Serializable {
int getuserid();
void setuserid(int userid);
String getusername();
void setusername(String username);
String getlogonname();
void setlogonname(String logonname);
String getuserpassword();
void setuserpassword(String userpassword);
int getbranchid();
void setbranchid(int branchid);
String getbranchname();
void setbranchname(String branchname);
int getstatus();
void setstatus(int status);
}