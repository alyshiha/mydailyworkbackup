/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface RepAddDTOInter {

    int getCreatedbyid();

    void setCreatedbyid(int createdbyid);

    int getAtmId();

    String getAtmapplicationid();

    String getCreatedby();

    Date getCreateddate();

    Date getDateFrom();

    Date getDateTo();

    int getId();

    int getRepid();

    void setAtmId(int atmId);

    void setAtmapplicationid(String atmapplicationid);

    void setCreatedby(String createdby);

    void setCreateddate(Date createddate);

    void setDateFrom(Date dateFrom);

    void setDateTo(Date dateTo);

    void setId(int id);

    void setRepid(int repid);

}
