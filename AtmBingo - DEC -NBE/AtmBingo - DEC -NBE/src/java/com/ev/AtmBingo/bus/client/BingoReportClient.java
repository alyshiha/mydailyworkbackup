/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.AutoComplete;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.bo.AtmGroupEditorBoInter;
import com.ev.AtmBingo.bus.bo.AtmRemainingRepBOInter;
import com.ev.AtmBingo.bus.bo.AtmStatByCashRepBOInter;
import com.ev.AtmBingo.bus.bo.AtmStatByTransRepBOInter;
import com.ev.AtmBingo.bus.bo.AtmsTotalRepBOInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.BanksTotalRepBOInter;
import com.ev.AtmBingo.bus.bo.CardNoRepBOInter;
import com.ev.AtmBingo.bus.bo.DisputeRepBOInter;
import com.ev.AtmBingo.bus.bo.RepStatBORepInter;
import com.ev.AtmBingo.bus.bo.MissingJournalsRepBOInter;
import com.ev.AtmBingo.bus.bo.ReplainshmentLogBOInter;
import com.ev.AtmBingo.bus.bo.ReplanishmentHistoryRepBOInter;
import com.ev.AtmBingo.bus.bo.ReplanishmentReportBOInter;
import com.ev.AtmBingo.bus.bo.RevenueRepBOInter;
import com.ev.AtmBingo.bus.bo.SwGlDifferenceRepBOInter;
import com.ev.AtmBingo.bus.dao.AVGDailyDispenseDTOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DisputeRepDTOInter;
import com.ev.AtmBingo.bus.dto.MissingJournalDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkGroupDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkMasterDTOInter;
import com.ev.AtmBingo.bus.dto.PrintcardtakenExcelInter;
import com.ev.AtmBingo.bus.dto.ReplainshmentLogDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import com.ev.AtmBingo.bus.dto.TotalWithDrawalsDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.atmremainingDTOInter;
import com.ev.AtmBingo.bus.dto.bycardDTOInter;
import com.ev.AtmBingo.bus.dto.cardissuerDTOInter;
import com.ev.AtmBingo.bus.dto.replanishmentHistoryRepDTOInter;
import com.ev.AtmBingo.bus.dto.statbytransDTOInter;
import com.ev.AtmBingo.bus.dto.swglDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author ISLAM
 */
public class BingoReportClient extends BaseBean implements Serializable {

    private AtmsTotalRepBOInter atmTotalRep;
    private ReplanishmentReportBOInter repDetailsReport;
    private Session utillist = new Session();
    private ReplanishmentHistoryRepBOInter repHistoryRep;
    private Boolean printCheck, deselectFlag, selectFlag;
    private SwGlDifferenceRepBOInter repSWGL;
    private BanksTotalRepBOInter banksTotalRep;
    private DisputeRepBOInter DispRepBO;
    private ReplainshmentLogBOInter RepLogBO;
    private CardNoRepBOInter cardNoRep;
    private AtmStatByTransRepBOInter atmByTransRep;
    private AtmStatByCashRepBOInter atmByCashRep;
    private List<AtmGroupDTOInter> groupList;
    private AtmGroupEditorBoInter myObject;
    private Integer group2, procstatus;
    private MissingJournalsRepBOInter missingJourRep;
    private RepStatBORepInter LoadamoutRep;
    private RevenueRepBOInter revenueBO;
    private AtmRemainingRepBOInter atmRemainRep;
    private String groupNam, userName, message;
    private String rephistoryatmid = null, atmids = null;
    private Date fromDate, toDate, toCreate, dailySheetfrom, dailySheetto, fromCreate, toDate_ico, toDate_ico1, fromDate_ico, fromDate_ico1;
    private Integer iconCheck, listSize, atmInt, atmno, drawInt, sideInt, sortBy, userInt, atmID, exepInt, netInt, typeInt;
    private AtmGroupDTOInter selectedGrp;
    private UsersDTOInter selectedUser;
    private List<AtmGroupDTOInter> atmGroupList;
    private List<NetworkGroupDTOInter> netList;
    private List<NetworkMasterDTOInter> exeptList;
    private List<UsersDTOInter> repDetailsUsers;
    private List<AtmMachineDTOInter> atmIDList;
    private List<ReplanishmentMasterDTOInter> repList;
    private ReplanishmentMasterDTOInter[] selectedRep;
    private AutoComplete AC;
    private Boolean exceptBool;
    private String temp = "";
    private Boolean datetoFlag, datefromFlag, subdatetoFlag, subdatefromFlag, fromOverlay, toOverlay;

    public Boolean getDatefromFlag() {
        return datefromFlag;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public void setDatefromFlag(Boolean datefromFlag) {
        this.datefromFlag = datefromFlag;
    }

    public Boolean getDatetoFlag() {
        return datetoFlag;
    }

    public void setDatetoFlag(Boolean datetoFlag) {
        this.datetoFlag = datetoFlag;
    }

    public Boolean getFromOverlay() {
        return fromOverlay;
    }

    public void setFromOverlay(Boolean fromOverlay) {
        this.fromOverlay = fromOverlay;
    }

    public ReplainshmentLogBOInter getRepLogBO() {
        return RepLogBO;
    }

    public void setRepLogBO(ReplainshmentLogBOInter RepLogBO) {
        this.RepLogBO = RepLogBO;
    }

    public Boolean getSubdatefromFlag() {
        return subdatefromFlag;
    }

    public void setSubdatefromFlag(Boolean subdatefromFlag) {
        this.subdatefromFlag = subdatefromFlag;
    }

    public Boolean getSubdatetoFlag() {
        return subdatetoFlag;
    }

    public void setSubdatetoFlag(Boolean subdatetoFlag) {
        this.subdatetoFlag = subdatetoFlag;
    }

    public Boolean getToOverlay() {
        return toOverlay;
    }

    public void setToOverlay(Boolean toOverlay) {
        this.toOverlay = toOverlay;
    }

    public Integer getProcstatus() {
        return procstatus;
    }

    public void setProcstatus(Integer procstatus) {
        this.procstatus = procstatus;
    }

    public String getRephistoryatmid() {
        return rephistoryatmid;
    }

    public String getAtmids() {
        return atmids;
    }

    public void setAtmids(String atmids) {
        this.atmids = atmids;
    }

    public void setRephistoryatmid(String rephistoryatmid) {
        this.rephistoryatmid = rephistoryatmid;
    }

    public Boolean getDeselectFlag() {
        return deselectFlag;
    }

    public Integer getAtmno() {
        return atmno;
    }

    public void setAtmno(Integer atmno) {
        this.atmno = atmno;
    }

    public void setDeselectFlag(Boolean deselectFlag) {
        this.deselectFlag = deselectFlag;
    }

    public Boolean getPrintCheck() {
        return printCheck;
    }

    public void setPrintCheck(Boolean printCheck) {
        this.printCheck = printCheck;
    }

    public Boolean getSelectFlag() {
        return selectFlag;
    }

    public void setSelectFlag(Boolean selectFlag) {
        this.selectFlag = selectFlag;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public Integer getTypeInt() {
        return typeInt;
    }

    public void setTypeInt(Integer typeInt) {
        this.typeInt = typeInt;
    }

    public List<NetworkMasterDTOInter> getExeptList() {
        return exeptList;
    }

    public void setExeptList(List<NetworkMasterDTOInter> exeptList) {
        this.exeptList = exeptList;
    }

    public List<NetworkGroupDTOInter> getNetList() {
        return netList;
    }

    public void setNetList(List<NetworkGroupDTOInter> netList) {
        this.netList = netList;
    }

    public Boolean getExceptBool() {
        return exceptBool;
    }

    public void setExceptBool(Boolean exceptBool) {
        this.exceptBool = exceptBool;
    }

    public Integer getExepInt() {
        return exepInt;
    }

    public void setExepInt(Integer exepInt) {
        this.exepInt = exepInt;
    }

    public Integer getNetInt() {
        return netInt;
    }

    public void setNetInt(Integer netInt) {
        this.netInt = netInt;
    }

    public Integer getAtmID() {
        return atmID;
    }

    public void setAtmID(Integer atmID) {
        this.atmID = atmID;
    }

    public ReplanishmentMasterDTOInter[] getSelectedRep() {
        return selectedRep;
    }

    public void setSelectedRep(ReplanishmentMasterDTOInter[] selectedRep) {
        this.selectedRep = selectedRep;
    }

    public List<AtmMachineDTOInter> getAtmIDList() {
        return atmIDList;
    }

    public void setAtmIDList(List<AtmMachineDTOInter> atmIDList) {
        this.atmIDList = atmIDList;
    }

    public List<ReplanishmentMasterDTOInter> getRepList() {
        return repList;
    }

    public void setRepList(List<ReplanishmentMasterDTOInter> repList) {
        this.repList = repList;
    }

    public Integer getUserInt() {
        return userInt;
    }

    public void setUserInt(Integer userInt) {
        this.userInt = userInt;
    }

    public Date getFromCreate() {
        return fromCreate;
    }

    public void setFromCreate(Date fromCreate) {
        this.fromCreate = fromCreate;
    }

    public Date getToCreate() {
        return toCreate;
    }

    public Date getDailySheetfrom() {
        return dailySheetfrom;
    }

    public void setDailySheetfrom(Date dailySheetfrom) {
        this.dailySheetfrom = dailySheetfrom;
    }

    public Date getDailySheetto() {
        return dailySheetto;
    }

    public void setDailySheetto(Date dailySheetto) {
        this.dailySheetto = dailySheetto;
    }

    public void setToCreate(Date toCreate) {
        this.toCreate = toCreate;
    }

    public List<UsersDTOInter> getRepDetailsUsers() {
        return repDetailsUsers;
    }

    public void setRepDetailsUsers(List<UsersDTOInter> repDetailsUsers) {
        this.repDetailsUsers = repDetailsUsers;
    }

    public UsersDTOInter getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(UsersDTOInter selectedUser) {
        this.selectedUser = selectedUser;
    }

    public AtmsTotalRepBOInter getAtmTotalRep() {
        return atmTotalRep;
    }

    public void setAtmTotalRep(AtmsTotalRepBOInter atmTotalRep) {
        this.atmTotalRep = atmTotalRep;
    }

    public ReplanishmentReportBOInter getRepDetailsReport() {
        return repDetailsReport;
    }

    public void setRepDetailsReport(ReplanishmentReportBOInter repDetailsReport) {
        this.repDetailsReport = repDetailsReport;
    }

    public Session getUtillist() {
        return utillist;
    }

    public void setUtillist(Session utillist) {
        this.utillist = utillist;
    }

    public ReplanishmentHistoryRepBOInter getRepHistoryRep() {
        return repHistoryRep;
    }

    public void setRepHistoryRep(ReplanishmentHistoryRepBOInter repHistoryRep) {
        this.repHistoryRep = repHistoryRep;
    }

    public SwGlDifferenceRepBOInter getRepSWGL() {
        return repSWGL;
    }

    public void setRepSWGL(SwGlDifferenceRepBOInter repSWGL) {
        this.repSWGL = repSWGL;
    }

    public BanksTotalRepBOInter getBanksTotalRep() {
        return banksTotalRep;
    }

    public void setBanksTotalRep(BanksTotalRepBOInter banksTotalRep) {
        this.banksTotalRep = banksTotalRep;
    }

    public DisputeRepBOInter getDispRepBO() {
        return DispRepBO;
    }

    public void setDispRepBO(DisputeRepBOInter DispRepBO) {
        this.DispRepBO = DispRepBO;
    }

    public CardNoRepBOInter getCardNoRep() {
        return cardNoRep;
    }

    public void setCardNoRep(CardNoRepBOInter cardNoRep) {
        this.cardNoRep = cardNoRep;
    }

    public AtmStatByTransRepBOInter getAtmByTransRep() {
        return atmByTransRep;
    }

    public void setAtmByTransRep(AtmStatByTransRepBOInter atmByTransRep) {
        this.atmByTransRep = atmByTransRep;
    }

    public AtmStatByCashRepBOInter getAtmByCashRep() {
        return atmByCashRep;
    }

    public void setAtmByCashRep(AtmStatByCashRepBOInter atmByCashRep) {
        this.atmByCashRep = atmByCashRep;
    }

    public List<ReplainshmentLogDTOInter> getPrintRepLog() {
        return printRepLog;
    }

    public void setPrintRepLog(List<ReplainshmentLogDTOInter> printRepLog) {
        this.printRepLog = printRepLog;
    }

    public AtmGroupEditorBoInter getMyObject() {
        return myObject;
    }

    public void setMyObject(AtmGroupEditorBoInter myObject) {
        this.myObject = myObject;
    }

    public MissingJournalsRepBOInter getMissingJourRep() {
        return missingJourRep;
    }

    public void setMissingJourRep(MissingJournalsRepBOInter missingJourRep) {
        this.missingJourRep = missingJourRep;
    }

    public RepStatBORepInter getLoadamoutRep() {
        return LoadamoutRep;
    }

    public void setLoadamoutRep(RepStatBORepInter LoadamoutRep) {
        this.LoadamoutRep = LoadamoutRep;
    }

    public RevenueRepBOInter getRevenueBO() {
        return revenueBO;
    }

    public void setRevenueBO(RevenueRepBOInter revenueBO) {
        this.revenueBO = revenueBO;
    }

    public AtmRemainingRepBOInter getAtmRemainRep() {
        return atmRemainRep;
    }

    public void setAtmRemainRep(AtmRemainingRepBOInter atmRemainRep) {
        this.atmRemainRep = atmRemainRep;
    }

    public Integer getIconCheck() {
        return iconCheck;
    }

    public void setIconCheck(Integer iconCheck) {
        this.iconCheck = iconCheck;
    }

    public AutoComplete getAC() {
        return AC;
    }

    public void setAC(AutoComplete AC) {
        this.AC = AC;
    }

    public Session getSessionutil() {
        return sessionutil;
    }

    public void setSessionutil(Session sessionutil) {
        this.sessionutil = sessionutil;
    }

    public List<DisputeRepDTOInter> getPrintDisputeRep() {
        return printDisputeRep;
    }

    public void setPrintDisputeRep(List<DisputeRepDTOInter> printDisputeRep) {
        this.printDisputeRep = printDisputeRep;
    }

    public Integer getSortBy() {
        return sortBy;
    }

    public void setSortBy(Integer sortBy) {
        this.sortBy = sortBy;
    }

    public Integer getSideInt() {
        return sideInt;
    }

    public void setSideInt(Integer sideInt) {
        this.sideInt = sideInt;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AtmGroupDTOInter getSelectedGrp() {
        return selectedGrp;
    }

    public void setSelectedGrp(AtmGroupDTOInter selectedGrp) {
        this.selectedGrp = selectedGrp;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getDrawInt() {
        return drawInt;
    }

    public void setDrawInt(Integer drawInt) {
        this.drawInt = drawInt;
    }

    public List<AtmGroupDTOInter> getAtmGroupList() {
        return atmGroupList;
    }

    public void setAtmGroupList(List<AtmGroupDTOInter> atmGroupList) {
        this.atmGroupList = atmGroupList;
    }

    public Integer getAtmInt() {
        return atmInt;
    }

    public void setAtmInt(Integer atmInt) {
        this.atmInt = atmInt;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public List<AtmGroupDTOInter> getGroupList() {
        return groupList;
    }

    public Integer getGroup2() {
        return group2;
    }

    public void setGroup2(Integer group2) {
        this.group2 = group2;
    }

    public void setGroupList(List<AtmGroupDTOInter> groupList) {
        this.groupList = groupList;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getGroupNam() {
        return groupNam;
    }

    public void setGroupNam(String groupNam) {
        this.groupNam = groupNam;
    }

    public Date getFromDate_ico() {
        return fromDate_ico;
    }

    public void setFromDate_ico(Date fromDate_ico) {
        this.fromDate_ico = fromDate_ico;
    }

    public Date getFromDate_ico1() {
        return fromDate_ico1;
    }

    public void setFromDate_ico1(Date fromDate_ico1) {
        this.fromDate_ico1 = fromDate_ico1;
    }

    public Date getToDate_ico() {
        return toDate_ico;
    }

    public void setToDate_ico(Date toDate_ico) {
        this.toDate_ico = toDate_ico;
    }

    public Date getToDate_ico1() {
        return toDate_ico1;
    }

    public void setToDate_ico1(Date toDate_ico1) {
        this.toDate_ico1 = toDate_ico1;
    }
    private Session sessionutil;

    /**
     * Creates a new instance of BingoReportClient
     */
    public BingoReportClient() throws Throwable {
        super();
        super.GetAccess();
        printCheck = true;
        selectFlag = true;
        deselectFlag = false;
        repSWGL = BOFactory.createSwGLInter(null);
        revenueBO = BOFactory.createRevenueBOInter(null);

        atmByCashRep = BOFactory.createAtmStatByCashRepBO(null);
        atmByTransRep = BOFactory.createAtmStatByTransRepBO(null);
        atmRemainRep = BOFactory.createAtmRemainingRepBO(null);
        atmTotalRep = BOFactory.createAtmsTotalRepBO(null);
        banksTotalRep = BOFactory.createBanksTotalRepBO(null);
        DispRepBO = BOFactory.createDisputeRepBO(null);
        RepLogBO = BOFactory.createReplainshmentLogBO(null);
        missingJourRep = BOFactory.createMissingJournalsRepBO(null);
        LoadamoutRep = BOFactory.createLoadamoutRepInter(null);
        repDetailsReport = BOFactory.createReplanishmentReportBO(null);
        repHistoryRep = BOFactory.createReplanishmentHistoryRepBO(null);
        cardNoRep = BOFactory.createCardNoRepBO(null);
        // exeptList = (List<NetworkMasterDTOInter>) revenueBO.findExceptions(netInt);
        netList = (List<NetworkGroupDTOInter>) utillist.GetNetworkGroupList();
        sessionutil = new Session();
        exceptBool = true;
        drawInt = 1;
        groupNam = "All";
        sideInt = 1;
        typeInt = 1;

        exepInt = 0;
        atmGroupList = (List<AtmGroupDTOInter>) utillist.GetATMGroupList();
        repDetailsUsers = (List<UsersDTOInter>) utillist.GetUsersList();
        listSize = 0;
        FacesContext context = FacesContext.getCurrentInstance();

        Session sessionutil = new Session();
        UsersDTOInter currentUser = sessionutil.GetUserLogging();
        userName = currentUser.getUserName();
        atmIDList = (List<AtmMachineDTOInter>) utillist.GetAtmMachineList(currentUser);
        myObject = BOFactory.AtmGroupEditorBoInter(null);
        AC = new AutoComplete();
        groupList = (List<AtmGroupDTOInter>) utillist.GetATMGroupList();

        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public List<String> complete(String query) {
        return AC.complete(query);
    }

    public void changeValue(ValueChangeEvent e) {
        try {
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    List<statbytransDTOInter> printCashresult;

    public List<statbytransDTOInter> getPrintCashresult() {
        return printCashresult;
    }

    public void setPrintCashresult(List<statbytransDTOInter> printCashresult) {
        this.printCashresult = printCashresult;
    }

    public void printCashexcel() {
        try {
            printCashresult = (List<statbytransDTOInter>) atmByCashRep.runReportExcel(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmInt, userName, Integer.valueOf(temp), drawInt, sessionutil.Getclient());
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void printCash() {
        try {
            String path = atmByCashRep.runReport(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmInt, userName, Integer.valueOf(temp), drawInt, sessionutil.Getclient());
            prepDownload(path);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    private List<atmremainingDTOInter> printAtmRemresult;

    public List<atmremainingDTOInter> getPrintAtmRemresult() {
        return printAtmRemresult;
    }

    public void setPrintAtmRemresult(List<atmremainingDTOInter> printAtmRemresult) {
        this.printAtmRemresult = printAtmRemresult;
    }

    public void printAtmRemexcel() {
        try {
            if (fromDate == null || toDate == null) {
                message = super.showMessage(REQ_FIELD);
            } else {
                message = "";
                printAtmRemresult = (List<atmremainingDTOInter>) atmRemainRep.runReportExcel(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmInt, userName, sessionutil.Getclient());
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void printAtmRem() {
        try {
            if (fromDate == null || toDate == null) {
                message = super.showMessage(REQ_FIELD);
            } else {
                message = "";
                String path = atmRemainRep.runReport(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmInt, userName, sessionutil.Getclient());
                //HttpServletResponse response =(HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                FacesContext context = FacesContext.getCurrentInstance();
                HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                response.sendRedirect(path);
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    private List<MissingJournalDTOInter> printjournalresult;

    public List<MissingJournalDTOInter> getPrintjournalresult() {
        return printjournalresult;
    }

    public void setPrintjournalresult(List<MissingJournalDTOInter> printjournalresult) {
        this.printjournalresult = printjournalresult;
    }

    public void printJournalsexcel() {
        try {
            printjournalresult = (List<MissingJournalDTOInter>) missingJourRep.runReportExcel(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate));
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void printJournals() {
        try {
            String path = missingJourRep.runReport(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), userName, sessionutil.Getclient());
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect(path);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    private List<AVGDailyDispenseDTOInter> Loadamoutresult;

    public List<AVGDailyDispenseDTOInter> getLoadamoutresult() {
        return Loadamoutresult;
    }

    public void setLoadamoutresult(List<AVGDailyDispenseDTOInter> Loadamoutresult) {
        this.Loadamoutresult = Loadamoutresult;
    }

    public void PrintLoadamoutexcel() {
        try {
            String ATMIDTemp = "";
            if (atmno != null) {
                ATMIDTemp = atmno.toString();
            }
            String ATMGroupTemp = "";
            if (atmInt.toString() == null ? "0" != null : !atmInt.toString().equals("0")) {
                ATMGroupTemp = atmInt.toString();
            }
            Loadamoutresult = (List<AVGDailyDispenseDTOInter>) LoadamoutRep.runReportATMAmountexcel(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), userName, sessionutil.Getclient(), ATMIDTemp, ATMGroupTemp);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void PrintLoadamout() {
        try {
            String ATMIDTemp = "";
            if (atmno != null) {
                ATMIDTemp = atmno.toString();
            }
            String ATMGroupTemp = "";
            if (atmInt.toString() == null ? "0" != null : !atmInt.toString().equals("0")) {
                ATMGroupTemp = atmInt.toString();
            }
            String path = LoadamoutRep.runReportATMAmount(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), userName, sessionutil.Getclient(), ATMIDTemp, ATMGroupTemp);
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect(path);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void PrintRepGaps() {
        try {
            String ATMIDTemp = "";
            if (atmno != null) {
                ATMIDTemp = atmno.toString();
            }
            String ATMGroupTemp = "";
            if (atmInt.toString() == null ? "0" != null : !atmInt.toString().equals("0")) {
                ATMGroupTemp = atmInt.toString();
            }

            String path = LoadamoutRep.runReportRepGaps(fromDate, toDate, userName, sessionutil.Getclient(), ATMIDTemp, ATMGroupTemp);
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect(path);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    private List<AVGDailyDispenseDTOInter> averageRepCycleresult;

    public List<AVGDailyDispenseDTOInter> getAverageRepCycleresult() {
        return averageRepCycleresult;
    }

    public void setAverageRepCycleresult(List<AVGDailyDispenseDTOInter> averageRepCycleresult) {
        this.averageRepCycleresult = averageRepCycleresult;
    }

    public void PrintaverageRepCycleexcel() {
        try {
            String ATMIDTemp = "";
            if (atmno != null) {
                ATMIDTemp = atmno.toString();
            }
            String ATMGroupTemp = "";
            if (atmInt.toString() == null ? "0" != null : !atmInt.toString().equals("0")) {
                ATMGroupTemp = atmInt.toString();
            }
            averageRepCycleresult = (List<AVGDailyDispenseDTOInter>) LoadamoutRep.runReportaverageRepCycleexcel(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), userName, sessionutil.Getclient(), ATMIDTemp, ATMGroupTemp);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void PrintaverageRepCycle() {
        try {
            String ATMIDTemp = "";
            if (atmno != null) {
                ATMIDTemp = atmno.toString();
            }
            String ATMGroupTemp = "";
            if (atmInt.toString() == null ? "0" != null : !atmInt.toString().equals("0")) {
                ATMGroupTemp = atmInt.toString();
            }
            String path = LoadamoutRep.runReportaverageRepCycle(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), userName, sessionutil.Getclient(), ATMIDTemp, ATMGroupTemp);
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect(path);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    private List<AVGDailyDispenseDTOInter> Dailydispenseresult;

    public List<AVGDailyDispenseDTOInter> getDailydispenseresult() {
        return Dailydispenseresult;
    }

    public void setDailydispenseresult(List<AVGDailyDispenseDTOInter> Dailydispenseresult) {
        this.Dailydispenseresult = Dailydispenseresult;
    }

    public void PrintDailydispenseexcel() {
        try {
            String ATMIDTemp = "";
            if (atmno != null) {
                ATMIDTemp = atmno.toString();
            }
            String ATMGroupTemp = "";
            if (atmInt.toString() == null ? "0" != null : !atmInt.toString().equals("0")) {
                ATMGroupTemp = atmInt.toString();
            }
            Dailydispenseresult = (List<AVGDailyDispenseDTOInter>) LoadamoutRep.runReportDailydispenseexcel(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), userName, sessionutil.Getclient(), ATMIDTemp, ATMGroupTemp);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void PrintDailydispense() {
        try {
            String ATMIDTemp = "";
            if (atmno != null) {
                ATMIDTemp = atmno.toString();
            }
            String ATMGroupTemp = "";
            if (atmInt.toString() == null ? "0" != null : !atmInt.toString().equals("0")) {
                ATMGroupTemp = atmInt.toString();
            }
            String path = LoadamoutRep.runReportDailydispense(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), userName, sessionutil.Getclient(), ATMIDTemp, ATMGroupTemp);
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect(path);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    private List<TotalWithDrawalsDTOInter> printAtmTotalResult;

    public List<TotalWithDrawalsDTOInter> getPrintAtmTotalResult() {
        return printAtmTotalResult;
    }

    public void setPrintAtmTotalResult(List<TotalWithDrawalsDTOInter> printAtmTotalResult) {
        this.printAtmTotalResult = printAtmTotalResult;
    }

    public void printAtmTotalExcel() {
        try {

            printAtmTotalResult = (List<TotalWithDrawalsDTOInter>) atmTotalRep.runReportExcel(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmInt);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void printAtmTotal() {
        try {
            String path = atmTotalRep.runReport(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmInt, userName, sessionutil.Getclient());
            prepDownload(path);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    private List<statbytransDTOInter> printTransresult;

    public List<statbytransDTOInter> getPrintTransresult() {
        return printTransresult;
    }

    public void setPrintTransresult(List<statbytransDTOInter> printTransresult) {
        this.printTransresult = printTransresult;
    }

    public void printTransexcel() {
        try {
            printTransresult = (List<statbytransDTOInter>) atmByTransRep.runReportexcel(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmInt, userName, Integer.valueOf(temp), sessionutil.Getclient());
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printTrans() {
        try {
            String path = atmByTransRep.runReport(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmInt, userName, Integer.valueOf(temp), sessionutil.Getclient());
            prepDownload(path);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void searchReplanishment() throws Throwable {
        boolean val = false;
        if (fromDate_ico == null) {
            if (toDate_ico == null) {
                val = true;
            }
        }
        if (fromDate_ico != null) {
            if (toDate_ico != null) {
                val = true;
            }
        }
        if (val == true) {
            val = false;
            if (fromDate_ico1 == null) {
                if (toDate_ico1 == null) {
                    val = true;
                }
            }
            if (fromDate_ico1 != null) {
                if (toDate_ico1 != null) {
                    val = true;
                }
            }
        }
        if (val == true) {
            repList = (List<ReplanishmentMasterDTOInter>) repDetailsReport.getReplanishmentData(dailySheetfrom, fromDate, toDate, atmids, fromCreate, toCreate, userInt, group2, procstatus, fromDate_ico, toDate_ico, fromDate_ico1, toDate_ico1);
        } else {
            message = "Insert Complete Date Range";
            return;
        }
        listSize = repList.size();
    }

    public void printReplanishment() throws Throwable {
        String path = (String) repDetailsReport.print(selectedRep, userName, sessionutil.Getclient());
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.sendRedirect(path);
    }
    private List<cardissuerDTOInter> printBankResult;

    public List<cardissuerDTOInter> getPrintBankResult() {
        return printBankResult;
    }

    public void setPrintBankResult(List<cardissuerDTOInter> printBankResult) {
        this.printBankResult = printBankResult;
    }

    public void printBankExcel() {
        try {
            printBankResult = (List<cardissuerDTOInter>) banksTotalRep.runReportrxcel(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmInt, userName, sideInt, sessionutil.Getclient());
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void printBank() {
        try {
            String path = banksTotalRep.runReport(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmInt, userName, sideInt, sessionutil.Getclient());
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect(path);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    private List<bycardDTOInter> printCardNoResult;

    public List<bycardDTOInter> getPrintCardNoResult() {
        return printCardNoResult;
    }

    public void setPrintCardNoResult(List<bycardDTOInter> printCardNoResult) {
        this.printCardNoResult = printCardNoResult;
    }

    public void printCardNoexcel() {
        try {
            atmno = Integer.valueOf(temp);
            printCardNoResult = (List<bycardDTOInter>) cardNoRep.runReportexcel(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmInt, userName, sideInt, sortBy, atmno, drawInt, sessionutil.Getclient());
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void printCardNo() {
        try {
            atmno = Integer.valueOf(temp);
            String path = cardNoRep.runReport(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmInt, userName, sideInt, sortBy, atmno, drawInt, sessionutil.Getclient());
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect(path);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    private List<replanishmentHistoryRepDTOInter> printRepHistoryresult;

    public List<replanishmentHistoryRepDTOInter> getPrintRepHistoryresult() {
        return printRepHistoryresult;
    }

    public void setPrintRepHistoryresult(List<replanishmentHistoryRepDTOInter> printRepHistoryresult) {
        this.printRepHistoryresult = printRepHistoryresult;
    }

    public void printRepHistoryExcel() {
        try {
            if (fromDate == null || toDate == null) {
                message = super.showMessage(REQ_FIELD);
            } else {
                printRepHistoryresult = (List<replanishmentHistoryRepDTOInter>) repHistoryRep.findReportexcel(fromDate, toDate, rephistoryatmid, atmInt, sessionutil.Getclient(), userName);
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    private List<PrintcardtakenExcelInter> printcardtakenresult;

    public List<PrintcardtakenExcelInter> getPrintcardtakenresult() {
        return printcardtakenresult;
    }

    public void setPrintcardtakenresult(List<PrintcardtakenExcelInter> printcardtakenresult) {
        this.printcardtakenresult = printcardtakenresult;
    }

    public void printCardTakenExcel() {
        try {
            if (fromDate == null || toDate == null) {
                message = super.showMessage(REQ_FIELD);
            } else {
                printcardtakenresult = (List<PrintcardtakenExcelInter>) repHistoryRep.findcardtakenexcel(fromDate, toDate, rephistoryatmid, atmInt, sessionutil.Getclient(), userName);
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void printcardtaken() {
        try {
            if (fromDate == null || toDate == null) {
                message = super.showMessage(REQ_FIELD);
            } else {

                String path = (String) repHistoryRep.findcardtakenReport(fromDate, toDate, rephistoryatmid, atmInt, sessionutil.Getclient(), userName);
                FacesContext context = FacesContext.getCurrentInstance();
                HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                response.sendRedirect(path);
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void printRepHistory() {
        try {
            if (fromDate == null || toDate == null) {
                message = super.showMessage(REQ_FIELD);
            } else {

                String path = (String) repHistoryRep.findReport(fromDate, toDate, rephistoryatmid, atmInt, sessionutil.Getclient(), userName);
                FacesContext context = FacesContext.getCurrentInstance();
                HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                response.sendRedirect(path);
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void groupChange(ValueChangeEvent e) {
        try {
            atmInt = (Integer) e.getNewValue();
            atmIDList = (List<AtmMachineDTOInter>) repHistoryRep.getAtmMachines(super.getLoggedInUser(), atmInt);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
    private List<swglDTOInter> printSWGLresult;

    public List<swglDTOInter> getPrintSWGLresult() {
        return printSWGLresult;
    }

    public void setPrintSWGLresult(List<swglDTOInter> printSWGLresult) {
        this.printSWGLresult = printSWGLresult;
    }

    public void printSWGLexcel() throws Throwable {
        if (!fromDate.equals(null) || !toDate.equals(null)) {
            try {
                printSWGLresult = (List<swglDTOInter>) repSWGL.printExcel(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate));
            } catch (Throwable ex) {
                message = ex.getMessage();
            }
        } else {
            message = super.showMessage(REQ_FIELD);
        }
    }

    public void printSWGL() throws Throwable {
        if (!fromDate.equals(null) || !toDate.equals(null)) {
            try {
                String path = (String) repSWGL.print(super.getLoggedInUser().getUserName(), sessionutil.Getclient(), fromDate, toDate);
                FacesContext context = FacesContext.getCurrentInstance();
                HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                response.sendRedirect(path);
            } catch (Throwable ex) {
                message = ex.getMessage();
            }
        } else {
            message = super.showMessage(REQ_FIELD);
        }
    }

    public void networkChange(ValueChangeEvent ex) throws Throwable {
        Integer newV = (Integer) ex.getNewValue();
        if (newV != 0) {
            exceptBool = false;
            exeptList = (List<NetworkMasterDTOInter>) revenueBO.findExceptions(newV);
        } else {
            exceptBool = true;
            exepInt = 0;
        }
    }

    public void printRevenue() throws Throwable {
        if (!fromDate.equals(null) || !toDate.equals(null)) {
            try {
                String path = (String) revenueBO.print(super.getLoggedInUser().getUserName(), sessionutil.Getclient(), fromDate, toDate, exepInt, netInt, 1, atmno, drawInt, typeInt);
                FacesContext context = FacesContext.getCurrentInstance();
                HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                response.sendRedirect(path);
            } catch (Throwable ex) {
                message = ex.getMessage();
            }
        } else {
            message = super.showMessage(REQ_FIELD);
        }
    }

    public void selectAllRows() {
        DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:blockReason");
        repTable.setSelection(repList.toArray());
        printCheck = true;
        deselectFlag = true;
        selectFlag = false;
    }

    public void deselectAllRows() {
        printCheck = false;
        deselectFlag = false;
        selectFlag = true;
    }

    public void disablePrint(AjaxBehaviorEvent e) {
        if (toDate == null || fromDate == null) {
            printCheck = true;
        } else {
            printCheck = false;
        }
    }

    public void disablePrint1(AjaxBehaviorEvent e) {
        if (atmno == null) {
            printCheck = true;
        } else {
            printCheck = false;
        }
    }

    public void disablePrint2(AjaxBehaviorEvent e) {
        if (atmInt == null) {
            printCheck = true;
        } else {
            printCheck = false;
        }
    }

    public void overlayCheck() {
        if (fromDate != null || toDate != null) {
            fromDate_ico = null;
            toDate_ico = null;
            fromDate_ico1 = null;
            toDate_ico1 = null;
            subdatefromFlag = true;
            subdatetoFlag = true;
            fromOverlay = false;
            toOverlay = false;
        } else if (fromDate_ico != null || toDate_ico != null) {
            datefromFlag = true;
            fromOverlay = true;
            datetoFlag = true;
            fromDate = null;
            toDate = null;
            fromDate_ico1 = null;
            subdatetoFlag = true;
            toDate_ico1 = null;
            toOverlay = false;

        } else if (fromDate_ico1 != null || toDate_ico1 != null) {
            datefromFlag = true;
            fromOverlay = true;
            datetoFlag = true;
            toOverlay = true;
            subdatefromFlag = true;
            fromDate_ico = null;
            toDate_ico = null;
            fromDate = null;
            toDate = null;
        } else {
            subdatefromFlag = false;
            datefromFlag = false;
            fromOverlay = true;
            subdatetoFlag = false;
            datetoFlag = false;
            toOverlay = true;
        }
    }

    public void calenderIconChange(ValueChangeEvent e) {
        if (toDate_ico != null || fromDate_ico != null) {
            iconCheck = 1;
        } else {
            iconCheck = 0;
        }
    }

    public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();

    }

    private List<DisputeRepDTOInter> printDisputeRep;

    public void printDisputeRepExcel() {
        try {
            printDisputeRep = (List<DisputeRepDTOInter>) DispRepBO.runReportrxcel(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmids, userInt, group2);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void printDisputeReps() {
        try {
            String path = DispRepBO.runReport(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), atmids, userName, sessionutil.Getclient(), userInt, group2);
            prepDownload(path);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    private List<ReplainshmentLogDTOInter> printRepLog;

    public void printRepLogExcel() {

        try {
            atmInt = 0;
            printRepLog = (List<ReplainshmentLogDTOInter>) RepLogBO.runReportrxcel(DateFormatter.changeDateFormat(dailySheetfrom), DateFormatter.changeDateFormat(dailySheetto), DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), userInt);
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void printRepLog2() {
        try {
            String path = RepLogBO.runReport(DateFormatter.changeDateFormat(dailySheetfrom), DateFormatter.changeDateFormat(dailySheetto), DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate), userInt, sessionutil.Getclient(), userName);
            prepDownload(path);
        } catch (Throwable ex) {
            System.out.println(ex.getMessage());
            message = ex.getMessage();
        }
    }

}
