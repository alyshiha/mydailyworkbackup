/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface UsersDTOInter extends Serializable {

    Boolean getUnsettlementboolean();

    void setUnsettlementboolean(Boolean unsettlementboolean);

    Boolean getSettlementboolean();

    void setSettlementboolean(Boolean settlementboolean);

    int getUnsettlement();

    void setUnsettlement(int unsettlement);

    int getSettlement();

    void setSettlement(int settlement);

    public int getCorrectiveEntry();

    public Boolean getShowCorrect();

    public void setShowCorrect(Boolean ShowCorrect);

    public void setCorrectiveEntry(int CorrectiveEntry);

    void setShowcorrect(Boolean showcorrect);

    Boolean getShowcorrect();

    void setShowexport(Boolean showexport);

    Boolean getShowexport();

    public int getExport();

    public void setExport(int Export);

    public Boolean getErrorsB();

    public void setErrorsB(Boolean errorsB);

    public Boolean getNotificationB();

    public void setNotificationB(Boolean notificationB);

    public Boolean getOnlineuserB();

    public void setOnlineuserB(Boolean onlineuserB);

    public Boolean getPendingAtms();

    public void setPendingAtms(Boolean pendingAtms);

    public int getErrors();

    public void setErrors(int errors);

    public int getNotification();

    public void setNotification(int notification);

    public int getOnlineuser();

    public void setOnlineuser(int onlineuser);

    public int getPendingatms();

    public void setPendingatms(int pendingatms);

    public ProfileDTOInter getProfileDTO();

    public void setProfileDTO(ProfileDTOInter profileDTO);

    public int getStatus();

    public void setStatus(int status);

    public int getRestrected();

    public void setRestrected(int restrected);

    public int getSeeRestrected();

    public void setSeeRestrected(int seeRestrected);

    String getConfirmPassword();

    void setConfirmPassword(String confirmPassword);

    List<AtmMachineDTOInter> getAtmMachineDTOL();

    void setAtmMachineDTOL(List<AtmMachineDTOInter> atmMachineDTOL);

    void setShowNewAtmsB(Boolean showNewAtmsB);

    Boolean getShowNewAtmsB();

    void setHolidayEnabledB(Boolean holidayEnabledB);

    Boolean getHolidayEnabledB();

    void setEmrHolidayEnabledB(Boolean emrHolidayEnabledB);

    Boolean getEmrHolidayEnabledB();

    void setDisputGraphB(Boolean disputGraphB);

    Boolean getDisputGraphB();

    int getChangeOtherUserPassword();

    int getDisputeGraph();

    int getEmrHolidayEnabled();

    int getHolidayEnabled();

    String getLogonName();

    int getSeeUnvisble();

    int getShiftId();

    int getShowNewAtms();

    String getUserAName();

    int getUserId();

    int getUserLang();

    String getUserName();

    String getUserPassword();

    int getVisible();

    void setChangeOtherUserPassword(int changeOtherUserPassword);

    void setDisputeGraph(int disputeGraph);

    void setEmrHolidayEnabled(int emrHolidayEnabled);

    void setHolidayEnabled(int holidayEnabled);

    void setLogonName(String logonName);

    void setSeeUnvisble(int seeUnvisble);

    void setShiftId(int shiftId);

    void setShowNewAtms(int showNewAtms);

    void setUserAName(String userAName);

    void setUserId(int userId);

    void setUserLang(int userLang);

    void setUserName(String userName);

    void setUserPassword(String userPassword);

    void setVisible(int visible);
}
