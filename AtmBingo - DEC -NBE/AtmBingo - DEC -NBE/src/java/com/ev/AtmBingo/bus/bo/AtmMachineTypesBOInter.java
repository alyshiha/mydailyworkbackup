/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;

/**
 *
 * @author Administrator
 */
public interface AtmMachineTypesBOInter extends BaseBOInter{

    Object editeAtmMachineType(AtmMachineTypeDTOInter amtDTO, int operation) throws Throwable;

    Object getAtmMachineType() throws Throwable;

}
