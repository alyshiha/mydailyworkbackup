/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.DisputesDTOInter;
import com.ev.AtmBingo.bus.dto.RepTransDetailDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface DisputesDAOInter {

    Object insertBlackListed(Object... obj) throws Throwable;

    void saveAll(String Query, Integer UserID) throws SQLException;

    Object[] customSearch(String whereClause, String field, String index) throws Throwable;

    Object delete(Object... obj) throws Throwable;

    Object deleteForDisputePage(Object... obj) throws Throwable;

    Object find(Date transactionDate, String atmApplicationId, String cardNo, int amount, String currency, String responseCode, int matchingType, int recordType) throws Throwable;

    Object findAll() throws Throwable;

    String Findtexttransaction(Object obj) throws Throwable;

    Clob FindFiletransaction(Object obj) throws Throwable;

    Object findAnotherSide(Object obj) throws Throwable;

    String Findtexttransaction_other(Object obj) throws Throwable;

    Clob FindFiletransaction_other(Object obj) throws Throwable;

    Object findReport(String fields, String whereClause, DisputesDTOInter[] dDTOArr) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object countDisputesAndNotSettled(Object... obj) throws Throwable;

    Object findTotalAmount(String whereClause) throws Throwable;

    Object countDashBoard(Object... obj) throws Throwable;

    Object countDisputesAndSettled(Object... obj) throws Throwable;

    Object insertForDisputePage(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

    Object updateForCorrectiveEntryrep(RepTransDetailDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable;

    Object updateForCorrectiveEntry(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable;

    Object updateForCorrectiveEntry2(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable;

    Object updateForCorrectiveEntry2rep(RepTransDetailDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable;

    Object updateForExport(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable;

    Object updateForExport2(DisputesDTOInter uDTO, UsersDTOInter logedUser) throws Throwable;

    Object updateForDisputePage(DisputesDTOInter uDTO, UsersDTOInter logedUser) throws Throwable;

    Object updateForDisputePage(RepTransDetailDTOInter uDTO, UsersDTOInter logedUser) throws Throwable;

    Object updateComments(RepTransDetailDTOInter uDTO) throws Throwable;

    Object updateComments(DisputesDTOInter uDTO) throws Throwable;

    Object updatecorrectiveamount(DisputesDTOInter uDTO, UsersDTOInter user) throws Throwable;

    Object updateForDisputePage2(DisputesDTOInter uDTO, UsersDTOInter logedUser) throws Throwable;
}
