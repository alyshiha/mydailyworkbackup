/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.HolidaysDAOInter;
import com.ev.AtmBingo.bus.dto.HolidaysDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class HolidaysBO extends BaseBO implements HolidaysBOInter,Serializable{

    protected HolidaysBO() {
        super();
    }

    public Object getHolidays()throws Throwable{
        HolidaysDAOInter hDAO = DAOFactory.createHolidaysDAO(null);
        List<HolidaysDTOInter> hDTOL = (List<HolidaysDTOInter>)hDAO.findAll();

        return hDTOL;
    }

    public Object editeHolidays(Object obj,Integer oldMonth, Integer oldDay, int operation)throws Throwable{
        HolidaysDAOInter hDAO = DAOFactory.createHolidaysDAO(null);
        HolidaysDTOInter hDTO = (HolidaysDTOInter)obj;

        switch(operation){
            case INSERT:
                hDAO.insert(hDTO);
                return "Insert Done";
            case UPDATE:
                hDAO.update(hDTO,oldMonth,oldDay);
                return "Update Done";
            case DELETE:
                hDAO.delete(hDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }

}
