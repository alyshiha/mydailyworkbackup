/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Administrator
 */
 

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class ExportLogDTO extends BaseDTO implements ExportLogDTOInter , Serializable{

    String atmApplicationId;
    String currency;
    Date transactionDate;
    String transactionSequence;
    String cardNo;
    Integer amount;
    Date Export_date;
    String responseCode;
    Integer matchingType;
    Integer recordType;
    Integer ExportUser;
    Integer ExportState;

    public Integer getExportState() {
        return ExportState;
    }

    public void setExportState(Integer ExportState) {
        this.ExportState = ExportState;
    }
    protected ExportLogDTO() {
        super();
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getAtmApplicationId() {
        return atmApplicationId;
    }

    public void setAtmApplicationId(String atmApplicationId) {
        this.atmApplicationId = atmApplicationId;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public Date getExport_date() {
        return Export_date;
    }

    public void setExport_date(Date Export_date) {
        this.Export_date = Export_date;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getMatchingType() {
        return matchingType;
    }

    public void setMatchingType(Integer matchingType) {
        this.matchingType = matchingType;
    }

    public Integer getRecordType() {
        return recordType;
    }

    public void setRecordType(Integer recordType) {
        this.recordType = recordType;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getExportUser() {
        return ExportUser;
    }

    public void setExportUser(Integer ExportUser) {
        this.ExportUser = ExportUser;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionSequence() {
        return transactionSequence;
    }

    public void setTransactionSequence(String transactionSequence) {
        this.transactionSequence = transactionSequence;
    }

}

