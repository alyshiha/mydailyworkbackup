/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AtmGroupEditorDaoInter extends BaseBOInter{
   public void UpdateAtmByGroupId(List<String> entities,Integer UserID) throws SQLException ;

}
