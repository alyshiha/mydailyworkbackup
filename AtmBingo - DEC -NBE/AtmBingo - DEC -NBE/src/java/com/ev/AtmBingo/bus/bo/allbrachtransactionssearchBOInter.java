/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.allbranchtransactionssearchDTOInter;

/**
 *
 * @author Aly
 */
public interface allbrachtransactionssearchBOInter {

    Object deleterecord(Integer tempid) throws Throwable;

    Object gettemplate(Integer tempid) throws Throwable;

    Object getusertemplatelist(Integer userid) throws Throwable;

  Object insertrecord(allbranchtransactionssearchDTOInter RecordToInsert,Integer userid) throws Throwable ;

    Object updatetemplate(Object... obj) throws Throwable;
    
}
