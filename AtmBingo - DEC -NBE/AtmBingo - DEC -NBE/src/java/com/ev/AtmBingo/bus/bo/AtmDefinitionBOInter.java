/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;

/**
 *
 * @author Administrator
 */
public interface AtmDefinitionBOInter extends BaseBOInter{

    Object editeAtmMachine(Object obj, int operation) throws Throwable;

    Object getAtmMachines(UsersDTOInter u) throws Throwable;

    Object getAtmMachinesOutLic() throws Throwable;

    Object getAtmGroups() throws Throwable;

    Object getAtmMachineType()throws Throwable;

    Object getCassetteGroup()throws Throwable;

    Object getGroupCount()throws Throwable;


}
