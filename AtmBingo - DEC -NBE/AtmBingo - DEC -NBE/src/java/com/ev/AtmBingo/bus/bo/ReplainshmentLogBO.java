/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.ReplainshmentLogDAOInter;
import com.ev.AtmBingo.bus.dto.ReplainshmentLogDTOInter;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author Aly-Shiha
 */
public class ReplainshmentLogBO extends BaseBO implements ReplainshmentLogBOInter {

    protected ReplainshmentLogBO() {
        super();
    }

    @Override
    public List<ReplainshmentLogDTOInter> runReportrxcel(String dailysheetFrom, String dailysheetTo, String dateFrom, String dateTo, int user) throws Throwable {
        ReplainshmentLogDAOInter arrRepDAO = DAOFactory.createReplainshmentLogDAO();
        List<ReplainshmentLogDTOInter> rs = (List<ReplainshmentLogDTOInter>) arrRepDAO.findDataTable(dailysheetFrom, dailysheetTo, dateFrom, dateTo, user);
        return rs;
    }

    @Override
    public String runReport(String dailysheetFrom, String dailysheetTo, String dateFrom, String dateTo, int user, String cust, String username) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        JRResultSetDataSource resultSetDataSource;
        ReplainshmentLogDAOInter arrRepDAO = DAOFactory.createReplainshmentLogDAO();

        ResultSet rs = (ResultSet) arrRepDAO.findReport(dailysheetFrom, dailysheetTo, dateFrom, dateTo, user);
        resultSetDataSource = new JRResultSetDataSource(rs);
        
        if (dailysheetFrom == "") {
            params.put("dailysheetfrom", "1");
        } else {
            params.put("dailysheetfrom", dailysheetFrom);
        }
        if (dailysheetTo == "") {
            params.put("dailysheetto", "1");
        } else {
            params.put("dailysheetto", dailysheetTo);
        }
        if (dateFrom == "") {
            params.put("DateFrom", "1");
        } else {
            params.put("DateFrom", dateFrom);
        }
        if (dateTo == "") {
            params.put("DateTo", "1");
        } else {
            params.put("DateTo", dateFrom);
        }
        params.put("workinguser", ""+user);
        params.put("user", username);
        params.put("customer", cust);

        try {

            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/RepLog.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);

            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, resultSetDataSource);

            Calendar c = Calendar.getInstance();
            String uri = "DisputeRep" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {

                String x = pdfPath + uri;

                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                resultSetDataSource = null;
                CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());
                rs.close();

                return x;

            } catch (Exception ex) {
                ex.printStackTrace();
                return  ex.getMessage();
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();

            return connectMsg;
        }

    }

}
