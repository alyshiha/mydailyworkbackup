/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface TransactionResponseCodeDTOInter extends Serializable{

    void setDefaultFlagB(Boolean defaultFlagB);

    Boolean getDefaultFlagB();

    int getAmountType();

    int getDefaultFlag();

    String getDescription();

    int getId();

    int getMasterCode();

    String getName();

    void setAmountType(int amountType);

    void setDefaultFlag(int defaultFlag);

    void setDescription(String description);

    void setId(int id);

    void setMasterCode(int masterCode);

    void setName(String name);

}
