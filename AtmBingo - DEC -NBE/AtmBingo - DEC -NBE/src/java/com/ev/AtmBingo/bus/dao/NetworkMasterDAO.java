/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.NetworkGroupDTOInter;
import com.ev.AtmBingo.bus.dto.NetworkMasterDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author KhAiReE
 */
public class NetworkMasterDAO extends BaseDAO implements NetworkMasterDAOInter {

    protected NetworkMasterDAO() {
        super();
        super.setTableName("network_master");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        NetworkMasterDTOInter nmDTO = (NetworkMasterDTOInter) obj[0];
        String insertStat = "insert into $table values ($group_id ,$currency , $rate , $rat_type "
                + ", $amount_to , $transaction_type, $amoun_from)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$group_id", "" + nmDTO.getGroupId());
        insertStat = insertStat.replace("$currency", "" + nmDTO.getCurrency());
        insertStat = insertStat.replace("$rate", "" + nmDTO.getRate());
        insertStat = insertStat.replace("$rat_type", "" + nmDTO.getRateType());
        insertStat = insertStat.replace("$amoun_from", "" + nmDTO.getAmountFrom());
        insertStat = insertStat.replace("$amount_to", "" + nmDTO.getAmountTo());
        insertStat = insertStat.replace("$transaction_type", "" + nmDTO.getTransactionType());
        super.executeUpdate(insertStat);
        NetworkGroupDAOInter ngDAO = DAOFactory.createNetworkGroupDAO(null);
        NetworkGroupDTOInter ngDTO = (NetworkGroupDTOInter) ngDAO.find(nmDTO.getGroupId());
        String action = "Add a new " + ngDTO.getNetwork() + " network's defualt";
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        NetworkMasterDTOInter nmDTO = (NetworkMasterDTOInter) obj[0];
        Integer curr = (Integer) obj[1];
        Integer trans = (Integer) obj[2];
        String updateStat = "update $table set currency = $currency , rate = $rate ,"
                + "rate_type = $rat_type , amount_to = $amoun_to, amount_from = $amount_from , "
                + "transaction_type= $transaction_type where currency = $oldCurr and transaction_type = $oldTrans and group_id = $group_id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$oldCurr", "" + curr);
        updateStat = updateStat.replace("$oldTrans", "" + trans);
        updateStat = updateStat.replace("$group_id", "" + nmDTO.getGroupId());
        updateStat = updateStat.replace("$currency", "" + nmDTO.getCurrency());
        updateStat = updateStat.replace("$rate", "" + nmDTO.getRate());
        updateStat = updateStat.replace("$rat_type", "" + nmDTO.getRateType());
        updateStat = updateStat.replace("$amoun_to", "" + nmDTO.getAmountTo());
        updateStat = updateStat.replace("$amount_from", "" + nmDTO.getAmountFrom());
        updateStat = updateStat.replace("$transaction_type", "" + nmDTO.getTransactionType());
        super.executeUpdate(updateStat);
        NetworkGroupDAOInter ngDAO = DAOFactory.createNetworkGroupDAO(null);
        NetworkGroupDTOInter ngDTO = (NetworkGroupDTOInter) ngDAO.find(nmDTO.getGroupId());
        String action = "Update " + ngDTO.getNetwork() + " network's defualt";
        super.postUpdate(action, false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        NetworkMasterDTOInter nmDTO = (NetworkMasterDTOInter) obj[0];
        String deleteStat = "delete from $table where transaction_type = $trans and currency = $curr and group_id = $group_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$curr", "" + nmDTO.getCurrency());
        deleteStat = deleteStat.replace("$trans", "" + nmDTO.getTransactionType());
        deleteStat = deleteStat.replace("$group_id", "" + nmDTO.getGroupId());
        super.executeUpdate(deleteStat);
        NetworkGroupDAOInter ngDAO = DAOFactory.createNetworkGroupDAO(null);
        NetworkGroupDTOInter ngDTO = (NetworkGroupDTOInter) ngDAO.find(nmDTO.getGroupId());
        String action = "Delete " + ngDTO.getNetwork() + " network's defualt";
        super.postUpdate(action, false);
        return null;
    }

    public Object findAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "select * from $table order by transaction_type";
        selectStat = selectStat.replace("$table", super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<NetworkMasterDTOInter> nmDTOL = new ArrayList<NetworkMasterDTOInter>();
        while (rs.next()) {
            NetworkMasterDTOInter nmDTO = DTOFactory.createNetworkMasterDTO();
            nmDTO.setGroupId(rs.getInt("group_id"));
            nmDTO.setCurrency(rs.getInt("currency"));
            nmDTO.setRate(rs.getBigDecimal("rate"));
            nmDTO.setRateType(rs.getInt("rate_type"));
            nmDTO.setAmountTo(rs.getInt("amount_to"));
            nmDTO.setAmountFrom(rs.getInt("amount_from"));
            nmDTO.setTransactionType(rs.getInt("transaction_type"));
            nmDTOL.add(nmDTO);
        }
        postSelect(rs);
        return nmDTOL;
    }

    public Object find(int id, int curr, int trans) throws Throwable {
        preSelect();
        String selectStat = "select * from $table where group_id = $id and currency = $curr and transaction_type = $trans order by transaction_type";
        selectStat = selectStat.replace("$table", super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        selectStat = selectStat.replace("$curr", "" + curr);
        selectStat = selectStat.replace("$trans", "" + trans);
        ResultSet rs = executeQuery(selectStat);
        NetworkMasterDTOInter nmDTO = DTOFactory.createNetworkMasterDTO();
        while (rs.next()) {
            nmDTO.setGroupId(rs.getInt("group_id"));
            nmDTO.setCurrency(rs.getInt("currency"));
            nmDTO.setRate(rs.getBigDecimal("rate"));
            nmDTO.setRateType(rs.getInt("rate_type"));
            nmDTO.setAmountTo(rs.getInt("amount_to"));
            nmDTO.setAmountFrom(rs.getInt("amount_from"));
            nmDTO.setTransactionType(rs.getInt("transaction_type"));
        }
        postSelect(rs);
        return nmDTO;
    }

    public Object findByIdMaster(int id) throws Throwable {
        preSelect();
        String selectStat = "select * from $table where group_id = $id order by transaction_type";
        selectStat = selectStat.replace("$table", super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        ResultSet rs = executeQuery(selectStat);
        List<NetworkMasterDTOInter> nmDTOL = new ArrayList<NetworkMasterDTOInter>();
        while (rs.next()) {
            NetworkMasterDTOInter nmDTO = DTOFactory.createNetworkMasterDTO();
            nmDTO.setGroupId(rs.getInt("group_id"));
            nmDTO.setCurrency(rs.getInt("currency"));
            nmDTO.setRate(rs.getBigDecimal("rate"));
            nmDTO.setRateType(rs.getInt("rate_type"));
            nmDTO.setAmountTo(rs.getInt("amount_to"));
            nmDTO.setAmountFrom(rs.getInt("amount_from"));
            nmDTO.setTransactionType(rs.getInt("transaction_type"));
            nmDTOL.add(nmDTO);
        }
        postSelect(rs);
        return nmDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        Object obj1 = super.preSelect();
        super.postSelect(obj1);
        return null;
    }
}
