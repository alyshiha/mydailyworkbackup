package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.ev.AtmBingo.bus.dto.usersbranchDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UsersDAO extends BaseDAO implements UsersDAOInter {

    protected UsersDAO() {
        super();
        super.setTableName("users");
    }

    public String ValidateLog(String userName, String password, String version, Integer NumOfUsers, Date EndDate) throws Throwable {

        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select login_validation('$u_name','$u_pass','$Runningversion',$NumOfUsers,to_date('$enddate','YY mm dd')) from dual";
        selectStat = selectStat.replace("$u_name", "" + userName);
        selectStat = selectStat.replace("$u_pass", "" + password);
        selectStat = selectStat.replace("$Runningversion", "" + version);
        selectStat = selectStat.replace("$NumOfUsers", "" + NumOfUsers);
        selectStat = selectStat.replace("$enddate", "" + EndDate);
        ResultSet rs = executeQueryFirst(selectStat);
        String Result = "";
        while (rs.next()) {
            Result = rs.getString(1);

        }
        super.postSelect(rs);
        return Result;
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        UsersDTOInter uDTO = (UsersDTOInter) obj[0];
        uDTO.setUserId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + "(user_id, user_name, user_a_name, user_lang,  logon_name, user_password,  dispute_graph, show_new_atms, visible, see_unvisible, change_other_user_password, restrected, see_restrected, status, show_pending_atms, show_notifications, show_errors, show_online_user, show_export, show_corrective_entry,show_settlement,show_unsettlement)"
                + "values"
                + "($user_id, '$user_name', '$user_a_name', $user_lang,  '$logon_name', conv.conv_string('$user_password'),  $dispute_graph, $show_new_atms, $visible, $see_unvisible, $change_other_user_password, $restrected, $seeRestrected, $status, $showpendingatms, 1, $showerrors, $showonlineuser, $show_Export, $showCorrectiveEntry,'$showsettlement','$showunsettlement')";

        insertStat = insertStat.replace("$showpendingatms", "" + uDTO.getPendingatms());
        insertStat = insertStat.replace("$showsettlement", "" + uDTO.getSettlement());
        insertStat = insertStat.replace("$showunsettlement", "" + uDTO.getUnsettlement());
        insertStat = insertStat.replace("$shownotifications", "" + uDTO.getNotification());
        insertStat = insertStat.replace("$showerrors", "" + uDTO.getErrors());
        insertStat = insertStat.replace("$showonlineuser", "" + uDTO.getOnlineuser());
        //   insertStat = insertStat.replace("$showCorrectiveEntry", "" + uDTO.getCorrectiveEntry());
        //insertStat = insertStat.replace("$show_Export", "" + uDTO.getExport());
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$user_id", "" + uDTO.getUserId());
        insertStat = insertStat.replace("$user_name", "" + uDTO.getUserName());
        insertStat = insertStat.replace("$user_a_name", "" + uDTO.getUserAName());
        insertStat = insertStat.replace("$user_lang", "" + uDTO.getUserLang());
        insertStat = insertStat.replace("$logon_name", "" + uDTO.getLogonName());
        insertStat = insertStat.replace("$user_password", "" + uDTO.getUserPassword());

        insertStat = insertStat.replace("$dispute_graph", "" + uDTO.getDisputeGraph());
        insertStat = insertStat.replace("$show_new_atms", "" + uDTO.getShowNewAtms());
        insertStat = insertStat.replace("$visible", "" + 1);
        insertStat = insertStat.replace("$see_unvisible", "" + 1);
        insertStat = insertStat.replace("$change_other_user_password", "" + uDTO.getChangeOtherUserPassword());
        insertStat = insertStat.replace("$restrected", "" + 2);
        insertStat = insertStat.replace("$seeRestrected", "" + 2);
        insertStat = insertStat.replace("$status", "" + 2);
        Integer temp;
        if (uDTO.getShowCorrect() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        insertStat = insertStat.replace("$showCorrectiveEntry", "" + temp);
        if (uDTO.getShowexport() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        insertStat = insertStat.replace("$show_Export", "" + temp);
        super.executeUpdate(insertStat);

        //    String stat = "insert into user_pass values($userId,conv.conv_string('$upass'),sysdate)";
        //  stat = stat.replace("$userId", "" + uDTO.getUserId());
//        stat = stat.replace("$upass", uDTO.getUserPassword());
        //      super.executeUpdate(stat);
        super.postUpdate("Add User With Name " + uDTO.getUserName(), false);
        return null;
    }

    public Object updateRest(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        UsersDTOInter uDTO = (UsersDTOInter) obj[0];
        String updateStat = "update $table set "
                + "restrected = $rest, see_restrected = $seeRest"
                + " where user_id = $user_id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$user_id", "" + uDTO.getUserId());

        updateStat = updateStat.replace("$rest", "" + uDTO.getRestrected());
        updateStat = updateStat.replace("$seeRest", "" + uDTO.getSeeRestrected());
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object updateStatus(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        UsersDTOInter uDTO = (UsersDTOInter) obj[0];
        String updateStat = "update $table set "
                + "status = $status"
                + " where user_id = $user_id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$user_id", "" + uDTO.getUserId());

        updateStat = updateStat.replace("$status", "" + uDTO.getStatus());
        super.executeUpdate(updateStat);
        super.postUpdate((uDTO.getStatus() == 1 ? "Login" : "Logout"), false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        UsersDTOInter uDTO = (UsersDTOInter) obj[0];
        String updateStat = "update $table set show_settlement = '$showsettlement' ,show_unsettlement = '$showunsettlement',user_name = '$user_name', user_a_name = '$user_a_name', user_lang = $user_lang"
                + ",  logon_name = '$logon_name',"
                + "dispute_graph = $dispute_graph, show_new_atms = $show_new_atms, visible = $visible, "
                + "see_unvisible = $see_unvisible, change_other_user_password = $change_other_user_password,"
                + "restrected = $rest, see_restrected = $seeRest,show_Export = $showExport,show_Corrective_Entry = $showCorrectiveEntry"
                + ",show_pending_atms = $showpendingatms,show_notifications = 1,show_errors = $showerrors,show_online_user = $showonlineuser"
                + " where user_id = $user_id ";
        updateStat = updateStat.replace("$showExport", "" + uDTO.getExport());
        updateStat = updateStat.replace("$showCorrectiveEntry", "" + uDTO.getCorrectiveEntry());
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$user_id", "" + uDTO.getUserId());
        updateStat = updateStat.replace("$user_name", "" + uDTO.getUserName());
        updateStat = updateStat.replace("$user_a_name", "" + uDTO.getUserAName());
        updateStat = updateStat.replace("$user_lang", "" + uDTO.getUserLang());
        updateStat = updateStat.replace("$showsettlement", "" + uDTO.getSettlement());
        updateStat = updateStat.replace("$showunsettlement", "" + uDTO.getUnsettlement());
        updateStat = updateStat.replace("$logon_name", "" + uDTO.getLogonName());

        updateStat = updateStat.replace("$showpendingatms", "" + uDTO.getPendingatms());
        updateStat = updateStat.replace("$shownotifications", "" + uDTO.getNotification());
        updateStat = updateStat.replace("$showerrors", "" + uDTO.getErrors());
        updateStat = updateStat.replace("$showonlineuser", "" + uDTO.getOnlineuser());

        updateStat = updateStat.replace("$dispute_graph", "" + uDTO.getDisputeGraph());
        updateStat = updateStat.replace("$show_new_atms", "" + uDTO.getShowNewAtms());
        updateStat = updateStat.replace("$visible", "" + uDTO.getVisible());
        updateStat = updateStat.replace("$see_unvisible", "" + uDTO.getSeeUnvisble());
        updateStat = updateStat.replace("$change_other_user_password", "" + uDTO.getChangeOtherUserPassword());
        updateStat = updateStat.replace("$rest", "" + uDTO.getRestrected());
        updateStat = updateStat.replace("$seeRest", "" + uDTO.getSeeRestrected());
        super.executeUpdate(updateStat);
        super.postUpdate("Update " + uDTO.getUserName(), false);
        return null;
    }

    public Boolean getaccess(UsersDTOInter user) throws Throwable {
        super.preSelect();
        Boolean found = Boolean.FALSE;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select s.* from MENU_LABELS m,USER_PROFILE up,PROFILE_MENU pm,users s \n"
                + "where upper(m.page) = upper('Users.xhtml') \n"
                + "and s.user_id = up.user_id\n"
                + "and s.user_id = '$id'\n"
                + "and pm.profile_id=up.profile_id\n"
                + "and pm.menu_id = m.menu_id";
        selectStat = selectStat.replace("$id", "" + user.getUserId() + "");
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            found = Boolean.TRUE;
        }
        super.postSelect(rs);
        return found;
    }

    public int userlic() throws Throwable {
        super.preSelect();
        int linc = 0;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "SELECT (select no_of_users from license) -\n"
                + "(select  count(distinct action) activeusers from V$session where schemaname = 'ATM' and module = 'ATMBingo') RemainUsers\n"
                + "FROM DUAL";
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            linc = rs.getInt("RemainUsers");
        }
        super.postSelect(rs);
        return linc;
    }

    public Boolean Checkpass(UsersDTOInter uDTO, String confPass, String newPass) throws Throwable {
        super.preSelect();
        Boolean find = false;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from user_pass where user_id = $user_id and pass =  conv.conv_string('$Password')";
        selectStat = selectStat.replace("$user_id", "" + uDTO.getUserId());
        selectStat = selectStat.replace("$Password", "" + newPass);
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            find = true;
        }
        super.postSelect(rs);
        return find;
    }

    public Object Lockuser(String userName) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String updateStat = "update $table s set s.locked = 1 where upper(s.logon_name) = upper('$user_id')";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$user_id", "" + userName);
        super.executeUpdate(updateStat);

        super.postUpdate("Password updated of " + userName, false);
        return null;
    }

    public Object updatePassBranch(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        usersbranchDTOInter uDTO = (usersbranchDTOInter) obj[0];
        String updateStat = "update $table set user_password = conv.conv_string('$password')"
                + " where user_id = $user_id";
        updateStat = updateStat.replace("$table", "USERS_BRANCH");
        updateStat = updateStat.replace("$user_id", "" + uDTO.getuserid());

        updateStat = updateStat.replace("$password", "" + uDTO.getuserpassword());

        super.executeUpdate(updateStat);

        String stat = "insert into USER_PASS_BRANCH values($userId,conv.conv_string('$upass'),sysdate)";
        stat = stat.replace("$userId", "" + uDTO.getuserid());
        stat = stat.replace("$upass", uDTO.getuserpassword());
        super.executeUpdate(stat);

        super.postUpdate("Password updated of " + uDTO.getusername(), false);
        return null;
    }

    public Object updatePass(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        UsersDTOInter uDTO = (UsersDTOInter) obj[0];
        String updateStat = "update $table set user_password = conv.conv_string('$password')"
                + " where user_id = $user_id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$user_id", "" + uDTO.getUserId());

        updateStat = updateStat.replace("$password", "" + uDTO.getUserPassword());

        super.executeUpdate(updateStat);

        String stat = "insert into user_pass values($userId,conv.conv_string('$upass'),sysdate)";
        stat = stat.replace("$userId", "" + uDTO.getUserId());
        stat = stat.replace("$upass", uDTO.getUserPassword());
        super.executeUpdate(stat);

        super.postUpdate("Password updated of " + uDTO.getUserName(), false);
        return null;
    }

    public Object updatePass2(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        UsersDTOInter uDTO = (UsersDTOInter) obj[0];
        String updateStat = "update $table set user_password = conv.conv_string('$password')"
                + " where user_id = $user_id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$user_id", "" + uDTO.getUserId());

        updateStat = updateStat.replace("$password", "" + uDTO.getUserPassword());

        super.executeUpdate(updateStat);

        String stat = "insert into user_pass values($userId,conv.conv_string('$upass'),to_date('01 01 1900','dd mm YY'))";
        stat = stat.replace("$userId", "" + uDTO.getUserId());
        stat = stat.replace("$upass", uDTO.getUserPassword());
        super.executeUpdate(stat);

        super.postUpdate("Password updated of " + uDTO.getUserName(), false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();

        // databse insert statement usinf conn pool getConn().createStatement();
        UsersDTOInter uDTO = (UsersDTOInter) obj[0];
        String deleteStat = "delete from $table where user_id = $user_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$user_id", "" + uDTO.getUserId());
        super.executeUpdate(deleteStat);
        super.postUpdate(uDTO.getUserName() + " deleted", false);
        return null;
    }

    public Object findALl(int rest) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by status,user_name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$rest", "" + rest);
        ResultSet rs = executeQuery(selectStat);
        List<UsersDTOInter> uDTOL = new ArrayList<UsersDTOInter>();
        while (rs.next()) {
            UsersDTOInter uDTO = DTOFactory.createUsersDTO();
            uDTO.setChangeOtherUserPassword(rs.getInt("change_other_user_password"));
            uDTO.setDisputeGraph(rs.getInt("dispute_graph"));

            uDTO.setLogonName(rs.getString("logon_name"));
            uDTO.setSeeUnvisble(rs.getInt("see_unvisible"));

            uDTO.setShowNewAtms(rs.getInt("show_new_atms"));
            uDTO.setUserAName(rs.getString("user_a_name"));
            uDTO.setUserId(rs.getInt("user_id"));
            uDTO.setUserLang(rs.getInt("user_lang"));
            uDTO.setUserName(rs.getString("user_name"));
            uDTO.setUserPassword(rs.getString("user_password"));
            uDTO.setVisible(rs.getInt("visible"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTO.setSeeRestrected(rs.getInt("see_restrected"));
            uDTO.setDisputeGraph(rs.getInt("dispute_graph"));
            uDTO.setShowNewAtms(rs.getInt("show_new_atms"));
            uDTO.setPendingatms(rs.getInt("show_pending_atms"));
            uDTO.setNotification(rs.getInt("show_notifications"));
            uDTO.setErrors(rs.getInt("show_errors"));
            uDTO.setOnlineuser(rs.getInt("show_online_user"));
            uDTO.setExport(rs.getInt("show_Export"));
            uDTO.setStatus(rs.getInt("status"));
            uDTO.setSettlement(rs.getInt("show_settlement"));
            uDTO.setUnsettlement(rs.getInt("show_unsettlement"));
            if (uDTO.getSettlement() == 1) {
                uDTO.setSettlementboolean(Boolean.TRUE);
            } else {
                uDTO.setSettlementboolean(Boolean.FALSE);
            }
            if (uDTO.getUnsettlement() == 1) {
                uDTO.setUnsettlementboolean(Boolean.TRUE);
            } else {
                uDTO.setUnsettlementboolean(Boolean.FALSE);
            }
            Integer temp = rs.getInt("show_Corrective_Entry");
            if (temp == 1) {
                uDTO.setShowCorrect(Boolean.TRUE);
            } else {
                uDTO.setShowCorrect(Boolean.FALSE);
            }
            Integer temp2 = rs.getInt("show_Export");
            if (temp2 == 1) {
                uDTO.setShowexport(Boolean.TRUE);
            } else {
                uDTO.setShowexport(Boolean.FALSE);
            }

            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer userId = (Integer) obj[0];
        String selectStat = "select * from $table where user_id = $user_id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$user_id", "" + userId);
        ResultSet rs = executeQuery(selectStat);
        UsersDTOInter uDTO = DTOFactory.createUsersDTO();
        while (rs.next()) {
            uDTO.setChangeOtherUserPassword(rs.getInt("change_other_user_password"));
            uDTO.setDisputeGraph(rs.getInt("dispute_graph"));

            uDTO.setLogonName(rs.getString("logon_name"));
            uDTO.setSeeUnvisble(rs.getInt("see_unvisible"));

            uDTO.setShowNewAtms(rs.getInt("show_new_atms"));
            uDTO.setUserAName(rs.getString("user_a_name"));
            uDTO.setUserId(rs.getInt("user_id"));
            uDTO.setUserLang(rs.getInt("user_lang"));
            uDTO.setUserName(rs.getString("user_name"));
            uDTO.setUserPassword(rs.getString("user_password"));
            uDTO.setVisible(rs.getInt("visible"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTO.setSeeRestrected(rs.getInt("see_restrected"));
            uDTO.setStatus(rs.getInt("status"));

        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findBranch(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer userId = (Integer) obj[0];
        String selectStat = "select * from $table where user_id = $user_id";
        selectStat = selectStat.replace("$table", "USERS_BRANCH");
        selectStat = selectStat.replace("$user_id", "" + userId);
        ResultSet rs = executeQuery(selectStat);
        usersbranchDTOInter uDTO = DTOFactory.createusersbranchDTO();
        while (rs.next()) {
            uDTO.setlogonname(rs.getString("logon_name"));
            uDTO.setbranchname(rs.getString("branchname"));
            uDTO.setuserid(rs.getInt("user_id"));
            uDTO.setusername(rs.getString("user_name"));
            uDTO.setuserpassword(rs.getString("user_password"));
            uDTO.setstatus(rs.getInt("status"));

        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findByLogonName(String logonName) throws Throwable {
        //CreateDTO cd = new CreateDTO();
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select *"
                + " from $table"
                + " where upper(logon_name) = upper('$user_name')";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$user_name", "" + logonName);
        ResultSet rs = executeQueryFirst(selectStat);
        UsersDTOInter uDTO = DTOFactory.createUsersDTO();
        while (rs.next()) {
            uDTO.setChangeOtherUserPassword(rs.getInt("change_other_user_password"));

            uDTO.setDisputeGraph(rs.getInt("dispute_graph"));
            uDTO.setShowNewAtms(rs.getInt("show_new_atms"));
            uDTO.setPendingatms(rs.getInt("show_pending_atms"));
            uDTO.setNotification(rs.getInt("show_notifications"));
            uDTO.setErrors(rs.getInt("show_errors"));
            uDTO.setOnlineuser(rs.getInt("show_online_user"));
            uDTO.setExport(rs.getInt("show_Export"));
            uDTO.setCorrectiveEntry(rs.getInt("show_Corrective_Entry"));

            uDTO.setLogonName(rs.getString("logon_name"));
            uDTO.setSeeUnvisble(rs.getInt("see_unvisible"));

            uDTO.setShowNewAtms(rs.getInt("show_new_atms"));
            uDTO.setUserAName(rs.getString("user_a_name"));
            uDTO.setUserId(rs.getInt("user_id"));
            uDTO.setUserLang(rs.getInt("user_lang"));
            uDTO.setUserName(rs.getString("user_name"));
            uDTO.setUserPassword(rs.getString("user_password"));
            uDTO.setVisible(rs.getInt("visible"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTO.setSeeRestrected(rs.getInt("see_restrected"));
            uDTO.setStatus(rs.getInt("status"));
            uDTO.setSettlement(rs.getInt("show_settlement"));
            uDTO.setUnsettlement(rs.getInt("show_unsettlement"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object checkUserPass(String userName, String pass) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select *"
                + " from $table"
                + " where upper(logon_name) = upper('$user_name')"
                + " and conv.conv_string('$user_password') = user_password";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$user_name", "" + userName);
        selectStat = selectStat.replace("$user_password", "" + pass);
        ResultSet rs = executeQuery(selectStat);
        boolean isValid = rs.next();
        super.postSelect(rs);
        return isValid;
    }

    public Object encrUserPass(String pass) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select conv.conv_string('$user_password')"
                + " from dual";
        selectStat = selectStat.replace("$table", "" + super.getTableName());

        selectStat = selectStat.replace("$user_password", "" + pass);
        ResultSet rs = executeQuery(selectStat);
        String encPass = "";
        while (rs.next()) {
            encPass = rs.getString(1);
        }
        super.postSelect(rs);
        return encPass;
    }

    public Object FirstTimeLogin(String UserName) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select count(*)   from user_pass where user_id in (select user_id from users where user_name = '$user_Name') and change_date = to_date('01 01 1900','dd mm YY')";

        selectStat = selectStat.replace("$user_Name", "" + UserName);
        ResultSet rs = executeQuery(selectStat);
        String User = "";
        while (rs.next()) {
            User = rs.getString(1);
        }
        super.postSelect(rs);
        return User;
    }

    public Object findComboBox() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select *"
                + " from $table"
                + "  order by user_name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<UsersDTOInter> uDTOL = new ArrayList<UsersDTOInter>();
        while (rs.next()) {
            UsersDTOInter uDTO = DTOFactory.createUsersDTO();
            uDTO.setChangeOtherUserPassword(rs.getInt("change_other_user_password"));
            uDTO.setDisputeGraph(rs.getInt("dispute_graph"));

            uDTO.setLogonName(rs.getString("logon_name"));
            uDTO.setSeeUnvisble(rs.getInt("see_unvisible"));

            uDTO.setShowNewAtms(rs.getInt("show_new_atms"));
            uDTO.setUserAName(rs.getString("user_a_name"));
            uDTO.setUserId(rs.getInt("user_id"));
            uDTO.setUserLang(rs.getInt("user_lang"));
            uDTO.setUserName(rs.getString("user_name"));
            uDTO.setUserPassword(rs.getString("user_password"));
            uDTO.setVisible(rs.getInt("visible"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTO.setSeeRestrected(rs.getInt("see_restrected"));
            uDTO.setStatus(rs.getInt("status"));
            Integer temp = rs.getInt("SHOW_CORRECTIVE_ENTRY");
            if (temp == 1) {
                uDTO.setShowCorrect(Boolean.TRUE);
            } else {
                uDTO.setShowCorrect(Boolean.FALSE);
            }
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findDisputeGraphUser() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select *"
                + " from $table"
                + " where dispute_graph = 1 order by user_name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<UsersDTOInter> uDTOL = new ArrayList<UsersDTOInter>();
        while (rs.next()) {
            UsersDTOInter uDTO = DTOFactory.createUsersDTO();
            uDTO.setChangeOtherUserPassword(rs.getInt("change_other_user_password"));
            uDTO.setDisputeGraph(rs.getInt("dispute_graph"));

            uDTO.setLogonName(rs.getString("logon_name"));
            uDTO.setSeeUnvisble(rs.getInt("see_unvisible"));

            uDTO.setShowNewAtms(rs.getInt("show_new_atms"));
            uDTO.setUserAName(rs.getString("user_a_name"));
            uDTO.setUserId(rs.getInt("user_id"));
            uDTO.setUserLang(rs.getInt("user_lang"));
            uDTO.setUserName(rs.getString("user_name"));
            uDTO.setUserPassword(rs.getString("user_password"));
            uDTO.setVisible(rs.getInt("visible"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTO.setSeeRestrected(rs.getInt("see_restrected"));
            uDTO.setStatus(rs.getInt("status"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findHasProfile(int profileId, int rest) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select *"
                + " from $table"
                + " where user_id in (select user_id from user_profile where profile_id = $profileId) order by user_name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$profileId", "" + profileId);
        selectStat = selectStat.replace("$rest", "" + rest);
        ResultSet rs = executeQuery(selectStat);
        List<UsersDTOInter> uDTOL = new ArrayList<UsersDTOInter>();
        while (rs.next()) {
            UsersDTOInter uDTO = DTOFactory.createUsersDTO();
            uDTO.setChangeOtherUserPassword(rs.getInt("change_other_user_password"));
            uDTO.setDisputeGraph(rs.getInt("dispute_graph"));

            uDTO.setLogonName(rs.getString("logon_name"));
            uDTO.setSeeUnvisble(rs.getInt("see_unvisible"));

            uDTO.setShowNewAtms(rs.getInt("show_new_atms"));
            uDTO.setUserAName(rs.getString("user_a_name"));
            uDTO.setUserId(rs.getInt("user_id"));
            uDTO.setUserLang(rs.getInt("user_lang"));
            uDTO.setUserName(rs.getString("user_name"));
            uDTO.setUserPassword(rs.getString("user_password"));
            uDTO.setVisible(rs.getInt("visible"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTO.setSeeRestrected(rs.getInt("see_restrected"));
            uDTO.setStatus(rs.getInt("status"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findHasNotProfile() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select *"
                + " from $table"
                + " where user_id not in (select user_id from user_profile) order by user_name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<UsersDTOInter> uDTOL = new ArrayList<UsersDTOInter>();
        while (rs.next()) {
            UsersDTOInter uDTO = DTOFactory.createUsersDTO();
            uDTO.setChangeOtherUserPassword(rs.getInt("change_other_user_password"));
            uDTO.setDisputeGraph(rs.getInt("dispute_graph"));

            uDTO.setLogonName(rs.getString("logon_name"));
            uDTO.setSeeUnvisble(rs.getInt("see_unvisible"));

            uDTO.setShowNewAtms(rs.getInt("show_new_atms"));
            uDTO.setUserAName(rs.getString("user_a_name"));
            uDTO.setUserId(rs.getInt("user_id"));
            uDTO.setUserLang(rs.getInt("user_lang"));
            uDTO.setUserName(rs.getString("user_name"));
            uDTO.setUserPassword(rs.getString("user_password"));
            uDTO.setVisible(rs.getInt("visible"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTO.setSeeRestrected(rs.getInt("see_restrected"));
            uDTO.setStatus(rs.getInt("status"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findOnline() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select *"
                + " from $table"
                + " where status = 1 by status";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<UsersDTOInter> uDTOL = new ArrayList<UsersDTOInter>();
        while (rs.next()) {
            UsersDTOInter uDTO = DTOFactory.createUsersDTO();
            uDTO.setChangeOtherUserPassword(rs.getInt("change_other_user_password"));
            uDTO.setDisputeGraph(rs.getInt("dispute_graph"));

            uDTO.setLogonName(rs.getString("logon_name"));
            uDTO.setSeeUnvisble(rs.getInt("see_unvisible"));

            uDTO.setShowNewAtms(rs.getInt("show_new_atms"));
            uDTO.setUserAName(rs.getString("user_a_name"));
            uDTO.setUserId(rs.getInt("user_id"));
            uDTO.setUserLang(rs.getInt("user_lang"));
            uDTO.setUserName(rs.getString("user_name"));
            uDTO.setUserPassword(rs.getString("user_password"));
            uDTO.setVisible(rs.getInt("visible"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTO.setSeeRestrected(rs.getInt("see_restrected"));
            uDTO.setStatus(rs.getInt("status"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findOffline() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select *"
                + " from $table"
                + " where status = 2 by status";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<UsersDTOInter> uDTOL = new ArrayList<UsersDTOInter>();
        while (rs.next()) {
            UsersDTOInter uDTO = DTOFactory.createUsersDTO();
            uDTO.setChangeOtherUserPassword(rs.getInt("change_other_user_password"));
            uDTO.setDisputeGraph(rs.getInt("dispute_graph"));

            uDTO.setLogonName(rs.getString("logon_name"));
            uDTO.setSeeUnvisble(rs.getInt("see_unvisible"));

            uDTO.setShowNewAtms(rs.getInt("show_new_atms"));
            uDTO.setUserAName(rs.getString("user_a_name"));
            uDTO.setUserId(rs.getInt("user_id"));
            uDTO.setUserLang(rs.getInt("user_lang"));
            uDTO.setUserName(rs.getString("user_name"));
            uDTO.setUserPassword(rs.getString("user_password"));
            uDTO.setVisible(rs.getInt("visible"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTO.setSeeRestrected(rs.getInt("see_restrected"));
            uDTO.setStatus(rs.getInt("status"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findUnblockUsers(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select *"
                + " from $table"
                + " where user_id not in (select user_id from blocked_users) order by user_name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<UsersDTOInter> uDTOL = new ArrayList<UsersDTOInter>();
        while (rs.next()) {
            UsersDTOInter uDTO = DTOFactory.createUsersDTO();
            uDTO.setChangeOtherUserPassword(rs.getInt("change_other_user_password"));
            uDTO.setDisputeGraph(rs.getInt("dispute_graph"));

            uDTO.setLogonName(rs.getString("logon_name"));
            uDTO.setSeeUnvisble(rs.getInt("see_unvisible"));

            uDTO.setShowNewAtms(rs.getInt("show_new_atms"));
            uDTO.setUserAName(rs.getString("user_a_name"));
            uDTO.setUserId(rs.getInt("user_id"));
            uDTO.setUserLang(rs.getInt("user_lang"));
            uDTO.setUserName(rs.getString("user_name"));
            uDTO.setUserPassword(rs.getString("user_password"));
            uDTO.setVisible(rs.getInt("visible"));
            uDTO.setRestrected(rs.getInt("restrected"));
            uDTO.setSeeRestrected(rs.getInt("see_restrected"));
            uDTO.setStatus(rs.getInt("status"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public static void main(String[] args) throws Throwable {
        UsersDAOInter udao = DAOFactory.createUsersDAO(null);
    }

    public Object findUnlockUsers(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select *"
                + " from $table"
                + " where locked = 1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<UsersDTOInter> uDTOL = new ArrayList<UsersDTOInter>();
        while (rs.next()) {
            UsersDTOInter uDTO = DTOFactory.createUsersDTO();
            uDTO.setLogonName(rs.getString("logon_name"));
            uDTO.setUserAName(rs.getString("user_a_name"));
            uDTO.setUserName(rs.getString("user_name"));
            uDTO.setUserId(rs.getInt("user_id"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }
}
