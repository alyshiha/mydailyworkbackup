
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.allbrachtransactionssearchDAOInter;
import com.ev.AtmBingo.bus.dto.allbranchtransactionssearchDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author KhAiReE
 */
public class allbrachtransactionssearchBO extends BaseBO implements allbrachtransactionssearchBOInter,Serializable {

    protected allbrachtransactionssearchBO() {
        super();
    }

    @Override
    public Object getusertemplatelist(Integer userid) throws Throwable {
        allbrachtransactionssearchDAOInter uDAO = DAOFactory.createallbrachtransactionssearchDAO();
        List<allbranchtransactionssearchDTOInter> uDTOL = (List<allbranchtransactionssearchDTOInter>) uDAO.findRecordsList(userid);
        return uDTOL;
    }

    @Override
    public Object gettemplate(Integer tempid) throws Throwable {
        allbrachtransactionssearchDAOInter uDAO = DAOFactory.createallbrachtransactionssearchDAO();
        allbranchtransactionssearchDTOInter pDTOL = (allbranchtransactionssearchDTOInter) uDAO.findRecord(tempid);
        return pDTOL;
    }

    @Override
    public Object deleterecord(Integer tempid) throws Throwable {
        allbrachtransactionssearchDAOInter uDAO = DAOFactory.createallbrachtransactionssearchDAO();
        uDAO.deleterecord(tempid);
        return "Template Has Been Deleted";
    }

    @Override
    public Object updatetemplate(Object... obj) throws Throwable {
        allbranchtransactionssearchDTOInter RecordToUpdate = (allbranchtransactionssearchDTOInter) obj[0];
        allbrachtransactionssearchDAOInter uDAO = DAOFactory.createallbrachtransactionssearchDAO();
        uDAO.updaterecord(RecordToUpdate);
        return "Template Has Been Updated";
    }

    
    public Object insertrecord(allbranchtransactionssearchDTOInter RecordToInsert,Integer userid) throws Throwable {
        allbrachtransactionssearchDAOInter uDAO = DAOFactory.createallbrachtransactionssearchDAO();
        uDAO.insertrecord(RecordToInsert,userid);
        return "Template Has Been Inserted";
    }

}
