/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.CurrencyDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class CurrencyDAO extends BaseDAO implements CurrencyDAOInter {

    protected CurrencyDAO() {
        super();
        super.setTableName("currency");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        CurrencyDTOInter uDTO = (CurrencyDTOInter) obj[0];
        uDTO.setId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ($id, '$abbreviation' , $master_id,$DefCur)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$abbreviation", "" + uDTO.getAbbreviation());
        insertStat = insertStat.replace("$master_id", "" + uDTO.getMasterId());
    
         Integer temp;
        if(uDTO.getDefcurr() == true){temp=1;}else{temp=2;}
        insertStat = insertStat.replace("$DefCur", "" + temp);


        super.executeUpdate(insertStat);
        String action = "Add a new currency with abbreviation" + uDTO.getAbbreviation();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        CurrencyDTOInter uDTO = (CurrencyDTOInter) obj[0];
        String updateStat = "update $table set DEFAULT_CURRENCY = $DefCur, abbreviation = '$abbreviation', master_id = '$master_id' where id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$abbreviation", "" + uDTO.getAbbreviation());
        updateStat = updateStat.replace("$master_id", "" + uDTO.getMasterId());


               Integer temp;
        if(uDTO.getDefcurr() == true){temp=1;}else{temp=2;}
        updateStat = updateStat.replace("$DefCur", "" + temp);


        super.executeUpdate(updateStat);
        String action = "Update currency with abbreviation" + uDTO.getAbbreviation();
        super.postUpdate(action, false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        CurrencyDTOInter uDTO = (CurrencyDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        String action = "Delete currency with abbreviation" + uDTO.getAbbreviation();
        super.postUpdate(action, false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        CurrencyDTOInter uDTO = DTOFactory.createCurrencyDTO();
        while (rs.next()) {
            uDTO.setAbbreviation(rs.getString("abbreviation"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setMasterId(rs.getInt("master_id"));
             Integer temp = rs.getInt("DEFAULT_CURRENCY");
            if(temp == 1){uDTO.setDefcurr(Boolean.TRUE);}
            else{uDTO.setDefcurr(Boolean.FALSE);}
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<CurrencyDTOInter> uDTOL = new ArrayList<CurrencyDTOInter>();
        while (rs.next()) {
            CurrencyDTOInter uDTO = DTOFactory.createCurrencyDTO();
            uDTO.setAbbreviation(rs.getString("abbreviation"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setMasterId(rs.getInt("master_id"));
                    Integer temp = rs.getInt("DEFAULT_CURRENCY");
            if(temp == 1){uDTO.setDefcurr(Boolean.TRUE);}
            else{uDTO.setDefcurr(Boolean.FALSE);}
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByMaster(CurrencyMasterDTOInter cmDTO) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where master_id = $masterId";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$masterId", "" + cmDTO.getId());
        ResultSet rs = executeQuery(selectStat);
        List<CurrencyDTOInter> uDTOL = new ArrayList<CurrencyDTOInter>();
        while (rs.next()) {
            CurrencyDTOInter uDTO = DTOFactory.createCurrencyDTO();
            uDTO.setAbbreviation(rs.getString("abbreviation"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setMasterId(rs.getInt("master_id"));
     Integer temp = rs.getInt("DEFAULT_CURRENCY");
            if(temp == 1){uDTO.setDefcurr(Boolean.TRUE);}
            else{uDTO.setDefcurr(Boolean.FALSE);}
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
public int CountCurrencydetail(int cmDTO)throws Throwable{
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select count(*) elcheak from $table where master_id = $masterId and DEFAULT_CURRENCY = 1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$masterId", "" + cmDTO);
        ResultSet rs = executeQuery(selectStat);
        Integer temp = 0;
        while (rs.next()) {
      temp = rs.getInt("elcheak");
        }
        super.postSelect(rs);
        return temp; }
}
