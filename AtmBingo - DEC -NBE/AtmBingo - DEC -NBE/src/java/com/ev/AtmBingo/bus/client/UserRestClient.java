/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.UserRestBOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author Administrator
 */
public class UserRestClient extends BaseBean  implements Serializable{

    private List<UsersDTOInter> uDTOL;
    private UsersDTOInter selectedUser;
    private UserRestBOInter urBO;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UsersDTOInter getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(UsersDTOInter selectedUser) {
        this.selectedUser = selectedUser;
    }

    public List<UsersDTOInter> getuDTOL() {
        return uDTOL;
    }

    public void setuDTOL(List<UsersDTOInter> uDTOL) {
        this.uDTOL = uDTOL;
    }

    /** Creates a new instance of UserRestClient */
    public UserRestClient() {
        super();
        super.GetAccess();
        try {
            urBO = BOFactory.createUserRestBO(null);
            uDTOL = (List<UsersDTOInter>) urBO.getUsers(super.getRestrected());
                 FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        } catch (Throwable ex) {
            Logger.getLogger(UserRestClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void fillUserUp(ActionEvent e){
        selectedUser = (UsersDTOInter) e.getComponent().getAttributes().get("updateRowUser");
        message = "";
    }

    public void update(){
        try {
            urBO.update(selectedUser);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
            Logger.getLogger(UserRestClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
