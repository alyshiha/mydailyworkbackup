/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.EmgHolidaysDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class EmgHolidaysDAO extends BaseDAO implements EmgHolidaysDAOInter {

    protected EmgHolidaysDAO() {
        super();
        super.setTableName("emg_holidays");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        EmgHolidaysDTOInter uDTO = (EmgHolidaysDTOInter) obj[0];
        String insertStat = "insert into $table values (to_date('$p_from','dd.mm.yyyy') , to_date('$p_to','dd.mm.yyyy'), '$reason', '$e_reason')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$p_from", "" + DateFormatter.changeDateFormat(uDTO.getpFrom()));
        insertStat = insertStat.replace("$p_to", "" + DateFormatter.changeDateFormat(uDTO.getpTo()));
        insertStat = insertStat.replace("$reason", "" + uDTO.getReason());
        insertStat = insertStat.replace("$e_reason", "" + uDTO.geteReason());
        super.executeUpdate(insertStat);
        super.postUpdate("Add Emergency Holidays For Reason " + uDTO.getReason(), false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        EmgHolidaysDTOInter uDTO = (EmgHolidaysDTOInter) obj[0];
        Date p_fromOld = (Date) obj[1];
        String updateStat = "update $table set p_from = to_date('$p_from','dd.MM.yyyy') ,p_to = to_date('$p_to','dd.MM.yyyy'), a_reason = '$e_reason', reason = '$reason'"
                + " where p_from = to_date('$p_frmOld','dd.MM.yyyy')";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$p_from", "" + DateFormatter.changeDateFormat(uDTO.getpFrom()));
        updateStat = updateStat.replace("$p_frmOld", "" + DateFormatter.changeDateFormat(p_fromOld));
        updateStat = updateStat.replace("$p_to", "" + DateFormatter.changeDateFormat(uDTO.getpTo()));
        updateStat = updateStat.replace("$reason", "" + uDTO.getReason());
        updateStat = updateStat.replace("$e_reason", "" + uDTO.geteReason());
        super.executeUpdate(updateStat);
        super.postUpdate("Update Emergency Holidays For Reason " + uDTO.getReason(), false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        EmgHolidaysDTOInter uDTO = (EmgHolidaysDTOInter) obj[0];
        String deleteStat = "delete from $table where p_from = to_date('$p_from','dd.mm.yyyy')";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$p_from", "" + DateFormatter.changeDateFormat(uDTO.getpFrom()));
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Emergency Holidays Because Reason " + uDTO.getReason(), false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer pFrom = (Integer) obj[0];
        String selectStat = "select * from $table where p_from = $p_from";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$p_from", "" + pFrom);
        ResultSet rs = executeQuery(selectStat);
        EmgHolidaysDTOInter uDTO = DTOFactory.createEmgHolidaysDTO();
        while (rs.next()) {
            uDTO.setReason(rs.getString("reason"));
            uDTO.seteReason(rs.getString("a_reason"));
            uDTO.setpFrom(rs.getDate("p_from"));
            uDTO.setpTo(rs.getDate("p_to"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<EmgHolidaysDTOInter> uDTOL = new ArrayList<EmgHolidaysDTOInter>();
        while (rs.next()) {
            EmgHolidaysDTOInter uDTO = DTOFactory.createEmgHolidaysDTO();
            uDTO.setReason(rs.getString("reason"));
            uDTO.seteReason(rs.getString("a_reason"));
            uDTO.setpFrom(rs.getDate("p_from"));
            uDTO.setpTo(rs.getDate("p_to"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findNowIsHoliday() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "SELECT *"
                + " FROM HOIDAYS H"
                + " WHERE H.MONTH = TO_NUMBER(TO_CHAR(SYSDATE,'MM'))"
                + " AND H.DAY = TO_NUMBER(TO_CHAR(SYSDATE,'DD'))";
        ResultSet rs = executeQuery(selectStat);
        boolean isHoliday = rs.next();
        super.postSelect(rs);
        return isHoliday;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
