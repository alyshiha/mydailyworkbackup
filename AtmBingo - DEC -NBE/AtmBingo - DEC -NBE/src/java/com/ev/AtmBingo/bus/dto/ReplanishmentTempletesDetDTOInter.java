/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface ReplanishmentTempletesDetDTOInter  extends Serializable {

    Integer getCassitteId();

    Integer getCurrency();

    int getId();

    Integer getLenght();

    Integer getLine();

    Integer getPosition();

    String getStartingString();

    Integer getType();

    Integer getTypeNo();

    void setCassitteId(Integer cassitteId);

    void setCurrency(Integer currency);

    void setId(int id);

    void setLenght(Integer lenght);

    void setLine(Integer line);

    void setPosition(Integer position);

    void setStartingString(String startingString);

    void setType(Integer type);

    void setTypeNo(Integer typeNo);

}
