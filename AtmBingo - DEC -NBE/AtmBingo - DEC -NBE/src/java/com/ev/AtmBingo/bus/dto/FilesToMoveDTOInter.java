/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface FilesToMoveDTOInter extends Serializable {

    int getId();

    void setId(int id);

    void setTypename(String typename);

    String getTypename();

    int getActive();

    String getFileName();

    int getFileSize();

    int getType();

    void setActive(int active);

    void setFileName(String fileName);

    void setFileSize(int fileSize);

    void setType(int type);
}
