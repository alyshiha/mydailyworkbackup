/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class PendingAtmsDTO extends BaseDTO implements PendingAtmsDTOInter, Serializable{

    protected  PendingAtmsDTO() {
        super();
    }

    Integer atmId ;
    Integer Done ;
    Integer AtmName ;
    String appName;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Integer getAtmName() {
        return AtmName;
    }

    public void setAtmName(Integer AtmName) {
        this.AtmName = AtmName;
    }

      public Integer getAtmId() {
        return atmId;
    }

    public void setAtmId(Integer atmId) {
        this.atmId = atmId;
    }

      public Integer getDone() {
        return Done;
    }

    public void setDone(Integer Done) {
        this.Done = Done;
    }
  

}
