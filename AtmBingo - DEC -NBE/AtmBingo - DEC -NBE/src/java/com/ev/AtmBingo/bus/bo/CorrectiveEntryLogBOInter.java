/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface CorrectiveEntryLogBOInter extends Serializable {

    Object getCorrectiveLogged(Date dateF, Date dateT, Integer user,  Integer ATMID, Integer State) throws Throwable;

    Object getUsers(int rest) throws Throwable;

    Object getAtmMachines(UsersDTOInter loggedIn) throws Throwable;
}
