package com.ev.AtmBingo.bus.bo;
import java.sql.SQLException;
import java.util.List;
public interface useratmbranchBOInter {

Object insertrecord(Object... obj) throws Throwable;

Object updaterecord(Object... obj) throws Throwable;
Object deleteallrecord(Object... obj) throws Throwable; 
Object deleterecord(Object... obj) throws Throwable;
Object GetListOfRecords(Object... obj) throws Throwable;
Object GetRecord(Object... obj) throws Throwable;
Object GetAllRecords(Object... obj) throws Throwable;
Object getPickList(int userId,String sourcelikevalue,String targetlikevalue) throws Throwable;
 Object editUserProfile(List<String> target, int userid) throws Throwable;
}