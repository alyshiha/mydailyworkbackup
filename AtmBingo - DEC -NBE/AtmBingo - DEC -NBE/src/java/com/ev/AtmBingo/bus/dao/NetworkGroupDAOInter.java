/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import java.io.Serializable;

/**
 *
 * @author KhAiReE
 */
public interface NetworkGroupDAOInter extends Serializable {

    Object delete(Object... obj) throws Throwable;

    Object find(int id) throws Throwable;

    Object findAll(Object... obj) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
