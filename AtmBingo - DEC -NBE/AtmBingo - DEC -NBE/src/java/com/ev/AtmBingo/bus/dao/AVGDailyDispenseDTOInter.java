/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Administrator
 */
public interface AVGDailyDispenseDTOInter {

    String getAtmid();

    String getAvg();

    void setAtmid(String atmid);

    void setAvg(String avg);
    
}
