package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface allbranchtransactionsDTOInter extends Serializable {
void setiscollectedtime(int iscollectedtime);
int getiscollectedtime();
    void setrowid(String rowid);
void setiscollected(int iscollected) ;
 int getiscollected();
    String getrowid();
String getreason();
 void setreason(String reason);
    String gettabletype();

    void settabletype(String tabletype);

    int getfileid();

    void setfileid(int fileid);

    void setstatusdate(Date statusdate);

    Date getstatusdate();

    void setstatusid(int statusid);

    int getstatusid();

    void setusername(String username);

    String getusername();

    int getuserid();

    void setuserid(int userid);

    Date getloadingdate();

    void setloadingdate(Date loadingdate);

    String getatmapplicationid();

    void setatmapplicationid(String atmapplicationid);

    int getatmid();

    void setatmid(int atmid);

    String gettransactiontype();

    void settransactiontype(String transactiontype);

    int gettransactiontypeid();

    void settransactiontypeid(int transactiontypeid);

    String getcurrency();

    void setcurrency(String currency);

    int getcurrencyid();

    void setcurrencyid(int currencyid);

    String gettransactionstatus();

    void settransactionstatus(String transactionstatus);

    int gettransactionstatusid();

    void settransactionstatusid(int transactionstatusid);

    String getresponsecode();

    void setresponsecode(String responsecode);

    int getresponsecodeid();

    void setresponsecodeid(int responsecodeid);

    Date gettransactiondate();

    void settransactiondate(Date transactiondate);

    String gettransactionsequence();

    void settransactionsequence(String transactionsequence);

    String getcardno();

    void setcardno(String cardno);

    int getamount();

    void setamount(int amount);

    Date getsettlementdate();

    void setsettlementdate(Date settlementdate);

    String getnotespresented();

    void setnotespresented(String notespresented);

    String getcustomeraccountnumber();

    void setcustomeraccountnumber(String customeraccountnumber);

    int gettransactionsequenceorderby();

    void settransactionsequenceorderby(int transactionsequenceorderby);

    Date gettransactiontime();

    void settransactiontime(Date transactiontime);

    int getmatchingtype();

    void setmatchingtype(int matchingtype);

    int getrecordtype();

    void setrecordtype(int recordtype);

    int getamounttype();

    void setamounttype(int amounttype);

    int getcardnosuffix();

    void setcardnosuffix(int cardnosuffix);

    String getcolumn1();

    void setcolumn1(String column1);

    String getcolumn2();

    void setcolumn2(String column2);

    String getcolumn3();

    void setcolumn3(String column3);

    String getcolumn4();

    void setcolumn4(String column4);

    String getcolumn5();

    void setcolumn5(String column5);

    int gettransactiontypemaster();

    void settransactiontypemaster(int transactiontypemaster);
}
