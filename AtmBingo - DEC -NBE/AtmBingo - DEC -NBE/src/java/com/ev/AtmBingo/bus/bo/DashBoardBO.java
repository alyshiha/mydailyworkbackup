/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.DisputesDAOInter;
import com.ev.AtmBingo.bus.dao.MatchedDataDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UserWorkDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author KhAiReE
 */
public class DashBoardBO extends BaseBO implements DashBoardBOInter,Serializable {

    protected DashBoardBO() {
        super();
    }

    public List<UserWorkDTOInter> getUserWorkGraph(int userId,String from,String to) throws Throwable {
        DisputesDAOInter dDOA = DAOFactory.createDisputesDAO(null);
        return (List<UserWorkDTOInter>)dDOA.countDashBoard(userId,from,to);
    }

    public Object getNewAtms(int logedinUser)throws Throwable{
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<String> atms = (List<String>)amDAO.findNewAtms(logedinUser);
        amDAO.deleteNewAtms(logedinUser);
        return atms;
    }

    public Object getOnlineUser(int rest)throws Throwable{
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> uDTOL = (List<UsersDTOInter>)uDAO.findALl(rest);
        return uDTOL;
    }

    public Object getOfflineUser()throws Throwable{
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> uDTOL = (List<UsersDTOInter>)uDAO.findOffline();
        return uDTOL;
    }
}
