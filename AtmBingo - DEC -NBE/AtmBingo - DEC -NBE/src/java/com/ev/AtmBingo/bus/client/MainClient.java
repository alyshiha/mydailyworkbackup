/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.CoonectionHandler;
import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
//import com.ev.AtmBingo.base.data.DBCache;

import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.LoginBOInter;
import com.ev.AtmBingo.bus.bo.PrivilageBOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.SystemTableDAOInter;
import com.ev.AtmBingo.bus.dto.SystemTableDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.*;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Administrator
 */
public class MainClient extends BaseBean implements Serializable {

    List<String> miDTOL;
    private String errorMsg;
    private String password, userName_pop;
    String menuItems;
    OutputStream out;
    String customer;
    String userName;
    private Boolean outputFlag;
    private TimeZone timeZone;
    private Boolean loginFlag;
    private String timeOut;
    private PrivilageBOInter prBO;
    private String iframeSrc, bankName;
    private LoginBOInter myLogin;

    public String getUserName_pop() {
        return userName_pop;
    }

    public void setUserName_pop(String userName_pop) {
        this.userName_pop = userName_pop;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getIframeSrc() {
        return iframeSrc;
    }

    public void setIframeSrc(String iframeSrc) {
        this.iframeSrc = iframeSrc;
    }

    public Boolean getOutputFlag() {
        return outputFlag;
    }

    public void setOutputFlag(Boolean outputFlag) {
        this.outputFlag = outputFlag;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public Boolean getLoginFlag() {
        return loginFlag;
    }

    public void setLoginFlag(Boolean loginFlag) {
        this.loginFlag = loginFlag;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getMenuItems() throws Throwable {
        return menuItems;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<String> getMiDTOL() {
        miDTOL = new ArrayList<String>();
        miDTOL.add("aaa");
        miDTOL.add("bbb");
        miDTOL.add("ccc");
        return miDTOL;
    }

    /**
     * Creates a new instance of MainClient
     */
    public MainClient() throws Throwable {

        super();

        SystemTableDAOInter stDAO = DAOFactory.createSystemTableDAO(null);
        SystemTableDTOInter stDTO = null;
        try {
            stDTO = (SystemTableDTOInter) stDAO.find("SESSION_TIME");

        } catch (Throwable ex) {
            Logger.getLogger(MainClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        timeOut = stDTO.getParameterValue();
        prBO = BOFactory.createPrivilageBO(null);
        FacesContext context = FacesContext.getCurrentInstance();

        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "DENY");
        
//        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
//        if(req.getParameter("uid") == null){
//             response.sendRedirect("Login.xhtml");
//             return;
//        }

        Session sessionutil = new Session();
        UsersDTOInter currentUser = sessionutil.GetUserLogging();
        currentUser.setStatus(1);
        prBO.updateStatus(currentUser);
        menuItems = prBO.generateMenu(currentUser.getUserId());

        if (menuItems.equals("") || menuItems == null) {
            outputFlag = true;
        } else {
            outputFlag = false;
        }
        //iframeSrc = prBO.getDashURL();
        iframeSrc = "DashBoard.xhtml";
        userName = currentUser.getUserName();
        timeZone = TimeZone.getDefault();
        // session.setAttribute("validUser", "valid");
        bankName = sessionutil.Getclient();
    }

    public String logout() {
        try {
            //System.out.println("logout");
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
            HttpSession session = (HttpSession) req.getSession(true);
            CoonectionHandler.getInstance().returnAllConnection();

            Session sessionutil = new Session();
            UsersDTOInter u = sessionutil.GetUserLogging();

            u.setStatus(2);
            try {

                prBO.updateStatus(u);
            } catch (Throwable ex) {
                Logger.getLogger(MainClient.class.getName()).log(Level.SEVERE, null, ex);
            }
            Enumeration attributeNames = session.getAttributeNames();
            while (attributeNames.hasMoreElements()) {
                String sAttribute = attributeNames.nextElement().toString();
                session.removeAttribute(sAttribute);
            }

            session.invalidate();
            //    Runtime.getRuntime().gc();
            return "Logout";
        } catch (Throwable ex) {
            Logger.getLogger(MainClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

//    public void releasePOP(IdleEvent e) {
//        FacesContext context = FacesContext.getCurrentInstance();
//         
//        session.setAttribute("LoginFlag", true);
////        SessionCounter.counter--;
//    }
    public String validateLoginForSession() {
        try {
            // LicenseDAOInter lDAO = DAOFactory.createLicenseDAO();
            //LicenseDTOInter lDTO = (LicenseDTOInter) lDAO.findLicense();
            FacesContext context = FacesContext.getCurrentInstance();

            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect("faces/Login.xhtml");
            return "Success";
        } catch (Throwable e) {
            return "Fail";
        }

    }

    public void testClick() {
        //System.out.println("close");
        logout();

    }

    public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        try {
            CoonectionHandler.getInstance().returnAllConnection();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MainClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MainClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
    }

    public void onActive() {
        //System.out.println("Active");
    }

    public void showMessege() {
        if (!errorMsg.equals("Success")) {

            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, ""));
        }
    }
}
