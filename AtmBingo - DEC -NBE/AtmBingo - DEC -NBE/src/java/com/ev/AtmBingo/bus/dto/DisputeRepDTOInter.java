/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author Aly-Shiha
 */
public interface DisputeRepDTOInter {

    String getAtmAppID();

    String getAtmFullName();

    String getCassetteValue();

    Date getDateFrom();

    Date getDateTo();

    void setAtmAppID(String atmAppID);

    void setAtmFullName(String atmFullName);

    void setCassetteValue(String cassetteValue);

    void setDateFrom(Date dateFrom);

    void setDateTo(Date dateTo);
    
}
