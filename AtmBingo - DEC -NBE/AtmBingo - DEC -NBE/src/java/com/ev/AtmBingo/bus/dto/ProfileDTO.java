/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.MenuLabelDAOInter;
import com.ev.AtmBingo.bus.dao.ProfileMenuDAOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ProfileDTO extends BaseDTO implements Serializable, ProfileDTOInter {

    protected ProfileDTO() {
        super();
    }
    private int profileId;
    private String profileName;
    private int restrected;
    private List<MenuLabelDTOInter> mlDTOL;

    public int getRestrected() {
        return restrected;
    }

    public void setRestrected(int restrected) {
        this.restrected = restrected;
    }


    public List<MenuLabelDTOInter> getMlDTOL() throws Throwable {
        ProfileMenuDAOInter pmDAO = DAOFactory.createProfileMenuDAO(null);
        MenuLabelDAOInter mlDAO = DAOFactory.createMenuLabelDAO(null);
        List<ProfileMenuDTOInter> pmDTOL = (List<ProfileMenuDTOInter>) pmDAO.findByProfileId(profileId);
        mlDTOL = new ArrayList<MenuLabelDTOInter>();
        if (pmDTOL.size() != 0) {
            for (ProfileMenuDTOInter pm : pmDTOL) {
                MenuLabelDTOInter mlDTO = (MenuLabelDTOInter)mlDAO.find(pm.getMenuId());
                mlDTOL.add(mlDTO);
            }
        }
        return mlDTOL;
    }

    public void setMlDTOL(List<MenuLabelDTOInter> mlDTOL) {
        this.mlDTOL = mlDTOL;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }
}
