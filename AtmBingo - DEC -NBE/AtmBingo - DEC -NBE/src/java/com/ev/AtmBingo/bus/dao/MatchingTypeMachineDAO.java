/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.MatchingTypeMachinesDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class MatchingTypeMachineDAO extends BaseDAO implements MatchingTypeMachineDAOInter {

    protected MatchingTypeMachineDAO() {
        super();
        super.setTableName("matching_type_machines");

    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchingTypeMachinesDTOInter matchingTypeMachine = (MatchingTypeMachinesDTOInter) obj[0];
        String insertStat = "insert into " + super.getTableName() + " values(" + matchingTypeMachine.getMatchingType() + ","
                + matchingTypeMachine.getMachineType() + "," + matchingTypeMachine.getDefaultMatchingSetting() + ")";
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchingTypeMachinesDTOInter matchingTypeMachine = (MatchingTypeMachinesDTOInter) obj[0];
        Integer oldMachineType = (Integer) obj[1];
        String updateStat = "update " + super.getTableName() + " set matching_type = " + matchingTypeMachine.getMatchingType() + ",machine_type = " + matchingTypeMachine.getMachineType()
                + ", default_matching_setting = " + matchingTypeMachine.getDefaultMatchingSetting()
                + "where matching_type = " + matchingTypeMachine.getMatchingType() + " and machine_type = " + oldMachineType;
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        MatchingTypeMachinesDTOInter matchingTypeMachine = (MatchingTypeMachinesDTOInter) obj[0];
        String deleteStat = "delete from " + super.getTableName() + " where matching_type = " + matchingTypeMachine.getMatchingType() + " and machine_type = " + matchingTypeMachine.getMachineType();
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer matchingTypeMachine = (Integer) obj[0];
        String updateStat = "select * from " + super.getTableName() + " where matching_type = " + matchingTypeMachine.intValue();
        ResultSet result = executeQuery(updateStat);
        List<MatchingTypeMachinesDTOInter> mtL = new ArrayList<MatchingTypeMachinesDTOInter>();
        while (result.next()) {
            MatchingTypeMachinesDTOInter mt = DTOFactory.createMatchingTypeMachinesDTO();
            mt.setDefaultMatchingSetting(result.getInt("default_matching_setting"));
            mt.setMachineType(result.getInt("machine_type"));
            mt.setMatchingType(result.getInt("matching_type"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();

        super.postSelect();
        return null;
    }
}
