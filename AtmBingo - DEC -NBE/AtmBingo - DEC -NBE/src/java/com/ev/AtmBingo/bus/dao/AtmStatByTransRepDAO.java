/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.statbytransDTOInter;
import com.ev.AtmBingo.bus.dto.swglDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmStatByTransRepDAO extends BaseDAO implements AtmStatByTransRepDAOInter {

    protected AtmStatByTransRepDAO() {
        super();
    }

    public Object findReport(String dateFrom, String dateTo, int atmGroupInt, int NoOfAtms) throws Throwable {
        super.preSelect();
        String selectStatment = "select\n"
                + "/*+INDEX (MATCHED_DATA MATCHED_DATA_DATE_INDEX) */\n"
                + "*\n"
                + "from\n"
                + "(select  count(*) transaction#, atm_application_id,atm_id, (  select\n"
                + "/*+INDEX (MATCHED_DATA MATCHED_DATA_DATE_INDEX) */\n"
                + "  count(*)\n"
                + "  from all_transactions\n"
                + "  where transaction_date between to_date($P{DateFrom},'dd.MM.yyyy hh24:mi:ss') and to_date($P{DateTo},'dd.MM.yyyy hh24:mi:ss')\n"
                + "  and ((ATM_ID in (select id from atm_machine where atm_group = $P{AtmGroupint} )) or $P{AtmGroupint} = 0)\n"
                + "  and record_type = 2\n"
                + "  and matching_type =(select id from matching_type where active = 1 and rownum = 1))total\n"
                + "from all_transactions\n"
                + "where transaction_date between to_date($P{DateFrom},'dd.MM.yyyy hh24:mi:ss') and to_date($P{DateTo},'dd.MM.yyyy hh24:mi:ss')\n"
                + "and ((ATM_ID in (select id from atm_machine where atm_group in (select id from  (select * from atm_group l order BY l.parent_id,name) start with id = $P{AtmGroupint} connect by prior id = parent_id))) or $P{AtmGroupint} = 0)\n"
                + "and record_type = 2\n"
                + "and matching_type = (select id from matching_type where active = 1 and (type1 = 2 or type2=2) and rownum = 1)\n"
                + "group by atm_application_id,atm_id\n"
                + "order by 1 desc)\n"
                + "where (rownum <= $P{NoOfAtms} or $P{NoOfAtms} is null)";
        selectStatment = selectStatment.replace("$P{DateFrom}", "'" + dateFrom + "'");
        selectStatment = selectStatment.replace("$P{DateTo}", "'" + dateTo + "'");
        selectStatment = selectStatment.replace("$P{AtmGroupint}", "" + atmGroupInt);
        selectStatment = selectStatment.replace("$P{NoOfAtms}", "" + NoOfAtms);

        ResultSet rs = executeQueryReport(selectStatment);
        //System.out.println(selectStatment);
        super.postSelect();
        return rs;
    }

    public Object findswglexcelReport(String dateFrom, String dateTo) throws Throwable {
        super.preSelect();
        String selectStatment = "select (select sum(a.amount) from all_transactions a where a.record_type = 2 and a.matching_type = 2 and a.transaction_date between to_date('$P{dateF}','dd.mm.rrrr hh24:mi:ss')and to_date('$P{dateT}','dd.mm.rrrr hh24:mi:ss'))sw , (select sum(a.amount) from all_transactions a where a.record_type = 3 and a.matching_type =2 and a.transaction_date between to_date('$P{dateF}','dd.mm.rrrr hh24:mi:ss')and to_date('$P{dateT}','dd.mm.rrrr hh24:mi:ss'))gl, ((select sum(a.amount) from all_transactions a where a.record_type = 3 and a.matching_type =2 and a.transaction_date between to_date('$P{dateF}','dd.mm.rrrr hh24:mi:ss')and to_date('$P{dateT}','dd.mm.rrrr hh24:mi:ss')) - (select sum(a.amount) from all_transactions a where a.record_type = 2 and a.matching_type = 2 and a.transaction_date between to_date('$P{dateF}','dd.mm.rrrr hh24:mi:ss')and to_date('$P{dateT}','dd.mm.rrrr hh24:mi:ss')))diff from dual";
        selectStatment = selectStatment.replace("$P{dateF}", dateFrom);
        selectStatment = selectStatment.replace("$P{dateT}", dateTo);
        ResultSet rs = executeQueryReport(selectStatment);
        List<swglDTOInter> Records = new ArrayList<swglDTOInter>();
        while (rs.next()) {
            swglDTOInter record = DTOFactory.createswglDTO();
            record.setDifferent(rs.getInt("diff"));
            record.setGlvalue(rs.getInt("gl"));
            record.setSwitchvalue(rs.getInt("sw"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findstatbycashexcelReport(String dateFrom, String dateTo, int atmGroupInt, int NoOfAtms, int tType) throws Throwable {
        super.preSelect();
        String selectStatment = "select * from"
                + " (select  sum(amount*amount_type) transaction#, atm_application_id,atm_id, (select"
                + " /*+INDEX (MATCHED_DATA MATCHED_DATA_DATE_INDEX) */ "
                + " sum(t.amount)"
                + " from all_transactions t"
                + " where transaction_date between to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and ($P{AtmGroupInt}  = 0 or (ATM_ID in (select id from atm_machine where atm_group = $P{AtmGroupInt})))"
                + " and transaction_type_id in (select id from transaction_type where TYPE_CATEGORY = $P{tType})"
                + " and record_type = 2"
                + " and matching_type =(select id from matching_type where active = 1 and rownum = 1))total"
                + " from all_transactions"
                + " where transaction_date between to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and ($P{AtmGroupInt} = 0 or (ATM_ID in (select id from atm_machine where atm_group in (select id from  (select * from atm_group l order BY l.parent_id,name) start with id =  $P{AtmGroupInt} connect by prior id = parent_id))) )"
                + " and transaction_type_id in (select id from transaction_type where TYPE_CATEGORY = $P{tType})"
                + " and record_type = 2"
                + " and matching_type = (select id from matching_type where active = 1 and (type1 = 2 or type2=2) and rownum = 1)"
                + " group by atm_application_id,atm_id"
                + " order by 1 desc)"
                + " where (rownum <= $P{NoOfAtms} or $P{NoOfAtms} = 0)";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DatTo}", dateTo);
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        selectStatment = selectStatment.replace("$P{NoOfAtms}", "" + NoOfAtms);
        selectStatment = selectStatment.replace("$P{tType}", "" + tType);

        ResultSet rs = executeQueryReport(selectStatment);
        List<statbytransDTOInter> Records = new ArrayList<statbytransDTOInter>();
        while (rs.next()) {
            statbytransDTOInter record = DTOFactory.createstatbytransDTO();
            record.setTransnumber(rs.getInt("transaction#"));
            record.setName(rs.getString("atm_id"));
            record.setAtmapplicationid(rs.getString("atm_application_id"));
            record.setTotal(rs.getInt("total"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findstatbytransexcelReport(String dateFrom, String dateTo, int atmGroupInt, int NoOfAtms) throws Throwable {
        super.preSelect();
        String selectStatment = "select"
                + " /*+INDEX (MATCHED_DATA MATCHED_DATA_DATE_INDEX) */"
                + " *"
                + " from"
                + " (select  count(*) transaction#, atm_application_id,atm_id, (  select"
                + " /*+INDEX (MATCHED_DATA MATCHED_DATA_DATE_INDEX) */"
                + "  count(*)"
                + "  from all_transactions"
                + "  where transaction_date between to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + "  and ((ATM_ID in (select id from atm_machine where atm_group = $P{AtmGroupInt} )) or $P{AtmGroupInt} = 0)"
                + "  and record_type = 2"
                + "  and matching_type =(select id from matching_type where active = 1 and rownum = 1))total"
                + " from all_transactions"
                + " where transaction_date between to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and ((ATM_ID in (select id from atm_machine where atm_group in (select id from  (select * from atm_group l order BY l.parent_id,name) start with id = $P{AtmGroupInt} connect by prior id = parent_id))) or $P{AtmGroupInt} = 0)"
                + " and record_type = 2"
                + " and matching_type = (select id from matching_type where active = 1 and (type1 = 2 or type2=2) and rownum = 1)"
                + " group by atm_application_id,atm_id"
                + " order by 1 desc)"
                + " where (rownum <= $P{NoOfAtms} or $P{NoOfAtms} = 0)";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DatTo}", dateTo);
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        selectStatment = selectStatment.replace("$P{NoOfAtms}", "" + NoOfAtms);

        ResultSet rs = executeQueryReport(selectStatment);
        List<statbytransDTOInter> Records = new ArrayList<statbytransDTOInter>();
        while (rs.next()) {
            statbytransDTOInter record = DTOFactory.createstatbytransDTO();
            record.setTransnumber(rs.getInt("transaction#"));
            record.setName(rs.getString("atm_id"));
            record.setAtmapplicationid(rs.getString("atm_application_id"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }
}
