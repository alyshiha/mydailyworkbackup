/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class DefualtColumnsDTO extends BaseDTO implements DefualtColumnsDTOInter, Serializable{

    private int columnId;

    public int getColumnId() {
        return columnId;
    }

    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }
    
    protected DefualtColumnsDTO() {
        super();
    }

}
