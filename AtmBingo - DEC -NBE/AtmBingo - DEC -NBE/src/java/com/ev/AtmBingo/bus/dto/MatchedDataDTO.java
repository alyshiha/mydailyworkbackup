/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author administrator
 */

public class MatchedDataDTO extends BaseDTO implements MatchedDataDTOInter , Serializable{

    private int fileid;
    private Date loadingdate;
    private String atmapplicationid;
    private AtmMachineDTOInter atmid;
    private String transactiontype;
    private TransactionTypeDTOInter transactiontypeid;
    private String currency;
    private CurrencyMasterDTOInter currencyid;
    private String transactionstatus;
    private TransactionStatusDTOInter transactionstatusid;
    private String responsecode;
    private TransactionResponseCodeDTOInter responsecodeid;
    private Date transactiondate;
    private String transactionsequence;
    private String cardno;
    private int amount;
    private Date settlementdate;
    private String notespresented;
    private String customeraccountnumber;
    private int transactionsequenceorderby;
    private Date transactiontime;
    private int matchingtype;
    private int recordtype;
    private int matchkey;
    private int settledflag;
    private Date settleddate;
    private UsersDTOInter settleduser;
    private String comments;
    private int matchby;
    private Date matchdate;
    private String column1;
    private String column2;
    private String column3;
    private String column4;
    private String column5;
    private int transactiontypemaster;
    private int responsecodemaster;
    private int cardnosuffix;
    private int blacklist;
    private String rownum,atmname;
    private Date correctivedate;
    private Date exportdate;
    public Date getCorrectivedate() {
        return correctivedate;
    }

    public void setCorrectivedate(Date correctivedate) {
        this.correctivedate = correctivedate;
    }
private int exportuser;
private int correctiveuser;
    public Date getExportdate() {
        return exportdate;
    }
    public void setExportdate(Date exportdate) {
        this.exportdate = exportdate;
    }
    public int getExportuser() {
        return exportuser;
    }
private Boolean isjournal;

    public Boolean getIsjournal() {
        return isjournal;
    }

    public void setIsjournal(Boolean isjournal) {
        this.isjournal = isjournal;
    }
    public void setExportuser(int exportuser) {
        this.exportuser = exportuser;
    }

    public int getCorrectiveuser() {
        return correctiveuser;
    }

    public void setCorrectiveuser(int correctiveuser) {
        this.correctiveuser = correctiveuser;
    }
    
 public String getatmname() {
        return atmname;
    }

    public void setatmname(String atmname) {
        this.atmname = atmname;
    }
    public int getBlacklist() {
        return blacklist;
    }

    public void setBlacklist(int blacklist) {
        this.blacklist = blacklist;
    }

    public Date getmatchdate() {
        return matchdate;
    }

    public void setmatchdate(Date Matchdate) {
        this.matchdate = Matchdate;
    }

    public int getamount() {
        return amount;
    }

    public void setamount(int amount) {
        this.amount = amount;
    }

    public String getatmapplicationid() {
        return atmapplicationid;
    }

    public void setatmapplicationid(String atmapplicationid) {
        this.atmapplicationid = atmapplicationid;
    }

    public AtmMachineDTOInter getatmid() {
        return atmid;
    }

    public void setatmid(AtmMachineDTOInter atmid) {
        this.atmid = atmid;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public int getCardnosuffix() {
        return cardnosuffix;
    }

    public void setCardnosuffix(int cardnosuffix) {
        this.cardnosuffix = cardnosuffix;
    }

    public String getColumn1() {
        return column1;
    }

    public void setColumn1(String column1) {
        this.column1 = column1;
    }

    public String getColumn2() {
        return column2;
    }

    public void setColumn2(String column2) {
        this.column2 = column2;
    }

    public String getColumn3() {
        return column3;
    }

    public void setColumn3(String column3) {
        this.column3 = column3;
    }

    public String getColumn4() {
        return column4;
    }

    public void setColumn4(String column4) {
        this.column4 = column4;
    }

    public String getColumn5() {
        return column5;
    }

    public void setColumn5(String column5) {
        this.column5 = column5;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public CurrencyMasterDTOInter getCurrencyid() {
        return currencyid;
    }

    public void setCurrencyid(CurrencyMasterDTOInter currencyid) {
        this.currencyid = currencyid;
    }

    public String getCustomeraccountnumber() {
        return customeraccountnumber;
    }

    public void setCustomeraccountnumber(String customeraccountnumber) {
        this.customeraccountnumber = customeraccountnumber;
    }

    public int getFileid() {
        return fileid;
    }

    public void setFileid(int fileid) {
        this.fileid = fileid;
    }

    public Date getLoadingdate() {
        return loadingdate;
    }

    public void setLoadingdate(Date loadingdate) {
        this.loadingdate = loadingdate;
    }

    public int getMatchby() {
        return matchby;
    }

    public void setMatchby(int matchby) {
        this.matchby = matchby;
    }

    public int getMatchingtype() {
        return matchingtype;
    }

    public void setMatchingtype(int matchingtype) {
        this.matchingtype = matchingtype;
    }

    public int getMatchkey() {
        return matchkey;
    }

    public void setMatchkey(int matchkey) {
        this.matchkey = matchkey;
    }

    public String getNotespresented() {
        return notespresented;
    }

    public void setNotespresented(String notespresented) {
        this.notespresented = notespresented;
    }

    public int getRecordtype() {
        return recordtype;
    }

    public void setRecordtype(int recordtype) {
         if (recordtype == 1) {
            setIsjournal(Boolean.TRUE);
        } else {
            setIsjournal(Boolean.FALSE);
        }
        this.recordtype = recordtype;
    }

    public String getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

    public TransactionResponseCodeDTOInter getResponsecodeid() {
        return responsecodeid;
    }

    public void setResponsecodeid(TransactionResponseCodeDTOInter responsecodeid) {
        this.responsecodeid = responsecodeid;
    }

    public int getResponsecodemaster() {
        return responsecodemaster;
    }

    public void setResponsecodemaster(int responsecodemaster) {
        this.responsecodemaster = responsecodemaster;
    }

    public String getRownum() {
        return rownum;
    }

    public void setRownum(String rownum) {
        this.rownum = rownum;
    }

    public Date getSettleddate() {
        return settleddate;
    }

    public void setSettleddate(Date settleddate) {
        this.settleddate = settleddate;
    }

    public int getSettledflag() {
        return settledflag;
    }

    public void setSettledflag(int settledflag) {
        this.settledflag = settledflag;
    }

    public UsersDTOInter getSettleduser() {
        return settleduser;
    }

    public void setSettleduser(UsersDTOInter settleduser) {
        this.settleduser = settleduser;
    }

    public Date getSettlementdate() {
        return settlementdate;
    }

    public void setSettlementdate(Date settlementdate) {
        this.settlementdate = settlementdate;
    }

    public Date getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(Date transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getTransactionsequence() {
        return transactionsequence;
    }

    public void setTransactionsequence(String transactionsequence) {
        this.transactionsequence = transactionsequence;
    }

    

    public int getTransactionsequenceorderby() {
        return transactionsequenceorderby;
    }

    public void setTransactionsequenceorderby(int transactionsequenceorderby) {
        this.transactionsequenceorderby = transactionsequenceorderby;
    }

    public String getTransactionstatus() {
        return transactionstatus;
    }

    public void setTransactionstatus(String transactionstatus) {
        this.transactionstatus = transactionstatus;
    }

    public TransactionStatusDTOInter getTransactionstatusid() {
        return transactionstatusid;
    }

    public void setTransactionstatusid(TransactionStatusDTOInter transactionstatusid) {
        this.transactionstatusid = transactionstatusid;
    }

    public Date getTransactiontime() {
        return transactiontime;
    }

    public void setTransactiontime(Date transactiontime) {
        this.transactiontime = transactiontime;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public TransactionTypeDTOInter getTransactiontypeid() {
        return transactiontypeid;
    }

    public void setTransactiontypeid(TransactionTypeDTOInter transactiontypeid) {
        this.transactiontypeid = transactiontypeid;
    }

    public int getTransactiontypemaster() {
        return transactiontypemaster;
    }

    public void setTransactiontypemaster(int transactiontypemaster) {
        this.transactiontypemaster = transactiontypemaster;
    }



    protected MatchedDataDTO() {
        super();
    }

}
