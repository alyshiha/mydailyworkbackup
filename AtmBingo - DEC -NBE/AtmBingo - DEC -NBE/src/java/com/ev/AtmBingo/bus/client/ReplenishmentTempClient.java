/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.ReplanishmentTempletesBOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ReplanishmentTempletesDTO;
import com.ev.AtmBingo.bus.dto.ReplanishmentTempletesDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentTempletesDetDTO;
import com.ev.AtmBingo.bus.dto.ReplanishmentTempletesDetDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class ReplenishmentTempClient extends BaseBean  implements Serializable{

    private ReplanishmentTempletesBOInter repBo;
    private List<ReplanishmentTempletesDTOInter> repList;
    private List<ReplanishmentTempletesDetDTOInter> repListDetail;
    private ReplanishmentTempletesDTOInter selectedRepTemp;
    private ReplanishmentTempletesDetDTOInter newRepDetail, updateRepDetail, deleteRepDetail;
    private List<AtmMachineTypeDTOInter> atmList;
    private List<CurrencyMasterDTOInter> currList;
    private List<CassetteDTOInter> cassetList;
    private Boolean addedName, addName, showAdd, showConfirm, showAddDetail, showConfirmDetail;
    private String message, messageDetail;
    private Integer selectedRepTempInt, oldTypeNo, oldDataType;

    public Integer getOldDataType() {
        return oldDataType;
    }

    public void setOldDataType(Integer oldDataType) {
        this.oldDataType = oldDataType;
    }

    public Integer getOldTypeNo() {
        return oldTypeNo;
    }

    public void setOldTypeNo(Integer oldTypeNo) {
        this.oldTypeNo = oldTypeNo;
    }

    public List<CassetteDTOInter> getCassetList() {
        return cassetList;
    }

    public void setCassetList(List<CassetteDTOInter> cassetList) {
        this.cassetList = cassetList;
    }

    public String getMessageDetail() {
        return messageDetail;
    }

    public void setMessageDetail(String messageDetail) {
        this.messageDetail = messageDetail;
    }

    public Boolean getShowAddDetail() {
        return showAddDetail;
    }

    public void setShowAddDetail(Boolean showAddDetail) {
        this.showAddDetail = showAddDetail;
    }

    public Boolean getShowConfirmDetail() {
        return showConfirmDetail;
    }

    public void setShowConfirmDetail(Boolean showConfirmDetail) {
        this.showConfirmDetail = showConfirmDetail;
    }

    public List<ReplanishmentTempletesDetDTOInter> getRepListDetail() {
        return repListDetail;
    }

    public void setRepListDetail(List<ReplanishmentTempletesDetDTOInter> repListDetail) {
        this.repListDetail = repListDetail;
    }

    public Integer getSelectedRepTempInt() {
        return selectedRepTempInt;
    }

    public void setSelectedRepTempInt(Integer selectedRepTempInt) {
        this.selectedRepTempInt = selectedRepTempInt;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReplanishmentTempletesDTOInter getSelectedRepTemp() {
        return selectedRepTemp;
    }

    public void setSelectedRepTemp(ReplanishmentTempletesDTOInter selectedRepTemp) {
        this.selectedRepTemp = selectedRepTemp;
    }

    public Boolean getAddName() {
        return addName;
    }

    public void setAddName(Boolean addName) {
        this.addName = addName;
    }

    public Boolean getAddedName() {
        return addedName;
    }

    public void setAddedName(Boolean addedName) {
        this.addedName = addedName;
    }

    public List<AtmMachineTypeDTOInter> getAtmList() {
        return atmList;
    }

    public void setAtmList(List<AtmMachineTypeDTOInter> atmList) {
        this.atmList = atmList;
    }

    public List<CurrencyMasterDTOInter> getCurrList() {
        return currList;
    }

    public void setCurrList(List<CurrencyMasterDTOInter> currList) {
        this.currList = currList;
    }

    public List<ReplanishmentTempletesDTOInter> getRepList() {
        return repList;
    }

    public void setRepList(List<ReplanishmentTempletesDTOInter> repList) {
        this.repList = repList;
    }

    /**
     * Creates a new instance of ReplenishmentTempClient
     */
    public ReplenishmentTempClient() throws Throwable {
        super();
        super.GetAccess();
        repBo = BOFactory.createReplanishmenTempletesBOInter(null);
        repList = (List<ReplanishmentTempletesDTOInter>) repBo.getReplanishmentTempletesMaster();
        if (repList.size() > 0) {
            selectedRepTempInt = repList.get(0).getId();
            selectedRepTemp = repList.get(0);
            repListDetail = (List<ReplanishmentTempletesDetDTOInter>) repBo.getReplanishmentmTempletesDetails(selectedRepTempInt);
        } else {
            repList = new ArrayList<ReplanishmentTempletesDTOInter>();
            selectedRepTemp = DTOFactory.createReplanishmentTempletesDTO();
        }
        currList = (List<CurrencyMasterDTOInter>) repBo.getCurrencyMaster();
        cassetList = (List<CassetteDTOInter>) repBo.getCassitte();
        atmList = (List<AtmMachineTypeDTOInter>) repBo.getAtmMachineType();
        addName = true;
        addedName = false;
        showAdd = true;
        showConfirm = false;
        showAddDetail = true;
        showConfirmDetail = false;
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void changeRepTemp(ValueChangeEvent e) throws Throwable {
        selectedRepTemp = (ReplanishmentTempletesDTO) repBo.findByIdMaster((Integer) e.getNewValue());
        repListDetail = (List<ReplanishmentTempletesDetDTOInter>) repBo.getReplanishmentmTempletesDetails(selectedRepTemp.getId());
        resetVars();
    }

    public void addRecord() {
        message = "";
        if (repList == null) {
            repList = new ArrayList<ReplanishmentTempletesDTOInter>();
        }
        selectedRepTempInt = 0;
        selectedRepTemp = DTOFactory.createReplanishmentTempletesDTO();
        repList.add(selectedRepTemp);
        if (repListDetail != null) {
            repListDetail.clear();
        }
        resetVars();
        showAdd = false;
        showConfirm = true;
        addedName = true;
        addName = false;
    }

    public void confirmRecord() {
        try {
            if (selectedRepTemp.getName().equals("") || selectedRepTemp.getStartingPosition() == null || selectedRepTemp.getStartingString().equals("")
                    || selectedRepTemp.getEndingPosition() == null || selectedRepTemp.getEndingString().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                String[] inputs = {selectedRepTemp.getAddingDateFormat(), selectedRepTemp.getAddingDateStartingString(), selectedRepTemp.getAtmIdString(), selectedRepTemp.getCardtakenstartingstring(), selectedRepTemp.getEndingString(), selectedRepTemp.getName(), selectedRepTemp.getRemainingDateFormat(), selectedRepTemp.getRemainingDateStartingString(), selectedRepTemp.getStartingString()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace("\"", "").replace("/", "").replace(":", "").replace("*", "").replace(".", "").replace("-", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    repBo.editeReplanishmentTempletesMaster(selectedRepTemp, repBo.INSERT);
                    showConfirm = false;
                    showAdd = true;
                    addedName = false;
                    addName = true;
                    message = super.showMessage(SUCC);
                } else {
                    message = "Please Enter Valid Data";
                }

            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }

    }

    public void deleteRecord() {
        try {
            String[] inputs = {selectedRepTemp.getAddingDateFormat(), selectedRepTemp.getAddingDateStartingString(), selectedRepTemp.getAtmIdString(), selectedRepTemp.getCardtakenstartingstring(), selectedRepTemp.getEndingString(), selectedRepTemp.getName(), selectedRepTemp.getRemainingDateFormat(), selectedRepTemp.getRemainingDateStartingString(), selectedRepTemp.getStartingString()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace("\"", "").replace("/", "").replace(":", "").replace("*", "").replace(".", "").replace("-", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
                repBo.editeReplanishmentTempletesMaster(selectedRepTemp, repBo.DELETE);
                repList.remove(selectedRepTemp);
                if (repList.size() != 0) {
                    selectedRepTemp = repList.get(0);
                    selectedRepTempInt = repList.get(0).getId();
                }
                showAdd = true;
                showConfirm = false;
                addedName = false;
                addName = true;
                message = super.showMessage(SUCC);
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
            message =super.showMessage(REC_EXIST);
        }
    }

    public void updateRecord() {
        try {

            if (showAdd == false) {
                message = super.showMessage(CONF_FIRST);
            } else if (selectedRepTemp.getName().equals("") || selectedRepTemp.getStartingPosition() == null || selectedRepTemp.getStartingString().equals("")
                    || selectedRepTemp.getEndingPosition() == null || selectedRepTemp.getEndingString().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                String[] inputs = {selectedRepTemp.getAddingDateFormat(), selectedRepTemp.getAddingDateStartingString(), selectedRepTemp.getAtmIdString(), selectedRepTemp.getCardtakenstartingstring(), selectedRepTemp.getEndingString(), selectedRepTemp.getName(), selectedRepTemp.getRemainingDateFormat(), selectedRepTemp.getRemainingDateStartingString(), selectedRepTemp.getStartingString()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace("\"", "").replace("/", "").replace(":", "").replace("*", "").replace(".", "").replace("-", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    repBo.editeReplanishmentTempletesMaster(selectedRepTemp, repBo.UPDATE);
                    message = super.showMessage(SUCC);
                } else {
                    message = "Please Enter Valid Data";
                }

            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void addRecordDetail() {
        if (repListDetail == null) {
            repListDetail = new ArrayList<ReplanishmentTempletesDetDTOInter>();
        }
        if (showConfirm == true) {
            messageDetail = "Please confirm the master";
        }
        else{
        newRepDetail = DTOFactory.createReplanishmentTempletesDetDTO();
        repListDetail.add(newRepDetail);
        showAddDetail = false;
        showConfirmDetail = true;
        }
    }


public void oldTypeNo(ValueChangeEvent e) {
        oldTypeNo = (Integer) e.getOldValue();
        resetVars();
    }

    public void oldDataType(ValueChangeEvent e) {
        oldDataType = (Integer) e.getOldValue();
        resetVars();
    }

    public void setDeleteRecord(ActionEvent e) {
        deleteRepDetail = (ReplanishmentTempletesDetDTO) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void deleteRecordDetail() {
        try {
            String[] inputs = {deleteRepDetail.getStartingString()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace("\"", "").replace("/", "").replace(":", "").replace("*", "").replace(".", "").replace("-", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
                repListDetail.remove(deleteRepDetail);
                repBo.editeReplanishmentTempletesDetSingle(deleteRepDetail, repBo.DELETE, 0, 0, 0);
                messageDetail = super.showMessage(SUCC);
                showAddDetail = true;
                showConfirmDetail = false;
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
            messageDetail = super.showMessage(REC_EXIST);
        }
    }

    public void setUpdateRecord(ActionEvent e) {
        updateRepDetail = (ReplanishmentTempletesDetDTO) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    public void updateRecordDetail() {
        try {
            if (showAddDetail == false) {
                messageDetail = super.showMessage(CONF_FIRST);
            } else if (updateRepDetail.getTypeNo() == null) {
                messageDetail = super.showMessage(REQ_FIELD);
            } else {
                if (oldTypeNo == null) {
                    oldTypeNo = updateRepDetail.getTypeNo();
                }
                if (oldDataType == null) {
                    oldDataType = updateRepDetail.getType();
                }
                String[] inputs = {updateRepDetail.getStartingString()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace("\"", "").replace("/", "").replace(":", "").replace("*", "").replace(".", "").replace("-", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    repBo.editeReplanishmentTempletesDetSingle(updateRepDetail, repBo.UPDATE, oldDataType, oldTypeNo, selectedRepTemp.getId());
                    messageDetail = super.showMessage(SUCC);
                    oldTypeNo = null;
                    oldDataType = null;
                } else {
                    messageDetail = "Please Enter Valid Data";
                }

            }
        } catch (Throwable ex) {
            //messageDetail = super.showMessage(REC_EXIST);
            messageDetail = super.showMessage(REC_EXIST);
        }
    }

    public void confirmRecordDetail() {
        try {
            if (newRepDetail.getStartingString().equals("") || newRepDetail.getLenght() == null || newRepDetail.getLine() == null || newRepDetail.getPosition() == null || newRepDetail.getTypeNo() == null) {
                messageDetail = super.showMessage(REQ_FIELD);
            } else {
                String[] inputs = {newRepDetail.getStartingString()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace("\"", "").replace("/", "").replace(":", "").replace("*", "").replace(".", "").replace("-", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    newRepDetail.setId(selectedRepTemp.getId());
                    repBo.editeReplanishmentTempletesDetSingle(newRepDetail, repBo.INSERT, 0, 0, 0);
                    messageDetail = super.showMessage(SUCC);
                    showAddDetail = true;
                    showConfirmDetail = false;
                } else {
                    messageDetail = "Please Enter Valid Data";
                }

            }
        } catch (Throwable ex) {
            messageDetail = super.showMessage(REC_EXIST);
        }
    }

    public void resetVars() {
        message = "";
        messageDetail = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
