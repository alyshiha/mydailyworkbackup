/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class atmremainingDTO implements atmremainingDTOInter, Serializable {
    private String atmapplicationid, name,group;
    private Integer remaining;

    @Override
    public String getAtmapplicationid() {
        return atmapplicationid;
    }

    @Override
    public void setAtmapplicationid(String atmapplicationid) {
        this.atmapplicationid = atmapplicationid;
    }

    @Override
    public String getGroup() {
        return group;
    }

    @Override
    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Integer getRemaining() {
        return remaining;
    }

    @Override
    public void setRemaining(Integer remaining) {
        this.remaining = remaining;
    }
    
}
