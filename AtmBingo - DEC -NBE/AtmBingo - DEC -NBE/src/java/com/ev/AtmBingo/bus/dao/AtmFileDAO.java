/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class AtmFileDAO extends BaseDAO implements AtmFileDAOInter {

    protected AtmFileDAO() {
        super();
        super.setTableName("atm_file");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileDTOInter uDTO = (AtmFileDTOInter) obj[0];
        uDTO.setId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ($id, '$name' , '$backupName', to_date('$loadingDate','dd.MM.yyyy hh24:mi:ss'), $template,"
                + " to_date('$finishLoadingDate','dd.MM.yyyy hh24:mi:ss'), $numberOfTransaction"
                + ", $numberOfDuplication, $type)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$template", "" + uDTO.getTemplate());
        insertStat = insertStat.replace("$name", "" + uDTO.getName());
        insertStat = insertStat.replace("$backupName", "" + uDTO.getBuckupName());
        insertStat = insertStat.replace("$loadingDate", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getLoadingDate()));
        insertStat = insertStat.replace("$finishLoadingDate", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getFinishLaodingDate()));
        insertStat = insertStat.replace("$numberOfTransaction", "" + uDTO.getNumberOfTransaction());
        insertStat = insertStat.replace("$numberOfDuplication", "" + uDTO.getNumberOfDuplication());
        insertStat = insertStat.replace("$type", "" + uDTO.getType());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileDTOInter uDTO = (AtmFileDTOInter) obj[0];
        String updateStat = "update $table set name = '$name', backup_name = '$buckupName', "
                + "loading_date = to_date('$loadingDate','dd.MM.yyyy hh24:mi:ss')"
                + ", template = $template, finish_loading_date = to_date('$finishLoadingDate','dd.MM.yyyy hh24:mi:ss')"
                + ", number_of_transactions = $numberOfTransaction, number_of_duplication = $numberOfDuplication"
                + ", type = $type"
                + "where id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$template", "" + uDTO.getTemplate());
        updateStat = updateStat.replace("$name", "" + uDTO.getName());
        updateStat = updateStat.replace("$backupName", "" + uDTO.getBuckupName());
        updateStat = updateStat.replace("$loadingDate", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getLoadingDate()));
        updateStat = updateStat.replace("$finishLoadingDate", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getFinishLaodingDate()));
        updateStat = updateStat.replace("$numberOfTransaction", "" + uDTO.getNumberOfTransaction());
        updateStat = updateStat.replace("$numberOfDuplication", "" + uDTO.getNumberOfDuplication());
        updateStat = updateStat.replace("$type", "" + uDTO.getType());
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileDTOInter uDTO = (AtmFileDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        AtmFileDTOInter uDTO = DTOFactory.createAtmFileDTO();
        while (rs.next()) {
            uDTO.setBuckupName(rs.getString("backup_name"));
            uDTO.setFinishLaodingDate(rs.getTimestamp("finish_loading_date"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLoadingDate(rs.getTimestamp("loading_date"));
            uDTO.setName(rs.getString("name"));
            uDTO.setNumberOfDuplication(rs.getInt("number_of_duplication"));
            uDTO.setNumberOfTransaction(rs.getInt("number_of_transactions"));
            uDTO.setTemplate(rs.getInt("template"));
            uDTO.setType(rs.getInt("type"));
        }
        super.postSelect(rs);

        return uDTO;
    }

    public Object findAll(Date loadingfrom, Date loadingto, int FileType) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where  loading_date between to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$col2','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')  $type order by loading_date";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(loadingfrom));
        selectStat = selectStat.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(loadingto));
        if (FileType == 0) {
            selectStat = selectStat.replace("$type", " ");
        } else {
            selectStat = selectStat.replace("$type", "and type = " + FileType);
        }
        ResultSet rs = executeQuery(selectStat);
        List<AtmFileDTOInter> uDTOL = new ArrayList<AtmFileDTOInter>();
        while (rs.next()) {
            AtmFileDTOInter uDTO = DTOFactory.createAtmFileDTO();
            uDTO.setBuckupName(rs.getString("backup_name"));
            uDTO.setFinishLaodingDate(rs.getTimestamp("finish_loading_date"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLoadingDate(rs.getTimestamp("loading_date"));
            uDTO.setName(rs.getString("name"));
            uDTO.setNumberOfDuplication(rs.getInt("number_of_duplication"));
            uDTO.setNumberOfTransaction(rs.getInt("number_of_transactions"));
            uDTO.setTemplate(rs.getInt("template"));
            uDTO.setType(rs.getInt("type"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByDate(Date loadingfrom, Date loadingto) throws Throwable {
        super.preSelect();

        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where  loading_date between to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and to_date(to_char(nvl(to_date('$col2','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') order by loading_date";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(loadingfrom));
        selectStat = selectStat.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(loadingto));
        
        ResultSet rs = executeQuery(selectStat);
        List<AtmFileDTOInter> uDTOL = new ArrayList<AtmFileDTOInter>();
        while (rs.next()) {
            AtmFileDTOInter uDTO = DTOFactory.createAtmFileDTO();
            uDTO.setBuckupName(rs.getString("backup_name"));
            uDTO.setFinishLaodingDate(rs.getTimestamp("finish_loading_date"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLoadingDate(rs.getTimestamp("loading_date"));
            uDTO.setName(rs.getString("name"));
            uDTO.setNumberOfDuplication(rs.getInt("number_of_duplication"));
            uDTO.setNumberOfTransaction(rs.getInt("number_of_transactions"));
            uDTO.setTemplate(rs.getInt("template"));
            uDTO.setType(rs.getInt("type"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
