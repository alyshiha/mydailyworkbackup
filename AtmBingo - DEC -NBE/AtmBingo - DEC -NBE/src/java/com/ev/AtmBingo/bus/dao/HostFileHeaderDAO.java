/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.HostFileHeaderDTOInter;
import com.ev.AtmBingo.bus.dto.HostFileTemplateDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class HostFileHeaderDAO extends BaseDAO implements HostFileHeaderDAOInter {

    protected HostFileHeaderDAO() {
        super();
        super.setTableName("host_file_template_header");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        HostFileHeaderDTOInter uDTO = (HostFileHeaderDTOInter) obj[0];
        uDTO.setId(super.generateSequence("switch_file_template_hdr"));
        String insertStat = "insert into $table values ($id, $template , $columnId, $position, '$format', '$formt2',"
                + " $dataType, $length)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$template", "" + uDTO.getTemplate());
        insertStat = insertStat.replace("$columnId", "" + uDTO.getColumnId());
        insertStat = insertStat.replace("$position", "" + uDTO.getPosition());
        insertStat = insertStat.replace("$format", "" + uDTO.getFormat());
        insertStat = insertStat.replace("$formt2", "" + uDTO.getFormat2());
        insertStat = insertStat.replace("$dataType", "" + uDTO.getDataType());
        insertStat = insertStat.replace("$length", "" + uDTO.getLength());
        super.executeUpdate(insertStat);
        HostFileTemplateDAOInter hftDAO = DAOFactory.createHostFileTemplateDAO(null);
        HostFileTemplateDTOInter hftDTO = (HostFileTemplateDTOInter) hftDAO.find(uDTO.getTemplate());
        String action = "Add a new " + hftDTO.getName() + " Host template's header";
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        HostFileHeaderDTOInter uDTO = (HostFileHeaderDTOInter) obj[0];
        String updateStat = "update $table set template = $template, column_id = $columnId, position = $position"
                + ", format = '$format', format2 = '$formt2', data_type = $dataType, length = $length"
                + "where id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$template", "" + uDTO.getTemplate());
        updateStat = updateStat.replace("$columnId", "" + uDTO.getColumnId());
        updateStat = updateStat.replace("$position", "" + uDTO.getPosition());
        updateStat = updateStat.replace("$format", "" + uDTO.getFormat());
        updateStat = updateStat.replace("$formt2", "" + uDTO.getFormat2());
        updateStat = updateStat.replace("$dataType", "" + uDTO.getDataType());
        updateStat = updateStat.replace("$length", "" + uDTO.getLength());
        super.executeUpdate(updateStat);
        HostFileTemplateDAOInter hftDAO = DAOFactory.createHostFileTemplateDAO(null);
        HostFileTemplateDTOInter hftDTO = (HostFileTemplateDTOInter) hftDAO.find(uDTO.getTemplate());
        String action = "Update " + hftDTO.getName() + " Host template's header";
        super.postUpdate(action, false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        HostFileHeaderDTOInter uDTO = (HostFileHeaderDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        HostFileTemplateDAOInter hftDAO = DAOFactory.createHostFileTemplateDAO(null);
        HostFileTemplateDTOInter hftDTO = (HostFileTemplateDTOInter) hftDAO.find(uDTO.getTemplate());
        String action = "Delete " + hftDTO.getName() + " Host template's header";
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        HostFileHeaderDTOInter uDTO = DTOFactory.createHostFileHeaderDTO();
        while (rs.next()) {
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getInt("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getInt("length"));
            uDTO.setPosition(rs.getInt("position"));
            uDTO.setTemplate(rs.getInt("template"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<HostFileHeaderDTOInter> uDTOL = new ArrayList<HostFileHeaderDTOInter>();
        while (rs.next()) {
            HostFileHeaderDTOInter uDTO = DTOFactory.createHostFileHeaderDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getInt("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getInt("length"));
            uDTO.setPosition(rs.getInt("position"));
            uDTO.setTemplate(rs.getInt("template"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByTemplate(HostFileTemplateDTOInter aftDTO) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where template = $template";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$template", "" + aftDTO.getId());
        ResultSet rs = executeQuery(selectStat);
        List<HostFileHeaderDTOInter> uDTOL = new ArrayList<HostFileHeaderDTOInter>();
        while (rs.next()) {
            HostFileHeaderDTOInter uDTO = DTOFactory.createHostFileHeaderDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getInt("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getInt("length"));
            uDTO.setPosition(rs.getInt("position"));
            uDTO.setTemplate(rs.getInt("template"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
