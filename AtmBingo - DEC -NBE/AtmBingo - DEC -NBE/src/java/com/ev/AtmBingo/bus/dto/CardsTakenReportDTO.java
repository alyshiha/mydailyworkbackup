/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;
import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
/**
 *
 * @author Aly
 */
public class CardsTakenReportDTO  extends BaseDTO implements CardsTakenReportDTOInter, Serializable{
 private String atmaplication;
    private String cardonfile;
    private String caronatm;   

    @Override
    public String getAtmaplication() {
        return atmaplication;
    }

    @Override
    public void setAtmaplication(String atmaplication) {
        this.atmaplication = atmaplication;
    }

    @Override
    public String getCardonfile() {
        return cardonfile;
    }

    @Override
    public void setCardonfile(String cardonfile) {
        this.cardonfile = cardonfile;
    }

    @Override
    public String getCaronatm() {
        return caronatm;
    }

    @Override
    public void setCaronatm(String caronatm) {
        this.caronatm = caronatm;
    }
    
}
