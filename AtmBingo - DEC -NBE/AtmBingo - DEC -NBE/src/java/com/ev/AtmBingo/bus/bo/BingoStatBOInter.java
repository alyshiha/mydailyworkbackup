/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

/**
 *
 * @author Administrator
 */
public interface BingoStatBOInter {

    Object GetTransactionDetails(Object... obj) throws Throwable;
    
    Object GetTransactionDetailsbyatmexcel(Object... obj) throws Throwable;
    
    Object GetTransactionDetailsexcel(Object... obj) throws Throwable ;

    Object GetTransactionStat(Object... obj) throws Throwable;

    Object GetTransactionStatcount(Object... obj) throws Throwable;
    
    Object GetTransactionDetailsbyatm(Object... obj) throws Throwable ;
}
