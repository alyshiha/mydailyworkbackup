/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.DisputeRulesDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface DisputeRulesBOInter extends BaseBOInter {

    String genarateValueString(Integer[] values);

    String genarateValueDate(Date d1,Date d2);
 
     Object createFormula(int column, String opt, String value, DisputeRulesDTOInter vrDTO,Integer Type) throws Throwable ;

    Object getColumnValues(int columnId)throws Throwable;

    String getColumnName(int columnId) throws Throwable;

    Object getDisputeRules()throws Throwable;

    Object editDisputeRules(Object obj, int operation) throws Throwable;

    Object getColumns() throws Throwable;

    Object validateFormula(DisputeRulesDTOInter drDTO,Integer type)throws Throwable;

}
