/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import DBCONN.Session;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDetailDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class AtmFileTemplateDetailDAO extends BaseDAO implements AtmFileTemplateDetailDAOInter {

    protected AtmFileTemplateDetailDAO() {
        super();
        super.setTableName("atm_file_template_detail");
    }

    public String ValidateNullDeleteTemplateAllHeader(Object... obj) {
        AtmFileTemplateDTOInter RecordToInsert = (AtmFileTemplateDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getId() == 0) {
            Validate = Validate + " Feild Template id is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object deleterecorddetailTemplate(Object... obj) throws Throwable {
        super.preUpdate();
        AtmFileTemplateDTOInter RecordToDelete = (AtmFileTemplateDTOInter) obj[0];
        String TName = (String) obj[1];
        String msg = ValidateNullDeleteTemplateAllHeader(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  template = $id";
            deleteStat = deleteStat.replace("$table", "" + TName + "_DETAIL");
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getId());
            msg = super.executeUpdate(deleteStat).toString();
            msg = msg + " Detail Record Has Been deleted";
            super.postUpdate("Delete Record In Table " + TName + "_DETAIL", false);
        }
        return msg;
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileTemplateDetailDTOInter uDTO = (AtmFileTemplateDetailDTOInter) obj[0];
        String TName = (String) obj[1];
        if (TName.toUpperCase().equals("SWITCH_FILE_TEMPLATE_DETAIL")) {
            uDTO.setId(super.generateSequence("SWITCH_FILE_TEMPLATE_DTL"));
        } else if (TName.toUpperCase().equals("HOST_FILE_TEMPLATE_DETAIL")) {
            uDTO.setId(super.generateSequence("HOST_FILE_TEMPLATE_DTL"));
        } else {
            uDTO.setId(super.generateSequence(TName));
        }
        String insertStat = "insert into $table   (id, column_id, template, position, line_number, format, format2, data_type, length, load_when_mapped, length_dir, starting_position, tag_string, mandatory, negative_amount_flag, add_decimal, decimal_pos)"
                + "values ($id,   $columnId,$template, $position,$lineNumber, '$format', '$formt2',"
                + " $dataType, $length, $loadWhenMapped, $lngthDir, $startPos,'$tagstring',$mandatory,$negativeflag,$decimalpoint,'$decimalpointposition')";
        insertStat = insertStat.replace("$table", "" + TName);
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$template", "" + uDTO.getTwmplate());
        insertStat = insertStat.replace("$columnId", "" + uDTO.getColumnId());
        insertStat = insertStat.replace("$position", "" + uDTO.getPosition());
        insertStat = insertStat.replace("$format", "" + uDTO.getFormat());
        insertStat = insertStat.replace("$tagstring", "" + uDTO.getTagstring());
        //$decimalpoint,'$decimalpointposition'
        Integer temp;
        if (uDTO.getDecimalpoint() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        insertStat = insertStat.replace("$decimalpointposition", "" + uDTO.getDecimalpointposition());
        insertStat = insertStat.replace("$decimalpoint", "" + temp);

        if (uDTO.getMandatory() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        insertStat = insertStat.replace("$mandatory", "" + temp);
        if (uDTO.getNegativeflag() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        insertStat = insertStat.replace("$negativeflag", "" + temp);
        insertStat = insertStat.replace("$formt2", "" + uDTO.getFormat2());
        insertStat = insertStat.replace("$dataType", "" + uDTO.getDataType());
        insertStat = insertStat.replace("$length", "" + uDTO.getLength());
        insertStat = insertStat.replace("$lineNumber", "" + uDTO.getLineNumber());
        insertStat = insertStat.replace("$loadWhenMapped", "" + uDTO.getLoadWhenMapped());
        insertStat = insertStat.replace("$lngthDir", "" + uDTO.getLengthDir());
        insertStat = insertStat.replace("$startPos", "" + uDTO.getStartingPosition());
        Integer msg = super.executeUpdate(insertStat);
        String action = "Add detail to " + TName;
        super.postUpdate(action, false);
        return msg;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileTemplateDetailDTOInter uDTO = (AtmFileTemplateDetailDTOInter) obj[0];
        String TName = (String) obj[1];
        String updateStat = "update $table set  column_id = $columnId, position = $position"
                + ", format = '$format', format2 = '$formt2', data_type = $dataType, length = $length, line_number = $lineNumber"
                + ", load_when_mapped = $loadWhenMapped, length_dir = $lngthDir, starting_position = $startPos"
                + " ,TAG_STRING = '$tagstring',MANDATORY = $mandatory ,NEGATIVE_AMOUNT_FLAG = $negativeflag ,ADD_DECIMAL=$decimalpoint,DECIMAL_POS='$decimalpointposition'"
                + " where id = $id";
        updateStat = updateStat.replace("$table", "" + TName);
        updateStat = updateStat.replace("$tagstring", "" + uDTO.getTagstring());
        Integer temp;

        if (uDTO.getDecimalpoint() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        updateStat = updateStat.replace("$decimalpointposition", "" + uDTO.getDecimalpointposition());
        updateStat = updateStat.replace("$decimalpoint", "" + temp);

        if (uDTO.getMandatory() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        updateStat = updateStat.replace("$mandatory", "" + temp);
        if (uDTO.getNegativeflag() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        updateStat = updateStat.replace("$negativeflag", "" + temp);

        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$template", "" + uDTO.getTwmplate());
        updateStat = updateStat.replace("$columnId", "" + uDTO.getColumnId());
        updateStat = updateStat.replace("$position", "" + uDTO.getPosition());
        updateStat = updateStat.replace("$format", "" + uDTO.getFormat());
        updateStat = updateStat.replace("$formt2", "" + uDTO.getFormat2());
        updateStat = updateStat.replace("$dataType", "" + uDTO.getDataType());
        updateStat = updateStat.replace("$length", "" + uDTO.getLength());
        updateStat = updateStat.replace("$lineNumber", "" + uDTO.getLineNumber());
        updateStat = updateStat.replace("$loadWhenMapped", "" + uDTO.getLoadWhenMapped());
        updateStat = updateStat.replace("$lngthDir", "" + uDTO.getLengthDir());
        updateStat = updateStat.replace("$startPos", "" + uDTO.getStartingPosition());
        Integer msg = super.executeUpdate(updateStat);
        //AtmFileTemplateDTOInter aftDTO = (AtmFileTemplateDTOInter) aftDAO.find(uDTO.getTwmplate(),TName);
        String action = "Update Record detail In " + TName;
        super.postUpdate(action, false);
        return msg;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileTemplateDetailDTOInter uDTO = (AtmFileTemplateDetailDTOInter) obj[0];
        String TName = (String) obj[1];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + TName);
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        AtmFileTemplateDAOInter aftDAO = DAOFactory.createAtmFileTemplateDAO(null);
        AtmFileTemplateDTOInter aftDTO = (AtmFileTemplateDTOInter) aftDAO.find(uDTO.getTwmplate(), TName);
        String action = "Delete " + aftDTO.getName() + " template's  detail template";
        super.postUpdate(action, false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        AtmFileTemplateDetailDTOInter uDTO = DTOFactory.createAtmFileTemplateDetailDTO();
        while (rs.next()) {
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getBigDecimal("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setTagstring(rs.getString("TAG_STRING"));

            Integer temp3 = rs.getInt("ADD_DECIMAL");
            if (temp3 == 1) {
                uDTO.setDecimalpoint(Boolean.TRUE);
            } else {
                uDTO.setDecimalpoint(Boolean.FALSE);
            }
            uDTO.setDecimalpointposition(rs.getString("DECIMAL_POS"));

            Integer temp = rs.getInt("MANDATORY");
            if (temp == 1) {
                uDTO.setMandatory(Boolean.TRUE);
            } else {
                uDTO.setMandatory(Boolean.FALSE);
            }
            Integer temp2 = rs.getInt("NEGATIVE_AMOUNT_FLAG");
            if (temp2 == 1) {
                uDTO.setNegativeflag(Boolean.TRUE);
            } else {
                uDTO.setNegativeflag(Boolean.FALSE);
            }
            uDTO.setLength(rs.getBigDecimal("length"));
            uDTO.setLengthDir(rs.getBigDecimal("length_dir"));
            uDTO.setLineNumber(rs.getBigDecimal("line_number"));
            uDTO.setLoadWhenMapped(rs.getInt("load_when_mapped"));
            uDTO.setPosition(rs.getBigDecimal("position"));
            uDTO.setTwmplate(rs.getInt("template"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<AtmFileTemplateDetailDTOInter> uDTOL = new ArrayList<AtmFileTemplateDetailDTOInter>();
        while (rs.next()) {
            AtmFileTemplateDetailDTOInter uDTO = DTOFactory.createAtmFileTemplateDetailDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getBigDecimal("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getBigDecimal("length"));
            uDTO.setLengthDir(rs.getBigDecimal("length_dir"));
            uDTO.setLineNumber(rs.getBigDecimal("line_number"));
            uDTO.setLoadWhenMapped(rs.getInt("load_when_mapped"));
            uDTO.setPosition(rs.getBigDecimal("position"));
            uDTO.setTagstring(rs.getString("TAG_STRING"));

            Integer temp3 = rs.getInt("ADD_DECIMAL");
            if (temp3 == 1) {
                uDTO.setDecimalpoint(Boolean.TRUE);
            } else {
                uDTO.setDecimalpoint(Boolean.FALSE);
            }
            uDTO.setDecimalpointposition(rs.getString("DECIMAL_POS"));
            Integer temp = rs.getInt("MANDATORY");
            if (temp == 1) {
                uDTO.setMandatory(Boolean.TRUE);
            } else {
                uDTO.setMandatory(Boolean.FALSE);
            }
            Integer temp2 = rs.getInt("NEGATIVE_AMOUNT_FLAG");
            if (temp2 == 1) {
                uDTO.setNegativeflag(true);
            } else {
                uDTO.setNegativeflag(false);
            }
            uDTO.setTwmplate(rs.getInt("template"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByTemplate(AtmFileTemplateDTOInter aftDTO, String TName) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where template = $template";
        if (TName.toUpperCase().indexOf("_DETAIL") == -1) {
            TName = TName + "_DETAIL";
        }
        selectStat = selectStat.replace("$table", "" + TName);
        selectStat = selectStat.replace("$template", "" + aftDTO.getId());
        ResultSet rs = executeQuery(selectStat);
        List<AtmFileTemplateDetailDTOInter> uDTOL = new ArrayList<AtmFileTemplateDetailDTOInter>();
        while (rs.next()) {
            AtmFileTemplateDetailDTOInter uDTO = DTOFactory.createAtmFileTemplateDetailDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getBigDecimal("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getBigDecimal("length"));
            uDTO.setLengthDir(rs.getBigDecimal("length_dir"));
            uDTO.setLineNumber(rs.getBigDecimal("line_number"));
            uDTO.setLoadWhenMapped(rs.getInt("load_when_mapped"));
            uDTO.setTagstring(rs.getString("TAG_STRING"));
            Integer temp = rs.getInt("MANDATORY");
            if (temp == 1) {
                uDTO.setMandatory(Boolean.TRUE);
            } else {
                uDTO.setMandatory(Boolean.FALSE);
            }
            Integer temp2 = rs.getInt("NEGATIVE_AMOUNT_FLAG");
            if (temp2 == 1) {
                uDTO.setNegativeflag(Boolean.TRUE);
            } else {
                uDTO.setNegativeflag(Boolean.FALSE);
            }
            uDTO.setPosition(rs.getBigDecimal("position"));
            uDTO.setTwmplate(rs.getInt("template"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));

            Integer temp3 = rs.getInt("ADD_DECIMAL");
            if (temp3 == 1) {
                uDTO.setDecimalpoint(Boolean.TRUE);
            } else {
                uDTO.setDecimalpoint(Boolean.FALSE);
            }
            uDTO.setDecimalpointposition(rs.getString("DECIMAL_POS"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByColumnId(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String TName = (String) obj[1];
        String selectStat = "select * from $table where column_id = $id";
        selectStat = selectStat.replace("$table", "" + TName);
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        AtmFileTemplateDetailDTOInter uDTO = DTOFactory.createAtmFileTemplateDetailDTO();
        while (rs.next()) {
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getBigDecimal("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getBigDecimal("length"));
            uDTO.setLengthDir(rs.getBigDecimal("length_dir"));
            uDTO.setLineNumber(rs.getBigDecimal("line_number"));
            uDTO.setTagstring(rs.getString("TAG_STRING"));

            Integer temp3 = rs.getInt("ADD_DECIMAL");
            if (temp3 == 1) {
                uDTO.setDecimalpoint(Boolean.TRUE);
            } else {
                uDTO.setDecimalpoint(Boolean.FALSE);
            }
            uDTO.setDecimalpointposition(rs.getString("DECIMAL_POS"));
            Integer temp = rs.getInt("MANDATORY");
            if (temp == 1) {
                uDTO.setMandatory(Boolean.TRUE);
            } else {
                uDTO.setMandatory(Boolean.FALSE);
            }
            Integer temp2 = rs.getInt("NEGATIVE_AMOUNT_FLAG");
            if (temp == 1) {
                uDTO.setNegativeflag(Boolean.TRUE);
            } else {
                uDTO.setNegativeflag(Boolean.FALSE);
            }

            uDTO.setLoadWhenMapped(rs.getInt("load_when_mapped"));
            uDTO.setPosition(rs.getBigDecimal("position"));
            uDTO.setTwmplate(rs.getInt("template"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findByTemplateAndColumnId(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer templateId = (Integer) obj[0];
        Integer columnId = (Integer) obj[1];
        String TName = (String) obj[2];
        String selectStat = "select * from $table where column_id = $ColumnId and template = $templateId";
        selectStat = selectStat.replace("$table", "" + TName + "_Detail");
        selectStat = selectStat.replace("$ColumnId", "" + columnId);
        selectStat = selectStat.replace("$templateId", "" + templateId);

        ResultSet rs = executeQuery(selectStat);
        boolean exist = false;
        while (rs.next()) {
            exist = true;
        }
        super.postSelect(rs);
        return exist;
    }

    public String ValidateNull(Object... obj) {
        AtmFileTemplateDetailDTOInter RecordToInsert = (AtmFileTemplateDetailDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getTwmplate() == 0) {
            Validate = Validate + " Feild template is a requiered field";
        }
        if (RecordToInsert.getColumnId() == 0) {
            Validate = Validate + " Feild columnid is a requiered field";
        }
        if (RecordToInsert.getPosition() == null) {
            Validate = Validate + " Feild position is a requiered field";
        }
        if (RecordToInsert.getDataType() == null) {
            Validate = Validate + " Feild datatype is a requiered field";
        }
        if (RecordToInsert.getLineNumber() == null) {
            Validate = Validate + " Feild linenumber is a requiered field";
        }
        if (RecordToInsert.getLoadWhenMapped() == 0) {
            Validate = Validate + " Feild loadwhenmapped is a requiered field";
        }
        if (RecordToInsert.getLengthDir() == null) {
            Validate = Validate + " Feild lengthdir is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public String CheckPresent(Object... obj) throws Throwable {

        AtmFileTemplateDetailDTOInter RecordToSelect = (AtmFileTemplateDetailDTOInter) obj[0];
        String TName = (String) obj[1];
        Connection Conn = (Connection) obj[2];
        String selectStat = "Select ID From " + TName + "  where  ID= ?";
        PreparedStatement prpStatementCheck = Conn.prepareStatement(selectStat);
        prpStatementCheck.setInt(1, RecordToSelect.getId());
        ResultSet rsCheck = prpStatementCheck.executeQuery();
        if (rsCheck.next()) {
            return "Update";
        } else {
            return "Insert";
        }

    }

    public String CreateMessage(Integer UpdateTotal, Integer InsertTotal, Integer ValidationMsg, Integer Idel) {
        String Msg = "No Record Found";
        if (UpdateTotal > 0) {
            Msg = Msg.replace("No Record Found", "");
            Msg = Msg + UpdateTotal.toString() + " Record Has Been Updated ";
        }
        if (InsertTotal > 0) {
            Msg = Msg.replace("No Record Found", "");
            Msg = Msg + InsertTotal.toString() + " Record Has Been Inserted ";
        }
        if (Idel > 0) {
            Msg = Msg.replace("No Record Found", "");
            Msg = Msg + Idel.toString() + " Idel Record";
        }
        if (ValidationMsg > 0) {
            Msg = Msg.replace("No Record Found", "");
            Msg = Msg + ValidationMsg.toString() + " Record Is InValid,Please Insert Requierd Fields";
        }
        return Msg;
    }

    public Integer getLoggedInUser() {
        try {
            Session sessionutil = new Session();
            UsersDTOInter currentUser = sessionutil.GetUserLogging();
            Integer uDTO = currentUser.getUserId();
            return uDTO;
        } catch (Exception ex) {
            return 0;
        } catch (Throwable ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Object SaveAllDetail(List<AtmFileTemplateDetailDTOInter> filetemplatedetailList, String TName, Integer templateid) throws Throwable {
        String Message = "";
        Integer UpdateTotal = 0, InsertTotal = 0, ValidationMsg = 0, Idel = 0;
        Connection Conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
        for (AtmFileTemplateDetailDTOInter Record : filetemplatedetailList) {
            Record.setTwmplate(templateid);
            if (ValidateNull(Record).equals("Validate")) {
                if (CheckPresent(Record, TName, Conn).contains("Update")) {
                    UpdateTotal = (Integer) update(Record, TName) + UpdateTotal;
                } else if (CheckPresent(Record, TName, Conn).contains("Insert")) {
                    InsertTotal = (Integer) insert(Record, TName) + InsertTotal;
                }
            } else {
                if (ValidateNull(Record).equals(" Feild position is a requiered field Feild linenumber is a requiered field")) {
                    Idel = Idel + 1;
                } else {
                    ValidationMsg = ValidationMsg + 1;
                }
            }
        }
        Message = CreateMessage(UpdateTotal, InsertTotal, ValidationMsg, Idel);
        return Message;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
