/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dao.AVGDailyDispenseDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface RepStatBORepInter {
 String runReportATMAmount(String dateFrom, String dateTo, String user,String cust,String ATMID,String ATMGroup) throws Throwable;
 List<AVGDailyDispenseDTOInter> runReportATMAmountexcel(String dateFrom, String dateTo, String user, String cust, String ATMID, String ATMGroup) throws Throwable ;
 String runReportaverageRepCycle(String dateFrom, String dateTo, String user, String cust, String ATMID,String ATMGroup) throws Throwable;
    List<AVGDailyDispenseDTOInter> runReportaverageRepCycleexcel(String dateFrom, String dateTo, String user, String cust, String ATMID,String ATMGroup) throws Throwable ;
 String runReportDailydispense(String dateFrom, String dateTo, String user, String cust, String ATMID,String ATMGroup) throws Throwable;
 List<AVGDailyDispenseDTOInter> runReportDailydispenseexcel(String dateFrom, String dateTo, String user, String cust, String ATMID, String ATMGroup) throws Throwable ;
 String runReportRepGaps(Date dateFrom, Date dateTo, String user, String cust, String ATMID,String ATMGroup) throws Throwable;
}

