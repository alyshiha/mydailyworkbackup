/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class DeleteTransactionsDataDTO extends BaseDTO implements DeleteTransactionsDataDTOInter, Serializable{
private int Fileid;
private int ATMid;
private Date SearchDateFrom;
private Date SearchDateTo;
private int userId;
private Date StartDate;

    public int getATMid() {
        return ATMid;
    }

    public void setATMid(int ATMid) {
        this.ATMid = ATMid;
    }

    public int getFileid() {
        return Fileid;
    }

    public void setFileid(int Fileid) {
        this.Fileid = Fileid;
    }

    public Date getSearchDateFrom() {
        return SearchDateFrom;
    }

    public void setSearchDateFrom(Date SearchDateFrom) {
        this.SearchDateFrom = SearchDateFrom;
    }

    public Date getSearchDateTo() {
        return SearchDateTo;
    }

    public void setSearchDateTo(Date SearchDateTo) {
        this.SearchDateTo = SearchDateTo;
    }

    public Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(Date StartDate) {
        this.StartDate = StartDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
