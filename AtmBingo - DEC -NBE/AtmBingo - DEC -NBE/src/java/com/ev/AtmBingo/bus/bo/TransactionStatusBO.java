/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.TransactionStatusDAOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TransactionStatusBO extends BaseBO implements TransactionStatusBOInter,Serializable{

    protected TransactionStatusBO() {
        super();
    }

    public Object editeTransactionStatus(Object obj, int operation)throws Throwable{
        TransactionStatusDAOInter tsDAO = DAOFactory.createTransactionStatusDAO(null);
        TransactionStatusDTOInter tsDTO = (TransactionStatusDTOInter)obj;

        switch(operation){
            case INSERT:
                tsDAO.insert(tsDTO);
                return "Insert Done";
            case UPDATE:
                tsDAO.update(tsDTO);
                return "Update Done";
            case DELETE:
                tsDAO.delete(tsDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }

    public Object getTransactionStatus()throws Throwable{
        TransactionStatusDAOInter tsDAO = DAOFactory.createTransactionStatusDAO(null);
        List<TransactionStatusDTOInter> tsDTOL = (List<TransactionStatusDTOInter>)tsDAO.findAll();
        return tsDTOL;
    }

}
