/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class ReplanishmentDetailDTO extends BaseDTO implements ReplanishmentDetailDTOInter, Serializable{

    private int id;
    private int cassete;
    private int casseteValue;
    private int totalAmount;
    private int currency;
    private int remaining;
    private int remainingAmount;
     private String cassetename;

    public String getCassetename() {
        return cassetename;
    }

    public void setCassetename(String cassetename) {
        this.cassetename = cassetename;
    }

    public int getCassete() {
        return cassete;
    }

    public void setCassete(int cassete) {
        this.cassete = cassete;
    }

    public int getCasseteValue() {
        return casseteValue;
    }

    public void setCasseteValue(int casseteValue) {
        this.casseteValue = casseteValue;
    }

    public int getCurrency() {
        return currency;
    }

    public void setCurrency(int currency) {
        this.currency = currency;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRemaining() {
        return remaining;
    }

    public void setRemaining(int remaining) {
        this.remaining = remaining;
    }

    public int getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(int remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }
    
    protected ReplanishmentDetailDTO() {
        super();
    }

}
