/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.client.BaseBean;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author Administrator
 */
public class SessionCounter extends BaseBean implements HttpSessionListener, Serializable {

    public void sessionCreated(HttpSessionEvent se) {

    }

    public void sessionDestroyed(HttpSessionEvent se) {
        Enumeration attributeNames = se.getSession().getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            if (!"Trial".equals(sAttribute)) {
                se.getSession().removeAttribute(sAttribute);
            } else {
                if (se.getSession().getAttribute("Trial") == null) {
                    se.getSession().removeAttribute(sAttribute);
                }
            }
        }
        DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Calendar calobj = Calendar.getInstance();
        System.out.println("Session: " + df.format(calobj.getTime()));

        se.getSession().invalidate();

    }

}
