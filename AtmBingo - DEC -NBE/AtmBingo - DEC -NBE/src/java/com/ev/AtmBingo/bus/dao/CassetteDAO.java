/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class CassetteDAO extends BaseDAO implements CassetteDAOInter {

    protected CassetteDAO() {
        super();
        super.setTableName("cassette");
    }
    public Object findCassetteGroup() throws Throwable{
        super.preSelect();
        String selectStat = "select * from cassette_group";
        ResultSet rs = executeQuery(selectStat);
        CassetteDTOInter uDTO = DTOFactory.createCassetteDTO();
        while (rs.next()) {
            uDTO.setId(rs.getInt("id"));
            uDTO.setName(rs.getString("name"));
        }
        super.postSelect(rs);
        return uDTO;
    }
    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        CassetteDTOInter uDTO = (CassetteDTOInter) obj[0];
        uDTO.setId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ($id, '$name' , '$description',$currency, $amount,$seq)";

        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$description", "" + uDTO.getDescription());
        insertStat = insertStat.replace("$name", "" + uDTO.getName());
        insertStat = insertStat.replace("$currency", "" + uDTO.getCurrency());
        insertStat = insertStat.replace("$amount", "" + uDTO.getAmount());
        insertStat = insertStat.replace("$seq", "" + uDTO.getSeq());
        super.executeUpdate(insertStat);
        String action = "Add a new cassete with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        CassetteDTOInter uDTO = (CassetteDTOInter) obj[0];
        String updateStat = "update $table set name = '$name', description = '$description', "
                + "currency = $currency"
                + ", amount = $amount, seq = $seq"
                + " where id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$description", "" + uDTO.getDescription());
        updateStat = updateStat.replace("$name", "" + uDTO.getName());
        updateStat = updateStat.replace("$currency", "" + uDTO.getCurrency());
        updateStat = updateStat.replace("$amount", "" + uDTO.getAmount());
        updateStat = updateStat.replace("$seq", "" + uDTO.getSeq());
        super.executeUpdate(updateStat);
        String action = "Update cassete with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        CassetteDTOInter uDTO = (CassetteDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        String action = "Delete cassete with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        CassetteDTOInter uDTO = DTOFactory.createCassetteDTO();
        while (rs.next()) {
            uDTO.setAmount(rs.getInt("amount"));
            uDTO.setCurrency(rs.getInt("currency"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setDescription(rs.getString("description"));
            uDTO.setName(rs.getString("name"));
            uDTO.setSeq(rs.getInt("seq"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<CassetteDTOInter> uDTOL = new ArrayList<CassetteDTOInter>();
        while (rs.next()) {
            CassetteDTOInter uDTO = DTOFactory.createCassetteDTO();
            uDTO.setAmount(rs.getInt("amount"));
            uDTO.setCurrency(rs.getInt("currency"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setDescription(rs.getString("description"));
            uDTO.setName(rs.getString("name"));
            uDTO.setSeq(rs.getInt("seq"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
