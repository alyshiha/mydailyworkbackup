/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.SystemTableDTOInter;

/**
 *
 * @author Administrator
 */
public interface SystemParametersBOInter extends BaseBOInter{

    Object edtiteSystemTable(SystemTableDTOInter stDTO, String oldParameterName, int operation) throws Throwable;

    Object getSystemTable() throws Throwable;

}
