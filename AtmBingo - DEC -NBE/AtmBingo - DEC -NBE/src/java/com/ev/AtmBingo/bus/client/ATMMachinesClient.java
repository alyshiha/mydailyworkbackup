/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.AtmDefinitionBOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class ATMMachinesClient extends BaseBean  implements Serializable{

    private AtmDefinitionBOInter atmObject;
    private List<CassetteDTOInter> CassetteGroup;
    private List<AtmMachineDTOInter> atmFileList;
    private AtmMachineDTOInter atmAttrDel;
    private AtmMachineDTOInter atmAttrUp;
    private List<AtmGroupDTOInter> atmGroup;

    private List<AtmMachineTypeDTOInter> atmTypes;
    private Integer atmGroupint, listSize;
    private String message, ATMGroupCount;
    private Integer atmTypeint;

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public String getATMGroupCount() {
        return ATMGroupCount;
    }

    public void setATMGroupCount(String ATMGroupCount) {
        this.ATMGroupCount = ATMGroupCount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AtmMachineDTOInter getAtmAttrUp() {
        return atmAttrUp;
    }

    public void setAtmAttrUp(AtmMachineDTOInter atmAttrUp) {
        this.atmAttrUp = atmAttrUp;
    }

    public Integer getAtmTypeint() {
        return atmTypeint;
    }

    public void setAtmTypeint(Integer atmTypeint) {
        this.atmTypeint = atmTypeint;
    }

    public List<AtmMachineTypeDTOInter> getAtmTypes() {
        return atmTypes;
    }

    public void setAtmTypes(List<AtmMachineTypeDTOInter> atmTypes) {
        this.atmTypes = atmTypes;
    }

    public List<AtmGroupDTOInter> getAtmGroup() {
        return atmGroup;
    }

    public void setAtmGroup(List<AtmGroupDTOInter> atmGroup) {
        this.atmGroup = atmGroup;
    }

    public List<CassetteDTOInter> getCassetteGroup() {
        return CassetteGroup;
    }

    public void setCassetteGroup(List<CassetteDTOInter> CassetteGroup) {
        this.CassetteGroup = CassetteGroup;
    }

    public Integer getAtmGroupint() {
        return atmGroupint;
    }

    public void setAtmGroupint(Integer atmGroupint) {
        this.atmGroupint = atmGroupint;
    }

    public AtmMachineDTOInter getAtmAttrDel() {
        return atmAttrDel;
    }

    public void setAtmAttr(AtmMachineDTOInter atmAttrDel) {
        this.atmAttrDel = atmAttrDel;
    }

    public List<AtmMachineDTOInter> getAtmFileList() {
        return atmFileList;
    }

    public void setAtmFileList(List<AtmMachineDTOInter> atmFileList) {
        this.atmFileList = atmFileList;
    }

    /**
     * Creates a new instance of ATMMachinesClient
     */
    public ATMMachinesClient() throws Throwable {
        super();
        GetAccess();
        
        FacesContext context = FacesContext.getCurrentInstance();
       
        
        Session sessionutil = new Session();
        UsersDTOInter userHolded = sessionutil.GetUserLogging();
        atmObject = BOFactory.createAtmDefinitionBO(null);
        ATMGroupCount = (String) atmObject.getGroupCount();
        atmFileList = (List<AtmMachineDTOInter>) atmObject.getAtmMachines(userHolded);
        atmGroup = (List<AtmGroupDTOInter>) atmObject.getAtmGroups();
        atmTypes = (List<AtmMachineTypeDTOInter>) atmObject.getAtmMachineType();
        CassetteGroup = (List<CassetteDTOInter>) atmObject.getAtmMachineType();
        listSize = atmFileList.size();
          
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void fillUpdate(ActionEvent e) throws Throwable {
        atmAttrUp = (AtmMachineDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();

    }

    public void updateRecord() {
        try {
            if (atmAttrUp.getName().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                String[] inputs = {atmAttrUp.getApplicationId(), atmAttrUp.getApplicationId2(), atmAttrUp.getLocation(), atmAttrUp.getName(), atmAttrUp.getSerialNo(), atmAttrUp.getUnitNumber(), "" + atmAttrUp.getGroupId(), "" + atmAttrUp.getId(), "" + atmAttrUp.getMachineType()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    if(input.length()>100){message = "value too long";return;}
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    atmObject.editeAtmMachine(atmAttrUp, atmObject.UPDATE);
                    message = super.showMessage(SUCC);
                } else {
                    message = "Please Enter Valid Data";
                }
            }
        } catch (Throwable ex) {
            if (ex.getMessage().contains("-00001")) {
                message = "Enter Unique Name";
            } else {
                message = ex.getMessage();
            }
        }
    }

    public void resetVars() {
        message = "";
    }
      public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }
}
