/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class CurrencyMasterDTO extends BaseDTO implements Serializable, CurrencyMasterDTOInter{

    private int id;
    private String name;
    private String symbol;
private  Boolean defcurr;

    public Boolean getDefcurr() {
        return defcurr;
    }

    public void setDefcurr(Boolean defcurr) {
        this.defcurr = defcurr;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
    
    protected CurrencyMasterDTO() {
        super();
    }

}
