/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.TimeShiftDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TimeShiftDAO extends BaseDAO implements TimeShiftDAOInter {

    protected TimeShiftDAO() {
        super();
        super.setTableName("time_shifts");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TimeShiftDTOInter uDTO = (TimeShiftDTOInter) obj[0];
        uDTO.setShiftId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ($shift_id , '$shiftAName', '$shiftEName')";

        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$shift_id", "" + uDTO.getShiftId());
        insertStat = insertStat.replace("$shiftAName", "" + uDTO.getShiftAName());
        insertStat = insertStat.replace("$shiftEName", "" + uDTO.getShiftEName());

        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TimeShiftDTOInter uDTO = (TimeShiftDTOInter) obj[0];
        String updateStat = "update $table set shift_a_name = '$shiftAName', shift_e_name = '$shiftEName'"
                + " where shift_id = $shift_id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$shift_id", "" + uDTO.getShiftId());
        updateStat = updateStat.replace("$shiftAName", "" + uDTO.getShiftAName());
        updateStat = updateStat.replace("$shiftEName", "" + uDTO.getShiftEName());
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TimeShiftDTOInter uDTO = (TimeShiftDTOInter) obj[0];
        String deleteStat = "delete from $table where shift_id = $shift_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$shift_id", "" + uDTO.getShiftId());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer shiftId = (Integer) obj[0];
        String selectStat = "select * from $table where shift_id = $shift_id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$shift_id", "" + shiftId);
        ResultSet rs = executeQuery(selectStat);
        TimeShiftDTOInter uDTO = DTOFactory.createTimeShiftDTO();
        while (rs.next()) {
            uDTO.setShiftAName(rs.getString("shift_a_name"));
            uDTO.setShiftId(rs.getInt("shift_id"));
            uDTO.setShiftEName(rs.getString("shift_e_name"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<TimeShiftDTOInter> uDTOL = new ArrayList<TimeShiftDTOInter>();
        while (rs.next()) {
            TimeShiftDTOInter uDTO = DTOFactory.createTimeShiftDTO();
            uDTO.setShiftAName(rs.getString("shift_a_name"));
            uDTO.setShiftId(rs.getInt("shift_id"));
            uDTO.setShiftEName(rs.getString("shift_e_name"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findRecordGroup() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat =
                "SELECT T.SHIFT_E_NAME LABEL , TO_CHAR(T.SHIFT_ID) SHIFT_ID"
                + " FROM TIME_SHIFTS T"
                + " ORDER BY 1 NULLS FIRST";
        ResultSet rs = executeQuery(selectStat);
        List<TimeShiftDTOInter> uDTOL = new ArrayList<TimeShiftDTOInter>();
        while (rs.next()) {
            TimeShiftDTOInter uDTO = DTOFactory.createTimeShiftDTO();
            uDTO.setShiftAName(rs.getString("LABEL"));
            uDTO.setShiftId(rs.getInt("shift_id"));
            uDTO.setShiftEName(rs.getString("LABEL"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public static void main(String[] args) throws Throwable {
    }
}
