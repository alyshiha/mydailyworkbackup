/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;

import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.BlackListedCardDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.io.IOException;
import java.io.Reader;
import com.ev.AtmBingo.bus.dto.DisputesDTOInter;
import com.ev.AtmBingo.bus.dto.RepTransDetailDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import com.ev.AtmBingo.bus.dto.UserWorkDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.BufferedReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.BatchUpdateException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class DisputesDAO extends BaseDAO implements DisputesDAOInter {

    protected DisputesDAO() {
        super();
        super.setTableName("disputes");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DisputesDTOInter uDTO = (DisputesDTOInter) obj[0];
        String insertStat = "insert into $table values ($file_id, '$loading_date' , '$atm_application_id', $atm_id"
                + ", '$transaction_type', $transaction_type_id, '$currency', $currency_id, '$transaction_status', $transaction_status_id"
                + ", '$response_code', $response_code_id, '$transaction_date', '$transaction_sequence', '$card_no'"
                + ", $amount, '$settlement_date', '$notes_presented', '$customer_account_number', $transaction_sequence_order_by"
                + ", '$transaction_time', $matching_type, $record_type, $dispute_key, '$dispute_reason' ,$settled_flag, '$settled_date', $settled_user"
                + ", '$comments', $dispute_by, '$dispute_date', $dispute_with_roles,'$column1', '$column2', '$column3', '$column4', '$column5', $transaction_type_master"
                + ", $response_code_master, $card_no_suffix)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$file_id", "" + uDTO.getfileid());
        insertStat = insertStat.replace("$loading_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getloadingdate()));

        insertStat = insertStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        insertStat = insertStat.replace("$atm_id", "" + uDTO.getatmid());
        insertStat = insertStat.replace("$transaction_type", "" + uDTO.gettransactiontype());
        insertStat = insertStat.replace("$transaction_type_id", "" + uDTO.gettransactiontypeid());
        insertStat = insertStat.replace("$currency", "" + uDTO.getcurrency());
        insertStat = insertStat.replace("$currency_id", "" + uDTO.getcurrencyid());
        insertStat = insertStat.replace("$transaction_status", "" + uDTO.gettransactionstatus());
        insertStat = insertStat.replace("$transaction_status_id", "" + uDTO.gettransactionstatusid());
        insertStat = insertStat.replace("$response_code", "" + uDTO.getresponsecode());
        insertStat = insertStat.replace("$response_code_id", "" + uDTO.getresponsecodeid());
        insertStat = insertStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.gettransactiondate()));
        insertStat = insertStat.replace("$transaction_sequence", "" + uDTO.gettransactionseqeunce());
        insertStat = insertStat.replace("$card_no", "" + uDTO.getcardno());
        insertStat = insertStat.replace("$amount", "" + uDTO.getamount());
        insertStat = insertStat.replace("$settlement_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getsettlementdate()));
        insertStat = insertStat.replace("$notes_presented", "" + uDTO.getnotespresented());
        insertStat = insertStat.replace("$customer_account_number", "" + uDTO.getcustomeraccountnumber());
        insertStat = insertStat.replace("$transaction_sequence_order_by", "" + uDTO.gettransactionsequenceorderby());
        insertStat = insertStat.replace("$transaction_time", "" + DateFormatter.changeDateAndTimeFormat(uDTO.gettransactiontime()));
        insertStat = insertStat.replace("$matching_type", "" + uDTO.getmatchingtype());
        insertStat = insertStat.replace("$record_type", "" + uDTO.getrecordtype());
        insertStat = insertStat.replace("$dispute_key", "" + uDTO.getdisputekey());
        insertStat = insertStat.replace("$dispute_reason", "" + uDTO.getdisputereason());
        insertStat = insertStat.replace("$settled_flag", "" + uDTO.getsettledflag());
        insertStat = insertStat.replace("$settled_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getsettleddate()));
        insertStat = insertStat.replace("$settled_user", "" + uDTO.getsettleduser());
        insertStat = insertStat.replace("$comments", "" + uDTO.getcomments());
        insertStat = insertStat.replace("$dispute_by", "" + uDTO.getdisputeby());
        insertStat = insertStat.replace("$dispute_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getdisputedate()));
        insertStat = insertStat.replace("$column1", "" + uDTO.getcolumn1());
        insertStat = insertStat.replace("$column2", "" + uDTO.getcolumn2());
        insertStat = insertStat.replace("$column3", "" + uDTO.getcolumn3());
        insertStat = insertStat.replace("$column4", "" + uDTO.getcolumn4());
        insertStat = insertStat.replace("$column5", "" + uDTO.getcolumn5());
        insertStat = insertStat.replace("$transaction_type_master", "" + uDTO.gettransactiontypemaster());
        insertStat = insertStat.replace("$response_code_master", "" + uDTO.getresponsecodemaster());
        insertStat = insertStat.replace("$card_no_suffix", "" + uDTO.getcardnosuffix());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object insertForDisputePage(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DisputesDTOInter uDTO = (DisputesDTOInter) obj[0];
        UsersDTOInter logedUser = (UsersDTOInter) obj[1];
        int s = super.generateSequence("matched_data");
        String insertStat = "insert into matched_data ( file_id, loading_date, atm_application_id, atm_id, transaction_type, "
                + "transaction_type_id, currency, currency_id, transaction_status, transaction_status_id, response_code, "
                + "response_code_id, transaction_date, transaction_sequence, card_no, amount, settlement_date, "
                + "notes_presented, customer_account_number, transaction_sequence_order_by, transaction_time, "
                + "matching_type, record_type, match_key,comments,match_by, column1, column2, column3, column4, column5, transaction_type_master, response_code_master, card_no_suffix, abs_amount) "
                + "select  file_id, loading_date, atm_application_id, atm_id, transaction_type, transaction_type_id, "
                + "currency, currency_id, transaction_status, transaction_status_id, response_code, response_code_id, "
                + "transaction_date, transaction_sequence, card_no, amount, settlement_date, notes_presented, "
                + "customer_account_number, transaction_sequence_order_by, transaction_time, matching_type, "
                + "record_type, $s, comments,$user, column1, column2, column3, column4, column5, transaction_type_master, response_code_master, card_no_suffix, abs(amount) "
                + "from $table where dispute_key = $dispute_key";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$user", "" + logedUser.getUserId());
        insertStat = insertStat.replace("$dispute_key", "" + uDTO.getdisputekey());
        insertStat = insertStat.replace("$s", "" + s);

        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DisputesDTOInter uDTO = (DisputesDTOInter) obj[0];
        String updateStat = "update $table set file_id = $file_id, atm_id = $atm_id, transaction_type = '$transaction_type'"
                + ", transaction_type_id = $transaction_type_id, currency_id = $currency_id, transaction_status = '$transaction_status'"
                + ", transaction_status_id = $transaction_status_id, response_code_id = $response_code_id, transaction_sequence = '$transaction_sequence'"
                + ", settlement_date = '$settlement_date', notes_presented = '$notes_presented', customer_account_number = '$customer_account_number'"
                + ", transaction_sequence_order_by = $transaction_sequence_order_by, transaction_time = '$transaction_time'"
                + ", dispute_key = $dispute_key, settled_flag = $settled_flag, settled_date = '$settled_date', settled_user = $settled_user"
                + ", comments = '$comments', dispute_by = $dispute_by, dispute_date = '$dispute_date', column1 = '$column1'"
                + ", column2 = '$column2', column3 = '$column3', column4 = '$column4', column5 = '$column5', transaction_type_master = $transaction_type_master"
                + ", response_code_master = $response_code_master, card_no_suffix = $card_no_suffix"
                + " where transaction_date = '$transaction_date' and atm_application_id = '$atm_application_id' "
                + "and card_no = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = '$response_code' "
                + "and record_type = $record_type and matching_type = $matching_type";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$file_id", "" + uDTO.getfileid());
        updateStat = updateStat.replace("$loading_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getloadingdate()));
        updateStat = updateStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        updateStat = updateStat.replace("$atm_id", "" + uDTO.getatmid().getId());
        updateStat = updateStat.replace("$transaction_type", "" + uDTO.gettransactiontype());
        updateStat = updateStat.replace("$transaction_type_id", "" + uDTO.gettransactiontypeid().getId());
        updateStat = updateStat.replace("$currency", "" + uDTO.getcurrency());
        updateStat = updateStat.replace("$currency_id", "" + uDTO.getcurrencyid().getId());
        updateStat = updateStat.replace("$transaction_status", "" + uDTO.gettransactionstatus());
        updateStat = updateStat.replace("$transaction_status_id", "" + uDTO.gettransactionstatusid().getId());
        updateStat = updateStat.replace("$response_code", "" + uDTO.getresponsecode());
        updateStat = updateStat.replace("$response_code_id", "" + uDTO.getresponsecodeid().getId());
        updateStat = updateStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.gettransactiondate()));
        updateStat = updateStat.replace("$transaction_sequence", "" + uDTO.gettransactionseqeunce());
        updateStat = updateStat.replace("$card_no", "" + uDTO.getcardno());
        updateStat = updateStat.replace("$amount", "" + uDTO.getamount());
        updateStat = updateStat.replace("$settlement_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getsettlementdate()));
        updateStat = updateStat.replace("$notes_presented", "" + uDTO.getnotespresented());
        updateStat = updateStat.replace("$customer_account_number", "" + uDTO.getcustomeraccountnumber());
        updateStat = updateStat.replace("$transaction_sequence_order_by", "" + uDTO.gettransactionsequenceorderby());
        updateStat = updateStat.replace("$transaction_time", "" + DateFormatter.changeDateAndTimeFormat(uDTO.gettransactiontime()));
        updateStat = updateStat.replace("$matching_type", "" + uDTO.getmatchingtype());
        updateStat = updateStat.replace("$record_type", "" + uDTO.getrecordtype());
        updateStat = updateStat.replace("$dispute_key", "" + uDTO.getdisputekey());
        updateStat = updateStat.replace("$settled_flag", "" + uDTO.getsettledflag());
        updateStat = updateStat.replace("$settled_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getsettleddate()));
        updateStat = updateStat.replace("$settled_user", "" + uDTO.getsettleduser().getUserId());
        updateStat = updateStat.replace("$comments", "" + uDTO.getcomments());
        updateStat = updateStat.replace("$dispute_by", "" + uDTO.getdisputeby());
        updateStat = updateStat.replace("$dispute_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getdisputedate()));
        updateStat = updateStat.replace("$column1", "" + uDTO.getcolumn1());
        updateStat = updateStat.replace("$column2", "" + uDTO.getcolumn2());
        updateStat = updateStat.replace("$column3", "" + uDTO.getcolumn3());
        updateStat = updateStat.replace("$column4", "" + uDTO.getcolumn4());
        updateStat = updateStat.replace("$column5", "" + uDTO.getcolumn5());
        updateStat = updateStat.replace("$transaction_type_master", "" + uDTO.gettransactiontypemaster());
        updateStat = updateStat.replace("$response_code_master", "" + uDTO.getresponsecodemaster());
        updateStat = updateStat.replace("$card_no_suffix", "" + uDTO.getcardnosuffix());
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object updateForExport(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        boolean ReturnFlag = true;
        Exportupdate(uDTO, logedUser);
        Exportlog(uDTO, logedUser);
        return ReturnFlag;
    }

    private boolean Exportlog(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String inserStat = "insert into export_log (atm_application_id, currency, transaction_date, transaction_sequence, card_no, amount, response_code, matching_type, record_type, export_user,export_date) values"
                    + "(?,?,to_date(?,'dd.mm.yyyy hh24:mi:ss'),?,?,?, ?, ?, ?, ?,sysdate)";
            statement = connection.prepareStatement(inserStat);
            for (int i = 0; i < uDTO.length; i++) {
                DisputesDTOInter entity = uDTO[i];

                statement.setString(1, entity.getatmapplicationid());
                statement.setString(2, entity.getcurrency());
                statement.setString(3, "" + DateFormatter.changeDateAndTimeFormat(entity.gettransactiondate()));
                statement.setString(4, entity.gettransactionseqeunce());
                statement.setString(5, entity.getcardno());
                statement.setInt(6, entity.getamount());
                statement.setString(7, entity.getresponsecode());
                statement.setInt(8, entity.getmatchingtype());
                statement.setInt(9, entity.getrecordtype());
                statement.setInt(10, logedUser.getUserId());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
    }

    public Object updateForExport2(DisputesDTOInter uDTO, UsersDTOInter logedUser) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String updateStat = "update $table set settled_flag=2 ,settled_date=sysdate,SETTLEMENT_DATE = sysdate ,settled_user=$user "
                + "where transaction_date = to_date('$transaction_date','dd.mm.yyyy hh24:mi:ss') and atm_application_id = '$atm_application_id' "
                + "and card_no = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = '$response_code' "
                + "and record_type = $record_type and matching_type = $matching_type";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$user", "" + logedUser.getUserId());
        updateStat = updateStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.gettransactiondate()));
        updateStat = updateStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        updateStat = updateStat.replace("$card_no", "" + uDTO.getcardno());
        updateStat = updateStat.replace("$amount", "" + uDTO.getamount());
        updateStat = updateStat.replace("$currency", "" + uDTO.getcurrency());
        updateStat = updateStat.replace("$response_code", "" + uDTO.getresponsecode());
        updateStat = updateStat.replace("$record_type", "" + uDTO.getrecordtype());
        updateStat = updateStat.replace("$matching_type", "" + uDTO.getmatchingtype());

        super.executeUpdate(updateStat);

        String inserStat = "insert into SETTLEMENT_LOG values ( '$atm_application_id'"
                + ",'$currency',to_date('$transaction_date','dd.mm.yyyy hh24:mi:ss'),'$trans_sequence','$card_no',$amount,sysdate,'$response_code'"
                + ",$matching_type,$record_type,$user,$flag)";

        inserStat = inserStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        inserStat = inserStat.replace("$card_no", "" + uDTO.getcardno());
        inserStat = inserStat.replace("$amount", "" + uDTO.getamount());
        inserStat = inserStat.replace("$currency", "" + uDTO.getcurrency());
        inserStat = inserStat.replace("$response_code", "" + uDTO.getresponsecode());
        inserStat = inserStat.replace("$record_type", "" + uDTO.getrecordtype());
        inserStat = inserStat.replace("$matching_type", "" + uDTO.getmatchingtype());
        inserStat = inserStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.gettransactiondate()));
        inserStat = inserStat.replace("$trans_sequence", "" + uDTO.gettransactionseqeunce());
        inserStat = inserStat.replace("$user", "" + logedUser.getUserId());
        inserStat = inserStat.replace("$flag", "" + 2);

        super.executeUpdate(inserStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object insertBlackListed(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DisputesDTOInter uDTO = (DisputesDTOInter) obj[0];
        String User = (String) obj[1];
        String insertStat = "insert into black_listed_card values ('$cardNo', '$customerNo' , 'From Transaction Screen By User $user' , sysdate)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$cardNo", "" + uDTO.getcardno());
        insertStat = insertStat.replace("$customerNo", "" + uDTO.getcustomeraccountnumber());
        insertStat = insertStat.replace("$user", "" + User);
        super.executeUpdate(insertStat);
        super.postUpdate("Add " + uDTO.getcardno() + " card no. to black list", false);
        return null;
    }

    private boolean updateDisputePage2rep(RepTransDetailDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String updateStat = "update disputes set settled_flag=2 ,settled_date=null,SETTLEMENT_DATE = null ,settled_user=null "
                    + " where transaction_date = to_date(?,'dd.mm.yyyy hh24:mi:ss') and atm_application_id = ? "
                    + " and card_no = ? and amount = ? and currency = ? and trim(response_code) = ? "
                    + " and record_type = ? and matching_type = ?";
            statement = connection.prepareStatement(updateStat);
            for (int i = 0; i < uDTO.length; i++) {
                RepTransDetailDTOInter entity = uDTO[i];
                statement.setString(1, DateFormatter.changeDateAndTimeFormat(entity.getTransactiondate()).toString());
                statement.setString(2, entity.getAtmapplicationid());
                statement.setString(3, entity.getCardno());
                statement.setInt(4, entity.getAmount());
                statement.setString(5, entity.getCurrency());
                statement.setString(6, entity.getResponsecode());
                statement.setInt(7, entity.getRecordtypevalue());
                statement.setInt(8, entity.getMatchingtype());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                    connection.commit();
                }

            }
            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }

        } finally {
            if (statement != null) {
                statement.close();
            }
            return true;
        }
    }

    private boolean updateDisputePage2(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String updateStat = "update disputes set settled_flag=2 ,settled_date=null,SETTLEMENT_DATE = null ,settled_user=null "
                    + " where transaction_date = to_date(?,'dd.mm.yyyy hh24:mi:ss') and atm_application_id = ? "
                    + " and card_no = ? and amount = ? and currency = ? and trim(response_code) = ? "
                    + " and record_type = ? and matching_type = ?";
            statement = connection.prepareStatement(updateStat);
            for (int i = 0; i < uDTO.length; i++) {
                DisputesDTOInter entity = uDTO[i];
                statement.setString(1, DateFormatter.changeDateAndTimeFormat(entity.gettransactiondate()).toString());
                statement.setString(2, entity.getatmapplicationid());
                statement.setString(3, entity.getcardno());
                statement.setInt(4, entity.getamount());
                statement.setString(5, entity.getcurrency());
                statement.setString(6, entity.getresponsecode());
                statement.setInt(7, entity.getrecordtype());
                statement.setInt(8, entity.getmatchingtype());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                    connection.commit();
                }

            }
            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }

        } finally {
            if (statement != null) {
                statement.close();
            }
            return true;
        }
    }

    private boolean DisputeKey2rep(RepTransDetailDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String updateStat = "update disputes set settled_flag=2 ,settled_date=null,SETTLEMENT_DATE = null ,settled_user=null "
                    + "where DISPUTE_KEY = ?";
            statement = connection.prepareStatement(updateStat);
            for (int i = 0; i < uDTO.length; i++) {
                RepTransDetailDTOInter entity = uDTO[i];
                if (entity.getDisputekey() != 0) {
                    //    statement.setString(1, super.getTableName());
                    statement.setInt(1, entity.getDisputekey());
                    // ...
                    statement.addBatch();
                }
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }

            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
    }

    private boolean DisputeKey2(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String updateStat = "update disputes set settled_flag=2 ,settled_date=null,SETTLEMENT_DATE = null ,settled_user=null "
                    + "where DISPUTE_KEY = ?";
            statement = connection.prepareStatement(updateStat);
            for (int i = 0; i < uDTO.length; i++) {
                DisputesDTOInter entity = uDTO[i];
                if (entity.getdisputekey() != 0) {
                    //    statement.setString(1, super.getTableName());
                    statement.setInt(1, entity.getdisputekey());
                    // ...
                    statement.addBatch();
                }
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }

            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
    }

    private boolean correctivelog2rep(RepTransDetailDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String inserStat = "insert into corrective_log (atm_application_id, currency, transaction_date, transaction_sequence, card_no, amount, corrective_date, response_code, matching_type, record_type, corrective_user, corrective_state) values"
                    + "(?,?,to_date(?,'dd.mm.yyyy hh24:mi:ss'),?,?,?,sysdate, ?, ?, ?, ?, ?)";
            statement = connection.prepareStatement(inserStat);
            for (int i = 0; i < uDTO.length; i++) {
                RepTransDetailDTOInter entity = uDTO[i];

                statement.setString(1, entity.getAtmapplicationid());
                statement.setString(2, entity.getCurrency());
                statement.setString(3, DateFormatter.changeDateAndTimeFormat(entity.getTransactiondate()).toString());
                statement.setString(4, entity.getTransactionsequence());
                statement.setString(5, entity.getCardno());
                statement.setInt(6, entity.getAmount());
                statement.setString(7, entity.getResponsecode());
                statement.setInt(8, entity.getMatchingtype());
                statement.setInt(9, entity.getRecordtypevalue());
                statement.setInt(10, logedUser.getUserId());
                statement.setInt(11, 2);
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
    }

    private boolean correctivelog2(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String inserStat = "insert into corrective_log (atm_application_id, currency, transaction_date, transaction_sequence, card_no, amount, corrective_date, response_code, matching_type, record_type, corrective_user, corrective_state) values"
                    + "(?,?,to_date(?,'dd.mm.yyyy hh24:mi:ss'),?,?,?,sysdate, ?, ?, ?, ?, ?)";
            statement = connection.prepareStatement(inserStat);
            for (int i = 0; i < uDTO.length; i++) {
                DisputesDTOInter entity = uDTO[i];

                statement.setString(1, entity.getatmapplicationid());
                statement.setString(2, entity.getcurrency());
                statement.setString(3, DateFormatter.changeDateAndTimeFormat(entity.gettransactiondate()).toString());
                statement.setString(4, entity.gettransactionseqeunce());
                statement.setString(5, entity.getcardno());
                statement.setInt(6, entity.getamount());
                statement.setString(7, entity.getresponsecode());
                statement.setInt(8, entity.getmatchingtype());
                statement.setInt(9, entity.getrecordtype());
                statement.setInt(10, logedUser.getUserId());
                statement.setInt(11, 2);
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
    }

    private boolean Exportupdate(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String updateStat = "";

            updateStat = "update disputes set CORRECTIVE_ENTRY_FLAG=2,export_date =sysdate,export_user=?"
                    + "where transaction_date = to_date(?,'dd.mm.yyyy hh24:mi:ss') and atm_application_id = ? "
                    + "and card_no = ? and amount = ? and currency = ? and trim(response_code) = ? "
                    + "and record_type = ? and matching_type = ?";

            statement = connection.prepareStatement(updateStat);
            for (int i = 0; i < uDTO.length; i++) {
                DisputesDTOInter entity = uDTO[i];
                // statement.setString(1, super.getTableName());
                statement.setInt(1, logedUser.getUserId());
                statement.setString(2, DateFormatter.changeDateAndTimeFormat(entity.gettransactiondate()).toString());
                statement.setString(3, entity.getatmapplicationid());
                statement.setString(4, entity.getcardno());
                statement.setInt(5, entity.getamount());
                statement.setString(6, entity.getcurrency());
                statement.setString(7, entity.getresponsecode());
                statement.setInt(8, entity.getrecordtype());
                statement.setInt(9, entity.getmatchingtype());
                // ...
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
    }

    private boolean CorrectiveEntryupdaterep(RepTransDetailDTOInter[] uDTO, int operation, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String updateStat = "";
            if (operation == 1) {
                updateStat = "update disputes set CORRECTIVE_ENTRY_FLAG=1,Corrective_Date = sysdate,corrective_user=?"
                        + "where transaction_date = to_date(?,'dd.mm.yyyy hh24:mi:ss') and atm_application_id = ? "
                        + "and card_no = ? and amount = ? and currency = ? and trim(response_code) = ? "
                        + "and record_type = ? and matching_type = ?";
            }
            if (operation == 2) {
                updateStat = "update disputes set CORRECTIVE_ENTRY_FLAG=null,Corrective_Date = sysdate,corrective_user=? "
                        + " where transaction_date = to_date(?,'dd.mm.yyyy hh24:mi:ss') and atm_application_id = ? "
                        + " and card_no = ? and amount = ? and currency = ? and trim(response_code) = ? "
                        + " and record_type = ? and matching_type = ?";
            }
            statement = connection.prepareStatement(updateStat);
            for (int i = 0; i < uDTO.length; i++) {
                RepTransDetailDTOInter entity = uDTO[i];
                // statement.setString(1, super.getTableName());
                statement.setInt(1, logedUser.getUserId());
                statement.setString(2, DateFormatter.changeDateAndTimeFormat(entity.getTransactiondate()).toString());
                statement.setString(3, entity.getAtmapplicationid());
                statement.setString(4, entity.getCardno());
                statement.setInt(5, entity.getAmount());
                statement.setString(6, entity.getCurrency());
                statement.setString(7, entity.getResponsecode());
                statement.setInt(8, entity.getRecordtypevalue());
                statement.setInt(9, entity.getMatchingtype());
                // ...
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
    }

    private boolean CorrectiveEntryupdate(DisputesDTOInter[] uDTO, int operation, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String updateStat = "";
            if (operation == 1) {
                updateStat = "update disputes set CORRECTIVE_ENTRY_FLAG=1,Corrective_Date = sysdate,corrective_user=?"
                        + "where transaction_date = to_date(?,'dd.mm.yyyy hh24:mi:ss') and atm_application_id = ? "
                        + "and card_no = ? and amount = ? and currency = ? and trim(response_code) = ? "
                        + "and record_type = ? and matching_type = ?";
            }
            if (operation == 2) {
                updateStat = "update disputes set CORRECTIVE_ENTRY_FLAG=null,Corrective_Date = sysdate,corrective_user=? "
                        + " where transaction_date = to_date(?,'dd.mm.yyyy hh24:mi:ss') and atm_application_id = ? "
                        + " and card_no = ? and amount = ? and currency = ? and trim(response_code) = ? "
                        + " and record_type = ? and matching_type = ?";
            }
            statement = connection.prepareStatement(updateStat);
            for (int i = 0; i < uDTO.length; i++) {
                DisputesDTOInter entity = uDTO[i];
                // statement.setString(1, super.getTableName());
                statement.setInt(1, logedUser.getUserId());
                statement.setString(2, DateFormatter.changeDateAndTimeFormat(entity.gettransactiondate()).toString());
                statement.setString(3, entity.getatmapplicationid());
                statement.setString(4, entity.getcardno());
                statement.setInt(5, entity.getamount());
                statement.setString(6, entity.getcurrency());
                statement.setString(7, entity.getresponsecode());
                statement.setInt(8, entity.getrecordtype());
                statement.setInt(9, entity.getmatchingtype());
                // ...
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
    }

    private boolean DisputeKeyrep(RepTransDetailDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String updateStat = "update disputes set settled_flag=1 ,settled_date=sysdate,SETTLEMENT_DATE = sysdate ,settled_user=? "
                    + "where DISPUTE_KEY = ?";
            statement = connection.prepareStatement(updateStat);
            for (int i = 0; i < uDTO.length; i++) {
                RepTransDetailDTOInter entity = uDTO[i];
                if (entity.getDisputekey() != 0) {
                    // statement.setString(1, super.getTableName());
                    statement.setInt(1, logedUser.getUserId());
                    statement.setInt(2, entity.getDisputekey());
                    // ...
                    statement.addBatch();
                }
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }

            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
    }

    private boolean DisputeKey(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String updateStat = "update disputes set settled_flag=1 ,settled_date=sysdate,SETTLEMENT_DATE = sysdate ,settled_user=? "
                    + "where DISPUTE_KEY = ?";
            statement = connection.prepareStatement(updateStat);
            for (int i = 0; i < uDTO.length; i++) {
                DisputesDTOInter entity = uDTO[i];
                if (entity.getdisputekey() != 0) {
                    // statement.setString(1, super.getTableName());
                    statement.setInt(1, logedUser.getUserId());
                    statement.setInt(2, entity.getdisputekey());
                    // ...
                    statement.addBatch();
                }
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }

            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
    }

    private boolean correctivelogrep(RepTransDetailDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String inserStat = "insert into corrective_log (atm_application_id, currency, transaction_date, transaction_sequence, card_no, amount, corrective_date, response_code, matching_type, record_type, corrective_user, corrective_state) values"
                    + "(?,?,to_date(?,'dd.mm.yyyy hh24:mi:ss'),?,?,?,sysdate, ?, ?, ?, ?, ?)";
            statement = connection.prepareStatement(inserStat);
            // System.out.println(logedUser.getUserId());
            for (int i = 0; i < uDTO.length; i++) {
                RepTransDetailDTOInter entity = uDTO[i];

                statement.setString(1, entity.getAtmapplicationid());
                statement.setString(2, entity.getCurrency());
                statement.setString(3, DateFormatter.changeDateAndTimeFormat(entity.getTransactiondate()).toString());
                statement.setString(4, entity.getTransactionsequence());
                statement.setString(5, entity.getCardno());
                statement.setInt(6, entity.getAmount());
                statement.setString(7, entity.getResponsecode());
                statement.setInt(8, entity.getMatchingtype());
                statement.setInt(9, entity.getRecordtypevalue());
                statement.setInt(10, logedUser.getUserId());
                statement.setInt(11, 1);
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
    }

    private boolean correctivelog(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String inserStat = "insert into corrective_log (atm_application_id, currency, transaction_date, transaction_sequence, card_no, amount, corrective_date, response_code, matching_type, record_type, corrective_user, corrective_state) values"
                    + "(?,?,to_date(?,'dd.mm.yyyy hh24:mi:ss'),?,?,?,sysdate, ?, ?, ?, ?, ?)";
            statement = connection.prepareStatement(inserStat);
            // System.out.println(logedUser.getUserId());
            for (int i = 0; i < uDTO.length; i++) {
                DisputesDTOInter entity = uDTO[i];

                statement.setString(1, entity.getatmapplicationid());
                statement.setString(2, entity.getcurrency());
                statement.setString(3, DateFormatter.changeDateAndTimeFormat(entity.gettransactiondate()).toString());
                statement.setString(4, entity.gettransactionseqeunce());
                statement.setString(5, entity.getcardno());
                statement.setInt(6, entity.getamount());
                statement.setString(7, entity.getresponsecode());
                statement.setInt(8, entity.getmatchingtype());
                statement.setInt(9, entity.getrecordtype());
                statement.setInt(10, logedUser.getUserId());
                statement.setInt(11, 1);
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
    }

    private boolean updateDisputePagerep(RepTransDetailDTOInter[] uDTO, UsersDTOInter logedUser) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String updateStat = "update disputes set settled_flag=1 ,settled_date=sysdate,SETTLEMENT_DATE = sysdate ,settled_user=? "
                    + " where transaction_date = to_date(?,'dd.mm.yyyy hh24:mi:ss') and atm_application_id = ? "
                    + " and card_no = ? and amount = ? and currency = ? and trim(response_code) = ? "
                    + " and record_type = ? and matching_type = ?";
            statement = connection.prepareStatement(updateStat);
            for (int i = 0; i < uDTO.length; i++) {
                RepTransDetailDTOInter entity = uDTO[i];
                //statement.setString(1, super.getTableName());
                statement.setInt(1, logedUser.getUserId());
                statement.setString(2, DateFormatter.changeDateAndTimeFormat(entity.getTransactiondate()).toString());
                statement.setString(3, entity.getAtmapplicationid());
                statement.setString(4, entity.getCardno());
                statement.setInt(5, entity.getAmount());
                statement.setString(6, entity.getCurrency());
                statement.setString(7, entity.getResponsecode());
                statement.setInt(8, entity.getRecordtypevalue());
                statement.setInt(9, entity.getMatchingtype());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                    connection.commit();
                }

            }

            int count[] = statement.executeBatch();
            connection.commit();
            CoonectionHandler.getInstance().returnConnection(connection);

        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DisputesDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return true;
        }
    }

    private boolean updateDisputePage(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            String updateStat = "update disputes set settled_flag=1 ,settled_date=sysdate,SETTLEMENT_DATE = sysdate ,settled_user=? "
                    + " where transaction_date = to_date(?,'dd.mm.yyyy hh24:mi:ss') and atm_application_id = ? "
                    + " and card_no = ? and amount = ? and currency = ? and trim(response_code) = ? "
                    + " and record_type = ? and matching_type = ?";
            statement = connection.prepareStatement(updateStat);
            for (int i = 0; i < uDTO.length; i++) {
                DisputesDTOInter entity = uDTO[i];
                //statement.setString(1, super.getTableName());
                statement.setInt(1, logedUser.getUserId());
                statement.setString(2, DateFormatter.changeDateAndTimeFormat(entity.gettransactiondate()).toString());
                statement.setString(3, entity.getatmapplicationid());
                statement.setString(4, entity.getcardno());
                statement.setInt(5, entity.getamount());
                statement.setString(6, entity.getcurrency());
                statement.setString(7, entity.getresponsecode());
                statement.setInt(8, entity.getrecordtype());
                statement.setInt(9, entity.getmatchingtype());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                    connection.commit();
                }

            }
            try {
                int count[] = statement.executeBatch();
                connection.commit();
                CoonectionHandler.getInstance().returnConnection(connection);

            } catch (BatchUpdateException s) {

            }

        } finally {
            if (statement != null) {
                statement.close();
            }
            return true;
        }
    }

    public Object updateForCorrectiveEntryrep(RepTransDetailDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
//here
        boolean ReturnFlag = true;
        updateDisputePagerep(uDTO, logedUser);
        CorrectiveEntryupdaterep(uDTO, 1, logedUser);
        DisputeKeyrep(uDTO, logedUser);
        correctivelogrep(uDTO, logedUser);
        return ReturnFlag;
    }

    public Object updateForCorrectiveEntry(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {

        boolean ReturnFlag = true;
        updateDisputePage(uDTO, logedUser);
        CorrectiveEntryupdate(uDTO, 1, logedUser);
        DisputeKey(uDTO, logedUser);
        correctivelog(uDTO, logedUser);
        return ReturnFlag;
    }

    public Object updateForCorrectiveEntry2(DisputesDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
//here
        boolean ReturnFlag = true;
        updateDisputePage2(uDTO, logedUser);
        CorrectiveEntryupdate(uDTO, 2, logedUser);
        DisputeKey2(uDTO, logedUser);
        correctivelog2(uDTO, logedUser);
        return ReturnFlag;
    }

    public Object updateForCorrectiveEntry2rep(RepTransDetailDTOInter[] uDTO, UsersDTOInter logedUser) throws Throwable {
//here
        boolean ReturnFlag = true;
        updateDisputePage2rep(uDTO, logedUser);
        CorrectiveEntryupdaterep(uDTO, 2, logedUser);
        DisputeKey2rep(uDTO, logedUser);
        correctivelog2rep(uDTO, logedUser);
        return ReturnFlag;
    }

    public Object updateComments(RepTransDetailDTOInter uDTO) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String updateStat = "update $table set comments='$comments' "
                + "where transaction_date = to_date('$transaction_date','dd.mm.yyyy hh24:mi:ss') and atm_application_id = '$atm_application_id' "
                + "and card_no = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = trim('$response_code') "
                + "and record_type = $record_type and matching_type = $matching_type";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$comments", "" + uDTO.getComments());
        updateStat = updateStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactiondate()));
        updateStat = updateStat.replace("$atm_application_id", "" + uDTO.getAtmapplicationid());
        updateStat = updateStat.replace("$card_no", "" + uDTO.getCardno());
        updateStat = updateStat.replace("$amount", "" + uDTO.getAmount());
        updateStat = updateStat.replace("$currency", "" + uDTO.getCurrency());
        updateStat = updateStat.replace("$response_code", "" + uDTO.getResponsecode());
        updateStat = updateStat.replace("$record_type", "" + uDTO.getRecordtypevalue());
        updateStat = updateStat.replace("$matching_type", "" + uDTO.getMatchingtype());

        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object updateForDisputePage(RepTransDetailDTOInter uDTO, UsersDTOInter logedUser) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String updateStat = "update $table set settled_flag=1 ,settled_date=sysdate,SETTLEMENT_DATE = sysdate ,settled_user=$user "
                + "where transaction_date = to_date('$transaction_date','dd.mm.yyyy hh24:mi:ss') and atm_application_id = '$atm_application_id' "
                + "and card_no = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = trim('$response_code') "
                + "and record_type = $record_type and matching_type = $matching_type";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$user", "" + logedUser.getUserId());
        updateStat = updateStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactiondate()));
        updateStat = updateStat.replace("$atm_application_id", "" + uDTO.getAtmapplicationid());
        updateStat = updateStat.replace("$card_no", "" + uDTO.getCardno());
        updateStat = updateStat.replace("$amount", "" + uDTO.getAmount());
        updateStat = updateStat.replace("$currency", "" + uDTO.getCurrency());
        updateStat = updateStat.replace("$response_code", "" + uDTO.getResponsecode());
        updateStat = updateStat.replace("$record_type", "" + uDTO.getRecordtypevalue());
        updateStat = updateStat.replace("$matching_type", "" + uDTO.getMatchingtype());

        super.executeUpdate(updateStat);

        updateStat = "";

        if (uDTO.getDisputekey() != 0) {
            updateStat = "update $table set settled_flag=1 ,settled_date=sysdate,SETTLEMENT_DATE = sysdate ,settled_user=$user "
                    + "where DISPUTE_KEY = $disputeKey";
            updateStat = updateStat.replace("$table", "" + super.getTableName());
            updateStat = updateStat.replace("$user", "" + logedUser.getUserId());
            updateStat = updateStat.replace("$disputeKey", "" + uDTO.getDisputekey());
            super.executeUpdate(updateStat);
        }
        String inserStat = "insert into SETTLEMENT_LOG values ( '$atm_application_id'"
                + ",'$currency',to_date('$transaction_date','dd.mm.yyyy hh24:mi:ss'),'$trans_sequence','$card_no',$amount,sysdate,'$response_code'"
                + ",$matching_type,$record_type,$user,$flag)";

        inserStat = inserStat.replace("$atm_application_id", "" + uDTO.getAtmapplicationid());
        inserStat = inserStat.replace("$card_no", "" + uDTO.getCardno());
        inserStat = inserStat.replace("$amount", "" + uDTO.getAmount());
        inserStat = inserStat.replace("$currency", "" + uDTO.getCurrency());
        inserStat = inserStat.replace("$response_code", "" + uDTO.getResponsecode());
        inserStat = inserStat.replace("$record_type", "" + uDTO.getRecordtypevalue());
        inserStat = inserStat.replace("$matching_type", "" + uDTO.getMatchingtype());
        inserStat = inserStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getTransactiondate()));
        inserStat = inserStat.replace("$trans_sequence", "" + uDTO.getTransactionsequence());
        inserStat = inserStat.replace("$user", "" + logedUser.getUserId());
        inserStat = inserStat.replace("$flag", "" + 1);

        super.executeUpdate(inserStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object updateForDisputePage(DisputesDTOInter uDTO, UsersDTOInter logedUser) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String updateStat = "update $table set settled_flag=1 ,settled_date=sysdate,SETTLEMENT_DATE = sysdate ,settled_user=$user "
                + "where transaction_date = to_date('$transaction_date','dd.mm.yyyy hh24:mi:ss') and atm_application_id = '$atm_application_id' "
                + "and card_no = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = trim('$response_code') "
                + "and record_type = $record_type and matching_type = $matching_type";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$user", "" + logedUser.getUserId());
        updateStat = updateStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.gettransactiondate()));
        updateStat = updateStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        updateStat = updateStat.replace("$card_no", "" + uDTO.getcardno());
        updateStat = updateStat.replace("$amount", "" + uDTO.getamount());
        updateStat = updateStat.replace("$currency", "" + uDTO.getcurrency());
        updateStat = updateStat.replace("$response_code", "" + uDTO.getresponsecode());
        updateStat = updateStat.replace("$record_type", "" + uDTO.getrecordtype());
        updateStat = updateStat.replace("$matching_type", "" + uDTO.getmatchingtype());

        super.executeUpdate(updateStat);

        updateStat = "";

        if (uDTO.getdisputekey() != 0) {
            updateStat = "update $table set settled_flag=1 ,settled_date=sysdate,SETTLEMENT_DATE = sysdate ,settled_user=$user "
                    + "where DISPUTE_KEY = $disputeKey";
            updateStat = updateStat.replace("$table", "" + super.getTableName());
            updateStat = updateStat.replace("$user", "" + logedUser.getUserId());
            updateStat = updateStat.replace("$disputeKey", "" + uDTO.getdisputekey());
            super.executeUpdate(updateStat);
        }
        String inserStat = "insert into SETTLEMENT_LOG values ( '$atm_application_id'"
                + ",'$currency',to_date('$transaction_date','dd.mm.yyyy hh24:mi:ss'),'$trans_sequence','$card_no',$amount,sysdate,'$response_code'"
                + ",$matching_type,$record_type,$user,$flag)";

        inserStat = inserStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        inserStat = inserStat.replace("$card_no", "" + uDTO.getcardno());
        inserStat = inserStat.replace("$amount", "" + uDTO.getamount());
        inserStat = inserStat.replace("$currency", "" + uDTO.getcurrency());
        inserStat = inserStat.replace("$response_code", "" + uDTO.getresponsecode());
        inserStat = inserStat.replace("$record_type", "" + uDTO.getrecordtype());
        inserStat = inserStat.replace("$matching_type", "" + uDTO.getmatchingtype());
        inserStat = inserStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.gettransactiondate()));
        inserStat = inserStat.replace("$trans_sequence", "" + uDTO.gettransactionseqeunce());
        inserStat = inserStat.replace("$user", "" + logedUser.getUserId());
        inserStat = inserStat.replace("$flag", "" + 1);

        super.executeUpdate(inserStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object updateForDisputePage2(DisputesDTOInter uDTO, UsersDTOInter logedUser) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String updateStat = "update $table set settled_flag=2 ,settled_date=null,SETTLEMENT_DATE = null ,settled_user=null "
                + "where transaction_date = to_date('$transaction_date','dd.mm.yyyy hh24:mi:ss') and atm_application_id = '$atm_application_id' "
                + "and card_no = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = '$response_code' "
                + "and record_type = $record_type and matching_type = $matching_type";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$user", "" + logedUser.getUserId());
        updateStat = updateStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.gettransactiondate()));
        updateStat = updateStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        updateStat = updateStat.replace("$card_no", "" + uDTO.getcardno());
        updateStat = updateStat.replace("$amount", "" + uDTO.getamount());
        updateStat = updateStat.replace("$currency", "" + uDTO.getcurrency());
        updateStat = updateStat.replace("$response_code", "" + uDTO.getresponsecode());
        updateStat = updateStat.replace("$record_type", "" + uDTO.getrecordtype());
        updateStat = updateStat.replace("$matching_type", "" + uDTO.getmatchingtype());

        super.executeUpdate(updateStat);
        if (uDTO.getdisputekey() != 0) {
            updateStat = "update $table set settled_flag=2 ,settled_date=null,SETTLEMENT_DATE = null ,settled_user=null "
                    + "where DISPUTE_KEY = $disputeKey";
            updateStat = updateStat.replace("$table", "" + super.getTableName());
            updateStat = updateStat.replace("$user", "" + logedUser.getUserId());
            updateStat = updateStat.replace("$disputeKey", "" + uDTO.getdisputekey());
            super.executeUpdate(updateStat);
        }
        String inserStat = "insert into SETTLEMENT_LOG values ( '$atm_application_id'"
                + ",'$currency',to_date('$transaction_date','dd.mm.yyyy hh24:mi:ss'),'$trans_sequence','$card_no',$amount,sysdate,'$response_code'"
                + ",$matching_type,$record_type,$user,$flag)";

        inserStat = inserStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        inserStat = inserStat.replace("$card_no", "" + uDTO.getcardno());
        inserStat = inserStat.replace("$amount", "" + uDTO.getamount());
        inserStat = inserStat.replace("$currency", "" + uDTO.getcurrency());
        inserStat = inserStat.replace("$response_code", "" + uDTO.getresponsecode());
        inserStat = inserStat.replace("$record_type", "" + uDTO.getrecordtype());
        inserStat = inserStat.replace("$matching_type", "" + uDTO.getmatchingtype());
        inserStat = inserStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.gettransactiondate()));
        inserStat = inserStat.replace("$trans_sequence", "" + uDTO.gettransactionseqeunce());
        inserStat = inserStat.replace("$user", "" + logedUser.getUserId());
        inserStat = inserStat.replace("$flag", "" + 2);

        super.executeUpdate(inserStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object updatecorrectiveamount(DisputesDTOInter uDTO, UsersDTOInter user) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool gcustomSearchetConn().createStatement();
        String updateStat = "update $table set corrective_amount='$comments' "
                + "where transaction_date = to_date('$transaction_date','dd.mm.yyyy hh24:mi:ss') and atm_application_id = '$atm_application_id' "
                + "and card_no = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = trim('$response_code') "
                + "and record_type = $record_type and matching_type = $matching_type";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$comments", "" + uDTO.getCorrectiveamount());
        updateStat = updateStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.gettransactiondate()));
        updateStat = updateStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        updateStat = updateStat.replace("$card_no", "" + uDTO.getcardno());
        updateStat = updateStat.replace("$amount", "" + uDTO.getamount());
        updateStat = updateStat.replace("$currency", "" + uDTO.getcurrency());
        updateStat = updateStat.replace("$response_code", "" + uDTO.getresponsecode());
        updateStat = updateStat.replace("$record_type", "" + uDTO.getrecordtype());
        updateStat = updateStat.replace("$matching_type", "" + uDTO.getmatchingtype());

        super.executeUpdate(updateStat);
        super.postUpdate("Corrective Amount Updated By " + user.getLogonName(), false);
        return null;
    }

    public Object updateComments(DisputesDTOInter uDTO) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String updateStat = "update $table set comments='$comments' "
                + "where transaction_date = to_date('$transaction_date','dd.mm.yyyy hh24:mi:ss') and atm_application_id = '$atm_application_id' "
                + "and card_no = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = trim('$response_code') "
                + "and record_type = $record_type and matching_type = $matching_type";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$comments", "" + uDTO.getcomments());
        updateStat = updateStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.gettransactiondate()));
        updateStat = updateStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        updateStat = updateStat.replace("$card_no", "" + uDTO.getcardno());
        updateStat = updateStat.replace("$amount", "" + uDTO.getamount());
        updateStat = updateStat.replace("$currency", "" + uDTO.getcurrency());
        updateStat = updateStat.replace("$response_code", "" + uDTO.getresponsecode());
        updateStat = updateStat.replace("$record_type", "" + uDTO.getrecordtype());
        updateStat = updateStat.replace("$matching_type", "" + uDTO.getmatchingtype());

        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object deleteForDisputePage(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DisputesDTOInter uDTO = (DisputesDTOInter) obj[0];
        String deleteStat = "delete from $table where dispute_key = $dispute_key";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$dispute_key", "" + uDTO.getdisputekey());

        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DisputesDTOInter uDTO = (DisputesDTOInter) obj[0];
        String deleteStat = "delete from $table where transaction_date = '$transaction_date' and atm_application_id = '$atm_application_id' "
                + "and card_no = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = trim('$response_code') "
                + "and record_type = $record_type and matching_type = $matching_type";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$atm_application_id", "" + uDTO.getatmapplicationid());
        deleteStat = deleteStat.replace("$currency", "" + uDTO.getcurrency());
        deleteStat = deleteStat.replace("$response_code", "" + uDTO.getresponsecode());
        deleteStat = deleteStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(uDTO.gettransactiondate()));
        deleteStat = deleteStat.replace("$card_no", "" + uDTO.getcardno());
        deleteStat = deleteStat.replace("$amount", "" + uDTO.getamount());
        deleteStat = deleteStat.replace("$matching_type", "" + uDTO.getmatchingtype());
        deleteStat = deleteStat.replace("$record_type", "" + uDTO.getrecordtype());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Date transactionDate, String atmApplicationId, String cardNo, int amount, String currency, String responseCode, int matchingType, int recordType) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select $table.*,$table.card_no card_no_decrypt from $table where transaction_date = '$transaction_date' and atm_application_id = '$atm_application_id' "
                + "and card_no = '$card_no' and amount = $amount and currency = '$currency' and trim(response_code) = trim('$response_code') "
                + "and record_type = $record_type and matching_type = $matching_type";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$atm_application_id", "" + atmApplicationId);
        selectStat = selectStat.replace("$currency", "" + currency);
        selectStat = selectStat.replace("$response_code", "" + responseCode);
        selectStat = selectStat.replace("$transaction_date", "" + transactionDate);
        selectStat = selectStat.replace("$card_no", "" + cardNo);
        selectStat = selectStat.replace("$amount", "" + amount);
        selectStat = selectStat.replace("$matching_type", "" + matchingType);
        selectStat = selectStat.replace("$record_type", "" + recordType);

        ResultSet rs = executeQuery(selectStat);
        DisputesDTOInter uDTO = DTOFactory.createDisputesDTO();
        while (rs.next()) {
            uDTO.setamount(rs.getInt("amount"));
            uDTO.setatmapplicationid(rs.getString("atm_application_id"));

            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
            amDTO.setId(rs.getInt("atm_id"));
            uDTO.setatmid(amDTO);

            uDTO.setcardno(rs.getString("card_no_decrypt"));
            uDTO.setcardnosuffix(rs.getInt("card_no_suffix"));
            uDTO.setcolumn1(rs.getString("column1"));
            uDTO.setcolumn2(rs.getString("column2"));
            uDTO.setcolumn3(rs.getString("column3"));
            uDTO.setcolumn4(rs.getString("column4"));
            uDTO.setcolumn5(rs.getString("column5"));
            uDTO.setcomments(rs.getString("comments"));
            uDTO.setcurrency(rs.getString("currency"));

            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
            cmDTO.setId((rs.getInt("currency_id")));
            uDTO.setcurrencyid(cmDTO);

            uDTO.setcustomeraccountnumber(rs.getString("customer_account_number"));
            uDTO.setfileid(rs.getInt("file_id"));
            uDTO.setloadingdate(rs.getTimestamp("loading_date"));
            uDTO.setdisputeby(rs.getInt("dispute_by"));
            uDTO.setdisputedate(rs.getTimestamp("dispute_date"));
            uDTO.setdisputekey(rs.getInt("dispute_key"));
            uDTO.setmatchingtype(rs.getInt("matching_type"));
            uDTO.setnotespresented(rs.getString("notes_presented"));
            uDTO.setrecordtype(rs.getInt("record_type"));
            uDTO.setresponsecode(rs.getString("response_code"));

            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
            trcDTO.setId((rs.getInt("response_code_id")));
            uDTO.setresponsecodeid(trcDTO);

            uDTO.setresponsecodemaster(rs.getInt("response_code_master"));
            uDTO.setsettleddate(rs.getTimestamp("settled_date"));
            uDTO.setsettledflag(rs.getInt("settled_flag"));

            UsersDTOInter userDTO = DTOFactory.createUsersDTO();
            userDTO.setUserId((rs.getInt("settled_user")));
            uDTO.setsettleduser(userDTO);

            uDTO.setsettlementdate(rs.getTimestamp("settlement_date"));
            uDTO.settransactiondate(rs.getTimestamp("transaction_date"));
            uDTO.settransactionseqeunce(rs.getString("transaction_sequence"));
            uDTO.settransactionsequenceorderby(rs.getInt("transaction_sequence_order_by"));
            uDTO.settransactionstatus(rs.getString("transaction_status"));

            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
            tsDTO.setId((rs.getInt("transaction_status_id")));
            uDTO.settransactionstatusid(tsDTO);

            uDTO.settransactiontime(rs.getTimestamp("transaction_time"));
            uDTO.settransactiontype(rs.getString("transaction_type"));

            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
            ttDTO.setId((rs.getInt("transaction_type_id")));
            uDTO.settransactiontypeid(ttDTO);

            uDTO.settransactiontypemaster(rs.getInt("transaction_type_master"));

        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select $table.*,$table.card_no card_no_decrypt from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<DisputesDTOInter> uDTOL = new ArrayList<DisputesDTOInter>();
        DisputesDTOInter uDTO = DTOFactory.createDisputesDTO();
        while (rs.next()) {
            uDTO.setamount(rs.getInt("amount"));
            uDTO.setatmapplicationid(rs.getString("atm_application_id"));

            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
            amDTO.setId(rs.getInt("atm_id"));
            uDTO.setatmid(amDTO);

            uDTO.setcardno(rs.getString("card_no_decrypt"));
            uDTO.setcardnosuffix(rs.getInt("card_no_suffix"));
            uDTO.setcolumn1(rs.getString("column1"));
            uDTO.setcolumn2(rs.getString("column2"));
            uDTO.setcolumn3(rs.getString("column3"));
            uDTO.setcolumn4(rs.getString("column4"));
            uDTO.setcolumn5(rs.getString("column5"));
            uDTO.setcomments(rs.getString("comments"));
            uDTO.setcurrency(rs.getString("currency"));

            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
            cmDTO.setId((rs.getInt("currency_id")));
            uDTO.setcurrencyid(cmDTO);

            uDTO.setcustomeraccountnumber(rs.getString("customer_account_number"));
            uDTO.setfileid(rs.getInt("file_id"));
            uDTO.setloadingdate(rs.getTimestamp("loading_date"));
            uDTO.setdisputeby(rs.getInt("dispute_by"));
            uDTO.setdisputedate(rs.getTimestamp("dispute_date"));
            uDTO.setdisputekey(rs.getInt("dispute_key"));
            uDTO.setmatchingtype(rs.getInt("matching_type"));
            uDTO.setnotespresented(rs.getString("notes_presented"));
            uDTO.setrecordtype(rs.getInt("record_type"));
            uDTO.setresponsecode(rs.getString("response_code"));

            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
            trcDTO.setId((rs.getInt("response_code_id")));
            uDTO.setresponsecodeid(trcDTO);

            uDTO.setresponsecodemaster(rs.getInt("response_code_master"));
            uDTO.setsettleddate(rs.getTimestamp("settled_date"));
            uDTO.setsettledflag(rs.getInt("settled_flag"));

            UsersDTOInter userDTO = DTOFactory.createUsersDTO();
            userDTO.setUserId((rs.getInt("settled_user")));
            uDTO.setsettleduser(userDTO);

            uDTO.setsettlementdate(rs.getTimestamp("settlement_date"));
            uDTO.settransactiondate(rs.getTimestamp("transaction_date"));
            uDTO.settransactionseqeunce(rs.getString("transaction_sequence"));
            uDTO.settransactionsequenceorderby(rs.getInt("transaction_sequence_order_by"));
            uDTO.settransactionstatus(rs.getString("transaction_status"));

            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
            tsDTO.setId((rs.getInt("transaction_status_id")));
            uDTO.settransactionstatusid(tsDTO);

            uDTO.settransactiontime(rs.getTimestamp("transaction_time"));
            uDTO.settransactiontype(rs.getString("transaction_type"));

            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
            ttDTO.setId((rs.getInt("transaction_type_id")));
            uDTO.settransactiontypeid(ttDTO);

            uDTO.settransactiontypemaster(rs.getInt("transaction_type_master"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public void saveAll(String Query, Integer UserID) throws SQLException {
        String test = " select  atm_application_id, atm_id, transaction_type, transaction_type_id, currency, currency_id, transaction_status, transaction_status_id, response_code, response_code_id, transaction_date, transaction_sequence, card_no, amount, settlement_date, notes_presented, customer_account_number, transaction_sequence_order_by, transaction_time, matching_type, record_type, dispute_key, dispute_reason, settled_flag, settled_date, settled_user, comments, dispute_by, dispute_date, dispute_with_roles, column1, column2, column3, column4, column5, transaction_type_master, response_code_master, card_no_suffix," + UserID.toString() + " from disputes  where 1=1 " + Query;

        test = "insert into disputes_report (atm_application_id, atm_id, transaction_type, transaction_type_id, currency, currency_id, transaction_status, transaction_status_id, response_code, response_code_id, transaction_date, transaction_sequence, card_no, amount, settlement_date, notes_presented, customer_account_number, transaction_sequence_order_by, transaction_time, matching_type, record_type, dispute_key, dispute_reason, settled_flag, settled_date, settled_user, comments, dispute_by, dispute_date, dispute_with_roles, column1, column2, column3, column4, column5, transaction_type_master, response_code_master, card_no_suffix, user_id)" + test;

        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.executeUpdate(test);
        super.postUpdate(null, true);

    }

    public Clob FindFiletransaction(Object obj) throws Throwable {
        super.preSelect();
        DisputesDTOInter mdDTO = (DisputesDTOInter) obj;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select decrypt_blob(t.id) filetext from ATM_FILE t where t.id in "
                + "(select d.file_id from disputes d where "
                + "TRANSACTION_DATE =  to_date('$TransDate','dd.mm.yyyy hh24:mi:ss')"
                + "and ATM_APPLICATION_ID = '$AtmAppId' "
                + "and CARD_NO = '$Card' "
                + "and TRANSACTION_SEQUENCE = '$TranSeq' "
                + "and AMOUNT = '$Amount' "
                + "and CURRENCY = '$Curr' "
                + "and trim(RESPONSE_CODE) = trim('$RespCode') "
                + "and MATCHING_TYPE = '$MT'"
                + "and record_type = $rectype)";
        Clob elmsg = null;
        selectStat = selectStat.replace("$TransDate", "" + DateFormatter.changeDateAndTimeFormat(mdDTO.gettransactiondate()));
        selectStat = selectStat.replace("$AtmAppId", "" + mdDTO.getatmapplicationid());
        selectStat = selectStat.replace("$Card", "" + mdDTO.getcardno());
        selectStat = selectStat.replace("$TranSeq", "" + mdDTO.gettransactionseqeunce());
        selectStat = selectStat.replace("$Amount", "" + mdDTO.getamount());
        selectStat = selectStat.replace("$Curr", "" + mdDTO.getcurrency());
        selectStat = selectStat.replace("$RespCode", "" + mdDTO.getresponsecode());
        selectStat = selectStat.replace("$MT", "" + mdDTO.getmatchingtype());
        selectStat = selectStat.replace("$rectype", "" + mdDTO.getrecordtype());
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            elmsg = rs.getClob("filetext");
        }
        super.postSelect(rs);
        return elmsg;
    }

    public Clob FindFiletransaction_other(Object obj) throws Throwable {
        super.preSelect();
        DisputesDTOInter mdDTO = (DisputesDTOInter) obj;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select decrypt_blob(t.id) filetext from ATM_FILE t where t.id in "
                + "(select d.file_id from disputes d where "
                + "TRANSACTION_DATE =  to_date('$TransDate','dd.mm.yyyy hh24:mi:ss')"
                + "and ATM_APPLICATION_ID = '$AtmAppId' "
                + "and CARD_NO = '$Card' "
                + "and TRANSACTION_SEQUENCE = '$TranSeq' "
                + "and AMOUNT = '$Amount' "
                + "and CURRENCY = '$Curr' "
                + "and trim(RESPONSE_CODE) = '$RespCode' "
                + "and MATCHING_TYPE = '$MT'"
                + "and record_type = $rectype)";
        Clob elmsg = null;
        selectStat = selectStat.replace("$TransDate", "" + DateFormatter.changeDateAndTimeFormat(mdDTO.gettransactiondate()));
        selectStat = selectStat.replace("$AtmAppId", "" + mdDTO.getatmapplicationid());
        selectStat = selectStat.replace("$Card", "" + mdDTO.getcardno());
        selectStat = selectStat.replace("$TranSeq", "" + mdDTO.gettransactionseqeunce());
        selectStat = selectStat.replace("$Amount", "" + mdDTO.getamount());
        selectStat = selectStat.replace("$Curr", "" + mdDTO.getcurrency());
        selectStat = selectStat.replace("$RespCode", "" + mdDTO.getresponsecode());
        selectStat = selectStat.replace("$MT", "" + mdDTO.getmatchingtype());
        selectStat = selectStat.replace("$rectype", "" + mdDTO.getrecordtype());

        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            elmsg = rs.getClob("filetext");
        }
        super.postSelect(rs);
        return elmsg;
    }

    public String Findtexttransaction(Object obj) throws Throwable {
        super.preSelect();
        DisputesDTOInter mdDTO = (DisputesDTOInter) obj;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select transaction_data transaction_data  from disputes where  disputes.dispute_key_uk = '$disputekeyuk' ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        String elmsg = null;
        //selectStat = selectStat.replace("$disputekeyuk", "" + mdDTO.getDisputekeyuk());
        selectStat = selectStat.replace("$record_Type", "" + mdDTO.getrecordtype());
        selectStat = selectStat.replace("$disputekeyuk", "" + mdDTO.getDisputekeyuk());

        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            // File data = new File("C:\\a.txt");
            elmsg = rs.getString("transaction_data");
            //clobToString(elmsg);
            /*      Reader reader = rs.getCharacterStream("transaction_data");
             FileWriter writer = new FileWriter(data);
             char[] buffer = new char[1];
             while (reader.read(buffer) > 0) {
             writer.write(buffer);            
             }
             writer.close();*/
        }
        super.postSelect(rs);
        return elmsg;
    }

    public String Findtexttransaction_other(Object obj) throws Throwable {
        super.preSelect();
        DisputesDTOInter mdDTO = (DisputesDTOInter) obj;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select transaction_data transaction_data  from disputes   where dispute_key = '$disputekey' and record_type = '$record_Type' ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        String elmsg = null;
        //selectStat = selectStat.replace("$disputekeyuk", "" + mdDTO.getDisputekeyuk());
        selectStat = selectStat.replace("$disputekey", "" + mdDTO.getdisputekey());

        selectStat = selectStat.replace("$record_Type", "" + mdDTO.getrecordtype());

        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            // File data = new File("C:\\a.txt");
            elmsg = rs.getString("transaction_data");
            // clobToString( elmsg );
  /*      Reader reader = rs.getCharacterStream("transaction_data");
             FileWriter writer = new FileWriter(data);
             char[] buffer = new char[1];
             while (reader.read(buffer) > 0) {
             writer.write(buffer);
             }
             writer.close();*/
        }
        super.postSelect(rs);
        return elmsg;
    }

    private String clobToString(Clob data) {
        StringBuilder sb = new StringBuilder();
        try {
            Reader reader;
            try {
                reader = data.getCharacterStream();
                BufferedReader br = new BufferedReader(reader);
                String line;
                while (null != (line = br.readLine())) {

                }
                br.close();
            } catch (SQLException ex) {
                Logger.getLogger(DisputesDAO.class.getName()).log(Level.SEVERE, null, ex);
                return "Fail";
            }
        } catch (IOException ex) {
            Logger.getLogger(DisputesDAO.class.getName()).log(Level.SEVERE, null, ex);
            return "Fail";
        }

        return sb.toString();
    }

    public Object findAnotherSide(Object obj) throws Throwable {
        super.preSelect();
        Integer HaveDis = 0;
        // databse insert statement usinf conn pool getConn().createStatement();
        DisputesDTOInter mdDTO = (DisputesDTOInter) obj;
        if (mdDTO.getdisputekey() != 0) {
            String selectStat = "select $table.*,$table.card_no card_no_decrypt,get_disputes_reason(dispute_key_uk,dispute_reason) ReasonToDisp from $table where dispute_key = $dispute_key and record_type <> $record_Type";
            selectStat = selectStat.replace("$table", "" + super.getTableName());
            selectStat = selectStat.replace("$dispute_key", "" + mdDTO.getdisputekey());
            selectStat = selectStat.replace("$record_Type", "" + mdDTO.getrecordtype());
            ResultSet rs = executeQueryReport(selectStat);
            DisputesDTOInter uDTO = DTOFactory.createDisputesDTO();
            while (rs.next()) {

                uDTO.setamount(rs.getInt("amount"));
                uDTO.setatmapplicationid(rs.getString("atm_application_id"));
                uDTO.setdisputereason(rs.getString("ReasonToDisp"));
                AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
                amDTO.setId(rs.getInt("atm_id"));
                uDTO.setatmid(amDTO);

                uDTO.setcardno(rs.getString("card_no_decrypt"));
                uDTO.setcardnosuffix(rs.getInt("card_no_suffix"));
                uDTO.setcolumn1(rs.getString("column1"));
                uDTO.setcolumn2(rs.getString("column2"));
                uDTO.setcolumn3(rs.getString("column3"));
                uDTO.setcolumn4(rs.getString("column4"));
                uDTO.setcolumn5(rs.getString("column5"));
                uDTO.setcomments(rs.getString("comments"));
                uDTO.setcurrency(rs.getString("currency"));

                CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
                cmDTO.setId((rs.getInt("currency_id")));
                uDTO.setcurrencyid(cmDTO);

                uDTO.setcustomeraccountnumber(rs.getString("customer_account_number"));
                uDTO.setfileid(rs.getInt("file_id"));
                uDTO.setloadingdate(rs.getTimestamp("loading_date"));
                uDTO.setdisputeby(rs.getInt("dispute_by"));
                uDTO.setdisputedate(rs.getTimestamp("dispute_date"));
                uDTO.setdisputekey(rs.getInt("dispute_key"));
                uDTO.setDisputekeyuk(rs.getString("dispute_key_uk"));
                uDTO.setmatchingtype(rs.getInt("matching_type"));
                uDTO.setnotespresented(rs.getString("notes_presented"));
                uDTO.setrecordtype(rs.getInt("record_type"));
                uDTO.setresponsecode(rs.getString("response_code"));

                TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
                trcDTO.setId((rs.getInt("response_code_id")));
                uDTO.setresponsecodeid(trcDTO);

                uDTO.setresponsecodemaster(rs.getInt("response_code_master"));
                uDTO.setsettleddate(rs.getTimestamp("settled_date"));
                uDTO.setsettledflag(rs.getInt("settled_flag"));

                UsersDTOInter userDTO = DTOFactory.createUsersDTO();
                userDTO.setUserId((rs.getInt("settled_user")));
                uDTO.setsettleduser(userDTO);

                uDTO.setsettlementdate(rs.getTimestamp("settlement_date"));
                uDTO.settransactiondate(rs.getTimestamp("transaction_date"));
                uDTO.settransactionseqeunce(rs.getString("transaction_sequence"));
                uDTO.settransactionsequenceorderby(rs.getInt("transaction_sequence_order_by"));
                uDTO.settransactionstatus(rs.getString("transaction_status"));

                TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
                tsDTO.setId((rs.getInt("transaction_status_id")));
                uDTO.settransactionstatusid(tsDTO);

                uDTO.settransactiontime(rs.getTimestamp("transaction_time"));
                uDTO.settransactiontype(rs.getString("transaction_type"));

                TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
                ttDTO.setId((rs.getInt("transaction_type_id")));
                uDTO.settransactiontypeid(ttDTO);

                uDTO.settransactiontypemaster(rs.getInt("transaction_type_master"));
                uDTO.setdisputereason(rs.getString("dispute_reason"));
                uDTO.setreasontodisp(rs.getString("dispute_reason"));
                HaveDis = 1;
            }
            if (HaveDis == 1) {
                super.postSelect(rs);
                return uDTO;
            } else {
                uDTO.setdisputekey(0);
                return uDTO;
            }
        } else {
            return null;
        }
    }

    public Object findReport(String fields, String whereClause, DisputesDTOInter[] dDTOArr) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select decode(record_type,1,'J',2,'S',3,'H')record_type,"
                + " decode(settled_flag,1,'Y',2,'N')settled_flag,"
                + " comments,"
                + " atm_application_id,"
                + " currency,"
                + " (select unit_number from atm_machine m where m.application_id = atm_application_id)unit_id,"
                + " $field from $table disputes where 1=1 $whereClause";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$whereClause", "" + whereClause);
        selectStat = selectStat.replace("$field", "" + fields);
        ResultSet rs = executeQuery(selectStat);
        super.postSelect(rs);
        return rs;
    }

    public Object[] customSearch(String whereClause, String field, String index) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select $index "
                + "decode(corrective_amount,null,amount,corrective_amount)corrective_amount ,"
                + "rownum elrow,COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5,"
                + "CUSTOMER_ACCOUNT_NUMBER,card_no,TRANSACTION_DATE,ATM_APPLICATION_ID\n"
                + ",AMOUNT,CURRENCY,RESPONSE_CODE,matching_type,record_type,dispute_key\n"
                + ",dispute_key_uk,corrective_entry_flag,settled_flag,comments,"
                + "TRANSACTION_SEQUENCE,TRANSACTION_SEQUENCE_ORDER_BY,TRANSACTION_TYPE,\n"
                + "SETTLEMENT_DATE,CARD_NO_SUFFIX,TRANSACTION_STATUS,NOTES_PRESENTED,"
                + "is_black_list($table.card_no) blacklist, "
                + "get_disputes_reason(dispute_key_uk,dispute_reason) ReasonToDisp "
                + "from $table  where 1=1 $whereClause";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$whereClause", "" + whereClause);

        selectStat = selectStat.replace("$index", "/*+ parallel(4) */ " + index);

        fullamount = new BigDecimal(BigInteger.ZERO);
        long start = System.currentTimeMillis();
        Class.forName("oracle.jdbc.OracleDriver");
        Connection conn = CoonectionHandler.getInstance().getConnection();
        PreparedStatement stmt = conn.prepareStatement(selectStat, java.sql.ResultSet.TYPE_FORWARD_ONLY,
                java.sql.ResultSet.CONCUR_READ_ONLY);
        stmt.setFetchSize(1000000);
        ResultSet rs = stmt.executeQuery(selectStat);
        System.out.println(selectStat);
        System.out.println("Fetching result");
        //ResultSet rs = executeQuery(selectStat);
        long end = System.currentTimeMillis();
        NumberFormat formatter = new DecimalFormat("#0.00000");
        System.out.println("Execution time in DB: " + formatter.format((end - start) / 1000d) + " seconds");
        start = System.currentTimeMillis();
        List<DisputesDTOInter> uDTOL = new ArrayList<DisputesDTOInter>();

        while (rs.next()) {
            DisputesDTOInter uDTO = DTOFactory.createDisputesDTO();
            uDTO.setrownum(rs.getString("elrow"));
            uDTO.setCorrectiveamount(rs.getString("corrective_amount"));
            uDTO.setBlacklist(rs.getInt("blacklist"));
            uDTO.setdisputereason(rs.getString("ReasonToDisp"));
            uDTO.setcomments(rs.getString("comments"));
            uDTO.setsettledflag(rs.getInt("settled_flag"));
            uDTO.setcorrectivestatus(rs.getInt("corrective_entry_flag"));
            uDTO.setDisputekeyuk(rs.getString("dispute_key_uk"));
            uDTO.setdisputekey(rs.getInt("dispute_key"));
            uDTO.setrecordtype(rs.getInt("record_type"));
            uDTO.setmatchingtype(rs.getInt("matching_type"));
            uDTO.setresponsecode(rs.getString("RESPONSE_CODE"));
            uDTO.setcurrency(rs.getString("CURRENCY"));
            uDTO.setamount(rs.getInt("AMOUNT"));
            uDTO.setatmapplicationid(rs.getString("ATM_APPLICATION_ID"));
            uDTO.settransactiondate(rs.getTimestamp("TRANSACTION_DATE"));
            uDTO.setcardno(rs.getString("card_no"));
            uDTO.setcustomeraccountnumber(rs.getString("CUSTOMER_ACCOUNT_NUMBER"));
            uDTO.settransactionseqeunce(rs.getString("TRANSACTION_SEQUENCE"));
            uDTO.settransactionsequenceorderby(rs.getInt("TRANSACTION_SEQUENCE_ORDER_BY"));
            uDTO.settransactiontype(rs.getString("TRANSACTION_TYPE"));
            uDTO.setsettlementdate(rs.getTimestamp("SETTLEMENT_DATE"));
            uDTO.setcardnosuffix(rs.getInt("CARD_NO_SUFFIX"));
            uDTO.settransactionstatus(rs.getString("TRANSACTION_STATUS"));
            uDTO.setnotespresented(rs.getString("NOTES_PRESENTED"));
            uDTO.setcolumn1(rs.getString("COLUMN1"));
            uDTO.setcolumn2(rs.getString("COLUMN2"));
            uDTO.setcolumn3(rs.getString("COLUMN3"));
            uDTO.setcolumn4(rs.getString("COLUMN4"));
            uDTO.setcolumn5(rs.getString("COLUMN5"));
            fullamount = fullamount.add(new BigDecimal(uDTO.getamount()));

//            uDTO.setExportuser(rs.getString("export"));
//            uDTO.setCorrectiveuser(rs.getString("corrective"));
//            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
//            amDTO.setId(rs.getInt("ATM_ID"));
//            uDTO.setatmid(amDTO);
//            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
//            cmDTO.setId((rs.getInt("CURRENCY_ID")));
//            uDTO.setcurrencyid(cmDTO);
//            uDTO.setfileid(rs.getInt("file_id"));
//            uDTO.setloadingdate(rs.getTimestamp("loading_date"));
//            uDTO.setdisputeby(rs.getInt("dispute_by"));
//            uDTO.setdisputedate(rs.getTimestamp("dispute_date"));
//            uDTO.setCorrectivedate(rs.getTimestamp("corrective_date"));
//            uDTO.setExportdate(rs.getTimestamp("export_date"));
//            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
//            trcDTO.setId((rs.getInt("RESPONSE_CODE_ID")));
//            uDTO.setresponsecodeid(trcDTO);
//            uDTO.setresponsecodemaster(rs.getInt("RESPONSE_CODE_MASTER"));
//            uDTO.setsettleddate(rs.getTimestamp("settled_date"));
//            UsersDTOInter userDTO = DTOFactory.createUsersDTO();
//            userDTO.setUserId((rs.getInt("settled_user")));
//            uDTO.setsettleduser(userDTO);
//            uDTO.setatmname(rs.getString("atmname"));
//            uDTO.setsettlementdate(rs.getTimestamp("SETTLEMENT_DATE"));
//            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
//            tsDTO.setId((rs.getInt("TRANSACTION_STATUS_ID")));
//            uDTO.settransactionstatusid(tsDTO);
//            uDTO.settransactiontime(rs.getTimestamp("TRANSACTION_TIME"));
//            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
//            ttDTO.setId((rs.getInt("TRANSACTION_TYPE_ID")));
//            uDTO.settransactiontypeid(ttDTO);
//            uDTO.setreasontodisp(rs.getString("dispute_reason"));
//            uDTO.settransactiontypemaster(rs.getInt("TRANSACTION_TYPE_MASTER"));
            uDTOL.add(uDTO);
        }
        rs.close();
        stmt.close();
        if (conn != null) {
            try {
                conn.close();
                //   System.out.println("Database connection terminated");
            } catch (Exception e) { /* ignore close errors */ }
        }
        //super.postSelect(rs);
        end = System.currentTimeMillis();
        System.out.println("Execution time in Java: " + formatter.format((end - start) / 1000d) + " seconds");
        Object[] results = new Object[2];
        results[0] = uDTOL;
        results[1] = fullamount;
        return results;
    }
    private BigDecimal fullamount = null;

    public Object findTotalAmount(String whereClause) throws Throwable {
        return fullamount;
    }

    public Object countDisputesAndNotSettled(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        int userId = (Integer) obj[0];
        String selectStat = "select count(*)from disputes m "
                + " where m.transaction_date between trunc(sysdate) - 1 and sysdate "
                + " and m.atm_id in (select a.atm_id from user_atm a where a.user_id = $user) "
                + " and m.settled_flag = 2";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$user", "" + userId);
        ResultSet rs = executeQuery(selectStat);
        int count = 0;
        while (rs.next()) {
            count = rs.getInt(1);
            //  count = 62000;
        }
        super.postSelect(rs);
        return count;
    }

    public Object countDisputesAndSettled(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        int userId = (Integer) obj[0];
        String selectStat = "select count(*)from disputes m "
                + " where m.transaction_date between trunc(sysdate) - 1 and sysdate "
                + " and m.atm_id in (select a.atm_id from user_atm a where a.user_id = $user) "
                + " and m.settled_flag = 1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$user", "" + userId);
        ResultSet rs = executeQuery(selectStat);
        int count = 0;
        while (rs.next()) {
            count = rs.getInt(1);
            // count = 81000;
        }
        super.postSelect(rs);
        return count;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public Object countDashBoard(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        int userId = (Integer) obj[0];
        String from = (String) obj[1];
        String to = (String) obj[2];
        String selectStat = "select (select/*+INDEX(m DISPUTES_GRAPH_INDEX)*/ count(*)from disputes m where m.transaction_date between trunc(sysdate)  $from and sysdate  $to and m.atm_id in (select a.atm_id from user_atm a where a.user_id = $user) and m.settled_flag = 2)NotSettled,(select /*+INDEX(m DISPUTES_GRAPH_INDEX)*/ count(*)from disputes m where m.transaction_date between trunc(sysdate)  $from and sysdate  $to and m.atm_id in (select a.atm_id from user_atm a where a.user_id = $user)and m.settled_flag = 1) Settled,(select /*+INDEX(m MATCHED_DATA_GRAPH_INDEX)*/ count(*) from matched_data m  where m.transaction_date between trunc(sysdate)  $from and sysdate $to and m.atm_id in (select a.atm_id from user_atm a where a.user_id = $user))Matched from dual";
        //String selectStat = "select (select count(*)from disputes m where m.transaction_date between trunc(sysdate) - 1 and sysdate and m.atm_id in (select a.atm_id from user_atm a where a.user_id = $user) and m.settled_flag = 2)NotSettled,(select count(*)from disputes m where m.transaction_date between trunc(sysdate) - 1 and sysdate and m.atm_id in (select a.atm_id from user_atm a where a.user_id = $user) and m.settled_flag = 1) Settled,(select count(*)from matched_data m where m.transaction_date between trunc(sysdate) - 1 and sysdate and m.atm_id in (select a.atm_id from user_atm a where a.user_id = $user))Matched from dual";
        selectStat = selectStat.replace("$user", "" + userId);
        selectStat = selectStat.replace("$from", "" + to);
        selectStat = selectStat.replace("$to", "" + from);
        ResultSet rs = executeQuery(selectStat);
        List<UserWorkDTOInter> uwDTOL = new ArrayList<UserWorkDTOInter>();
        while (rs.next()) {
            UserWorkDTOInter uwDTONotSettled = DTOFactory.createUserWorkDTO();
            UserWorkDTOInter uwDTOSettled = DTOFactory.createUserWorkDTO();
            UserWorkDTOInter uwDTOMatched = DTOFactory.createUserWorkDTO();
            uwDTONotSettled.setName("Not Settled");
            uwDTONotSettled.setValue(rs.getInt(1));
            uwDTOL.add(uwDTONotSettled);
            uwDTOSettled.setName("Settled");
            uwDTOSettled.setValue(rs.getInt(2));
            uwDTOL.add(uwDTOSettled);
            uwDTOMatched.setName("Matched");
            uwDTOMatched.setValue(rs.getInt(3));
            uwDTOL.add(uwDTOMatched);
        }
        super.postSelect(rs);
        return uwDTOL;
    }

    public static void main(String[] args) throws Throwable {
        DisputesDAOInter dDAO = DAOFactory.createDisputesDAO(null);
        int mDTOL = (Integer) dDAO.countDisputesAndNotSettled(1);

    }
}
//
