/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.TransactionResponseMasterDTOInter;

/**
 *
 * @author Administrator
 */
public interface TransactionResponseCodeDAOInter {

    Object findByMaster(TransactionResponseMasterDTOInter trmDTO)throws Throwable;
int getDefFlagCount(int trmDTO,int id)throws Throwable;
    Object delete(Object... obj) throws Throwable;

    Object find(Object... obj) throws Throwable;

    Object findAll() throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
