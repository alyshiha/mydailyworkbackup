/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.BlackListedCardDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class BlackListedCardDAO extends BaseDAO implements BlackListedCardDAOInter {

    protected BlackListedCardDAO() {
        super();
        super.setTableName("black_listed_card");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        BlackListedCardDTOInter uDTO = (BlackListedCardDTOInter) obj[0];
        String insertStat = "insert into $table values ('$cardNo', '$customerNo' , '$comments', to_date('$blackListedDate','dd.MM.yyyy hh24:mi:ss'))";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$cardNo", "" + uDTO.getCardNo());
        insertStat = insertStat.replace("$customerNo", "" + uDTO.getCustomerNo());
        insertStat = insertStat.replace("$comments", "" + uDTO.getComments());
        insertStat = insertStat.replace("$blackListedDate", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getBlackListedDate()));
        super.executeUpdate(insertStat);
        super.postUpdate("Add " + uDTO.getCardNo() + " card no. to black list", false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        BlackListedCardDTOInter uDTO = (BlackListedCardDTOInter) obj[0];
        String oldCardNo = (String) obj[1];
        String updateStat = "update $table set card_no = '$cardNo', customer_no = '$customerNo', comments = '$comments'"
                + ", black_listed_date = to_date('$blackListedDate','dd.MM.yyyy hh24:mi:ss') where card_no = '$oldCardNo'";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$cardNo", "" + uDTO.getCardNo());
        updateStat = updateStat.replace("$customerNo", "" + uDTO.getCustomerNo());
        updateStat = updateStat.replace("$comments", "" + uDTO.getComments());
        updateStat = updateStat.replace("$blackListedDate", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getBlackListedDate()));
        updateStat = updateStat.replace("$oldCardNo", "" + oldCardNo);
        super.executeUpdate(updateStat);
        super.postUpdate("Update " + oldCardNo + " card no. to " + uDTO.getCardNo() + " in black list", false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        BlackListedCardDTOInter uDTO = (BlackListedCardDTOInter) obj[0];
        String deleteStat = "delete from $table where card_no = '$id'";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getCardNo());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete " + uDTO.getCardNo() + " Card no. from black list", false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select $table.*,$table.card_no card_no_decrypt from $table where card_no = '$id'";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        BlackListedCardDTOInter uDTO = DTOFactory.createBlackListedCardDTO();
        while (rs.next()) {
            uDTO.setBlackListedDate(rs.getTimestamp("black_listed_date"));
            uDTO.setCardNo(rs.getString("card_no_decrypt"));
            uDTO.setComments(rs.getString("comments"));
            uDTO.setCustomerNo(rs.getString("customer_no"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select $table.*,$table.card_no card_no_decrypt from $table order by card_no";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<BlackListedCardDTOInter> uDTOL = new ArrayList<BlackListedCardDTOInter>();
        while (rs.next()) {
            BlackListedCardDTOInter uDTO = DTOFactory.createBlackListedCardDTO();
            uDTO.setBlackListedDate(rs.getTimestamp("black_listed_date"));
            uDTO.setCardNo(rs.getString("card_no_decrypt"));
            uDTO.setComments(rs.getString("comments"));
            uDTO.setCustomerNo(rs.getString("customer_no"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public static void main(String[] args) throws Throwable {
    }
}
