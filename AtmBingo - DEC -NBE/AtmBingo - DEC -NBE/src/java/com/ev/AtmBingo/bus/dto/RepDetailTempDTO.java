/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public class RepDetailTempDTO implements RepDetailTempDTOInter {
    private Integer repid;
    private Integer cassetteid;
    private String cassettename;
    private Integer remaining;
    private Integer totalamount;

    @Override
    public Integer getRepid() {
        return repid;
    }

    @Override
    public void setRepid(Integer repid) {
        this.repid = repid;
    }

    @Override
    public Integer getCassetteid() {
        return cassetteid;
    }

    @Override
    public void setCassetteid(Integer cassetteid) {
        this.cassetteid = cassetteid;
    }

    @Override
    public String getCassettename() {
        return cassettename;
    }

    @Override
    public void setCassettename(String cassettename) {
        this.cassettename = cassettename;
    }

    @Override
    public Integer getRemaining() {
        return remaining;
    }

    @Override
    public void setRemaining(Integer remaining) {
        this.remaining = remaining;
    }

    @Override
    public Integer getTotalamount() {
        return totalamount;
    }

    @Override
    public void setTotalamount(Integer totalamount) {
        this.totalamount = totalamount;
    }
    
}
