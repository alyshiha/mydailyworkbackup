/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface TransStatDTOInter extends Serializable  {

    String getDisputescount();

    String getApplicationidcount();

    void setApplicationidcount(String applicationidcount);

    void setDisputescount(String disputescount);

    String getErrorscount();

    void setErrorscount(String errorscount);

    String getMatchedcount();

    String getMissingsidecount();

    void setMatchedcount(String matchedcount);

    void setMissingsidecount(String missingsidecount);

    String getApplicationid();

    String getDisputes();

    String getErrors();

    String getMatched();

    String getMissingside();

    void setApplicationid(String applicationid);

    void setDisputes(String disputes);

    void setErrors(String errors);

    void setMatched(String matched);

    void setMissingside(String missingside);
}
