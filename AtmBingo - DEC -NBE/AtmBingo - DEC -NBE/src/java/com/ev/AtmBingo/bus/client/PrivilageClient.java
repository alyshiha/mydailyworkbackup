/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.ProfileDetailsBOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ItemBingoDTOInter;
import com.ev.AtmBingo.bus.dto.MenuLabelDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileMenuDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileMenuItemDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.codehaus.groovy.runtime.metaclass.NewMetaMethod;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author ISLAM
 */
public class PrivilageClient extends BaseBean  implements Serializable{

    private List<ProfileDTOInter> profileList;
    private ProfileDTOInter selectedProfile, upAttr, delAttr, newProfile;
    private List<MenuLabelDTOInter> profileMenuList;
    private List<ItemBingoDTOInter> profileItemsList, currentItemModes;
    private ItemBingoDTOInter selectedItem;
    private MenuLabelDTOInter selectedMenu;
    private TreeNode profileMenu;
    private ProfileDetailsBOInter profileBO;
    private String message;
    private Boolean showAdd, showConfirm;

    public List<ItemBingoDTOInter> getCurrentItemModes() {
        return currentItemModes;
    }

    public void setCurrentItemModes(List<ItemBingoDTOInter> currentItemModes) {
        this.currentItemModes = currentItemModes;
    }

    public List<ItemBingoDTOInter> getProfileItemsList() {
        return profileItemsList;
    }

    public void setProfileItemsList(List<ItemBingoDTOInter> profileItemsList) {
        this.profileItemsList = profileItemsList;
    }

    public ItemBingoDTOInter getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(ItemBingoDTOInter selectedItem) {
        this.selectedItem = selectedItem;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public ProfileDTOInter getNewProfile() {
        return newProfile;
    }

    public void setNewProfile(ProfileDTOInter newProfile) {
        this.newProfile = newProfile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProfileDTOInter getDelAttr() {
        return delAttr;
    }

    public void setDelAttr(ProfileDTOInter delAttr) {
        this.delAttr = delAttr;
    }

    public ProfileDTOInter getUpAttr() {
        return upAttr;
    }

    public void setUpAttr(ProfileDTOInter upAttr) {
        this.upAttr = upAttr;
    }

    public List<ProfileDTOInter> getProfileList() {
        return profileList;
    }

    public void setProfileList(List<ProfileDTOInter> profileList) {
        this.profileList = profileList;
    }

    public TreeNode getProfileMenu() {
        return profileMenu;
    }

    public void setProfileMenu(TreeNode profileMenu) {
        this.profileMenu = profileMenu;
    }

    public List<MenuLabelDTOInter> getProfileMenuList() {
        return profileMenuList;
    }

    public void setProfileMenuList(List<MenuLabelDTOInter> profileMenuList) {
        this.profileMenuList = profileMenuList;
    }

    public MenuLabelDTOInter getSelectedMenu() {
        return selectedMenu;
    }

    public void setSelectedMenu(MenuLabelDTOInter selectedMenu) {
        this.selectedMenu = selectedMenu;
    }

    public ProfileDTOInter getSelectedProfile() {
        return selectedProfile;
    }

    public void setSelectedProfile(ProfileDTOInter selectedProfile) {
        this.selectedProfile = selectedProfile;
    }

    /**
     * Creates a new instance of PrivilageClient1
     */
    public PrivilageClient() throws Throwable {
        super();
        super.GetAccess();
        profileBO = BOFactory.createProfileDetailsBO(null);
        profileList = (List<ProfileDTOInter>) profileBO.getProfiles(super.getRestrected());
        profileMenu = new DefaultTreeNode();
        profileMenu = profileBO.getMenuTree(super.getRestrected());
        profileItemsList = (List<ItemBingoDTOInter>) profileBO.getAllItemBingo();
        showAdd = true;
        showConfirm = false;
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void onRowSelect1(SelectEvent e) throws Throwable {
        selectedProfile = (ProfileDTOInter) e.getObject();
        profileMenuList = selectedProfile.getMlDTOL();
        resetVars();
    }

    public void onRowUnSelect1(UnselectEvent e) {
        profileMenuList.clear();
        selectedProfile = null;
        resetVars();
    }

    public void onRowSelect2(SelectEvent e) throws Throwable {
        selectedMenu = (MenuLabelDTOInter) e.getObject();
        currentItemModes = (List<ItemBingoDTOInter>) profileBO.getItemBingo(selectedProfile.getProfileId(), selectedMenu.getMenuId());
        resetVars();
    }

    public void onRowUnSelect2(UnselectEvent e) {
        currentItemModes.clear();
        selectedMenu = null;
        resetVars();
    }

    public void fillUp(ActionEvent e) {
        upAttr = (ProfileDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    private Boolean ifexist(String name) {
        int temp = 0;
        for (ProfileDTOInter prof : profileList) {
            if (prof.getProfileName().equals(name)) {
                temp = temp + 1;
                if (temp == 2) {
                    return Boolean.TRUE;
                }
            }
        }
        return Boolean.FALSE;
    }

    public void updateProfile(ActionEvent e) {
        try {
            if (ifexist(upAttr.getProfileName())) {
                message = "Please Enter Valid Data";
                return;
            }
            if (showAdd == false) {
                message = super.showMessage(CONF_FIRST);
            } else if (upAttr.getProfileName() == "") {

                message = super.showMessage(REQ_FIELD);
            } else {
                String[] inputs = {upAttr.getProfileName()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    profileBO.editProfile(upAttr, profileBO.UPDATE);
                    message = super.showMessage(SUCC);
                } else {
                    message = "Please Enter Valid Data";
                }

            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void deleteProfile(ActionEvent e) {
        try {
            if (profileBO.ProfileHasUser(delAttr)) {
                message = "Can't delete, there are users under this profile";
            } else {
                String[] inputs = {delAttr.getProfileName()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    profileList.remove(delAttr);
                    profileBO.editProfile(delAttr, profileBO.DELETE);
                    message = super.showMessage(SUCC);
                    showAdd = true;
                    showConfirm = false;
                } else {
                    message = "Please Enter Valid Data";
                }

            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void fillDel(ActionEvent e) {
        delAttr = (ProfileDTOInter) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void addProfile() {
        try {
            if (profileList == null) {
                profileList = new ArrayList<ProfileDTOInter>();
            }
            newProfile = DTOFactory.createProfileDTO();
            profileList.add(newProfile);
            showAdd = false;
            showConfirm = true;
            resetVars();
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }

    }

    public void addConfirm() {
        try {
            if (ifexist(newProfile.getProfileName())) {
                message = "Please Enter Valid Data";
                return;
            }
            if (newProfile.getProfileName() == "") {
                message = super.showMessage(REQ_FIELD);
            } else {
                String[] inputs = {newProfile.getProfileName()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (!found) {
                    profileBO.editProfile(newProfile, profileBO.INSERT);
                    message = super.showMessage(SUCC);
                    showAdd = true;
                    showConfirm = false;
                } else {
                    message = "Please Enter Valid Data";
                }

            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void fillItemMc(ActionEvent e) {
        selectedMenu = (MenuLabelDTOInter) e.getComponent().getAttributes().get("updateRowMenuItem");
        resetVars();
    }

    public void transferMenu(ActionEvent e) {
        selectedMenu = (MenuLabelDTOInter) e.getComponent().getAttributes().get("updateRowMenu");
        resetVars();
    }

    public void doTransfer(ActionEvent e) {
        try {
            if (selectedProfile == null) {
                message = "Please select a profile first";
            } else {

                ProfileMenuDTOInter holdMenu = DTOFactory.createProfileMenuDTO();
                holdMenu.setMenuId(selectedMenu.getMenuId());
                holdMenu.setProfileId(selectedProfile.getProfileId());
                profileBO.editProfileMenu(holdMenu, profileBO.INSERT);
                profileMenuList.add(selectedMenu);
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
        }
    }

    public void doTransferBack(ActionEvent e) {
        try {

            ProfileMenuDTOInter holdMenu = DTOFactory.createProfileMenuDTO();
            holdMenu.setMenuId(selectedMenu.getMenuId());
            holdMenu.setProfileId(selectedProfile.getProfileId());
            profileBO.editProfileMenu(holdMenu, profileBO.DELETE);
            profileMenuList.remove(selectedMenu);
            resetVars();
        } catch (Throwable ex) {
        }
    }

    //-----------------------//
    public void fillItemBingo(ActionEvent e) {
        selectedItem = (ItemBingoDTOInter) e.getComponent().getAttributes().get("updateRowMenuItem");
        resetVars();
    }

    public void transferMenuBingo(ActionEvent e) {
        selectedItem = (ItemBingoDTOInter) e.getComponent().getAttributes().get("updateRowMenuItemBingo");
        resetVars();
    }

    public void doTransferBingo(ActionEvent e) {
        try {
            if (currentItemModes == null) {
                currentItemModes = new ArrayList<ItemBingoDTOInter>();
            }
            ProfileMenuItemDTOInter holdMenu = DTOFactory.createProfileMenuItemDTO();
            holdMenu.setMenuId(selectedMenu.getMenuId());
            holdMenu.setProfileId(selectedProfile.getProfileId());
            holdMenu.setItemId(selectedItem.getItemId());
            profileBO.editProfileMenuItem(holdMenu, profileBO.INSERT);
            currentItemModes.add(selectedItem);
            message = super.showMessage(SUCC);

        } catch (Throwable ex) {
        }
    }

    public void doTransferBackBingo(ActionEvent e) {
        try {
            ProfileMenuItemDTOInter holdItem = DTOFactory.createProfileMenuItemDTO();
            holdItem.setMenuId(selectedMenu.getMenuId());
            holdItem.setProfileId(selectedProfile.getProfileId());
            holdItem.setItemId(selectedItem.getItemId());
            profileBO.editProfileMenuItem(holdItem, profileBO.DELETE);
            currentItemModes.remove(selectedItem);
            message = super.showMessage(SUCC);
        } catch (Throwable ex) {
        }
    }

    public void resetVars() {
        message = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
