/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface TransactionStatusDTOInter extends Serializable {

    String getDescription();

    int getId();

    String getName();

    void setDescription(String description);

    void setId(int id);

    void setName(String name);

}
