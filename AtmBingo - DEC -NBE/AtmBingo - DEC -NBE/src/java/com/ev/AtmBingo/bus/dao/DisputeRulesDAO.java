/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DisputeRulesDTOInter;
import com.ev.AtmBingo.bus.dto.DisputesDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class DisputeRulesDAO extends BaseDAO implements DisputeRulesDAOInter {

    protected DisputeRulesDAO() {
        super();
        super.setTableName("dispute_rules");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DisputeRulesDTOInter uDTO = (DisputeRulesDTOInter) obj[0];
        uDTO.setId(super.generateSequence(super.getTableName()));
        editRules(uDTO, 3);
        String action = "Add a new dispute rule with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DisputeRulesDTOInter uDTO = (DisputeRulesDTOInter) obj[0];
        editRules(uDTO, 4);
        String action = "Update dispute rule with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    private Object editRules(DisputeRulesDTOInter drDTO, int operation) throws Throwable {
        super.preCollable();
        CallableStatement stat = super.executeCallableStatment("{call edit_rules(?,?,?,?,?,?,?,?)}");
        stat.setInt(1, operation);
        stat.setInt(2, drDTO.getId());
        stat.setInt(3, drDTO.getValidationType());
        stat.setInt(4, drDTO.getColumnId());
        stat.setString(5, drDTO.getName());
        stat.setString(6, drDTO.getFormula());
        stat.setString(7, drDTO.getFormulaCode());
        stat.setInt(8, 1);
        stat.executeUpdate();
        super.postCollable(stat);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        DisputeRulesDTOInter uDTO = (DisputeRulesDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        String action = "Delete dispute rule with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        DisputeRulesDTOInter uDTO = DTOFactory.createDisputeRulesDTO();
        while (rs.next()) {
            uDTO.setId(rs.getInt("id"));
            uDTO.setValidationType(rs.getInt("valdation_type"));
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setName(rs.getString("name"));
            uDTO.setFormula(rs.getString("formula"));
            uDTO.setFormulaCode(rs.getString("formula_code"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<DisputeRulesDTOInter> uDTOL = new ArrayList<DisputeRulesDTOInter>();
        while (rs.next()) {
            DisputeRulesDTOInter uDTO = DTOFactory.createDisputeRulesDTO();
            uDTO.setId(rs.getInt("id"));
            uDTO.setValidationType(rs.getInt("valdation_type"));
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setName(rs.getString("name"));
            uDTO.setFormula(rs.getString("formula"));
            uDTO.setFormulaCode(rs.getString("formula_code"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findColumnId(int columnId) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where column_id = $column order by name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$column", "" + columnId);
        ResultSet rs = executeQuery(selectStat);
        List<DisputeRulesDTOInter> uDTOL = new ArrayList<DisputeRulesDTOInter>();
        while (rs.next()) {
            DisputeRulesDTOInter uDTO = DTOFactory.createDisputeRulesDTO();
            uDTO.setId(rs.getInt("id"));
            uDTO.setValidationType(rs.getInt("valdation_type"));
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setName(rs.getString("name"));
            uDTO.setFormula(rs.getString("formula"));
            uDTO.setFormulaCode(rs.getString("formula_code"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Boolean checkFormula(String formula,Integer Type) throws Throwable {
       super.preCollable();
        CallableStatement stat = super.executeCallableStatment("{call ? := check_formula_java(?,?)}");
        Boolean done = false;
        stat.registerOutParameter(1, Types.NUMERIC);
        stat.setString(2, formula);
        stat.setInt(3, Type);
        stat.executeUpdate();
        int i = stat.getInt(1);
        done = i == 1;
        super.postCollable(stat);
        return done;
    }

    public Object findDisputesReasons(DisputesDTOInter dDTO) throws Throwable {
        super.preSelect();
        String selectStat = "select c.* from dispute_rules c"
                + " where c.id in"
                + " (select rule_id from disputes_rejected_rules"
                + " where transaction_date = to_date('$transaction_date','dd.mm.yyyy hh24:mi:ss')"
                + " and ATM_APPLICATION_ID = '$atm_application_id'"
                + " and CARD_NO  = '$card_no'"
                + " and TRANSACTION_SEQUENCE = '$transaction_sequence'"
                + " and AMOUNT = $amount"
                + " and CURRENCY='$currency'"
                + " and record_type = $record_type"
                + " and matching_type = $matching_type)"
                + " order by 1";
        selectStat = selectStat.replace("$transaction_date", "" + DateFormatter.changeDateAndTimeFormat(dDTO.gettransactiondate()));
        selectStat = selectStat.replace("$atm_application_id", "" + dDTO.getatmapplicationid());
        selectStat = selectStat.replace("$card_no", "" + dDTO.getcardno());
        selectStat = selectStat.replace("$transaction_sequence", "" + dDTO.gettransactionseqeunce());
        selectStat = selectStat.replace("$amount", "" + dDTO.getamount());
        selectStat = selectStat.replace("$currency", "" + dDTO.getcurrency());
        selectStat = selectStat.replace("$record_type", "" + dDTO.getrecordtype());
        selectStat = selectStat.replace("$matching_type", "" + dDTO.getmatchingtype());
 
        ResultSet rs = executeQuery(selectStat);
        List<DisputeRulesDTOInter> uDTOL = new ArrayList<DisputeRulesDTOInter>();
        while (rs.next()) {
            DisputeRulesDTOInter uDTO = DTOFactory.createDisputeRulesDTO();
            uDTO.setId(rs.getInt("id"));
            uDTO.setValidationType(rs.getInt("valdation_type"));
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setName(rs.getString("name"));
            uDTO.setFormula(rs.getString("formula"));
            uDTO.setFormulaCode(rs.getString("formula_code"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
