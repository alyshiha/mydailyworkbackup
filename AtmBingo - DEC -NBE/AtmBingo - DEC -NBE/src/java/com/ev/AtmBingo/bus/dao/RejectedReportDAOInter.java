/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Administrator
 */
public interface RejectedReportDAOInter {
 
    Object delete(int userId) throws Throwable;

    Object findByUserId(int userId, String fields) throws Throwable;

    Object insert(Object... obj) throws Throwable;

}
