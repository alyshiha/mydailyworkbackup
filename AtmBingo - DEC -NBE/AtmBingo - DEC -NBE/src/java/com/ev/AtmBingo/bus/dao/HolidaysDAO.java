/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.HolidaysDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class HolidaysDAO extends BaseDAO implements HolidaysDAOInter {

    protected HolidaysDAO() {
        super();
        super.setTableName("hoidays");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        HolidaysDTOInter uDTO = (HolidaysDTOInter) obj[0];
        String insertStat = "insert into $table values ($month , $day, '$a_reason', '$e_reason')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$month", "" + uDTO.getMonth());
        insertStat = insertStat.replace("$day", "" + uDTO.getDay());
        insertStat = insertStat.replace("$a_reason", "" + uDTO.getaReason());
        insertStat = insertStat.replace("$e_reason", "" + uDTO.geteReason());
        super.executeUpdate(insertStat);
        super.postUpdate("Add Holiday For Reason" + uDTO.getaReason(), false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        HolidaysDTOInter uDTO = (HolidaysDTOInter) obj[0];
        Integer month = (Integer) obj[1];
        Integer day = (Integer) obj[2];
        String updateStat = "update $table set a_reason = '$a_reason', e_reason = '$e_reason', month = $newmonth, day = $newday"
                + " where month = $month and day = $day";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$month", "" + month);
        updateStat = updateStat.replace("$day", "" + day);
        updateStat = updateStat.replace("$newmonth", "" + uDTO.getMonth());
        updateStat = updateStat.replace("$newday", "" + uDTO.getDay());
        updateStat = updateStat.replace("$a_reason", "" + uDTO.getaReason());
        updateStat = updateStat.replace("$e_reason", "" + uDTO.geteReason());
        super.executeUpdate(updateStat);
        super.postUpdate("Update Holiday For Reason" + uDTO.getaReason(), false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        HolidaysDTOInter uDTO = (HolidaysDTOInter) obj[0];
        String deleteStat = "delete from $table where month = $month and day = $day";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$month", "" + uDTO.getMonth());
        deleteStat = deleteStat.replace("$day", "" + uDTO.getDay());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Holiday Because Reason" + uDTO.getaReason(), false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer month = (Integer) obj[0];
        Integer day = (Integer) obj[1];
        String selectStat = "select * from $table where month = $month and day = $day";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$month", "" + month);
        selectStat = selectStat.replace("$day", "" + day);
        ResultSet rs = executeQuery(selectStat);
        HolidaysDTOInter uDTO = DTOFactory.createHolidaysDTO();
        while (rs.next()) {
            uDTO.setDay(rs.getInt("day"));
            uDTO.setMonth(rs.getInt("month"));
            uDTO.setaReason(rs.getString("a_reason"));
            uDTO.seteReason(rs.getString("e_reason"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<HolidaysDTOInter> uDTOL = new ArrayList<HolidaysDTOInter>();
        while (rs.next()) {
            HolidaysDTOInter uDTO = DTOFactory.createHolidaysDTO();
            uDTO.setDay(rs.getInt("day"));
            uDTO.setMonth(rs.getInt("month"));
            uDTO.setaReason(rs.getString("a_reason"));
            uDTO.seteReason(rs.getString("e_reason"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findNowIsHoliday() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "SELECT *"
                + " FROM HOIDAYS H"
                + " WHERE H.MONTH = TO_NUMBER(TO_CHAR(SYSDATE,'MM'))"
                + " AND H.DAY = TO_NUMBER(TO_CHAR(SYSDATE,'DD'))";
        ResultSet rs = executeQuery(selectStat);
        boolean isHoliday = rs.next();
        super.postSelect(rs);
        return isHoliday;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
