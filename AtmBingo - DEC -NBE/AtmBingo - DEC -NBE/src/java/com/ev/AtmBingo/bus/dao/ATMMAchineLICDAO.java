/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.ATMMAchineLICInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.LincManagmentDetailsDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import oracle.sql.ARRAY;

/**
 *
 * @author Administrator
 */
public class ATMMAchineLICDAO extends BaseDAO implements ATMMAchineLICDAOInter {

    

    protected ATMMAchineLICDAO() {
        super();
        super.setTableName("atm_machine");
    }

    public Object find() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select t.application_id,(Select Count(*) from atm_machine where id = decryptnew(atm_inc) and id = t.id) LIC,id from atm_machine t order by t.application_id";
        ResultSet result = executeQuery(selectStat);
        List<ATMMAchineLICInter> res = new ArrayList<ATMMAchineLICInter>();
        while (result.next()) {
            ATMMAchineLICInter mt = DTOFactory.createATMMAchineLIC();
            if (result.getInt(2) == 1) {
                mt.setLIC(Boolean.TRUE);
            } else {
                mt.setLIC(Boolean.FALSE);
            }
            mt.setApplicationId(result.getString(1));
            mt.setID(result.getInt(3));
            res.add(mt);
        }
        super.postSelect(result);
        return res;
    }

    public Object findfilter(List<ATMMAchineLICInter> AtmList, String Like) throws Throwable {

        List<ATMMAchineLICInter> res = new ArrayList<ATMMAchineLICInter>();
        for (int i = 0; i < AtmList.size(); i++) {
            ATMMAchineLICInter mt = DTOFactory.createATMMAchineLIC();
            mt = AtmList.get(i);
            if (mt.getApplicationId().contains(Like)) {
                res.add(mt);
            }
        }
        return res;
    }

    public Integer GetLIC() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = " select atm_no  from license where license_key = incryptnew(atm_no || to_char(end_date, 'dd.mm.yyyy') || no_of_users)";
        ResultSet result = executeQuery(selectStat);
        Integer mt = 0;
        while (result.next()) {
            mt = result.getInt("atm_no");
        }
        super.postSelect(result);
        return mt;
    }

    public void save(List<ATMMAchineLICInter> entities, String State) throws SQLException {

        PreparedStatement statement = null;
        try {

            Connection conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());

            statement = conn.prepareStatement("update  atm_machine set ATM_inc = encryptnew(?) Where application_id = ?");
            for (int i = 0; i < entities.size(); i++) {
                ATMMAchineLICInter entity = entities.get(i);

                if (entity.getLIC() == Boolean.FALSE) {
                    statement.setString(1, "");
                } else {
                    statement.setString(1, entity.getID().toString());
                }
                statement.setString(2, entity.getApplicationId());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            statement.executeBatch();
            CoonectionHandler.getInstance().returnConnection(conn);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ATMMAchineLICDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ATMMAchineLICDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }

        }
    }

    public LincManagmentDetailsDTOInter LicMangDetail() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select (Select Count(*) from atm_machine where id = decryptnew(atm_inc)) InLIC,(Select Count(*) from atm_machine)allmachines,((Select Count(*) from atm_machine) - (Select Count(*) from atm_machine where id = decryptnew(atm_inc)))outoflic,(select atm_no from license) AtmNumberInLinc,((select atm_no from license)-(Select Count(*) from atm_machine where id = decryptnew(atm_inc))) remeainoflic from dual t";
        ResultSet result = executeQuery(selectStat);
        LincManagmentDetailsDTOInter uDTO = DTOFactory.createLincManagmentDetailsDTO();
        while (result.next()) {
            uDTO.setInlic(result.getString("InLIC"));
            uDTO.setOutlic(result.getString("outoflic"));
            uDTO.setRemaininlic(result.getString("remeainoflic"));
            uDTO.setAllmachines(result.getString("allmachines"));
            uDTO.setLic(result.getString("AtmNumberInLinc"));
        }
        super.postSelect(result);
        return uDTO;
    }

    public void Delete(List<ATMMAchineLICInter> entities) throws SQLException {

        PreparedStatement statement = null;
        try {

            Connection conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            statement = conn.prepareStatement("insert into delete_atm_by_id (atm_id) values (?)");
            for (int i = 0; i < entities.size(); i++) {
                ATMMAchineLICInter entity = entities.get(i);
                if (entity.getDel() == Boolean.TRUE) {
                    statement.setString(1, entity.getID().toString());
                    statement.addBatch();
                }

                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            statement.executeBatch();
            CoonectionHandler.getInstance().returnConnection(conn);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ATMMAchineLICDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ATMMAchineLICDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
        }
    }
}
