package com.ev.AtmBingo.bus.bo;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
public interface usersbranchBOInter {

Object insertrecord(Object... obj) throws Throwable;
Object save(Object... obj) throws SQLException, Throwable;
Object updaterecord(Object... obj) throws Throwable;
Object deleteallrecord(Object... obj) throws Throwable; 
Object deleterecord(Object... obj) throws Throwable;
Object GetListOfRecords(Object... obj) throws Throwable;
Object GetRecord(Object... obj) throws Throwable;
Object GetAllRecords(Object... obj) throws Throwable;
String ValidateLog(String userName,String password,String version,Integer NumOfUsers,Date EndDate) throws Throwable ;

}