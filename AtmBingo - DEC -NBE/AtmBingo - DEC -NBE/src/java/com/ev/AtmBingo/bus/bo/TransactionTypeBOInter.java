/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeMasterDTOInter;

/**
 *
 * @author Administrator
 */
public interface TransactionTypeBOInter extends BaseBOInter {

    Object editeTransactionType(TransactionTypeDTOInter cDTO, int operation) throws Throwable;

    Object editeTransactionTypeMaster(TransactionTypeMasterDTOInter cmDTO, int operation) throws Throwable;

    Object getTranasactionType(TransactionTypeMasterDTOInter cmDTO) throws Throwable;

    Object getTransactionTypeMaster() throws Throwable;

}
