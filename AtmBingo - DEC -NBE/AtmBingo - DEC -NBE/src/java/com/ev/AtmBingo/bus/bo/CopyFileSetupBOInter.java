/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.CopyFilesDTOInter;
import com.ev.AtmBingo.bus.dto.FilesToMoveDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface CopyFileSetupBOInter extends BaseBOInter {

    Object getAllFiles() throws Throwable;
    
     Object deletefile(List<FilesToMoveDTOInter> deleteitems)throws Throwable;

    Object editeCopyFile(CopyFilesDTOInter cfDTO, String oldSourceFolder, int operation) throws Throwable;

    Object getCopyFiles() throws Throwable;
}
