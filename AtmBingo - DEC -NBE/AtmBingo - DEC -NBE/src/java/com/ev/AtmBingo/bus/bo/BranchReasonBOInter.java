/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;

/**
 *
 * @author Aly
 */
public interface BranchReasonBOInter extends BaseBOInter{

    Object editeBlockReason(Object obj, int operation) throws Throwable;

    Object getBlockReasons() throws Throwable;
    
}
