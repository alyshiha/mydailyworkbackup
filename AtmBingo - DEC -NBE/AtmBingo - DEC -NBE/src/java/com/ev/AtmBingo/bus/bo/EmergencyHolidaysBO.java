/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.EmgHolidaysDAOInter;
import com.ev.AtmBingo.bus.dto.EmgHolidaysDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class EmergencyHolidaysBO extends BaseBO implements EmergencyHolidaysBOInter,Serializable{

    protected EmergencyHolidaysBO() {
        super();
    }

    public Object getEmgHolidays()throws Throwable{
        EmgHolidaysDAOInter ehDAO = DAOFactory.createEmgHolidaysDAO(null);
        List<EmgHolidaysDTOInter> ehDTOL = (List<EmgHolidaysDTOInter>)ehDAO.findAll();
        return ehDTOL;
    }

    public Boolean checkDate(Date from, Date to){
        if(!to.before(from) || from.equals(to)){
            return true;
        }
        else{
            return false;
        }

    }
    public Object editeEmgHolidays(Object obj ,Date oldPFrom ,int operation) throws Throwable{
        EmgHolidaysDAOInter ehDAO = DAOFactory.createEmgHolidaysDAO(null);
        EmgHolidaysDTOInter ehDTO = (EmgHolidaysDTOInter)obj;

        switch(operation){
            case INSERT:
                ehDAO.insert(ehDTO);
                return "Insert Done";
            case UPDATE:
                ehDAO.update(ehDTO,oldPFrom);
                return "Update Done";
            case DELETE:
                ehDAO.delete(ehDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }

}
