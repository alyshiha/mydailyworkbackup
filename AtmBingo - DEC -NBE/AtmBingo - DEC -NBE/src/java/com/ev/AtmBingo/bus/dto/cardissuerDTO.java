/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class cardissuerDTO implements cardissuerDTOInter, Serializable {
      private String  name,currency;
    private Integer totalamount,totalcount;

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Integer getTotalamount() {
        return totalamount;
    }

    @Override
    public void setTotalamount(Integer totalamount) {
        this.totalamount = totalamount;
    }

    @Override
    public Integer getTotalcount() {
        return totalcount;
    }

    @Override
    public void setTotalcount(Integer totalcount) {
        this.totalcount = totalcount;
    }
    
    
}
