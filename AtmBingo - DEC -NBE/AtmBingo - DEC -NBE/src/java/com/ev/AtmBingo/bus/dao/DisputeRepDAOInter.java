/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Aly-Shiha
 */
public interface DisputeRepDAOInter {

    Object findDataTable(String dateFrom, String dateTo, String atmID,Integer uservalue,Integer group) throws Throwable;

    Object findReport(String dateFrom, String dateTo, String atmID,Integer uservalue,Integer group) throws Throwable;
    
}
