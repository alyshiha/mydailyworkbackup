/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class ReplanishmentTempletesDetDTO extends BaseDTO implements ReplanishmentTempletesDetDTOInter, Serializable{

    private int id ;
    private Integer cassitteId ;
    private String startingString ;
    private Integer position ;
    private Integer lenght ;
    private Integer type ;
    private Integer line ;
    private Integer typeNo ;
    private Integer currency ;

    public Integer getCassitteId() {
        return cassitteId;
    }

    public void setCassitteId(Integer cassitteId) {
        this.cassitteId = cassitteId;
    }

    public Integer getCurrency() {
        return currency;
    }

    public void setCurrency(Integer currency) {
        this.currency = currency;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getLenght() {
        return lenght;
    }

    public void setLenght(Integer lenght) {
        this.lenght = lenght;
    }

    public Integer getLine() {
        return line;
    }

    public void setLine(Integer line) {
        this.line = line;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getStartingString() {
        return startingString;
    }

    public void setStartingString(String startingString) {
        this.startingString = startingString;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getTypeNo() {
        return typeNo;
    }

    public void setTypeNo(Integer typeNo) {
        this.typeNo = typeNo;
    }

}
