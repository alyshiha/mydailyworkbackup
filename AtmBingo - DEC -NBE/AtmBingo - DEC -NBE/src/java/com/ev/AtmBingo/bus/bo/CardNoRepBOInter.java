/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.bycardDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface CardNoRepBOInter {

    Object getGrp()throws Throwable;

    String runReport(String dateFrom, String dateTo, int atmGroupInt, String user, int side, int ord, int topCard, int tType,String cust) throws Throwable;
    
    List<bycardDTOInter> runReportexcel(String dateFrom, String dateTo, int atmGroupInt, String user, int side, int ord, int topCard, int tType, String cust) throws Throwable ;

}
