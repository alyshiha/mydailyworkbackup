/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dao.BingoLogDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.ProfileDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.BingoLogDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileDTOInter;
import com.ev.AtmBingo.bus.dto.UserProfileDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author KhAiReE
 */
public class BingoLogBO extends BaseBO implements BingoLogBOInter ,Serializable{

    protected BingoLogBO() {
        super();
    }
    public Object getProfiles (int rest) throws Throwable {
        ProfileDAOInter pDAO = DAOFactory.createProfileDAO(null);
        List<ProfileDTOInter> pl = (List<ProfileDTOInter>) pDAO.findAll(rest);
        return pl ;
    }
      public Object getuserProfiles () throws Throwable {
        ProfileDAOInter pDAO = DAOFactory.createProfileDAO(null);
        List<UserProfileDTOInter> pl = (List<UserProfileDTOInter>) pDAO.findAlluserprofile();
        return pl ;
    }

    public Object getUsers (int rest) throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> ul = (List<UsersDTOInter>)uDAO.findALl(rest) ;
        return ul ;
    }
    public Object getBingoLogged ( Date dateF , Date dateT , Integer user , Integer profile ) throws Throwable {
        String whereClause  = " and action_date between to_date('$dateF','dd.mm.yyyy hh24:mi:ss') " +
                "and to_date('$dateT','dd.mm.yyyy hh24:mi:ss')";
        whereClause = whereClause.replace("$dateT", "" + DateFormatter.changeDateAndTimeFormat(dateT));
        whereClause = whereClause.replace("$dateF", "" + DateFormatter.changeDateAndTimeFormat(dateF));

        if (user !=0 )
        {
            whereClause+= " and user_id = $user";
            whereClause = whereClause.replace("$user", "" + user);
        }
        if (profile !=0 )
        {
            whereClause+= " and user_id in (select user_id from user_profile where profile_id = $profile)";
            whereClause = whereClause.replace("$profile", "" + profile);
        }

        BingoLogDAOInter blDAO = DAOFactory.createBingoLogDAO();
        List<BingoLogDTOInter> blL = (List<BingoLogDTOInter>)blDAO.CustomSearch(whereClause);
        return blL ;
    }
}
