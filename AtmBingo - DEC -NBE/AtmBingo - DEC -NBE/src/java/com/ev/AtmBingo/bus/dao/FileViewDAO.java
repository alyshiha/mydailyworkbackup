/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.Clob;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class FileViewDAO extends BaseDAO implements FileViewDAOInter {

    protected FileViewDAO() {
        super();
        super.setTableName("atm_file");
    }

    public Clob FindFiletransaction(Object obj) throws Throwable {
        super.preSelect();
        AtmFileDTOInter mdDTO = (AtmFileDTOInter) obj;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select decrypt_blob(t.id) filetext from ATM_FILE t where t.id = '$fileid'";
        Clob elmsg = null;
        selectStat = selectStat.replace("$fileid", "" + mdDTO.getId());
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            elmsg = rs.getClob("filetext");
        }
        super.postSelect(rs);
        return elmsg;
    }

    public List<AtmFileDTOInter> dosearch(String atmInt, Date loadingFrom, Date loadingTo, String cardno, String sequence) throws Throwable {
        super.preSelect();
        String selectStat = "select * from $table where 1=1 ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        if (loadingFrom != null) {
            selectStat += " and loading_date > to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
            selectStat = selectStat.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(loadingFrom));
        }
        if (loadingTo != null) {
            selectStat += " and loading_date < to_date(to_char(nvl(to_date('$col2','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') ";
            selectStat = selectStat.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(loadingTo));
        }
        if (atmInt == null ? "" != null : !atmInt.equals("")) {
            selectStat += " and decrypt_blob(id) like '%$atmid%'";
            selectStat = selectStat.replace("$atmid", "" + atmInt);
        }
        if ( cardno == null ? "" != null : !cardno.equals("")) {
            selectStat += " and decrypt_blob(id) like '%$cardnumber%'";
            selectStat = selectStat.replace("$cardnumber", "" + cardno);
        }
        if (sequence == null ? "" != null : !sequence.equals("")) {
            selectStat += " and decrypt_blob(id) like '%$sequence%'";
            selectStat = selectStat.replace("$sequence", "" + sequence);
        }
        selectStat += " order by loading_date";
        
        ResultSet rs = executeQuery(selectStat);
        List<AtmFileDTOInter> uDTOL = new ArrayList<AtmFileDTOInter>();
        while (rs.next()) {
            AtmFileDTOInter uDTO = DTOFactory.createAtmFileDTO();
            uDTO.setBuckupName(rs.getString("backup_name"));
            uDTO.setFinishLaodingDate(rs.getTimestamp("finish_loading_date"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLoadingDate(rs.getTimestamp("loading_date"));
            uDTO.setName(rs.getString("name"));
            uDTO.setNumberOfDuplication(rs.getInt("number_of_duplication"));
            uDTO.setNumberOfTransaction(rs.getInt("number_of_transactions"));
            uDTO.setTemplate(rs.getInt("template"));
            uDTO.setType(rs.getInt("type"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;


    }
}
