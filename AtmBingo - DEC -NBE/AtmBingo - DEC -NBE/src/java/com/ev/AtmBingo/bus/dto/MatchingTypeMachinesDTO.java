/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class MatchingTypeMachinesDTO extends BaseDTO implements MatchingTypeMachinesDTOInter, Serializable {

    private int matchingType;
    private int machineType;
    private int defaultMatchingSetting;

    protected MatchingTypeMachinesDTO() {
    }

    public int getDefaultMatchingSetting() {
        return defaultMatchingSetting;
    }

    public void setDefaultMatchingSetting(int defaultMatchingSetting) {
        this.defaultMatchingSetting = defaultMatchingSetting;
    }

    public int getMachineType() {
        return machineType;
    }

    public void setMachineType(int machineType) {
        this.machineType = machineType;
    }

    public int getMatchingType() {
        return matchingType;
    }

    public void setMatchingType(int matchingType) {
        this.matchingType = matchingType;
    }
    
}
