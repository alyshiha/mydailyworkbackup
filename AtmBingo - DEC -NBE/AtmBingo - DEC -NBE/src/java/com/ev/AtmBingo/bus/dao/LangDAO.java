/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.LangDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class LangDAO extends BaseDAO implements LangDAOInter {

    protected LangDAO() {
        super();
        super.setTableName("lang");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        LangDTOInter uDTO = (LangDTOInter) obj[0];
        uDTO.setLangId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ($lang_id, '$lang_name' , '$lang_a_name')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$lang_id", "" + uDTO.getLangId());
        insertStat = insertStat.replace("$lang_name", "" + uDTO.getLangName());
        insertStat = insertStat.replace("$lang_a_name", "" + uDTO.getLangAName());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        LangDTOInter uDTO = (LangDTOInter) obj[0];
        String updateStat = "update $table set lang_name = '$name', lang_a_name = '$symbol' where lang_id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getLangId());
        updateStat = updateStat.replace("$name", "" + uDTO.getLangName());
        updateStat = updateStat.replace("$symbol", "" + uDTO.getLangAName());
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        LangDTOInter uDTO = (LangDTOInter) obj[0];
        String deleteStat = "delete from $table where lang_id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getLangId());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where lang_id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        LangDTOInter uDTO = DTOFactory.createLangDTO();
        while (rs.next()) {
            uDTO.setLangAName(rs.getString("lang_a_name"));
            uDTO.setLangId(rs.getInt("lang_id"));
            uDTO.setLangName(rs.getString("lang_name"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<LangDTOInter> uDTOL = new ArrayList<LangDTOInter>();
        while (rs.next()) {
            LangDTOInter uDTO = DTOFactory.createLangDTO();
            uDTO.setLangAName(rs.getString("lang_a_name"));
            uDTO.setLangId(rs.getInt("lang_id"));
            uDTO.setLangName(rs.getString("lang_name"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
