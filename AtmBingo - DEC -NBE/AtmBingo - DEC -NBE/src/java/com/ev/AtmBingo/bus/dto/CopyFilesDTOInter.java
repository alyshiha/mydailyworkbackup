/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface CopyFilesDTOInter extends Serializable {

    String getFolder1();

    String getFolder2();

    String getSourceFolder();

    void setFolder1(String folder1);

    void setFolder2(String folder2);

    void setSourceFolder(String sourceFolder);

}
