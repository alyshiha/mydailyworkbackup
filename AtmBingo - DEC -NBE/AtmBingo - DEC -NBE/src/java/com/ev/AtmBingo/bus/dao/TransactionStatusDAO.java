/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TransactionStatusDAO extends BaseDAO implements TransactionStatusDAOInter {

    protected TransactionStatusDAO() {
        super();
        super.setTableName("transaction_status");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TransactionStatusDTOInter uDTO = (TransactionStatusDTOInter) obj[0];
        uDTO.setId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ($id, '$name' , '$description')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$name", "" + uDTO.getName());
        insertStat = insertStat.replace("$description", "" + uDTO.getDescription());
        super.executeUpdate(insertStat);
        String action = "Add a new trans. status with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TransactionStatusDTOInter uDTO = (TransactionStatusDTOInter) obj[0];
        String updateStat = "update $table set name = '$name', description = '$description' where id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$name", "" + uDTO.getName());
        updateStat = updateStat.replace("$description", "" + uDTO.getDescription());
        super.executeUpdate(updateStat);
        String action = "Update trans. status with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TransactionStatusDTOInter uDTO = (TransactionStatusDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        String action = "Delete trans. status with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        TransactionStatusDTOInter uDTO = DTOFactory.createTransactionStatusDTO();
        while (rs.next()) {
            uDTO.setDescription(rs.getString("description"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setName(rs.getString("name"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<TransactionStatusDTOInter> uDTOL = new ArrayList<TransactionStatusDTOInter>();
        while (rs.next()) {
            TransactionStatusDTOInter uDTO = DTOFactory.createTransactionStatusDTO();
            uDTO.setDescription(rs.getString("description"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setName(rs.getString("name"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
