/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.FileColumnDefinitionDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class FileColumnDefinitionDAO extends BaseDAO implements FileColumnDefinitionDAOInter {

    protected FileColumnDefinitionDAO() {
        super();
        super.setTableName("FILE_COLUMN_DEFINITION");

    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        FileColumnDefinitionDTOInter fileColumnDef = (FileColumnDefinitionDTOInter) obj[0];
        String insertStat = "insert into " + super.getTableName() + " values(" + fileColumnDef.getId() + ",'"
                + fileColumnDef.getName() + "','" + fileColumnDef.getColumnName() + "'," + fileColumnDef.getDataType() + ")";
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        FileColumnDefinitionDTOInter fileColumnDef = (FileColumnDefinitionDTOInter) obj[0];
        String updateStat = "update " + super.getTableName() + " set name = '" + fileColumnDef.getName() + "',column_name = '" + fileColumnDef.getColumnName()
                + "', data_type = " + fileColumnDef.getDataType()
                + "where id = " + fileColumnDef.getId();
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        FileColumnDefinitionDTOInter fileColumnDef = (FileColumnDefinitionDTOInter) obj[0];
        String deleteStat = "delete from " + super.getTableName() + " where id = " + fileColumnDef.getId();
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer fileColumnDef = (Integer) obj[0];
        String selectStat = "select * from " + super.getTableName() + " where id = " + fileColumnDef.intValue();
        ResultSet result = executeQuery(selectStat);
        List<FileColumnDefinitionDTOInter> mtL = new ArrayList<FileColumnDefinitionDTOInter>();
        while (result.next()) {
            FileColumnDefinitionDTOInter mt = DTOFactory.createFileColumnDefinitionDTO();
            mt.setId(result.getInt("id"));
            mt.setColumnName(result.getString("column_name"));
            mt.setName(result.getString("name"));
            mt.setDataType(result.getInt("data_type"));
          //  mt.setProperty(result.getString("property"));
            mt.setDISPLAYINLOADING(result.getInt("DISPLAY_IN_LOADING"));
            mt.setDUPLICATENO(result.getInt("DUPLICATE_NO"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL.get(0);
    }
   public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from " + super.getTableName();
        ResultSet result = executeQuery(selectStat);
        List<FileColumnDefinitionDTOInter> mtL = new ArrayList<FileColumnDefinitionDTOInter>();
        while (result.next()) {
            FileColumnDefinitionDTOInter mt = DTOFactory.createFileColumnDefinitionDTO();
            mt.setId(result.getInt("id"));
            mt.setColumnName(result.getString("column_name"));
            mt.setName(result.getString("name"));
            mt.setDataType(result.getInt("data_type"));
            //mt.setProperty(result.getString("property"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object findAllmatching() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from " + super.getTableName() + " where display_in_matching = 1";
        ResultSet result = executeQuery(selectStat);
        List<FileColumnDefinitionDTOInter> mtL = new ArrayList<FileColumnDefinitionDTOInter>();
        while (result.next()) {
            FileColumnDefinitionDTOInter mt = DTOFactory.createFileColumnDefinitionDTO();
            mt.setId(result.getInt("id"));
            mt.setColumnName(result.getString("column_name"));
            mt.setName(result.getString("name"));
            mt.setDataType(result.getInt("data_type"));
            //mt.setProperty(result.getString("property"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object findComboBox(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from " + super.getTableName() + " where column_name not in ('TRANSACTION_TIME') "
                + "order by 1";
        ResultSet result = executeQuery(selectStat);
        List<FileColumnDefinitionDTOInter> mtL = new ArrayList<FileColumnDefinitionDTOInter>();
        while (result.next()) {
            FileColumnDefinitionDTOInter mt = DTOFactory.createFileColumnDefinitionDTO();
            mt.setId(result.getInt("id"));
            mt.setColumnName(result.getString("column_name"));
            mt.setName(result.getString("name"));
            mt.setDataType(result.getInt("data_type"));
            //mt.setProperty(result.getString("property"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

     public Object FindAllByDublicateid(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer duplicateno = (Integer) obj[0];
        String selectStat = "select * from " + super.getTableName() + " where duplicate_no = '" + duplicateno + "'";
        ResultSet result = executeQuery(selectStat);
        List<FileColumnDefinitionDTOInter> mtL = new ArrayList<FileColumnDefinitionDTOInter>();
        while (result.next()) {
            FileColumnDefinitionDTOInter mt = DTOFactory.createFileColumnDefinitionDTO();
            mt.setId(result.getInt("id"));
            //mt.setProperty(result.getString("property"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String name = (String) obj[0];
        String selectStat = "select * from " + super.getTableName() + " where name = '" + name + "'";
        ResultSet result = executeQuery(selectStat);
        FileColumnDefinitionDTOInter mt = DTOFactory.createFileColumnDefinitionDTO();
        while (result.next()) {
            mt.setId(result.getInt("id"));
            mt.setColumnName(result.getString("column_name"));
            mt.setName(result.getString("name"));
            mt.setDataType(result.getInt("data_type"));
            //mt.setProperty(result.getString("property"));
        }
        super.postSelect(result);
        return mt;
    }
 public Object searchById(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer id = (Integer) obj[0];
        String selectStat = "select * from " + super.getTableName() + " where id = " + id + "";
        ResultSet result = executeQuery(selectStat);
        FileColumnDefinitionDTOInter mt = DTOFactory.createFileColumnDefinitionDTO();
        while (result.next()) {
            mt.setId(result.getInt("id"));
            mt.setColumnName(result.getString("column_name"));
            mt.setName(result.getString("name"));
            mt.setDataType(result.getInt("data_type"));
           // mt.setProperty(result.getString("property"));
        }
        super.postSelect(result);
        return mt;
    }

    public Object searchByColumnName(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String colName = (String) obj[0];
        String selectStat = "select * from " + super.getTableName() + " where column_name = '" + colName + "'";
        ResultSet result = executeQuery(selectStat);
        List<FileColumnDefinitionDTOInter> mtL = new ArrayList<FileColumnDefinitionDTOInter>();
        while (result.next()) {
            FileColumnDefinitionDTOInter mt = DTOFactory.createFileColumnDefinitionDTO();
            mt.setId(result.getInt("id"));
            mt.setName(result.getString("name"));
            //mt.setProperty(result.getString("property"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL.get(0);
    }

    public Object findForRules(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from " + super.getTableName() + " ";
        ResultSet result = executeQuery(selectStat);
        List<FileColumnDefinitionDTOInter> mtL = new ArrayList<FileColumnDefinitionDTOInter>();
        while (result.next()) {
            FileColumnDefinitionDTOInter mt = DTOFactory.createFileColumnDefinitionDTO();
            mt.setId(result.getInt("id"));
            mt.setColumnName(result.getString("column_name"));
            mt.setCname(result.getString("column_name"));
            mt.setName(result.getString("name"));
            mt.setDataType(result.getInt("data_type"));
           // mt.setProperty(result.getString("property"));
            mtL.add(mt);
        }
        super.postSelect(result);
        return mtL;
    }
}
