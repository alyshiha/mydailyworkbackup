/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.BingoLogBOInter;
import com.ev.AtmBingo.bus.dto.BingoLogDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileDTOInter;
import com.ev.AtmBingo.bus.dto.UserProfileDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class BingoLogClient extends BaseBean  implements Serializable{

    private Date fromDate, toDate;
    private Integer userId, profileId;
    private BingoLogBOInter bingoBo;
    private List<UsersDTOInter> userList;
    private List<ProfileDTOInter> profileList;
    private List<BingoLogDTOInter> logList;
    private List<UserProfileDTOInter> UserProfile;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserProfileDTOInter> getUserProfile() {
        return UserProfile;
    }

    public void setUserProfile(List<UserProfileDTOInter> UserProfile) {
        this.UserProfile = UserProfile;
    }

    

    public List<BingoLogDTOInter> getLogList() {
        return logList;
    }

    public void setLogList(List<BingoLogDTOInter> logList) {
        this.logList = logList;
    }

    public List<ProfileDTOInter> getProfileList() {
        return profileList;
    }

    public void setProfileList(List<ProfileDTOInter> profileList) {
        this.profileList = profileList;
    }

    public List<UsersDTOInter> getUserList() {
        return userList;
    }

    public void setUserList(List<UsersDTOInter> userList) {
        this.userList = userList;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Integer getProfileId() {
        return profileId;
    }

    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /** Creates a new instance of BingoLogClient */
    public BingoLogClient() throws Throwable {
        super();
        super.GetAccess();
        bingoBo = BOFactory.createBingoLogBO(null);
        userList = (List<UsersDTOInter>) bingoBo.getUsers(super.getRestrected());
        profileList = (List<ProfileDTOInter>) bingoBo.getProfiles(super.getRestrected());
        UserProfile = (List<UserProfileDTOInter>) bingoBo.getuserProfiles();
             FacesContext context = FacesContext.getCurrentInstance();
        
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void doSearch() {
        try {
            if (fromDate == null || toDate == null) {
                message = super.showMessage(REQ_FIELD);
            } else {
                logList = (List<BingoLogDTOInter>) bingoBo.getBingoLogged(fromDate, toDate, userId, profileId);
                resetVars();
            }
        } catch (Throwable ex) {
        }
    }
    public void resetVars() {
        message = "";
    }
      public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }
}
