/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;

import com.ev.AtmBingo.bus.dto.AutoRepDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class AutoRepAtmToGroupDAO extends BaseDAO implements AutoRepAtmToGroupDAOInter {
public Object saveGroup(Integer sourceList, String[] targetList,Integer selectedSource, String[] selectedTarget)throws Throwable{
Connection connection = null;
        PreparedStatement statement = null;
        try {
  connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());

            statement = connection.prepareStatement("update atm_machine set cassette_group = ? Where  trim(application_id) = ?");

            for (int i = 0; i < targetList.length; i++) {
                String entity = targetList[i];
                String[] ATMID = entity.split(" --- ");
                statement.setInt(1,selectedSource);
                statement.setString(2, ATMID[2].toString().trim());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }

            }
 for (int i = 0; i < selectedTarget.length; i++) {
                String entity = selectedTarget[i];
                String[] ATMID = entity.split(" --- ");
                statement.setInt(1,sourceList);
                statement.setString(2, ATMID[2].toString().trim());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }

            }
            statement.executeBatch();

        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                  CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
 }

public Object unsaveGroup(Integer sourceList, String[] targetList,Integer selectedSource, String[] selectedTarget)throws Throwable{
Connection connection = null;
        PreparedStatement statement = null;
        try {
     connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());

            statement = connection.prepareStatement("update atm_machine set cassette_group = null Where  trim(application_id) = ?");

            for (int i = 0; i < targetList.length; i++) {
                String entity = targetList[i];
                String[] ATMID = entity.split(" --- ");
                statement.setString(1, ATMID[2].toString().trim());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }

            }
 for (int i = 0; i < selectedTarget.length; i++) {
                String entity = selectedTarget[i];
                String[] ATMID = entity.split(" --- ");
                statement.setInt(1,sourceList);
                statement.setString(2, ATMID[2].toString().trim());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }

            }
            statement.executeBatch();

        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                   CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            return true;
        }
 }
    public Object deleteByUserId(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        int uDTO = (Integer) obj[0];
        String deleteStat = "update ATM_MACHINE where user_id = $userid";
        deleteStat = deleteStat.replace("$userid", "" + uDTO);
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object findAllcassettegroup() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from cassette_group order by name";
        ResultSet rs = executeQuery(selectStat);
        List<AutoRepDTOInter> uDTOL = new ArrayList<AutoRepDTOInter>();
        while (rs.next()) {
            AutoRepDTOInter uDTO = DTOFactory.createAutoRepDTO();
            uDTO.setGroupName(rs.getString("name"));
            uDTO.setGroupid(rs.getInt("id"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findAll(Integer groupid) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select CASSETTE_GROUP.name,cassette_group_definition.group_id,cassette_group_definition.type_cassette_id,cassette_group_definition.currency,cassette_group_definition.type_number,cassette_group_definition.rowid rowidnumber from cassette_group_definition,CASSETTE_GROUP where CASSETTE_GROUP.id = cassette_group_definition.group_id and cassette_group_definition.group_id = $GROuPID";
         selectStat = selectStat.replace("$GROuPID", "" + groupid);
        ResultSet rs = executeQuery(selectStat);
        List<AutoRepDTOInter> uDTOL = new ArrayList<AutoRepDTOInter>();
        while (rs.next()) {
            AutoRepDTOInter uDTO = DTOFactory.createAutoRepDTO();
            uDTO.setGroupName(rs.getString(1));
            uDTO.setGroupid(rs.getInt(2));
            uDTO.setTypecassette(rs.getInt(3));
            uDTO.setCurrency(rs.getInt(4));
            uDTO.setTypenumber(rs.getInt(5));
            uDTO.setrowid(rs.getString("rowidnumber"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public boolean insertAtms(List<String> entities, Integer UserID) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
             connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            statement = connection.prepareStatement("insert into user_atm (atm_id,user_id) Select id,? From atm_machine Where  trim(application_id) = ?");

            for (int i = 0; i < entities.size(); i++) {
                String entity = entities.get(i);
                String[] ATMID = entity.split(" --- ");

                statement.setInt(1, UserID);
                statement.setString(2, ATMID[2].toString().trim());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }

            }

            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                  CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(AutoRepAtmToGroupDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(AutoRepAtmToGroupDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return true;
        }
    }
      public boolean grouphaveatms(AutoRepDTOInter entity ) throws Throwable {
        super.preSelect();
        boolean have = false;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from atm_machine where cassette_group = $Groupid";
        
        selectStat = selectStat.replace("$Groupid", "" + entity.getGroupid());
        ResultSet rs = executeQuery(selectStat);
        if (rs.next()) {
           have = true;
        }
        super.postSelect(rs);
        return have;
      }
      public void DeleteGroup(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AutoRepDTOInter entity = (AutoRepDTOInter) obj[0];
       // super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        if(!grouphaveatms(entity)){
        String deleteStat = "delete CASSETTE_GROUP where id = $Groupid ";
        deleteStat = deleteStat.replace("$Groupid", "" + entity.getGroupid());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);}
    }
   public void UpdateGroup(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AutoRepDTOInter entity = (AutoRepDTOInter) obj[0];
       // super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();

        String deleteStat = "update CASSETTE_GROUP set  name = '$Typename' where id = $Groupid ";
        deleteStat = deleteStat.replace("$Groupid", "" + entity.getGroupid());
        deleteStat = deleteStat.replace("$Typename", "" + entity.getGroupName());
        super.executeUpdate(deleteStat);
        //super.postUpdate(null, true);
    }

     public void AddGroup(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AutoRepDTOInter entity = (AutoRepDTOInter) obj[0];
       // super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
Integer Seq = super.generateSequence("cassette_group");
        String deleteStat = "insert into cassette_group (id,name) values ($Seq,'$TypeName') ";
        deleteStat = deleteStat.replace("$Seq", "" + Seq);
        deleteStat = deleteStat.replace("$TypeName", "" + entity.getGroupName());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
    }
    public void AtmGroup(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AutoRepDTOInter entity = (AutoRepDTOInter) obj[0];
     //   super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();

        String deleteStat = "update CASSETTE_GROUP_DEFINITION set Type_number = $Typenumber , type_cassette_id = $Typecassette,currency=$Currency where group_id = '$Groupid' and type_cassette_id = $Typecassette";
        deleteStat = deleteStat.replace("$Groupid", "" + entity.getGroupid());
        deleteStat = deleteStat.replace("$Typenumber", "" + entity.getTypenumber());
        deleteStat = deleteStat.replace("$Typecassette", "" + entity.getTypecassette());
        deleteStat = deleteStat.replace("$Currency", "" + entity.getCurrency());
        super.executeUpdate(deleteStat);
//        super.postUpdate(null, true);
       


    }

      public void deleteAtmGroup(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AutoRepDTOInter entity = (AutoRepDTOInter) obj[0];
        PreparedStatement statement = null;
        try {

            Connection conn  = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            statement = conn.prepareStatement("delete from CASSETTE_GROUP_DEFINITION   where group_id = ? and type_cassette_id = ? and type_number = ?");


                statement.setInt(1, entity.getGroupid());
                statement.setInt(2, entity.getTypecassette());
                statement.setInt(3, entity.getTypenumber());
                statement.addBatch();
            statement.executeBatch();
            statement = conn.prepareStatement("update atm_machine  set cassette_group = null  where cassette_group = ?");
                statement.setInt(1, entity.getGroupid());
                statement.addBatch();
            statement.executeBatch();
            CoonectionHandler.getInstance().returnConnection(conn);
        } catch (SQLException logOrIgnore) {
        }


    }

       public void InsertAtmGroup(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AutoRepDTOInter entity = (AutoRepDTOInter) obj[0];
        String insertStat = "insert into cassette_group_definition (group_id, type_number, type_cassette_id, currency) values ($groupid, $typenumber, $typecassetteid, $currency)";
        insertStat = insertStat.replace("$groupid", "" + entity.getGroupid());
        insertStat = insertStat.replace("$typenumber", "" + entity.getTypenumber());
        insertStat = insertStat.replace("$typecassetteid", "" + entity.getTypecassette());
        insertStat = insertStat.replace("$currency", "" + entity.getCurrency());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);

    }
}

