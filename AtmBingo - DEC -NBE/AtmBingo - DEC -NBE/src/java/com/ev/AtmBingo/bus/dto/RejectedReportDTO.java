/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class RejectedReportDTO extends BaseDTO implements RejectedReportDTOInter, Serializable{

    private String atmApplicationId;
    private AtmMachineDTOInter atmId;
    private String transactionType;
    private TransactionTypeDTOInter transactionTypeId;
    private String currency;
    private CurrencyMasterDTOInter currencyId;
    private String transactionStatus;
    private TransactionStatusDTOInter transactionStatusId;
    private String responseCode;
    private TransactionResponseCodeDTOInter responseCodeId;
    private Date transactionDate;
    private String transactionSeqeunce;
    private String cardNo;
    private int amount;
    private Date settlementDate;
    private String notesPresented;
    private String customerAccountNumber;
    private int transactionSequenceOrderBy;
    private Date transactionTime;
    private int recordType;
    private String column1;
    private String column2;
    private String column3;
    private String column4;
    private String column5;
    private int userId;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getAtmApplicationId() {
        return atmApplicationId;
    }

    public void setAtmApplicationId(String atmApplicationId) {
        this.atmApplicationId = atmApplicationId;
    }

    public AtmMachineDTOInter getAtmId() {
        return atmId;
    }

    public void setAtmId(AtmMachineDTOInter atmId) {
        this.atmId = atmId;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getColumn1() {
        return column1;
    }

    public void setColumn1(String column1) {
        this.column1 = column1;
    }

    public String getColumn2() {
        return column2;
    }

    public void setColumn2(String column2) {
        this.column2 = column2;
    }

    public String getColumn3() {
        return column3;
    }

    public void setColumn3(String column3) {
        this.column3 = column3;
    }

    public String getColumn4() {
        return column4;
    }

    public void setColumn4(String column4) {
        this.column4 = column4;
    }

    public String getColumn5() {
        return column5;
    }

    public void setColumn5(String column5) {
        this.column5 = column5;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public CurrencyMasterDTOInter getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(CurrencyMasterDTOInter currencyId) {
        this.currencyId = currencyId;
    }

    public String getCustomerAccountNumber() {
        return customerAccountNumber;
    }

    public void setCustomerAccountNumber(String customerAccountNumber) {
        this.customerAccountNumber = customerAccountNumber;
    }

    public String getNotesPresented() {
        return notesPresented;
    }

    public void setNotesPresented(String notesPresented) {
        this.notesPresented = notesPresented;
    }

    public int getRecordType() {
        return recordType;
    }

    public void setRecordType(int recordType) {
        this.recordType = recordType;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public TransactionResponseCodeDTOInter getResponseCodeId() {
        return responseCodeId;
    }

    public void setResponseCodeId(TransactionResponseCodeDTOInter responseCodeId) {
        this.responseCodeId = responseCodeId;
    }

    public Date getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(Date settlementDate) {
        this.settlementDate = settlementDate;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionSeqeunce() {
        return transactionSeqeunce;
    }

    public void setTransactionSeqeunce(String transactionSeqeunce) {
        this.transactionSeqeunce = transactionSeqeunce;
    }

    public int getTransactionSequenceOrderBy() {
        return transactionSequenceOrderBy;
    }

    public void setTransactionSequenceOrderBy(int transactionSequenceOrderBy) {
        this.transactionSequenceOrderBy = transactionSequenceOrderBy;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public TransactionStatusDTOInter getTransactionStatusId() {
        return transactionStatusId;
    }

    public void setTransactionStatusId(TransactionStatusDTOInter transactionStatusId) {
        this.transactionStatusId = transactionStatusId;
    }

    public Date getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Date transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public TransactionTypeDTOInter getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(TransactionTypeDTOInter transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    protected RejectedReportDTO() {
        super();
    }

}
