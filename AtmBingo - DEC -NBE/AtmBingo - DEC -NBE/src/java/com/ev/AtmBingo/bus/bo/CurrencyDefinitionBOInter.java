/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.CurrencyDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;

/**
 *
 * @author Administrator
 */
public interface CurrencyDefinitionBOInter extends BaseBOInter{

    Object editeCurrency(CurrencyDTOInter cDTO, int operation) throws Throwable;
int CountCurrencydetail(int cmDTO) throws Throwable;
    Object editeCurrencyMaster(CurrencyMasterDTOInter cmDTO, int operation) throws Throwable;
 int CountCurrencyMaster() throws Throwable;
    Object getCurrency(CurrencyMasterDTOInter cmDTO) throws Throwable;

    Object getCurrencyMaster() throws Throwable;

}
