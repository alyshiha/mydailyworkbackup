/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.RepTransDetailDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentDetailDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class ReplanishmentDetailDAO extends BaseDAO implements ReplanishmentDetailDAOInter {

    protected ReplanishmentDetailDAO() {
        super();
        super.setTableName("replanishment_detail");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ReplanishmentDetailDTOInter uDTO = (ReplanishmentDetailDTOInter) obj[0];
        String insertStat = "insert into $table values ($id, $cassete , $cassetValue, $totalAmount, $currency, $remaining, $remaininAmount)";

        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$cassete", "" + uDTO.getCassete());
        insertStat = insertStat.replace("$cassetValue", "" + uDTO.getCasseteValue());
        insertStat = insertStat.replace("$currency", "" + uDTO.getCurrency());
        insertStat = insertStat.replace("$remaining", "" + uDTO.getRemaining());
        insertStat = insertStat.replace("$remaininAmount", "" + uDTO.getRemainingAmount());
        insertStat = insertStat.replace("$totalAmount", "" + uDTO.getTotalAmount());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public void UpdateRep(List<ReplanishmentDetailDTOInter> entities, Integer id) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            statement = connection.prepareStatement("update replanishment_detail set cassete_value = ?, currency = ?, remaining = ?"
                    + ", remaining_amount = ?, total_amount = ?"
                    + " where id = ? and cassete = ?");
            CassetteDAOInter cDAO = DAOFactory.createCassetteDAO(null);
            for (int i = 0; i < entities.size(); i++) {
                try {
                    ReplanishmentDetailDTOInter entity = entities.get(i);
                    CassetteDTOInter cDTO;
                    cDTO = (CassetteDTOInter) cDAO.find(entity.getCassete());
                    int amount = cDTO.getAmount();
                    int value = entity.getCasseteValue();
                    int remaining = entity.getRemaining();
                    int totalAmount = (value - remaining) * amount;
                    int remainingAmount = remaining * amount;
                    entity.setTotalAmount(totalAmount);
                    entity.setRemainingAmount(remainingAmount);

                    statement.setInt(6, id);
                    statement.setInt(7, entity.getCassete());
                    statement.setInt(1, entity.getCasseteValue());
                    statement.setInt(2, entity.getCurrency());
                    statement.setInt(3, entity.getRemaining());
                    statement.setInt(4, entity.getRemainingAmount());
                    statement.setInt(5, entity.getTotalAmount());

                    statement.addBatch();
                    if ((i + 1) % 1000 == 0) {
                        statement.executeBatch(); // Execute every 1000 items.

                    }
                } catch (Throwable ex) {
                    Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            statement.executeBatch();
            try {

            } catch (Throwable ex) {
                Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ReplanishmentDetailDTOInter uDTO = (ReplanishmentDetailDTOInter) obj[0];
        String updateStat = "update $table set cassete_value = $cassetValue, currency = $currency, remaining = $remaining"
                + ", remaining_amount = $remaininAmount, total_amount = $totalAmount"
                + " where id = $id and cassete = $cassete";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$cassete", "" + uDTO.getCassete());
        updateStat = updateStat.replace("$cassetValue", "" + uDTO.getCasseteValue());
        updateStat = updateStat.replace("$currency", "" + uDTO.getCurrency());
        updateStat = updateStat.replace("$remaining", "" + uDTO.getRemaining());
        updateStat = updateStat.replace("$remaininAmount", "" + uDTO.getRemainingAmount());
        updateStat = updateStat.replace("$totalAmount", "" + uDTO.getTotalAmount());
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ReplanishmentDetailDTOInter uDTO = (ReplanishmentDetailDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id and cassete = $cassete";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        deleteStat = deleteStat.replace("$cassete", "" + uDTO.getCassete());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public void DeleteRep(List<ReplanishmentDetailDTOInter> entities) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            statement = connection.prepareStatement("delete from replanishment_detail where id = ? and cassete = ?");
            for (int i = 0; i < entities.size(); i++) {
                ReplanishmentDetailDTOInter entity = entities.get(i);
                statement.setInt(1, entity.getId());
                statement.setInt(2, entity.getCassete());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            statement.executeBatch();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ReplanishmentDetailDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        ReplanishmentDetailDTOInter uDTO = DTOFactory.createReplanishmentDetailDTO();
        while (rs.next()) {
            uDTO.setCassete(rs.getInt("cassete"));
            uDTO.setCasseteValue(rs.getInt("cassete_value"));
            uDTO.setCurrency(rs.getInt("currency"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setRemaining(rs.getInt("remaining"));
            uDTO.setRemainingAmount(rs.getInt("remaining_amount"));
            uDTO.setTotalAmount(rs.getInt("total_amount"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<ReplanishmentDetailDTOInter> uDTOL = new ArrayList<ReplanishmentDetailDTOInter>();
        while (rs.next()) {
            ReplanishmentDetailDTOInter uDTO = DTOFactory.createReplanishmentDetailDTO();
            uDTO.setCassete(rs.getInt("cassete"));
            uDTO.setCasseteValue(rs.getInt("cassete_value"));
            uDTO.setCurrency(rs.getInt("currency"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setRemaining(rs.getInt("remaining"));
            uDTO.setRemainingAmount(rs.getInt("remaining_amount"));
            uDTO.setTotalAmount(rs.getInt("total_amount"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findById(int id) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "SELECT   cassete,\n"
                + "         cassete_value,\n"
                + "         r.currency currency,\n"
                + "         r.id id,\n"
                + "         remaining,\n"
                + "         remaining_amount,\n"
                + "         total_amount,\n"
                + "         NVL (CASSETE_NAME, cassette.name) CName\n"
                + "  FROM   replanishment_detail r, cassette\n"
                + " WHERE   r.cassete = cassette.id AND r.id = $id";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        ResultSet rs = executeQuery(selectStat);
        List<ReplanishmentDetailDTOInter> uDTOL = new ArrayList<ReplanishmentDetailDTOInter>();
        while (rs.next()) {
            ReplanishmentDetailDTOInter uDTO = DTOFactory.createReplanishmentDetailDTO();
            uDTO.setCassetename(rs.getString("CName"));
            uDTO.setCassete(rs.getInt("cassete"));
            uDTO.setCasseteValue(rs.getInt("cassete_value"));
            uDTO.setCurrency(rs.getInt("currency"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setRemaining(rs.getInt("remaining"));
            uDTO.setRemainingAmount(rs.getInt("remaining_amount"));
            uDTO.setTotalAmount(rs.getInt("total_amount"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public List<RepTransDetailDTOInter> findTransDetail(String ATM, Date FromDate, Date ToDate) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select /*+INDEX(m DIS_REPLANISHMENT_INDEX)*/ distinct\n"
                + "m.atm_application_id                         atmapplicationid,\n"
                + "m.currency                                   currency,\n"
                + "decode(m.record_type,1,'J',2,'S',3,'H')      recordtype,\n"
                + "decode(m.settled_flag,1,'Y',2,'N')           settledflag,\n"
                + "decode(m.corrective_entry_flag,'1','C','2','R','3','E','U')          corrflag,\n"
                + "m.transaction_date                           transactiondate,\n"
                + "m.transaction_sequence                       transactionsequence,\n"
                + "m.amount                                     amount,\n"
                + "m.transaction_type                           transactiontype,\n"
                + "m.card_no                                    cardno,\n"
                + "m.customer_account_number                    customeraccountnumber,\n"
                + "m.response_code                              responsecode,\n"
                + "m.transaction_status                         transactionstatus,\n"
                + "m.column1                                    SAmt,\n"
                + "m.column2                                    ISS,\n"
                + "m.column3                                    Ref,\n"
                + "m.notes_presented                            notespresented,\n"
                + "m.settled_date                               settleddate,\n"
                + "m.comments                                   comments,\n"
                + "is_black_list(m.card_no)                     blacklist,\n"
                + "rownum                                        rowidnumber,\n"
                + "DISPUTE_KEY dispkey,\n"
                + "matching_type matchingtype,\n"
                + "record_type rectype\n"
                + "from disputes m\n"
                + "where  m.transaction_date between to_date('$P{DFrom}','dd.mm.yyyy hh24:mi:ss') \n"
                + "                          and to_date('$P{DTo}','dd.mm.yyyy hh24:mi:ss')  \n"
                + "and    m.atm_id = '$P{ATMID}'\n"
                + "and    m.record_type = 2 \n"
                + "and    m.transaction_type_id in (select id from transaction_type where type_category = 1) \n"
                + "and    m.matching_type = 1\n"
                + "order by atm_application_id,\n"
                + "         transaction_date,\n"
                + "         TRANSACTION_SEQUENCE,\n"
                + "         card_no,\n"
                + "         recordtype \n"
                + "desc";

        selectStat = selectStat.replace("$P{DFrom}", "" + DateFormatter.changeDateAndTimeFormat(FromDate));
        selectStat = selectStat.replace("$P{DTo}", "" + DateFormatter.changeDateAndTimeFormat(ToDate));
        selectStat = selectStat.replace("$P{ATMID}", "" + ATM);
        ResultSet rs = executeQuery(selectStat);
        List<RepTransDetailDTOInter> records = new ArrayList<RepTransDetailDTOInter>();
        while (rs.next()) {
            RepTransDetailDTOInter record = DTOFactory.createRepTransDetailDTO();
            record.setDisputekey(rs.getInt("dispkey"));
            record.setRecordtypevalue(rs.getInt("rectype"));
            record.setMatchingtype(rs.getInt("matchingtype"));
            record.setAtmapplicationid(rs.getString("atmapplicationid"));
            record.setCurrency(rs.getString("currency"));
            record.setRecordtype(rs.getString("recordtype"));
            record.setSettledflag(rs.getString("settledflag"));
            record.setCorrflag(rs.getString("corrflag"));
            record.setTransactiondate(rs.getTimestamp("transactiondate"));
            record.setTransactionsequence(rs.getString("transactionsequence"));
            record.setAmount(rs.getInt("amount"));
            record.setTransactiontype(rs.getString("transactiontype"));
            record.setCardno(rs.getString("cardno"));
            record.setCustomeraccountnumber(rs.getString("customeraccountnumber"));
            record.setResponsecode(rs.getString("responsecode"));
            record.setTransactionstatus(rs.getString("transactionstatus"));
            record.setAmt(rs.getString("SAmt"));
            record.setIss(rs.getString("ISS"));
            record.setRef(rs.getString("Ref"));
            record.setNotespresented(rs.getString("notespresented"));
            record.setSettleddate(rs.getTimestamp("settleddate"));
            record.setComments(rs.getString("comments"));
            record.setBlacklist(rs.getInt("blacklist"));
            record.setRowidnumber(rs.getInt("rowidnumber"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
