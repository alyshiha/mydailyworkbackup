/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Aly
 */
public class RepPopDTO implements RepPopDTOInter, Serializable {

    private Date currentdatefrom;
    private Date currentdateto;
    private Long currentcassetvalue;
    private Long currentcassetdiff;
    private Long currentswitchvalue;
    private Long currentswitchdiff;
    private Long currentdispute;
    private String currentcomment;

    private Date lastdatefrom;
    private Date lastdateto;
    private Long lastcassetvalue;
    private Long lastcassetdiff;
    private Long lastswitchvalue;
    private Long lastswitchdiff;
    private Long lastdispute;
    private String lastcomment;

    public Date getCurrentdatefrom() {
        return currentdatefrom;
    }

    public void setCurrentdatefrom(Date currentdatefrom) {
        this.currentdatefrom = currentdatefrom;
    }

    public Date getCurrentdateto() {
        return currentdateto;
    }

    public void setCurrentdateto(Date currentdateto) {
        this.currentdateto = currentdateto;
    }

    public Long getCurrentcassetvalue() {
        return currentcassetvalue;
    }

    public void setCurrentcassetvalue(Long currentcassetvalue) {
        this.currentcassetvalue = currentcassetvalue;
    }

    public Long getCurrentcassetdiff() {
        return currentcassetdiff;
    }

    public void setCurrentcassetdiff(Long currentcassetdiff) {
        this.currentcassetdiff = currentcassetdiff;
    }

    public Long getCurrentswitchvalue() {
        return currentswitchvalue;
    }

    public void setCurrentswitchvalue(Long currentswitchvalue) {
        this.currentswitchvalue = currentswitchvalue;
    }

    public Long getCurrentswitchdiff() {
        return currentswitchdiff;
    }

    public void setCurrentswitchdiff(Long currentswitchdiff) {
        this.currentswitchdiff = currentswitchdiff;
    }

    public Long getCurrentdispute() {
        return currentdispute;
    }

    public void setCurrentdispute(Long currentdispute) {
        this.currentdispute = currentdispute;
    }

    public String getCurrentcomment() {
        return currentcomment;
    }

    public void setCurrentcomment(String currentcomment) {
        this.currentcomment = currentcomment;
    }

    public Date getLastdatefrom() {
        return lastdatefrom;
    }

    public void setLastdatefrom(Date lastdatefrom) {
        this.lastdatefrom = lastdatefrom;
    }

    public Date getLastdateto() {
        return lastdateto;
    }

    public void setLastdateto(Date lastdateto) {
        this.lastdateto = lastdateto;
    }

    public Long getLastcassetvalue() {
        return lastcassetvalue;
    }

    public void setLastcassetvalue(Long lastcassetvalue) {
        this.lastcassetvalue = lastcassetvalue;
    }

    public Long getLastcassetdiff() {
        return lastcassetdiff;
    }

    public void setLastcassetdiff(Long lastcassetdiff) {
        this.lastcassetdiff = lastcassetdiff;
    }

    public Long getLastswitchvalue() {
        return lastswitchvalue;
    }

    public void setLastswitchvalue(Long lastswitchvalue) {
        this.lastswitchvalue = lastswitchvalue;
    }

    public Long getLastswitchdiff() {
        return lastswitchdiff;
    }

    public void setLastswitchdiff(Long lastswitchdiff) {
        this.lastswitchdiff = lastswitchdiff;
    }

    public Long getLastdispute() {
        return lastdispute;
    }

    public void setLastdispute(Long lastdispute) {
        this.lastdispute = lastdispute;
    }

    public String getLastcomment() {
        return lastcomment;
    }

    public void setLastcomment(String lastcomment) {
        this.lastcomment = lastcomment;
    }

   
}
