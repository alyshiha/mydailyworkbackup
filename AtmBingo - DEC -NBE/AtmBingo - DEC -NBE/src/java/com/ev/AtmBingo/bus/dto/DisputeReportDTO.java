/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.Date;
import oracle.sql.CLOB;

/**
 *
 * @author Administrator
 */
public class DisputeReportDTO extends BaseDTO implements DisputeReportDTOInter, Serializable{

    private String atmApplicationId;
    private AtmMachineDTOInter atmId;
    private String transactionType;
    private TransactionTypeDTOInter transactionTypeId;
    private String currency;
    private CurrencyMasterDTOInter currencyId;
    private String transactionStatus;
    private TransactionStatusDTOInter transactionStatusId;
    private String responseCode;
    private TransactionResponseCodeDTOInter responseCodeId;
    private Date transactionDate;
    private String transactionSeqeunce;
    private String cardNo;
    private int amount;
    private Date settlementDate;
    private String notesPresented;
    private String customerAccountNumber;
    private int transactionSequenceOrderBy;
    private Date transactionTime;
    private int matchingType;
    private int recordType;
    private int disputeKey;
    private String disputeReason;
    private int settledFlag;
    private Date settledDate;
    private UsersDTOInter settledUser;
    private String comments;
    private int disputeBy;
    private Date disputeDate;
    private int disputeWithRoles;
    private String column1;
    private String column2;
    private String column3;
    private String column4;
    private String column5;
    private int transactionTypeMaster;
    private int responseCodeMaster;
    private int cardNoSuffix;
    private int userId;
private CLOB transactiondata;
private String exportuser;
private String correctiveuser;
 private Date correctivedate;
    private Date exportdate;

    public String getExportuser() {
        return exportuser;
    }

    public void setExportuser(String exportuser) {
        this.exportuser = exportuser;
    }

    public String getCorrectiveuser() {
        return correctiveuser;
    }

    public void setCorrectiveuser(String correctiveuser) {
        this.correctiveuser = correctiveuser;
    }

  
    
    public Date getCorrectivedate() {
        return correctivedate;
    }

    public void setCorrectivedate(Date correctivedate) {
        this.correctivedate = correctivedate;
    }

    public Date getExportdate() {
        return exportdate;
    }
    public void setExportdate(Date exportdate) {
        this.exportdate = exportdate;
    }


    public CLOB getTransactiondata() {
        return transactiondata;
    }

    public void setTransactiondata(CLOB transactiondata) {
        this.transactiondata = transactiondata;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getAtmApplicationId() {
        return atmApplicationId;
    }

    public void setAtmApplicationId(String atmApplicationId) {
        this.atmApplicationId = atmApplicationId;
    }

    public AtmMachineDTOInter getAtmId() {
        return atmId;
    }

    public void setAtmId(AtmMachineDTOInter atmId) {
        this.atmId = atmId;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public int getCardNoSuffix() {
        return cardNoSuffix;
    }

    public void setCardNoSuffix(int cardNoSuffix) {
        this.cardNoSuffix = cardNoSuffix;
    }

    public String getColumn1() {
        return column1;
    }

    public void setColumn1(String column1) {
        this.column1 = column1;
    }

    public String getColumn2() {
        return column2;
    }

    public void setColumn2(String column2) {
        this.column2 = column2;
    }

    public String getColumn3() {
        return column3;
    }

    public void setColumn3(String column3) {
        this.column3 = column3;
    }

    public String getColumn4() {
        return column4;
    }

    public void setColumn4(String column4) {
        this.column4 = column4;
    }

    public String getColumn5() {
        return column5;
    }

    public void setColumn5(String column5) {
        this.column5 = column5;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public CurrencyMasterDTOInter getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(CurrencyMasterDTOInter currencyId) {
        this.currencyId = currencyId;
    }

    public String getCustomerAccountNumber() {
        return customerAccountNumber;
    }

    public void setCustomerAccountNumber(String customerAccountNumber) {
        this.customerAccountNumber = customerAccountNumber;
    }

    public int getDisputeBy() {
        return disputeBy;
    }

    public void setDisputeBy(int disputeBy) {
        this.disputeBy = disputeBy;
    }

    public Date getDisputeDate() {
        return disputeDate;
    }

    public void setDisputeDate(Date disputeDate) {
        this.disputeDate = disputeDate;
    }

    public int getDisputeKey() {
        return disputeKey;
    }

    public void setDisputeKey(int disputeKey) {
        this.disputeKey = disputeKey;
    }

    public String getDisputeReason() {
        return disputeReason;
    }

    public void setDisputeReason(String disputeReason) {
        this.disputeReason = disputeReason;
    }

    public int getDisputeWithRoles() {
        return disputeWithRoles;
    }

    public void setDisputeWithRoles(int disputeWithRoles) {
        this.disputeWithRoles = disputeWithRoles;
    }

    public int getMatchingType() {
        return matchingType;
    }

    public void setMatchingType(int matchingType) {
        this.matchingType = matchingType;
    }

    public String getNotesPresented() {
        return notesPresented;
    }

    public void setNotesPresented(String notesPresented) {
        this.notesPresented = notesPresented;
    }

    public int getRecordType() {
        return recordType;
    }

    public void setRecordType(int recordType) {
        this.recordType = recordType;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public TransactionResponseCodeDTOInter getResponseCodeId() {
        return responseCodeId;
    }

    public void setResponseCodeId(TransactionResponseCodeDTOInter responseCodeId) {
        this.responseCodeId = responseCodeId;
    }

    public int getResponseCodeMaster() {
        return responseCodeMaster;
    }

    public void setResponseCodeMaster(int responseCodeMaster) {
        this.responseCodeMaster = responseCodeMaster;
    }

    public Date getSettledDate() {
        return settledDate;
    }

    public void setSettledDate(Date settledDate) {
        this.settledDate = settledDate;
    }

    public int getSettledFlag() {
        return settledFlag;
    }

    public void setSettledFlag(int settledFlag) {
        this.settledFlag = settledFlag;
    }

    public UsersDTOInter getSettledUser() {
        return settledUser;
    }

    public void setSettledUser(UsersDTOInter settledUser) {
        this.settledUser = settledUser;
    }

    public Date getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(Date settlementDate) {
        this.settlementDate = settlementDate;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionSeqeunce() {
        return transactionSeqeunce;
    }

    public void setTransactionSeqeunce(String transactionSeqeunce) {
        this.transactionSeqeunce = transactionSeqeunce;
    }

    public int getTransactionSequenceOrderBy() {
        return transactionSequenceOrderBy;
    }

    public void setTransactionSequenceOrderBy(int transactionSequenceOrderBy) {
        this.transactionSequenceOrderBy = transactionSequenceOrderBy;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public TransactionStatusDTOInter getTransactionStatusId() {
        return transactionStatusId;
    }

    public void setTransactionStatusId(TransactionStatusDTOInter transactionStatusId) {
        this.transactionStatusId = transactionStatusId;
    }

    public Date getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Date transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public TransactionTypeDTOInter getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(TransactionTypeDTOInter transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public int getTransactionTypeMaster() {
        return transactionTypeMaster;
    }

    public void setTransactionTypeMaster(int transactionTypeMaster) {
        this.transactionTypeMaster = transactionTypeMaster;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    
    protected DisputeReportDTO() {
        super();
    }

}
