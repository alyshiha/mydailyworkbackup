/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.LoadingMonitorDTOInter;

/**
 *
 * @author Administrator
 */
public interface FilesToMoveDAOInter {

    Object findAllFiles() throws Throwable;
    
    Object deletefile(Object... obj) throws Throwable ;

    Object findAll() throws Throwable;

    int findActiveFiles() throws Throwable;

    int findFilesToBuckup() throws Throwable;

    Object getErrorsMonitor() throws Throwable;

    Object getLoadingMonitor() throws Throwable;

    Object getMatchingMonitor() throws Throwable;

    Object getValidationMonitor() throws Throwable;

    Object rematch() throws Throwable;
}
