/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.AtmFileLogBOInter;
import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import com.ev.AtmBingo.bus.dto.DuplicateTransactionsDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class ATMFileLogClient extends BaseBean implements Serializable {

    private AtmFileLogBOInter atmObject;
    private List<AtmFileDTOInter> atmFileList;
    private AtmFileDTOInter atmAttr;
    private List<DuplicateTransactionsDTOInter> duplicateList;
    private Integer listSize, fileType, template;
    private Date fromDate, toDate;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getFileType() {
        return fileType;
    }

    public void setFileType(Integer fileType) {
        this.fileType = fileType;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Integer getTemplate() {
        return template;
    }

    public void setTemplate(Integer template) {
        this.template = template;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public List<DuplicateTransactionsDTOInter> getDuplicateList() {
        return duplicateList;
    }

    public void setDuplicateList(List<DuplicateTransactionsDTOInter> duplicateList) {
        this.duplicateList = duplicateList;
    }

    public AtmFileDTOInter getAtmAttr() {
        return atmAttr;
    }

    public void setAtmAttr(AtmFileDTOInter atmAttr) {
        this.atmAttr = atmAttr;
    }

    public List<AtmFileDTOInter> getAtmFileList() {
        return atmFileList;
    }

    public void setAtmFileList(List<AtmFileDTOInter> atmFileList) {
        this.atmFileList = atmFileList;
    }

    /**
     * Creates a new instance of ATMFileLogClient
     */
    public ATMFileLogClient() throws Throwable {
        super();
        super.GetAccess();
        atmObject = BOFactory.createAtmFileLogBO(null);
        FacesContext context = FacesContext.getCurrentInstance();

        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        // atmFileList = (List<AtmFileDTOInter>) atmObject.getAtmFile();
        //  listSize = atmFileList.size();
    }

    public void showDuplicatedItem(ActionEvent e) throws Throwable {
        atmAttr = (AtmFileDTOInter) e.getComponent().getAttributes().get("duplicatedATM");
        duplicateList = (List<DuplicateTransactionsDTOInter>) atmObject.getDuplicationTransaction(atmAttr);
    }

    public void doSearch() {
        try {
            if (fromDate == null || toDate == null) {
                message = super.showMessage(REQ_FIELD);
            } else {
                atmFileList = (List<AtmFileDTOInter>) atmObject.getAtmFile(fromDate, toDate, fileType);
                listSize = atmFileList.size();
                message = "";
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();

    }
}
