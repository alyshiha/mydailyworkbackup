/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.AtmDefinitionBOInter;
import com.ev.AtmBingo.bus.bo.AtmFileLogBOInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.DeleteTransactionsDataBOInter;
import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DeleteTransactionsDataDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class DeleteTransactionsDataClient extends BaseBean  implements Serializable{

    private UsersDTOInter logedinUser;
    private List<AtmMachineDTOInter> atmList;
    private AtmDefinitionBOInter atmObject;
    private AtmFileLogBOInter atmFileObject;
    private DeleteTransactionsDataBOInter myObject;
    private List<AtmFileDTOInter> atmFileList;
    private Date dateFrom, dateTo, startdate, loadingFrom, loadingTo;
    private DeleteTransactionsDataBOInter DeleteTransBO;
    private DeleteTransactionsDataDTOInter MyEntryList;
    private Integer atmID, fileID;
    private String message;
    private Boolean fileFlag, atmFlag;

    public Date getLoadingFrom() {
        return loadingFrom;
    }

    public void setLoadingFrom(Date loadingFrom) {
        this.loadingFrom = loadingFrom;
    }

    public Date getLoadingTo() {
        return loadingTo;
    }

    public void setLoadingTo(Date loadingTo) {
        this.loadingTo = loadingTo;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Boolean getAtmFlag() {
        return atmFlag;
    }

    public void setAtmFlag(Boolean atmFlag) {
        this.atmFlag = atmFlag;
    }

    public Boolean getFileFlag() {
        return fileFlag;
    }

    public void setFileFlag(Boolean fileFlag) {
        this.fileFlag = fileFlag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getAtmID() {
        return atmID;
    }

    public void setAtmID(Integer atmID) {
        this.atmID = atmID;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Integer getFileID() {
        return fileID;
    }

    public void setFileID(Integer fileID) {
        this.fileID = fileID;
    }

    public List<AtmMachineDTOInter> getAtmList() {
        return atmList;
    }

    public void setAtmList(List<AtmMachineDTOInter> atmList) {
        this.atmList = atmList;
    }

    public DeleteTransactionsDataDTOInter getMyEntryList() {
        return MyEntryList;
    }

    public void setMyEntryList(DeleteTransactionsDataDTOInter MyEntryList) {
        this.MyEntryList = MyEntryList;
    }

    public List<AtmFileDTOInter> getAtmFileList() {
        return atmFileList;
    }

    public void setAtmFileList(List<AtmFileDTOInter> atmFileList) {
        this.atmFileList = atmFileList;
    }

    public UsersDTOInter getLogedinUser() {
        return logedinUser;
    }

    public void setLogedinUser(UsersDTOInter logedinUser) {
        this.logedinUser = logedinUser;
    }

    public List<AtmMachineDTOInter> getatmList() {
        return atmList;
    }

    public void setatmList(List<AtmMachineDTOInter> atmList) {
        this.atmList = atmList;
    }

    public DeleteTransactionsDataClient() throws Throwable {
        super();
        super.GetAccess();
        myObject = BOFactory.createDeleteTransactionsDataBO(null);
        atmObject = BOFactory.createAtmDefinitionBO(null);
        atmList = (List<AtmMachineDTOInter>) atmObject.getAtmMachinesOutLic();
        atmFileObject = BOFactory.createAtmFileLogBO(null);
     FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void atmChange(ValueChangeEvent e) {
        if ((Integer) e.getNewValue() == 0) {
            fileFlag = false;
        } else {
            fileFlag = true;
        }
        resetVars();
    }

    public void fileChange(ValueChangeEvent e) {
        if ((Integer) e.getNewValue() == 0) {
            atmFlag = false;
        } else {
            atmFlag = true;
        }
        resetVars();
    }

    public void deleteData() {
        try {
            if (startdate == null) {
                message = super.showMessage(REQ_FIELD);
            } else if (dateTo == null && dateFrom == null && loadingFrom == null && loadingTo == null) {
                message = super.showMessage(REQ_FIELD);
            } else {
                MyEntryList = DTOFactory.createDeleteTransactionsDataDTO();
                MyEntryList.setUserId(super.getLoggedInUser().getUserId());
                if (atmID != null && !atmID.equals("")) {
                    MyEntryList.setATMid(atmID);
                }
                if (fileID != null && !fileID.equals("")) {
                    MyEntryList.setFileid(fileID);
                }
                MyEntryList.setSearchDateFrom(dateFrom);
                MyEntryList.setSearchDateTo(dateTo);
                MyEntryList.setStartDate(startdate);
                myObject.DeleteTransactionsData(MyEntryList);



                message = super.showMessage(SUCC);
            }
            // validate ya solom 3ala
            // -1- Date From Lazem Ykon Less Than Date To--
            // -2- have To Choose File Id or Atm Id  ---Note:'ONLY ONE OF THEM'-----
            // -3- ATMID Or FileID Is Needed--
            // -4- Pop Up ---Are You Sure Do You Want To Delete Transactions Data---
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void checkLoading(AjaxBehaviorEvent e) {
        if (loadingFrom != null) {
            if (loadingTo != null) {
                try {
                    atmFileList = (List<AtmFileDTOInter>) atmFileObject.getAtmFileByloadingdate(loadingFrom, loadingTo);
                } catch (Throwable ex) {
                    Logger.getLogger(DeleteTransactionsDataClient.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void resetVars() {
        message = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
