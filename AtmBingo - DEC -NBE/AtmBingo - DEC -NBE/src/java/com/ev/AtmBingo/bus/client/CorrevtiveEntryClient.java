/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.CorrectiveEntryLogBOInter;
import com.ev.AtmBingo.bus.bo.ExportBoInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.CorrectiveEntryLogDTOInter;
import com.ev.AtmBingo.bus.dto.ExportLogDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class CorrevtiveEntryClient extends BaseBean  implements Serializable{

    private CorrectiveEntryLogBOInter myObject;
    private ExportBoInter exportObject;
    private List<ExportLogDTOInter> exportList;
    private List<CorrectiveEntryLogDTOInter> correctiveList;
    private List<AtmMachineDTOInter> atmList;
    private List<UsersDTOInter> userList;
    private Date fromDate, toDate;
    private Integer userId, atmId, operationId;
    private String message;

    public List<ExportLogDTOInter> getExportList() {
        return exportList;
    }

    public void setExportList(List<ExportLogDTOInter> exportList) {
        this.exportList = exportList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public Integer getAtmId() {
        return atmId;
    }

    public void setAtmId(Integer atmId) {
        this.atmId = atmId;
    }

    public List<AtmMachineDTOInter> getAtmList() {
        return atmList;
    }

    public void setAtmList(List<AtmMachineDTOInter> atmList) {
        this.atmList = atmList;
    }

    public List<CorrectiveEntryLogDTOInter> getCorrectiveList() {
        return correctiveList;
    }

    public void setCorrectiveList(List<CorrectiveEntryLogDTOInter> correctiveList) {
        this.correctiveList = correctiveList;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<UsersDTOInter> getUserList() {
        return userList;
    }

    public void setUserList(List<UsersDTOInter> userList) {
        this.userList = userList;
    }

    /** Creates a new instance of CorrevtiveEntryClient */
    public CorrevtiveEntryClient() throws Throwable {
        super();
        super.GetAccess();
        
        myObject = BOFactory.createCorrectiveEntryLogBO(null);
        exportObject = BOFactory.createExportBo(null);
        atmList = (List<AtmMachineDTOInter>) myObject.getAtmMachines(super.getLoggedInUser());
        userList = (List<UsersDTOInter>) myObject.getUsers(super.getRestrected());
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void retrieveCorretive() {
        try {
            correctiveList = (List<CorrectiveEntryLogDTOInter>) myObject.getCorrectiveLogged(fromDate, toDate, userId, atmId, operationId);
            resetVars();
        } catch (Throwable ex) {
        }
    }

    public void retrieveExport() {
        try {
            exportList = (List<ExportLogDTOInter>) exportObject.getExportLogged(fromDate, toDate, userId, atmId, operationId);
            resetVars();
        } catch (Throwable ex) {
        }
    }

    public void resetVars() {
        message = "";
    }  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }
}
