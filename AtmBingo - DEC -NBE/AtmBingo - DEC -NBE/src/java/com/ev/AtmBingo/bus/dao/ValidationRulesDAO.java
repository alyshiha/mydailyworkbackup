/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ValidationRulesDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ValidationRulesDAO extends BaseDAO implements ValidationRulesDAOInter {

    protected ValidationRulesDAO() {
        super();
        super.setTableName("validation_rules");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ValidationRulesDTOInter uDTO = (ValidationRulesDTOInter) obj[0];

        uDTO.setId(super.generateSequence(super.getTableName()));
        editRules(uDTO, 1);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ValidationRulesDTOInter uDTO = (ValidationRulesDTOInter) obj[0];
        editRules(uDTO, 2);
        super.postUpdate(null, true);
        return null;
    }

    private Object editRules(ValidationRulesDTOInter drDTO, int operation) throws Throwable {
        super.preCollable();
        CallableStatement stat = super.executeCallableStatment("{call edit_rules(?,?,?,?,?,?,?,?)}");
        stat.setInt(1, operation);
        stat.setInt(2, drDTO.getId());
        stat.setInt(3, drDTO.getValidationType());
        stat.setInt(4, drDTO.getColumnId());
        stat.setString(5, drDTO.getName());
        stat.setString(6, drDTO.getFormula());
        stat.setString(7, drDTO.getFormulaCode());
        stat.setInt(8, drDTO.getActive());
        stat.executeUpdate();
        super.postCollable(stat);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ValidationRulesDTOInter uDTO = (ValidationRulesDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        ValidationRulesDTOInter uDTO = DTOFactory.createValidationRulesDTO();
        while (rs.next()) {
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setActive(rs.getInt("active"));
            uDTO.setFormula(rs.getString("formula"));
            uDTO.setFormulaCode(rs.getString("formula_code"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setName(rs.getString("name"));
            uDTO.setValidationType(rs.getInt("valdation_type"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<ValidationRulesDTOInter> uDTOL = new ArrayList<ValidationRulesDTOInter>();
        while (rs.next()) {
            ValidationRulesDTOInter uDTO = DTOFactory.createValidationRulesDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setActive(rs.getInt("active"));
            uDTO.setFormula(rs.getString("formula"));
            uDTO.setFormulaCode(rs.getString("formula_code"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setName(rs.getString("name"));
            uDTO.setValidationType(rs.getInt("valdation_type"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByValidationType(int validationType) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where valdation_type = $validationType order by name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$validationType", "" + validationType);
        ResultSet rs = executeQuery(selectStat);
        List<ValidationRulesDTOInter> uDTOL = new ArrayList<ValidationRulesDTOInter>();
        while (rs.next()) {
            ValidationRulesDTOInter uDTO = DTOFactory.createValidationRulesDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setActive(rs.getInt("active"));
            uDTO.setFormula(rs.getString("formula"));
            uDTO.setFormulaCode(rs.getString("formula_code"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setName(rs.getString("name"));
            uDTO.setValidationType(rs.getInt("valdation_type"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByColumnId(int columnId) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where column_id = $column order by name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$column", "" + columnId);
        ResultSet rs = executeQuery(selectStat);
        List<ValidationRulesDTOInter> uDTOL = new ArrayList<ValidationRulesDTOInter>();
        while (rs.next()) {
            ValidationRulesDTOInter uDTO = DTOFactory.createValidationRulesDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setActive(rs.getInt("active"));
            uDTO.setFormula(rs.getString("formula"));
            uDTO.setFormulaCode(rs.getString("formula_code"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setName(rs.getString("name"));
            uDTO.setValidationType(rs.getInt("valdation_type"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Boolean checkFormula(String formula,Integer Type) throws Throwable {

        super.preCollable();
        //String test = formula.replace("'", "''");
        CallableStatement stat = super.executeCallableStatment("{call ? := check_formula_java(?,?)}");
        Boolean done = false;
        stat.registerOutParameter(1, Types.NUMERIC);
        stat.setString(2,formula );
        stat.setInt(3, Type);
        stat.executeUpdate();
           
        int i = stat.getInt(1);
        done = i == 1;
        super.postCollable(stat);
        return done;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
