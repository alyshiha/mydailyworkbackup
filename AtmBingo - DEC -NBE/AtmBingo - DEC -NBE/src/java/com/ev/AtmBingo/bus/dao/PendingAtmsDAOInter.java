/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.PendingAtmsDTOInter;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Administrator
 */

 public interface PendingAtmsDAOInter extends Serializable{

      Object findAllPendingAtms(Object... obj) throws Throwable;

      Object findAtmMachineType(Object... obj) throws Throwable;

      void save(List<PendingAtmsDTOInter> entities) throws SQLException;
      void saveAtm(PendingAtmsDTOInter entities) throws SQLException;
      

}
