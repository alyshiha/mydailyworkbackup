/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ReplanishmentReportDTOInter;
import com.ev.AtmBingo.bus.dto.repreportcassetteexcelDTOInter;
import com.ev.AtmBingo.bus.dto.repreportexcelDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ReplanishmentReportDAO extends BaseDAO implements ReplanishmentReportDAOInter {

    protected ReplanishmentReportDAO() {
        super();
        super.setTableName("REPLANISHMENT_REPORT");
    }

    public int getSequence() throws Throwable {
        String seq = "select $table_seq.nextval from dual";
        seq = seq.replace("$table", super.getTableName());
        ResultSet rs = executeQueryReport(seq);
        int s = 0;
        while (rs.next()) {
            s = rs.getInt(1);
        }
        postSelect(rs);
        return s;
    }

    public List<AVGDailyDispenseDTOInter> averageloadamountexcel(String dateFrom, String dateTo, String group, String atm) throws Throwable {
        String seq = " select (select a.application_id from atm_machine a where a.id = t.atm_id) atm_id, avg(t.cassitte_value) AVG from replanishment_master t where t.date_from  between to_date('$DateFrom','dd.MM.yyyy hh24:mi:ss') and to_date('$DateTo','dd.MM.yyyy hh24:mi:ss') and t.atm_id in (select a.id from atm_machine a where a.id = t.atm_id and a.atm_group like trim('%$ATMGroup%')) and rownum < $ATMID group by t.atm_id order by 1";
        seq = seq.replace("$DateFrom", "" + dateFrom);
        seq = seq.replace("$DateTo", "" + dateTo);
        seq = seq.replace("$ATMGroup", "" + group);
        seq = seq.replace("$ATMID", "" + atm);
        ResultSet rs = executeQueryReport(seq);
        List<AVGDailyDispenseDTOInter> uDTOL = new ArrayList<AVGDailyDispenseDTOInter>();
        while (rs.next()) {
            AVGDailyDispenseDTOInter uDTO = DTOFactory.createAVGDailyDispenseDTO();
            uDTO.setAtmid(rs.getString("atm_id"));
            uDTO.setAvg(rs.getString("AVG"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public String averageloadamount(String dateFrom, String dateTo, String group, String atm) throws Throwable {
        String seq = " select "
                + " (select sum(t.cassitte_value)"
                + " from replanishment_master t"
                + " where t.date_from  between to_date('$DateFrom','dd.MM.yyyy hh24:mi:ss') and to_date('$DateTo','dd.MM.yyyy hh24:mi:ss')"
                + " and t.atm_id in (select a.id from atm_machine a where a.id = t.atm_id and a.atm_group like trim('%$ATMGroup%'))"
                + " and rownum < $ATMID group by 1)"
                + " /"
                + " (select count((select a.application_id from atm_machine a where a.id = t.atm_id)) atm_id"
                + " from replanishment_master t"
                + " where t.date_from  between to_date('$DateFrom','dd.MM.yyyy hh24:mi:ss') and to_date('$DateTo','dd.MM.yyyy hh24:mi:ss')"
                + " and t.atm_id in (select a.id from atm_machine a where a.id = t.atm_id and a.atm_group like trim('%$ATMGroup%'))"
                + " and rownum < $ATMID)"
                + " from dual";
        seq = seq.replace("$DateFrom", "" + dateFrom);
        seq = seq.replace("$DateTo", "" + dateTo);
        seq = seq.replace("$ATMGroup", "" + group);
        seq = seq.replace("$ATMID", "" + atm);
        ResultSet rs = executeQueryReport(seq);
        String s = "";
        while (rs.next()) {
            s = Integer.toString(rs.getInt(1));
        }
        postSelect(rs);
        return s;
    }

    public List<AVGDailyDispenseDTOInter> averageRepCycleexcel(String AtmID, String AtmGroup, String dateFrom, String dateTo) throws Throwable {
        String Stat = "select (select a.application_id from atm_machine a where a.id = t.atm_id) atm_id, avg((t.date_to - t.date_from)) avg  from replanishment_master t where 1=1 ";

        if (AtmID == null ? "" != null : !AtmID.equals("")) {
            Stat += " and t.atm_id in (select a.id from atm_machine a where a.id = t.atm_id and a.application_id like trim('%$ATMID%'))";
            Stat = Stat.replace("$ATMID", "" + AtmID);
        }
        if (AtmGroup.toString() == null ? "0" != null : !AtmGroup.toString().equals("0")) {
            Stat += " and t.atm_id in (select a.id from atm_machine a where a.id = t.atm_id and a.atm_group like trim('%$ATMGroup%'))";
            Stat = Stat.replace("$ATMGroup", AtmGroup);
        }

        Stat += " and  t.date_from  between to_date('$DateFrom','dd.MM.yyyy hh24:mi:ss') and to_date('$DateTo','dd.MM.yyyy hh24:mi:ss')";
        Stat = Stat.replace("$DateFrom", dateFrom);
        Stat = Stat.replace("$DateTo", dateTo);
        Stat += " group by t.atm_id order by 1";

        ResultSet rs = executeQueryReport(Stat);
        List<AVGDailyDispenseDTOInter> uDTOL = new ArrayList<AVGDailyDispenseDTOInter>();
        while (rs.next()) {
            AVGDailyDispenseDTOInter uDTO = DTOFactory.createAVGDailyDispenseDTO();
            uDTO.setAtmid(rs.getString("atm_id"));
            uDTO.setAvg(rs.getString("avg"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public String averageRepCycle(String AtmID, String AtmGroup, String dateFrom, String dateTo) throws Throwable {
        String Stat = "select (select a.application_id from atm_machine a where a.id = t.atm_id) atm_id, avg((t.date_to - t.date_from))  from replanishment_master t where 1=1 ";

        if (AtmID == null ? "" != null : !AtmID.equals("")) {
            Stat += " and t.atm_id in (select a.id from atm_machine a where a.id = t.atm_id and a.application_id like trim('%$ATMID%'))";
            Stat = Stat.replace("$ATMID", "" + AtmID);
        }
        if (AtmGroup.toString() == null ? "0" != null : !AtmGroup.toString().equals("0")) {
            Stat += " and t.atm_id in (select a.id from atm_machine a where a.id = t.atm_id and a.atm_group like trim('%$ATMGroup%'))";
            Stat = Stat.replace("$ATMGroup", AtmGroup);
        }

        Stat += " and  t.date_from  between to_date('$DateFrom','dd.MM.yyyy hh24:mi:ss') and to_date('$DateTo','dd.MM.yyyy hh24:mi:ss')";
        Stat = Stat.replace("$DateFrom", dateFrom);
        Stat = Stat.replace("$DateTo", dateTo);
        Stat += " group by t.atm_id order by 1";

        ResultSet rs = executeQueryReport(Stat);
        String s = "";
        Integer count = 0;
        Integer magmo3 = 0;
        while (rs.next()) {
            magmo3 = magmo3 + rs.getInt(2);
            count++;
        }
        magmo3 = magmo3 / count;
        postSelect(rs);
        return magmo3.toString();
    }

    public List<AVGDailyDispenseDTOInter> Dailydispenseexcel(String dateFrom, String dateTo, String AtmID, String AtmGroup) throws Throwable {
        String Stat = "select (select a.application_id from atm_machine a where a.id = t.atm_id) atm_id, round(avg(t.switch/ (t.date_to - t.date_from))) avg_dispense from replanishment_master t where 1=1 ";
        if (AtmID == null ? "" != null : !AtmID.equals("")) {
            Stat += " and t.atm_id in (select a.id from atm_machine a where a.id = t.atm_id and a.application_id like trim('%$ATMID%'))";
            Stat = Stat.replace("$ATMID", "" + AtmID);
        }
        if (AtmGroup.toString() == null ? "0" != null : !AtmGroup.toString().equals("0")) {
            Stat += " and t.atm_id in (select a.id from atm_machine a where a.id = t.atm_id and a.atm_group like trim('%$ATMGroup%'))";
            Stat = Stat.replace("$ATMGroup", AtmGroup);
        }

        Stat += " and  t.date_from  between to_date('$DateFrom','dd.MM.yyyy hh24:mi:ss') and to_date('$DateTo','dd.MM.yyyy hh24:mi:ss')";
        Stat = Stat.replace("$DateFrom", dateFrom);
        Stat = Stat.replace("$DateTo", dateTo);
        Stat += " and t.date_to > t.date_from group by t.atm_id order by 2 desc";

        ResultSet rs = executeQueryReport(Stat);
        List<AVGDailyDispenseDTOInter> uDTOL = new ArrayList<AVGDailyDispenseDTOInter>();
        while (rs.next()) {
            AVGDailyDispenseDTOInter uDTO = DTOFactory.createAVGDailyDispenseDTO();
            uDTO.setAtmid(rs.getString("atm_id"));
            uDTO.setAvg(rs.getString("avg_dispense"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public String Dailydispense(String dateFrom, String dateTo, String AtmID, String AtmGroup) throws Throwable {
        String Stat = "select (select a.application_id from atm_machine a where a.id = t.atm_id) atm_id, round(avg(t.switch/ (t.date_to - t.date_from))) avg_dispense from replanishment_master t where 1=1 ";
        if (AtmID == null ? "" != null : !AtmID.equals("")) {
            Stat += " and t.atm_id in (select a.id from atm_machine a where a.id = t.atm_id and a.application_id like trim('%$ATMID%'))";
            Stat = Stat.replace("$ATMID", "" + AtmID);
        }
        if (AtmGroup.toString() == null ? "0" != null : !AtmGroup.toString().equals("0")) {
            Stat += " and t.atm_id in (select a.id from atm_machine a where a.id = t.atm_id and a.atm_group like trim('%$ATMGroup%'))";
            Stat = Stat.replace("$ATMGroup", AtmGroup);
        }

        Stat += " and  t.date_from  between to_date('$DateFrom','dd.MM.yyyy hh24:mi:ss') and to_date('$DateTo','dd.MM.yyyy hh24:mi:ss')";
        Stat = Stat.replace("$DateFrom", dateFrom);
        Stat = Stat.replace("$DateTo", dateTo);
        Stat += " and t.date_to > t.date_from group by t.atm_id order by 2 desc";

        ResultSet rs = executeQueryReport(Stat);
        String s = "";
        Integer count = 0;
        Integer magmo3 = 0;
        while (rs.next()) {
            magmo3 = magmo3 + rs.getInt(2);
            count++;
        }
        magmo3 = magmo3 / count;
        postSelect(rs);
        return magmo3.toString();
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ReplanishmentReportDTOInter uDTO = (ReplanishmentReportDTOInter) obj[0];
        String insertStat = "insert into $table (id,rep_id,rep_id2) values ($id, $rep_id,$rid2)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$rep_id", "" + uDTO.getRepId());
        insertStat = insertStat.replace("$rid2", "" + uDTO.getRepId2());

        super.executeUpdateReport(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object insertPrv(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        ReplanishmentReportDTOInter uDTO = (ReplanishmentReportDTOInter) obj[0];
        String insertStat = "insert into REPLANISHMENT_REPORT_PRV (id,rep_id,rep_id2) values ($id, $rep_id,$rid2)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$rep_id", "" + uDTO.getRepId());
        insertStat = insertStat.replace("$rid2", "" + uDTO.getRepId2());

        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        int uDTO = (Integer) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO);
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object deletePrv(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        int uDTO = (Integer) obj[0];
        String deleteStat = "delete from REPLANISHMENT_REPORT_PRV where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO);
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object calcReplanishment(int id) throws Throwable {
        preCollable();
        CallableStatement stat = super.executeCallableStatment("{call replanishment.calc_replanishment("
                + "?)}");
        stat.setInt(1, id);
        stat.execute();
        postCollable(stat);
        return null;
    }

    public Integer calcReplanishmentGaps(String AtmID, String AtmGroup, Date dateFrom, Date dateTo) throws Throwable {
        super.preSelect();
        String Stat = "select get_replanishment_gaps('$AtmID','$AtmGroup',to_date('$dateFrom','dd.mm.yyyy hh24:mi:ss'),to_date('$dateT0','dd.mm.yyyy hh24:mi:ss')) from dual";
        Stat = Stat.replace("$AtmID", "" + AtmID);
        Stat = Stat.replace("$AtmGroup", AtmGroup);
        Stat = Stat.replace("$dateFrom", DateFormatter.changeDateAndTimeFormat(dateFrom).toString());
        Stat = Stat.replace("$dateT0", DateFormatter.changeDateAndTimeFormat(dateTo).toString());
        ResultSet rs = executeQueryReport(Stat);
        Integer result = 0;
        while (rs.next()) {
            result = rs.getInt(1);
        }
        postSelect(rs);
        return result;
    }

    public Object DeletReplanishmentGaps(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        int ID = (Integer) obj[0];
        String deleteStat = "delete from REPLANISHMENT_GAPS where S_ID = $id";
        deleteStat = deleteStat.replace("$id", "" + ID);
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public List<repreportexcelDTOInter> getrepreoprtexcel(Object... obj) throws Throwable {
        int ID = (Integer) obj[0];
        String seq = "select m.* ,to_char(m.dailysheet,'dd mm YYYY') dailysheetdate,(select application_id||'     -     '||name||'     -     '||  unit_number from atm_machine where id = m.atm_id) atm_data,(select mm.date_from from replanishment_master mm where mm.id = (select rep_id2 from replanishment_report where id = $repid and rep_id = m.id))last_date_from,(select mm.date_to from replanishment_master mm where mm.id = (select rep_id2 from replanishment_report where id = $repid and rep_id = m.id ))last_date_to,(select mm.comments from replanishment_master mm where mm.id = (select rep_id2 from replanishment_report where id = $repid and rep_id = m.id))last_comment,(select r.cassitte_value from replanishment_master r where id = (select rep_id2 from replanishment_report where id = $repid and rep_id = m.id)) cassitte_value2,(select r.SWITCH from replanishment_master r where id = (select rep_id2 from replanishment_report where id = $repid and rep_id = m.id) ) switch_value2,(select r.switch_diff from replanishment_master r where id = (select rep_id2 from replanishment_report where id = $repid and rep_id = m.id) ) switch_diff2,(select r.cassitte_diff from replanishment_master r where id = (select rep_id2 from replanishment_report where id = $repid and rep_id = m.id) ) cassitte_diff2,(select r.disputes from replanishment_master r where id = (select rep_id2 from replanishment_report where id = $repid and rep_id = m.id) ) disputes2 from replanishment_master m where m.id in (select rep_id from replanishment_report where id = $repid) order by (select application_id from atm_machine where id =m.atm_id),m.date_from";
        seq = seq.replace("$repid", "" + ID);
        ResultSet rs = executeQueryReport(seq);
        //System.out.println(seq);
        Integer round = 2;
        List<repreportexcelDTOInter> uDTOL = new ArrayList<repreportexcelDTOInter>();
        while (rs.next()) {
            if (round == 2) {
                repreportexcelDTOInter uDTO = DTOFactory.createrepreportexcelDTO();
                uDTO.setColoumn1("ATM: ");
                uDTO.setColoumn2(rs.getString("atm_data"));
                uDTO.setColoumn4("Daily Sheet Date: ");
                uDTO.setColoumn5(rs.getString("dailysheetdate"));
                uDTOL.add(uDTO);
                repreportexcelDTOInter uDTO2 = DTOFactory.createrepreportexcelDTO();
                uDTOL.add(uDTO2);
                round = 0;
            }
            if (round == 0) {
                repreportexcelDTOInter uDTO3 = DTOFactory.createrepreportexcelDTO();
                uDTO3.setColoumn1("Previous ");
                uDTO3.setColoumn2("Replenishment:");
                uDTO3.setColoumn9(rs.getString("id"));
                uDTOL.add(uDTO3);
                repreportexcelDTOInter uDTO = DTOFactory.createrepreportexcelDTO();
                uDTO.setColoumn1("Date From");
                uDTO.setColoumn2("Date To");
                uDTO.setColoumn3("Cassette Value");
                uDTO.setColoumn4("Switch Value");
                uDTO.setColoumn5("Switch Diff");
                uDTO.setColoumn6("Cassette Diff");
                uDTO.setColoumn7("Disputes");
                uDTO.setColoumn8("Comments");
                uDTOL.add(uDTO);
                repreportexcelDTOInter uDTO2 = DTOFactory.createrepreportexcelDTO();
                uDTO2.setColoumn1(rs.getString("last_date_from"));
                uDTO2.setColoumn2(rs.getString("last_date_to"));
                uDTO2.setColoumn3(rs.getString("cassitte_value2"));
                uDTO2.setColoumn4(rs.getString("switch_value2"));
                uDTO2.setColoumn5(rs.getString("switch_diff2"));
                uDTO2.setColoumn6(rs.getString("cassitte_diff2"));
                uDTO2.setColoumn7(rs.getString("disputes2"));
                uDTO2.setColoumn8(rs.getString("last_comment"));
                uDTOL.add(uDTO2);
                round = 1;
            }
            if (round == 1) {
                repreportexcelDTOInter uDTO3 = DTOFactory.createrepreportexcelDTO();
                uDTO3.setColoumn1("Current ");
                uDTO3.setColoumn2("Replenishment");
                uDTOL.add(uDTO3);
                repreportexcelDTOInter uDTO = DTOFactory.createrepreportexcelDTO();
                uDTO.setColoumn1("Date From");
                uDTO.setColoumn2("Date To");
                uDTO.setColoumn3("Cassette Value");
                uDTO.setColoumn4("Switch Value");
                uDTO.setColoumn5("Switch Diff");
                uDTO.setColoumn6("Cassette Diff");
                uDTO.setColoumn7("Disputes");
                uDTO.setColoumn8("Comments");
                uDTOL.add(uDTO);
                repreportexcelDTOInter uDTO2 = DTOFactory.createrepreportexcelDTO();
                uDTO2.setColoumn1(rs.getString("date_from"));
                uDTO2.setColoumn2(rs.getString("date_to"));
                uDTO2.setColoumn3(rs.getString("cassitte_value"));
                uDTO2.setColoumn4(rs.getString("switch"));
                uDTO2.setColoumn5(rs.getString("switch_diff"));
                uDTO2.setColoumn6(rs.getString("cassitte_diff"));
                uDTO2.setColoumn7(rs.getString("disputes"));
                uDTO2.setColoumn8(rs.getString("comments"));
                uDTOL.add(uDTO2);
                round = 2;
            }
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public List<repreportexcelDTOInter> getrepreoprtcassetexcel(Object... obj) throws Throwable {
        String ID = (String) obj[0];
        String seq = "select  (select seq from cassette where id = d.cassete) seq, (select name from cassette where id = d.cassete) casste,d.cassete_value,d.remaining,d.id detail_id from replanishment_detail d where id = $repid order by 1";
        seq = seq.replace("$repid", "" + ID);
        ResultSet rs = executeQueryReport(seq);
        
        Integer round = 0;
        List<repreportexcelDTOInter> uDTOL = new ArrayList<repreportexcelDTOInter>();
        while (rs.next()) {

            if (round == 0) {
                repreportexcelDTOInter uDTO2 = DTOFactory.createrepreportexcelDTO();
                uDTOL.add(uDTO2);
                repreportexcelDTOInter uDTO = DTOFactory.createrepreportexcelDTO();
                uDTO.setColoumn1("Casstte");
                uDTO.setColoumn2("Cassette Value");
                uDTO.setColoumn3("Remaining");
                uDTOL.add(uDTO);
                repreportexcelDTOInter uDT3 = DTOFactory.createrepreportexcelDTO();
                uDT3.setColoumn1(rs.getString("casste"));
                uDT3.setColoumn2(rs.getString("cassete_value"));
                uDT3.setColoumn3(rs.getString("remaining"));
                uDTOL.add(uDT3);
                round = 1;
            } else {
                repreportexcelDTOInter uDTO = DTOFactory.createrepreportexcelDTO();
                uDTO.setColoumn1(rs.getString("casste"));
                uDTO.setColoumn2(rs.getString("cassete_value"));
                uDTO.setColoumn3(rs.getString("remaining"));
                uDTOL.add(uDTO);
            }
        }
        repreportexcelDTOInter uDTO = DTOFactory.createrepreportexcelDTO();
        uDTO.setColoumn1("-----------------");
        uDTO.setColoumn2("-----------------");
        uDTO.setColoumn3("-----------------");
        uDTO.setColoumn4("-----------------");
        uDTO.setColoumn5("-----------------");
        uDTO.setColoumn6("-----------------");
        uDTO.setColoumn7("-----------------");
        uDTO.setColoumn8("-----------------");
        uDTOL.add(uDTO);
        super.postSelect(rs);
        return uDTOL;
    }
}
