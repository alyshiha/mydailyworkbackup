/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.bo.BaseBO;

import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.AtmStatByTransRepDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.swglDTOInter;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author KhAiReE
 */
public class SwGlDifferenceRepBO extends BaseBO implements SwGlDifferenceRepBOInter,Serializable {

    protected SwGlDifferenceRepBO() {
        super();
    }

    public Object printExcel(String dateF, String dateT) throws Throwable {
        AtmStatByTransRepDAOInter engin = DAOFactory.createAtmStatByTransRepDAO(null);
        List<swglDTOInter> rs = (List<swglDTOInter>) engin.findswglexcelReport(dateF, dateT);
        return rs;
    }

    public Object print(String user, String customer, Date dateF, Date dateT) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();


        params.put("user", user);
        params.put("customer", customer);
        params.put("dateF", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("dateT", DateFormatter.changeDateAndTimeFormat(dateT));


        try {
   Connection conn =CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/SW-GL Deffrence.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);


            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);



            Calendar c = Calendar.getInstance();
            String uri = "SW-GL Deffrence" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";

            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
         

            try {

                String x = "../PDF/" + uri;

                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
         CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
           
            return "fail";
        }
    }
}
