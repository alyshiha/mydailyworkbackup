/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.AtmtemplateDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.AtmFileHeaderDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDTOInter;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDetailDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineTypeDTOInter;
import com.ev.AtmBingo.bus.dto.FileColumnDisplayDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmFileTemplateBO extends BaseBO implements AtmFileTemplateBOInter,Serializable {

    protected AtmFileTemplateBO() {
        super();
    }

    public Object SaveAllDetail(List<AtmFileTemplateDetailDTOInter> filetemplatedetailList, String TName, Integer templateid) throws Throwable {
        AtmtemplateDAOInter afhDAO = DAOFactory.createAtmtemplateDAO();
        return afhDAO.SaveAllDetail(filetemplatedetailList, TName, templateid);
    }
     public Object SaveAllHeader(List<AtmFileHeaderDTOInter> filetemplateHeaderList, String TName, Integer templateid) throws Throwable {
        AtmtemplateDAOInter afhDAO = DAOFactory.createAtmtemplateDAO();
        return afhDAO.SaveAllHeader(filetemplateHeaderList, TName, templateid);
    }

    public Object DeleteDetail(AtmFileTemplateDetailDTOInter filetemplatedetail, String TName) throws Throwable {
        AtmtemplateDAOInter afhDAO = DAOFactory.createAtmtemplateDAO();
        return afhDAO.DeleteDetail(filetemplatedetail, TName);
    }
    //DeleteDetail

    public Object InsertAtmHeader(AtmFileHeaderDTOInter afhDTO, String TName) throws Throwable {
        AtmtemplateDAOInter afhDAO = DAOFactory.createAtmtemplateDAO();
        return afhDAO.insertrecordHeader(afhDTO, TName);
    }

    public Object UpdateAtmHeader(AtmFileHeaderDTOInter afhDTO, String TName) throws Throwable {
        AtmtemplateDAOInter afhDAO = DAOFactory.createAtmtemplateDAO();
        return afhDAO.updaterecordHeader(afhDTO, TName);
    }

    public Object DeleteAtmHeader(AtmFileHeaderDTOInter afhDTO, String TName) throws Throwable {
        AtmtemplateDAOInter afhDAO = DAOFactory.createAtmtemplateDAO();
        return afhDAO.deleterecordHeader(afhDTO, TName);
    }

    public Object insertAtmTemplate(AtmFileTemplateDTOInter afhDTO, String TName) throws Throwable {
        AtmtemplateDAOInter afhDAO = DAOFactory.createAtmtemplateDAO();
        return afhDAO.insert(afhDTO, TName);
    }

    public Object UpdateAtmTemplate(AtmFileTemplateDTOInter afhDTO, String TName) throws Throwable {
        AtmtemplateDAOInter afhDAO = DAOFactory.createAtmtemplateDAO();
        return afhDAO.update(afhDTO, TName);
    }

    public Object DeleteAtmTemplate(AtmFileTemplateDTOInter afhDTO, String TName) throws Throwable {
        AtmtemplateDAOInter afhDAO = DAOFactory.createAtmtemplateDAO();
        return afhDAO.deleterecordTemplate(afhDTO, TName);
    }

    public Object getMachineTypes() throws Throwable {
        AtmtemplateDAOInter amtDAO = DAOFactory.createAtmtemplateDAO();
        List<AtmMachineTypeDTOInter> amtDTOL = (List<AtmMachineTypeDTOInter>) amtDAO.findAllMachineType();
        return amtDTOL;
    }

    public Object fetFileColumnDefinition() throws Throwable {
        AtmtemplateDAOInter fcdDAO = DAOFactory.createAtmtemplateDAO();
        List<FileColumnDisplayDTOInter> fcdDTOL = (List<FileColumnDisplayDTOInter>) fcdDAO.findAllColumns();
        return fcdDTOL;
    }

    public Object getAtmFileTemplates(String Table) throws Throwable {
        AtmtemplateDAOInter aftDAO = DAOFactory.createAtmtemplateDAO();
        List<AtmFileTemplateDTOInter> aftDTOL = (List<AtmFileTemplateDTOInter>) aftDAO.findAllTemplates(Table);
        return aftDTOL;
    }

    public Object DeleteAlldetailTemplate(AtmFileTemplateDTOInter afhDTO, String TName) throws Throwable {
        AtmtemplateDAOInter afhDAO = DAOFactory.createAtmtemplateDAO();
        return afhDAO.deleterecorddetailTemplate(afhDTO, TName);
    }

    public Object deleterecordAllHeader(AtmFileTemplateDTOInter afhDTO, String TName) throws Throwable {
        AtmtemplateDAOInter afhDAO = DAOFactory.createAtmtemplateDAO();
        return afhDAO.deleterecordHeaderByTemplateID(afhDTO, TName);
    }
}
