/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dao.CurrencyMasterDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.DisputeRulesDAOInter;
import com.ev.AtmBingo.bus.dao.FileColumnDefinitionDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionResponseCodeDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionResponseMasterDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionStatusDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionTypeDAOInter;
import com.ev.AtmBingo.bus.dao.TransactionTypeMasterDAOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.DisputeRulesDTOInter;
import com.ev.AtmBingo.bus.dto.FileColumnDefinitionDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseMasterDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeMasterDTOInter;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class DisputeRulesBO extends BaseBO implements DisputeRulesBOInter,Serializable {

    private final int STRING = 1;
    private final int NUMBER = 2;
    private final int DATE = 3;

    protected DisputeRulesBO() {
        super();
    }

    public String getColumnName(int columnId) throws Throwable {
        FileColumnDefinitionDAOInter fcdDAO = DAOFactory.createFileColumnDefinitionDAO(null);
        FileColumnDefinitionDTOInter fcdDTO = (FileColumnDefinitionDTOInter) fcdDAO.find(columnId);
        return fcdDTO.getName();
    }

    public Object getDisputeRules() throws Throwable {
        DisputeRulesDAOInter drDAO = DAOFactory.createDisputeRulesDAO(null);
        List<DisputeRulesDTOInter> drDTOL = (List<DisputeRulesDTOInter>) drDAO.findAll();
        return drDTOL;
    }

    public Object getColumns() throws Throwable {
        FileColumnDefinitionDAOInter fcdDAO = DAOFactory.createFileColumnDefinitionDAO(null);
        List<FileColumnDefinitionDTOInter> fcdDTOL = (List<FileColumnDefinitionDTOInter>) fcdDAO.findForRules();
        return fcdDTOL;
    }

    public Object getColumnValues(int columnId) throws Throwable {
        switch (columnId) {
            case 13:
                TransactionResponseCodeDAOInter trcDAO = DAOFactory.createTransactionResponseCodeDAO(null);
                List<TransactionResponseCodeDTOInter> trcDTOL = (List<TransactionResponseCodeDTOInter>) trcDAO.findAll();
                return trcDTOL;

            case 26:
                TransactionResponseMasterDAOInter trmDAO = DAOFactory.createTransactionResponseMasterDAO(null);
                List<TransactionResponseMasterDTOInter> trmDTOL = (List<TransactionResponseMasterDTOInter>) trmDAO.findAll();
                return trmDTOL;

            case 10:
                TransactionStatusDAOInter tsDAO = DAOFactory.createTransactionStatusDAO(null);
                List<TransactionStatusDTOInter> tsDTOL = (List<TransactionStatusDTOInter>) tsDAO.findAll();
                return tsDTOL;

            case 14:
                TransactionTypeDAOInter ttDAO = DAOFactory.createTransactionTypeDAO(null);
                List<TransactionTypeDTOInter> ttDTOL = (List<TransactionTypeDTOInter>) ttDAO.findAll();
                return ttDTOL;

            case 25:
                TransactionTypeMasterDAOInter ttmDAO = DAOFactory.createTransactionTypeMasterDAO(null);
                List<TransactionTypeMasterDTOInter> ttmDTOL = (List<TransactionTypeMasterDTOInter>) ttmDAO.findAll();
                return ttmDTOL;

            case 99:
                CurrencyMasterDAOInter cmDAO = DAOFactory.createCurrencyMasterDAO(null);
                List<CurrencyMasterDTOInter> cmDTOL = (List<CurrencyMasterDTOInter>) cmDAO.findAll();
                return cmDTOL;
            default:
                return null;
        }
    }

    private String getValues(int column, int valueId) throws Throwable {
        switch (column) {
            case 13:
                TransactionResponseCodeDAOInter trcDAO = DAOFactory.createTransactionResponseCodeDAO(null);
                TransactionResponseCodeDTOInter trcDTO = (TransactionResponseCodeDTOInter) trcDAO.find(valueId);
                return trcDTO.getName();

            case 26:
                TransactionResponseMasterDAOInter trmDAO = DAOFactory.createTransactionResponseMasterDAO(null);
                TransactionResponseMasterDTOInter trmDTOL = (TransactionResponseMasterDTOInter) trmDAO.find(valueId);
                return trmDTOL.getName();

            case 17:
                TransactionStatusDAOInter tsDAO = DAOFactory.createTransactionStatusDAO(null);
                TransactionStatusDTOInter tsDTOL = (TransactionStatusDTOInter) tsDAO.find(valueId);
                return tsDTOL.getName();

            case 14:
                TransactionTypeDAOInter ttDAO = DAOFactory.createTransactionTypeDAO(null);
                TransactionTypeDTOInter ttDTOL = (TransactionTypeDTOInter) ttDAO.find(valueId);
                return ttDTOL.getName();

            case 25:
                TransactionTypeMasterDAOInter ttmDAO = DAOFactory.createTransactionTypeMasterDAO(null);
                TransactionTypeMasterDTOInter ttmDTOL = (TransactionTypeMasterDTOInter) ttmDAO.find(valueId);
                return ttmDTOL.getName();

            case 10:
                CurrencyMasterDAOInter cmDAO = DAOFactory.createCurrencyMasterDAO(null);
                CurrencyMasterDTOInter cmDTO = (CurrencyMasterDTOInter) cmDAO.find(valueId);
                return cmDTO.getSymbol();
            default:
                return null;
        }
    }

    public Object createFormula(int column, String opt, String value, DisputeRulesDTOInter vrDTO, Integer Type) throws Throwable {

        DisputeRulesDAOInter vrDAO = DAOFactory.createDisputeRulesDAO(null);

        String formula = "";
        String formulaCode = "#Column Value#";

        String columnName = getColumnName(column);

        formula += columnName + " " + opt;
        formulaCode += " " + opt;

        if (value != null || !value.equals("")) {
            String[] separate = value.split(",");
            if (opt.equals("in") || opt.equals("not in")) {
                formula += "(";
                formulaCode += "(";
                for (int i = 0; i < separate.length; i++) {
                    String stringVlaue = getValues(column, Integer.valueOf(separate[i]));
                    if (stringVlaue == null) {
                        if (i == separate.length - 1) {
                            formula += separate[i] + ")";
                            formulaCode += "'" + separate[i] + "'" + ")";
                        } else {
                            formula += separate[i] + ",";
                            formulaCode += "'" + separate[i] + "'" + ",";
                        }
                    } else {
                        if (i == separate.length - 1) {
                            formula += stringVlaue + ")";
                            formulaCode += separate[i] + ")";
                        } else {
                            formula += stringVlaue + ",";
                            formulaCode += separate[i] + ",";
                        }
                    }
                }
            } else if (opt.equals("=") || opt.equals(">") || opt.equals("<") || opt.equals("<=") || opt.equals("<=")) {
                if (isValidDate(value.toString())) {
                    formulaCode += "to_date('" + value + "','dd-mm-yyyy hh24:mi:ss')";
                    formula += " " + value;
                } else if(isValidNumber(value.toString())) {
                    formulaCode += "" + value + "";
                    formula += " " + value;
                }
                else  {
                    formulaCode += "'" + value + "'";
                    formula += " " + value;
                }
                // formulaCode += " " + "'" + value + "'";
            } else if (opt.equals("between")) {
                String[] separated = value.split("and");
                formula += " " + separated[0] + " and " + separated[1];
                formulaCode += " to_date('" + separated[0] + "','dd-mm-yyyy hh24:mi:ss') and to_date('" + separated[1] + "','dd-mm-yyyy hh24:mi:ss')";
            }

        }
        boolean valid = vrDAO.checkFormula(formulaCode, Type);
        if (valid) {
            vrDTO.setFormula(formula);
            vrDTO.setFormulaCode(formulaCode);
            return "done";
        } else {
            return "Invalid Formula";
        }
    }

    public boolean isValidNumber(String Input) {
        try {
            int x = Integer.parseInt(Input);
            return true;
        } catch (NumberFormatException nFE) {
            return false;
        }
    }

    public boolean isValidDate(String inDate) {

        if (inDate == null) {
            return false;
        }

        //set the format to use as a constructor argument
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        if (inDate.trim().length() != dateFormat.toPattern().length()) {
            return false;
        }

        dateFormat.setLenient(false);

        try {
            //parse the inDate parameter
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }

    public String genarateValueString(Integer[] values) {
        String result = "";
        for (Object o : values) {
            result += String.valueOf(o) + ",";
        }
        return result;
    }

    public String genarateValueDate(Date d1, Date d2) {
        String result = DateFormatter.changeDateAndTimeFormat(d1) + " and " + DateFormatter.changeDateAndTimeFormat(d2);
        return result;
    }

    public Object validateFormula(DisputeRulesDTOInter drDTO, Integer Type) throws Throwable {
        DisputeRulesDAOInter vrDAO = DAOFactory.createDisputeRulesDAO(null);
        boolean valid = vrDAO.checkFormula(drDTO.getFormula().toLowerCase(), Type);
        return valid;
        /*  String msg = "";
        String formula = drDTO.getFormula().toLowerCase();
        String formulaCode = "#Column Value#";
        DisputeRulesDAOInter drDAO = DAOFactory.createDisputeRulesDAO(null);
        List<FileColumnDefinitionDTOInter> fcdDTOL = (List<FileColumnDefinitionDTOInter>) getColumns();
        Boolean pass = false;
        FileColumnDefinitionDTOInter theColumn = null;
        for (FileColumnDefinitionDTOInter fcdDTO : fcdDTOL) {
        if (formula.contains(fcdDTO.getName().toLowerCase())) {
        pass = true;
        theColumn = fcdDTO;
        formula = formula.replace(fcdDTO.getName().toLowerCase(), fcdDTO.getColumnName());
        formulaCode += formula.replace(fcdDTO.getColumnName(), "");
        break;
        }
        }
        String[] fas = formula.split(" ");
        if (pass) {
        drDTO.setColumnId(theColumn.getId());
        if (fas[1].equals("is")) {
        if (fas[2].equals("null") || (fas[2].equals("not") && fas[3].equals("null"))) {
        drDTO.setFormulaCode(formulaCode);
        Boolean n = drDAO.checkFormula(formulaCode);
        if (n) {
        msg = "done";
        } else {
        msg = "Invalid Formula";
        }
        } else {
        msg = "invalid formula ,must end with (null) or (not null)";
        }
        } else if (theColumn.getDataType() == STRING) {
        if (fas[1].equals("=") || fas[1].equals("<>") || fas[1].equals("like")) {
        if (fas[2].matches("('[^']*')")) {
        drDTO.setFormulaCode(formulaCode);
        Boolean n = drDAO.checkFormula(formulaCode);
        if (n) {
        msg = "done";
        } else {
        msg = "Invalid Formula";
        }
        } else {
        msg = "String value must be like 'VALUE'";
        }
        } else if (fas[1].equals("not") && fas[2].equals("like")) {
        if (fas[3].matches("('[^']*')")) {
        drDTO.setFormulaCode(formulaCode);
        Boolean n = drDAO.checkFormula(formulaCode);
        if (n) {
        msg = "done";
        } else {
        msg = "Invalid Formula";
        }
        } else {
        msg = "String value must be like 'VALUE'";
        }

        } else if (fas[1].equals("in")) {
        if (fas[2].equals("(")) {
        if (fas[3].matches("(($'[^']*'),)*('[^']*')$")) {
        if (fas[4].equals(")")) {
        drDTO.setFormulaCode(formulaCode);
        Boolean n = drDAO.checkFormula(formulaCode);
        if (n) {
        msg = "done";
        } else {
        msg = "Invalid Formula";
        }
        } else {
        msg = "invalid end you must end with )";
        }
        } else {
        msg = "invalid values, values must be like 'value1','value2','value3'";
        }

        } else {
        msg = "invalid start you must start with (";
        }
        } else if (fas[1].equals("not") && fas[2].equals("in")) {
        if (fas[3].equals("(")) {
        if (fas[4].matches("(($'[^']*'),)*('[^']*')$")) {
        if (fas[5].equals(")")) {
        drDTO.setFormulaCode(formulaCode);
        Boolean n = drDAO.checkFormula(formulaCode);
        if (n) {
        msg = "done";
        } else {
        msg = "Invalid Formula";
        }
        } else {
        msg = "invalid end you must end with )";
        }
        } else {
        msg = "invalid values, values must be like 'value1','value2','value3'";
        }

        } else {
        msg = "invalid start you must start with (";
        }
        } else {
        msg = "invalid oepration in string value";
        }
        } else if (theColumn.getDataType() == NUMBER) {
        if (fas[1].equals("=") || fas[1].equals("<>") || fas[1].equals("like") || fas[1].equals("<")
        || fas[1].equals(">") || fas[1].equals(">=") || fas[1].equals("<=")) {
        if (fas[2].matches("^[0-9]+$")) {
        drDTO.setFormulaCode(formulaCode);
        Boolean n = drDAO.checkFormula(formulaCode);
        if (n) {
        msg = "done";
        } else {
        msg = "Invalid Formula";
        }
        }
        } else if (fas[1].equals("Between")) {
        if (fas[2].matches("^[0-9]+$")) {
        if (fas[3].equals("and")) {
        if (fas[4].matches("^[0-9]+$")) {
        } else {
        msg = "invalid second value";
        }
        } else {
        msg = "Between operation must follow by value1 then (and) then value2";
        }
        } else {
        msg = "invalid first value";
        }
        } else if (fas[1].equals("in")) {
        if (fas[2].equals("(")) {
        if (fas[3].matches("^([0-9]+,)*[0-9]$")) {
        if (fas[4].equals(")")) {
        msg = "done";
        drDTO.setFormulaCode(formulaCode);
        } else {
        msg = "invalid end you must end with )";
        }
        } else {
        msg = "invalid values, values must be like value1,value2,value3";
        }

        } else {
        msg = "invalid start you must start with (";
        }

        } else if (fas[1].equals("not") && fas[2].equals("in")) {
        if (fas[3].equals("(")) {
        if (fas[4].matches("^([0-9]+,)*[0-9]$")) {
        if (fas[5].equals(")")) {
        drDTO.setFormulaCode(formulaCode);
        Boolean n = drDAO.checkFormula(formulaCode);
        if (n) {
        msg = "done";
        } else {
        msg = "Invalid Formula";
        }
        } else {
        msg = "invalid end you must end with )";
        }
        } else {
        msg = "invalid values, values must be like value1,value2,value3";
        }

        } else {
        msg = "invalid start you must start with (";
        }

        } else {
        msg = "invalid operation in number value";
        }
        } else if (theColumn.getDataType() == DATE) {
        if (fas[1].equals("=") || fas[1].equals("<>") || fas[1].equals("<")
        || fas[1].equals(">") || fas[1].equals(">=") || fas[1].equals("<=")) {
        if (fas[2].matches("('[^']([0]?[1-9]|[1|2][0-9]|[3][0|1])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4}|[0-9]{2})-(([0-1][0-9])|([2][0-3])):([0-5][0-9]):([0-5][0-9])')")) {
        drDTO.setFormulaCode(formulaCode);
        Boolean n = drDAO.checkFormula(formulaCode);
        if (n) {
        msg = "done";
        } else {
        msg = "Invalid Formula";
        }
        } else {
        msg = "invalid date value or date format('dd.mm.yyyy-hh:MM:ss')";
        }

        } else if (fas[1].equals("Between")) {
        if (fas[2].matches("('[^']([0]?[1-9]|[1|2][0-9]|[3][0|1])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4}|[0-9]{2})-(([0-1][0-9])|([2][0-3])):([0-5][0-9]):([0-5][0-9])')")) {
        if (fas[3].equals("and")) {
        if (fas[4].matches("('[^']([0]?[1-9]|[1|2][0-9]|[3][0|1])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4}|[0-9]{2})-(([0-1][0-9])|([2][0-3])):([0-5][0-9]):([0-5][0-9])')")) {
        drDTO.setFormulaCode(formulaCode);
        Boolean n = drDAO.checkFormula(formulaCode);
        if (n) {
        msg = "done";
        } else {
        msg = "Invalid Formula";
        }
        } else {
        msg = "invalid date value or date format('dd.mm.yyyy-hh:MM:ss')";
        }
        } else {
        msg = "Between operation must follow by 'value1' then (and) then 'value2'";
        }
        } else {
        msg = "invalid date value or date format(dd.mm.yyyy-hh:MM:ss)";
        }
        } else {
        msg = "invalid operation in date value";
        }
        }


        } else {
        msg = "column not found";
        }

        return msg;
        }*/
    }

    public Object editDisputeRules(Object obj, int operation) throws Throwable {
        DisputeRulesDAOInter drDAO = DAOFactory.createDisputeRulesDAO(null);
        DisputeRulesDTOInter drDTO = (DisputeRulesDTOInter) obj;

        switch (operation) {
            case INSERT:
                drDAO.insert(drDTO);
                return "Insert Done";
            case UPDATE:
                drDAO.update(drDTO);
                return "Update Done";
            case DELETE:
                drDAO.delete(drDTO);
                return "Delete Done";
            default:
                return "Their Is No Operation";
        }
    }

    public static void main(String[] args) throws Throwable {
        DisputeRulesBOInter drBO = BOFactory.createDisputeRulesBO(null);
        DisputeRulesDTOInter d = DTOFactory.createDisputeRulesDTO();
        // d.setFormula("Transaction Date = '12.12.2001-00:00:00' ");
        // String msg = (String) drBO.validateFormula(d);
        
    }
}
