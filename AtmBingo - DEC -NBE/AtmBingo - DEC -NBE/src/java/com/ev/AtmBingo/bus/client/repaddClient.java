/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.RepAddBOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.RepAddDTOInter;
import com.ev.AtmBingo.bus.dto.RepDetailTempDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Aly
 */
public class repaddClient extends BaseBean implements Serializable {

    private TimeZone timeZone;
    private RepAddDTOInter selectedRecord, newRecord;
    private List<RepAddDTOInter> recordsList;
    private List<RepDetailTempDTOInter> cassettelist;
    private int listcount;
    private RepAddBOInter engin;
    private List<String> privelageList;
    private List<AtmMachineDTOInter> atmList;
    private UsersDTOInter userHolded;
    Session sessionUtil = new Session();

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public RepAddDTOInter getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(RepAddDTOInter selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public RepAddDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(RepAddDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public List<RepAddDTOInter> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<RepAddDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public RepAddBOInter getEngin() {
        return engin;
    }

    public void setEngin(RepAddBOInter engin) {
        this.engin = engin;
    }

    public List<String> getPrivelageList() {
        return privelageList;
    }

    public void setPrivelageList(List<String> privelageList) {
        this.privelageList = privelageList;
    }

    public List<RepDetailTempDTOInter> getCassettelist() {
        return cassettelist;
    }

    public void setCassettelist(List<RepDetailTempDTOInter> cassettelist) {
        this.cassettelist = cassettelist;
    }

    public repaddClient() throws Throwable {
        engin = BOFactory.createRepAddBO(null);
        timeZone = TimeZone.getDefault();
        recordsList = engin.GetAllRecords();
        listcount = recordsList.size();
        userHolded = sessionUtil.GetUserLogging();
        atmList = (List<AtmMachineDTOInter>) sessionUtil.GetAtmMachineList(userHolded);
    }

    public void addRecord() {
        newRecord = DTOFactory.createRepAddDTO();
    }

    public void saveRecord() throws SQLException, Throwable {
        String message = "Fail";
        int atmid = getATMID(newRecord.getAtmapplicationid());
        if (atmid != 0) {
            List<ReplanishmentMasterDTOInter> mainrecord = (List<ReplanishmentMasterDTOInter>) engin.GetRecord(newRecord.getDateTo(), atmid);
            if (mainrecord.size() > 1) {
                message = "To Many Replainshments";
            } else if (mainrecord.isEmpty()) {
                message = "No Replainshments Found";
            } else {
                if (engin.findInTemp(mainrecord.get(0).getId()) == 1) {
                    message = "Replenishments Already Exist";
                } else {
                    newRecord.setCreatedbyid(userHolded.getUserId());
                    newRecord.setCreatedby(userHolded.getUserName());
                    newRecord.setCreateddate(new Date());
                    newRecord.setAtmId(atmid);
                    newRecord.setDateFrom(mainrecord.get(0).getDateFrom());
                    newRecord.setDateTo(mainrecord.get(0).getDateTo());
                    newRecord.setRepid(mainrecord.get(0).getId());
                    newRecord.setId(engin.insertrecord(newRecord));
                    message = "Record Has Been Inserted";
                    RequestContext context = RequestContext.getCurrentInstance();
                    context.execute("carDialog2.hide();");
                    recordsList.add(newRecord);
                }
            }
        } else {
            message = "Please Enter A Valid ATMID";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, ""));
    }

    public void updateRecord() throws SQLException, Throwable {
        String message = engin.savecassette(cassettelist);
        selectedRecord = null;
        cassettelist = null;
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("carDialog.hide();");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, ""));
    }

    public void deleteRecord() throws SQLException, Throwable {
        String message = engin.deleterecord(selectedRecord);
        if (message.contains("Deleted")) {
            recordsList.remove(selectedRecord);
            listcount = recordsList.size();
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, ""));
    }

    public List<String> complete(String query) {
        List<String> results = new ArrayList<String>();
        for (int i = 0; i < atmList.size(); i++) {
            if (atmList.get(i).getApplicationId().toString().startsWith(query)) {
                results.add(atmList.get(i).getApplicationId().toString());
            }
        }
        return results;
    }

    public int getATMID(String appID) {
        for (int i = 0; i < atmList.size(); i++) {
            if (atmList.get(i).getApplicationId().toString().startsWith(appID)) {
                return atmList.get(i).getId();
            }
        }
        return 0;
    }

    public void onRowSelect(SelectEvent e) throws Throwable {
        selectedRecord = (RepAddDTOInter) e.getObject();
        cassettelist = engin.GetCassette(selectedRecord.getRepid());
    }
    
      public void excute() throws SQLException, Throwable {
        String message = engin.Excute(selectedRecord);
        recordsList = engin.GetAllRecords();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, ""));
    }
}
