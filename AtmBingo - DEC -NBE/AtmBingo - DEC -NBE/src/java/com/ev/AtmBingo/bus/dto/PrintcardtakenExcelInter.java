/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface PrintcardtakenExcelInter  extends Serializable {
void setDateto(String dateto);
String getDateto();
void setDatefrom(String datefrom);
String getDatefrom() ;
    String getAtmapplicationid();

    Integer getCardCount();

    void setAtmapplicationid(String atmapplicationid);

    void setCardCount(Integer cardCount);
    
}
