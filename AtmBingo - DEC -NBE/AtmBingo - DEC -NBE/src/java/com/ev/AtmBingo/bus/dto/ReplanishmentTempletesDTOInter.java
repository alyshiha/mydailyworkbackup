/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface ReplanishmentTempletesDTOInter  extends Serializable {
    void setCardtakenlength(Integer cardtakenlength);

    Integer getCardtakenlength();

    Integer getCardtakenline();

    void setCardtakenline(Integer cardtakenline);

    Integer getCardtakenposition();

    void setCardtakenstartingstring(String cardtakenstartingstring);

    String getCardtakenstartingstring();

    void setCardtakenposition(Integer cardtakenposition);

    String getAddingDateFormat();

    void setAddingDateFormat(String addingDateFormat);

    Integer getAddingDateLenght();

    Integer getAddingDateLine();

    Integer getAddingDatePosition();

    String getAddingDateStartingString();

    Integer getAtmIdLenght();

    Integer getAtmIdPosition();

    String getAtmIdString();

    Integer getAtmMachineType();

    Integer getAtmStringPosition();

    Integer getEndingPosition();

    String getEndingString();

    int getId();

    String getName();

    String getRemainingDateFormat();

    Integer getRemainingDateLenght();

    Integer getRemainingDateLine();

    Integer getRemainingDatePosition();

    String getRemainingDateStartingString();

    Integer getStartingPosition();

    String getStartingString();

    void setAddingDateLenght(Integer addingDateLenght);

    void setAddingDateLine(Integer addingDateLine);

    void setAddingDatePosition(Integer addingDatePosition);

    void setAddingDateStartingString(String addingDateStartingString);

    void setAtmIdLenght(Integer atmIdLenght);

    void setAtmIdPosition(Integer atmIdPosition);

    void setAtmIdString(String atmIdString);

    void setAtmMachineType(Integer atmMachineType);

    void setAtmStringPosition(Integer atmStringPosition);

    void setEndingPosition(Integer endingPosition);

    void setEndingString(String endingString);

    void setId(int id);

    void setName(String name);

    void setRemainingDateFormat(String remainingDateFormat);

    void setRemainingDateLenght(Integer remainingDateLenght);

    void setRemainingDateLine(Integer remainingDateLine);

    void setRemainingDatePosition(Integer remainingDatePosition);

    void setRemainingDateStartingString(String remainingDateStartingString);

    void setStartingPosition(Integer startingPosition);

    void setStartingString(String startingString);
}
