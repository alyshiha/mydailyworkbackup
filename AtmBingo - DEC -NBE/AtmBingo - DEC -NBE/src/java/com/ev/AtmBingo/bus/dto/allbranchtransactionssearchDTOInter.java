/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Aly
 */
public interface allbranchtransactionssearchDTOInter   extends Serializable {
void setid(int id);
int getid();
void setuserid(int userid);
int getuserid();
 void setdefaulttransbol(Boolean defaulttransbol) ;
 Boolean getdefaulttransbol();
 void setdefaulttrans(int defaulttrans);
 int getdefaulttrans();
 void settempname(String tempname);
 String gettempname();
    int getatmid();

    int getbranchid();

    String getcardno();

    String getcardop();

    String getcustaccnumop();

    String getcustomeraccountnumber();

    int getreasonid();

    int getresponsecodeid();

    int getresponsecodeidop();

    Date gettransactiondatefrom();

    String gettransactiondateoper();

    Date gettransactiondateto();

    int gettransactiontypeid();

    void setatmid(int atmid);

    void setbranchid(int branchid);

    void setcardno(String cardno);

    void setcardop(String cardop);

    void setcustaccnumop(String custaccnumop);

    void setcustomeraccountnumber(String customeraccountnumber);

    void setreasonid(int reasonid);

    void setresponsecodeid(int responsecodeid);

    void setresponsecodeidop(int responsecodeidop);

    void settransactiondatefrom(Date transactiondatefrom);

    void settransactiondateoper(String transactiondateoper);

    void settransactiondateto(Date transactiondateto);

    void settransactiontypeid(int transactiontypeid);
    
}
