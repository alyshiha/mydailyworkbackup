/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.HelpBOInter;
import com.ev.AtmBingo.bus.dto.LicenseDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.Enumeration;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author ISLAM
 */
public class HelpDeskClient extends BaseBean  implements Serializable{
    private HelpBOInter helpBo;
    private LicenseDTOInter helpDto;
    private String licTo, version;
    private Integer noOfUsers, noOfAtms;
    private Date expiredDate;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getLicTo() {
        return licTo;
    }

    public void setLicTo(String licTo) {
        this.licTo = licTo;
    }

    public Integer getNoOfAtms() {
        return noOfAtms;
    }

    public void setNoOfAtms(Integer noOfAtms) {
        this.noOfAtms = noOfAtms;
    }

    public Integer getNoOfUsers() {
        return noOfUsers;
    }

    public void setNoOfUsers(Integer noOfUsers) {
        this.noOfUsers = noOfUsers;
    }

    

    public LicenseDTOInter getHelpDto()  {
        return helpDto;
    }

    public void setHelpDto(LicenseDTOInter helpDto) {
        this.helpDto = helpDto;
    }

    


    /** Creates a new instance of HelpDeskClient */
    public HelpDeskClient() throws Throwable {
        super();
        super.GetAccess();
        helpBo = BOFactory.createHelpBOInter(null);
        helpDto =(LicenseDTOInter) helpBo.getLicense();
        licTo = helpDto.getLicenseTo();
        expiredDate =  helpDto.getEndDate();
        noOfAtms = helpDto.getAtmNO();
        noOfUsers = helpDto.getNoOfUser();
        version = helpDto.getVersion();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
