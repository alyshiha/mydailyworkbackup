    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ExportLogDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ExportLogDAO extends BaseDAO implements ExportLogDAOInter {

    protected ExportLogDAO() {
        super();
        super.setTableName("Export_log");

    }

    public Object findAll(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select " + super.getTableName() + ".*," + super.getTableName() + ".card_no card_no_Decrypt from " + super.getTableName() + " order by corrective_date desc ";
        ResultSet result = executeQuery(selectStat);
        List<ExportLogDTOInter> slL = new ArrayList<ExportLogDTOInter>();
        while (result.next()) {
            ExportLogDTOInter sl = DTOFactory.createExportLogDTO();
            sl.setAmount(result.getInt("amount"));
            sl.setAtmApplicationId(result.getString("atm_application_id"));
            sl.setCardNo(result.getString("card_no_Decrypt"));
            sl.setCurrency(result.getString("currency"));
            sl.setMatchingType(result.getInt("matching_type"));
            sl.setRecordType(result.getInt("record_type"));
            sl.setResponseCode(result.getString("response_code"));
            sl.setExportUser(result.getInt("Export_user"));
            sl.setTransactionDate(result.getDate("transaction_date"));
            sl.setExport_date(result.getDate("Export_date"));
            sl.setTransactionSequence(result.getString("transaction_sequence"));
            sl.setExportState(result.getInt("Export_STATE"));
            slL.add(sl);
        }
        super.postSelect(result);
        return slL;
    }

    public Object CustomSearch(Object... obj) throws Throwable {
        super.preSelect();
        String whereCluase = (String) obj[0];
        String selectStat = "select  " + super.getTableName() + ".*," + super.getTableName() + ".card_no card_no_Decrypt from " + super.getTableName() + " where 1=1 $whereCluase order by export_date desc";
        selectStat = selectStat.replace("$whereCluase", "" + whereCluase);
        List<ExportLogDTOInter> slL = new ArrayList<ExportLogDTOInter>();
        ResultSet result = executeQuery(selectStat);
        while (result.next()) {
            ExportLogDTOInter sl = DTOFactory.createExportLogDTO();
            sl.setAmount(result.getInt("amount"));
            sl.setAtmApplicationId(result.getString("atm_application_id"));
            sl.setCardNo(result.getString("card_no_Decrypt"));
            sl.setCurrency(result.getString("currency"));
            sl.setMatchingType(result.getInt("matching_type"));
            sl.setRecordType(result.getInt("record_type"));
            sl.setResponseCode(result.getString("response_code"));
            sl.setExportUser(result.getInt("Export_user"));
            sl.setTransactionDate(result.getDate("transaction_date"));
            sl.setExport_date(result.getDate("Export_date"));
            sl.setTransactionSequence(result.getString("transaction_sequence"));
//            sl.setExportState(result.getInt("Export_STATE"));
            slL.add(sl);
        }
        super.postSelect(result);
        return slL;
    }
}

