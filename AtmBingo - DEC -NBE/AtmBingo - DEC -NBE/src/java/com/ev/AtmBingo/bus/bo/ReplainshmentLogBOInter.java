/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.ReplainshmentLogDTOInter;
import java.util.List;

/**
 *
 * @author Aly-Shiha
 */
public interface ReplainshmentLogBOInter {

    String runReport(String dailysheetFrom, String dailysheetTo, String dateFrom, String dateTo, int user, String cust,String username) throws Throwable;

    List<ReplainshmentLogDTOInter> runReportrxcel(String dailysheetFrom, String dailysheetTo, String dateFrom, String dateTo, int user) throws Throwable;

}
