/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface LangDTOInter extends Serializable {

    String getLangAName();

    int getLangId();

    String getLangName();

    void setLangAName(String langAName);

    void setLangId(int langId);

    void setLangName(String langName);

}
