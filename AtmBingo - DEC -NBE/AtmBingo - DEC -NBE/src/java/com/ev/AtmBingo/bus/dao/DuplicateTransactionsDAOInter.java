/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface DuplicateTransactionsDAOInter {

    Object delete(Object... obj) throws Throwable;

    Object find(Date transactionDate, String atmApplicationId, String cardNo, int amount, String currency, String responseCode, int matchingType, int recordType) throws Throwable;

    Object findAll(AtmFileDTOInter fileId) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
