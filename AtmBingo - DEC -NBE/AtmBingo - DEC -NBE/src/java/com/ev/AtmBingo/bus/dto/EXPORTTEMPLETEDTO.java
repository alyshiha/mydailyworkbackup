/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;
import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
/**
 *
 * @author Administrator
 */
public class EXPORTTEMPLETEDTO  extends BaseDTO implements EXPORTTEMPLETEDTOInter , Serializable {
private int templateid;
private int active;
private boolean  activebool;
private String templatename;
private String separator;

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
   this.active = active;
    if(active==1){
        this.activebool = true;}
         if(active==2){
        this.activebool = false;}
    }
  public boolean  getBoolActive() {
        return activebool;
    }

    public void setBoolActive(boolean active) {
              if(active==true){
        setActive(1);}
         if(active==false){
        setActive(2);}
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public int getTemplateid() {
        return templateid;
    }

    public void setTemplateid(int templateid) {
        this.templateid = templateid;
    }

    public String getTemplatename() {
        return templatename;
    }

    public void setTemplatename(String templatename) {
        this.templatename = templatename;
    }

}
