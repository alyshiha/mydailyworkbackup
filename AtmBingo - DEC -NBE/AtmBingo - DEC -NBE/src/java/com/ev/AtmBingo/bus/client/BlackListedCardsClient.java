/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.BlackListedCardBOInter;
import com.ev.AtmBingo.bus.dto.BlackListedCardDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.faces.event.ActionEvent;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class BlackListedCardsClient extends BaseBean  implements Serializable{

    private BlackListedCardBOInter blObject;
    private List<BlackListedCardDTOInter> blList;
    private BlackListedCardDTOInter blUpdateAttr, blDeleteAttr;
    private BlackListedCardDTOInter newBL;
    private Boolean showAdd, showConfirm;
    private String message;
    private String oldCard;
    private Integer listSize;

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public String getOldCard() {
        return oldCard;
    }

    public void setOldCard(String oldCard) {
        this.oldCard = oldCard;
    }

    public BlackListedCardDTOInter getBlDeleteAttr() {
        return blDeleteAttr;
    }

    public void setBlDeleteAttr(BlackListedCardDTOInter blDeleteAttr) {
        this.blDeleteAttr = blDeleteAttr;
    }

    public List<BlackListedCardDTOInter> getBlList() {
        return blList;
    }

    public void setBlList(List<BlackListedCardDTOInter> blList) {
        this.blList = blList;
    }

    public BlackListedCardDTOInter getBlUpdateAttr() {
        return blUpdateAttr;
    }

    public void setBlUpdateAttr(BlackListedCardDTOInter blUpdateAttr) {
        this.blUpdateAttr = blUpdateAttr;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BlackListedCardDTOInter getNewBL() {
        return newBL;
    }

    public void setNewBL(BlackListedCardDTOInter newBL) {
        this.newBL = newBL;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    /** Creates a new instance of BlackListedCardsClient */
    public BlackListedCardsClient() throws Throwable {
        super();
        GetAccess();
        blObject = BOFactory.createBlackListedBO(null);
        blList = (List<BlackListedCardDTOInter>) blObject.getBlackListed();
        showAdd = true;
        showConfirm = false;
        listSize = blList.size();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void fillDelete(ActionEvent e) {
        blDeleteAttr = (BlackListedCardDTOInter) e.getComponent().getAttributes().get("removedRow");
        resetVars();
    }

    public void deleteRecord() {
        try {
            if (blDeleteAttr.getCardNo() == null || (blDeleteAttr.getCardNo() == null ? "" == null : blDeleteAttr.getCardNo().equals(""))) {
                blList.remove(blDeleteAttr);
                showAdd = true;
                showConfirm = false;
               message = super.showMessage(SUCC);
            } else {
                blObject.editeBlackListed(blDeleteAttr, null, blObject.DELETE);
                blList.remove(blDeleteAttr);
                message = super.showMessage(SUCC);
                showAdd = true;
                showConfirm = false;
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void fillUpdate(ActionEvent e) {
        blUpdateAttr = (BlackListedCardDTOInter) e.getComponent().getAttributes().get("updateRow");
        message = "";
    }

    public void updateRecord() {
        try {
            if (showAdd == false) {
                message = super.showMessage(CONF_FIRST);
            } else if (blUpdateAttr.getCardNo() == null || blUpdateAttr.getBlackListedDate() == null) {
                message = super.showMessage(REQ_FIELD);
            } else {
                if (oldCard == null) {
                    oldCard = blUpdateAttr.getCardNo();
                }
                blObject.editeBlackListed(blUpdateAttr, oldCard, blObject.UPDATE);
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void addRecord() {
        if (blList == null) {
            blList = new ArrayList<BlackListedCardDTOInter>();
        }
        newBL = DTOFactory.createBlackListedCardDTO();
        blList.add(newBL);
        showAdd = false;
        showConfirm = true;
        resetVars();
    }

    public void confirmRecord() {
        try {
            if (newBL.getCardNo() == null || newBL.getBlackListedDate() == null) {
                message = super.showMessage(REQ_FIELD);
            } else {
                blObject.editeBlackListed(newBL, null, blObject.INSERT);
                showAdd = true;
                showConfirm = false;
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = super.showMessage(REC_EXIST);
        }
    }

    public void oldValue(ValueChangeEvent e) {
        oldCard = (String) e.getOldValue();
        resetVars();
    }
    public void resetVars() {
        message = "";
    }
      public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }
}
