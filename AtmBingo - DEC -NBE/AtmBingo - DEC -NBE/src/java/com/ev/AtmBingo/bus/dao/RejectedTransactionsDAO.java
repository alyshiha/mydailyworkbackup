/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.CurrencyMasterDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.RejectedTransactionsDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionResponseCodeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class RejectedTransactionsDAO extends BaseDAO implements RejectedTransactionsDAOInter {

    protected RejectedTransactionsDAO() {
        super();
        super.setTableName("rejected_transactions");
    }

    private String clobToString(Clob data) {
        StringBuilder sb = new StringBuilder();
        try {
            Reader reader;
            try {
                reader = data.getCharacterStream();
                BufferedReader br = new BufferedReader(reader);
                String line;
                while (null != (line = br.readLine())) {

                }
                br.close();
            } catch (SQLException ex) {
                Logger.getLogger(DisputesDAO.class.getName()).log(Level.SEVERE, null, ex);
                return "Fail";
            }
        } catch (IOException ex) {
            Logger.getLogger(DisputesDAO.class.getName()).log(Level.SEVERE, null, ex);
            return "Fail";
        }

        return sb.toString();
    }

    public String Findtexttransaction(Object obj) throws Throwable {
        super.preSelect();
        RejectedTransactionsDTOInter mdDTO = (RejectedTransactionsDTOInter) obj;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "";
        if (mdDTO.getrecordtype() == 1) {
            selectStat = "select transaction_data transaction_data  from REJECTED_JOURNAL t where t.rejected_journal_key= '$disputekeyuk'";
        }
        if (mdDTO.getrecordtype() == 2) {
            selectStat = "select transaction_data transaction_data  from REJECTED_switch t  where t.rejected_switch_key= '$disputekeyuk'";
        }
        if (mdDTO.getrecordtype() == 3) {
            selectStat = "select transaction_data transaction_data  from REJECTED_host t  where t.rejected_host_key = '$disputekeyuk'";
        }

        String elmsg = null;
        selectStat = selectStat.replace("$disputekeyuk", "" + mdDTO.getRejkey());
//        selectStat = selectStat.replace("$disputekeyuk", "" + mdDTO.getDisputekeyuk());

        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            // File data = new File("C:\\a.txt");
            elmsg = rs.getString("transaction_data");
            //clobToString(elmsg);
            /*      Reader reader = rs.getCharacterStream("transaction_data");
             FileWriter writer = new FileWriter(data);
             char[] buffer = new char[1];
             while (reader.read(buffer) > 0) {
             writer.write(buffer);
             }
             writer.close();*/
        }
        super.postSelect(rs);
        return elmsg;
    }

    public Clob FindFiletransaction(Object obj) throws Throwable {
        super.preSelect();
        RejectedTransactionsDTOInter mdDTO = (RejectedTransactionsDTOInter) obj;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "";
        if (mdDTO.getrecordtype() == 1) {
            selectStat = "select decrypt_blob(t.id) filetext from ATM_FILE t where exists (select 1 from rejected_journal t where t.rejected_journal_key = '$disputekeyuk')";
        }
        if (mdDTO.getrecordtype() == 2) {
            selectStat = "select decrypt_blob(t.id) filetext from ATM_FILE t where exists (select 1 from rejected_switch t where t.rejected_switch_key = '$disputekeyuk')";
        }
        if (mdDTO.getrecordtype() == 3) {
            selectStat = "select decrypt_blob(t.id) filetext from ATM_FILE t where exists (select 1 from rejected_host t where t.rejected_host_key = '$disputekeyuk')";
        }
        Clob elmsg = null;
        selectStat = selectStat.replace("$disputekeyuk", "" + mdDTO.getRejkey());
        // selectStat = selectStat.replace("$disputekeyuk", "" + mdDTO.getr);

        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            elmsg = rs.getClob("filetext");
        }
        super.postSelect(rs);
        return elmsg;
    }

    public Object customSearch(String whereClause, String field, String temp) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select get_rejected_reseaon(rejected_transactions.record_type,rejected_transactions.rejected_key)rejectedreseaon,rownum elrow,(select l.name from atm_machine l where l.id = $table.atm_id) atmname,$table.*,$table.card_no card_no_decrypt,is_black_list($table.card_no) blacklist from $table   where $temp $whereClause";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$whereClause", "" + whereClause);
        selectStat = selectStat.replace("$temp", "" + temp);
        //selectStat = selectStat.replace("$field", "" + field);
        ResultSet rs = executeQuery(selectStat);

        List<RejectedTransactionsDTOInter> uDTOL = new ArrayList<RejectedTransactionsDTOInter>();

        while (rs.next()) {
            RejectedTransactionsDTOInter uDTO = DTOFactory.createRejectedTransactionsDTO();
            uDTO.setamount(rs.getInt("amount"));
            uDTO.setRejkey(rs.getString("rejected_key"));
            uDTO.setrejectedreason(rs.getString("rejectedreseaon"));
            uDTO.setatmapplicationid(rs.getString("atm_application_id"));
            uDTO.setrownum(rs.getString("elrow"));

            uDTO.setBlacklist(rs.getInt("blacklist"));
            AtmMachineDTOInter amDTO = DTOFactory.createAtmMachineDTO();
            amDTO.setId(rs.getInt("atm_id"));
            uDTO.setatmid(amDTO);
            uDTO.setatmname(rs.getString("atmname"));
            uDTO.setcardno(rs.getString("card_no_decrypt"));
            uDTO.setcolumn1(rs.getString("column1"));
            uDTO.setcolumn2(rs.getString("column2"));
            uDTO.setcolumn3(rs.getString("column3"));
            uDTO.setcolumn4(rs.getString("column4"));
            uDTO.setcolumn5(rs.getString("column5"));
            uDTO.setcurrency(rs.getString("currency"));

            CurrencyMasterDTOInter cmDTO = DTOFactory.createCurrencyMasterDTO();
            cmDTO.setId((rs.getInt("currency_id")));
            uDTO.setcurrencyid(cmDTO);

            uDTO.setcustomeraccountnumber(rs.getString("customer_account_number"));
            uDTO.setfileid(rs.getInt("file_id"));
            uDTO.setloadingdate(rs.getTimestamp("loading_date"));
            uDTO.setnotespresented(rs.getString("notes_presented"));
            uDTO.setrecordtype(rs.getInt("record_type"));
            uDTO.setresponsecode(rs.getString("response_code"));

            TransactionResponseCodeDTOInter trcDTO = DTOFactory.createTransactionResponseCodeDTO();
            trcDTO.setId((rs.getInt("response_code_id")));
            uDTO.setresponsecodeid(trcDTO);

            uDTO.setsettlementdate(rs.getTimestamp("settlement_date"));
            uDTO.settransactiondate(rs.getTimestamp("transaction_date"));
            uDTO.settransactionsequence(rs.getString("transaction_sequence"));
            uDTO.settransactionsequenceorderby(rs.getInt("transaction_sequence_order_by"));
            uDTO.settransactionstatus(rs.getString("transaction_status"));

            TransactionStatusDTOInter tsDTO = DTOFactory.createTransactionStatusDTO();
            tsDTO.setId((rs.getInt("transaction_status_id")));
            uDTO.settransactionstatusid(tsDTO);

            uDTO.settransactiontime(rs.getTimestamp("transaction_time"));
            uDTO.settransactiontype(rs.getString("transaction_type"));

            TransactionTypeDTOInter ttDTO = DTOFactory.createTransactionTypeDTO();
            ttDTO.setId((rs.getInt("transaction_type_id")));
            uDTO.settransactiontypeid(ttDTO);

            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object revalidateRecord(RejectedTransactionsDTOInter[] rtDTOArr) throws Throwable {
        super.preCollable();

        CallableStatement stat = super.executeCallableStatment("{call validation_pkg.revalidate_record("
                + "?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
        String selectStatment = "select call validation_pkg.revalidate_record('$atmapplicationid',$atmid,$transactiontypeid,'$transactiontype',$transactionStatus,'$transactionS',$responsecode,'$response',$currencyid,'$currency',$tseqorder,$cardno,$amount,to_date('$transdate','dd.mm.yyyy hh24:mi:ss')) from dual";
        int[] done = null;
        for (RejectedTransactionsDTOInter r : rtDTOArr) {
            stat.setString(1, r.getatmapplicationid());
            selectStatment = selectStatment.replace("$atmapplicationid", r.getatmapplicationid());
            stat.setInt(2, r.getatmid().getId());
            selectStatment = selectStatment.replace("$atmid", "" + r.getatmid().getId());
            stat.setInt(3, r.gettransactiontypeid().getId());
            selectStatment = selectStatment.replace("$transactiontypeid", "" + r.gettransactiontypeid().getId());
            stat.setString(4, r.gettransactiontype());
            selectStatment = selectStatment.replace("$transactiontype", "" + r.gettransactiontype());
            stat.setInt(5, r.gettransactionstatusid().getId());
            selectStatment = selectStatment.replace("$transactionStatus", "" + r.gettransactionstatusid().getId());
            stat.setString(6, r.gettransactionstatus());
            selectStatment = selectStatment.replace("$transactionS", "" + r.gettransactionstatus());
            stat.setInt(7, r.getresponsecodeid().getId());
            selectStatment = selectStatment.replace("$responsecode", "" + r.getresponsecodeid().getId());
            stat.setString(8, r.getresponsecode());
            selectStatment = selectStatment.replace("$response", "" + r.getresponsecode());
            stat.setInt(9, r.getcurrencyid().getId());
            selectStatment = selectStatment.replace("$currencyid", "" + r.getcurrencyid().getId());
            stat.setString(10, r.getcurrency());
            selectStatment = selectStatment.replace("$currency", "" + r.getcurrency());
            stat.setInt(11, r.gettransactionsequenceorderby());
            selectStatment = selectStatment.replace("$tseqorder", "" + r.gettransactionsequenceorderby());
            stat.setString(12, r.getcardno());
            selectStatment = selectStatment.replace("$cardno", "" + r.getcardno());
            stat.setInt(13, r.getamount());
            selectStatment = selectStatment.replace("$amount", "" + r.getamount());
            stat.setTimestamp(14, (Timestamp) r.gettransactiondate());
            selectStatment = selectStatment.replace("$transdate", "" + r.gettransactiondate());
            selectStatment = selectStatment.replace("null", "");

            stat.execute();
        }
        super.postCollable(stat);
        return 0;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public void deleteForRejected(RejectedTransactionsDTOInter record, String RecordType) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();

        String deleteStat = "delete from $TABLE Where "
                + "TRANSACTION_DATE = to_date('$TRANSACTIONDATE','dd.mm.yyyy hh24:mi:ss') "
                + "AND ATM_APPLICATION_ID = '$ATMAPPLICATIONID' "
                + "AND CARD_NO = '$CARDNO' "
                + "AND TRANSACTION_SEQUENCE = '$TRANSACTIONSEQUENCE' "
                + "AND AMOUNT = $AMOUNT "
                + "AND CURRENCY = '$CURRENCY' "
                + "AND TRIM(RESPONSE_CODE) = TRIM('$RESPONSECODE') ";
        if ("1".equals(RecordType)) {
            deleteStat = deleteStat.replace("$TABLE", "rejected_journal");
        } else if ("2".equals(RecordType)) {
            deleteStat = deleteStat.replace("$TABLE", "rejected_switch");
        } else if ("3".equals(RecordType)) {
            deleteStat = deleteStat.replace("$TABLE", "rejected_host");
        }
        deleteStat = deleteStat.replace("$TRANSACTIONDATE", "" + DateFormatter.changeDateAndTimeFormat(record.gettransactiondate()).toString());
        deleteStat = deleteStat.replace("$ATMAPPLICATIONID", record.getatmapplicationid());
        deleteStat = deleteStat.replace("$CARDNO", record.getcardno());
        deleteStat = deleteStat.replace("$TRANSACTIONSEQUENCE", record.gettransactionsequence());
        deleteStat = deleteStat.replace("$AMOUNT", "" + record.getamount());
        deleteStat = deleteStat.replace("$CURRENCY", record.getcurrency());
        deleteStat = deleteStat.replace("$RESPONSECODE", record.getresponsecode());
        System.out.println(deleteStat);
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
    }

    public Object insertForRejected(RejectedTransactionsDTOInter record, String RecordType) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        String insertStat = "insert into disputes\n"
                + "      (file_id, loading_date, atm_application_id, atm_id, transaction_type, transaction_type_id, \n"
                + "       currency, currency_id, transaction_status, transaction_status_id, response_code, response_code_id, \n"
                + "       transaction_date, transaction_sequence, card_no, amount, settlement_date, notes_presented, \n"
                + "       customer_account_number, transaction_sequence_order_by, transaction_time, record_type, column1, \n"
                + "       column2, column3, column4, column5, transaction_type_master, response_code_master, card_no_suffix, \n"
                + "       abs_amount, transaction_data, MATCHING_TYPE, SETTLED_FLAG, DISPUTE_DATE, DISPUTE_WITH_ROLES, DISPUTE_KEY_UK)\n"
                + "select file_id, loading_date, atm_application_id, atm_id, transaction_type, transaction_type_id, currency, \n"
                + "       currency_id, transaction_status, transaction_status_id, response_code, response_code_id, transaction_date, \n"
                + "       transaction_sequence, card_no, amount, settlement_date, notes_presented, customer_account_number, \n"
                + "       transaction_sequence_order_by, transaction_time, $recordtype, column1, column2, column3, column4, column5, \n"
                + "       transaction_type_master, response_code_master, card_no_suffix, abs_amount, transaction_data, 1, 2, sysdate, 2, dispute_key_uk_seq.nextval \n"
                + "from   $TABLE\n"
                + "where  TRANSACTION_DATE = to_date('$TRANSACTIONDATE','dd.mm.yyyy hh24:mi:ss') \n"
                + "   AND ATM_APPLICATION_ID = '$ATMAPPLICATIONID' \n"
                + "   AND CARD_NO = '$CARDNO' \n"
                + "   AND TRANSACTION_SEQUENCE = '$TRANSACTIONSEQUENCE' \n"
                + "   AND AMOUNT = $AMOUNT \n"
                + "   AND CURRENCY = '$CURRENCY' \n"
                + "   AND TRIM(RESPONSE_CODE) = TRIM('$RESPONSECODE') ";
        if ("1".equals(RecordType)) {
            insertStat = insertStat.replace("$TABLE", "rejected_journal");
        } else if ("2".equals(RecordType)) {
            insertStat = insertStat.replace("$TABLE", "rejected_switch");
        } else if ("3".equals(RecordType)) {
            insertStat = insertStat.replace("$TABLE", "rejected_host");
        }
        insertStat = insertStat.replace("$recordtype", RecordType);
        insertStat = insertStat.replace("$TRANSACTIONDATE", "" + DateFormatter.changeDateAndTimeFormat(record.gettransactiondate()).toString());
        insertStat = insertStat.replace("$ATMAPPLICATIONID", record.getatmapplicationid());
        insertStat = insertStat.replace("$CARDNO", record.getcardno());
        insertStat = insertStat.replace("$TRANSACTIONSEQUENCE", record.gettransactionsequence());
        insertStat = insertStat.replace("$AMOUNT", "" + record.getamount());
        insertStat = insertStat.replace("$CURRENCY", record.getcurrency());
        insertStat = insertStat.replace("$RESPONSECODE", record.getresponsecode());
        System.out.println(insertStat);
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public void markasdisp(RejectedTransactionsDTOInter[] entities, String RecordType) throws Throwable {
        for (int i = 0; i < entities.length; i++) {
            insertForRejected(entities[i], RecordType);
            deleteForRejected(entities[i], RecordType);
        }
    }

    public static void main(String[] args) throws Throwable {
    }
}
