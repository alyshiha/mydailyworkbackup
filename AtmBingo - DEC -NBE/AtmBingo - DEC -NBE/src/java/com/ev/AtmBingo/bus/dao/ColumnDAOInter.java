/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.ColumnDTOInter;

/**
 *
 * @author Administrator
 */
public interface ColumnDAOInter {

    Object find(Object... obj) throws Throwable;

    Object searchByName(Object... obj) throws Throwable;

    Object findAll() throws Throwable;
     Object findAllColoumn() throws Throwable;

    Object searchByColumnName(Object... obj) throws Throwable;

    Object updateNameOnly(Object... obj) throws Throwable;
    
    Object findDefualt()throws Throwable;
    Object AddCol()throws Throwable;

    Object findNotDefualt()throws Throwable;

}
