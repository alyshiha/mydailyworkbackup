/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author ISLAM
 */
public interface FileColumnDisplayDetailsDTOInter extends Serializable{

    FileColumnDefinitionDTOInter getColumnId();

    FileColumnDisplayDTOInter getDisId();

    void setColumnId(FileColumnDefinitionDTOInter columnId);

    void setDisId(FileColumnDisplayDTOInter disId);

}
