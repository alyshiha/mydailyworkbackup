/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

/**
 *
 * @author Aly-Shiha
 */
public interface ReplainshmentLogDAOInter {

    Object findDataTable(String dailysheetFrom, String dailysheetTo,String dateFrom, String dateTo,int user) throws Throwable;

    Object findReport(String dailysheetFrom, String dailysheetTo, String dateFrom, String dateTo, int user) throws Throwable;
    
}
