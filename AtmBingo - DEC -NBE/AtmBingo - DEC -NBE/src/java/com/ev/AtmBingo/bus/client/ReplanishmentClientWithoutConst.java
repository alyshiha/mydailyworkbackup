/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.ReplanishmentMasterDAOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class ReplanishmentClientWithoutConst  implements Serializable{

         public Object getReplanishmentData(Date from, Date to, int atmId, Date createdF, Date createdT, int userId) throws Throwable {
        String whereClaus = "";
        String dateF = "";
        String dateT = "";
        String creaF = "";
        String creaT = "";
        if (from == null) {
            dateF = "null";
        } else {
            dateF = "'";
            dateF += DateFormatter.changeDateAndTimeFormat(from);
            dateF += "'";
        }

        if (to == null) {
            dateT = "null";
        } else {
            dateT = "'";
            dateT += DateFormatter.changeDateAndTimeFormat(to);
            dateT += "'";
        }

        if (createdF == null) {
            creaF = "null";
        } else {
            creaF = "'";
            creaF += DateFormatter.changeDateAndTimeFormat(createdF);
            creaF += "'";
        }

        if (createdT == null) {
            creaT = "null";
        } else {
            creaT = "'";
            creaT += DateFormatter.changeDateAndTimeFormat(createdT);
            creaT += "'";
        }

        whereClaus = "(((Date_from >=to_date($dateF,'dd.mm.yyyy hh24:mi:ss') or $dateF is null)"
                + " and (Date_to <=to_date($datT,'dd.mm.yyyy hh24:mi:ss') or $datT is null))or date_from is null or date_to is null) "
                + " and (CREATED_DATE >=to_date($creaF,'dd.mm.yyyy hh24:mi:ss') or $creaF is null)"
                + " and (CREATED_DATE <=to_date($creT,'dd.mm.yyyy hh24:mi:ss') or $creT is null)";
        if (atmId != 0) {
            whereClaus += " and atm_id = $atmId";
            whereClaus = whereClaus.replace("$atmId", "" + atmId);
        }
        if (userId != 0) {
            if (userId != -1) {
                whereClaus += " and created_by = $userId";
                whereClaus = whereClaus.replace("$userId", "" + userId);
            }
            else{
                whereClaus += " and created_by = null";
            }
        }


        whereClaus = whereClaus.replace("$dateF", dateF);
        whereClaus = whereClaus.replace("$datT", dateT);
        whereClaus = whereClaus.replace("$creaF", creaF);
        whereClaus = whereClaus.replace("$creT", creaT);

        whereClaus += " order by date_from,created_date ASC";
        ReplanishmentMasterDAOInter rmDAO = DAOFactory.createReplanishmentMasterDAO(null);
        FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

          Session sessionutil = new Session();
        UsersDTOInter currentUser = sessionutil.GetUserLogging();
        List<ReplanishmentMasterDTOInter> rmDTOL = (List<ReplanishmentMasterDTOInter>) rmDAO.customSearch(whereClaus,currentUser.getUserId());

        return rmDTOL;

    }

  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }    }




