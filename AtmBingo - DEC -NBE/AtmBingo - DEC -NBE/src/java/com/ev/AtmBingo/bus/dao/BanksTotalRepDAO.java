/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.cardissuerDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class BanksTotalRepDAO extends BaseDAO implements BanksTotalRepDAOInter {

    protected BanksTotalRepDAO() {
        super();
    }
    public Object findCardIssuerexcelReport(String dateFrom, String dateTo, int atmGroupInt, int side) throws Throwable {
        super.preSelect();
        String selectStatment = "select sum(get_number_type(m.amount)) type_total,t.name,1 dummy,sum(m.amount*m.AMOUNT_TYPE) total_amount,(select symbol from currency_master cm where cm.id = m.currency_id) currency"
                + " from all_transactions m , bank_card_ranges c , banks t"
                + " where m.transaction_date between to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and ((m.atm_id in (select id from atm_machine where atm_group in (select id from  (select * from atm_group l order BY l.parent_id,name) start with id =  $P{AtmGroupInt} connect by prior id = parent_id))) or $P{AtmGroupInt} = 0)"
                + " and m.card_no_suffix between c.range_from and c.range_to"
                + " and t.id = c.bank_id"
                + " and m.record_type = $P{Side}"
                + " group by t.name,m.currency_id"
                + " union"
                + " select sum(get_number_type(m.amount)),'Others',2 dummy,sum(m.amount*m.AMOUNT_TYPE) total_amount,(select symbol from currency_master cm where cm.id = m.currency_id) currency"
                + " from all_transactions m"
                + " where m.transaction_date between to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and ((m.atm_id in (select id from atm_machine where atm_group in (select id from  (select * from atm_group l order BY l.parent_id,name) start with id =  $P{AtmGroupInt} connect by prior id = parent_id))) or $P{AtmGroupInt} = 0)"
                + " and m.record_type = $P{Side}"
                + " and not exists"
                + " (select 1"
                + " from matched_data x , bank_card_ranges c"
                + " where x.TRANSACTION_DATE = m.TRANSACTION_DATE"
                + " and x.ATM_APPLICATION_ID = m.ATM_APPLICATION_ID"
                + " and x.CARD_NO = m.CARD_NO"
                + " and x.TRANSACTION_SEQUENCE = m.TRANSACTION_SEQUENCE"
                + " and x.AMOUNT = m.AMOUNT"
                + " and x.CURRENCY = m.CURRENCY"
                + " and trim(x.RESPONSE_CODE) = m.RESPONSE_CODE"
                + " and x.RECORD_TYPE = m.RECORD_TYPE"
                + " and x.MATCHING_TYPE = m.MATCHING_TYPE"
                + " and x.card_no_suffix between c.range_from and c.range_to)"
                + " group by m.currency_id"
                + " order by 3,2";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DatTo}", dateTo);
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        selectStatment = selectStatment.replace("$P{Side}", "" + side);
        ResultSet rs = executeQueryReport(selectStatment);
        List<cardissuerDTOInter> Records = new ArrayList<cardissuerDTOInter>();
        while (rs.next()) {
            cardissuerDTOInter record = DTOFactory.createcardissuerDTO();
            record.setTotalcount(rs.getInt("type_total"));
            record.setName(rs.getString("name"));
            record.setTotalamount(rs.getInt("total_amount"));
            record.setCurrency(rs.getString("currency"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }
    public Object findReport(String dateFrom, String dateTo, int atmGroupInt, int side) throws Throwable {
        super.preSelect();
        String selectStatment = "select sum(get_number_type(m.amount)) type_total,t.name,1 dummy,sum(m.amount*m.AMOUNT_TYPE) total_amount,(select symbol from currency_master cm where cm.id = m.currency_id) currency"
                + " from all_transactions m , bank_card_ranges c , banks t"
                + " where m.transaction_date between to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and ((m.atm_id in (select id from atm_machine where atm_group in (select id from  (select * from atm_group l order BY l.parent_id,name) start with id =  $P{AtmGroupInt} connect by prior id = parent_id))) or $P{AtmGroupInt} = 0)"
                + " and m.card_no_suffix between c.range_from and c.range_to"
                + " and t.id = c.bank_id"
                + " and m.record_type = $P{Side}"
                + " group by t.name,m.currency_id"
                + " union"
                + " select sum(get_number_type(m.amount)),'Others',2 dummy,sum(m.amount*m.AMOUNT_TYPE) total_amount,(select symbol from currency_master cm where cm.id = m.currency_id) currency"
                + " from all_transactions m"
                + " where m.transaction_date between to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and ((m.atm_id in (select id from atm_machine where atm_group in (select id from  (select * from atm_group l order BY l.parent_id,name) start with id =  $P{AtmGroupInt} connect by prior id = parent_id))) or $P{AtmGroupInt} = 0)"
                + " and m.record_type = $P{Side}"
                + " and not exists"
                + " (select 1"
                + " from matched_data x , bank_card_ranges c"
                + " where x.TRANSACTION_DATE = m.TRANSACTION_DATE"
                + " and x.ATM_APPLICATION_ID = m.ATM_APPLICATION_ID"
                + " and x.CARD_NO = m.CARD_NO"
                + " and x.TRANSACTION_SEQUENCE = m.TRANSACTION_SEQUENCE"
                + " and x.AMOUNT = m.AMOUNT"
                + " and x.CURRENCY = m.CURRENCY"
                + " and trim(x.RESPONSE_CODE) = m.RESPONSE_CODE"
                + " and x.RECORD_TYPE = m.RECORD_TYPE"
                + " and x.MATCHING_TYPE = m.MATCHING_TYPE"
                + " and x.card_no_suffix between c.range_from and c.range_to)"
                + " group by m.currency_id"
                + " order by 3,2";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DatTo}", dateTo);
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        selectStatment = selectStatment.replace("$P{Side}", "" + side);
        ResultSet rs = executeQueryReport(selectStatment);
        super.postSelect();
        return rs;
    }
}
