/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.SettlementLogDAOInter;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.SettlementLogDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author KhAiReE
 */
public class SettlementLogBO extends BaseBO implements SettlementLogBOInter,Serializable{

    protected SettlementLogBO() {
        super();
    }
       public Object getAtmMachines(UsersDTOInter loggedIn) throws Throwable {
        AtmMachineDAOInter amDAO = DAOFactory.createAtmMachineDAO(null);
        List<AtmMachineDTOInter> amDTOL = (List<AtmMachineDTOInter>) amDAO.findRecordGroup(loggedIn);
        return amDTOL;
    }
    public Object getUsers (int rest)throws Throwable {
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> ul = (List<UsersDTOInter>)uDAO.findALl(rest);
        return ul ;
    }
    public Object getSettlementLogged ( Date dateF , Date dateT , Integer user , Integer status ,String cardNo,Integer ATMID) throws Throwable {
        String whereClause  = "";
       // whereClause = whereClause.replace("$dateT", "" + DateFormatter.changeDateAndTimeFormat(dateT));
      //  whereClause = whereClause.replace("$dateF", "" + DateFormatter.changeDateAndTimeFormat(dateF));


         if (dateF != null && dateT != null)
        {
            whereClause += " and settlement_date between to_date('$dateF','dd.mm.yyyy hh24:mi:ss') and to_date('$dateT','dd.mm.yyyy hh24:mi:ss')";
            whereClause = whereClause.replace("$dateF", "" + DateFormatter.changeDateAndTimeFormat(dateF));
            whereClause = whereClause.replace("$dateT", "" + DateFormatter.changeDateAndTimeFormat(dateT));
        }


         if (ATMID.toString() != null && !ATMID.toString().equals("0"))
        {
            whereClause += " and atm_application_id = $ATM";
            whereClause = whereClause.replace("$ATM", "" + ATMID.toString());
        }
        if (user !=0 )
        {
            whereClause += " and settled_user = $user";
            whereClause = whereClause.replace("$user", "" + user);
        }
        if (status != 0)
        {
            whereClause += " and settled_flag = $status";
            whereClause = whereClause.replace("$status", "" + status);
        }

          if (cardNo != null && !cardNo.equals(""))
        {
            whereClause += " and card_no like '%$card_no%'";
            whereClause = whereClause.replace("$card_no", "" + cardNo);
        }

        SettlementLogDAOInter slDAO = DAOFactory.createSettlementLogDAO();
        List<SettlementLogDTOInter> slL = (List<SettlementLogDTOInter>)slDAO.CustomSearch(whereClause);
        return slL ;
    }

}
