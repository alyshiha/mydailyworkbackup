/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import DBCONN.Session;
import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.AtmDefinitionBOInter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.UserAtmsBOInter;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.UserAtmDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.TransferHandler;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.DragDropEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DualListModel;

/**
 *
 * @author ISLAM
 */
public class UserATMClient extends BaseBean  implements Serializable{

    private AtmDefinitionBOInter atmObject2;
    private UserAtmsBOInter uatmObject;
    private List<UserAtmDTOInter> uatmList;
    private List<String> targetList, sourceList;
    private String[] userCounts;
    private List<AtmGroupDTOInter> atmGList;
    private List<UsersDTOInter> userList;
    private DualListModel<String> pickValues;
    private String[] selectedSource, selectedTarget;
    private Integer atmGInt, userInt;
    private String message, ATMGroupCount, selectText, selectText1;
    private Boolean disabledSave;

    public String getSelectText1() {
        return selectText1;
    }

    public void setSelectText1(String selectText1) {
        this.selectText1 = selectText1;
    }

    public String getSelectText() {
        return selectText;
    }

    public void setSelectText(String selectText) {
        this.selectText = selectText;
    }

    public String[] getSelectedSource() {
        return selectedSource;
    }

    public void setSelectedSource(String[] selectedSource) {
        this.selectedSource = selectedSource;
    }

    public String[] getSelectedTarget() {
        return selectedTarget;
    }

    public void setSelectedTarget(String[] selectedTarget) {
        this.selectedTarget = selectedTarget;
    }

    public String[] getUserCounts() {
        return userCounts;
    }

    public void setUserCounts(String[] userCounts) {
        this.userCounts = userCounts;
    }

    public String getATMGroupCount() {
        return ATMGroupCount;
    }

    public void setATMGroupCount(String ATMGroupCount) {
        this.ATMGroupCount = ATMGroupCount;
    }

    public Boolean getDisabledSave() {
        return disabledSave;
    }

    public void setDisabledSave(Boolean disabledSave) {
        this.disabledSave = disabledSave;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DualListModel<String> getPickValues() {
        return pickValues;
    }

    public void setPickValues(DualListModel<String> pickValues) {
        this.pickValues = pickValues;
    }

    public Integer getAtmGInt() {
        return atmGInt;
    }

    public void setAtmGInt(Integer atmGInt) {
        this.atmGInt = atmGInt;
    }

    public Integer getUserInt() {
        return userInt;
    }

    public void setUserInt(Integer userInt) {
        this.userInt = userInt;
    }

    public List<AtmGroupDTOInter> getAtmGList() {
        return atmGList;
    }

    public void setAtmGList(List<AtmGroupDTOInter> atmGList) {
        this.atmGList = atmGList;
    }

    public List<String> getSourceList() {
        return sourceList;
    }

    public void setSourceList(List<String> sourceList) {
        this.sourceList = sourceList;
    }

    public List<String> getTargetList() {
        return targetList;
    }

    public void setTargetList(List<String> targetList) {
        this.targetList = targetList;
    }

    public List<UserAtmDTOInter> getUatmList() {
        return uatmList;
    }

    public void setUatmList(List<UserAtmDTOInter> uatmList) {
        this.uatmList = uatmList;
    }

    public List<UsersDTOInter> getUserList() {
        return userList;
    }

    public void setUserList(List<UsersDTOInter> userList) {
        this.userList = userList;
    }

    /** Creates a new instance of UserATMClient */
    private Session utillist = new Session();
    public UserATMClient() throws Throwable {
        super();
        super.GetAccess();
        disabledSave = true;
        selectText = "Select all records";
        selectText1 = "Select all records";
        targetList = new ArrayList<String>();
        sourceList = new ArrayList<String>();
        pickValues = new DualListModel<String>(targetList, sourceList);
        uatmObject = BOFactory.createUserAtmBO(null);
        atmObject2 = BOFactory.createAtmDefinitionBO(null);
        ATMGroupCount = (String)  atmObject2.getGroupCount();
        userCounts = ATMGroupCount.split(",");
        atmGList = (List<AtmGroupDTOInter>) utillist.GetATMGroupList();
        userList = (List<UsersDTOInter>) utillist.GetUsersList();
     FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void userChange(ValueChangeEvent e) throws Throwable {
        message = "";
        DataTable repTable1 = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:blockReason1");
        DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:blockReason");
        userInt = (Integer) e.getNewValue();
        if (userInt == 0) {
            disabledSave = true;
            targetList.clear();
            sourceList.clear();
        } else {
            disabledSave = false;
            if (atmGInt == null) {
                atmGInt = 0;
            }
            userInt = (Integer) e.getNewValue();
            targetList = (List<String>) uatmObject.getTargetList(userInt, atmGInt);
            sourceList = (List<String>) uatmObject.getSourceList(userInt, atmGInt);
        }
        selectText = "Select all records";
        selectText1 = "Select all records";
        selectedSource = null;
        selectedTarget = null;
        repTable.setSelection(null);
        repTable1.setSelection(null);
    }

    public void atmChange(ValueChangeEvent e) throws Throwable {
        resetVars();
            DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:blockReason");
            DataTable repTable1 = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:blockReason1");
        atmGInt = (Integer) e.getNewValue();
        if (userInt == null || userInt == 0) {
            targetList.clear();
            userInt = 0;
            message = "Please select user ";
        } else {
            targetList = (List<String>) uatmObject.getTargetList(userInt, atmGInt);
            atmGInt = (Integer) e.getNewValue();
            sourceList = (List<String>) uatmObject.getSourceList(userInt, atmGInt);
        }
        selectText = "Select all records";
        selectText1 = "Select all records";
        selectedSource = null;
        selectedTarget = null;
        repTable.setSelection(null);
        repTable1.setSelection(null);
    }
    
    public void saveChange() throws Throwable {
        if (selectedSource == null && selectedTarget == null) {
            message = "No records to transfer";
        } else if (uatmObject.saveUserAtm(userInt, selectedTarget, selectedSource, atmGInt).equals("True")) {
            message = super.showMessage(SUCC);
            targetList = (List<String>) uatmObject.getTargetList(userInt, atmGInt);
            sourceList = (List<String>) uatmObject.getSourceList(userInt, atmGInt);
            selectText = "Select all records";
            selectText1 = "Select all records";
        } else {
            message = super.showMessage(REQ_FIELD);
        }
    }

    public void selectTargetTable() {
        resetVars();
        if (targetList.size() != 0) {
            DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:blockReason");
            if (selectText.equals("Select all records")) {
                repTable.setSelection(targetList.toArray());
                selectText = "Deselect all records";
            } else {
                selectedTarget = null;
                selectText = "Select all records";
            }
        }
    }

    public void selectSourceTable() {
        resetVars();
        if (sourceList.size() != 0) {
            DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form1:blockReason1");
            if (selectText1.equals("Select all records")) {
                repTable.setSelection(sourceList.toArray());
                selectText1 = "Deselect all records";
            } else {
                selectedSource = null;
                selectText1 = "Select all records";
            }
        }
    }

    public void resetVars() {
        message = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
