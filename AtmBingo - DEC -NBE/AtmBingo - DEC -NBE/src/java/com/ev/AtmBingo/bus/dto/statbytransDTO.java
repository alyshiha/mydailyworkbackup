/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class statbytransDTO implements statbytransDTOInter, Serializable {
    private String atmapplicationid, name;
    private Integer transnumber,total;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    @Override
    public String getAtmapplicationid() {
        return atmapplicationid;
    }

    @Override
    public void setAtmapplicationid(String atmapplicationid) {
        this.atmapplicationid = atmapplicationid;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Integer getTransnumber() {
        return transnumber;
    }

    @Override
    public void setTransnumber(Integer transnumber) {
        this.transnumber = transnumber;
    }
    
}
