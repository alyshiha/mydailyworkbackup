/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;

/**
 *
 * @author Administrator
 */
public interface UsersBOInter extends BaseBOInter {
UsersDTOInter finduser(Integer user) throws Throwable;
    Object editeUsers(UsersDTOInter uDTO, int operation) throws Throwable;

    Object getTimeShifts() throws Throwable;
Boolean getaccess(UsersDTOInter user) throws Throwable ;
    Object getUsers(int rest) throws Throwable;

    Object getlang() throws Throwable;

    Object getPrifiles(int restricted)throws Throwable;

}
