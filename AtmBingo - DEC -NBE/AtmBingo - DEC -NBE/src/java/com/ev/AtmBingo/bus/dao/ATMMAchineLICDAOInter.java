/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.ATMMAchineLICInter;
import com.ev.AtmBingo.bus.dto.LincManagmentDetailsDTOInter;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface ATMMAchineLICDAOInter {
public LincManagmentDetailsDTOInter LicMangDetail() throws Throwable ;
 public  Object find() throws Throwable;
 public  Object findfilter(List<ATMMAchineLICInter> AtmList,String Like) throws Throwable;
 public Integer GetLIC() throws Throwable;
 public void save(List<ATMMAchineLICInter> entities,String State ) throws SQLException;
 public void Delete(List<ATMMAchineLICInter> entities) throws SQLException;
}
