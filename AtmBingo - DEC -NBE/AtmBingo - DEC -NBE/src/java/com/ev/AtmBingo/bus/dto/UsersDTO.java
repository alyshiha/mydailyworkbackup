package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;
import java.util.List;

public class UsersDTO extends BaseDTO implements Serializable, UsersDTOInter {

    private int userId;
    private String userName;
    private String userAName;
    private int userLang;
    private int holidayEnabled;
    private String logonName;
    private int unsettlement;
    private int settlement;
    private String userPassword;
    private String confirmPassword;
    private int shiftId;
    private int emrHolidayEnabled;
    private int disputeGraph;
    private int showNewAtms;
    private int onlineuser;
    private int errors;
    private int notification;
    private int pendingatms;
    private int visible;
    private int seeUnvisble;
    private int changeOtherUserPassword;
    private Boolean disputGraphB;
    private Boolean holidayEnabledB;
    private Boolean emrHolidayEnabledB;
    private Boolean showNewAtmsB;
    private Boolean showcorrect, showexport;
    private Boolean unsettlementboolean;
    private Boolean settlementboolean;
    private int Export, CorrectiveEntry;
    private Boolean pendingAtms, notificationB, errorsB, onlineuserB;
    private int restrected;
    private int seeRestrected;
    private int status;
    private ProfileDTOInter profileDTO;

    public Boolean getUnsettlementboolean() {
        return unsettlementboolean;
    }

    public void setUnsettlementboolean(Boolean unsettlementboolean) {
        this.unsettlementboolean = unsettlementboolean;
    }

    public Boolean getSettlementboolean() {
        return settlementboolean;
    }

    public void setSettlementboolean(Boolean settlementboolean) {
        this.settlementboolean = settlementboolean;
    }

    public Boolean getShowcorrect() {
        return showcorrect;
    }

    public int getUnsettlement() {
        return unsettlement;
    }

    public void setUnsettlement(int unsettlement) {
        this.unsettlement = unsettlement;
    }

    public int getSettlement() {
        return settlement;
    }

    public void setSettlement(int settlement) {
        this.settlement = settlement;
    }

    public void setShowcorrect(Boolean showcorrect) {
        this.showcorrect = showcorrect;
    }

    public Boolean getShowexport() {
        return showexport;
    }

    public void setShowexport(Boolean showexport) {
        this.showexport = showexport;
    }

    public Boolean getShowCorrect() {
        return showcorrect;
    }

    public void setShowCorrect(Boolean ShowCorrect) {
        this.showcorrect = ShowCorrect;
    }

    public int getCorrectiveEntry() {
        return CorrectiveEntry;
    }

    public void setCorrectiveEntry(int CorrectiveEntry) {
        this.CorrectiveEntry = CorrectiveEntry;
    }

    public int getExport() {
        return Export;
    }

    public void setExport(int Export) {
        this.Export = Export;
    }

    public Boolean getErrorsB() {
        return errorsB;
    }

    public void setErrorsB(Boolean errorsB) {
        this.errorsB = errorsB;
    }

    public Boolean getNotificationB() {
        return notificationB;
    }

    public void setNotificationB(Boolean notificationB) {
        this.notificationB = notificationB;
    }

    public Boolean getOnlineuserB() {
        return onlineuserB;
    }

    public void setOnlineuserB(Boolean onlineuserB) {
        this.onlineuserB = onlineuserB;
    }

    public Boolean getPendingAtms() {
        return pendingAtms;
    }

    public void setPendingAtms(Boolean pendingAtms) {
        this.pendingAtms = pendingAtms;
    }

    public int getErrors() {
        return errors;
    }

    public void setErrors(int errors) {
        this.errors = errors;
    }

    public int getNotification() {
        return notification;
    }

    public void setNotification(int notification) {
        this.notification = notification;
    }

    public int getOnlineuser() {
        return onlineuser;
    }

    public void setOnlineuser(int onlineuser) {
        this.onlineuser = onlineuser;
    }

    public int getPendingatms() {
        return pendingatms;
    }

    public void setPendingatms(int pendingatms) {
        this.pendingatms = pendingatms;
    }

    public ProfileDTOInter getProfileDTO() {

        return profileDTO;

    }

    public void setProfileDTO(ProfileDTOInter profileDTO) {
        this.profileDTO = profileDTO;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getRestrected() {
        return restrected;
    }

    public void setRestrected(int restrected) {
        this.restrected = restrected;
    }

    public int getSeeRestrected() {
        return seeRestrected;
    }

    public void setSeeRestrected(int seeRestrected) {
        this.seeRestrected = seeRestrected;
    }
    private List<AtmMachineDTOInter> atmMachineDTOL;

    public String getConfirmPassword() {
        confirmPassword = userPassword;
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public List<AtmMachineDTOInter> getAtmMachineDTOL() {
        return atmMachineDTOL;
    }

    public void setAtmMachineDTOL(List<AtmMachineDTOInter> atmMachineDTOL) {
        this.atmMachineDTOL = atmMachineDTOL;
    }

    public Boolean getDisputGraphB() {
        disputGraphB = disputeGraph == 1;
        return disputGraphB;
    }

    public void setDisputGraphB(Boolean disputGraphB) {
        if (disputGraphB) {
            disputeGraph = 1;
        } else {
            disputeGraph = 2;
        }
        this.disputGraphB = disputGraphB;
    }

    public Boolean getEmrHolidayEnabledB() {
        emrHolidayEnabledB = emrHolidayEnabled == 1;
        return emrHolidayEnabledB;
    }

    public void setEmrHolidayEnabledB(Boolean emrHolidayEnabledB) {
        if (emrHolidayEnabledB) {
            emrHolidayEnabled = 1;
        } else {
            emrHolidayEnabled = 2;
        }
        this.emrHolidayEnabledB = emrHolidayEnabledB;
    }

    public Boolean getHolidayEnabledB() {
        holidayEnabledB = holidayEnabled == 1;
        return holidayEnabledB;
    }

    public void setHolidayEnabledB(Boolean holidayEnabledB) {
        if (holidayEnabledB) {
            holidayEnabled = 1;
        } else {
            holidayEnabled = 2;
        }
        this.holidayEnabledB = holidayEnabledB;
    }

    public Boolean getShowNewAtmsB() {
        showNewAtmsB = showNewAtms == 1;
        return showNewAtmsB;
    }

    public void setShowNewAtmsB(Boolean showNewAtmsB) {
        if (showNewAtmsB) {
            showNewAtms = 1;
        } else {
            showNewAtms = 2;
        }
        this.showNewAtmsB = showNewAtmsB;
    }

    public int getChangeOtherUserPassword() {
        return changeOtherUserPassword;
    }

    public void setChangeOtherUserPassword(int changeOtherUserPassword) {
        this.changeOtherUserPassword = changeOtherUserPassword;
    }

    public int getDisputeGraph() {
        return disputeGraph;
    }

    public void setDisputeGraph(int disputeGraph) {
        this.disputeGraph = disputeGraph;
    }

    public int getEmrHolidayEnabled() {
        return emrHolidayEnabled;
    }

    public void setEmrHolidayEnabled(int emrHolidayEnabled) {
        this.emrHolidayEnabled = emrHolidayEnabled;
    }

    public int getHolidayEnabled() {
        return holidayEnabled;
    }

    public void setHolidayEnabled(int holidayEnabled) {
        this.holidayEnabled = holidayEnabled;
    }

    public String getLogonName() {
        return logonName;
    }

    public void setLogonName(String logonName) {
        this.logonName = logonName;
    }

    public int getSeeUnvisble() {
        return seeUnvisble;
    }

    public void setSeeUnvisble(int seeUnvisble) {
        this.seeUnvisble = seeUnvisble;
    }

    public int getShiftId() {
        return shiftId;
    }

    public void setShiftId(int shiftId) {
        this.shiftId = shiftId;
    }

    public int getShowNewAtms() {
        return showNewAtms;
    }

    public void setShowNewAtms(int showNewAtms) {
        this.showNewAtms = showNewAtms;
    }

    public String getUserAName() {
        return userAName;
    }

    public void setUserAName(String userAName) {
        this.userAName = userAName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserLang() {
        return userLang;
    }

    public void setUserLang(int userLang) {
        this.userLang = userLang;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public int getVisible() {
        return visible;
    }

    public void setVisible(int visible) {
        this.visible = visible;
    }

    protected UsersDTO() {
        super();
        profileDTO = DTOFactory.createProfileDTO();
    }
}
