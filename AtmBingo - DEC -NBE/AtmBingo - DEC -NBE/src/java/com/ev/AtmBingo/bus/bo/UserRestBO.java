/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.UsersDAOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class UserRestBO extends BaseBO implements UserRestBOInter,Serializable{

    protected UserRestBO() {
        super();
    }

    public Object getUsers(int rest)throws Throwable{
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        List<UsersDTOInter> uDTOL = (List<UsersDTOInter>)uDAO.findALl(rest);
        return uDTOL;
    }

    public Object update(UsersDTOInter uDTO)throws Throwable{
        UsersDAOInter uDAO = DAOFactory.createUsersDAO(null);
        uDAO.updateRest(uDTO);
        return "Done";
    }

}
