package com.ev.AtmBingo.bus.dto;
import java.io.Serializable;
import java.util.Date;
public interface useratmbranchDTOInter extends Serializable {
int getuserid();
void setuserid(int userid);
int getatmid();
void setatmid(int atmid);
}