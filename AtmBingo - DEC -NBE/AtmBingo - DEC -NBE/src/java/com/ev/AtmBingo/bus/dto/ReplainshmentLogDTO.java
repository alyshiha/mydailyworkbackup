/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author Aly-Shiha
 */
public class ReplainshmentLogDTO implements ReplainshmentLogDTOInter {

    private String atmID;
    private String comments;
    private String user;
    private Date createdBy;
    private Date dateFrom;
    private Date dateTo;
    private Date dailySheet;

    public ReplainshmentLogDTO() {
    }

    public ReplainshmentLogDTO(String atmID, String comments, String user, Date createdBy, Date dateFrom, Date dateTo,Date dailySheet) {
        this.atmID = atmID;
        this.comments = comments;
        this.user = user;
        this.createdBy = createdBy;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.dailySheet = dailySheet;
    }

    public Date getDailySheet() {
        return dailySheet;
    }

    public void setDailySheet(Date dailySheet) {
        this.dailySheet = dailySheet;
    }

    @Override
    public String getAtmID() {
        return atmID;
    }

    @Override
    public void setAtmID(String atmID) {
        this.atmID = atmID;
    }

    @Override
    public String getComments() {
        return comments;
    }

    @Override
    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String getUser() {
        return user;
    }

    @Override
    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public Date getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(Date createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getDateFrom() {
        return dateFrom;
    }

    @Override
    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Override
    public Date getDateTo() {
        return dateTo;
    }

    @Override
    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
