/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.AtmGroupDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author Administrator
 */
public class AtmGroupBO extends BaseBO implements AtmGroupBOInter,Serializable{

     private TreeNode root;
    private TreeNode[] allNodes;
    

    protected AtmGroupBO() throws Throwable {
        super();
        fillTree();
    }

    public TreeNode[] getSelectedNode() {
        return allNodes;
    }

    public void setSelectedNode(TreeNode[] allNodes) {
        this.allNodes = allNodes;
    }

    public TreeNode getRoot() {
        return root;
    }

    public void setRoot(TreeNode root) {
        this.root = root;
    }

    public Object getAllGroups()throws Throwable{
        AtmGroupDAOInter agDAO = DAOFactory.createAtmGroupDAO(null);
        List<AtmGroupDTOInter> agDTOL = (List<AtmGroupDTOInter>)agDAO.findAll();
        return agDTOL;
    }
    public Object getChildren(String name)throws Throwable{
        AtmGroupDAOInter agDAO = DAOFactory.createAtmGroupDAO(null);
        AtmGroupDTOInter agDTO = (AtmGroupDTOInter)agDAO.findByName(name);
        List<AtmGroupDTOInter> agDTOL = (List<AtmGroupDTOInter>)agDAO.findChildren(agDTO);
        return agDTOL;
    }
    public Object editeAtmGroup(AtmGroupDTOInter agDTO,String parent, int operation )throws Throwable{
        AtmGroupDAOInter agDAO = DAOFactory.createAtmGroupDAO(null);
        AtmGroupDTOInter parentDTO = (AtmGroupDTOInter)agDAO.findByName(parent);

        switch(operation){
            case INSERT:
                agDTO.setParentId(parentDTO.getId());
                agDAO.insert(agDTO);
                return "Insert Done";
            case UPDATE:
                agDAO.update(agDTO);
                return "Update Done";
            case DELETE:
                agDAO.delete(agDTO);
                return "Delete Done";
                default:
                    return "Their Is No Operation";
        }
    }



    private  void fillTree() throws Throwable{
        AtmGroupDAOInter agDAO = DAOFactory.createAtmGroupDAO(null);
        List<AtmGroupDTOInter> agDTOL = new ArrayList<AtmGroupDTOInter>();
        agDTOL = (List<AtmGroupDTOInter>) agDAO.findTree();
        int level2 = 0;
        int level3 = 0;
        int level4 = 0;
        int level5 = 0;
        root = new DefaultTreeNode("Root", null);
        allNodes = new TreeNode[agDTOL.size()];
        int i = 0;

        for (AtmGroupDTOInter o : agDTOL) {
            if (o.getParentId() == 0) {
                allNodes[i] = new DefaultTreeNode(o.getName(), root);
            } else if (o.getLev() == 2) {
                allNodes[i] = new DefaultTreeNode(o.getName(), allNodes[0]);
                level2 = i;
            } else if (o.getLev() == 3) {
                allNodes[i] = new DefaultTreeNode(o.getName(), allNodes[level2]);
                level3 = i;
            }
            else if (o.getLev() == 4) {
                allNodes[i] = new DefaultTreeNode(o.getName(), allNodes[level3]);
                level4 = i;
            }
            else if (o.getLev() == 5) {
                allNodes[i] = new DefaultTreeNode(o.getName(), allNodes[level4]);
                level5 = i;
            }
            i++;
        }
    }
    public static void main (String[] args) throws Throwable{
      
    }
}
