/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface BlockReasonDTOInter extends Serializable {

    String getReasonAName();

    int getReasonId();

    String getReasonName();

    void setReasonAName(String reasonAName);

    void setReasonId(int reasonId);

    void setReasonName(String reasonName);

}
