/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.CardsTakenReportDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.bybranchreportDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Aly
 */
public class bybranchreportDAO extends BaseDAO implements bybranchreportDAOInter {
@Override
    public Object findAllcards(Date datefrom, Date dateto) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select count(distinct t.card_no),t.atm_id,(select sum(r.cards_taken) from replanishment_master r where r.atm_id = t.atm_id and r.date_from > to_date('$datef','dd.mm.yyyy hh24:mi:ss') and r.date_to < to_date('$datet','dd.mm.yyyy hh24:mi:ss')) Card_Collected  from ALL_BRANCH_TRANSACTIONS t where t.transaction_date between to_date('$datef','dd.mm.yyyy hh24:mi:ss') and to_date('$datet','dd.mm.yyyy hh24:mi:ss')  group by t.atm_id";
        selectStat = selectStat.replace("$datef", "" + DateFormatter.changeDateAndTimeFormat(datefrom));
        selectStat = selectStat.replace("$datet", "" + DateFormatter.changeDateAndTimeFormat(dateto));
        ResultSet rs = executeQuery(selectStat);
        List<CardsTakenReportDTOInter> uDTOL = new ArrayList<CardsTakenReportDTOInter>();
        while (rs.next()) {
            CardsTakenReportDTOInter uDTO = DTOFactory.createCardsTakenReportDTO();
            uDTO.setAtmaplication(rs.getString(2));
            uDTO.setCardonfile(rs.getString(1));
            uDTO.setCaronatm(rs.getString(3));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }
    @Override
    public Object findAll(int branch, Date datefrom, Date dateto) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select t.card_no cardnum,t.takendate CollectedDate,nvl((select  name from branch_reason WHERE id = t.reason),'Pending') Status,(select name from branch_code where id = b.branchname) BranchName "
                + "from CARD_TAKEN t,users_branch b where b.user_id=t.userid  "
                + "and ($userid = 0 or b.branchname = $userid) and t.takendate between to_date('$datef','dd.mm.yyyy hh24:mi:ss') and to_date('$datet','dd.mm.yyyy hh24:mi:ss')";
        selectStat = selectStat.replace("$userid", "" + branch);
        selectStat = selectStat.replace("$datef", "" + DateFormatter.changeDateAndTimeFormat(datefrom));
        selectStat = selectStat.replace("$datet", "" + DateFormatter.changeDateAndTimeFormat(dateto));
        ResultSet rs = executeQuery(selectStat);
        List<bybranchreportDTOInter> uDTOL = new ArrayList<bybranchreportDTOInter>();
        while (rs.next()) {
            bybranchreportDTOInter uDTO = DTOFactory.createbybranchreportDTO();
            uDTO.setCollecteddate(rs.getTimestamp("CollectedDate"));
            uDTO.setStatus(rs.getString("Status"));
            uDTO.setCardnum(rs.getString("cardnum"));
            uDTO.setBranch(rs.getString("BranchName"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }
}
