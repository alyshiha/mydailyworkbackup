/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.TimeShiftDetailDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TimeShiftDetailDAO extends BaseDAO implements TimeShiftDetailDAOInter {

    protected TimeShiftDetailDAO() {
        super();
        super.setTableName("time_shift_detail");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TimeShiftDetailDTOInter uDTO = (TimeShiftDetailDTOInter) obj[0];
        String insertStat = "insert into $table values ($shift_id , $day_no, '$time_from', '$time_to'"
                + ", $working)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$shift_id", "" + uDTO.getShiftId());
        insertStat = insertStat.replace("$day_no", "" + uDTO.getDayNo());
        insertStat = insertStat.replace("$time_from", "" + DateFormatter.changeTimeFormat(uDTO.getTimeFrom()));
        insertStat = insertStat.replace("$time_to", "" + DateFormatter.changeTimeFormat(uDTO.getTimeTo()));
        insertStat = insertStat.replace("$working", "" + uDTO.getWorking());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TimeShiftDetailDTOInter uDTO = (TimeShiftDetailDTOInter) obj[0];
        Integer oldDay = (Integer) obj[1];
        String updateStat = "update $table set day_no = $day_no, time_from = to_date('$time_from','hh24:mi:ss'), time_to = to_date('$time_to','hh24:mi:ss')"
                + ", working = $working"
                + " where shift_id = $shift_id and day_no = $oldDay";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$shift_id", "" + uDTO.getShiftId());
        updateStat = updateStat.replace("$day_no", "" + uDTO.getDayNo());
        updateStat = updateStat.replace("$time_from", "" + DateFormatter.changeTimeFormat(uDTO.getTimeFrom()));
        updateStat = updateStat.replace("$time_to", "" + DateFormatter.changeTimeFormat(uDTO.getTimeTo()));
        updateStat = updateStat.replace("$working", "" + uDTO.getWorking());
        updateStat = updateStat.replace("$oldDay", "" + oldDay);
        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TimeShiftDetailDTOInter uDTO = (TimeShiftDetailDTOInter) obj[0];
        String deleteStat = "delete from $table where shift_id = $shift_id and day_no = $day_no";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$shift_id", "" + uDTO.getShiftId());
        deleteStat = deleteStat.replace("$day_no", "" + uDTO.getDayNo());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer shiftId = (Integer) obj[0];
        Integer dayNo = (Integer) obj[1];
        String selectStat = "select * from $table where shift_id = $shift_id and day_no = $day_no";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$shift_id", "" + shiftId);
        selectStat = selectStat.replace("$day_no", "" + dayNo);
        ResultSet rs = executeQuery(selectStat);
        TimeShiftDetailDTOInter uDTO = DTOFactory.createTimeShiftDetailDTO();
        while (rs.next()) {
            uDTO.setDayNo(rs.getInt("day_no"));
            uDTO.setShiftId(rs.getInt("shift_id"));
            uDTO.setTimeFrom(rs.getTime("time_from"));
            uDTO.setTimeTo(rs.getTime("time_to"));
            uDTO.setWorking(rs.getInt("working"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findByShiftId(int shiftId) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where shift_id = $shift_id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$shift_id", "" + shiftId);
        ResultSet rs = executeQuery(selectStat);
        List<TimeShiftDetailDTOInter> uDTOL = new ArrayList<TimeShiftDetailDTOInter>();
        while (rs.next()) {
            TimeShiftDetailDTOInter uDTO = DTOFactory.createTimeShiftDetailDTO();
            uDTO.setDayNo(rs.getInt("day_no"));
            uDTO.setShiftId(rs.getInt("shift_id"));
            uDTO.setTimeFrom(rs.getTime("time_from"));
            uDTO.setTimeTo(rs.getTime("time_to"));
            uDTO.setWorking(rs.getInt("working"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findShiftDay(int shiftId) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where shift_id = $shift_id and DAY_NO = TO_NUMBER(TO_CHAR(SYSDATE,'D'))+1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$shift_id", "" + shiftId);
        ResultSet rs = executeQuery(selectStat);
        TimeShiftDetailDTOInter uDTO = DTOFactory.createTimeShiftDetailDTO();
        while (rs.next()) {
            uDTO.setDayNo(rs.getInt("day_no"));
            uDTO.setShiftId(rs.getInt("shift_id"));
            uDTO.setTimeFrom(rs.getTime("time_from"));
            uDTO.setTimeTo(rs.getTime("time_to"));
            uDTO.setWorking(rs.getInt("working"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public static void main(String[] args) throws Throwable {
        TimeShiftDetailDAOInter timeShiftDetailDAOInter = DAOFactory.createTimeShiftDetailDAO(null);
        TimeShiftDetailDTOInter shiftDetailDTOInter = (TimeShiftDetailDTOInter) timeShiftDetailDAOInter.findShiftDay(8);
     
    }
}
