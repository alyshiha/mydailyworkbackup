/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.TotalWithDrawalsDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmsTotalRepDAO extends BaseDAO implements AtmsTotalRepDAOInter {

    protected AtmsTotalRepDAO() {
        super();
    }
public Object findTotalswithdrawalsexcelReport(String dateFrom, String dateTo, int atmGroupInt) throws Throwable {
        super.preSelect();

        String selectStatment = "select"
                + " sum(amount*amount_type) transaction, atm_application_id,atm_id,currency"
                + " from all_transactions"
                + " where transaction_date between to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and ((ATM_ID in (select id from atm_machine where atm_group in (select id from  (select * from atm_group l order BY l.parent_id,name) start with id = $P{AtmGroupInt}  connect by prior id = parent_id))) or $P{AtmGroupInt} = 0)"
                + " and record_type = 2"
                + " and matching_type = (select id from matching_type where active = 1 and (type1 = 2 or type2=2) and rownum = 1)"
                + " group by atm_application_id,atm_id,currency"
                + " order by 1 desc";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DatTo}", dateTo);
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        ResultSet rs = executeQueryReport(selectStatment);
        List<TotalWithDrawalsDTOInter> Records = new ArrayList<TotalWithDrawalsDTOInter>();
        while (rs.next()) {
            TotalWithDrawalsDTOInter record = DTOFactory.createTotalWithDrawalsDTO();
            record.setAtm_application_id(rs.getString("atm_application_id"));
            record.setAtm_id(rs.getInt("atm_id"));
            record.setCurrency(rs.getString("currency"));
            record.setTransaction(rs.getInt("transaction"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }
    public Object findReport(String dateFrom, String dateTo, int atmGroupInt) throws Throwable {
        super.preSelect();
        String selectStatment = "select"
                + " sum(amount*amount_type) transaction#, atm_application_id,atm_id,currency"
                + " from all_transactions"
                + " where transaction_date between to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and ((ATM_ID in (select id from atm_machine where atm_group in (select id from  (select * from atm_group l order BY l.parent_id,name) start with id = $P{AtmGroupInt}  connect by prior id = parent_id))) or $P{AtmGroupInt} = 0)"
                + " and record_type = 2"
                + " and matching_type = (select id from matching_type where active = 1 and (type1 = 2 or type2=2) and rownum = 1)"
                + " group by atm_application_id,atm_id,currency"
                + " order by 1 desc";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DatTo}", dateTo);
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        ResultSet rs = executeQueryReport(selectStatment);
        super.postSelect();
        return rs;
    }
}
