/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.AtmFileDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.DuplicateTransactionsDAOInter;
import com.ev.AtmBingo.bus.dto.AtmFileDTOInter;
import com.ev.AtmBingo.bus.dto.DuplicateTransactionsDTOInter;
import java.io.Serializable;
import java.util.List;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class AtmFileLogBO extends BaseBO implements AtmFileLogBOInter,Serializable{

    protected AtmFileLogBO() {
        super();
    }

    public Object editeAtmFile(Object obj, int operation) throws Throwable{
        return null;
    }
    
    public Object getDuplicationTransaction(AtmFileDTOInter afDTO)throws Throwable{
        DuplicateTransactionsDAOInter dtDAO = DAOFactory.createDuplicateTransactionsDAO(null);
        List<DuplicateTransactionsDTOInter> dtDTOL = (List<DuplicateTransactionsDTOInter>)dtDAO.findAll(afDTO);
        return dtDTOL;
    }

    public Object getAtmFile( Date loadingfrom,Date loadingto,int FileType)throws Throwable{
        AtmFileDAOInter afDAO = DAOFactory.createAtmFileDAO(null);
        List<AtmFileDTOInter> afDTOL = (List<AtmFileDTOInter>)afDAO.findAll(loadingfrom,loadingto,FileType);
        return afDTOL;
    }
  public Object getAtmFileByloadingdate(Date loadingfrom,Date loadingto)throws Throwable{
        AtmFileDAOInter afDAO = DAOFactory.createAtmFileDAO(null);
        List<AtmFileDTOInter> afDTOL = (List<AtmFileDTOInter>)afDAO.findByDate(loadingfrom,loadingto);
        return afDTOL;
    }
    


}
