/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.TransactionTypeDTOInter;
import com.ev.AtmBingo.bus.dto.TransactionTypeMasterDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TransactionTypeDAO extends BaseDAO implements TransactionTypeDAOInter {

    protected TransactionTypeDAO() {
        super();
        super.setTableName("transaction_type");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TransactionTypeDTOInter uDTO = (TransactionTypeDTOInter) obj[0];
        uDTO.setId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table  (id, name, description, type_category, type_master,reverse_flag) values ($id, '$name' , '$description', $type_category, $type_master,$reverseflag)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$name", "" + uDTO.getName());
        insertStat = insertStat.replace("$description", "" + uDTO.getDescription());
        insertStat = insertStat.replace("$type_category", "" + uDTO.getTypeCategory());
        insertStat = insertStat.replace("$type_master", "" + uDTO.getTypeMaster());
        Integer temp;
        if (uDTO.getReverseflag() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        insertStat = insertStat.replace("$reverseflag", "" + temp);
        super.executeUpdate(insertStat);
        String action = "Add a new trans. type detail with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TransactionTypeDTOInter uDTO = (TransactionTypeDTOInter) obj[0];
        String updateStat = "update $table set name = '$name', description = '$description', type_category = $type_category, "
                + "type_master = $type_master,reverse_flag=$reverseflag where id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$name", "" + uDTO.getName());
        updateStat = updateStat.replace("$description", "" + uDTO.getDescription());
        updateStat = updateStat.replace("$type_category", "" + uDTO.getTypeCategory());
        updateStat = updateStat.replace("$type_master", "" + uDTO.getTypeMaster());
        Integer temp;
        if (uDTO.getReverseflag() == true) {
            temp = 1;
        } else {
            temp = 2;
        }
        updateStat = updateStat.replace("$reverseflag", "" + temp);
        super.executeUpdate(updateStat);
        String action = "Update trans. type detail with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TransactionTypeDTOInter uDTO = (TransactionTypeDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        String action = "Delete trans. type detail with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        TransactionTypeDTOInter uDTO = DTOFactory.createTransactionTypeDTO();
        while (rs.next()) {
            uDTO.setDescription(rs.getString("description"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setName(rs.getString("name"));
            uDTO.setTypeCategory(rs.getInt("type_category"));
            uDTO.setTypeMaster(rs.getInt("type_master"));
            Integer temp = rs.getInt("reverse_flag");
            if (temp == 1) {
                uDTO.setReverseflag(Boolean.TRUE);
            } else {
                uDTO.setReverseflag(Boolean.FALSE);
            }
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<TransactionTypeDTOInter> uDTOL = new ArrayList<TransactionTypeDTOInter>();
        while (rs.next()) {
            TransactionTypeDTOInter uDTO = DTOFactory.createTransactionTypeDTO();
            uDTO.setDescription(rs.getString("description"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setName(rs.getString("name"));
            uDTO.setTypeCategory(rs.getInt("type_category"));
            uDTO.setTypeMaster(rs.getInt("type_master"));
            Integer temp = rs.getInt("reverse_flag");
            if (temp == 1) {
                uDTO.setReverseflag(Boolean.TRUE);
            } else {
                uDTO.setReverseflag(Boolean.FALSE);
            }
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByMaster(TransactionTypeMasterDTOInter ttmDTO) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where type_master = $master";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$master", "" + ttmDTO.getId());
        ResultSet rs = executeQuery(selectStat);
        List<TransactionTypeDTOInter> uDTOL = new ArrayList<TransactionTypeDTOInter>();
        while (rs.next()) {
            TransactionTypeDTOInter uDTO = DTOFactory.createTransactionTypeDTO();
            uDTO.setDescription(rs.getString("description"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setName(rs.getString("name"));
            uDTO.setTypeCategory(rs.getInt("type_category"));
            uDTO.setTypeMaster(rs.getInt("type_master"));
            Integer temp = rs.getInt("reverse_flag");
            if (temp == 1) {
                uDTO.setReverseflag(Boolean.TRUE);
            } else {
                uDTO.setReverseflag(Boolean.FALSE);
            }
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
