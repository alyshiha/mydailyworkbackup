/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.MenuLabelDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileDTOInter;
import com.ev.AtmBingo.bus.dto.ProfileMenuDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ProfileMenuDAO extends BaseDAO implements ProfileMenuDAOInter {

    protected ProfileMenuDAO() {
        super();
        super.setTableName("profile_menu");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        ProfileMenuDTOInter pDTO = (ProfileMenuDTOInter) obj[0];
        String insertStat = "insert into $table values ($profile_id , $menu_id)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$profile_id", "" + pDTO.getProfileId());
        insertStat = insertStat.replace("$menu_id", "" + pDTO.getMenuId());
        super.executeUpdate(insertStat);
        ProfileDAOInter pDAO = DAOFactory.createProfileDAO(null);
        MenuLabelDAOInter mlDAO = DAOFactory.createMenuLabelDAO(null);

        ProfileDTOInter ppDTO = (ProfileDTOInter) pDAO.find(pDTO.getProfileId());
        MenuLabelDTOInter mlDTO = (MenuLabelDTOInter) mlDAO.find(pDTO.getMenuId());
        String action = "Assign " + mlDTO.getEnglishLabel() + " Screen to " + ppDTO.getProfileName() + " profile";
        super.postUpdate(action, false);
        return null;
    }

    public Object deleteByProfileId(Object... obj) throws Throwable {
        super.preUpdate();
        ProfileMenuDTOInter pDTO = (ProfileMenuDTOInter) obj[0];
        String deleteStat = "delete from $table where profile_id = $profile_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$profile_id", "" + pDTO.getProfileId());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object deleteByMenuId(Object... obj) throws Throwable {
        super.preUpdate();
        ProfileMenuDTOInter pDTO = (ProfileMenuDTOInter) obj[0];
        String deleteStat = "delete from $table where menu_id = $menu_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$menu_id", "" + pDTO.getMenuId());
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object deleteByMenuIdAndProfileId(Object... obj) throws Throwable {
        super.preUpdate();
        ProfileMenuDTOInter pDTO = (ProfileMenuDTOInter) obj[0];
        String deleteStat = "delete from $table where menu_id = $menu_id and profile_id = $profile_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$menu_id", "" + pDTO.getMenuId());
        deleteStat = deleteStat.replace("$profile_id", "" + pDTO.getProfileId());
        super.executeUpdate(deleteStat);
        ProfileDAOInter pDAO = DAOFactory.createProfileDAO(null);
        MenuLabelDAOInter mlDAO = DAOFactory.createMenuLabelDAO(null);

        ProfileDTOInter ppDTO = (ProfileDTOInter) pDAO.find(pDTO.getProfileId());
        MenuLabelDTOInter mlDTO = (MenuLabelDTOInter) mlDAO.find(pDTO.getMenuId());
        String action = "Unassign " + mlDTO.getEnglishLabel() + " Screen to " + ppDTO.getProfileName() + " profile";
        super.postUpdate(action, false);
        return null;
    }

    public Object findAll(Object... obj) throws Throwable {
        preUpdate();
        postUpdate();
        return null;
    }

    public Object findByProfileId(int profileId) throws Throwable {
        preSelect();
        String selectStat = "select * from $table where profile_id = $profileId";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$profileId", "" + profileId);
        ResultSet rs = executeQuery(selectStat);
        List<ProfileMenuDTOInter> pmDTOL = new ArrayList<ProfileMenuDTOInter>();
        while (rs.next()) {
            ProfileMenuDTOInter pmDTO = DTOFactory.createProfileMenuDTO();
            pmDTO.setMenuId(rs.getInt("menu_id"));
            pmDTO.setProfileId(rs.getInt("profile_id"));
            pmDTOL.add(pmDTO);
        }
        postSelect(rs);
        return pmDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        Object obj1 = super.preSelect();
        super.postSelect(obj1);
        return null;
    }
}
