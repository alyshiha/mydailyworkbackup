/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.BlockReasonDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class BlockReasonDAO extends BaseDAO implements BlockReasonDAOInter {

    protected BlockReasonDAO() {
        super();
        super.setTableName("block_reasons");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        BlockReasonDTOInter uDTO = (BlockReasonDTOInter) obj[0];
        uDTO.setReasonId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ($reason_id, '$reason_name' , '$reason_a_name')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$reason_id", "" + uDTO.getReasonId());
        insertStat = insertStat.replace("$reason_name", "" + uDTO.getReasonName());
        insertStat = insertStat.replace("$reason_a_name", "" + uDTO.getReasonAName());
        super.executeUpdate(insertStat);
        super.postUpdate("Add " + uDTO.getReasonName() + " to block reasons", false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        BlockReasonDTOInter uDTO = (BlockReasonDTOInter) obj[0];
        String updateStat = "update $table set reason_name = '$reason_name', reason_a_name = '$reason_a_name'"
                + " where reason_id = $reason_id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$reason_id", "" + uDTO.getReasonId());
        updateStat = updateStat.replace("$reason_name", "" + uDTO.getReasonName());
        updateStat = updateStat.replace("$reason_a_name", "" + uDTO.getReasonAName());
        super.executeUpdate(updateStat);
        super.postUpdate("Update " + uDTO.getReasonName() + " in block reasons", false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        BlockReasonDTOInter uDTO = (BlockReasonDTOInter) obj[0];
        String deleteStat = "delete from $table where reason_id = $reason_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$reason_id", "" + uDTO.getReasonId());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete " + uDTO.getReasonName() + " from block reasons", false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where reason_id = $reason_id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$reason_id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        BlockReasonDTOInter uDTO = DTOFactory.createBlockReasonDTO();
        while (rs.next()) {
            uDTO.setReasonAName(rs.getString("reason_a_name"));
            uDTO.setReasonId(rs.getInt("reason_id"));
            uDTO.setReasonName(rs.getString("reason_name"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by reason_name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<BlockReasonDTOInter> uDTOL = new ArrayList<BlockReasonDTOInter>();
        while (rs.next()) {
            BlockReasonDTOInter uDTO = DTOFactory.createBlockReasonDTO();
            uDTO.setReasonAName(rs.getString("reason_a_name"));
            uDTO.setReasonId(rs.getInt("reason_id"));
            uDTO.setReasonName(rs.getString("reason_name"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
