/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class swglDTO implements swglDTOInter, Serializable {
    private Integer switchvalue,glvalue,different;

    @Override
    public Integer getDifferent() {
        return different;
    }

    @Override
    public void setDifferent(Integer different) {
        this.different = different;
    }

    @Override
    public Integer getGlvalue() {
        return glvalue;
    }

    @Override
    public void setGlvalue(Integer glvalue) {
        this.glvalue = glvalue;
    }

    @Override
    public Integer getSwitchvalue() {
        return switchvalue;
    }

    @Override
    public void setSwitchvalue(Integer switchvalue) {
        this.switchvalue = switchvalue;
    }
    
}
