/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ReplanishmentTempletesDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ReplanishmentTempletesDAO extends BaseDAO implements ReplanishmentTempletesDAOInter {

    protected ReplanishmentTempletesDAO() {
        super();
        super.setTableName("replanishment_templetes");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        ReplanishmentTempletesDTOInter uDTO = (ReplanishmentTempletesDTOInter) obj[0];
        uDTO.setId(super.generateSequence("REPLANISHMENT_TEMP"));
        String insertStat = "insert into $table values ($id ,'$name', '$starting_string' , $starting_position ,'$ending_string' , $ending_position "
                + " , '$atm_id_string' , $atm_id_position , $atm_id_lenght , '$adding_date_starting_string' , "
                + "  $adding_date_line , $adding_date_position , $adding_date_lenght , '$adding_date_format'  , '$remaining_date_starting_string' , "
                + " $remaining_date_line , $remaining_date_position , $remaining_date_lenght , '$remaining_date_format'  , "
                + "  $atm_string_position , $atm_machine_type,'$cardstakenstartingstring',$cardstakenline,$cardstakenposition,$cardstakenlenght)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$cardstakenlenght", "" + uDTO.getCardtakenlength());
        insertStat = insertStat.replace("$cardstakenposition", "" + uDTO.getCardtakenposition());
        insertStat = insertStat.replace("$cardstakenline", "" + uDTO.getCardtakenline());
        insertStat = insertStat.replace("$cardstakenstartingstring", "" + uDTO.getCardtakenstartingstring());
        insertStat = insertStat.replace("'$name'", "" + (uDTO.getName() == null ? null : "'" + uDTO.getName() + "'"));
        insertStat = insertStat.replace("'$starting_string'", "" + (uDTO.getStartingString() == null ? null : "'" + uDTO.getStartingString() + "'"));
        insertStat = insertStat.replace("$starting_position", "" + uDTO.getStartingPosition());
        insertStat = insertStat.replace("'$ending_string'", "" + (uDTO.getEndingString() == null ? null : "'" + uDTO.getEndingString() + "'"));
        insertStat = insertStat.replace("$ending_position", "" + uDTO.getEndingPosition());
        insertStat = insertStat.replace("'$atm_id_string'", "" + (uDTO.getAtmIdString() == null ? null : "'" + uDTO.getAtmIdString() + "'"));
        insertStat = insertStat.replace("$atm_id_position", "" + uDTO.getAtmIdPosition());
        insertStat = insertStat.replace("$atm_id_lenght", "" + uDTO.getAtmIdLenght());
        insertStat = insertStat.replace("'$adding_date_starting_string'", "" + (uDTO.getAddingDateStartingString() == null ? null : "'" + uDTO.getAddingDateStartingString() + "'"));
        insertStat = insertStat.replace("$adding_date_line", "" + uDTO.getAddingDateLine());
        insertStat = insertStat.replace("$adding_date_position", "" + uDTO.getAddingDatePosition());
        insertStat = insertStat.replace("$adding_date_lenght", "" + uDTO.getAddingDateLenght());
        insertStat = insertStat.replace("'$adding_date_format'", "" + (uDTO.getAddingDateFormat() == null ? null : "'" + uDTO.getAddingDateFormat() + "'"));
        insertStat = insertStat.replace("'$remaining_date_starting_string'", "" + (uDTO.getRemainingDateStartingString() == null ? null : "'" + uDTO.getRemainingDateStartingString() + "'"));
        insertStat = insertStat.replace("$remaining_date_line", "" + uDTO.getRemainingDateLine());
        insertStat = insertStat.replace("$remaining_date_position", "" + uDTO.getRemainingDatePosition());
        insertStat = insertStat.replace("$remaining_date_lenght", "" + uDTO.getRemainingDateLenght());
        insertStat = insertStat.replace("'$remaining_date_format'", "" + (uDTO.getRemainingDateFormat() == null ? null : "'" + uDTO.getRemainingDateFormat() + "'"));
        insertStat = insertStat.replace("$atm_string_position", "" + uDTO.getAtmStringPosition());
        insertStat = insertStat.replace("$atm_machine_type", "" + uDTO.getAtmMachineType());
        super.executeUpdate(insertStat);
        String action = "Add a new replenishment template with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        ReplanishmentTempletesDTOInter uDTO = (ReplanishmentTempletesDTOInter) obj[0];
        String updateStat = "update $table set name = '$name' , starting_string = '$starting_string' , "
                + " ending_string = '$ending_string' , starting_position = $starting_position , ending_position = $ending_position , atm_id_string = '$atm_id_string', "
                + " atm_id_position = $atm_id_position , atm_id_length = $atm_id_lenght , adding_date_starting_string = '$adding_date_starting_string' ,"
                + " adding_date_line = $adding_date_line , adding_date_position = $adding_date_position , adding_date_lenght= $adding_date_lenght ,adding_date_format = '$adding_date_format', "
                + " remaining_date_starting_string = '$remaining_date_starting_string' , remaining_date_line = $remaining_date_line , "
                + " remaining_date_position = $remaining_date_position , remaining_date_lenght = $remaining_date_lenght , "
                + " remaining_date_format = '$remaining_date_format' , atm_string_position = $atm_string_position , "
                + " atm_machine_type = $atm_machine_type ,cards_taken_starting_string = '$cardstakenstartingstring',"
                + " cards_taken_line = $cardstakenline, "
                + "cards_taken_position = $cardstakenposition , cards_taken_lenght = $cardstakenlenght where id = $id ";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("'$name'", "" + (uDTO.getName() == null ? null : "'" + uDTO.getName() + "'"));
        updateStat = updateStat.replace("$cardstakenstartingstring", "" + uDTO.getCardtakenstartingstring());
        updateStat = updateStat.replace("$cardstakenline", "" + uDTO.getCardtakenline());
        updateStat = updateStat.replace("$cardstakenposition", "" + uDTO.getCardtakenposition());
        updateStat = updateStat.replace("$cardstakenlenght", "" + uDTO.getCardtakenlength());
        updateStat = updateStat.replace("'$starting_string'", "" + (uDTO.getName() == null ? null : "'" + uDTO.getStartingString() + "'"));
        updateStat = updateStat.replace("$starting_position", "" + uDTO.getStartingPosition());
        updateStat = updateStat.replace("'$ending_string'", "" + (uDTO.getName() == null ? null : "'" + uDTO.getEndingString() + "'"));
        updateStat = updateStat.replace("$ending_position", "" + uDTO.getEndingPosition());
        updateStat = updateStat.replace("'$atm_id_string'", "" + (uDTO.getName() == null ? null : "'" + uDTO.getAtmIdString() + "'"));
        updateStat = updateStat.replace("$atm_id_position", "" + uDTO.getAtmIdPosition());
        updateStat = updateStat.replace("$atm_id_lenght", "" + uDTO.getAtmIdLenght());
        updateStat = updateStat.replace("'$adding_date_starting_string'", "" + (uDTO.getName() == null ? null : "'" + uDTO.getAddingDateStartingString() + "'"));
        updateStat = updateStat.replace("$adding_date_line", "" + uDTO.getAddingDateLine());
        updateStat = updateStat.replace("$adding_date_position", "" + uDTO.getAddingDatePosition());
        updateStat = updateStat.replace("$adding_date_lenght", "" + uDTO.getAddingDateLenght());
        updateStat = updateStat.replace("$adding_date_format", "" + uDTO.getAddingDateFormat());
        updateStat = updateStat.replace("'$remaining_date_starting_string'", "" + (uDTO.getName() == null ? null : "'" + uDTO.getRemainingDateStartingString() + "'"));
        updateStat = updateStat.replace("$remaining_date_line", "" + uDTO.getRemainingDateLine());
        updateStat = updateStat.replace("$remaining_date_position", "" + uDTO.getRemainingDatePosition());
        updateStat = updateStat.replace("$remaining_date_lenght", "" + uDTO.getRemainingDateLenght());
        updateStat = updateStat.replace("$remaining_date_format", "" + uDTO.getRemainingDateFormat());
        updateStat = updateStat.replace("$atm_string_position", "" + uDTO.getAtmStringPosition());
        updateStat = updateStat.replace("$atm_machine_type", "" + uDTO.getAtmMachineType());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(updateStat);
        String action = "Update replenishment template with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        ReplanishmentTempletesDTOInter uDTO = (ReplanishmentTempletesDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        String action = "Delete replenishment template with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object find(int id) throws Throwable {
        super.preSelect();
        String selectStat = "select * from $table where id = $id order by name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        ResultSet rs = executeQuery(selectStat);
        ReplanishmentTempletesDTOInter uDTO = DTOFactory.createReplanishmentTempletesDTO();
        while (rs.next()) {
            uDTO.setAddingDateFormat(rs.getString("adding_date_format"));
            uDTO.setAddingDateLenght(rs.getBigDecimal("adding_date_lenght") == null ? null : rs.getInt("adding_date_lenght"));
            uDTO.setAddingDateLine(rs.getBigDecimal("adding_date_line") == null ? null : rs.getInt("adding_date_line"));
            uDTO.setAddingDatePosition(rs.getBigDecimal("adding_date_position") == null ? null : rs.getInt("adding_date_position"));
            uDTO.setAddingDateStartingString(rs.getString("adding_date_starting_string"));
            uDTO.setAtmIdLenght(rs.getBigDecimal("atm_id_length") == null ? null : rs.getInt("atm_id_length"));
            uDTO.setAtmIdPosition(rs.getBigDecimal("atm_id_position") == null ? null : rs.getInt("atm_id_position"));
            uDTO.setAtmIdString(rs.getString("atm_id_string"));
            uDTO.setAtmMachineType(rs.getBigDecimal("atm_machine_type") == null ? null : rs.getInt("atm_machine_type"));
            uDTO.setAtmStringPosition(rs.getBigDecimal("atm_string_position") == null ? null : rs.getInt("atm_string_position"));
            uDTO.setEndingPosition(rs.getBigDecimal("ending_position") == null ? null : rs.getInt("ending_position"));
            uDTO.setEndingString(rs.getString("ending_string"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setName(rs.getString("name"));
            uDTO.setRemainingDateFormat(rs.getString("remaining_date_format"));
            uDTO.setRemainingDateLenght(rs.getBigDecimal("remaining_date_lenght") == null ? null : rs.getInt("remaining_date_lenght"));
            uDTO.setRemainingDateLine(rs.getBigDecimal("remaining_date_line") == null ? null : rs.getInt("remaining_date_line"));
            uDTO.setRemainingDatePosition(rs.getBigDecimal("remaining_date_position") == null ? null : rs.getInt("remaining_date_position"));
            uDTO.setRemainingDateStartingString(rs.getString("remaining_date_starting_string"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position") == null ? null : rs.getInt("starting_position"));
            uDTO.setStartingString(rs.getString("starting_string"));
            uDTO.setCardtakenlength(rs.getInt("cards_taken_lenght"));
            uDTO.setCardtakenline(rs.getInt("cards_taken_line"));
            uDTO.setCardtakenposition(rs.getInt("cards_taken_position"));
            uDTO.setCardtakenstartingstring(rs.getString("cards_taken_starting_string"));

        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "select * from $table  order by name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<ReplanishmentTempletesDTOInter> uDTOL = new ArrayList<ReplanishmentTempletesDTOInter>();
        while (rs.next()) {
            ReplanishmentTempletesDTOInter uDTO = DTOFactory.createReplanishmentTempletesDTO();
            uDTO.setAddingDateFormat(rs.getString("adding_date_format"));
            uDTO.setAddingDateLenght(rs.getBigDecimal("adding_date_lenght") == null ? null : rs.getInt("adding_date_lenght"));
            uDTO.setAddingDateLine(rs.getBigDecimal("adding_date_line") == null ? null : rs.getInt("adding_date_line"));
            uDTO.setAddingDatePosition(rs.getBigDecimal("adding_date_position") == null ? null : rs.getInt("adding_date_position"));
            uDTO.setAddingDateStartingString(rs.getString("adding_date_starting_string"));
            uDTO.setAtmIdLenght(rs.getBigDecimal("atm_id_length") == null ? null : rs.getInt("atm_id_length"));
            uDTO.setAtmIdPosition(rs.getBigDecimal("atm_id_position") == null ? null : rs.getInt("atm_id_position"));
            uDTO.setAtmIdString(rs.getString("atm_id_string"));
            uDTO.setAtmMachineType(rs.getBigDecimal("atm_machine_type") == null ? null : rs.getInt("atm_machine_type"));
            uDTO.setAtmStringPosition(rs.getBigDecimal("atm_string_position") == null ? null : rs.getInt("atm_string_position"));
            uDTO.setEndingPosition(rs.getBigDecimal("ending_position") == null ? null : rs.getInt("ending_position"));
            uDTO.setEndingString(rs.getString("ending_string"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setName(rs.getString("name"));
            uDTO.setRemainingDateFormat(rs.getString("remaining_date_format"));
            uDTO.setRemainingDateLenght(rs.getBigDecimal("remaining_date_lenght") == null ? null : rs.getInt("remaining_date_lenght"));
            uDTO.setRemainingDateLine(rs.getBigDecimal("remaining_date_line") == null ? null : rs.getInt("remaining_date_line"));
            uDTO.setRemainingDatePosition(rs.getBigDecimal("remaining_date_position") == null ? null : rs.getInt("remaining_date_position"));
            uDTO.setRemainingDateStartingString(rs.getString("remaining_date_starting_string"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position") == null ? null : rs.getInt("starting_position"));
            uDTO.setStartingString(rs.getString("starting_string"));
            uDTO.setCardtakenlength(rs.getInt("cards_taken_lenght"));
            uDTO.setCardtakenline(rs.getInt("cards_taken_line"));
            uDTO.setCardtakenposition(rs.getInt("cards_taken_position"));
            uDTO.setCardtakenstartingstring(rs.getString("cards_taken_starting_string"));

            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {

        return null;
    }
}
