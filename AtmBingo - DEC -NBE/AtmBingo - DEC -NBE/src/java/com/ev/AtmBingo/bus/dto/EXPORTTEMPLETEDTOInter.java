/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface EXPORTTEMPLETEDTOInter  extends Serializable {

    int getActive();

    String getSeparator();
 boolean  getBoolActive();

    int getTemplateid();

    String getTemplatename();

    void setActive(int active);

    void setSeparator(String separator);

    void setTemplateid(int templateid);

    void setTemplatename(String templatename);

}
