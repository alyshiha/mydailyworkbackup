/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface HostFileHeaderDTOInter extends Serializable{

    int getColumnId();

    Integer getDataType();

    String getFormat();

    String getFormat2();

    int getId();

    Integer getLength();

    Integer getPosition();

    int getTemplate();

    void setColumnId(int columnId);

    void setDataType(Integer dataType);

    void setFormat(String format);

    void setFormat2(String format2);

    void setId(int id);

    void setLength(Integer length);

    void setPosition(Integer position);

    void setTemplate(int template);

}
