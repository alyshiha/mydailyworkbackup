/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.TransactionResponseMasterDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TransactionResponseMasterDAO extends BaseDAO implements TransactionResponseMasterDAOInter {

    protected TransactionResponseMasterDAO() {
        super();
        super.setTableName("TRANSACTION_RESPONSE_MASTER");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TransactionResponseMasterDTOInter uDTO = (TransactionResponseMasterDTOInter) obj[0];
        uDTO.setId(super.generateSequence("transaction_response_mas"));
        String insertStat = "insert into $table values ($id, '$name')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$name", "" + uDTO.getName());
        super.executeUpdate(insertStat);
        String action = "Add a new response master with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TransactionResponseMasterDTOInter uDTO = (TransactionResponseMasterDTOInter) obj[0];
        String updateStat = "update $table set name = '$name' where id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$name", "" + uDTO.getName());
        super.executeUpdate(updateStat);
        String action = "update response master with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        TransactionResponseMasterDTOInter uDTO = (TransactionResponseMasterDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        String action = "Delete response master with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        TransactionResponseMasterDTOInter uDTO = DTOFactory.createTransactionResponseMasterDTO();
        while (rs.next()) {
            uDTO.setName(rs.getString("name"));
            uDTO.setId(rs.getInt("id"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<TransactionResponseMasterDTOInter> uDTOL = new ArrayList<TransactionResponseMasterDTOInter>();
        while (rs.next()) {
            TransactionResponseMasterDTOInter uDTO = DTOFactory.createTransactionResponseMasterDTO();
            uDTO.setName(rs.getString("name"));
            uDTO.setId(rs.getInt("id"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
