/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmGroupDAO extends BaseDAO implements AtmGroupDAOInter {

    protected AtmGroupDAO() {
        super();
        super.setTableName("atm_group");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmGroupDTOInter uDTO = (AtmGroupDTOInter) obj[0];
        uDTO.setId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ($id, '$name' , '$description', $parent_id)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        insertStat = insertStat.replace("$name", "" + uDTO.getName());
        insertStat = insertStat.replace("$description", "" + uDTO.getDescription());
        insertStat = insertStat.replace("$parent_id", "" + uDTO.getParentId());
        super.executeUpdate(insertStat);
        super.postUpdate("Add " + uDTO.getName() + " ATM group", false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmGroupDTOInter uDTO = (AtmGroupDTOInter) obj[0];
        String updateStat = "update $table set name = '$name', description = '$description', parent_id = $parent_id where id = $id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$name", "" + uDTO.getName());
        updateStat = updateStat.replace("$description", "" + uDTO.getDescription());
        updateStat = updateStat.replace("$parent_id", "" + uDTO.getParentId());
        super.executeUpdate(updateStat);
        super.postUpdate("Update " + uDTO.getName() + " ATM group", false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmGroupDTOInter uDTO = (AtmGroupDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete " + uDTO.getName() + " ATM group", false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        AtmGroupDTOInter uDTO = DTOFactory.createAtmGroupDTO();
        while (rs.next()) {
            uDTO.setName(rs.getString("name"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setDescription(rs.getString("description"));
            uDTO.setParentId(rs.getInt("parent_id"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by name";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<AtmGroupDTOInter> uDTOL = new ArrayList<AtmGroupDTOInter>();
        while (rs.next()) {
            AtmGroupDTOInter uDTO = DTOFactory.createAtmGroupDTO();
            uDTO.setName(rs.getString("name"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setDescription(rs.getString("description"));
            uDTO.setParentId(rs.getInt("parent_id"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByName(String name) throws Throwable {

        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where name = '$name'";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$name", "" + name);
        ResultSet rs = executeQuery(selectStat);
        AtmGroupDTOInter uDTO = DTOFactory.createAtmGroupDTO();
        while (rs.next()) {
            uDTO.setName(rs.getString("name"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setDescription(rs.getString("description"));
            uDTO.setParentId(rs.getInt("parent_id"));
        }
        super.postSelect(rs);
        return uDTO;

    }

    public Object findTree() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select  level , name,parent_id, id,description from"
                + " (select * from atm_group l"
                + " order BY l.parent_id,l.name)"
                + "start with parent_id is null connect by prior id = parent_id ORDER SIBLINGS BY name";
        ResultSet rs = executeQuery(selectStat);
        List<AtmGroupDTOInter> uDTOL = new ArrayList<AtmGroupDTOInter>();
        while (rs.next()) {
            AtmGroupDTOInter uDTO = DTOFactory.createAtmGroupDTO();
            uDTO.setName(rs.getString("name"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setDescription(rs.getString("description"));
            uDTO.setParentId(rs.getInt("parent_id"));
            uDTO.setLev(rs.getInt("level"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findChildren(AtmGroupDTOInter parent) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from atm_group l where l.parent_id = $parent"
                + " order BY l.parent_id,l.name";

        selectStat = selectStat.replace("$parent", "" + parent.getId());
        ResultSet rs = executeQuery(selectStat);
        List<AtmGroupDTOInter> uDTOL = new ArrayList<AtmGroupDTOInter>();
        while (rs.next()) {
            AtmGroupDTOInter uDTO = DTOFactory.createAtmGroupDTO();
            uDTO.setName(rs.getString("name"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setDescription(rs.getString("description"));
            uDTO.setParentId(rs.getInt("parent_id"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public static void main(String[] args) throws Throwable {
    }
}
