package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.useratmbranchDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.usersbranchDAOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.UserProfileDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.AtmBingo.bus.dto.useratmbranchDTOInter;
import java.io.Serializable;
import org.primefaces.model.DualListModel;

public class useratmbranchBo extends BaseBO implements useratmbranchBOInter,Serializable {

    protected useratmbranchBo() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        useratmbranchDTOInter RecordToInsert = (useratmbranchDTOInter) obj[0];
        useratmbranchDAOInter engin = DAOFactory.createuseratmbranchDAO();
        engin.insertrecord(RecordToInsert);
        return "Insert Done";
    }

    public Object getPickList(int userId,String sourcelikevalue,String targetlikevalue) throws Throwable {
        useratmbranchDAOInter uDAO = DAOFactory.createuseratmbranchDAO();
        List<AtmMachineDTOInter> hasatm = (List<AtmMachineDTOInter>) uDAO.findassign(userId,targetlikevalue);
        List<AtmMachineDTOInter> hasNotatm = (List<AtmMachineDTOInter>) uDAO.findnotassign(userId,sourcelikevalue);

        DualListModel pickList = new DualListModel();
        List<String> source = new ArrayList<String>();
        List<String> target = new ArrayList<String>();

        for (AtmMachineDTOInter u : hasatm) {
            target.add(u.getApplicationId());
        }
        for (AtmMachineDTOInter u : hasNotatm) {
            source.add(u.getApplicationId());
        }

        pickList.setSource(source);
        pickList.setTarget(target);

        return pickList;
    }

    public Object editUserProfile(List<String> target, int userid) throws Throwable {

        useratmbranchDAOInter upDAO = DAOFactory.createuseratmbranchDAO();
        usersbranchDAOInter uDAO = DAOFactory.createusersbranchDAO();
        List<UserProfileDTOInter> upDTOL = new ArrayList<UserProfileDTOInter>();

        for (String s : target) {
            AtmMachineDTOInter uDTO = (AtmMachineDTOInter) upDAO.findRecordGroupAll(s);
            UserProfileDTOInter upDTO = DTOFactory.createUserProfileDTO();
            upDTO.setProfileId(uDTO.getId());
            upDTO.setUserId(userid);
            upDTOL.add(upDTO);
        }
        upDAO.deletealluserrecord(userid);
        upDAO.save(upDTOL);

        return "Done";
    }

    public Object updaterecord(Object... obj) throws Throwable {
        useratmbranchDTOInter RecordToUpdate = (useratmbranchDTOInter) obj[0];
        useratmbranchDAOInter engin = DAOFactory.createuseratmbranchDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        useratmbranchDAOInter engin = DAOFactory.createuseratmbranchDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        useratmbranchDTOInter RecordToDelete = (useratmbranchDTOInter) obj[0];
        useratmbranchDAOInter engin = DAOFactory.createuseratmbranchDAO();
        engin.deleterecord(RecordToDelete);
        return "record has been deleted";
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<useratmbranchDTOInter> SearchRecords = (List<useratmbranchDTOInter>) obj[0];
        useratmbranchDAOInter engin = DAOFactory.createuseratmbranchDAO();
        List<useratmbranchDTOInter> AllRecords = (List<useratmbranchDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        useratmbranchDAOInter engin = DAOFactory.createuseratmbranchDAO();
        List<useratmbranchDTOInter> AllRecords = (List<useratmbranchDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        useratmbranchDTOInter SearchParameter = (useratmbranchDTOInter) obj[0];
        useratmbranchDAOInter engin = DAOFactory.createuseratmbranchDAO();
        useratmbranchDTOInter Record = (useratmbranchDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
