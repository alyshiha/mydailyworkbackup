package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.AtmBingo.base.util.DateFormatter;
import java.util.ArrayList;
import java.util.List;
import com.ev.AtmBingo.bus.dto.branchcodeDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class branchcodeDAO extends BaseDAO implements branchcodeDAOInter {

    protected branchcodeDAO() {
        super();
        super.setTableName("BRANCH_CODE");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        branchcodeDTOInter RecordToInsert = (branchcodeDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (ID,NAME) "
                + " values "
                + " ($id,'$name')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$name", "" + RecordToInsert.getname());
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To BRANCH_CODE", false);
        return null;
    }

    public Boolean ValidateNull(Object... obj) {
        branchcodeDTOInter RecordToInsert = (branchcodeDTOInter) obj[0];
        if (RecordToInsert.getid() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        branchcodeDTOInter RecordToUpdate = (branchcodeDTOInter) obj[0];
        String UpdateStat = "update $table set "
                + " (ID= $id,NAME= '$name') "
                + "  where  ID= $id";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$id", "" + RecordToUpdate.getid());
        UpdateStat = UpdateStat.replace("$name", "" + RecordToUpdate.getname());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table BRANCH_CODE", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        branchcodeDTOInter RecordToDelete = (branchcodeDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        deleteStat = deleteStat.replace("$name", "" + RecordToDelete.getname());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table BRANCH_CODE", false);
        return "";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table BRANCH_CODE", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        branchcodeDTOInter RecordToSelect = (branchcodeDTOInter) obj[0];
        String selectStat = "Select ID,NAME From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        branchcodeDTOInter SelectedRecord = DTOFactory.createbranchcodeDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        branchcodeDTOInter RecordToSelect = (branchcodeDTOInter) obj[0];
        String selectStat = "Select ID,NAME From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<branchcodeDTOInter> Records = new ArrayList<branchcodeDTOInter>();
        while (rs.next()) {
            branchcodeDTOInter SelectedRecord = DTOFactory.createbranchcodeDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,NAME From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<branchcodeDTOInter> Records = new ArrayList<branchcodeDTOInter>();
        while (rs.next()) {
            branchcodeDTOInter SelectedRecord = DTOFactory.createbranchcodeDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public void save(Object... obj) throws SQLException, Throwable {
        List<branchcodeDTOInter> entities = (List<branchcodeDTOInter>) obj[0];
        String insertStat = "Update BRANCH_CODE Set NAME = ? where ID = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
     connection  = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.size(); i++) {
                branchcodeDTOInter RecordtoInsert = entities.get(i);
                statement.setString(1, RecordtoInsert.getname());
                statement.setInt(2, RecordtoInsert.getid());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
    }
}
