/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ItemBingoDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ItemBingoDAO extends BaseDAO implements ItemBingoDAOInter {

    protected ItemBingoDAO() {
        super();
        super.setTableName("item_bingo");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        ItemBingoDTOInter iDTO = (ItemBingoDTOInter) obj[0];
        String insertStat = "insert into $table values ($item_id , '$item_name')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$item_id", "" + iDTO.getItemId());
        insertStat = insertStat.replace("$item_name", "" + iDTO.getItemName());
        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        ItemBingoDTOInter iDTO = (ItemBingoDTOInter) obj[0];
        String updateStat = "update $table set item_name = '$item_name' where item_id = $item_id ";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$profile_id", "" + iDTO.getItemId());
        updateStat = updateStat.replace("$profile_name", "" + iDTO.getItemName());
        super.postUpdate(null, true);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        ItemBingoDTOInter iDTO = (ItemBingoDTOInter) obj[0];
        String deleteStat = "delete from $table where profile_id = $profile_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$profile_id", "" + iDTO.getItemName());
        super.postUpdate(null, true);
        return null;
    }

    public Object findAll(Object... obj) throws Throwable {
        preSelect();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<ItemBingoDTOInter> ibDTOL = new ArrayList<ItemBingoDTOInter>();
        while (rs.next()) {
            ItemBingoDTOInter ibDTO = DTOFactory.createItemBingoDTO();
            ibDTO.setItemId(rs.getInt("item_id"));
            ibDTO.setItemName(rs.getString("item_name"));
            ibDTOL.add(ibDTO);
        }
        postSelect(rs);
        return ibDTOL;
    }

    public Object find(int id) throws Throwable {
        preSelect();
        String selectStat = "select * from $table where item_id = $itemId";
        selectStat = selectStat.replace("$table", super.getTableName());
        selectStat = selectStat.replace("$itemId", "" + id);
        ResultSet rs = executeQuery(selectStat);
        ItemBingoDTOInter ibDTO = DTOFactory.createItemBingoDTO();
        while (rs.next()) {

            ibDTO.setItemId(rs.getInt("item_id"));
            ibDTO.setItemName(rs.getString("item_name"));

        }
        postSelect(rs);
        return ibDTO;
    }

    public Object search(Object... obj) throws Throwable {
        Object obj1 = super.preSelect();
        super.postSelect(obj1);
        return null;
    }
}
