/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.bus.dao.DAOFactory;

import com.ev.AtmBingo.bus.dao.FilesToMoveDAOInter;
import com.ev.AtmBingo.bus.dao.JobsStatusDAOInter;

import com.ev.AtmBingo.bus.dto.DTOFactory;

import com.ev.AtmBingo.bus.dto.ErrorsMonitorDTOInter;
import com.ev.AtmBingo.bus.dto.FilesToMoveDTOInter;
import com.ev.AtmBingo.bus.dto.JobsStatusDTOInter;
import com.ev.AtmBingo.bus.dto.LoadingMonitorDTOInter;
import com.ev.AtmBingo.bus.dto.MatchingMonitorDTOInter;
import com.ev.AtmBingo.bus.dto.RunningGraphDTOInter;
import com.ev.AtmBingo.bus.dto.ValidationMonitorDTOInter;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class JobMonitorBO extends BaseBO implements JobMonitorBOInter,Serializable {

    protected JobMonitorBO() {
        super();
    }

    public Object getLoadingMonitor() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        LoadingMonitorDTOInter lmDTO = (LoadingMonitorDTOInter) ftmDAO.getLoadingMonitor();
        return lmDTO;
    }

    public Object getValidationMonitor() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        ValidationMonitorDTOInter lmDTO = (ValidationMonitorDTOInter) ftmDAO.getValidationMonitor();
        return lmDTO;
    }

    public Object getMatchingMonitor() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        MatchingMonitorDTOInter lmDTO = (MatchingMonitorDTOInter) ftmDAO.getMatchingMonitor();
        return lmDTO;
    }

    public Object getErrorMonitor() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        ErrorsMonitorDTOInter lmDTO = (ErrorsMonitorDTOInter) ftmDAO.getErrorsMonitor();
        return lmDTO;
    }

    public int getActiveFiles() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        int i = ftmDAO.findActiveFiles();
        return i;
    }

    public int getFilesToBuckup() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        int i = ftmDAO.findFilesToBuckup();
        return i;
    }

    public Object rematch() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        return ftmDAO.rematch();
    }

  /*  public Object getRunningGraph() throws Throwable {
        JobsStatusDAOInter jsDAO = DAOFactory.createJobsStatusDAO(null);
        List<JobsStatusDTOInter> jsDTOL = (List<JobsStatusDTOInter>) jsDAO.findAll();
        int countL = 0;
        int countJV = 0;
        int countSV = 0;
        int countHV = 0;
        int countM = 0;

        int countBL = 0;
        int countBJV = 0;
        int countBSV = 0;
        int countBHV = 0;
        int countBM = 0;

        int countDL = 0;
        int countDJV = 0;
        int countDSV = 0;
        int countDHV = 0;
        int countDM = 0;

        int countSL = 0;
        int countSJV = 0;
        int countSSV = 0;
        int countSHV = 0;
        int countSM = 0;

        List<RunningGraphDTOInter> rgDTOL = new ArrayList<RunningGraphDTOInter>();
        for (JobsStatusDTOInter j : jsDTOL) {
            int lId = j.getLoadingJobId();
            int jvId = j.getJournalValidationJobId();
            int svId = j.getSwitchValidationJobId();
            int hvId = j.getHostValidationJobId();
            int mId = j.getMatchingJobId();

            for (int i = 0; i < 5; i++) {
                if (i == 0) {

                    try {
                        Date d = (Date) jsDAO.findRunning(lId);
                        if (d != null) {
                            countL++;
                        } else {
                            countSL++;
                        }
                        try {
                            String s = (String) jsDAO.findBroken(lId);
                            if (s.equals("Y")) {
                                countBL++;
                            }
                        } catch (Throwable ex) {
                            countDL++;
                        }
                    } catch (Throwable throwable) {
                        countDL++;
                    }
                } else if (i == 1) {
                    try {
                        Date d = (Date) jsDAO.findRunning(jvId);
                        if (d != null) {
                            countJV++;
                        } else {
                            countSJV++;
                        }
                        try {
                            String s = (String) jsDAO.findBroken(jvId);
                            if (s.equals("Y")) {
                                countBJV++;
                            }
                        } catch (Throwable ex) {
                            countDJV++;
                        }
                    } catch (Throwable throwable) {
                        countDJV++;
                    }
                } else if (i == 2) {
                    try {
                        Date d = (Date) jsDAO.findRunning(svId);
                        if (d != null) {
                            countSV++;
                        } else {
                            countSSV++;
                        }
                        try {
                            String s = (String) jsDAO.findBroken(svId);
                            if (s.equals("Y")) {
                                countBSV++;
                            }
                        } catch (Throwable ex) {
                            countDSV++;
                        }
                    } catch (Throwable throwable) {
                        countDSV++;
                    }
                } else if (i == 3) {
                    try {
                        Date d = (Date) jsDAO.findRunning(hvId);
                        if (d != null) {
                            countHV++;
                        } else {
                            countSHV++;
                        }
                        try {
                            String s = (String) jsDAO.findBroken(hvId);
                            if (s.equals("Y")) {
                                countBHV++;
                            }
                        } catch (Throwable ex) {
                            countDHV++;
                        }
                    } catch (Throwable throwable) {
                        countDHV++;
                    }
                } else if (i == 4) {
                    try {
                        Date d = (Date) jsDAO.findRunning(mId);
                        if (d != null) {
                            countM++;
                        } else {
                            countSM++;
                        }
                        try {
                            String s = (String) jsDAO.findBroken(mId);
                            if (s.equals("Y")) {
                                countBM++;
                            }
                        } catch (Throwable ex) {
                            countDM++;
                        }
                    } catch (Throwable throwable) {
                        countDM++;
                    }
                }

            }

        }
        RunningGraphDTOInter graphL = DTOFactory.createRunningGraphDTO();
        RunningGraphDTOInter graphJV = DTOFactory.createRunningGraphDTO();
        RunningGraphDTOInter graphSV = DTOFactory.createRunningGraphDTO();
        RunningGraphDTOInter graphHV = DTOFactory.createRunningGraphDTO();
        RunningGraphDTOInter graphM = DTOFactory.createRunningGraphDTO();

        graphL.setCountDown(countDL);
        graphL.setCountRunning(countL);
        graphL.setCountStandBy(countSL);
        graphL.setCoutnBroken(countBL);
        graphL.setLabel("Loading");
        rgDTOL.add(graphL);

        graphJV.setCountDown(countDJV);
        graphJV.setCountRunning(countJV);
        graphJV.setCountStandBy(countSJV);
        graphJV.setCoutnBroken(countBJV);
        graphJV.setLabel("Journal Validation");
        rgDTOL.add(graphJV);

        graphSV.setCountDown(countDSV);
        graphSV.setCountRunning(countSV);
        graphSV.setCountStandBy(countSSV);
        graphSV.setCoutnBroken(countBSV);
        graphSV.setLabel("Switch Validation");
        rgDTOL.add(graphSV);

        graphHV.setCountDown(countDHV);
        graphHV.setCountRunning(countHV);
        graphHV.setCountStandBy(countSHV);
        graphHV.setCoutnBroken(countBHV);
        graphHV.setLabel("Host Validation");
        rgDTOL.add(graphHV);

        graphM.setCountDown(countDM);
        graphM.setCountRunning(countM);
        graphM.setCountStandBy(countSM);
        graphM.setCoutnBroken(countBM);
        graphM.setLabel("Matching");
        rgDTOL.add(graphM);

        return rgDTOL;
    }*/

    public Object getFilesToMove() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        List<FilesToMoveDTOInter> ftmDTOL = (List<FilesToMoveDTOInter>) ftmDAO.findAll();
        return ftmDTOL;
    }

    public void disableJob(boolean status, int process)throws Throwable{
        JobsStatusDAOInter jsDAO = DAOFactory.createJobsStatusDAO(null);
        jsDAO.disableJob(status, process);
    }

    public boolean getStatus(int process)throws Throwable{
        JobsStatusDAOInter jsDAO = DAOFactory.createJobsStatusDAO(null);
        List<JobsStatusDTOInter> jsDTOL = (List<JobsStatusDTOInter>) jsDAO.findAll();
        JobsStatusDTOInter jsDTO = jsDTOL.get(0);
        if(process == 1){
            if(jsDTO.getLoading() == 1){
                return false;
            }
            else{
                return true;
            }
        }
        else if(process == 2){
            if(jsDTO.getJournalValidation() == 1){
                return false;
            }
            else{
                return true;
            }
        }
        else if(process == 3){
            if(jsDTO.getSwitchValidation() == 1){
                return false;
            }
            else{
                return true;
            }
        }
        else if(process == 4){
            if(jsDTO.getHostValidation() == 1){
                return false;
            }
            else{
                return true;
            }
        }
        else if(process == 5){
            if(jsDTO.getMatching() == 1){
                return false;
            }
            else{
                return true;
            }
        }
        else{
            return false;
        }
    }
    public static void main(String[] args) throws Throwable {
    }
}
