/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.SwitchFileTemplateDTOInter;
import com.ev.AtmBingo.bus.dto.SwitchFileTemplateDetailDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class SwitchFileTemplateDetailDAO extends BaseDAO implements SwitchFileTemplateDetailDAOInter {

    protected SwitchFileTemplateDetailDAO() {
        super();
        super.setTableName("switch_file_template_detail");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SwitchFileTemplateDetailDTOInter uDTO = (SwitchFileTemplateDetailDTOInter) obj[0];
        uDTO.setId(super.generateSequence("switch_file_template_dtl"));
        String insertStat = "insert into $table values ($id, $template , $columnId, $position,$lineNumber, '$format', '$formt2',"
                + " $dataType, $length,  $loadWhenMapped, $lngthDir, $startPos,'$tagstring',$mandatory,$negativeflag,'$decimalpoint','$decimalpointposition')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$tagstring", "" + uDTO.getTagstring());
        Integer temp;
        if(uDTO.getMandatory() == true){temp=1;}else{temp=2;}
        insertStat = insertStat.replace("$mandatory", "" + temp);
        if(uDTO.getNegativeflag() == true){temp=1;}else{temp=2;}
        insertStat = insertStat.replace("$negativeflag", "" + temp);
        insertStat = insertStat.replace("$id", "" + uDTO.getId());
        if(uDTO.getDecimalpoint() == true){temp=1;}else{temp=2;}
        insertStat = insertStat.replace("$decimalpointposition", "" + uDTO.getDecimalpointposition());
        insertStat = insertStat.replace("$decimalpoint", "" + temp);
        


        insertStat = insertStat.replace("$template", "" + uDTO.getTwmplate());
        insertStat = insertStat.replace("$columnId", "" + uDTO.getColumnId());
        insertStat = insertStat.replace("$position", "" + uDTO.getPosition());
        insertStat = insertStat.replace("$format", "" + uDTO.getFormat());
        insertStat = insertStat.replace("$formt2", "" + uDTO.getFormat2());
        insertStat = insertStat.replace("$dataType", "" + uDTO.getDataType());
        insertStat = insertStat.replace("$length", "" + uDTO.getLength());
        insertStat = insertStat.replace("$lineNumber", "" + uDTO.getLineNumber());
        insertStat = insertStat.replace("$loadWhenMapped", "" + uDTO.getLoadWhenMapped());
        insertStat = insertStat.replace("$lngthDir", "" + uDTO.getLengthDir());
        insertStat = insertStat.replace("$startPos", "" + uDTO.getStartingPosition());
        super.executeUpdate(insertStat);
        SwitchFileTemplateDAOInter sftDAO = DAOFactory.createSwitchFileTemplateDAO(null);
        SwitchFileTemplateDTOInter sftDTO = (SwitchFileTemplateDTOInter) sftDAO.find(uDTO.getTwmplate());
        String action = "Add a new Detail template to Switch template's with name " + sftDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SwitchFileTemplateDetailDTOInter uDTO = (SwitchFileTemplateDetailDTOInter) obj[0];
        String updateStat = "update $table set template = $template, column_id = $columnId, position = $position"
                + ", format = '$format', format2 = '$formt2', data_type = $dataType, length = $length, line_number = $lineNumber"
                + ", load_when_mapped = $loadWhenMapped, length_dir = $lngthDir, starting_position = $startPos"
                 +" ,TAG_STRING = '$tagstring',MANDATORY = $mandatory ,NEGATIVE_AMOUNT_FLAG = $negativeflag ,ADD_DECIMAL='$decimalpoint',DECIMAL_POS='$decimalpointposition'"
                + " where id = $id";

        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$table", "" + super.getTableName());
         updateStat = updateStat.replace("$tagstring", "" + uDTO.getTagstring());
           Integer temp;
                if(uDTO.getDecimalpoint() == true){temp=1;}else{temp=2;}
        updateStat = updateStat.replace("$decimalpointposition", "" + uDTO.getDecimalpointposition());
           updateStat = updateStat.replace("$decimalpoint", "" + temp);
        

        if(uDTO.getMandatory() == true)
        {temp=1;}
        else
        {temp=2;}
        updateStat = updateStat.replace("$mandatory", "" + temp);
        if(uDTO.getNegativeflag() == true){temp=1;}else{temp=2;}
        updateStat = updateStat.replace("$negativeflag", "" + temp);
        updateStat = updateStat.replace("$id", "" + uDTO.getId());
        updateStat = updateStat.replace("$template", "" + uDTO.getTwmplate());
        updateStat = updateStat.replace("$columnId", "" + uDTO.getColumnId());
        updateStat = updateStat.replace("$position", "" + uDTO.getPosition());
        updateStat = updateStat.replace("$format", "" + uDTO.getFormat());
        updateStat = updateStat.replace("$formt2", "" + uDTO.getFormat2());
        updateStat = updateStat.replace("$dataType", "" + uDTO.getDataType());
        updateStat = updateStat.replace("$length", "" + uDTO.getLength());
        updateStat = updateStat.replace("$lineNumber", "" + uDTO.getLineNumber());
        updateStat = updateStat.replace("$loadWhenMapped", "" + uDTO.getLoadWhenMapped());
        updateStat = updateStat.replace("$lngthDir", "" + uDTO.getLengthDir());
        updateStat = updateStat.replace("$startPos", "" + uDTO.getStartingPosition());
        super.executeUpdate(updateStat);
        SwitchFileTemplateDAOInter sftDAO = DAOFactory.createSwitchFileTemplateDAO(null);
        SwitchFileTemplateDTOInter sftDTO = (SwitchFileTemplateDTOInter) sftDAO.find(uDTO.getTwmplate());
        String action = "Update Detail template for Switch template's with name " + sftDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        SwitchFileTemplateDetailDTOInter uDTO = (SwitchFileTemplateDetailDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        SwitchFileTemplateDAOInter sftDAO = DAOFactory.createSwitchFileTemplateDAO(null);
        SwitchFileTemplateDTOInter sftDTO = (SwitchFileTemplateDTOInter) sftDAO.find(uDTO.getTwmplate());
        String action = "Delete Detail template for Switch template's with name " + sftDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        SwitchFileTemplateDetailDTOInter uDTO = DTOFactory.createSwitchFileTemplateDetailDTO();
        while (rs.next()) {
            uDTO.setTagstring(rs.getString("TAG_STRING"));
               Integer temp3 = rs.getInt("ADD_DECIMAL");
            if(temp3 == 1){uDTO.setDecimalpoint(Boolean.TRUE);}
            else{uDTO.setDecimalpoint(Boolean.FALSE);}
            uDTO.setDecimalpointposition(rs.getString("DECIMAL_POS"));

            Integer temp = rs.getInt("MANDATORY");
            if(temp == 1){uDTO.setMandatory(Boolean.TRUE);}
            else{uDTO.setMandatory(Boolean.FALSE);}
   Integer temp2 = rs.getInt("NEGATIVE_AMOUNT_FLAG");
            if(temp2 == 1){uDTO.setNegativeflag(Boolean.TRUE);}
            else{uDTO.setNegativeflag(Boolean.FALSE);}
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getBigDecimal("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getBigDecimal("length"));
            uDTO.setLengthDir(rs.getBigDecimal("length_dir"));
            uDTO.setLineNumber(rs.getBigDecimal("line_number"));
            uDTO.setLoadWhenMapped(rs.getInt("load_when_mapped"));
            uDTO.setPosition(rs.getBigDecimal("position"));
            uDTO.setTwmplate(rs.getInt("template"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<SwitchFileTemplateDetailDTOInter> uDTOL = new ArrayList<SwitchFileTemplateDetailDTOInter>();
        while (rs.next()) {
            SwitchFileTemplateDetailDTOInter uDTO = DTOFactory.createSwitchFileTemplateDetailDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getBigDecimal("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));

                           Integer temp3 = rs.getInt("ADD_DECIMAL");
            if(temp3 == 1){uDTO.setDecimalpoint(Boolean.TRUE);}
            else{uDTO.setDecimalpoint(Boolean.FALSE);}
            uDTO.setDecimalpointposition(rs.getString("DECIMAL_POS"));

            uDTO.setLength(rs.getBigDecimal("length"));
            uDTO.setLengthDir(rs.getBigDecimal("length_dir"));
            uDTO.setLineNumber(rs.getBigDecimal("line_number"));
            uDTO.setLoadWhenMapped(rs.getInt("load_when_mapped"));
            uDTO.setPosition(rs.getBigDecimal("position"));
            uDTO.setTwmplate(rs.getInt("template"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));
             uDTO.setTagstring(rs.getString("TAG_STRING"));

                 Integer temp = rs.getInt("MANDATORY");
            if(temp == 1){uDTO.setMandatory(Boolean.TRUE);}
            else{uDTO.setMandatory(Boolean.FALSE);}
   Integer temp2 = rs.getInt("NEGATIVE_AMOUNT_FLAG");
            if(temp2 == 1){uDTO.setNegativeflag(true);}
            else{uDTO.setNegativeflag(false);}
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByTemplate(SwitchFileTemplateDTOInter aftDTO) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where template = $template";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$template", "" + aftDTO.getId());
        ResultSet rs = executeQuery(selectStat);
        List<SwitchFileTemplateDetailDTOInter> uDTOL = new ArrayList<SwitchFileTemplateDetailDTOInter>();
        while (rs.next()) {
            SwitchFileTemplateDetailDTOInter uDTO = DTOFactory.createSwitchFileTemplateDetailDTO();
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getBigDecimal("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));

                           Integer temp3 = rs.getInt("ADD_DECIMAL");
            if(temp3 == 1){uDTO.setDecimalpoint(Boolean.TRUE);}
            else{uDTO.setDecimalpoint(Boolean.FALSE);}
            uDTO.setDecimalpointposition(rs.getString("DECIMAL_POS"));

            uDTO.setLength(rs.getBigDecimal("length"));
            uDTO.setLengthDir(rs.getBigDecimal("length_dir"));
            uDTO.setLineNumber(rs.getBigDecimal("line_number"));
            uDTO.setLoadWhenMapped(rs.getInt("load_when_mapped"));
            uDTO.setPosition(rs.getBigDecimal("position"));
            uDTO.setTwmplate(rs.getInt("template"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));
             uDTO.setTagstring(rs.getString("TAG_STRING"));
                 Integer temp = rs.getInt("MANDATORY");
            if(temp == 1){uDTO.setMandatory(Boolean.TRUE);}
            else{uDTO.setMandatory(Boolean.FALSE);}
   Integer temp2 = rs.getInt("NEGATIVE_AMOUNT_FLAG");
            if(temp2 == 1){uDTO.setNegativeflag(Boolean.TRUE);}
            else{uDTO.setNegativeflag(Boolean.FALSE);}
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findByColumnId(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select * from $table where column_id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        SwitchFileTemplateDetailDTOInter uDTO = DTOFactory.createSwitchFileTemplateDetailDTO();
        while (rs.next()) {
            uDTO.setColumnId(rs.getInt("column_id"));
            uDTO.setDataType(rs.getBigDecimal("data_type"));
            uDTO.setFormat(rs.getString("format"));
            uDTO.setFormat2(rs.getString("format2"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setLength(rs.getBigDecimal("length"));
            uDTO.setLengthDir(rs.getBigDecimal("length_dir"));
            uDTO.setLineNumber(rs.getBigDecimal("line_number"));
            uDTO.setLoadWhenMapped(rs.getInt("load_when_mapped"));
            uDTO.setPosition(rs.getBigDecimal("position"));
            uDTO.setTwmplate(rs.getInt("template"));
                 uDTO.setTagstring(rs.getString("TAG_STRING"));
     Integer temp = rs.getInt("MANDATORY");
            if(temp == 1){uDTO.setMandatory(Boolean.TRUE);}
            else{uDTO.setMandatory(Boolean.FALSE);}
      Integer temp2 = rs.getInt("NEGATIVE_AMOUNT_FLAG");
            if(temp == 1){uDTO.setNegativeflag(Boolean.TRUE);}
            else{uDTO.setNegativeflag(Boolean.FALSE);}

               Integer temp3 = rs.getInt("ADD_DECIMAL");
            if(temp3 == 1){uDTO.setDecimalpoint(Boolean.TRUE);}
            else{uDTO.setDecimalpoint(Boolean.FALSE);}
            uDTO.setDecimalpointposition(rs.getString("DECIMAL_POS"));

            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findByTemplateAndColumnId(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer templateId = (Integer) obj[0];
        Integer columnId = (Integer) obj[1];

        String selectStat = "select * from $table where column_id = $ColumnId and template = $templateId";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$ColumnId", "" + columnId);
        selectStat = selectStat.replace("$templateId", "" + templateId);
        ResultSet rs = executeQuery(selectStat);
        Boolean exist = false;
        while (rs.next()) {
            exist = true;
        }
        super.postSelect(rs);
        return exist;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }
}
