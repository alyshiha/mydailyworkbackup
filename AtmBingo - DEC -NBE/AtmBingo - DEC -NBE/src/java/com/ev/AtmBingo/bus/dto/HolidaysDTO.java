/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class HolidaysDTO extends BaseDTO implements Serializable, HolidaysDTOInter{


    private String smonth;
    private int month;
    private Integer day;
    private String aReason;
    private String eReason;
    private Integer dayCount;

    public Integer getDayCount() {
        return dayCount;
    }

    public void setDayCount(Integer dayCount) {
        this.dayCount = dayCount;
    }

    

    public String getSmonth() {
        if(getMonth() == 1){
            setSmonth("January");
            dayCount = 31;
        }
        else if(getMonth() == 2){
            setSmonth("February");
            dayCount = 29;
        }
        else if(getMonth() == 3){
            setSmonth("March");
            dayCount = 31;
        }
        else if(getMonth() == 4){
            setSmonth("April");
            dayCount = 30;
        }
        else if(getMonth() == 5){
            setSmonth("May");
            dayCount = 31;
        }
        else if(getMonth() == 6){
            setSmonth("June");
            dayCount = 30;
        }
        else if(getMonth() == 7){
            setSmonth("July");
            dayCount = 31;
        }
        else if(getMonth() == 8){
            setSmonth("August");
            dayCount = 31;
        }
        else if(getMonth() == 9){
            setSmonth("September");
            dayCount = 30;
        }
        else if(getMonth() == 10){
            setSmonth("October");
            dayCount = 31;
        }
        else if(getMonth() == 11){
            setSmonth("November");
            dayCount = 30;
        }
        else if(getMonth() == 12){
            setSmonth("December");
            dayCount = 31;
        }
        return smonth;
    }

    public void setSmonth(String smonth) {
        if(smonth.equals("January")){
            setMonth(1);
        }
        else if(smonth.equals("February")){
            setMonth(2);
        }
        else if(smonth.equals("March")){
            setMonth(3);
        }
        else if(smonth.equals("April")){
            setMonth(4);
        }
        else if(smonth.equals("May")){
            setMonth(5);
        }
        else if(smonth.equals("June")){
            setMonth(6);
        }
        else if(smonth.equals("July")){
            setMonth(7);
        }
        else if(smonth.equals("August")){
            setMonth(8);
        }
        else if(smonth.equals("September")){
            setMonth(9);
        }
        else if(smonth.equals("October")){
            setMonth(10);
        }
        else if(smonth.equals("November")){
            setMonth(11);
        }
        else if(smonth.equals("December")){
            setMonth(12);
        }
        this.smonth = smonth;
    }

    public String getaReason() {
        return aReason;
    }

    public void setaReason(String aReason) {
        this.aReason = aReason;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public String geteReason() {
        return eReason;
    }

    public void seteReason(String eReason) {
        this.eReason = eReason;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        

        this.month = month;
    }
    
    protected HolidaysDTO() {
        super();
    }

}
