/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.bo.TransactionStatusBOInter;
import com.ev.AtmBingo.bus.dto.TransactionStatusDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class TransactionStatusClient extends BaseBean  implements Serializable{

    private TransactionStatusBOInter transObject;
    private List<TransactionStatusDTOInter> transDefList;
    private TransactionStatusDTOInter transAttr;
    private TransactionStatusDTOInter transDelAttr;
    private TransactionStatusDTOInter transNewRecord;
    private String message;
    private Boolean showAdd;
    private Boolean showConfirm;
    private Integer listSize;

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public TransactionStatusDTOInter getTransDelAttr() {
        return transDelAttr;
    }

    public void setTransDelAttr(TransactionStatusDTOInter transDelAttr) {
        this.transDelAttr = transDelAttr;
    }

    public TransactionStatusDTOInter getTransAttr() {
        return transAttr;
    }

    public void setTransAttr(TransactionStatusDTOInter transAttr) {
        this.transAttr = transAttr;
    }

    public List<TransactionStatusDTOInter> getTransDefList() {
        return transDefList;
    }

    public void setTransDefList(List<TransactionStatusDTOInter> transDefList) {
        this.transDefList = transDefList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Creates a new instance of TransactionStatusClient
     */
    public TransactionStatusClient() throws Throwable {
        super();
        super.GetAccess();
        transObject = BOFactory.createTransactionStatusBO(null);
        transDefList = (List<TransactionStatusDTOInter>) transObject.getTransactionStatus();
        showAdd = true;
        showConfirm = false;
        listSize = transDefList.size();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void fillUpdate(ActionEvent e) {
        transAttr = (TransactionStatusDTOInter) e.getComponent().getAttributes().get("updateRow");
        message = "";
    }

    public void updateRecord() {
        try {
            if (!showAdd) {
                message = super.showMessage(CONF_FIRST);
            } else {
                if (transAttr.getName().equals("")) {
                    message = super.showMessage(REQ_FIELD);
                } else {
                    String[] inputs = {transAttr.getDescription(), transAttr.getName(), "" + transAttr.getId()};
                    String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                    Boolean found = Boolean.FALSE;
                    for (String input : inputs) {
                        boolean b = Pattern.matches("^[a-zA-Z0-9*]*$", input.replace(" ", ""));
                        if (b == false) {
                            found = Boolean.TRUE;
                        }
                        if (!"".equals(input)) {
                            for (String validate1 : validate) {
                                if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                    found = Boolean.TRUE;
                                }
                            }
                        }
                    }
                    if (!found) {
                        transObject.editeTransactionStatus(transAttr, transObject.UPDATE);
                        message = super.showMessage(SUCC);
                    } else {
                        message = "Please Enter Valid Data";
                    }

                }
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void fillDelete(ActionEvent e) {
        transDelAttr = (TransactionStatusDTOInter) e.getComponent().getAttributes().get("removedRow");
        message = "";
    }

    public void deleteRecord() {
        try {
            String[] inputs = {transDelAttr.getDescription(), transDelAttr.getName(), "" + transDelAttr.getId()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9*]*$", input.replace(" ", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
                transObject.editeTransactionStatus(transDelAttr, transObject.DELETE);
                transDefList.remove(transDelAttr);
                message = super.showMessage(SUCC);
                showAdd = true;
                showConfirm = false;
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void addTransaction() {
        transNewRecord = DTOFactory.createTransactionStatusDTO();
        transDefList.add(transNewRecord);
        showAdd = false;
        showConfirm = true;
        message = "";
    }

    public void confirmTransaction() {
        try {
            if (transNewRecord.getName().equals("")) {
                message = super.showMessage(REQ_FIELD);
            } else {
                String[] inputs = {transNewRecord.getDescription(), transNewRecord.getName(), "" + transNewRecord.getId()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                boolean b = Pattern.matches("^[a-zA-Z0-9*]*$", input.replace(" ", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            if (!found) {
               transObject.editeTransactionStatus(transNewRecord, transObject.INSERT);
                message = super.showMessage(SUCC);
                showAdd = true;
                showConfirm = false;
            } else {
                message = "Please Enter Valid Data";
            }
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
            transDefList.remove(transNewRecord);
            showAdd = false;
            showConfirm = true;
        }
    }

    public void resetVars() {
        message = "";
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
