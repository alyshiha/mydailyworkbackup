/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.bo.BaseBO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dao.AtmStatByCashRepDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.statbytransDTOInter;
import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author Administrator
 */
public class AtmStatByCashRepBO extends BaseBO implements AtmStatByCashRepBOInter ,Serializable{

    protected AtmStatByCashRepBO() {
        super();
    }

    public Object getGrp()throws Throwable{
        return super.getAtmGrp();
    }

    private String grpName(int grpId)throws Throwable{
        return super.getGrpName(grpId);
    }
 public List<statbytransDTOInter> runReportExcel(String dateFrom, String dateTo, int atmGroupInt,String user, int NoOfAtms, int tType,String cust) throws Throwable {
 AtmStatByCashRepDAOInter arrRepDAO = DAOFactory.createAtmStatByCashRepDAO(null);

        List<statbytransDTOInter> rs = (List<statbytransDTOInter>) arrRepDAO.findstatbycashexcelReport(dateFrom, dateTo, atmGroupInt, NoOfAtms, tType);
        return rs;
 }
    public String runReport(String dateFrom, String dateTo, int atmGroupInt,String user, int NoOfAtms, int tType,String cust) throws Throwable {
        String atmGroup = "";
        if(atmGroupInt == 0){
            atmGroup = "All";
        }
        else{
         atmGroup = grpName(atmGroupInt);
        }
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        JRResultSetDataSource resultSetDataSource;
        AtmStatByCashRepDAOInter arrRepDAO = DAOFactory.createAtmStatByCashRepDAO(null);

        ResultSet rs = (ResultSet) arrRepDAO.findReport(dateFrom, dateTo, atmGroupInt, NoOfAtms, tType);
        resultSetDataSource = new JRResultSetDataSource(rs);

        params.put("DateFrom", dateFrom);
        params.put("user", user);
        params.put("DateTo", dateTo);
        params.put("AtmGroupint", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("NoOfAtms", NoOfAtms);
        params.put("tType", tType);
        params.put("customer", cust);

        try {


            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/BingoReports/ATM Stat By Cash.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);

            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, resultSetDataSource);
            Calendar c = Calendar.getInstance();

            String uri = "ATMStatByCash" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);


            
            try {

                String x = pdfPath + uri;

                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                resultSetDataSource = null;
                CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());
                rs.close();
           
                
                return x;

            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            
            return "fail";
        }

    }

    public static void main(String[] args) throws Throwable {
        AtmStatByCashRepBOInter arrBo = BOFactory.createAtmStatByCashRepBO(null);
        //arrBo.runReport("01.01.2007 00:00:00", "01.12.2011 00:00:00", 0, "Admin", 3, 1);
    }
}
