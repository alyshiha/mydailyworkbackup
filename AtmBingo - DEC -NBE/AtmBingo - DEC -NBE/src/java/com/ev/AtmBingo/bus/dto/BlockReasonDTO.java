/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class BlockReasonDTO extends BaseDTO implements Serializable, BlockReasonDTOInter{

    private int reasonId;
    private String reasonName;
    private String reasonAName;

    public String getReasonAName() {
        return reasonAName;
    }

    public void setReasonAName(String reasonAName) {
        this.reasonAName = reasonAName;
    }

    public int getReasonId() {
        return reasonId;
    }

    public void setReasonId(int reasonId) {
        this.reasonId = reasonId;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }
    
    protected BlockReasonDTO() {
        super();
    }


}
