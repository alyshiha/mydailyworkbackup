/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.client;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.regex.*;

public class GetMac
{

public String getMacAddress() throws IOException
{
String macAddress = null;
String command = "ifconfig -a ";

Process pid = Runtime.getRuntime().exec(command);
BufferedReader in =
new BufferedReader(
new InputStreamReader(pid.getInputStream()));
while (true) {
String line = in.readLine();
if (line == null)
break;
Pattern p = Pattern.compile(".*Link encap:Ethernet  HWaddr.*: (.*)");
Matcher m = p.matcher(line);
if (m.matches()) {
macAddress = m.group(1);
break;
}
}
in.close();
return macAddress;
}

}

