/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class AutoRepDTO extends BaseDTO implements AutoRepDTOInter  , Serializable {

    Integer groupid ;
    String groupName,rowid;
    Integer typenumber ;
    Integer typecassette ;
    Integer currency ;
 public String getrowid() {
        return rowid;
    }

    public void setrowid(String rowid) {
        this.rowid = rowid;
    }
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String GroupName) {
        this.groupName = GroupName;
    }

    public Integer getCurrency() {
        return currency;
    }

    public void setCurrency(Integer currency) {
        this.currency = currency;
    }

    public Integer getGroupid() {
        return groupid;
    }

    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }

    public Integer getTypecassette() {
        return typecassette;
    }

    public void setTypecassette(Integer typecassette) {
        this.typecassette = typecassette;
    }

    public Integer getTypenumber() {
        return typenumber;
    }

    public void setTypenumber(Integer typenumber) {
        this.typenumber = typenumber;
    }


}
