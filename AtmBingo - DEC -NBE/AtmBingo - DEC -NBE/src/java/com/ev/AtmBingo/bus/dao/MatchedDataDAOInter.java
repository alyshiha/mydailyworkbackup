/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.bus.dto.MatchedDataDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.sql.Clob;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface MatchedDataDAOInter {


    String Findtexttransaction(Object... obj) throws Throwable;
    String Findtexttransaction_other(Object... obj) throws Throwable;

    Clob FindFiletransaction(Object obj) throws Throwable;
    void deleteFromMatchedReport(int userId)throws Throwable;

    Object findReport(String fields,String whereClause, MatchedDataDTOInter[] mdDTOArr,Integer userId)throws Throwable;

    Object findAnotherSide(Object obj)throws Throwable;

    Object deleteForMatchedPage(Object... obj) throws Throwable;

    Object insertForMatchedPage(Object... obj) throws Throwable ;

    Object updateForMatchedPage(MatchedDataDTOInter uDTO,UsersDTOInter logedUser) throws Throwable;

    Object[] customSearch(String whereClause, String field,String temp,String index)throws Throwable;

    Object delete(Object... obj) throws Throwable;

    Object find(Date transactionDate, String atmApplicationId, String cardNo, int amount, String currency, String responseCode, int matchingType, int recordType) throws Throwable;

    Object findAll() throws Throwable;

    Object countMatched(Object... obj) throws Throwable;

    Object findTotalAmount(String whereClause,String temp) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object search(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
