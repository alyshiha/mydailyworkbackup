/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.UsersDTOInter;

/**
 *
 * @author Administrator
 */
public interface PrivilageBOInter{

    Object updateStatus(UsersDTOInter uDTO)throws Throwable;

    String generateMenu(int userId) throws Throwable;

    String getDashURL();

}
