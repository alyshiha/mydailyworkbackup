/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.AtmFileTemplateDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmFileTemplateDAO extends BaseDAO implements AtmFileTemplateDAOInter {

    protected AtmFileTemplateDAO() {
        super();
        super.setTableName("atm_file_template");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileTemplateDTOInter uDTO = (AtmFileTemplateDTOInter) obj[0];
        String TName = (String) obj[1];
        uDTO.setId(super.generateSequence(TName));
        String msg = ValidateNull(uDTO, TName);
        if (msg.equals("Validate")) {
            String insertStat = "";
            if (TName.equals("SWITCH_FILE_TEMPLATE")) {
                insertStat = "insert into switch_file_template"
                        + "(id, name, processing_type, number_of_lines, loading_folder, starting_data_type, starting_format, starting_position, backup_folder, copy_folder, ignored_lines, starting_length, date_separator, date_separator_pos1, date_separator_pos2, server_folder, header_data_type, header_format, header_position, header_length, header_date_separator, header_date_separator_pos1, header_date_separator_pos2, header_string, starting_value, active, separator, tags_ending_data_type, tags_ending_format, tags_ending_position, tags_ending_length, tags_ending_value, atm_add)"
                        + "values"
                        + "('$id', '$name', $processingType, $numberOfLines, '$loadingFolder', $sstartingDataType, '$startingFormat', $strtingPosition, '$buckupFolder', '$copyFolder', $ignoredLines, $strtngLength, '$dateSeparator', $dSsPos1, $dSPos2, '$serverFolder', $headerDataType, '$headerFormat', '$headerPosition', $headerLength, '$headerDateSeparator', $hDSPos1, $hDSPs2, '$headerString', '$startingValue', $active, '$separator', $TAGSENDINGDATATYPE, '$TAGSENDINGFORMAT', $TAGSENDINGPOSITION, $TAGSENDINGLENGTH, '$TAGSENDINGVALUE', '$atmadd')";
            }
            if (TName.equals("HOST_FILE_TEMPLATE")) {
                insertStat = "insert into host_file_template"
                        + "(id, name, processing_type, number_of_lines, loading_folder, starting_data_type, starting_format, starting_position, backup_folder, copy_folder, ignored_lines, starting_length, date_separator, date_separator_pos1, date_separator_pos2, server_folder, header_data_type, header_format, header_position, header_length, header_date_separator, header_date_separator_pos1, header_date_separator_pos2, header_string, starting_value, active, separator, tags_ending_data_type, tags_ending_format, tags_ending_position, tags_ending_length, tags_ending_value, atm_add)"
                        + "values"
                        + "('$id', '$name', $processingType, $numberOfLines, '$loadingFolder', $sstartingDataType, '$startingFormat', $strtingPosition, '$buckupFolder', '$copyFolder', $ignoredLines, $strtngLength, '$dateSeparator', $dSsPos1, $dSPos2, '$serverFolder', $headerDataType, '$headerFormat', '$headerPosition', $headerLength, '$headerDateSeparator', $hDSPos1, $hDSPs2, '$headerString', '$startingValue', $active, '$separator', $TAGSENDINGDATATYPE, '$TAGSENDINGFORMAT', $TAGSENDINGPOSITION, $TAGSENDINGLENGTH, '$TAGSENDINGVALUE', '$atmadd')";
            }
            if (TName.equals("ATM_FILE_TEMPLATE")) {
                insertStat = "insert into atm_file_template"
                        + "(id, name, processing_type, number_of_lines, loading_folder, starting_data_type, starting_format, starting_position, backup_folder, copy_folder, ignored_lines, starting_length, date_separator, date_separator_pos1, date_separator_pos2, server_folder, header_data_type, header_format, header_position, header_length, header_date_separator, header_date_separator_pos1, header_date_separator_pos2, header_string, starting_value, active, separator, atm_machine_type, atm_add, tags_ending_data_type, tags_ending_format, tags_ending_position, tags_ending_length, tags_ending_value)"
                        + "values"
                        + "('$id', '$name', $processingType, $numberOfLines, '$loadingFolder', $sstartingDataType, '$startingFormat', $strtingPosition, '$buckupFolder', '$copyFolder', $ignoredLines, $strtngLength, '$dateSeparator', $dSsPos1, $dSPos2, '$serverFolder', $headerDataType, '$headerFormat', '$headerPosition', $headerLength, '$headerDateSeparator', $hDSPos1, $hDSPs2, '$headerString', '$startingValue', $active, '$separator',$machineType,'$atmadd', $TAGSENDINGDATATYPE, '$TAGSENDINGFORMAT', $TAGSENDINGPOSITION, $TAGSENDINGLENGTH, '$TAGSENDINGVALUE')";
            }

            insertStat = insertStat.replace("$table", "" + TName);
            insertStat = insertStat.replace("$id", "" + uDTO.getId());
            insertStat = insertStat.replace("$atmadd", "" + uDTO.getAtmadd());
            insertStat = insertStat.replace("$TAGSENDINGVALUE", "" + uDTO.getTagendingvalue());
            insertStat = insertStat.replace("$TAGSENDINGFORMAT", "" + uDTO.getTagendingformat());
            insertStat = insertStat.replace("$TAGSENDINGDATATYPE", "" + uDTO.getTagendingdatatype());
            insertStat = insertStat.replace("$TAGSENDINGLENGTH", "" + uDTO.getTagendinglength());
            insertStat = insertStat.replace("$TAGSENDINGPOSITION", "" + uDTO.getTagendingposition());
            insertStat = insertStat.replace("$name", "" + uDTO.getName());
            insertStat = insertStat.replace("$processingType", "" + uDTO.getProcessingType());
            insertStat = insertStat.replace("$numberOfLines", "" + uDTO.getNumberOfLines());
            insertStat = insertStat.replace("$loadingFolder", "" + uDTO.getLoadingFolder());
            insertStat = insertStat.replace("$sstartingDataType", "" + uDTO.getStartingDataType());
            insertStat = insertStat.replace("$startingFormat", "" + uDTO.getStartingFormat());
            insertStat = insertStat.replace("$strtingPosition", "" + uDTO.getStartingPosition());
            insertStat = insertStat.replace("$buckupFolder", "" + uDTO.getBuckupFolder());
            insertStat = insertStat.replace("$copyFolder", "" + uDTO.getCopyFolder());
            insertStat = insertStat.replace("$ignoredLines", "" + uDTO.getIgnoredLines());
            insertStat = insertStat.replace("$strtngLength", "" + uDTO.getStartingLength());
            insertStat = insertStat.replace("$dateSeparator", "" + uDTO.getDateSeparator());
            insertStat = insertStat.replace("$dSsPos1", "" + uDTO.getDateSeparatorPos1());
            insertStat = insertStat.replace("$dSPos2", "" + uDTO.getDateSeparatorPos2());
            insertStat = insertStat.replace("$serverFolder", "" + uDTO.getServerFolder());
            insertStat = insertStat.replace("$headerDataType", "" + uDTO.getHeaderDataType());
            insertStat = insertStat.replace("$headerFormat", "" + uDTO.getHeaderFormat());
            insertStat = insertStat.replace("$headerPosition", "" + uDTO.getHeaderPosition());
            insertStat = insertStat.replace("$headerLength", "" + uDTO.getHeaderLength());
            insertStat = insertStat.replace("$headerDateSeparator", "" + uDTO.getHeaderDateSeparator());
            insertStat = insertStat.replace("$hDSPos1", "" + uDTO.getHeaderDateSeparatorPos1());
            insertStat = insertStat.replace("$hDSPs2", "" + uDTO.getHeaderDateSeparatorPos2());
            insertStat = insertStat.replace("$headerString", "" + uDTO.getHeaderString());
            insertStat = insertStat.replace("$startingValue", "" + uDTO.getStartingValue());
            insertStat = insertStat.replace("$active", "" + uDTO.getActive());
            insertStat = insertStat.replace("$separator", "" + uDTO.getSeparator());
            insertStat = insertStat.replace("$machineType", "" + uDTO.getMachineType());

            msg = super.executeUpdate(insertStat).toString();
            msg = msg + " Template Has Been Inserted";
            super.postUpdate("Add New Record To " + TName, false);
        }
        return msg;
    }

    public String ValidateNull(Object... obj) {
        AtmFileTemplateDTOInter RecordToInsert = (AtmFileTemplateDTOInter) obj[0];
        String TName = (String) obj[1];
        String Validate = "Validate";
        if (RecordToInsert.getId() == 0) {
            Validate = Validate + " Feild id is a requiered field";
        }
        if (RecordToInsert.getName() == null || RecordToInsert.getName().equals("")) {
            Validate = Validate + " Feild name is a requiered field";
        }
        if (RecordToInsert.getProcessingType() == null) {
            Validate = Validate + " Feild processingtype is a requiered field";
        }
        if (RecordToInsert.getNumberOfLines() == null) {
            Validate = Validate + " Feild numberoflines is a requiered field";
        }
        if (RecordToInsert.getLoadingFolder() == null || RecordToInsert.getLoadingFolder().equals("")) {
            Validate = Validate + " Feild loadingfolder is a requiered field";
        }
        if (RecordToInsert.getBuckupFolder() == null || RecordToInsert.getBuckupFolder().equals("")) {
            Validate = Validate + " Feild backupfolder is a requiered field";
        }
        if (RecordToInsert.getCopyFolder() == null || RecordToInsert.getCopyFolder().equals("")) {
            Validate = Validate + " Feild copyfolder is a requiered field";
        }
        if (RecordToInsert.getIgnoredLines() == null) {
            Validate = Validate + " Feild ignoredlines is a requiered field";
        }
        if (RecordToInsert.getActive() == 0) {
            Validate = Validate + " Feild active is a requiered field";
        }
        if (TName.equals("ATM_FILE_TEMPLATE")) {
            if (RecordToInsert.getMachineType() == null) {
                Validate = Validate + " Feild atmmachinetype is a requiered field";
            }
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileTemplateDTOInter uDTO = (AtmFileTemplateDTOInter) obj[0];
        String TName = (String) obj[1];
        String msg = ValidateNull(uDTO, TName);
        if (msg.equals("Validate")) {
            String updateStat = "update $table set  name = '$name', processing_type = $processingType"
                    + ", number_of_lines = $numberOfLines, loading_folder = '$lodingFolder', starting_data_type = $startingDataType"
                    + ", starting_format = '$startingFormat', starting_position = $startingPosition, backup_folder = '$buckupFolder'"
                    + ", copy_folder = '$copyFolder', ignored_lines = $ignoredLines, starting_length = $startingLength"
                    + ", date_separator = '$dateSparator', date_separator_pos1 = $dSeparatorPos1, date_separator_pos2 = $dateSeparatorPos2"
                    + ", server_folder = '$serverFolder', header_data_type = $headerDataType, header_format = '$headerFormat'"
                    + ", header_position = '$headerPosition', header_length = $headerLength, header_date_separator = '$headerDateSparator'"
                    + ", header_date_separator_pos1 = $hDateSeparatorPos1, header_date_separator_pos2 = $headerDateSeparatorPos2"
                    + ", starting_value = '$startingValue', active = $active, separator = '$separator',atm_add = '$atm_add',";
            if (TName.equals("ATM_FILE_TEMPLATE")) {
                updateStat = updateStat + "atm_machine_type = $machineType, ";
            }
            updateStat = updateStat + "TAGS_ENDING_VALUE='$TAGSENDINGVALUE',TAGS_ENDING_LENGTH=$TAGSENDINGLENGTH,TAGS_ENDING_POSITION= $TAGSENDINGPOSITION "
                    + " ,TAGS_ENDING_FORMAT='$TAGSENDINGFORMAT',TAGS_ENDING_DATA_TYPE=$TAGSENDINGDATATYPE,header_string='$headerString'"
                    + " where id = $id";

            updateStat = updateStat.replace("$table", "" + TName);
            updateStat = updateStat.replace("$id", "" + uDTO.getId());
            updateStat = updateStat.replace("$atm_add", "" + uDTO.getAtmadd());
            updateStat = updateStat.replace("$TAGSENDINGVALUE", "" + uDTO.getTagendingvalue());
            updateStat = updateStat.replace("$TAGSENDINGFORMAT", "" + uDTO.getTagendingformat());
            updateStat = updateStat.replace("$TAGSENDINGDATATYPE", "" + uDTO.getTagendingdatatype());
            updateStat = updateStat.replace("$TAGSENDINGLENGTH", "" + uDTO.getTagendinglength());
            updateStat = updateStat.replace("$TAGSENDINGPOSITION", "" + uDTO.getTagendingposition());
            updateStat = updateStat.replace("$name", "" + uDTO.getName());
            updateStat = updateStat.replace("$processingType", "" + uDTO.getProcessingType());
            updateStat = updateStat.replace("$numberOfLines", "" + uDTO.getNumberOfLines());
            updateStat = updateStat.replace("$lodingFolder", "" + uDTO.getLoadingFolder());
            updateStat = updateStat.replace("$startingDataType", "" + uDTO.getStartingDataType());
            updateStat = updateStat.replace("$headerString", "" + uDTO.getHeaderString());
            updateStat = updateStat.replace("$startingFormat", "" + uDTO.getStartingFormat());
            updateStat = updateStat.replace("$startingPosition", "" + uDTO.getStartingPosition());
            updateStat = updateStat.replace("$buckupFolder", "" + uDTO.getBuckupFolder());
            updateStat = updateStat.replace("$copyFolder", "" + uDTO.getCopyFolder());
            updateStat = updateStat.replace("$ignoredLines", "" + uDTO.getIgnoredLines());
            updateStat = updateStat.replace("$startingLength", "" + uDTO.getStartingLength());
            updateStat = updateStat.replace("$dateSparator", "" + uDTO.getDateSeparator());
            updateStat = updateStat.replace("$dSeparatorPos1", "" + uDTO.getDateSeparatorPos1());
            updateStat = updateStat.replace("$dateSeparatorPos2", "" + uDTO.getDateSeparatorPos2());
            updateStat = updateStat.replace("$serverFolder", "" + uDTO.getServerFolder());
            updateStat = updateStat.replace("$headerDataType", "" + uDTO.getHeaderDataType());
            updateStat = updateStat.replace("$headerFormat", "" + uDTO.getHeaderFormat());
            updateStat = updateStat.replace("$headerPosition", "" + uDTO.getHeaderPosition());
            updateStat = updateStat.replace("$headerLength", "" + uDTO.getHeaderLength());
            updateStat = updateStat.replace("$headerDateSparator", "" + uDTO.getHeaderDateSeparator());
            updateStat = updateStat.replace("$hDateSeparatorPos1", "" + uDTO.getHeaderDateSeparatorPos1());
            updateStat = updateStat.replace("$headerDateSeparatorPos2", "" + uDTO.getHeaderDateSeparatorPos2());
            updateStat = updateStat.replace("$startingValue", "" + uDTO.getStartingValue());
            updateStat = updateStat.replace("$active", "" + uDTO.getActive());
            updateStat = updateStat.replace("$separator", "" + uDTO.getSeparator());
            updateStat = updateStat.replace("$machineType", "" + uDTO.getMachineType());
            
            msg = super.executeUpdate(updateStat).toString();
            msg = msg + " Template Has Been updated";
            super.postUpdate("Update Record In Table ATM_FILE_TEMPLATE_DETAIL", false);
        }
        ;
        return msg;
    }

    public String ValidateNullDelete(Object... obj) {
        AtmFileTemplateDTOInter RecordToInsert = (AtmFileTemplateDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getId() == 0) {
            Validate = Validate + " Feild id is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object deleterecordTemplate(Object... obj) throws Throwable {
        super.preUpdate();
        AtmFileTemplateDTOInter RecordToDelete = (AtmFileTemplateDTOInter) obj[0];
        String TName = (String) obj[1];
        String msg = ValidateNullDelete(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  ID= $id";
            deleteStat = deleteStat.replace("$table", "" + TName);
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getId());
            msg = super.executeUpdate(deleteStat).toString();
            msg = msg + " Template Has Been deleted";
            super.postUpdate("Delete Record In Table " + TName, false);
        }
        return msg;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        AtmFileTemplateDTOInter uDTO = (AtmFileTemplateDTOInter) obj[0];
        String TName = (String) obj[1];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + TName);
        deleteStat = deleteStat.replace("$id", "" + uDTO.getId());
        super.executeUpdate(deleteStat);
        String action = "Delete Journal Template with name " + uDTO.getName();
        super.postUpdate(action, false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer userId = (Integer) obj[0];
        String Tablename = (String) obj[1];
        String selectStat = "select * from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + Tablename);
        selectStat = selectStat.replace("$id", "" + userId);
        ResultSet rs = executeQuery(selectStat);
        AtmFileTemplateDTOInter uDTO = DTOFactory.createAtmFileTemplateDTO();
        while (rs.next()) {
            uDTO.setActive(rs.getInt("active"));
            uDTO.setBuckupFolder(rs.getString("backup_folder"));

            uDTO.setAtmadd(rs.getString("atm_add"));

            uDTO.setTagendingvalue(rs.getString("TAGS_ENDING_VALUE"));
            uDTO.setTagendingformat(rs.getString("TAGS_ENDING_FORMAT"));
            uDTO.setTagendingdatatype(rs.getInt("TAGS_ENDING_DATA_TYPE"));
            uDTO.setTagendingposition(rs.getInt("TAGS_ENDING_POSITION"));
            uDTO.setTagendinglength(rs.getInt("TAGS_ENDING_LENGTH"));
            uDTO.setCopyFolder(rs.getString("copy_folder"));
            uDTO.setDateSeparator(rs.getString("date_separator"));
            uDTO.setDateSeparatorPos1(rs.getBigDecimal("date_separator_pos1"));
            uDTO.setDateSeparatorPos2(rs.getBigDecimal("date_separator_pos2"));
            uDTO.setHeaderDataType(rs.getBigDecimal("header_data_type"));
            uDTO.setHeaderDateSeparator(rs.getString("header_date_separator"));
            uDTO.setHeaderDateSeparatorPos1(rs.getBigDecimal("header_date_separator_pos1"));
            uDTO.setHeaderDateSeparatorPos2(rs.getBigDecimal("header_date_separator_pos2"));
            uDTO.setHeaderFormat(rs.getString("header_format"));
            uDTO.setHeaderLength(rs.getBigDecimal("header_length"));
            uDTO.setHeaderPosition(rs.getString("header_position"));
            uDTO.setHeaderString(rs.getString("header_string"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setIgnoredLines(rs.getBigDecimal("ignored_lines"));
            uDTO.setLoadingFolder(rs.getString("loading_folder"));
            uDTO.setName(rs.getString("name"));
            uDTO.setNumberOfLines(rs.getBigDecimal("number_of_lines"));
            uDTO.setProcessingType(rs.getBigDecimal("processing_type"));
            uDTO.setSeparator(rs.getString("separator"));
            uDTO.setServerFolder(rs.getString("server_folder"));
            uDTO.setStartingDataType(rs.getBigDecimal("starting_data_type"));
            uDTO.setStartingFormat(rs.getString("starting_format"));
            uDTO.setStartingLength(rs.getBigDecimal("starting_length"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));
            uDTO.setStartingValue(rs.getString("starting_value"));
            if (Tablename.equals("ATM_FILE_TEMPLATE")) {
                uDTO.setMachineType(rs.getBigDecimal("atm_machine_type"));
            }


        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll(String Table) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by name";
        selectStat = selectStat.replace("$table", "" + Table);
        ResultSet rs = executeQuery(selectStat);
        List<AtmFileTemplateDTOInter> uDTOL = new ArrayList<AtmFileTemplateDTOInter>();
        while (rs.next()) {
            AtmFileTemplateDTOInter uDTO = DTOFactory.createAtmFileTemplateDTO();
            uDTO.setActive(rs.getInt("active"));
            uDTO.setTagendingvalue(rs.getString("TAGS_ENDING_VALUE"));
            uDTO.setTagendingformat(rs.getString("TAGS_ENDING_FORMAT"));
            uDTO.setTagendingdatatype(rs.getInt("TAGS_ENDING_DATA_TYPE"));
            uDTO.setTagendingposition(rs.getInt("TAGS_ENDING_POSITION"));
            uDTO.setTagendinglength(rs.getInt("TAGS_ENDING_LENGTH"));
            uDTO.setBuckupFolder(rs.getString("backup_folder"));
            uDTO.setCopyFolder(rs.getString("copy_folder"));
            uDTO.setDateSeparator(rs.getString("date_separator"));
            uDTO.setDateSeparatorPos1(rs.getBigDecimal("date_separator_pos1"));
            uDTO.setDateSeparatorPos2(rs.getBigDecimal("date_separator_pos2"));
            uDTO.setHeaderDataType(rs.getBigDecimal("header_data_type"));
            uDTO.setHeaderDateSeparator(rs.getString("header_date_separator"));
            uDTO.setHeaderDateSeparatorPos1(rs.getBigDecimal("header_date_separator_pos1"));
            uDTO.setHeaderDateSeparatorPos2(rs.getBigDecimal("header_date_separator_pos2"));
            uDTO.setHeaderFormat(rs.getString("header_format"));
            uDTO.setHeaderLength(rs.getBigDecimal("header_length"));
            uDTO.setHeaderPosition(rs.getString("header_position"));
            uDTO.setHeaderString(rs.getString("header_string"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setIgnoredLines(rs.getBigDecimal("ignored_lines"));
            uDTO.setLoadingFolder(rs.getString("loading_folder"));
            uDTO.setName(rs.getString("name"));
            uDTO.setNumberOfLines(rs.getBigDecimal("number_of_lines"));
            uDTO.setProcessingType(rs.getBigDecimal("processing_type"));
            uDTO.setSeparator(rs.getString("separator"));
            uDTO.setServerFolder(rs.getString("server_folder"));
            uDTO.setStartingDataType(rs.getBigDecimal("starting_data_type"));
            uDTO.setStartingFormat(rs.getString("starting_format"));
            uDTO.setStartingLength(rs.getBigDecimal("starting_length"));
            uDTO.setStartingPosition(rs.getBigDecimal("starting_position"));
            uDTO.setStartingValue(rs.getString("starting_value"));
            if (Table.equals("ATM_FILE_TEMPLATE")) {
                uDTO.setMachineType(rs.getBigDecimal("atm_machine_type"));

            }
            uDTO.setAtmadd(rs.getString("atm_add"));
            uDTO.setHeaderString(rs.getString("header_string"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object search(Object... obj) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        Object obj1 = super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        super.postSelect(obj1);
        return null;
    }

    public static void main(String[] args) throws Throwable {
        AtmFileTemplateDAOInter a = DAOFactory.createAtmFileTemplateDAO(null);
        AtmFileTemplateDTOInter aa = DTOFactory.createAtmFileTemplateDTO();
//            aa.setActive(1);
//            aa.setBuckupFolder("asd");
//            aa.setCopyFolder("sdad");
//            aa.setDateSeparator(",");
//            aa.setDateSeparatorPos1(1);
//            aa.setMachineType(1);
//            aa.setLoadingFolder("dsasd");
//            aa.setName("gdasg");
//            aa.setNumberOfLines(1);
//            aa.setStartingDataType("null");
        a.insert(aa);

    }
}
