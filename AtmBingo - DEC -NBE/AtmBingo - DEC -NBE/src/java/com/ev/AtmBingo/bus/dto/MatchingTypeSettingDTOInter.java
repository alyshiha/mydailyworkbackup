/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Administrator
 */
public interface MatchingTypeSettingDTOInter extends Serializable{

    int getColumnId();

    int getId();

    int getMatchingType();

    BigDecimal getPartLength();

    int getPartSetting();

    void setColumnId(int columnId);

    void setId(int id);

    void setMatchingType(int matchingType);

    void setPartLength(BigDecimal partLength);

    void setPartSetting(int partSetting);

}
