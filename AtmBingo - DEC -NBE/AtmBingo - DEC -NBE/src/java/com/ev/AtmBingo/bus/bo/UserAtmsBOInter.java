/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface UserAtmsBOInter extends BaseBOInter {

    Object getAtmGroup()throws Throwable;

    Object getUsers()throws Throwable;

    Object getSourceList(int userId, int atmGroup) throws Throwable;

    Object getTargetList(int userId, int atmGroup) throws Throwable;

    Object saveUserAtm(int userId, String[] source,String[] target,int atmGInt) throws Throwable;

}
