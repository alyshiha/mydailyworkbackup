/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface ProfileMenuDTOInter extends Serializable {

    int getMenuId();

    int getProfileId();

    void setMenuId(int MenuId);

    void setProfileId(int ProfileId);

}
