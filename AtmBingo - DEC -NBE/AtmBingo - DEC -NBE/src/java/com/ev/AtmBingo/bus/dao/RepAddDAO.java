/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.RepAddDTOInter;
import com.ev.AtmBingo.bus.dto.RepDetailTempDTOInter;
import com.ev.AtmBingo.bus.dto.ReplanishmentMasterDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RepAddDAO extends BaseDAO implements RepAddDAOInter {

    protected RepAddDAO() {
        super();
        super.setTableName("REP_TMP_MASTER");
    }

    @Override
    public int insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        RepAddDTOInter uDTO = (RepAddDTOInter) obj[0];
        uDTO.setId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $TABLE"
                + " (id, repid, createdby, createddate)"
                + " values"
                + " ($ID, $REPID, $CBY, to_date('$CDATE','dd.MM.yyyy hh24:mi:ss'))";

        insertStat = insertStat.replace("$TABLE", "" + super.getTableName());
        insertStat = insertStat.replace("$CBY", "" + uDTO.getCreatedbyid());
        insertStat = insertStat.replace("$CDATE", "" + DateFormatter.changeDateAndTimeFormat(uDTO.getCreateddate()));
        insertStat = insertStat.replace("$REPID", "" + uDTO.getRepid());
        insertStat = insertStat.replace("$ID", "" + uDTO.getId());

        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
        insertcassette(uDTO.getRepid());
        return uDTO.getId();
    }

    @Override
    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        RepAddDTOInter uDTO = (RepAddDTOInter) obj[0];
        String updateStat = "update $table set shift_a_name = '$shiftAName', shift_e_name = '$shiftEName'"
                + " where shift_id = $shift_id";

        updateStat = updateStat.replace("$TABLE", "" + super.getTableName());
        updateStat = updateStat.replace("$ATMID", "" + uDTO.getAtmId());
        updateStat = updateStat.replace("$APPID", "" + uDTO.getAtmapplicationid());
        updateStat = updateStat.replace("$CBY", "" + uDTO.getCreatedby());
        updateStat = updateStat.replace("$CDATE", "" + uDTO.getCreateddate());
        updateStat = updateStat.replace("$DATEFROM", "" + uDTO.getDateFrom());
        updateStat = updateStat.replace("$DATETO", "" + uDTO.getDateTo());
        updateStat = updateStat.replace("$REPID", "" + uDTO.getRepid());
        updateStat = updateStat.replace("$ID", "" + uDTO.getId());

        super.executeUpdate(updateStat);
        super.postUpdate(null, true);
        return null;
    }

    @Override
    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        RepAddDTOInter uDTO = (RepAddDTOInter) obj[0];
        String deleteStat = "delete from $TABLE where id = $ID";

        deleteStat = deleteStat.replace("$TABLE", "" + super.getTableName());
        deleteStat = deleteStat.replace("$ID", "" + uDTO.getId());

        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        deletecassette(uDTO.getRepid());
        return null;
    }

    @Override
    public Object find(Date todate, int atmid) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select id,date_from,date_to "
                + "from replanishment_master "
                + "where atm_id = '$ATMID' "
                + "AND TRUNC(date_to) = to_date('$DATET','dd.MM.yyyy') ";
        selectStat = selectStat.replace("$DATET", "" + DateFormatter.changeDateFormat(todate));
        selectStat = selectStat.replace("$ATMID", "" + atmid);
        ResultSet rs = executeQuery(selectStat);
        List<ReplanishmentMasterDTOInter> uDTOL = new ArrayList<ReplanishmentMasterDTOInter>();
        while (rs.next()) {
            ReplanishmentMasterDTOInter uDTO = DTOFactory.createReplanishmentMasterDTO();
            uDTO.setDateFrom(rs.getTimestamp("date_from"));
            uDTO.setId(rs.getInt("id"));
            uDTO.setDateTo(rs.getTimestamp("date_to"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    @Override
    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select  t.id id, m.id repid, m.date_from datefrom, m.date_to dateto,\n"
                + " t.createdby createdby, m.created_date createddate, m.atm_id atmid,\n"
                + " (select a.application_id from atm_machine a where a.id = m.atm_id) appid,\n"
                + " (select s.user_name from users s where s.user_id = t.createdby) username\n"
                + " from    REP_TMP_MASTER t,replanishment_master m\n"
                + " where   t.repid = m.id";
        ResultSet rs = executeQuery(selectStat);
        List<RepAddDTOInter> uDTOL = new ArrayList<RepAddDTOInter>();
        while (rs.next()) {
            RepAddDTOInter uDTO = DTOFactory.createRepAddDTO();
            uDTO.setId(rs.getInt("id"));
            uDTO.setRepid(rs.getInt("repid"));
            uDTO.setDateFrom(rs.getTimestamp("datefrom"));
            uDTO.setDateTo(rs.getTimestamp("dateto"));
            uDTO.setCreatedby(rs.getString("username"));
            uDTO.setCreatedbyid(rs.getInt("createdby"));
            uDTO.setCreateddate(rs.getTimestamp("createddate"));
            uDTO.setAtmId(rs.getInt("atmid"));
            uDTO.setAtmapplicationid(rs.getString("appid"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public void insertcassette(int ID) throws Throwable {
        super.preUpdate();
        String insertStat = "insert into REP_TMP_DETAIL\n"
                + "  (repid, cassetteid, remaining, total_amount)\n"
                + "SELECT   r.id repid,\n"
                + "         cassette.id cassetteid,\n"
                + "         remaining,\n"
                + "         cassete_value\n"
                + "  FROM   replanishment_detail r, cassette \n"
                + " WHERE   r.cassete = cassette.id AND r.id = $ID";

        insertStat = insertStat.replace("$ID", "" + ID);

        super.executeUpdate(insertStat);
        super.postUpdate(null, true);
    }

    public Object deletecassette(int ID) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from REP_TMP_DETAIL where repid = $ID";
        deleteStat = deleteStat.replace("$ID", "" + ID);
        super.executeUpdate(deleteStat);
        super.postUpdate(null, true);
        return null;
    }

    @Override
    public int findInTemp(int ID) throws Throwable {
        super.preSelect();
        int count = 0;
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select count(1) countresult from REP_TMP_MASTER t where t.repid = $ID";
        selectStat = selectStat.replace("$ID", "" + ID);
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            count = rs.getInt("countresult");
        }
        super.postSelect(rs);
        return count;
    }

    @Override
    public Object findAllCassette(int RepID) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "SELECT   r.repid repid,\n"
                + "         cassette.id cassetteid,\n"
                + "         NVL (r.cassetteid, cassette.name) CName,\n"
                + "         remaining,\n"
                + "         total_amount\n"
                + "  FROM   REP_TMP_DETAIL r, cassette \n"
                + " WHERE   r.cassetteid = cassette.id AND r.repid = $RepID";
        selectStat = selectStat.replace("$RepID", "" + RepID);
        ResultSet rs = executeQuery(selectStat);
        List<RepDetailTempDTOInter> records = new ArrayList<RepDetailTempDTOInter>();
        while (rs.next()) {
            RepDetailTempDTOInter record = DTOFactory.createRepDetailTempDTO();
            record.setCassetteid(rs.getInt("cassetteid"));
            record.setRepid(rs.getInt("repid"));
            record.setRemaining(rs.getInt("remaining"));
            record.setTotalamount(rs.getInt("total_amount"));
            record.setCassettename(rs.getString("CName"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public String save(List<RepDetailTempDTOInter> entities) throws SQLException, Throwable {
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Cassettes Found";
        } else {
            String validate = "Validate";
            if (validate.equals("Validate")) {
                String updateStat = "update REP_TMP_DETAIL\n"
                        + "   set total_amount = ? , remaining = ?\n"
                        + "   where repid = ? and cassetteid = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                try {

                    connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
                    statement = connection.prepareStatement(updateStat);
                    for (int i = 0; i < entities.size(); i++) {
                        RepDetailTempDTOInter RecordtoUpdate = entities.get(i);
                        statement.setInt(1, RecordtoUpdate.getTotalamount());
                        statement.setInt(2, RecordtoUpdate.getRemaining());
                        statement.setInt(3, RecordtoUpdate.getRepid());
                        statement.setInt(4, RecordtoUpdate.getCassetteid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                            CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }

    public String Excute(RepAddDTOInter record) throws Throwable {
        super.preCollable();
        String temp = "";
        CallableStatement stat = super.executeCallableStatment("{call ? := ATM.update_rep_tmp(?,?)}");
        stat.registerOutParameter(1, Types.VARCHAR);
        stat.registerOutParameter(3, Types.VARCHAR);
        stat.setInt(2, record.getRepid());
        stat.executeUpdate();
        String Result = stat.getString(1);
        if ("FALSE".equals(Result)) {
            Result = stat.getString(3);
        } else {
            Result = "Record Has Been Updated Successfully";
        }
        super.postCollable(stat);
        return Result;
    }

}
