/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface BranchReasonDTOInter extends Serializable {

    int getReasonId();

    String getReasonName();

    void setReasonId(int reasonId);

    void setReasonName(String reasonName);

}
