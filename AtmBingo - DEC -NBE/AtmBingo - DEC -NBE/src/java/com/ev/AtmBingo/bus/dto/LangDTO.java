/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class LangDTO extends BaseDTO implements LangDTOInter, Serializable{

    private int langId;
    private String langName;
    private String langAName;

    protected LangDTO() {
        super();
    }

    public String getLangAName() {
        return langAName;
    }

    public void setLangAName(String langAName) {
        this.langAName = langAName;
    }

    public int getLangId() {
        return langId;
    }

    public void setLangId(int langId) {
        this.langId = langId;
    }

    public String getLangName() {
        return langName;
    }

    public void setLangName(String langName) {
        this.langName = langName;
    }

}
