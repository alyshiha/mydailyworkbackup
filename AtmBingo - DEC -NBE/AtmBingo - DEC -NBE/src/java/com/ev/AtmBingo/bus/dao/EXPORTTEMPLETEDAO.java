/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.EXPORTTEMPLETEDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */

public class EXPORTTEMPLETEDAO extends BaseDAO implements EXPORTTEMPLETEDAOInter {

    protected EXPORTTEMPLETEDAO() {
        super();
        super.setTableName("EXPORT_TEMPLETE");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        EXPORTTEMPLETEDTOInter uDTO = (EXPORTTEMPLETEDTOInter) obj[0];
        uDTO.setTemplateid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table (id, name, seperator, active) values ($v_id, '$v_name', '$v_seperator', $v_active)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$v_id", "" +  uDTO.getTemplateid());
        insertStat = insertStat.replace("$v_name", "" + uDTO.getTemplatename());
        insertStat = insertStat.replace("$v_seperator", "" + uDTO.getSeparator());
        insertStat = insertStat.replace("$v_active", "" + uDTO.getActive());
        super.executeUpdate(insertStat);
       // super.postUpdate("Add " + uDTO.getTemplateid() + " NEW EXPORT TEMPLETE", false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        EXPORTTEMPLETEDTOInter uDTO = (EXPORTTEMPLETEDTOInter) obj[0];
        String updateStat = "update $table set name = '$v_name', seperator = '$v_seperator',active = $v_active where id = $v_id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$v_name", "" + uDTO.getTemplatename());
        updateStat = updateStat.replace("$v_seperator", "" + uDTO.getSeparator());
        updateStat = updateStat.replace("$v_active", "" + uDTO.getActive());
        updateStat = updateStat.replace("$v_id", "" + uDTO.getTemplateid());
        super.executeUpdate(updateStat);
        super.postUpdate("Update " + uDTO.getTemplatename() + " EXPORT TEMPLETE", false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        EXPORTTEMPLETEDTOInter uDTO = (EXPORTTEMPLETEDTOInter) obj[0];
        String deleteStat = "delete from $table where id = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + uDTO.getTemplateid());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete " + uDTO.getTemplatename() + "  EXPORT TEMPLETE", false);
        return null;
    }

    public Object find(Object... obj) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        Integer reasonId = (Integer) obj[0];
        String selectStat = "select id, name, seperator, active from $table where id = $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + reasonId);
        ResultSet rs = executeQuery(selectStat);
        EXPORTTEMPLETEDTOInter uDTO = DTOFactory.createEXPORTTEMPLETEDTO();
        while (rs.next()) {
            uDTO.setTemplateid(rs.getInt("id"));
            uDTO.setTemplatename(rs.getString("name"));
            uDTO.setSeparator(rs.getString("seperator"));
            uDTO.setActive(rs.getInt("active"));
        }
        super.postSelect(rs);
        return uDTO;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select id, name, seperator, active from $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<EXPORTTEMPLETEDTOInter> uDTOL = new ArrayList<EXPORTTEMPLETEDTOInter>();
        while (rs.next()) {
            EXPORTTEMPLETEDTOInter uDTO = DTOFactory.createEXPORTTEMPLETEDTO();
            uDTO.setTemplateid(rs.getInt("id"));
            uDTO.setTemplatename(rs.getString("name"));
            uDTO.setSeparator(rs.getString("seperator"));
            uDTO.setActive(rs.getInt("active"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }
}
