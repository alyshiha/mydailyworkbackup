/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.CassetteDTOInter;

/**
 *
 * @author Administrator
 */
public interface CasseteBOInter extends BaseBOInter{

    Object getCurrency()throws Throwable;

    Object editeCassette(CassetteDTOInter cDTO, int operation) throws Throwable;

    Object getCassetes() throws Throwable;
Object getCurrencys() throws Throwable ;
}
