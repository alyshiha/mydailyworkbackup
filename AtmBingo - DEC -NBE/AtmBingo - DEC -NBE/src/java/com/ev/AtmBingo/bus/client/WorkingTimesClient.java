/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.bo.WorkingTimesBOIntert;
import com.ev.AtmBingo.bus.dto.SystemTimesDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author ISLAM
 */
public class WorkingTimesClient extends BaseBean  implements Serializable{

    private WorkingTimesBOIntert wtObject;
    private List<SystemTimesDTOInter> wtList;
    private SystemTimesDTOInter wtUpAttr;
    private SystemTimesDTOInter wtUpdateAttr;
    private Boolean showAdd;
    private Boolean showConfirm;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public SystemTimesDTOInter getWtUpAttr() {
        return wtUpAttr;
    }

    public void setWtUpAttr(SystemTimesDTOInter wtUpAttr) {
        this.wtUpAttr = wtUpAttr;
    }

    public List<SystemTimesDTOInter> getWtList() {
        return wtList;
    }

    public void setWtList(List<SystemTimesDTOInter> wtList) {
        this.wtList = wtList;
    }

    public SystemTimesDTOInter getWtUpdateAttr() {
        return wtUpdateAttr;
    }

    public void setWtUpdateAttr(SystemTimesDTOInter wtUpdateAttr) {
        this.wtUpdateAttr = wtUpdateAttr;
    }

    /** Creates a new instance of WorkingTimesClient */
    public WorkingTimesClient() throws Throwable {
        super();
        super.GetAccess();
        wtObject = BOFactory.createWorkingTimesBO(null);
        wtList = (List<SystemTimesDTOInter>) wtObject.getSystemTime();
        showAdd = true;
        showConfirm = false;
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void fillUpdate(ActionEvent e) {
        wtUpdateAttr = (SystemTimesDTOInter) e.getComponent().getAttributes().get("updateRow1");
        message = "";
    }

    public void updateRecord() {
        try {
            if(wtUpdateAttr.getStartTime() == null || wtUpdateAttr.getEndTime() == null){
                message = super.showMessage(REQ_FIELD);
            }
            else if (wtUpdateAttr.getStartTime().after(wtUpdateAttr.getEndTime())) {
                message = super.showMessage(INV_TIME);
            } else {
                wtObject.editeSystemTimes(wtUpdateAttr, wtObject.UPDATE);
                message = super.showMessage(SUCC);
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void resetDates(ActionEvent e) {
        wtUpAttr = (SystemTimesDTOInter) e.getComponent().getAttributes().get("updateRow");
        wtUpAttr.getEndTime();
        wtUpAttr.getStartTime();
        wtUpAttr.setEndTime(DateFormatter.getTimeFormat("23:59:59"));
        wtUpAttr.setStartTime(DateFormatter.getTimeFormat("00:00:00"));
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
