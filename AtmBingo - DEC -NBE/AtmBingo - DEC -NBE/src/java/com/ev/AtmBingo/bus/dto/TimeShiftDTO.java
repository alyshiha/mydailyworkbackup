/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import com.ev.AtmBingo.base.dto.BaseDTO;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.TimeShiftDetailDAOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TimeShiftDTO extends BaseDTO implements TimeShiftDTOInter,Serializable{

    private int shiftId;
    private String shiftAName;
    private String shiftEName;
    private List<TimeShiftDetailDTOInter> timeShiftDetails;

    public List<TimeShiftDetailDTOInter> getTimeShiftDetails()throws Throwable {
        TimeShiftDetailDAOInter tsdDAO = DAOFactory.createTimeShiftDetailDAO(null);
        timeShiftDetails = (List<TimeShiftDetailDTOInter>)tsdDAO.findByShiftId(getShiftId());
        return timeShiftDetails;
    }

    public void setTimeShiftDetails(List<TimeShiftDetailDTOInter> timeShiftDetails) {
        this.timeShiftDetails = timeShiftDetails;
    }

    
    public String getShiftAName() {
        return shiftAName;
    }

    public void setShiftAName(String shiftAName) {
        this.shiftAName = shiftAName;
    }

    public String getShiftEName() {
        return shiftEName;
    }

    public void setShiftEName(String shiftEName) {
        this.shiftEName = shiftEName;
    }

    public int getShiftId() {
        return shiftId;
    }

    public void setShiftId(int shiftId) {
        this.shiftId = shiftId;
    }

    
    protected TimeShiftDTO() {
        super();
    }

}
