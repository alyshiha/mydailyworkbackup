/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface ReplanishmentMasterDTOInter extends Serializable {
     Date getDailySheet();
     void setDailySheet(Date dailySheet);
     void setCardstaken(int cardstaken) ;
     int getCardstaken();
     boolean isCardtakenstate();
     void setCardtakenstate(boolean cardtakenstate) ;
public boolean getRowstatus();
void setRowstatus(boolean rowstatus);
    public String getComment();

    String getcalcflag();

    void setcalcflag(String calcflag);


    String getUname();

    void setUname(String uname);

    public void setComment(String comment);

    int getAtmId();

    int getCreatedBy();

    Date getCreatedDate();

    Date getDateFrom();

    Date getDateTo();

    int getId();

    void setAtmId(int atmId);

    void setCreatedBy(int createdBy);

    void setCreatedDate(Date createdDate);

    void setDateFrom(Date dateFrom);

    void setDateTo(Date dateTo);

    void setId(int id);
}
