/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.NetworkGroupDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author KhAiReE
 */
public class NetworkGroupDAO extends BaseDAO implements NetworkGroupDAOInter {

    protected NetworkGroupDAO() {
        super();
        super.setTableName("network_group");
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        NetworkGroupDTOInter ngDTO = (NetworkGroupDTOInter) obj[0];
        ngDTO.setGroupId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ( $group_id , '$group_name', '$network')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$group_id", "" + ngDTO.getGroupId());
        insertStat = insertStat.replace("$group_name", "" + ngDTO.getGroupName());
        insertStat = insertStat.replace("$network", "" + ngDTO.getNetwork());
        super.executeUpdate(insertStat);
        super.postUpdate("Add Network Group With Name" + ngDTO.getGroupName(), false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        NetworkGroupDTOInter ngDTO = (NetworkGroupDTOInter) obj[0];
        String updateStat = "update $table set group_name = '$group_name', network = '$network' where group_id = $group_id";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$group_id", "" + ngDTO.getGroupId());
        updateStat = updateStat.replace("$group_name", "" + ngDTO.getGroupName());
        updateStat = updateStat.replace("$network", "" + ngDTO.getNetwork());
        super.executeUpdate(updateStat);
        super.postUpdate("Update Network Group With Name" + ngDTO.getGroupName(), false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        NetworkGroupDTOInter ngDTO = (NetworkGroupDTOInter) obj[0];
        String deleteStat = "delete from $table where group_id = $group_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$group_id", "" + ngDTO.getGroupId());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Network Group With Name" + ngDTO.getGroupName(), false);
        return null;
    }

    public Object findAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "select group_id,group_name,network,rownum  from $table order by group_name";
        selectStat = selectStat.replace("$table", super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<NetworkGroupDTOInter> ngDTOL = new ArrayList<NetworkGroupDTOInter>();
        while (rs.next()) {
            NetworkGroupDTOInter ngDTO = DTOFactory.createNetworkGroupDTO();
            ngDTO.setGroupId(rs.getInt("group_id"));
            ngDTO.setGroupName(rs.getString("group_name"));
            ngDTO.setNetwork(rs.getString("network"));
            ngDTO.setRowid(rs.getInt("rownum"));
            ngDTOL.add(ngDTO);
        }
        postSelect(rs);
        return ngDTOL;
    }

    public Object find(int id) throws Throwable {
        preSelect();
        String selectStat = "select * from $table where group_id = $id order by group_name";
        selectStat = selectStat.replace("$table", super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        ResultSet rs = executeQuery(selectStat);
        NetworkGroupDTOInter ngDTO = DTOFactory.createNetworkGroupDTO();
        while (rs.next()) {

            ngDTO.setGroupId(rs.getInt("group_id"));
            ngDTO.setGroupName(rs.getString("group_name"));

            ngDTO.setNetwork(rs.getString("network"));
        }
        postSelect(rs);
        return ngDTO;
    }

    public Object search(Object... obj) throws Throwable {
        Object obj1 = super.preSelect();
        super.postSelect(obj1);
        return null;
    }
}
