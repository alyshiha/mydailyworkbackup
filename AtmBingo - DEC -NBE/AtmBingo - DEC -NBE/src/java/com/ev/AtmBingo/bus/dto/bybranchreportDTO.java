/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Aly
 */
public class bybranchreportDTO implements bybranchreportDTOInter , Serializable{

    private Date collecteddate;
    private String cardnum;
    private String status;
    private String branch;

    @Override
    public Date getCollecteddate() {
        return collecteddate;
    }

    @Override
    public void setCollecteddate(Date collecteddate) {
        this.collecteddate = collecteddate;
    }

    @Override
    public String getCardnum() {
        return cardnum;
    }

    @Override
    public void setCardnum(String cardnum) {
        this.cardnum = cardnum;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getBranch() {
        return branch;
    }

    @Override
    public void setBranch(String branch) {
        this.branch = branch;
    }

}
