/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.presdto;

import java.util.List;

/**
 *
 * @author Administrator
 */
public interface MenuItemDTOInter {

    String getItemId();

    String getTitle();

    void setItemId(String itemId);

    void setTitle(String title);

     List<MenuItemDTOInter> getSubItem();

    void setSubItem(List<MenuItemDTOInter> subItem) ;

}
