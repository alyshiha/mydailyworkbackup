/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.DisputeRepDTOInter;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Aly-Shiha
 */
public interface DisputeRepBOInter extends Serializable {

    String runReport(String dateFrom, String dateTo, String atmID, String user,String cust,Integer uservalue,Integer group) throws Throwable;

    List<DisputeRepDTOInter> runReportrxcel(String dateFrom, String dateTo, String atmID,Integer uservalue,Integer group) throws Throwable;
    
}
