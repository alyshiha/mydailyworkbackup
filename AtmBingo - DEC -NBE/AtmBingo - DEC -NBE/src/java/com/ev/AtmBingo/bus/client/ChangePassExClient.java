/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import static com.ev.AtmBingo.base.client.BaseBean.SUCC;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.ChangePassBOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.SystemTableDAOInter;
import com.ev.AtmBingo.bus.dto.SystemTableDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author Administrator
 */
public class ChangePassExClient extends BaseBean  implements Serializable{

    private String oldPass;
    private String newPass;
    private String confPass;
    private String message;
    private UsersDTOInter loggedIn;
    private ChangePassBOInter cpBO;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getConfPass() {
        return confPass;
    }

    public void setConfPass(String confPass) {
        this.confPass = confPass;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    /** Creates a new instance of ChangePassExClient */
    public ChangePassExClient() {
        super();
        super.GetAccess();
        try {
            cpBO = BOFactory.createChangePassBO(null);
                 FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
        } catch (Throwable ex) {
            Logger.getLogger(ChangePassFroUserClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        loggedIn = super.getLoggedInUser();
    }


     public void save() {
        try {
            SystemTableDAOInter stDAO = DAOFactory.createSystemTableDAO(null);
            SystemTableDTOInter stDTO = null;
            try {
                stDTO = (SystemTableDTOInter) stDAO.find("PASSLENGTH");


            } catch (Throwable ex) {
                Logger.getLogger(MainClient.class.getName()).log(Level.SEVERE, null, ex);
            }
            String matchinfo = "(?=^.{@passlength,}$)(?=.*\\d)(?=.*\\W+)(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$";
            matchinfo = matchinfo.replace("@passlength", stDTO.getParameterValue());
            if (!newPass.matches(matchinfo)) {
                message = "The input password volates the policy, please make sure that your password follows the following rules:"
                        + "Password length is "
                        + stDTO.getParameterValue()
                        + "characters or more."
                        + "It contains one or more capital letter(s)."
                        + "It contains a combination of characters, numbers and special characters.";
                
            } else {
                stDTO = (SystemTableDTOInter) stDAO.find("WRONG PASS NUMBER");
                if (oldPass.equals(newPass)) {
                    message = "New Password must not be equal your last "
                            + "passwords";
                    
                } else if (cpBO.validatePassword(newPass, loggedIn.getUserId())) {
                    message = "New Password must not be equal the last "
                            + stDTO.getParameterValue()
                            + "passwords";
                    
                } else {
                    cpBO.updatePass(loggedIn, oldPass, confPass, newPass,Boolean.FALSE);
                    message = "Done";
                    
                }
            }

        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }
  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
