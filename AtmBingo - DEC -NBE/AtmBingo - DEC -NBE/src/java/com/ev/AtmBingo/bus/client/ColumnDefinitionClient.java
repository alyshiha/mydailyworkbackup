/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.client;

import com.ev.AtmBingo.base.client.BaseBean;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.ColumnsDefinitionBOInter;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ISLAM
 */
public class ColumnDefinitionClient extends BaseBean  implements Serializable{

    private ColumnsDefinitionBOInter colObject;
    private List<ColumnDTOInter> colDefList;
    private ColumnDTOInter colAttr, newSP;
    private String message;
    private Integer listSize;
    private Boolean showAdd;
    private Boolean showConfirm;

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ColumnDTOInter> getColDefList() {
        return colDefList;
    }

    public void setColDefList(List<ColumnDTOInter> colDefList) {
        this.colDefList = colDefList;
    }

    /**
     * Creates a new instance of ColumnDefinitionClient
     */
    public ColumnDefinitionClient() throws Throwable {
        super();
        super.GetAccess();
        colObject = BOFactory.createColumnDefinitionBO(null);
        colDefList = (List<ColumnDTOInter>) colObject.findAllColoumn();
        listSize = colDefList.size();
             FacesContext context = FacesContext.getCurrentInstance();
         
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.addHeader("X-Frame-Options", "SAMEORIGIN");
    }

    public void AddCol(ActionEvent e) throws Throwable {
        colObject.AddCol();
    }

    public void fillUpdate(ActionEvent e) {
        colAttr = (ColumnDTOInter) e.getComponent().getAttributes().get("updateRow");
        resetVars();
    }

    public String TestColName(String ColName) {
        String MSG = "";
        int j = 0;
        for (int i = 0; i < colDefList.size(); i++) {
            if (colDefList.get(i).getHeader().toString().equals(ColName)) {
                j++;
                if (j > 1) {
                    return "Found";
                }
            }
        }
        return "Not Found";
    }

    public void updateColumnRecord() throws Throwable {
        try {
            String[] inputs = {colAttr.getClazzType(), colAttr.getDatatype(), colAttr.getDbName(), colAttr.getDuplicate_no(), colAttr.getExpression(), colAttr.getHeader(), colAttr.getProperty(), "" + colAttr.getActive(), "" + colAttr.getDisplayinloading(), "" + colAttr.getDisplayinmatching(), "" + colAttr.getDisplayintransaction(), "" + colAttr.getId(), "" + colAttr.getWidth()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
            Boolean found = Boolean.FALSE;
            for (String input : inputs) {
                if(input!=null){
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace("_", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
            }
            if (!found) {
                if (TestColName(colAttr.getHeader()).equals("Found")) {
                    message = "Douplicate Name";
                    return;
                }
                if (colAttr.getHeader().equals("")) {
                    message = super.showMessage(REQ_FIELD);
                } else {
                    colObject.editeColumnDefinition(colAttr, colObject.UPDATE);
                    message = super.showMessage(SUCC);
                }
            } else {
                message = "Please Enter Valid Data";
            }

        } catch (Throwable ex) {
            message = ex.getMessage();

        }
    }

    public void resetVars() {
        message = "";
    }

  public void onIdle() {
        System.out.println("idle");
        //logout();

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        HttpSession session = (HttpSession) req.getSession(true);
        Enumeration attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String sAttribute = attributeNames.nextElement().toString();
            session.removeAttribute(sAttribute);
        }
        session.invalidate();
        
    }}
