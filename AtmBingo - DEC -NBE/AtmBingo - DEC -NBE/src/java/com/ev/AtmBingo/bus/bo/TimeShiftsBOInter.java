/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import com.ev.AtmBingo.bus.dto.TimeShiftDTOInter;
import com.ev.AtmBingo.bus.dto.TimeShiftDetailDTOInter;

/**
 *
 * @author Administrator
 */
public interface TimeShiftsBOInter extends BaseBOInter{

    Object editeTimeShits(TimeShiftDTOInter tsDTO, int operation) throws Throwable;

    Object edteTimeShiftsDetail(TimeShiftDetailDTOInter tsdDTO,int oldDay,int operation)throws Throwable;

    Object getTimeShifts() throws Throwable;

}
