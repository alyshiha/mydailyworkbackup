/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.ProfileDTOInter;
import com.ev.AtmBingo.bus.dto.UserProfileDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ProfileDAO extends BaseDAO implements ProfileDAOInter {

    protected ProfileDAO() {
        super();
        super.setTableName("profile");
    }

    public boolean ProfileHasUser(ProfileDTOInter pDTO) throws Throwable {
        preSelect();
        String selectStat = "select count(*) asa from user_profile where profile_id = $profileid ";
        selectStat = selectStat.replace("$profileid", "" + pDTO.getProfileId());
        ResultSet rs = executeQuery(selectStat);
        int res = 0;
        while (rs.next()) {
            res = rs.getInt("asa");
        }
        if (res == 0) {
            return false;
        } else {
            return true;
        }
    }

    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        ProfileDTOInter pDTO = (ProfileDTOInter) obj[0];
        pDTO.setProfileId(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table values ($profile_id , '$profile_name',$restrected)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$profile_id", "" + pDTO.getProfileId());
        insertStat = insertStat.replace("$profile_name", "" + pDTO.getProfileName());
        insertStat = insertStat.replace("$restrected", "" + 2);
        super.executeUpdate(insertStat);
        super.postUpdate("Add Profile With Name" + pDTO.getProfileName(), false);
        return null;
    }

    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        ProfileDTOInter pDTO = (ProfileDTOInter) obj[0];
        String updateStat = "update $table set profile_name = '$profile_name', restrected = $rest where profile_id = $profile_id ";
        updateStat = updateStat.replace("$table", "" + super.getTableName());
        updateStat = updateStat.replace("$profile_id", "" + pDTO.getProfileId());
        updateStat = updateStat.replace("$profile_name", "" + pDTO.getProfileName());
        updateStat = updateStat.replace("$rest", "" + pDTO.getRestrected());
        super.executeUpdate(updateStat);
        super.postUpdate("Udpate Profile With Name" + pDTO.getProfileName(), false);
        return null;
    }

    public Object delete(Object... obj) throws Throwable {
        super.preUpdate();
        ProfileDTOInter pDTO = (ProfileDTOInter) obj[0];
        String deleteStat = "delete from $table where profile_id = $profile_id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$profile_id", "" + pDTO.getProfileId());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Profile With Name" + pDTO.getProfileName(), false);
        return null;
    }

    public Object findAll(Object... obj) throws Throwable {
        preSelect();
        int rest;
        if (obj.length > 0) {
             rest = (Integer) obj[0];
        } else {
            rest = 1;
        }
        String selectStat = "select * from $table order by profile_name";
        selectStat = selectStat.replace("$table", super.getTableName());
        selectStat = selectStat.replace("$rest", "" + rest);
        ResultSet rs = executeQuery(selectStat);
        List<ProfileDTOInter> pDTOL = new ArrayList<ProfileDTOInter>();
        while (rs.next()) {
            ProfileDTOInter pDTO = DTOFactory.createProfileDTO();
            pDTO.setProfileId(rs.getInt("profile_id"));
            pDTO.setProfileName(rs.getString("profile_name"));
            pDTO.setRestrected(rs.getInt("restrected"));

            pDTOL.add(pDTO);
        }
        postSelect(rs);
        return pDTOL;
    }

    public Object find(int id) throws Throwable {
        preSelect();
        String selectStat = "select * from $table where profile_id = $id order by profile_name";
        selectStat = selectStat.replace("$table", super.getTableName());
        selectStat = selectStat.replace("$id", "" + id);
        ResultSet rs = executeQuery(selectStat);
        ProfileDTOInter pDTO = DTOFactory.createProfileDTO();
        while (rs.next()) {

            pDTO.setProfileId(rs.getInt("profile_id"));
            pDTO.setProfileName(rs.getString("profile_name"));
            pDTO.setRestrected(rs.getInt("restrected"));

        }
        postSelect(rs);
        return pDTO;
    }

    public Object findAlluserprofile() throws Throwable {
        preSelect();
        String selectStat = "select * from user_profile ";
        ResultSet rs = executeQuery(selectStat);
        List<UserProfileDTOInter> res = new ArrayList<UserProfileDTOInter>();
        while (rs.next()) {
            UserProfileDTOInter pDTO = DTOFactory.createUserProfileDTO();
            pDTO.setUserId(rs.getInt("user_id"));
            pDTO.setProfileId(rs.getInt("profile_id"));
            res.add(pDTO);
        }
        postSelect(rs);
        return res;
    }

    public Object search(Object... obj) throws Throwable {
        Object obj1 = super.preSelect();
        super.postSelect(obj1);
        return null;
    }
}
