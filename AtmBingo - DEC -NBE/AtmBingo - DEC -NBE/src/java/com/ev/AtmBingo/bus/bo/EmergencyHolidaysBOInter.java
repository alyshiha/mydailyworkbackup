/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.base.bo.BaseBOInter;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface EmergencyHolidaysBOInter extends BaseBOInter {

   

    Boolean checkDate(Date from, Date to);

    Object editeEmgHolidays(Object obj ,Date oldPFrom ,int operation)throws Throwable;

    Object getEmgHolidays() throws Throwable;

}
