/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dao;

import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dto.DTOFactory;
import com.ev.AtmBingo.bus.dto.atmremainingDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class AtmRemainingRepDAO extends BaseDAO implements AtmRemainingRepDAOInter {

    protected AtmRemainingRepDAO() {
        super();
    }
    public Object findremainingcashexcelReport(String dateFrom, String dateTo, int atmGroupInt) throws Throwable {
        super.preSelect();
        String selectStatment = "select m.atm_id, (select a.name from atm_machine a where a.id = m.atm_id) atm_name,"
                + "(select a.application_id from atm_machine a where a.id = m.atm_id) atm_apllication_id, "
                + "sum(d.remaining_amount) remaining"
                + " from replanishment_master m , replanishment_detail d"
                + " where m.date_from between "
                + "to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and m.date_to between "
                + "to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and ((m.atm_id in (select id from atm_machine where atm_group in"
                + " (select id from  (select * from atm_group l order BY l.parent_id,name) start with id =  $P{AtmGroupInt} connect by prior id = parent_id))) or $P{AtmGroupInt} = 0)"
                + " and d.id = m.id and atm_valid(m.id) = 2"
                + " group by m.atm_id"
                + " order by remaining desc";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DatTo}", dateTo);
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        ResultSet rs = executeQueryReport(selectStatment);
        List<atmremainingDTOInter> Records = new ArrayList<atmremainingDTOInter>();
        while (rs.next()) {
            atmremainingDTOInter record = DTOFactory.createatmremainingDTO();
            record.setAtmapplicationid(rs.getString("atm_apllication_id"));
            record.setName(rs.getString("atm_name"));
            record.setRemaining(rs.getInt("remaining"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }
    public Object findReport(String dateFrom, String dateTo, int atmGroupInt) throws Throwable {
        super.preSelect();
        String selectStatment = "select m.atm_id, (select a.name from atm_machine a where a.id = m.atm_id) atm_name,"
                + "(select a.application_id from atm_machine a where a.id = m.atm_id) atm_apllication_id, "
                + "sum(d.remaining_amount) remaining"
                + " from replanishment_master m , replanishment_detail d"
                + " where m.date_from between "
                + "to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and m.date_to between "
                + "to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss') and to_date('$P{DatTo}','dd.MM.yyyy hh24:mi:ss')"
                + " and ((m.atm_id in (select id from atm_machine where atm_group in"
                + " (select id from  (select * from atm_group l order BY l.parent_id,name) start with id =  $P{AtmGroupInt} connect by prior id = parent_id))) or $P{AtmGroupInt} = 0)"
                + " and d.id = m.id and atm_valid(m.id) = 2"
                + " group by m.atm_id"
                + " order by remaining desc";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DatTo}", dateTo);
        selectStatment = selectStatment.replace("$P{AtmGroupInt}", "" + atmGroupInt);
        ResultSet rs = executeQueryReport(selectStatment);
        super.postSelect();
        return rs;
    }
}
