/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.bo;

import com.ev.AtmBingo.bus.dto.MissingJournalDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface MissingJournalsRepBOInter {

    String runReport(String dateFrom, String dateTo, String user,String cust) throws Throwable;
    
     List<MissingJournalDTOInter> runReportExcel(String dateFrom, String dateTo) throws Throwable ;
    

}
