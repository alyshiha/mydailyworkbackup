/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.bus.dto;

import java.util.Date;

/**
 *
 * @author Aly-Shiha
 */
public class DisputeRepDTO implements DisputeRepDTOInter {

    private String atmFullName;
    private Date dateFrom;
    private Date dateTo;
    private String cassetteValue;
    private String atmAppID;

    public DisputeRepDTO() {
    }

    public DisputeRepDTO(String atmFullName, Date dateFrom, Date dateTo, String cassetteValue, String atmAppID) {
        this.atmFullName = atmFullName;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.cassetteValue = cassetteValue;
        this.atmAppID = atmAppID;
    }

    @Override
    public String getAtmFullName() {
        return atmFullName;
    }

    @Override
    public void setAtmFullName(String atmFullName) {
        this.atmFullName = atmFullName;
    }

    @Override
    public Date getDateFrom() {
        return dateFrom;
    }

    @Override
    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    @Override
    public Date getDateTo() {
        return dateTo;
    }

    @Override
    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    @Override
    public String getCassetteValue() {
        return cassetteValue;
    }

    @Override
    public void setCassetteValue(String cassetteValue) {
        this.cassetteValue = cassetteValue;
    }

    @Override
    public String getAtmAppID() {
        return atmAppID;
    }

    @Override
    public void setAtmAppID(String atmAppID) {
        this.atmAppID = atmAppID;
    }

}
