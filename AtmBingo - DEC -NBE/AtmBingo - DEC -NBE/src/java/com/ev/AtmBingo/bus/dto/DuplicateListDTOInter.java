/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.AtmBingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface DuplicateListDTOInter  extends Serializable  {

    int getColID();

    String getColName();

    int getDupColID();

    String getDupColName();

    int getDupCol2ID();

    String getDupCol2Name();

    void setColID(int ColID);

    void setColName(String ColName);

    void setDupColID(int DupColID);

    void setDupColName(String DupColName);

      void setDupCol2ID(int DupCol2ID);

    void setDupCol2Name(String DupCol2Name);

}
