/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.base.util;

import com.ev.AtmBingo.bus.dao.ColumnDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.DisputeReportDTOInter;
import com.ev.AtmBingo.bus.dto.DisputesDTOInter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRReportFont;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JRDesignReportFont;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;

/**
 *
 * @author Administrator
 */
public class ReportingTool  implements Serializable{

    private JRResultSetDataSource resultSetDataSource;
private  JRBeanCollectionDataSource JRBeanCollection;


       public  ReportingTool(List<DisputeReportDTOInter> rs, ArrayList columns, boolean rejected,String fields) {
        List<DisputeReportDTOInter> r = rs;
        generateReportList(r, columns, rejected,fields);
        JRBeanCollection = new JRBeanCollectionDataSource(rs);
    }

    private void generateReportList( List<DisputeReportDTOInter> rs, ArrayList columns, boolean rejected,String  fields) {
        Writer writer = null;
        VelocityEngine ve = new VelocityEngine();
        ArrayList fieldList = new ArrayList();
        Field field;
        ArrayList columnList1 = new ArrayList();
        ArrayList columnList = columns;
        Column column;
//        ColumnDTOInter col;

        try {
          //  ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);
            Properties properties = new Properties();
            properties.setProperty("file.resource.loader.path",
                    "c:/BingoReports/");

            Velocity.init(properties);

            VelocityContext context = new VelocityContext();
            Template template;
            // Load template file
            if (rejected) {
                template = Velocity.getTemplate("BasicReportR.vm");
            } else {
                template = Velocity.getTemplate("BasicReport.vm");
            }

            // Get result set meta data
            String[] FieldGet = fields.split(",");


            int numColumns = FieldGet.length;



            // Get the column names; column indices start from 1
            for (int i = 1; i < numColumns + 1; i++) {
                field = new Field();
                field.setName(FieldGet[i].toString());
                if (FieldGet[i].toString().contains("Date")) {
                    field.setClazzName("java.sql.Timestamp");
                } else {
                    try{Integer.parseInt(FieldGet[i].toString());field.setClazzName("java.sql.Integer");}catch(Exception ex){field.setClazzName("java.sql.String");}
                }

                fieldList.add(field);
                column = new Column();
                // column = (ColumnDTOInter) cDAO.searchByColumnName(rsmd.getColumnName(i));
                column.setName(FieldGet[i].toString());
                column.setExpression(buildExpression(FieldGet[i].toString()));
//                column.setWidth(100);
//                column.setAlignment("Center");
                 if (FieldGet[i].toString().contains("Date")) {
                    field.setClazzName("java.sql.Timestamp");
                } else {
                    try{Integer.parseInt(FieldGet[i].toString());field.setClazzName("java.sql.Integer");}catch(Exception ex){field.setClazzName("java.sql.String");}
                }
                columnList1.add(column);
            }
            int r = 0;
            int i = 0;
            for (Column d : (ArrayList<Column>) columnList1) {
                if (!rejected) {
                    if (i != 0 && i != 1) {
                        if (r < columnList.size()) {
                            ((ArrayList<ColumnDTOInter>) columnList).get(r).setClazzType(d.getClazzType());
                            ((ArrayList<ColumnDTOInter>) columnList).get(r).setExpression(d.getExpression());
                            r++;
                        } else {
                            break;
                        }
                    } else {
                        i++;
                    }
                } else {
                    if (i != 0) {
                        if (r < columnList.size()) {
                            ((ArrayList<ColumnDTOInter>) columnList).get(r).setClazzType(d.getClazzType());
                            ((ArrayList<ColumnDTOInter>) columnList).get(r).setExpression(d.getExpression());
                            r++;
                        } else {
                            break;
                        }
                    } else {
                        i++;
                    }
                }
            }
            context.put("columnList", columnList);
            context.put("fieldList", fieldList);
            context.put("user", buildParameter("user"));
            context.put("title", buildParameter("title"));
            context.put("params", buildParameter("params"));
            context.put("customer", buildParameter("customer"));
            context.put("totaltrans", buildParameter("totaltrans"));
            context.put("PAGE_NUMBER", buildVariable("PAGE_NUMBER"));

            context.put("ATM", buildExpression("ATM_APPLICATION_ID"));
            context.put("UNIT", buildExpression("UNIT_ID"));
            context.put("CURRENCY", buildExpression("CURRENCY"));
            context.put("NAME", buildExpression("NAME"));

            context.put("RECORD_TYPE", buildExpression("RECORD_TYPE"));
            if (!rejected) {
                context.put("AMOUNT", buildExpression("AMOUNT"));
                context.put("COMMENTS", buildExpression("COMMENTS"));
                context.put("SETTLEDFLAG", buildExpression("SETTLED_FLAG"));
                context.put("AMNT", buildExpression("AMNT"));
                context.put("amn_1", buildVariable("amn_1"));
                context.put("amt_2", buildVariable("amt_2"));
            }

            context.put("RECORDTYPE", buildExpression("RECORD_TYPE"));

            // Merge template with the context in
            writer = new StringWriter();
            template.merge(context, writer);

            if (rejected) {
                FileWriter fw = new FileWriter("c:/BingoReports/BasicReportR.jrxml");

                fw.write(writer.toString());
                fw.close();
            } else {
                FileWriter fw = new FileWriter("c:/BingoReports/BasicReport.jrxml");

                fw.write(writer.toString());
                fw.close();
            }

        } catch (Throwable e) {
        }
    }




    public ReportingTool(ResultSet rs, ArrayList columns, boolean rejected) {
        ResultSet r = rs;
        generateReport(r, columns, rejected);
        resultSetDataSource = new JRResultSetDataSource(rs);
    }

    private void generateReport(ResultSet rs, ArrayList columns, boolean rejected) {
        Writer writer = null;
        VelocityEngine ve = new VelocityEngine();
        ArrayList fieldList = new ArrayList();
        Field field;
        ArrayList columnList1 = new ArrayList();
        ArrayList columnList = columns;
        Column column;
//        ColumnDTOInter col;

        try {
            ColumnDAOInter cDAO = DAOFactory.createColumnDAO(null);
            Properties properties = new Properties();
            properties.setProperty("file.resource.loader.path",
                    "c:/BingoReports/");

            Velocity.init(properties);

            VelocityContext context = new VelocityContext();
            Template template;
            // Load template file
            if (rejected) {
                template = Velocity.getTemplate("BasicReportR.vm");
            } else {
                template = Velocity.getTemplate("BasicReport.vm");
            }

            // Get result set meta data
            ResultSetMetaData rsmd = rs.getMetaData();
            int numColumns = rsmd.getColumnCount();



            // Get the column names; column indices start from 1
            for (int i = 1; i < numColumns + 1; i++) {
                field = new Field();
                field.setName(rsmd.getColumnName(i));
                if (rsmd.getColumnClassName(i).contains("Date")) {
                    field.setClazzName("java.sql.Timestamp");
                } else {
                    field.setClazzName(rsmd.getColumnClassName(i));
                }

                fieldList.add(field);
                column = new Column();
                // column = (ColumnDTOInter) cDAO.searchByColumnName(rsmd.getColumnName(i));
                column.setName(rsmd.getColumnName(i));
                column.setExpression(buildExpression(rsmd.getColumnName(i)));
//                column.setWidth(100);
//                column.setAlignment("Center");
                if (rsmd.getColumnClassName(i).contains("Date")) {
                    column.setClazzType("java.sql.Timestamp");
                } else {
                    column.setClazzType(rsmd.getColumnClassName(i));
                }
                columnList1.add(column);
            }
            int r = 0;
            int i = 0;
            for (Column d : (ArrayList<Column>) columnList1) {
                if (!rejected) {
                    if (i != 0 && i != 1) {
                        if (r < columnList.size()) {
                            ((ArrayList<ColumnDTOInter>) columnList).get(r).setClazzType(d.getClazzType());
                            ((ArrayList<ColumnDTOInter>) columnList).get(r).setExpression(d.getExpression());
                            r++;
                        } else {
                            break;
                        }
                    } else {
                        i++;
                    }
                } else {
                    if (i != 0) {
                        if (r < columnList.size()) {
                            ((ArrayList<ColumnDTOInter>) columnList).get(r).setClazzType(d.getClazzType());
                            ((ArrayList<ColumnDTOInter>) columnList).get(r).setExpression(d.getExpression());
                            r++;
                        } else {
                            break;
                        }
                    } else {
                        i++;
                    }
                }
            }
            context.put("columnList", columnList);
            context.put("fieldList", fieldList);
            context.put("user", buildParameter("user"));
            context.put("title", buildParameter("title"));
            context.put("params", buildParameter("params"));
            context.put("customer", buildParameter("customer"));
            context.put("totaltrans", buildParameter("totaltrans"));
            
            context.put("PAGE_NUMBER", buildVariable("PAGE_NUMBER"));
            

            context.put("ATM", buildExpression("ATM_APPLICATION_ID"));
            context.put("UNIT", buildExpression("UNIT_ID"));
            context.put("CURRENCY", buildExpression("CURRENCY"));
            context.put("NAME", buildExpression("NAME"));

            context.put("RECORD_TYPE", buildExpression("RECORD_TYPE"));
            if (!rejected) {
                context.put("AMOUNT", buildExpression("AMOUNT"));
                context.put("COMMENTS", buildExpression("COMMENTS"));
                context.put("SETTLEDFLAG", buildExpression("SETTLED_FLAG"));
                context.put("AMNT", buildExpression("AMNT"));
                context.put("amn_1", buildVariable("amn_1"));
                context.put("amt_2", buildVariable("amt_2"));
            }

            context.put("RECORDTYPE", buildExpression("RECORD_TYPE"));

            // Merge template with the context in
            writer = new StringWriter();
            template.merge(context, writer);

            if (rejected) {
                FileWriter fw = new FileWriter("c:/BingoReports/BasicReportR.jrxml");
                fw.write(writer.toString());
                fw.close();
            } else {
                FileWriter fw = new FileWriter("c:/BingoReports/BasicReport.jrxml");
                fw.write(writer.toString());
                fw.close();
            }
        } catch (Throwable e) {
        }
    }

    private String buildExpression(String fieldName) {
        String expression;

        expression = "$F{" + fieldName + "}";

        return expression;
    }

    private String buildVariable(String fieldName) {
        String expression;

        expression = "$V{" + fieldName + "}";

        return expression;
    }

    private String buildParameter(String fieldName) {
        String expression;

        expression = "$P{" + fieldName + "}";

        return expression;
    }

    public String generate(String title, String user, String reportParams, boolean rejected, String cust, BigDecimal totalAmount,String len) throws FileNotFoundException, IOException {

        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("title", title);
        params.put("user", user);
        params.put("params", reportParams);
        params.put("customer", cust);
        params.put("totalAmount", totalAmount);
    params.put("totaltrans", len);

        try {

            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design;
            FileWriter f;
            if (rejected) {
                
                design = JRXmlLoader.load("c:/BingoReports/BasicReportR.jrxml");


                f = new FileWriter("c:/BingoReports/BasicReportR.jrxml");
            } else {
                design = JRXmlLoader.load("c:/BingoReports/BasicReport.jrxml");


                f = new FileWriter("c:/BingoReports/BasicReport.jrxml");
            }
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);

            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, resultSetDataSource);


            JRReportFont font = new JRDesignReportFont();
            font.setPdfFontName("c:/windows/fonts/arial.ttf");
            font.setPdfEncoding(com.lowagie.text.pdf.BaseFont.IDENTITY_H);
            font.setPdfEmbedded(true);

            jasperPrint.setDefaultFont(font);

            Calendar c = Calendar.getInstance();
            String uri = title + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            //JasperExportManager.exportReportToPdfFile(jasperPrint, "c:/Reports/" + title + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf");

            f.write("");
            f.close();
            
            try {

                String x = "../PDF/" + uri;
                return x;


            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            
            return "fail";
        }
    }

    public static void main(String[] args) throws Throwable {
//        MatchedDataDAOInter mdDAO = DAOFactory.createMatchedDataDAO(null);
//        ResultSet rs = (ResultSet) mdDAO.findReport("transaction_date,transaction_sequence,"
//                + "amount,transaction_type,customer_account_number,currency", "match_key in (17351746,17351747)");
//
////        ReportingTool rt = new ReportingTool(rs);
//        rt.generate("Matched Transaction(Journals)", "Admin", "From : 10/10/2011 To : 20/10/2011\nATM ID: 1234");
    }
}
