/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.base.client;

import DBCONN.Session;
import com.ev.AtmBingo.bus.bo.BOFactory;
import com.ev.AtmBingo.bus.bo.menulabelsBOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dao.SystemTableDAOInter;
import com.ev.AtmBingo.bus.dto.SystemTableDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class BaseBean implements Serializable {

    public static final int SUCC = 1;
    public static final int REQ_FIELD = 2;
    public static final int CONF_FIRST = 3;
    public static final int INVA_DAY = 4;
    public static final int REC_EXIST = 5;
    public static final int MACH_TYPE = 6;
    public static final int CHECK_DATE = 7;
    public static final int INV_TIME = 8;
    public static final int CNT_DEL = 9;
    private List<String> months;
    private List<String> rulesValue;
    private List<String> trans;
    private List<String> trans_op;
    private List<String> settledList;
    private List<String> MESSAGES;
    private String transBtn;

    public String getTransBtn() {
        return transBtn;
    }

    public void setTransBtn(String transBtn) {
        this.transBtn = transBtn;
    }

    public BaseBean() {
        System.setProperty("org.apache.el.parser.COERCE_TO_ZERO", "false");

        rulesValue = new ArrayList<String>();
        months = new ArrayList<String>();
        trans = new ArrayList<String>();
        trans_op = new ArrayList<String>();
        settledList = new ArrayList<String>();
        MESSAGES = new ArrayList<String>();
        MESSAGES.add(0, "");
        MESSAGES.add(SUCC, "Done Successfully");
        MESSAGES.add(REQ_FIELD, "Please insert required fields");
        MESSAGES.add(CONF_FIRST, "Please confirm first");
        MESSAGES.add(INVA_DAY, "Invalid day");
        MESSAGES.add(REC_EXIST, "Record already exists");
        MESSAGES.add(MACH_TYPE, "There are no other machine types");
        MESSAGES.add(CHECK_DATE, "Invalid Date To");
        MESSAGES.add(INV_TIME, "Invalid time To");
        MESSAGES.add(CNT_DEL, "Can't be deleted , it has dependecies");
    }

    public void redirect() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect("Login.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(BaseBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void GetAccess() {
        try {
            String Page = getRequestURL();
            Page = Page.replace(Page.substring(0, Page.indexOf("faces/")), "").replace("faces/", "");

            menulabelsBOInter MenuTest = BOFactory.createmenulabelsBO();
            if (!MenuTest.ValidatePage(getLoggedInUser().getUserId(), Page)) {
                FacesContext context = FacesContext.getCurrentInstance();
                HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                response.sendRedirect("Login.xhtml");
            }
        } catch (Throwable ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            try {
                response.sendRedirect("Login.xhtml");
            } catch (IOException ex1) {
                Logger.getLogger(BaseBean.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    public String getRequestURL() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String url = request.getRequestURI().toString();
        return url;
    }

    public void clearsessionattributes() throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session != null) {
            Enumeration attributeNames = session.getAttributeNames();
            while (attributeNames.hasMoreElements()) {
                String sAttribute = attributeNames.nextElement().toString();
                session.removeAttribute(sAttribute);
            }
        }
    }

    public void OpenHTTPSession() {
        FacesContext context = FacesContext.getCurrentInstance();

        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
    }

    public List<String> getRulesValue() {
        rulesValue.add("=");
        rulesValue.add(">");
        rulesValue.add("<");
        rulesValue.add(">=");
        rulesValue.add("<=");
        rulesValue.add("<>");
        rulesValue.add("In");
        rulesValue.add("Not In");
        rulesValue.add("Like");
        rulesValue.add("Not Like");
        rulesValue.add("is Null");
        rulesValue.add("is Not Null");
        rulesValue.add("Between");
        return rulesValue;
    }

    public void setRulesValue(List<String> rulesValue) {
        this.rulesValue = rulesValue;
    }

    public String showMessage(int msg) {
        return MESSAGES.get(msg);
    }

    public List<String> getSettledList() {
        settledList.add("Not Settled.");
        settledList.add("Settled.");
        settledList.add("All");
        return settledList;
    }

    public void setSettledList(List<String> settledList) {
        this.settledList = settledList;
    }

    public List<String> getTrans() {
        trans.add("=");
        trans.add(">");
        trans.add("<");
        trans.add(">=");
        trans.add("<=");
        trans.add("<>");
        trans.add("Between");
        return trans;
    }

    public void setTrans(List<String> trans) {
        this.trans = trans;
    }

    public List<String> getTrans_op() {
        trans_op.add("=");
        trans_op.add("<>");
        trans_op.add("Like");
        trans_op.add("Not Like");
        trans_op.add("is null");
        trans_op.add("is not null");
        return trans_op;
    }

    public void setTrans_op(List<String> trans_op) {
        this.trans_op = trans_op;
    }

    public List<String> getMonths() {
        months.add("January");
        months.add("February");
        months.add("March");
        months.add("April");
        months.add("May");
        months.add("June");
        months.add("July");
        months.add("August");
        months.add("September");
        months.add("October");
        months.add("November");
        months.add("December");
        return months;
    }

    public void setMonths(List<String> months) {
        this.months = months;
    }

    public int getRestrected() throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();

        Session sessionutil = new Session();
        UsersDTOInter uDTO = sessionutil.GetUserLogging();
        return uDTO.getSeeRestrected();
    }

    public UsersDTOInter getLoggedInUser() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();

            Session sessionutil = new Session();
            UsersDTOInter uDTO = sessionutil.GetUserLogging();
            return uDTO;
        } catch (Throwable ex) {
            Logger.getLogger(BaseBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String getIpAddress() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
        String ip = req.getRemoteAddr();
        return ip;
    }

    public Date getTodaytrans(String time) {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String[] sArr = time.split(":");
        cal.add(Calendar.DATE, Integer.valueOf(GetTimeFrom()));
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sArr[0]));
        cal.set(Calendar.MINUTE, Integer.parseInt(sArr[1]));
        cal.set(Calendar.SECOND, Integer.parseInt(sArr[2]) - 1);

        String s = dateFormat.format(cal.getTime());
        Date d = dateFormat.getCalendar().getTime();
        return d;

    }

    public String GetTimeFrom() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("DateFromVariable") == null) {
            session.setAttribute("DateFromVariable", (String) getParameter("TIMETO"));
        }
        return (String) session.getAttribute("DateFromVariable");
    }

    public String GetTimeTo() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("DateToVariable") == null) {
            session.setAttribute("DateToVariable", (String) getParameter("TIMEFROM"));
        }
        return (String) session.getAttribute("DateToVariable");
    }

    public Date getYesterdaytrans(String time) {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        cal.add(Calendar.DATE, Integer.valueOf(GetTimeTo()));
        String[] sArr = time.split(":");
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sArr[0]));
        cal.set(Calendar.MINUTE, Integer.parseInt(sArr[1]));
        cal.set(Calendar.SECOND, Integer.parseInt(sArr[2]));
        String s = dateFormat.format(cal.getTime());
        Date d = dateFormat.getCalendar().getTime();
        return d;
    }

    public Object getParameter(String paramName) {
        SystemTableDAOInter stDAO = DAOFactory.createSystemTableDAO(null);
        SystemTableDTOInter stDTO = null;
        try {
            stDTO = (SystemTableDTOInter) stDAO.find(paramName);
            return stDTO.getParameterValue();
        } catch (Throwable ex) {
            return null;
        }
    }

    public static void main(String[] args) {
        BaseBean b = new BaseBean();
        b.getYesterdaytrans("00:00:00");
    }
}
