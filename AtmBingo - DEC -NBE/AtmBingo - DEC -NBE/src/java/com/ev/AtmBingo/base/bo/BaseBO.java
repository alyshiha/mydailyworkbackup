/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.base.bo;

import DBCONN.Session;
import com.ev.AtmBingo.base.dao.BaseDAO;
import com.ev.AtmBingo.bus.dao.AtmGroupDAOInter;
import com.ev.AtmBingo.bus.dao.AtmMachineDAOInter;
import com.ev.AtmBingo.bus.dao.DAOFactory;
import com.ev.AtmBingo.bus.dto.AtmGroupDTOInter;
import com.ev.AtmBingo.bus.dto.AtmMachineDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class BaseBO implements Serializable {

    public Object getAtmGrp() throws Throwable {
        AtmGroupDAOInter agDAo = DAOFactory.createAtmGroupDAO(null);
        List<AtmGroupDTOInter> agDTOL = (List<AtmGroupDTOInter>) agDAo.findAll();
        return agDTOL;
    }

    public Integer getLoggedInUser() {
        try {
            Session sessionutil = new Session();
            UsersDTOInter currentUser = sessionutil.GetUserLogging();
            Integer uDTO = currentUser.getUserId();
            return uDTO;
        } catch (Exception ex) {
            return 0;
        } catch (Throwable ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String getGrpName(int grpId) throws Throwable {
        AtmGroupDAOInter agDAo = DAOFactory.createAtmGroupDAO(null);
        AtmGroupDTOInter agDTO = (AtmGroupDTOInter) agDAo.find(grpId);
        return agDTO.getName();
    }

    public String getAtmName(int atmId) throws Throwable {
        AtmMachineDAOInter agDAo = DAOFactory.createAtmMachineDAO(null);
        AtmMachineDTOInter agDTO = (AtmMachineDTOInter) agDAo.find(atmId);
        return agDTO.getName();
    }

    public Date getTime() throws Throwable {
        BaseDAO bDAO = new BaseDAO();
        Date now = bDAO.getCurrentTime();
        return now;
    }
}
