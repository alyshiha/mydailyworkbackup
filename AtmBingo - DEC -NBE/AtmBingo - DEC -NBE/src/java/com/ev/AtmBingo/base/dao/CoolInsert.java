/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.base.dao;

import DBCONN.CoonectionHandler;
import DBCONN.Session;
import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.bus.dao.DisputesDAOInter;
import com.ev.AtmBingo.bus.dto.DisputeReportDTOInter;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class CoolInsert implements Serializable {

    public Object GetStatment() throws Throwable {

        //DisputeReportDTOInter uDTO = (DisputeReportDTOInter) obj[0];
        String insertStat = "insert into disputes_report\n"
                + "(column1, column2, column3, column4, column5,notes_presented,transaction_status,card_no_suffix,settled_date,transaction_type,transaction_sequence_order_by,customer_account_number,transaction_sequence, card_no,transaction_date,atm_application_id,amount,currency,response_code,matching_type, record_type, dispute_key,settled_flag,comments,dispute_reason,USER_ID)\n"
                + "values\n"
                + "  (?, ?, ?, ?, ?, ?, ?, ?, to_date(?,'dd.mm.yyyy hh24:mi:ss'), ?, ?, ?, ?, ?, to_date(?,'dd.mm.yyyy hh24:mi:ss'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
        return insertStat;
    }

    public Integer getLoggedInUser() {
        try {
            Session sessionutil = new Session();
            UsersDTOInter currentUser = sessionutil.GetUserLogging();
            Integer uDTO = currentUser.getUserId();
            return uDTO;
        } catch (Exception ex) {
            return 0;
        } catch (Throwable ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void save(List<DisputeReportDTOInter> entities) throws SQLException, Throwable {
        Connection connection = null;
        PreparedStatement statement = null;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {

            connection = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            statement = connection.prepareStatement(GetStatment().toString());
            for (int i = 0; i < entities.size(); i++) {
                DisputeReportDTOInter uDTO = entities.get(i);
                //  statement.setString(1, "");
                if (uDTO.getColumn1() != null) {
                    statement.setString(1, uDTO.getColumn1());
                } else {
                    statement.setString(1, uDTO.getColumn1());
                }

                if (uDTO.getColumn2() != null) {
                    statement.setString(2, uDTO.getColumn2());
                } else {
                    statement.setString(2, uDTO.getColumn2());
                }

                if (uDTO.getColumn3() != null) {
                    statement.setString(3, uDTO.getColumn3());
                } else {
                    statement.setString(3, uDTO.getColumn3());
                }

                if (uDTO.getColumn4() != null) {
                    statement.setString(4, uDTO.getColumn4());
                } else {
                    statement.setString(4, uDTO.getColumn4());
                }

                if (uDTO.getColumn5() != null) {
                    statement.setString(5, uDTO.getColumn5());
                } else {
                    statement.setString(5, uDTO.getColumn5());
                }
                statement.setString(6, uDTO.getNotesPresented());
                statement.setString(7, uDTO.getTransactionStatus());
                statement.setInt(8, uDTO.getCardNoSuffix());
                statement.setString(9, DateFormatter.changeDateAndTimeFormat(uDTO.getSettlementDate()).toString());
                statement.setString(10, uDTO.getTransactionType());
                statement.setInt(11, uDTO.getTransactionSequenceOrderBy());
                if (uDTO.getCustomerAccountNumber() != null) {
                    statement.setString(12, uDTO.getCustomerAccountNumber());
                } else {
                    statement.setString(12, "");
                }
                statement.setString(13, uDTO.getTransactionSeqeunce());
                statement.setString(14, uDTO.getCardNo());
                statement.setString(15, DateFormatter.changeDateAndTimeFormat(uDTO.getTransactionDate()).toString());
                statement.setString(16, uDTO.getAtmApplicationId());
                statement.setInt(17, uDTO.getAmount());
                statement.setString(18, uDTO.getCurrency());
                statement.setString(19, uDTO.getResponseCode());
                statement.setInt(20, uDTO.getMatchingType());
                statement.setInt(21, uDTO.getRecordType());
                statement.setInt(22, uDTO.getDisputeKey());
                statement.setInt(23, uDTO.getSettledFlag());
                if (uDTO.getComments() != null) {
                    statement.setString(24, uDTO.getComments());
                } else {
                    statement.setString(24, "");
                }
                statement.setString(25, uDTO.getDisputeReason());
                statement.setInt(26, getLoggedInUser());
                statement.addBatch();

                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch(); // Execute every 1000 items.
                }
            }
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
    }
}
