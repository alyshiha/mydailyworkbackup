package com.ev.AtmBingo.base.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import java.util.Properties;

public final class PropertyReader  implements Serializable{
    
    private String propertiesFileName = null;
    private static Properties props = null;
    
    private PropertyReader(String propertiesFileName) {
        try {
            this.propertiesFileName = propertiesFileName;
            props = new Properties();
            InputStream in = 
                PropertyReader.class.getResourceAsStream(propertiesFileName);
            props.load(in);

            //PropertyConfigurator.configure(props);
        } catch (FileNotFoundException fnfe) {
            System.out.println("PropertyReader.java - FileNotFoundException : " + 
                               fnfe.toString());
        } catch (IOException ioe) {
            System.out.println("PropertyReader.java - IOException : " + 
                               ioe.toString());
        }
    }

    public static void main(String[] args) {
        //PropertyReader propertyReader = new PropertyReader("conn.properties");
        String s = PropertyReader.getProperty("ev.db_server_ip");
        
    }

    private String getPropertiesFileName() {
        return propertiesFileName;
    }

    private Properties getProps() {
        return props;
    }
    
    public static String getProperty(String prop) {
        if(props==null){
            new PropertyReader("/com/ev/AtmBingo/conn.properties");
        }
        return props.getProperty(prop);
    }
}