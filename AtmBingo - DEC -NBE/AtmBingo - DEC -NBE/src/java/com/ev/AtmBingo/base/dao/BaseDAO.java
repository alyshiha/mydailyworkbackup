package com.ev.AtmBingo.base.dao;

import DBCONN.CoonectionHandler;
import DBCONN.Session;
import com.ev.AtmBingo.bus.dto.UsersDTOInter;
import com.sun.crypto.provider.RSACipher;
import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class BaseDAO implements Serializable {

    private String tableName;

    public synchronized String getTableName() {
        return tableName;
    }

    public synchronized void setTableName(String tableName) {
        this.tableName = tableName;
    }

    protected synchronized Object preUpdate(Object... obj1) {
        return null;
    }

    protected synchronized Object postUpdate(Object... obj1) {
        String action = (String) obj1[0];
        boolean exc = (Boolean) obj1[1];
        Integer userId = getLoggedInUser();
        String statement = "";
        if (!exc) {
            statement = "insert into bingo_log values(bingo_log_seq.nextval,sysdate,$userId,'127.0.0.1','$action')";
            statement = statement.replace("$userId", "" + userId);

            statement = statement.replace("$action", "" + action.replaceAll("'", ""));
            try {
                executeUpdate(statement);
            } catch (Exception ex) {
                Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "done";
    }

    protected synchronized Object preSelect(Object... obj1) {
        return null;
    }

    protected synchronized Object postSelect(Object... obj1) throws Exception {
        if (obj1.length > 0) {
            ResultSet rs = (ResultSet) obj1[0];
            Connection Con = rs.getStatement().getConnection();
            Statement Stat = rs.getStatement();
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {;
                }
                rs = null;
            }
            if (Stat != null) {
                try {
                    Stat.close();
                } catch (SQLException e) {;
                }
                Stat = null;
            }
            if (Con != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(Con);
                } catch (SQLException e) {;
                }
                Con = null;
            }
        } else {
            //System.out.println("NO");
        }
        return null;
    }

    protected synchronized Object preCollable(Object... obj1) {
        return null;
    }

    protected synchronized Object postCollable(Object... obj1) throws ClassNotFoundException, Exception {
        try {
            if (obj1.length > 0) {

                CallableStatement Stat = (CallableStatement) obj1[0];
                Connection Con = Stat.getConnection();
                if (Stat != null) {
                    try {
                        Stat.close();
                    } catch (SQLException e) {;
                    }
                    Stat = null;
                }
                if (Con != null) {
                    try {
                        CoonectionHandler.getInstance().returnConnection(Con);
                    } catch (SQLException e) {;
                    }
                    Con = null;
                }
            } else {
                //System.out.println("NO");
            }

        } catch (SQLException ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);

        }

        return null;
    }

    public BaseDAO() {
    }

    public synchronized ResultSet executeQueryFirst(String sql) throws ClassNotFoundException, Exception {

        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            Statement statement = conn.createStatement();

            ResultSet rs = statement.executeQuery(sql);
            // System.out.println(sql);
            return rs;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return null;
    }

    public synchronized ResultSet executeQuery(String sql) throws ClassNotFoundException, Exception {

        try {
            
            //System.out.println(sql);
            Connection conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(sql);
           
            return rs;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return null;
    }

    public synchronized void OnlineUser() throws Throwable {
        Integer User = getLoggedInUser();
        if (User != 1) {
            String SQL = "update users s set s.last_online_time = sysdate where s.user_id = " + User;
            executeUpdate(SQL);
        }
    }

    public synchronized ResultSet executeQueryJobMon(String sql) throws SQLException, ClassNotFoundException, Exception {
        //  System.out.println(sql);

        Connection conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
        Statement statement = conn.createStatement();
        //System.out.println(sql);
        ResultSet rs = statement.executeQuery(sql);

        return rs;
    }

    public synchronized ResultSet executeQueryReport(String sql) throws SQLException, ClassNotFoundException, Exception {

        Connection conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
        Statement statement = conn.createStatement();
        //System.out.println(sql);
        ResultSet rs = statement.executeQuery(sql);

        return rs;
    }

    public synchronized Integer executeUpdateReport(String sql) throws SQLException, ClassNotFoundException, Exception {
        // IDataProvider prov = DataProviderFactory.createPoolProvider();
        //  System.out.println(sql);

        Connection conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
        Statement statement = conn.createStatement();
        int rc = statement.executeUpdate(sql);

        try {
            CoonectionHandler.getInstance().returnConnection(conn);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BaseDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return rc;
    }

    public synchronized Integer executeUpdate(String sql) {
        try {
            // IDataProvider prov = DataProviderFactory.createPoolProvider();

            Connection conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
            Statement statement = conn.createStatement();
            int rc = statement.executeUpdate(sql);
            try {
                CoonectionHandler.getInstance().returnConnection(conn);

            } catch (ClassNotFoundException ex) {
                Logger.getLogger(BaseDAO.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
            return rc;

        } catch (SQLException ex) {
            Logger.getLogger(BaseDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(BaseDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public synchronized int generateSequence(String tableName) throws SQLException, ClassNotFoundException, Exception {

        Connection conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
        Statement statement = conn.createStatement();
        String seq = "select " + tableName + "_seq.nextval from dual";
        // System.out.println(seq);
        ResultSet rs = statement.executeQuery(seq);

        int s = 0;
        while (rs.next()) {
            s = rs.getInt(1);
        }
        statement.close();

        try {
            CoonectionHandler.getInstance().returnConnection(conn);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BaseDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
//        statement = null;
        return s;
    }

    public synchronized CallableStatement executeCallableStatmentJobMon(String sql) throws SQLException, ClassNotFoundException, Exception {

        Connection conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
        CallableStatement statement = conn.prepareCall(sql);
        return statement;

    }

    public synchronized CallableStatement executeCallableStatment(String sql) throws SQLException, ClassNotFoundException, Exception {

        Connection conn = CoonectionHandler.getInstance().getConnection(getLoggedInUser());
        CallableStatement statement = conn.prepareCall(sql);
        return statement;

    }

    public synchronized Integer getLoggedInUser() {
        try {
            Session sessionutil = new Session();
            UsersDTOInter currentUser = sessionutil.GetUserLogging();
            Integer uDTO = currentUser.getUserId();
            return uDTO;
        } catch (Exception ex) {
            return 0;

        } catch (Throwable ex) {
            Logger.getLogger(BaseDAO.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public synchronized Date getCurrentTime() throws Throwable {
        try {
            String selectStat = "select to_char(sysdate,'hh24:mi:ss') from dual";
            ResultSet rs = executeQuery(selectStat);
            Date now = null;
            while (rs.next()) {
                now = rs.getTime(1);
            }
            CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());
            return now;

        } catch (SQLException ex) {
            Logger.getLogger(BaseDAO.class
                    .getName()).log(Level.SEVERE, null, ex);

            return null;
        }
    }
}
