/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.AtmBingo.base.client;

// File Name SendEmail.java

import com.ev.AtmBingo.base.util.DateFormatter;
import com.ev.AtmBingo.base.util.PropertyReader;
import com.ev.AtmBingo.bus.dto.ColumnDTOInter;
import com.ev.AtmBingo.bus.dto.DisputesDTOInter;
import com.ev.AtmBingo.bus.dto.FileColumnDefinitionDTOInter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class ExportExcel  implements Serializable{

    public void exportexcel(List<ColumnDTOInter> coloumns, DisputesDTOInter[] SearchRecords) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Sample sheet");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        Object[] coloumnslist = new Object[coloumns.size()];
        int index = 0;
        for (ColumnDTOInter coloumn : coloumns) {
            coloumnslist[index] = coloumn.getHeader();
            sheet.setDefaultColumnWidth(20);
            index = index + 1;
        }
        data.put(1, coloumnslist);
        index = 2;

        for (DisputesDTOInter Records : SearchRecords) {
            Object[] coloumnsData = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                if ("TRANSACTION_TYPE_MASTER".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiontypemaster()).toString();
                }
                if ("RESPONSE_CODE_MASTER".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getresponsecodemaster()).toString();
                }
                if ("CARD_NO_SUFFIX".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcardnosuffix()).toString();
                }
                if ("TRANSACTION_DATA".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiondata()).toString();
                }
                if ("TRANSACTION_DATE".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiondate()).toString();
                }
                if ("SETTLEMENT_DATE".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getsettlementdate()).toString();
                }
                if ("TRANSACTION_SEQUENCE_ORDER_BY".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactionsequenceorderby()).toString();
                }
                if ("TRANSACTION_TYPE".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiontype()).toString();
                }
                if ("CARD_NO".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcardno()).toString();
                }
                if ("AMOUNT".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getamount()).toString();
                }
                if ("CURRENCY".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcurrency()).toString();
                }
                if ("CURRENCY_ID".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcurrencyid()).toString();
                }
                if ("CUSTOMER_ACCOUNT_NUMBER".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcustomeraccountnumber()).toString();
                }
                if ("RESPONSE_CODE".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getresponsecode()).toString();
                }
                if ("RESPONSE_CODE_ID".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getresponsecodeid()).toString();
                }
                if ("TRANSACTION_STATUS".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactionstatus()).toString();
                }
                if ("NOTES_PRESENTED".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getnotespresented()).toString();
                }
                if ("ATM_ID".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getatmid()).toString();
                }
                if ("TRANSACTION_TIME".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiontime()).toString();
                }
                if ("ATM_APPLICATION_ID".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getatmapplicationid()).toString();
                }
                if ("COLUMN1".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn1()).toString();
                }
                if ("COLUMN2".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn2()).toString();
                }

                if ("COLUMN3".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn3()).toString();
                }

                if ("COLUMN4".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn4()).toString();
                }

                if ("COLUMN5".equals(coloumns.get(i).getDbName().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn5()).toString();
                }

            }
            data.put(index, coloumnsData);
            index = index + 1;
        }
        Set<Integer> keyset = data.keySet();
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            Calendar c = Calendar.getInstance();  
            String uri = "Trans" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".xls";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            pdfPath = pdfPath + uri;
            FileOutputStream out = new FileOutputStream(new File(pdfPath));
            workbook.write(out);
            out.close();
            ProcessBuilder pb = new ProcessBuilder("cmd.exe","/c",pdfPath);
            Process p = pb.start();
            

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
