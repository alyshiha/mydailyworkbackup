/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Aly-Shiha
 */
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

public class PerfSelect {

    public static void main(String[] args) throws ClassNotFoundException,
            SQLException, IOException {
        for (int i = 100; i < 1100;) {

            Class.forName("oracle.jdbc.OracleDriver");
            String url = "jdbc:oracle:thin:@192.168.42.138:1521:db";
            Connection conn = DriverManager.getConnection(url, "atm", "atm");
            conn.setAutoCommit(false);

            String sql = "select  (select p.user_name from users p where p.user_id = export_user) export,(select p.user_name from users p where p.user_id = corrective_user) corrective,decode(corrective_amount,null,amount,corrective_amount)corrective_amount ,rownum elrow,(select l.name from atm_machine l where l.id = disputes.atm_id) atmname,disputes.*,disputes.card_no card_no_decrypt,is_black_list(disputes.card_no) blacklist, get_disputes_reason(dispute_key_uk,dispute_reason) ReasonToDisp from disputes  where 1=1  and transaction_date >= to_date('01.03.2017 17:00:00','dd.mm.yyyy hh24:mi:ss')  and transaction_date <= to_date('01.05.2017 16:59:59','dd.mm.yyyy hh24:mi:ss')   and record_type = 2 and matching_type = 1 and atm_valid(atm_id) = 1   and Exists (select 1 from user_atm s where s.user_id = 1 and s.atm_id = disputes.ATM_ID) order by  ATM_APPLICATION_ID,transaction_date, transaction_sequence_order_by";
            long start = System.currentTimeMillis();
            Statement stmt = conn.createStatement();
            stmt.setFetchSize(i);
            ResultSet rs = stmt.executeQuery(sql);
            FileWriter fw = new FileWriter("E:/temp/o.out");
            while (rs.next()) {
                fw.write(rs.getString(1) + " " + rs.getString(2) + "\n");
            }
            long end = System.currentTimeMillis();

            NumberFormat formatter = new DecimalFormat("#0.00000");
            //System.out.print("Execution time is " + formatter.format((end - start) / 1000d) + " seconds");
            stmt.close();
            rs.close();
            fw.flush();
            fw.close();
            //System.out.println("Round:" + i);

            i = i + 100;
        }
    }
}
