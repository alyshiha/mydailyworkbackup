/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author AlyShiha
 */
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.pool.OracleDataSource;

class JDBCVersion
{
public static void main (String args[]) 
{       
    try {
        OracleDataSource ods = new OracleDataSource();
        ods.setURL("jdbc:oracle:thin:@//192.168.1.15:1521/db");
        ods.setUser("atm");
        ods.setPassword("atm");
        Connection conn = ods.getConnection();
        // Create Oracle DatabaseMetaData object
        DatabaseMetaData meta = conn.getMetaData();
        // gets driver info:
        //System.out.println("JDBC driver version is " + meta.getDriverVersion());
    } catch (SQLException ex) {
        Logger.getLogger(JDBCVersion.class.getName()).log(Level.SEVERE, null, ex);
    }
}
}