/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision;

/**
 *
 * @author AlyShiha
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

public class ReadPropertiesXmlFile {

    static File file;
    
    public static PropDTO readProperties() {
        try {
            ClassLoader classLoader = ReadPropertiesXmlFile.class.getClassLoader();
            file = new File("C:\\Windows\\System32\\ISOProperties.xml");
            FileInputStream fileInput = new FileInputStream(file);
            Properties properties = new Properties();
            properties.loadFromXML(fileInput);
            fileInput.close();
            Enumeration enuKeys = properties.keys();
            while (enuKeys.hasMoreElements()) {
                String key = (String) enuKeys.nextElement();
                String value = properties.getProperty(key);
                return new PropDTO(properties.getProperty("FromFolder"), properties.getProperty("ToFolder"),properties.getProperty("BackUpFolder"));
            }

        } catch (FileNotFoundException e) {
            System.err.println("Can`t Find the properties file Please Move It To C:\\Windows\\System32\\ISOProperties.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
