/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.base.util;

import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;

import java.io.FileWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Properties;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;

/**
 *
 * @author Administrator
 */
public class ReportingTool {

    private JRResultSetDataSource resultSetDataSource;

    public ReportingTool(ResultSet rs, ArrayList columns, boolean rejected) {
        ResultSet r = rs;
        generateReport(r, columns, rejected);
        resultSetDataSource = new JRResultSetDataSource(rs);
    }

    private void generateReport(ResultSet rs, ArrayList columns, boolean rejected) {
        Writer writer = null;
        VelocityEngine ve = new VelocityEngine();
        ArrayList fieldList = new ArrayList();
        Field field;
        ArrayList columnList1 = new ArrayList();
        ArrayList columnList = getColoumnList(columns);

        Column column;
//        ColumnDTOInter col;

        try {
            //columnsDAOInter cDAO = DAOFactory.createcolumnsDAO();
            Properties properties = new Properties();
            properties.setProperty("file.resource.loader.path",
                    "c:/BingoReports/");

            Velocity.init(properties);

            VelocityContext context = new VelocityContext();
            Template template;
            // Load template file
            if (rejected) {
                template = Velocity.getTemplate("BasicReportR.vm");
            } else {
                template = Velocity.getTemplate("C:\\BingoReports\\BasicReport.vm");
            }

            // Get result set meta data
            ResultSetMetaData rsmd = rs.getMetaData();
            int numColumns = rsmd.getColumnCount();

            // Get the column names; column indices start from 1
            for (int i = 1; i < numColumns + 1; i++) {
                field = new Field();
                field.setName(rsmd.getColumnName(i));
                if (rsmd.getColumnClassName(i).contains("Date")) {
                    field.setClazzName("java.sql.Timestamp");
                } else {
                    field.setClazzName(rsmd.getColumnClassName(i));
                }

                fieldList.add(field);
                column = new Column();
                // column = (ColumnDTOInter) cDAO.searchByColumnName(rsmd.getColumnName(i));
                column.setName(rsmd.getColumnName(i));
                column.setExpression(buildExpression(rsmd.getColumnName(i)));
//                column.setWidth(100);
//                column.setAlignment("Center");
                if (rsmd.getColumnClassName(i).contains("Date")) {
                    column.setClazzType("java.sql.Timestamp");
                } else {
                    column.setClazzType(rsmd.getColumnClassName(i));
                }
                columnList1.add(column);
            }
            int r = 0;
            int i = 0;
            for (Column d : (ArrayList<Column>) columnList1) {
                if (!rejected) {
                    if (i != 0 && i != 1) {
                        if (r < columnList.size()) {
                            ((ArrayList<filecolumndefinitionDTOInter>) columnList).get(r).setClazzType(d.getClazzType());
                            System.out.println(d.getClazzType());
                            ((ArrayList<filecolumndefinitionDTOInter>) columnList).get(r).setExpression(d.getExpression());
                            System.out.println(d.getExpression());
                            r++;
                        } else {
                            break;
                        }
                    } else {
                        i++;
                    }
                } else {
                    if (i != 0) {
                        if (r < columnList.size()) {
                            ((ArrayList<filecolumndefinitionDTOInter>) columnList).get(r).setClazzType(d.getClazzType());
                            ((ArrayList<filecolumndefinitionDTOInter>) columnList).get(r).setExpression(d.getExpression());
                            r++;
                        } else {
                            break;
                        }
                    } else {
                        i++;
                    }
                }
            }
            context.put("columnList", columnList);
            context.put("fieldList", fieldList);
            context.put("user", buildParameter("user"));
            context.put("title", buildParameter("title"));
            context.put("params", buildParameter("params"));
            context.put("customer", buildParameter("customer"));
            context.put("totaltrans", buildParameter("totaltrans"));
            context.put("PAGE_NUMBER", buildVariable("PAGE_NUMBER"));

            context.put("ATMheader", buildExpression("ATM_APPLICATION_ID"));
            context.put("UNIT", buildExpression("UNIT_ID"));
            context.put("CURRENCY", buildExpression("CURRENCY"));
            context.put("NAME", buildExpression("NAME"));

            context.put("RECORD_TYPE", buildExpression("RECORD_TYPE"));
            if (!rejected) {
                context.put("AMOUNT", buildExpression("AMOUNT"));
                context.put("COMMENTS", buildExpression("COMMENTS"));
                context.put("SETTLEDFLAG", buildExpression("SETTLED_FLAG"));
                context.put("AMNT", buildExpression("AMNT"));
                context.put("amn_1", buildVariable("amn_1"));
                context.put("amt_2", buildVariable("amt_2"));
            }

            context.put("RECORDTYPE", buildExpression("RECORD_TYPE"));

            // Merge template with the context in
            writer = new StringWriter();
            template.merge(context, writer);

            if (rejected) {
                FileWriter fw = new FileWriter("c:/BingoReports/BasicReportR.jrxml");

                fw.write(writer.toString());
                fw.close();
            } else {
                FileWriter fw = new FileWriter("c:/BingoReports/BasicReport.jrxml");

                fw.write(writer.toString());
                fw.close();
            }

        } catch (Throwable e) {
        }
    }

    public ArrayList getColoumnList(ArrayList columnList) {
        ArrayList columnListtemp = new ArrayList();
        String constant = "RECORD_TYPE,SETTLED_FLAG,COMMENTS,ATM_APPLICATION_ID,CURRENCY,UNIT_ID";
        for (int i = 0; i < columnList.size(); i++) {
            if (!constant.contains(((ArrayList<filecolumndefinitionDTOInter>) columnList).get(i).getcolumnname())) {
                columnListtemp.add(((ArrayList<filecolumndefinitionDTOInter>) columnList).get(i));
            }
        }
        return columnListtemp;
    }

    private String buildParameter(String fieldName) {
        String expression;

        expression = "$P{" + fieldName + "}";

        return expression;
    }

    private String buildExpression(String fieldName) {
        String expression;

        expression = "$F{" + fieldName + "}";

        return expression;
    }

    private String buildVariable(String fieldName) {
        String expression;

        expression = "$V{" + fieldName + "}";

        return expression;
    }

}
