/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.base.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class DateFormatter {
    public static String changeDateFormatdate(Date d) {
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String newDate = df.format(d);
        return newDate;
    }
    public static String changeDateFormat(Date d) {
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        String newDate = df.format(d);
        return newDate;
    }

    public static String changeDateAndTimeFormat(Date d) {
        if (d != null) {
            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            String newDate = df.format(d);
            return newDate;
        } else {
            return "";
        }
    }

    public static String changeDateAndTimeFormatForReport(Date d) {
        if (d != null) {
            SimpleDateFormat df = new SimpleDateFormat("ddMMyyyyHHmmss");
            String newDate = df.format(d);
            return newDate;
        } else {
            return "";
        }
    }

    public static String changeTimeFormat(Date d) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        String newDate = df.format(d);
        return newDate;
    }

    public static String changeTimeWithoutSecFormat(Date d) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        String newDate = df.format(d);
        return newDate;
    }

    public static Date getTimeFormat(String time) {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        Date d = null;
        try {
            d = (Date) df.parse(time);
        } catch (ParseException ex) {
            Logger.getLogger(DateFormatter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return d;
    }

    public static Date getDTFormat(Date date) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date d = null;

        String dd = df.format(date);

        d = (Date) df.parse(dd);

        return d;
    }

    public static void main(String[] args) throws ParseException {
        Date d = DateFormatter.getDTFormat(new Date());
        System.out.println(d.toString());
    }
}
