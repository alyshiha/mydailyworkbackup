/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.base.util;

import java.io.File;

/**
 *
 * @author Administrator
 */
public class DirectoryCreator {

    public Boolean createDirectory(String path) {
        try {
            Boolean succ = (new File(path)).mkdir();

            return succ;

        } catch (Throwable ex) {
            return false;
        }
    }

    public static void main(String[] args) {
        DirectoryCreator dc = new DirectoryCreator();
        System.out.print(dc.createDirectory("c:/Ahmed"));
    }
}
