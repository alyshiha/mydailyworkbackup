/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.base.util;

/**
 *
 * @author Administrator
 */
public class Group {

    private String groupName;
    private String groupField;
    private String groupHeader;
    private String groupFooter;

    public String getGroupField() {
        return groupField;
    }

    public void setGroupField(String groupField) {
        this.groupField = groupField;
    }

    public String getGroupFooter() {
        return groupFooter;
    }

    public void setGroupFooter(String groupFooter) {
        this.groupFooter = groupFooter;
    }

    public String getGroupHeader() {
        return groupHeader;
    }

    public void setGroupHeader(String groupHeader) {
        this.groupHeader = groupHeader;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

}
