package com.ev.Bingo.base.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import java.util.Properties;

public final class PropertyReader {

    private String propertiesFileName = null;
    private static Properties props = null;

    private PropertyReader(String propertiesFileName) {
        try {
            this.propertiesFileName = propertiesFileName;
            props = new Properties();
            InputStream in
                    = PropertyReader.class.getResourceAsStream(propertiesFileName);
            props.load(in);

            //PropertyConfigurator.configure(props);
        } catch (FileNotFoundException fnfe) {
            System.out.println("PropertyReader.java - FileNotFoundException : "
                    + fnfe.toString());
        } catch (IOException ioe) {
            System.out.println("PropertyReader.java - IOException : "
                    + ioe.toString());
        }
    }

    public static void main(String[] args) {
        //PropertyReader propertyReader = new PropertyReader("conn.properties");
        String s = PropertyReader.getProperty("ev.db_server_ip");
        System.out.println("(" + s + ")");
    }

    private String getPropertiesFileName() {
        return propertiesFileName;
    }

    private Properties getProps() {
        return props;
    }

    public static String getProperty(String prop) {
        if (props == null) {
            new PropertyReader("/com/ev/Bingo/conn.properties");
        }
        return props.getProperty(prop);
    }
}
