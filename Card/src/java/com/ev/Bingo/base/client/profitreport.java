/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.base.client;

import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dto.OffusDTOInter;
import com.ev.Bingo.bus.dto.OffusTotalsDTOInter;
import com.ev.Bingo.bus.dto.OnusDTOInter;
import com.ev.Bingo.bus.dto.OnusTotalsDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusInterDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusInterTotalsDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusTotalsDTOInter;
import com.ev.Bingo.bus.dto.disputesDTOInter;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author Aly.Shiha
 */
public class profitreport {

    public String exportexcel(List<OnusTotalsDTOInter> AllOnusRecords, List<OffusTotalsDTOInter> AllOffusRecords, List<VisaOffusTotalsDTOInter> AllVisaOffusRecords, List<VisaOffusInterTotalsDTOInter> allVisaOffusInternationalRecords, List<OnusTotalsDTOInter> AllOnusRecordstotals, List<OffusTotalsDTOInter> AllOffusRecordstotals, List<VisaOffusTotalsDTOInter> AllVisaOffusRecordstotals, List<VisaOffusInterTotalsDTOInter> allVisaOffusInternationalRecordstotals) {

        int index = 0;
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("ProfitReport");
        sheet.setDefaultColumnWidth(40);
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();

        if (AllOnusRecords.size() > 0) {

            Object[] OnusTitle = new Object[7];
            OnusTitle[0] = "Onus Search Results Totals";
            data.put(++index, OnusTitle);

            data.put(++index, new Object[7]);

            Object[] OnusHeader = new Object[7];
            OnusHeader[0] = "Count";
            OnusHeader[1] = "Total Amount";
            OnusHeader[2] = "Type";
            OnusHeader[3] = "ATM ID";
            OnusHeader[4] = "ATM Name";
            OnusHeader[5] = "Month";

            data.put(++index, OnusHeader);

            for (OnusTotalsDTOInter Records : AllOnusRecords) {
                Object[] OnusData = new Object[7];
                OnusData[0] = String.valueOf(Records.getCount()).replace("null", "0");
                OnusData[1] = String.valueOf(Records.getTotalAmount()).replace("null", "0");
                OnusData[2] = String.valueOf(Records.getType()).replace("null", " ");
                OnusData[3] = String.valueOf(Records.getTerminal()).replace("null", " ");
                OnusData[4] = String.valueOf(Records.getTerminalName()).replace("null", " ");
                OnusData[5] = String.valueOf(Records.getTransMonth()).replace("null", " ");
                data.put(++index, OnusData);
            }
            data.put(++index, new Object[7]);

        }

        if (AllOffusRecords.size() > 0) {

            Object[] OffusTitle = new Object[7];
            OffusTitle[0] = "Offus Search Results Totals";
            data.put(++index, OffusTitle);

            data.put(++index, new Object[7]);

            Object[] OffusHeader = new Object[7];
            OffusHeader[0] = "Count";
            OffusHeader[1] = "Related Income";
            OffusHeader[2] = "Type";
            OffusHeader[3] = "ATM ID";
            OffusHeader[4] = "ATM Name";
            OffusHeader[5] = "Month";
            data.put(++index, OffusHeader);

            for (OffusTotalsDTOInter Records : AllOffusRecords) {
                Object[] OffusData = new Object[7];
                OffusData[0] = String.valueOf(Records.getCount()).replace("null", "0");
                OffusData[1] = String.valueOf(Records.getRelatedIncome()).replace("null", "0");
                OffusData[2] = String.valueOf(Records.getType()).replace("null", " ");
                OffusData[3] = String.valueOf(Records.getTerminal()).replace("null", " ");
                OffusData[4] = String.valueOf(Records.getTerminalName()).replace("null", " ");
                OffusData[5] = String.valueOf(Records.getTransMonth()).replace("null", " ");
                data.put(++index, OffusData);
            }
            data.put(++index, new Object[7]);

        }
        if (AllVisaOffusRecords.size() > 0) {

            Object[] VisaOffusTitle = new Object[7];
            VisaOffusTitle[0] = "Visa Offus Search Results Totals";
            data.put(++index, VisaOffusTitle);

            data.put(++index, new Object[7]);

            Object[] VisaOffusHeader = new Object[7];
            VisaOffusHeader[0] = "Count";
            VisaOffusHeader[1] = "Related Income";
            VisaOffusHeader[2] = "Type";
            VisaOffusHeader[3] = "ATM ID";
            VisaOffusHeader[4] = "ATM Name";
            VisaOffusHeader[5] = "Month";
            data.put(++index, VisaOffusHeader);

            for (VisaOffusTotalsDTOInter Records : AllVisaOffusRecords) {
                Object[] OnusData = new Object[7];
                OnusData[0] = String.valueOf(Records.getTransCount()).replace("null", "0");
                OnusData[1] = String.valueOf(Records.getIncome()).replace("null", "0");
                OnusData[2] = String.valueOf(Records.getType()).replace("null", " ");
                OnusData[3] = String.valueOf(Records.getTerminal()).replace("null", " ");
                OnusData[4] = String.valueOf(Records.getTerminalName()).replace("null", " ");
                OnusData[5] = String.valueOf(Records.getTransMonth()).replace("null", " ");
                data.put(++index, OnusData);
            }
            data.put(++index, new Object[7]);

        }
        if (allVisaOffusInternationalRecords.size() > 0) {

            Object[] VisaOffusInternationalTitle = new Object[7];
            VisaOffusInternationalTitle[0] = "Visa International Search Results Totals";
            data.put(++index, VisaOffusInternationalTitle);

            data.put(++index, new Object[7]);

            Object[] VisaOffusInternationalHeader = new Object[7];
            VisaOffusInternationalHeader[0] = "Count";
            VisaOffusInternationalHeader[1] = "Related Income";
            VisaOffusInternationalHeader[2] = "Total Amount";
            VisaOffusInternationalHeader[3] = "Type";
            VisaOffusInternationalHeader[4] = "ATM ID";
            VisaOffusInternationalHeader[5] = "ATM Name";
            VisaOffusInternationalHeader[6] = "Month";
            data.put(++index, VisaOffusInternationalHeader);

            for (VisaOffusInterTotalsDTOInter Records : allVisaOffusInternationalRecords) {
                Object[] OnusData = new Object[7];
                OnusData[0] = String.valueOf(Records.getCount()).replace("null", "0");
                OnusData[1] = String.valueOf(Records.getIncome()).replace("null", "0");
                OnusData[2] = String.valueOf(Records.getTotalAmount()).replace("null", "0");
                OnusData[3] = String.valueOf(Records.getType()).replace("null", " ");
                OnusData[4] = String.valueOf(Records.getTerminal()).replace("null", " ");
                OnusData[5] = String.valueOf(Records.getTerminalName()).replace("null", " ");
                OnusData[6] = String.valueOf(Records.getTransMonth()).replace("null", " ");
                data.put(++index, OnusData);
            }
            data.put(++index, new Object[7]);

        }
        Set<Integer> keyset = data.keySet();
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);
            }
        }

        try {
            FileOutputStream out
                    = new FileOutputStream(new File(PropertyReader.getProperty("ev.installation.path.pdf") + "//" + "ProfitReport.xls"));
            workbook.write(out);
            out.close();
            return PropertyReader.getProperty("ev.installation.path.pdf") + "//" + "ProfitReport.xls";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return PropertyReader.getProperty("ev.installation.path.pdf") + "//" + "ProfitReport.xls";
        } catch (IOException e) {
            e.printStackTrace();
            return PropertyReader.getProperty("ev.installation.path.pdf") + "//" + "ProfitReport.xls";
        }
    }
}
