/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.base.client;

// File Name SendEmail.java
import com.ev.Bingo.bus.dto.disputesDTOInter;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class ExportExcel {

    public disputesDTOInter[] SortOrder(disputesDTOInter[] entities, String sortby) {
        int[] res = new int[entities.length];
        for (int i = 0; i < entities.length; i++) {
            res[i] = entities[i].getRownum();
        }
        if ("DSC".equals(sortby)) {
            Arrays.sort(res);
        } else {
            Arrays.sort(res);
        }
        disputesDTOInter[] Result = new disputesDTOInter[entities.length];
        for (int u = 0; u < res.length; u++) {
            for (int l = 0; l < entities.length; l++) {
                if (res[u] == entities[l].getRownum()) {
                    Result[u] = entities[l];
                    break;
                }
            }
        }
        return Result;
    }

    public String exportexcel(List<filecolumndefinitionDTOInter> coloumns, disputesDTOInter[] Search, String Path, String sortby) {
        disputesDTOInter[] SearchRecords = SortOrder(Search, sortby);
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Sheet");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        Object[] coloumnslist = new Object[coloumns.size()];
        int index = 0;
        for (filecolumndefinitionDTOInter coloumn : coloumns) {
            coloumnslist[index] = coloumn.getname();
            sheet.setDefaultColumnWidth(40);
            index = index + 1;
        }
        data.put(1, coloumnslist);
        index = 2;

        for (disputesDTOInter Records : SearchRecords) {
            Object[] coloumnsData = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                if ("get_rejected_reseaon(record_type,transaction_id) reason".equals(coloumns.get(i).getcolumnname().toLowerCase())) {
                    coloumnsData[i] = String.valueOf(Records.getdisputereason()).toString();
                }
                if ("TRANSACTION_DATE".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiondate()).toString();
                }
                if ("LOADING_DATE".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getloadingdate()).toString();
                }
                if ("SETTLEMENT_DATE".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getsettlementdate()).toString();
                }
                if ("TRANSACTION_SEQUENCE".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactionsequence()).toString();
                }
                if ("TRANSACTION_TYPE".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiontype()).toString();
                }
                if ("CARD_NO".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcardno()).toString();
                }
                if ("AMOUNT".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    if ("0".equals(Records.getCorrectiveamount()) || Records.getCorrectiveamount() == null) {
                        coloumnsData[i] = String.valueOf(Records.getamount()).toString();
                    } else {
                        coloumnsData[i] = String.valueOf(Records.getCorrectiveamount()).toString();
                    }
                }
                if ("CURRENCY".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcurrency()).toString();
                }
                if ("CURRENCY_ID".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcurrencyid()).toString();
                }
                if ("CUSTOMER_ACCOUNT_NUMBER".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcustomeraccountnumber()).toString();
                }
                if ("RESPONSE_CODE".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getresponsecode()).toString();
                }
                if ("RESPONSE_CODE_ID".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getresponsecodeid()).toString();
                }
                if ("TRANSACTION_TIME".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiontime()).toString();
                }
                if ("COLUMN1".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn1()).toString();
                }
                if ("COLUMN2".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn2()).toString();
                }

                if ("COLUMN3".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn3()).toString();
                }

                if ("COLUMN4".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn4()).toString();
                }
                if ("COLUMN5".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcolumn5()).toString();
                }
                if ("TRANSACTION_TYPE_MASTER".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiontypemaster()).toString();
                }
                if ("RESPONSE_CODE_MASTER".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getresponsecodemaster()).toString();
                }
                if ("CARD_NO_SUFFIX".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getcardnosuffix()).toString();
                }
                if ("TRANSACTION_TYPE_ID".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactiontypeid()).toString();
                }
                if ("TRANSACTION_SEQUENCE_ORDER_BY".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.gettransactionsequenceorderby()).toString();
                }
                if ("TERMINAL".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getterminal()).toString();
                }
                if ("ACQUIRER_CURRENCY".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getacquirercurrency()).toString();
                }
                if ("ACQUIRER_CURRENCY_ID".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getacquirercurrencyid()).toString();
                }
                if ("ACQUIRER_AMOUNT".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getacquireramount()).toString();
                }
                if ("SETTLEMENT_CURRENCY".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getsettlementcurrency()).toString();
                }
                if ("SETTLEMENT_CURRENCY_ID".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getsettlementcurrencyid()).toString();
                }
                if ("SETTLEMENT_AMOUNT".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getsettlementamount()).toString();
                }
                if ("AUTHORIZATION_NO".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getauthorizationno()).toString();
                }
                if ("REFERENCE_NO".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getreferenceno()).toString();
                }
                if ("BANK".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getbank()).toString();
                }
                if ("NETWORK_CODE".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getnetworkcode()).toString();
                }
                if ("PROCESS_CODE".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getprocesscode()).toString();
                }
                if ("AREA_CODE".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getareacode()).toString();
                }
                if ("NETWORK_COMM".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getnetworkcomm()).toString();
                }
                if ("NETWORK_COMM_FLAG".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getnetworkcommflag()).toString();
                }
                if ("FEE_LEVEL".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getfeelevel()).toString();
                }
                if ("NETWORK_ID".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getnetworkid()).toString();
                }
                if ("REPORT_ID".equals(coloumns.get(i).getcolumnname().toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Records.getreportid()).toString();
                }

            }
            data.put(index, coloumnsData);
            index = index + 1;
        }
        Set<Integer> keyset = data.keySet();
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {

            FileOutputStream out
                    = new FileOutputStream(new File(Path));
            workbook.write(out);
            out.close();
            return Path;

        } catch (FileNotFoundException e) {

            e.printStackTrace();
            return Path;
        } catch (IOException e) {
            e.printStackTrace();
            return Path;
        }
    }
}
