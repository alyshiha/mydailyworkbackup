/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.base.client;

import com.ev.Bingo.base.data.DataProviderFactory;

import com.ev.Bingo.bus.bo.*;
import com.ev.Bingo.bus.dto.*;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class BaseBean implements Serializable {

    public void showMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void RefreshProfile() throws Throwable {
        profileBOInter profileBO;
        profileBO = BOFactory.createprofileBO();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.setAttribute("ProfileList", (List<profileDTOInter>) profileBO.GetAllRecords());
    }

    public Date getBeforYesterdaytrans(String time) {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        cal.add(Calendar.DATE, Integer.valueOf(-2));
        String[] sArr = time.split(":");
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sArr[0]));
        cal.set(Calendar.MINUTE, Integer.parseInt(sArr[1]));
        cal.set(Calendar.SECOND, Integer.parseInt(sArr[2]));
        String s = dateFormat.format(cal.getTime());
        Date d = dateFormat.getCalendar().getTime();
        return d;
    }

    public Date getYesterdaytrans(String time) {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        cal.add(Calendar.DATE, Integer.valueOf("-1"));
        String[] sArr = time.split(":");
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sArr[0]));
        cal.set(Calendar.MINUTE, Integer.parseInt(sArr[1]));
        cal.set(Calendar.SECOND, Integer.parseInt(sArr[2]));
        String s = dateFormat.format(cal.getTime());
        Date d = dateFormat.getCalendar().getTime();
        return d;
    }

    public Date getTodaytrans(String time) {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String[] sArr = time.split(":");
        cal.add(Calendar.DATE, Integer.valueOf("0"));
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sArr[0]));
        cal.set(Calendar.MINUTE, Integer.parseInt(sArr[1]));
        cal.set(Calendar.SECOND, Integer.parseInt(sArr[2]) - 1);

        String s = dateFormat.format(cal.getTime());
        Date d = dateFormat.getCalendar().getTime();
        return d;

    }

    public List<profileDTOInter> GetProfile() throws Throwable {
        profileBOInter profileBO;
        profileBO = BOFactory.createprofileBO();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("ProfileList") == null) {
            session.setAttribute("ProfileList", (List<profileDTOInter>) profileBO.GetAllRecords());
        }
        return (List<profileDTOInter>) session.getAttribute("ProfileList");
    }

    public Boolean checkint(String input) {
        try {
            int x = Integer.parseInt(input);
            return Boolean.TRUE;
        } catch (NumberFormatException nFE) {
            return Boolean.FALSE;
        }
    }

    public void Testlogin() throws Throwable {
        if (!"Valid".equals(GetLogin())) {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect("Login.xhtml");
        }
    }

    public void OpenHTTPSession() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
    }

    public List<String> Getmasterrecord(String MatchingType) {
        List<String> masterrecord = new ArrayList<String>();
        if ("Journal - Switch".equals(MatchingType)) {
            masterrecord.add("Journal");
            masterrecord.add("Switch");
        }
        if ("Switch - Host".equals(MatchingType)) {
            masterrecord.add("Switch");
            masterrecord.add("Host");
        }
        return masterrecord;
    }

    public List<String> Getmatchingtype() {
        List<String> matchingtype = new ArrayList<String>();
        matchingtype.add("Journal - Switch");
        matchingtype.add("Switch - Host");
        return matchingtype;
    }

    public String ConvertDate(Date todaysDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(todaysDate);
    }

    public usersDTOInter GetUser() throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        return (usersDTOInter) session.getAttribute("logedinUser");
    }

    public void SetIP() throws Throwable {
        String ip = "";
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if (req.getHeader("x-forwarded-for") == null) {
            ip = req.getRemoteAddr();
        } else {
            ip = req.getHeader("x-forwarded-for");
        }
        session.setAttribute("ip", ip);
    }

    public void SetUser(String UserName) throws Throwable {
        usersBOInter ubo = BOFactory.createusersBO();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        usersDTOInter searchrec = DTOFactory.createusersDTO();
        searchrec.setUsername(UserName);
        session.setAttribute("logedinUser", (usersDTOInter) ubo.GetRecord(searchrec));
    }

    public void SetLogin() throws Throwable {
        usersBOInter ubo = BOFactory.createusersBO();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.setAttribute("LoginValid", (String) "Valid");
    }

    public String GetLogin() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("LoginValid") == null) {
            return "InValid";
        } else {
            return session.getAttribute("LoginValid").toString();
        }
    }

    public String getRequestURL() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String url = request.getRequestURI().toString();
        return url;
    }

    public void GetAccess() {
        try {
            FacesContext context2 = FacesContext.getCurrentInstance();
            HttpSession session2 = (HttpSession) context2.getExternalContext().getSession(true);
            String Page = getRequestURL();
            Page = Page.replace(Page.substring(0, Page.indexOf("faces/")), "").replace("faces/", "");
            System.out.println(Page);
            MainBoInter MenuTest = BOFactory.createMainBo();
            if (!MenuTest.ValidatePage(GetUser().getUserid(), Page)) {
                FacesContext context = FacesContext.getCurrentInstance();
                HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                response.sendRedirect("Login.xhtml");
            }
            HttpServletResponse response2 = (HttpServletResponse) context2.getExternalContext().getResponse();
            response2.addHeader("X-Frame-Options", "SAMEORIGIN");

        } catch (Throwable ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            try {
                response.sendRedirect("Login.xhtml");
            } catch (IOException ex1) {
                Logger.getLogger(BaseBean.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    public boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public systemtableDTOInter Getseesiontime() throws Throwable {
        systemtableBOInter atmgBO;
        atmgBO = BOFactory.createsystemtableBO();
        FacesContext context = FacesContext.getCurrentInstance();
        systemtableDTOInter sessiontimerec = DTOFactory.createsystemtableDTO();
        sessiontimerec.setparametername("SESSION_TIME");
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("SeesionTime") == null) {
            session.setAttribute("SeesionTime", (systemtableDTOInter) atmgBO.GetRecord(sessiontimerec));
        }
        return (systemtableDTOInter) session.getAttribute("SeesionTime");
    }

    public void setLicenseSessions(licenseDTOInter LDTO) {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.setAttribute("client", LDTO.getlicenseto() + " - " + LDTO.getcountry());
        session.setAttribute("version", LDTO.getversion());
        session.setAttribute("UserCount", LDTO.getnoofusers());
        session.setAttribute("EndDate", LDTO.getenddate());
        session.setAttribute("client", LDTO.getlicenseto() + " - " + LDTO.getcountry());
    }

    public String GetClient() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        return session.getAttribute("client").toString();
    }

    public void clearsessionattributes() throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session != null) {
            Enumeration attributeNames = session.getAttributeNames();
            while (attributeNames.hasMoreElements()) {
                String sAttribute = attributeNames.nextElement().toString();
                session.removeAttribute(sAttribute);
            }
        }
    }

    public void RefreshUsers() throws Throwable {
        usersBOInter usersBO;
        usersBO = BOFactory.createusersBO();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.setAttribute("UsersList", (List<usersDTOInter>) usersBO.GetAllRecords());
    }

    public List<usersDTOInter> GetUsers() throws Throwable {
        usersBOInter usersBO;
        usersBO = BOFactory.createusersBO();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("UsersList") == null) {
            session.setAttribute("UsersList", (List<usersDTOInter>) usersBO.GetAllRecords());
        }
        return (List<usersDTOInter>) session.getAttribute("UsersList");
    }

    public List<String> findPrivelage(Integer userid, String PageName) throws Throwable {
        userPrivilegeBOInter engin = BOFactory.createuserPrivilegeBO(null);
        List<String> records = engin.findPrivelage(userid, PageName);
        return records;
    }

    public Boolean findPrivelageOperation(List<String> records, String Operation) {
        return records.contains(Operation.toUpperCase());
    }
   //
}
