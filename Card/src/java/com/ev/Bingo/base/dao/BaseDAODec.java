package com.ev.Bingo.base.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.data.DataProviderFactory;
 
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.bingologDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class BaseDAODec {

    private String tableName;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    protected Object preUpdate(Object... obj1) {
        return null;
    }

    public void SaveLogSesion() throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        List<bingologDTOInter> BingoLogList = (List<bingologDTOInter>) session.getAttribute("BingoLog");
        SaveLog(BingoLogList, getIpAddress());
        BingoLogList = null;
    }

    public usersDTOInter GetUser() throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        return (usersDTOInter) session.getAttribute("logedinUser");
    }

    public int SetBingoLog(bingologDTOInter record) throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("BingoLog") == null) {
            List<bingologDTOInter> BingoLogListTemp = new ArrayList<bingologDTOInter>();
            session.setAttribute("BingoLog", (List<bingologDTOInter>) BingoLogListTemp);
        }
        List<bingologDTOInter> BingoLogList = (List<bingologDTOInter>) session.getAttribute("BingoLog");
        BingoLogList.add(record);
        session.setAttribute("BingoLog", (List<bingologDTOInter>) BingoLogList);
        return BingoLogList.size();
    }

    protected Object postUpdate(Object... obj1) throws Throwable {
        bingologDTOInter record = DTOFactory.createbingologDTO();
        String action = (String) obj1[0];
        record.setaction(action);
        record.setuserid(GetUser().getUserid());
        Date date = new Date();
        record.setactiondate(date);
        SaveLog(record, getIpAddress());
        return "done";
    }

    public void SaveLog(Object... obj) throws SQLException, Throwable {

        bingologDTOInter entities = (bingologDTOInter) obj[0];
        String ip = (String) obj[1];
        String insertStat = "insert into BINGO_LOG (ID,ACTION_DATE,USER_ID,IP_ADDRESS,ACTION) values (bingo_log_seq.nextval,to_date(?,'dd.mm.yyyy hh24:mi:ss'),?,?,?)";
        Connection connection = null;
        PreparedStatement statement = null;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);

            statement.setString(1, DateFormatter.changeDateAndTimeFormat(entities.getactiondate()).toString());
            statement.setInt(2, entities.getuserid());
            statement.setString(3, ip);
            statement.setString(4, entities.getaction());
            statement.addBatch();
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
    }

    protected Object preSelect(Object... obj1) {
        return null;
    }

    protected Object postSelect(Object... obj1) {
        if (obj1.length > 0) {
            ResultSet set = (ResultSet) obj1[0];
            try {
               // set.close();
               // set.getStatement().close();
                CoonectionHandler.getInstance().returnConnection(set.getStatement().getConnection());
            } catch (SQLException ex) {
                Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);

            } catch (Exception ex) {
                Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return null;
    }

    protected Object preCollable(Object... obj1) {
        return null;
    }

    protected Object postCollable(Object... obj1) {
//        try {
//            
//            IDataProvider prov = DataProviderFactory.createPoolProvider();
//            prov.CloseConn();
//
//        } catch (SQLException ex) {
//            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
//
//        }
        return null;
    }

    public BaseDAODec() {
    }

    public synchronized ResultSet executeQuery(String sql) throws SQLException, Exception {
        Connection conn = CoonectionHandler.getInstance().getConnection();
        
        Statement statement = conn.createStatement();
        System.out.println(sql);
        ResultSet rs = statement.executeQuery(sql);
        return rs;
    }

    public synchronized ResultSet executeQueryJobMon(String sql) throws SQLException, Exception {
      Connection conn = CoonectionHandler.getInstance().getConnection();
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery(sql);
        return rs;
    }

    public synchronized ResultSet executeQueryReport(String sql) throws SQLException, Exception {
      Connection conn = CoonectionHandler.getInstance().getConnection();
        Statement statement = conn.createStatement();
        System.out.println(sql);
        ResultSet rs = statement.executeQuery(sql);
        return rs;
    }

    public Integer executeUpdateReport(String sql) throws SQLException, Exception {
        // IDataProvider prov = DataProviderFactory.createPoolProvider();
       Connection conn = CoonectionHandler.getInstance().getConnection();
        Statement statement = conn.createStatement();
        Integer rc = statement.executeUpdate(sql);
CoonectionHandler.getInstance().returnConnection(conn);
        return rc;
    }

    public String executeUpdate(String sql) throws SQLException, Exception {
        // IDataProvider prov = DataProviderFactory.createPoolProvider();
        Connection conn = CoonectionHandler.getInstance().getConnection();
        Statement statement = conn.createStatement();
        //  System.out.println(sql);
        System.out.println(sql);
        Integer rc = statement.executeUpdate(sql);
CoonectionHandler.getInstance().returnConnection(conn);
        return rc.toString();
    }

    public int generateSequence(String tableName) throws SQLException, Exception {
        Connection conn = CoonectionHandler.getInstance().getConnection();
        Statement statement = conn.createStatement();
        String seq = "select " + tableName + "_seq.nextval from dual";
        ResultSet rs = statement.executeQuery(seq);

        int s = 0;
        while (rs.next()) {
            s = rs.getInt(1);
        }
        statement.close();
        CoonectionHandler.getInstance().returnConnection(conn);
//        statement = null;
        return s;
    }

    public CallableStatement executeCallableStatmentJobMon(String sql) throws SQLException, Exception {
       Connection conn = CoonectionHandler.getInstance().getConnection();
        CallableStatement statement = conn.prepareCall(sql);

        return statement;

    }

    public CallableStatement executeCallableStatment(String sql) throws SQLException, Exception {
        Connection conn = CoonectionHandler.getInstance().getConnection();
        CallableStatement statement = conn.prepareCall(sql);
        return statement;

    }

    public String getIpAddress() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        String ip = (String) session.getAttribute("ip");
        return ip;
    }

    public Date getCurrentTime() {
        try {
            String selectStat = "select to_char(sysdate,'hh24:mi:ss') from dual";
            ResultSet rs = executeQuery(selectStat);
            Date now = null;
            while (rs.next()) {
                now = rs.getTime(1);
            }
            CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());
            return now;
        } catch (SQLException ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (Exception ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public String GetEncrypt(String Variable) {
        return " '" + Variable + "' ";
    }

    public String GetDecryptLinc() {
        return " LICENCE_KEY ";
    }

    public String GetDecryptCardNo() {
        return " CARD_NO  ";
    }

    public String GetDecryptCardNowithAlias() {
        return " m.CARD_NO  ";
    }
}
