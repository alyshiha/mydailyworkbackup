package com.ev.Bingo.base.data;

public class DataDTO implements IDataDTO {

    private IEntityBean entity;
    private Object[] attributeValues;

    public DataDTO() {
    }

    public void setEntity(IEntityBean entity) {
        this.entity = entity;
    }

    public IEntityBean getEntity() {
        return entity;
    }

    public void setAttributeValues(Object[] attributeValues) {
        this.attributeValues = attributeValues;
    }

    public Object[] getAttributeValues() {
        return attributeValues;
    }

    public static IDataDTO create() {
        return new DataDTO();
    }
}
