/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.base.data;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class UserCounter {
    private String UserID;
    private Date lastaccess;

    public UserCounter(String UserID, Date lastaccess) {
        this.UserID = UserID;
        this.lastaccess = lastaccess;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String UserID) {
        this.UserID = UserID;
    }

    public Date getLastaccess() {
        return lastaccess;
    }

    public void setLastaccess(Date lastaccess) {
        this.lastaccess = lastaccess;
    }

 
    
}
