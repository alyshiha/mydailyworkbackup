package com.ev.Bingo.base.data;

import java.sql.SQLException;

import java.util.List;

public interface IEntityBean {

    public int insert(IDataDTO data) throws SQLException;

    public int update(IDataDTO data) throws SQLException;

    public int delete(IDataDTO data) throws SQLException;

    public IDataDTO find(IDataDTO data) throws SQLException;

    public List<IDataDTO> getAll() throws SQLException;

    public List<IDataDTO> search(String criteria) throws SQLException;

    public void setTableName(String tableName);

    public String getTableName();

    public void setAttributes(Attribute[] attributes);

    public Attribute[] getAttributes();

}
