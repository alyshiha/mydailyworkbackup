package com.ev.Bingo.base.data;

public class Attribute {

    private int order;
    private String attName;
    private int type;

    public static final int TYPE_NUMBER = 1;
    public static final int TYPE_STRING = 2;
    public static final int TYPE_DATE = 3;

    private Attribute() {

    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getOrder() {
        return order;
    }

    public void setAttName(String attName) {
        this.attName = attName;
    }

    public String getAttName() {
        return attName;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public static Attribute create(int order, String attName, int type) {
        Attribute att = new Attribute();
        att.setOrder(order);
        att.setAttName(attName);
        att.setType(type);
        return att;
    }
}
