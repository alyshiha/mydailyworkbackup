package com.ev.Bingo.base.data;

import com.ev.Bingo.base.util.PropertyReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import javax.sql.PooledConnection;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

public class PoolProvider {
    
    private PooledConnection[] pc_Report = null;
    private PooledConnection[] pc_2 = null;
    private PooledConnection[] pc_JobMon = null;
    private static final int POOL_SIZE = 5;
    private static final int POOL_SIZE_Report = 3;
    private static final int POOL_SIZE_JobMon = 3;
    private int index = 0, conIndex = 0, indexReport = 0, indexJobMon = 0;
    private static List<UserCounter> UserList = new ArrayList<UserCounter>();
    
    protected PoolProvider() {
    }

    /*   private synchronized void initiateConnection() throws SQLException {
     OracleConnectionPoolDataSource ocpds =
     new OracleConnectionPoolDataSource();
     ocpds.setURL("jdbc:oracle:thin:@"
     + PropertyReader.getProperty("ev.db_server_ip") + ":1521:"
     + PropertyReader.getProperty("ev.db_sid"));
     ocpds.setUser(PropertyReader.getProperty("ev.db_user"));
     ocpds.setPassword(PropertyReader.getProperty("ev.db_pass"));
     pc_1 = ocpds.getPooledConnection();
     }

     public Connection getConn() throws SQLException {

     if (pc_1 == null) {
     initiateConnection();
     }

     return pc_1.getConnection();
     }
     */
    private synchronized void initiateConnectionsJob() throws SQLException {
        pc_JobMon = new PooledConnection[POOL_SIZE_JobMon];
        SimpleProtector sb = new SimpleProtector();
        //ConnectionList = new ConnectionContainer();
        for (int i = 0; i < pc_JobMon.length; i++) {
            try {
                OracleConnectionPoolDataSource ocpds
                        = new OracleConnectionPoolDataSource();
                ocpds.setURL("jdbc:oracle:thin:@"
                        + PropertyReader.getProperty("ev.db_server_ip") + ":1521:"
                        + PropertyReader.getProperty("ev.db_sid"));
                ocpds.setUser(PropertyReader.getProperty("ev.db_user"));
                ocpds.setPassword(sb.decrypt(PropertyReader.getProperty("ev.db_pass")));
                
                pc_JobMon[i] = ocpds.getPooledConnection();
                //ConnectionList.add(Integer.toString(i), 0);
            } catch (Exception ex) {
                Logger.getLogger(PoolProvider.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public synchronized Connection getConnJobMon() throws SQLException {
        
        if (pc_JobMon == null) {
            initiateConnectionsJob();
        }
        Connection conns = null;
        try {
            Thread.currentThread().sleep(5);
        } catch (InterruptedException e) {
            System.out.println(e);
        }
        conns = pc_JobMon[indexJobMon++].getConnection();
        if (indexJobMon == pc_JobMon.length) {
            indexJobMon = 0;
        }
        ;
        return conns;
    }
    
    private synchronized void initiateConnectionsReport() throws SQLException {
        pc_Report = new PooledConnection[POOL_SIZE_Report];
        SimpleProtector sb = new SimpleProtector();
        //ConnectionList = new ConnectionContainer();
        for (int i = 0; i < pc_Report.length; i++) {
            try {
                OracleConnectionPoolDataSource ocpds
                        = new OracleConnectionPoolDataSource();
                ocpds.setURL("jdbc:oracle:thin:@"
                        + PropertyReader.getProperty("ev.db_server_ip") + ":1521:"
                        + PropertyReader.getProperty("ev.db_sid"));
                ocpds.setUser(PropertyReader.getProperty("ev.db_user"));
                ocpds.setPassword(sb.decrypt(PropertyReader.getProperty("ev.db_pass")));
                pc_Report[i] = ocpds.getPooledConnection();
                //ConnectionList.add(Integer.toString(i), 0);
            } catch (Exception ex) {
                Logger.getLogger(PoolProvider.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public synchronized Connection getConnsrep() throws SQLException {
        
        if (pc_Report == null) {
            initiateConnectionsReport();
        }
        Connection conns = null;
        try {
            Thread.currentThread().sleep(5);
        } catch (InterruptedException e) {
            System.out.println(e);
        }
        conns = pc_Report[indexReport++].getConnection();
        if (indexReport == pc_Report.length) {
            indexReport = 0;
        }
        ;
        return conns;
    }
    
    private synchronized void initiateConnections() throws SQLException {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        System.out.println(session.getAttribute("username"));
        UserList.add(new UserCounter((String) session.getAttribute("username"), new Date()));
        pc_2 = new PooledConnection[POOL_SIZE];
        //ConnectionList = new ConnectionContainer();
        SimpleProtector sb = new SimpleProtector();
        for (int i = 0; i < pc_2.length; i++) {
            try {
                OracleConnectionPoolDataSource ocpds
                        = new OracleConnectionPoolDataSource();
                ocpds.setURL("jdbc:oracle:thin:@"
                        + PropertyReader.getProperty("ev.db_server_ip") + ":1521:"
                        + PropertyReader.getProperty("ev.db_sid"));
                ocpds.setUser(PropertyReader.getProperty("ev.db_user"));
                ocpds.setPassword(sb.decrypt(PropertyReader.getProperty("ev.db_pass")));
                pc_2[i] = ocpds.getPooledConnection();
                //ConnectionList.add(Integer.toString(i), 0);
            } catch (Exception ex) {
                Logger.getLogger(PoolProvider.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public synchronized Connection getConnsImp() throws SQLException {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        for (UserCounter record : UserList) {
            
            if (record.getUserID() == session.getAttribute("username")) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.add(Calendar.MINUTE, -10);
                if (record.getLastaccess().before(cal.getTime())) {
                    System.out.println("Found");
                    session.invalidate();
                }else{
                 record.setLastaccess(new Date());
                }
            }
        }
        
        if (pc_2 == null) {
            initiateConnections();
        }
        Connection conns = null;
        try {
            Thread.currentThread().sleep(5);
        } catch (InterruptedException e) {
            System.out.println(e);
        }
        conns = pc_2[index++].getConnection();
        //FacesContext context = FacesContext.getCurrentInstance();
        //HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        //session.setAttribute("Connection", (Connection) conns);
        if (index == pc_2.length) {
            index = 0;
        }
        ;
        //    if (conIndex > 749) {
        //      System.out.println(conIndex++ + "Index =" + index);
        // }
        //System.out.println(conIndex++ + "Index =" + index);
        return conns;
    }
    
    public void CloseConn() throws SQLException {
        
    }
    
    public void CloseConnection() throws SQLException {
        if (pc_2 != null) {
            for (int i = 0; i < pc_2.length; i++) {
                pc_2[i].close();
                pc_2[i] = null;
                System.gc();
            }
        }
    }
    
    public synchronized Connection getConns() throws SQLException {
        return getConnsImp();
    }
    
    public synchronized Connection getConnsreport() throws SQLException {
        return getConnsrep();
    }
    
    public synchronized Connection getConnsjobMon() throws SQLException {
        return getConnJobMon();
    }
    /*public static void main(String args[]) throws Exception{
     PoolProvider pp = new PoolProvider();
     //for(int i=0;i<41;i++){
     //        pp.getConns().createStatement().executeQuery("select * from tab");
     //}
     }*/
}
