package com.ev.Bingo.base.data;

public interface IDataDTO {

    public IEntityBean getEntity();

    public void setEntity(IEntityBean entity);

    public Object[] getAttributeValues();

    public void setAttributeValues(Object[] atts);

}
