/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.base.bo;

import com.ev.Bingo.base.dao.BaseDAO;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class BaseBO {

    public String GetDecryptCardNo() {
        return " decrypt(CARD_NO)  ";
    }

    public String GetDecryptLinc() {
        return " decryptnew(LICENCE_KEY) ";
    }

    public Date getTime() throws Throwable {
        BaseDAO bDAO = new BaseDAO();
        Date now = bDAO.getCurrentTime();
        return now;
    }
}
