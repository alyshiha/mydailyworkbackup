package com.ev.Bingo.bus.dto;

import java.io.Serializable;

public interface userprofileDTOInter extends Serializable {

    int getuserid();

    String getusername();

    void setusername(String username);

    void setuserid(int userid);

    int getprofileid();

    void setprofileid(int profileid);
}
