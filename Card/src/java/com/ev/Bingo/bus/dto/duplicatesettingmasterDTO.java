package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;

public class duplicatesettingmasterDTO extends BaseDTO implements duplicatesettingmasterDTOInter, Serializable {

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private int rectype;

    public int getrectype() {
        return rectype;
    }

    public void setrectype(int rectype) {
        this.rectype = rectype;
    }
    private int networkid;

    public int getnetworkid() {
        return networkid;
    }

    public void setnetworkid(int networkid) {
        this.networkid = networkid;
    }
}
