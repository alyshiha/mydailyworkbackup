package com.ev.Bingo.bus.dto;

import java.io.Serializable;

public interface transactiontypemasterDTOInter extends Serializable {

    int getid();

    void setid(int id);

    String getname();

    void setname(String name);
}
