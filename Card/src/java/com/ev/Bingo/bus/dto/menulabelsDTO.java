package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class menulabelsDTO extends BaseDTO implements menulabelsDTOInter, Serializable {

    private int menuid;

    public int getmenuid() {
        return menuid;
    }

    public void setmenuid(int menuid) {
        this.menuid = menuid;
    }
    private String arabiclabel;

    public String getarabiclabel() {
        return arabiclabel;
    }

    public void setarabiclabel(String arabiclabel) {
        this.arabiclabel = arabiclabel;
    }
    private String englishlabel;

    public String getenglishlabel() {
        return englishlabel;
    }

    public void setenglishlabel(String englishlabel) {
        this.englishlabel = englishlabel;
    }
    private int type;

    public int gettype() {
        return type;
    }

    public void settype(int type) {
        this.type = type;
    }
    private int parentid;

    public int getparentid() {
        return parentid;
    }

    public void setparentid(int parentid) {
        this.parentid = parentid;
    }
    private int formid;

    public int getformid() {
        return formid;
    }

    public void setformid(int formid) {
        this.formid = formid;
    }
    private int reportid;

    public int getreportid() {
        return reportid;
    }

    public void setreportid(int reportid) {
        this.reportid = reportid;
    }
    private int seq;

    public int getseq() {
        return seq;
    }

    public void setseq(int seq) {
        this.seq = seq;
    }
    private int visability;

    public int getvisability() {
        return visability;
    }

    public void setvisability(int visability) {
        this.visability = visability;
    }
    private String page;

    public String getpage() {
        return page;
    }

    public void setpage(String page) {
        this.page = page;
    }
    private int restrected;

    public int getrestrected() {
        return restrected;
    }

    public void setrestrected(int restrected) {
        this.restrected = restrected;
    }
}
