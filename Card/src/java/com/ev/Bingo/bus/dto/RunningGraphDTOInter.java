/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface RunningGraphDTOInter extends Serializable {

    int getCountRunning();

    String getLabel();

    void setCountRunning(int countRunning);

    void setLabel(String label);

    int getCountDown();

    void setCountDown(int countDown);

    int getCountStandBy();

    void setCountStandBy(int countStandBy);

    int getCoutnBroken();

    void setCoutnBroken(int coutnBroken);
}
