/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author Aly
 */
public interface cardfiletemplateDTOInter {

    int getactive();

    Boolean getactiveB();

    String getbackupfolder();

    String getcopyfolder();

    String getdateseparator();

    String getdateseparatorpos1();

    String getdateseparatorpos2();

    int getheaderdatatype();

    String getheaderdateseparator();

    String getheaderdateseparatorpos1();

    String getheaderdateseparatorpos2();

    String getheaderformat();

    String getheaderlength();

    String getheaderlines();

    String getheaderposition();

    String getheaderstring();

    int getid();

    String getignoredlines();

    String getloadingfolder();

    String getmaxtaglines();

    String getname();

    int getnetworkid();

    String getnumberoflines();

    int getprocessingtype();

    String getseparator();

    String getserverfolder();

    int getstartingdatatype();

    String getstartingformat();

    String getstartinglength();

    String getstartingposition();

    String getstartingvalue();

    int gettagsendingdatatype();

    void setTagsendfor(String tagsendfor);

    String getTagsendfor();

    String gettagsendinglength();

    String gettagsendingposition();

    String gettagsendingvalue();

    void setactive(int active);

    void setactiveB(Boolean activeB);

    void setbackupfolder(String backupfolder);

    void setcopyfolder(String copyfolder);

    void setdateseparator(String dateseparator);

    void setdateseparatorpos1(String dateseparatorpos1);

    void setdateseparatorpos2(String dateseparatorpos2);

    void setheaderdatatype(int headerdatatype);

    void setheaderdateseparator(String headerdateseparator);

    void setheaderdateseparatorpos1(String headerdateseparatorpos1);

    void setheaderdateseparatorpos2(String headerdateseparatorpos2);

    void setheaderformat(String headerformat);

    void setheaderlength(String headerlength);

    void setheaderlines(String headerlines);

    void setheaderposition(String headerposition);

    void setheaderstring(String headerstring);

    void setid(int id);

    void setignoredlines(String ignoredlines);

    void setloadingfolder(String loadingfolder);

    void setmaxtaglines(String maxtaglines);

    void setname(String name);

    void setnetworkid(int networkid);

    void setnumberoflines(String numberoflines);

    void setprocessingtype(int processingtype);

    void setseparator(String separator);

    void setserverfolder(String serverfolder);

    void setstartingdatatype(int startingdatatype);

    void setstartingformat(String startingformat);

    void setstartinglength(String startinglength);

    void setstartingposition(String startingposition);

    void setstartingvalue(String startingvalue);

    void settagsendingdatatype(int tagsendingdatatype);


    void settagsendinglength(String tagsendinglength);

    void settagsendingposition(String tagsendingposition);

    void settagsendingvalue(String tagsendingvalue);

}
