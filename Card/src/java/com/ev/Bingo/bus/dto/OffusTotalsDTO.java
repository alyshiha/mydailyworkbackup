/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.math.BigDecimal;

/**
 *
 * @author Aly-Shiha
 */
public class OffusTotalsDTO implements OffusTotalsDTOInter {

    private Integer Count;
    private BigDecimal RelatedIncome;
    private String Type;
    private String Terminal;
    private String TerminalName;
    private String TransMonth;

    public OffusTotalsDTO() {
    }

    public OffusTotalsDTO(Integer Count, BigDecimal RelatedIncome, String Type, String Terminal, String TerminalName, String TransMonth) {
        this.Count = Count;
        this.RelatedIncome = RelatedIncome;
        this.Type = Type;
        this.Terminal = Terminal;
        this.TerminalName = TerminalName;
        this.TransMonth = TransMonth;
    }

    @Override
    public Integer getCount() {
        return Count;
    }

    @Override
    public void setCount(Integer Count) {
        this.Count = Count;
    }

    @Override
    public BigDecimal getRelatedIncome() {
        return RelatedIncome;
    }

    @Override
    public void setRelatedIncome(BigDecimal RelatedIncome) {
        this.RelatedIncome = RelatedIncome;
    }

    @Override
    public String getType() {
        return Type;
    }

    @Override
    public void setType(String Type) {
        this.Type = Type;
    }

    @Override
    public String getTerminal() {
        return Terminal;
    }

    @Override
    public void setTerminal(String Terminal) {
        this.Terminal = Terminal;
    }

    @Override
    public String getTerminalName() {
        return TerminalName;
    }

    @Override
    public void setTerminalName(String TerminalName) {
        this.TerminalName = TerminalName;
    }

    @Override
    public String getTransMonth() {
        return TransMonth;
    }

    @Override
    public void setTransMonth(String TransMonth) {
        this.TransMonth = TransMonth;
    }

}
