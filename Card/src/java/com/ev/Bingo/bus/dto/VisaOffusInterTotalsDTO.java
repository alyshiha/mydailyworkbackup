/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.math.BigDecimal;

/**
 *
 * @author Aly-Shiha
 */
public class VisaOffusInterTotalsDTO implements VisaOffusInterTotalsDTOInter {

    private Integer Count;
    private BigDecimal TotalAmount;
    private BigDecimal Income;
    private String Type;
    private String Terminal;
    private String TerminalName;
    private String TransMonth;

    public VisaOffusInterTotalsDTO() {
    }

    public VisaOffusInterTotalsDTO(Integer Count, BigDecimal TotalAmount, BigDecimal Income, String Type, String Terminal, String TerminalName, String TransMonth) {
        this.Count = Count;
        this.TotalAmount = TotalAmount;
        this.Income = Income;
        this.Type = Type;
        this.Terminal = Terminal;
        this.TerminalName = TerminalName;
        this.TransMonth = TransMonth;
    }

    @Override
    public Integer getCount() {
        return Count;
    }

    @Override
    public void setCount(Integer Count) {
        this.Count = Count;
    }

    @Override
    public BigDecimal getTotalAmount() {
        return TotalAmount;
    }

    @Override
    public void setTotalAmount(BigDecimal TotalAmount) {
        this.TotalAmount = TotalAmount;
    }

    @Override
    public BigDecimal getIncome() {
        return Income;
    }

    @Override
    public void setIncome(BigDecimal Income) {
        this.Income = Income;
    }

    @Override
    public String getType() {
        return Type;
    }

    @Override
    public void setType(String Type) {
        this.Type = Type;
    }

    @Override
    public String getTerminal() {
        return Terminal;
    }

    @Override
    public void setTerminal(String Terminal) {
        this.Terminal = Terminal;
    }

    @Override
    public String getTerminalName() {
        return TerminalName;
    }

    @Override
    public void setTerminalName(String TerminalName) {
        this.TerminalName = TerminalName;
    }

    @Override
    public String getTransMonth() {
        return TransMonth;
    }

    @Override
    public void setTransMonth(String TransMonth) {
        this.TransMonth = TransMonth;
    }

}
