/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class AtmMachineDTO extends BaseDTO implements AtmMachineDTOInter, Serializable {
    private int id;
    private AtmGroupDTOInter atmGroup;
    private String applicationId;
    private String name;
    private String location;
    private int machineType;
    private String unitNumber;
    private String SerialNo;
    private String applicationId2;
private int GroupId;

    public int getGroupId() {
        return GroupId;
    }

    public void setGroupId(int GroupId) {
        this.GroupId = GroupId;
    }


    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String SerialNo) {
        this.SerialNo = SerialNo;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationId2() {
        return applicationId2;
    }

    public void setApplicationId2(String applicationId2) {
        this.applicationId2 = applicationId2;
    }


  
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getMachineType() {
        return machineType;
    }

    public void setMachineType(int machineType) {
        this.machineType = machineType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }
    
}
