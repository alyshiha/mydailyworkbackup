/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface ExceptionLoadingErrorsDTOInter {

    int getErrorCode();

    Date getErrorDate();

    String getErrorName();

    String getFunctionName();

    void setErrorCode(int errorCode);

    void setErrorDate(Date errorDate);

    void setErrorName(String errorName);

    void setFunctionName(String functionName);

}
