package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class matchingtypesettingDTO extends BaseDTO implements matchingtypesettingDTOInter, Serializable {

    private String rowid;

    public String getRowid() {
        return rowid;
    }

    public void setRowid(String rowid) {
        this.rowid = rowid;
    }
    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private int columnid;

    public int getcolumnid() {
        return columnid;
    }

    public void setcolumnid(int columnid) {
        this.columnid = columnid;
    }
    private int partsetting;

    public int getpartsetting() {
        return partsetting;
    }

    public void setpartsetting(int partsetting) {
        this.partsetting = partsetting;
    }
    private String partlength;

    public String getpartlength() {
        return partlength;
    }

    public void setpartlength(String partlength) {
        this.partlength = partlength;
    }
    private int networkid;

    public int getnetworkid() {
        return networkid;
    }

    public void setnetworkid(int networkid) {
        this.networkid = networkid;
    }
}
