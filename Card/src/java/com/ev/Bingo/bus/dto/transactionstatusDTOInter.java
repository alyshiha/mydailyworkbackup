package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface transactionstatusDTOInter extends Serializable {

    int getid();

    void setid(int id);

    String getname();

    void setname(String name);

    String getdescription();

    void setdescription(String description);
}
