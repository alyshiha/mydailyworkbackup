package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface networkdefaultcolumnsDTOInter extends Serializable {

    Boolean getbselected();

    int getOrderby();

    void setOrderby(int orderby);

    void setbselected(Boolean bselected);

    int getnetworkid();

    void setnetworkid(int networkid);

    int getcolumnid();

    public void setrowid(String rowid);

    String getrowid();

    void setselected(int selected);

    int getselected();

    void setcolumnid(int columnid);
}
