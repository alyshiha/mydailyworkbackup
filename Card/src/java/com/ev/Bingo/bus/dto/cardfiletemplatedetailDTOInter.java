/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Aly
 */
public interface cardfiletemplatedetailDTOInter extends Serializable {

    Boolean getDecimalposboolean();

    Boolean getMandatoryboolean();

    Boolean getNegativeamountflagboolean();

    String getadddecimal();

    int getcolumnid();

    int getdatatype();

    int getdecimalpos();

    String getformat();

    String getformat2();

    int getid();

    String getlength();

    String getlengthdir();

    String getlinenumber();

    String getloadwhenmapped();

    int getmandatory();

    int getnegativeamountflag();

    String getposition();

    String getstartingposition();

    String gettagstring();

    int gettemplate();

    void setDecimalposboolean(Boolean decimalposboolean);

    void setMandatoryboolean(Boolean mandatoryboolean);

    void setNegativeamountflagboolean(Boolean negativeamountflagboolean);

    void setadddecimal(String adddecimal);

    void setcolumnid(int columnid);

    void setdatatype(int datatype);

    void setdecimalpos(int decimalpos);

    void setformat(String format);

    void setformat2(String format2);

    void setid(int id);

    void setlength(String length);

    void setlengthdir(String lengthdir);

    void setlinenumber(String linenumber);

    void setloadwhenmapped(String loadwhenmapped);

    void setmandatory(int mandatory);

    void setnegativeamountflag(int negativeamountflag);

    void setposition(String position);

    void setstartingposition(String startingposition);

    void settagstring(String tagstring);

    void settemplate(int template);
    
}
