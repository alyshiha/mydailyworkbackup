package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface licenseDTOInter extends Serializable {

    Date getcreateddate();

    void setcreateddate(Date createddate);

    int getatmno();

    void setatmno(int atmno);

    Date getenddate();

    void setenddate(Date enddate);

    void setnooftrans(String nooftrans);

    String getnooftrans();

    String getlicensekey();

    void setlicensekey(String licensekey);

    int getnoofusers();

    void setnoofusers(int noofusers);

    String getlicenseto();

    void setlicenseto(String licenseto);

    String getcountry();

    void setcountry(String country);

    String getversion();

    void setversion(String version);
}
