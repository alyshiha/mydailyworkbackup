/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author Aly
 */
public class BankCodeDTO implements BankCodeDTOInter {

    String bankname;
    int id;

    @Override
    public String getBankname() {
        return bankname;
    }

    @Override
    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

}
