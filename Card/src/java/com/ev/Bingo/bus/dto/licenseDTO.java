package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class licenseDTO extends BaseDTO implements licenseDTOInter, Serializable {

    private Date createddate;

    public Date getcreateddate() {
        return createddate;
    }

    public void setcreateddate(Date createddate) {
        this.createddate = createddate;
    }
    private int atmno;

    public int getatmno() {
        return atmno;
    }

    public void setatmno(int atmno) {
        this.atmno = atmno;
    }
    private Date enddate;

    public Date getenddate() {
        return enddate;
    }

    public void setenddate(Date enddate) {
        this.enddate = enddate;
    }
    private String licensekey;

    public String getlicensekey() {
        return licensekey;
    }

    public void setlicensekey(String licensekey) {
        this.licensekey = licensekey;
    }
    private int noofusers;

    public int getnoofusers() {
        return noofusers;
    }

    public void setnoofusers(int noofusers) {
        this.noofusers = noofusers;
    }
    private String licenseto;

    public String getlicenseto() {
        return licenseto;
    }

    public void setlicenseto(String licenseto) {
        this.licenseto = licenseto;
    }
    private String country;

    public String getcountry() {
        return country;
    }

    public void setcountry(String country) {
        this.country = country;
    }
    private String nooftrans;

    public String getnooftrans() {
        return nooftrans;
    }

    public void setnooftrans(String nooftrans) {
        this.nooftrans = nooftrans;
    }
    private String version;

    public String getversion() {
        return version;
    }

    public void setversion(String version) {
        this.version = version;
    }
}
