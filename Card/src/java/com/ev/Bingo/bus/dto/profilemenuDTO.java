package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class profilemenuDTO extends BaseDTO implements profilemenuDTOInter, Serializable {

    private int profileid;

    public int getprofileid() {
        return profileid;
    }

    public void setprofileid(int profileid) {
        this.profileid = profileid;
    }
    private int menuid;

    public int getmenuid() {
        return menuid;
    }

    public void setmenuid(int menuid) {
        this.menuid = menuid;
    }
}
