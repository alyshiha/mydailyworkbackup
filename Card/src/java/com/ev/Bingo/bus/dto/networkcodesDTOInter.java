package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface networkcodesDTOInter extends Serializable {

    int getnetworkid();

    void setnetworkid(int networkid);

    String getnetworkcode();

    void setnetworkcode(String networkcode);

    void setrowid(String rowid);

    String getrowid();
}
