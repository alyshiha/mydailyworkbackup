/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface DiffCardAtmJournalDTOInter {

    void setFlag(Integer flag);

    void setDiffdate(Date diffdate);
    Integer getDiffamount();
    void setDiffamount(Integer diffamount) ;
    Date getDiffdate();

    Integer getFlag();

    Integer getAtmamount();

    Date getAtmfromdate();

    String getAtmid();

    Date getAtmtodate();

    String getBranch();

    Integer getCardamount();

    String getCreditaccount();

    String getDebitaccount();

    Integer getDiffid();

    Integer getExportflag();

    String getIndication();

    Date getJournaldate();

    String getJournaltype();

    Integer getOperation();

    void setAtmamount(Integer atmamount);

    void setAtmfromdate(Date atmfromdate);

    void setAtmid(String atmid);

    void setAtmtodate(Date atmtodate);

    void setBranch(String branch);

    void setCardamount(Integer cardamount);

    void setCreditaccount(String creditaccount);

    void setDebitaccount(String debitaccount);

    void setDiffid(Integer diffid);

    void setExportflag(Integer exportflag);

    void setIndication(String indication);

    void setJournaldate(Date journaldate);

    void setJournaltype(String journaltype);

    void setOperation(Integer operation);

}
