/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class DiffCardAtmTransJournalDTO implements DiffCardAtmTransJournalDTOInter {

    private Integer diffid;
    private String cardno;
    private String networkid;
    private String transactionsequence;
    private String transactionstatus;
    private String transactiontype;
    private String atmid;
    private Date transactiondate;
    private Date settlementdate;
    private String responsecode;
    private String customeraccountnumber;
    private String referenceno;
    private String processcode;
    private Integer amount;
    private Integer settlementamount;
    private String currency;
    private String acq;
    private String issuer;
    private Integer diffparentid;
private String transside;
    
    public DiffCardAtmTransJournalDTO() {
    }

    public DiffCardAtmTransJournalDTO(Integer diffid, String cardno, String networkid, String transactionsequence, String transactionstatus, String transactiontype, String atmid, Date transactiondate, Date settlementdate, String responsecode, String customeraccountnumber, String referenceno, String processcode, Integer amount, Integer settlementamount, String currency, String acq, String issuer, Integer diffparentid) {
        this.diffid = diffid;
        this.cardno = cardno;
        this.networkid = networkid;
        this.transactionsequence = transactionsequence;
        this.transactionstatus = transactionstatus;
        this.transactiontype = transactiontype;
        this.atmid = atmid;
        this.transactiondate = transactiondate;
        this.settlementdate = settlementdate;
        this.responsecode = responsecode;
        this.customeraccountnumber = customeraccountnumber;
        this.referenceno = referenceno;
        this.processcode = processcode;
        this.amount = amount;
        this.settlementamount = settlementamount;
        this.currency = currency;
        this.acq = acq;
        this.issuer = issuer;
        this.diffparentid = diffparentid;
    }

    @Override
    public Integer getDiffid() {
        return diffid;
    }

    public String getTransside() {
        return transside;
    }

    public void setTransside(String transside) {
        this.transside = transside;
    }

    @Override
    public void setDiffid(Integer diffid) {
        this.diffid = diffid;
    }

    @Override
    public String getCardno() {
        return cardno;
    }

    @Override
    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    @Override
    public String getNetworkid() {
        return networkid;
    }

    @Override
    public void setNetworkid(String networkid) {
        this.networkid = networkid;
    }

    @Override
    public String getTransactionsequence() {
        return transactionsequence;
    }

    @Override
    public void setTransactionsequence(String transactionsequence) {
        this.transactionsequence = transactionsequence;
    }

    @Override
    public String getTransactionstatus() {
        return transactionstatus;
    }

    @Override
    public void setTransactionstatus(String transactionstatus) {
        this.transactionstatus = transactionstatus;
    }

    @Override
    public String getTransactiontype() {
        return transactiontype;
    }

    @Override
    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    @Override
    public String getAtmid() {
        return atmid;
    }

    @Override
    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    @Override
    public Date getTransactiondate() {
        return transactiondate;
    }

    @Override
    public void setTransactiondate(Date transactiondate) {
        this.transactiondate = transactiondate;
    }

    @Override
    public Date getSettlementdate() {
        return settlementdate;
    }

    @Override
    public void setSettlementdate(Date settlementdate) {
        this.settlementdate = settlementdate;
    }

    @Override
    public String getResponsecode() {
        return responsecode;
    }

    @Override
    public void setResponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

    @Override
    public String getCustomeraccountnumber() {
        return customeraccountnumber;
    }

    @Override
    public void setCustomeraccountnumber(String customeraccountnumber) {
        this.customeraccountnumber = customeraccountnumber;
    }

    @Override
    public String getReferenceno() {
        return referenceno;
    }

    @Override
    public void setReferenceno(String referenceno) {
        this.referenceno = referenceno;
    }

    @Override
    public String getProcesscode() {
        return processcode;
    }

    @Override
    public void setProcesscode(String processcode) {
        this.processcode = processcode;
    }

    @Override
    public Integer getAmount() {
        return amount;
    }

    @Override
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public Integer getSettlementamount() {
        return settlementamount;
    }

    @Override
    public void setSettlementamount(Integer settlementamount) {
        this.settlementamount = settlementamount;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String getAcq() {
        return acq;
    }

    @Override
    public void setAcq(String acq) {
        this.acq = acq;
    }

    @Override
    public String getIssuer() {
        return issuer;
    }

    @Override
    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    @Override
    public Integer getDiffparentid() {
        return diffparentid;
    }

    @Override
    public void setDiffparentid(Integer diffparentid) {
        this.diffparentid = diffparentid;
    }

}
