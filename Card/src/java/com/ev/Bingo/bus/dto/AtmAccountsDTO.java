/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public class AtmAccountsDTO implements AtmAccountsDTOInter {

    private Integer id;
    private String atmid;
    private String atmapplicationid;
    private String description;
    private String accountid;
    private String branch;
    private String accountname;
    private Integer accountnameid;
private Integer groupid;
private String groupvalue;

    public Integer getGroupid() {
        return groupid;
    }

    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }

    public String getGroupvalue() {
        return groupvalue;
    }

    public void setGroupvalue(String groupvalue) {
        this.groupvalue = groupvalue;
    }

    public AtmAccountsDTO() {
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public AtmAccountsDTO(Integer id, String atmid, String atmapplicationid, String description, String accountid, Integer accountname) {
        this.id = id;
        this.atmid = atmid;
        this.atmapplicationid = atmapplicationid;
        this.description = description;
        this.accountid = accountid;
        this.accountnameid = accountname;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getAtmid() {
        return atmid;
    }

    @Override
    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    @Override
    public String getAtmapplicationid() {
        return atmapplicationid;
    }

    @Override
    public void setAtmapplicationid(String atmapplicationid) {
        this.atmapplicationid = atmapplicationid;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getAccountid() {
        return accountid;
    }

    @Override
    public void setAccountid(String accountid) {
        this.accountid = accountid;
    }

    @Override
    public Integer getAccountnameid() {
        return accountnameid;
    }

    @Override
    public void setAccountnameid(Integer accountnameid) {
        this.accountnameid = accountnameid;
    }
    
    

}
