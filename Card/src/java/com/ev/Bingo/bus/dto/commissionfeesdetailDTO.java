package com.ev.Bingo.bus.dto;
import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;
public class commissionfeesdetailDTO extends BaseDTO implements commissionfeesdetailDTOInter, Serializable {
private int id;
public int getid(){
return id;
}
public void setid(int id) {
this.id = id;
}
private int commissionfeesid;
public int getcommissionfeesid(){
return commissionfeesid;
}
public void setcommissionfeesid(int commissionfeesid) {
this.commissionfeesid = commissionfeesid;
}
private int transtype;
public int gettranstype(){
return transtype;
}
public void settranstype(int transtype) {
this.transtype = transtype;
}
private float comm;
public float getcomm(){
return comm;
}
public void setcomm(float comm) {
this.comm = comm;
}
private float fees;
public float getfees(){
return fees;
}
public void setfees(float fees) {
this.fees = fees;
}
private int cardtype;
public int getcardtype(){
return cardtype;
}
public void setcardtype(int cardtype) {
this.cardtype = cardtype;
}
}