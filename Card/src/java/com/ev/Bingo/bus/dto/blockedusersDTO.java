package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class blockedusersDTO extends BaseDTO implements blockedusersDTOInter, Serializable {

    private int userid;
    private String username, reasonname;

    private Date blocktime;

    private int reasonid;

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getReasonname() {
        return reasonname;
    }

    public void setReasonname(String reasonname) {
        this.reasonname = reasonname;
    }

    public Date getBlocktime() {
        return blocktime;
    }

    public void setBlocktime(Date blocktime) {
        this.blocktime = blocktime;
    }

    public int getReasonid() {
        return reasonid;
    }

    public void setReasonid(int reasonid) {
        this.reasonid = reasonid;
    }

}
