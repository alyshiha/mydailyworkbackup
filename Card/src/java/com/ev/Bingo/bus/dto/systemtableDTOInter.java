package com.ev.Bingo.bus.dto;

import java.io.Serializable;

public interface systemtableDTOInter extends Serializable {

    String getparametername();

    void setparametername(String parametername);

    String getparametervalue();

    void setparametervalue(String parametervalue);

    int getparameterdatatype();

    void setparameterdatatype(int parameterdatatype);
}
