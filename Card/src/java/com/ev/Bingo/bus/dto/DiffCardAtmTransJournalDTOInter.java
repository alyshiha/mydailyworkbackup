/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface DiffCardAtmTransJournalDTOInter {
String getTransside();
 void setTransside(String transside);
    String getAcq();

    Integer getAmount();

    String getAtmid();

    String getCardno();

    String getCurrency();

    String getCustomeraccountnumber();

    Integer getDiffid();

    Integer getDiffparentid();

    String getIssuer();

    String getNetworkid();

    String getProcesscode();

    String getReferenceno();

    String getResponsecode();

    Integer getSettlementamount();

    Date getSettlementdate();

    Date getTransactiondate();

    String getTransactionsequence();

    String getTransactionstatus();

    String getTransactiontype();

    void setAcq(String acq);

    void setAmount(Integer amount);

    void setAtmid(String atmid);

    void setCardno(String cardno);

    void setCurrency(String currency);

    void setCustomeraccountnumber(String customeraccountnumber);

    void setDiffid(Integer diffid);

    void setDiffparentid(Integer diffparentid);

    void setIssuer(String issuer);

    void setNetworkid(String networkid);

    void setProcesscode(String processcode);

    void setReferenceno(String referenceno);

    void setResponsecode(String responsecode);

    void setSettlementamount(Integer settlementamount);

    void setSettlementdate(Date settlementdate);

    void setTransactiondate(Date transactiondate);

    void setTransactionsequence(String transactionsequence);

    void setTransactionstatus(String transactionstatus);

    void setTransactiontype(String transactiontype);
    
}
