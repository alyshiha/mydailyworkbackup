/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Administrator
 */
public interface AtmGroupDTOInter extends Serializable{

    int getLev() ;

     void setLev(int lev);

    String getDescription();

    int getId();

    String getName();

    int getParentId();

    void setDescription(String description);

    void setId(int id);

    void setName(String name);

    void setParentId(int parentId);

}
