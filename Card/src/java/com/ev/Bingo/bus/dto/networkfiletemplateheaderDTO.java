package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;

public class networkfiletemplateheaderDTO extends BaseDTO implements networkfiletemplateheaderDTOInter, Serializable {

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private int template;

    public int gettemplate() {
        return template;
    }

    public void settemplate(int template) {
        this.template = template;
    }
    private int columnid;

    public int getcolumnid() {
        return columnid;
    }

    public void setcolumnid(int columnid) {
        this.columnid = columnid;
    }
    private int position;

    public int getposition() {
        return position;
    }

    public void setposition(int position) {
        this.position = position;
    }
    private String format;

    public String getformat() {
        return format;
    }

    public void setformat(String format) {
        this.format = format;
    }
    private String format2;

    public String getformat2() {
        return format2;
    }

    public void setformat2(String format2) {
        this.format2 = format2;
    }
    private int datatype;

    public int getdatatype() {
        return datatype;
    }

    public void setdatatype(int datatype) {
        this.datatype = datatype;
    }
    private int length;

    public int getlength() {
        return length;
    }

    public void setlength(int length) {
        this.length = length;
    }
    private int lineno;

    public int getlineno() {
        return lineno;
    }

    public void setlineno(int lineno) {
        this.lineno = lineno;
    }
}
