package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface matchingtypesettingDTOInter extends Serializable {

    int getid();

    void setid(int id);

    int getcolumnid();

    void setcolumnid(int columnid);

    int getpartsetting();

    void setpartsetting(int partsetting);

    String getpartlength();

    void setpartlength(String partlength);

    int getnetworkid();

    void setRowid(String rowid);

    String getRowid();

    void setnetworkid(int networkid);
}
