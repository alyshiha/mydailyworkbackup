package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class transactiontypemasterDTO extends BaseDTO implements transactiontypemasterDTOInter, Serializable {

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private String name;

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }
}
