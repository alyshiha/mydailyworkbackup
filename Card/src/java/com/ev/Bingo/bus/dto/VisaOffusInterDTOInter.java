/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.math.BigDecimal;

/**
 *
 * @author shi7a
 */
public interface VisaOffusInterDTOInter {
String getTerminal();
void setTerminal(String terminal);
    Integer getCount();

    BigDecimal getIncome();

    BigDecimal getTotalamount();

    String getType();

    void setCount(Integer count);

    void setIncome(BigDecimal income);

    void setTotalamount(BigDecimal totalamount);

    void setType(String type);
    
}
