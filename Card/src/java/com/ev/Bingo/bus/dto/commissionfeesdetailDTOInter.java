package com.ev.Bingo.bus.dto;
import java.io.Serializable;
import java.util.Date;
public interface commissionfeesdetailDTOInter extends Serializable {
int getid();
void setid(int id);
int getcommissionfeesid();
void setcommissionfeesid(int commissionfeesid);
int gettranstype();
void settranstype(int transtype);
float getcomm();
void setcomm(float comm);
float getfees();
void setfees(float fees);
int getcardtype();
void setcardtype(int cardtype);
}