package com.ev.Bingo.bus.dto;

public class DTOFactory {

    public DTOFactory() {
    }

    public static AtmMachineDTOInter createAtmMachineDTO(Object... obj) {
        return new AtmMachineDTO();
    }
        public static OnusTotalsDTOInter createOnusTotalsDTO(Object... obj) {
        return new OnusTotalsDTO();
    }
        public static VisaOffusInterTotalsDTOInter createVisaOffusInterTotalsDTO(Object... obj) {
        return new VisaOffusInterTotalsDTO();
    }

    public static VisaOffusTotalsDTOInter createVisaOffusTotalsDTO(Object... obj) {
        return new VisaOffusTotalsDTO();
    }

    public static OffusTotalsDTOInter createOffusTotalsDTO(Object... obj) {
        return new OffusTotalsDTO();
    }

    public static OnusDTOInter createOnusDTO(Object... obj) {
        return new OnusDTO();
    }

    public static VisaOffusInterDTOInter createVisaOffusInterDTO(Object... obj) {
        return new VisaOffusInterDTO();
    }

    public static VisaOffusDTOInter createVisaOffusDTO(Object... obj) {
        return new VisaOffusDTO();
    }

    public static OffusDTOInter createOffusDTO(Object... obj) {
        return new OffusDTO();
    }

    public static timeFromSwitchDTOInter createtimeFromSwitchDTO(Object... obj) {
        return new timeFromSwitchDTO();
    }

    public static settlementsDTOInter createsettlementsDTO(Object... obj) {
        return new settlementsDTO();
    }

    public static RejectedVisaDTOInter createRejectedVisaDTO(Object... obj) {
        return new RejectedVisaDTO();
    }

    public static HolidaysDTOInter createHolidaysDTO(Object... obj) {
        return new HolidaysDTO();
    }

    public static DiffCardAtmTransJournalDTOInter createDiffCardAtmTransJournalDTO(Object... obj) {
        return new DiffCardAtmTransJournalDTO();
    }

    public static DiffCardAtmJournalDTOInter createDiffCardAtmJournalDTO(Object... obj) {
        return new DiffCardAtmJournalDTO();
    }

    public static AtmJournalDTOInter createAtmJournalDTO(Object... obj) {
        return new AtmJournalDTO();
    }

    public static ExceptionLoadingErrorsDTOInter createExceptionLoadingErrorsDTO(Object... obj) {
        return new ExceptionLoadingErrorsDTO();
    }

    public static ATMMACHINECASHDTOINTER createATMMACHINECASHDTO(Object... obj) {
        return new ATMMACHINECASHDTO();
    }

    public static CashManagmentLogDTOInter createCashManagmentLogDTO(Object... obj) {
        return new CashManagmentLogDTO();
    }

    public static AtmAccountsDTOInter createAtmAccountsDTO(Object... obj) {
        return new AtmAccountsDTO();
    }

    public static NetworkTypeCodeDTOInter createNetworkTypeCodeDTO(Object... obj) {
        return new NetworkTypeCodeDTO();
    }

    public static AccountNameDTOInter createAccountNameDTO(Object... obj) {
        return new AccountNameDTO();
    }

    public static privilegeStatusNameDTOInter createprivilegeStatusNameDTO(Object... obj) {
        return new privilegeStatusNameDTO();
    }

    public static userPrivilegeDTOInter createuserPrivilegeDTO(Object... obj) {
        return new userPrivilegeDTO();
    }

    public static Col4DTOInter createCol4DTO(Object... obj) {
        return new Col4DTO();
    }

    public static BankCodeDTOInter createBankCodeDTO(Object... obj) {
        return new BankCodeDTO();
    }

    public static CommFeeReportDTOInter createCommFeeReportDTO(Object... obj) {
        return new CommFeeReportDTO();
    }

    public static transactionstatisticsrepDTOInter createtransactionstatisticsrepDTO(Object... obj) {
        return new transactionstatisticsrepDTO();
    }

    public static commissionfeesDTOInter createcommissionfeesDTO(Object... obj) {
        return new commissionfeesDTO();
    }

    public static commissionfeesdetailDTOInter createcommissionfeesdetailDTO(Object... obj) {
        return new commissionfeesdetailDTO();
    }

    public static diffamountreportDTOInter creatediffamountreportDTO(Object... obj) {
        return new diffamountreportDTO();
    }

    public static transactionmanagementDTOInter createtransactionmanagementDTO(Object... obj) {
        return new transactionmanagementDTO();
    }

    public static UserActivityDTOInter createUserActivityDTO(Object... obj) {
        return new UserActivityDTO();
    }

    public static cardfiletemplatedetailDTOInter createcardfiletemplatedetailDTO(Object... obj) {
        return new cardfiletemplatedetailDTO();
    }

    public static cardfiletemplateDTOInter createcardfiletemplateDTO(Object... obj) {
        return new cardfiletemplateDTO();
    }

    public static networkfiletemplateheaderDTOInter createnetworkfiletemplateheaderDTO(Object... obj) {
        return new networkfiletemplateheaderDTO();
    }

    public static hostfiletemplateDTOInter createhostfiletemplateDTO(Object... obj) {
        return new hostfiletemplateDTO();
    }

    public static hostfiletemplatedetailDTOInter createhostfiletemplatedetailDTO(Object... obj) {
        return new hostfiletemplatedetailDTO();
    }

    public static filetemplateheaderDTOInter createfiletemplateheaderDTO(Object... obj) {
        return new filetemplateheaderDTO();
    }

    public static switchfiletemplateDTOInter createswitchfiletemplateDTO(Object... obj) {
        return new switchfiletemplateDTO();
    }

    public static switchfiletemplatedetailDTOInter createswitchfiletemplatedetailDTO(Object... obj) {
        return new switchfiletemplatedetailDTO();
    }

    public static switchfiletemplateheaderDTOInter createswitchfiletemplateheaderDTO(Object... obj) {
        return new switchfiletemplateheaderDTO();
    }

    public static UserNetworkDTOInter createUserNetworkDTO(Object... obj) {
        return new UserNetworkDTO();
    }

    public static matchingtypeDTOInter creatematchingtypeDTO(Object... obj) {
        return new matchingtypeDTO();
    }

    public static matchingtypenetworksDTOInter creatematchingtypenetworksDTO(Object... obj) {
        return new matchingtypenetworksDTO();
    }

    public static matchingtypesettingDTOInter creatematchingtypesettingDTO(Object... obj) {
        return new matchingtypesettingDTO();
    }

    public static DAshBoardDTOInter createDAshBoardDTO(Object... obj) {
        return new DAshBoardDTO();
    }

    public static networkdefaultcolumnsDTOInter createnetworkdefaultcolumnsDTO(Object... obj) {
        return new networkdefaultcolumnsDTO();
    }

    public static settlementlogsearchDTOInter createsettlementlogsearchDTO(Object... obj) {
        return new settlementlogsearchDTO();
    }

    public static DiffAmountDTOInter createDiffAmountDTO(Object... obj) {
        return new DiffAmountDTO();
    }

    public static settlementlogDTOInter createsettlementlogDTO(Object... obj) {
        return new settlementlogDTO();
    }

    public static TransSearchOperatorsDTOInter createtranssearchoperatorsDTO(Object... obj) {
        return new TransSearchOperatorsDTO();
    }

    public static TranSearchFieldDTOInter createtransearchfieldDTO(Object... obj) {
        return new TranSearchFieldDTO();
    }

    public static JobsStatusDTOInter createJobsStatusDTO(Object... obj) {
        return new JobsStatusDTO();
    }

    public static disputesDTOInter createdisputesDTO(Object... obj) {
        return new disputesDTO();
    }

    public static matchingerrorsdataDTOInter creatematchingerrorsdataDTO(Object... obj) {
        return new matchingerrorsdataDTO();
    }

    public static atmfileDTOInter createatmfileDTO(Object... obj) {
        return new atmfileDTO();
    }

    public static columnssetupdetailsDTOInter createcolumnssetupdetailsDTO(Object... obj) {
        return new columnssetupdetailsDTO();
    }

    public static disputerulesDTOInter createdisputerulesDTO(Object... obj) {
        return new disputerulesDTO();
    }

    public static transactionstatusDTOInter createtransactionstatusDTO(Object... obj) {
        return new transactionstatusDTO();
    }

    public static validationrulesDTOInter createvalidationrulesDTO(Object... obj) {
        return new validationrulesDTO();
    }

    public static columnssetupDTOInter createcolumnssetupDTO(Object... obj) {
        return new columnssetupDTO();
    }

    public static networksDTOInter createnetworksDTO(Object... obj) {
        return new networksDTO();
    }

    public static filecolumndefinitionDTOInter createfilecolumndefinitionDTO(Object... obj) {
        return new filecolumndefinitionDTO();
    }

    public static duplicatesettingdetailDTOInter createduplicatesettingdetailDTO(Object... obj) {
        return new duplicatesettingdetailDTO();
    }

    public static duplicatesettingmasterDTOInter createduplicatesettingmasterDTO(Object... obj) {
        return new duplicatesettingmasterDTO();
    }

    public static duplicatetransactionsDTOInter createduplicatetransactionsDTO(Object... obj) {
        return new duplicatetransactionsDTO();
    }

    public static ErrorsMonitorDTOInter createErrorsMonitorDTO(Object... obj) {
        return new ErrorsMonitorDTO();
    }

    public static RunningGraphDTOInter createRunningGraphDTO(Object... obj) {
        return new RunningGraphDTO();
    }

    public static FilesToMoveDTOInter createFilesToMoveDTO(Object... obj) {
        return new FilesToMoveDTO();
    }

    public static ValidationMonitorDTOInter createValidationMonitorDTO(Object... obj) {
        return new ValidationMonitorDTO();
    }

    public static LoadingMonitorDTOInter createLoadingMonitorDTO(Object... obj) {
        return new LoadingMonitorDTO();
    }

    public static MatchingMonitorDTOInter createMatchingMonitorDTO(Object... obj) {
        return new MatchingMonitorDTO();
    }

    public static licenseDTOInter createlicenseDTO(Object... obj) {
        return new licenseDTO();
    }

    public static copyfilesDTOInter createcopyfilesDTO(Object... obj) {
        return new copyfilesDTO();
    }

    public static currencyDTOInter createcurrencyDTO(Object... obj) {
        return new currencyDTO();
    }

    public static currencymasterDTOInter createcurrencymasterDTO(Object... obj) {
        return new currencymasterDTO();
    }

    public static blockreasonsDTOInter createblockreasonsDTO(Object... obj) {
        return new blockreasonsDTO();
    }

    public static blacklistedcardDTOInter createblacklistedcardDTO(Object... obj) {
        return new blacklistedcardDTO();
    }

    public static transactionresponsemasterDTOInter createtransactionresponsemasterDTO(Object... obj) {
        return new transactionresponsemasterDTO();
    }

    public static transactionresponsecodeDTOInter createtransactionresponsecodeDTO(Object... obj) {
        return new transactionresponsecodeDTO();
    }

    public static transactiontypemasterDTOInter createtransactiontypemasterDTO(Object... obj) {
        return new transactiontypemasterDTO();
    }

    public static transactiontypeDTOInter createtransactiontypeDTO(Object... obj) {
        return new transactiontypeDTO();
    }

    public static blockedusersDTOInter createblockedusersDTO(Object... obj) {
        return new blockedusersDTO();
    }

    public static userpassDTOInter createuserpassDTO(Object... obj) {
        return new userpassDTO();
    }

    public static itembingoDTOInter createitembingoDTO(Object... obj) {
        return new itembingoDTO();
    }

    public static profileDTOInter createprofileDTO(Object... obj) {
        return new profileDTO();
    }

    public static menulabelsDTOInter createmenulabelsDTO(Object... obj) {
        return new menulabelsDTO();
    }

    public static profilemenuDTOInter createprofilemenuDTO(Object... obj) {
        return new profilemenuDTO();
    }

    public static networkcodesDTOInter createnetworkcodesDTO(Object... obj) {
        return new networkcodesDTO();
    }

    public static bingologDTOInter createbingologDTO(Object... obj) {
        return new bingologDTO();
    }

    public static systemtableDTOInter createsystemtableDTO(Object... obj) {
        return new systemtableDTO();
    }

    public static userprofileDTOInter createuserprofileDTO(Object... obj) {
        return new userprofileDTO();
    }

    public static usersDTOInter createusersDTO(Object... obj) {
        return new usersDTO();
    }

}
