package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class userpassDTO extends BaseDTO implements userpassDTOInter, Serializable {

    private int userid;

    public int getuserid() {
        return userid;
    }

    public void setuserid(int userid) {
        this.userid = userid;
    }
    private String pass;

    public String getpass() {
        return pass;
    }

    public void setpass(String pass) {
        this.pass = pass;
    }
    private Date changedate;

    public Date getchangedate() {
        return changedate;
    }

    public void setchangedate(Date changedate) {
        this.changedate = changedate;
    }
}
