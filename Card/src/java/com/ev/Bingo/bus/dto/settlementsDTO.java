/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.math.BigDecimal;

/**
 *
 * @author shi7a
 */
public class settlementsDTO implements settlementsDTOInter {

    private String title;
    private String pan;
    private BigDecimal amount;
    private Integer count;

    public settlementsDTO() {
    }

    public settlementsDTO(String pan,String title, BigDecimal amount, Integer count) {
        this.title = title;
        this.amount = amount;
        this.count = count;
        this.pan = pan;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public Integer getCount() {
        return count;
    }

    @Override
    public void setCount(Integer count) {
        this.count = count;
    }

}
