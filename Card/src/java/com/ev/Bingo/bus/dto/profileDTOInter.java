package com.ev.Bingo.bus.dto;

import java.io.Serializable;

public interface profileDTOInter extends Serializable {

    int getprofileid();

    void setprofileid(int profileid);

    String getprofilename();

    void setprofilename(String profilename);

    int getrestrected();

    void setrestrected(int restrected);
}
