/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.math.BigDecimal;

/**
 *
 * @author shi7a
 */
public interface settlementsDTOInter {

    String getPan();

    void setPan(String pan);

    BigDecimal getAmount();

    Integer getCount();

    String getTitle();

    void setAmount(BigDecimal amount);

    void setCount(Integer count);

    void setTitle(String title);

}
