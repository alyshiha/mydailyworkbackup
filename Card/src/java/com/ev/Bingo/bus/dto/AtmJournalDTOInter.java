/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface AtmJournalDTOInter {

    int getAmount();

    String getAtmid();

    int getAtmjournalid();

    String getBranch();

    String getCreditaccount();

    int getCurrency();

    String getCurrencyname();

    int getCurrencyrate();

    Date getDatefrom();

    Date getDateto();

    String getDebitaccount();

    int getExportflag();

    String getIndication();

    Date getJournaldate();

    String getJournalrefernce();

    int getOperation();

    String getTransactiontype();

    void setAmount(int amount);

    void setAtmid(String atmid);

    void setAtmjournalid(int atmjournalid);

    void setBranch(String branch);

    void setCreditaccount(String creditaccount);

    void setCurrency(int currency);

    void setCurrencyname(String currencyname);

    void setCurrencyrate(int currencyrate);

    void setDatefrom(Date datefrom);

    void setDateto(Date dateto);

    void setDebitaccount(String debitaccount);

    void setExportflag(int exportflag);

    void setIndication(String indication);

    void setJournaldate(Date journaldate);

    void setJournalrefernce(String journalrefernce);

    void setOperation(int operation);

    void setTransactiontype(String transactiontype);

    String toCSVString(String Separator);
    
}
