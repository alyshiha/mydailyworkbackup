/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author Aly.Shiha
 */
public interface usersDTOInter extends Cloneable{

    Boolean getBoollockedstate();

    int getChangeotheruserpassword();

    int getDisputegraph();

    int getEmrholidayenabled();

    Boolean getErrorsB();

    int getHolidayenabled();

    int getLockedstate();

    String getLogonname();

    Boolean getNotificationB();

    Boolean getOnlineuserB();

    int getProfileid();

    String getProfilename();

    int getRestrected();

    int getSeerestrected();

    int getSeeunvisible();

    int getShiftid();

    Boolean getShowcorrectineentryB();

    int getShowcorrectiveentry();

    String getShowdashboard();

    int getShowerrors();

    int getShowexport();

    Boolean getShowexportB();

    int getShownewatms();

    int getShownotifications();

    int getShowonlineuser();

    int getShowpendingatms();

    int getStatus();

    String getUseraname();

    int getUserid();

    int getUserlang();

    String getUsername();

    String getUserpassword();

    int getVisible();

    void setBoollockedstate(Boolean boollockedstate);

    void setChangeotheruserpassword(int changeotheruserpassword);

    void setDisputegraph(int disputegraph);

    void setEmrholidayenabled(int emrholidayenabled);

    void setErrorsB(Boolean errorsB);

    void setHolidayenabled(int holidayenabled);

    void setLockedstate(int lockedstate);

    void setLogonname(String logonname);

    void setNotificationB(Boolean notificationB);

    void setOnlineuserB(Boolean onlineuserB);

    void setProfileid(int profileid);

    void setProfilename(String Profilename);

    void setRestrected(int restrected);

    void setSeerestrected(int seerestrected);

    void setSeeunvisible(int seeunvisible);

    void setShiftid(int shiftid);

    void setShowcorrectineentryB(Boolean showcorrectineentryB);

    void setShowcorrectiveentry(int showcorrectiveentry);

    void setShowdashboard(String showdashboard);

    void setShowerrors(int showerrors);

    void setShowexport(int showexport);

    void setShowexportB(Boolean showexportB);

    void setShownewatms(int shownewatms);

    void setShownotifications(int shownotifications);

    void setShowonlineuser(int showonlineuser);

    void setShowpendingatms(int showpendingatms);

    void setStatus(int status);

    void setUseraname(String useraname);

    void setUserid(int userid);

    void setUserlang(int userlang);

    void setUsername(String username);

    void setUserpassword(String userpassword);

    void setVisible(int visible);

}
