/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.math.BigDecimal;

/**
 *
 * @author Aly-Shiha
 */
public interface VisaOffusTotalsDTOInter {

    BigDecimal getIncome();

    String getTerminal();

    String getTerminalName();

    Integer getTransCount();

    String getTransMonth();

    String getType();

    void setIncome(BigDecimal Income);

    void setTerminal(String Terminal);

    void setTerminalName(String TerminalName);

    void setTransCount(Integer TransCount);

    void setTransMonth(String TransMonth);

    void setType(String Type);
    
}
