/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.math.BigDecimal;

/**
 *
 * @author shi7a
 */
public class VisaOffusDTO implements VisaOffusDTOInter {
       private Integer  count;
    private BigDecimal relatedincome;
    private String type;
private String terminal;

    public String getTerminal() {
        return terminal;
    }
    
    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }
    @Override
    public Integer getCount() {
        return count;
    }

    @Override
    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public BigDecimal getRelatedincome() {
        return relatedincome;
    }

    @Override
    public void setRelatedincome(BigDecimal relatedincome) {
        this.relatedincome = relatedincome;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

}
