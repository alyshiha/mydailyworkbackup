/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class ExceptionLoadingErrorsDTO implements ExceptionLoadingErrorsDTOInter {
    private String functionName; 
    private int errorCode; 
    private String errorName;
    private Date errorDate;
    

    public ExceptionLoadingErrorsDTO() {
    }

    public ExceptionLoadingErrorsDTO(String functionName, int errorCode, String errorName, Date errorDate) {
        this.functionName = functionName;
        this.errorCode = errorCode;
        this.errorName = errorName;
        this.errorDate = errorDate;
        
    }

    @Override
    public String getFunctionName() {
        return functionName;
    }

    @Override
    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    @Override
    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String getErrorName() {
        return errorName;
    }

    @Override
    public void setErrorName(String errorName) {
        this.errorName = errorName;
    }

    @Override
    public Date getErrorDate() {
        return errorDate;
    }

    @Override
    public void setErrorDate(Date errorDate) {
        this.errorDate = errorDate;
    }

   
    
}
