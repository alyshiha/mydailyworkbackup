/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;

/**
 *
 * @author Aly
 */
public class CommFeeReportDTO extends BaseDTO implements CommFeeReportDTOInter{
    private String network;
    private String pin;
    private int commtranscount;
    private Float comm;
    private int feetranscount;
    private Float fee;

    @Override
    public String getNetwork() {
        return network;
    }

    @Override
    public void setNetwork(String network) {
        this.network = network;
    }

    @Override
    public String getPin() {
        return pin;
    }

    @Override
    public void setPin(String pin) {
        this.pin = pin;
    }

    @Override
    public int getCommtranscount() {
        return commtranscount;
    }

    @Override
    public void setCommtranscount(int commtranscount) {
        this.commtranscount = commtranscount;
    }

    @Override
    public Float getComm() {
        return comm;
    }

    @Override
    public void setComm(Float comm) {
        this.comm = comm;
    }

    @Override
    public int getFeetranscount() {
        return feetranscount;
    }

    @Override
    public void setFeetranscount(int feetranscount) {
        this.feetranscount = feetranscount;
    }

    @Override
    public Float getFee() {
        return fee;
    }

    @Override
    public void setFee(Float fee) {
        this.fee = fee;
    }
    
}
