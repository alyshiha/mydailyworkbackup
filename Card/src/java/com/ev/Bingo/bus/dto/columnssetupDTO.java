package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;

public class columnssetupDTO extends BaseDTO implements columnssetupDTOInter, Serializable {

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private String name;

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }
}
