/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author shi7a
 */
public class RejectedVisaDTO implements RejectedVisaDTOInter {

    private Integer rejectid;
    private String reportid;

    public RejectedVisaDTO() {
    }

    public RejectedVisaDTO(Integer rejectid, String reportid) {
        this.rejectid = rejectid;
        this.reportid = reportid;
    }

    @Override
    public Integer getRejectid() {
        return rejectid;
    }

    @Override
    public void setRejectid(Integer rejectid) {
        this.rejectid = rejectid;
    }

    @Override
    public String getReportid() {
        return reportid;
    }

    @Override
    public void setReportid(String reportid) {
        this.reportid = reportid;
    }

}
