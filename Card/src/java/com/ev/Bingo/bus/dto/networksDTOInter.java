package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface networksDTOInter extends Serializable {

    int getid();

    void setid(int id);

    String getname();

    void setname(String name);
}
