package com.ev.Bingo.bus.dto;

import java.io.Serializable;

public interface profilemenuDTOInter extends Serializable {

    int getprofileid();

    void setprofileid(int profileid);

    int getmenuid();

    void setmenuid(int menuid);
}
