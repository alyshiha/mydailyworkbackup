/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public interface NetworkTypeCodeDTOInter {

    String getCode();

    Integer getColumnid();

    Integer getId();

    Integer getLength();

    Integer getNetworkid();

    Integer getStartposition();

    void setNetworkvalue(String networkvalue);

    String getNetworkvalue();

    void setColumnvalue(String columnvalue);

    String getColumnvalue();

    void setCode(String code);

    void setColumnid(Integer columnid);

    void setId(Integer id);

    void setLength(Integer length);

    void setNetworkid(Integer networkid);

    void setStartposition(Integer startposition);

}
