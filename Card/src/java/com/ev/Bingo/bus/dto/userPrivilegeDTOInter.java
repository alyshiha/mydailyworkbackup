/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public interface userPrivilegeDTOInter {

    int getMenuid();

    int getPrevid();

    int getUserid();

    int getUserprevid();

    void setMenuid(int menuid);

    void setPrevid(int previd);

    void setUserid(int userid);

    void setUserprevid(int userprevid);
    
}
