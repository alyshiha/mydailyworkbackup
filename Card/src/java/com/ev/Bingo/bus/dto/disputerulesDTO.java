package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class disputerulesDTO extends BaseDTO implements Serializable, disputerulesDTOInter {

    private int id;

    @Override
    public int getid() {
        return id;
    }

    @Override
    public void setid(int id) {
        this.id = id;
    }
    private int valdationtype;

    @Override
    public int getvaldationtype() {
        return valdationtype;
    }

    @Override
    public void setvaldationtype(int valdationtype) {
        this.valdationtype = valdationtype;
    }

    private String columnname;

    @Override
    public String getcolumnname() {
        return columnname;
    }

    @Override
    public void setcolumnname(String columnname) {
        this.columnname = columnname;
    }

    private int columnid;

    @Override
    public int getcolumnid() {
        return columnid;
    }

    @Override
    public void setcolumnid(int columnid) {
        this.columnid = columnid;
    }
    private String name;

    @Override
    public String getname() {
        return name;
    }

    @Override
    public void setname(String name) {
        this.name = name;
    }
    private String formula;

    @Override
    public String getformula() {
        return formula;
    }

    @Override
    public void setformula(String formula) {
        this.formula = formula;
    }
    private String formulacode;

    @Override
    public String getformulacode() {
        return formulacode;
    }

    @Override
    public void setformulacode(String formulacode) {
        this.formulacode = formulacode;
    }
    private int active;

}
