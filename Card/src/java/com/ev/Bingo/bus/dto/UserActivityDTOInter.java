/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author Administrator
 */
public interface UserActivityDTOInter {

    int getTranscount();

    String getType();

    String getUsername();

    void setTranscount(int transcount);

    void setType(String type);

    void setUsername(String username);

}
