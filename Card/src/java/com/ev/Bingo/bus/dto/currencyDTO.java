package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class currencyDTO extends BaseDTO implements currencyDTOInter, Serializable {

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private String abbreviation;

    public String getabbreviation() {
        return abbreviation;
    }

    public void setabbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
    private int masterid;

    public int getmasterid() {
        return masterid;
    }

    public void setmasterid(int masterid) {
        this.masterid = masterid;
    }
    private int defaultcurrency;

    public int getdefaultcurrency() {
        return defaultcurrency;
    }

    public void setdefaultcurrency(int defaultcurrency) {
        this.defaultcurrency = defaultcurrency;
    }
    private boolean defaultcurrencyB;

    public boolean isDefaultcurrencyB() {
        return defaultcurrencyB;
    }

    public void setDefaultcurrencyB(boolean defaultcurrencyB) {
        this.defaultcurrencyB = defaultcurrencyB;
    }
}
