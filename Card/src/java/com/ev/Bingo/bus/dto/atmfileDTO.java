package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class atmfileDTO extends BaseDTO implements atmfileDTOInter, Serializable {

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private String name;

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }
    private String backupname;

    public String getbackupname() {
        return backupname;
    }

    public void setbackupname(String backupname) {
        this.backupname = backupname;
    }
    private Date loadingdate;

    public Date getloadingdate() {
        return loadingdate;
    }

    public void setloadingdate(Date loadingdate) {
        this.loadingdate = loadingdate;
    }
    private int template;

    public int gettemplate() {
        return template;
    }

    public void settemplate(int template) {
        this.template = template;
    }
    private Date finishloadingdate;

    public Date getfinishloadingdate() {
        return finishloadingdate;
    }

    public void setfinishloadingdate(Date finishloadingdate) {
        this.finishloadingdate = finishloadingdate;
    }
    private int numberoftransactions;

    public int getnumberoftransactions() {
        return numberoftransactions;
    }

    public void setnumberoftransactions(int numberoftransactions) {
        this.numberoftransactions = numberoftransactions;
    }
    private int numberofduplication;

    public int getnumberofduplication() {
        return numberofduplication;
    }

    public void setnumberofduplication(int numberofduplication) {
        this.numberofduplication = numberofduplication;
    }
    private String type;

    public String gettype() {
        return type;
    }

    public void settype(String type) {
        this.type = type;
    }
}
