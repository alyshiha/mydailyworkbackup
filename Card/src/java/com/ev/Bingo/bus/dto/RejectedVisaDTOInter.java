/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author shi7a
 */
public interface RejectedVisaDTOInter {

    Integer getRejectid();

    String getReportid();

    void setRejectid(Integer rejectid);

    void setReportid(String reportid);
    
}
