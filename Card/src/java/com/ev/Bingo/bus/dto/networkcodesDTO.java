package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;

public class networkcodesDTO extends BaseDTO implements networkcodesDTOInter, Serializable {

    private String rowid;

    public String getrowid() {
        return rowid;
    }

    public void setrowid(String rowid) {
        this.rowid = rowid;
    }

    private int networkid;

    public int getnetworkid() {
        return networkid;
    }

    public void setnetworkid(int networkid) {
        this.networkid = networkid;
    }
    private String networkcode;

    public String getnetworkcode() {
        return networkcode;
    }

    public void setnetworkcode(String networkcode) {
        this.networkcode = networkcode;
    }
}
