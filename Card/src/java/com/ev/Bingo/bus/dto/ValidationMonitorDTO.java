/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;

/**
 *
 * @author Administrator
 */
public class ValidationMonitorDTO extends BaseDTO implements ValidationMonitorDTOInter {

    private int pendingJournals;
    private int pendingSwitch;
    private int pendingHost;

    public int getPendingHost() {
        return pendingHost;
    }

    public void setPendingHost(int pendingHost) {
        this.pendingHost = pendingHost;
    }

    public int getPendingJournals() {
        return pendingJournals;
    }

    public void setPendingJournals(int pendingJournals) {
        this.pendingJournals = pendingJournals;
    }

    public int getPendingSwitch() {
        return pendingSwitch;
    }

    public void setPendingSwitch(int pendingSwitch) {
        this.pendingSwitch = pendingSwitch;
    }

    protected ValidationMonitorDTO() {
        super();
    }
}
