package com.ev.Bingo.bus.dto;

import java.io.Serializable;

public interface currencymasterDTOInter extends Serializable {

    int getid();

    void setid(int id);

    String getname();

    void setname(String name);

    String getsymbol();

    void setsymbol(String symbol);

    int getdefaultcurrency();

    void setdefaultcurrency(int defaultcurrency);

    void setdefaultcurrencyB(boolean defaultcurrencyB);

    boolean getdefaultcurrencyB();
}
