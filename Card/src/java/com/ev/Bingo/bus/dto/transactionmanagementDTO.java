package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;

public class transactionmanagementDTO extends BaseDTO implements transactionmanagementDTOInter, Serializable {

    private int tranno;

    public int gettranno() {
        return tranno;
    }

    public void settranno(int tranno) {
        this.tranno = tranno;
    }
    private String key;

    public String getkey() {
        return key;
    }

    public void setkey(String key) {
        this.key = key;
    }
    private int tranmonth;

    public int gettranmonth() {
        return tranmonth;
    }

    public void settranmonth(int tranmonth) {
        this.tranmonth = tranmonth;
    }
    private int tranyear;

    public int gettranyear() {
        return tranyear;
    }

    public void settranyear(int tranyear) {
        this.tranyear = tranyear;
    }
}
