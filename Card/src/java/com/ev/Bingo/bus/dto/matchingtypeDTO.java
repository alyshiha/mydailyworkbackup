package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class matchingtypeDTO extends BaseDTO implements matchingtypeDTOInter, Serializable {

    private String typename2;

    public String gettypename2() {
        return typename2;
    }

    public void settypename2(String typename2) {
        this.typename2 = typename2;
    }
    private String typename1;

    public String gettypename1() {
        return typename1;
    }

    public void settypename1(String typename1) {
        this.typename1 = typename1;
    }

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private int type1;

    public int gettype1() {
        return type1;
    }

    public void settype1(int type1) {
        this.type1 = type1;
    }
    private int type2;

    public int gettype2() {
        return type2;
    }

    public void settype2(int type2) {
        this.type2 = type2;
    }
    private int active;

    public int getactive() {
        return active;
    }

    public void setactive(int active) {
        this.active = active;
    }
    private String name;

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }
    private Boolean activeB;

    public Boolean getActiveB() {
        return activeB;
    }

    public void setActiveB(Boolean activeB) {
        this.activeB = activeB;
    }

}
