package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;

public class settlementlogDTO extends BaseDTO implements settlementlogDTOInter, Serializable {

    private String atmapplicationid;

    public String getatmapplicationid() {
        return atmapplicationid;
    }

    public void setatmapplicationid(String atmapplicationid) {
        this.atmapplicationid = atmapplicationid;
    }
    private String currency;

    public String getcurrency() {
        return currency;
    }

    public void setcurrency(String currency) {
        this.currency = currency;
    }
    private Date transactiondate;

    public Date gettransactiondate() {
        return transactiondate;
    }

    public void settransactiondate(Date transactiondate) {
        this.transactiondate = transactiondate;
    }
    private String transactionsequence;

    public String gettransactionsequence() {
        return transactionsequence;
    }

    public void settransactionsequence(String transactionsequence) {
        this.transactionsequence = transactionsequence;
    }
    private String cardno;

    public String getcardno() {
        return cardno;
    }

    public void setcardno(String cardno) {
        this.cardno = cardno;
    }
    private Float amount;

    public Float getamount() {
        return amount;
    }

    public void setamount(Float amount) {
        this.amount = amount;
    }
    private Date settlementdate;

    public Date getsettlementdate() {
        return settlementdate;
    }

    public void setsettlementdate(Date settlementdate) {
        this.settlementdate = settlementdate;
    }
    private String responsecode;

    public String getresponsecode() {
        return responsecode;
    }

    public void setresponsecode(String responsecode) {
        this.responsecode = responsecode;
    }
    private String matchingtype;

    public String getmatchingtype() {
        return matchingtype;
    }

    public void setmatchingtype(String matchingtype) {
        this.matchingtype = matchingtype;
    }
    private String recordtype;

    public String getrecordtype() {
        return recordtype;
    }

    public void setrecordtype(String recordtype) {
        this.recordtype = recordtype;
    }
    private String settleduser;

    public String getsettleduser() {
        return settleduser;
    }

    public void setsettleduser(String settleduser) {
        this.settleduser = settleduser;
    }
    private String settledflag;

    public String getsettledflag() {
        return settledflag;
    }

    public void setsettledflag(String settledflag) {
        this.settledflag = settledflag;
    }
}
