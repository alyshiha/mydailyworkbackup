package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class bingologDTO extends BaseDTO implements bingologDTOInter, Serializable {

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private Date actiondate;

    public Date getactiondate() {
        return actiondate;
    }

    public void setactiondate(Date actiondate) {
        this.actiondate = actiondate;
    }
    private int userid;

    public int getuserid() {
        return userid;
    }

    public void setuserid(int userid) {
        this.userid = userid;
    }
    private String ipaddress;

    public String getipaddress() {
        return ipaddress;
    }

    public void setipaddress(String ipaddress) {
        this.ipaddress = ipaddress;
    }
    private String action;

    public String getaction() {
        return action;
    }

    public void setaction(String action) {
        this.action = action;
    }
    private String username;

    public String getusername() {
        return username;
    }

    public void setusername(String username) {
        this.username = username;
    }
    private String profilename;

    public String getprofilename() {
        return profilename;
    }

    public void setprofilename(String profilename) {
        this.profilename = profilename;
    }

}
