package com.ev.Bingo.bus.dto;

import java.io.Serializable;

public interface blockreasonsDTOInter extends Serializable {

    int getreasonid();

    void setreasonid(int reasonid);

    String getreasonname();

    void setreasonname(String reasonname);

    String getreasonaname();

    void setreasonaname(String reasonaname);
}
