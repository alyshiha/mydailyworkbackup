/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public class DAshBoardDTO implements DAshBoardDTOInter {

    private int matched;
    private int disputes;
    private String network;

    @Override
    public int getMatched() {
        return matched;
    }

    @Override
    public void setMatched(int matched) {
        this.matched = matched;
    }

    @Override
    public int getDisputes() {
        return disputes;
    }

    @Override
    public void setDisputes(int disputes) {
        this.disputes = disputes;
    }

    @Override
    public String getNetwork() {
        return network;
    }

    @Override
    public void setNetwork(String network) {
        this.network = network;
    }

}
