package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface bingologDTOInter extends Serializable {

    int getid();

    void setid(int id);

    Date getactiondate();

    void setactiondate(Date actiondate);

    int getuserid();

    void setuserid(int userid);

    String getipaddress();

    void setipaddress(String ipaddress);

    String getaction();

    void setaction(String action);

    void setprofilename(String profilename);

    String getprofilename();

    void setusername(String username);

    String getusername();
}
