package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface settlementlogDTOInter extends Serializable {

    String getatmapplicationid();

    void setatmapplicationid(String atmapplicationid);

    String getcurrency();

    void setcurrency(String currency);

    Date gettransactiondate();

    void settransactiondate(Date transactiondate);

    String gettransactionsequence();

    void settransactionsequence(String transactionsequence);

    String getcardno();

    void setcardno(String cardno);

    Float getamount();

    void setamount(Float amount);

    Date getsettlementdate();

    void setsettlementdate(Date settlementdate);

    String getresponsecode();

    void setresponsecode(String responsecode);

    String getmatchingtype();

    void setmatchingtype(String matchingtype);

    String getrecordtype();

    void setrecordtype(String recordtype);

    String getsettleduser();

    void setsettleduser(String settleduser);

    String getsettledflag();

    void setsettledflag(String settledflag);
}
