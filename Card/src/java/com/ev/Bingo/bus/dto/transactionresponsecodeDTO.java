package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class transactionresponsecodeDTO extends BaseDTO implements transactionresponsecodeDTOInter, Serializable {

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private String name;

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }
    private String description;

    public String getdescription() {
        return description;
    }

    public void setdescription(String description) {
        this.description = description;
    }
    private int amounttype;

    public int getamounttype() {
        return amounttype;
    }

    public void setamounttype(int amounttype) {
        this.amounttype = amounttype;
    }
    private int defaultflag;

    public int getdefaultflag() {
        return defaultflag;
    }

    public void setdefaultflag(int defaultflag) {
        this.defaultflag = defaultflag;
    }
    private boolean defaultflagB;

    public boolean getdefaultflagB() {
        return defaultflagB;
    }

    public void setdefaultflagB(boolean defaultflagB) {
        this.defaultflagB = defaultflagB;
    }
    private int mastercode;

    public int getmastercode() {
        return mastercode;
    }

    public void setmastercode(int mastercode) {
        this.mastercode = mastercode;
    }
}
