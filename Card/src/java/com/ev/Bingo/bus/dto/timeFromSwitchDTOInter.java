/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.math.BigDecimal;

/**
 *
 * @author shi7a
 */
public interface timeFromSwitchDTOInter {

    String getAcq();

    BigDecimal getAmount();

    Integer getCount();

    String getCurrency();

    String getIss();

    void setAcq(String acq);

    void setAmount(BigDecimal amount);

    void setCount(Integer count);

    void setCurrency(String currency);

    void setIss(String iss);
    
}
