/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public interface validationrulesDTOInter {

    void setcolumnname(String columnname);

    String getcolumnname();

    void setActiveB(Boolean activeB);

    Boolean getActiveB();

    int getactive();

    int getcolumnid();

    String getformula();

    String getformulacode();

    int getid();

    String getname();

    int getvaldationtype();

    void setactive(int active);

    void setcolumnid(int columnid);

    void setformula(String formula);

    void setformulacode(String formulacode);

    void setid(int id);

    void setname(String name);

    void setvaldationtype(int valdationtype);

}
