package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface matchingtypenetworksDTOInter extends Serializable {

    int getmatchingtype();

    void setmatchingtype(int matchingtype);

    int getnetworkid();

    void setnetworkid(int networkid);

    void setRowid(String rowid);

    String getRowid();
}
