package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface columnssetupdetailsDTOInter extends Serializable {

    int getdisplaycolumnid();

    void setdisplaycolumnid(int displaycolumnid);

    int getcolumnid();

    void setcolumnid(int columnid);

    void setrowid(String rowid);

    String getrowid();
}
