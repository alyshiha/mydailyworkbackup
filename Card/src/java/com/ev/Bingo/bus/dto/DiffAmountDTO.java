/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class DiffAmountDTO implements DiffAmountDTOInter {

    Date transactiondate;
    String cardno,acc, network;
    float amount1, amount2, switchD, networkD;
    int rownum;

    public String getAcc() {
        return acc;
    }

    public void setAcc(String acc) {
        this.acc = acc;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    @Override
    public Date getTransactiondate() {
        return transactiondate;
    }

    @Override
    public void setTransactiondate(Date transactiondate) {
        this.transactiondate = transactiondate;
    }

    @Override
    public String getCardno() {
        return cardno;
    }

    @Override
    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public float getAmount1() {
        return amount1;
    }

    public void setAmount1(float amount1) {
        this.amount1 = amount1;
    }

    public float getAmount2() {
        return amount2;
    }

    public void setAmount2(float amount2) {
        this.amount2 = amount2;
    }

    public float getSwitchD() {
        return switchD;
    }

    public void setSwitchD(float switchD) {
        this.switchD = switchD;
    }

    public float getNetworkD() {
        return networkD;
    }

    public void setNetworkD(float networkD) {
        this.networkD = networkD;
    }

    @Override
    public int getRownum() {
        return rownum;
    }

    @Override
    public void setRownum(int rownum) {
        this.rownum = rownum;
    }

}
