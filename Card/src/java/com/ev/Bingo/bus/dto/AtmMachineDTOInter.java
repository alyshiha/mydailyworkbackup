/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.Bingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface AtmMachineDTOInter extends Serializable{
void setGroupId(int GroupId);
int getGroupId() ;
    String getApplicationId();

    String getApplicationId2();

    

    int getId();

    String getLocation();

    int getMachineType();

    String getName();

    String getSerialNo();

    String getUnitNumber();

    void setApplicationId(String applicationId);

    void setApplicationId2(String applicationId2);

    
    void setId(int id);

    void setLocation(String location);

    void setMachineType(int machineType);

    void setName(String name);

    void setSerialNo(String SerialNo);

    void setUnitNumber(String unitNumber);

}
