/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public interface DAshBoardDTOInter {

    int getDisputes();

    int getMatched();

    String getNetwork();

    void setDisputes(int disputes);

    void setMatched(int matched);

    void setNetwork(String network);

}
