package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface matchingtypeDTOInter extends Serializable {

    int getid();

    String gettypename2();

    void settypename2(String typename2);

    String gettypename1();

    void settypename1(String typename1);

    void setid(int id);

    int gettype1();

    void settype1(int type1);

    int gettype2();

    void settype2(int type2);

    int getactive();

    void setactive(int active);

    String getname();

    void setname(String name);

    void setActiveB(Boolean activeB);

    Boolean getActiveB();
}
