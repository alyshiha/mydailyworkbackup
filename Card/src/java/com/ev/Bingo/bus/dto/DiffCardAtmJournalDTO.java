/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class DiffCardAtmJournalDTO implements DiffCardAtmJournalDTOInter {

    private Integer flag;
    private Integer diffid;
    private Integer cardamount;
    private Integer atmamount;
    private Integer diffamount;
    private String journaltype;
    private String atmid;
    private Date journaldate;
    private Date atmfromdate;
    private Date diffdate;
    private Date atmtodate;
    private String creditaccount;
    private String debitaccount;
    private String indication;
    private String branch;
    private Integer exportflag;
    private Integer operation;

    public DiffCardAtmJournalDTO() {
    }

    public DiffCardAtmJournalDTO(Integer diffid, Integer cardamount, Integer atmamount, String journaltype, String atmid, Date journaldate, Date atmfromdate, Date atmtodate, String creditaccount, String debitaccount, String indication, String branch, Integer exportflag, Integer operation) {
        this.diffid = diffid;
        this.cardamount = cardamount;
        this.atmamount = atmamount;
        this.journaltype = journaltype;
        this.atmid = atmid;
        this.journaldate = journaldate;
        this.atmfromdate = atmfromdate;
        this.atmtodate = atmtodate;
        this.creditaccount = creditaccount;
        this.debitaccount = debitaccount;
        this.indication = indication;
        this.branch = branch;
        this.exportflag = exportflag;
        this.operation = operation;
    }

    public Integer getDiffamount() {
        return diffamount;
    }

    public void setDiffamount(Integer diffamount) {
        this.diffamount = diffamount;
    }

    @Override
    public Integer getFlag() {
        return flag;
    }

    @Override
    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    @Override
    public Integer getDiffid() {
        return diffid;
    }

    @Override
    public void setDiffid(Integer diffid) {
        this.diffid = diffid;
    }

    @Override
    public Integer getCardamount() {
        return cardamount;
    }

    @Override
    public void setCardamount(Integer cardamount) {
        this.cardamount = cardamount;
    }

    public Date getDiffdate() {
        return diffdate;
    }

    public void setDiffdate(Date diffdate) {
        this.diffdate = diffdate;
    }

    @Override
    public Integer getAtmamount() {
        return atmamount;
    }

    @Override
    public void setAtmamount(Integer atmamount) {
        this.atmamount = atmamount;
    }

    @Override
    public String getJournaltype() {
        return journaltype;
    }

    @Override
    public void setJournaltype(String journaltype) {
        this.journaltype = journaltype;
    }

    @Override
    public String getAtmid() {
        return atmid;
    }

    @Override
    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    @Override
    public Date getJournaldate() {
        return journaldate;
    }

    @Override
    public void setJournaldate(Date journaldate) {
        this.journaldate = journaldate;
    }

    @Override
    public Date getAtmfromdate() {
        return atmfromdate;
    }

    @Override
    public void setAtmfromdate(Date atmfromdate) {
        this.atmfromdate = atmfromdate;
    }

    @Override
    public Date getAtmtodate() {
        return atmtodate;
    }

    @Override
    public void setAtmtodate(Date atmtodate) {
        this.atmtodate = atmtodate;
    }

    @Override
    public String getCreditaccount() {
        return creditaccount;
    }

    @Override
    public void setCreditaccount(String creditaccount) {
        this.creditaccount = creditaccount;
    }

    @Override
    public String getDebitaccount() {
        return debitaccount;
    }

    @Override
    public void setDebitaccount(String debitaccount) {
        this.debitaccount = debitaccount;
    }

    @Override
    public String getIndication() {
        return indication;
    }

    @Override
    public void setIndication(String indication) {
        this.indication = indication;
    }

    @Override
    public String getBranch() {
        return branch;
    }

    @Override
    public void setBranch(String branch) {
        this.branch = branch;
    }

    @Override
    public Integer getExportflag() {
        return exportflag;
    }

    @Override
    public void setExportflag(Integer exportflag) {
        this.exportflag = exportflag;
    }

    @Override
    public Integer getOperation() {
        return operation;
    }

    @Override
    public void setOperation(Integer operation) {
        this.operation = operation;
    }

}
