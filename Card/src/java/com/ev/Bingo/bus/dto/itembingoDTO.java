package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class itembingoDTO extends BaseDTO implements itembingoDTOInter, Serializable {

    private int itemid;

    public int getitemid() {
        return itemid;
    }

    public void setitemid(int itemid) {
        this.itemid = itemid;
    }
    private String itemname;

    public String getitemname() {
        return itemname;
    }

    public void setitemname(String itemname) {
        this.itemname = itemname;
    }
}
