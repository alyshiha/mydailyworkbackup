package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class matchingtypenetworksDTO extends BaseDTO implements matchingtypenetworksDTOInter, Serializable {

    private int matchingtype;

    public int getmatchingtype() {
        return matchingtype;
    }

    public void setmatchingtype(int matchingtype) {
        this.matchingtype = matchingtype;
    }
    private int networkid;

    public int getnetworkid() {
        return networkid;
    }

    public void setnetworkid(int networkid) {
        this.networkid = networkid;
    }
    private String rowid;

    public String getRowid() {
        return rowid;
    }

    public void setRowid(String rowid) {
        this.rowid = rowid;
    }

}
