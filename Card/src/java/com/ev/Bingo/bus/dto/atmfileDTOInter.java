package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface atmfileDTOInter extends Serializable {

    int getid();

    void setid(int id);

    String getname();

    void setname(String name);

    String getbackupname();

    void setbackupname(String backupname);

    Date getloadingdate();

    void setloadingdate(Date loadingdate);

    int gettemplate();

    void settemplate(int template);

    Date getfinishloadingdate();

    void setfinishloadingdate(Date finishloadingdate);

    int getnumberoftransactions();

    void setnumberoftransactions(int numberoftransactions);

    int getnumberofduplication();

    void setnumberofduplication(int numberofduplication);

    String gettype();

    void settype(String type);
}
