/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.math.BigDecimal;

/**
 *
 * @author Aly-Shiha
 */
public interface OffusTotalsDTOInter {

    Integer getCount();

    BigDecimal getRelatedIncome();

    String getTerminal();

    String getTerminalName();

    String getTransMonth();

    String getType();

    void setCount(Integer Count);

    void setRelatedIncome(BigDecimal RelatedIncome);

    void setTerminal(String Terminal);

    void setTerminalName(String TerminalName);

    void setTransMonth(String TransMonth);

    void setType(String Type);
    
}
