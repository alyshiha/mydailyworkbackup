package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class currencymasterDTO extends BaseDTO implements currencymasterDTOInter, Serializable {

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private String name;

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }
    private String symbol;

    public String getsymbol() {
        return symbol;
    }

    public void setsymbol(String symbol) {
        this.symbol = symbol;
    }
    private int defaultcurrency;

    public int getdefaultcurrency() {
        return defaultcurrency;
    }

    public void setdefaultcurrency(int defaultcurrency) {
        this.defaultcurrency = defaultcurrency;
    }

    private boolean defaultcurrencyB;

    public boolean getdefaultcurrencyB() {
        return defaultcurrencyB;
    }

    public void setdefaultcurrencyB(boolean defaultcurrencyB) {
        this.defaultcurrencyB = defaultcurrencyB;
    }

}
