/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author Administrator
 */
public class TranSearchFieldDTO implements TranSearchFieldDTOInter  {

    private String terminal;

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }
    private String currencyoperator;

    public String getCurrencyoperator() {
        return currencyoperator;
    }

    public void setCurrencyoperator(String currencyoperator) {
        this.currencyoperator = currencyoperator;
    }

  
    
    @Override
    public Integer getBank() {
        return bank;
    }

    @Override
    public void setBank(Integer bank) {
        this.bank = bank;
    }

    @Override
    public String getCardnumsuffix() {
        return cardnumsuffix;
    }

    @Override
    public void setCardnumsuffix(String cardnumsuffix) {
        this.cardnumsuffix = cardnumsuffix;
    }

    @Override
    public String getAmountfrom() {
        return amountfrom;
    }

    @Override
    public void setAmountfrom(String amountfrom) {
        this.amountfrom = amountfrom;
    }

    @Override
    public String getAmountto() {
        return amountto;
    }

    @Override
    public void setAmountto(String amountto) {
        this.amountto = amountto;
    }

    @Override
    public String getSettamountfrom() {
        return settamountfrom;
    }

    @Override
    public void setSettamountfrom(String settamountfrom) {
        this.settamountfrom = settamountfrom;
    }

    @Override
    public String getAqramountfrom() {
        return aqramountfrom;
    }

    @Override
    public void setAqramountfrom(String aqramountfrom) {
        this.aqramountfrom = aqramountfrom;
    }

    @Override
    public String getAqramountto() {
        return aqramountto;
    }

    @Override
    public void setAqramountto(String aqramountto) {
        this.aqramountto = aqramountto;
    }

    @Override
    public String getSettamountto() {
        return settamountto;
    }

    @Override
    public void setSettamountto(String settamountto) {
        this.settamountto = settamountto;
    }
    
    public String amountfrom;
    public String amountto;
    public String settamountfrom, aqramountfrom, aqramountto, settamountto;
    public String filename, cardnumsuffix;
    public String matchingtypeid, sortby, sortorder, sortcol;
    public String masterrecordid;
    public Integer atmgroupid, fileid,bank;
    public Integer atmnamedropdown;
    public Integer reversedby;
    public Integer atmnameauto;
    public String atmidauto;
    public Date transdatefrom;
    public Date transdateto;
    public Date setteltdatefrom;
    public Date setteltdateto;
    public Date loadingdatefrom;
    public Date loadingdateto;

    public String seqfrom, seqto;
    public Integer errorid;
    public String errorcodename;
    public Integer typecodeid;
    public Integer currencyid, settcurrencyid, aqccurrencyid;
    public String typecodename;
    public Integer responceid;
    public String responcecodename;
    public String sequencefrom;
    public String sequenceto;
    public Integer disputeid;
    public Integer setteltbyid;
    public Integer viewid;
    public Integer reverseid;
    public Integer setteltstatus;
    public String cardno, authno, refno;
    public String accountno;
    public Boolean blacklistbool;
    public String atmidoperator;
    public String transdateoperator;
    public String setteltdateoperator;
    public String loadingdateoperator;
    public String amountoperator, settamountoperator, aqramountoperator, seqoperator;
    public String sequenceoperator;
    public String responceidoperator;
    public String responcecodenameoperator;
    public String cardnumoperator;
    public String accountnumoperator;
    public String recordtypeid, col1, sorttype;
    public Integer disputeuserid, network,col4;
    public Integer validationruleid;

    @Override
    public Integer getCol4() {
        return col4;
    }

    @Override
    public void setCol4(Integer col4) {
        this.col4 = col4;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public Integer getFileid() {
        return fileid;
    }

    @Override
    public Integer getReversedby() {
        return reversedby;
    }

    @Override
    public void setReversedby(Integer reversedby) {
        this.reversedby = reversedby;
    }

    @Override
    public void setFileid(Integer fileid) {
        this.fileid = fileid;
    }

    @Override
    public String getSorttype() {
        return sorttype;
    }

    @Override
    public void setSorttype(String sorttype) {
        this.sorttype = sorttype;
    }

    @Override
    public String getCol1() {
        return col1;
    }

    @Override
    public void setCol1(String col1) {
        this.col1 = col1;
    }

    @Override
    public Integer getNetwork() {
        return network;
    }

    @Override
    public void setNetwork(Integer network) {
        this.network = network;
    }

    @Override
    public String getAuthno() {
        return authno;
    }

    @Override
    public void setAuthno(String authno) {
        this.authno = authno;
    }

    @Override
    public String getRefno() {
        return refno;
    }

    @Override
    public void setRefno(String refno) {
        this.refno = refno;
    }

    @Override
    public String getSeqfrom() {
        return seqfrom;
    }

    @Override
    public void setSeqfrom(String seqfrom) {
        this.seqfrom = seqfrom;
    }

    @Override
    public String getSeqto() {
        return seqto;
    }

    @Override
    public void setSeqto(String seqto) {
        this.seqto = seqto;
    }

    @Override
    public String getSeqoperator() {
        return seqoperator;
    }

    @Override
    public void setSeqoperator(String seqoperator) {
        this.seqoperator = seqoperator;
    }

    @Override
    public Integer getAqccurrencyid() {
        return aqccurrencyid;
    }

    @Override
    public void setAqccurrencyid(Integer aqccurrencyid) {
        this.aqccurrencyid = aqccurrencyid;
    }

    @Override
    public Integer getSettcurrencyid() {
        return settcurrencyid;
    }

    @Override
    public void setSettcurrencyid(Integer settcurrencyid) {
        this.settcurrencyid = settcurrencyid;
    }


    @Override
    public String getAqramountoperator() {
        return aqramountoperator;
    }

    @Override
    public void setAqramountoperator(String aqramountoperator) {
        this.aqramountoperator = aqramountoperator;
    }


    @Override
    public String getSortcol() {
        return sortcol;
    }

    @Override
    public void setSortcol(String sortcol) {
        this.sortcol = sortcol;
    }

    @Override
    public Date getLoadingdatefrom() {
        return loadingdatefrom;
    }

    @Override
    public String getSettamountoperator() {
        return settamountoperator;
    }

    @Override
    public void setSettamountoperator(String settamountoperator) {
        this.settamountoperator = settamountoperator;
    }

    @Override
    public void setLoadingdatefrom(Date loadingdatefrom) {
        this.loadingdatefrom = loadingdatefrom;
    }

    @Override
    public Date getLoadingdateto() {
        return loadingdateto;
    }

    @Override
    public void setLoadingdateto(Date loadingdateto) {
        this.loadingdateto = loadingdateto;
    }

    @Override
    public String getLoadingdateoperator() {
        return loadingdateoperator;
    }

    @Override
    public void setLoadingdateoperator(String loadingdateoperator) {
        this.loadingdateoperator = loadingdateoperator;
    }

    @Override
    public String getSortby() {
        return sortby;
    }

    @Override
    public void setSortby(String sortby) {
        this.sortby = sortby;
    }

    @Override
    public String getSortorder() {
        return sortorder;
    }

    @Override
    public void setSortorder(String sortorder) {
        this.sortorder = sortorder;
    }

    @Override
    public void setDisputeuserid(Integer disputeuserid) {
        this.disputeuserid = disputeuserid;
    }

    @Override
    public Integer getDisputeuserid() {
        return disputeuserid;
    }

    @Override
    public Integer getCurrencyid() {
        return currencyid;
    }

    @Override
    public void setCurrencyid(Integer currencyid) {
        this.currencyid = currencyid;
    }

    @Override
    public String getAccountno() {
        return accountno;
    }

    @Override
    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    @Override
    public String getAccountnumoperator() {
        return accountnumoperator;
    }

    @Override
    public void setAccountnumoperator(String accountnumoperator) {
        this.accountnumoperator = accountnumoperator;
    }



    @Override
    public String getAmountoperator() {
        return amountoperator;
    }

    @Override
    public void setAmountoperator(String amountoperator) {
        this.amountoperator = amountoperator;
    }

 

    @Override
    public Integer getAtmgroupid() {
        return atmgroupid;
    }

    @Override
    public void setAtmgroupid(Integer atmgroupid) {
        this.atmgroupid = atmgroupid;
    }

    @Override
    public String getAtmidauto() {
        return atmidauto;
    }

    @Override
    public void setAtmidauto(String atmidauto) {
        this.atmidauto = atmidauto;
    }

    @Override
    public String getAtmidoperator() {
        return atmidoperator;
    }

    @Override
    public void setAtmidoperator(String atmidoperator) {
        this.atmidoperator = atmidoperator;
    }

    @Override
    public Integer getAtmnameauto() {
        return atmnameauto;
    }

    @Override
    public void setAtmnameauto(Integer atmnameauto) {
        this.atmnameauto = atmnameauto;
    }

    @Override
    public Integer getAtmnamedropdown() {
        return atmnamedropdown;
    }

    @Override
    public void setAtmnamedropdown(Integer atmnamedropdown) {
        this.atmnamedropdown = atmnamedropdown;
    }

    @Override
    public Boolean getBlacklistbool() {
        return blacklistbool;
    }

    @Override
    public void setBlacklistbool(Boolean blacklistbool) {
        this.blacklistbool = blacklistbool;
    }

    @Override
    public String getCardno() {
        return cardno;
    }

    @Override
    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    @Override
    public String getCardnumoperator() {
        return cardnumoperator;
    }

    @Override
    public void setCardnumoperator(String cardnumoperator) {
        this.cardnumoperator = cardnumoperator;
    }

    @Override
    public Integer getDisputeid() {
        return disputeid;
    }

    @Override
    public void setDisputeid(Integer disputeid) {
        this.disputeid = disputeid;
    }

    @Override
    public String getErrorcodename() {
        return errorcodename;
    }

    @Override
    public void setErrorcodename(String errorcodename) {
        this.errorcodename = errorcodename;
    }

    @Override
    public Integer getErrorid() {
        return errorid;
    }

    @Override
    public void setErrorid(Integer errorid) {
        this.errorid = errorid;
    }

    @Override
    public String getMasterrecordid() {
        return masterrecordid;
    }

    @Override
    public void setMasterrecordid(String masterrecordid) {
        this.masterrecordid = masterrecordid;
    }

    @Override
    public String getMatchingtypeid() {
        return matchingtypeid;
    }

    @Override
    public void setMatchingtypeid(String matchingtypeid) {
        this.matchingtypeid = matchingtypeid;
    }

    @Override
    public String getRecordtypeid() {
        return recordtypeid;
    }

    @Override
    public void setRecordtypeid(String recordtypeid) {
        this.recordtypeid = recordtypeid;
    }

    @Override
    public String getResponcecodename() {
        return responcecodename;
    }

    @Override
    public void setResponcecodename(String responcecodename) {
        this.responcecodename = responcecodename;
    }

    @Override
    public String getResponcecodenameoperator() {
        return responcecodenameoperator;
    }

    @Override
    public void setResponcecodenameoperator(String responcecodenameoperator) {
        this.responcecodenameoperator = responcecodenameoperator;
    }

    @Override
    public Integer getResponceid() {
        return responceid;
    }

    @Override
    public void setResponceid(Integer responceid) {
        this.responceid = responceid;
    }

    @Override
    public String getResponceidoperator() {
        return responceidoperator;
    }

    @Override
    public void setResponceidoperator(String responceidoperator) {
        this.responceidoperator = responceidoperator;
    }

    @Override
    public Integer getReverseid() {
        return reverseid;
    }

    @Override
    public void setReverseid(Integer reverseid) {
        this.reverseid = reverseid;
    }

    @Override
    public String getSequencefrom() {
        return sequencefrom;
    }

    @Override
    public void setSequencefrom(String sequencefrom) {
        this.sequencefrom = sequencefrom;
    }

    @Override
    public String getSequenceoperator() {
        return sequenceoperator;
    }

    @Override
    public void setSequenceoperator(String sequenceoperator) {
        this.sequenceoperator = sequenceoperator;
    }

    @Override
    public String getSequenceto() {
        return sequenceto;
    }

    @Override
    public void setSequenceto(String sequenceto) {
        this.sequenceto = sequenceto;
    }

    @Override
    public Integer getSetteltbyid() {
        return setteltbyid;
    }

    @Override
    public void setSetteltbyid(Integer setteltbyid) {
        this.setteltbyid = setteltbyid;
    }

    @Override
    public Date getSetteltdatefrom() {
        return setteltdatefrom;
    }

    @Override
    public void setSetteltdatefrom(Date setteltdatefrom) {
        this.setteltdatefrom = setteltdatefrom;
    }

    @Override
    public String getSetteltdateoperator() {
        return setteltdateoperator;
    }

    @Override
    public void setSetteltdateoperator(String setteltdateoperator) {
        this.setteltdateoperator = setteltdateoperator;
    }

    @Override
    public Date getSetteltdateto() {
        return setteltdateto;
    }

    @Override
    public void setSetteltdateto(Date setteltdateto) {
        this.setteltdateto = setteltdateto;
    }

    @Override
    public Integer getSetteltstatus() {
        return setteltstatus;
    }

    @Override
    public void setSetteltstatus(Integer setteltstatus) {
        this.setteltstatus = setteltstatus;
    }

    @Override
    public Date getTransdatefrom() {
        return transdatefrom;
    }

    @Override
    public void setTransdatefrom(Date transdatefrom) {
        this.transdatefrom = transdatefrom;
    }

    @Override
    public String getTransdateoperator() {
        return transdateoperator;
    }

    @Override
    public void setTransdateoperator(String transdateoperator) {
        this.transdateoperator = transdateoperator;
    }

    @Override
    public Date getTransdateto() {
        return transdateto;
    }

    @Override
    public void setTransdateto(Date transdateto) {
        this.transdateto = transdateto;
    }

    @Override
    public Integer getTypecodeid() {
        return typecodeid;
    }

    @Override
    public void setTypecodeid(Integer typecodeid) {
        this.typecodeid = typecodeid;
    }

    @Override
    public String getTypecodename() {
        return typecodename;
    }

    @Override
    public void setTypecodename(String typecodename) {
        this.typecodename = typecodename;
    }

    @Override
    public Integer getValidationruleid() {
        return validationruleid;
    }

    @Override
    public void setValidationruleid(Integer validationruleid) {
        this.validationruleid = validationruleid;
    }

    @Override
    public Integer getViewid() {
        return viewid;
    }

    @Override
    public void setViewid(Integer viewid) {
        this.viewid = viewid;
    }

}
