package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface switchfiletemplatedetailDTOInter extends Serializable {

    int getid();

    void setid(int id);

    int gettemplate();

    void settemplate(int template);

    int getcolumnid();

    void setcolumnid(int columnid);

    int getposition();

    void setposition(int position);

    int getlinenumber();

    void setlinenumber(int linenumber);

    String getformat();

    void setformat(String format);

    String getformat2();

    void setformat2(String format2);

    int getdatatype();

    void setdatatype(int datatype);

    int getlength();

    void setlength(int length);

    int getloadwhenmapped();

    void setloadwhenmapped(int loadwhenmapped);

    int getlengthdir();

    void setlengthdir(int lengthdir);

    int getstartingposition();

    void setstartingposition(int startingposition);

    String gettagstring();

    void settagstring(String tagstring);

    int getmandatory();

    void setmandatory(int mandatory);

    int getnegativeamountflag();

    void setnegativeamountflag(int negativeamountflag);

    int getadddecimal();

    void setadddecimal(int adddecimal);

    int getdecimalpos();

    void setdecimalpos(int decimalpos);
}
