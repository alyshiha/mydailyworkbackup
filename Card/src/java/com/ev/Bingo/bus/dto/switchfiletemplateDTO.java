package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;

public class switchfiletemplateDTO extends BaseDTO implements switchfiletemplateDTOInter, Serializable {

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private String name;

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }
    private int processingtype;

    public int getprocessingtype() {
        return processingtype;
    }

    public void setprocessingtype(int processingtype) {
        this.processingtype = processingtype;
    }
    private int numberoflines;

    public int getnumberoflines() {
        return numberoflines;
    }

    public void setnumberoflines(int numberoflines) {
        this.numberoflines = numberoflines;
    }
    private String loadingfolder;

    public String getloadingfolder() {
        return loadingfolder;
    }

    public void setloadingfolder(String loadingfolder) {
        this.loadingfolder = loadingfolder;
    }
    private int startingdatatype;

    public int getstartingdatatype() {
        return startingdatatype;
    }

    public void setstartingdatatype(int startingdatatype) {
        this.startingdatatype = startingdatatype;
    }
    private String startingformat;

    public String getstartingformat() {
        return startingformat;
    }

    public void setstartingformat(String startingformat) {
        this.startingformat = startingformat;
    }
    private int startingposition;

    public int getstartingposition() {
        return startingposition;
    }

    public void setstartingposition(int startingposition) {
        this.startingposition = startingposition;
    }
    private String backupfolder;

    public String getbackupfolder() {
        return backupfolder;
    }

    public void setbackupfolder(String backupfolder) {
        this.backupfolder = backupfolder;
    }
    private String copyfolder;

    public String getcopyfolder() {
        return copyfolder;
    }

    public void setcopyfolder(String copyfolder) {
        this.copyfolder = copyfolder;
    }
    private int ignoredlines;

    public int getignoredlines() {
        return ignoredlines;
    }

    public void setignoredlines(int ignoredlines) {
        this.ignoredlines = ignoredlines;
    }
    private int startinglength;

    public int getstartinglength() {
        return startinglength;
    }

    public void setstartinglength(int startinglength) {
        this.startinglength = startinglength;
    }
    private String dateseparator;

    public String getdateseparator() {
        return dateseparator;
    }

    public void setdateseparator(String dateseparator) {
        this.dateseparator = dateseparator;
    }
    private int dateseparatorpos1;

    public int getdateseparatorpos1() {
        return dateseparatorpos1;
    }

    public void setdateseparatorpos1(int dateseparatorpos1) {
        this.dateseparatorpos1 = dateseparatorpos1;
    }
    private int dateseparatorpos2;

    public int getdateseparatorpos2() {
        return dateseparatorpos2;
    }

    public void setdateseparatorpos2(int dateseparatorpos2) {
        this.dateseparatorpos2 = dateseparatorpos2;
    }
    private String serverfolder;

    public String getserverfolder() {
        return serverfolder;
    }

    public void setserverfolder(String serverfolder) {
        this.serverfolder = serverfolder;
    }
    private int headerdatatype;

    public int getheaderdatatype() {
        return headerdatatype;
    }

    public void setheaderdatatype(int headerdatatype) {
        this.headerdatatype = headerdatatype;
    }
    private String headerformat;

    public String getheaderformat() {
        return headerformat;
    }

    public void setheaderformat(String headerformat) {
        this.headerformat = headerformat;
    }
    private String headerposition;

    public String getheaderposition() {
        return headerposition;
    }

    public void setheaderposition(String headerposition) {
        this.headerposition = headerposition;
    }
    private int headerlength;

    public int getheaderlength() {
        return headerlength;
    }

    public void setheaderlength(int headerlength) {
        this.headerlength = headerlength;
    }
    private String headerdateseparator;

    public String getheaderdateseparator() {
        return headerdateseparator;
    }

    public void setheaderdateseparator(String headerdateseparator) {
        this.headerdateseparator = headerdateseparator;
    }
    private int headerdateseparatorpos1;

    public int getheaderdateseparatorpos1() {
        return headerdateseparatorpos1;
    }

    public void setheaderdateseparatorpos1(int headerdateseparatorpos1) {
        this.headerdateseparatorpos1 = headerdateseparatorpos1;
    }
    private int headerdateseparatorpos2;

    public int getheaderdateseparatorpos2() {
        return headerdateseparatorpos2;
    }

    public void setheaderdateseparatorpos2(int headerdateseparatorpos2) {
        this.headerdateseparatorpos2 = headerdateseparatorpos2;
    }
    private String headerstring;

    public String getheaderstring() {
        return headerstring;
    }

    public void setheaderstring(String headerstring) {
        this.headerstring = headerstring;
    }
    private String startingvalue;

    public String getstartingvalue() {
        return startingvalue;
    }

    public void setstartingvalue(String startingvalue) {
        this.startingvalue = startingvalue;
    }
    private int active;

    public int getactive() {
        return active;
    }

    public void setactive(int active) {
        this.active = active;
    }
    private String separator;

    public String getseparator() {
        return separator;
    }

    public void setseparator(String separator) {
        this.separator = separator;
    }
    private int tagsendingdatatype;

    public int gettagsendingdatatype() {
        return tagsendingdatatype;
    }

    public void settagsendingdatatype(int tagsendingdatatype) {
        this.tagsendingdatatype = tagsendingdatatype;
    }
    private String tagsendingformat;

    public String gettagsendingformat() {
        return tagsendingformat;
    }

    public void settagsendingformat(String tagsendingformat) {
        this.tagsendingformat = tagsendingformat;
    }
    private int tagsendingposition;

    public int gettagsendingposition() {
        return tagsendingposition;
    }

    public void settagsendingposition(int tagsendingposition) {
        this.tagsendingposition = tagsendingposition;
    }
    private int tagsendinglength;

    public int gettagsendinglength() {
        return tagsendinglength;
    }

    public void settagsendinglength(int tagsendinglength) {
        this.tagsendinglength = tagsendinglength;
    }
    private String tagsendingvalue;

    public String gettagsendingvalue() {
        return tagsendingvalue;
    }

    public void settagsendingvalue(String tagsendingvalue) {
        this.tagsendingvalue = tagsendingvalue;
    }
    private int headerlines;

    public int getheaderlines() {
        return headerlines;
    }

    public void setheaderlines(int headerlines) {
        this.headerlines = headerlines;
    }
    private int maxtaglines;

    public int getmaxtaglines() {
        return maxtaglines;
    }

    public void setmaxtaglines(int maxtaglines) {
        this.maxtaglines = maxtaglines;
    }
}
