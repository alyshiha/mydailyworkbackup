package com.ev.Bingo.bus.dto;

import java.io.Serializable;

public interface transactiontypeDTOInter extends Serializable {

    int getid();

    void setid(int id);

    String getname();

    void setname(String name);

    String getdescription();

    void setdescription(String description);

    int gettypecategory();

    void settypecategory(int typecategory);

    int gettypemaster();

    void settypemaster(int typemaster);

    boolean getreverseflag();

    void setreverseflag(boolean reverseflag);

    boolean getreversetype();

    void setreversetype(boolean reversetype);
}
