package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface blockedusersDTOInter extends Serializable {

    int getUserid();
             void setUserid(int userid);
                     String getUsername();
             void setUsername(String username);
                     String getReasonname();
              void setReasonname(String reasonname);
                      Date getBlocktime();
               void setBlocktime(Date blocktime) ;
                        int getReasonid();
                                void setReasonid(int reasonid) ;
}
