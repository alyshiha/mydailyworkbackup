/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public interface UserNetworkDTOInter {

    String getRowid();

    void setRowid(String rowid);

    int getNetworkid();

    int getUserid();

    void setNetworkid(int networkid);

    void setUserid(int userid);

}
