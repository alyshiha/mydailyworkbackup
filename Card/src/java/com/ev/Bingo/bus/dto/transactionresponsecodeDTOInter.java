package com.ev.Bingo.bus.dto;

import java.io.Serializable;

public interface transactionresponsecodeDTOInter extends Serializable {

    int getid();

    void setid(int id);

    String getname();

    void setname(String name);

    String getdescription();

    void setdescription(String description);

    int getamounttype();

    void setamounttype(int amounttype);

    int getdefaultflag();

    void setdefaultflag(int defaultflag);

    int getmastercode();

    void setmastercode(int mastercode);

    void setdefaultflagB(boolean defaultflagB);

    boolean getdefaultflagB();
}
