package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class systemtableDTO extends BaseDTO implements systemtableDTOInter, Serializable {

    private String parametername;

    public String getparametername() {
        return parametername;
    }

    public void setparametername(String parametername) {
        this.parametername = parametername;
    }
    private String parametervalue;

    public String getparametervalue() {
        return parametervalue;
    }

    public void setparametervalue(String parametervalue) {
        this.parametervalue = parametervalue;
    }
    private int parameterdatatype;

    public int getparameterdatatype() {
        return parameterdatatype;
    }

    public void setparameterdatatype(int parameterdatatype) {
        this.parameterdatatype = parameterdatatype;
    }
}
