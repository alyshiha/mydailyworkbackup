/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface ValidationMonitorDTOInter extends Serializable {

    int getPendingHost();

    int getPendingJournals();

    int getPendingSwitch();

    void setPendingHost(int pendingHost);

    void setPendingJournals(int pendingJournals);

    void setPendingSwitch(int pendingSwitch);

}
