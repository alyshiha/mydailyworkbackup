package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface transactionmanagementDTOInter extends Serializable {

    int gettranno();

    void settranno(int tranno);

    String getkey();

    void setkey(String key);

    int gettranmonth();

    void settranmonth(int tranmonth);

    int gettranyear();

    void settranyear(int tranyear);
}
