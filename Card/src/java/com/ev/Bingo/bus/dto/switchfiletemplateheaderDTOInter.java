package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface switchfiletemplateheaderDTOInter extends Serializable {

    int getid();

    void setid(int id);

    int getcolumnid();

    void setcolumnid(int columnid);

    int gettemplate();

    void settemplate(int template);

    int getposition();

    void setposition(int position);

    String getformat();

    void setformat(String format);

    String getformat2();

    void setformat2(String format2);

    int getdatatype();

    void setdatatype(int datatype);

    int getlength();

    void setlength(int length);

    int getlineno();

    void setlineno(int lineno);
}
