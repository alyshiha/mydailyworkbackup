package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class matchingerrorsdataDTO extends BaseDTO implements matchingerrorsdataDTOInter, Serializable {

    private String cards;

    public String getcards() {
        return cards;
    }

    public void setcards(String cards) {
        this.cards = cards;
    }
    private String filename;

    public String getfilename() {
        return filename;
    }

    public void setfilename(String filename) {
        this.filename = filename;
    }
    private String transactionmaster;

    public String gettransactionmaster() {
        return transactionmaster;
    }

    public void settransactionmaster(String transactionmaster) {
        this.transactionmaster = transactionmaster;
    }
    private String record;

    public String getrecord() {
        return record;
    }

    public void setrecord(String record) {
        this.record = record;
    }
    private int fileid;

    public int getfileid() {
        return fileid;
    }

    public void setfileid(int fileid) {
        this.fileid = fileid;
    }
    private int rownum;

    public int getRownum() {
        return rownum;
    }

    public void setRownum(int rownum) {
        this.rownum = rownum;
    }
    private Date loadingdate;

    public Date getloadingdate() {
        return loadingdate;
    }

    public void setloadingdate(Date loadingdate) {
        this.loadingdate = loadingdate;
    }
    private String rowid;

    public String getrowid() {
        return rowid;
    }

    public void setrowid(String rowid) {
        this.rowid = rowid;
    }
    private String transactiontype;

    public String gettransactiontype() {
        return transactiontype;
    }

    public void settransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }
    private int transactiontypeid;

    public int gettransactiontypeid() {
        return transactiontypeid;
    }

    public void settransactiontypeid(int transactiontypeid) {
        this.transactiontypeid = transactiontypeid;
    }
    private String currency;

    public String getcurrency() {
        return currency;
    }

    public void setcurrency(String currency) {
        this.currency = currency;
    }
    private int currencyid;

    public int getcurrencyid() {
        return currencyid;
    }

    public void setcurrencyid(int currencyid) {
        this.currencyid = currencyid;
    }
    private String responsecode;

    public String getresponsecode() {
        return responsecode;
    }

    public void setresponsecode(String responsecode) {
        this.responsecode = responsecode;
    }
    private int responsecodeid;

    public int getresponsecodeid() {
        return responsecodeid;
    }

    public void setresponsecodeid(int responsecodeid) {
        this.responsecodeid = responsecodeid;
    }
    private Date transactiondate;

    public Date gettransactiondate() {
        return transactiondate;
    }

    public void settransactiondate(Date transactiondate) {
        this.transactiondate = transactiondate;
    }
    private String transactionsequence;

    public String gettransactionsequence() {
        return transactionsequence;
    }

    public void settransactionsequence(String transactionsequence) {
        this.transactionsequence = transactionsequence;
    }
    private String cardno;

    public String getcardno() {
        return cardno;
    }

    public void setcardno(String cardno) {
        this.cardno = cardno;
    }
    private Float amount;

    public Float getamount() {
        return amount;
    }

    public void setamount(Float amount) {
        this.amount = amount;
    }
    private Date settlementdate;

    public Date getsettlementdate() {
        return settlementdate;
    }

    public void setsettlementdate(Date settlementdate) {
        this.settlementdate = settlementdate;
    }
    private String customeraccountnumber;

    public String getcustomeraccountnumber() {
        return customeraccountnumber;
    }

    public void setcustomeraccountnumber(String customeraccountnumber) {
        this.customeraccountnumber = customeraccountnumber;
    }
    private int transactionsequenceorderby;

    public int gettransactionsequenceorderby() {
        return transactionsequenceorderby;
    }

    public void settransactionsequenceorderby(int transactionsequenceorderby) {
        this.transactionsequenceorderby = transactionsequenceorderby;
    }
    private Date transactiontime;

    public Date gettransactiontime() {
        return transactiontime;
    }

    public void settransactiontime(Date transactiontime) {
        this.transactiontime = transactiontime;
    }
    private String column1;

    public String getcolumn1() {
        return column1;
    }

    public void setcolumn1(String column1) {
        this.column1 = column1;
    }
    private String column2;

    public String getcolumn2() {
        return column2;
    }

    public void setcolumn2(String column2) {
        this.column2 = column2;
    }
    private String column3;

    public String getcolumn3() {
        return column3;
    }

    public void setcolumn3(String column3) {
        this.column3 = column3;
    }
    private String column4;

    public String getcolumn4() {
        return column4;
    }

    public void setcolumn4(String column4) {
        this.column4 = column4;
    }
    private String column5;

    public String getcolumn5() {
        return column5;
    }

    public void setcolumn5(String column5) {
        this.column5 = column5;
    }
    private int jobid;

    public int getjobid() {
        return jobid;
    }

    public void setjobid(int jobid) {
        this.jobid = jobid;
    }
    private int transactiontypemaster;

    public int gettransactiontypemaster() {
        return transactiontypemaster;
    }

    public void settransactiontypemaster(int transactiontypemaster) {
        this.transactiontypemaster = transactiontypemaster;
    }
    private int responsecodemaster;

    public int getresponsecodemaster() {
        return responsecodemaster;
    }

    public void setresponsecodemaster(int responsecodemaster) {
        this.responsecodemaster = responsecodemaster;
    }
    private int cardnosuffix;

    public int getcardnosuffix() {
        return cardnosuffix;
    }

    public void setcardnosuffix(int cardnosuffix) {
        this.cardnosuffix = cardnosuffix;
    }
    private int recordtype;

    public int getrecordtype() {
        return recordtype;
    }

    public void setrecordtype(int recordtype) {
        this.recordtype = recordtype;
    }
    private int errorcode;

    public int geterrorcode() {
        return errorcode;
    }

    public void seterrorcode(int errorcode) {
        this.errorcode = errorcode;
    }
    private String errormessage;

    public String geterrormessage() {
        return errormessage;
    }

    public void seterrormessage(String errormessage) {
        this.errormessage = errormessage;
    }
    private Date errordate;

    public Date geterrordate() {
        return errordate;
    }

    public void seterrordate(Date errordate) {
        this.errordate = errordate;
    }
    private String sqlstatemenmt;

    public String getsqlstatemenmt() {
        return sqlstatemenmt;
    }

    public void setsqlstatemenmt(String sqlstatemenmt) {
        this.sqlstatemenmt = sqlstatemenmt;
    }
    private Float absamount;

    public Float getabsamount() {
        return absamount;
    }

    public void setabsamount(Float absamount) {
        this.absamount = absamount;
    }
    private String terminal;

    public String getterminal() {
        return terminal;
    }

    public void setterminal(String terminal) {
        this.terminal = terminal;
    }
    private String networkid;

    public String getnetworkid() {
        return networkid;
    }

    public void setnetworkid(String networkid) {
        this.networkid = networkid;
    }
    private String acquirercurrency;

    public String getacquirercurrency() {
        return acquirercurrency;
    }

    public void setacquirercurrency(String acquirercurrency) {
        this.acquirercurrency = acquirercurrency;
    }
    private int acquirercurrencyid;

    public int getacquirercurrencyid() {
        return acquirercurrencyid;
    }

    public void setacquirercurrencyid(int acquirercurrencyid) {
        this.acquirercurrencyid = acquirercurrencyid;
    }
    private Float acquireramount;

    public Float getacquireramount() {
        return acquireramount;
    }

    public void setacquireramount(Float acquireramount) {
        this.acquireramount = acquireramount;
    }
    private String settlementcurrency;

    public String getsettlementcurrency() {
        return settlementcurrency;
    }

    public void setsettlementcurrency(String settlementcurrency) {
        this.settlementcurrency = settlementcurrency;
    }
    private int settlementcurrencyid;

    public int getsettlementcurrencyid() {
        return settlementcurrencyid;
    }

    public void setsettlementcurrencyid(int settlementcurrencyid) {
        this.settlementcurrencyid = settlementcurrencyid;
    }
    private Float settlementamount;

    public Float getsettlementamount() {
        return settlementamount;
    }

    public void setsettlementamount(Float settlementamount) {
        this.settlementamount = settlementamount;
    }
    private String authorizationno;

    public String getauthorizationno() {
        return authorizationno;
    }

    public void setauthorizationno(String authorizationno) {
        this.authorizationno = authorizationno;
    }
    private String referenceno;

    public String getreferenceno() {
        return referenceno;
    }

    public void setreferenceno(String referenceno) {
        this.referenceno = referenceno;
    }
    private String bank;

    public String getbank() {
        return bank;
    }

    public void setbank(String bank) {
        this.bank = bank;
    }
    private int transactionid;

    public int gettransactionid() {
        return transactionid;
    }

    public void settransactionid(int transactionid) {
        this.transactionid = transactionid;
    }
    private int disputekey;

    public int getdisputekey() {
        return disputekey;
    }

    public void setdisputekey(int disputekey) {
        this.disputekey = disputekey;
    }
    private String licencekey;

    public String getlicencekey() {
        return licencekey;
    }

    public void setlicencekey(String licencekey) {
        this.licencekey = licencekey;
    }
    private String networkcode;

    public String getnetworkcode() {
        return networkcode;
    }

    public void setnetworkcode(String networkcode) {
        this.networkcode = networkcode;
    }
    private String processcode;

    public String getprocesscode() {
        return processcode;
    }

    public void setprocesscode(String processcode) {
        this.processcode = processcode;
    }
    private String areacode;

    public String getareacode() {
        return areacode;
    }

    public void setareacode(String areacode) {
        this.areacode = areacode;
    }
    private String reportid;

    public String getreportid() {
        return reportid;
    }

    public void setreportid(String reportid) {
        this.reportid = reportid;
    }
    private int networkcomm;

    public int getnetworkcomm() {
        return networkcomm;
    }

    public void setnetworkcomm(int networkcomm) {
        this.networkcomm = networkcomm;
    }
    private String networkcommflag;

    public String getnetworkcommflag() {
        return networkcommflag;
    }

    public void setnetworkcommflag(String networkcommflag) {
        this.networkcommflag = networkcommflag;
    }
    private String feelevel;

    public String getfeelevel() {
        return feelevel;
    }

    public void setfeelevel(String feelevel) {
        this.feelevel = feelevel;
    }
}
