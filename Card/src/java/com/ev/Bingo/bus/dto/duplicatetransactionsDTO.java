package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class duplicatetransactionsDTO extends BaseDTO implements duplicatetransactionsDTOInter, Serializable {

    private int fileid;

    public int getfileid() {
        return fileid;
    }

    public void setfileid(int fileid) {
        this.fileid = fileid;
    }
    private Date loadingdate;

    public Date getloadingdate() {
        return loadingdate;
    }

    public void setloadingdate(Date loadingdate) {
        this.loadingdate = loadingdate;
    }
    private String atmapplicationid;

    public String getatmapplicationid() {
        return atmapplicationid;
    }

    public void setatmapplicationid(String atmapplicationid) {
        this.atmapplicationid = atmapplicationid;
    }
    private int atmid;

    public int getatmid() {
        return atmid;
    }

    public void setatmid(int atmid) {
        this.atmid = atmid;
    }
    private String transactiontype;

    public String gettransactiontype() {
        return transactiontype;
    }

    public void settransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }
    private int transactiontypeid;

    public int gettransactiontypeid() {
        return transactiontypeid;
    }

    public void settransactiontypeid(int transactiontypeid) {
        this.transactiontypeid = transactiontypeid;
    }
    private String currency;

    public String getcurrency() {
        return currency;
    }

    public void setcurrency(String currency) {
        this.currency = currency;
    }
    private int currencyid;

    public int getcurrencyid() {
        return currencyid;
    }

    public void setcurrencyid(int currencyid) {
        this.currencyid = currencyid;
    }
    private String transactionstatus;

    public String gettransactionstatus() {
        return transactionstatus;
    }

    public void settransactionstatus(String transactionstatus) {
        this.transactionstatus = transactionstatus;
    }
    private int transactionstatusid;

    public int gettransactionstatusid() {
        return transactionstatusid;
    }

    public void settransactionstatusid(int transactionstatusid) {
        this.transactionstatusid = transactionstatusid;
    }
    private String responsecode;

    public String getresponsecode() {
        return responsecode;
    }

    public void setresponsecode(String responsecode) {
        this.responsecode = responsecode;
    }
    private int responsecodeid;

    public int getresponsecodeid() {
        return responsecodeid;
    }

    public void setresponsecodeid(int responsecodeid) {
        this.responsecodeid = responsecodeid;
    }
    private Date transactiondate;

    public Date gettransactiondate() {
        return transactiondate;
    }

    public void settransactiondate(Date transactiondate) {
        this.transactiondate = transactiondate;
    }
    private String transactionsequence;

    public String gettransactionsequence() {
        return transactionsequence;
    }

    public void settransactionsequence(String transactionsequence) {
        this.transactionsequence = transactionsequence;
    }
    private String cardno;

    public String getcardno() {
        return cardno;
    }

    public void setcardno(String cardno) {
        this.cardno = cardno;
    }
    private Float amount;

    public Float getamount() {
        return amount;
    }

    public void setamount(Float amount) {
        this.amount = amount;
    }
    private Date settlementdate;

    public Date getsettlementdate() {
        return settlementdate;
    }

    public void setsettlementdate(Date settlementdate) {
        this.settlementdate = settlementdate;
    }
    private String notespresented;

    public String getnotespresented() {
        return notespresented;
    }

    public void setnotespresented(String notespresented) {
        this.notespresented = notespresented;
    }
    private String customeraccountnumber;

    public String getcustomeraccountnumber() {
        return customeraccountnumber;
    }

    public void setcustomeraccountnumber(String customeraccountnumber) {
        this.customeraccountnumber = customeraccountnumber;
    }
    private int transactionsequenceorderby;

    public int gettransactionsequenceorderby() {
        return transactionsequenceorderby;
    }

    public void settransactionsequenceorderby(int transactionsequenceorderby) {
        this.transactionsequenceorderby = transactionsequenceorderby;
    }
    private Date transactiontime;

    public Date gettransactiontime() {
        return transactiontime;
    }

    public void settransactiontime(Date transactiontime) {
        this.transactiontime = transactiontime;
    }
    private String recordtype;

    public String getrecordtype() {
        return recordtype;
    }

    public void setrecordtype(String recordtype) {
        this.recordtype = recordtype;
    }
    private String column1;

    public String getcolumn1() {
        return column1;
    }

    public void setcolumn1(String column1) {
        this.column1 = column1;
    }
    private String column2;

    public String getcolumn2() {
        return column2;
    }

    public void setcolumn2(String column2) {
        this.column2 = column2;
    }
    private String column3;

    public String getcolumn3() {
        return column3;
    }

    public void setcolumn3(String column3) {
        this.column3 = column3;
    }
    private String column4;

    public String getcolumn4() {
        return column4;
    }

    public void setcolumn4(String column4) {
        this.column4 = column4;
    }
    private String column5;

    public String getcolumn5() {
        return column5;
    }

    public void setcolumn5(String column5) {
        this.column5 = column5;
    }
    private int transactiontypemaster;

    public int gettransactiontypemaster() {
        return transactiontypemaster;
    }

    public void settransactiontypemaster(int transactiontypemaster) {
        this.transactiontypemaster = transactiontypemaster;
    }
    private int responsecodemaster;

    public int getresponsecodemaster() {
        return responsecodemaster;
    }

    public void setresponsecodemaster(int responsecodemaster) {
        this.responsecodemaster = responsecodemaster;
    }
    private String cardnosuffix;

    public String getcardnosuffix() {
        return cardnosuffix;
    }

    public void setcardnosuffix(String cardnosuffix) {
        this.cardnosuffix = cardnosuffix;
    }
}
