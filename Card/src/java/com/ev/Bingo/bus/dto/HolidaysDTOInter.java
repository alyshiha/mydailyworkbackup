/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author shi7a
 */
public interface HolidaysDTOInter {

    Date getHolidaydate();

    int getHolidayid();

    String getHolidayname();

    String getHolidaytype();

    void setHolidaydate(Date holidaydate);

    void setHolidayid(int holidayid);

    void setHolidayname(String holidayname);

    void setHolidaytype(String holidaytype);
    
}
