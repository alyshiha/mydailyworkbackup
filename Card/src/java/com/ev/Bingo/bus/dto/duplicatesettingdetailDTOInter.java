package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface duplicatesettingdetailDTOInter extends Serializable {

    String getRowid();

    void setRowid(String rowid);

    int getid();

    void setid(int id);

    int getcolumnid();

    void setcolumnid(int columnid);
}
