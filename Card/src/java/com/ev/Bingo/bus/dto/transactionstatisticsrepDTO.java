package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;

public class transactionstatisticsrepDTO extends BaseDTO implements transactionstatisticsrepDTOInter, Serializable {

    private int reportid;

    public int getreportid() {
        return reportid;
    }

    public void setreportid(int reportid) {
        this.reportid = reportid;
    }
    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private String name;

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }
    private Float matchedcount;

    public Float getmatchedcount() {
        return matchedcount;
    }

    public void setmatchedcount(Float matchedcount) {
        this.matchedcount = matchedcount;
    }
    private Float matchedamountnetwork;

    public Float getmatchedamountnetwork() {
        return matchedamountnetwork;
    }

    public void setmatchedamountnetwork(Float matchedamountnetwork) {
        this.matchedamountnetwork = matchedamountnetwork;
    }
    private Float matchedamountswitch;

    public Float getmatchedamountswitch() {
        return matchedamountswitch;
    }

    public void setmatchedamountswitch(Float matchedamountswitch) {
        this.matchedamountswitch = matchedamountswitch;
    }
    private Float matchedwithdiffamounttotal;

    public Float getmatchedwithdiffamounttotal() {
        return matchedwithdiffamounttotal;
    }

    public void setmatchedwithdiffamounttotal(Float matchedwithdiffamounttotal) {
        this.matchedwithdiffamounttotal = matchedwithdiffamounttotal;
    }
    private Float matchedwithdiffamountcount;

    public Float getmatchedwithdiffamountcount() {
        return matchedwithdiffamountcount;
    }

    public void setmatchedwithdiffamountcount(Float matchedwithdiffamountcount) {
        this.matchedwithdiffamountcount = matchedwithdiffamountcount;
    }
    private Float countdisputesnetwork;

    public Float getcountdisputesnetwork() {
        return countdisputesnetwork;
    }

    public void setcountdisputesnetwork(Float countdisputesnetwork) {
        this.countdisputesnetwork = countdisputesnetwork;
    }
    private Float totaldisputesnetwork;

    public Float gettotaldisputesnetwork() {
        return totaldisputesnetwork;
    }

    public void settotaldisputesnetwork(Float totaldisputesnetwork) {
        this.totaldisputesnetwork = totaldisputesnetwork;
    }
    private Float countdisputesswitch;

    public Float getcountdisputesswitch() {
        return countdisputesswitch;
    }

    public void setcountdisputesswitch(Float countdisputesswitch) {
        this.countdisputesswitch = countdisputesswitch;
    }
    private Float totaldisputesswitch;

    public Float gettotaldisputesswitch() {
        return totaldisputesswitch;
    }

    public void settotaldisputesswitch(Float totaldisputesswitch) {
        this.totaldisputesswitch = totaldisputesswitch;
    }
    private Float countdisputesswitch000;

    public Float getcountdisputesswitch000() {
        return countdisputesswitch000;
    }

    public void setcountdisputesswitch000(Float countdisputesswitch000) {
        this.countdisputesswitch000 = countdisputesswitch000;
    }
    private Float totaldisputesswitch000;

    public Float gettotaldisputesswitch000() {
        return totaldisputesswitch000;
    }

    public void settotaldisputesswitch000(Float totaldisputesswitch000) {
        this.totaldisputesswitch000 = totaldisputesswitch000;
    }
    private Float countdisputesnetworknotrev;

    public Float getcountdisputesnetworknotrev() {
        return countdisputesnetworknotrev;
    }

    public void setcountdisputesnetworknotrev(Float countdisputesnetworknotrev) {
        this.countdisputesnetworknotrev = countdisputesnetworknotrev;
    }
    private Float totaldisputesnetworknotrev;

    public Float gettotaldisputesnetworknotrev() {
        return totaldisputesnetworknotrev;
    }

    public void settotaldisputesnetworknotrev(Float totaldisputesnetworknotrev) {
        this.totaldisputesnetworknotrev = totaldisputesnetworknotrev;
    }
    private Float countdisputesnetworkrev;

    public Float getcountdisputesnetworkrev() {
        return countdisputesnetworkrev;
    }

    public void setcountdisputesnetworkrev(Float countdisputesnetworkrev) {
        this.countdisputesnetworkrev = countdisputesnetworkrev;
    }
    private Float totaldisputesnetworkrev;

    public Float gettotaldisputesnetworkrev() {
        return totaldisputesnetworkrev;
    }

    public void settotaldisputesnetworkrev(Float totaldisputesnetworkrev) {
        this.totaldisputesnetworkrev = totaldisputesnetworkrev;
    }
}
