package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class userprofileDTO extends BaseDTO implements userprofileDTOInter, Serializable {

    private int userid;

    public int getuserid() {
        return userid;
    }

    public void setuserid(int userid) {
        this.userid = userid;
    }
    private String username;

    public String getusername() {
        return username;
    }

    public void setusername(String username) {
        this.username = username;
    }
    private int profileid;

    public int getprofileid() {
        return profileid;
    }

    public void setprofileid(int profileid) {
        this.profileid = profileid;
    }
}
