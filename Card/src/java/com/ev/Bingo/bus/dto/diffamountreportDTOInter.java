package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface diffamountreportDTOInter extends Serializable {

    Date gettransdate();

    void settransdate(Date transdate);

    String getcardnumber();

    void setcardnumber(String cardnumber);

    int getnetwork();

    void setnetwork(int network);

    int getSwitchvalue();

    void setSwitchvalue(int switchvalue);

    int getnetworkdiff();

    void setnetworkdiff(int networkdiff);

    int getswitchdiff();

    void setswitchdiff(int switchdiff);

    int getreortid();

    void setreortid(int reortid);
}
