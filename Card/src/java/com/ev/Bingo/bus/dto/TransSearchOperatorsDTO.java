/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class TransSearchOperatorsDTO implements TransSearchOperatorsDTOInter {

    private List<String> rulesValue1;
    private List<String> rulesValue2;
    private List<String> rulesValue3;
    private List<String> rulesValue4;

    public void setRulesValue1(List<String> rulesValue1) {
        this.rulesValue1 = rulesValue1;
    }

    public void setRulesValue2(List<String> rulesValue2) {
        this.rulesValue2 = rulesValue2;
    }

    public void setRulesValue3(List<String> rulesValue3) {
        this.rulesValue3 = rulesValue3;
    }

    public void setRulesValue4(List<String> rulesValue4) {
        this.rulesValue4 = rulesValue4;
    }

    public List<String> getRulesValue1() {
        rulesValue1 = new ArrayList<String>();
        rulesValue1.add("=");
        rulesValue1.add(">");
        rulesValue1.add("<");
        rulesValue1.add(">=");
        rulesValue1.add("<=");
        rulesValue1.add("<>");
        rulesValue1.add("Between");
        return rulesValue1;
    }

    public List<String> getRulesValue2() {
        rulesValue2 = new ArrayList<String>();
        rulesValue2.add("=");
        rulesValue2.add("<>");
        rulesValue2.add("Like");
        rulesValue2.add("Not Like");
        rulesValue2.add("is Null");
        rulesValue2.add("is Not Null");
        return rulesValue2;
    }

    public List<String> getRulesValue3() {
        rulesValue3 = new ArrayList<String>();
        rulesValue3.add("=");
        rulesValue3.add("<>");
        return rulesValue3;
    }

    public List<String> getRulesValue4() {
        rulesValue4 = new ArrayList<String>();
        rulesValue4.add("=");
        rulesValue4.add("Like");
        return rulesValue4;
    }
}
