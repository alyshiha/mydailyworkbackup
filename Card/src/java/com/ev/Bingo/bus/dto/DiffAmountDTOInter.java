/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public interface DiffAmountDTOInter {
void setAcc(String acc);
String getAcc() ;
    void setNetwork(String network);

    String getNetwork();

    String getCardno();

    int getRownum();

    Date getTransactiondate();

    float getAmount1();

    void setAmount1(float amount1);

    float getAmount2();

    void setAmount2(float amount2);

    float getSwitchD();

    void setSwitchD(float switchD);

    float getNetworkD();

    void setNetworkD(float networkD);

    void setCardno(String cardno);

    void setRownum(int rownum);

    void setTransactiondate(Date transactiondate);

}
