/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.math.BigDecimal;

/**
 *
 * @author Aly-Shiha
 */
public class VisaOffusTotalsDTO implements VisaOffusTotalsDTOInter {

    private Integer transCount;
    private BigDecimal income;
    private String type;
    private String terminal;
    private String terminalName;
    private String transMonth;

    public VisaOffusTotalsDTO() {
    }

    public Integer getTransCount() {
        return transCount;
    }

    public void setTransCount(Integer transCount) {
        this.transCount = transCount;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public String getTransMonth() {
        return transMonth;
    }

    public void setTransMonth(String transMonth) {
        this.transMonth = transMonth;
    }

    public VisaOffusTotalsDTO(Integer transCount, BigDecimal income, String type, String terminal, String terminalName, String transMonth) {
        this.transCount = transCount;
        this.income = income;
        this.type = type;
        this.terminal = terminal;
        this.terminalName = terminalName;
        this.transMonth = transMonth;
    }

}
