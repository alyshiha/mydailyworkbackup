package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class blacklistedcardDTO extends BaseDTO implements blacklistedcardDTOInter, Serializable {

    private String cardno;

    public String getcardno() {
        return cardno;
    }

    public void setcardno(String cardno) {
        this.cardno = cardno;
    }
    private String customerno;

    public String getcustomerno() {
        return customerno;
    }

    public void setcustomerno(String customerno) {
        this.customerno = customerno;
    }
    private String comments;

    public String getcomments() {
        return comments;
    }

    public void setcomments(String comments) {
        this.comments = comments;
    }
    private Date blacklisteddate;

    public Date getblacklisteddate() {
        return blacklisteddate;
    }

    public void setblacklisteddate(Date blacklisteddate) {
        this.blacklisteddate = blacklisteddate;
    }
}
