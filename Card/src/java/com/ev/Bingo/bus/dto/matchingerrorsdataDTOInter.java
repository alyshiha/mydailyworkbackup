package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface matchingerrorsdataDTOInter extends Serializable {

    int getfileid();

    String getcards();

    void setcards(String cards);

    String getfilename();

    void setfilename(String filename);

    String gettransactionmaster();

    void settransactionmaster(String transactionmaster);

    String getrecord();

    void setrecord(String record);

    void setfileid(int fileid);

    Date getloadingdate();

    void setloadingdate(Date loadingdate);

    String gettransactiontype();

    void settransactiontype(String transactiontype);

    int gettransactiontypeid();

    void settransactiontypeid(int transactiontypeid);

    String getcurrency();

    public String getrowid();

    int getRownum();

    public void setRownum(int rownum);

    public void setrowid(String rowid);

    public int getdisputekey();

    public void setdisputekey(int disputekey);

    void setcurrency(String currency);

    int getcurrencyid();

    void setcurrencyid(int currencyid);

    String getresponsecode();

    void setresponsecode(String responsecode);

    int getresponsecodeid();

    void setresponsecodeid(int responsecodeid);

    Date gettransactiondate();

    void settransactiondate(Date transactiondate);

    String gettransactionsequence();

    void settransactionsequence(String transactionsequence);

    String getcardno();

    void setcardno(String cardno);

    Float getamount();

    void setamount(Float amount);

    Date getsettlementdate();

    void setsettlementdate(Date settlementdate);

    String getcustomeraccountnumber();

    void setcustomeraccountnumber(String customeraccountnumber);

    int gettransactionsequenceorderby();

    void settransactionsequenceorderby(int transactionsequenceorderby);

    Date gettransactiontime();

    void settransactiontime(Date transactiontime);

    String getcolumn1();

    void setcolumn1(String column1);

    String getcolumn2();

    void setcolumn2(String column2);

    String getcolumn3();

    void setcolumn3(String column3);

    String getcolumn4();

    void setcolumn4(String column4);

    String getcolumn5();

    void setcolumn5(String column5);

    int getjobid();

    void setjobid(int jobid);

    int gettransactiontypemaster();

    void settransactiontypemaster(int transactiontypemaster);

    int getresponsecodemaster();

    void setresponsecodemaster(int responsecodemaster);

    int getcardnosuffix();

    void setcardnosuffix(int cardnosuffix);

    int getrecordtype();

    void setrecordtype(int recordtype);

    int geterrorcode();

    void seterrorcode(int errorcode);

    String geterrormessage();

    void seterrormessage(String errormessage);

    Date geterrordate();

    void seterrordate(Date errordate);

    String getsqlstatemenmt();

    void setsqlstatemenmt(String sqlstatemenmt);

    Float getabsamount();

    void setabsamount(Float absamount);

    String getterminal();

    void setterminal(String terminal);

    String getnetworkid();

    void setnetworkid(String networkid);

    String getacquirercurrency();

    void setacquirercurrency(String acquirercurrency);

    int getacquirercurrencyid();

    void setacquirercurrencyid(int acquirercurrencyid);

    Float getacquireramount();

    void setacquireramount(Float acquireramount);

    String getsettlementcurrency();

    void setsettlementcurrency(String settlementcurrency);

    int getsettlementcurrencyid();

    void setsettlementcurrencyid(int settlementcurrencyid);

    Float getsettlementamount();

    void setsettlementamount(Float settlementamount);

    String getauthorizationno();

    void setauthorizationno(String authorizationno);

    String getreferenceno();

    void setreferenceno(String referenceno);

    String getbank();

    void setbank(String bank);

    int gettransactionid();

    void settransactionid(int transactionid);

    String getlicencekey();

    void setlicencekey(String licencekey);

    String getnetworkcode();

    void setnetworkcode(String networkcode);

    String getprocesscode();

    void setprocesscode(String processcode);

    String getareacode();

    void setareacode(String areacode);

    String getreportid();

    void setreportid(String reportid);

    int getnetworkcomm();

    void setnetworkcomm(int networkcomm);

    String getnetworkcommflag();

    void setnetworkcommflag(String networkcommflag);

    String getfeelevel();

    void setfeelevel(String feelevel);
}
