/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Aly
 */
public interface TranSearchFieldDTOInter     extends Serializable{
 void setTerminal(String terminal);
 String getTerminal() ;
    String getAccountno();
 void setCurrencyoperator(String currencyoperator);
  String getCurrencyoperator() ;
    String getAccountnumoperator();

    String getAmountfrom();

    String getAmountoperator();

    String getAmountto();

    Integer getAqccurrencyid();

    String getAqramountfrom();

    String getAqramountoperator();

    String getAqramountto();

    Integer getAtmgroupid();

    String getAtmidauto();

    String getAtmidoperator();

    Integer getAtmnameauto();

    Integer getAtmnamedropdown();

    String getAuthno();

    Integer getBank();

    Boolean getBlacklistbool();

    String getCardno();

    String getCardnumoperator();

    String getCardnumsuffix();

    String getCol1();

    Integer getCol4();

    Integer getCurrencyid();

    Integer getDisputeid();

    Integer getDisputeuserid();

    String getErrorcodename();

    Integer getErrorid();

    Integer getFileid();

    String getFilename();

    Date getLoadingdatefrom();

    String getLoadingdateoperator();

    Date getLoadingdateto();

    String getMasterrecordid();

    String getMatchingtypeid();

    Integer getNetwork();

    String getRecordtypeid();

    String getRefno();

    String getResponcecodename();

    String getResponcecodenameoperator();

    Integer getResponceid();

    String getResponceidoperator();

    Integer getReversedby();

    Integer getReverseid();

    String getSeqfrom();

    String getSeqoperator();

    String getSeqto();

    String getSequencefrom();

    String getSequenceoperator();

    String getSequenceto();

    String getSettamountfrom();

    String getSettamountoperator();

    String getSettamountto();

    Integer getSettcurrencyid();

    Integer getSetteltbyid();

    Date getSetteltdatefrom();

    String getSetteltdateoperator();

    Date getSetteltdateto();

    Integer getSetteltstatus();

    String getSortby();

    String getSortcol();

    String getSortorder();

    String getSorttype();

    Date getTransdatefrom();

    String getTransdateoperator();

    Date getTransdateto();

    Integer getTypecodeid();

    String getTypecodename();

    Integer getValidationruleid();

    Integer getViewid();

    void setAccountno(String accountno);

    void setAccountnumoperator(String accountnumoperator);

    void setAmountfrom(String amountfrom);

    void setAmountoperator(String amountoperator);

    void setAmountto(String amountto);

    void setAqccurrencyid(Integer aqccurrencyid);

    void setAqramountfrom(String aqramountfrom);

    void setAqramountoperator(String aqramountoperator);

    void setAqramountto(String aqramountto);

    void setAtmgroupid(Integer atmgroupid);

    void setAtmidauto(String atmidauto);

    void setAtmidoperator(String atmidoperator);

    void setAtmnameauto(Integer atmnameauto);

    void setAtmnamedropdown(Integer atmnamedropdown);

    void setAuthno(String authno);

    void setBank(Integer bank);

    void setBlacklistbool(Boolean blacklistbool);

    void setCardno(String cardno);

    void setCardnumoperator(String cardnumoperator);

    void setCardnumsuffix(String cardnumsuffix);

    void setCol1(String col1);

    void setCol4(Integer col4);

    void setCurrencyid(Integer currencyid);

    void setDisputeid(Integer disputeid);

    void setDisputeuserid(Integer disputeuserid);

    void setErrorcodename(String errorcodename);

    void setErrorid(Integer errorid);

    void setFileid(Integer fileid);

    void setFilename(String filename);

    void setLoadingdatefrom(Date loadingdatefrom);

    void setLoadingdateoperator(String loadingdateoperator);

    void setLoadingdateto(Date loadingdateto);

    void setMasterrecordid(String masterrecordid);

    void setMatchingtypeid(String matchingtypeid);

    void setNetwork(Integer network);

    void setRecordtypeid(String recordtypeid);

    void setRefno(String refno);

    void setResponcecodename(String responcecodename);

    void setResponcecodenameoperator(String responcecodenameoperator);

    void setResponceid(Integer responceid);

    void setResponceidoperator(String responceidoperator);

    void setReversedby(Integer reversedby);

    void setReverseid(Integer reverseid);

    void setSeqfrom(String seqfrom);

    void setSeqoperator(String seqoperator);

    void setSeqto(String seqto);

    void setSequencefrom(String sequencefrom);

    void setSequenceoperator(String sequenceoperator);

    void setSequenceto(String sequenceto);

    void setSettamountfrom(String settamountfrom);

    void setSettamountoperator(String settamountoperator);

    void setSettamountto(String settamountto);

    void setSettcurrencyid(Integer settcurrencyid);

    void setSetteltbyid(Integer setteltbyid);

    void setSetteltdatefrom(Date setteltdatefrom);

    void setSetteltdateoperator(String setteltdateoperator);

    void setSetteltdateto(Date setteltdateto);

    void setSetteltstatus(Integer setteltstatus);

    void setSortby(String sortby);

    void setSortcol(String sortcol);

    void setSortorder(String sortorder);

    void setSorttype(String sorttype);

    void setTransdatefrom(Date transdatefrom);

    void setTransdateoperator(String transdateoperator);

    void setTransdateto(Date transdateto);

    void setTypecodeid(Integer typecodeid);

    void setTypecodename(String typecodename);

    void setValidationruleid(Integer validationruleid);

    void setViewid(Integer viewid);
    
}
