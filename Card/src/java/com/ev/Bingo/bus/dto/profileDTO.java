package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class profileDTO extends BaseDTO implements profileDTOInter, Serializable {

    private int profileid;

    public int getprofileid() {
        return profileid;
    }

    public void setprofileid(int profileid) {
        this.profileid = profileid;
    }
    private String profilename;

    public String getprofilename() {
        return profilename;
    }

    public void setprofilename(String profilename) {
        this.profilename = profilename;
    }
    private int restrected;

    public int getrestrected() {
        return restrected;
    }

    public void setrestrected(int restrected) {
        this.restrected = restrected;
    }
}
