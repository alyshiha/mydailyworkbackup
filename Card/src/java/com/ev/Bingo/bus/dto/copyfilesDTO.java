package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class copyfilesDTO extends BaseDTO implements copyfilesDTOInter, Serializable {

    private String sourcefolder;

    public String getsourcefolder() {
        return sourcefolder;
    }

    public void setsourcefolder(String sourcefolder) {
        this.sourcefolder = sourcefolder;
    }
    private String folder1;

    public String getfolder1() {
        return folder1;
    }

    public void setfolder1(String folder1) {
        this.folder1 = folder1;
    }
    private String folder2;

    public String getfolder2() {
        return folder2;
    }

    public void setfolder2(String folder2) {
        this.folder2 = folder2;
    }
    private String rownum;

    public String getrownum() {
        return rownum;
    }

    public void setrownum(String rownum) {
        this.rownum = rownum;
    }
}
