/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;

/**
 *
 * @author AlyShiha
 */
public class UserNetworkDTO extends BaseDTO implements UserNetworkDTOInter {

    private int userid;
    private int networkid;
    public String rowid;

    public String getRowid() {
        return rowid;
    }

    public void setRowid(String rowid) {
        this.rowid = rowid;
    }

    @Override
    public int getUserid() {
        return userid;
    }

    @Override
    public void setUserid(int userid) {
        this.userid = userid;
    }

    @Override
    public int getNetworkid() {
        return networkid;
    }

    @Override
    public void setNetworkid(int networkid) {
        this.networkid = networkid;
    }

}
