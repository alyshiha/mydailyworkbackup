package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface switchfiletemplateDTOInter extends Serializable {

    int getid();

    void setid(int id);

    String getname();

    void setname(String name);

    int getprocessingtype();

    void setprocessingtype(int processingtype);

    int getnumberoflines();

    void setnumberoflines(int numberoflines);

    String getloadingfolder();

    void setloadingfolder(String loadingfolder);

    int getstartingdatatype();

    void setstartingdatatype(int startingdatatype);

    String getstartingformat();

    void setstartingformat(String startingformat);

    int getstartingposition();

    void setstartingposition(int startingposition);

    String getbackupfolder();

    void setbackupfolder(String backupfolder);

    String getcopyfolder();

    void setcopyfolder(String copyfolder);

    int getignoredlines();

    void setignoredlines(int ignoredlines);

    int getstartinglength();

    void setstartinglength(int startinglength);

    String getdateseparator();

    void setdateseparator(String dateseparator);

    int getdateseparatorpos1();

    void setdateseparatorpos1(int dateseparatorpos1);

    int getdateseparatorpos2();

    void setdateseparatorpos2(int dateseparatorpos2);

    String getserverfolder();

    void setserverfolder(String serverfolder);

    int getheaderdatatype();

    void setheaderdatatype(int headerdatatype);

    String getheaderformat();

    void setheaderformat(String headerformat);

    String getheaderposition();

    void setheaderposition(String headerposition);

    int getheaderlength();

    void setheaderlength(int headerlength);

    String getheaderdateseparator();

    void setheaderdateseparator(String headerdateseparator);

    int getheaderdateseparatorpos1();

    void setheaderdateseparatorpos1(int headerdateseparatorpos1);

    int getheaderdateseparatorpos2();

    void setheaderdateseparatorpos2(int headerdateseparatorpos2);

    String getheaderstring();

    void setheaderstring(String headerstring);

    String getstartingvalue();

    void setstartingvalue(String startingvalue);

    int getactive();

    void setactive(int active);

    String getseparator();

    void setseparator(String separator);

    int gettagsendingdatatype();

    void settagsendingdatatype(int tagsendingdatatype);

    String gettagsendingformat();

    void settagsendingformat(String tagsendingformat);

    int gettagsendingposition();

    void settagsendingposition(int tagsendingposition);

    int gettagsendinglength();

    void settagsendinglength(int tagsendinglength);

    String gettagsendingvalue();

    void settagsendingvalue(String tagsendingvalue);

    int getheaderlines();

    void setheaderlines(int headerlines);

    int getmaxtaglines();

    void setmaxtaglines(int maxtaglines);
}
