package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class usersDTO extends BaseDTO implements usersDTOInter,Serializable,Cloneable  {

    private Boolean boollockedstate;
    private int lockedstate;
    private String showdashboard;
    private Boolean showexportB;
    private Boolean showcorrectineentryB;
    private int profileid;
    private String Profilename;
    private Boolean notificationB;
    private Boolean errorsB;
    private Boolean onlineuserB;
    private int userid;
    private String username;
    private String useraname;
    private int userlang;
    private int holidayenabled;
    private String logonname;
    private String userpassword;
    private int shiftid;
    private int emrholidayenabled;
    private int disputegraph;
    private int shownewatms;
    private int visible;
    private int seeunvisible;
    private int changeotheruserpassword;
    private int restrected;
    private int seerestrected;
    private int status;
    private int showpendingatms;
    private int shownotifications;
    private int showerrors;
    private int showonlineuser;
    private int showexport;
    private int showcorrectiveentry;

    @Override
    public Boolean getBoollockedstate() {
        return boollockedstate;
    }

    @Override
    public void setBoollockedstate(Boolean boollockedstate) {
        this.boollockedstate = boollockedstate;
    }

    @Override
    public int getLockedstate() {
        return lockedstate;
    }

    @Override
    public void setLockedstate(int lockedstate) {
        this.lockedstate = lockedstate;
    }

    @Override
    public String getShowdashboard() {
        return showdashboard;
    }

    @Override
    public void setShowdashboard(String showdashboard) {
        this.showdashboard = showdashboard;
    }

    @Override
    public Boolean getShowexportB() {
        return showexportB;
    }

    @Override
    public void setShowexportB(Boolean showexportB) {
        this.showexportB = showexportB;
    }

    @Override
    public Boolean getShowcorrectineentryB() {
        return showcorrectineentryB;
    }

    @Override
    public void setShowcorrectineentryB(Boolean showcorrectineentryB) {
        this.showcorrectineentryB = showcorrectineentryB;
    }

    @Override
    public int getProfileid() {
        return profileid;
    }

    @Override
    public void setProfileid(int profileid) {
        this.profileid = profileid;
    }

    @Override
    public String getProfilename() {
        return Profilename;
    }

    @Override
    public void setProfilename(String Profilename) {
        this.Profilename = Profilename;
    }

    @Override
    public Boolean getNotificationB() {
        return notificationB;
    }

    @Override
    public void setNotificationB(Boolean notificationB) {
        this.notificationB = notificationB;
    }

    @Override
    public Boolean getErrorsB() {
        return errorsB;
    }

    @Override
    public void setErrorsB(Boolean errorsB) {
        this.errorsB = errorsB;
    }

    @Override
    public Boolean getOnlineuserB() {
        return onlineuserB;
    }

    @Override
    public void setOnlineuserB(Boolean onlineuserB) {
        this.onlineuserB = onlineuserB;
    }

    @Override
    public int getUserid() {
        return userid;
    }

    @Override
    public void setUserid(int userid) {
        this.userid = userid;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getUseraname() {
        return useraname;
    }

    @Override
    public void setUseraname(String useraname) {
        this.useraname = useraname;
    }

    @Override
    public int getUserlang() {
        return userlang;
    }

    @Override
    public void setUserlang(int userlang) {
        this.userlang = userlang;
    }

    @Override
    public int getHolidayenabled() {
        return holidayenabled;
    }

    @Override
    public void setHolidayenabled(int holidayenabled) {
        this.holidayenabled = holidayenabled;
    }

    @Override
    public String getLogonname() {
        return logonname;
    }

    @Override
    public void setLogonname(String logonname) {
        this.logonname = logonname;
    }

    @Override
    public String getUserpassword() {
        return userpassword;
    }

    @Override
    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword;
    }

    @Override
    public int getShiftid() {
        return shiftid;
    }

    @Override
    public void setShiftid(int shiftid) {
        this.shiftid = shiftid;
    }

    @Override
    public int getEmrholidayenabled() {
        return emrholidayenabled;
    }

    @Override
    public void setEmrholidayenabled(int emrholidayenabled) {
        this.emrholidayenabled = emrholidayenabled;
    }

    @Override
    public int getDisputegraph() {
        return disputegraph;
    }

    @Override
    public void setDisputegraph(int disputegraph) {
        this.disputegraph = disputegraph;
    }

    @Override
    public int getShownewatms() {
        return shownewatms;
    }

    @Override
    public void setShownewatms(int shownewatms) {
        this.shownewatms = shownewatms;
    }

    @Override
    public int getVisible() {
        return visible;
    }

    @Override
    public void setVisible(int visible) {
        this.visible = visible;
    }

    @Override
    public int getSeeunvisible() {
        return seeunvisible;
    }

    @Override
    public void setSeeunvisible(int seeunvisible) {
        this.seeunvisible = seeunvisible;
    }

    @Override
    public int getChangeotheruserpassword() {
        return changeotheruserpassword;
    }

    @Override
    public void setChangeotheruserpassword(int changeotheruserpassword) {
        this.changeotheruserpassword = changeotheruserpassword;
    }

    @Override
    public int getRestrected() {
        return restrected;
    }

    @Override
    public void setRestrected(int restrected) {
        this.restrected = restrected;
    }

    @Override
    public int getSeerestrected() {
        return seerestrected;
    }

    @Override
    public void setSeerestrected(int seerestrected) {
        this.seerestrected = seerestrected;
    }

    @Override
    public int getStatus() {
        return status;
    }

    @Override
    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public int getShowpendingatms() {
        return showpendingatms;
    }

    @Override
    public void setShowpendingatms(int showpendingatms) {
        this.showpendingatms = showpendingatms;
    }

    @Override
    public int getShownotifications() {
        return shownotifications;
    }

    @Override
    public void setShownotifications(int shownotifications) {
        this.shownotifications = shownotifications;
    }

    @Override
    public int getShowerrors() {
        return showerrors;
    }

    @Override
    public void setShowerrors(int showerrors) {
        this.showerrors = showerrors;
    }

    @Override
    public int getShowonlineuser() {
        return showonlineuser;
    }

    @Override
    public void setShowonlineuser(int showonlineuser) {
        this.showonlineuser = showonlineuser;
    }

    @Override
    public int getShowexport() {
        return showexport;
    }

    @Override
    public void setShowexport(int showexport) {
        this.showexport = showexport;
    }

    @Override
    public int getShowcorrectiveentry() {
        return showcorrectiveentry;
    }

    @Override
    public void setShowcorrectiveentry(int showcorrectiveentry) {
        this.showcorrectiveentry = showcorrectiveentry;
    }

}
