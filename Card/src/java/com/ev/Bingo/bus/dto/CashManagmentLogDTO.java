/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author AlyShiha
 */
public class CashManagmentLogDTO implements CashManagmentLogDTOInter {

    public CashManagmentLogDTO(Integer id, Date actionDate, Integer userId,  String ipAddress, String action) {
        this.id = id;
        this.actionDate = actionDate;
        this.userId = userId;
        
        this.ipAddress = ipAddress;
        this.action = action;
    }

    public CashManagmentLogDTO() {
    }

    private Integer id;
    private Date actionDate;
    private Integer userId;
    private String userName;
    private String ipAddress;
    private String action;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public Date getActionDate() {
        return actionDate;
    }

    @Override
    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    @Override
    public Integer getUserId() {
        return userId;
    }

    @Override
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String getIpAddress() {
        return ipAddress;
    }

    @Override
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }
    
}
