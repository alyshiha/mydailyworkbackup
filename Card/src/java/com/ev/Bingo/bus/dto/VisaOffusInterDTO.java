/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.math.BigDecimal;

/**
 *
 * @author shi7a
 */
public class VisaOffusInterDTO implements VisaOffusInterDTOInter {
      private Integer  count;
private BigDecimal totalamount;
      private BigDecimal income;
    private String type;
private String terminal;

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }
    @Override
    public Integer getCount() {
        return count;
    }

    @Override
    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public BigDecimal getTotalamount() {
        return totalamount;
    }

    @Override
    public void setTotalamount(BigDecimal totalamount) {
        this.totalamount = totalamount;
    }

    @Override
    public BigDecimal getIncome() {
        return income;
    }

    @Override
    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }
    
}
