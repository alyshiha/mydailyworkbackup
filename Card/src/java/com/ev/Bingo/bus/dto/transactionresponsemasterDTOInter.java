package com.ev.Bingo.bus.dto;

import java.io.Serializable;

public interface transactionresponsemasterDTOInter extends Serializable {

    int getid();

    void setid(int id);

    String getname();

    void setname(String name);
}
