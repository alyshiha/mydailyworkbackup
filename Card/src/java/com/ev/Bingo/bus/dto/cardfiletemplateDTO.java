package com.ev.Bingo.bus.dto;
import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;
public class cardfiletemplateDTO extends BaseDTO implements cardfiletemplateDTOInter  {
private int id;
    @Override
    public int getid(){
return id;
}
    @Override
    public void setid(int id) {
this.id = id;
}
private String name;
    @Override
    public String getname(){
return name;
}
    @Override
    public void setname(String name) {
this.name = name;
}
private int processingtype;
    @Override
    public int getprocessingtype(){
return processingtype;
}
    @Override
    public void setprocessingtype(int processingtype) {
this.processingtype = processingtype;
}
private String numberoflines;
    @Override
    public String getnumberoflines(){
return numberoflines;
}
    @Override
    public void setnumberoflines(String numberoflines) {
this.numberoflines = numberoflines;
}
private String loadingfolder;
    @Override
    public String getloadingfolder(){
return loadingfolder;
}
    @Override
    public void setloadingfolder(String loadingfolder) {
this.loadingfolder = loadingfolder;
}
private int startingdatatype;
    @Override
    public int getstartingdatatype(){
return startingdatatype;
}
    @Override
    public void setstartingdatatype(int startingdatatype) {
this.startingdatatype = startingdatatype;
}
private String startingformat;
    @Override
    public String getstartingformat(){
return startingformat;
}
    @Override
    public void setstartingformat(String startingformat) {
this.startingformat = startingformat;
}
private String startingposition;
    @Override
    public String getstartingposition(){
return startingposition;
}
    @Override
    public void setstartingposition(String startingposition) {
this.startingposition = startingposition;
}
private String backupfolder;
    @Override
    public String getbackupfolder(){
return backupfolder;
}
    @Override
    public void setbackupfolder(String backupfolder) {
this.backupfolder = backupfolder;
}
private String copyfolder;
    @Override
    public String getcopyfolder(){
return copyfolder;
}
    @Override
    public void setcopyfolder(String copyfolder) {
this.copyfolder = copyfolder;
}
private String ignoredlines;
    @Override
    public String getignoredlines(){
return ignoredlines;
}
    @Override
    public void setignoredlines(String ignoredlines) {
this.ignoredlines = ignoredlines;
}
private String startinglength;
    @Override
    public String getstartinglength(){
return startinglength;
}
    @Override
    public void setstartinglength(String startinglength) {
this.startinglength = startinglength;
}
private String dateseparator;
    @Override
    public String getdateseparator(){
return dateseparator;
}
    @Override
    public void setdateseparator(String dateseparator) {
this.dateseparator = dateseparator;
}
private String dateseparatorpos1;
    @Override
    public String getdateseparatorpos1(){
return dateseparatorpos1;
}
    @Override
    public void setdateseparatorpos1(String dateseparatorpos1) {
this.dateseparatorpos1 = dateseparatorpos1;
}
private String dateseparatorpos2;
    @Override
    public String getdateseparatorpos2(){
return dateseparatorpos2;
}
    @Override
    public void setdateseparatorpos2(String dateseparatorpos2) {
this.dateseparatorpos2 = dateseparatorpos2;
}
private String serverfolder;
    @Override
    public String getserverfolder(){
return serverfolder;
}
    @Override
    public void setserverfolder(String serverfolder) {
this.serverfolder = serverfolder;
}
private int headerdatatype;
    @Override
    public int getheaderdatatype(){
return headerdatatype;
}
    @Override
    public void setheaderdatatype(int headerdatatype) {
this.headerdatatype = headerdatatype;
}
private String headerformat;
    @Override
    public String getheaderformat(){
return headerformat;
}
    @Override
    public void setheaderformat(String headerformat) {
this.headerformat = headerformat;
}
private String headerposition;
    @Override
    public String getheaderposition(){
return headerposition;
}
    @Override
    public void setheaderposition(String headerposition) {
this.headerposition = headerposition;
}
private String headerlength;
    @Override
    public String getheaderlength(){
return headerlength;
}
    @Override
    public void setheaderlength(String headerlength) {
this.headerlength = headerlength;
}
private String headerdateseparator;
    @Override
    public String getheaderdateseparator(){
return headerdateseparator;
}
    @Override
    public void setheaderdateseparator(String headerdateseparator) {
this.headerdateseparator = headerdateseparator;
}
private String headerdateseparatorpos1;
    @Override
    public String getheaderdateseparatorpos1(){
return headerdateseparatorpos1;
}
    @Override
    public void setheaderdateseparatorpos1(String headerdateseparatorpos1) {
this.headerdateseparatorpos1 = headerdateseparatorpos1;
}
private String headerdateseparatorpos2;
    @Override
    public String getheaderdateseparatorpos2(){
return headerdateseparatorpos2;
}
    @Override
    public void setheaderdateseparatorpos2(String headerdateseparatorpos2) {
this.headerdateseparatorpos2 = headerdateseparatorpos2;
}
private String headerstring;
    @Override
    public String getheaderstring(){
return headerstring;
}
    @Override
    public void setheaderstring(String headerstring) {
this.headerstring = headerstring;
}
private String startingvalue;
    @Override
    public String getstartingvalue(){
return startingvalue;
}
    @Override
    public void setstartingvalue(String startingvalue) {
this.startingvalue = startingvalue;
}
private int active;
    @Override
    public int getactive(){
return active;
}
    @Override
    public void setactive(int active) {
this.active = active;
}
private Boolean activeB;
    @Override
    public Boolean getactiveB(){
return activeB;
}
    @Override
    public void setactiveB(Boolean activeB) {
this.activeB = activeB;
}
private String separator;
    @Override
    public String getseparator(){
return separator;
}
    @Override
    public void setseparator(String separator) {
this.separator = separator;
}
private int tagsendingdatatype;
    @Override
    public int gettagsendingdatatype(){
return tagsendingdatatype;
}
    @Override
    public void settagsendingdatatype(int tagsendingdatatype) {
this.tagsendingdatatype = tagsendingdatatype;
}

private String tagsendfor;

    public String getTagsendfor() {
        return tagsendfor;
    }

    public void setTagsendfor(String tagsendfor) {
        this.tagsendfor = tagsendfor;
    }

 

private String tagsendingposition;
    @Override
    public String gettagsendingposition(){
return tagsendingposition;
}
    @Override
    public void settagsendingposition(String tagsendingposition) {
this.tagsendingposition = tagsendingposition;
}
private String tagsendinglength;
    @Override
    public String gettagsendinglength(){
return tagsendinglength;
}
    @Override
    public void settagsendinglength(String tagsendinglength) {
this.tagsendinglength = tagsendinglength;
}
private String tagsendingvalue;
    @Override
    public String gettagsendingvalue(){
return tagsendingvalue;
}
    @Override
    public void settagsendingvalue(String tagsendingvalue) {
this.tagsendingvalue = tagsendingvalue;
}
private int networkid;
    @Override
    public int getnetworkid(){
return networkid;
}
    @Override
    public void setnetworkid(int networkid) {
this.networkid = networkid;
}
private String headerlines;
    @Override
    public String getheaderlines(){
return headerlines;
}
    @Override
    public void setheaderlines(String headerlines) {
this.headerlines = headerlines;
}
private String maxtaglines;
    @Override
    public String getmaxtaglines(){
return maxtaglines;
}
    @Override
    public void setmaxtaglines(String maxtaglines) {
this.maxtaglines = maxtaglines;
}
}