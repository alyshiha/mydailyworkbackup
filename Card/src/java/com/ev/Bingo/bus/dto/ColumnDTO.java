/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
 public  class ColumnDTO extends BaseDTO implements Serializable, ColumnDTOInter {

    private int id;
    private String header;
    private String property;
    private String dbName,duplicateno;
    private int width;
    private String expression;
    private String clazzType,datatype;
private boolean  active,displayinloading,displayinmatching,displayintransaction;

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public boolean getDisplayinloading() {
        return displayinloading;
    }

    public void setDisplayinloading(boolean displayinloading) {
        this.displayinloading = displayinloading;
    }

    public boolean getDisplayinmatching() {
        return displayinmatching;
    }

    public void setDisplayinmatching(boolean displayinmatching) {
        this.displayinmatching = displayinmatching;
    }

    public boolean getDisplayintransaction() {
        return displayintransaction;
    }

    public void setDisplayintransaction(boolean displayintransaction) {
        this.displayintransaction = displayintransaction;
    }

    public String getDuplicate_no() {
        return duplicateno;
    }

    public void setDuplicate_no(String duplicate_no) {
        this.duplicateno = duplicate_no;
    }

   protected ColumnDTO() {
    }

    public String getClazzType() {
        return clazzType;
    }

    public void setClazzType(String clazzType) {
        this.clazzType = clazzType;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }
    
}
