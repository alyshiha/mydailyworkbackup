package com.ev.Bingo.bus.dto;

import java.io.Serializable;

public interface itembingoDTOInter extends Serializable {

    int getitemid();

    void setitemid(int itemid);

    String getitemname();

    void setitemname(String itemname);
}
