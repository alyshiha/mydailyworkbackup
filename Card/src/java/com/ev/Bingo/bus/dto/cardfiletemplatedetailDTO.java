package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class cardfiletemplatedetailDTO extends BaseDTO implements cardfiletemplatedetailDTOInter {

    private int id;

    @Override
    public int getid() {
        return id;
    }

    @Override
    public void setid(int id) {
        this.id = id;
    }
    private int template;

    @Override
    public int gettemplate() {
        return template;
    }

    @Override
    public void settemplate(int template) {
        this.template = template;
    }
    private int columnid;

    @Override
    public int getcolumnid() {
        return columnid;
    }

    @Override
    public void setcolumnid(int columnid) {
        this.columnid = columnid;
    }
    private String position;

    @Override
    public String getposition() {
        return position;
    }

    @Override
    public void setposition(String position) {
        this.position = position;
    }
    private String format;

    @Override
    public String getformat() {
        return format;
    }

    @Override
    public void setformat(String format) {
        this.format = format;
    }
    private String format2;

    @Override
    public String getformat2() {
        return format2;
    }

    @Override
    public void setformat2(String format2) {
        this.format2 = format2;
    }
    private int datatype;

    @Override
    public int getdatatype() {
        return datatype;
    }

    @Override
    public void setdatatype(int datatype) {
        this.datatype = datatype;
    }
    private String length;

    @Override
    public String getlength() {
        return length;
    }

    @Override
    public void setlength(String length) {
        this.length = length;
    }
    private String linenumber;

    @Override
    public String getlinenumber() {
        return linenumber;
    }

    @Override
    public void setlinenumber(String linenumber) {
        this.linenumber = linenumber;
    }
    private String loadwhenmapped;

    @Override
    public String getloadwhenmapped() {
        return loadwhenmapped;
    }

    @Override
    public void setloadwhenmapped(String loadwhenmapped) {
        this.loadwhenmapped = loadwhenmapped;
    }
    private String lengthdir;

    @Override
    public String getlengthdir() {
        return lengthdir;
    }

    @Override
    public void setlengthdir(String lengthdir) {
        this.lengthdir = lengthdir;
    }
    private String startingposition;

    @Override
    public String getstartingposition() {
        return startingposition;
    }

    @Override
    public void setstartingposition(String startingposition) {
        this.startingposition = startingposition;
    }
    private String tagstring;

    @Override
    public String gettagstring() {
        return tagstring;
    }

    @Override
    public void settagstring(String tagstring) {
        this.tagstring = tagstring;
    }

    private Boolean mandatoryboolean;

    private Boolean negativeamountflagboolean;

    @Override
    public Boolean getMandatoryboolean() {
        return mandatoryboolean;
    }

    @Override
    public void setMandatoryboolean(Boolean mandatoryboolean) {
        this.mandatoryboolean = mandatoryboolean;
    }

    @Override
    public Boolean getNegativeamountflagboolean() {
        return negativeamountflagboolean;
    }

    @Override
    public void setNegativeamountflagboolean(Boolean negativeamountflagboolean) {
        this.negativeamountflagboolean = negativeamountflagboolean;
    }

    private Boolean decimalposboolean;

    @Override
    public Boolean getDecimalposboolean() {
        return decimalposboolean;
    }

    @Override
    public void setDecimalposboolean(Boolean decimalposboolean) {
        this.decimalposboolean = decimalposboolean;
    }
   
    
    private int mandatory;

    @Override
    public int getmandatory() {
        return mandatory;
    }

    @Override
    public void setmandatory(int mandatory) {
        this.mandatory = mandatory;
    }
    private int negativeamountflag;

    @Override
    public int getnegativeamountflag() {
        return negativeamountflag;
    }

    @Override
    public void setnegativeamountflag(int negativeamountflag) {
        this.negativeamountflag = negativeamountflag;
    }
    private String adddecimal;

    @Override
    public String getadddecimal() {
        return adddecimal;
    }

    @Override
    public void setadddecimal(String adddecimal) {
        this.adddecimal = adddecimal;
    }
    private int decimalpos;

    @Override
    public int getdecimalpos() {
        return decimalpos;
    }

    @Override
    public void setdecimalpos(int decimalpos) {
        this.decimalpos = decimalpos;
    }
}
