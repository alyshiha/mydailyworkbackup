/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public interface NetworksDTOInter_1 {

    Integer getId();

    String getName();

    void setId(Integer id);

    void setName(String name);
    
}
