/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public interface ATMMACHINECASHDTOINTER {

    Integer getId();

    String getAtmname();

    void setId(Integer id);

    void setAtmname(String atmname);

    String getAtmid();

    void setAtmid(String atmid);
}
