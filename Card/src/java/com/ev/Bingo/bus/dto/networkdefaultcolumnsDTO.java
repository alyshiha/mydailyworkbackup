package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.util.Date;
import java.io.Serializable;

public class networkdefaultcolumnsDTO extends BaseDTO implements networkdefaultcolumnsDTOInter, Serializable {

    private Boolean bselected;

    public Boolean getbselected() {
        return bselected;
    }

    public void setbselected(Boolean bselected) {
        this.bselected = bselected;
    }
    private int orderby;

    public int getOrderby() {
        return orderby;
    }

    public void setOrderby(int orderby) {
        this.orderby = orderby;
    }

    private int selected;

    public int getselected() {
        return selected;
    }

    public void setselected(int selected) {
        this.selected = selected;
    }
    private int networkid;

    public int getnetworkid() {
        return networkid;
    }

    public void setnetworkid(int networkid) {
        this.networkid = networkid;
    }
    private String rowid;

    public String getrowid() {
        return rowid;
    }

    public void setrowid(String rowid) {
        this.rowid = rowid;
    }
    private int columnid;

    public int getcolumnid() {
        return columnid;
    }

    public void setcolumnid(int columnid) {
        this.columnid = columnid;
    }
}
