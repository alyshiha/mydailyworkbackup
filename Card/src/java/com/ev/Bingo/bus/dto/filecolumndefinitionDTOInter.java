package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface filecolumndefinitionDTOInter extends Serializable {

    void setselected(int selected);

    int getselected();

    String getColumnvariable();

    void setExpression(String expression);

    String getExpression();

    void setClazzType(String clazzType);

    String getClazzType();

    void setColumnvariable(String columnvariable);

    int getid();

    void setid(int id);

    String getname();

    void setname(String name);

    String getcolumnname();

    void setcolumnname(String columnname);

    int getdatatype();

    void setdatatype(int datatype);

    int getprint();

    void setprint(int print);

    String getprintformat();

    void setprintformat(String printformat);

    int getprintorder();

    void setprintorder(int printorder);
}
