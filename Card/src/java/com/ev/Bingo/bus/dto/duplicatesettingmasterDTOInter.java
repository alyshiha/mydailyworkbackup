package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface duplicatesettingmasterDTOInter extends Serializable {

    int getid();

    void setid(int id);

    int getrectype();

    void setrectype(int rectype);

    int getnetworkid();

    void setnetworkid(int networkid);
}
