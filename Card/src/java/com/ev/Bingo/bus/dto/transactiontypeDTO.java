package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class transactiontypeDTO extends BaseDTO implements transactiontypeDTOInter, Serializable {

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private String name;

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }
    private String description;

    public String getdescription() {
        return description;
    }

    public void setdescription(String description) {
        this.description = description;
    }
    private int typecategory;

    public int gettypecategory() {
        return typecategory;
    }

    public void settypecategory(int typecategory) {
        this.typecategory = typecategory;
    }
    private int typemaster;

    public int gettypemaster() {
        return typemaster;
    }

    public void settypemaster(int typemaster) {
        this.typemaster = typemaster;
    }
    private boolean reverseflag;

    public boolean getreverseflag() {
        return reverseflag;
    }

    public void setreverseflag(boolean reverseflag) {
        this.reverseflag = reverseflag;
    }
    private boolean reversetype;

    public boolean getreversetype() {
        return reversetype;
    }

    public void setreversetype(boolean reversetype) {
        this.reversetype = reversetype;
    }
}
