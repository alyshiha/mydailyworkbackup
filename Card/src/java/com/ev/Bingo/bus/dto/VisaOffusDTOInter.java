/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.math.BigDecimal;

/**
 *
 * @author shi7a
 */
public interface VisaOffusDTOInter {
String getTerminal();
void setTerminal(String terminal);
    Integer getCount();

    BigDecimal getRelatedincome();

    String getType();

    void setCount(Integer count);

    void setRelatedincome(BigDecimal relatedincome);

    void setType(String type);
    
}
