/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TransSearchOperatorsDTOInter {

    List<String> getRulesValue1();

    List<String> getRulesValue2();

    List<String> getRulesValue3();

    List<String> getRulesValue4();

    void setRulesValue1(List<String> rulesValue1);

    void setRulesValue2(List<String> rulesValue2);

    void setRulesValue3(List<String> rulesValue3);

    void setRulesValue4(List<String> rulesValue4);

}
