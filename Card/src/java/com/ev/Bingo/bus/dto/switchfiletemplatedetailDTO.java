package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;

public class switchfiletemplatedetailDTO extends BaseDTO implements switchfiletemplatedetailDTOInter, Serializable {

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }
    private int template;

    public int gettemplate() {
        return template;
    }

    public void settemplate(int template) {
        this.template = template;
    }
    private int columnid;

    public int getcolumnid() {
        return columnid;
    }

    public void setcolumnid(int columnid) {
        this.columnid = columnid;
    }
    private int position;

    public int getposition() {
        return position;
    }

    public void setposition(int position) {
        this.position = position;
    }
    private int linenumber;

    public int getlinenumber() {
        return linenumber;
    }

    public void setlinenumber(int linenumber) {
        this.linenumber = linenumber;
    }
    private String format;

    public String getformat() {
        return format;
    }

    public void setformat(String format) {
        this.format = format;
    }
    private String format2;

    public String getformat2() {
        return format2;
    }

    public void setformat2(String format2) {
        this.format2 = format2;
    }
    private int datatype;

    public int getdatatype() {
        return datatype;
    }

    public void setdatatype(int datatype) {
        this.datatype = datatype;
    }
    private int length;

    public int getlength() {
        return length;
    }

    public void setlength(int length) {
        this.length = length;
    }
    private int loadwhenmapped;

    public int getloadwhenmapped() {
        return loadwhenmapped;
    }

    public void setloadwhenmapped(int loadwhenmapped) {
        this.loadwhenmapped = loadwhenmapped;
    }
    private int lengthdir;

    public int getlengthdir() {
        return lengthdir;
    }

    public void setlengthdir(int lengthdir) {
        this.lengthdir = lengthdir;
    }
    private int startingposition;

    public int getstartingposition() {
        return startingposition;
    }

    public void setstartingposition(int startingposition) {
        this.startingposition = startingposition;
    }
    private String tagstring;

    public String gettagstring() {
        return tagstring;
    }

    public void settagstring(String tagstring) {
        this.tagstring = tagstring;
    }
    private int mandatory;

    public int getmandatory() {
        return mandatory;
    }

    public void setmandatory(int mandatory) {
        this.mandatory = mandatory;
    }
    private int negativeamountflag;

    public int getnegativeamountflag() {
        return negativeamountflag;
    }

    public void setnegativeamountflag(int negativeamountflag) {
        this.negativeamountflag = negativeamountflag;
    }
    private int adddecimal;

    public int getadddecimal() {
        return adddecimal;
    }

    public void setadddecimal(int adddecimal) {
        this.adddecimal = adddecimal;
    }
    private int decimalpos;

    public int getdecimalpos() {
        return decimalpos;
    }

    public void setdecimalpos(int decimalpos) {
        this.decimalpos = decimalpos;
    }
}
