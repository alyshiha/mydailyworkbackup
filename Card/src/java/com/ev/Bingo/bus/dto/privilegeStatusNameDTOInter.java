/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public interface privilegeStatusNameDTOInter {

    int getPrev_id();
    String getSymbol();
    void setSymbol(String symbol);

    String getPrev_name();

    void setPrev_id(int prev_id);

    void setPrev_name(String prev_name);
    
}
