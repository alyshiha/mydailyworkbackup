/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author Aly
 */
public interface BankCodeDTOInter {

    String getBankname();

    int getId();

    void setBankname(String bankname);

    void setId(int id);

}
