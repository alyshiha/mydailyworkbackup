package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class filecolumndefinitionDTO extends BaseDTO implements filecolumndefinitionDTOInter, Serializable {

    private int selected;

    public int getselected() {
        return selected;
    }

    public void setselected(int selected) {
        this.selected = selected;
    }
    private String expression;
    private String clazzType;

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getClazzType() {
        return clazzType;
    }

    public void setClazzType(String clazzType) {
        this.clazzType = clazzType;
    }

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }

    private String columnvariable;

    public String getColumnvariable() {
        return columnvariable;
    }

    public void setColumnvariable(String columnvariable) {
        this.columnvariable = columnvariable;
    }
    private String name;

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }
    private String columnname;

    public String getcolumnname() {
        return columnname;
    }

    public void setcolumnname(String columnname) {
        this.columnname = columnname;
    }
    private int datatype;

    public int getdatatype() {
        return datatype;
    }

    public void setdatatype(int datatype) {
        this.datatype = datatype;
    }
    private int print;

    public int getprint() {
        return print;
    }

    public void setprint(int print) {
        this.print = print;
    }
    private String printformat;

    public String getprintformat() {
        return printformat;
    }

    public void setprintformat(String printformat) {
        this.printformat = printformat;
    }
    private int printorder;

    public int getprintorder() {
        return printorder;
    }

    public void setprintorder(int printorder) {
        this.printorder = printorder;
    }
}
