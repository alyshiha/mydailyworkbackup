package com.ev.Bingo.bus.dto;

import java.io.Serializable;

public interface currencyDTOInter extends Serializable {

    int getid();

    void setid(int id);

    String getabbreviation();

    void setabbreviation(String abbreviation);

    int getmasterid();

    void setmasterid(int masterid);

    int getdefaultcurrency();

    void setdefaultcurrency(int defaultcurrency);

    boolean isDefaultcurrencyB();

    void setDefaultcurrencyB(boolean defaultcurrencyB);
}
