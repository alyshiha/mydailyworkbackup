/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author AlyShiha
 */
public class ATMMACHINECASHDTO implements ATMMACHINECASHDTOINTER {
    private Integer id;
    private String atmname;
    private String atmid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAtmname() {
        return atmname;
    }

    public void setAtmname(String atmname) {
        this.atmname = atmname;
    }

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }
      
}
