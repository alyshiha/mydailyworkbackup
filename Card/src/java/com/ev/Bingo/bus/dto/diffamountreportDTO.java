package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;

public class diffamountreportDTO extends BaseDTO implements diffamountreportDTOInter, Serializable {

    private Date transdate;

    public Date gettransdate() {
        return transdate;
    }

    public void settransdate(Date transdate) {
        this.transdate = transdate;
    }
    private String cardnumber;

    public String getcardnumber() {
        return cardnumber;
    }

    public void setcardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }
    private int network;

    public int getnetwork() {
        return network;
    }

    public void setnetwork(int network) {
        this.network = network;
    }
    private int switchvalue;

    public int getSwitchvalue() {
        return switchvalue;
    }

    public void setSwitchvalue(int switchvalue) {
        this.switchvalue = switchvalue;
    }

    private int networkdiff;

    public int getnetworkdiff() {
        return networkdiff;
    }

    public void setnetworkdiff(int networkdiff) {
        this.networkdiff = networkdiff;
    }
    private int switchdiff;

    public int getswitchdiff() {
        return switchdiff;
    }

    public void setswitchdiff(int switchdiff) {
        this.switchdiff = switchdiff;
    }
    private int reortid;

    public int getreortid() {
        return reortid;
    }

    public void setreortid(int reortid) {
        this.reortid = reortid;
    }
}
