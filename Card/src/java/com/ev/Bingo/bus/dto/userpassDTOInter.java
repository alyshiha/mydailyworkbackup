package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface userpassDTOInter extends Serializable {

    int getuserid();

    void setuserid(int userid);

    String getpass();

    void setpass(String pass);

    Date getchangedate();

    void setchangedate(Date changedate);
}
