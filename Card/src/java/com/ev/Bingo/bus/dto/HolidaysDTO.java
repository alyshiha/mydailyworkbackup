/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author shi7a
 */
public class HolidaysDTO implements HolidaysDTOInter {
    private int holidayid;
    private Date holidaydate;
    private String holidayname;
    private String holidaytype;

    public HolidaysDTO() {
    }

    public HolidaysDTO(int holidayid, Date holidaydate, String holidayname, String holidaytype) {
        this.holidayid = holidayid;
        this.holidaydate = holidaydate;
        this.holidayname = holidayname;
        this.holidaytype = holidaytype;
    }

    @Override
    public int getHolidayid() {
        return holidayid;
    }

    @Override
    public void setHolidayid(int holidayid) {
        this.holidayid = holidayid;
    }

    @Override
    public Date getHolidaydate() {
        return holidaydate;
    }

    @Override
    public void setHolidaydate(Date holidaydate) {
        this.holidaydate = holidaydate;
    }

    @Override
    public String getHolidayname() {
        return holidayname;
    }

    @Override
    public void setHolidayname(String holidayname) {
        this.holidayname = holidayname;
    }

    @Override
    public String getHolidaytype() {
        return holidaytype;
    }

    @Override
    public void setHolidaytype(String holidaytype) {
        this.holidaytype = holidaytype;
    }
    
}
