package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import java.io.Serializable;

public class blockreasonsDTO extends BaseDTO implements blockreasonsDTOInter, Serializable {

    private int reasonid;

    public int getreasonid() {
        return reasonid;
    }

    public void setreasonid(int reasonid) {
        this.reasonid = reasonid;
    }
    private String reasonname;

    public String getreasonname() {
        return reasonname;
    }

    public void setreasonname(String reasonname) {
        this.reasonname = reasonname;
    }
    private String reasonaname;

    public String getreasonaname() {
        return reasonaname;
    }

    public void setreasonaname(String reasonaname) {
        this.reasonaname = reasonaname;
    }
}
