package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;

public class duplicatesettingdetailDTO extends BaseDTO implements duplicatesettingdetailDTOInter, Serializable {

    private int id;

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }

    private String rowid;

    public String getRowid() {
        return rowid;
    }

    public void setRowid(String rowid) {
        this.rowid = rowid;
    }

    private int columnid;

    public int getcolumnid() {
        return columnid;
    }

    public void setcolumnid(int columnid) {
        this.columnid = columnid;
    }
}
