/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public interface ColumnDTOInter extends Serializable {

    void setDatatype(String datatype);

    String getDatatype();

    void setActive(boolean active);

    boolean getActive();

    void setDisplayinloading(boolean displayinloading);

    boolean getDisplayinloading();

    boolean getDisplayinmatching();

    void setDisplayinmatching(boolean displayinmatching);

    boolean getDisplayintransaction();

    void setDisplayintransaction(boolean displayintransaction);

    String getDuplicate_no();

    void setDuplicate_no(String duplicate_no);

    String getExpression();

    void setExpression(String expression);

    String getClazzType();

    void setClazzType(String clazzType);

    String getHeader();

    String getProperty();

    String getDbName();

    int getId();

    int getWidth();

    void setDbName(String dbName);

    void setId(int id);

    void setWidth(int width);

    void setHeader(String header);

    void setProperty(String property);
}
