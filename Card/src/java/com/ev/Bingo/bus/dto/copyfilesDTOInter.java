package com.ev.Bingo.bus.dto;

import java.io.Serializable;

public interface copyfilesDTOInter extends Serializable {

    String getsourcefolder();

    void setsourcefolder(String sourcefolder);

    String getfolder1();

    void setfolder1(String folder1);

    String getfolder2();

    void setfolder2(String folder2);

    String getrownum();

    void setrownum(String rownum);
}
