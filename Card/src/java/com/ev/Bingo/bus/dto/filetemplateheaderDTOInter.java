package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface filetemplateheaderDTOInter extends Serializable {

    int getid();

    void setid(int id);

    int gettemplate();

    void settemplate(int template);

    int getcolumnid();

    void setcolumnid(int columnid);

    String getposition();

    void setposition(String position);

    String getformat();

    void setformat(String format);

    String getformat2();

    void setformat2(String format2);

    int getdatatype();

    void setdatatype(int datatype);

    String getlength();

    void setlength(String length);

    String getlineno();

    void setlineno(String lineno);
}
