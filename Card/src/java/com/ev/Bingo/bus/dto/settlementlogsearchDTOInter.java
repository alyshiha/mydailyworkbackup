/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface settlementlogsearchDTOInter {

    usersDTOInter getUserloged();

    void setUserloged(usersDTOInter userloged);

    String getAtmid();

    String getCardno();

    Integer getSetteledid();

    Date getTransactiondatefrom();

    Date getTransactiondateto();

    Integer getUserid();

    void setAtmid(String atmid);

    void setCardno(String cardno);

    void setSetteledid(Integer setteledid);

    void setTransactiondatefrom(Date transactiondatefrom);

    void setTransactiondateto(Date transactiondateto);

    void setUserid(Integer userid);
}
