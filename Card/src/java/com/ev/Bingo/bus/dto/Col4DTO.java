
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author Aly
 */
public class Col4DTO implements Col4DTOInter  {

    String name;
    int id;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }



    @Override
    public int getId() {
        return id;
    }

    
    @Override
    public void setId(int id) {
        this.id = id;
    }

}
