/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author Administrator
 */
public class UserActivityDTO implements UserActivityDTOInter {

    private String username;
    private String type;
    private int transcount;

    public int getTranscount() {
        return transcount;
    }

    public void setTranscount(int transcount) {
        this.transcount = transcount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
