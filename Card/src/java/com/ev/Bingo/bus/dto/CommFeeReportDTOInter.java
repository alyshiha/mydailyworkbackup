/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

/**
 *
 * @author Aly
 */
public interface CommFeeReportDTOInter {

    Float getComm();

    int getCommtranscount();

    Float getFee();

    int getFeetranscount();

    String getNetwork();

    String getPin();

    void setComm(Float comm);

    void setCommtranscount(int commtranscount);

    void setFee(Float fee);

    void setFeetranscount(int feetranscount);

    void setNetwork(String network);

    void setPin(String pin);
    
}
