package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface disputesDTOInter extends Serializable {

    void setCorrectiveamount(String correctiveamount);

    String getCorrectiveamount();

    int getresponsecodeid();

    void setRecordname(String recordname);

    String getRecordname();

    void setmatchkey(String matchkey);

    String getmatchkey();

    void setresponsecodeid(int responsecodeid);

    Date gettransactiondate();

    void settransactiondate(Date transactiondate);

    String gettransactionsequence();

    void settransactionsequence(String transactionsequence);

    String getcardno();

    void setcardno(String cardno);

    Float getamount();

    public String getrowid();

    int getRownum();

    String getcardnos();

    void setcardnos(String cardnos);

    public void setRownum(int rownum);

    public void setrowid(String rowid);

    void setamount(Float amount);

    Date getsettlementdate();

    void setsettlementdate(Date settlementdate);

    String getcustomeraccountnumber();

    void setcustomeraccountnumber(String customeraccountnumber);

    String gettransactionsequenceorderby();

    void settransactionsequenceorderby(String transactionsequenceorderby);

    Date gettransactiontime();

    void settransactiontime(Date transactiontime);

    int getmatchingtype();

    void setmatchingtype(int matchingtype);

    int getrecordtype();

    void setrecordtype(int recordtype);

    int getdisputekey();

    void setdisputekey(int disputekey);

    String getdisputereason();

    void setdisputereason(String disputereason);

    int getsettledflag();

    void setsettledflag(int settledflag);

    Date getsettleddate();

    void setsettleddate(Date settleddate);

    int getsettleduser();

    void setsettleduser(int settleduser);

    String getcomments();

    void setcomments(String comments);

    int getdisputeby();

    void setdisputeby(int disputeby);

    Date getdisputedate();

    void setdisputedate(Date disputedate);

    int getdisputewithroles();

    void setdisputewithroles(int disputewithroles);

    String getcolumn1();

    void setcolumn1(String column1);

    String getcolumn2();

    void setcolumn2(String column2);

    String getcolumn3();

    void setcolumn3(String column3);

    String getcolumn4();

    void setcolumn4(String column4);

    String getcolumn5();

    void setcolumn5(String column5);

    int gettransactiontypemaster();

    void settransactiontypemaster(int transactiontypemaster);

    int getresponsecodemaster();

    void setresponsecodemaster(int responsecodemaster);

    String getcardnosuffix();

    void setcardnosuffix(String cardnosuffix);

    int getCorrectiveentryflag();

    void setCorrectiveentryflag(int correctiveentryflag);

    Float getabsamount();

    void setabsamount(Float absamount);

    String getterminal();

    void setterminal(String terminal);

    String getnetworkid();

    void setnetworkid(String networkid);

    String getacquirercurrency();

    void setacquirercurrency(String acquirercurrency);

    int getacquirercurrencyid();

    void setacquirercurrencyid(int acquirercurrencyid);

    Float getacquireramount();

    void setacquireramount(Float acquireramount);

    String getsettlementcurrency();

    void setsettlementcurrency(String settlementcurrency);

    int getsettlementcurrencyid();

    void setsettlementcurrencyid(int settlementcurrencyid);

    Float getsettlementamount();

    void setsettlementamount(Float settlementamount);

    String getauthorizationno();

    void setauthorizationno(String authorizationno);

    String getreferenceno();

    void setreferenceno(String referenceno);

    String getbank();

    void setbank(String bank);

    int gettransactionid();

    void settransactionid(int transactionid);

    String getlicencekey();

    void setlicencekey(String licencekey);

    String getnetworkcode();

    void setnetworkcode(String networkcode);

    String getprocesscode();

    void setprocesscode(String processcode);

    String getareacode();

    void setareacode(String areacode);

    String getreportid();

    void setreportid(String reportid);

    int getnetworkcomm();

    void setnetworkcomm(int networkcomm);

    String getnetworkcommflag();

    void setnetworkcommflag(String networkcommflag);

    String getfeelevel();

    void setfeelevel(String feelevel);

    int getreverseflag();

    void setreverseflag(int reverseflag);

    int getfileid();

    void setfileid(int fileid);

    Date getloadingdate();

    void setloadingdate(Date loadingdate);

    String gettransactiontype();

    void settransactiontype(String transactiontype);

    int gettransactiontypeid();

    void settransactiontypeid(int transactiontypeid);

    String getcurrency();

    void setcurrency(String currency);

    int getcurrencyid();

    void setcurrencyid(int currencyid);

    String getresponsecode();

    void setresponsecode(String responsecode);

    void setBlacklisted(int blacklisted);

    int getBlacklisted();
}
