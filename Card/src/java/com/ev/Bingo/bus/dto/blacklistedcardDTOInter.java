package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface blacklistedcardDTOInter extends Serializable {

    String getcardno();

    void setcardno(String cardno);

    String getcustomerno();

    void setcustomerno(String customerno);

    String getcomments();

    void setcomments(String comments);

    Date getblacklisteddate();

    void setblacklisteddate(Date blacklisteddate);
}
