/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.util.Date;

/**
 *
 * @author Administrator
 */
public class settlementlogsearchDTO implements settlementlogsearchDTOInter {

    private Date transactiondatefrom;
    private Date transactiondateto;
    private Integer userid;
    private Integer setteledid;
    private String cardno;
    private String atmid;
    private usersDTOInter userloged;

    public usersDTOInter getUserloged() {
        return userloged;
    }

    public void setUserloged(usersDTOInter userloged) {
        this.userloged = userloged;
    }

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public Integer getSetteledid() {
        return setteledid;
    }

    public void setSetteledid(Integer setteledid) {
        this.setteledid = setteledid;
    }

    public Date getTransactiondatefrom() {
        return transactiondatefrom;
    }

    public void setTransactiondatefrom(Date transactiondatefrom) {
        this.transactiondatefrom = transactiondatefrom;
    }

    public Date getTransactiondateto() {
        return transactiondateto;
    }

    public void setTransactiondateto(Date transactiondateto) {
        this.transactiondateto = transactiondateto;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

}
