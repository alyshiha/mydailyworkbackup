/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dto;

import java.math.BigDecimal;

/**
 *
 * @author shi7a
 */
public class timeFromSwitchDTO implements timeFromSwitchDTOInter {
    private BigDecimal amount;
    private Integer count;
    private String acq;
    private String iss;
    private String currency;

    public timeFromSwitchDTO() {
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public Integer getCount() {
        return count;
    }

    @Override
    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String getAcq() {
        return acq;
    }

    @Override
    public void setAcq(String acq) {
        this.acq = acq;
    }

    @Override
    public String getIss() {
        return iss;
    }

    @Override
    public void setIss(String iss) {
        this.iss = iss;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    
}
