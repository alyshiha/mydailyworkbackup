package com.ev.Bingo.bus.dto;

import com.ev.Bingo.base.dto.BaseDTO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.util.Date;
import java.io.Serializable;

public class columnssetupdetailsDTO extends BaseDTO implements columnssetupdetailsDTOInter, Serializable {

    private int displaycolumnid;

    public int getdisplaycolumnid() {
        return displaycolumnid;
    }

    public void setdisplaycolumnid(int displaycolumnid) {
        this.displaycolumnid = displaycolumnid;
    }
    private int columnid;

    public int getcolumnid() {
        return columnid;
    }

    public void setcolumnid(int columnid) {
        this.columnid = columnid;
    }
    public String rowid;

    public String getrowid() {
        return rowid;
    }

    public void setrowid(String rowid) {
        this.rowid = rowid;
    }

}
