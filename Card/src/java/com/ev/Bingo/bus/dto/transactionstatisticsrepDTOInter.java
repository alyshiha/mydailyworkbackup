package com.ev.Bingo.bus.dto;

import java.io.Serializable;
import java.util.Date;

public interface transactionstatisticsrepDTOInter extends Serializable {

    int getreportid();

    void setreportid(int reportid);

    int getid();

    void setid(int id);

    String getname();

    void setname(String name);

    Float getmatchedcount();

    void setmatchedcount(Float matchedcount);

    Float getmatchedamountnetwork();

    void setmatchedamountnetwork(Float matchedamountnetwork);

    Float getmatchedamountswitch();

    void setmatchedamountswitch(Float matchedamountswitch);

    Float getmatchedwithdiffamounttotal();

    void setmatchedwithdiffamounttotal(Float matchedwithdiffamounttotal);

    Float getmatchedwithdiffamountcount();

    void setmatchedwithdiffamountcount(Float matchedwithdiffamountcount);

    Float getcountdisputesnetwork();

    void setcountdisputesnetwork(Float countdisputesnetwork);

    Float gettotaldisputesnetwork();

    void settotaldisputesnetwork(Float totaldisputesnetwork);

    Float getcountdisputesswitch();

    void setcountdisputesswitch(Float countdisputesswitch);

    Float gettotaldisputesswitch();

    void settotaldisputesswitch(Float totaldisputesswitch);

    Float getcountdisputesswitch000();

    void setcountdisputesswitch000(Float countdisputesswitch000);

    Float gettotaldisputesswitch000();

    void settotaldisputesswitch000(Float totaldisputesswitch000);

    Float getcountdisputesnetworknotrev();

    void setcountdisputesnetworknotrev(Float countdisputesnetworknotrev);

    Float gettotaldisputesnetworknotrev();

    void settotaldisputesnetworknotrev(Float totaldisputesnetworknotrev);

    Float getcountdisputesnetworkrev();

    void setcountdisputesnetworkrev(Float countdisputesnetworkrev);

    Float gettotaldisputesnetworkrev();

    void settotaldisputesnetworkrev(Float totaldisputesnetworkrev);
}
