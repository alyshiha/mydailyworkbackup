/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.bus.dto.HolidaysDTOInter;
import java.util.List;

/**
 *
 * @author shi7a
 */
public interface HolidaysDAOInter {

    String delete(Object... obj);

    List<HolidaysDTOInter> findAll() throws Throwable;

    List<HolidaysDTOInter> findRecord(String Type,String Name) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;
    
}
