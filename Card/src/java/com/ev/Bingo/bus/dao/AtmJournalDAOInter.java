/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.bus.dto.AtmJournalDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface AtmJournalDAOInter {

    List<AtmJournalDTOInter> findrecordjournal(String dateFrom, String dateTo, String NoOfAtms, int export, int group, String Reversal, int user,String indication,Boolean realesed) throws Throwable;
void recalc2() throws Throwable;
    void recalc(Date selectedDate) throws Throwable;

    Object delete(Object... obj) throws Throwable;

    List<AtmJournalDTOInter> findAll() throws Throwable;

    List<AtmJournalDTOInter> findrecord(String AppID, int repid, int export, String FromDate) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
