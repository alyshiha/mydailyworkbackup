package com.ev.Bingo.bus.dao;

import java.util.Date;
import java.util.List;
import java.sql.SQLException;

public interface columnssetupdetailsDAOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object deleterecorddetail(Object... obj) throws Throwable;

    Object findRecordsListCS(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;
}
