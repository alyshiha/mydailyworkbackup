/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.bus.dto.privilegeStatusNameDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface privilegeStatusNameDAOInter {

    String delete(Object... obj) ;

    List<privilegeStatusNameDTOInter> findAll() throws Throwable;

    List<privilegeStatusNameDTOInter> findRecord(String Name) throws Throwable;

    Object insertpriv(String Userid, String MenuID, String PrevID) throws Throwable;

    List<privilegeStatusNameDTOInter> findprivassigned(String Userid, String MenuID) throws Throwable;

    List<privilegeStatusNameDTOInter> deleteprivassigned(String Userid, String MenuID) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
