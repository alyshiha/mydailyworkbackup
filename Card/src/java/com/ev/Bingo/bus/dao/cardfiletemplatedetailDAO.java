package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import com.ev.Bingo.bus.dto.cardfiletemplateDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.cardfiletemplatedetailDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class cardfiletemplatedetailDAO extends BaseDAO implements cardfiletemplatedetailDAOInter {

    protected cardfiletemplatedetailDAO() {
        super();
        super.setTableName("NETWORK_FILE_TEMPLATE_DETAIL");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        cardfiletemplatedetailDTOInter RecordToInsert = (cardfiletemplatedetailDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (ID,TEMPLATE,COLUMN_ID,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LINE_NUMBER,LOAD_WHEN_MAPPED,LENGTH_DIR,STARTING_POSITION,TAG_STRING,MANDATORY,NEGATIVE_AMOUNT_FLAG,ADD_DECIMAL,DECIMAL_POS) "
                + " values "
                + " ($id,$template,$columnid,$position,'$format','$format2',$datatype,$length,$linenumber,$loadwhenmapped,$lengthdir,$startingposition,'$tagstring',$mandatory,$negativeamountflag,$adddecimal,$decimalpos)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$template", "" + RecordToInsert.gettemplate());
        insertStat = insertStat.replace("$columnid", "" + RecordToInsert.getcolumnid());
        insertStat = insertStat.replace("$position", "" + RecordToInsert.getposition());
        insertStat = insertStat.replace("$format2", "" + RecordToInsert.getformat2());
        insertStat = insertStat.replace("$format", "" + RecordToInsert.getformat());
        
        insertStat = insertStat.replace("$datatype", "" + RecordToInsert.getdatatype());
        insertStat = insertStat.replace("$length", "" + RecordToInsert.getlength());
        insertStat = insertStat.replace("$linenumber", "" + RecordToInsert.getlinenumber());
        insertStat = insertStat.replace("$loadwhenmapped", "" + RecordToInsert.getloadwhenmapped());
        insertStat = insertStat.replace("$lengthdir", "" + RecordToInsert.getlengthdir());
        insertStat = insertStat.replace("$startingposition", "" + RecordToInsert.getstartingposition());
        insertStat = insertStat.replace("$tagstring", "" + RecordToInsert.gettagstring());
        insertStat = insertStat.replace("$mandatory", "" + RecordToInsert.getmandatory());
        insertStat = insertStat.replace("$negativeamountflag", "" + RecordToInsert.getnegativeamountflag());
        insertStat = insertStat.replace("$adddecimal", "" + RecordToInsert.getadddecimal());
        insertStat = insertStat.replace("$decimalpos", "" + RecordToInsert.getdecimalpos());
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To NETWORK_FILE_TEMPLATE_DETAIL", false);
        return null;
    }

    public Boolean ValidateNull(Object... obj) {
        cardfiletemplatedetailDTOInter RecordToInsert = (cardfiletemplatedetailDTOInter) obj[0];
        if (RecordToInsert.getid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.gettemplate() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getcolumnid() == 0) {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;

    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        cardfiletemplatedetailDTOInter RecordToUpdate = (cardfiletemplatedetailDTOInter) obj[0];
        String UpdateStat = "update $table set "
                + " (ID= $id,TEMPLATE= $template,COLUMN_ID= $columnid,POSITION= $position,FORMAT= '$format',FORMAT2= '$format2',DATA_TYPE= $datatype,LENGTH= $length,LINE_NUMBER= $linenumber,LOAD_WHEN_MAPPED= $loadwhenmapped,LENGTH_DIR= $lengthdir,STARTING_POSITION= $startingposition,TAG_STRING= '$tagstring',MANDATORY= $mandatory,NEGATIVE_AMOUNT_FLAG= $negativeamountflag,ADD_DECIMAL= $adddecimal,DECIMAL_POS= $decimalpos) "
                + "  where  ID= $id";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$id", "" + RecordToUpdate.getid());
        UpdateStat = UpdateStat.replace("$template", "" + RecordToUpdate.gettemplate());
        UpdateStat = UpdateStat.replace("$columnid", "" + RecordToUpdate.getcolumnid());
        UpdateStat = UpdateStat.replace("$position", "" + RecordToUpdate.getposition());
        UpdateStat = UpdateStat.replace("$format2", "" + RecordToUpdate.getformat2());
        UpdateStat = UpdateStat.replace("$format", "" + RecordToUpdate.getformat());
        
        
        UpdateStat = UpdateStat.replace("$datatype", "" + RecordToUpdate.getdatatype());
        UpdateStat = UpdateStat.replace("$length", "" + RecordToUpdate.getlength());
        UpdateStat = UpdateStat.replace("$linenumber", "" + RecordToUpdate.getlinenumber());
        UpdateStat = UpdateStat.replace("$loadwhenmapped", "" + RecordToUpdate.getloadwhenmapped());
        UpdateStat = UpdateStat.replace("$lengthdir", "" + RecordToUpdate.getlengthdir());
        UpdateStat = UpdateStat.replace("$startingposition", "" + RecordToUpdate.getstartingposition());
        UpdateStat = UpdateStat.replace("$tagstring", "" + RecordToUpdate.gettagstring());
        UpdateStat = UpdateStat.replace("$mandatory", "" + RecordToUpdate.getmandatory());
        UpdateStat = UpdateStat.replace("$negativeamountflag", "" + RecordToUpdate.getnegativeamountflag());
        UpdateStat = UpdateStat.replace("$adddecimal", "" + RecordToUpdate.getadddecimal());
        UpdateStat = UpdateStat.replace("$decimalpos", "" + RecordToUpdate.getdecimalpos());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table NETWORK_FILE_TEMPLATE_DETAIL", false);
        return null;
    }

    public Object deleterecord(int seq) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from array_temp "
                + "  where  seq= $seq";
        deleteStat = deleteStat.replace("$seq", "" + seq);
        super.executeUpdate(deleteStat);
        return null;
    }

    public String deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        cardfiletemplateDTOInter Record = (cardfiletemplateDTOInter) obj[0];
        String TableName = (String) obj[1];
        String deleteStat = "delete from $table WHERE TEMPLATE = $TEMPLATE";
        deleteStat = deleteStat.replace("$table", "" + TableName + "_DETAIL");
        deleteStat = deleteStat.replace("$TEMPLATE", "" + Record.getid());
        String msg = super.executeUpdate(deleteStat);
        return msg;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        cardfiletemplatedetailDTOInter RecordToSelect = (cardfiletemplatedetailDTOInter) obj[0];
        String selectStat = "Select ID,TEMPLATE,COLUMN_ID,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LINE_NUMBER,LOAD_WHEN_MAPPED,LENGTH_DIR,STARTING_POSITION,TAG_STRING,MANDATORY,NEGATIVE_AMOUNT_FLAG,ADD_DECIMAL,DECIMAL_POS From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        cardfiletemplatedetailDTOInter SelectedRecord = DTOFactory.createcardfiletemplatedetailDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.settemplate(rs.getInt("TEMPLATE"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.setposition(rs.getString("POSITION"));
            SelectedRecord.setformat(rs.getString("FORMAT"));
            SelectedRecord.setformat2(rs.getString("FORMAT2"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));
            SelectedRecord.setlength(rs.getString("LENGTH"));
            SelectedRecord.setlinenumber(rs.getString("LINE_NUMBER"));
            SelectedRecord.setloadwhenmapped(rs.getString("LOAD_WHEN_MAPPED"));
            SelectedRecord.setlengthdir(rs.getString("LENGTH_DIR"));
            SelectedRecord.setstartingposition(rs.getString("STARTING_POSITION"));
            SelectedRecord.settagstring(rs.getString("TAG_STRING"));
            SelectedRecord.setmandatory(rs.getInt("MANDATORY"));
            SelectedRecord.setnegativeamountflag(rs.getInt("NEGATIVE_AMOUNT_FLAG"));
            SelectedRecord.setadddecimal(rs.getString("ADD_DECIMAL"));
            SelectedRecord.setdecimalpos(rs.getInt("DECIMAL_POS"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public int FindTableName(String TabName) {
        int TableName = 0;
        if (TabName.equals("NETWORK_FILE_TEMPLATE")) {
            TableName = 1;
        } else if (TabName.equals("SWITCH_FILE_TEMPLATE")) {
            TableName = 2;
        } else if (TabName.equals("HOST_FILE_TEMPLATE")) {
            TableName = 3;
        }
        return TableName;
    }

    public int calltemplate(int networktype, int TemplateID) throws Throwable {
        super.preCollable();
        CallableStatement stat = super.executeCallableStatment("{call query_templete(?,?,?)}");
        stat.registerOutParameter(1, Types.NUMERIC);
        stat.setInt(2, TemplateID);
        stat.setInt(3, networktype);
        stat.executeUpdate();
        int i = stat.getInt(1);
        super.postCollable(stat);
        
        return i;
    }

    public int updatetemplate(int seq, int TemplateID, int networktype) throws Throwable {
        super.preCollable();
        CallableStatement stat = super.executeCallableStatment("{call update_templete(?,?,?)}");
        System.out.println("update_templete(" + seq + "," + TemplateID + "," + networktype + ")");
        stat.setInt(1, seq);
        stat.setInt(2, TemplateID);
        stat.setInt(3, networktype);
        stat.executeUpdate();
        super.postCollable(stat);
        
        return seq;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        String TableName = (String) obj[0];
        cardfiletemplateDTOInter SearchRecords = (cardfiletemplateDTOInter) obj[1];
        int networktype = FindTableName(TableName);
        int TemplateID = SearchRecords.getid();
        int seq = calltemplate(networktype, TemplateID);
        String selectStat = "select seq, add_decimal, column_id, column_name, data_type, decimal_pos, format, format2, "
                + "id, length, length_dir, line_number, load_when_mapped, mandatory, negative_amount_flag, position,"
                + " starting_position, tag_string, template from array_temp where seq = $seq";
        selectStat = selectStat.replace("$seq", "" + seq);
        ResultSet rs = executeQuery(selectStat);
        List<cardfiletemplatedetailDTOInter> Records = new ArrayList<cardfiletemplatedetailDTOInter>();
        while (rs.next()) {
            cardfiletemplatedetailDTOInter SelectedRecord = DTOFactory.createcardfiletemplatedetailDTO();
            SelectedRecord.setadddecimal(rs.getString("add_decimal"));
            SelectedRecord.setcolumnid(rs.getInt("column_id"));
            SelectedRecord.setdatatype(rs.getInt("data_type"));
            SelectedRecord.setdecimalpos(rs.getInt("decimal_pos"));
            if (SelectedRecord.getdecimalpos() == 1) {
                SelectedRecord.setDecimalposboolean(Boolean.TRUE);
            } else {
                SelectedRecord.setDecimalposboolean(Boolean.FALSE);
            }
            SelectedRecord.setformat2(rs.getString("format2"));
            SelectedRecord.setformat(rs.getString("format"));
            SelectedRecord.setid(rs.getInt("id"));
            SelectedRecord.setlength(rs.getString("length"));
            SelectedRecord.setlengthdir(rs.getString("length_dir"));
            SelectedRecord.setlinenumber(rs.getString("line_number"));
            SelectedRecord.setloadwhenmapped(rs.getString("load_when_mapped"));
            SelectedRecord.setmandatory(rs.getInt("mandatory"));
            if (SelectedRecord.getmandatory() == 1) {
                SelectedRecord.setMandatoryboolean(Boolean.TRUE);
            } else {
                SelectedRecord.setMandatoryboolean(Boolean.FALSE);
            }
            SelectedRecord.setnegativeamountflag(rs.getInt("negative_amount_flag"));
            if (SelectedRecord.getnegativeamountflag() == 1) {
                SelectedRecord.setNegativeamountflagboolean(Boolean.TRUE);
            } else {
                SelectedRecord.setNegativeamountflagboolean(Boolean.FALSE);
            }
            SelectedRecord.setposition(rs.getString("position"));
            SelectedRecord.setstartingposition(rs.getString("starting_position"));
            SelectedRecord.settagstring(rs.getString("tag_string"));
            SelectedRecord.settemplate(rs.getInt("template"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        deleterecord(seq);
        return Records;
    }

    public Object findRecordsListUpdate(int seq) throws Throwable {
        super.preSelect();
        String selectStat = "select seq, add_decimal, column_id, column_name, data_type, decimal_pos, format, format2, "
                + "id, length, length_dir, line_number, load_when_mapped, mandatory, negative_amount_flag, position,"
                + " starting_position, tag_string, template from array_temp where seq = $seq";
        selectStat = selectStat.replace("$seq", "" + seq);
        ResultSet rs = executeQuery(selectStat);
        List<cardfiletemplatedetailDTOInter> Records = new ArrayList<cardfiletemplatedetailDTOInter>();
        while (rs.next()) {
            cardfiletemplatedetailDTOInter SelectedRecord = DTOFactory.createcardfiletemplatedetailDTO();
            SelectedRecord.setadddecimal(rs.getString("add_decimal"));
            SelectedRecord.setcolumnid(rs.getInt("column_id"));
            SelectedRecord.setdatatype(rs.getInt("data_type"));
            SelectedRecord.setdecimalpos(rs.getInt("decimal_pos"));
            if (SelectedRecord.getdecimalpos() == 1) {
                SelectedRecord.setDecimalposboolean(Boolean.TRUE);
            } else {
                SelectedRecord.setDecimalposboolean(Boolean.FALSE);
            }
            SelectedRecord.setformat2(rs.getString("format2"));
            SelectedRecord.setformat(rs.getString("format"));
            SelectedRecord.setid(rs.getInt("id"));
            SelectedRecord.setlength(rs.getString("length"));
            SelectedRecord.setlengthdir(rs.getString("length_dir"));
            SelectedRecord.setlinenumber(rs.getString("line_number"));
            SelectedRecord.setloadwhenmapped(rs.getString("load_when_mapped"));
            SelectedRecord.setmandatory(rs.getInt("mandatory"));
            if (SelectedRecord.getmandatory() == 1) {
                SelectedRecord.setMandatoryboolean(Boolean.TRUE);
            } else {
                SelectedRecord.setMandatoryboolean(Boolean.FALSE);
            }
            SelectedRecord.setnegativeamountflag(rs.getInt("negative_amount_flag"));
            if (SelectedRecord.getnegativeamountflag() == 1) {
                SelectedRecord.setNegativeamountflagboolean(Boolean.TRUE);
            } else {
                SelectedRecord.setNegativeamountflagboolean(Boolean.FALSE);
            }
            SelectedRecord.setposition(rs.getString("position"));
            SelectedRecord.setstartingposition(rs.getString("starting_position"));
            SelectedRecord.settagstring(rs.getString("tag_string"));
            SelectedRecord.settemplate(rs.getInt("template"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        //deleterecord(seq);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,TEMPLATE,COLUMN_ID,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LINE_NUMBER,LOAD_WHEN_MAPPED,LENGTH_DIR,STARTING_POSITION,TAG_STRING,MANDATORY,NEGATIVE_AMOUNT_FLAG,ADD_DECIMAL,DECIMAL_POS From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<cardfiletemplatedetailDTOInter> Records = new ArrayList<cardfiletemplatedetailDTOInter>();
        while (rs.next()) {
            cardfiletemplatedetailDTOInter SelectedRecord = DTOFactory.createcardfiletemplatedetailDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.settemplate(rs.getInt("TEMPLATE"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.setposition(rs.getString("POSITION"));
            SelectedRecord.setformat(rs.getString("FORMAT"));
            SelectedRecord.setformat2(rs.getString("FORMAT2"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));
            SelectedRecord.setlength(rs.getString("LENGTH"));
            SelectedRecord.setlinenumber(rs.getString("LINE_NUMBER"));
            SelectedRecord.setloadwhenmapped(rs.getString("LOAD_WHEN_MAPPED"));
            SelectedRecord.setlengthdir(rs.getString("LENGTH_DIR"));
            SelectedRecord.setstartingposition(rs.getString("STARTING_POSITION"));
            SelectedRecord.settagstring(rs.getString("TAG_STRING"));
            SelectedRecord.setmandatory(rs.getInt("MANDATORY"));
            SelectedRecord.setnegativeamountflag(rs.getInt("NEGATIVE_AMOUNT_FLAG"));
            SelectedRecord.setadddecimal(rs.getString("ADD_DECIMAL"));
            SelectedRecord.setdecimalpos(rs.getInt("DECIMAL_POS"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String toAscii(String Text) {
        String Result = "";
        if (!"".equals(Text) && Text != null) {
            char[] charArray = Text.toCharArray();
            for (int i = 0; i < charArray.length; i++) {
                char Char = charArray[i];
                Result = Result + "CHR('" + (int) Char + "')||";
            }
            if (Result.length() > 4) {
                Result = Result.substring(0, Result.length() - 2);
            } else {
                Result = "''";
            }
        } else {
            Result = "''";
        }
        return Result;
    }

    public int save(Object... obj) throws SQLException, Throwable {
        List<cardfiletemplatedetailDTOInter> entities = (List<cardfiletemplatedetailDTOInter>) obj[0];
        String TableName = (String) obj[1];
        int Seq = super.generateSequence("array");
        String insertStat = "";
        for (int i = 0; i < entities.size(); i++) {
            insertStat = "insert into array_temp\n"
                    + "  (seq, add_decimal, column_id, data_type, decimal_pos, format, format2, id,length_dir, length,  line_number, load_when_mapped, mandatory, negative_amount_flag, starting_position,"
                    + " position, tag_string, template)\n"
                    + "values\n"
                    + "  ($Seq, $adddecimal, '$columnid', '$datatype', '$Decimalpos', '$format', '$format2', $id, '$lengthdir', '$length', '$linenumber', '$loadwhenmapped', $Mandatory,$Negativeamountflag, '$startingposition','$position','$tagstring',$template)";
            super.preUpdate();
            cardfiletemplatedetailDTOInter RecordtoInsert = entities.get(i);

            insertStat = insertStat.replace("$Seq", "" + Seq);
            String adddec = toAscii(RecordtoInsert.getadddecimal());
            if ("''".equals(adddec)) {
                adddec = "'1'";
            }
            insertStat = insertStat.replace("$adddecimal", "" + adddec);
            insertStat = insertStat.replace("$columnid", "" + RecordtoInsert.getcolumnid());
            if (RecordtoInsert.getdatatype() != 0) {
                insertStat = insertStat.replace("$datatype", "" + RecordtoInsert.getdatatype());
            } else {
                insertStat = insertStat.replace("$datatype", "0");
            }
            if (RecordtoInsert.getDecimalposboolean() == Boolean.TRUE) {
                insertStat = insertStat.replace("$Decimalpos", "" + 1);
            } else {
                insertStat = insertStat.replace("$Decimalpos", "" + 2);
            }
            insertStat = insertStat.replace("$format2", "" + toAscii(RecordtoInsert.getformat2()));
            insertStat = insertStat.replace("$format", "" + toAscii(RecordtoInsert.getformat()));
            insertStat = insertStat.replace("$id", "" + RecordtoInsert.getid());
            insertStat = insertStat.replace("$lengthdir", "" + RecordtoInsert.getlengthdir());
            insertStat = insertStat.replace("$length", "" + RecordtoInsert.getlength());
            if ("".equals(RecordtoInsert.getlinenumber()) || RecordtoInsert.getlinenumber() == null) {
                insertStat = insertStat.replace("$linenumber", "0");
            } else {
                insertStat = insertStat.replace("$linenumber", "" + RecordtoInsert.getlinenumber());
            }
            insertStat = insertStat.replace("$loadwhenmapped", "" + RecordtoInsert.getloadwhenmapped());

            if (RecordtoInsert.getMandatoryboolean() == Boolean.TRUE) {
                insertStat = insertStat.replace("$Mandatory", "" + 1);
            } else {
                insertStat = insertStat.replace("$Mandatory", "" + 2);
            }
            if (RecordtoInsert.getNegativeamountflagboolean() == Boolean.TRUE) {
                insertStat = insertStat.replace("$Negativeamountflag", "" + 1);
            } else {
                insertStat = insertStat.replace("$Negativeamountflag", "" + 2);
            }
            insertStat = insertStat.replace("$startingposition", "" + RecordtoInsert.getstartingposition());
            if ("".equals(RecordtoInsert.getposition()) || RecordtoInsert.getposition() == null) {
                insertStat = insertStat.replace("$position", "0");
            } else {
                insertStat = insertStat.replace("$position", "" + RecordtoInsert.getposition());
            }
            insertStat = insertStat.replace("$tagstring", "" + toAscii(RecordtoInsert.gettagstring()));
            insertStat = insertStat.replace("$template", "" + RecordtoInsert.gettemplate());
            insertStat = insertStat.replace("'CHR", "CHR");
            insertStat = insertStat.replace("')',", "'),");
            super.executeUpdate(insertStat);
        }

        return Seq;
    }
}
