package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

public interface transactionresponsemasterDAOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    String save(Object... obj) throws SQLException, Throwable;

    Object findRecordsAll(Object... obj) throws Throwable;
//used in system rules

    Object findRecord(Object... obj) throws Throwable;
}
