package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import com.ev.Bingo.bus.dto.duplicatesettingdetailDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.duplicatesettingmasterDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class duplicatesettingmasterDAO extends BaseDAO implements duplicatesettingmasterDAOInter {

    protected duplicatesettingmasterDAO() {
        super();
        super.setTableName("DUPLICATE_SETTING_MASTER");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        duplicatesettingmasterDTOInter RecordToInsert = (duplicatesettingmasterDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (ID,REC_TYPE,NETWORK_ID) "
                + " values "
                + " ($id,$rectype,$networkid)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$rectype", "" + RecordToInsert.getrectype());
        insertStat = insertStat.replace("$networkid", "" + RecordToInsert.getnetworkid());
        String mm = "";
        try {

            String msg = super.executeUpdate(insertStat);
            mm = msg + " Record has been inserted";
        } catch (Exception ex) {
            mm = ex.getMessage();
        }
        super.postUpdate("Adding New MASTER DUPLICATE SETTING Record With Record Type " + RecordToInsert.getrectype() + " ,Network " + RecordToInsert.getnetworkid(), false);
        return mm;
    }

    public Boolean ValidateNull(Object... obj) {
        duplicatesettingmasterDTOInter RecordToInsert = (duplicatesettingmasterDTOInter) obj[0];
        if (RecordToInsert.getid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getrectype() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getnetworkid() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Object deleterecordbyid(Object... obj) throws Throwable {
        super.preUpdate();
        duplicatesettingdetailDTOInter RecordToDelete = (duplicatesettingdetailDTOInter) obj[0];
        String deleteStat = "delete from DUPLICATE_SETTING_DETAIL "
                + "  where  ID= $id AND column_id = $columnid";
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        deleteStat = deleteStat.replace("$columnid", "" + RecordToDelete.getcolumnid());

        String msg = super.executeUpdate(deleteStat);
        super.postUpdate("Deleting A MASTER DUPLICATE SETTING With Column ID " + RecordToDelete.getcolumnid(), false);
        return msg + " Record Has Been Deleted";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        duplicatesettingmasterDTOInter RecordToDelete = (duplicatesettingmasterDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        deleteStat = deleteStat.replace("$rectype", "" + RecordToDelete.getrectype());
        deleteStat = deleteStat.replace("$networkid", "" + RecordToDelete.getnetworkid());
        String msg = super.executeUpdate(deleteStat);
        super.postUpdate("Deleting A MASTER DUPLICATE SETTING With ID " + RecordToDelete.getid(), false);
        return msg + " Record Has Been Deleted";
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,REC_TYPE,NETWORK_ID From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<duplicatesettingmasterDTOInter> Records = new ArrayList<duplicatesettingmasterDTOInter>();
        while (rs.next()) {
            duplicatesettingmasterDTOInter SelectedRecord = DTOFactory.createduplicatesettingmasterDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setrectype(rs.getInt("REC_TYPE"));
            SelectedRecord.setnetworkid(rs.getInt("NETWORK_ID"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<duplicatesettingmasterDTOInter> entities = (List<duplicatesettingmasterDTOInter>) obj[0];
        String insertStat = "Update DUPLICATE_SETTING_MASTER Set REC_TYPE = ?,NETWORK_ID = ? Where ID = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        String mess = "", mess2 = "";
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
           connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.size(); i++) {
                duplicatesettingmasterDTOInter RecordtoInsert = entities.get(i);

                statement.setInt(1, RecordtoInsert.getrectype());
                statement.setInt(2, RecordtoInsert.getnetworkid());
                statement.setInt(3, RecordtoInsert.getid());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            int[] numUpdates = statement.executeBatch();
            Integer valid = 0, notvalid = 0;

            for (int i = 0; i < numUpdates.length; i++) {
                if (numUpdates[i] == -2) {
                    valid++;
                    mess = valid + " rows has been updated";
                } else {
                    notvalid++;
                    mess2 = notvalid + " can`t be updated";
                }
            }
            if (!mess2.equals("")) {
                mess = mess + "," + mess2;
            }
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                    mess = logOrIgnore.getMessage();
                }
            }
            if (connection != null) {
                try {
         CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                    mess = logOrIgnore.getMessage();
                }
            }
        }
        return mess;
    }
}
