
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.BankCodeDTOInter;
import com.ev.Bingo.bus.dto.Col4DTOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aly
 */
public class Col4DAO extends BaseDAO implements Col4DAOInter {

    @Override
    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "select id, name from col4_code order by Name";
        ResultSet rs = executeQuery(selectStat);
        List<Col4DTOInter> Records = new ArrayList<Col4DTOInter>();
        while (rs.next()) {
            Col4DTOInter SelectedRecord = DTOFactory.createCol4DTO();
            SelectedRecord.setId(rs.getInt("id"));
            SelectedRecord.setName(rs.getString("Name"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

}
