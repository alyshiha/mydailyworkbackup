package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

public interface copyfilesDAOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object updaterecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object deleteallrecord(Object... obj) throws Throwable;

    Object findRecord(Object... obj) throws Throwable;

    Object findRecordsList(Object... obj) throws Throwable;

    Object findRecordsAll(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;
}
