/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;

import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.DiffAmountDTOInter;
import com.ev.Bingo.bus.dto.UserNetworkDTOInter;
import com.ev.Bingo.bus.dto.atmfileDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author AlyShiha
 */
public class DiffAmountDAO extends BaseDAO implements DiffAmountDAOInter {

    protected DiffAmountDAO() {
        super();
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<DiffAmountDTOInter> entities = (List<DiffAmountDTOInter>) obj[0];
        String mess = "", mess2 = "";
        Integer index = super.generateSequence("diff_amount_report");
        if (entities.isEmpty()) {
            mess = "No Columns Found To Be Updated";
        } else {

            String insertStat = "insert into diff_amount_report\n"
                    + "  (transdate, cardnumber, network, switch, networkdiff, switchdiff, reortid,NETWORKID,CUSTOMER_ACCOUNT_NUMBER)\n"
                    + "values\n"
                    + "  (to_date(?,'dd.mm.yyyy hh24:mi:ss'), ?, ?, ?, ?, ?, ?,?,?)";
            Connection connection = null;
            PreparedStatement statement = null;
            try {
                connection = CoonectionHandler.getInstance().getConnection();
                statement = connection.prepareStatement(insertStat);
                for (int i = 0; i < entities.size(); i++) {
                    DiffAmountDTOInter RecordtoInsert = entities.get(i);
                    statement.setString(1, "" + DateFormatter.changeDateAndTimeFormat(RecordtoInsert.getTransactiondate()).toString());
                    statement.setString(2, RecordtoInsert.getCardno());
                    statement.setFloat(3, RecordtoInsert.getAmount1());
                    statement.setFloat(4, RecordtoInsert.getAmount2());
                    statement.setFloat(5, RecordtoInsert.getSwitchD());
                    statement.setFloat(6, RecordtoInsert.getNetworkD());
                    statement.setInt(7, index);
                    statement.setString(8, RecordtoInsert.getNetwork());
                    statement.setString(9, RecordtoInsert.getAcc());
                    statement.addBatch();
                    if ((i + 1) % 1000 == 0) {
                        statement.executeBatch();
                    }
                }
                int[] numUpdates = statement.executeBatch();
                Integer valid = 0, notvalid = 0;

                for (int i = 0; i < numUpdates.length; i++) {
                    if (numUpdates[i] == -2) {
                        valid++;
                        mess = valid + " rows has been updated";
                    } else {
                        notvalid++;
                        mess2 = notvalid + " can`t be updated";
                    }
                }
                if (!mess2.equals("")) {
                    mess = mess + "," + mess2;
                }
            } finally {
                if (statement != null) {
                    try {
                        connection.commit();
                        statement.close();
                    } catch (SQLException logOrIgnore) {
                    }
                }
                if (connection != null) {
                    try {
                        CoonectionHandler.getInstance().returnConnection(connection);
                    } catch (SQLException logOrIgnore) {
                    }
                }
            }
            return index;

        }
        return index;
    }

    public Object findfileall(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "select id,name from LOADING_FILE_LOG ";
        ResultSet rs = executeQuery(selectStat);
        List<atmfileDTOInter> Records = new ArrayList<atmfileDTOInter>();
        while (rs.next()) {
            atmfileDTOInter SelectedRecord = DTOFactory.createatmfileDTO();
            SelectedRecord.setid(rs.getInt("id"));
            SelectedRecord.setname(rs.getString("name"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findAllRecordsPrint(Object... obj) throws Throwable {
        super.preSelect();
        Integer index = (Integer) obj[0];
        String selectStat = "select transdate, cardnumber, network, switch, networkdiff, switchdiff,NETWORKID,CUSTOMER_ACCOUNT_NUMBER from diff_amount_report where reortid = $index ";
        selectStat = selectStat.replace("$index", "" + index);
        ResultSet rs = executeQuery(selectStat);
        super.postSelect();
        return rs;
    }

    public Object findAllRecords(Object... obj) throws Throwable {
        super.preSelect();
        Date fromDate = (Date) obj[0];
        Date toDate = (Date) obj[1];
        Integer network = (Integer) obj[2];
        String filename = (String) obj[3];
        usersDTOInter UserDTO = (usersDTOInter) obj[4];
        String selectStat = "select m.customer_account_number acc," + super.GetDecryptCardNowithAlias() + " card_no ,m.transaction_date , (Select NAME From NETWORKS Where ID = m.network_id) network_id , m.amount amount_1 , \n"
                + "(select amount from matched_data d where d.match_key = m.match_key and d.record_type <> m.record_type and d.amount <> m.amount) amount_2\n"
                + "from matched_data m\n"
                + "where transaction_date between to_date('$col1','dd.mm.yyyy hh24:mi:ss') \n"
                + "and to_date('$col2','dd.mm.yyyy hh24:mi:ss')\n"
                + "and record_type = 2\n"
                + " and m.network_id IN (select u.networkid from user_network u where u.userid = $user) \n"
                + "and (m.network_id = $network  or $network is null)\n"
                + "and (\n"
                + "(\n"
                + "m.file_id = $fileid \n"
                + "or exists (select 1\n"
                + "from matched_data d\n"
                + "where d.match_key = m.match_key\n"
                + "and d.record_type <> m.record_type\n"
                + "and d.file_id = $fileid\n"
                + ")\n"
                + "\n"
                + ")\n"
                + "\n"
                + "or $fileid is null)\n"
                + "and exists\n"
                + "(select 1\n"
                + "from matched_data d\n"
                + "where d.match_key = m.match_key\n"
                + "and d.record_type <> m.record_type\n"
                + "and d.amount <> m.amount) order by m.transaction_date\n"
                + "";
        selectStat = selectStat.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(fromDate));
        selectStat = selectStat.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(toDate));
        selectStat = selectStat.replace("$user", "" + UserDTO.getUserid());

        if (network != 0) {
            selectStat = selectStat.replace("$network", "" + network);
        } else {
            selectStat = selectStat.replace("$network", "null");
        }
        if (!"0".equals(filename)) {
            selectStat = selectStat.replace("$fileid", "" + filename);
        } else {
            selectStat = selectStat.replace("$fileid", "null");
        }
        ResultSet rs = executeQuery(selectStat);
        List<DiffAmountDTOInter> Records = new ArrayList<DiffAmountDTOInter>();
        while (rs.next()) {
            DiffAmountDTOInter SelectedRecord = DTOFactory.createDiffAmountDTO();
            SelectedRecord.setAcc(rs.getString("acc"));
            SelectedRecord.setCardno(rs.getString("card_no"));
            SelectedRecord.setTransactiondate(rs.getTimestamp("transaction_date"));
            SelectedRecord.setAmount1(rs.getFloat("amount_1"));
            SelectedRecord.setAmount2(rs.getFloat("amount_2"));
            SelectedRecord.setSwitchD(SelectedRecord.getAmount1() - SelectedRecord.getAmount2());
            SelectedRecord.setNetworkD(SelectedRecord.getAmount2() - SelectedRecord.getAmount1());
            if (SelectedRecord.getSwitchD() > SelectedRecord.getNetworkD()) {
                SelectedRecord.setNetworkD((float) 0.0);
            } else if (SelectedRecord.getSwitchD() < SelectedRecord.getNetworkD()) {
                SelectedRecord.setSwitchD((float) 0.0);
            } else if (SelectedRecord.getSwitchD() == SelectedRecord.getNetworkD()) {
                SelectedRecord.setSwitchD((float) 0.0);
                SelectedRecord.setNetworkD((float) 0.0);
            }
            SelectedRecord.setNetwork(rs.getString("network_id"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public void recalc() {
        try {
            preCollable();
            CallableStatement stat = super.executeCallableStatment("{CARDS.CHECK_JOURNAL_DIFF()}");
            stat.execute();
            postCollable(stat);
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
