package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.transactiontypeDTOInter;
import com.ev.Bingo.bus.dto.transactiontypemasterDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class transactiontypeDAO extends BaseDAO implements transactiontypeDAOInter {

    protected transactiontypeDAO() {
        super();
        super.setTableName("TRANSACTION_TYPE");
    }
//used in system rules

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,NAME,DESCRIPTION,TYPE_CATEGORY,TYPE_MASTER,REVERSE_FLAG From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<transactiontypeDTOInter> Records = new ArrayList<transactiontypeDTOInter>();
        while (rs.next()) {
            transactiontypeDTOInter SelectedRecord = DTOFactory.createtransactiontypeDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setdescription(rs.getString("DESCRIPTION"));
            SelectedRecord.settypecategory(rs.getInt("TYPE_CATEGORY"));
            SelectedRecord.settypemaster(rs.getInt("TYPE_MASTER"));
            Integer temp = rs.getInt("REVERSE_FLAG");
            if (temp == 1) {
                SelectedRecord.setreverseflag(Boolean.TRUE);
            } else {
                SelectedRecord.setreverseflag(Boolean.FALSE);
            }
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactiontypeDTOInter RecordToInsert = (transactiontypeDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
            String insertStat = "insert into $table"
                    + " (id, name, description, type_category, type_master, reverse_flag, reverse_type) "
                    + " values "
                    + " ($id,'$name','$description',$typecategory,$typemaster,$reverseflag,$reversetype)";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
            insertStat = insertStat.replace("$name", "" + RecordToInsert.getname());
            insertStat = insertStat.replace("$description", "" + RecordToInsert.getdescription());
            insertStat = insertStat.replace("$typecategory", "" + RecordToInsert.gettypecategory());
            insertStat = insertStat.replace("$typemaster", "" + RecordToInsert.gettypemaster());
            boolean temp = RecordToInsert.getreverseflag();
            if (temp == Boolean.TRUE) {
                insertStat = insertStat.replace("$reverseflag", "" + 1);
            } else {
                insertStat = insertStat.replace("$reverseflag", "" + 2);
            }
            boolean temp2 = RecordToInsert.getreversetype();
            if (temp2 == Boolean.TRUE) {
                insertStat = insertStat.replace("$reversetype", "" + 1);
            } else {
                insertStat = insertStat.replace("$reversetype", "" + 2);
            }
            try {
                msg = super.executeUpdate(insertStat);
                msg = msg + " Record Has Been Inserted";
                super.postUpdate("Add New Detailed TRANSACTION TYPE With Name " + RecordToInsert.getname(), false);
            } catch (Exception ex) {
                msg = "Please Enter A Unique Trans Type";
            }
        }
        return msg;
    }

    public String ValidateNullList(Object... obj) {
        List<transactiontypeDTOInter> AllRecords = (List<transactiontypeDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (transactiontypeDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getid() == 0) {
                Validate = Validate + "," + String.valueOf(i);
            }
            if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {
                Validate = Validate + "," + String.valueOf(i);
            }
            if (RecordToInsert.gettypecategory() == 0) {
                Validate = Validate + "," + String.valueOf(i);
            }
            if (RecordToInsert.gettypemaster() == 0) {
                Validate = Validate + "," + String.valueOf(i);
            }

            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = "Record With Index " + Validate + " Contain Null Name Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    public String ValidateNullDelete(Object... obj) {
        transactiontypeDTOInter RecordToInsert = (transactiontypeDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getid() == 0) {
            Validate = Validate + "   id  ";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required fields";
        }
        return Validate;
    }

    public String ValidateNull(Object... obj) {
        transactiontypeDTOInter RecordToInsert = (transactiontypeDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getid() == 0) {
            Validate = Validate + "   id  ";
        }
        if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {
            Validate = Validate + " Name";
        }
        if (RecordToInsert.gettypecategory() == 0) {
            Validate = Validate + " type category";
        }
        if (RecordToInsert.gettypemaster() == 0) {
            Validate = Validate + " type Master";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required fields";
        }
        return Validate;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactiontypeDTOInter RecordToDelete = (transactiontypeDTOInter) obj[0];
        String msg = ValidateNullDelete(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  ID= $id";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " Record Has Been deleted";
            super.postUpdate("Deleting A Detailed TRANSACTION TYPE With Name " + RecordToDelete.getname(), false);
        }
        return msg;
    }
//used in system rules

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        transactiontypeDTOInter RecordToSelect = (transactiontypeDTOInter) obj[0];
        String selectStat = "Select ID,NAME,DESCRIPTION,TYPE_CATEGORY,TYPE_MASTER,REVERSE_FLAG From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        transactiontypeDTOInter SelectedRecord = DTOFactory.createtransactiontypeDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setdescription(rs.getString("DESCRIPTION"));
            SelectedRecord.settypecategory(rs.getInt("TYPE_CATEGORY"));
            SelectedRecord.settypemaster(rs.getInt("TYPE_MASTER"));
            Integer temp = rs.getInt("REVERSE_FLAG");
            if (temp == 1) {
                SelectedRecord.setreverseflag(Boolean.TRUE);
            } else {
                SelectedRecord.setreverseflag(Boolean.FALSE);
            }
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        transactiontypemasterDTOInter RecordToSelect = (transactiontypemasterDTOInter) obj[0];
        String selectStat = "Select ID,NAME,DESCRIPTION,TYPE_CATEGORY,TYPE_MASTER,REVERSE_FLAG,REVERSE_Type From $table"
                + "  where  TYPE_MASTER= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<transactiontypeDTOInter> Records = new ArrayList<transactiontypeDTOInter>();
        while (rs.next()) {
            transactiontypeDTOInter SelectedRecord = DTOFactory.createtransactiontypeDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setdescription(rs.getString("DESCRIPTION"));
            SelectedRecord.settypecategory(rs.getInt("TYPE_CATEGORY"));
            SelectedRecord.settypemaster(rs.getInt("TYPE_MASTER"));
            Integer temp = rs.getInt("REVERSE_FLAG");
            if (temp == 1) {
                SelectedRecord.setreverseflag(Boolean.TRUE);
            } else {
                SelectedRecord.setreverseflag(Boolean.FALSE);
            }
            Integer temp2 = rs.getInt("REVERSE_Type");
            if (temp2 == 1) {
                SelectedRecord.setreversetype(Boolean.TRUE);
            } else {
                SelectedRecord.setreversetype(Boolean.FALSE);
            }
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<transactiontypeDTOInter> entities = (List<transactiontypeDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Transaction Type Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "update TRANSACTION_TYPE set  NAME = ?,DESCRIPTION =?,REVERSE_FLAG = ? ,TYPE_CATEGORY = ?,reverse_type = ?  where id = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        transactiontypeDTOInter RecordtoInsert = entities.get(i);

                        statement.setString(1, RecordtoInsert.getname());
                        statement.setString(2, RecordtoInsert.getdescription());
                        boolean temp = RecordtoInsert.getreverseflag();
                        if (temp == Boolean.TRUE) {
                            statement.setInt(3, 1);
                        } else {
                            statement.setInt(3, 2);
                        }
                        statement.setInt(4, RecordtoInsert.gettypecategory());
                        boolean temp2 = RecordtoInsert.getreversetype();
                        if (temp2 == Boolean.TRUE) {
                            statement.setInt(5, 1);
                        } else {
                            statement.setInt(5, 2);
                        }
                        statement.setInt(6, RecordtoInsert.getid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }

                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                            CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
