/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.BankCodeDTOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aly
 */
public class BankCodeDAO extends BaseDAO implements BankCodeDAOInter {

    @Override
    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "select id, bank from bank_code order by bank";
        ResultSet rs = executeQuery(selectStat);
        List<BankCodeDTOInter> Records = new ArrayList<BankCodeDTOInter>();
        while (rs.next()) {
            BankCodeDTOInter SelectedRecord = DTOFactory.createBankCodeDTO();
            SelectedRecord.setId(rs.getInt("id"));
            SelectedRecord.setBankname(rs.getString("bank"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

}
