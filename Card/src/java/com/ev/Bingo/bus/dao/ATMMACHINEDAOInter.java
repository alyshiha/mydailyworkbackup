/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.bus.dto.ATMMACHINECASHDTOINTER;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface ATMMACHINEDAOInter {

    Object insert(String ATMID, String ATMNAME) throws Throwable;

    List<ATMMACHINECASHDTOINTER> findRecord(String Name, String ID) throws Throwable;

    List<ATMMACHINECASHDTOINTER> findAll() throws Throwable;
    
    List<ATMMACHINECASHDTOINTER> findAllDistinct() throws Throwable;

    Object update(Object... obj) throws Throwable;

    String delete(Object... obj);

}
