/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.bus.dto.RejectedVisaDTOInter;
import java.util.List;

/**
 *
 * @author shi7a
 */
public interface RejectedVisaDAOInter {

    Object delete(Object... obj);

    List<RejectedVisaDTOInter> findAll() throws Throwable;

    Boolean findDuplicateRecord(String REPORTID) throws Throwable;

    Boolean findDuplicatesaveRecord(String REPORTID, Integer REJECTID) throws Throwable;

    List<RejectedVisaDTOInter> findRecord(String REPORTID) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;
    
}
