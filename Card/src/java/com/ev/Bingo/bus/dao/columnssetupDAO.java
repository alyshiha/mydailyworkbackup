package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.columnssetupDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class columnssetupDAO extends BaseDAO implements columnssetupDAOInter {

    protected columnssetupDAO() {
        super();
        super.setTableName("COLUMNS_SETUP");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String message = "";
        columnssetupDTOInter RecordToInsert = (columnssetupDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (ID,NAME) "
                + " values "
                + " (columns_setup_seq.nextval,'$name')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$name", "" + RecordToInsert.getname());
        try {
            message = (String) super.executeUpdate(insertStat);
            message = message + " Coloumn Has Been Inserted";
        } catch (Exception ex) {
            message = "Columns can't be Duplicated";
        }
        super.postUpdate("Adding New Column Record to Column Setup With Name " + RecordToInsert.getname(), false);
        return message;
    }

    public Boolean ValidateNull(Object... obj) {
        columnssetupDTOInter RecordToInsert = (columnssetupDTOInter) obj[0];
        if (RecordToInsert.getid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getname() == null && RecordToInsert.getname().equals("")) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        columnssetupDTOInter RecordToDelete = (columnssetupDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        String result = (String) super.executeUpdate(deleteStat);
        super.postUpdate(result + " Column Has Been Deleted", false);
        return result + " Column Has Been Deleted";
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,NAME From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<columnssetupDTOInter> Records = new ArrayList<columnssetupDTOInter>();
        while (rs.next()) {
            columnssetupDTOInter SelectedRecord = DTOFactory.createcolumnssetupDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String ValidateNullList(Object... obj) {
        List<columnssetupDTOInter> AllRecords = (List<columnssetupDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (columnssetupDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getid() == 0) {
                Validate = Validate + "," + String.valueOf(i);
            }
            if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {
                Validate = Validate + "," + String.valueOf(i);
            }
            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = "Record With Index " + Validate + " Contain Null Name Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<columnssetupDTOInter> entities = (List<columnssetupDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.isEmpty()) {
            mess = "No Columns Found To Be Updated";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "UPDATE COLUMNS_SETUP SET NAME = ? WHERE ID = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        columnssetupDTOInter RecordtoInsert = entities.get(i);
                        statement.setString(1, RecordtoInsert.getname());
                        statement.setInt(2, RecordtoInsert.getid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    try {
                        int[] numUpdates = statement.executeBatch();
                        Integer valid = 0, notvalid = 0;

                        for (int i = 0; i < numUpdates.length; i++) {
                            if (numUpdates[i] == -2) {
                                valid++;
                                mess = valid + " Rows Has Been Updated";
                            } else {
                                notvalid++;
                                mess2 = notvalid + " can`t be updated";
                            }
                        }
                        if (!mess2.equals("")) {
                            mess = mess + "," + mess2;
                        }
                    } catch (Exception ex) {
                        mess = "Columns can't be Duplicated";
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                  CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
