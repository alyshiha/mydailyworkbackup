package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import com.ev.Bingo.bus.dto.columnssetupDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.columnssetupdetailsDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class columnssetupdetailsDAO extends BaseDAO implements columnssetupdetailsDAOInter {

    protected columnssetupdetailsDAO() {
        super();
        super.setTableName("COLUMNS_SETUP_DETAILS");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String msg = "";
        columnssetupdetailsDTOInter RecordToInsert = (columnssetupdetailsDTOInter) obj[0];
        String insertStat = "insert into $table"
                + " (DISPLAY_COLUMN_ID,COLUMN_ID) "
                + " values "
                + " ($displaycolumnid,$columnid)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$displaycolumnid", "" + RecordToInsert.getdisplaycolumnid());
        insertStat = insertStat.replace("$columnid", "" + RecordToInsert.getcolumnid());
        try {
            msg = (String) super.executeUpdate(insertStat);
            msg = msg + " Column Has Been Inserted";
        } catch (Exception ex) {
            msg = "Columns can't be Duplicated";
        }
        super.postUpdate("Adding New Culumn Record With Id " + RecordToInsert.getcolumnid() + " Child Of " + RecordToInsert.getdisplaycolumnid(), false);
        return msg;
    }

    public Boolean ValidateNull(Object... obj) {
        columnssetupdetailsDTOInter RecordToInsert = (columnssetupdetailsDTOInter) obj[0];
        if (RecordToInsert.getdisplaycolumnid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getcolumnid() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        columnssetupDTOInter RecordToDelete = (columnssetupDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  DISPLAY_COLUMN_ID= $displaycolumnid";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$displaycolumnid", "" + RecordToDelete.getid());
        String result = (String) super.executeUpdate(deleteStat);
        super.postUpdate(result + " Detailed Column Has Been Deleted From Parent ID " + RecordToDelete.getid(), false);
        return result + " Detailed Column Has Been Deleted";
    }

    public Object deleterecorddetail(Object... obj) throws Throwable {
        super.preUpdate();
        columnssetupdetailsDTOInter RecordToDelete = (columnssetupdetailsDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  DISPLAY_COLUMN_ID= $displaycolumnid AND column_id = $columnid";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$displaycolumnid", "" + RecordToDelete.getdisplaycolumnid());
        deleteStat = deleteStat.replace("$columnid", "" + RecordToDelete.getcolumnid());
        String result = (String) super.executeUpdate(deleteStat);
        super.postUpdate(result + " Detailed Column Has Been Deleted From Parent ID " + RecordToDelete.getcolumnid() + " And Display ID = " + RecordToDelete.getdisplaycolumnid(), false);
        return result + " Detailed Column Has Been Deleted";
    }

    public Object findRecordsListCS(Object... obj) throws Throwable {
        super.preSelect();
        columnssetupDTOInter RecordToSelect = (columnssetupDTOInter) obj[0];
        String selectStat = "Select DISPLAY_COLUMN_ID,COLUMN_ID,rowid From $table"
                + "  where  DISPLAY_COLUMN_ID= $displaycolumnid";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$displaycolumnid", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<columnssetupdetailsDTOInter> Records = new ArrayList<columnssetupdetailsDTOInter>();
        while (rs.next()) {
            columnssetupdetailsDTOInter SelectedRecord = DTOFactory.createcolumnssetupdetailsDTO();
            SelectedRecord.setdisplaycolumnid(rs.getInt("DISPLAY_COLUMN_ID"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.setrowid(rs.getString("rowid"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String ValidateNullList(Object... obj) {
        List<columnssetupdetailsDTOInter> AllRecords = (List<columnssetupdetailsDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (columnssetupdetailsDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getcolumnid() == 0) {
                Validate = Validate + "," + String.valueOf(i);
            }
            if (RecordToInsert.getdisplaycolumnid() == 0) {
                Validate = Validate + "," + String.valueOf(i);
            }
            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = "Record With Index " + Validate + " Contain Null Card Number Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<columnssetupdetailsDTOInter> entities = (List<columnssetupdetailsDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.isEmpty()) {
            mess = "No Columns Found To Be Updated";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "UPDATE COLUMNS_SETUP_DETAILS SET COLUMN_ID = ? WHERE rowid = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        columnssetupdetailsDTOInter RecordtoInsert = entities.get(i);
                        statement.setInt(1, RecordtoInsert.getcolumnid());
                        statement.setString(2, RecordtoInsert.getrowid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    try {
                        int[] numUpdates = statement.executeBatch();
                        Integer valid = 0, notvalid = 0;

                        for (int i = 0; i < numUpdates.length; i++) {
                            if (numUpdates[i] == -2) {
                                valid++;
                                mess = valid + " rows has been updated";
                            } else {
                                notvalid++;
                                mess2 = notvalid + " can`t be updated";
                            }
                        }
                        if (!mess2.equals("")) {
                            mess = mess + "," + mess2;
                        }
                    } catch (Exception ex) {
                        mess = "Columns can't be Duplicated";
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
