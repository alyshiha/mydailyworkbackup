package com.ev.Bingo.bus.dao;

public class DAOFactory {

    public DAOFactory() {
    }

    public static ProfitReportDAOInter createProfitReportDAO(Object... obj) {
        return new ProfitReportDAO();
    }

    public static VisaDAOInter createvisaDAO(Object... obj) {
        return new VisaDAO();
    }

    public static masterDAOInter createmasterDAO(Object... obj) {
        return new masterDAO();
    }

    public static settlementsDAOInter createsettlementsDAO(Object... obj) {
        return new settlementsDAO();
    }

    public static RejectedVisaDAOInter createRejectedVisaDAO(Object... obj) {
        return new RejectedVisaDAO();
    }

    public static HolidaysDAOInter createHolidaysDAO(Object... obj) {
        return new HolidaysDAO();
    }

    public static DiffCardAtmDAOInter createDiffCardAtmDAO(Object... obj) {
        return new DiffCardAtmDAO();
    }

    public static AtmJournalDAOInter createAtmJournalDAO(Object... obj) {
        return new AtmJournalDAO();
    }

    public static LogErrorDAOInter createLogErrorDAO(Object... obj) {
        return new LogErrorDAO();
    }

    public static ATMMACHINEDAOInter createATMMACHINEDAO(Object... obj) {
        return new ATMMACHINEDAO();
    }

    public static AccountNameDAOInter createAccountNameDAO(Object... obj) {
        return new AccountNameDAO();
    }

    public static Col4DAOInter createCol4DAO(Object... obj) {
        return new Col4DAO();
    }

    public static userPrivilegeDAOInter createuserPrivilegeDAO(Object... obj) {
        return new userPrivilegeDAO();
    }

    public static AtmAccountsDAOInter createAtmAccountsDAO(Object... obj) {
        return new AtmAccountsDAO();
    }

    public static NetworkTypeCodeDAOInter createNetworkTypeCodeDAO(Object... obj) {
        return new NetworkTypeCodeDAO();
    }

    public static BankCodeDAOInter createBankCodeDAO(Object... obj) {
        return new BankCodeDAO();
    }

    public static privilegeStatusNameDAOInter createprivilegeStatusName(Object... obj) {
        return new privilegeStatusNameDAO();
    }

    public static CashManagmentLogDAOInter createCashManagmentLogDAO(Object... obj) {
        return new CashManagmentLogDAO();
    }

    public static commissionfeesDAOInter createcommissionfeesDAO(Object... obj) {
        return new commissionfeesDAO();
    }

    public static commissionfeesdetailDAOInter createcommissionfeesdetailDAO(Object... obj) {
        return new commissionfeesdetailDAO();
    }

    public static cardfiletemplatedetailDAOInter createcardfiletemplatedetailDAO(Object... obj) {
        return new cardfiletemplatedetailDAO();
    }

    public static hostfiletemplatedetailDAOInter createhostfiletemplatedetailDAO(Object... obj) {
        return new hostfiletemplatedetailDAO();
    }

    public static switchfiletemplatedetailDAOInter createswitchfiletemplatedetailDAO(Object... obj) {
        return new switchfiletemplatedetailDAO();
    }

    public static hostfiletemplateDAOInter createhostfiletemplateDAO(Object... obj) {
        return new hostfiletemplateDAO();
    }

    public static switchfiletemplateDAOInter createswitchfiletemplateDAO(Object... obj) {
        return new switchfiletemplateDAO();
    }

    public static cardfiletemplateDAOInter createnetworkfiletemplateDAO(Object... obj) {
        return new cardfiletemplateDAO();
    }

    public static filetemplateheaderDAOInter createfiletemplateheaderDAO(Object... obj) {
        return new filetemplateheaderDAO();
    }

    public static switchfiletemplateheaderDAOInter createswitchfiletemplateheaderDAO(Object... obj) {
        return new switchfiletemplateheaderDAO();
    }

    public static networkfiletemplateheaderDAOInter createnetworkfiletemplateheaderDAO(Object... obj) {
        return new networkfiletemplateheaderDAO();
    }

    public static transactionstatisticsrepDAOInter createtransactionstatisticsrepDAO(Object... obj) {
        return new transactionstatisticsrepDAO();
    }

    public static diffamountreportDAOInter creatediffamountreportDAO(Object... obj) {
        return new diffamountreportDAO();
    }

    public static transactionmanagementDAOInter createtransactionmanagementDAO(Object... obj) {
        return new transactionmanagementDAO();
    }

    public static UserActivtyDAOInter createUserActivtyDAO(Object... obj) {
        return new UserActivtyDAO();
    }

    public static matchingtypeDAOInter creatematchingtypeDAO(Object... obj) {
        return new matchingtypeDAO();
    }

    public static DAshBoardDAOInter createDAshBoardDAO(Object... obj) {
        return new DAshBoardDAO();
    }

    public static networkdefaultcolumnsDAOInter createnetworkdefaultcolumnsDAO(Object... obj) {
        return new networkdefaultcolumnsDAO();
    }

    public static DiffAmountDAOInter createDiffAmountDAO(Object... obj) {
        return new DiffAmountDAO();
    }

    public static settlementlogDAOInter createsettlementlogDAO(Object... obj) {
        return new settlementlogDAO();
    }

    public static transsearchDAOInter createtranssearchDAO(Object... obj) {
        return new transsearchDAO();
    }

    public static atmfileDAOInter createatmfileDAO(Object... obj) {
        return new atmfileDAO();
    }

    public static UserNetworkDAOInter createUserNetworkDAO(Object... obj) {
        return new UserNetworkDAO();
    }

    public static matchingerrorsdataDAOInter creatematchingerrorsdataDAO(Object... obj) {
        return new matchingerrorsdataDAO();
    }

    public static filecolumndefinitionDAOInter createfilecolumndefinitionDAO(Object... obj) {
        return new filecolumndefinitionDAO();
    }

    public static disputerulesDAOInter createdisputerulesDAO(Object... obj) {
        return new disputerulesDAO();
    }

    public static transactionstatusDAOInter createtransactionstatusDAO(Object... obj) {
        return new transactionstatusDAO();
    }

    public static validationrulesDAOInter createvalidationrulesDAO(Object... obj) {
        return new validationrulesDAO();
    }

    public static networksDAOInter createnetworksDAO(Object... obj) {
        return new networksDAO();
    }

    public static bingologDAOInter createbingologDAO(Object... obj) {
        return new bingologDAO();
    }

    public static columnssetupdetailsDAOInter createcolumnssetupdetailsDAO(Object... obj) {
        return new columnssetupdetailsDAO();
    }

    public static columnssetupDAOInter createcolumnssetupDAO(Object... obj) {
        return new columnssetupDAO();
    }

    public static duplicatetransactionsDAOInter createduplicatetransactionsDAO(Object... obj) {
        return new duplicatetransactionsDAO();
    }

    public static licenseDAOInter createlicenseDAO(Object... obj) {
        return new licenseDAO();
    }

    public static copyfilesDAOInter createcopyfilesDAO(Object... obj) {
        return new copyfilesDAO();
    }

    public static JobsStatusDAOInter createJobsStatusDAO(Object obj) {
        return new JobsStatusDAO();
    }

    public static FilesToMoveDAOInter createFilesToMoveDAO(Object obj) {
        return new FilesToMoveDAO();
    }

    public static currencyDAOInter createcurrencyDAO(Object... obj) {
        return new currencyDAO();
    }

    public static currencymasterDAOInter createcurrencymasterDAO(Object... obj) {
        return new currencymasterDAO();
    }

    public static blacklistedcardDAOInter createblacklistedcardDAO(Object... obj) {
        return new blacklistedcardDAO();
    }

    public static transactionresponsecodeDAOInter createtransactionresponsecodeDAO(Object... obj) {
        return new transactionresponsecodeDAO();
    }

    public static transactiontypemasterDAOInter createtransactiontypemasterDAO(Object... obj) {
        return new transactiontypemasterDAO();
    }

    public static transactiontypeDAOInter createtransactiontypeDAO(Object... obj) {
        return new transactiontypeDAO();
    }

    public static transactionresponsemasterDAOInter createtransactionresponsemasterDAO(Object... obj) {
        return new transactionresponsemasterDAO();
    }

    public static blockedusersDAOInter createblockedusersDAO(Object... obj) {
        return new blockedusersDAO();
    }

    public static profileDAOInter createprofileDAO(Object... obj) {
        return new profileDAO();
    }

    public static blockreasonsDAOInter createblockreasonsDAO(Object... obj) {
        return new blockreasonsDAO();
    }

    public static userpassDAOInter createuserpassDAO(Object... obj) {
        return new userpassDAO();
    }

    public static LoginDAOInter createloginDAO(Object... obj) {
        return new LoginDAO();
    }

    public static duplicatesettingdetailDAOInter createduplicatesettingdetailDAO(Object... obj) {
        return new duplicatesettingdetailDAO();
    }

    public static duplicatesettingmasterDAOInter createduplicatesettingmasterDAO(Object... obj) {
        return new duplicatesettingmasterDAO();
    }

    public static usersDAOInter createusersDAO(Object... obj) {
        return new usersDAO();
    }

    public static networkcodesDAOInter createnetworkcodesDAO(Object... obj) {
        return new networkcodesDAO();
    }

    public static profilemenuDAOInter createprofilemenuDAO(Object... obj) {
        return new profilemenuDAO();
    }

    public static systemtableDAOInter createsystemtableDAO(Object... obj) {
        return new systemtableDAO();
    }

    public static menulabelsDAOInter createmenulabelsDAO(Object... obj) {
        return new menulabelsDAO();
    }

    public static userprofileDAOInter createuserprofileDAO(Object... obj) {
        return new userprofileDAO();
    }
}
