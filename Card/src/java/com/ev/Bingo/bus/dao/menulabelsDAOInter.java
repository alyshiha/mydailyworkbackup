package com.ev.Bingo.bus.dao;

import java.util.Date;
import java.util.List;
import java.sql.SQLException;

public interface menulabelsDAOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object findRecordsList(Object... obj) throws Throwable;

    Object findPageRecordsList(Object... obj) throws Throwable;

    Object findParentRecordsList(Object... obj) throws Throwable;

    Object findRecordsAll(Object... obj) throws Throwable;
    Object findRecordsAll2(Object... obj) throws Throwable ;

    String save(Object... obj) throws SQLException, Throwable;

    Object findmenu(int user) throws Throwable;
Boolean findmenuitemcheck(int user,String Page) throws Throwable;
    Object findParents(int profileId) throws Throwable;

    Object findParents() throws Throwable;
}
