/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import java.sql.CallableStatement;

import java.sql.Connection;
 
import com.ev.Bingo.base.util.DateFormatter;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class LoginDAO extends BaseDAO implements LoginDAOInter {

    public void SaveLogSeesion() throws Throwable {
        super.SaveLogSesion();
    }

    public String ValidateLog(String userName, String password, String version, Integer NumOfUsers, Date EndDate) throws Throwable {
        try {
            String sql = "{ ? = call login_validation(?,?,?,?,to_date(?,'dd.mm.yyyy hh24:mi:ss')) }";
           Connection conn = CoonectionHandler.getInstance().getConnection();
            CallableStatement statement = conn.prepareCall(sql);
            statement.registerOutParameter(1, java.sql.Types.VARCHAR);
            statement.setString(2, "" + userName);
            statement.setString(3, "" + password);
            statement.setString(4, "" + version);
            statement.setInt(5, NumOfUsers);
            statement.setString(6, "" + DateFormatter.changeDateAndTimeFormat(EndDate));
            Boolean check = statement.execute();
            String id = statement.getString(1);
            statement.close();
            CoonectionHandler.getInstance().returnConnection(conn);
            return "" + id;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "";
    }

    public String ValidateLogold(String userName, String password, String version, Integer NumOfUsers, String EndDate) throws Throwable {
        //CreateFiles cf = new CreateFiles();
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select login_validation('$u_name','$u_pass','$Runningversion',$NumOfUsers,to_date('$enddate','YY mm dd')) from dual";
        selectStat = selectStat.replace("$u_name", "" + userName);
        selectStat = selectStat.replace("$u_pass", "" + password);
        selectStat = selectStat.replace("$Runningversion", "" + version);
        selectStat = selectStat.replace("$NumOfUsers", "" + NumOfUsers);
        selectStat = selectStat.replace("$enddate", "" + EndDate);
        ResultSet rs = executeQuery(selectStat);
        String Result = "";
        while (rs.next()) {
            Result = rs.getString(1);

        }
        super.postSelect(rs);
        return Result;
    }
}
