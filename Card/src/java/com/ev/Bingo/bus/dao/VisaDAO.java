/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.settlementsDTOInter;
import com.ev.Bingo.bus.dto.timeFromSwitchDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author shi7a
 */
public class VisaDAO extends BaseDAO implements VisaDAOInter {

    @Override
    public Object findRecordsIssuer(Date dateFrom, Date dateTo) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT   'Matched' Title,\n"
                + "         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                           1,\n"
                + "                           NVL (AMOUNT, 0),\n"
                + "                           2,\n"
                + "                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "            AMOUNT,\n"
                + "         COUNT (1) NO,\n"
                + "         1 ordering\n"
                + "  FROM   MATCHED_DATA\n"
                + " WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ($P{DateTo},\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND record_type = 1\n"
                + "         AND network_id = 1\n"
                + "         AND CARD_NO NOT LIKE '411739%'\n"
                + "UNION\n"
                + "SELECT   'Unmatched Switch' Title,\n"
                + "         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                           1,\n"
                + "                           NVL (AMOUNT, 0),\n"
                + "                           2,\n"
                + "                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "            AMOUNT,\n"
                + "         COUNT (1) NO,\n"
                + "         2 ordering\n"
                + "  FROM   disputes\n"
                + " WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ($P{DateTo},\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND record_type = 2\n"
                + "         AND network_id = 1\n"
                + "         AND CARD_NO NOT LIKE '411739%'\n"
                + "UNION\n"
                + "SELECT   'Unmatched Network' Title,\n"
                + "         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                           1,\n"
                + "                           NVL (AMOUNT, 0),\n"
                + "                           2,\n"
                + "                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "            AMOUNT,\n"
                + "         COUNT (1) NO,\n"
                + "         3 ordering\n"
                + "  FROM   disputes\n"
                + " WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ($P{DateTo},\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND record_type = 1\n"
                + "         AND network_id = 1\n"
                + "         AND CARD_NO NOT LIKE '411739%'\n"
                + "UNION\n"
                + "SELECT   'Pre-paid Matched' Title,\n"
                + "         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                           1,\n"
                + "                           NVL (AMOUNT, 0),\n"
                + "                           2,\n"
                + "                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "            AMOUNT,\n"
                + "         COUNT (1) NO,\n"
                + "         4 ordering\n"
                + "  FROM   matched_data\n"
                + " WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ($P{DateTo},\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND record_type = 1\n"
                + "         AND network_id = 1\n"
                + "         AND CARD_NO LIKE '411739%'\n"
                + "UNION\n"
                + "SELECT   'Pre-paid Unmatched Switch' Title,\n"
                + "         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                           1,\n"
                + "                           NVL (AMOUNT, 0),\n"
                + "                           2,\n"
                + "                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "            AMOUNT,\n"
                + "         COUNT (1) NO,\n"
                + "         5 ordering\n"
                + "  FROM   disputes\n"
                + " WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ($P{DateTo},\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND record_type = 2\n"
                + "         AND network_id = 1\n"
                + "         AND CARD_NO LIKE '411739%'\n"
                + "UNION\n"
                + "SELECT   'Pre-paid Unmatched Network' Title,\n"
                + "         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                           1,\n"
                + "                           NVL (AMOUNT, 0),\n"
                + "                           2,\n"
                + "                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "            AMOUNT,\n"
                + "         COUNT (1) NO,\n"
                + "         6 ordering\n"
                + "  FROM   disputes\n"
                + " WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ($P{DateTo},\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND record_type = 1\n"
                + "         AND network_id = 1\n"
                + "         AND CARD_NO LIKE '411739%'\n"
                + "ORDER BY   ordering";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$P{DateFrom}", "'" + DateFormatter.changeDateAndTimeFormat(dateFrom) + "'");
        selectStat = selectStat.replace("$P{DateTo}", "'" + DateFormatter.changeDateAndTimeFormat(dateTo) + "'");

        ResultSet rs = executeQuery(selectStat);
        List<settlementsDTOInter> Records = new ArrayList<settlementsDTOInter>();
        while (rs.next()) {
            settlementsDTOInter SelectedRecord = DTOFactory.createsettlementsDTO();
            SelectedRecord.setTitle(rs.getString("Title"));
            SelectedRecord.setAmount(rs.getBigDecimal("AMOUNT"));
            SelectedRecord.setCount(rs.getInt("NO"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findRecordsAcquier(Date dateFrom, Date dateTo) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT   'Matched' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           1 ordering,\n"
                + "           currency\n"
                + "    FROM   matched_data\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 6\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT   'Unmatched' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           2 ordering,\n"
                + "           currency\n"
                + "    FROM   disputes\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 6\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT   'Report 601' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           3 ordering,\n"
                + "           currency\n"
                + "    FROM   rejected_network\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND network_id = 6\n"
                + "           AND report_id LIKE '%601%'\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT   'Report 611' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           4 ordering,\n"
                + "           currency\n"
                + "    FROM   rejected_network\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND network_id = 6\n"
                + "           AND report_id LIKE '%611%'\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT   'Report 613' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           5 ordering,\n"
                + "           currency\n"
                + "    FROM   rejected_network\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND network_id = 6\n"
                + "           AND report_id LIKE '%613%'\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT   'Report 615' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           6 ordering,\n"
                + "           currency\n"
                + "    FROM   rejected_network\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND network_id = 6\n"
                + "           AND report_id LIKE '%615%'\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT   'Trx.Code 112' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           7 ordering,\n"
                + "           currency\n"
                + "    FROM   rejected_switch\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND network_id = 6\n"
                + "           AND RESPONSE_CODE = '112'\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT   'Total Switch' Title,\n"
                + "           SUM (amount) Amount,\n"
                + "           SUM (no) No,\n"
                + "           8 ordering,\n"
                + "           currency\n"
                + "    FROM   (  SELECT   NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                                         1,\n"
                + "                                         NVL (AMOUNT, 0),\n"
                + "                                         2,\n"
                + "                                         -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "                          Amount, COUNT (1) No, currency\n"
                + "                FROM   disputes\n"
                + "               WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    $P{DateTo},\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )\n"
                + "                       AND record_type = 2\n"
                + "                       AND network_id = 6\n"
                + "            GROUP BY   currency\n"
                + "            UNION\n"
                + "              SELECT   NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                                         1,\n"
                + "                                         NVL (AMOUNT, 0),\n"
                + "                                         2,\n"
                + "                                         -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "                          Amount, COUNT (1) No, currency\n"
                + "                FROM   matched_data\n"
                + "               WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    $P{DateTo},\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )\n"
                + "                       AND record_type = 2\n"
                + "                       AND network_id = 6\n"
                + "            GROUP BY   currency\n"
                + "            UNION\n"
                + "              SELECT   NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                                         1,\n"
                + "                                         NVL (AMOUNT, 0),\n"
                + "                                         2,\n"
                + "                                         -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "                          Amount, COUNT (1) No, currency\n"
                + "                FROM   rejected_switch\n"
                + "               WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    $P{DateTo},\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )\n"
                + "                       AND network_id = 6\n"
                + "            GROUP BY   currency)\n"
                + "GROUP BY   currency\n"
                + "ORDER BY   ordering";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$P{DateFrom}", "'" + DateFormatter.changeDateAndTimeFormat(dateFrom) + "'");
        selectStat = selectStat.replace("$P{DateTo}", "'" + DateFormatter.changeDateAndTimeFormat(dateTo) + "'");

        ResultSet rs = executeQuery(selectStat);
        List<settlementsDTOInter> Records = new ArrayList<settlementsDTOInter>();
        while (rs.next()) {
            settlementsDTOInter SelectedRecord = DTOFactory.createsettlementsDTO();
            SelectedRecord.setTitle(rs.getString("Title"));
            SelectedRecord.setAmount(rs.getBigDecimal("Amount"));
            SelectedRecord.setPan(rs.getString("currency"));
            SelectedRecord.setCount(rs.getInt("No"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findRecordstimefromswitch(Date dateFrom, Date dateTo) throws Throwable {
        super.preSelect();
        String selectStat = "  SELECT   SUM (AMOUNT) SAMOUNT, COUNT (1) NO, currency\n"
                + "    FROM   DISPUTES\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND RECORD_TYPE = 2\n"
                + "           AND network_id = 6\n"
                + "GROUP BY   currency";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$P{DateFrom}", "'" + DateFormatter.changeDateAndTimeFormat(dateFrom) + "'");
        selectStat = selectStat.replace("$P{DateTo}", "'" + DateFormatter.changeDateAndTimeFormat(dateTo) + "'");

        ResultSet rs = executeQuery(selectStat);
        List<timeFromSwitchDTOInter> Records = new ArrayList<timeFromSwitchDTOInter>();
        while (rs.next()) {
            timeFromSwitchDTOInter SelectedRecord = DTOFactory.createtimeFromSwitchDTO();
            SelectedRecord.setCurrency(rs.getString("currency"));
            SelectedRecord.setAmount(rs.getBigDecimal("SAMOUNT"));
            SelectedRecord.setCount(rs.getInt("NO"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findRecordscommision(Date dateFrom, Date dateTo) throws Throwable {
        super.preSelect();
        String selectStat = "  SELECT   SUM (amount) SSUM,\n"
                + "           SUM (NO) SNO,\n"
                + "           Title,\n"
                + "           TYPE\n"
                + "    FROM   (  SELECT   'ISSUER' Title,\n"
                + "                       NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                                         1,\n"
                + "                                         NVL (AMOUNT, 0),\n"
                + "                                         2,\n"
                + "                                         -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "                          AMOUNT,\n"
                + "                       COUNT (1) NO,\n"
                + "                       DECODE (SETTLEMENT_CURRENCY,\n"
                + "                               'EGP', 'Domestic',\n"
                + "                               'International')\n"
                + "                          TYPE\n"
                + "                FROM   disputes\n"
                + "               WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    $P{DateTo},\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )\n"
                + "                       AND record_type = 1\n"
                + "                       AND network_id = 1\n"
                + "            GROUP BY   SETTLEMENT_CURRENCY\n"
                + "            UNION\n"
                + "              SELECT   'ISSUER' Title,\n"
                + "                       NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                                         1,\n"
                + "                                         NVL (AMOUNT, 0),\n"
                + "                                         2,\n"
                + "                                         -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "                          AMOUNT,\n"
                + "                       COUNT (1) NO,\n"
                + "                       DECODE (SETTLEMENT_CURRENCY,\n"
                + "                               'EGP', 'Domestic',\n"
                + "                               'International')\n"
                + "                          TYPE\n"
                + "                FROM   MATCHED_DATA\n"
                + "               WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    $P{DateTo},\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )\n"
                + "                       AND record_type = 1\n"
                + "                       AND network_id = 1\n"
                + "            GROUP BY   SETTLEMENT_CURRENCY\n"
                + "            UNION\n"
                + "              SELECT   'ISSUER' Title,\n"
                + "                       NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                                         1,\n"
                + "                                         NVL (AMOUNT, 0),\n"
                + "                                         2,\n"
                + "                                         -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "                          AMOUNT,\n"
                + "                       COUNT (1) NO,\n"
                + "                       DECODE (SETTLEMENT_CURRENCY,\n"
                + "                               'EGP', 'Domestic',\n"
                + "                               'International')\n"
                + "                          TYPE\n"
                + "                FROM   rejected_network\n"
                + "               WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    $P{DateTo},\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )\n"
                + "                       AND network_id = 1\n"
                + "            GROUP BY   SETTLEMENT_CURRENCY\n"
                + "            UNION\n"
                + "              SELECT   'ACQUIRER' Title,\n"
                + "                       NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                                         1,\n"
                + "                                         NVL (AMOUNT, 0),\n"
                + "                                         2,\n"
                + "                                         -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "                          AMOUNT,\n"
                + "                       COUNT (1) NO,\n"
                + "                       DECODE (SETTLEMENT_CURRENCY,\n"
                + "                               'EGP', 'Domestic',\n"
                + "                               'International')\n"
                + "                          TYPE\n"
                + "                FROM   disputes\n"
                + "               WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    $P{DateTo},\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )\n"
                + "                       AND record_type = 1\n"
                + "                       AND network_id = 6\n"
                + "            GROUP BY   SETTLEMENT_CURRENCY\n"
                + "            UNION\n"
                + "              SELECT   'ACQUIRER' Title,\n"
                + "                       NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                                         1,\n"
                + "                                         NVL (AMOUNT, 0),\n"
                + "                                         2,\n"
                + "                                         -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "                          AMOUNT,\n"
                + "                       COUNT (1) NO,\n"
                + "                       DECODE (SETTLEMENT_CURRENCY,\n"
                + "                               'EGP', 'Domestic',\n"
                + "                               'International')\n"
                + "                          TYPE\n"
                + "                FROM   MATCHED_DATA\n"
                + "               WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    $P{DateTo},\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )\n"
                + "                       AND record_type = 1\n"
                + "                       AND network_id = 6\n"
                + "            GROUP BY   SETTLEMENT_CURRENCY\n"
                + "            UNION\n"
                + "              SELECT   'ACQUIRER' Title,\n"
                + "                       NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                                         1,\n"
                + "                                         NVL (AMOUNT, 0),\n"
                + "                                         2,\n"
                + "                                         -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "                          AMOUNT,\n"
                + "                       COUNT (1) NO,\n"
                + "                       DECODE (SETTLEMENT_CURRENCY,\n"
                + "                               'EGP', 'Domestic',\n"
                + "                               'International')\n"
                + "                          TYPE\n"
                + "                FROM   rejected_network\n"
                + "               WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    $P{DateTo},\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )\n"
                + "                       AND network_id = 6\n"
                + "            GROUP BY   SETTLEMENT_CURRENCY)\n"
                + "GROUP BY   Title, TYPE";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$P{DateFrom}", "'" + DateFormatter.changeDateAndTimeFormat(dateFrom) + "'");
        selectStat = selectStat.replace("$P{DateTo}", "'" + DateFormatter.changeDateAndTimeFormat(dateTo) + "'");

        ResultSet rs = executeQuery(selectStat);
        List<timeFromSwitchDTOInter> Records = new ArrayList<timeFromSwitchDTOInter>();
        while (rs.next()) {
            timeFromSwitchDTOInter SelectedRecord = DTOFactory.createtimeFromSwitchDTO();
            SelectedRecord.setCurrency(rs.getString("TYPE"));
            SelectedRecord.setAcq(rs.getString("Title"));
            SelectedRecord.setAmount(rs.getBigDecimal("SSUM"));
            SelectedRecord.setCount(rs.getInt("SNO"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

}
