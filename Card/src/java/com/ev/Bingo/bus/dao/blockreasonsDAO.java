package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.blockreasonsDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class blockreasonsDAO extends BaseDAO implements blockreasonsDAOInter {

    protected blockreasonsDAO() {
        super();
        super.setTableName("BLOCK_REASONS");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        blockreasonsDTOInter RecordToInsert = (blockreasonsDTOInter) obj[0];
        RecordToInsert.setreasonid(super.generateSequence(super.getTableName()));
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
            String insertStat = "insert into $table"
                    + " (REASON_ID,REASON_NAME,REASON_A_NAME) "
                    + " values "
                    + " ($reasonid,'$reasonname','$reasonaname')";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$reasonid", "" + RecordToInsert.getreasonid());
            insertStat = insertStat.replace("$reasonname", "" + RecordToInsert.getreasonname());
            insertStat = insertStat.replace("$reasonaname", "" + RecordToInsert.getreasonaname());
            msg = super.executeUpdate(insertStat);
            msg = msg + " Record Has Been Inserted";
            super.postUpdate("Add New Reasom To BLOCK REASONS With Value " + RecordToInsert.getreasonname(), false);
        }
        return msg;
    }

    public String ValidateNullList(Object... obj) {
        List<blockreasonsDTOInter> AllRecords = (List<blockreasonsDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (blockreasonsDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getreasonid() == 0) {
                Validate = Validate + "," + String.valueOf(i);
            }
            if (RecordToInsert.getreasonname().equals("")) {
                Validate = Validate + "," + String.valueOf(i);
            }
            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = "Record With Index " + Validate + " Contain Null Reason Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    public String ValidateNull(Object... obj) {
        blockreasonsDTOInter RecordToInsert = (blockreasonsDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getreasonid() == 0) {
            Validate = Validate + " reasonid  ";
        }
        if (RecordToInsert.getreasonname().equals("")) {
            Validate = Validate + " Name  ";
        }
        if (!Validate.equals("Validate")) {
            Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        blockreasonsDTOInter RecordToDelete = (blockreasonsDTOInter) obj[0];
        String msg = ValidateNull(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  REASON_ID= $reasonid";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$reasonid", "" + RecordToDelete.getreasonid());
            deleteStat = deleteStat.replace("$reasonname", "" + RecordToDelete.getreasonname());
            deleteStat = deleteStat.replace("$reasonaname", "" + RecordToDelete.getreasonaname());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " Record Has Been deleted";
            super.postUpdate("Delete Reason from BLOCK REASONS With Value " + RecordToDelete.getreasonname(), false);
        }
        return msg;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select REASON_ID,REASON_NAME,REASON_A_NAME From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<blockreasonsDTOInter> Records = new ArrayList<blockreasonsDTOInter>();
        while (rs.next()) {
            blockreasonsDTOInter SelectedRecord = DTOFactory.createblockreasonsDTO();
            SelectedRecord.setreasonid(rs.getInt("REASON_ID"));
            SelectedRecord.setreasonname(rs.getString("REASON_NAME"));
            SelectedRecord.setreasonaname(rs.getString("REASON_A_NAME"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<blockreasonsDTOInter> entities = (List<blockreasonsDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Users Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "update BLOCK_REASONS set REASON_NAME = ?,REASON_A_NAME = ? where REASON_ID = ? ";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                   connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        blockreasonsDTOInter RecordtoInsert = entities.get(i);
                        statement.setString(1, RecordtoInsert.getreasonname());
                        statement.setString(2, RecordtoInsert.getreasonaname());
                        statement.setInt(3, RecordtoInsert.getreasonid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
