package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

public interface blockedusersDAOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object findRecordsAll(Object... obj) throws Throwable;

    Object findByReasonID(Object... obj) throws Throwable;
}
