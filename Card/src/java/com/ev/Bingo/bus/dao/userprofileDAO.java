package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;

import java.util.ArrayList;
import com.ev.Bingo.bus.dto.userprofileDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class userprofileDAO extends BaseDAO implements userprofileDAOInter {

    protected userprofileDAO() {
        super();
        super.setTableName("USER_PROFILE");
    }
//userd in user mangement

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        userprofileDTOInter RecordToInsert = (userprofileDTOInter) obj[0];
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
//RecordToInsert.setid(super.generateSequence(super.getTableName()));
            String insertStat = "insert into $table"
                    + " (USER_ID,PROFILE_ID) "
                    + " values "
                    + " ($userid,$profileid)";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$userid", "" + RecordToInsert.getuserid());
            insertStat = insertStat.replace("$profileid", "" + RecordToInsert.getprofileid());
            msg = super.executeUpdate(insertStat);
            msg = msg + " profile has been assigned to user";
            super.postUpdate("Adding New User ID " + RecordToInsert.getuserid() + " Record To Profile ID " + RecordToInsert.getprofileid(), false);
        }
        return msg;
    }

    public String ValidateNullList(Object... obj) {
        List<userprofileDTOInter> AllRecords = (List<userprofileDTOInter>) obj[0];
        String Validate = "Validate";
        for (userprofileDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getprofileid() == 0) {
                Validate = Validate + " Feild profile is a requiered field";
            }
            if (RecordToInsert.getusername() == null) {
                Validate = Validate + " Feild user is a requiered field";
            }
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public String ValidateNull(Object... obj) {
        userprofileDTOInter RecordToInsert = (userprofileDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getprofileid() == 0) {
            Validate = Validate + " Feild profileid is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        userprofileDTOInter RecordToDelete = (userprofileDTOInter) obj[0];
        String msg = ValidateNull(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table where profile_id = '$profileid'";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$userid", "" + RecordToDelete.getuserid());
            deleteStat = deleteStat.replace("$profileid", "" + RecordToDelete.getprofileid());
            msg = super.executeUpdate(deleteStat);
            super.postUpdate("Delete Record In Table USER_PROFILE", false);
        }
        return msg;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        usersDTOInter RecordToSelect = (usersDTOInter) obj[0];
        String selectStat = "Select USER_ID,PROFILE_ID From $table"
                + "  where  USER_ID = $USERID";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$USERID", "" + RecordToSelect.getUserid());
        ResultSet rs = executeQuery(selectStat);
        userprofileDTOInter SelectedRecord = DTOFactory.createuserprofileDTO();
        while (rs.next()) {
            SelectedRecord.setuserid(rs.getInt("USER_ID"));
            SelectedRecord.setprofileid(rs.getInt("PROFILE_ID"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<userprofileDTOInter> entities = (List<userprofileDTOInter>) obj[0];
        String profile = (String) obj[1];
        String userss = "";
        for (userprofileDTOInter entity : entities) {
            userss = userss + entity.getusername() + " ";
        }
        super.postUpdate("Profile " + profile + " has been update to new set of users (" + userss + ")".replace("()", "(No Users)"));
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Users Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "insert into USER_PROFILE (USER_ID,PROFILE_ID) values ((select n.user_id from users n where n.logon_name = ?),?)";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        userprofileDTOInter RecordtoInsert = entities.get(i);
                        statement.setString(1, RecordtoInsert.getusername());
                        statement.setInt(2, RecordtoInsert.getprofileid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " User Has Been Assigned";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }

                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                            CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
