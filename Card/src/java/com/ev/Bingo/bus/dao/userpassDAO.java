package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.userpassDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class userpassDAO extends BaseDAO implements userpassDAOInter {

    protected userpassDAO() {
        super();
        super.setTableName("USER_PASS");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        userpassDTOInter RecordToInsert = (userpassDTOInter) obj[0];
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
//RecordToInsert.setid(super.generateSequence(super.getTableName()));
            String insertStat = "insert into $table"
                    + " (USER_ID,PASS,CHANGE_DATE) "
                    + " values "
                    + " ($userid,conv.conv_string('$pass'),SYSDATE)";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$userid", "" + RecordToInsert.getuserid());
            insertStat = insertStat.replace("$pass", "" + RecordToInsert.getpass());
            insertStat = insertStat.replace("$changedate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.getchangedate()));
            msg = super.executeUpdate(insertStat);
            msg = msg + " Row Has Been Inserted";
            super.postUpdate("Add New Password To USER " + RecordToInsert.getuserid(), false);
        }
        return msg;
    }

    public String ValidateNull(Object... obj) {
        userpassDTOInter RecordToInsert = (userpassDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getuserid() == 0) {
            Validate = Validate + "   userid  ";
        }
        if (RecordToInsert.getpass() == null && RecordToInsert.getpass().equals("")) {
            Validate = Validate + "   pass  ";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required fields";
        }
        return Validate;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        userpassDTOInter RecordToSelect = (userpassDTOInter) obj[0];
        String selectStat = "Select USER_ID,PASS,CHANGE_DATE From $table"
                + "  where  USER_ID= $userid and PASS= conv.conv_string('$pass')";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$userid", "" + RecordToSelect.getuserid());
        selectStat = selectStat.replace("$pass", "" + RecordToSelect.getpass());
        ResultSet rs = executeQuery(selectStat);
        userpassDTOInter SelectedRecord = DTOFactory.createuserpassDTO();
        while (rs.next()) {
            SelectedRecord.setuserid(rs.getInt("USER_ID"));
            SelectedRecord.setpass(rs.getString("PASS"));
            SelectedRecord.setchangedate(rs.getTimestamp("CHANGE_DATE"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }
}
