package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import com.ev.Bingo.bus.dto.disputesDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.settlementlogDTOInter;
import com.ev.Bingo.bus.dto.settlementlogsearchDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class settlementlogDAO extends BaseDAO implements settlementlogDAOInter {

    protected settlementlogDAO() {
        super();
        super.setTableName("SETTLEMENT_LOG");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        disputesDTOInter RecordToInsert = (disputesDTOInter) obj[0];
//RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (CURRENCY,TRANSACTION_DATE,TRANSACTION_SEQUENCE,CARD_NO,AMOUNT,SETTLEMENT_DATE,RESPONSE_CODE,MATCHING_TYPE,RECORD_TYPE,SETTLED_USER,SETTLED_FLAG) "
                + " values "
                + " ('$currency',to_date('$transactiondate','dd.mm.yyyy hh24:mi:ss'),'$transactionsequence',"+super.GetEncrypt("$cardno")+",$amount,to_date('$settlementdate','dd.mm.yyyy hh24:mi:ss'),'$responsecode',$matchingtype,$recordtype,$settleduser,$settledflag)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$currency", "" + RecordToInsert.getcurrency());
        insertStat = insertStat.replace("$transactiondate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiondate()));
        insertStat = insertStat.replace("$transactionsequence", "" + RecordToInsert.gettransactionsequence());
        insertStat = insertStat.replace("$cardno", "" + RecordToInsert.getcardno());
        insertStat = insertStat.replace("$amount", "" + RecordToInsert.getamount());
        insertStat = insertStat.replace("$settlementdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.getsettlementdate()));
        insertStat = insertStat.replace("$responsecode", "" + RecordToInsert.getresponsecode());
        insertStat = insertStat.replace("$matchingtype", "" + RecordToInsert.getmatchingtype());
        insertStat = insertStat.replace("$recordtype", "" + RecordToInsert.getrecordtype());
        insertStat = insertStat.replace("$settleduser", "" + RecordToInsert.getsettleduser());
        insertStat = insertStat.replace("$settledflag", "" + RecordToInsert.getsettledflag());
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To SETTLEMENT_LOG", false);
        return null;
    }

    public String SearchRecord(Object... obj) {
        settlementlogsearchDTOInter Record = (settlementlogsearchDTOInter) obj[0];
        String Validate = "where 1 = 1";
        if (Record.getTransactiondatefrom() != null) {
            Validate = Validate + " and settlement_date > to_date('$dateF','dd.mm.yyyy hh24:mi:ss')";
            Validate = Validate.replace("$dateF", "" + DateFormatter.changeDateAndTimeFormat(Record.getTransactiondatefrom()));
        }
        if (Record.getTransactiondateto() != null) {
            Validate = Validate + " and settlement_date < to_date('$dateT','dd.mm.yyyy hh24:mi:ss')";
            Validate = Validate.replace("$dateT", "" + DateFormatter.changeDateAndTimeFormat(Record.getTransactiondateto()));
        }
        if (Record.getAtmid() != null) {

            Validate = Validate + " and atm_application_id like '%$ATM%'";
            Validate = Validate.replace("$ATM", "" + Record.getAtmid().toString());
        }
        if (!Record.getUserid().toString().trim().equals("0")) {
            Validate = Validate + " and settled_user = $user";
            Validate = Validate.replace("$user", "" + Record.getUserid().toString());
        }
        if (!Record.getSetteledid().toString().trim().equals("0")) {
            Validate = Validate + " and settled_flag = $status";
            Validate = Validate.replace("$status", "" + Record.getSetteledid().toString());
        }
        if (Record.getCardno().trim().toString() != null && !Record.getCardno().trim().toString().equals("")) {
            Validate = Validate + " and "+super.GetDecryptCardNo()+" like '%$card_no%'";
            Validate = Validate.replace("$card_no", "" + Record.getCardno().toString());
        }
        if (!"where 1 = 1".equals(Validate)) {
            Validate = Validate.replace(" 1 = 1 and", " ");
        } else {
            Validate = "";
        }
        return Validate;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        settlementlogsearchDTOInter RecordToSelect = (settlementlogsearchDTOInter) obj[0];
        String where = (String) SearchRecord(RecordToSelect);
        String selectStat = "select  CURRENCY, TRANSACTION_DATE, TRANSACTION_SEQUENCE, "+super.GetDecryptCardNo()+" CARD_NO, AMOUNT, SETTLEMENT_DATE, RESPONSE_CODE,decode(matching_type,1,'Network-Switch',2,'Switch-Host','Other') MATCHING_TYPE,decode(record_type,1,'Network',2,'Switch',3,'Host','Other')RECORD_TYPE, (select s.user_name from users s where s.user_id = settled_user ) SETTLED_USER,decode(settled_flag,1,'Y','N') SETTLED_FLAG from  $table $where order by SETTLEMENT_DATE";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$where", "" + where);
        ResultSet rs = executeQuery(selectStat);
        List<settlementlogDTOInter> Records = new ArrayList<settlementlogDTOInter>();
        while (rs.next()) {
            settlementlogDTOInter SelectedRecord = DTOFactory.createsettlementlogDTO();
            SelectedRecord.setcurrency(rs.getString("CURRENCY"));
            SelectedRecord.settransactiondate(rs.getTimestamp("TRANSACTION_DATE"));
            SelectedRecord.settransactionsequence(rs.getString("TRANSACTION_SEQUENCE"));
            SelectedRecord.setcardno(rs.getString("CARD_NO"));
            SelectedRecord.setamount(rs.getFloat("AMOUNT"));
            SelectedRecord.setsettlementdate(rs.getTimestamp("SETTLEMENT_DATE"));
            SelectedRecord.setresponsecode(rs.getString("RESPONSE_CODE"));
            SelectedRecord.setmatchingtype(rs.getString("MATCHING_TYPE"));
            SelectedRecord.setrecordtype(rs.getString("RECORD_TYPE"));
            SelectedRecord.setsettleduser(rs.getString("SETTLED_USER"));
            SelectedRecord.setsettledflag(rs.getString("SETTLED_FLAG"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

}
