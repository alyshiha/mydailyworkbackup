/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.bus.dto.ATMMACHINECASHDTOINTER;
import com.ev.Bingo.bus.dto.DiffCardAtmJournalDTOInter;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface DiffCardAtmDAOInter {

    Object findDetailRecords(String DIFFID) throws Throwable;

    List<ATMMACHINECASHDTOINTER> findMachineAll() throws Throwable;

    Object findMasterRecords(String ATMID, String INDICATION, String JournalType,Date From, Date TO) throws Throwable;
    
    Integer SaveRecordPrint(DiffCardAtmJournalDTOInter[] entities) throws SQLException, Throwable;
    void recalc() throws Throwable ;
    void DeleteRecordPrint(Integer ID) throws Throwable;
}
