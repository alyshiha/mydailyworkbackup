package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.currencyDTOInter;
import com.ev.Bingo.bus.dto.currencymasterDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class currencyDAO extends BaseDAO implements currencyDAOInter {

    protected currencyDAO() {
        super();
        super.setTableName("CURRENCY");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        currencyDTOInter RecordToInsert = (currencyDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
            String insertStat = "insert into $table"
                    + " (ID,ABBREVIATION,MASTER_ID,DEFAULT_CURRENCY) "
                    + " values "
                    + " ($id,'$abbreviation',$masterid,$defaultcurrency)";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
            insertStat = insertStat.replace("$abbreviation", "" + RecordToInsert.getabbreviation());
            insertStat = insertStat.replace("$masterid", "" + RecordToInsert.getmasterid());
            Boolean temp = RecordToInsert.isDefaultcurrencyB();
            if (temp == Boolean.TRUE) {
                RecordToInsert.setdefaultcurrency(1);
            } else {
                RecordToInsert.setdefaultcurrency(2);
            }
            insertStat = insertStat.replace("$defaultcurrency", "" + RecordToInsert.getdefaultcurrency());
            msg = super.executeUpdate(insertStat);
            msg = msg + " Record Has Been Inserted";
            super.postUpdate("Adding New CURRENCY Detail Record With Abbreviation " + RecordToInsert.getabbreviation(), false);
        }
        return msg;
    }

    public String ValidateNullList(Object... obj) {
        List<currencyDTOInter> AllRecords = (List<currencyDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (currencyDTOInter RecordToInsert : AllRecords) {

            if (RecordToInsert.getid() == 0) {
                Validate = Validate + "   id  ";
            }
            if (RecordToInsert.getabbreviation() == null || RecordToInsert.getabbreviation().equals("")) {
                Validate = Validate + "," + String.valueOf(i);
            }
            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = "Record With Index " + Validate + " Contain Null Abbreviation Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    public String ValidateNull(Object... obj) {
        currencyDTOInter RecordToInsert = (currencyDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getid() == 0) {
            Validate = Validate + "   id  ";
        }
        if (RecordToInsert.getabbreviation() == null || RecordToInsert.getabbreviation().equals("")) {
            Validate = Validate + "   Abbreviation  ";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required field";
        }
        return Validate;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        currencyDTOInter RecordToDelete = (currencyDTOInter) obj[0];
        String msg = "";
        String deleteStat = "delete from $table "
                + "  where  id= $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        msg = super.executeUpdate(deleteStat);
        msg = msg + " Currency Detail Has Been deleted";
        super.postUpdate("Deleting New CURRENCY Detail Record With Abbreviation " + RecordToDelete.getabbreviation(), false);
        return msg;
    }

    public Object findRecordsListCurrency(Object... obj) throws Throwable {
        super.preSelect();
        currencyDTOInter RecordToSelect = (currencyDTOInter) obj[0];
        String selectStat = "Select ID,ABBREVIATION,MASTER_ID,DEFAULT_CURRENCY From $table"
                + "  where  MASTER_ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getmasterid());
        ResultSet rs = executeQuery(selectStat);
        List<currencyDTOInter> Records = new ArrayList<currencyDTOInter>();
        while (rs.next()) {
            currencyDTOInter SelectedRecord = DTOFactory.createcurrencyDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setabbreviation(rs.getString("ABBREVIATION"));
            SelectedRecord.setmasterid(rs.getInt("MASTER_ID"));
            Integer temp = rs.getInt("DEFAULT_CURRENCY");
            if (temp == 1) {
                SelectedRecord.setDefaultcurrencyB(Boolean.TRUE);
            } else {
                SelectedRecord.setDefaultcurrencyB(Boolean.FALSE);
            }
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        currencymasterDTOInter RecordToSelect = (currencymasterDTOInter) obj[0];
        String selectStat = "Select ID,ABBREVIATION,MASTER_ID,DEFAULT_CURRENCY From $table"
                + "  where  MASTER_ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<currencyDTOInter> Records = new ArrayList<currencyDTOInter>();
        while (rs.next()) {
            currencyDTOInter SelectedRecord = DTOFactory.createcurrencyDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setabbreviation(rs.getString("ABBREVIATION"));
            SelectedRecord.setmasterid(rs.getInt("MASTER_ID"));
            Integer temp = rs.getInt("DEFAULT_CURRENCY");
            if (temp == 1) {
                SelectedRecord.setDefaultcurrencyB(Boolean.TRUE);
            } else {
                SelectedRecord.setDefaultcurrencyB(Boolean.FALSE);
            }
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<currencyDTOInter> entities = (List<currencyDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Users Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "update CURRENCY set ABBREVIATION = ? ,MASTER_ID = ?,DEFAULT_CURRENCY = ? where ID = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        currencyDTOInter RecordtoInsert = entities.get(i);

                        statement.setString(1, RecordtoInsert.getabbreviation());
                        statement.setInt(2, RecordtoInsert.getmasterid());
                        Boolean Temp = RecordtoInsert.isDefaultcurrencyB();
                        if (Temp == Boolean.TRUE) {
                            statement.setInt(3, 1);
                        } else if (Temp == Boolean.FALSE) {
                            statement.setInt(3, 2);
                        }
                        statement.setInt(4, RecordtoInsert.getid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                      CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }

}
