package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.menulabelsDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class menulabelsDAO extends BaseDAO implements menulabelsDAOInter {

    protected menulabelsDAO() {
        super();
        super.setTableName("MENU_LABELS");
    }
 public Object findRecordsAll2(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select MENU_ID,ARABIC_LABEL,ENGLISH_LABEL,TYPE,PARENT_ID,FORM_ID,REPORT_ID,SEQ,PAGE From $table where page in('AtmAccount.xhtml','AccountName.xhtml','Networks.xhtml','AtmJournal.xhtml','privilegeStatus.xhtml','NetworkTypeCode.xhtml','AtmJournalAtm.xhtml','Networks.xhtml','userPrivilege.xhtml','CashManagmentLog.xhtml','privilegeStatus.xhtml','atmmachinecash.xhtml','Replanishment.xhtml','BankHolidays.xhtml','BankHolidays.xhtml') order by ENGLISH_LABEL";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> Records = new ArrayList<menulabelsDTOInter>();
        while (rs.next()) {
            menulabelsDTOInter SelectedRecord = DTOFactory.createmenulabelsDTO();
            SelectedRecord.setmenuid(rs.getInt("MENU_ID"));
            SelectedRecord.setarabiclabel(rs.getString("ARABIC_LABEL"));
            SelectedRecord.setenglishlabel(rs.getString("ENGLISH_LABEL"));
            SelectedRecord.settype(rs.getInt("TYPE"));
            SelectedRecord.setparentid(rs.getInt("PARENT_ID"));
            SelectedRecord.setformid(rs.getInt("FORM_ID"));
            SelectedRecord.setreportid(rs.getInt("REPORT_ID"));
            SelectedRecord.setseq(rs.getInt("SEQ"));
            
            SelectedRecord.setpage(rs.getString("PAGE"));
            
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }
    public String ValidateNull(Object... obj) {
        menulabelsDTOInter RecordToInsert = (menulabelsDTOInter) obj[0];
        String Validate = "Validate";

        if (RecordToInsert.getenglishlabel() == null && RecordToInsert.getenglishlabel().equals("")) {
            Validate = Validate + "   +englishlabel  ";
        }
        if (RecordToInsert.gettype() == 0) {
            Validate = Validate + "   +type  ";
        }

        if (!Validate.equals("Validate")) {
            Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        menulabelsDTOInter RecordToDelete = (menulabelsDTOInter) obj[0];
        String msg = ValidateNull(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  MENU_ID= $menuid";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$menuid", "" + RecordToDelete.getmenuid());

            msg = super.executeUpdate(deleteStat);
            super.postUpdate("Delete Record In Table MENU_LABELS With Page Name " + RecordToDelete.getenglishlabel(), false);
        }
        return msg + " Record Has Been Deleted";
    }

    public Object findParentRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select MENU_ID,PAGE,ENGLISH_LABEL From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> Records = new ArrayList<menulabelsDTOInter>();
        while (rs.next()) {
            menulabelsDTOInter SelectedRecord = DTOFactory.createmenulabelsDTO();
            SelectedRecord.setmenuid(rs.getInt("MENU_ID"));
            SelectedRecord.setpage(rs.getString("PAGE"));
            SelectedRecord.setenglishlabel(rs.getString("ENGLISH_LABEL"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findPageRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select MENU_ID,PAGE,ENGLISH_LABEL From $table where PAGE is not null";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> Records = new ArrayList<menulabelsDTOInter>();
        while (rs.next()) {
            menulabelsDTOInter SelectedRecord = DTOFactory.createmenulabelsDTO();
            SelectedRecord.setmenuid(rs.getInt("MENU_ID"));
            SelectedRecord.setpage(rs.getString("PAGE"));
            SelectedRecord.setenglishlabel(rs.getString("ENGLISH_LABEL"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        Integer RecordToSelect = (Integer) obj[0];
        String selectStat = "Select MENU_ID,ARABIC_LABEL,ENGLISH_LABEL,TYPE,PARENT_ID,FORM_ID,REPORT_ID,SEQ,PAGE From $table  where  MENU_ID in (Select MENU_ID From PROFILE_MENU  where  PROFILE_ID= $menuid)";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$menuid", "" + RecordToSelect);
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> Records = new ArrayList<menulabelsDTOInter>();
        while (rs.next()) {
            menulabelsDTOInter SelectedRecord = DTOFactory.createmenulabelsDTO();
            SelectedRecord.setmenuid(rs.getInt("MENU_ID"));
            SelectedRecord.setarabiclabel(rs.getString("ARABIC_LABEL"));
            SelectedRecord.setenglishlabel(rs.getString("ENGLISH_LABEL"));
            SelectedRecord.settype(rs.getInt("TYPE"));
            SelectedRecord.setparentid(rs.getInt("PARENT_ID"));
            SelectedRecord.setformid(rs.getInt("FORM_ID"));
            SelectedRecord.setreportid(rs.getInt("REPORT_ID"));
            SelectedRecord.setseq(rs.getInt("SEQ"));

            SelectedRecord.setpage(rs.getString("PAGE"));

            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select MENU_ID,ARABIC_LABEL,ENGLISH_LABEL,TYPE,PARENT_ID,FORM_ID,REPORT_ID,SEQ,PAGE From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> Records = new ArrayList<menulabelsDTOInter>();
        while (rs.next()) {
            menulabelsDTOInter SelectedRecord = DTOFactory.createmenulabelsDTO();
            SelectedRecord.setmenuid(rs.getInt("MENU_ID"));
            SelectedRecord.setarabiclabel(rs.getString("ARABIC_LABEL"));
            SelectedRecord.setenglishlabel(rs.getString("ENGLISH_LABEL"));
            SelectedRecord.settype(rs.getInt("TYPE"));
            SelectedRecord.setparentid(rs.getInt("PARENT_ID"));
            SelectedRecord.setformid(rs.getInt("FORM_ID"));
            SelectedRecord.setreportid(rs.getInt("REPORT_ID"));
            SelectedRecord.setseq(rs.getInt("SEQ"));

            SelectedRecord.setpage(rs.getString("PAGE"));

            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Boolean findmenuitemcheck(int user, String Page) throws Throwable {

        super.preSelect();
        String selectStat = "select m.page from menu_labels m,user_profile p,profile_menu s where p.user_id = '$UserID' and LOWER(m.page) = LOWER('$Page') and s.profile_id = p.profile_id and s.menu_id = m.menu_id and m.page is not null";
        selectStat = selectStat.replace("$UserID", "" + user);
        selectStat = selectStat.replace("$Page", "" + Page);
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> uDTOL = new ArrayList<menulabelsDTOInter>();
        Boolean found = Boolean.FALSE;
        if (rs.next()) {
            found = Boolean.TRUE;
        }
        super.postSelect(rs);
        return found;
    }

    public Object findmenu(int user) throws Throwable {

        super.preSelect();
        String selectStat = "select t.menu_id MenuID, t.page PageLink,t.english_label EngLabel, t.parent_id  ParentID, level LevelID from (select m.* from  menu_labels  m, profile_menu p where  m.menu_id = p.menu_id and p.profile_id = (select profile_id from user_profile where user_id = $UserID)order BY parent_id,seq nulls last ,english_label) t connect by prior  t.menu_id  = t.parent_id start with t.parent_id is null ORDER SIBLINGS BY seq nulls last , english_label";
        selectStat = selectStat.replace("$UserID", "" + user);
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> uDTOL = new ArrayList<menulabelsDTOInter>();

        while (rs.next()) {

            menulabelsDTOInter uDTO = DTOFactory.createmenulabelsDTO();
            uDTO.setmenuid(rs.getInt("MenuID"));
            uDTO.setparentid(rs.getInt("ParentID"));
            uDTO.setpage(rs.getString("PageLink"));
            uDTO.setenglishlabel(rs.getString("EngLabel"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findParents() throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table m where parent_id is null  and exists(select 1 from profile_menu where  menu_id = m.menu_id) order by seq";
        selectStat = selectStat.replace("$table", "" + super.getTableName());

        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> uDTOL = new ArrayList<menulabelsDTOInter>();
        while (rs.next()) {
            menulabelsDTOInter uDTO = DTOFactory.createmenulabelsDTO();
            uDTO.setarabiclabel(rs.getString("arabic_label"));
            uDTO.setenglishlabel(rs.getString("english_label"));
            uDTO.setmenuid(rs.getInt("menu_id"));
            uDTO.setparentid(rs.getInt("parent_id"));
            uDTO.setpage(rs.getString("page"));

            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findParents(int profileId) throws Throwable {
        // user info, reqid, trans info, info for post, paging, logging
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table m where parent_id is null  and exists(select 1 from profile_menu where profile_id = $prof and menu_id = m.menu_id) order by seq";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$prof", "" + profileId);
        ResultSet rs = executeQuery(selectStat);
        List<menulabelsDTOInter> uDTOL = new ArrayList<menulabelsDTOInter>();
        while (rs.next()) {
            menulabelsDTOInter uDTO = DTOFactory.createmenulabelsDTO();
            uDTO.setarabiclabel(rs.getString("arabic_label"));
            uDTO.setenglishlabel(rs.getString("english_label"));
            uDTO.setmenuid(rs.getInt("menu_id"));
            uDTO.setparentid(rs.getInt("parent_id"));
            uDTO.setpage(rs.getString("page"));

            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        menulabelsDTOInter RecordToInsert = (menulabelsDTOInter) obj[0];

        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
            RecordToInsert.setmenuid(super.generateSequence(super.getTableName()));
            String insertStat = "insert into $table"
                    + " (MENU_ID,ARABIC_LABEL,ENGLISH_LABEL,TYPE,PARENT_ID,FORM_ID,REPORT_ID,SEQ,PAGE) "
                    + " values "
                    + " ($menuid,'$arabiclabel','$englishlabel',$type,$parentid,$formid,$reportid,$seq,'$page')";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$menuid", "" + RecordToInsert.getmenuid());
            insertStat = insertStat.replace("$arabiclabel", "" + RecordToInsert.getarabiclabel());
            insertStat = insertStat.replace("$englishlabel", "" + RecordToInsert.getenglishlabel());
            insertStat = insertStat.replace("$type", "" + RecordToInsert.gettype());
            insertStat = insertStat.replace("$parentid", "" + RecordToInsert.getparentid());
            insertStat = insertStat.replace("$formid", "" + RecordToInsert.getformid());
            insertStat = insertStat.replace("$reportid", "" + RecordToInsert.getreportid());
            insertStat = insertStat.replace("$seq", "" + RecordToInsert.getseq());

            insertStat = insertStat.replace("$page", "" + RecordToInsert.getpage());
            String mmm = "free";
            try {
                msg = super.executeUpdate(insertStat);
            } catch (Exception ex) {
                mmm = ex.getMessage();
            }
            if ("free".equals(mmm)) {
                msg = msg + " Row Has Been Inserted";
            } else {
                msg = mmm;
            }
            super.postUpdate("Add New Record To MENU_LABELS with Name " + RecordToInsert.getenglishlabel(), false);
        }
        return msg;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<menulabelsDTOInter> entities = (List<menulabelsDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Users Found";
        } else {
            String validate = "Validate";
            if (validate.equals("Validate")) {
                String insertStat = "update menu_labels set arabic_label = ?, english_label = ?, "
                        + "type = ?,form_id = ?,report_id = ?,seq = ?, page = ?, "
                        + "parent_id = ? where menu_id = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                   connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        menulabelsDTOInter RecordtoInsert = entities.get(i);

                        statement.setString(1, RecordtoInsert.getenglishlabel());
                        statement.setString(2, RecordtoInsert.getenglishlabel());
                        statement.setInt(3, RecordtoInsert.gettype());
                        statement.setInt(4, RecordtoInsert.getformid());
                        statement.setInt(5, RecordtoInsert.getreportid());
                        statement.setInt(6, RecordtoInsert.getseq());

                        if ("0".equals(RecordtoInsert.getpage())) {
                            RecordtoInsert.setpage("");
                        }
                        statement.setString(7, RecordtoInsert.getpage());

                        if (RecordtoInsert.getparentid() != 0) {
                            statement.setInt(8, RecordtoInsert.getparentid());
                        } else {
                            statement.setString(8, "");
                        }
                        statement.setInt(9, RecordtoInsert.getmenuid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }

                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                          CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
