package com.ev.Bingo.bus.dao;

import java.util.Date;
import java.util.List;
import java.sql.SQLException;

public interface networksDAOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object findUserRecordsAll(Object... obj) throws Throwable;

    Object findRecordsAll(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;
}
