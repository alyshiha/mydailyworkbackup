package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.hostfiletemplateDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class hostfiletemplateDAO extends BaseDAO implements hostfiletemplateDAOInter {

    protected hostfiletemplateDAO() {
        super();
        super.setTableName("HOST_FILE_TEMPLATE");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        hostfiletemplateDTOInter RecordToInsert = (hostfiletemplateDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (ID,NAME,PROCESSING_TYPE,NUMBER_OF_LINES,LOADING_FOLDER,STARTING_DATA_TYPE,STARTING_FORMAT,STARTING_POSITION,BACKUP_FOLDER,COPY_FOLDER,IGNORED_LINES,STARTING_LENGTH,DATE_SEPARATOR,DATE_SEPARATOR_POS1,DATE_SEPARATOR_POS2,SERVER_FOLDER,HEADER_DATA_TYPE,HEADER_FORMAT,HEADER_POSITION,HEADER_LENGTH,HEADER_DATE_SEPARATOR,HEADER_DATE_SEPARATOR_POS1,HEADER_DATE_SEPARATOR_POS2,HEADER_STRING,STARTING_VALUE,ACTIVE,SEPARATOR,TAGS_ENDING_DATA_TYPE,TAGS_ENDING_FORMAT,TAGS_ENDING_POSITION,TAGS_ENDING_LENGTH,TAGS_ENDING_VALUE,HEADER_LINES,MAX_TAG_LINES) "
                + " values "
                + " ($id,'$name',$processingtype,$numberoflines,'$loadingfolder',$startingdatatype,'$startingformat',$startingposition,'$backupfolder','$copyfolder',$ignoredlines,$startinglength,'$dateseparator',$dateseparatorpos1,$dateseparatorpos2,'$serverfolder',$headerdatatype,'$headerformat','$headerposition',$headerlength,'$headerdateseparator',$headerdateseparatorpos1,$headerdateseparatorpos2,'$headerstring','$startingvalue',$active,'$separator',$tagsendingdatatype,'$tagsendingformat',$tagsendingposition,$tagsendinglength,'$tagsendingvalue',$headerlines,$maxtaglines)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$name", "" + RecordToInsert.getname());
        insertStat = insertStat.replace("$processingtype", "" + RecordToInsert.getprocessingtype());
        insertStat = insertStat.replace("$numberoflines", "" + RecordToInsert.getnumberoflines());
        insertStat = insertStat.replace("$loadingfolder", "" + RecordToInsert.getloadingfolder());
        insertStat = insertStat.replace("$startingdatatype", "" + RecordToInsert.getstartingdatatype());
        insertStat = insertStat.replace("$startingformat", "" + RecordToInsert.getstartingformat());
        insertStat = insertStat.replace("$startingposition", "" + RecordToInsert.getstartingposition());
        insertStat = insertStat.replace("$backupfolder", "" + RecordToInsert.getbackupfolder());
        insertStat = insertStat.replace("$copyfolder", "" + RecordToInsert.getcopyfolder());
        insertStat = insertStat.replace("$ignoredlines", "" + RecordToInsert.getignoredlines());
        insertStat = insertStat.replace("$startinglength", "" + RecordToInsert.getstartinglength());
        insertStat = insertStat.replace("$dateseparator", "" + RecordToInsert.getdateseparator());
        insertStat = insertStat.replace("$dateseparatorpos1", "" + RecordToInsert.getdateseparatorpos1());
        insertStat = insertStat.replace("$dateseparatorpos2", "" + RecordToInsert.getdateseparatorpos2());
        insertStat = insertStat.replace("$serverfolder", "" + RecordToInsert.getserverfolder());
        insertStat = insertStat.replace("$headerdatatype", "" + RecordToInsert.getheaderdatatype());
        insertStat = insertStat.replace("$headerformat", "" + RecordToInsert.getheaderformat());
        insertStat = insertStat.replace("$headerposition", "" + RecordToInsert.getheaderposition());
        insertStat = insertStat.replace("$headerlength", "" + RecordToInsert.getheaderlength());
        insertStat = insertStat.replace("$headerdateseparator", "" + RecordToInsert.getheaderdateseparator());
        insertStat = insertStat.replace("$headerdateseparatorpos1", "" + RecordToInsert.getheaderdateseparatorpos1());
        insertStat = insertStat.replace("$headerdateseparatorpos2", "" + RecordToInsert.getheaderdateseparatorpos2());
        insertStat = insertStat.replace("$headerstring", "" + RecordToInsert.getheaderstring());
        insertStat = insertStat.replace("$startingvalue", "" + RecordToInsert.getstartingvalue());
        insertStat = insertStat.replace("$active", "" + RecordToInsert.getactive());
        insertStat = insertStat.replace("$separator", "" + RecordToInsert.getseparator());
        insertStat = insertStat.replace("$tagsendingdatatype", "" + RecordToInsert.gettagsendingdatatype());
        insertStat = insertStat.replace("$tagsendingformat", "" + RecordToInsert.gettagsendingformat());
        insertStat = insertStat.replace("$tagsendingposition", "" + RecordToInsert.gettagsendingposition());
        insertStat = insertStat.replace("$tagsendinglength", "" + RecordToInsert.gettagsendinglength());
        insertStat = insertStat.replace("$tagsendingvalue", "" + RecordToInsert.gettagsendingvalue());
        insertStat = insertStat.replace("$headerlines", "" + RecordToInsert.getheaderlines());
        insertStat = insertStat.replace("$maxtaglines", "" + RecordToInsert.getmaxtaglines());
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To HOST_FILE_TEMPLATE", false);
        return null;
    }

    public Boolean ValidateNull(Object... obj) {
        hostfiletemplateDTOInter RecordToInsert = (hostfiletemplateDTOInter) obj[0];
        if (RecordToInsert.getid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getname() == null && RecordToInsert.getname().equals("")) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getprocessingtype() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getnumberoflines() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getloadingfolder() == null && RecordToInsert.getloadingfolder().equals("")) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getignoredlines() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getactive() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getmaxtaglines() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        hostfiletemplateDTOInter RecordToUpdate = (hostfiletemplateDTOInter) obj[0];
        String UpdateStat = "update $table set "
                + " (ID= $id,NAME= '$name',PROCESSING_TYPE= $processingtype,NUMBER_OF_LINES= $numberoflines,LOADING_FOLDER= '$loadingfolder',STARTING_DATA_TYPE= $startingdatatype,STARTING_FORMAT= '$startingformat',STARTING_POSITION= $startingposition,BACKUP_FOLDER= '$backupfolder',COPY_FOLDER= '$copyfolder',IGNORED_LINES= $ignoredlines,STARTING_LENGTH= $startinglength,DATE_SEPARATOR= '$dateseparator',DATE_SEPARATOR_POS1= $dateseparatorpos1,DATE_SEPARATOR_POS2= $dateseparatorpos2,SERVER_FOLDER= '$serverfolder',HEADER_DATA_TYPE= $headerdatatype,HEADER_FORMAT= '$headerformat',HEADER_POSITION= '$headerposition',HEADER_LENGTH= $headerlength,HEADER_DATE_SEPARATOR= '$headerdateseparator',HEADER_DATE_SEPARATOR_POS1= $headerdateseparatorpos1,HEADER_DATE_SEPARATOR_POS2= $headerdateseparatorpos2,HEADER_STRING= '$headerstring',STARTING_VALUE= '$startingvalue',ACTIVE= $active,SEPARATOR= '$separator',TAGS_ENDING_DATA_TYPE= $tagsendingdatatype,TAGS_ENDING_FORMAT= '$tagsendingformat',TAGS_ENDING_POSITION= $tagsendingposition,TAGS_ENDING_LENGTH= $tagsendinglength,TAGS_ENDING_VALUE= '$tagsendingvalue',HEADER_LINES= $headerlines,MAX_TAG_LINES= $maxtaglines) "
                + "  where  ID= $id";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$id", "" + RecordToUpdate.getid());
        UpdateStat = UpdateStat.replace("$name", "" + RecordToUpdate.getname());
        UpdateStat = UpdateStat.replace("$processingtype", "" + RecordToUpdate.getprocessingtype());
        UpdateStat = UpdateStat.replace("$numberoflines", "" + RecordToUpdate.getnumberoflines());
        UpdateStat = UpdateStat.replace("$loadingfolder", "" + RecordToUpdate.getloadingfolder());
        UpdateStat = UpdateStat.replace("$startingdatatype", "" + RecordToUpdate.getstartingdatatype());
        UpdateStat = UpdateStat.replace("$startingformat", "" + RecordToUpdate.getstartingformat());
        UpdateStat = UpdateStat.replace("$startingposition", "" + RecordToUpdate.getstartingposition());
        UpdateStat = UpdateStat.replace("$backupfolder", "" + RecordToUpdate.getbackupfolder());
        UpdateStat = UpdateStat.replace("$copyfolder", "" + RecordToUpdate.getcopyfolder());
        UpdateStat = UpdateStat.replace("$ignoredlines", "" + RecordToUpdate.getignoredlines());
        UpdateStat = UpdateStat.replace("$startinglength", "" + RecordToUpdate.getstartinglength());
        UpdateStat = UpdateStat.replace("$dateseparator", "" + RecordToUpdate.getdateseparator());
        UpdateStat = UpdateStat.replace("$dateseparatorpos1", "" + RecordToUpdate.getdateseparatorpos1());
        UpdateStat = UpdateStat.replace("$dateseparatorpos2", "" + RecordToUpdate.getdateseparatorpos2());
        UpdateStat = UpdateStat.replace("$serverfolder", "" + RecordToUpdate.getserverfolder());
        UpdateStat = UpdateStat.replace("$headerdatatype", "" + RecordToUpdate.getheaderdatatype());
        UpdateStat = UpdateStat.replace("$headerformat", "" + RecordToUpdate.getheaderformat());
        UpdateStat = UpdateStat.replace("$headerposition", "" + RecordToUpdate.getheaderposition());
        UpdateStat = UpdateStat.replace("$headerlength", "" + RecordToUpdate.getheaderlength());
        UpdateStat = UpdateStat.replace("$headerdateseparator", "" + RecordToUpdate.getheaderdateseparator());
        UpdateStat = UpdateStat.replace("$headerdateseparatorpos1", "" + RecordToUpdate.getheaderdateseparatorpos1());
        UpdateStat = UpdateStat.replace("$headerdateseparatorpos2", "" + RecordToUpdate.getheaderdateseparatorpos2());
        UpdateStat = UpdateStat.replace("$headerstring", "" + RecordToUpdate.getheaderstring());
        UpdateStat = UpdateStat.replace("$startingvalue", "" + RecordToUpdate.getstartingvalue());
        UpdateStat = UpdateStat.replace("$active", "" + RecordToUpdate.getactive());
        UpdateStat = UpdateStat.replace("$separator", "" + RecordToUpdate.getseparator());
        UpdateStat = UpdateStat.replace("$tagsendingdatatype", "" + RecordToUpdate.gettagsendingdatatype());
        UpdateStat = UpdateStat.replace("$tagsendingformat", "" + RecordToUpdate.gettagsendingformat());
        UpdateStat = UpdateStat.replace("$tagsendingposition", "" + RecordToUpdate.gettagsendingposition());
        UpdateStat = UpdateStat.replace("$tagsendinglength", "" + RecordToUpdate.gettagsendinglength());
        UpdateStat = UpdateStat.replace("$tagsendingvalue", "" + RecordToUpdate.gettagsendingvalue());
        UpdateStat = UpdateStat.replace("$headerlines", "" + RecordToUpdate.getheaderlines());
        UpdateStat = UpdateStat.replace("$maxtaglines", "" + RecordToUpdate.getmaxtaglines());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table HOST_FILE_TEMPLATE", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        hostfiletemplateDTOInter RecordToDelete = (hostfiletemplateDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        deleteStat = deleteStat.replace("$name", "" + RecordToDelete.getname());
        deleteStat = deleteStat.replace("$processingtype", "" + RecordToDelete.getprocessingtype());
        deleteStat = deleteStat.replace("$numberoflines", "" + RecordToDelete.getnumberoflines());
        deleteStat = deleteStat.replace("$loadingfolder", "" + RecordToDelete.getloadingfolder());
        deleteStat = deleteStat.replace("$startingdatatype", "" + RecordToDelete.getstartingdatatype());
        deleteStat = deleteStat.replace("$startingformat", "" + RecordToDelete.getstartingformat());
        deleteStat = deleteStat.replace("$startingposition", "" + RecordToDelete.getstartingposition());
        deleteStat = deleteStat.replace("$backupfolder", "" + RecordToDelete.getbackupfolder());
        deleteStat = deleteStat.replace("$copyfolder", "" + RecordToDelete.getcopyfolder());
        deleteStat = deleteStat.replace("$ignoredlines", "" + RecordToDelete.getignoredlines());
        deleteStat = deleteStat.replace("$startinglength", "" + RecordToDelete.getstartinglength());
        deleteStat = deleteStat.replace("$dateseparator", "" + RecordToDelete.getdateseparator());
        deleteStat = deleteStat.replace("$dateseparatorpos1", "" + RecordToDelete.getdateseparatorpos1());
        deleteStat = deleteStat.replace("$dateseparatorpos2", "" + RecordToDelete.getdateseparatorpos2());
        deleteStat = deleteStat.replace("$serverfolder", "" + RecordToDelete.getserverfolder());
        deleteStat = deleteStat.replace("$headerdatatype", "" + RecordToDelete.getheaderdatatype());
        deleteStat = deleteStat.replace("$headerformat", "" + RecordToDelete.getheaderformat());
        deleteStat = deleteStat.replace("$headerposition", "" + RecordToDelete.getheaderposition());
        deleteStat = deleteStat.replace("$headerlength", "" + RecordToDelete.getheaderlength());
        deleteStat = deleteStat.replace("$headerdateseparator", "" + RecordToDelete.getheaderdateseparator());
        deleteStat = deleteStat.replace("$headerdateseparatorpos1", "" + RecordToDelete.getheaderdateseparatorpos1());
        deleteStat = deleteStat.replace("$headerdateseparatorpos2", "" + RecordToDelete.getheaderdateseparatorpos2());
        deleteStat = deleteStat.replace("$headerstring", "" + RecordToDelete.getheaderstring());
        deleteStat = deleteStat.replace("$startingvalue", "" + RecordToDelete.getstartingvalue());
        deleteStat = deleteStat.replace("$active", "" + RecordToDelete.getactive());
        deleteStat = deleteStat.replace("$separator", "" + RecordToDelete.getseparator());
        deleteStat = deleteStat.replace("$tagsendingdatatype", "" + RecordToDelete.gettagsendingdatatype());
        deleteStat = deleteStat.replace("$tagsendingformat", "" + RecordToDelete.gettagsendingformat());
        deleteStat = deleteStat.replace("$tagsendingposition", "" + RecordToDelete.gettagsendingposition());
        deleteStat = deleteStat.replace("$tagsendinglength", "" + RecordToDelete.gettagsendinglength());
        deleteStat = deleteStat.replace("$tagsendingvalue", "" + RecordToDelete.gettagsendingvalue());
        deleteStat = deleteStat.replace("$headerlines", "" + RecordToDelete.getheaderlines());
        deleteStat = deleteStat.replace("$maxtaglines", "" + RecordToDelete.getmaxtaglines());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table HOST_FILE_TEMPLATE", false);
        return null;
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table HOST_FILE_TEMPLATE", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        hostfiletemplateDTOInter RecordToSelect = (hostfiletemplateDTOInter) obj[0];
        String selectStat = "Select ID,NAME,PROCESSING_TYPE,NUMBER_OF_LINES,LOADING_FOLDER,STARTING_DATA_TYPE,STARTING_FORMAT,STARTING_POSITION,BACKUP_FOLDER,COPY_FOLDER,IGNORED_LINES,STARTING_LENGTH,DATE_SEPARATOR,DATE_SEPARATOR_POS1,DATE_SEPARATOR_POS2,SERVER_FOLDER,HEADER_DATA_TYPE,HEADER_FORMAT,HEADER_POSITION,HEADER_LENGTH,HEADER_DATE_SEPARATOR,HEADER_DATE_SEPARATOR_POS1,HEADER_DATE_SEPARATOR_POS2,HEADER_STRING,STARTING_VALUE,ACTIVE,SEPARATOR,TAGS_ENDING_DATA_TYPE,TAGS_ENDING_FORMAT,TAGS_ENDING_POSITION,TAGS_ENDING_LENGTH,TAGS_ENDING_VALUE,HEADER_LINES,MAX_TAG_LINES From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        hostfiletemplateDTOInter SelectedRecord = DTOFactory.createhostfiletemplateDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setprocessingtype(rs.getInt("PROCESSING_TYPE"));
            SelectedRecord.setnumberoflines(rs.getInt("NUMBER_OF_LINES"));
            SelectedRecord.setloadingfolder(rs.getString("LOADING_FOLDER"));
            SelectedRecord.setstartingdatatype(rs.getInt("STARTING_DATA_TYPE"));
            SelectedRecord.setstartingformat(rs.getString("STARTING_FORMAT"));
            SelectedRecord.setstartingposition(rs.getInt("STARTING_POSITION"));
            SelectedRecord.setbackupfolder(rs.getString("BACKUP_FOLDER"));
            SelectedRecord.setcopyfolder(rs.getString("COPY_FOLDER"));
            SelectedRecord.setignoredlines(rs.getInt("IGNORED_LINES"));
            SelectedRecord.setstartinglength(rs.getInt("STARTING_LENGTH"));
            SelectedRecord.setdateseparator(rs.getString("DATE_SEPARATOR"));
            SelectedRecord.setdateseparatorpos1(rs.getInt("DATE_SEPARATOR_POS1"));
            SelectedRecord.setdateseparatorpos2(rs.getInt("DATE_SEPARATOR_POS2"));
            SelectedRecord.setserverfolder(rs.getString("SERVER_FOLDER"));
            SelectedRecord.setheaderdatatype(rs.getInt("HEADER_DATA_TYPE"));
            SelectedRecord.setheaderformat(rs.getString("HEADER_FORMAT"));
            SelectedRecord.setheaderposition(rs.getString("HEADER_POSITION"));
            SelectedRecord.setheaderlength(rs.getInt("HEADER_LENGTH"));
            SelectedRecord.setheaderdateseparator(rs.getString("HEADER_DATE_SEPARATOR"));
            SelectedRecord.setheaderdateseparatorpos1(rs.getInt("HEADER_DATE_SEPARATOR_POS1"));
            SelectedRecord.setheaderdateseparatorpos2(rs.getInt("HEADER_DATE_SEPARATOR_POS2"));
            SelectedRecord.setheaderstring(rs.getString("HEADER_STRING"));
            SelectedRecord.setstartingvalue(rs.getString("STARTING_VALUE"));
            SelectedRecord.setactive(rs.getInt("ACTIVE"));
            SelectedRecord.setseparator(rs.getString("SEPARATOR"));
            SelectedRecord.settagsendingdatatype(rs.getInt("TAGS_ENDING_DATA_TYPE"));
            SelectedRecord.settagsendingformat(rs.getString("TAGS_ENDING_FORMAT"));
            SelectedRecord.settagsendingposition(rs.getInt("TAGS_ENDING_POSITION"));
            SelectedRecord.settagsendinglength(rs.getInt("TAGS_ENDING_LENGTH"));
            SelectedRecord.settagsendingvalue(rs.getString("TAGS_ENDING_VALUE"));
            SelectedRecord.setheaderlines(rs.getInt("HEADER_LINES"));
            SelectedRecord.setmaxtaglines(rs.getInt("MAX_TAG_LINES"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        hostfiletemplateDTOInter RecordToSelect = (hostfiletemplateDTOInter) obj[0];
        String selectStat = "Select ID,NAME,PROCESSING_TYPE,NUMBER_OF_LINES,LOADING_FOLDER,STARTING_DATA_TYPE,STARTING_FORMAT,STARTING_POSITION,BACKUP_FOLDER,COPY_FOLDER,IGNORED_LINES,STARTING_LENGTH,DATE_SEPARATOR,DATE_SEPARATOR_POS1,DATE_SEPARATOR_POS2,SERVER_FOLDER,HEADER_DATA_TYPE,HEADER_FORMAT,HEADER_POSITION,HEADER_LENGTH,HEADER_DATE_SEPARATOR,HEADER_DATE_SEPARATOR_POS1,HEADER_DATE_SEPARATOR_POS2,HEADER_STRING,STARTING_VALUE,ACTIVE,SEPARATOR,TAGS_ENDING_DATA_TYPE,TAGS_ENDING_FORMAT,TAGS_ENDING_POSITION,TAGS_ENDING_LENGTH,TAGS_ENDING_VALUE,HEADER_LINES,MAX_TAG_LINES From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<hostfiletemplateDTOInter> Records = new ArrayList<hostfiletemplateDTOInter>();
        while (rs.next()) {
            hostfiletemplateDTOInter SelectedRecord = DTOFactory.createhostfiletemplateDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setprocessingtype(rs.getInt("PROCESSING_TYPE"));
            SelectedRecord.setnumberoflines(rs.getInt("NUMBER_OF_LINES"));
            SelectedRecord.setloadingfolder(rs.getString("LOADING_FOLDER"));
            SelectedRecord.setstartingdatatype(rs.getInt("STARTING_DATA_TYPE"));
            SelectedRecord.setstartingformat(rs.getString("STARTING_FORMAT"));
            SelectedRecord.setstartingposition(rs.getInt("STARTING_POSITION"));
            SelectedRecord.setbackupfolder(rs.getString("BACKUP_FOLDER"));
            SelectedRecord.setcopyfolder(rs.getString("COPY_FOLDER"));
            SelectedRecord.setignoredlines(rs.getInt("IGNORED_LINES"));
            SelectedRecord.setstartinglength(rs.getInt("STARTING_LENGTH"));
            SelectedRecord.setdateseparator(rs.getString("DATE_SEPARATOR"));
            SelectedRecord.setdateseparatorpos1(rs.getInt("DATE_SEPARATOR_POS1"));
            SelectedRecord.setdateseparatorpos2(rs.getInt("DATE_SEPARATOR_POS2"));
            SelectedRecord.setserverfolder(rs.getString("SERVER_FOLDER"));
            SelectedRecord.setheaderdatatype(rs.getInt("HEADER_DATA_TYPE"));
            SelectedRecord.setheaderformat(rs.getString("HEADER_FORMAT"));
            SelectedRecord.setheaderposition(rs.getString("HEADER_POSITION"));
            SelectedRecord.setheaderlength(rs.getInt("HEADER_LENGTH"));
            SelectedRecord.setheaderdateseparator(rs.getString("HEADER_DATE_SEPARATOR"));
            SelectedRecord.setheaderdateseparatorpos1(rs.getInt("HEADER_DATE_SEPARATOR_POS1"));
            SelectedRecord.setheaderdateseparatorpos2(rs.getInt("HEADER_DATE_SEPARATOR_POS2"));
            SelectedRecord.setheaderstring(rs.getString("HEADER_STRING"));
            SelectedRecord.setstartingvalue(rs.getString("STARTING_VALUE"));
            SelectedRecord.setactive(rs.getInt("ACTIVE"));
            SelectedRecord.setseparator(rs.getString("SEPARATOR"));
            SelectedRecord.settagsendingdatatype(rs.getInt("TAGS_ENDING_DATA_TYPE"));
            SelectedRecord.settagsendingformat(rs.getString("TAGS_ENDING_FORMAT"));
            SelectedRecord.settagsendingposition(rs.getInt("TAGS_ENDING_POSITION"));
            SelectedRecord.settagsendinglength(rs.getInt("TAGS_ENDING_LENGTH"));
            SelectedRecord.settagsendingvalue(rs.getString("TAGS_ENDING_VALUE"));
            SelectedRecord.setheaderlines(rs.getInt("HEADER_LINES"));
            SelectedRecord.setmaxtaglines(rs.getInt("MAX_TAG_LINES"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,NAME,PROCESSING_TYPE,NUMBER_OF_LINES,LOADING_FOLDER,STARTING_DATA_TYPE,STARTING_FORMAT,STARTING_POSITION,BACKUP_FOLDER,COPY_FOLDER,IGNORED_LINES,STARTING_LENGTH,DATE_SEPARATOR,DATE_SEPARATOR_POS1,DATE_SEPARATOR_POS2,SERVER_FOLDER,HEADER_DATA_TYPE,HEADER_FORMAT,HEADER_POSITION,HEADER_LENGTH,HEADER_DATE_SEPARATOR,HEADER_DATE_SEPARATOR_POS1,HEADER_DATE_SEPARATOR_POS2,HEADER_STRING,STARTING_VALUE,ACTIVE,SEPARATOR,TAGS_ENDING_DATA_TYPE,TAGS_ENDING_FORMAT,TAGS_ENDING_POSITION,TAGS_ENDING_LENGTH,TAGS_ENDING_VALUE,HEADER_LINES,MAX_TAG_LINES From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<hostfiletemplateDTOInter> Records = new ArrayList<hostfiletemplateDTOInter>();
        while (rs.next()) {
            hostfiletemplateDTOInter SelectedRecord = DTOFactory.createhostfiletemplateDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setprocessingtype(rs.getInt("PROCESSING_TYPE"));
            SelectedRecord.setnumberoflines(rs.getInt("NUMBER_OF_LINES"));
            SelectedRecord.setloadingfolder(rs.getString("LOADING_FOLDER"));
            SelectedRecord.setstartingdatatype(rs.getInt("STARTING_DATA_TYPE"));
            SelectedRecord.setstartingformat(rs.getString("STARTING_FORMAT"));
            SelectedRecord.setstartingposition(rs.getInt("STARTING_POSITION"));
            SelectedRecord.setbackupfolder(rs.getString("BACKUP_FOLDER"));
            SelectedRecord.setcopyfolder(rs.getString("COPY_FOLDER"));
            SelectedRecord.setignoredlines(rs.getInt("IGNORED_LINES"));
            SelectedRecord.setstartinglength(rs.getInt("STARTING_LENGTH"));
            SelectedRecord.setdateseparator(rs.getString("DATE_SEPARATOR"));
            SelectedRecord.setdateseparatorpos1(rs.getInt("DATE_SEPARATOR_POS1"));
            SelectedRecord.setdateseparatorpos2(rs.getInt("DATE_SEPARATOR_POS2"));
            SelectedRecord.setserverfolder(rs.getString("SERVER_FOLDER"));
            SelectedRecord.setheaderdatatype(rs.getInt("HEADER_DATA_TYPE"));
            SelectedRecord.setheaderformat(rs.getString("HEADER_FORMAT"));
            SelectedRecord.setheaderposition(rs.getString("HEADER_POSITION"));
            SelectedRecord.setheaderlength(rs.getInt("HEADER_LENGTH"));
            SelectedRecord.setheaderdateseparator(rs.getString("HEADER_DATE_SEPARATOR"));
            SelectedRecord.setheaderdateseparatorpos1(rs.getInt("HEADER_DATE_SEPARATOR_POS1"));
            SelectedRecord.setheaderdateseparatorpos2(rs.getInt("HEADER_DATE_SEPARATOR_POS2"));
            SelectedRecord.setheaderstring(rs.getString("HEADER_STRING"));
            SelectedRecord.setstartingvalue(rs.getString("STARTING_VALUE"));
            SelectedRecord.setactive(rs.getInt("ACTIVE"));
            SelectedRecord.setseparator(rs.getString("SEPARATOR"));
            SelectedRecord.settagsendingdatatype(rs.getInt("TAGS_ENDING_DATA_TYPE"));
            SelectedRecord.settagsendingformat(rs.getString("TAGS_ENDING_FORMAT"));
            SelectedRecord.settagsendingposition(rs.getInt("TAGS_ENDING_POSITION"));
            SelectedRecord.settagsendinglength(rs.getInt("TAGS_ENDING_LENGTH"));
            SelectedRecord.settagsendingvalue(rs.getString("TAGS_ENDING_VALUE"));
            SelectedRecord.setheaderlines(rs.getInt("HEADER_LINES"));
            SelectedRecord.setmaxtaglines(rs.getInt("MAX_TAG_LINES"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public void save(Object... obj) throws SQLException, Throwable {
        List<hostfiletemplateDTOInter> entities = (List<hostfiletemplateDTOInter>) obj[0];
        String insertStat = "insert into HOST_FILE_TEMPLATE (ID,NAME,PROCESSING_TYPE,NUMBER_OF_LINES,LOADING_FOLDER,STARTING_DATA_TYPE,STARTING_FORMAT,STARTING_POSITION,BACKUP_FOLDER,COPY_FOLDER,IGNORED_LINES,STARTING_LENGTH,DATE_SEPARATOR,DATE_SEPARATOR_POS1,DATE_SEPARATOR_POS2,SERVER_FOLDER,HEADER_DATA_TYPE,HEADER_FORMAT,HEADER_POSITION,HEADER_LENGTH,HEADER_DATE_SEPARATOR,HEADER_DATE_SEPARATOR_POS1,HEADER_DATE_SEPARATOR_POS2,HEADER_STRING,STARTING_VALUE,ACTIVE,SEPARATOR,TAGS_ENDING_DATA_TYPE,TAGS_ENDING_FORMAT,TAGS_ENDING_POSITION,TAGS_ENDING_LENGTH,TAGS_ENDING_VALUE,HEADER_LINES,MAX_TAG_LINES) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        Connection connection = null;
        PreparedStatement statement = null;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.size(); i++) {
                hostfiletemplateDTOInter RecordtoInsert = entities.get(i);
                statement.setInt(1, RecordtoInsert.getid());
                statement.setString(2, RecordtoInsert.getname());
                statement.setInt(3, RecordtoInsert.getprocessingtype());
                statement.setInt(4, RecordtoInsert.getnumberoflines());
                statement.setString(5, RecordtoInsert.getloadingfolder());
                statement.setInt(6, RecordtoInsert.getstartingdatatype());
                statement.setString(7, RecordtoInsert.getstartingformat());
                statement.setInt(8, RecordtoInsert.getstartingposition());
                statement.setString(9, RecordtoInsert.getbackupfolder());
                statement.setString(10, RecordtoInsert.getcopyfolder());
                statement.setInt(11, RecordtoInsert.getignoredlines());
                statement.setInt(12, RecordtoInsert.getstartinglength());
                statement.setString(13, RecordtoInsert.getdateseparator());
                statement.setInt(14, RecordtoInsert.getdateseparatorpos1());
                statement.setInt(15, RecordtoInsert.getdateseparatorpos2());
                statement.setString(16, RecordtoInsert.getserverfolder());
                statement.setInt(17, RecordtoInsert.getheaderdatatype());
                statement.setString(18, RecordtoInsert.getheaderformat());
                statement.setString(19, RecordtoInsert.getheaderposition());
                statement.setInt(20, RecordtoInsert.getheaderlength());
                statement.setString(21, RecordtoInsert.getheaderdateseparator());
                statement.setInt(22, RecordtoInsert.getheaderdateseparatorpos1());
                statement.setInt(23, RecordtoInsert.getheaderdateseparatorpos2());
                statement.setString(24, RecordtoInsert.getheaderstring());
                statement.setString(25, RecordtoInsert.getstartingvalue());
                statement.setInt(26, RecordtoInsert.getactive());
                statement.setString(27, RecordtoInsert.getseparator());
                statement.setInt(28, RecordtoInsert.gettagsendingdatatype());
                statement.setString(29, RecordtoInsert.gettagsendingformat());
                statement.setInt(30, RecordtoInsert.gettagsendingposition());
                statement.setInt(31, RecordtoInsert.gettagsendinglength());
                statement.setString(32, RecordtoInsert.gettagsendingvalue());
                statement.setInt(33, RecordtoInsert.getheaderlines());
                statement.setInt(34, RecordtoInsert.getmaxtaglines());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                  CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
    }
}
