package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

public interface systemtableDAOInter {

    Object insertrecord(Object... obj) throws Throwable;

    String deleterecord(Object... obj) throws Throwable;

    Object findRecord(Object... obj) throws Throwable;
    Object findRecordexport(String Path) throws Throwable;

    Object findRecordsAll(Object... obj) throws Throwable;

    String save(Object... obj) throws SQLException, Throwable;
}
