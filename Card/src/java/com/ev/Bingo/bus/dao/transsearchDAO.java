/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;

import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.disputesDTOInter;
import java.io.IOException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 * +
 *
 *
 * @author Aly
 */
public class transsearchDAO extends BaseDAO implements transsearchDAOInter {

    public List<disputesDTOInter> getselecteditems(String Statment) throws Throwable {
        super.preSelect();
        ResultSet rs = executeQuery(Statment);
        List<disputesDTOInter> Records = new ArrayList<disputesDTOInter>();
        while (rs.next()) {
            disputesDTOInter SelectedRecord = DTOFactory.createdisputesDTO();

            SelectedRecord.setfileid(rs.getInt("file_id"));
            SelectedRecord.settransactionid(rs.getInt("transaction_id"));
            SelectedRecord.settransactiondate(rs.getTimestamp("TRANSACTION_DATE"));
            SelectedRecord.setsettlementdate(rs.getTimestamp("SETTLEMENT_DATE"));
            SelectedRecord.settransactionsequence(rs.getString("TRANSACTION_SEQUENCE"));
            SelectedRecord.settransactiontype(rs.getString("TRANSACTION_TYPE"));
            SelectedRecord.setcardno(rs.getString("CARD_NO"));
            SelectedRecord.setloadingdate(rs.getTimestamp("LOADING_DATE"));
            SelectedRecord.setamount(rs.getFloat("AMOUNT"));
            SelectedRecord.setcurrency(rs.getString("CURRENCY"));
            SelectedRecord.setcurrencyid(rs.getInt("CURRENCY_ID"));
            SelectedRecord.setcustomeraccountnumber(rs.getString("CUSTOMER_ACCOUNT_NUMBER"));
            SelectedRecord.setresponsecode(rs.getString("RESPONSE_CODE"));
            SelectedRecord.setresponsecodeid(rs.getInt("RESPONSE_CODE_ID"));
            SelectedRecord.settransactiontypeid(rs.getInt("TRANSACTION_TYPE_ID"));
            SelectedRecord.settransactiontime(rs.getTimestamp("TRANSACTION_TIME"));
            SelectedRecord.settransactionsequenceorderby(rs.getString("TRANSACTION_SEQUENCE_ORDER_BY"));
            SelectedRecord.setcolumn1(rs.getString("COLUMN1"));
            SelectedRecord.setcolumn2(rs.getString("COLUMN2"));
            SelectedRecord.setcolumn3(rs.getString("COLUMN3"));
            SelectedRecord.setcolumn4(rs.getString("COLUMN4"));
            SelectedRecord.setcolumn5(rs.getString("COLUMN5"));
            SelectedRecord.setRownum(rs.getInt("rn"));
            if (!Statment.contains("REJECTED_VISA")) {
                SelectedRecord.setrecordtype(rs.getInt("record_type"));
                SelectedRecord.setBlacklisted(rs.getInt("BLACKLISTED"));
            }
            if (Statment.contains("rejected_transactions")) {
                SelectedRecord.setdisputereason(rs.getString("REASON"));

            }
            if (!Statment.contains("rejected_transactions")) {
                if (Statment.contains("Matched_Data")) {
                    SelectedRecord.setmatchkey(rs.getString("match_key"));
                }
                if (Statment.contains("From Disputes")) {
                    SelectedRecord.setdisputekey(rs.getInt("dispute_key"));
                    SelectedRecord.setcomments(rs.getString("COMMENTS"));
                    SelectedRecord.setCorrectiveentryflag(rs.getInt("corrective_entry_flag"));
                    SelectedRecord.setCorrectiveamount(rs.getInt("CORRECTIVE_AMOUNT") + "");
                }
                if (!Statment.contains("REJECTED_VISA")) {
                    SelectedRecord.setmatchingtype(rs.getInt("matching_type"));
                    SelectedRecord.setsettledflag(rs.getInt("settled_flag"));
                }
                SelectedRecord.setterminal(rs.getString("TERMINAL"));
                SelectedRecord.settransactiontypemaster(rs.getInt("TRANSACTION_TYPE_MASTER"));
                SelectedRecord.setresponsecodemaster(rs.getInt("RESPONSE_CODE_MASTER"));
                SelectedRecord.setcardnosuffix(rs.getString("CARD_NO_SUFFIX"));
                SelectedRecord.setacquirercurrency(rs.getString("ACQUIRER_CURRENCY"));
                SelectedRecord.setacquirercurrencyid(rs.getInt("ACQUIRER_CURRENCY_ID"));
                SelectedRecord.setacquireramount(rs.getFloat("ACQUIRER_AMOUNT"));
                SelectedRecord.setsettlementcurrency(rs.getString("SETTLEMENT_CURRENCY"));
                SelectedRecord.setsettlementcurrencyid(rs.getInt("SETTLEMENT_CURRENCY_ID"));
                SelectedRecord.setsettlementamount(rs.getFloat("SETTLEMENT_AMOUNT"));
                SelectedRecord.setauthorizationno(rs.getString("AUTHORIZATION_NO"));
                SelectedRecord.setreferenceno(rs.getString("REFERENCE_NO"));
                SelectedRecord.setbank(rs.getString("BANK"));
                SelectedRecord.setnetworkcode(rs.getString("NETWORK_CODE"));
                SelectedRecord.setprocesscode(rs.getString("PROCESS_CODE"));
                SelectedRecord.setareacode(rs.getString("AREA_CODE"));
                SelectedRecord.setreportid(rs.getString("REPORT_ID"));
                SelectedRecord.setnetworkcomm(rs.getInt("NETWORK_COMM"));
                SelectedRecord.setnetworkcommflag(rs.getString("NETWORK_COMM_FLAG"));
                SelectedRecord.setfeelevel(rs.getString("FEE_LEVEL"));
                SelectedRecord.setnetworkid(rs.getString("NETWORK_ID"));
            }

            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findotherside(Object... obj) throws Throwable {
        super.preSelect();
        disputesDTOInter SelectedRecord = DTOFactory.createdisputesDTO();
        disputesDTOInter RecordToSelect = (disputesDTOInter) obj[0];
        String selectStat = "select ROWNUM AS rn,corrective_entry_flag,CORRECTIVE_AMOUNT ,settled_flag,COMMENTS,LOADING_DATE,file_id,record_type,transaction_id,TRANSACTION_DATE,SETTLEMENT_DATE,TRANSACTION_SEQUENCE,TRANSACTION_TYPE," + super.GetDecryptCardNo() + "CARD_NO,AMOUNT,CURRENCY,CURRENCY_ID,CUSTOMER_ACCOUNT_NUMBER,RESPONSE_CODE,RESPONSE_CODE_ID,TRANSACTION_TYPE_ID,TERMINAL,TRANSACTION_TIME,TRANSACTION_SEQUENCE_ORDER_BY,COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5,TRANSACTION_TYPE_MASTER,RESPONSE_CODE_MASTER,CARD_NO_SUFFIX,ACQUIRER_CURRENCY,ACQUIRER_CURRENCY_ID,ACQUIRER_AMOUNT,SETTLEMENT_CURRENCY,SETTLEMENT_CURRENCY_ID,SETTLEMENT_AMOUNT,AUTHORIZATION_NO,REFERENCE_NO,BANK,NETWORK_CODE,PROCESS_CODE,AREA_CODE,REPORT_ID,NETWORK_COMM,NETWORK_COMM_FLAG,FEE_LEVEL,(select name from networks where id = NETWORK_ID)NETWORK_ID From Disputes "
                + "  where " + super.GetDecryptLinc() + " = TRANSACTION_ID and dispute_key = $transid and  RECORD_TYPE <> $recordtype";

        selectStat = selectStat.replace("$transid", "" + RecordToSelect.getdisputekey());
        selectStat = selectStat.replace("$recordtype", "" + RecordToSelect.getrecordtype());
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            SelectedRecord.setCorrectiveentryflag(rs.getInt("corrective_entry_flag"));
            SelectedRecord.setCorrectiveamount("" + rs.getInt("CORRECTIVE_AMOUNT"));
            SelectedRecord.setsettledflag(rs.getInt("settled_flag"));
            SelectedRecord.setloadingdate(rs.getTimestamp("LOADING_DATE"));
            SelectedRecord.settransactionid(rs.getInt("transaction_id"));
            SelectedRecord.settransactiondate(rs.getTimestamp("TRANSACTION_DATE"));
            SelectedRecord.setsettlementdate(rs.getTimestamp("SETTLEMENT_DATE"));
            SelectedRecord.settransactionsequence(rs.getString("TRANSACTION_SEQUENCE"));
            SelectedRecord.settransactiontype(rs.getString("TRANSACTION_TYPE"));
            SelectedRecord.setrecordtype(rs.getInt("record_type"));
            SelectedRecord.setcomments(rs.getString("COMMENTS"));
            SelectedRecord.setfileid(rs.getInt("file_id"));
            SelectedRecord.setcardno(rs.getString("CARD_NO"));
            SelectedRecord.setamount(rs.getFloat("AMOUNT"));
            SelectedRecord.setcurrency(rs.getString("CURRENCY"));
            SelectedRecord.setcurrencyid(rs.getInt("CURRENCY_ID"));
            SelectedRecord.setcustomeraccountnumber(rs.getString("CUSTOMER_ACCOUNT_NUMBER"));
            SelectedRecord.setresponsecode(rs.getString("RESPONSE_CODE"));
            SelectedRecord.setresponsecodeid(rs.getInt("RESPONSE_CODE_ID"));
            SelectedRecord.settransactiontypeid(rs.getInt("TRANSACTION_TYPE_ID"));
            SelectedRecord.setterminal(rs.getString("TERMINAL"));
            SelectedRecord.settransactiontime(rs.getTimestamp("TRANSACTION_TIME"));
            SelectedRecord.settransactionsequenceorderby(rs.getString("TRANSACTION_SEQUENCE_ORDER_BY"));
            SelectedRecord.setcolumn1(rs.getString("COLUMN1"));
            SelectedRecord.setcolumn2(rs.getString("COLUMN2"));
            SelectedRecord.setcolumn3(rs.getString("COLUMN3"));
            SelectedRecord.setcolumn4(rs.getString("COLUMN4"));
            SelectedRecord.setcolumn5(rs.getString("COLUMN5"));
            SelectedRecord.settransactiontypemaster(rs.getInt("TRANSACTION_TYPE_MASTER"));
            SelectedRecord.setresponsecodemaster(rs.getInt("RESPONSE_CODE_MASTER"));
            SelectedRecord.setcardnosuffix(rs.getString("CARD_NO_SUFFIX"));
            SelectedRecord.setacquirercurrency(rs.getString("ACQUIRER_CURRENCY"));
            SelectedRecord.setacquirercurrencyid(rs.getInt("ACQUIRER_CURRENCY_ID"));
            SelectedRecord.setacquireramount(rs.getFloat("ACQUIRER_AMOUNT"));
            SelectedRecord.setsettlementcurrency(rs.getString("SETTLEMENT_CURRENCY"));
            SelectedRecord.setsettlementcurrencyid(rs.getInt("SETTLEMENT_CURRENCY_ID"));
            SelectedRecord.setsettlementamount(rs.getFloat("SETTLEMENT_AMOUNT"));
            SelectedRecord.setauthorizationno(rs.getString("AUTHORIZATION_NO"));
            SelectedRecord.setreferenceno(rs.getString("REFERENCE_NO"));
            SelectedRecord.setbank(rs.getString("BANK"));
            SelectedRecord.setnetworkcode(rs.getString("NETWORK_CODE"));
            SelectedRecord.setprocesscode(rs.getString("PROCESS_CODE"));
            SelectedRecord.setareacode(rs.getString("AREA_CODE"));
            SelectedRecord.setreportid(rs.getString("REPORT_ID"));
            SelectedRecord.setnetworkcomm(rs.getInt("NETWORK_COMM"));
            SelectedRecord.setnetworkcommflag(rs.getString("NETWORK_COMM_FLAG"));
            SelectedRecord.setfeelevel(rs.getString("FEE_LEVEL"));
            SelectedRecord.setnetworkid(rs.getString("NETWORK_ID"));
            SelectedRecord.setRownum(rs.getInt("rn"));
        }
        super.postSelect(rs);

        return SelectedRecord;
    }

    public Object findothersideMatched(Object... obj) throws Throwable {
        super.preSelect();
        disputesDTOInter SelectedRecord = DTOFactory.createdisputesDTO();
        disputesDTOInter RecordToSelect = (disputesDTOInter) obj[0];
        String selectStat = "select ROWNUM AS rn,corrective_entry_flag,CORRECTIVE_AMOUNT,settled_flag,file_id,LOADING_DATE,record_type,transaction_id,TRANSACTION_DATE,SETTLEMENT_DATE,TRANSACTION_SEQUENCE,TRANSACTION_TYPE," + super.GetDecryptCardNo() + "CARD_NO,AMOUNT,CURRENCY,CURRENCY_ID,CUSTOMER_ACCOUNT_NUMBER,RESPONSE_CODE,RESPONSE_CODE_ID,TRANSACTION_TYPE_ID,TERMINAL,TRANSACTION_TIME,TRANSACTION_SEQUENCE_ORDER_BY,COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5,TRANSACTION_TYPE_MASTER,RESPONSE_CODE_MASTER,CARD_NO_SUFFIX,ACQUIRER_CURRENCY,ACQUIRER_CURRENCY_ID,ACQUIRER_AMOUNT,SETTLEMENT_CURRENCY,SETTLEMENT_CURRENCY_ID,SETTLEMENT_AMOUNT,AUTHORIZATION_NO,REFERENCE_NO,BANK,NETWORK_CODE,PROCESS_CODE,AREA_CODE,REPORT_ID,NETWORK_COMM,NETWORK_COMM_FLAG,FEE_LEVEL,(select name from networks where id = NETWORK_ID)NETWORK_ID From Matched_data "
                + "  where " + super.GetDecryptLinc() + " = TRANSACTION_ID and match_key = $transid and  RECORD_TYPE <> $recordtype";

        selectStat = selectStat.replace("$transid", "" + RecordToSelect.getmatchkey());
        selectStat = selectStat.replace("$recordtype", "" + RecordToSelect.getrecordtype());
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            SelectedRecord.setCorrectiveentryflag(rs.getInt("corrective_entry_flag"));
            SelectedRecord.setCorrectiveamount("" + rs.getInt("CORRECTIVE_AMOUNT"));
            SelectedRecord.setloadingdate(rs.getTimestamp("LOADING_DATE"));

            SelectedRecord.setsettledflag(rs.getInt("settled_flag"));
            SelectedRecord.setrecordtype(rs.getInt("record_type"));
            SelectedRecord.settransactionid(rs.getInt("transaction_id"));
            SelectedRecord.settransactiondate(rs.getTimestamp("TRANSACTION_DATE"));
            SelectedRecord.setsettlementdate(rs.getTimestamp("SETTLEMENT_DATE"));
            SelectedRecord.settransactionsequence(rs.getString("TRANSACTION_SEQUENCE"));
            SelectedRecord.settransactiontype(rs.getString("TRANSACTION_TYPE"));
            SelectedRecord.setfileid(rs.getInt("file_id"));
            SelectedRecord.setcardno(rs.getString("CARD_NO"));
            SelectedRecord.setamount(rs.getFloat("AMOUNT"));
            SelectedRecord.setcurrency(rs.getString("CURRENCY"));
            SelectedRecord.setcurrencyid(rs.getInt("CURRENCY_ID"));
            SelectedRecord.setcustomeraccountnumber(rs.getString("CUSTOMER_ACCOUNT_NUMBER"));
            SelectedRecord.setresponsecode(rs.getString("RESPONSE_CODE"));
            SelectedRecord.setresponsecodeid(rs.getInt("RESPONSE_CODE_ID"));
            SelectedRecord.settransactiontypeid(rs.getInt("TRANSACTION_TYPE_ID"));
            SelectedRecord.setterminal(rs.getString("TERMINAL"));
            SelectedRecord.settransactiontime(rs.getTimestamp("TRANSACTION_TIME"));
            SelectedRecord.settransactionsequenceorderby(rs.getString("TRANSACTION_SEQUENCE_ORDER_BY"));
            SelectedRecord.setcolumn1(rs.getString("COLUMN1"));
            SelectedRecord.setcolumn2(rs.getString("COLUMN2"));
            SelectedRecord.setcolumn3(rs.getString("COLUMN3"));
            SelectedRecord.setcolumn4(rs.getString("COLUMN4"));
            SelectedRecord.setcolumn5(rs.getString("COLUMN5"));
            SelectedRecord.settransactiontypemaster(rs.getInt("TRANSACTION_TYPE_MASTER"));
            SelectedRecord.setresponsecodemaster(rs.getInt("RESPONSE_CODE_MASTER"));
            SelectedRecord.setcardnosuffix(rs.getString("CARD_NO_SUFFIX"));
            SelectedRecord.setacquirercurrency(rs.getString("ACQUIRER_CURRENCY"));
            SelectedRecord.setacquirercurrencyid(rs.getInt("ACQUIRER_CURRENCY_ID"));
            SelectedRecord.setacquireramount(rs.getFloat("ACQUIRER_AMOUNT"));
            SelectedRecord.setsettlementcurrency(rs.getString("SETTLEMENT_CURRENCY"));
            SelectedRecord.setsettlementcurrencyid(rs.getInt("SETTLEMENT_CURRENCY_ID"));
            SelectedRecord.setsettlementamount(rs.getFloat("SETTLEMENT_AMOUNT"));
            SelectedRecord.setauthorizationno(rs.getString("AUTHORIZATION_NO"));
            SelectedRecord.setreferenceno(rs.getString("REFERENCE_NO"));
            SelectedRecord.setbank(rs.getString("BANK"));
            SelectedRecord.setnetworkcode(rs.getString("NETWORK_CODE"));
            SelectedRecord.setprocesscode(rs.getString("PROCESS_CODE"));
            SelectedRecord.setareacode(rs.getString("AREA_CODE"));
            SelectedRecord.setreportid(rs.getString("REPORT_ID"));
            SelectedRecord.setnetworkcomm(rs.getInt("NETWORK_COMM"));
            SelectedRecord.setnetworkcommflag(rs.getString("NETWORK_COMM_FLAG"));
            SelectedRecord.setfeelevel(rs.getString("FEE_LEVEL"));
            SelectedRecord.setnetworkid(rs.getString("NETWORK_ID"));
            SelectedRecord.setRownum(rs.getInt("rn"));
        }
        super.postSelect(rs);

        return SelectedRecord;
    }

    public Object UpdateCommentRecord(Object... obj) throws Throwable {
        super.preUpdate();
        disputesDTOInter RecordToSelect = (disputesDTOInter) obj[0];
        String UpdateStat = "Update disputes Set comments = '$comment'"
                + "  where  transaction_id = $transactionid and record_type = $recordtype";

        UpdateStat = UpdateStat.replace("$recordtype", "" + RecordToSelect.getrecordtype());
        UpdateStat = UpdateStat.replace("$transactionid", "" + RecordToSelect.gettransactionid());
        UpdateStat = UpdateStat.replace("$comment", "" + RecordToSelect.getcomments());

        String msg = super.executeUpdate(UpdateStat);
        msg = msg + " Comment Has Been Updated";
        super.postUpdate("Update Comment In dispute with transaction id = " + RecordToSelect.gettransactionid(), false);
        return msg;
    }

    public void correctiveamountupdate(Object... obj) throws Throwable {
        super.preUpdate();
        disputesDTOInter RecordToSelect = (disputesDTOInter) obj[0];
        String UpdateStat = "Update disputes Set CORRECTIVE_AMOUNT = $corrective "
                + "  where  transaction_id = $transactionid and record_type = $recordtype";
        UpdateStat = UpdateStat.replace("$recordtype", "" + RecordToSelect.getrecordtype());
        UpdateStat = UpdateStat.replace("$transactionid", "" + RecordToSelect.gettransactionid());
        UpdateStat = UpdateStat.replace("$corrective", "" + RecordToSelect.getCorrectiveamount());

        super.executeUpdate(UpdateStat);
        super.postUpdate("Update corrective amount with transaction id = " + RecordToSelect.gettransactionid(), false);
    }

    public void exportRecord(Object... obj) throws Throwable {
        super.preUpdate();
        disputesDTOInter RecordToSelect = (disputesDTOInter) obj[0];
        String UpdateStat = "Update disputes Set corrective_entry_flag = $correctiveflag "
                + "  where  transaction_id = $transactionid and record_type = $recordtype";
        UpdateStat = UpdateStat.replace("$recordtype", "" + RecordToSelect.getrecordtype());
        UpdateStat = UpdateStat.replace("$transactionid", "" + RecordToSelect.gettransactionid());
        UpdateStat = UpdateStat.replace("$correctiveflag", "" + RecordToSelect.getCorrectiveentryflag());

        super.executeUpdate(UpdateStat);
        super.postUpdate("Update corrective Status to export with transaction id = " + RecordToSelect.gettransactionid(), false);
    }

    public Object UpdatesetteledRecord(Object... obj) throws Throwable {
        super.preUpdate();
        disputesDTOInter RecordToSelect = (disputesDTOInter) obj[0];
        String UpdateStat = "Update disputes Set settled_flag = '$settledflag',settled_user = '$settleduser',SETTLEMENT_DATE=to_date('$settleddate','dd.mm.yyyy hh24:mi:ss')"
                + "  where  transaction_id = $transactionid and record_type = $recordtype";
        UpdateStat = UpdateStat.replace("$settleddate", DateFormatter.changeDateAndTimeFormat(RecordToSelect.getsettlementdate()));
        UpdateStat = UpdateStat.replace("$recordtype", "" + RecordToSelect.getrecordtype());
        UpdateStat = UpdateStat.replace("$transactionid", "" + RecordToSelect.gettransactionid());
        UpdateStat = UpdateStat.replace("$settledflag", "" + RecordToSelect.getsettledflag());
        UpdateStat = UpdateStat.replace("$settleduser", "" + RecordToSelect.getsettleduser());
        String msg = super.executeUpdate(UpdateStat);
        msg = msg + " Settled Status Has Been Updated";
        String status = "";
        if (RecordToSelect.getsettledflag() == 1) {
            status = "Setteled";
        } else {
            status = "Not Setteled";
        }
        super.postUpdate("Update Settled Status In dispute by " + status + " with transaction id = " + RecordToSelect.gettransactionid(), false);
        return msg;
    }

    public Object UpdatecorrectiveRecord(Object... obj) throws Throwable {
        super.preUpdate();
        disputesDTOInter RecordToSelect = (disputesDTOInter) obj[0];
        String UpdateStat = "Update disputes Set corrective_entry_flag = '$settledflag'"
                + "  where  transaction_id = $transactionid and record_type = $recordtype";

        UpdateStat = UpdateStat.replace("$recordtype", "" + RecordToSelect.getrecordtype());
        UpdateStat = UpdateStat.replace("$transactionid", "" + RecordToSelect.gettransactionid());
        UpdateStat = UpdateStat.replace("$settledflag", "" + RecordToSelect.getCorrectiveentryflag());

        String msg = super.executeUpdate(UpdateStat);
        msg = msg + " corrective Status Has Been Updated";
        String status = "";
        if (RecordToSelect.getCorrectiveentryflag()== 1) {
            status = "corrective";
        } else {
            status = "Not corrective";
        }
        super.postUpdate("Update corrective Status In dispute by " + status + " with transaction id = " + RecordToSelect.gettransactionid(), false);
        return msg;
    }

    public Integer savePrint(Object... obj) throws SQLException, Throwable {
        disputesDTOInter[] entities = (disputesDTOInter[]) obj[0];
        String PAGE = (String) obj[1];
        String FIELDS = (String) obj[2];
        Integer seq = super.generateSequence("disputes_report");
        String insertStat = getinsertstat(PAGE, FIELDS);
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.length; i++) {
                disputesDTOInter RecordtoInsert = entities[i];
                statement.setInt(1, seq);
                if (PAGE.contains("Rejected")) {
                    statement.setInt(2, RecordtoInsert.gettransactionid());
                } else if (PAGE.contains("Matched")) {
                    statement.setString(2, RecordtoInsert.getmatchkey());
                } else {
                    statement.setInt(2, RecordtoInsert.getdisputekey());
                    statement.setInt(3, RecordtoInsert.gettransactionid());
                }

                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            int[] numUpdates = statement.executeBatch();
            Integer valid = 0, notvalid = 0;

        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
        return seq;
    }

    public String getinsertstat(Object... obj) throws Throwable {
        String PAGE = (String) obj[0];
        String FIELDS = (String) obj[1];
        String Insert = "";
        String Select = "";
        int index = 1;
        String[] ColCount = FIELDS.split(",");
        for (String Col : ColCount) {
            if (index < 11) {
                Insert += "COL" + index + ",";
                Select += Col + ",";
            }
            index = index + 1;
        }
        String insertStat = "";
        if (PAGE.contains("Rejected")) {
            Select = Select.replaceAll("reason", "get_rejected_reseaon(record_type,transaction_id) REASON");
            Select = Select.replaceAll("response_code_id,", "nvl(response_code_id,'0.0')response_code_id,");
            insertStat = "insert into disputes_report (ReportID,$Insert Network,record_type,amount, currency) \n"
                    + "select ?,$Select 'No Network','1',amount,currency from rejected_transactions\n"
                    + "where transaction_id = ?";
        } else if (PAGE.contains("Matched")) {
            Select = Select.replace("REJECTED_RESEAON", "GET_REJECTED_RESEAON(RECORD_TYPE,TRANSACTION_ID) REJECTED_RESEAON");
            Select = Select.replaceAll("response_code_id,", "nvl(response_code_id,'0.0')response_code_id,");
            insertStat = "insert into disputes_report (ReportID,$Insert Network,record_type,amount, samount, aqamount, currency, settlement_curr, acquirer_curr) \n"
                    + "select ?,$Select (select h.name from networks h where h.id = NETWORK_ID) network,record_type,NVL(amount, '0.0') amount,settlement_amount,acquirer_amount,currency,settlement_currency,acquirer_currency from matched_data\n"
                    + "where " + super.GetDecryptLinc() + " = TRANSACTION_ID and match_key = ? order by NETWORK_ID";
        } else {
            Select = Select.replace("SETTLED_FLAG", "DECODE(SETTLED_FLAG,1,'Setteled','2','Not Setteled','Other') SETTLED_FLAG");
            Select = Select.replaceAll("response_code_id,", "nvl(response_code_id,'0.0')response_code_id,");
            insertStat = "insert into disputes_report (ReportID,$Insert Network,record_type,amount, samount, aqamount, currency, settlement_curr, acquirer_curr) \n"
                    + "select ?,$Select (select h.name from networks h where h.id = NETWORK_ID) network,record_type,NVL(CORRECTIVE_AMOUNT, NVL(amount, '0.0')) amount,settlement_amount,acquirer_amount,currency,settlement_currency,acquirer_currency from disputes\n"
                    + "where " + super.GetDecryptLinc() + " = TRANSACTION_ID and (dispute_key = ? or (TRANSACTION_ID = ? and dispute_key is null)) order by NETWORK_ID";

        }
        
        if (PAGE.contains("Pending")) {
            Select = Select.replace(",amount,", ",NVL(CORRECTIVE_AMOUNT, NVL(amount, '0.0')) amount,");
        }
        insertStat = insertStat.replace("$Select", Select);
        insertStat = insertStat.replace("card_no,", super.GetDecryptCardNo() + " card_no,");

        insertStat = insertStat.replace("network_id", "(select name from networks where id = NETWORK_ID)network_id");
        insertStat = insertStat.replace("acquirer_amount", "NVL(acquirer_amount, '0.0') acquirer_amount");
        insertStat = insertStat.replace("settlement_amount", "NVL(settlement_amount, '0.0') settlement_amount");
        insertStat = insertStat.replace("loading_date", "to_char(loading_date,'dd.MM.yyyy hh24:mi:ss') loading_date");
        insertStat = insertStat.replace("transaction_date", "to_char(transaction_date,'dd.MM.yyyy hh24:mi:ss') transaction_date");
        insertStat = insertStat.replace("settlement_date", "to_char(settlement_date,'dd.MM.yyyy hh24:mi:ss') settlement_date");

        insertStat = insertStat.replace("$Insert", Insert);
        System.out.println(insertStat);
        return insertStat;
    }

    public String findMatchCret(Object... obj) throws Throwable {
        String Query = (String) obj[0];
        String Result = "";
        super.preSelect();
        String selectStat = "Select " + Query + " From Dual";
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            Result = rs.getString(1);
        }
        super.postSelect(rs);
        return Result;
    }

    public ResultSet findRecordsPrint(Object... obj) throws Throwable {
        Integer SEQ = (Integer) obj[0];
        super.preSelect();
        String selectStat = "SELECT COL1,COL2,COL3,COL4,COL5,COL6,COL7,COL8,COL9,COL10,Network,REPORTID,record_type, amount, samount, aqamount FROM disputes_report WHERE ReportID = $ReportID";
        selectStat = selectStat.replace("$ReportID", "" + SEQ);
        ResultSet rs = executeQuery(selectStat);
        return rs;
    }
}
