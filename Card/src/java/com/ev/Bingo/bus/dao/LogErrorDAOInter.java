/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

/**
 *
 * @author AlyShiha
 */
public interface LogErrorDAOInter {

    Object findExceptionLoadingErrors(String dateFrom, String dateTo) throws Throwable;
    
}
