package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import com.ev.Bingo.bus.dto.CommFeeReportDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.commissionfeesDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class commissionfeesDAO extends BaseDAO implements commissionfeesDAOInter {

    protected commissionfeesDAO() {
        super();
        super.setTableName("COMMISSION_FEES");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        commissionfeesDTOInter RecordToInsert = (commissionfeesDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (NETWORK_ID,BAN,ID) "
                + " values "
                + " ($networkid,'$ban',$id)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$networkid", "" + RecordToInsert.getnetworkid());
        insertStat = insertStat.replace("$ban", "" + RecordToInsert.getban());
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        String msg = super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To COMMISSION_FEES", false);
        return msg + " Has Been Inserted";
    }

    public Boolean ValidateNull(Object... obj) {
        commissionfeesDTOInter RecordToInsert = (commissionfeesDTOInter) obj[0];
        if (RecordToInsert.getid() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        commissionfeesDTOInter RecordToUpdate = (commissionfeesDTOInter) obj[0];
        String UpdateStat = "update $table set "
                + " (NETWORK_ID= $networkid,BAN= '$ban',ID= $id) "
                + "  where  ID= $id";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$networkid", "" + RecordToUpdate.getnetworkid());
        UpdateStat = UpdateStat.replace("$ban", "" + RecordToUpdate.getban());
        UpdateStat = UpdateStat.replace("$id", "" + RecordToUpdate.getid());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table COMMISSION_FEES", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        commissionfeesDTOInter RecordToDelete = (commissionfeesDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$networkid", "" + RecordToDelete.getnetworkid());
        deleteStat = deleteStat.replace("$ban", "" + RecordToDelete.getban());
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        String msg = super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table COMMISSION_FEES", false);
        return msg + " Record Has Been Deleted";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table COMMISSION_FEES", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        commissionfeesDTOInter RecordToSelect = (commissionfeesDTOInter) obj[0];
        String selectStat = "Select NETWORK_ID,BAN,ID From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        commissionfeesDTOInter SelectedRecord = DTOFactory.createcommissionfeesDTO();
        while (rs.next()) {
            SelectedRecord.setnetworkid(rs.getInt("NETWORK_ID"));
            SelectedRecord.setban(rs.getString("BAN"));
            SelectedRecord.setid(rs.getInt("ID"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        networksDTOInter RecordToSelect = (networksDTOInter) obj[0];
        String selectStat = "Select NETWORK_ID,BAN,ID From $table"
                + "  where  NETWORK_ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<commissionfeesDTOInter> Records = new ArrayList<commissionfeesDTOInter>();
        while (rs.next()) {
            commissionfeesDTOInter SelectedRecord = DTOFactory.createcommissionfeesDTO();
            SelectedRecord.setnetworkid(rs.getInt("NETWORK_ID"));
            SelectedRecord.setban(rs.getString("BAN"));
            SelectedRecord.setid(rs.getInt("ID"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }
 public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        Date fromdate = (Date) obj[0];
        Date todate = (Date) obj[1];
        Integer network = (Integer) obj[2];
        String pin = (String) obj[3];
        Integer user = (Integer) obj[4];
        Boolean showpin = (Boolean) obj[5];
        String selectStat = "";
        if (showpin) {
            selectStat = "Select (select k.name from networks k where k.id = NETWORK_ID) NETWORK,PIN,"
                    + " Sum(Comm) CommSum,Sum(FEE) FeeSum,COUNT(case when comm <> 0 then 1 end) AS CommCount,COUNT(case when fee <> 0 then 1 end) AS FeeCount "
                    + " From COMMISSION_FEES_REPORT"
                    + " WHERE TRANSACTION_DATE BETWEEN to_date('$fromdate','dd.mm.yyyy hh24:mi:ss') AND to_date('$todate','dd.mm.yyyy hh24:mi:ss')";
        } else {
            selectStat = "Select (select k.name from networks k where k.id = NETWORK_ID) NETWORK,"
                    + " Sum(Comm) CommSum,Sum(FEE) FeeSum,COUNT(case when comm <> 0 then 1 end) AS CommCount,COUNT(case when fee <> 0 then 1 end) AS FeeCount "
                    + " From COMMISSION_FEES_REPORT"
                    + " WHERE TRANSACTION_DATE BETWEEN to_date('$fromdate','dd.mm.yyyy hh24:mi:ss') AND to_date('$todate','dd.mm.yyyy hh24:mi:ss')";
        }
        selectStat = selectStat.replace("$fromdate", "" + DateFormatter.changeDateAndTimeFormat(fromdate));
        selectStat = selectStat.replace("$todate", "" + DateFormatter.changeDateAndTimeFormat(todate));
        if (network != 0 && network != null) {
            selectStat = selectStat + " AND Network_ID = $network";
            selectStat = selectStat.replace("$network", "" + network);
        }
        if (pin != null && !pin.equals("")) {
            selectStat = selectStat + " AND PIN LIKE '%$pin%'";
            selectStat = selectStat.replace("$pin", "" + pin);
        }
        selectStat = selectStat + " AND Network_ID in (select n.networkid from user_network n where n.userid = $user) ";
        selectStat = selectStat.replace("$user", "" + user);
        if (showpin) {
            selectStat = selectStat + " Group by Network_ID,PIN";
        } else {
            selectStat = selectStat + " Group by Network_ID";
        }
        ResultSet rs = executeQuery(selectStat);
        List<CommFeeReportDTOInter> Records = new ArrayList<CommFeeReportDTOInter>();
        while (rs.next()) {
            CommFeeReportDTOInter SelectedRecord = DTOFactory.createCommFeeReportDTO();
            SelectedRecord.setNetwork(rs.getString("NETWORK"));
            if (showpin) {
            SelectedRecord.setPin(rs.getString("PIN"));}
            SelectedRecord.setComm(rs.getFloat("CommSum"));
            SelectedRecord.setCommtranscount(rs.getInt("CommCount"));
            SelectedRecord.setFee(rs.getFloat("FeeSum"));
            SelectedRecord.setFeetranscount(rs.getInt("FeeCount"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAllResultSet(Object... obj) throws Throwable {
        super.preSelect();
        Date fromdate = (Date) obj[0];
        Date todate = (Date) obj[1];
        Integer network = (Integer) obj[2];
        String pin = (String) obj[3];
        Integer user = (Integer) obj[4];
        Boolean showpin = (Boolean) obj[5];
        String selectStat = "";
        if (showpin) {
            selectStat = "Select (select k.name from networks k where k.id = NETWORK_ID) NETWORK,PIN,"
                    + " Sum(Comm) CommSum,Sum(FEE) FeeSum,Count(TRANSACTION_ID) CommCount,Count(TRANSACTION_ID) FeeCount "
                    + " From COMMISSION_FEES_REPORT"
                    + " WHERE TRANSACTION_DATE BETWEEN to_date('$fromdate','dd.mm.yyyy hh24:mi:ss') AND to_date('$todate','dd.mm.yyyy hh24:mi:ss')";
        } else {
            selectStat = "Select (select k.name from networks k where k.id = NETWORK_ID) NETWORK,"
                    + " Sum(Comm) CommSum,Sum(FEE) FeeSum,Count(TRANSACTION_ID) CommCount,Count(TRANSACTION_ID) FeeCount "
                    + " From COMMISSION_FEES_REPORT"
                    + " WHERE TRANSACTION_DATE BETWEEN to_date('$fromdate','dd.mm.yyyy hh24:mi:ss') AND to_date('$todate','dd.mm.yyyy hh24:mi:ss')";
        }
        selectStat = selectStat.replace("$fromdate", "" + DateFormatter.changeDateAndTimeFormat(fromdate));
        selectStat = selectStat.replace("$todate", "" + DateFormatter.changeDateAndTimeFormat(todate));
        if (network != 0 && network != null) {
            selectStat = selectStat + " AND Network_ID = $network";
            selectStat = selectStat.replace("$network", "" + network);
        }
        if (pin != null && !pin.equals("")) {
            selectStat = selectStat + " AND PIN LIKE '%$pin%'";
            selectStat = selectStat.replace("$pin", "" + pin);
        }
        selectStat = selectStat + " AND Network_ID in (select n.networkid from user_network n where n.userid = $user) ";
        selectStat = selectStat.replace("$user", "" + user);
        if (showpin) {
            selectStat = selectStat + " Group by Network_ID,PIN";
        } else {
            selectStat = selectStat + " Group by Network_ID";
        }
        ResultSet rs = executeQuery(selectStat);
        return rs;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<commissionfeesDTOInter> entities = (List<commissionfeesDTOInter>) obj[0];
        String insertStat = "Update COMMISSION_FEES Set BAN = ? Where ID = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        String mess = "", mess2 = "";
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.size(); i++) {
                commissionfeesDTOInter RecordtoInsert = entities.get(i);
                statement.setString(1, RecordtoInsert.getban());
                statement.setInt(2, RecordtoInsert.getid());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            try {
                int[] numUpdates = statement.executeBatch();
                Integer valid = 0, notvalid = 0;

                for (int i = 0; i < numUpdates.length; i++) {
                    if (numUpdates[i] == -2) {
                        valid++;
                        mess = valid + " rows has been updated";
                    } else {
                        notvalid++;
                        mess2 = notvalid + " can`t be updated";
                    }
                }
                if (!mess2.equals("")) {
                    mess = mess + "," + mess2;
                }
            } catch (Exception ex) {
                mess = "pins can't be Duplicated";
            }
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
        return mess;
    }
}
