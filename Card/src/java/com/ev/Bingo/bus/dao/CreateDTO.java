package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import java.sql.ResultSet;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CreateDTO extends BaseDAO {

    public static final List toList(ResultSet rs, List wantedColumnNames) throws SQLException {
        List rows = new ArrayList();

        int numWantedColumns = wantedColumnNames.size();
        while (rs.next()) {
            Map row = new LinkedHashMap();

            for (int i = 0; i < numWantedColumns; ++i) {
                String columnName = (String) wantedColumnNames.get(i);
                Object value = rs.getObject(columnName);
                row.put(columnName, value);
            }

            rows.add(row);
        }

        return rows;
    }

    public void runtest() throws Throwable {

        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String sql = "select * from USERS";
        ResultSet resultset = executeQuery(sql);//from DB      
        int numcols = resultset.getMetaData().getColumnCount();
        List ColNames = new ArrayList();
        List ResultSetData = new ArrayList();
        for (int i = 1; i < numcols; i++) {
            ColNames.add(resultset.getMetaData().getColumnName(i));
        }
        ResultSetData = toList(resultset, ColNames);
        super.postSelect(resultset);
    }

    public CreateDTO() {
        super();
        try {

            runDTO();
            runDAO();
            runBO();
        } catch (Throwable ex) {
            Logger.getLogger(CreateDTO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<String> findAllTables() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select distinct t.TABLE_NAME from all_tab_columns t where t.user = 'CARDS'";
        ResultSet result = executeQuery(selectStat);
        List<String> Tables = new ArrayList<String>();
        while (result.next()) {
            Tables.add(result.getString(1));
        }
        super.postSelect(result);
        return Tables;
    }

    public List<String> findAllNulls(String TableName) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "SELECT cons.COLUMN_NAME,cons.DATA_TYPE"
                + " FROM all_tab_cols cons"
                + " WHERE  cons.user = 'CARDS'"
                + " AND cons.TABLE_NAME = '$TName'"
                + " and cons.NULLABLE = 'N'";
        selectStat = selectStat.replace("$TName", "" + TableName);
        ResultSet result = executeQuery(selectStat);
        List<String> Col = new ArrayList<String>();
        while (result.next()) {
            Col.add(result.getString(1) + "-" + result.getString(2));
        }
        super.postSelect(result);
        return Col;
    }

    public List<String> findAllKeys(String TableName) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "SELECT cols.column_name,(select t.DATA_TYPE from all_tab_columns t where t.user = 'CARDS' and t.TABLE_NAME = cons.TABLE_NAME and t.COLUMN_NAME = cols.COLUMN_NAME) DataType"
                + " FROM all_constraints cons, all_cons_columns cols"
                + " WHERE cons.constraint_type = 'P'"
                + " AND cons.constraint_name = cols.constraint_name"
                + " AND cons.user = cols.user"
                + " AND cons.user = 'CARDS'"
                + " AND cons.TABLE_NAME = '$TName'";
        selectStat = selectStat.replace("$TName", "" + TableName);
        ResultSet result = executeQuery(selectStat);
        List<String> Col = new ArrayList<String>();
        while (result.next()) {
            Col.add(result.getString(1) + "-" + result.getString(2));
        }
        super.postSelect(result);
        return Col;
    }

    public List<String> ValidateColoumns(String TableName) throws Throwable {
        return null;
    }

    public List<String> findAllColoumns(String TableName) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select t.COLUMN_NAME,t.DATA_TYPE from all_tab_columns t where t.user = 'CARDS' and t.TABLE_NAME = '$TName'";
        selectStat = selectStat.replace("$TName", "" + TableName);
        ResultSet result = executeQuery(selectStat);
        List<String> Col = new ArrayList<String>();
        while (result.next()) {
            Col.add(result.getString(1).replace("_", "").toLowerCase() + "-" + result.getString(2) + "-" + result.getString(1));
        }
        super.postSelect(result);
        return Col;
    }

    public void WriteDTOHeader(String Filename, BufferedWriter out) throws Throwable {
        try {
            out.write("package com.ev.Bingo.bus.dto;");
            out.newLine();
            out.write("import com.ev.Bingo.base.dto.BaseDTO;");
            out.newLine();
            out.write("import com.ev.Bingo.bus.dao.DTOFactory;");
            out.newLine();
            out.write("import java.util.Date;");
            out.newLine();
            out.write("import java.io.Serializable;");
            out.newLine();
            out.write("public class " + Filename + "DTO extends BaseDTO implements " + Filename + "DTOInter, Serializable {");
            out.newLine();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteBOHeader(String Filename, BufferedWriter out) throws Throwable {
        try {
            out.write("package com.ev.Bingo.bus.bo;");
            out.newLine();
            out.write("import com.ev.Bingo.base.bo.BaseBO;");
            out.newLine();
            out.write("import java.sql.SQLException;");
            out.newLine();
            out.write("import com.ev.Bingo.bus.dao." + Filename + "DAOInter;");
            out.newLine();
            out.write("import com.ev.Bingo.bus.dao.DAOFactory;");
            out.newLine();
            out.write("import com.ev.Bingo.base.util.DateFormatter;");
            out.newLine();
            out.write("import com.ev.Bingo.bus.dto." + Filename + "DTOInter;");
            out.newLine();
            out.write("import java.util.ArrayList;");
            out.newLine();
            out.write("import java.util.List;");
            out.newLine();
            out.write("import com.ev.Bingo.bus.dto." + Filename + "DTOInter;");
            out.newLine();
            out.write("import java.text.SimpleDateFormat;");
            out.newLine();
            out.write("public class " + Filename + "Bo extends BaseBO implements " + Filename + "BoInter {");
            out.newLine();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteDAOHeader(String Filename, BufferedWriter out) throws Throwable {
        try {
            out.write("package com.ev.Bingo.bus.dao;");
            out.newLine();
            out.write("import com.ev.Bingo.base.dao.BaseDAO;");
            out.newLine();
            out.write("import com.ev.Bingo.bus.dto.DTOFactory;");
            out.newLine();
            out.write("import java.sql.ResultSet;");
            out.newLine();
            out.write("import com.ev.Bingo.base.util.DateFormatter;");
            out.newLine();
            out.write(" ");
            out.newLine();
            out.write("import java.util.ArrayList;");
            out.newLine();
            out.write("import java.util.List;");
            out.newLine();
            out.write("import com.ev.Bingo.bus.dto." + Filename + "DTOInter;");
            out.newLine();
            out.write("import java.sql.Connection;");
            out.newLine();
            out.write("import java.sql.PreparedStatement;");
            out.newLine();
            out.write("import java.sql.SQLException;");
            out.newLine();
            out.write("import java.text.SimpleDateFormat;");
            out.newLine();
            out.write("import java.util.List;");
            out.newLine();
            out.write("import javax.faces.context.FacesContext;");
            out.newLine();
            out.write("import javax.servlet.http.HttpSession;");
            out.newLine();
            out.write("public class " + Filename + "DAO extends BaseDAO implements " + Filename + "DAOInter {");
            out.newLine();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteDAOInsertAll(String Filename, BufferedWriter out, List<String> Col, String TableName) throws Throwable {
        try {
            //  List<CurrencyMasterDTOInter> cmDTOL = (List<CurrencyMasterDTOInter>) cmDAO.findAll();
            String Q = "\"";
            out.write("public void save(Object... obj) throws SQLException, Throwable {");
            out.newLine();
            out.write("List<" + Filename + "DTOInter> entities = (List<" + Filename + "DTOInter>) obj[0];");
            out.newLine();
            out.write("String insertStat = " + Q + "insert into " + TableName + " (" + GetColoumnName(Col) + ") values (" + GetColAll(Col) + ")" + Q + ";");
            out.newLine();
            out.write("Connection connection = null;");
            out.newLine();
            out.write("PreparedStatement statement = null;");
            out.newLine();
            out.write("SimpleDateFormat df = new SimpleDateFormat(" + Q + "dd.MM.yyyy HH:mm:ss" + Q + ");");
            out.newLine();
            out.write("try {");
            out.newLine();
            out.write("FacesContext context = FacesContext.getCurrentInstance();");
            out.newLine();
            out.write("HttpSession session = (HttpSession) context.getExternalContext().getSession(false);");
            out.newLine();
            out.write("IDataProvider prov = (IDataProvider) session.getAttribute(" + Q + "ReportConnectionString" + Q + ");");
            out.newLine();
            out.write("connection = prov.getConnsreport();");
            out.newLine();
            out.write("statement = connection.prepareStatement(insertStat);");
            out.newLine();
            out.write("for (int i = 0; i < entities.size(); i++) {");
            out.newLine();
            out.write(Filename + "DTOInter RecordtoInsert = entities.get(i);");
            out.newLine();
            GetInsertBatch(Col, out, Filename);
            out.write("statement.addBatch();");
            out.newLine();
            out.write("if ((i + 1) % 1000 == 0) {");
            out.newLine();
            out.write("statement.executeBatch(); ");
            out.newLine();
            out.write("}}");
            out.newLine();
            out.write("statement.executeBatch();");
            out.newLine();
            out.write(" } finally {if (statement != null) {try {connection.commit();statement.close();} catch (SQLException logOrIgnore) {}}");
            out.newLine();
            out.write("if (connection != null) {try {connection.close();} catch (SQLException logOrIgnore) {}}}");
            out.newLine();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteInterHeader(String Filename, BufferedWriter out) throws Throwable {
        try {
            out.write("package com.ev.Bingo.bus.dto;");
            out.newLine();
            out.write("import java.io.Serializable;");
            out.newLine();
            out.write("import java.util.Date;");
            out.newLine();
            out.write("public interface " + Filename + "DTOInter extends Serializable {");
            out.newLine();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteDAOInterHeader(String Filename, BufferedWriter out) throws Throwable {
        try {
            out.write("package com.ev.Bingo.bus.dao;");
            out.newLine();
            out.write("import java.util.Date;");
            out.newLine();
            out.write("import java.util.List;");
            out.newLine();
            out.write("import java.sql.SQLException;");
            out.newLine();
            out.write("public interface " + Filename + "DAOInter {");
            out.newLine();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteFactoryHeader(List<String> Tables, BufferedWriter out) throws Throwable {
        try {
            String name = "";
            out.write("package com.ev.Bingo.bus.dto;");
            out.newLine();
            out.write("public class DTOFactory {");
            out.newLine();
            out.write("public DTOFactory() {}");
            out.newLine();
            for (int i = 0; i < Tables.size(); i++) {
                name = Tables.get(i).toString().replace("_", "").toLowerCase();
                out.write("public static " + name + "DTOInter create" + name + "DTO(Object... obj) {");
                out.newLine();
                out.write("return new " + name + "DTO();");
                out.newLine();
                out.write("}");
                out.newLine();
            }

        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteDAOFactoryHeader(List<String> Tables, BufferedWriter out) throws Throwable {
        try {
            String name = "";
            out.write("package com.ev.Bingo.bus.dao;");
            out.newLine();
            out.write("public class DAOFactory {");
            out.newLine();
            out.write("public DAOFactory() {}");
            out.newLine();
            for (int i = 0; i < Tables.size(); i++) {
                name = Tables.get(i).toString().replace("_", "").toLowerCase();
                out.write("public static " + name + "DAOInter create" + name + "DAO(Object... obj) {");
                out.newLine();
                out.write("return new " + name + "DAO();");
                out.newLine();
                out.write("}");
                out.newLine();
            }

        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteBOFactoryHeader(List<String> Tables, BufferedWriter out) throws Throwable {
        try {
            String name = "";
            out.write("package com.ev.Bingo.bus.bo;");
            out.newLine();
            out.write("public class BOFactory {");
            out.newLine();
            out.write("public BOFactory() {}");
            out.newLine();
            for (int i = 0; i < Tables.size(); i++) {
                name = Tables.get(i).toString().replace("_", "").toLowerCase();
                out.write("public static " + name + "BOInter create" + name + "BO(Object... obj) {");
                out.newLine();
                out.write("return new " + name + "BO();");
                out.newLine();
                out.write("}");
                out.newLine();
            }

        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public String GetDataType(String Type) {
        String DataType = "";
        if (Type.equals("NUMBER")) {
            DataType = "int";
        } else if (Type.equals("VARCHAR2")) {
            DataType = "String";
        } else if (Type.equals("DATE")) {
            DataType = "Date";
        } else {
            DataType = "Other";
        }
        return DataType;
    }

    public String GetVariable(List<String> Col) {
        String var = "", VarName = "", DataType = "";
        String[] ColDetail = new String[3];

        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            VarName = ColDetail[0].toLowerCase();
            DataType = ColDetail[1].toUpperCase();
            if (DataType.equals("NUMBER")) {
                if (i != Col.size() - 1) {
                    var = var + "$" + VarName + ",";
                } else {
                    var = var + "$" + VarName;
                }
            } else if (DataType.equals("VARCHAR2")) {
                if (i != Col.size() - 1) {
                    var = var + "'$" + VarName + "',";
                } else {
                    var = var + "'$" + VarName + "'";
                }
            } else if (DataType.equals("DATE")) {
                if (i != Col.size() - 1) {
                    var = var + "'$" + VarName + "',";
                } else {
                    var = var + "'$" + VarName + "'";
                }
            }
        }
        return var;
    }

    public String GetColAll(List<String> Col) {
        String var = "", DataType = "";
        String[] ColDetail = new String[3];

        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            DataType = ColDetail[1].toUpperCase();
            if (DataType.equals("NUMBER")) {
                if (i != Col.size() - 1) {
                    var = var + "?,";
                } else {
                    var = var + "?";
                }
            } else if (DataType.equals("VARCHAR2")) {
                if (i != Col.size() - 1) {
                    var = var + "?,";
                } else {
                    var = var + "?";
                }
            } else if (DataType.equals("DATE")) {
                if (i != Col.size() - 1) {
                    //
                    var = var + "to_date(?,'dd.mm.yyyy hh24:mi:ss'),";
                } else {
                    var = var + "to_date(?,'dd.mm.yyyy hh24:mi:ss')";
                }
            }
        }
        return var;
    }

    public String GetColoumnName(List<String> Col) {
        String var = "", ColName = "";
        String[] ColDetail = new String[3];

        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            ColName = ColDetail[2];
            if (i != Col.size() - 1) {
                var = var + ColName + ",";
            } else {
                var = var + ColName;
            }
        }
        return var;
    }

    public String GetInsertBatch(List<String> Col, BufferedWriter out, String FileName) throws Throwable {
        String var = "", ColName = "", VarName = "", DataType = "";
        String[] ColDetail = new String[3];
        Integer temp = 1;
        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            ColName = ColDetail[2];
            VarName = ColDetail[0].toLowerCase();
            DataType = ColDetail[1].toUpperCase();
            if (DataType.equals("NUMBER")) {
                temp = i + 1;
                out.write("statement.setInt(" + temp.toString() + ",RecordtoInsert.get" + ColName.replace("_", "").toLowerCase() + "());");
                out.newLine();
            } else if (DataType.equals("VARCHAR2")) {
                temp = i + 1;
                out.write("statement.setString(" + temp.toString() + ",RecordtoInsert.get" + ColName.replace("_", "").toLowerCase() + "());");
                out.newLine();
            } else if (DataType.equals("DATE")) {
                temp = i + 1;
                out.write("statement.setString(" + temp.toString() + ",DateFormatter.changeDateAndTimeFormat(RecordtoInsert.get" + ColName.replace("_", "").toLowerCase() + "()).toString());");
                out.newLine();
            }
        }
        return var;
    }

    public String GetUpdateStatment(List<String> Col) {
        String var = "", ColName = "", VarName = "", DataType = "";
        String[] ColDetail = new String[3];

        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            ColName = ColDetail[2];
            VarName = ColDetail[0].toLowerCase();
            DataType = ColDetail[1].toUpperCase();
            if (DataType.equals("NUMBER")) {
                if (i != Col.size() - 1) {
                    var = var + ColName + "= $" + VarName + ",";
                } else {
                    var = var + ColName + "= $" + VarName;
                }
            } else if (DataType.equals("VARCHAR2")) {
                if (i != Col.size() - 1) {
                    var = var + ColName + "= '$" + VarName + "',";
                } else {
                    var = var + ColName + "= '$" + VarName + "'";
                }
            } else if (DataType.equals("DATE")) {
                if (i != Col.size() - 1) {
                    var = var + ColName + "= '$" + VarName + "',";
                } else {
                    var = var + ColName + "= '$" + VarName + "'";
                }
            }
        }
        return var;
    }

    public String GetKeysSearch(String Table, BufferedWriter out) throws Throwable {

        List<String> Col = new ArrayList<String>();
        Col = findAllKeys(Table);
        String var = "", Q = "\"";
        String ColName = "";
        String VarName = "";
        String[] ColDetail = new String[3];
        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            ColName = ColDetail[0];
            VarName = ColName.replace("_", "").toLowerCase();
            out.write("selectStat = selectStat.replace(" + Q + "$" + VarName + Q + "," + Q + Q + "+ RecordToSelect.get" + VarName + "());");
            out.newLine();
        }
        return var;

    }

    public String GetKeys(String Table) throws Throwable {

        List<String> Col = new ArrayList<String>();
        Col = findAllKeys(Table);
        String var = "";
        String ColName = "";
        String VarName = "";
        String DataType = "";
        String[] ColDetail = new String[3];
        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            ColName = ColDetail[0];
            DataType = ColDetail[1].toUpperCase();
            VarName = ColName.replace("_", "").toLowerCase();
            if (DataType.equals("NUMBER")) {
                if (i != Col.size() - 1) {
                    var = var + ColName + "= $" + VarName + ",";
                } else {
                    var = var + ColName + "= $" + VarName;
                }
            } else if (DataType.equals("VARCHAR2")) {
                if (i != Col.size() - 1) {
                    var = var + ColName + "= '$" + VarName + "',";
                } else {
                    var = var + ColName + "= '$" + VarName + "'";
                }
            } else if (DataType.equals("DATE")) {
                if (i != Col.size() - 1) {
                    var = var + ColName + "= '$" + VarName + "',";
                } else {
                    var = var + ColName + "= '$" + VarName + "'";
                }
            }
        }
        return var;

    }

    public void WriteDAOUpdateStatment(BufferedWriter out, List<String> Col) throws Throwable {
        String VarName = "", DataType = "", Q = "\"";
        String[] ColDetail = new String[3];

        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            VarName = ColDetail[0].toLowerCase();
            DataType = ColDetail[1].toUpperCase();
            if (DataType.equals("NUMBER")) {
                out.write("UpdateStat = UpdateStat.replace(" + Q + "$" + VarName + "" + Q + ", " + Q + Q + " + RecordToUpdate.get" + VarName + "());");
                out.newLine();
            } else if (DataType.equals("VARCHAR2")) {
                out.write("UpdateStat = UpdateStat.replace(" + Q + "$" + VarName + "" + Q + ", " + Q + Q + " + RecordToUpdate.get" + VarName + "());");
                out.newLine();
            } else if (DataType.equals("DATE")) {
                out.write("UpdateStat = UpdateStat.replace(" + Q + "$" + VarName + "" + Q + ", " + Q + Q + " + DateFormatter.changeDateAndTimeFormat(RecordToUpdate.get" + VarName + "()));");
                out.newLine();
            }
        }
    }

    public void WriteDAODeleteStatment(BufferedWriter out, List<String> Col) throws Throwable {
        String VarName = "", DataType = "", Q = "\"";
        String[] ColDetail = new String[3];

        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            VarName = ColDetail[0].toLowerCase();
            DataType = ColDetail[1].toUpperCase();
            if (DataType.equals("NUMBER")) {
                out.write("deleteStat = deleteStat.replace(" + Q + "$" + VarName + "" + Q + ", " + Q + Q + " + RecordToDelete.get" + VarName + "());");
                out.newLine();
            } else if (DataType.equals("VARCHAR2")) {
                out.write("deleteStat = deleteStat.replace(" + Q + "$" + VarName + "" + Q + ", " + Q + Q + " + RecordToDelete.get" + VarName + "());");
                out.newLine();
            } else if (DataType.equals("DATE")) {
                out.write("deleteStat = deleteStat.replace(" + Q + "$" + VarName + "" + Q + ", " + Q + Q + " + DateFormatter.changeDateAndTimeFormat(RecordToDelete.get" + VarName + "()));");
                out.newLine();
            }
        }
    }

    public void WriteDAOSelectRetreive(BufferedWriter out, List<String> Col) throws Throwable {
        String VarName = "", DataType = "", Q = "\"", ColName = "";
        String[] ColDetail = new String[3];

        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            VarName = ColDetail[0].toLowerCase();
            DataType = ColDetail[1].toUpperCase();
            ColName = ColDetail[2].toUpperCase();
            if (DataType.equals("NUMBER")) {
                out.write("SelectedRecord.set" + VarName + "(rs.getInt(" + Q + ColName + Q + "));");
                out.newLine();
            } else if (DataType.equals("VARCHAR2")) {
                out.write("SelectedRecord.set" + VarName + "(rs.getString(" + Q + ColName + Q + "));");
                out.newLine();
            } else if (DataType.equals("DATE")) {
                out.write("SelectedRecord.set" + VarName + "(rs.getTimestamp(" + Q + ColName + Q + "));");
                out.newLine();
            }
        }
    }

    public void WriteDAOInsertStatment(BufferedWriter out, List<String> Col) throws Throwable {
        String VarName = "", DataType = "", Q = "\"";
        String[] ColDetail = new String[3];

        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            VarName = ColDetail[0].toLowerCase();
            DataType = ColDetail[1].toUpperCase();
            if (DataType.equals("NUMBER")) {
                out.write("insertStat = insertStat.replace(" + Q + "$" + VarName + "" + Q + ", " + Q + Q + " + RecordToInsert.get" + VarName + "());");
                out.newLine();
            } else if (DataType.equals("VARCHAR2")) {
                out.write("insertStat = insertStat.replace(" + Q + "$" + VarName + "" + Q + ", " + Q + Q + " + RecordToInsert.get" + VarName + "());");
                out.newLine();
            } else if (DataType.equals("DATE")) {
                out.write("insertStat = insertStat.replace(" + Q + "$" + VarName + "" + Q + ", " + Q + Q + " + DateFormatter.changeDateAndTimeFormat(RecordToInsert.get" + VarName + "()));");
                out.newLine();
            }
        }
    }

    public void WriteBOInter(BufferedWriter out) throws Throwable {
        out.newLine();
        out.write("Object insertrecord(Object... obj) throws Throwable;");
        out.newLine();
        out.write("Object save(Object... obj) throws SQLException ,Throwable;");
        out.newLine();
        out.write("Object updaterecord(Object... obj) throws Throwable;");
        out.newLine();
        out.write("Object deleteallrecord(Object... obj) throws Throwable; ");
        out.newLine();
        out.write("Object deleterecord(Object... obj) throws Throwable;");
        out.newLine();
        out.write("Object GetListOfRecords(Object... obj) throws Throwable;");
        out.newLine();
        out.write("Object GetRecord(Object... obj) throws Throwable;");
        out.newLine();
        out.write("Object GetAllRecords(Object... obj) throws Throwable;");
        out.newLine();

    }

    public void WriteBOClass(String Table, String FileName, BufferedWriter out, List<String> Col) throws Throwable {
        out.write(" public Object insertrecord(Object... obj) throws Throwable {");
        out.newLine();
        String Q = "\"";
        out.write(FileName + "DTOInter RecordToInsert = (" + FileName + "DTOInter) obj[0];");
        out.newLine();
        out.write(FileName + "DAOInter engin = DAOFactory.create" + FileName + "DAO();");
        out.newLine();
        out.write("engin.insertrecord(RecordToInsert);");
        out.newLine();
        out.write("return " + Q + "Insert Done" + Q + ";");
        out.newLine();
        out.write("}");
        out.newLine();
        out.write("public Object save(Object... obj) throws SQLException, Throwable {");
        out.newLine();
        out.write("List<" + FileName + "DTOInter> entities = (List<" + FileName + "DTOInter>) obj[0];");
        out.newLine();
        out.write(FileName + "DAOInter engin = DAOFactory.create" + FileName + "DAO();");
        out.newLine();
        out.write("engin.save(entities);");
        out.newLine();
        out.write("return " + Q + "Insert Done" + Q + ";");
        out.newLine();
        out.write("}");
        out.newLine();

        out.write(" public Object updaterecord(Object... obj) throws Throwable {");
        out.newLine();
        out.write(FileName + "DTOInter RecordToUpdate = (" + FileName + "DTOInter) obj[0];");
        out.newLine();
        out.write(FileName + "DAOInter engin = DAOFactory.create" + FileName + "DAO();");
        out.newLine();
        out.write("engin.updaterecord(RecordToUpdate);");
        out.newLine();
        out.write("return " + Q + "Update Done" + Q + ";");
        out.newLine();
        out.write("}");
        out.newLine();
        out.write(" public Object deleteallrecord(Object... obj) throws Throwable {");
        out.newLine();
        out.write(FileName + "DAOInter engin = DAOFactory.create" + FileName + "DAO();");
        out.newLine();
        out.write("engin.deleteallrecord();");
        out.newLine();
        out.write("return " + Q + "Delete Complete" + Q + ";");
        out.newLine();
        out.write("}");
        out.newLine();
        out.write(" public Object deleterecord(Object... obj) throws Throwable {");
        out.newLine();
        out.write(FileName + "DTOInter RecordToDelete = (" + FileName + "DTOInter) obj[0];");
        out.newLine();
        out.write(FileName + "DAOInter engin = DAOFactory.create" + FileName + "DAO();");
        out.newLine();
        out.write("engin.deleterecord(RecordToDelete);");
        out.newLine();
        out.write("return " + Q + "record has been deleted" + Q + ";");
        out.newLine();
        out.write("}");
        out.newLine();
        out.write(" public Object GetListOfRecords(Object... obj) throws Throwable {");
        out.newLine();
        out.write("List<" + FileName + "DTOInter> SearchRecords = (List<" + FileName + "DTOInter>) obj[0];");
        out.newLine();
        out.write(FileName + "DAOInter engin = DAOFactory.create" + FileName + "DAO();");
        out.newLine();
        out.write("List<" + FileName + "DTOInter> AllRecords = (List<" + FileName + "DTOInter>) engin.findRecordsList(SearchRecords);");
        out.newLine();
        out.write("return AllRecords;");
        out.newLine();
        out.write("}");
        out.newLine();
        out.write(" public Object GetAllRecords(Object... obj) throws Throwable {");
        out.newLine();
        out.write(FileName + "DAOInter engin = DAOFactory.create" + FileName + "DAO();");
        out.newLine();
        out.write("List<" + FileName + "DTOInter> AllRecords = (List<" + FileName + "DTOInter>) engin.findRecordsAll();");
        out.newLine();
        out.write("return AllRecords;");
        out.newLine();
        out.write("}");
        out.newLine();
        out.write(" public Object GetRecord(Object... obj) throws Throwable {");
        out.newLine();
        out.write(FileName + "DTOInter SearchParameter = (" + FileName + "DTOInter) obj[0];");
        out.newLine();
        out.write(FileName + "DAOInter engin = DAOFactory.create" + FileName + "DAO();");
        out.newLine();
        out.write("" + FileName + "DTOInter Record = (" + FileName + "DTOInter) engin.findRecord(SearchParameter);");
        out.newLine();
        out.write("return Record;");
        out.newLine();
        out.write("}");
        out.newLine();

    }

    public void ValidateNull(String Table, String FileName, BufferedWriter out) throws Throwable {
        out.write("public Boolean ValidateNull(Object... obj){");
        out.newLine();
        List<String> Col = new ArrayList<String>();
        Col = findAllNulls(Table);
        out.write(FileName + "DTOInter RecordToInsert = (" + FileName + "DTOInter) obj[0];");
        out.newLine();
        String VarName = "", DataType = "", Q = "\"";

        for (int i = 0; i < Col.size(); i++) {
            String[] ColDetail = new String[3];
            ColDetail = Col.get(i).split("-");
            VarName = ColDetail[0].toLowerCase().replace("_", "");
            DataType = ColDetail[1].toUpperCase();
            if (DataType.equals("NUMBER")) {
                out.write("if (RecordToInsert.get" + VarName + "() == 0)");
                out.newLine();
                out.write("{return  Boolean.FALSE;}");
                out.newLine();
            } else if (DataType.equals("VARCHAR2")) {
                out.write("if (RecordToInsert.get" + VarName + "() == null && RecordToInsert.get" + VarName + "().equals(" + Q + Q + "))");
                out.newLine();
                out.write("{return  Boolean.FALSE;}");
                out.newLine();
            } else if (DataType.equals("DATE")) {
                out.write("if (RecordToInsert.get" + VarName + "() == null)");
                out.newLine();
                out.write("{return  Boolean.FALSE;}");
                out.newLine();
            }
        }
        out.write("return Boolean.TRUE;");
        out.newLine();
        out.write("}");
        out.newLine();
    }

    public void WriteDAOInsert(String Table, String FileName, BufferedWriter out, List<String> Col) throws Throwable {
        out.write(" public Object insertrecord(Object... obj) throws Throwable {");
        out.newLine();
        out.write("super.preUpdate();");
        out.newLine();
        String Q = "\"";
        out.write(FileName + "DTOInter RecordToInsert = (" + FileName + "DTOInter) obj[0];");
        out.newLine();
        out.write("RecordToInsert.setid(super.generateSequence(super.getTableName()));");
        out.newLine();
        out.write("String insertStat = " + Q + "insert into $table" + Q);
        out.newLine();
        out.write("+" + Q + " (" + GetColoumnName(Col) + ") " + Q);
        out.newLine();
        out.write("+" + Q + " values " + Q);
        out.newLine();
        out.write("+" + Q + " (" + GetVariable(Col) + ")" + Q + ";");
        out.newLine();
        out.write("insertStat = insertStat.replace(" + Q + "$table" + Q + ", " + Q + Q + " + super.getTableName());");
        out.newLine();
        WriteDAOInsertStatment(out, Col);
        out.write("super.executeUpdate(insertStat);");
        out.newLine();
        out.write("super.postUpdate(" + Q + "Add New Record To " + Table + Q + ", false);");
        out.newLine();
        out.write("return null;");
        out.newLine();
        out.write("}");
        out.newLine();
    }

    public void WriteDAODelete(String Table, String FileName, BufferedWriter out, List<String> Col) throws Throwable {
        out.write("  public Object deleterecord(Object... obj) throws Throwable {");
        out.newLine();
        out.write("super.preUpdate();");
        out.newLine();
        String Q = "\"";
        out.write(FileName + "DTOInter RecordToDelete = (" + FileName + "DTOInter) obj[0];");
        out.newLine();
        out.write("String deleteStat = " + Q + "delete from $table " + Q);
        out.newLine();
        out.write("+ " + Q + "  where  " + GetKeys(Table.toUpperCase()) + Q + ";");
        out.newLine();
        out.write("deleteStat = deleteStat.replace(" + Q + "$table" + Q + ", " + Q + Q + " + super.getTableName());");
        out.newLine();
        WriteDAODeleteStatment(out, Col);
        out.write("super.executeUpdate(deleteStat);");
        out.newLine();
        out.write("super.postUpdate(" + Q + "Delete Record In Table " + Table + Q + ", false);");
        out.newLine();
        out.write("return null;");
        out.newLine();
    }

    public void WriteDAODeleteAll(String Table, String FileName, BufferedWriter out, List<String> Col) throws Throwable {
        out.write("  public Object deleteallrecord(Object... obj) throws Throwable {");
        out.newLine();
        out.write("super.preUpdate();");
        out.newLine();
        String Q = "\"";
        out.write("String deleteStat = " + Q + "delete from $table " + Q + ";");
        out.newLine();
        out.write("deleteStat = deleteStat.replace(" + Q + "$table" + Q + ", " + Q + Q + " + super.getTableName());");
        out.newLine();
        out.write("super.executeUpdate(deleteStat);");
        out.newLine();
        out.write("super.postUpdate(" + Q + "Delete All Record In Table " + Table + Q + ", false);");
        out.newLine();
        out.write("return null;");
        out.newLine();
    }

    public void WriteDAOUpdate(String Table, String FileName, BufferedWriter out, List<String> Col) throws Throwable {
        out.write(" public Object updaterecord(Object... obj) throws Throwable {");
        out.newLine();
        out.write("super.preUpdate();");
        out.newLine();
        String Q = "\"";
        out.write(FileName + "DTOInter RecordToUpdate = (" + FileName + "DTOInter) obj[0];");
        out.newLine();
        out.write("String UpdateStat = " + Q + "update $table set " + Q);
        out.newLine();
        out.write("+ " + Q + " (" + GetUpdateStatment(Col) + ") " + Q);
        out.newLine();
        out.write("+ " + Q + "  where  " + GetKeys(Table.toUpperCase()) + Q + ";");
        out.newLine();
        out.write("UpdateStat = UpdateStat.replace(" + Q + "$table" + Q + ", " + Q + Q + " + super.getTableName());");
        out.newLine();
        WriteDAOUpdateStatment(out, Col);
        out.write("super.executeUpdate(UpdateStat);");
        out.newLine();
        out.write("super.postUpdate(" + Q + "Update Record In Table " + Table + Q + ", false);");
        out.newLine();
        out.write("return null;");
        out.newLine();
    }

    public void WriteDAOSelect(String Table, String FileName, BufferedWriter out, List<String> Col) throws Throwable {
        out.write(" public Object findRecord(Object... obj) throws Throwable {");
        out.newLine();
        out.write("super.preSelect();");
        out.newLine();
        String Q = "\"";
        out.write(FileName + "DTOInter RecordToSelect = (" + FileName + "DTOInter) obj[0];");
        out.newLine();
        out.write("String selectStat = " + Q + "Select " + GetColoumnName(Col) + " From $table" + Q);
        out.newLine();
        out.write("+ " + Q + "  where  " + GetKeys(Table.toUpperCase()) + Q + ";");
        out.newLine();
        out.write("selectStat = selectStat.replace(" + Q + "$table" + Q + ", " + Q + Q + " + super.getTableName());");
        out.newLine();
        GetKeysSearch(Table, out);
        out.write("ResultSet rs = executeQuery(selectStat);");
        out.newLine();
        out.write(FileName + "DTOInter SelectedRecord = DTOFactory.create" + FileName + "DTO();");
        out.newLine();
        out.write("while (rs.next()) {");
        out.newLine();
        WriteDAOSelectRetreive(out, Col);
        out.write("}");
        out.newLine();
        out.write("super.postSelect(rs);");
        out.newLine();
        out.write("return SelectedRecord;");
        out.newLine();
    }

    public void WriteDAOSelectList(String Table, String FileName, BufferedWriter out, List<String> Col) throws Throwable {
        out.write(" public Object findRecordsList(Object... obj) throws Throwable {");
        out.newLine();
        out.write("super.preSelect();");
        out.newLine();
        String Q = "\"";
        out.write(FileName + "DTOInter RecordToSelect = (" + FileName + "DTOInter) obj[0];");
        out.newLine();
        out.write("String selectStat = " + Q + "Select " + GetColoumnName(Col) + " From $table" + Q);
        out.newLine();
        out.write("+ " + Q + "  where  " + GetKeys(Table.toUpperCase()) + Q + ";");
        out.newLine();
        out.write("selectStat = selectStat.replace(" + Q + "$table" + Q + ", " + Q + Q + " + super.getTableName());");
        out.newLine();
        GetKeysSearch(Table, out);
        out.write("ResultSet rs = executeQuery(selectStat);");
        out.newLine();
        //List<AtmGroupDTOInter> uDTOL = new ArrayList<AtmGroupDTOInter>();
        out.write("List<" + FileName + "DTOInter> Records = new ArrayList<" + FileName + "DTOInter>();");
        out.newLine();
        out.write("while (rs.next()) {");
        out.newLine();
        out.write(FileName + "DTOInter SelectedRecord = DTOFactory.create" + FileName + "DTO();");
        out.newLine();
        WriteDAOSelectRetreive(out, Col);
        out.write("Records.add(SelectedRecord);");
        out.newLine();
        out.write("}");
        out.newLine();
        out.write("super.postSelect(rs);");
        out.newLine();
        out.write("return Records;");
        out.newLine();
    }

    public void WriteDAOSelectAll(String Table, String FileName, BufferedWriter out, List<String> Col) throws Throwable {
        out.write(" public Object findRecordsAll(Object... obj) throws Throwable {");
        out.newLine();
        out.write("super.preSelect();");
        out.newLine();
        String Q = "\"";
        out.write("String selectStat = " + Q + "Select " + GetColoumnName(Col) + " From $table" + Q + ";");
        out.newLine();
        out.write("selectStat = selectStat.replace(" + Q + "$table" + Q + ", " + Q + Q + " + super.getTableName());");
        out.newLine();
        out.write("ResultSet rs = executeQuery(selectStat);");
        out.newLine();
        //List<AtmGroupDTOInter> uDTOL = new ArrayList<AtmGroupDTOInter>();
        out.write("List<" + FileName + "DTOInter> Records = new ArrayList<" + FileName + "DTOInter>();");
        out.newLine();
        out.write("while (rs.next()) {");
        out.newLine();
        out.write(FileName + "DTOInter SelectedRecord = DTOFactory.create" + FileName + "DTO();");
        out.newLine();
        WriteDAOSelectRetreive(out, Col);
        out.write("Records.add(SelectedRecord);");
        out.newLine();
        out.write("}");
        out.newLine();
        out.write("super.postSelect(rs);");
        out.newLine();
        out.write("return Records;");
        out.newLine();
    }

    public void WriteDAOConstractor(String Table, String FileName, BufferedWriter out) throws Throwable {
        out.write(" protected " + FileName + "DAO() {");
        out.newLine();
        out.write(" super();");
        out.newLine();
        String Q = "\"";
        out.write(" super.setTableName(" + Q + Table + Q + ");");
        out.newLine();
        out.write("}");
        out.newLine();
    }

    public void WriteBOConstractor(String Table, String FileName, BufferedWriter out) throws Throwable {
        out.write(" protected " + FileName + "BO() {");
        out.newLine();
        out.write(" super();");
        out.newLine();
        out.write("}");
        out.newLine();
    }

    public String WriteDef(String Detail) throws Throwable {
        String[] ColDetail = new String[3];
        ColDetail = Detail.split("-");
        return "private " + GetDataType(ColDetail[1]) + " " + ColDetail[0].toLowerCase() + ";";
    }

    public void WriteGetterSetter(String Detail, BufferedWriter out) throws Throwable {
        String[] ColDetail = new String[3];
        ColDetail = Detail.split("-");
        String VarName = ColDetail[0].toLowerCase();
        String DataType = GetDataType(ColDetail[1]);
        out.write("public " + DataType + " get" + VarName + "(){");
        out.newLine();
        out.write("return " + VarName + ";");
        out.newLine();
        out.write("}");
        out.newLine();
        out.write("public void set" + VarName + "(" + DataType + " " + VarName + ") {");
        out.newLine();
        out.write("this." + VarName + " = " + VarName + ";");
        out.newLine();
        out.write("}");
        out.newLine();
    }

    public void WriteDAOInter(BufferedWriter out) throws Throwable {
        out.newLine();
        out.write("Object insertrecord(Object... obj) throws Throwable ;");
        out.newLine();
        out.write("Object updaterecord(Object... obj) throws Throwable;");
        out.newLine();
        out.write("Object deleterecord(Object... obj) throws Throwable;");
        out.newLine();
        out.write("Object deleteallrecord(Object... obj) throws Throwable;");
        out.newLine();
        out.write("Object findRecord(Object... obj) throws Throwable ;");
        out.newLine();
        out.write("Object findRecordsList(Object... obj) throws Throwable;");
        out.newLine();
        out.write("Object findRecordsAll(Object... obj) throws Throwable ;");
        out.newLine();
        out.write("void save(Object... obj) throws SQLException, Throwable;");
        out.newLine();
    }

    public void WriteInterGetterSetter(String Detail, BufferedWriter out) throws Throwable {
        String[] ColDetail = new String[3];
        ColDetail = Detail.split("-");
        String VarName = ColDetail[0].toLowerCase();
        String DataType = GetDataType(ColDetail[1]);
        out.write(DataType + " get" + VarName + "();");
        out.newLine();
        out.write("void set" + VarName + "(" + DataType + " " + VarName + ");");
        out.newLine();
    }

    public void WriteDTOFile(String Filename, List<String> Col) throws Throwable {
        try {
            FileWriter fstream = new FileWriter("E:\\Test\\DTO\\" + Filename + "DTO.java");
            BufferedWriter out = new BufferedWriter(fstream);
            WriteDTOHeader(Filename, out);
            for (int i = 0; i < Col.size(); i++) {
                out.write(WriteDef(Col.get(i)));
                out.newLine();
                WriteGetterSetter(Col.get(i), out);
            }
            out.write("}");
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteDAOFile(String Filename, List<String> Col, String Tables) throws Throwable {
        try {
            FileWriter fstream = new FileWriter("E:\\Test\\DAO\\" + Filename + "DAO.java");
            BufferedWriter out = new BufferedWriter(fstream);
            WriteDAOHeader(Filename, out);
            WriteDAOConstractor(Tables, Filename, out);
            WriteDAOInsert(Tables, Filename, out, Col);
            ValidateNull(Tables, Filename, out);
            out.write("}");
            out.newLine();
            WriteDAOUpdate(Tables, Filename, out, Col);
            out.write("}");
            out.newLine();
            WriteDAODelete(Tables, Filename, out, Col);
            out.write("}");
            out.newLine();
            WriteDAODeleteAll(Tables, Filename, out, Col);
            out.write("}");
            out.newLine();
            WriteDAOSelect(Tables, Filename, out, Col);
            out.write("}");
            out.newLine();
            WriteDAOSelectList(Tables, Filename, out, Col);
            out.write("}");
            out.newLine();
            WriteDAOSelectAll(Tables, Filename, out, Col);
            out.write("}");
            out.newLine();
            WriteDAOInsertAll(Filename, out, Col, Tables);
            out.write("}");
            out.newLine();
            out.write("}");
            out.newLine();
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteBOFile(String Filename, List<String> Col, String Tables) throws Throwable {
        try {
            FileWriter fstream = new FileWriter("E:\\Test\\BO\\" + Filename + "BO.java");
            BufferedWriter out = new BufferedWriter(fstream);
            WriteBOHeader(Filename, out);
            WriteBOConstractor(Tables, Filename, out);
            WriteBOClass(Tables, Filename, out, Col);
            out.write("}");
            out.newLine();
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteDAOInterFile(String Filename, List<String> Col) throws Throwable {
        try {
            FileWriter fstream = new FileWriter("E:\\Test\\DAO\\" + Filename + "DAOInter.java");
            BufferedWriter out = new BufferedWriter(fstream);
            WriteDAOInterHeader(Filename, out);
            WriteDAOInter(out);
            out.write("}");
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteBOInterHeader(String Filename, BufferedWriter out) throws Throwable {
        try {
            out.write("package com.ev.Bingo.bus.bo;");
            out.newLine();
            out.write("import java.sql.SQLException;");
            out.newLine();
            out.write("public interface " + Filename + "BOInter {");
            out.newLine();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteBOInterFile(String Filename, List<String> Col) throws Throwable {
        try {
            FileWriter fstream = new FileWriter("E:\\Test\\BO\\" + Filename + "BOInter.java");
            BufferedWriter out = new BufferedWriter(fstream);
            WriteBOInterHeader(Filename, out);
            WriteBOInter(out);
            out.write("}");
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteDTOInterFile(String Filename, List<String> Col) throws Throwable {
        try {
            FileWriter fstream = new FileWriter("E:\\Test\\DTO\\" + Filename + "DTOInter.java");
            BufferedWriter out = new BufferedWriter(fstream);
            WriteInterHeader(Filename, out);
            for (int i = 0; i < Col.size(); i++) {
                WriteInterGetterSetter(Col.get(i), out);
            }
            out.write("}");
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteDTOFactoryFile(List<String> Tables) throws Throwable {
        try {
            FileWriter fstream = new FileWriter("E:\\Test\\DTO\\DTOFactory.java");
            BufferedWriter out = new BufferedWriter(fstream);
            WriteFactoryHeader(Tables, out);
            out.write("}");
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteDAOFactoryFile(List<String> Tables) throws Throwable {
        try {
            FileWriter fstream = new FileWriter("E:\\Test\\DAO\\DAOFactory.java");
            BufferedWriter out = new BufferedWriter(fstream);
            WriteDAOFactoryHeader(Tables, out);
            out.write("}");
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void WriteBOFactoryFile(List<String> Tables) throws Throwable {
        try {
            FileWriter fstream = new FileWriter("E:\\Test\\BO\\BOFactory.java");
            BufferedWriter out = new BufferedWriter(fstream);
            WriteBOFactoryHeader(Tables, out);
            out.write("}");
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void runDTO() throws Throwable {
        String FileName;
        List<String> Tables = new ArrayList<String>();
        Tables = findAllTables();
        for (int i = 0; i < Tables.size(); i++) {
            List<String> Col = new ArrayList<String>();
            Col = findAllColoumns(Tables.get(i).toString());
            FileName = Tables.get(i).toString().replace("_", "").toLowerCase();
            WriteDTOFile(FileName, Col);
            WriteDTOInterFile(FileName, Col);
        }
        WriteDTOFactoryFile(Tables);

    }

    public void runDAO() throws Throwable {
        String FileName, TableName;
        List<String> Tables = new ArrayList<String>();
        Tables = findAllTables();
        for (int i = 0; i < Tables.size(); i++) {
            List<String> Col = new ArrayList<String>();
            Col = findAllColoumns(Tables.get(i).toString());
            FileName = Tables.get(i).toString().replace("_", "").toLowerCase();
            TableName = Tables.get(i).toString();
            WriteDAOFile(FileName, Col, TableName);
            WriteDAOInterFile(FileName, Col);
        }
        WriteDAOFactoryFile(Tables);
    }

    public void runBO() throws Throwable {
        String FileName, TableName;
        List<String> Tables = new ArrayList<String>();
        Tables = findAllTables();
        for (int i = 0; i < Tables.size(); i++) {
            List<String> Col = new ArrayList<String>();
            Col = findAllColoumns(Tables.get(i).toString());
            FileName = Tables.get(i).toString().replace("_", "").toLowerCase();
            TableName = Tables.get(i).toString();
            WriteBOFile(FileName, Col, TableName);
            WriteBOInterFile(FileName, Col);
        }
        WriteBOFactoryFile(Tables);
    }
}
