package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.licenseDTOInter;
import java.util.List;

public class licenseDAO extends BaseDAO implements licenseDAOInter {

    protected licenseDAO() {
        super();
        super.setTableName("LICENSE");
    }

    public String GetRecord(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "select 2 Result from dual";
        ResultSet rs = executeQuery(selectStat);
        String Linc = "";
        while (rs.next()) {
            Linc = rs.getString("Result");
        }
        super.postSelect(rs);
        return Linc;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select * From $table where (select check_lic from dual) = 1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<licenseDTOInter> Records = new ArrayList<licenseDTOInter>();
        while (rs.next()) {
            licenseDTOInter SelectedRecord = DTOFactory.createlicenseDTO();
            SelectedRecord.setcreateddate(rs.getTimestamp("CREATED_DATE"));
            SelectedRecord.setenddate(rs.getTimestamp("END_DATE"));
            SelectedRecord.setlicensekey(rs.getString("LICENSE_KEY"));
            SelectedRecord.setnoofusers(rs.getInt("NO_OF_USERS"));
            SelectedRecord.setlicenseto(rs.getString("LICENSE_TO"));
            SelectedRecord.setcountry(rs.getString("COUNTRY"));
            SelectedRecord.setversion(rs.getString("VERSION"));
            SelectedRecord.setnooftrans(rs.getString("no_of_tran"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

}
