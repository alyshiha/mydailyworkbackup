package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

public interface licenseDAOInter {

    Object findRecordsAll(Object... obj) throws Throwable;

    String GetRecord(Object... obj) throws Throwable;

}
