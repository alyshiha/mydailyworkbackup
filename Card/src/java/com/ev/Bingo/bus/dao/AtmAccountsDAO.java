/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.AccountNameDTOInter;
import com.ev.Bingo.bus.dto.AtmAccountsDTOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AlyShiha
 */
public class AtmAccountsDAO extends BaseDAO implements AtmAccountsDAOInter {

    @Override
    public Boolean findDuplicate(String atmid, int accname, String name, int count) throws Throwable {
        super.preSelect();
        String selectStat = "select Count(t.id) resultcount from ATM_ACCOUNTS t where t.atm_id = $atmid and t.account_name_id = $atmaccountname and Lower(t.atm_account_id) = Lower('$name') ";
        selectStat = selectStat.replace("$atmid", "" + atmid);
        selectStat = selectStat.replace("$atmaccountname", "" + accname);
        selectStat = selectStat.replace("$name", "" + name);
        ResultSet rs = executeQuery(selectStat);
        Boolean result = Boolean.FALSE;
        while (rs.next()) {
            if (rs.getInt("resultcount") > count) {
                result = Boolean.TRUE;
            }
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select records From ATM Accounts with atm id = " + atmid + " account name: " + accname + " Name: " + name);
        return result;
    }

    @Override
    public List<AtmAccountsDTOInter> findrecord(int atmid, String accid, String accname, int group, String branch) throws Throwable {
        super.preSelect();
        String selectStat = "select c.id,c.atm_id, c.atm_describtion, c.atm_account_id,\n"
                + "(select n.symbol  from atm_account_name n where n.id = c.ACCOUNT_NAME_ID) ACCOUNTNAME , \n"
                + "concat('EG001',substr(c.atm_account_id,-4)) branch,\n"
                + "c.ACCOUNT_NAME_ID  "
                + "from atm_accounts c where (atm_id = '$atmid' OR '$atmid' = 0) and atm_account_id like '%$atmaccountid%' and (account_name_id = $atmaccountname OR '$atmaccountname' = 0)";

        selectStat = selectStat.replace("$atmid", "" + atmid);
        selectStat = selectStat.replace("$atmaccountid", accid);
        selectStat = selectStat.replace("$atmaccountname", accname);
        selectStat = selectStat.replace("$branch", branch);
        selectStat = selectStat.replace("$atmgroup", "" + group);
        ResultSet rs = executeQuery(selectStat);
        List<AtmAccountsDTOInter> records = new ArrayList<AtmAccountsDTOInter>();
        while (rs.next()) {
            AtmAccountsDTOInter record = DTOFactory.createAtmAccountsDTO();
            record.setId(rs.getInt("id"));
            record.setAccountid(rs.getString("atm_account_id"));
            record.setAccountnameid(rs.getInt("ACCOUNT_NAME_ID"));
            record.setAtmid(rs.getString("atm_id"));
            record.setDescription(rs.getString("atm_describtion"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select records From ATM Accounts with atm id = " + atmid + " account name: " + accname + " branch: " + branch + " group: " + group);
        return records;
    }

    @Override
    public List<String> findAllBranchs() throws Throwable {
        super.preSelect();
        String selectStat = "select Distinct concat('EG001',substr(atm_account_id,-4)) BranchID from atm_accounts order by 1";
        ResultSet rs = executeQuery(selectStat);
        List<String> records = new ArrayList<String>();
        while (rs.next()) {
            records.add(rs.getString("BranchID"));
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select All Branches to search in ATM Accounts");
        return records;
    }

    @Override
    public List<AtmAccountsDTOInter> findAll(String user) throws Throwable {
        super.preSelect();
        String selectStat = "select c.id, c.atm_id, c.atm_describtion, c.atm_account_id,\n"
                + "(select n.symbol  from atm_account_name n where n.id = c.ACCOUNT_NAME_ID) ACCOUNTNAME , \n"
                + "concat('EG001',substr(c.atm_account_id,-4)) branch,\n"
                + "c.ACCOUNT_NAME_ID  from atm_accounts c ";
        ResultSet rs = executeQuery(selectStat);
        List<AtmAccountsDTOInter> records = new ArrayList<AtmAccountsDTOInter>();
        while (rs.next()) {
            AtmAccountsDTOInter record = DTOFactory.createAtmAccountsDTO();

            record.setId(rs.getInt("id"));
            record.setBranch(rs.getString("branch"));
            record.setAccountname(rs.getString("ACCOUNTNAME"));
            record.setAccountid(rs.getString("atm_account_id"));
            record.setAccountnameid(rs.getInt("ACCOUNT_NAME_ID"));
            record.setAtmid(rs.getString("atm_id"));
            record.setDescription(rs.getString("atm_describtion"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select All records From ATM Accounts");
        return records;
    }

    public String update(Object... obj) throws Exception {
        super.preUpdate();
        AtmAccountsDTOInter record = (AtmAccountsDTOInter) obj[0];
        String insertStat = "update atm_accounts\n"
                + "   set \n"
                + "       atm_id = 'v_atm_id',\n"
                + "       atm_describtion = 'v_atm_describtion',\n"
                + "       atm_account_id = 'v_atm_account_id',\n"
                + "       account_name_id = 'v_account_name_id'\n"
                + " where id = v_id";
        insertStat = insertStat.replace("v_id", "" + record.getId());
        insertStat = insertStat.replace("v_atm_id", "" + record.getAtmid());
        insertStat = insertStat.replace("v_atm_describtion", "" + record.getDescription());
        insertStat = insertStat.replace("v_atm_account_id", "" + record.getAccountid());
        insertStat = insertStat.replace("v_account_name_id", "" + record.getAccountnameid());

        try {
            super.executeUpdate(insertStat);
        } catch (SQLException ex) {
            return "Duplicate Record";
        }

        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        try {
            logEngin.insert("update record with id " + record.getId() + " in ATM Accounts");
        } catch (Throwable ex) {
            Logger.getLogger(AtmAccountsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Record Has Been Updated Successfully";
    }

    public String insert(Object... obj) {
        try {
            super.preUpdate();
            AtmAccountsDTOInter record = (AtmAccountsDTOInter) obj[0];
            record.setId(super.generateSequence("atm_accounts"));
            String insertStat = "insert into atm_accounts\n"
                    + "  (id, atm_id, atm_describtion, atm_account_id, account_name_id)\n"
                    + "values\n"
                    + "  (v_id, 'v_atm_id', 'v_atm_describtion', 'v_atm_account_id', 'v_account_name_id')";
            insertStat = insertStat.replace("v_id", "" + record.getId());
            insertStat = insertStat.replace("v_atm_id", "" + record.getAtmid());
            insertStat = insertStat.replace("v_atm_describtion", "" + record.getDescription());
            insertStat = insertStat.replace("v_atm_account_id", "" + record.getAccountid());
            insertStat = insertStat.replace("v_account_name_id", "" + record.getAccountnameid());
            super.executeUpdate(insertStat);
            CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
            logEngin.insert("insert record in ATM_ACCOUNT_NAME with id: " + record.getId());
            return "Record Has Been inserted Successfully";
        } catch (SQLException ex) {
            return "Duplicate Record";
        } catch (Throwable ex) {
            return "Duplicate Record";
        }
    }

    public String delete(Object... obj) throws Throwable {
        super.preUpdate();
        AtmAccountsDTOInter record = (AtmAccountsDTOInter) obj[0];
        String deleteStat = "delete atm_accounts\n"
                + " where id = $id";
        deleteStat = deleteStat.replace("$id", "" + record.getId());
        super.executeUpdate(deleteStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("delete record with id " + record.getId() + " in ATM Accounts");
        return "Record Has Been Deleted Successfully";
    }
}
