package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.bus.dto.UserActivityDTOInter;
import java.util.ArrayList;
import java.util.List;

public class UserActivtyDAO extends BaseDAO implements UserActivtyDAOInter {

    protected UserActivtyDAO() {
        super();
    }

    public Object findRecordsList(Integer user) throws Throwable {
        super.preSelect();
        String selectStat
                = "select /*+INDEX (d user_activity_index)*/ count(*) countdis  , s.logon_name username , 'D' type from disputes d, users s where  d.transaction_date > sysdate-2 and d.settled_flag = 2 and network_id in (select un.networkid from user_network un where un.userid = s.user_id)   group by s.logon_name union select /*+INDEX (d user_activity_index)*/ count( *) countdis  , s.logon_name username,'S' type from disputes d, users s where  d.transaction_date > sysdate - 2 and d.settled_flag = 1 and d.settled_user = s.user_id  and network_id in (select un.networkid from user_network un where un.userid = s.user_id)  group by s.logon_name order by username";
        selectStat = selectStat.replace("$user", "" + user);
        ResultSet rs = executeQuery(selectStat);
        List<UserActivityDTOInter> Records = new ArrayList<UserActivityDTOInter>();
        while (rs.next()) {
            UserActivityDTOInter SelectedRecord = DTOFactory.createUserActivityDTO();
            SelectedRecord.setType(rs.getString("type"));
            SelectedRecord.setUsername(rs.getString("username"));
            SelectedRecord.setTranscount(rs.getInt("countdis"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }
}
