package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.validationrulesDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class validationrulesDAO extends BaseDAO implements validationrulesDAOInter {

    protected validationrulesDAO() {
        super();
        super.setTableName("VALIDATION_RULES");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        validationrulesDTOInter RecordToInsert = (validationrulesDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (ID,VALDATION_TYPE,COLUMN_ID,NAME,FORMULA,FORMULA_CODE,ACTIVE) "
                + " values "
                + " ($id,$valdationtype,$columnid,'$name','$formula','$formulacode',$active)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$valdationtype", "" + RecordToInsert.getvaldationtype());
        insertStat = insertStat.replace("$columnid", "" + RecordToInsert.getcolumnid());
        insertStat = insertStat.replace("$name", "" + RecordToInsert.getname());
        insertStat = insertStat.replace("$formulacode", "" + RecordToInsert.getformulacode());
        insertStat = insertStat.replace("$formula", "" + RecordToInsert.getformula());
        insertStat = insertStat.replace("$active", "" + RecordToInsert.getactive());
        String msg = "";
        msg = super.executeUpdate(insertStat);
        msg = msg + " Row Has Been Inserted";
        super.postUpdate("Add New Record To VALIDATION_RULES By Name " + RecordToInsert.getname(), false);
        return msg;
    }

    public Boolean checkFormula(String formula, Integer Type) throws Throwable {
        super.preCollable();
        CallableStatement stat = super.executeCallableStatment("{call ? := check_formula_java(?,?)}");
        Boolean done = false;
        stat.registerOutParameter(1, Types.NUMERIC);
        stat.setString(2, formula);
        stat.setInt(3, Type);
        stat.executeUpdate();
        int i = stat.getInt(1);
        done = i == 1;
        super.postCollable(stat);
        
        return done;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,(Select name from file_column_definition where id = COLUMN_ID)COLUMN_NAME,VALDATION_TYPE,COLUMN_ID,NAME,FORMULA,FORMULA_CODE,ACTIVE From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<validationrulesDTOInter> Records = new ArrayList<validationrulesDTOInter>();
        while (rs.next()) {
            validationrulesDTOInter SelectedRecord = DTOFactory.createvalidationrulesDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setvaldationtype(rs.getInt("VALDATION_TYPE"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setformulacode(rs.getString("FORMULA_CODE"));
            SelectedRecord.setformula(rs.getString("FORMULA"));
            SelectedRecord.setcolumnname(rs.getString("COLUMN_NAME"));
            SelectedRecord.setactive(rs.getInt("ACTIVE"));
            if (SelectedRecord.getactive() == 1) {
                SelectedRecord.setActiveB(Boolean.TRUE);
            } else {
                SelectedRecord.setActiveB(Boolean.FALSE);
            }
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        validationrulesDTOInter RecordToDelete = (validationrulesDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        String msg = "";
        msg = super.executeUpdate(deleteStat);
        msg = msg + " Row Has Been deleted";
        super.postUpdate("Delete Record In Table VALIDATION_RULES", false);
        return msg;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<validationrulesDTOInter> entities = (List<validationrulesDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Transaction Type Found";
        } else {
            String validate = "Validate";
            if (validate.equals("Validate")) {
                String insertStat = "Update VALIDATION_RULES Set ACTIVE = ? Where ID = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        validationrulesDTOInter RecordtoInsert = entities.get(i);
                        if (RecordtoInsert.getActiveB() == Boolean.TRUE) {
                            RecordtoInsert.setactive(1);
                        } else {
                            RecordtoInsert.setactive(2);
                        }
                        statement.setInt(1, RecordtoInsert.getactive());
                        statement.setInt(2, RecordtoInsert.getid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " Record has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }

                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                            CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
