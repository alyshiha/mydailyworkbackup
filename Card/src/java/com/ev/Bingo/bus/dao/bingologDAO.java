package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.bingologDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class bingologDAO extends BaseDAO implements bingologDAOInter {

    protected bingologDAO() {
        super();
        super.setTableName("BINGO_LOG");
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        String wherecondition = (String) obj[0];
        String selectStat = "Select b.ACTION_DATE,(select s.user_name from users s where s.user_id = b.USER_ID) UserName ,(select r.profile_name from profile r , user_profile u where u.profile_id = r.profile_id and u.user_id = b.USER_ID) userprofile ,b.IP_ADDRESS,decode(b.ACTION,'Update User Status 2 In Table USERS','User Login','Update User Status 1 In Table USERS','User LogOut',b.ACTION) ACTION From bingo_log b ,user_profile up where  up.user_id=B.USER_ID ";
        selectStat = selectStat + wherecondition + " order by b.ACTION_DATE desc";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<bingologDTOInter> Records = new ArrayList<bingologDTOInter>();
        while (rs.next()) {
            bingologDTOInter SelectedRecord = DTOFactory.createbingologDTO();
            SelectedRecord.setactiondate(rs.getTimestamp("ACTION_DATE"));
            SelectedRecord.setusername(rs.getString("UserName"));
            SelectedRecord.setprofilename(rs.getString("userprofile"));
            SelectedRecord.setipaddress(rs.getString("IP_ADDRESS"));
            SelectedRecord.setaction(rs.getString("ACTION"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

}
