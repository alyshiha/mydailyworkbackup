package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.disputerulesDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class disputerulesDAO extends BaseDAO implements disputerulesDAOInter {

    protected disputerulesDAO() {
        super();
        super.setTableName("dispute_rules");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        disputerulesDTOInter RecordToInsert = (disputerulesDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (ID,VALDATION_TYPE,COLUMN_ID,NAME,FORMULA,FORMULA_CODE) "
                + " values "
                + " ($id,$valdationtype,$columnid,'$name','$formula','$formulacode')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$valdationtype", "" + RecordToInsert.getvaldationtype());
        insertStat = insertStat.replace("$columnid", "" + RecordToInsert.getcolumnid());
        insertStat = insertStat.replace("$name", "" + RecordToInsert.getname());
        insertStat = insertStat.replace("$formulacode", "" + RecordToInsert.getformulacode());
        insertStat = insertStat.replace("$formula", "" + RecordToInsert.getformula());
        String msg = "";
        msg = super.executeUpdate(insertStat);
        msg = msg + " Row Has Been Inserted";
        super.postUpdate("Add New Record To dispute_rules By Name " + RecordToInsert.getname(), false);
        return msg;
    }

    public Boolean checkFormula(String formula, Integer Type) throws Throwable {
        super.preCollable();
        CallableStatement stat = super.executeCallableStatment("{call ? := check_formula_java(?,?)}");
        Boolean done = false;
        stat.registerOutParameter(1, Types.NUMERIC);
        stat.setString(2, formula);
        stat.setInt(3, Type);
        stat.executeUpdate();
        int i = stat.getInt(1);
        done = i == 1;
        super.postCollable(stat);
        
        return done;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,(Select name from file_column_definition where id = COLUMN_ID)COLUMN_NAME,VALDATION_TYPE,COLUMN_ID,NAME,FORMULA,FORMULA_CODE From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<disputerulesDTOInter> Records = new ArrayList<disputerulesDTOInter>();
        while (rs.next()) {
            disputerulesDTOInter SelectedRecord = DTOFactory.createdisputerulesDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setvaldationtype(rs.getInt("VALDATION_TYPE"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setformulacode(rs.getString("FORMULA_CODE"));
            SelectedRecord.setformula(rs.getString("FORMULA"));
            SelectedRecord.setcolumnname(rs.getString("COLUMN_NAME"));

            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        disputerulesDTOInter RecordToDelete = (disputerulesDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        String msg = "";
        msg = super.executeUpdate(deleteStat);
        msg = msg + " Row Has Been deleted";
        super.postUpdate("Delete Record In Table dispute_rules", false);
        return msg;
    }

}
