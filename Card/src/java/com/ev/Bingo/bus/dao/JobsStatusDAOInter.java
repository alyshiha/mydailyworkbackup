/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

/**
 *
 * @author Administrator
 */
public interface JobsStatusDAOInter {

    Object findAll() throws Throwable;

    Object findBroken(int jobId) throws Throwable;

    Object findRunning(int jobId) throws Throwable;

    Object disableJob(boolean status, int process) throws Throwable;

}
