package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.Date;
import com.ev.Bingo.bus.dto.atmfileDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class atmfileDAO extends BaseDAO implements atmfileDAOInter {

    protected atmfileDAO() {
        super();
        super.setTableName("loading_file_log");
    }

    public String SearchRecord(Object... obj) {
        Date loadingfrom = (Date) obj[0];
        Date loadingto = (Date) obj[1];
        Integer FileId = (Integer) obj[2];
        String Validate = "where 1 = 1";
        if (loadingfrom != null) {
            Validate = Validate + " and loading_date > to_date('$dateF','dd.mm.yyyy hh24:mi:ss')";
            Validate = Validate.replace("$dateF", "" + DateFormatter.changeDateAndTimeFormat(loadingfrom));
        }
        if (loadingto != null) {
            Validate = Validate + " and loading_date < to_date('$dateT','dd.mm.yyyy hh24:mi:ss')";
            Validate = Validate.replace("$dateT", "" + DateFormatter.changeDateAndTimeFormat(loadingto));
        }
        if (!FileId.toString().trim().equals("0")) {
            Validate = Validate + " and type = $ATM";
            Validate = Validate.replace("$ATM", "" + FileId.toString());
        }
        if (!"where 1 = 1".equals(Validate)) {
            Validate = Validate.replace(" 1 = 1 and", " ");
        } else {
            Validate = "";
        }
        return Validate;
    }

    public Object findRecordsListSearch(Object... obj) throws Throwable {

        super.preSelect();
        Date loadingfrom = (Date) obj[0];
        Date loadingto = (Date) obj[1];
        Integer FileId = (Integer) obj[2];
        String Where = SearchRecord(loadingfrom, loadingto, FileId);
        String selectStat = "Select ID,NAME,BACKUP_NAME,LOADING_DATE,TEMPLATE,FINISH_LOADING_DATE,NUMBER_OF_TRANSACTIONS,NUMBER_OF_DUPLICATION,decode(TYPE,1,'Network',2,'Switch',3,'Host','Other') TYPE From  $table "
                + "  $Where order by loading_date";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$Where", "" + Where);
        ResultSet rs = executeQuery(selectStat);
        List<atmfileDTOInter> Records = new ArrayList<atmfileDTOInter>();
        while (rs.next()) {
            atmfileDTOInter SelectedRecord = DTOFactory.createatmfileDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setbackupname(rs.getString("BACKUP_NAME"));
            SelectedRecord.setloadingdate(rs.getTimestamp("LOADING_DATE"));
            SelectedRecord.settemplate(rs.getInt("TEMPLATE"));
            SelectedRecord.setfinishloadingdate(rs.getTimestamp("FINISH_LOADING_DATE"));
            SelectedRecord.setnumberoftransactions(rs.getInt("NUMBER_OF_TRANSACTIONS"));
            SelectedRecord.setnumberofduplication(rs.getInt("NUMBER_OF_DUPLICATION"));
            SelectedRecord.settype(rs.getString("TYPE"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

}
