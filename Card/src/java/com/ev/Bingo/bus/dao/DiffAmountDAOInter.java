/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

/**
 *
 * @author AlyShiha
 */
public interface DiffAmountDAOInter {

    Object findAllRecordsPrint(Object... obj) throws Throwable;

    Object findAllRecords(Object... obj) throws Throwable;

    Object findfileall(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;
    
    void recalc() throws Throwable;
}
