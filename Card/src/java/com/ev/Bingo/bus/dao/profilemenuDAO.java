package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.profilemenuDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class profilemenuDAO extends BaseDAO implements profilemenuDAOInter {

    protected profilemenuDAO() {
        super();
        super.setTableName("PROFILE_MENU");
    }

    public String ValidateNull(Object... obj) {
        profilemenuDTOInter RecordToInsert = (profilemenuDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getprofileid() == 0) {
            Validate = Validate + " Feild +profileid is a requiered field";
        }
        if (RecordToInsert.getmenuid() == 0) {
            Validate = Validate + " Feild +menuid is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        profilemenuDTOInter RecordToDelete = (profilemenuDTOInter) obj[0];
        String msg = ValidateNull(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table where MENU_ID = $menuid and profile_id = $profileid ";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$profileid", "" + RecordToDelete.getprofileid());
            deleteStat = deleteStat.replace("$menuid", "" + RecordToDelete.getmenuid());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " Page Has Been UnAssigned";
            super.postUpdate("Deleting Page  With ID " + RecordToDelete.getmenuid() + " & Profile " + RecordToDelete.getprofileid(), false);
        }
        return msg;
    }

}
