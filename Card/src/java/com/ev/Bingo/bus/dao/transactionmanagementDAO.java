package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.transactionmanagementDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class transactionmanagementDAO extends BaseDAO implements transactionmanagementDAOInter {

    protected transactionmanagementDAO() {
        super();
        super.setTableName("TRANSACTION_MANAGEMENT");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactionmanagementDTOInter RecordToInsert = (transactionmanagementDTOInter) obj[0];

        String insertStat = "insert into $table"
                + " (TRAN_NO,KEY,TRAN_MONTH,TRAN_YEAR) "
                + " values "
                + " ($tranno,'$key',$tranmonth,$tranyear)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$tranno", "" + RecordToInsert.gettranno());
        insertStat = insertStat.replace("$key", "" + RecordToInsert.getkey());
        insertStat = insertStat.replace("$tranmonth", "" + RecordToInsert.gettranmonth());
        insertStat = insertStat.replace("$tranyear", "" + RecordToInsert.gettranyear());
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To TRANSACTION_MANAGEMENT", false);
        return null;

    }

    public Boolean ValidateNull(Object... obj) {
        transactionmanagementDTOInter RecordToInsert = (transactionmanagementDTOInter) obj[0];
        if (RecordToInsert.gettranno() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getkey() == null && RecordToInsert.getkey().equals("")) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.gettranmonth() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.gettranyear() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactionmanagementDTOInter RecordToUpdate = (transactionmanagementDTOInter) obj[0];
        String UpdateStat = "update $table set "
                + " (TRAN_NO= $tranno,KEY= '$key',TRAN_MONTH= $tranmonth,TRAN_YEAR= $tranyear) "
                + "  where  TRAN_MONTH= $tranmonth,TRAN_YEAR= $tranyear";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$tranno", "" + RecordToUpdate.gettranno());
        UpdateStat = UpdateStat.replace("$key", "" + RecordToUpdate.getkey());
        UpdateStat = UpdateStat.replace("$tranmonth", "" + RecordToUpdate.gettranmonth());
        UpdateStat = UpdateStat.replace("$tranyear", "" + RecordToUpdate.gettranyear());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table TRANSACTION_MANAGEMENT", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactionmanagementDTOInter RecordToDelete = (transactionmanagementDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  TRAN_MONTH= $tranmonth,TRAN_YEAR= $tranyear";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$tranno", "" + RecordToDelete.gettranno());
        deleteStat = deleteStat.replace("$key", "" + RecordToDelete.getkey());
        deleteStat = deleteStat.replace("$tranmonth", "" + RecordToDelete.gettranmonth());
        deleteStat = deleteStat.replace("$tranyear", "" + RecordToDelete.gettranyear());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table TRANSACTION_MANAGEMENT", false);
        return null;
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table TRANSACTION_MANAGEMENT", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        transactionmanagementDTOInter RecordToSelect = (transactionmanagementDTOInter) obj[0];
        String selectStat = "Select TRAN_NO,KEY,TRAN_MONTH,TRAN_YEAR From $table"
                + "  where  TRAN_MONTH= $tranmonth,TRAN_YEAR= $tranyear";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$tranmonth", "" + RecordToSelect.gettranmonth());
        selectStat = selectStat.replace("$tranyear", "" + RecordToSelect.gettranyear());
        ResultSet rs = executeQuery(selectStat);
        transactionmanagementDTOInter SelectedRecord = DTOFactory.createtransactionmanagementDTO();
        while (rs.next()) {
            SelectedRecord.settranno(rs.getInt("TRAN_NO"));
            SelectedRecord.setkey(rs.getString("KEY"));
            SelectedRecord.settranmonth(rs.getInt("TRAN_MONTH"));
            SelectedRecord.settranyear(rs.getInt("TRAN_YEAR"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        transactionmanagementDTOInter RecordToSelect = (transactionmanagementDTOInter) obj[0];
        String selectStat = "Select TRAN_NO,KEY,TRAN_MONTH,TRAN_YEAR From $table"
                + "  where  TRAN_MONTH= $tranmonth,TRAN_YEAR= $tranyear";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$tranmonth", "" + RecordToSelect.gettranmonth());
        selectStat = selectStat.replace("$tranyear", "" + RecordToSelect.gettranyear());
        ResultSet rs = executeQuery(selectStat);
        List<transactionmanagementDTOInter> Records = new ArrayList<transactionmanagementDTOInter>();
        while (rs.next()) {
            transactionmanagementDTOInter SelectedRecord = DTOFactory.createtransactionmanagementDTO();
            SelectedRecord.settranno(rs.getInt("TRAN_NO"));
            SelectedRecord.setkey(rs.getString("KEY"));
            SelectedRecord.settranmonth(rs.getInt("TRAN_MONTH"));
            SelectedRecord.settranyear(rs.getInt("TRAN_YEAR"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select TRAN_NO,KEY,TRAN_MONTH,TRAN_YEAR From $table order by TRAN_YEAR,TRAN_MONTH desc";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        transactionmanagementDTOInter SelectedRecord = DTOFactory.createtransactionmanagementDTO();
        if (rs.next()) {

            SelectedRecord.settranno(rs.getInt("TRAN_NO"));
            SelectedRecord.setkey(rs.getString("KEY"));
            SelectedRecord.settranmonth(rs.getInt("TRAN_MONTH"));
            SelectedRecord.settranyear(rs.getInt("TRAN_YEAR"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public void save(Object... obj) throws SQLException, Throwable {
        List<transactionmanagementDTOInter> entities = (List<transactionmanagementDTOInter>) obj[0];
        String insertStat = "insert into TRANSACTION_MANAGEMENT (TRAN_NO,KEY,TRAN_MONTH,TRAN_YEAR) values (?,?,?,?)";
        Connection connection = null;
        PreparedStatement statement = null;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.size(); i++) {
                transactionmanagementDTOInter RecordtoInsert = entities.get(i);
                statement.setInt(1, RecordtoInsert.gettranno());
                statement.setString(2, RecordtoInsert.getkey());
                statement.setInt(3, RecordtoInsert.gettranmonth());
                statement.setInt(4, RecordtoInsert.gettranyear());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                   CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
    }
}
