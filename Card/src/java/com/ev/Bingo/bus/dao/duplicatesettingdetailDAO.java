package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.duplicatesettingdetailDTOInter;
import com.ev.Bingo.bus.dto.duplicatesettingmasterDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class duplicatesettingdetailDAO extends BaseDAO implements duplicatesettingdetailDAOInter {

    protected duplicatesettingdetailDAO() {
        super();
        super.setTableName("DUPLICATE_SETTING_DETAIL");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        duplicatesettingdetailDTOInter RecordToInsert = (duplicatesettingdetailDTOInter) obj[0];
        String insertStat = "insert into $table"
                + " (ID,COLUMN_ID) "
                + " values "
                + " ($id,$columnid)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$columnid", "" + RecordToInsert.getcolumnid());
        String msg = "";
        try {
            msg = super.executeUpdate(insertStat);
            msg = msg + " Column Has Been Inserted";
        } catch (Exception ex) {
            msg = "Columns can't be Duplicated";
        }
        super.postUpdate("Adding New Column In Duplicate Details With Column ID " + RecordToInsert.getcolumnid(), false);
        return msg;
    }

    public Boolean ValidateNull(Object... obj) {
        duplicatesettingdetailDTOInter RecordToInsert = (duplicatesettingdetailDTOInter) obj[0];
        if (RecordToInsert.getid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getcolumnid() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        duplicatesettingmasterDTOInter RecordToDelete = (duplicatesettingmasterDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        String msg = super.executeUpdate(deleteStat);
        super.postUpdate("Deleting Column In Duplicate Details With Network ID " + RecordToDelete.getnetworkid() + " Record Type " + RecordToDelete.getrectype(), false);
        return msg + " Column Has Been Deleted";
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        duplicatesettingmasterDTOInter RecordToSelect = (duplicatesettingmasterDTOInter) obj[0];
        String selectStat = "Select ID,COLUMN_ID,rowid From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<duplicatesettingdetailDTOInter> Records = new ArrayList<duplicatesettingdetailDTOInter>();
        while (rs.next()) {
            duplicatesettingdetailDTOInter SelectedRecord = DTOFactory.createduplicatesettingdetailDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.setRowid(rs.getString("rowid"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,COLUMN_ID From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<duplicatesettingdetailDTOInter> Records = new ArrayList<duplicatesettingdetailDTOInter>();
        while (rs.next()) {
            duplicatesettingdetailDTOInter SelectedRecord = DTOFactory.createduplicatesettingdetailDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<duplicatesettingdetailDTOInter> entities = (List<duplicatesettingdetailDTOInter>) obj[0];
        String insertStat = "update DUPLICATE_SETTING_DETAIL set COLUMN_ID = ? where rowid = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        String mess = "", mess2 = "";
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.size(); i++) {
                duplicatesettingdetailDTOInter RecordtoInsert = entities.get(i);
                statement.setInt(1, RecordtoInsert.getcolumnid());
                statement.setString(2, RecordtoInsert.getRowid());

                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            try {
                int[] numUpdates = statement.executeBatch();
                Integer valid = 0, notvalid = 0;

                for (int i = 0; i < numUpdates.length; i++) {
                    if (numUpdates[i] == -2) {
                        valid++;
                        mess = valid + " rows has been updated";
                    } else {
                        notvalid++;
                        mess2 = notvalid + " can`t be updated";
                    }
                }
                if (!mess2.equals("")) {
                    mess = mess + "," + mess2;
                }
            } catch (Exception ex) {
                mess = "Columns can't be Duplicated";
            }
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
        return mess;
    }
}
