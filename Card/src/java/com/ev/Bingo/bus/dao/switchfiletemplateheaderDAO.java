package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.switchfiletemplateheaderDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class switchfiletemplateheaderDAO extends BaseDAO implements switchfiletemplateheaderDAOInter {

    protected switchfiletemplateheaderDAO() {
        super();
        super.setTableName("SWITCH_FILE_TEMPLATE_HEADER");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        switchfiletemplateheaderDTOInter RecordToInsert = (switchfiletemplateheaderDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (ID,COLUMN_ID,TEMPLATE,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LINE_NO) "
                + " values "
                + " ($id,$columnid,$template,$position,'$format','$format2',$datatype,$length,$lineno)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$columnid", "" + RecordToInsert.getcolumnid());
        insertStat = insertStat.replace("$template", "" + RecordToInsert.gettemplate());
        insertStat = insertStat.replace("$position", "" + RecordToInsert.getposition());
        insertStat = insertStat.replace("$format", "" + RecordToInsert.getformat());
        insertStat = insertStat.replace("$format2", "" + RecordToInsert.getformat2());
        insertStat = insertStat.replace("$datatype", "" + RecordToInsert.getdatatype());
        insertStat = insertStat.replace("$length", "" + RecordToInsert.getlength());
        insertStat = insertStat.replace("$lineno", "" + RecordToInsert.getlineno());
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To SWITCH_FILE_TEMPLATE_HEADER", false);
        return null;
    }

    public Boolean ValidateNull(Object... obj) {
        switchfiletemplateheaderDTOInter RecordToInsert = (switchfiletemplateheaderDTOInter) obj[0];
        if (RecordToInsert.getid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getcolumnid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.gettemplate() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getposition() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getdatatype() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        switchfiletemplateheaderDTOInter RecordToUpdate = (switchfiletemplateheaderDTOInter) obj[0];
        String UpdateStat = "update $table set "
                + " (ID= $id,COLUMN_ID= $columnid,TEMPLATE= $template,POSITION= $position,FORMAT= '$format',FORMAT2= '$format2',DATA_TYPE= $datatype,LENGTH= $length,LINE_NO= $lineno) "
                + "  where  ID= $id";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$id", "" + RecordToUpdate.getid());
        UpdateStat = UpdateStat.replace("$columnid", "" + RecordToUpdate.getcolumnid());
        UpdateStat = UpdateStat.replace("$template", "" + RecordToUpdate.gettemplate());
        UpdateStat = UpdateStat.replace("$position", "" + RecordToUpdate.getposition());
        UpdateStat = UpdateStat.replace("$format", "" + RecordToUpdate.getformat());
        UpdateStat = UpdateStat.replace("$format2", "" + RecordToUpdate.getformat2());
        UpdateStat = UpdateStat.replace("$datatype", "" + RecordToUpdate.getdatatype());
        UpdateStat = UpdateStat.replace("$length", "" + RecordToUpdate.getlength());
        UpdateStat = UpdateStat.replace("$lineno", "" + RecordToUpdate.getlineno());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table SWITCH_FILE_TEMPLATE_HEADER", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        switchfiletemplateheaderDTOInter RecordToDelete = (switchfiletemplateheaderDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        deleteStat = deleteStat.replace("$columnid", "" + RecordToDelete.getcolumnid());
        deleteStat = deleteStat.replace("$template", "" + RecordToDelete.gettemplate());
        deleteStat = deleteStat.replace("$position", "" + RecordToDelete.getposition());
        deleteStat = deleteStat.replace("$format", "" + RecordToDelete.getformat());
        deleteStat = deleteStat.replace("$format2", "" + RecordToDelete.getformat2());
        deleteStat = deleteStat.replace("$datatype", "" + RecordToDelete.getdatatype());
        deleteStat = deleteStat.replace("$length", "" + RecordToDelete.getlength());
        deleteStat = deleteStat.replace("$lineno", "" + RecordToDelete.getlineno());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table SWITCH_FILE_TEMPLATE_HEADER", false);
        return null;
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table SWITCH_FILE_TEMPLATE_HEADER", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        switchfiletemplateheaderDTOInter RecordToSelect = (switchfiletemplateheaderDTOInter) obj[0];
        String selectStat = "Select ID,COLUMN_ID,TEMPLATE,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LINE_NO From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        switchfiletemplateheaderDTOInter SelectedRecord = DTOFactory.createswitchfiletemplateheaderDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.settemplate(rs.getInt("TEMPLATE"));
            SelectedRecord.setposition(rs.getInt("POSITION"));
            SelectedRecord.setformat(rs.getString("FORMAT"));
            SelectedRecord.setformat2(rs.getString("FORMAT2"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));
            SelectedRecord.setlength(rs.getInt("LENGTH"));
            SelectedRecord.setlineno(rs.getInt("LINE_NO"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        switchfiletemplateheaderDTOInter RecordToSelect = (switchfiletemplateheaderDTOInter) obj[0];
        String selectStat = "Select ID,COLUMN_ID,TEMPLATE,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LINE_NO From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<switchfiletemplateheaderDTOInter> Records = new ArrayList<switchfiletemplateheaderDTOInter>();
        while (rs.next()) {
            switchfiletemplateheaderDTOInter SelectedRecord = DTOFactory.createswitchfiletemplateheaderDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.settemplate(rs.getInt("TEMPLATE"));
            SelectedRecord.setposition(rs.getInt("POSITION"));
            SelectedRecord.setformat(rs.getString("FORMAT"));
            SelectedRecord.setformat2(rs.getString("FORMAT2"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));
            SelectedRecord.setlength(rs.getInt("LENGTH"));
            SelectedRecord.setlineno(rs.getInt("LINE_NO"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,COLUMN_ID,TEMPLATE,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LINE_NO From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<switchfiletemplateheaderDTOInter> Records = new ArrayList<switchfiletemplateheaderDTOInter>();
        while (rs.next()) {
            switchfiletemplateheaderDTOInter SelectedRecord = DTOFactory.createswitchfiletemplateheaderDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.settemplate(rs.getInt("TEMPLATE"));
            SelectedRecord.setposition(rs.getInt("POSITION"));
            SelectedRecord.setformat(rs.getString("FORMAT"));
            SelectedRecord.setformat2(rs.getString("FORMAT2"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));
            SelectedRecord.setlength(rs.getInt("LENGTH"));
            SelectedRecord.setlineno(rs.getInt("LINE_NO"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public void save(Object... obj) throws SQLException, Throwable {
        List<switchfiletemplateheaderDTOInter> entities = (List<switchfiletemplateheaderDTOInter>) obj[0];
        String insertStat = "insert into SWITCH_FILE_TEMPLATE_HEADER (ID,COLUMN_ID,TEMPLATE,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LINE_NO) values (?,?,?,?,?,?,?,?,?)";
        Connection connection = null;
        PreparedStatement statement = null;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.size(); i++) {
                switchfiletemplateheaderDTOInter RecordtoInsert = entities.get(i);
                statement.setInt(1, RecordtoInsert.getid());
                statement.setInt(2, RecordtoInsert.getcolumnid());
                statement.setInt(3, RecordtoInsert.gettemplate());
                statement.setInt(4, RecordtoInsert.getposition());
                statement.setString(5, RecordtoInsert.getformat());
                statement.setString(6, RecordtoInsert.getformat2());
                statement.setInt(7, RecordtoInsert.getdatatype());
                statement.setInt(8, RecordtoInsert.getlength());
                statement.setInt(9, RecordtoInsert.getlineno());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                   CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
    }
}
