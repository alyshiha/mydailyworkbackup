package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;

import java.util.ArrayList;
import com.ev.Bingo.bus.dto.blockedusersDTOInter;
import com.ev.Bingo.bus.dto.blockreasonsDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class blockedusersDAO extends BaseDAO implements blockedusersDAOInter {

    protected blockedusersDAO() {
        super();
        super.setTableName("BLOCKED_USERS");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        blockedusersDTOInter RecordToInsert = (blockedusersDTOInter) obj[0];
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
//RecordToInsert.setid(super.generateSequence(super.getTableName()));
            String insertStat = "insert into $table"
                    + " (USER_ID,BLOCK_TIME,REASON_ID) "
                    + " values "
                    + " ($userid,sysdate,$reasonid)";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$userid", "" + RecordToInsert.getUserid());
            insertStat = insertStat.replace("$reasonid", "" + RecordToInsert.getReasonid());
            msg = super.executeUpdate(insertStat);
            msg = msg + " Row Has Been Inserted";
            super.postUpdate("User " + RecordToInsert.getUsername() + " Has Been Blocked", false);
        }
        return msg;
    }

    public String ValidateNull(Object... obj) {
        blockedusersDTOInter RecordToInsert = (blockedusersDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getUserid()== 0) {
            Validate = Validate + "   user name  ";
        }
        if (RecordToInsert.getReasonid()== 0) {
            Validate = Validate + "   reason  ";
        }
        if (!Validate.equals("Validate")) {
            Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        blockedusersDTOInter RecordToDelete = (blockedusersDTOInter) obj[0];
        String msg = ValidateNull(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  USER_ID= $userid";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$userid", "" + RecordToDelete.getUserid());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " Row Has Been deleted";
            super.postUpdate("User " + RecordToDelete.getUsername() + " Has Been UnBlocked", false);
        }
        return msg;
    }

    public Object findByReasonID(Object... obj) throws Throwable {
        super.preSelect();
        blockreasonsDTOInter RecordToSelect = (blockreasonsDTOInter) obj[0];
        String selectStat = "Select USER_ID,BLOCK_TIME,REASON_ID From $table"
                + "  where  REASON_ID= $REASONID";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$REASONID", "" + RecordToSelect.getreasonid());
        ResultSet rs = executeQuery(selectStat);
        List<blockedusersDTOInter> Records = new ArrayList<blockedusersDTOInter>();
        while (rs.next()) {
            blockedusersDTOInter SelectedRecord = DTOFactory.createblockedusersDTO();
            SelectedRecord.setUserid(rs.getInt("USER_ID"));
            SelectedRecord.setBlocktime(rs.getTimestamp("BLOCK_TIME"));
            SelectedRecord.setReasonid(rs.getInt("REASON_ID"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select USER_ID,(select s.reason_name from block_reasons s where s.reason_id = $table.REASON_ID) Reasonname,(select h.user_name from users h where h.user_id = $table.USER_ID) username,BLOCK_TIME,REASON_ID From $table ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<blockedusersDTOInter> Records = new ArrayList<blockedusersDTOInter>();
        while (rs.next()) {
            blockedusersDTOInter SelectedRecord = DTOFactory.createblockedusersDTO();
            SelectedRecord.setUserid(rs.getInt("USER_ID"));
            SelectedRecord.setUsername(rs.getString("username"));
            SelectedRecord.setReasonname(rs.getString("Reasonname"));
            SelectedRecord.setBlocktime(rs.getTimestamp("BLOCK_TIME"));
            SelectedRecord.setReasonid(rs.getInt("REASON_ID"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }
}
