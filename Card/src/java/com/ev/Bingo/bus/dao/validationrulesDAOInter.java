package com.ev.Bingo.bus.dao;

import java.util.Date;
import java.util.List;
import java.sql.SQLException;

public interface validationrulesDAOInter {

    Object findRecordsAll(Object... obj) throws Throwable;

    String save(Object... obj) throws SQLException, Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object insertrecord(Object... obj) throws Throwable;

    Boolean checkFormula(String formula, Integer Type) throws Throwable;
}
