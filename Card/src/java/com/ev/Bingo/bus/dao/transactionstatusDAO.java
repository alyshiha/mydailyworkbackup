package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.transactionstatusDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class transactionstatusDAO extends BaseDAO implements transactionstatusDAOInter {

    protected transactionstatusDAO() {
        super();
        super.setTableName("TRANSACTION_STATUS");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactionstatusDTOInter RecordToInsert = (transactionstatusDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
            String insertStat = "insert into $table"
                    + " (ID,NAME,DESCRIPTION) "
                    + " values "
                    + " ($id,'$name','$description')";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
            insertStat = insertStat.replace("$name", "" + RecordToInsert.getname());
            insertStat = insertStat.replace("$description", "" + RecordToInsert.getdescription());
            msg = super.executeUpdate(insertStat);
            msg = msg + " Record Has Been Inserted";
            super.postUpdate("Add New Record To TRANSACTION_STATUS", false);
        }
        return msg;
    }

    public String ValidateNull(Object... obj) {
        transactionstatusDTOInter RecordToInsert = (transactionstatusDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getid() == 0) {
            Validate = Validate + "   id  ";
        }
        if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {
            Validate = Validate + "   name  ";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required fields";
        }
        return Validate;
    }

    public String ValidateNullDelete(Object... obj) {
        transactionstatusDTOInter RecordToInsert = (transactionstatusDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getid() == 0) {
            Validate = Validate + "   id  ";
        }

        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required fields";
        }
        return Validate;
    }

    public String ValidateNullList(Object... obj) {
        List<transactionstatusDTOInter> AllRecords = (List<transactionstatusDTOInter>) obj[0];
        String Validate = "Validate";
        for (transactionstatusDTOInter RecordToInsert : AllRecords) {

            if (RecordToInsert.getid() == 0) {
                Validate = Validate + "   id  ";
            }
            if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {
                Validate = Validate + "   name  ";
            }
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required fields";
        }
        return Validate;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactionstatusDTOInter RecordToUpdate = (transactionstatusDTOInter) obj[0];
        String msg = ValidateNull(RecordToUpdate);
        if (msg.equals("Validate")) {
            String UpdateStat = "update $table set "
                    + "ID= $id,NAME= '$name',DESCRIPTION= '$description'"
                    + "  where  ID= $id";
            UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
            UpdateStat = UpdateStat.replace("$id", "" + RecordToUpdate.getid());
            UpdateStat = UpdateStat.replace("$name", "" + RecordToUpdate.getname());
            UpdateStat = UpdateStat.replace("$description", "" + RecordToUpdate.getdescription());
            msg = super.executeUpdate(UpdateStat);
            msg = msg + " Row Has Been updated";
            super.postUpdate("Update Record In Table TRANSACTION_STATUS", false);
        };
        return msg;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactionstatusDTOInter RecordToDelete = (transactionstatusDTOInter) obj[0];
        String msg = ValidateNull(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  ID= $id";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " Record Has Been deleted";
            super.postUpdate("Delete Record In Table TRANSACTION_STATUS", false);
        }
        return msg;
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table TRANSACTION_STATUS", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        transactionstatusDTOInter RecordToSelect = (transactionstatusDTOInter) obj[0];
        String selectStat = "Select ID,NAME,DESCRIPTION From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        transactionstatusDTOInter SelectedRecord = DTOFactory.createtransactionstatusDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setdescription(rs.getString("DESCRIPTION"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        transactionstatusDTOInter RecordToSelect = (transactionstatusDTOInter) obj[0];
        String selectStat = "Select ID,NAME,DESCRIPTION From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<transactionstatusDTOInter> Records = new ArrayList<transactionstatusDTOInter>();
        while (rs.next()) {
            transactionstatusDTOInter SelectedRecord = DTOFactory.createtransactionstatusDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setdescription(rs.getString("DESCRIPTION"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,NAME,DESCRIPTION From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<transactionstatusDTOInter> Records = new ArrayList<transactionstatusDTOInter>();
        while (rs.next()) {
            transactionstatusDTOInter SelectedRecord = DTOFactory.createtransactionstatusDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setdescription(rs.getString("DESCRIPTION"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<transactionstatusDTOInter> entities = (List<transactionstatusDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No TRANSACTION STATUS Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "update TRANSACTION_STATUS set NAME = ?,DESCRIPTION = ? where ID  = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        transactionstatusDTOInter RecordtoInsert = entities.get(i);

                        statement.setString(1, RecordtoInsert.getname());
                        statement.setString(2, RecordtoInsert.getdescription());
                        statement.setInt(3, RecordtoInsert.getid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " Record has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                            CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
