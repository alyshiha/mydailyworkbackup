package com.ev.Bingo.bus.dao;

import java.util.Date;
import java.util.List;
import java.sql.SQLException;

public interface diffamountreportDAOInter {

    Object findRecordsList(Object... obj) throws Throwable;

    Object findRecordsAll(Object... obj) throws Throwable;

}
