/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.CashManagmentLogDTOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.userPrivilegeDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class userPrivilegeDAO extends BaseDAO implements userPrivilegeDAOInter {

    public List<String> findPrivelage(Integer userid, String PageName) throws Throwable {
        super.preSelect();
        String selectStat = "select (select upper(p.prev_name) from privilege_status_name p where p.prev_id = u.prev_id) prev from user_privilege u where  \n"
                + "u.user_id = $userid and u.menu_id in (select m.menu_id from menu_labels m where upper(m.page) = upper('$page')) ";
        selectStat = selectStat.replace("$userid", "" + userid);
        selectStat = selectStat.replace("$page", "" + PageName);
        ResultSet rs = executeQuery(selectStat);
        List<String> records = new ArrayList<String>();
        while (rs.next()) {
            records.add(rs.getString("prev"));
        }
        super.postSelect(rs);
        return records;
    }

    public Boolean findDuplicatesaveRecord(int user, int menu, int prev, int id) throws Throwable {
        super.preSelect();
        String selectStat = "select user_prev_id, user_id, prev_id, menu_id from user_privilege where  "
                + "user_id = $userid and menu_id = $menuid and prev_id = $previd and user_prev_id <> $id";
        selectStat = selectStat.replace("$userid", "" + user);
        selectStat = selectStat.replace("$menuid", "" + menu);
        selectStat = selectStat.replace("$previd", "" + prev);
        selectStat = selectStat.replace("$id", "" + id);
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            super.postSelect(rs);
            CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
            logEngin.insert(" Duplicate records found by user id = " + user + " menu id: " + menu + " prev: " + prev);
            return Boolean.TRUE;
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("no Duplicate records found by user id = " + user + " menu id: " + menu + " prev: " + prev);
        return Boolean.FALSE;
    }

    public Boolean findDuplicateRecord(int user, int menu, int prev) throws Throwable {
        super.preSelect();
        String selectStat = "select user_prev_id, user_id, prev_id, menu_id from user_privilege where  "
                + "user_id = $userid and menu_id = $menuid and prev_id = $previd ";
        selectStat = selectStat.replace("$userid", "" + user);
        selectStat = selectStat.replace("$menuid", "" + menu);
        selectStat = selectStat.replace("$previd", "" + prev);
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            super.postSelect(rs);
            CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
            logEngin.insert("Duplicate records found by user id = " + user + " menu id: " + menu + " prev: " + prev);
            return Boolean.TRUE;
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("no Duplicate records found by user id = " + user + " menu id: " + menu + " prev: " + prev);
        return Boolean.FALSE;
    }

    @Override
    public List<userPrivilegeDTOInter> findRecord(String userid) throws Throwable {
        super.preSelect();
        String selectStat = "select user_prev_id, user_id, prev_id, menu_id from user_privilege where  user_id = $userid order by user_id ";
        selectStat = selectStat.replace("$userid", userid);
        ResultSet rs = executeQuery(selectStat);
        List<userPrivilegeDTOInter> records = new ArrayList<userPrivilegeDTOInter>();
        while (rs.next()) {
            userPrivilegeDTOInter record = DTOFactory.createuserPrivilegeDTO();
            record.setUserprevid(rs.getInt("user_prev_id"));
            record.setUserid(rs.getInt("user_id"));
            record.setPrevid(rs.getInt("prev_id"));
            record.setMenuid(rs.getInt("menu_id"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select records From User Privilege with user id = " + userid);
        return records;
    }

    @Override
    public List<userPrivilegeDTOInter> findAll() throws Throwable {
        super.preSelect();
        String selectStat = "select user_prev_id, user_id, prev_id, menu_id from user_privilege order by user_id ";
        ResultSet rs = executeQuery(selectStat);
        List<userPrivilegeDTOInter> records = new ArrayList<userPrivilegeDTOInter>();
        while (rs.next()) {
            userPrivilegeDTOInter record = DTOFactory.createuserPrivilegeDTO();
            record.setUserprevid(rs.getInt("user_prev_id"));
            record.setUserid(rs.getInt("user_id"));
            record.setPrevid(rs.getInt("prev_id"));
            record.setMenuid(rs.getInt("menu_id"));
            records.add(record);
        }
        super.postSelect(rs);

        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select All records From User Privilege");
        return records;
    }

    @Override
    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        userPrivilegeDTOInter record = (userPrivilegeDTOInter) obj[0];
        record.setUserprevid(super.generateSequence("USER_PREV"));
        String insertStat = "insert into user_privilege (user_prev_id, user_id, prev_id, menu_id) values  ($userprevid, $userid, $previd, $menuid)";
        insertStat = insertStat.replace("$userprevid", "" + record.getUserprevid());
        insertStat = insertStat.replace("$userid", "" + record.getUserid());
        insertStat = insertStat.replace("$previd", "" + record.getPrevid());
        insertStat = insertStat.replace("$menuid", "" + record.getMenuid());
        super.executeUpdate(insertStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("insert record with id " + record.getUserprevid() + " in user privilege");
        return null;
    }

    @Override
    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        userPrivilegeDTOInter record = (userPrivilegeDTOInter) obj[0];
        String updateStat = "update user_privilege set user_id = $userid, prev_id = $previd, menu_id = $menuid where user_prev_id = $userprevid";
        updateStat = updateStat.replace("$userprevid", "" + record.getUserprevid());
        updateStat = updateStat.replace("$userid", "" + record.getUserid());
        updateStat = updateStat.replace("$previd", "" + record.getPrevid());
        updateStat = updateStat.replace("$menuid", "" + record.getMenuid());
        super.executeUpdate(updateStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("update record with id " + record.getUserprevid() + " in user privilege");
        return null;
    }

    @Override
    public Object delete(Object... obj) throws Throwable {

        super.preUpdate();
        userPrivilegeDTOInter record = (userPrivilegeDTOInter) obj[0];
        String deleteStat = "delete user_privilege\n"
                + " where user_prev_id = $userprevid";
        deleteStat = deleteStat.replace("$userprevid", "" + record.getUserprevid());
        super.executeUpdate(deleteStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("delete record with id " + record.getUserprevid() + " in user privilege");
        return null;
    }

}
