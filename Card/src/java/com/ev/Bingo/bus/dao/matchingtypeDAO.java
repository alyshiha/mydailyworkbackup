package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.matchingtypeDTOInter;
import com.ev.Bingo.bus.dto.matchingtypenetworksDTOInter;
import com.ev.Bingo.bus.dto.matchingtypesettingDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class matchingtypeDAO extends BaseDAO implements matchingtypeDAOInter {

    protected matchingtypeDAO() {
        super();
        super.setTableName("MATCHING_TYPE");
    }

    @Override
    public Object findAllmatchingtype(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,TYPE1,decode(TYPE1,1,'Network',2,'Switch',3,'Host','Other')TYPEName1,TYPE2,decode(TYPE2,1,'Network',2,'Switch',3,'Host','Other')TYPEName2,ACTIVE,NAME From matching_type";
        ResultSet rs = executeQuery(selectStat);
        List<matchingtypeDTOInter> Records = new ArrayList<matchingtypeDTOInter>();
        while (rs.next()) {
            matchingtypeDTOInter SelectedRecord = DTOFactory.creatematchingtypeDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.settype1(rs.getInt("TYPE1"));
            SelectedRecord.settypename1(rs.getString("TYPEName1"));
            SelectedRecord.settypename2(rs.getString("TYPEName2"));
            SelectedRecord.settype2(rs.getInt("TYPE2"));
            SelectedRecord.setactive(rs.getInt("ACTIVE"));
            if (SelectedRecord.getactive() == 1) {
                SelectedRecord.setActiveB(Boolean.TRUE);
            } else {
                SelectedRecord.setActiveB(Boolean.FALSE);
            }
            SelectedRecord.setname(rs.getString("NAME"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object updaterecordmatchingtype(Object... obj) throws Throwable {
        super.preUpdate();
        matchingtypeDTOInter RecordToUpdate = (matchingtypeDTOInter) obj[0];
        String UpdateStat = "update matching_type set ACTIVE= $active where  ID= $id";
        UpdateStat = UpdateStat.replace("$id", "" + RecordToUpdate.getid());
        if (RecordToUpdate.getActiveB() == Boolean.TRUE) {
            RecordToUpdate.setactive(1);
        } else {
            RecordToUpdate.setactive(2);
        }
        UpdateStat = UpdateStat.replace("$active", "" + RecordToUpdate.getactive());
        String msg = "";
        try {
            msg = super.executeUpdate(UpdateStat);
            super.postUpdate("Update Record " + RecordToUpdate.getname() + " Status In Table MATCHING TYPE", false);
        } catch (Exception ex) {
            msg = "Error";
        }
        return msg;
    }

    @Override
    public Object findRecordsListmatchingtypenetworks(Object... obj) throws Throwable {
        super.preSelect();
        matchingtypeDTOInter RecordToSelect = (matchingtypeDTOInter) obj[0];
        String selectStat = "Select rowid,MATCHING_TYPE,NETWORK_ID From matching_type_networks"
                + "  where  MATCHING_TYPE= $matchingtype";
        selectStat = selectStat.replace("$matchingtype", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<matchingtypenetworksDTOInter> Records = new ArrayList<matchingtypenetworksDTOInter>();
        while (rs.next()) {
            matchingtypenetworksDTOInter SelectedRecord = DTOFactory.creatematchingtypenetworksDTO();
            SelectedRecord.setmatchingtype(rs.getInt("MATCHING_TYPE"));
            SelectedRecord.setnetworkid(rs.getInt("NETWORK_ID"));
            SelectedRecord.setRowid(rs.getString("rowid"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object deleterecordmatchingtypenetworks(Object... obj) throws Throwable {
        super.preUpdate();
        matchingtypenetworksDTOInter RecordToDelete = (matchingtypenetworksDTOInter) obj[0];
        String deleteStat = "delete from matching_type_networks "
                + "  where  MATCHING_TYPE= $matchingtype and NETWORK_ID= $networkid and rowid = '$row'";
        deleteStat = deleteStat.replace("$row", "" + RecordToDelete.getRowid());
        deleteStat = deleteStat.replace("$matchingtype", "" + RecordToDelete.getmatchingtype());
        deleteStat = deleteStat.replace("$networkid", "" + RecordToDelete.getnetworkid());
        String msg = "";
        msg = super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table MATCHING_TYPE_NETWORKS By MATCHING_TYPE = " + RecordToDelete.getmatchingtype() + " and network ID = " + RecordToDelete.getnetworkid(), false);
        return msg + "Record Has Been Deleted";
    }

    @Override
    public Object insertrecordmatchingtypenetworks(Object... obj) throws Throwable {
        super.preUpdate();
        matchingtypenetworksDTOInter RecordToInsert = (matchingtypenetworksDTOInter) obj[0];
        String insertStat = "insert into matching_type_networks"
                + " (MATCHING_TYPE,NETWORK_ID) "
                + " values "
                + " ($matchingtype,$networkid)";
        insertStat = insertStat.replace("$matchingtype", "" + RecordToInsert.getmatchingtype());
        insertStat = insertStat.replace("$networkid", "" + RecordToInsert.getnetworkid());
        String msg = "";
        try {
            msg = super.executeUpdate(insertStat);
            super.postUpdate("Add New Record To MATCHING_TYPE_NETWORKS", false);
            msg = msg + " Network Has Been Inserted";
        } catch (Exception ex) {
            msg = "Duplicate Network";
        }
        return msg;
    }

    @Override
    public Object savematchingtypenetworks(Object... obj) throws SQLException, Throwable {
        List<matchingtypenetworksDTOInter> entities = (List<matchingtypenetworksDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.isEmpty()) {
            mess = "No Columns Found To Be Updated";
        } else {
            String insertStat = "UPDATE matching_type_networks SET NETWORK_ID = ? WHERE rowid = ?";
            Connection connection = null;
            PreparedStatement statement = null;
            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            try {
               connection = CoonectionHandler.getInstance().getConnection();
                statement = connection.prepareStatement(insertStat);
                for (int i = 0; i < entities.size(); i++) {
                    matchingtypenetworksDTOInter RecordtoInsert = entities.get(i);
                    statement.setInt(1, RecordtoInsert.getnetworkid());
                    statement.setString(2, RecordtoInsert.getRowid());
                    statement.addBatch();
                    if ((i + 1) % 1000 == 0) {
                        statement.executeBatch();
                    }
                }
                int[] numUpdates = statement.executeBatch();
                Integer valid = 0, notvalid = 0;

                for (int i = 0; i < numUpdates.length; i++) {
                    if (numUpdates[i] == -2) {
                        valid++;
                        mess = "Records Has Been Updated";
                    } else {
                        notvalid++;
                        mess2 = notvalid + " can`t be updated";
                    }
                }
                if (!mess2.equals("")) {
                    mess = mess + "," + mess2;
                }
            } catch (Exception ex) {
                mess = " Child Record Found";
            } finally {
                if (statement != null) {
                    try {
                        connection.commit();
                        statement.close();
                    } catch (SQLException logOrIgnore) {
                    }
                }
                if (connection != null) {
                    try {
                CoonectionHandler.getInstance().returnConnection(connection);
                    } catch (SQLException logOrIgnore) {
                    }
                }
            }
            return mess;

        }
        return mess;
    }

    @Override
    public Object findRecordsListmatchingtypesetting(Object... obj) throws Throwable {
        super.preSelect();
        matchingtypenetworksDTOInter RecordToSelect = (matchingtypenetworksDTOInter) obj[0];
        String selectStat = "Select rowid,ID,COLUMN_ID,PART_SETTING,PART_LENGTH,NETWORK_ID From matching_type_setting"
                + "  where  ID= $id And NETWORK_ID= $networkid";
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getmatchingtype());
        selectStat = selectStat.replace("$networkid", "" + RecordToSelect.getnetworkid());
        ResultSet rs = executeQuery(selectStat);
        List<matchingtypesettingDTOInter> Records = new ArrayList<matchingtypesettingDTOInter>();
        while (rs.next()) {
            matchingtypesettingDTOInter SelectedRecord = DTOFactory.creatematchingtypesettingDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.setpartsetting(rs.getInt("PART_SETTING"));
            SelectedRecord.setpartlength(rs.getString("PART_LENGTH"));
            SelectedRecord.setnetworkid(rs.getInt("NETWORK_ID"));
            SelectedRecord.setRowid(rs.getString("rowid"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object deleterecordmatchingtypesetting(Object... obj) throws Throwable {
        super.preUpdate();
        matchingtypesettingDTOInter RecordToDelete = (matchingtypesettingDTOInter) obj[0];
        String deleteStat = "delete from matching_type_setting "
                + "  where  rowid= '$id'";
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getRowid());
        String msg = "";
        msg = super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table MATCHING_TYPE_SETTING", false);
        return msg + " Record Has Been Deleted";
    }

    @Override
    public Object insertrecordmatchingtypesetting(Object... obj) throws Throwable {
        super.preUpdate();
        matchingtypesettingDTOInter RecordToInsert = (matchingtypesettingDTOInter) obj[0];
        String insertStat = "insert into matching_type_setting"
                + " (ID,COLUMN_ID,PART_SETTING,PART_LENGTH,NETWORK_ID) "
                + " values "
                + " ($id,$columnid,$partsetting,'$partlength',$networkid)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$columnid", "" + RecordToInsert.getcolumnid());
        insertStat = insertStat.replace("$partsetting", "" + RecordToInsert.getpartsetting());
        insertStat = insertStat.replace("$partlength", "" + RecordToInsert.getpartlength());
        insertStat = insertStat.replace("$networkid", "" + RecordToInsert.getnetworkid());
        String msg = "";
        try {
            super.executeUpdate(insertStat);
            super.postUpdate("Add New Record To MATCHING_TYPE_SETTING", false);
            msg = msg + " Record Has Been Inserted";
        } catch (Exception ex) {
            msg = "Duplicate Column";
        }
        return msg;
    }

    @Override
    public Object savematchingtypesetting(Object... obj) throws SQLException, Throwable {
        List<matchingtypesettingDTOInter> entities = (List<matchingtypesettingDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.isEmpty()) {
            mess = "No Columns Found To Be Updated";
        } else {
            String insertStat = "UPDATE matching_type_setting SET COLUMN_ID = ?,PART_SETTING = ?,PART_LENGTH = ? WHERE rowid = ?";
            Connection connection = null;
            PreparedStatement statement = null;
            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            try {
               connection = CoonectionHandler.getInstance().getConnection();
                
                statement = connection.prepareStatement(insertStat);
                for (int i = 0; i < entities.size(); i++) {
                    matchingtypesettingDTOInter RecordtoInsert = entities.get(i);
                    statement.setInt(1, RecordtoInsert.getcolumnid());
                    statement.setInt(2, RecordtoInsert.getpartsetting());
                    statement.setString(3, RecordtoInsert.getpartlength());
                    statement.setString(4, RecordtoInsert.getRowid());
                    statement.addBatch();
                    if ((i + 1) % 1000 == 0) {
                        statement.executeBatch();
                    }
                }
                int[] numUpdates = statement.executeBatch();
                Integer valid = 0, notvalid = 0;

                for (int i = 0; i < numUpdates.length; i++) {
                    if (numUpdates[i] == -2) {
                        valid++;
                        mess = valid + " Rows Has Been Updated";
                    } else {
                        notvalid++;
                        mess2 = notvalid + " can`t be updated";
                    }
                }
                if (!mess2.equals("")) {
                    mess = mess + "," + mess2;
                }
            } catch (Exception ex) {
                mess = "Duplicate COLUMN";
            } finally {
                if (statement != null) {
                    try {
                        connection.commit();
                        statement.close();

                    } catch (SQLException logOrIgnore) {
                    }
                }
                if (connection != null) {
                    try {
                        CoonectionHandler.getInstance().returnConnection(connection);
                    } catch (SQLException logOrIgnore) {
                    }
                }
            }
            return mess;

        }
        return mess;
    }

    public Object RecreateIndex(Object... obj) throws Throwable {
        super.preCollable();
        CallableStatement stat = super.executeCallableStatmentJobMon("{call  create_matching_indexes}");
        stat.execute();
        super.postCollable(stat);
        
        return "Index Has Been Recreated";
    }
}
