
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.privilegeStatusNameDTOInter;
import com.ev.Bingo.bus.dto.userPrivilegeDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AlyShiha
 */
public class privilegeStatusNameDAO extends BaseDAO implements privilegeStatusNameDAOInter {

    @Override
    public Object insertpriv(String Userid, String MenuID, String PrevID) throws Throwable {
        super.preUpdate();
        int seq = super.generateSequence("USER_PREV");
        String insertStat = "insert into user_privilege (user_prev_id, user_id, prev_id, menu_id) values  ($userprevid, $userid, $previd, $menuid)";
        insertStat = insertStat.replace("$userprevid", "" + seq);
        insertStat = insertStat.replace("$userid", "" + Userid);
        insertStat = insertStat.replace("$previd", "" + PrevID);
        insertStat = insertStat.replace("$menuid", "" + MenuID);
        super.executeUpdate(insertStat);
        return null;
    }

    @Override
    public List<privilegeStatusNameDTOInter> findprivassigned(String Userid, String MenuID) throws Throwable {
        super.preSelect();
        String selectStat = "select prev_id, prev_name, symbol \n"
                + "from privilege_status_name \n"
                + "where prev_id in (\n"
                + "select prev_id \n"
                + "from user_privilege \n"
                + "where user_id = $user and menu_id = $menu)";
        selectStat = selectStat.replace("$user", Userid);
        selectStat = selectStat.replace("$menu", MenuID);
        ResultSet rs = executeQuery(selectStat);
        List<privilegeStatusNameDTOInter> records = new ArrayList<privilegeStatusNameDTOInter>();
        while (rs.next()) {
            privilegeStatusNameDTOInter record = DTOFactory.createprivilegeStatusNameDTO();
            record.setPrev_id(rs.getInt("prev_id"));
            record.setPrev_name(rs.getString("prev_name"));
            record.setSymbol(rs.getString("symbol"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    @Override
    public List<privilegeStatusNameDTOInter> deleteprivassigned(String Userid, String MenuID) throws Throwable {
        super.preUpdate();
        String insertStat = "delete from user_privilege where user_id = $user and menu_id = $menu";
        insertStat = insertStat.replace("$user", "" + Userid);
        insertStat = insertStat.replace("$menu", "" + MenuID);
        super.executeUpdate(insertStat);
        return null;
    }

    @Override
    public List<privilegeStatusNameDTOInter> findRecord(String Name) throws Throwable {
        super.preSelect();
        String selectStat = "select prev_id, prev_name,symbol  from privilege_status_name where symbol like '%$name%' order by symbol ";
        selectStat = selectStat.replace("$name", Name);
        ResultSet rs = executeQuery(selectStat);
        List<privilegeStatusNameDTOInter> records = new ArrayList<privilegeStatusNameDTOInter>();
        while (rs.next()) {
            privilegeStatusNameDTOInter record = DTOFactory.createprivilegeStatusNameDTO();
            record.setPrev_id(rs.getInt("prev_id"));
            record.setPrev_name(rs.getString("prev_name"));
            record.setSymbol(rs.getString("symbol"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("search records in Status Name with name like: " + Name);
        return records;
    }

    @Override
    public List<privilegeStatusNameDTOInter> findAll() throws Throwable {
        super.preSelect();
        String selectStat = "select prev_id, prev_name,symbol from privilege_status_name order by symbol ";
        ResultSet rs = executeQuery(selectStat);
        List<privilegeStatusNameDTOInter> records = new ArrayList<privilegeStatusNameDTOInter>();
        while (rs.next()) {
            privilegeStatusNameDTOInter record = DTOFactory.createprivilegeStatusNameDTO();
            record.setPrev_id(rs.getInt("prev_id"));
            record.setPrev_name(rs.getString("prev_name"));
            record.setSymbol(rs.getString("symbol"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("search all records in Status Name");
        return records;
    }

    @Override
    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        privilegeStatusNameDTOInter record = (privilegeStatusNameDTOInter) obj[0];
        record.setPrev_id(super.generateSequence("privilege_status_name"));
        String insertStat = "insert into privilege_status_name (prev_id, prev_name,symbol) values ($previd, '$prevname','$prevname')";
        insertStat = insertStat.replace("$previd", "" + record.getPrev_id());
        insertStat = insertStat.replace("$prevname", "" + record.getSymbol());
        super.executeUpdate(insertStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("insert record in Status Name with id= " + record.getPrev_id());
        return null;
    }

    @Override
    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        privilegeStatusNameDTOInter record = (privilegeStatusNameDTOInter) obj[0];
        String updateStat = "update privilege_status_name set  symbol = '$prevname' where prev_id = $previd";
        updateStat = updateStat.replace("$previd", "" + record.getPrev_id());
        updateStat = updateStat.replace("$prevname", "" + record.getSymbol());

        super.executeUpdate(updateStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("update record in Status Name with id= " + record.getPrev_id());
        return null;
    }

    @Override
    public String delete(Object... obj) {
        try {
            super.preUpdate();
            privilegeStatusNameDTOInter record = (privilegeStatusNameDTOInter) obj[0];
            String deleteStat = "delete privilege_status_name\n"
                    + " where prev_id = $previd";
            deleteStat = deleteStat.replace("$previd", "" + record.getPrev_id());
            
            String i = super.executeUpdate(deleteStat);
            CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
            logEngin.insert("delete record in Status Name");
            if(i=="0"){
                return "Can't Delete, Its already assigned to User";
            }
            return "Record Has Been Succesfully Deleted";
        } catch (Throwable ex) {
            Logger.getLogger(privilegeStatusNameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return  "Record Has Been Succesfully Deleted";
    }

}
