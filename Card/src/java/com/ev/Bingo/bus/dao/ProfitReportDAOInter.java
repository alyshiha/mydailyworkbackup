/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.bus.dto.AtmMachineDTOInter;
import com.ev.Bingo.bus.dto.OffusDTOInter;
import com.ev.Bingo.bus.dto.OffusTotalsDTOInter;
import com.ev.Bingo.bus.dto.OnusDTOInter;
import com.ev.Bingo.bus.dto.OnusTotalsDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusInterDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusInterTotalsDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusTotalsDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author shi7a
 */
public interface ProfitReportDAOInter {

    List<OffusDTOInter> find123OffusTransactions(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable;

    List<OnusDTOInter> findOnusTransactions(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable;

    List<VisaOffusInterDTOInter> findVisaOffusInternationalTransactions(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable;

    List<VisaOffusDTOInter> findVisaOffusNationalTransactions(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable;

    List<OnusTotalsDTOInter> findOnusTransactionstotals(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable;

    List<VisaOffusInterTotalsDTOInter> findVisaOffusInternationalTransactionstotals(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable;

    List<VisaOffusTotalsDTOInter> findVisaOffusNationalTransactionstotals(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable;
List<AtmMachineDTOInter> findAtmMachineList() throws Throwable;
    List<OffusTotalsDTOInter> find123OffusTransactionstotals(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable;
}
