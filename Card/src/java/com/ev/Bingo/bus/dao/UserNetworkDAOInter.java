/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

/**
 *
 * @author AlyShiha
 */
public interface UserNetworkDAOInter {

    Boolean ValidateNull(Object... obj);

    String ValidateNullList(Object... obj);

    Object deleterecord(Object... obj) throws Throwable;

    Object findRecordsListCS(Object... obj) throws Throwable;

    Object insertrecord(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;

}
