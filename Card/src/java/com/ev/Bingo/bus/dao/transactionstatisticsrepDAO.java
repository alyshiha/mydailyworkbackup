package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.transactionstatisticsrepDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class transactionstatisticsrepDAO extends BaseDAO implements transactionstatisticsrepDAOInter {

    protected transactionstatisticsrepDAO() {
        super();
        super.setTableName("TRANSACTION_STATISTICS_REP");
    }

    public Object RunRep(Object... obj) throws Throwable {
        try {
            Date fromDate = (Date) obj[0];
            Date toDate = (Date) obj[1];
            Integer network = (Integer) obj[2];
            String fileid = (String) obj[3];
            String sql = "{ ? = call set_transaction_statistics_rep(1,?,to_date(?,'dd.mm.yyyy hh24:mi:ss'),to_date(?,'dd.mm.yyyy hh24:mi:ss'),?) }";
            Connection conn = CoonectionHandler.getInstance().getConnection();
            CallableStatement statement = conn.prepareCall(sql);
            statement.registerOutParameter(1, java.sql.Types.INTEGER);
            statement.setInt(2, network);
            statement.setString(3, "" + DateFormatter.changeDateAndTimeFormat(fromDate));
            statement.setString(4, "" + DateFormatter.changeDateAndTimeFormat(toDate));
            statement.setString(5, fileid);
            Boolean check = statement.execute();
            long id = statement.getLong(1);
            statement.close();
            CoonectionHandler.getInstance().returnConnection(conn);
            return "" + id;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return Boolean.FALSE;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        String RecordToSelect = (String) obj[0];
        String selectStat = "Select REPORT_ID,ID,NAME,MATCHED_COUNT,MATCHED_AMOUNT_NETWORK,MATCHED_AMOUNT_SWITCH,MATCHED_WITH_DIFF_AMOUNT_TOTAL,MATCHED_WITH_DIFF_AMOUNT_COUNT,COUNT_DISPUTES_NETWORK,TOTAL_DISPUTES_NETWORK,COUNT_DISPUTES_SWITCH,TOTAL_DISPUTES_SWITCH,COUNT_DISPUTES_SWITCH_000,TOTAL_DISPUTES_SWITCH_000,COUNT_DISPUTES_NETWORK_NOT_REV,TOTAL_DISPUTES_NETWORK_NOT_REV,COUNT_DISPUTES_NETWORK_REV,TOTAL_DISPUTES_NETWORK_REV From $table"
                + "  where  REPORT_ID= $reportid";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$reportid", "" + RecordToSelect);
        ResultSet rs = executeQuery(selectStat);
        List<transactionstatisticsrepDTOInter> Records = new ArrayList<transactionstatisticsrepDTOInter>();
        while (rs.next()) {
            transactionstatisticsrepDTOInter SelectedRecord = DTOFactory.createtransactionstatisticsrepDTO();
            SelectedRecord.setreportid(rs.getInt("REPORT_ID"));
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setmatchedcount(rs.getFloat("MATCHED_COUNT"));
            SelectedRecord.setmatchedamountnetwork(rs.getFloat("MATCHED_AMOUNT_NETWORK"));
            SelectedRecord.setmatchedamountswitch(rs.getFloat("MATCHED_AMOUNT_SWITCH"));
            SelectedRecord.setmatchedwithdiffamounttotal(rs.getFloat("MATCHED_WITH_DIFF_AMOUNT_TOTAL"));
            SelectedRecord.setmatchedwithdiffamountcount(rs.getFloat("MATCHED_WITH_DIFF_AMOUNT_COUNT"));
            SelectedRecord.setcountdisputesnetwork(rs.getFloat("COUNT_DISPUTES_NETWORK"));
            SelectedRecord.settotaldisputesnetwork(rs.getFloat("TOTAL_DISPUTES_NETWORK"));
            SelectedRecord.setcountdisputesswitch(rs.getFloat("COUNT_DISPUTES_SWITCH"));
            SelectedRecord.settotaldisputesswitch(rs.getFloat("TOTAL_DISPUTES_SWITCH"));
            SelectedRecord.setcountdisputesswitch000(rs.getFloat("COUNT_DISPUTES_SWITCH_000"));
            SelectedRecord.settotaldisputesswitch000(rs.getFloat("TOTAL_DISPUTES_SWITCH_000"));
            SelectedRecord.setcountdisputesnetworknotrev(rs.getFloat("COUNT_DISPUTES_NETWORK_NOT_REV"));
            SelectedRecord.settotaldisputesnetworknotrev(rs.getFloat("TOTAL_DISPUTES_NETWORK_NOT_REV"));
            SelectedRecord.setcountdisputesnetworkrev(rs.getFloat("COUNT_DISPUTES_NETWORK_REV"));
            SelectedRecord.settotaldisputesnetworkrev(rs.getFloat("TOTAL_DISPUTES_NETWORK_REV"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String RecordToSelect = (String) obj[0];
        String selectStat = "Select REPORT_ID,ID,NAME,MATCHED_COUNT,MATCHED_AMOUNT_NETWORK,MATCHED_AMOUNT_SWITCH,MATCHED_WITH_DIFF_AMOUNT_TOTAL,MATCHED_WITH_DIFF_AMOUNT_COUNT,COUNT_DISPUTES_NETWORK,TOTAL_DISPUTES_NETWORK,COUNT_DISPUTES_SWITCH,TOTAL_DISPUTES_SWITCH,COUNT_DISPUTES_SWITCH_000,TOTAL_DISPUTES_SWITCH_000,COUNT_DISPUTES_NETWORK_NOT_REV,TOTAL_DISPUTES_NETWORK_NOT_REV,COUNT_DISPUTES_NETWORK_REV,TOTAL_DISPUTES_NETWORK_REV From $table"
                + "  where  REPORT_ID= $reportid";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$reportid", "" + RecordToSelect);
        ResultSet rs = executeQuery(selectStat);

        return rs;
    }

}
