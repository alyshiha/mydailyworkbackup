/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.ATMMACHINECASHDTOINTER;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AlyShiha
 */
public class ATMMACHINEDAO extends BaseDAO implements ATMMACHINEDAOInter {

    @Override
    public Object insert(String ATMID, String ATMNAME) throws Throwable {
        super.preUpdate();
        int seq = super.generateSequence("ATM_MACHINE");
        String insertStat = "insert into ATM_MACHINE (ID, ATM_ID,ATM_NAME) values  ($id, '$atmid','$atmname')";
        insertStat = insertStat.replace("$id", "" + seq);
        insertStat = insertStat.replace("$atmid", "" + ATMID);
        insertStat = insertStat.replace("$atmname", "" + ATMNAME);

        super.executeUpdate(insertStat);
        return null;
    }

    public List<ATMMACHINECASHDTOINTER> findRecord(String Name, String ID) throws Throwable {
        super.preSelect();
        String selectStat = "select id, atm_id,ATM_NAME from atm_machine where ATM_NAME like '%$name%'  and atm_id like '%$ID%' order by ATM_NAME";
        selectStat = selectStat.replace("$name", Name);
        selectStat = selectStat.replace("$ID", ID);
        ResultSet rs = executeQuery(selectStat);
        List<ATMMACHINECASHDTOINTER> records = new ArrayList<ATMMACHINECASHDTOINTER>();
        while (rs.next()) {
            ATMMACHINECASHDTOINTER record = DTOFactory.createATMMACHINECASHDTO();
            record.setId(rs.getInt("id"));
            record.setAtmname(rs.getString("ATM_NAME"));
            record.setAtmid(rs.getString("atm_id"));
            records.add(record);
        }
        super.postSelect(rs);

        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select record in ATM_ACCOUNT_NAME by name like: " + Name);
        return records;
    }

    @Override
    public List<ATMMACHINECASHDTOINTER> findAllDistinct() throws Throwable {
        super.preSelect();
        String selectStat = "select distinct Machine.ID atm_id from(\n"
                + "select distinct t.atm_id ID from ATM_ACCOUNTS t \n"
                + "union\n"
                + "select distinct m.atm_id ID from atm_machine m) Machine\n"
                + "order by Machine.ID";
        ResultSet rs = executeQuery(selectStat);
        List<ATMMACHINECASHDTOINTER> records = new ArrayList<ATMMACHINECASHDTOINTER>();
        while (rs.next()) {
            ATMMACHINECASHDTOINTER record = DTOFactory.createATMMACHINECASHDTO();
            record.setAtmname(rs.getString("atm_id"));
            record.setAtmid(rs.getString("atm_id"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("search all records in ATM Name");
        return records;
    }

    @Override
    public List<ATMMACHINECASHDTOINTER> findAll() throws Throwable {
        super.preSelect();
        String selectStat = "select id, atm_id,atm_name from atm_machine order by atm_name ";
        ResultSet rs = executeQuery(selectStat);
        List<ATMMACHINECASHDTOINTER> records = new ArrayList<ATMMACHINECASHDTOINTER>();
        while (rs.next()) {
            ATMMACHINECASHDTOINTER record = DTOFactory.createATMMACHINECASHDTO();
            record.setId(rs.getInt("id"));
            record.setAtmname(rs.getString("atm_id"));
            record.setAtmid(rs.getString("atm_name"));
            records.add(record);
        }
        super.postSelect(rs);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("search all records in ATM Name");
        return records;
    }

    @Override
    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        ATMMACHINECASHDTOINTER record = (ATMMACHINECASHDTOINTER) obj[0];
        String updateStat = "update atm_machine set  atm_id = '$atmid',atm_name = '$atmname' where id = $id";
        updateStat = updateStat.replace("$id", "" + record.getId());
        updateStat = updateStat.replace("$atmid", "" + record.getAtmname());
        updateStat = updateStat.replace("$atmname", "" + record.getAtmid());

        super.executeUpdate(updateStat);
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("update record in ATM Name with id= " + record.getId());
        return null;
    }

    @Override
    public String delete(Object... obj) {
        try {
            super.preUpdate();
            ATMMACHINECASHDTOINTER record = (ATMMACHINECASHDTOINTER) obj[0];
            String deleteStat = "delete atm_machine\n"
                    + " where id = $id";
            deleteStat = deleteStat.replace("$id", "" + record.getId());

            String i = super.executeUpdate(deleteStat);
            CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
            logEngin.insert("delete record in Status Name");
            if (i == "0") {
                return "Can't Delete, Its already assigned to User";
            }
            return "Record Has Been Succesfully Deleted";
        } catch (Throwable ex) {
            Logger.getLogger(privilegeStatusNameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Record Has Been Succesfully Deleted";
    }

}
