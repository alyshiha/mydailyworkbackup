package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class filecolumndefinitionDAO extends BaseDAO implements filecolumndefinitionDAOInter {

    protected filecolumndefinitionDAO() {
        super();
        super.setTableName("FILE_COLUMN_DEFINITION");
    }
//used in system rules

    public Object findRecordByColName(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,NAME,COLUMN_NAME,DATA_TYPE From $table"
                + "  where  COLUMN_NAME= 'COLUMN1'";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        filecolumndefinitionDTOInter SelectedRecord = DTOFactory.createfilecolumndefinitionDTO();
        while (rs.next()) {

            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setcolumnname(rs.getString("COLUMN_NAME"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));

        }
        super.postSelect(rs);
        return SelectedRecord;
    }
  public Object findRecordByColName4(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,NAME,COLUMN_NAME,DATA_TYPE From $table"
                + "  where  COLUMN_NAME= 'COLUMN4'";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        filecolumndefinitionDTOInter SelectedRecord = DTOFactory.createfilecolumndefinitionDTO();
        while (rs.next()) {

            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setcolumnname(rs.getString("COLUMN_NAME"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));

        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordByID(Object... obj) throws Throwable {
        super.preSelect();
        Integer RecordToSelect = (Integer) obj[0];
        String selectStat = "Select ID,NAME,COLUMN_NAME,DATA_TYPE From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect);
        ResultSet rs = executeQuery(selectStat);
        filecolumndefinitionDTOInter SelectedRecord = DTOFactory.createfilecolumndefinitionDTO();
        while (rs.next()) {

            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setcolumnname(rs.getString("COLUMN_NAME"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));

        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsAllview(Object... obj) throws Throwable {
        Integer RecordToSelect = (Integer) obj[0];
        super.preSelect();
        String selectStat = "Select f.ID ID,f.NAME NAME,f.COLUMN_NAME COLUMN_NAME,f.DATA_TYPE DATA_TYPE,n.selected selected From FILE_COLUMN_DEFINITION f,NETWORK_DEFAULT_COLUMNS n WHERE f.ID = n.column_id and  n.NETWORK_ID = $ID order by f.NAME";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$ID", "" + RecordToSelect);
        ResultSet rs = executeQuery(selectStat);
        List<filecolumndefinitionDTOInter> Records = new ArrayList<filecolumndefinitionDTOInter>();
        while (rs.next()) {
            filecolumndefinitionDTOInter SelectedRecord = DTOFactory.createfilecolumndefinitionDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setcolumnname(rs.getString("COLUMN_NAME"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));
            SelectedRecord.setselected(rs.getInt("selected"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,NAME,COLUMN_NAME,DATA_TYPE From $table order by NAME";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<filecolumndefinitionDTOInter> Records = new ArrayList<filecolumndefinitionDTOInter>();
        while (rs.next()) {
            filecolumndefinitionDTOInter SelectedRecord = DTOFactory.createfilecolumndefinitionDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setcolumnname(rs.getString("COLUMN_NAME"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<filecolumndefinitionDTOInter> entities = (List<filecolumndefinitionDTOInter>) obj[0];
        String insertStat = "UPDATE FILE_COLUMN_DEFINITION SET NAME = ?,COLUMN_NAME = ?,DATA_TYPE = ?,PRINT = ?,PRINT_FORMAT = ?,PRINT_ORDER = ? WHERE ID = ?";
        Connection connection = null;
        String mess = "", mess2 = "";
        PreparedStatement statement = null;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.size(); i++) {
                filecolumndefinitionDTOInter RecordtoInsert = entities.get(i);

                statement.setString(1, RecordtoInsert.getname());
                statement.setString(2, RecordtoInsert.getcolumnname());
                statement.setInt(3, RecordtoInsert.getdatatype());
                statement.setInt(4, RecordtoInsert.getprint());
                statement.setString(5, RecordtoInsert.getprintformat());
                statement.setInt(6, RecordtoInsert.getprintorder());
                statement.setInt(7, RecordtoInsert.getid());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            int[] numUpdates = statement.executeBatch();
            Integer valid = 0, notvalid = 0;

            for (int i = 0; i < numUpdates.length; i++) {
                if (numUpdates[i] == -2) {
                    valid++;
                    mess = valid + " Columns Has Been Updated";
                } else {
                    notvalid++;
                    mess2 = notvalid + " can`t be updated";
                }
            }
            if (!mess2.equals("")) {
                mess = mess + "," + mess2;
            }
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
             CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
        return mess;
    }
}
