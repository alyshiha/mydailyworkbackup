package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.networkcodesDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class networkcodesDAO extends BaseDAO implements networkcodesDAOInter {

    protected networkcodesDAO() {
        super();
        super.setTableName("NETWORK_CODES");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        networkcodesDTOInter RecordToInsert = (networkcodesDTOInter) obj[0];
//        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (NETWORK_ID,NETWORK_CODE) "
                + " values "
                + " ($networkid,'$networkcode')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$networkid", "" + RecordToInsert.getnetworkid());
        insertStat = insertStat.replace("$networkcode", "" + RecordToInsert.getnetworkcode());
        String msg = "";
        try {
            msg = (String) super.executeUpdate(insertStat) + " Network Code Has Been Inserted";
        } catch (Exception ex) {
            msg = "Please Enter A Unique Code";
        }
        super.postUpdate("Add New Record To NETWORK CODES By Code" + RecordToInsert.getnetworkcode(), false);
        return msg;
    }

    public Boolean ValidateNull(Object... obj) {
        networkcodesDTOInter RecordToInsert = (networkcodesDTOInter) obj[0];
        if (RecordToInsert.getnetworkid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getnetworkcode() == null && RecordToInsert.getnetworkcode().equals("")) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;

    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        String msg;
        networksDTOInter RecordToDelete = (networksDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  NETWORK_ID= $networkid";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$networkid", "" + RecordToDelete.getid());
        msg = (String) super.executeUpdate(deleteStat);
        super.postUpdate("Deleting A Network With Name" + RecordToDelete.getname(), false);
        return msg;
    }

    public Object deleterecorddetail(Object... obj) throws Throwable {
        super.preUpdate();
        String msg;
        networkcodesDTOInter RecordToDelete = (networkcodesDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  NETWORK_ID= $networkid AND network_code = '$networkcode'";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$networkid", "" + RecordToDelete.getnetworkid());
        deleteStat = deleteStat.replace("$networkcode", "" + RecordToDelete.getnetworkcode());
        msg = (String) super.executeUpdate(deleteStat);
        super.postUpdate("Deleting Details of Network " + RecordToDelete.getnetworkcode(), false);
        return msg;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        networksDTOInter RecordToSelect = (networksDTOInter) obj[0];
        String selectStat = "Select NETWORK_ID,NETWORK_CODE,rowid From $table"
                + "  where  NETWORK_ID= $networkid";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$networkid", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<networkcodesDTOInter> Records = new ArrayList<networkcodesDTOInter>();
        while (rs.next()) {
            networkcodesDTOInter SelectedRecord = DTOFactory.createnetworkcodesDTO();
            SelectedRecord.setnetworkid(rs.getInt("NETWORK_ID"));
            SelectedRecord.setnetworkcode(rs.getString("NETWORK_CODE"));
            SelectedRecord.setrowid(rs.getString("rowid"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String ValidateNullList(Object... obj) {
        List<networkcodesDTOInter> AllRecords = (List<networkcodesDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (networkcodesDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getnetworkid() == 0) {
                Validate = Validate + "," + String.valueOf(i);
            }
            if (RecordToInsert.getnetworkcode() == null || RecordToInsert.getnetworkcode().equals("")) {
                Validate = Validate + "," + String.valueOf(i);
            }
            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = "Record With Index " + Validate + " Contain Null Name Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<networkcodesDTOInter> entities = (List<networkcodesDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.isEmpty()) {
            mess = "No Columns Found To Be Updated";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "UPDATE NETWORK_CODES SET network_code = ? ,network_id = ? WHERE rowid = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        networkcodesDTOInter RecordtoInsert = entities.get(i);
                        statement.setString(1, RecordtoInsert.getnetworkcode());
                        statement.setInt(2, RecordtoInsert.getnetworkid());
                        statement.setString(3, RecordtoInsert.getrowid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                          CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
