package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.hostfiletemplatedetailDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class hostfiletemplatedetailDAO extends BaseDAO implements hostfiletemplatedetailDAOInter {

    protected hostfiletemplatedetailDAO() {
        super();
        super.setTableName("HOST_FILE_TEMPLATE_DETAIL");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        hostfiletemplatedetailDTOInter RecordToInsert = (hostfiletemplatedetailDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (ID,COLUMN_ID,TEMPLATE,POSITION,LINE_NUMBER,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LOAD_WHEN_MAPPED,LENGTH_DIR,STARTING_POSITION,TAG_STRING,MANDATORY,NEGATIVE_AMOUNT_FLAG,ADD_DECIMAL,DECIMAL_POS) "
                + " values "
                + " ($id,$columnid,$template,$position,$linenumber,'$format','$format2',$datatype,$length,$loadwhenmapped,$lengthdir,$startingposition,'$tagstring',$mandatory,$negativeamountflag,$adddecimal,$decimalpos)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$columnid", "" + RecordToInsert.getcolumnid());
        insertStat = insertStat.replace("$template", "" + RecordToInsert.gettemplate());
        insertStat = insertStat.replace("$position", "" + RecordToInsert.getposition());
        insertStat = insertStat.replace("$linenumber", "" + RecordToInsert.getlinenumber());
        insertStat = insertStat.replace("$format", "" + RecordToInsert.getformat());
        insertStat = insertStat.replace("$format2", "" + RecordToInsert.getformat2());
        insertStat = insertStat.replace("$datatype", "" + RecordToInsert.getdatatype());
        insertStat = insertStat.replace("$length", "" + RecordToInsert.getlength());
        insertStat = insertStat.replace("$loadwhenmapped", "" + RecordToInsert.getloadwhenmapped());
        insertStat = insertStat.replace("$lengthdir", "" + RecordToInsert.getlengthdir());
        insertStat = insertStat.replace("$startingposition", "" + RecordToInsert.getstartingposition());
        insertStat = insertStat.replace("$tagstring", "" + RecordToInsert.gettagstring());
        insertStat = insertStat.replace("$mandatory", "" + RecordToInsert.getmandatory());
        insertStat = insertStat.replace("$negativeamountflag", "" + RecordToInsert.getnegativeamountflag());
        insertStat = insertStat.replace("$adddecimal", "" + RecordToInsert.getadddecimal());
        insertStat = insertStat.replace("$decimalpos", "" + RecordToInsert.getdecimalpos());
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To HOST_FILE_TEMPLATE_DETAIL", false);
        return null;
    }

    public Boolean ValidateNull(Object... obj) {
        hostfiletemplatedetailDTOInter RecordToInsert = (hostfiletemplatedetailDTOInter) obj[0];
        if (RecordToInsert.getid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getcolumnid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.gettemplate() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getposition() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getdatatype() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getloadwhenmapped() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getlengthdir() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getmandatory() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getadddecimal() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;

    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        hostfiletemplatedetailDTOInter RecordToUpdate = (hostfiletemplatedetailDTOInter) obj[0];
        String UpdateStat = "update $table set "
                + " (ID= $id,COLUMN_ID= $columnid,TEMPLATE= $template,POSITION= $position,LINE_NUMBER= $linenumber,FORMAT= '$format',FORMAT2= '$format2',DATA_TYPE= $datatype,LENGTH= $length,LOAD_WHEN_MAPPED= $loadwhenmapped,LENGTH_DIR= $lengthdir,STARTING_POSITION= $startingposition,TAG_STRING= '$tagstring',MANDATORY= $mandatory,NEGATIVE_AMOUNT_FLAG= $negativeamountflag,ADD_DECIMAL= $adddecimal,DECIMAL_POS= $decimalpos) "
                + "  where  ID= $id";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$id", "" + RecordToUpdate.getid());
        UpdateStat = UpdateStat.replace("$columnid", "" + RecordToUpdate.getcolumnid());
        UpdateStat = UpdateStat.replace("$template", "" + RecordToUpdate.gettemplate());
        UpdateStat = UpdateStat.replace("$position", "" + RecordToUpdate.getposition());
        UpdateStat = UpdateStat.replace("$linenumber", "" + RecordToUpdate.getlinenumber());
        UpdateStat = UpdateStat.replace("$format", "" + RecordToUpdate.getformat());
        UpdateStat = UpdateStat.replace("$format2", "" + RecordToUpdate.getformat2());
        UpdateStat = UpdateStat.replace("$datatype", "" + RecordToUpdate.getdatatype());
        UpdateStat = UpdateStat.replace("$length", "" + RecordToUpdate.getlength());
        UpdateStat = UpdateStat.replace("$loadwhenmapped", "" + RecordToUpdate.getloadwhenmapped());
        UpdateStat = UpdateStat.replace("$lengthdir", "" + RecordToUpdate.getlengthdir());
        UpdateStat = UpdateStat.replace("$startingposition", "" + RecordToUpdate.getstartingposition());
        UpdateStat = UpdateStat.replace("$tagstring", "" + RecordToUpdate.gettagstring());
        UpdateStat = UpdateStat.replace("$mandatory", "" + RecordToUpdate.getmandatory());
        UpdateStat = UpdateStat.replace("$negativeamountflag", "" + RecordToUpdate.getnegativeamountflag());
        UpdateStat = UpdateStat.replace("$adddecimal", "" + RecordToUpdate.getadddecimal());
        UpdateStat = UpdateStat.replace("$decimalpos", "" + RecordToUpdate.getdecimalpos());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table HOST_FILE_TEMPLATE_DETAIL", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        hostfiletemplatedetailDTOInter RecordToDelete = (hostfiletemplatedetailDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        deleteStat = deleteStat.replace("$columnid", "" + RecordToDelete.getcolumnid());
        deleteStat = deleteStat.replace("$template", "" + RecordToDelete.gettemplate());
        deleteStat = deleteStat.replace("$position", "" + RecordToDelete.getposition());
        deleteStat = deleteStat.replace("$linenumber", "" + RecordToDelete.getlinenumber());
        deleteStat = deleteStat.replace("$format", "" + RecordToDelete.getformat());
        deleteStat = deleteStat.replace("$format2", "" + RecordToDelete.getformat2());
        deleteStat = deleteStat.replace("$datatype", "" + RecordToDelete.getdatatype());
        deleteStat = deleteStat.replace("$length", "" + RecordToDelete.getlength());
        deleteStat = deleteStat.replace("$loadwhenmapped", "" + RecordToDelete.getloadwhenmapped());
        deleteStat = deleteStat.replace("$lengthdir", "" + RecordToDelete.getlengthdir());
        deleteStat = deleteStat.replace("$startingposition", "" + RecordToDelete.getstartingposition());
        deleteStat = deleteStat.replace("$tagstring", "" + RecordToDelete.gettagstring());
        deleteStat = deleteStat.replace("$mandatory", "" + RecordToDelete.getmandatory());
        deleteStat = deleteStat.replace("$negativeamountflag", "" + RecordToDelete.getnegativeamountflag());
        deleteStat = deleteStat.replace("$adddecimal", "" + RecordToDelete.getadddecimal());
        deleteStat = deleteStat.replace("$decimalpos", "" + RecordToDelete.getdecimalpos());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table HOST_FILE_TEMPLATE_DETAIL", false);
        return null;
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table HOST_FILE_TEMPLATE_DETAIL", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        hostfiletemplatedetailDTOInter RecordToSelect = (hostfiletemplatedetailDTOInter) obj[0];
        String selectStat = "Select ID,COLUMN_ID,TEMPLATE,POSITION,LINE_NUMBER,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LOAD_WHEN_MAPPED,LENGTH_DIR,STARTING_POSITION,TAG_STRING,MANDATORY,NEGATIVE_AMOUNT_FLAG,ADD_DECIMAL,DECIMAL_POS From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        hostfiletemplatedetailDTOInter SelectedRecord = DTOFactory.createhostfiletemplatedetailDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.settemplate(rs.getInt("TEMPLATE"));
            SelectedRecord.setposition(rs.getInt("POSITION"));
            SelectedRecord.setlinenumber(rs.getInt("LINE_NUMBER"));
            SelectedRecord.setformat(rs.getString("FORMAT"));
            SelectedRecord.setformat2(rs.getString("FORMAT2"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));
            SelectedRecord.setlength(rs.getInt("LENGTH"));
            SelectedRecord.setloadwhenmapped(rs.getInt("LOAD_WHEN_MAPPED"));
            SelectedRecord.setlengthdir(rs.getInt("LENGTH_DIR"));
            SelectedRecord.setstartingposition(rs.getInt("STARTING_POSITION"));
            SelectedRecord.settagstring(rs.getString("TAG_STRING"));
            SelectedRecord.setmandatory(rs.getInt("MANDATORY"));
            SelectedRecord.setnegativeamountflag(rs.getInt("NEGATIVE_AMOUNT_FLAG"));
            SelectedRecord.setadddecimal(rs.getInt("ADD_DECIMAL"));
            SelectedRecord.setdecimalpos(rs.getInt("DECIMAL_POS"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        hostfiletemplatedetailDTOInter RecordToSelect = (hostfiletemplatedetailDTOInter) obj[0];
        String selectStat = "Select ID,COLUMN_ID,TEMPLATE,POSITION,LINE_NUMBER,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LOAD_WHEN_MAPPED,LENGTH_DIR,STARTING_POSITION,TAG_STRING,MANDATORY,NEGATIVE_AMOUNT_FLAG,ADD_DECIMAL,DECIMAL_POS From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<hostfiletemplatedetailDTOInter> Records = new ArrayList<hostfiletemplatedetailDTOInter>();
        while (rs.next()) {
            hostfiletemplatedetailDTOInter SelectedRecord = DTOFactory.createhostfiletemplatedetailDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.settemplate(rs.getInt("TEMPLATE"));
            SelectedRecord.setposition(rs.getInt("POSITION"));
            SelectedRecord.setlinenumber(rs.getInt("LINE_NUMBER"));
            SelectedRecord.setformat(rs.getString("FORMAT"));
            SelectedRecord.setformat2(rs.getString("FORMAT2"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));
            SelectedRecord.setlength(rs.getInt("LENGTH"));
            SelectedRecord.setloadwhenmapped(rs.getInt("LOAD_WHEN_MAPPED"));
            SelectedRecord.setlengthdir(rs.getInt("LENGTH_DIR"));
            SelectedRecord.setstartingposition(rs.getInt("STARTING_POSITION"));
            SelectedRecord.settagstring(rs.getString("TAG_STRING"));
            SelectedRecord.setmandatory(rs.getInt("MANDATORY"));
            SelectedRecord.setnegativeamountflag(rs.getInt("NEGATIVE_AMOUNT_FLAG"));
            SelectedRecord.setadddecimal(rs.getInt("ADD_DECIMAL"));
            SelectedRecord.setdecimalpos(rs.getInt("DECIMAL_POS"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,COLUMN_ID,TEMPLATE,POSITION,LINE_NUMBER,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LOAD_WHEN_MAPPED,LENGTH_DIR,STARTING_POSITION,TAG_STRING,MANDATORY,NEGATIVE_AMOUNT_FLAG,ADD_DECIMAL,DECIMAL_POS From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<hostfiletemplatedetailDTOInter> Records = new ArrayList<hostfiletemplatedetailDTOInter>();
        while (rs.next()) {
            hostfiletemplatedetailDTOInter SelectedRecord = DTOFactory.createhostfiletemplatedetailDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.settemplate(rs.getInt("TEMPLATE"));
            SelectedRecord.setposition(rs.getInt("POSITION"));
            SelectedRecord.setlinenumber(rs.getInt("LINE_NUMBER"));
            SelectedRecord.setformat(rs.getString("FORMAT"));
            SelectedRecord.setformat2(rs.getString("FORMAT2"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));
            SelectedRecord.setlength(rs.getInt("LENGTH"));
            SelectedRecord.setloadwhenmapped(rs.getInt("LOAD_WHEN_MAPPED"));
            SelectedRecord.setlengthdir(rs.getInt("LENGTH_DIR"));
            SelectedRecord.setstartingposition(rs.getInt("STARTING_POSITION"));
            SelectedRecord.settagstring(rs.getString("TAG_STRING"));
            SelectedRecord.setmandatory(rs.getInt("MANDATORY"));
            SelectedRecord.setnegativeamountflag(rs.getInt("NEGATIVE_AMOUNT_FLAG"));
            SelectedRecord.setadddecimal(rs.getInt("ADD_DECIMAL"));
            SelectedRecord.setdecimalpos(rs.getInt("DECIMAL_POS"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public void save(Object... obj) throws SQLException, Throwable {
        List<hostfiletemplatedetailDTOInter> entities = (List<hostfiletemplatedetailDTOInter>) obj[0];
        String insertStat = "insert into HOST_FILE_TEMPLATE_DETAIL (ID,COLUMN_ID,TEMPLATE,POSITION,LINE_NUMBER,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LOAD_WHEN_MAPPED,LENGTH_DIR,STARTING_POSITION,TAG_STRING,MANDATORY,NEGATIVE_AMOUNT_FLAG,ADD_DECIMAL,DECIMAL_POS) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        Connection connection = null;
        PreparedStatement statement = null;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.size(); i++) {
                hostfiletemplatedetailDTOInter RecordtoInsert = entities.get(i);
                statement.setInt(1, RecordtoInsert.getid());
                statement.setInt(2, RecordtoInsert.getcolumnid());
                statement.setInt(3, RecordtoInsert.gettemplate());
                statement.setInt(4, RecordtoInsert.getposition());
                statement.setInt(5, RecordtoInsert.getlinenumber());
                statement.setString(6, RecordtoInsert.getformat());
                statement.setString(7, RecordtoInsert.getformat2());
                statement.setInt(8, RecordtoInsert.getdatatype());
                statement.setInt(9, RecordtoInsert.getlength());
                statement.setInt(10, RecordtoInsert.getloadwhenmapped());
                statement.setInt(11, RecordtoInsert.getlengthdir());
                statement.setInt(12, RecordtoInsert.getstartingposition());
                statement.setString(13, RecordtoInsert.gettagstring());
                statement.setInt(14, RecordtoInsert.getmandatory());
                statement.setInt(15, RecordtoInsert.getnegativeamountflag());
                statement.setInt(16, RecordtoInsert.getadddecimal());
                statement.setInt(17, RecordtoInsert.getdecimalpos());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
             CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
    }
}
