package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class networksDAO extends BaseDAO implements networksDAOInter {

    protected networksDAO() {
        super();
        super.setTableName("NETWORKS");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        networksDTOInter RecordToInsert = (networksDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (ID,NAME) "
                + " values "
                + " ($id,'$name')";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$name", "" + RecordToInsert.getname());
        String msg = (String) super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To NETWORKS By Name " + RecordToInsert.getname(), false);
        return msg;
    }

    public Boolean ValidateNull(Object... obj) {
        networksDTOInter RecordToInsert = (networksDTOInter) obj[0];
        if (RecordToInsert.getid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getname() == null && RecordToInsert.getname().equals("")) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;

    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        networksDTOInter RecordToDelete = (networksDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        String msg = "";
        try {
            msg = (String) super.executeUpdate(deleteStat);
        } catch (SQLException ex) {
            msg = ex.getMessage();
        }
        super.postUpdate("Delete Record In Table NETWORKS by name " + RecordToDelete.getname(), false);
        return msg;
    }

    public Object findUserRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String User = (String) obj[0];
        String selectStat = "Select ID,NAME From $table Where ID IN (select u.networkid from user_network u where u.userid = $user) ORDER BY NAME";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$user", "" + User);
        ResultSet rs = executeQuery(selectStat);
        List<networksDTOInter> Records = new ArrayList<networksDTOInter>();
        while (rs.next()) {
            networksDTOInter SelectedRecord = DTOFactory.createnetworksDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,NAME From $table ORDER BY NAME";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<networksDTOInter> Records = new ArrayList<networksDTOInter>();
        while (rs.next()) {
            networksDTOInter SelectedRecord = DTOFactory.createnetworksDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String ValidateNullList(Object... obj) {
        List<networksDTOInter> AllRecords = (List<networksDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (networksDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getid() == 0) {
                Validate = Validate + " ," + String.valueOf(i);
            }
            if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {
                Validate = Validate + "," + String.valueOf(i);
            }
            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = "Record With Index " + Validate + " Contain Null Name Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<networksDTOInter> entities = (List<networksDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.isEmpty()) {
            mess = "No Columns Found To Be Updated";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "UPDATE networks SET NAME = ? WHERE ID = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        networksDTOInter RecordtoInsert = entities.get(i);
                        statement.setString(1, RecordtoInsert.getname());
                        statement.setInt(2, RecordtoInsert.getid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                            CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
