package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

public interface usersDAOInter {

    Object insertrecord(Object... obj) throws Throwable;//userd in user mangement

    Object updaterecord(Object... obj) throws Throwable;//Used In ChangePassword

    Object deleterecord(Object... obj) throws Throwable;//userd in user mangement

    Object deleteallrecord(Object... obj) throws Throwable;

    Object findRecord(Object... obj) throws Throwable;

    Object updatedashboard(Object... obj) throws Throwable;

    Object updatedashboardunselect(Object... obj) throws Throwable;

    Object findAllUnBlockRecords(Object... obj) throws Throwable;//Used In Block User 

    Object findRecordById(Object... obj) throws Throwable;//Used In ChangePassword

    Object GetAllAssignToProfile(Object... obj) throws Throwable;//Used In Profile Assign

    Object GetAllUnAssignToProfile(Object... obj) throws Throwable;//Used In Profile Assign

    Object findRecordsList(Object... obj) throws Throwable;

    Object findRecordsAll(Object... obj) throws Throwable;//Used In ChangePassword

    String save(Object... obj) throws SQLException, Throwable;//userd in user mangement

    Object updateStatus(Object... obj) throws Throwable;//used in login
    Boolean findRecordpass(Object... obj) throws Throwable;

    Object findAllBlockRecords(Object... obj) throws Throwable;//Used In Block User 

    Object findRecordsAlluserManag(Object... obj) throws Throwable;//userd in user mangement
}
