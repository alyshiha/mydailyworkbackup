package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

public interface transactionresponsecodeDAOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;
//used in system rules

    Object findRecordsAll(Object... obj) throws Throwable;

    Object findRecordsList(Object... obj) throws Throwable;

    Object findRecord(Object... obj) throws Throwable;//used in system rules

    String save(Object... obj) throws SQLException, Throwable;

    Object deleterecordByMaster(Object... obj) throws Throwable;
}
