package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import com.ev.Bingo.bus.dto.atmfileDTOInter;
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.duplicatetransactionsDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class duplicatetransactionsDAO extends BaseDAO implements duplicatetransactionsDAOInter {

    protected duplicatetransactionsDAO() {
        super();
        super.setTableName("DUPLICATE_TRANSACTIONS");
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        atmfileDTOInter RecordToSelect = (atmfileDTOInter) obj[0];
        String selectStat = "Select FILE_ID,LOADING_DATE,TRANSACTION_TYPE,TRANSACTION_TYPE_ID,CURRENCY,CURRENCY_ID,RESPONSE_CODE,RESPONSE_CODE_ID,TRANSACTION_DATE,TRANSACTION_SEQUENCE,CARD_NO,AMOUNT,SETTLEMENT_DATE,CUSTOMER_ACCOUNT_NUMBER,TRANSACTION_SEQUENCE_ORDER_BY,TRANSACTION_TIME,decode(RECORD_TYPE,1,'Network',2,'Switch',3,'network')RECORD_TYPE,COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5,TRANSACTION_TYPE_MASTER,RESPONSE_CODE_MASTER,CARD_NO_SUFFIX,"+super.GetDecryptCardNo()+" card_no_decrypt From DUPLICATE_TRANSACTIONS"
                + "    where file_id = $fileId ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$fileId", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<duplicatetransactionsDTOInter> Records = new ArrayList<duplicatetransactionsDTOInter>();
        while (rs.next()) {
            duplicatetransactionsDTOInter SelectedRecord = DTOFactory.createduplicatetransactionsDTO();
            SelectedRecord.setfileid(rs.getInt("FILE_ID"));
            SelectedRecord.setloadingdate(rs.getTimestamp("LOADING_DATE"));
            SelectedRecord.settransactiontype(rs.getString("TRANSACTION_TYPE"));
            SelectedRecord.settransactiontypeid(rs.getInt("TRANSACTION_TYPE_ID"));
            SelectedRecord.setcurrency(rs.getString("CURRENCY"));
            SelectedRecord.setcurrencyid(rs.getInt("CURRENCY_ID"));
            SelectedRecord.setresponsecode(rs.getString("RESPONSE_CODE"));
            SelectedRecord.setresponsecodeid(rs.getInt("RESPONSE_CODE_ID"));
            SelectedRecord.settransactiondate(rs.getTimestamp("TRANSACTION_DATE"));
            SelectedRecord.settransactionsequence(rs.getString("TRANSACTION_SEQUENCE"));
            SelectedRecord.setcardno(rs.getString("card_no_decrypt"));
            SelectedRecord.setamount(rs.getFloat("AMOUNT"));
            SelectedRecord.setsettlementdate(rs.getTimestamp("SETTLEMENT_DATE"));
            SelectedRecord.setcustomeraccountnumber(rs.getString("CUSTOMER_ACCOUNT_NUMBER"));
            SelectedRecord.settransactionsequenceorderby(rs.getInt("TRANSACTION_SEQUENCE_ORDER_BY"));
            SelectedRecord.settransactiontime(rs.getTimestamp("TRANSACTION_TIME"));
            SelectedRecord.setrecordtype(rs.getString("RECORD_TYPE"));
            SelectedRecord.setcolumn1(rs.getString("COLUMN1"));
            SelectedRecord.setcolumn2(rs.getString("COLUMN2"));
            SelectedRecord.setcolumn3(rs.getString("COLUMN3"));
            SelectedRecord.setcolumn4(rs.getString("COLUMN4"));
            SelectedRecord.setcolumn5(rs.getString("COLUMN5"));
            SelectedRecord.settransactiontypemaster(rs.getInt("TRANSACTION_TYPE_MASTER"));
            SelectedRecord.setresponsecodemaster(rs.getInt("RESPONSE_CODE_MASTER"));
            SelectedRecord.setcardnosuffix(rs.getString("CARD_NO_SUFFIX"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }
}
