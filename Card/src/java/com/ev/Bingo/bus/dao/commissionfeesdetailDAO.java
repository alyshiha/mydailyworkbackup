package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import com.ev.Bingo.bus.dto.commissionfeesDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.commissionfeesdetailDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class commissionfeesdetailDAO extends BaseDAO implements commissionfeesdetailDAOInter {

    protected commissionfeesdetailDAO() {
        super();
        super.setTableName("COMMISSION_FEES_DETAIL");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        commissionfeesdetailDTOInter RecordToInsert = (commissionfeesdetailDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (ID,COMMISSION_FEES_ID,TRANS_TYPE,COMM,FEES,CARD_TYPE) "
                + " values "
                + " ($id,$commissionfeesid,$transtype,$comm,$fees,$cardtype)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$commissionfeesid", "" + RecordToInsert.getcommissionfeesid());
        insertStat = insertStat.replace("$transtype", "" + RecordToInsert.gettranstype());
        insertStat = insertStat.replace("$comm", "" + RecordToInsert.getcomm());
        insertStat = insertStat.replace("$fees", "" + RecordToInsert.getfees());
        insertStat = insertStat.replace("$cardtype", "" + RecordToInsert.getcardtype());
        String msg = super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To COMMISSION_FEES_DETAIL", false);
        return msg + " Record Has Been Inserted";
    }

    public Boolean ValidateNull(Object... obj) {
        commissionfeesdetailDTOInter RecordToInsert = (commissionfeesdetailDTOInter) obj[0];
        if (RecordToInsert.getid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getcommissionfeesid() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;

    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        commissionfeesdetailDTOInter RecordToUpdate = (commissionfeesdetailDTOInter) obj[0];
        String UpdateStat = "update $table set "
                + " (ID= $id,COMMISSION_FEES_ID= $commissionfeesid,TRANS_TYPE= $transtype,COMM= $comm,FEES= $fees,CARD_TYPE= $cardtype) "
                + "  where  ID= $id";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$id", "" + RecordToUpdate.getid());
        UpdateStat = UpdateStat.replace("$commissionfeesid", "" + RecordToUpdate.getcommissionfeesid());
        UpdateStat = UpdateStat.replace("$transtype", "" + RecordToUpdate.gettranstype());
        UpdateStat = UpdateStat.replace("$comm", "" + RecordToUpdate.getcomm());
        UpdateStat = UpdateStat.replace("$fees", "" + RecordToUpdate.getfees());
        UpdateStat = UpdateStat.replace("$cardtype", "" + RecordToUpdate.getcardtype());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table COMMISSION_FEES_DETAIL", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        commissionfeesdetailDTOInter RecordToDelete = (commissionfeesdetailDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        deleteStat = deleteStat.replace("$commissionfeesid", "" + RecordToDelete.getcommissionfeesid());
        deleteStat = deleteStat.replace("$transtype", "" + RecordToDelete.gettranstype());
        deleteStat = deleteStat.replace("$comm", "" + RecordToDelete.getcomm());
        deleteStat = deleteStat.replace("$fees", "" + RecordToDelete.getfees());
        deleteStat = deleteStat.replace("$cardtype", "" + RecordToDelete.getcardtype());
        String msg = super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table COMMISSION_FEES_DETAIL", false);
        return msg + " Record Has Been Deleted";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        commissionfeesDTOInter record = (commissionfeesDTOInter) obj[0];
        super.preUpdate();
        String deleteStat = "delete from $table where COMMISSION_FEES_ID = $id";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$id", "" + record.getid());
        String msg = super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table COMMISSION_FEES_DETAIL", false);
        return msg + "  Record Has Been Deleted";
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        commissionfeesdetailDTOInter RecordToSelect = (commissionfeesdetailDTOInter) obj[0];
        String selectStat = "Select ID,COMMISSION_FEES_ID,TRANS_TYPE,COMM,FEES,CARD_TYPE From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        commissionfeesdetailDTOInter SelectedRecord = DTOFactory.createcommissionfeesdetailDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setcommissionfeesid(rs.getInt("COMMISSION_FEES_ID"));
            SelectedRecord.settranstype(rs.getInt("TRANS_TYPE"));
            SelectedRecord.setcomm(rs.getInt("COMM"));
            SelectedRecord.setfees(rs.getInt("FEES"));
            SelectedRecord.setcardtype(rs.getInt("CARD_TYPE"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        commissionfeesDTOInter RecordToSelect = (commissionfeesDTOInter) obj[0];
        String selectStat = "Select ID,COMMISSION_FEES_ID,TRANS_TYPE,COMM,FEES,CARD_TYPE From $table"
                + "  where  COMMISSION_FEES_ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<commissionfeesdetailDTOInter> Records = new ArrayList<commissionfeesdetailDTOInter>();
        while (rs.next()) {
            commissionfeesdetailDTOInter SelectedRecord = DTOFactory.createcommissionfeesdetailDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setcommissionfeesid(rs.getInt("COMMISSION_FEES_ID"));
            SelectedRecord.settranstype(rs.getInt("TRANS_TYPE"));
            SelectedRecord.setcomm(rs.getFloat("COMM"));
            SelectedRecord.setfees(rs.getFloat("FEES"));
            SelectedRecord.setcardtype(rs.getInt("CARD_TYPE"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,COMMISSION_FEES_ID,TRANS_TYPE,COMM,FEES,CARD_TYPE From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<commissionfeesdetailDTOInter> Records = new ArrayList<commissionfeesdetailDTOInter>();
        while (rs.next()) {
            commissionfeesdetailDTOInter SelectedRecord = DTOFactory.createcommissionfeesdetailDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setcommissionfeesid(rs.getInt("COMMISSION_FEES_ID"));
            SelectedRecord.settranstype(rs.getInt("TRANS_TYPE"));
            SelectedRecord.setcomm(rs.getInt("COMM"));
            SelectedRecord.setfees(rs.getInt("FEES"));
            SelectedRecord.setcardtype(rs.getInt("CARD_TYPE"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<commissionfeesdetailDTOInter> entities = (List<commissionfeesdetailDTOInter>) obj[0];
        String insertStat = "Update COMMISSION_FEES_DETAIL set TRANS_TYPE = ?,COMM = ?,FEES = ?,CARD_TYPE =? where ID = ?";
        Connection connection = null;
        PreparedStatement statement = null;
        String mess = "", mess2 = "";
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
           connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.size(); i++) {
                commissionfeesdetailDTOInter RecordtoInsert = entities.get(i);
                statement.setInt(1, RecordtoInsert.gettranstype());
                statement.setFloat(2, RecordtoInsert.getcomm());
                statement.setFloat(3, RecordtoInsert.getfees());
                statement.setInt(4, RecordtoInsert.getcardtype());
                statement.setInt(5, RecordtoInsert.getid());
               statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            try {
                int[] numUpdates = statement.executeBatch();
                Integer valid = 0, notvalid = 0;

                for (int i = 0; i < numUpdates.length; i++) {
                    if (numUpdates[i] == -2) {
                        valid++;
                        mess = valid + " rows has been updated";
                    } else {
                        notvalid++;
                        mess2 = notvalid + " can`t be updated";
                    }
                }
                if (!mess2.equals("")) {
                    mess = mess + "," + mess2;
                }
            } catch (Exception ex) {
                mess = "Transaction Type can't be Duplicated";
            }
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
              CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
        return mess;
    }
}
