/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

/**
 *
 * @author AlyShiha
 */
public interface matchingtypeDAOInter {

    Object RecreateIndex(Object... obj) throws Throwable;

    Object deleterecordmatchingtypenetworks(Object... obj) throws Throwable;

    Object deleterecordmatchingtypesetting(Object... obj) throws Throwable;

    Object findAllmatchingtype(Object... obj) throws Throwable;

    Object findRecordsListmatchingtypenetworks(Object... obj) throws Throwable;

    Object findRecordsListmatchingtypesetting(Object... obj) throws Throwable;

    Object insertrecordmatchingtypenetworks(Object... obj) throws Throwable;

    Object insertrecordmatchingtypesetting(Object... obj) throws Throwable;

    Object savematchingtypenetworks(Object... obj) throws SQLException, Throwable;

    Object savematchingtypesetting(Object... obj) throws SQLException, Throwable;

    Object updaterecordmatchingtype(Object... obj) throws Throwable;

}
