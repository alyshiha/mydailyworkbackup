package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

public interface currencyDAOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object findRecordsList(Object... obj) throws Throwable;

    Object findRecordsListCurrency(Object... obj) throws Throwable;

    String save(Object... obj) throws SQLException, Throwable;
}
