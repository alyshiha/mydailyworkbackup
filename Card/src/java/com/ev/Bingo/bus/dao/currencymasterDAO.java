package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.currencymasterDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class currencymasterDAO extends BaseDAO implements currencymasterDAOInter {

    protected currencymasterDAO() {
        super();
        super.setTableName("CURRENCY_MASTER");
    }

    //used in system rules

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        currencymasterDTOInter RecordToSelect = (currencymasterDTOInter) obj[0];
        String selectStat = "Select ID,NAME,SYMBOL,DEFAULT_CURRENCY From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        currencymasterDTOInter SelectedRecord = DTOFactory.createcurrencymasterDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setsymbol(rs.getString("SYMBOL"));
            SelectedRecord.setdefaultcurrency(rs.getInt("DEFAULT_CURRENCY"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public String ValidateNullDel(Object... obj) {
        currencymasterDTOInter RecordToInsert = (currencymasterDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getid() == 0) {
            Validate = Validate + " record id  ";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required field";
        }
        return Validate;
    }

    public String ValidateNullList(Object... obj) {
        List<currencymasterDTOInter> AllRecords = (List<currencymasterDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (currencymasterDTOInter RecordToInsert : AllRecords) {

            if (RecordToInsert.getid() == 0) {
                Validate = Validate + "   id  ";
            }
            if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {

                Validate = Validate + "," + String.valueOf(i);
            }
            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");

            Validate = "Record With Index " + Validate + " Contain Null Name Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    public String ValidateNull(Object... obj) {
        currencymasterDTOInter RecordToInsert = (currencymasterDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getid() == 0) {
            Validate = Validate + "   id  ";
        }
        if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {
            Validate = Validate + "   Name  ";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required field";
        }
        return Validate;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<currencymasterDTOInter> entities = (List<currencymasterDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Users Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "update CURRENCY_MASTER set NAME = ? ,SYMBOL = ? ,DEFAULT_CURRENCY = ? Where ID = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                   connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        currencymasterDTOInter RecordtoInsert = entities.get(i);

                        statement.setString(1, RecordtoInsert.getname());
                        statement.setString(2, RecordtoInsert.getsymbol());
                        Boolean Temp = RecordtoInsert.getdefaultcurrencyB();
                        if (Temp == Boolean.TRUE) {
                            statement.setInt(3, 1);
                        } else if (Temp == Boolean.FALSE) {
                            statement.setInt(3, 2);
                        }
                        statement.setInt(4, RecordtoInsert.getid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                   CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,NAME,SYMBOL,DEFAULT_CURRENCY From $table ORDER BY SYMBOL" ;
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<currencymasterDTOInter> Records = new ArrayList<currencymasterDTOInter>();
        while (rs.next()) {
            currencymasterDTOInter SelectedRecord = DTOFactory.createcurrencymasterDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setsymbol(rs.getString("SYMBOL"));
            SelectedRecord.setdefaultcurrency(rs.getInt("DEFAULT_CURRENCY"));
            if (SelectedRecord.getdefaultcurrency() == 1) {
                SelectedRecord.setdefaultcurrencyB(Boolean.TRUE);
            } else if (SelectedRecord.getdefaultcurrency() == 2) {
                SelectedRecord.setdefaultcurrencyB(Boolean.FALSE);
            }
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object insertrecord(Object... obj) throws Throwable {

        currencymasterDTOInter RecordToInsert = (currencymasterDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
            super.preUpdate();
            String insertStat = "insert into $table"
                    + " (ID,NAME,SYMBOL,DEFAULT_CURRENCY) "
                    + " values "
                    + " ($id,'$name','$symbol',$defaultcurrency)";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
            insertStat = insertStat.replace("$name", "" + RecordToInsert.getname());
            insertStat = insertStat.replace("$symbol", "" + RecordToInsert.getsymbol());
            Integer Temp = 2;
            if (RecordToInsert.getdefaultcurrencyB() == Boolean.TRUE) {
                Temp = 1;
            } else if (RecordToInsert.getdefaultcurrencyB() == Boolean.FALSE) {
                Temp = 2;
            }
            insertStat = insertStat.replace("$defaultcurrency", "" + Temp);
            msg = super.executeUpdate(insertStat);
            msg = msg + " Record Has Been Inserted";
            super.postUpdate("Adding New CURRENCY Record With Name " + RecordToInsert.getname(), false);
        }
        return msg;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        currencymasterDTOInter RecordToDelete = (currencymasterDTOInter) obj[0];
        String msg = ValidateNullDel(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  ID= $id";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
            deleteStat = deleteStat.replace("$name", "" + RecordToDelete.getname());
            deleteStat = deleteStat.replace("$symbol", "" + RecordToDelete.getsymbol());
            deleteStat = deleteStat.replace("$defaultcurrency", "" + RecordToDelete.getdefaultcurrency());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " currency With ";
            super.postUpdate("Deleting A CURRENCY With Name " + RecordToDelete.getname(), false);
        }
        return msg;
    }
}
