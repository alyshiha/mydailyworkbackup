package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.profileDTOInter;
import com.ev.Bingo.bus.dto.profilemenuDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class profileDAO extends BaseDAO implements profileDAOInter {

    protected profileDAO() {
        super();
        super.setTableName("PROFILE");
    }

    public Object insertmenuitem(Object... obj) throws Throwable {

        profilemenuDTOInter RecordToInsert = (profilemenuDTOInter) obj[0];
        String msg = ValidateNullInsertMenuItem(RecordToInsert);
        if (msg.equals("Validate")) {
            super.preUpdate();
            String insertStat = " insert into profile_menu  values ($profileid, $menuid)";

            insertStat = insertStat.replace("$profileid", "" + RecordToInsert.getprofileid());
            insertStat = insertStat.replace("$menuid", "" + RecordToInsert.getmenuid());
            msg = super.executeUpdate(insertStat);
            msg = msg + " Page Has Been Assigned";
            super.postUpdate("Adding New Page To With ID " + RecordToInsert.getmenuid() + " & Profile " + RecordToInsert.getprofileid(), false);
        }
        return msg;
    }

    public Object insertrecord(Object... obj) throws Throwable {

        profileDTOInter RecordToInsert = (profileDTOInter) obj[0];
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
            super.preUpdate();
            RecordToInsert.setprofileid(super.generateSequence(super.getTableName()));

            String insertStat = "insert into $table"
                    + " (PROFILE_ID,PROFILE_NAME) "
                    + " values "
                    + " ($profileid,'$profilename')";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$profileid", "" + RecordToInsert.getprofileid());
            insertStat = insertStat.replace("$profilename", "" + RecordToInsert.getprofilename());

            msg = super.executeUpdate(insertStat);
            msg = msg + " Profile Has Been Inserted";
            super.postUpdate("Adding New Profile Record With Name " + RecordToInsert.getprofilename(), false);
        }
        return msg;
    }

    public String ValidateNull(Object... obj) {
        profileDTOInter RecordToInsert = (profileDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getprofilename() == null ? "" == null : RecordToInsert.getprofilename().equals("")) {
            Validate = " ProfileName  ";
        }
        if (!Validate.equals("Validate")) {
            Validate.replace("Validate", "");
        }
        return Validate;

    }

    public String ValidateNullInsertMenuItem(Object... obj) {
        profilemenuDTOInter RecordToInsert = (profilemenuDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getprofileid() == 0) {
            Validate = "Select A Profile First";
        }
        if (RecordToInsert.getmenuid() == 0) {
            Validate = " Field Item Menu  ";
        }
        if (!Validate.equals("Validate")) {
            Validate.replace("Validate", "");
        }
        return Validate;

    }

    public String ValidateNullList(Object... obj) {
        List<profileDTOInter> AllRecords = (List<profileDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (profileDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getprofilename() == null ? "" == null : RecordToInsert.getprofilename().equals("")) {
                Validate = Validate + "," + String.valueOf(i);
            }
            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = "Record With Index " + Validate + " Contain Null Card Number Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;

    }

    public Object deleterecord(Object... obj) throws Throwable {
        profileDTOInter RecordToDelete = (profileDTOInter) obj[0];
        String msg = ValidateNull(RecordToDelete);
        if (msg.equals("Validate")) {
            super.preUpdate();

            String deleteStat = "delete from $table "
                    + "  where  PROFILE_ID= $profileid";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$profileid", "" + RecordToDelete.getprofileid());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " Profile Has Been Deleted";
            super.postUpdate("Deleting A Profile With Name " + RecordToDelete.getprofilename(), false);
        }
        return msg;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select PROFILE_ID,PROFILE_NAME From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<profileDTOInter> Records = new ArrayList<profileDTOInter>();
        while (rs.next()) {
            profileDTOInter SelectedRecord = DTOFactory.createprofileDTO();
            SelectedRecord.setprofileid(rs.getInt("PROFILE_ID"));
            SelectedRecord.setprofilename(rs.getString("PROFILE_NAME"));

            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {

        List<profileDTOInter> entities = (List<profileDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Users Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "update PROFILE set PROFILE_NAME = ? where PROFILE_ID = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        profileDTOInter RecordtoInsert = entities.get(i);
                        statement.setString(1, RecordtoInsert.getprofilename());

                        statement.setInt(2, RecordtoInsert.getprofileid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " Profile Has Been Saved";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }

                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                         CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
