package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

public interface profileDAOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object findRecordsAll(Object... obj) throws Throwable;

    String save(Object... obj) throws SQLException, Throwable;

    Object insertmenuitem(Object... obj) throws Throwable;
}
