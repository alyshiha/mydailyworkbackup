/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import java.util.Date;

/**
 *
 * @author shi7a
 */
public interface settlementsDAOInter {

    Object findRecordsAcquier(Date dateFrom, Date dateTo) throws Throwable;

    Object findRecordsIssuer(Date dateFrom, Date dateTo) throws Throwable;
  
    Object findRecordstimefromswitch(Date dateFrom, Date dateTo) throws Throwable;
}
