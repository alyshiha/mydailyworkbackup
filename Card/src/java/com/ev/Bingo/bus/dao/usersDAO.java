package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;

import com.ev.Bingo.bus.dto.userprofileDTOInter;
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class usersDAO extends BaseDAO implements usersDAOInter {

    protected usersDAO() {
        super();
        super.setTableName("USERS");
    }

    //userd in user mangement
    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        usersDTOInter RecordToInsert = (usersDTOInter) obj[0];
        RecordToInsert.setUserid(super.generateSequence(super.getTableName()));
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
            String insertStat = "insert into $table"
                    + " (USER_ID,USER_NAME,USER_A_NAME,LOGON_NAME,USER_PASSWORD,SHOW_NOTIFICATIONS,SHOW_ERRORS,SHOW_ONLINE_USER,show_export,show_corrective_entry) "
                    + " values "
                    + " ($userid,'$username','$useraname','$logonname',conv.conv_string('$userpassword'),$shownotifications,$showerrors,$showonlineuser,$showexport,$showcorrectiveentry)";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$userid", "" + RecordToInsert.getUserid());
            insertStat = insertStat.replace("$username", "" + RecordToInsert.getUsername());
            insertStat = insertStat.replace("$useraname", "" + RecordToInsert.getUsername());
            insertStat = insertStat.replace("$logonname", "" + RecordToInsert.getLogonname());
            insertStat = insertStat.replace("$userpassword", "" + RecordToInsert.getUserpassword());
            insertStat = insertStat.replace("$shiftid", "" + RecordToInsert.getShiftid());
            insertStat = insertStat.replace("$emrholidayenabled", "" + RecordToInsert.getEmrholidayenabled());
            insertStat = insertStat.replace("$disputegraph", "" + RecordToInsert.getDisputegraph());
            insertStat = insertStat.replace("$shownewatms", "" + RecordToInsert.getShownewatms());
            insertStat = insertStat.replace("$visible", "" + RecordToInsert.getVisible());
            insertStat = insertStat.replace("$seeunvisible", "" + RecordToInsert.getSeeunvisible());
            insertStat = insertStat.replace("$changeotheruserpassword", "" + RecordToInsert.getChangeotheruserpassword());
            insertStat = insertStat.replace("$restrected", "" + RecordToInsert.getRestrected());
            insertStat = insertStat.replace("$seerestrected", "" + RecordToInsert.getSeerestrected());
            insertStat = insertStat.replace("$status", "" + RecordToInsert.getStatus());
            insertStat = insertStat.replace("$showpendingatms", "" + RecordToInsert.getShowpendingatms());
            insertStat = insertStat.replace("$shownotifications", "" + RecordToInsert.getShownotifications());
            insertStat = insertStat.replace("$showerrors", "" + RecordToInsert.getShowerrors());
            insertStat = insertStat.replace("$showonlineuser", "" + RecordToInsert.getShowonlineuser());
            insertStat = insertStat.replace("$showexport", "" + RecordToInsert.getShowexport());
            insertStat = insertStat.replace("$showcorrectiveentry", "" + RecordToInsert.getShowcorrectiveentry());
            msg = super.executeUpdate(insertStat);
            msg = msg + " new user has Been created ";
            userprofileDAOInter UPDAO = DAOFactory.createuserprofileDAO();
            userprofileDTOInter UPDTO = DTOFactory.createuserprofileDTO();
            UPDTO.setuserid(RecordToInsert.getUserid());
            UPDTO.setprofileid(RecordToInsert.getProfileid());
            msg = msg + (String) UPDAO.insertrecord(UPDTO);
            super.postUpdate("Adding New User Record With Name " + RecordToInsert.getUsername(), false);
        }

        return msg;
    }

    public String ValidateNulluser(Object... obj) {
        usersDTOInter RecordToInsert = (usersDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getUserid() == 0) {
            Validate = Validate + " Feild user name is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate.replace("Validate", "");
        }
        return Validate;
    }

    public String ValidateNullList(Object... obj) {
        List<usersDTOInter> AllRecords = (List<usersDTOInter>) obj[0];
        String Validate = "Validate";
        for (usersDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getUserid() == 0) {
                Validate = Validate + " Feild userid is a requiered field";
            }
            if (RecordToInsert.getUsername() == null || RecordToInsert.getUsername().equals("")) {
                Validate = Validate + " Feild username is a requiered field";
            }
            if (RecordToInsert.getLogonname() == null || RecordToInsert.getLogonname().equals("")) {
                Validate = Validate + " Feild logonname is a requiered field";
            }
            if (RecordToInsert.getUserpassword() == null || RecordToInsert.getUserpassword().equals("")) {
                Validate = Validate + " Feild Password is a requiered field";
            }

        }
        if (!Validate.equals("Validate")) {
            Validate.replace("Validate", "");
        }

        return Validate;
    }

    public String ValidateNull(Object... obj) {
        usersDTOInter RecordToInsert = (usersDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getUserid() == 0) {
            Validate = Validate + " Feild userid is a requiered field";
        }
        if (RecordToInsert.getUsername() == null || RecordToInsert.getUsername().equals("")) {
            Validate = Validate + " Feild username is a requiered field";
        }
        if (RecordToInsert.getLogonname() == null || RecordToInsert.getLogonname().equals("")) {
            Validate = Validate + " Feild logonname is a requiered field";
        }
        if (RecordToInsert.getUserpassword() == null || RecordToInsert.getUserpassword().equals("")) {
            Validate = Validate + " Feild Password is a requiered field";
        }

        if (!Validate.equals("Validate")) {
            Validate.replace("Validate", "");
        }
        return Validate;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        usersDTOInter RecordToUpdate = (usersDTOInter) obj[0];
        String msg = ValidateNull(RecordToUpdate);
        if (msg.equals("Validate")) {
            String UpdateStat = "update $table set "
                    + " USER_ID= $userid,user_password = conv.conv_string('$userpassword'),USER_NAME= '$username',USER_LANG= $userlang,LOGON_NAME= '$logonname',STATUS= $status "
                    + "  where  USER_ID= $userid";
            UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
            UpdateStat = UpdateStat.replace("$userid", "" + RecordToUpdate.getUserid());
            UpdateStat = UpdateStat.replace("$username", "" + RecordToUpdate.getUsername());
            UpdateStat = UpdateStat.replace("$useraname", "" + RecordToUpdate.getUseraname());
            UpdateStat = UpdateStat.replace("$userlang", "" + RecordToUpdate.getUserlang());
            UpdateStat = UpdateStat.replace("$holidayenabled", "" + RecordToUpdate.getHolidayenabled());
            UpdateStat = UpdateStat.replace("$logonname", "" + RecordToUpdate.getLogonname());
            UpdateStat = UpdateStat.replace("$userpassword", "" + RecordToUpdate.getUserpassword());
            UpdateStat = UpdateStat.replace("$shiftid", "" + RecordToUpdate.getShiftid());
            UpdateStat = UpdateStat.replace("$emrholidayenabled", "" + RecordToUpdate.getEmrholidayenabled());
            UpdateStat = UpdateStat.replace("$disputegraph", "" + RecordToUpdate.getDisputegraph());
            UpdateStat = UpdateStat.replace("$shownewatms", "" + RecordToUpdate.getShownewatms());
            UpdateStat = UpdateStat.replace("$visible", "" + RecordToUpdate.getVisible());
            UpdateStat = UpdateStat.replace("$seeunvisible", "" + RecordToUpdate.getSeeunvisible());
            UpdateStat = UpdateStat.replace("$changeotheruserpassword", "" + RecordToUpdate.getChangeotheruserpassword());
            UpdateStat = UpdateStat.replace("$restrected", "" + RecordToUpdate.getRestrected());
            UpdateStat = UpdateStat.replace("$seerestrected", "" + RecordToUpdate.getSeerestrected());
            UpdateStat = UpdateStat.replace("$status", "" + RecordToUpdate.getStatus());
            UpdateStat = UpdateStat.replace("$showpendingatms", "" + RecordToUpdate.getShowpendingatms());
            UpdateStat = UpdateStat.replace("$shownotifications", "" + RecordToUpdate.getShownotifications());
            UpdateStat = UpdateStat.replace("$showerrors", "" + RecordToUpdate.getShowerrors());
            UpdateStat = UpdateStat.replace("$showonlineuser", "" + RecordToUpdate.getShowonlineuser());
            UpdateStat = UpdateStat.replace("$showexport", "" + RecordToUpdate.getShowexport());
            UpdateStat = UpdateStat.replace("$showcorrectiveentry", "" + RecordToUpdate.getShowcorrectiveentry());
            msg = super.executeUpdate(UpdateStat);
            msg = msg + " Row Has Been Inserted";
            super.postUpdate("Update User Password Of User " + RecordToUpdate.getUsername(), false);
        }
        ;
        return msg;
    }

    public Object updatedashboardunselect(Object... obj) throws Throwable {
        super.preUpdate();
        usersDTOInter RecordToUpdate = (usersDTOInter) obj[0];
        String msg = ValidateNull(RecordToUpdate);
        if (msg.equals("Validate")) {
            String UpdateStat = "update $table set "
                    + " SHOW_DASHBBOARD = sysdate-1"
                    + "  where  USER_ID= $userid";
            UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
            UpdateStat = UpdateStat.replace("$userid", "" + RecordToUpdate.getUserid());
            msg = super.executeUpdate(UpdateStat);
            super.postUpdate("Update Record In Table USERS", false);
        }
        return msg;
    }

    public Object updatedashboard(Object... obj) throws Throwable {
        super.preUpdate();
        usersDTOInter RecordToUpdate = (usersDTOInter) obj[0];
        String msg = ValidateNull(RecordToUpdate);
        if (msg.equals("Validate")) {
            String UpdateStat = "update $table set "
                    + " SHOW_DASHBBOARD = sysdate"
                    + "  where  USER_ID= $userid";
            UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
            UpdateStat = UpdateStat.replace("$userid", "" + RecordToUpdate.getUserid());
            msg = super.executeUpdate(UpdateStat);
            super.postUpdate("Update Record In Table USERS", false);
        }
        return msg;
    }

    public Object updateStatus(Object... obj) throws Throwable {
        super.preUpdate();
        String RecordToUpdate = (String) obj[0];

        String UpdateStat = "update $table set "
                + " LOCKSTATUS = 1 "
                + "  where  upper(t.logon_name)= upper($userid)";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$userid", "" + RecordToUpdate);
        super.executeUpdate(UpdateStat);
        super.postUpdate("User " + RecordToUpdate + " Has Been Locked", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        usersDTOInter RecordToDelete = (usersDTOInter) obj[0];
        String msg = ValidateNulluser(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  USER_ID= $userid";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$userid", "" + RecordToDelete.getUserid());
            try {
                msg = super.executeUpdate(deleteStat);
            } catch (Exception ex) {
                msg = "This User Did Action(s), Can't Be Deleted";
                return msg;
            }
            msg = msg + " Record Has Been deleted";
            super.postUpdate("Deleting A User With Name " + RecordToDelete.getUsername(), false);
        }
        return msg;
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table USERS", false);
        return null;
    }

    public Boolean findRecordpass(Object... obj) throws Throwable {
        super.preSelect();
        usersDTOInter RecordToSelect = (usersDTOInter) obj[0];
        String Password = (String) obj[1];
        Boolean valid = Boolean.FALSE;
        String selectStat = "Select USER_ID,USER_NAME,USER_A_NAME,USER_LANG,HOLIDAY_ENABLED,LOGON_NAME,USER_PASSWORD,SHIFT_ID,EMR_HOLIDAY_ENABLED,DISPUTE_GRAPH,VISIBLE,SEE_UNVISIBLE,CHANGE_OTHER_USER_PASSWORD,RESTRECTED,SEE_RESTRECTED,STATUS,SHOW_NOTIFICATIONS,SHOW_ERRORS,SHOW_ONLINE_USER,SHOW_EXPORT,SHOW_CORRECTIVE_ENTRY  From $table"
                + "  where USER_PASSWORD = conv.conv_string('$userpassword')  and USER_ID = $USERID";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$USERID", "" + RecordToSelect.getUserid());
        selectStat = selectStat.replace("$userpassword", "" + Password);
        ResultSet rs = executeQuery(selectStat);
        usersDTOInter SelectedRecord = DTOFactory.createusersDTO();
        while (rs.next()) {
            valid = Boolean.TRUE;
        }
        super.postSelect(rs);
        return valid;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        usersDTOInter RecordToSelect = (usersDTOInter) obj[0];
        String selectStat = "Select USER_ID,USER_NAME,USER_A_NAME,USER_LANG,HOLIDAY_ENABLED,LOGON_NAME,USER_PASSWORD,SHIFT_ID,EMR_HOLIDAY_ENABLED,DISPUTE_GRAPH,VISIBLE,SEE_UNVISIBLE,CHANGE_OTHER_USER_PASSWORD,RESTRECTED,SEE_RESTRECTED,STATUS,SHOW_NOTIFICATIONS,SHOW_ERRORS,SHOW_ONLINE_USER,SHOW_EXPORT,SHOW_CORRECTIVE_ENTRY  From $table"
                + "  where upper(LOGON_NAME)= upper('$USERNAME')  ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$USERNAME", "" + RecordToSelect.getUsername());
        ResultSet rs = executeQuery(selectStat);
        usersDTOInter SelectedRecord = DTOFactory.createusersDTO();
        while (rs.next()) {
            SelectedRecord.setUserid(rs.getInt("USER_ID"));
            SelectedRecord.setUsername(rs.getString("USER_NAME"));
            SelectedRecord.setUseraname(rs.getString("USER_A_NAME"));
            SelectedRecord.setUserlang(rs.getInt("USER_LANG"));
            SelectedRecord.setHolidayenabled(rs.getInt("HOLIDAY_ENABLED"));
            SelectedRecord.setLogonname(rs.getString("LOGON_NAME"));
            SelectedRecord.setUserpassword(rs.getString("USER_PASSWORD"));
            SelectedRecord.setShiftid(rs.getInt("SHIFT_ID"));
            SelectedRecord.setEmrholidayenabled(rs.getInt("EMR_HOLIDAY_ENABLED"));
            SelectedRecord.setDisputegraph(rs.getInt("DISPUTE_GRAPH"));
            //SelectedRecord.setShowdashboard(rs.getString("SHOW_DASHBBOARD"));
            SelectedRecord.setVisible(rs.getInt("VISIBLE"));
            SelectedRecord.setSeeunvisible(rs.getInt("SEE_UNVISIBLE"));
            SelectedRecord.setChangeotheruserpassword(rs.getInt("CHANGE_OTHER_USER_PASSWORD"));
            SelectedRecord.setRestrected(rs.getInt("RESTRECTED"));
            SelectedRecord.setSeerestrected(rs.getInt("SEE_RESTRECTED"));
            SelectedRecord.setStatus(rs.getInt("STATUS"));

            SelectedRecord.setShownotifications(rs.getInt("SHOW_NOTIFICATIONS"));
            SelectedRecord.setShowerrors(rs.getInt("SHOW_ERRORS"));
            SelectedRecord.setShowonlineuser(rs.getInt("SHOW_ONLINE_USER"));
            SelectedRecord.setShowexport(rs.getInt("SHOW_EXPORT"));
            SelectedRecord.setShowcorrectiveentry(rs.getInt("SHOW_CORRECTIVE_ENTRY"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    //Used In ChangePassword
    public Object findRecordById(Object... obj) throws Throwable {
        super.preSelect();
        usersDTOInter RecordToSelect = (usersDTOInter) obj[0];
        String selectStat = "Select USER_ID,USER_NAME,USER_A_NAME,USER_LANG,HOLIDAY_ENABLED,LOGON_NAME,USER_PASSWORD,STATUS From $table"
                + "  where USER_ID= $USERID  and restrected <> 1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$USERID", "" + RecordToSelect.getUserid());
        ResultSet rs = executeQuery(selectStat);
        usersDTOInter SelectedRecord = DTOFactory.createusersDTO();
        while (rs.next()) {
            SelectedRecord.setUserid(rs.getInt("USER_ID"));
            SelectedRecord.setUsername(rs.getString("USER_NAME"));
            SelectedRecord.setUseraname(rs.getString("USER_A_NAME"));
            SelectedRecord.setUserlang(rs.getInt("USER_LANG"));
            SelectedRecord.setHolidayenabled(rs.getInt("HOLIDAY_ENABLED"));
            SelectedRecord.setLogonname(rs.getString("LOGON_NAME"));
            SelectedRecord.setUserpassword(rs.getString("USER_PASSWORD"));
            SelectedRecord.setStatus(rs.getInt("STATUS"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        usersDTOInter RecordToSelect = (usersDTOInter) obj[0];
        String selectStat = "Select USER_ID,USER_NAME,USER_A_NAME,USER_LANG,HOLIDAY_ENABLED,LOGON_NAME,USER_PASSWORD,SHIFT_ID,EMR_HOLIDAY_ENABLED,DISPUTE_GRAPH,VISIBLE,SEE_UNVISIBLE,CHANGE_OTHER_USER_PASSWORD,RESTRECTED,SEE_RESTRECTED,STATUS,SHOW_NOTIFICATIONS,SHOW_ERRORS,SHOW_ONLINE_USER,SHOW_EXPORT,SHOW_CORRECTIVE_ENTRY From $table"
                + "  where  USER_ID= $userid  and restrected <> 1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$userid", "" + RecordToSelect.getUserid());
        ResultSet rs = executeQuery(selectStat);
        List<usersDTOInter> Records = new ArrayList<usersDTOInter>();
        while (rs.next()) {
            usersDTOInter SelectedRecord = DTOFactory.createusersDTO();
            SelectedRecord.setUserid(rs.getInt("USER_ID"));
            SelectedRecord.setUsername(rs.getString("USER_NAME"));
            SelectedRecord.setUseraname(rs.getString("USER_A_NAME"));
            SelectedRecord.setUserlang(rs.getInt("USER_LANG"));
            SelectedRecord.setHolidayenabled(rs.getInt("HOLIDAY_ENABLED"));
            SelectedRecord.setLogonname(rs.getString("LOGON_NAME"));
            SelectedRecord.setUserpassword(rs.getString("USER_PASSWORD"));
            SelectedRecord.setShiftid(rs.getInt("SHIFT_ID"));
            SelectedRecord.setEmrholidayenabled(rs.getInt("EMR_HOLIDAY_ENABLED"));
            SelectedRecord.setDisputegraph(rs.getInt("DISPUTE_GRAPH"));

            SelectedRecord.setVisible(rs.getInt("VISIBLE"));
            SelectedRecord.setSeeunvisible(rs.getInt("SEE_UNVISIBLE"));
            SelectedRecord.setChangeotheruserpassword(rs.getInt("CHANGE_OTHER_USER_PASSWORD"));
            SelectedRecord.setRestrected(rs.getInt("RESTRECTED"));
            SelectedRecord.setSeerestrected(rs.getInt("SEE_RESTRECTED"));
            SelectedRecord.setStatus(rs.getInt("STATUS"));

            SelectedRecord.setShownotifications(rs.getInt("SHOW_NOTIFICATIONS"));
            SelectedRecord.setShowerrors(rs.getInt("SHOW_ERRORS"));
            SelectedRecord.setShowonlineuser(rs.getInt("SHOW_ONLINE_USER"));
            SelectedRecord.setShowexport(rs.getInt("SHOW_EXPORT"));
            SelectedRecord.setShowcorrectiveentry(rs.getInt("SHOW_CORRECTIVE_ENTRY"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findAllBlockRecords(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select USER_ID,USER_NAME,USER_A_NAME,USER_LANG,HOLIDAY_ENABLED,LOGON_NAME,USER_PASSWORD,SHIFT_ID,EMR_HOLIDAY_ENABLED,DISPUTE_GRAPH,VISIBLE,SEE_UNVISIBLE,CHANGE_OTHER_USER_PASSWORD,RESTRECTED,SEE_RESTRECTED,STATUS,SHOW_NOTIFICATIONS,SHOW_ERRORS,SHOW_ONLINE_USER,SHOW_EXPORT,SHOW_CORRECTIVE_ENTRY From $table where user_id  in (select b.user_id from blocked_users b)";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<usersDTOInter> Records = new ArrayList<usersDTOInter>();
        while (rs.next()) {
            usersDTOInter SelectedRecord = DTOFactory.createusersDTO();
            SelectedRecord.setUserid(rs.getInt("USER_ID"));
            SelectedRecord.setUsername(rs.getString("USER_NAME"));
            SelectedRecord.setUseraname(rs.getString("USER_A_NAME"));
            SelectedRecord.setUserlang(rs.getInt("USER_LANG"));
            SelectedRecord.setHolidayenabled(rs.getInt("HOLIDAY_ENABLED"));
            SelectedRecord.setLogonname(rs.getString("LOGON_NAME"));
            SelectedRecord.setUserpassword(rs.getString("USER_PASSWORD"));
            SelectedRecord.setShiftid(rs.getInt("SHIFT_ID"));
            SelectedRecord.setEmrholidayenabled(rs.getInt("EMR_HOLIDAY_ENABLED"));
            SelectedRecord.setDisputegraph(rs.getInt("DISPUTE_GRAPH"));

            SelectedRecord.setVisible(rs.getInt("VISIBLE"));
            SelectedRecord.setSeeunvisible(rs.getInt("SEE_UNVISIBLE"));
            SelectedRecord.setChangeotheruserpassword(rs.getInt("CHANGE_OTHER_USER_PASSWORD"));
            SelectedRecord.setRestrected(rs.getInt("RESTRECTED"));
            SelectedRecord.setSeerestrected(rs.getInt("SEE_RESTRECTED"));
            SelectedRecord.setStatus(rs.getInt("STATUS"));

            SelectedRecord.setShownotifications(rs.getInt("SHOW_NOTIFICATIONS"));
            SelectedRecord.setShowerrors(rs.getInt("SHOW_ERRORS"));
            SelectedRecord.setShowonlineuser(rs.getInt("SHOW_ONLINE_USER"));
            SelectedRecord.setShowexport(rs.getInt("SHOW_EXPORT"));
            SelectedRecord.setShowcorrectiveentry(rs.getInt("SHOW_CORRECTIVE_ENTRY"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findAllUnBlockRecords(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select USER_ID,USER_NAME,USER_A_NAME,USER_LANG,HOLIDAY_ENABLED,LOGON_NAME,USER_PASSWORD,SHIFT_ID,EMR_HOLIDAY_ENABLED,DISPUTE_GRAPH,VISIBLE,SEE_UNVISIBLE,CHANGE_OTHER_USER_PASSWORD,RESTRECTED,SEE_RESTRECTED,STATUS,SHOW_NOTIFICATIONS,SHOW_ERRORS,SHOW_ONLINE_USER,SHOW_EXPORT,SHOW_CORRECTIVE_ENTRY From $table where user_id not in (select b.user_id from blocked_users b)";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<usersDTOInter> Records = new ArrayList<usersDTOInter>();
        while (rs.next()) {
            usersDTOInter SelectedRecord = DTOFactory.createusersDTO();
            SelectedRecord.setUserid(rs.getInt("USER_ID"));
            SelectedRecord.setUsername(rs.getString("USER_NAME"));
            SelectedRecord.setUseraname(rs.getString("USER_A_NAME"));
            SelectedRecord.setUserlang(rs.getInt("USER_LANG"));
            SelectedRecord.setHolidayenabled(rs.getInt("HOLIDAY_ENABLED"));
            SelectedRecord.setLogonname(rs.getString("LOGON_NAME"));
            SelectedRecord.setUserpassword(rs.getString("USER_PASSWORD"));
            SelectedRecord.setShiftid(rs.getInt("SHIFT_ID"));
            SelectedRecord.setEmrholidayenabled(rs.getInt("EMR_HOLIDAY_ENABLED"));
            SelectedRecord.setDisputegraph(rs.getInt("DISPUTE_GRAPH"));

            SelectedRecord.setVisible(rs.getInt("VISIBLE"));
            SelectedRecord.setSeeunvisible(rs.getInt("SEE_UNVISIBLE"));
            SelectedRecord.setChangeotheruserpassword(rs.getInt("CHANGE_OTHER_USER_PASSWORD"));
            SelectedRecord.setRestrected(rs.getInt("RESTRECTED"));
            SelectedRecord.setSeerestrected(rs.getInt("SEE_RESTRECTED"));
            SelectedRecord.setStatus(rs.getInt("STATUS"));

            SelectedRecord.setShownotifications(rs.getInt("SHOW_NOTIFICATIONS"));
            SelectedRecord.setShowerrors(rs.getInt("SHOW_ERRORS"));
            SelectedRecord.setShowonlineuser(rs.getInt("SHOW_ONLINE_USER"));
            SelectedRecord.setShowexport(rs.getInt("SHOW_EXPORT"));
            SelectedRecord.setShowcorrectiveentry(rs.getInt("SHOW_CORRECTIVE_ENTRY"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAlluserManag(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select p.profile_name profile_name,s.LOCKSTATUS LOCKSTATUS,s.USER_ID USER_ID,s.USER_NAME USER_NAME,s.USER_A_NAME USER_A_NAME,s.USER_LANG USER_LANG,s.HOLIDAY_ENABLED HOLIDAY_ENABLED,s.LOGON_NAME LOGON_NAME,s.USER_PASSWORD USER_PASSWORD,s.SHIFT_ID SHIFT_ID,s.EMR_HOLIDAY_ENABLED EMR_HOLIDAY_ENABLED,s.DISPUTE_GRAPH DISPUTE_GRAPH,s.VISIBLE VISIBLE,s.SEE_UNVISIBLE SEE_UNVISIBLE,s.CHANGE_OTHER_USER_PASSWORD CHANGE_OTHER_USER_PASSWORD,s.RESTRECTED RESTRECTED,s.SEE_RESTRECTED SEE_RESTRECTED,s.STATUS STATUS,s.SHOW_NOTIFICATIONS SHOW_NOTIFICATIONS,s.SHOW_ERRORS SHOW_ERRORS,s.SHOW_ONLINE_USER SHOW_ONLINE_USER,s.SHOW_EXPORT SHOW_EXPORT,s.SHOW_CORRECTIVE_ENTRY SHOW_CORRECTIVE_ENTRY From users s, profile p , user_profile t where p.profile_id = t.profile_id and t.user_id = s.user_id ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<usersDTOInter> Records = new ArrayList<usersDTOInter>();
        while (rs.next()) {
            // ,s.SHOW_CORRECTIVE_ENTRY 
            usersDTOInter SelectedRecord = DTOFactory.createusersDTO();
            SelectedRecord.setUserid(rs.getInt("USER_ID"));
            SelectedRecord.setUsername(rs.getString("USER_NAME"));
            SelectedRecord.setProfilename(rs.getString("profile_name"));
            SelectedRecord.setUseraname(rs.getString("USER_A_NAME"));
            SelectedRecord.setUserlang(rs.getInt("USER_LANG"));
            SelectedRecord.setHolidayenabled(rs.getInt("HOLIDAY_ENABLED"));
            SelectedRecord.setLogonname(rs.getString("LOGON_NAME"));
            SelectedRecord.setUserpassword(rs.getString("USER_PASSWORD"));
//LOCKSTATUS
            SelectedRecord.setLockedstate(rs.getInt("LOCKSTATUS"));
            Integer temp2 = rs.getInt("SHOW_CORRECTIVE_ENTRY");
            if (temp2 == 1) {
                SelectedRecord.setShowcorrectineentryB(Boolean.TRUE);
            } else {
                SelectedRecord.setShowcorrectineentryB(Boolean.FALSE);
            }
            Integer temp3 = rs.getInt("SHOW_EXPORT");
            if (temp3 == 1) {
                SelectedRecord.setShowexportB(Boolean.TRUE);
            } else {
                SelectedRecord.setShowexportB(Boolean.FALSE);
            }
            Integer temp1 = rs.getInt("SHOW_NOTIFICATIONS");
            if (temp1 == 1) {
                SelectedRecord.setNotificationB(Boolean.TRUE);
            } else {
                SelectedRecord.setNotificationB(Boolean.FALSE);
            }

            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    //Used In ChangePassword
    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select USER_ID,USER_NAME,USER_A_NAME,USER_LANG,HOLIDAY_ENABLED,LOGON_NAME,USER_PASSWORD,STATUS From $table  order by USER_NAME  ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<usersDTOInter> Records = new ArrayList<usersDTOInter>();
        while (rs.next()) {
            usersDTOInter SelectedRecord = DTOFactory.createusersDTO();
            SelectedRecord.setUserid(rs.getInt("USER_ID"));
            SelectedRecord.setUsername(rs.getString("USER_NAME"));
            SelectedRecord.setUseraname(rs.getString("USER_A_NAME"));
            SelectedRecord.setUserlang(rs.getInt("USER_LANG"));
            SelectedRecord.setHolidayenabled(rs.getInt("HOLIDAY_ENABLED"));
            SelectedRecord.setLogonname(rs.getString("LOGON_NAME"));
            SelectedRecord.setUserpassword(rs.getString("USER_PASSWORD"));
            SelectedRecord.setStatus(rs.getInt("STATUS"));

            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<usersDTOInter> entities = (List<usersDTOInter>) obj[0];
        ArrayList result = (ArrayList) obj[1];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Users Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "update USERS set USER_NAME = ?,USER_A_NAME = ?,USER_LANG = ?,"
                        + "LOGON_NAME = ? ,"
                        + "LOCKSTATUS = ? ,show_corrective_entry = ?,show_export=? where USER_ID = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        usersDTOInter RecordtoInsert = entities.get(i);

                        statement.setString(1, RecordtoInsert.getUsername());
                        statement.setString(2, RecordtoInsert.getUseraname());
                        statement.setInt(3, RecordtoInsert.getUserlang());
                        statement.setString(4, RecordtoInsert.getLogonname());

                        statement.setInt(5, RecordtoInsert.getLockedstate());
                        statement.setInt(8, RecordtoInsert.getUserid());
                        if (RecordtoInsert.getShowcorrectineentryB()) {
                            statement.setInt(6, 1);
                        } else {
                            statement.setInt(6, 2);
                        } if (RecordtoInsert.getShowexportB()) {
                            statement.setInt(7, 1);
                        } else {
                            statement.setInt(7, 2);
                        }
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                            CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }

                }
                super.postUpdate(result.toString());
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }

    //Used In Profile Assign
    public Object GetAllUnAssignToProfile(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "select u.logon_name logonname from users u where u.user_id not in (select m.user_id from user_profile  m where m.user_id = u.user_id)  ";
        ResultSet rs = executeQuery(selectStat);
        List<usersDTOInter> Records = new ArrayList<usersDTOInter>();
        while (rs.next()) {
            usersDTOInter SelectedRecord = DTOFactory.createusersDTO();
            SelectedRecord.setLogonname(rs.getString("logonname"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    //Used In Profile Assign
    public Object GetAllAssignToProfile(Object... obj) throws Throwable {
        super.preSelect();
        Integer RecordToSelect = (Integer) obj[0];
        String selectStat = "select u.logon_name logonname,USER_ID,USER_NAME from users u where u.user_id  in (select m.user_id from user_profile  m where m.user_id = u.user_id and m.profile_id = $ProfileID) ";
        selectStat = selectStat.replace("$ProfileID", "" + RecordToSelect);
        ResultSet rs = executeQuery(selectStat);
        List<usersDTOInter> Records = new ArrayList<usersDTOInter>();
        while (rs.next()) {
            usersDTOInter SelectedRecord = DTOFactory.createusersDTO();
            SelectedRecord.setLogonname(rs.getString("logonname"));
            SelectedRecord.setUserid(rs.getInt("USER_ID"));
            SelectedRecord.setUsername(rs.getString("USER_NAME"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }
}
