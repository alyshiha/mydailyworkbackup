package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import com.ev.Bingo.bus.dto.disputesDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.matchingerrorsdataDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class matchingerrorsdataDAO extends BaseDAO implements matchingerrorsdataDAOInter {

    protected matchingerrorsdataDAO() {
        super();
        super.setTableName("MATCHING_ERRORS_DATA");
    }

    public Boolean ValidateNull(Object... obj) {
        matchingerrorsdataDTOInter RecordToInsert = (matchingerrorsdataDTOInter) obj[0];
        if (RecordToInsert.getfileid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getcurrency() == null && RecordToInsert.getcurrency().equals("")) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getresponsecode() == null && RecordToInsert.getresponsecode().equals("")) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.gettransactiondate() == null) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getcardno() == null && RecordToInsert.getcardno().equals("")) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getamount() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getjobid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getabsamount() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.gettransactionid() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        disputesDTOInter RecordToUpdate = (disputesDTOInter) obj[0];
        Integer disputekey = (Integer) obj[1];
        usersDTOInter user = (usersDTOInter) obj[2];
        String UpdateStat = "update disputes\n"
                + "               set dispute_key = $disputekey \n"
                + "              ,dispute_by = $user \n"
                + "               , dispute_reason = 'Manual Disputes'\n"
                + "               where rowid = '$rowid'";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$disputekey", "" + disputekey);
        UpdateStat = UpdateStat.replace("$rowid", "" + RecordToUpdate.getrowid());
        UpdateStat = UpdateStat.replace("$user", "" + user.getUserid());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table MATCHING_ERRORS_DATA", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        matchingerrorsdataDTOInter RecordToSelect = (matchingerrorsdataDTOInter) obj[0];
        String selectStat = "Select FILE_ID,LOADING_DATE,TRANSACTION_TYPE,TRANSACTION_TYPE_ID,CURRENCY,CURRENCY_ID,RESPONSE_CODE,RESPONSE_CODE_ID,TRANSACTION_DATE,TRANSACTION_SEQUENCE,CARD_NO,AMOUNT,SETTLEMENT_DATE,CUSTOMER_ACCOUNT_NUMBER,TRANSACTION_SEQUENCE_ORDER_BY,TRANSACTION_TIME,COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5,JOB_ID,TRANSACTION_TYPE_MASTER,RESPONSE_CODE_MASTER,CARD_NO_SUFFIX,RECORD_TYPE,ERROR_CODE,ERROR_MESSAGE,ERROR_DATE,SQL_STATEMENMT,ABS_AMOUNT,TERMINAL,NETWORK_ID,ACQUIRER_CURRENCY,ACQUIRER_CURRENCY_ID,ACQUIRER_AMOUNT,SETTLEMENT_CURRENCY,SETTLEMENT_CURRENCY_ID,SETTLEMENT_AMOUNT,AUTHORIZATION_NO,REFERENCE_NO,BANK,TRANSACTION_ID,LICENCE_KEY,NETWORK_CODE,PROCESS_CODE,AREA_CODE,REPORT_ID,NETWORK_COMM,NETWORK_COMM_FLAG,FEE_LEVEL From $table"
                + "  where  ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        matchingerrorsdataDTOInter SelectedRecord = DTOFactory.creatematchingerrorsdataDTO();
        while (rs.next()) {
            SelectedRecord.setfileid(rs.getInt("FILE_ID"));
            SelectedRecord.setloadingdate(rs.getTimestamp("LOADING_DATE"));
            SelectedRecord.settransactiontype(rs.getString("TRANSACTION_TYPE"));
            SelectedRecord.settransactiontypeid(rs.getInt("TRANSACTION_TYPE_ID"));
            SelectedRecord.setcurrency(rs.getString("CURRENCY"));
            SelectedRecord.setcurrencyid(rs.getInt("CURRENCY_ID"));
            SelectedRecord.setresponsecode(rs.getString("RESPONSE_CODE"));
            SelectedRecord.setresponsecodeid(rs.getInt("RESPONSE_CODE_ID"));
            SelectedRecord.settransactiondate(rs.getTimestamp("TRANSACTION_DATE"));

            SelectedRecord.setcardno(rs.getString("CARD_NO"));
            SelectedRecord.setamount(rs.getFloat("AMOUNT"));
            SelectedRecord.setsettlementdate(rs.getTimestamp("SETTLEMENT_DATE"));
            SelectedRecord.setcustomeraccountnumber(rs.getString("CUSTOMER_ACCOUNT_NUMBER"));
            SelectedRecord.settransactionsequenceorderby(rs.getInt("TRANSACTION_SEQUENCE_ORDER_BY"));
            SelectedRecord.settransactionsequence(rs.getString("TRANSACTION_SEQUENCE"));
            SelectedRecord.settransactiontime(rs.getTimestamp("TRANSACTION_TIME"));
            SelectedRecord.setcolumn1(rs.getString("COLUMN1"));
            SelectedRecord.setcolumn2(rs.getString("COLUMN2"));
            SelectedRecord.setcolumn3(rs.getString("COLUMN3"));
            SelectedRecord.setcolumn4(rs.getString("COLUMN4"));
            SelectedRecord.setcolumn5(rs.getString("COLUMN5"));
            SelectedRecord.setjobid(rs.getInt("JOB_ID"));
            SelectedRecord.settransactiontypemaster(rs.getInt("TRANSACTION_TYPE_MASTER"));
            SelectedRecord.setresponsecodemaster(rs.getInt("RESPONSE_CODE_MASTER"));
            SelectedRecord.setcardnosuffix(rs.getInt("CARD_NO_SUFFIX"));
            SelectedRecord.setrecordtype(rs.getInt("RECORD_TYPE"));
            SelectedRecord.seterrorcode(rs.getInt("ERROR_CODE"));
            SelectedRecord.seterrormessage(rs.getString("ERROR_MESSAGE"));
            SelectedRecord.seterrordate(rs.getTimestamp("ERROR_DATE"));
            SelectedRecord.setsqlstatemenmt(rs.getString("SQL_STATEMENMT"));
            SelectedRecord.setabsamount(rs.getFloat("ABS_AMOUNT"));
            SelectedRecord.setterminal(rs.getString("TERMINAL"));
            SelectedRecord.setnetworkid(rs.getString("NETWORK_ID"));
            SelectedRecord.setacquirercurrency(rs.getString("ACQUIRER_CURRENCY"));
            SelectedRecord.setacquirercurrencyid(rs.getInt("ACQUIRER_CURRENCY_ID"));
            SelectedRecord.setacquireramount(rs.getFloat("ACQUIRER_AMOUNT"));
            SelectedRecord.setsettlementcurrency(rs.getString("SETTLEMENT_CURRENCY"));
            SelectedRecord.setsettlementcurrencyid(rs.getInt("SETTLEMENT_CURRENCY_ID"));
            SelectedRecord.setsettlementamount(rs.getFloat("SETTLEMENT_AMOUNT"));
            SelectedRecord.setauthorizationno(rs.getString("AUTHORIZATION_NO"));
            SelectedRecord.setreferenceno(rs.getString("REFERENCE_NO"));
            SelectedRecord.setbank(rs.getString("BANK"));
            SelectedRecord.settransactionid(rs.getInt("TRANSACTION_ID"));
            SelectedRecord.setlicencekey(rs.getString("LICENCE_KEY"));
            SelectedRecord.setnetworkcode(rs.getString("NETWORK_CODE"));
            SelectedRecord.setprocesscode(rs.getString("PROCESS_CODE"));
            SelectedRecord.setareacode(rs.getString("AREA_CODE"));
            SelectedRecord.setreportid(rs.getString("REPORT_ID"));
            SelectedRecord.setnetworkcomm(rs.getInt("NETWORK_COMM"));
            SelectedRecord.setnetworkcommflag(rs.getString("NETWORK_COMM_FLAG"));
            SelectedRecord.setfeelevel(rs.getString("FEE_LEVEL"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object insertrecordmatched(Object... obj) throws Throwable {
        super.preUpdate();
        disputesDTOInter RecordToInsert = (disputesDTOInter) obj[0];
        Integer MatchedKey = (Integer) obj[1];
        String insertStat = "insert into matched_data\n"
                + "            (file_id,\n"
                + "             loading_date,\n"
                + "             transaction_type,\n"
                + "             transaction_type_id,\n"
                + "             currency,\n"
                + "             currency_id,\n"
                + "             response_code,\n"
                + "             response_code_id,\n"
                + "             transaction_date,\n"
                + "             transaction_sequence,\n"
                + "             card_no,\n"
                + "             amount,\n"
                + "             settlement_date,\n"
                + "             customer_account_number,\n"
                + "             transaction_sequence_order_by,\n"
                + "             transaction_time,\n"
                + "             matching_type,\n"
                + "             record_type,\n"
                + "             match_key,\n"
                + "             column1,\n"
                + "             column2,\n"
                + "             column3,\n"
                + "             column4,\n"
                + "             column5,\n"
                + "             transaction_type_master,\n"
                + "             response_code_master,\n"
                + "             card_no_suffix,\n"
                + "             abs_amount,\n"
                + "             terminal,\n"
                + "             network_id,\n"
                + "             acquirer_currency,\n"
                + "             acquirer_currency_id,\n"
                + "             acquirer_amount,\n"
                + "             settlement_currency,\n"
                + "             settlement_currency_id,\n"
                + "             settlement_amount,\n"
                + "             authorization_no,\n"
                + "             reference_no,\n"
                + "             bank,\n"
                + "             transaction_id,\n"
                + "             network_code,\n"
                + "             process_code,\n"
                + "             area_code,\n"
                + "             report_id,\n"
                + "             network_comm,\n"
                + "             network_comm_flag,\n"
                + "             fee_level,\n"
                + "             licence_key)\n"
                + "          values\n"
                + "            ('$fileid',\n"
                + "             to_date('$loadingdate','dd.mm.yyyy hh24:mi:ss'),\n"
                + "             '$transactiontype',\n"
                + "             '$transactiontypeid',\n"
                + "             '$currency',\n"
                + "             '$currencyid',\n"
                + "             '$responsecode',\n"
                + "             '$responsecodeid',\n"
                + "			  to_date('$transactiondate','dd.mm.yyyy hh24:mi:ss'),\n"
                + "             '$transactionsequence',\n"
                + "             "+super.GetEncrypt("$cardno")+",\n"
                + "             '$amount',\n"
                + "			 to_date('$settlementdate','dd.mm.yyyy hh24:mi:ss'),\n"
                + "             '$customeraccountnumber',\n"
                + "             '$transactionsequenceorderby',\n"
                + "			 to_date('$transactiontime','dd.mm.yyyy hh24:mi:ss'),\n"
                + "             '$matchingtype',\n"
                + "             '$recordtype',\n"
                + "             '$matchkey',\n"
                + "             '$column1',\n"
                + "             '$column2',\n"
                + "             '$column3',\n"
                + "             '$column4',\n"
                + "             '$column5',\n"
                + "             '$transactiontypemaster',\n"
                + "             '$responsecodemaster',\n"
                + "             '$cardnosuffix',\n"
                + "             '$absamount',\n"
                + "             '$terminal',\n"
                + "             (select id from networks where name = '$networkid'),\n"
                + "             '$acquirercurrency',\n"
                + "             '$acquirercurrencyid',\n"
                + "             '$acquireramount',\n"
                + "             '$settlementcurrency',\n"
                + "             $settlementcurrencyid,\n"
                + "             $settlementamount,\n"
                + "             '$authorizationno',\n"
                + "             '$referenceno',\n"
                + "             '$bank',\n"
                + "             $transactionid,\n"
                + "             '$networkcode',\n"
                + "             '$processcode',\n"
                + "             '$areacode',\n"
                + "             '$reportid',\n"
                + "             '$networkcomm',\n"
                + "             '$networkcommflag',\n"
                + "             '$feelevel',\n"
                + "             '$licencekey')";

        insertStat = insertStat.replace("$matchkey", "" + MatchedKey);
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$fileid", "" + RecordToInsert.getfileid());
        insertStat = insertStat.replace("$loadingdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.getloadingdate()));
        insertStat = insertStat.replace("$transactiontypeid", "" + RecordToInsert.gettransactiontypeid());

        insertStat = insertStat.replace("$currencyid", "" + RecordToInsert.getcurrencyid());
        insertStat = insertStat.replace("$currency", "" + RecordToInsert.getcurrency());
        insertStat = insertStat.replace("$responsecodeid", "" + RecordToInsert.getresponsecodeid());

        insertStat = insertStat.replace("$transactiondate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiondate()));

        insertStat = insertStat.replace("$amount", "" + RecordToInsert.getamount());
        insertStat = insertStat.replace("$settlementdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.getsettlementdate()));
        insertStat = insertStat.replace("$customeraccountnumber", "" + RecordToInsert.getcustomeraccountnumber());
        insertStat = insertStat.replace("$transactionsequenceorderby", "" + RecordToInsert.gettransactionsequenceorderby());
        insertStat = insertStat.replace("$transactionsequence", "" + RecordToInsert.gettransactionsequence());
        insertStat = insertStat.replace("$transactiontime", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiontime()));

        insertStat = insertStat.replace("$recordtype", "" + RecordToInsert.getrecordtype());
        insertStat = insertStat.replace("$matchkey", "" + RecordToInsert.getdisputekey());

        insertStat = insertStat.replace("$column1", "" + RecordToInsert.getcolumn1());
        insertStat = insertStat.replace("$column2", "" + RecordToInsert.getcolumn2());
        insertStat = insertStat.replace("$column3", "" + RecordToInsert.getcolumn3());
        insertStat = insertStat.replace("$column4", "" + RecordToInsert.getcolumn4());
        insertStat = insertStat.replace("$column5", "" + RecordToInsert.getcolumn5());
        insertStat = insertStat.replace("$transactiontypemaster", "" + RecordToInsert.gettransactiontypemaster());
        insertStat = insertStat.replace("$responsecodemaster", "" + RecordToInsert.getresponsecodemaster());
        insertStat = insertStat.replace("$transactiontype", "" + RecordToInsert.gettransactiontype());
        insertStat = insertStat.replace("$responsecode", "" + RecordToInsert.getresponsecode());
        insertStat = insertStat.replace("$cardnosuffix", "" + RecordToInsert.getcardnosuffix());
        insertStat = insertStat.replace("$absamount", "" + RecordToInsert.getabsamount());
        insertStat = insertStat.replace("$terminal", "" + RecordToInsert.getterminal());
        insertStat = insertStat.replace("$networkid", "" + RecordToInsert.getnetworkid());
        insertStat = insertStat.replace("$acquirercurrencyid", "" + RecordToInsert.getacquirercurrencyid());
        insertStat = insertStat.replace("$acquirercurrency", "" + RecordToInsert.getacquirercurrency());

        insertStat = insertStat.replace("$acquireramount", "" + RecordToInsert.getacquireramount());
        insertStat = insertStat.replace("$settlementcurrencyid", "" + RecordToInsert.getsettlementcurrencyid());
        insertStat = insertStat.replace("$settlementcurrency", "" + RecordToInsert.getsettlementcurrency());

        insertStat = insertStat.replace("$settlementamount", "" + RecordToInsert.getsettlementamount());
        insertStat = insertStat.replace("$authorizationno", "" + RecordToInsert.getauthorizationno());
        insertStat = insertStat.replace("$referenceno", "" + RecordToInsert.getreferenceno());
        insertStat = insertStat.replace("$bank", "" + RecordToInsert.getbank());
        insertStat = insertStat.replace("$transactionid", "" + RecordToInsert.gettransactionid());
        insertStat = insertStat.replace("$licencekey", "" + RecordToInsert.getlicencekey());
        insertStat = insertStat.replace("$networkcode", "" + RecordToInsert.getnetworkcode());
        insertStat = insertStat.replace("$processcode", "" + RecordToInsert.getprocesscode());
        insertStat = insertStat.replace("$areacode", "" + RecordToInsert.getareacode());
        insertStat = insertStat.replace("$reportid", "" + RecordToInsert.getreportid());
        insertStat = insertStat.replace("$networkcommflag", "" + RecordToInsert.getnetworkcommflag());
        insertStat = insertStat.replace("$networkcomm", "" + RecordToInsert.getnetworkcomm());
        insertStat = insertStat.replace("$cardno", "" + RecordToInsert.getcardno());
        insertStat = insertStat.replace("$matchingtype", "" + RecordToInsert.getmatchingtype());
        insertStat = insertStat.replace("null", "");
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To MATCHED_DATA", false);
        return null;
    }

    public Object insertrecordmatchedmatching(Object... obj) throws Throwable {
        super.preUpdate();
        matchingerrorsdataDTOInter RecordToInsert = (matchingerrorsdataDTOInter) obj[0];
        Integer matchkey = (Integer) obj[1];
        String matchtype = (String) obj[2];
        String insertStat = "insert into matched_data\n"
                + "            (file_id,\n"
                + "             loading_date,\n"
                + "             transaction_type,\n"
                + "             transaction_type_id,\n"
                + "             currency,\n"
                + "             currency_id,\n"
                + "             response_code,\n"
                + "             response_code_id,\n"
                + "             transaction_date,\n"
                + "             transaction_sequence,\n"
                + "             card_no,\n"
                + "             amount,\n"
                + "             settlement_date,\n"
                + "             customer_account_number,\n"
                + "             transaction_sequence_order_by,\n"
                + "             transaction_time,\n"
                + "             matching_type,\n"
                + "             record_type,\n"
                + "             match_key,\n"
                + "             column1,\n"
                + "             column2,\n"
                + "             column3,\n"
                + "             column4,\n"
                + "             column5,\n"
                + "             transaction_type_master,\n"
                + "             response_code_master,\n"
                + "             card_no_suffix,\n"
                + "             abs_amount,\n"
                + "             terminal,\n"
                + "             network_id,\n"
                + "             acquirer_currency,\n"
                + "             acquirer_currency_id,\n"
                + "             acquirer_amount,\n"
                + "             settlement_currency,\n"
                + "             settlement_currency_id,\n"
                + "             settlement_amount,\n"
                + "             authorization_no,\n"
                + "             reference_no,\n"
                + "             bank,\n"
                + "             transaction_id,\n"
                + "             network_code,\n"
                + "             process_code,\n"
                + "             area_code,\n"
                + "             report_id,\n"
                + "             network_comm,\n"
                + "             network_comm_flag,\n"
                + "             fee_level,\n"
                + "             licence_key)\n"
                + "          values\n"
                + "            ($fileid,\n"
                + "			 to_date('$loadingdate','dd.mm.yyyy hh24:mi:ss'),\n"
                + "             '$transactiontype',\n"
                + "             $transactiontypeid,\n"
                + "             '$currency',\n"
                + "             $currencyid,\n"
                + "             '$responsecode',\n"
                + "             $responsecodeid,\n"
                + "             to_date('$transactiondate','dd.mm.yyyy hh24:mi:ss'),\n"
                + "             '$transactionsequence',\n"
                + "             "+super.GetEncrypt("$cardno")+",\n"
                + "             $amount,\n"
                + "           to_date('$settlementdate','dd.mm.yyyy hh24:mi:ss'),\n"
                + "             '$customeraccountnumber',\n"
                + "             $transactionsequenceorderby,\n"
                + "			 to_date('$transactiontime','dd.mm.yyyy hh24:mi:ss'),\n"
                + "             $matchingtype,\n"
                + "             $recordtype,\n"
                + "             $matchkey,\n"
                + "             '$column1',\n"
                + "             '$column2',\n"
                + "             '$column3',\n"
                + "             '$column4',\n"
                + "             '$column5',\n"
                + "             $transactiontypemaster,\n"
                + "             $responsecodemaster,\n"
                + "             $cardnosuffix,\n"
                + "             $absamount,\n"
                + "             '$terminal',\n"
                + "             (select id from networks where name = '$networkid'),\n"
                + "             '$acquirercurrency',\n"
                + "             $acquirercurrencyid,\n"
                + "             $acquireramount,\n"
                + "             '$settlementcurrency',\n"
                + "             $settlementcurrencyid,\n"
                + "             $settlementamount,\n"
                + "             '$authorizationno',\n"
                + "             '$referenceno',\n"
                + "             '$bank',\n"
                + "             $transactionid,\n"
                + "             '$networkcode',\n"
                + "             '$processcode',\n"
                + "             '$areacode',\n"
                + "             '$reportid',\n"
                + "             $networkcomm,\n"
                + "             '$networkcommflag',\n"
                + "             '$feelevel',\n"
                + "             '$licencekey')";

        insertStat = insertStat.replace("$matchkey", "" + matchkey);
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$fileid", "" + RecordToInsert.getfileid());
        insertStat = insertStat.replace("$loadingdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.getloadingdate()));
        insertStat = insertStat.replace("$transactiontypeid", "" + RecordToInsert.gettransactiontypeid());

        insertStat = insertStat.replace("$currencyid", "" + RecordToInsert.getcurrencyid());
        insertStat = insertStat.replace("$currency", "" + RecordToInsert.getcurrency());
        insertStat = insertStat.replace("$responsecodeid", "" + RecordToInsert.getresponsecodeid());

        insertStat = insertStat.replace("$transactiondate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiondate()));

        insertStat = insertStat.replace("$amount", "" + RecordToInsert.getamount());
        insertStat = insertStat.replace("$settlementdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.getsettlementdate()));
        insertStat = insertStat.replace("$customeraccountnumber", "" + RecordToInsert.getcustomeraccountnumber());
        insertStat = insertStat.replace("$transactionsequenceorderby", "" + RecordToInsert.gettransactionsequenceorderby());
        insertStat = insertStat.replace("$transactionsequence", "" + RecordToInsert.gettransactionsequence());
        insertStat = insertStat.replace("$transactiontime", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiontime()));

        insertStat = insertStat.replace("$recordtype", "" + RecordToInsert.getrecordtype());
        insertStat = insertStat.replace("$matchkey", "" + RecordToInsert.getdisputekey());

        insertStat = insertStat.replace("$column1", "" + RecordToInsert.getcolumn1());
        insertStat = insertStat.replace("$column2", "" + RecordToInsert.getcolumn2());
        insertStat = insertStat.replace("$column3", "" + RecordToInsert.getcolumn3());
        insertStat = insertStat.replace("$column4", "" + RecordToInsert.getcolumn4());
        insertStat = insertStat.replace("$column5", "" + RecordToInsert.getcolumn5());
        insertStat = insertStat.replace("$transactiontypemaster", "" + RecordToInsert.gettransactiontypemaster());
        insertStat = insertStat.replace("$responsecodemaster", "" + RecordToInsert.getresponsecodemaster());
        insertStat = insertStat.replace("$transactiontype", "" + RecordToInsert.gettransactiontype());
        insertStat = insertStat.replace("$responsecode", "" + RecordToInsert.getresponsecode());
        insertStat = insertStat.replace("$cardnosuffix", "" + RecordToInsert.getcardnosuffix());
        insertStat = insertStat.replace("$absamount", "" + RecordToInsert.getabsamount());
        insertStat = insertStat.replace("$terminal", "" + RecordToInsert.getterminal());
        insertStat = insertStat.replace("$networkid", "" + RecordToInsert.getnetworkid());
        insertStat = insertStat.replace("$acquirercurrencyid", "" + RecordToInsert.getacquirercurrencyid());
        insertStat = insertStat.replace("$acquirercurrency", "" + RecordToInsert.getacquirercurrency());

        insertStat = insertStat.replace("$acquireramount", "" + RecordToInsert.getacquireramount());
        insertStat = insertStat.replace("$settlementcurrencyid", "" + RecordToInsert.getsettlementcurrencyid());
        insertStat = insertStat.replace("$settlementcurrency", "" + RecordToInsert.getsettlementcurrency());

        insertStat = insertStat.replace("$settlementamount", "" + RecordToInsert.getsettlementamount());
        insertStat = insertStat.replace("$authorizationno", "" + RecordToInsert.getauthorizationno());
        insertStat = insertStat.replace("$referenceno", "" + RecordToInsert.getreferenceno());
        insertStat = insertStat.replace("$bank", "" + RecordToInsert.getbank());
        insertStat = insertStat.replace("$transactionid", "" + RecordToInsert.gettransactionid());
        insertStat = insertStat.replace("$licencekey", "" + RecordToInsert.getlicencekey());
        insertStat = insertStat.replace("$networkcode", "" + RecordToInsert.getnetworkcode());
        insertStat = insertStat.replace("$processcode", "" + RecordToInsert.getprocesscode());
        insertStat = insertStat.replace("$areacode", "" + RecordToInsert.getareacode());
        insertStat = insertStat.replace("$reportid", "" + RecordToInsert.getreportid());
        insertStat = insertStat.replace("$networkcommflag", "" + RecordToInsert.getnetworkcommflag());
        insertStat = insertStat.replace("$networkcomm", "" + RecordToInsert.getnetworkcomm());
        insertStat = insertStat.replace("$cardno", "" + RecordToInsert.getcardno());
        insertStat = insertStat.replace("$matchingtype", "" + matchtype);
        insertStat = insertStat.replace("null", "");
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To MATCHED_DATA", false);
        return null;
    }

    public void MarkAsDispute(Object... obj) throws Throwable {

        matchingerrorsdataDTOInter RecordToInsert = (matchingerrorsdataDTOInter) obj[0];
        disputesDTOInter SecondRecordToInsert = (disputesDTOInter) obj[1];
        usersDTOInter user = (usersDTOInter) obj[2];
        if (SecondRecordToInsert == null) {
            List<String> matchingtypes = (List<String>) findmatchingtype(RecordToInsert);
            for (String matchingtype : matchingtypes) {
                deleterecord(RecordToInsert, matchingtype);
                insertrecord(RecordToInsert, matchingtype, user);
                deleterecordmatchingerrors(RecordToInsert);
            }
        } else {
            Integer disputekey = super.generateSequence("DISPUTE_KEY");
            List<String> matchingtypes = (List<String>) findmatchingtype(RecordToInsert);
            RecordToInsert.setdisputekey(disputekey);
            for (String matchingtype : matchingtypes) {
                deleterecord(RecordToInsert, matchingtype);
                insertrecorddisputee(RecordToInsert, matchingtype, user);
                updaterecord(SecondRecordToInsert, disputekey, user);
                deleterecordmatchingerrors(RecordToInsert);
            }
        }
    }

    public void MarkAsMatched(Object... obj) throws Throwable {
        matchingerrorsdataDTOInter RecordToInsert = (matchingerrorsdataDTOInter) obj[0];
        disputesDTOInter SecondRecordToInsert = (disputesDTOInter) obj[1];
        if (SecondRecordToInsert != null) {
            Integer matcheddatakey = super.generateSequence("MATCHED_DATA");
            insertrecordmatchedmatching(RecordToInsert, matcheddatakey, "" + SecondRecordToInsert.getmatchingtype());
            insertrecordmatched(SecondRecordToInsert, matcheddatakey);
            deleterecordmatchingerrors(RecordToInsert);
            deleterecordrowid(SecondRecordToInsert);
        }
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        matchingerrorsdataDTOInter RecordToInsert = (matchingerrorsdataDTOInter) obj[0];
        String Matchtype = (String) obj[1];
        usersDTOInter user = (usersDTOInter) obj[2];
        String insertStat = "	insert into disputes\n"
                + "              (file_id,\n"
                + "               loading_date,\n"
                + "               transaction_type,\n"
                + "               transaction_type_id,\n"
                + "               currency,\n"
                + "               currency_id,\n"
                + "               response_code,\n"
                + "               response_code_id,\n"
                + "               transaction_date,\n"
                + "               transaction_sequence,\n"
                + "               card_no,\n"
                + "               amount,\n"
                + "               settlement_date,\n"
                + "               customer_account_number,\n"
                + "               transaction_sequence_order_by,\n"
                + "               transaction_time,\n"
                + "               dispute_key,\n"
                + "               record_type,\n"
                + "               dispute_reason,\n"
                + "               matching_type,\n"
                + "               dispute_with_roles,\n"
                + "               column1,\n"
                + "               column2,\n"
                + "               column3,\n"
                + "               column4,\n"
                + "               column5,\n"
                + "               transaction_type_master,\n"
                + "               response_code_master,\n"
                + "               card_no_suffix,\n"
                + "               abs_amount,\n"
                + "               terminal,\n"
                + "               network_id,\n"
                + "               acquirer_currency,\n"
                + "               acquirer_currency_id,\n"
                + "               acquirer_amount,\n"
                + "               settlement_currency,\n"
                + "               settlement_currency_id,\n"
                + "               settlement_amount,\n"
                + "               authorization_no,\n"
                + "               reference_no,\n"
                + "               bank,\n"
                + "               transaction_id,\n"
                + "               network_code,\n"
                + "               process_code,\n"
                + "               area_code,\n"
                + "               report_id,\n"
                + "               network_comm,\n"
                + "               network_comm_flag,\n"
                + "               fee_level,\n"
                + "               licence_key,dispute_by)\n"
                + "            values\n"
                + "              ($fileid,\n"
                + "			  to_date('$loadingdate','dd.mm.yyyy hh24:mi:ss'),\n"
                + "               '$transactiontype',\n"
                + "               $transactiontypeid,\n"
                + "               '$currency',\n"
                + "               $currencyid,\n"
                + "               '$responsecode',\n"
                + "               $responsecodeid,\n"
                + "			   to_date('$transactiondate','dd.mm.yyyy hh24:mi:ss'),\n"
                + "               '$transactionsequence',\n"
                + "               "+super.GetEncrypt("$cardno")+",\n"
                + "               $amount,               \n"
                + "			    to_date('$settlementdate','dd.mm.yyyy hh24:mi:ss'),\n"
                + "               '$customeraccountnumber',\n"
                + "               $transactionsequenceorderby,\n"
                + "			   to_date('$transactiontime','dd.mm.yyyy hh24:mi:ss'),\n"
                + "               'null',\n"
                + "               $recordtype,\n"
                + "               'Manual Disputes',\n"
                + "               $MatchKey,\n"
                + "               2,\n"
                + "               '$column1',\n"
                + "               '$column2',\n"
                + "               '$column3',\n"
                + "               '$column4',\n"
                + "               '$column5',\n"
                + "               $transactiontypemaster,\n"
                + "               $responsecodemaster,\n"
                + "               $cardnosuffix,\n"
                + "               $absamount,\n"
                + "               '$terminal',\n"
                + "               (select id from networks where name = '$networkid'),\n"
                + "               '$acquirercurrency',\n"
                + "               $acquirercurrencyid,\n"
                + "               $acquireramount,\n"
                + "               '$settlementcurrency',\n"
                + "               $settlementcurrencyid,\n"
                + "               $settlementamount,\n"
                + "               '$authorizationno',\n"
                + "               '$referenceno',\n"
                + "               '$bank',\n"
                + "               $transactionid,\n"
                + "               '$networkcode',\n"
                + "               '$processcode',\n"
                + "               '$areacode',\n"
                + "               '$reportid',\n"
                + "               $networkcomm,\n"
                + "               '$networkcommflag',\n"
                + "               '$feelevel',\n"
                + "               '$licencekey',$disputeby)";

        insertStat = insertStat.replace("$disputeby", "" + user.getUserid());
        insertStat = insertStat.replace("$MatchKey", "" + Matchtype);
        insertStat = insertStat.replace("$responsecodeid", "" + RecordToInsert.getresponsecodeid());
        insertStat = insertStat.replace("$transactiondate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiondate()));
        insertStat = insertStat.replace("$amount", "" + RecordToInsert.getamount());
        insertStat = insertStat.replace("$settlementdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.getsettlementdate()));
        insertStat = insertStat.replace("$customeraccountnumber", "" + RecordToInsert.getcustomeraccountnumber());
        insertStat = insertStat.replace("$transactionsequenceorderby", "" + RecordToInsert.gettransactionsequenceorderby());
        insertStat = insertStat.replace("$transactionsequence", "" + RecordToInsert.gettransactionsequence());
        insertStat = insertStat.replace("$transactiontime", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiontime()));
        insertStat = insertStat.replace("$recordtype", "" + RecordToInsert.getrecordtype());
        insertStat = insertStat.replace("$disputekey", "" + RecordToInsert.getdisputekey());
        insertStat = insertStat.replace("$column1", "" + RecordToInsert.getcolumn1());
        insertStat = insertStat.replace("$column2", "" + RecordToInsert.getcolumn2());
        insertStat = insertStat.replace("$column3", "" + RecordToInsert.getcolumn3());
        insertStat = insertStat.replace("$column4", "" + RecordToInsert.getcolumn4());
        insertStat = insertStat.replace("$column5", "" + RecordToInsert.getcolumn5());
        insertStat = insertStat.replace("$transactiontypemaster", "" + RecordToInsert.gettransactiontypemaster());
        insertStat = insertStat.replace("$responsecodemaster", "" + RecordToInsert.getresponsecodemaster());
        insertStat = insertStat.replace("$cardnosuffix", "" + RecordToInsert.getcardnosuffix());
        insertStat = insertStat.replace("$absamount", "" + RecordToInsert.getabsamount());
        insertStat = insertStat.replace("$terminal", "" + RecordToInsert.getterminal());
        insertStat = insertStat.replace("$networkid", "" + RecordToInsert.getnetworkid());

        insertStat = insertStat.replace("$acquirercurrencyid", "" + RecordToInsert.getacquirercurrencyid());
        insertStat = insertStat.replace("$acquirercurrency", "" + RecordToInsert.getacquirercurrency());
        insertStat = insertStat.replace("$acquireramount", "" + RecordToInsert.getacquireramount());

        insertStat = insertStat.replace("$settlementcurrencyid", "" + RecordToInsert.getsettlementcurrencyid());
        insertStat = insertStat.replace("$settlementcurrency", "" + RecordToInsert.getsettlementcurrency());
        insertStat = insertStat.replace("$settlementamount", "" + RecordToInsert.getsettlementamount());
        insertStat = insertStat.replace("$authorizationno", "" + RecordToInsert.getauthorizationno());
        insertStat = insertStat.replace("$referenceno", "" + RecordToInsert.getreferenceno());
        insertStat = insertStat.replace("$bank", "" + RecordToInsert.getbank());
        insertStat = insertStat.replace("$transactionid", "" + RecordToInsert.gettransactionid());
        insertStat = insertStat.replace("$licencekey", "" + RecordToInsert.getlicencekey());
        insertStat = insertStat.replace("$networkcode", "" + RecordToInsert.getnetworkcode());
        insertStat = insertStat.replace("$processcode", "" + RecordToInsert.getprocesscode());
        insertStat = insertStat.replace("$areacode", "" + RecordToInsert.getareacode());
        insertStat = insertStat.replace("$reportid", "" + RecordToInsert.getreportid());
        insertStat = insertStat.replace("$cardno", "" + RecordToInsert.getcardno());
        insertStat = insertStat.replace("$networkcommflag", "" + RecordToInsert.getnetworkcommflag());
        insertStat = insertStat.replace("$networkcomm", "" + RecordToInsert.getnetworkcomm());
        insertStat = insertStat.replace("$feelevel", "" + RecordToInsert.getfeelevel());
        insertStat = insertStat.replace("$fileid", "" + RecordToInsert.getfileid());
        insertStat = insertStat.replace("$loadingdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.getloadingdate()));

        insertStat = insertStat.replace("$transactiontypeid", "" + RecordToInsert.gettransactiontypeid());
        insertStat = insertStat.replace("$transactiontype", "" + RecordToInsert.gettransactiontype());
        insertStat = insertStat.replace("$currencyid", "" + RecordToInsert.getcurrencyid());
        insertStat = insertStat.replace("$currency", "" + RecordToInsert.getcurrency());
        insertStat = insertStat.replace("$responsecode", "" + RecordToInsert.getresponsecode());
        insertStat = insertStat.replace("null", "");
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To DISPUTES", false);
        return null;
    }

    public Object insertrecorddisputee(Object... obj) throws Throwable {
        super.preUpdate();
        matchingerrorsdataDTOInter RecordToInsert = (matchingerrorsdataDTOInter) obj[0];
        String Matchtype = (String) obj[1];
        usersDTOInter user = (usersDTOInter) obj[2];
        String insertStat = "	insert into disputes\n"
                + "              (file_id,\n"
                + "               loading_date,\n"
                + "               transaction_type,\n"
                + "               transaction_type_id,\n"
                + "               currency,\n"
                + "               currency_id,\n"
                + "               response_code,\n"
                + "               response_code_id,\n"
                + "               transaction_date,\n"
                + "               transaction_sequence,\n"
                + "               card_no,\n"
                + "               amount,\n"
                + "               settlement_date,\n"
                + "               customer_account_number,\n"
                + "               transaction_sequence_order_by,\n"
                + "               transaction_time,\n"
                + "               dispute_key,\n"
                + "               record_type,\n"
                + "               dispute_reason,\n"
                + "               matching_type,\n"
                + "               dispute_with_roles,\n"
                + "               column1,\n"
                + "               column2,\n"
                + "               column3,\n"
                + "               column4,\n"
                + "               column5,\n"
                + "               transaction_type_master,\n"
                + "               response_code_master,\n"
                + "               card_no_suffix,\n"
                + "               abs_amount,\n"
                + "               terminal,\n"
                + "               network_id,\n"
                + "               acquirer_currency,\n"
                + "               acquirer_currency_id,\n"
                + "               acquirer_amount,\n"
                + "               settlement_currency,\n"
                + "               settlement_currency_id,\n"
                + "               settlement_amount,\n"
                + "               authorization_no,\n"
                + "               reference_no,\n"
                + "               bank,\n"
                + "               transaction_id,\n"
                + "               network_code,\n"
                + "               process_code,\n"
                + "               area_code,\n"
                + "               report_id,\n"
                + "               network_comm,\n"
                + "               network_comm_flag,\n"
                + "               fee_level,\n"
                + "               licence_key,dispute_by)\n"
                + "            values\n"
                + "              ($fileid,\n"
                + "			  to_date('$loadingdate','dd.mm.yyyy hh24:mi:ss'),\n"
                + "               '$transactiontype',\n"
                + "               $transactiontypeid,\n"
                + "               '$currency',\n"
                + "               $currencyid,\n"
                + "               '$responsecode',\n"
                + "               $responsecodeid,\n"
                + "			   to_date('$transactiondate','dd.mm.yyyy hh24:mi:ss'),\n"
                + "               '$transactionsequence',\n"
                + "               "+super.GetEncrypt("$cardno")+",\n"
                + "               $amount,               \n"
                + "			    to_date('$settlementdate','dd.mm.yyyy hh24:mi:ss'),\n"
                + "               '$customeraccountnumber',\n"
                + "               $transactionsequenceorderby,\n"
                + "			   to_date('$transactiontime','dd.mm.yyyy hh24:mi:ss'),\n"
                + "               '$disputekey',\n"
                + "               $recordtype,\n"
                + "               'Manual Disputes',\n"
                + "               $MatchKey,\n"
                + "               2,\n"
                + "               '$column1',\n"
                + "               '$column2',\n"
                + "               '$column3',\n"
                + "               '$column4',\n"
                + "               '$column5',\n"
                + "               $transactiontypemaster,\n"
                + "               $responsecodemaster,\n"
                + "               $cardnosuffix,\n"
                + "               $absamount,\n"
                + "               '$terminal',\n"
                + "               (select id from networks where name = '$networkid'),\n"
                + "               '$acquirercurrency',\n"
                + "               $acquirercurrencyid,\n"
                + "               $acquireramount,\n"
                + "               '$settlementcurrency',\n"
                + "               $settlementcurrencyid,\n"
                + "               $settlementamount,\n"
                + "               '$authorizationno',\n"
                + "               '$referenceno',\n"
                + "               '$bank',\n"
                + "               $transactionid,\n"
                + "               '$networkcode',\n"
                + "               '$processcode',\n"
                + "               '$areacode',\n"
                + "               '$reportid',\n"
                + "               $networkcomm,\n"
                + "               '$networkcommflag',\n"
                + "               '$feelevel',\n"
                + "               '$licencekey',$disputeby)";
        insertStat = insertStat.replace("$disputeby", "" + user.getUserid());
        insertStat = insertStat.replace("$disputekey", "" + RecordToInsert.getdisputekey());
        insertStat = insertStat.replace("$MatchKey", "" + Matchtype);
        insertStat = insertStat.replace("$responsecodeid", "" + RecordToInsert.getresponsecodeid());
        insertStat = insertStat.replace("$transactiondate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiondate()));
        insertStat = insertStat.replace("$amount", "" + RecordToInsert.getamount());
        insertStat = insertStat.replace("$settlementdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.getsettlementdate()));
        insertStat = insertStat.replace("$customeraccountnumber", "" + RecordToInsert.getcustomeraccountnumber());
        insertStat = insertStat.replace("$transactionsequenceorderby", "" + RecordToInsert.gettransactionsequenceorderby());
        insertStat = insertStat.replace("$transactionsequence", "" + RecordToInsert.gettransactionsequence());
        insertStat = insertStat.replace("$transactiontime", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.gettransactiontime()));
        insertStat = insertStat.replace("$recordtype", "" + RecordToInsert.getrecordtype());
        insertStat = insertStat.replace("$disputekey", "" + RecordToInsert.getdisputekey());
        insertStat = insertStat.replace("$column1", "" + RecordToInsert.getcolumn1());
        insertStat = insertStat.replace("$column2", "" + RecordToInsert.getcolumn2());
        insertStat = insertStat.replace("$column3", "" + RecordToInsert.getcolumn3());
        insertStat = insertStat.replace("$column4", "" + RecordToInsert.getcolumn4());
        insertStat = insertStat.replace("$column5", "" + RecordToInsert.getcolumn5());
        insertStat = insertStat.replace("$transactiontypemaster", "" + RecordToInsert.gettransactiontypemaster());
        insertStat = insertStat.replace("$responsecodemaster", "" + RecordToInsert.getresponsecodemaster());
        insertStat = insertStat.replace("$cardnosuffix", "" + RecordToInsert.getcardnosuffix());
        insertStat = insertStat.replace("$absamount", "" + RecordToInsert.getabsamount());
        insertStat = insertStat.replace("$terminal", "" + RecordToInsert.getterminal());
        insertStat = insertStat.replace("$networkid", "" + RecordToInsert.getnetworkid());

        insertStat = insertStat.replace("$acquirercurrencyid", "" + RecordToInsert.getacquirercurrencyid());
        insertStat = insertStat.replace("$acquirercurrency", "" + RecordToInsert.getacquirercurrency());
        insertStat = insertStat.replace("$acquireramount", "" + RecordToInsert.getacquireramount());

        insertStat = insertStat.replace("$settlementcurrencyid", "" + RecordToInsert.getsettlementcurrencyid());
        insertStat = insertStat.replace("$settlementcurrency", "" + RecordToInsert.getsettlementcurrency());
        insertStat = insertStat.replace("$settlementamount", "" + RecordToInsert.getsettlementamount());
        insertStat = insertStat.replace("$authorizationno", "" + RecordToInsert.getauthorizationno());
        insertStat = insertStat.replace("$referenceno", "" + RecordToInsert.getreferenceno());
        insertStat = insertStat.replace("$bank", "" + RecordToInsert.getbank());
        insertStat = insertStat.replace("$transactionid", "" + RecordToInsert.gettransactionid());
        insertStat = insertStat.replace("$licencekey", "" + RecordToInsert.getlicencekey());
        insertStat = insertStat.replace("$networkcode", "" + RecordToInsert.getnetworkcode());
        insertStat = insertStat.replace("$processcode", "" + RecordToInsert.getprocesscode());
        insertStat = insertStat.replace("$areacode", "" + RecordToInsert.getareacode());
        insertStat = insertStat.replace("$reportid", "" + RecordToInsert.getreportid());
        insertStat = insertStat.replace("$cardno", "" + RecordToInsert.getcardno());
        insertStat = insertStat.replace("$networkcommflag", "" + RecordToInsert.getnetworkcommflag());
        insertStat = insertStat.replace("$networkcomm", "" + RecordToInsert.getnetworkcomm());
        insertStat = insertStat.replace("$feelevel", "" + RecordToInsert.getfeelevel());
        insertStat = insertStat.replace("$fileid", "" + RecordToInsert.getfileid());
        insertStat = insertStat.replace("$loadingdate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.getloadingdate()));

        insertStat = insertStat.replace("$transactiontypeid", "" + RecordToInsert.gettransactiontypeid());
        insertStat = insertStat.replace("$transactiontype", "" + RecordToInsert.gettransactiontype());
        insertStat = insertStat.replace("$currencyid", "" + RecordToInsert.getcurrencyid());
        insertStat = insertStat.replace("$currency", "" + RecordToInsert.getcurrency());
        insertStat = insertStat.replace("$responsecode", "" + RecordToInsert.getresponsecode());
        insertStat = insertStat.replace("null", "");
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To DISPUTES", false);
        return null;
    }

    public Object deleterecordmatchingerrors(Object... obj) throws Throwable {
        super.preUpdate();
        matchingerrorsdataDTOInter RecordToDelete = (matchingerrorsdataDTOInter) obj[0];
        String deleteStat = "delete from MATCHING_ERRORS_DATA "
                + "               where rowid = '$rowid'";
        deleteStat = deleteStat.replace("$rowid", "" + RecordToDelete.getrowid());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table MATCHING_ERRORS_DATA", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        matchingerrorsdataDTOInter RecordToDelete = (matchingerrorsdataDTOInter) obj[0];
        String MatchType = (String) obj[1];
        String deleteStat = " delete from disputes"
                + "			 where transaction_id = $transactionid"
                + "			 and record_type = $recordtype"
                + "			 and matching_type = $MatchType";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$transactionid", "" + RecordToDelete.gettransactionid());
        deleteStat = deleteStat.replace("$recordtype", "" + RecordToDelete.getrecordtype());
        deleteStat = deleteStat.replace("$MatchType", "" + MatchType);
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table MATCHING_ERRORS_DATA", false);
        return null;
    }

    public Object deleterecordrowid(Object... obj) throws Throwable {
        super.preUpdate();
        disputesDTOInter RecordToDelete = (disputesDTOInter) obj[0];
        String deleteStat = "delete from disputes "
                + "               where rowid = '$rowid'";
        deleteStat = deleteStat.replace("$rowid", "" + RecordToDelete.getrowid());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table MATCHING_ERRORS_DATA", false);
        return null;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        matchingerrorsdataDTOInter RecordToSelect = (matchingerrorsdataDTOInter) obj[0];
        String selectStat = RecordToSelect.getsqlstatemenmt();
        selectStat = selectStat.replace("/*+INDEX (disputes DIS_MATCH_INDEX_1_10)*/  *", " /*+INDEX (disputes DIS_MATCH_INDEX_1_1)*/  rownum,rowid,RESPONSE_CODE_ID,decode(record_type,1,'Network',2,'Switch',3,'Host','No Record')record_name,TRANSACTION_DATE,FILE_ID,CURRENCY,CURRENCY_ID,RESPONSE_CODE,TRANSACTION_TYPE_ID,TRANSACTION_TYPE,LOADING_DATE,FEE_LEVEL,REVERSE_FLAG,NETWORK_COMM,NETWORK_COMM_FLAG,REPORT_ID,ACQUIRER_CURRENCY,AREA_CODE,PROCESS_CODE, network_code,LICENCE_KEY,TRANSACTION_ID,BANK,REFERENCE_NO,AUTHORIZATION_NO,SETTLEMENT_AMOUNT,SETTLEMENT_CURRENCY_ID,ACQUIRER_AMOUNT,SETTLEMENT_CURRENCY,ACQUIRER_CURRENCY_ID,DISPUTE_DATE,(select name from networks where id = NETWORK_ID)NETWORK_ID ,TERMINAL,ABS_AMOUNT,CORRECTIVE_ENTRY_FLAG,COLUMN5,CARD_NO_SUFFIX,RESPONSE_CODE_MASTER,TRANSACTION_TYPE_MASTER,COLUMN3,COLUMN4,COLUMN2,DISPUTE_WITH_ROLES,COLUMN1,DISPUTE_BY,COMMENTS,SETTLED_USER,SETTLED_FLAG,SETTLED_DATE,TRANSACTION_TIME,DISPUTE_KEY,DISPUTE_REASON,MATCHING_TYPE,RECORD_TYPE,"+super.GetDecryptCardNo()+" CARD_NO,AMOUNT,SETTLEMENT_DATE,CUSTOMER_ACCOUNT_NUMBER,TRANSACTION_SEQUENCE_ORDER_BY,TRANSACTION_SEQUENCE ");

        ResultSet rs = executeQuery(selectStat);
        List<disputesDTOInter> Records = new ArrayList<disputesDTOInter>();
        while (rs.next()) {
            disputesDTOInter SelectedRecord = DTOFactory.createdisputesDTO();

            SelectedRecord.setRecordname(rs.getString("record_name"));
            SelectedRecord.setrowid(rs.getString("rowid"));
            SelectedRecord.setRownum(rs.getInt("rownum"));
            SelectedRecord.setresponsecodeid(rs.getInt("RESPONSE_CODE_ID"));
            SelectedRecord.settransactiondate(rs.getTimestamp("TRANSACTION_DATE"));
            SelectedRecord.setcardno(rs.getString("CARD_NO"));
            SelectedRecord.setamount(rs.getFloat("AMOUNT"));
            SelectedRecord.setsettlementdate(rs.getTimestamp("SETTLEMENT_DATE"));
            SelectedRecord.setcustomeraccountnumber(rs.getString("CUSTOMER_ACCOUNT_NUMBER"));
            SelectedRecord.settransactionsequenceorderby(rs.getString("TRANSACTION_SEQUENCE_ORDER_BY"));
            SelectedRecord.settransactionsequence(rs.getString("TRANSACTION_SEQUENCE"));
            SelectedRecord.settransactiontime(rs.getTimestamp("TRANSACTION_TIME"));
            SelectedRecord.setmatchingtype(rs.getInt("MATCHING_TYPE"));
            SelectedRecord.setrecordtype(rs.getInt("RECORD_TYPE"));
            SelectedRecord.setdisputekey(rs.getInt("DISPUTE_KEY"));
            SelectedRecord.setdisputereason(rs.getString("DISPUTE_REASON"));
            SelectedRecord.setsettledflag(rs.getInt("SETTLED_FLAG"));
            SelectedRecord.setsettleddate(rs.getTimestamp("SETTLED_DATE"));
            SelectedRecord.setsettleduser(rs.getInt("SETTLED_USER"));
            SelectedRecord.setcomments(rs.getString("COMMENTS"));
            SelectedRecord.setdisputeby(rs.getInt("DISPUTE_BY"));
            SelectedRecord.setdisputedate(rs.getTimestamp("DISPUTE_DATE"));
            SelectedRecord.setdisputewithroles(rs.getInt("DISPUTE_WITH_ROLES"));
            SelectedRecord.setcolumn1(rs.getString("COLUMN1"));
            SelectedRecord.setcolumn2(rs.getString("COLUMN2"));
            SelectedRecord.setcolumn3(rs.getString("COLUMN3"));
            SelectedRecord.setcolumn4(rs.getString("COLUMN4"));
            SelectedRecord.setcolumn5(rs.getString("COLUMN5"));
            SelectedRecord.settransactiontypemaster(rs.getInt("TRANSACTION_TYPE_MASTER"));
            SelectedRecord.setresponsecodemaster(rs.getInt("RESPONSE_CODE_MASTER"));
            SelectedRecord.setcardnosuffix(rs.getString("CARD_NO_SUFFIX"));
            SelectedRecord.setCorrectiveentryflag(rs.getInt("CORRECTIVE_ENTRY_FLAG"));
            SelectedRecord.setabsamount(rs.getFloat("ABS_AMOUNT"));
            SelectedRecord.setterminal(rs.getString("TERMINAL"));
            SelectedRecord.setnetworkid(rs.getString("NETWORK_ID"));
            SelectedRecord.setacquirercurrency(rs.getString("ACQUIRER_CURRENCY"));
            SelectedRecord.setacquirercurrencyid(rs.getInt("ACQUIRER_CURRENCY_ID"));
            SelectedRecord.setacquireramount(rs.getFloat("ACQUIRER_AMOUNT"));
            SelectedRecord.setsettlementcurrency(rs.getString("SETTLEMENT_CURRENCY"));
            SelectedRecord.setsettlementcurrencyid(rs.getInt("SETTLEMENT_CURRENCY_ID"));
            SelectedRecord.setsettlementamount(rs.getFloat("SETTLEMENT_AMOUNT"));
            SelectedRecord.setauthorizationno(rs.getString("AUTHORIZATION_NO"));
            SelectedRecord.setreferenceno(rs.getString("REFERENCE_NO"));
            SelectedRecord.setbank(rs.getString("BANK"));
            SelectedRecord.settransactionid(rs.getInt("TRANSACTION_ID"));
            SelectedRecord.setlicencekey(rs.getString("LICENCE_KEY"));
            SelectedRecord.setnetworkcode(rs.getString("NETWORK_CODE"));
            SelectedRecord.setprocesscode(rs.getString("PROCESS_CODE"));
            SelectedRecord.setareacode(rs.getString("AREA_CODE"));
            SelectedRecord.setreportid(rs.getString("REPORT_ID"));
            SelectedRecord.setnetworkcomm(rs.getInt("NETWORK_COMM"));
            SelectedRecord.setnetworkcommflag(rs.getString("NETWORK_COMM_FLAG"));
            SelectedRecord.setfeelevel(rs.getString("FEE_LEVEL"));
            SelectedRecord.setreverseflag(rs.getInt("REVERSE_FLAG"));
            SelectedRecord.setfileid(rs.getInt("FILE_ID"));
            SelectedRecord.setloadingdate(rs.getTimestamp("LOADING_DATE"));
            SelectedRecord.settransactiontype(rs.getString("TRANSACTION_TYPE"));
            SelectedRecord.settransactiontypeid(rs.getInt("TRANSACTION_TYPE_ID"));
            SelectedRecord.setcurrency(rs.getString("CURRENCY"));
            SelectedRecord.setcurrencyid(rs.getInt("CURRENCY_ID"));
            SelectedRecord.setresponsecode(rs.getString("RESPONSE_CODE"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(usersDTOInter UserDTO, Date loadingfrom, Date loadingto, Integer network, Integer recordtypeid) throws Throwable {
        super.preSelect();
        String selectStat = "Select rownum,rowid,LOADING_DATE,JOB_ID,ERROR_CODE,ERROR_MESSAGE,ERROR_DATE,SQL_STATEMENMT,(select t.name from transaction_type t where t.id = TRANSACTION_TYPE_ID)TRANSACTION_TYPE,TRANSACTION_TYPE_ID,CURRENCY,CURRENCY_ID,RESPONSE_CODE,RESPONSE_CODE_ID,TRANSACTION_DATE,TRANSACTION_SEQUENCE,"+super.GetDecryptCardNo()+"CARD_NO,AMOUNT,SETTLEMENT_DATE,CUSTOMER_ACCOUNT_NUMBER,TRANSACTION_SEQUENCE_ORDER_BY,TRANSACTION_TIME,COLUMN1,COLUMN2,BANK,TRANSACTION_ID,(select name from networks where id = NETWORK_ID)NETWORK_ID ,ACQUIRER_AMOUNT,REFERENCE_NO,FEE_LEVEL,PROCESS_CODE,AREA_CODE,REPORT_ID,NETWORK_COMM,(select p.name from LOADING_FILE_LOG p where p.id=FILE_ID )FILEname,FILE_ID,NETWORK_COMM_FLAG,LICENCE_KEY, NETWORK_CODE,COLUMN3,COLUMN4,COLUMN5,(select j.name from transaction_type_master j where j.id = TRANSACTION_TYPE_MASTER)TRANSACTION_MASTER,TRANSACTION_TYPE_MASTER,RESPONSE_CODE_MASTER,CARD_NO_SUFFIX,decode(RECORD_TYPE,'1','Network','2','Switch','Host')RECORD,RECORD_TYPE,ABS_AMOUNT,TERMINAL,ACQUIRER_CURRENCY,ACQUIRER_CURRENCY_ID,SETTLEMENT_CURRENCY,SETTLEMENT_CURRENCY_ID,SETTLEMENT_AMOUNT,AUTHORIZATION_NO From $table WHERE "+super.GetDecryptLinc()+" = TRANSACTION_ID AND NETWORK_ID in (select un.networkid from user_network un where un.userid = $user) and LOADING_DATE > to_date('$dateF','dd.mm.yyyy hh24:mi:ss')and LOADING_DATE < to_date('$dateT','dd.mm.yyyy hh24:mi:ss') ";
        selectStat = selectStat.replace("$user", "" + UserDTO.getUserid());
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$dateT", "" + DateFormatter.changeDateAndTimeFormat(loadingto));
        selectStat = selectStat.replace("$dateF", "" + DateFormatter.changeDateAndTimeFormat(loadingfrom));
        if (network != 0) {
            selectStat += " and NETWORK_ID = " + network;
        }
        if (recordtypeid != 0) {
            selectStat += " and RECORD_TYPE = " + recordtypeid;
        }
        selectStat += " order by TRANSACTION_DATE";
        ResultSet rs = executeQuery(selectStat);
        List<matchingerrorsdataDTOInter> Records = new ArrayList<matchingerrorsdataDTOInter>();
        while (rs.next()) {
            matchingerrorsdataDTOInter SelectedRecord = DTOFactory.creatematchingerrorsdataDTO();
            SelectedRecord.setrowid(rs.getString("rowid"));
            SelectedRecord.setrecord(rs.getString("RECORD"));
            SelectedRecord.settransactionmaster(rs.getString("TRANSACTION_MASTER"));
            SelectedRecord.setfilename(rs.getString("FILEname"));
            SelectedRecord.setRownum(rs.getInt("rownum"));
            SelectedRecord.setfileid(rs.getInt("FILE_ID"));
            SelectedRecord.setloadingdate(rs.getTimestamp("LOADING_DATE"));
            SelectedRecord.settransactiontype(rs.getString("TRANSACTION_TYPE"));
            SelectedRecord.settransactiontypeid(rs.getInt("TRANSACTION_TYPE_ID"));
            SelectedRecord.setcurrency(rs.getString("CURRENCY"));
            SelectedRecord.setcurrencyid(rs.getInt("CURRENCY_ID"));
            SelectedRecord.setresponsecode(rs.getString("RESPONSE_CODE"));
            SelectedRecord.setresponsecodeid(rs.getInt("RESPONSE_CODE_ID"));
            SelectedRecord.settransactiondate(rs.getTimestamp("TRANSACTION_DATE"));
            SelectedRecord.setcardno(rs.getString("CARD_NO"));
            SelectedRecord.setamount(rs.getFloat("AMOUNT"));
            SelectedRecord.setsettlementdate(rs.getTimestamp("SETTLEMENT_DATE"));
            SelectedRecord.setcustomeraccountnumber(rs.getString("CUSTOMER_ACCOUNT_NUMBER"));
            SelectedRecord.settransactionsequenceorderby(rs.getInt("TRANSACTION_SEQUENCE_ORDER_BY"));
            SelectedRecord.settransactiontime(rs.getTimestamp("TRANSACTION_TIME"));
            SelectedRecord.settransactionsequence(rs.getString("TRANSACTION_SEQUENCE"));
            SelectedRecord.setcolumn1(rs.getString("COLUMN1"));
            SelectedRecord.setcolumn2(rs.getString("COLUMN2"));
            SelectedRecord.setcolumn3(rs.getString("COLUMN3"));
            SelectedRecord.setcolumn4(rs.getString("COLUMN4"));
            SelectedRecord.setcolumn5(rs.getString("COLUMN5"));
            SelectedRecord.setjobid(rs.getInt("JOB_ID"));
            SelectedRecord.settransactiontypemaster(rs.getInt("TRANSACTION_TYPE_MASTER"));
            SelectedRecord.setresponsecodemaster(rs.getInt("RESPONSE_CODE_MASTER"));
            SelectedRecord.setcardnosuffix(rs.getInt("CARD_NO_SUFFIX"));
            SelectedRecord.setrecordtype(rs.getInt("RECORD_TYPE"));
            SelectedRecord.seterrorcode(rs.getInt("ERROR_CODE"));
            SelectedRecord.seterrormessage(rs.getString("ERROR_MESSAGE"));
            SelectedRecord.seterrordate(rs.getTimestamp("ERROR_DATE"));
            SelectedRecord.setsqlstatemenmt(rs.getString("SQL_STATEMENMT"));
            SelectedRecord.setabsamount(rs.getFloat("ABS_AMOUNT"));
            SelectedRecord.setterminal(rs.getString("TERMINAL"));
            SelectedRecord.setnetworkid(rs.getString("NETWORK_ID"));
            SelectedRecord.setacquirercurrency(rs.getString("ACQUIRER_CURRENCY"));
            SelectedRecord.setacquirercurrencyid(rs.getInt("ACQUIRER_CURRENCY_ID"));
            SelectedRecord.setacquireramount(rs.getFloat("ACQUIRER_AMOUNT"));
            SelectedRecord.setsettlementcurrency(rs.getString("SETTLEMENT_CURRENCY"));
            SelectedRecord.setsettlementcurrencyid(rs.getInt("SETTLEMENT_CURRENCY_ID"));
            SelectedRecord.setsettlementamount(rs.getFloat("SETTLEMENT_AMOUNT"));
            SelectedRecord.setauthorizationno(rs.getString("AUTHORIZATION_NO"));
            SelectedRecord.setreferenceno(rs.getString("REFERENCE_NO"));
            SelectedRecord.setbank(rs.getString("BANK"));
            SelectedRecord.settransactionid(rs.getInt("TRANSACTION_ID"));
            SelectedRecord.setlicencekey(rs.getString("LICENCE_KEY"));
            SelectedRecord.setnetworkcode(rs.getString("NETWORK_CODE"));
            SelectedRecord.setprocesscode(rs.getString("PROCESS_CODE"));
            SelectedRecord.setareacode(rs.getString("AREA_CODE"));
            SelectedRecord.setreportid(rs.getString("REPORT_ID"));
            SelectedRecord.setnetworkcomm(rs.getInt("NETWORK_COMM"));
            SelectedRecord.setnetworkcommflag(rs.getString("NETWORK_COMM_FLAG"));
            SelectedRecord.setfeelevel(rs.getString("FEE_LEVEL"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findmatchingtype(Object... obj) throws Throwable {
        super.preSelect();
        matchingerrorsdataDTOInter RecordToSelect = (matchingerrorsdataDTOInter) obj[0];
        String selectStat = "select id from matching_type where (type1= $recordtype or type2 = $recordtype)";
        selectStat = selectStat.replace("$recordtype", "" + RecordToSelect.getrecordtype());
        ResultSet rs = executeQuery(selectStat);
        List<String> Records = new ArrayList<String>();
        while (rs.next()) {
            Records.add(rs.getString("id"));
        }
        super.postSelect(rs);
        return Records;
    }
}
