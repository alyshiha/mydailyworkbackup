package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.systemtableDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class systemtableDAO extends BaseDAO implements systemtableDAOInter {

    protected systemtableDAO() {
        super();
        super.setTableName("SYSTEM_TABLE");
    }
  public Object findRecordexport(String Path) throws Throwable {
        super.preSelect();        
        String selectStat = "Select PARAMETER_NAME,PARAMETER_VALUE,PARAMETER_DATA_TYPE From $table"
                + "  where  PARAMETER_NAME= '$parametername'";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$parametername", "" + Path);
        ResultSet rs = executeQuery(selectStat);
        systemtableDTOInter SelectedRecord = DTOFactory.createsystemtableDTO();
        while (rs.next()) {
            SelectedRecord.setparametername(rs.getString("PARAMETER_NAME"));
            SelectedRecord.setparametervalue(rs.getString("PARAMETER_VALUE"));
            SelectedRecord.setparameterdatatype(rs.getInt("PARAMETER_DATA_TYPE"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        systemtableDTOInter RecordToSelect = (systemtableDTOInter) obj[0];
        String selectStat = "Select PARAMETER_NAME,PARAMETER_VALUE,PARAMETER_DATA_TYPE From $table"
                + "  where  PARAMETER_NAME= '$parametername'";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$parametername", "" + RecordToSelect.getparametername());
        ResultSet rs = executeQuery(selectStat);
        systemtableDTOInter SelectedRecord = DTOFactory.createsystemtableDTO();
        while (rs.next()) {
            SelectedRecord.setparametername(rs.getString("PARAMETER_NAME"));
            SelectedRecord.setparametervalue(rs.getString("PARAMETER_VALUE"));
            SelectedRecord.setparameterdatatype(rs.getInt("PARAMETER_DATA_TYPE"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public String deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        systemtableDTOInter RecordToDelete = (systemtableDTOInter) obj[0];
        String msg = ValidateNull(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  PARAMETER_NAME= upper('$parametername')";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$parametername", "" + RecordToDelete.getparametername());
            deleteStat = deleteStat.replace("$parametervalue", "" + RecordToDelete.getparametervalue());
            deleteStat = deleteStat.replace("$parameterdatatype", "" + RecordToDelete.getparameterdatatype());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " Record Has Been Deleted";
            super.postUpdate("Deleting A Parameter With Name " + RecordToDelete.getparametername(), false);
        }
        return msg;
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        systemtableDTOInter RecordToInsert = (systemtableDTOInter) obj[0];

        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
//RecordToInsert.setid(super.generateSequence(super.getTableName()));
            String insertStat = "insert into $table"
                    + " (PARAMETER_NAME,PARAMETER_VALUE,PARAMETER_DATA_TYPE) "
                    + " values "
                    + " ('$parametername','$parametervalue',$parameterdatatype)";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$parametername", "" + RecordToInsert.getparametername());
            insertStat = insertStat.replace("$parametervalue", "" + RecordToInsert.getparametervalue());
            insertStat = insertStat.replace("$parameterdatatype", "" + RecordToInsert.getparameterdatatype());
            msg = super.executeUpdate(insertStat);
            msg = msg + " Row Has Been Inserted";
            super.postUpdate("Adding New Parameter Record With Name " + RecordToInsert.getparametername(), false);
        }
        return msg;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select PARAMETER_NAME,PARAMETER_VALUE,PARAMETER_DATA_TYPE From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<systemtableDTOInter> Records = new ArrayList<systemtableDTOInter>();
        while (rs.next()) {
            systemtableDTOInter SelectedRecord = DTOFactory.createsystemtableDTO();
            SelectedRecord.setparametername(rs.getString("PARAMETER_NAME"));
            SelectedRecord.setparametervalue(rs.getString("PARAMETER_VALUE"));
            SelectedRecord.setparameterdatatype(rs.getInt("PARAMETER_DATA_TYPE"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String ValidateNullList(Object... obj) {
        List<systemtableDTOInter> AllRecords = (List<systemtableDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (systemtableDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getparametername() == null || RecordToInsert.getparametername().equals("")) {
                Validate = Validate + "," + String.valueOf(i);
            }
            if (RecordToInsert.getparametervalue() == null || RecordToInsert.getparametervalue().equals("")) {
                Validate = Validate + "," + String.valueOf(i);
            }
            if (RecordToInsert.getparameterdatatype() == 0) {
                Validate = Validate + "," + String.valueOf(i);
            }
            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");

            Validate = "Record With Index " + Validate + " Contain Null Name Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    public String ValidateNull(Object... obj) {
        systemtableDTOInter RecordToInsert = (systemtableDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getparametername() == null || RecordToInsert.getparametername().equals("")) {
            Validate = Validate + " Name is a required field";
        }
        if (RecordToInsert.getparametervalue() == null || RecordToInsert.getparametervalue().equals("")) {
            Validate = Validate + " Value is a required field";
        }
        if (RecordToInsert.getparameterdatatype() == 0) {
            Validate = Validate + " Feild  datatype is a requiered field";
        }
        if (!Validate.equals("Validate")) {
            Validate.replace("Validate", "");
        }
        return Validate;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<systemtableDTOInter> entities = (List<systemtableDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Users Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "update SYSTEM_TABLE set PARAMETER_VALUE = ?,PARAMETER_DATA_TYPE = ? where PARAMETER_NAME = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        systemtableDTOInter RecordtoInsert = entities.get(i);
                        statement.setString(1, RecordtoInsert.getparametervalue());
                        statement.setInt(2, RecordtoInsert.getparameterdatatype());
                        statement.setString(3, RecordtoInsert.getparametername());

                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                           CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
