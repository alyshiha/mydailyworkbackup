package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import com.ev.Bingo.bus.dto.cardfiletemplateDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.filetemplateheaderDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class filetemplateheaderDAO extends BaseDAO implements filetemplateheaderDAOInter {

    protected filetemplateheaderDAO() {
        super();
        super.setTableName("HOST_FILE_TEMPLATE_HEADER");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        filetemplateheaderDTOInter RecordToInsert = (filetemplateheaderDTOInter) obj[0];
        String TableName = (String) obj[1];
        String MSG = "";
        if (!"NETWORK_FILE_TEMPLATE".equals(TableName)) {
            RecordToInsert.setid(super.generateSequence(TableName + "_HDR"));
        } else {
            RecordToInsert.setid(super.generateSequence("NETWORK_FILE_TEMPLATE" + "_HDR"));
        }
        String insertStat = "insert into $table"
                + " (ID,TEMPLATE,COLUMN_ID,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LINE_NO) "
                + " values "
                + " ('$id','$template','$columnid','$position','$format','$format2','$datatype','$length','$lineno')";
        insertStat = insertStat.replace("$table", "" + TableName + "_HEADER");
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$template", "" + RecordToInsert.gettemplate());
        insertStat = insertStat.replace("$columnid", "" + RecordToInsert.getcolumnid());
        insertStat = insertStat.replace("$position", "" + RecordToInsert.getposition());
        insertStat = insertStat.replace("$format2", "" + RecordToInsert.getformat2());
        insertStat = insertStat.replace("$format", "" + RecordToInsert.getformat());
        insertStat = insertStat.replace("$datatype", "" + RecordToInsert.getdatatype());
        insertStat = insertStat.replace("$length", "" + RecordToInsert.getlength());
        insertStat = insertStat.replace("$lineno", "" + RecordToInsert.getlineno());
        MSG = super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To HOST_FILE_TEMPLATE_HEADER", false);
        return MSG;
    }

    public Boolean ValidateNull(Object... obj) {
        filetemplateheaderDTOInter RecordToInsert = (filetemplateheaderDTOInter) obj[0];
        if (RecordToInsert.getid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.gettemplate() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getcolumnid() == 0) {
            return Boolean.FALSE;
        }

        if (RecordToInsert.getdatatype() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;

    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        filetemplateheaderDTOInter RecordToUpdate = (filetemplateheaderDTOInter) obj[0];
        String UpdateStat = "update $table set "
                + " (ID= $id,TEMPLATE= $template,COLUMN_ID= $columnid,POSITION= $position,FORMAT= '$format',FORMAT2= '$format2',DATA_TYPE= $datatype,LENGTH= $length,LINE_NO= $lineno) "
                + "  where  ID= $id";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$id", "" + RecordToUpdate.getid());
        UpdateStat = UpdateStat.replace("$template", "" + RecordToUpdate.gettemplate());
        UpdateStat = UpdateStat.replace("$columnid", "" + RecordToUpdate.getcolumnid());
        UpdateStat = UpdateStat.replace("$position", "" + RecordToUpdate.getposition());
        UpdateStat = UpdateStat.replace("$format", "" + RecordToUpdate.getformat());
        UpdateStat = UpdateStat.replace("$format2", "" + RecordToUpdate.getformat2());
        UpdateStat = UpdateStat.replace("$datatype", "" + RecordToUpdate.getdatatype());
        UpdateStat = UpdateStat.replace("$length", "" + RecordToUpdate.getlength());
        UpdateStat = UpdateStat.replace("$lineno", "" + RecordToUpdate.getlineno());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table HOST_FILE_TEMPLATE_HEADER", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        filetemplateheaderDTOInter RecordToDelete = (filetemplateheaderDTOInter) obj[0];
        String TableName = (String) obj[1];
        String MSG = "";
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + TableName + "_HEADER");
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        MSG = super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table HOST_FILE_TEMPLATE_HEADER", false);
        return MSG;
    }

    public String deleteallrecord(Object... obj) throws Throwable {
        cardfiletemplateDTOInter Record = (cardfiletemplateDTOInter) obj[0];
        String TableName = (String) obj[1];
        super.preUpdate();
        String deleteStat = "delete from $table WHERE TEMPLATE = $TEMPLATE";
        deleteStat = deleteStat.replace("$table", "" + TableName + "_HEADER");
        deleteStat = deleteStat.replace("$TEMPLATE", "" + Record.getid() );
        String msg = super.executeUpdate(deleteStat);
        return msg;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        filetemplateheaderDTOInter RecordToSelect = (filetemplateheaderDTOInter) obj[0];
        String selectStat = "Select ID,TEMPLATE,COLUMN_ID,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LINE_NO From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        filetemplateheaderDTOInter SelectedRecord = DTOFactory.createfiletemplateheaderDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.settemplate(rs.getInt("TEMPLATE"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.setposition(rs.getString("POSITION"));
            SelectedRecord.setformat(rs.getString("FORMAT"));
            SelectedRecord.setformat2(rs.getString("FORMAT2"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));
            SelectedRecord.setlength(rs.getString("LENGTH"));
            SelectedRecord.setlineno(rs.getString("LINE_NO"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        String TableName = (String) obj[0];
        cardfiletemplateDTOInter SearchTemplate = (cardfiletemplateDTOInter) obj[1];
        String selectStat = "Select ID,TEMPLATE,COLUMN_ID,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LINE_NO From $table"
                + "  where  TEMPLATE= $id";
        selectStat = selectStat.replace("$table", "" + TableName + "_HEADER");
        selectStat = selectStat.replace("$id", "" + SearchTemplate.getid());
        ResultSet rs = executeQuery(selectStat);
        List<filetemplateheaderDTOInter> Records = new ArrayList<filetemplateheaderDTOInter>();
        while (rs.next()) {
            filetemplateheaderDTOInter SelectedRecord = DTOFactory.createfiletemplateheaderDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.settemplate(rs.getInt("TEMPLATE"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.setposition(rs.getString("POSITION"));
            SelectedRecord.setformat(rs.getString("FORMAT"));
            SelectedRecord.setformat2(rs.getString("FORMAT2"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));
            SelectedRecord.setlength(rs.getString("LENGTH"));
            SelectedRecord.setlineno(rs.getString("LINE_NO"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,TEMPLATE,COLUMN_ID,POSITION,FORMAT,FORMAT2,DATA_TYPE,LENGTH,LINE_NO From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<filetemplateheaderDTOInter> Records = new ArrayList<filetemplateheaderDTOInter>();
        while (rs.next()) {
            filetemplateheaderDTOInter SelectedRecord = DTOFactory.createfiletemplateheaderDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.settemplate(rs.getInt("TEMPLATE"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.setposition(rs.getString("POSITION"));
            SelectedRecord.setformat(rs.getString("FORMAT"));
            SelectedRecord.setformat2(rs.getString("FORMAT2"));
            SelectedRecord.setdatatype(rs.getInt("DATA_TYPE"));
            SelectedRecord.setlength(rs.getString("LENGTH"));
            SelectedRecord.setlineno(rs.getString("LINE_NO"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<filetemplateheaderDTOInter> entities = (List<filetemplateheaderDTOInter>) obj[0];
        String TableName = (String) obj[1];
        String insertStat = "UPDATE $table SET COLUMN_ID = ?,POSITION = ?,FORMAT = ?,FORMAT2 = ?,DATA_TYPE = ?,LENGTH = ?,LINE_NO = ? WHERE ID = ?";
        insertStat = insertStat.replace("$table", TableName + "_HEADER");
        Connection connection = null;
        PreparedStatement statement = null;
        String mess = "", mess2 = "";
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.size(); i++) {
                filetemplateheaderDTOInter RecordtoInsert = entities.get(i);
                statement.setInt(1, RecordtoInsert.getcolumnid());
                statement.setString(2, RecordtoInsert.getposition());
                statement.setString(3, RecordtoInsert.getformat());
                statement.setString(4, RecordtoInsert.getformat2());
                statement.setInt(5, RecordtoInsert.getdatatype());
                statement.setString(6, RecordtoInsert.getlength());
                statement.setString(7, RecordtoInsert.getlineno());
                statement.setInt(8, RecordtoInsert.getid());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            int[] numUpdates = statement.executeBatch();
            Integer valid = 0, notvalid = 0;

            for (int i = 0; i < numUpdates.length; i++) {
                if (numUpdates[i] == -2) {
                    valid++;
                    mess = valid + " Record Has been Updated";
                } else {
                    notvalid++;
                    mess2 = notvalid + " can`t be updated";
                }
            }
            if (!mess2.equals("")) {
                mess = mess + "," + mess2;
            }
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
                 CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
        return mess;
    }
}
