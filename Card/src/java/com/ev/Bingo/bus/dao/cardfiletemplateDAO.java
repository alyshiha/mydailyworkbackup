package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import com.ev.Bingo.bus.dto.cardfiletemplateDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.cardfiletemplateDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class cardfiletemplateDAO extends BaseDAO implements cardfiletemplateDAOInter {

    protected cardfiletemplateDAO() {
        super();
        super.setTableName("NETWORK_FILE_TEMPLATE");
    }

    public int insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        cardfiletemplateDTOInter RecordToInsert = (cardfiletemplateDTOInter) obj[0];
        String TableName = (String) obj[1];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "";
        if (TableName.contains("NETW")) {
            insertStat = "insert into $table"
                    + " (ID,NAME,PROCESSING_TYPE,NUMBER_OF_LINES,LOADING_FOLDER,STARTING_DATA_TYPE,STARTING_FORMAT,STARTING_POSITION,BACKUP_FOLDER,COPY_FOLDER,IGNORED_LINES,STARTING_LENGTH,DATE_SEPARATOR,DATE_SEPARATOR_POS1,DATE_SEPARATOR_POS2,SERVER_FOLDER,HEADER_DATA_TYPE,HEADER_FORMAT,HEADER_POSITION,HEADER_LENGTH,HEADER_DATE_SEPARATOR,HEADER_DATE_SEPARATOR_POS1,HEADER_DATE_SEPARATOR_POS2,HEADER_STRING,STARTING_VALUE,ACTIVE,SEPARATOR,TAGS_ENDING_DATA_TYPE,TAGS_ENDING_FORMAT,TAGS_ENDING_POSITION,TAGS_ENDING_LENGTH,TAGS_ENDING_VALUE,NETWORK_ID,HEADER_LINES,MAX_TAG_LINES) "
                    + " values "
                    + " ($id,'$name','$processingtype','$numberoflines','$loadingfolder','$startingdatatype','$startingformat','$startingposition','$backupfolder','$copyfolder','$ignoredlines','$startinglength','$dateseparator','$dateseparatorpos1','$dateseparatorpos2','$serverfolder','$headerdatatype','$headerformat','$headerposition','$headerlength','$headerdateseparator','$headerdateseparatorpos1','$headerdateseparatorpos2','$headerstring','$startingvalue',$active,'$separator','$tagsendingdatatype','$tagsendingformat','$tagsendingposition','$tagsendinglength','$tagsendingvalue','$networkid','$headerlines','$maxtaglines')";
        } else {
            insertStat = "insert into $table"
                    + " (ID,NAME,PROCESSING_TYPE,NUMBER_OF_LINES,LOADING_FOLDER,STARTING_DATA_TYPE,STARTING_FORMAT,STARTING_POSITION,BACKUP_FOLDER,COPY_FOLDER,IGNORED_LINES,STARTING_LENGTH,DATE_SEPARATOR,DATE_SEPARATOR_POS1,DATE_SEPARATOR_POS2,SERVER_FOLDER,HEADER_DATA_TYPE,HEADER_FORMAT,HEADER_POSITION,HEADER_LENGTH,HEADER_DATE_SEPARATOR,HEADER_DATE_SEPARATOR_POS1,HEADER_DATE_SEPARATOR_POS2,HEADER_STRING,STARTING_VALUE,ACTIVE,SEPARATOR,TAGS_ENDING_DATA_TYPE,TAGS_ENDING_FORMAT,TAGS_ENDING_POSITION,TAGS_ENDING_LENGTH,TAGS_ENDING_VALUE,HEADER_LINES,MAX_TAG_LINES) "
                    + " values "
                    + " ($id,'$name','$processingtype','$numberoflines','$loadingfolder','$startingdatatype','$startingformat','$startingposition','$backupfolder','$copyfolder','$ignoredlines','$startinglength','$dateseparator','$dateseparatorpos1','$dateseparatorpos2','$serverfolder','$headerdatatype','$headerformat','$headerposition','$headerlength','$headerdateseparator','$headerdateseparatorpos1','$headerdateseparatorpos2','$headerstring','$startingvalue',$active,'$separator','$tagsendingdatatype','$tagsendingformat','$tagsendingposition','$tagsendinglength','$tagsendingvalue','$headerlines','$maxtaglines')";
        }
        insertStat = insertStat.replace("$table", "" + TableName);
        insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
        insertStat = insertStat.replace("$name", "" + RecordToInsert.getname());
        insertStat = insertStat.replace("$processingtype", "" + RecordToInsert.getprocessingtype());
        insertStat = insertStat.replace("$numberoflines", "" + RecordToInsert.getnumberoflines());
        insertStat = insertStat.replace("$loadingfolder", "" + RecordToInsert.getloadingfolder());
        insertStat = insertStat.replace("$startingdatatype", "" + RecordToInsert.getstartingdatatype());
        insertStat = insertStat.replace("$startingformat", "" + RecordToInsert.getstartingformat());
        insertStat = insertStat.replace("$startingposition", "" + RecordToInsert.getstartingposition());
        insertStat = insertStat.replace("$backupfolder", "" + RecordToInsert.getbackupfolder());
        insertStat = insertStat.replace("$copyfolder", "" + RecordToInsert.getcopyfolder());
        insertStat = insertStat.replace("$ignoredlines", "" + RecordToInsert.getignoredlines());
        insertStat = insertStat.replace("$startinglength", "" + RecordToInsert.getstartinglength());
        insertStat = insertStat.replace("$dateseparatorpos1", "" + RecordToInsert.getdateseparatorpos1());
        insertStat = insertStat.replace("$dateseparatorpos2", "" + RecordToInsert.getdateseparatorpos2());
        insertStat = insertStat.replace("$dateseparator", "" + RecordToInsert.getdateseparator());
        insertStat = insertStat.replace("$serverfolder", "" + RecordToInsert.getserverfolder());
        insertStat = insertStat.replace("$headerdatatype", "" + RecordToInsert.getheaderdatatype());
        insertStat = insertStat.replace("$headerformat", "" + RecordToInsert.getheaderformat());
        insertStat = insertStat.replace("$headerposition", "" + RecordToInsert.getheaderposition());
        insertStat = insertStat.replace("$headerlength", "" + RecordToInsert.getheaderlength());
        insertStat = insertStat.replace("$headerdateseparatorpos1", "" + RecordToInsert.getheaderdateseparatorpos1());
        insertStat = insertStat.replace("$headerdateseparatorpos2", "" + RecordToInsert.getheaderdateseparatorpos2());
        insertStat = insertStat.replace("$headerdateseparator", "" + RecordToInsert.getheaderdateseparator());
        insertStat = insertStat.replace("$headerstring", "" + RecordToInsert.getheaderstring());
        insertStat = insertStat.replace("$startingvalue", "" + RecordToInsert.getstartingvalue());
        if (RecordToInsert.getactiveB() == Boolean.TRUE) {
            RecordToInsert.setactive(1);
        } else {
            RecordToInsert.setactive(2);
        }
        insertStat = insertStat.replace("$active", "" + RecordToInsert.getactive());
        insertStat = insertStat.replace("$separator", "" + RecordToInsert.getseparator());
        insertStat = insertStat.replace("$tagsendingdatatype", "" + RecordToInsert.gettagsendingdatatype());
        insertStat = insertStat.replace("$tagsendingformat", "" + RecordToInsert.getTagsendfor());
        insertStat = insertStat.replace("$tagsendingposition", "" + RecordToInsert.gettagsendingposition());
        insertStat = insertStat.replace("$tagsendinglength", "" + RecordToInsert.gettagsendinglength());
        insertStat = insertStat.replace("$tagsendingvalue", "" + RecordToInsert.gettagsendingvalue());
        insertStat = insertStat.replace("$networkid", "" + RecordToInsert.getnetworkid());
        insertStat = insertStat.replace("$headerlines", "" + RecordToInsert.getheaderlines());
        insertStat = insertStat.replace("$maxtaglines", "" + RecordToInsert.getmaxtaglines());
        super.executeUpdate(insertStat);
        super.postUpdate("Add New Record To NETWORK_FILE_TEMPLATE", false);
        return RecordToInsert.getid();
    }

    public Boolean ValidateNull(Object... obj) {
        cardfiletemplateDTOInter RecordToInsert = (cardfiletemplateDTOInter) obj[0];
        if (RecordToInsert.getid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getname() == null && RecordToInsert.getname().equals("")) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getprocessingtype() == 0) {
            return Boolean.FALSE;
        }

        if (RecordToInsert.getloadingfolder() == null && RecordToInsert.getloadingfolder().equals("")) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getbackupfolder() == null && RecordToInsert.getbackupfolder().equals("")) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getcopyfolder() == null && RecordToInsert.getcopyfolder().equals("")) {
            return Boolean.FALSE;
        }

        if (RecordToInsert.getactive() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getnetworkid() == 0) {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        cardfiletemplateDTOInter RecordToUpdate = (cardfiletemplateDTOInter) obj[0];
        String TableName = (String) obj[1];
        String MSG = "";
        String UpdateStat = "";
        if (TableName.contains("NETW")) {
            UpdateStat = "update $table set "
                    + " NAME= '$name',PROCESSING_TYPE= '$processingtype',NUMBER_OF_LINES= '$numberoflines',LOADING_FOLDER= '$loadingfolder',STARTING_DATA_TYPE= '$startingdatatype',STARTING_FORMAT= '$startingformat',STARTING_POSITION= '$startingposition',BACKUP_FOLDER= '$backupfolder',COPY_FOLDER= '$copyfolder',IGNORED_LINES= '$ignoredlines',STARTING_LENGTH= '$startinglength',DATE_SEPARATOR_POS1= '$dateseparatorpos1',DATE_SEPARATOR= '$dateseparator',DATE_SEPARATOR_POS2= '$dateseparatorpos2',SERVER_FOLDER= '$serverfolder',HEADER_DATA_TYPE= '$headerdatatype',HEADER_FORMAT= '$headerformat',HEADER_POSITION= '$headerposition',HEADER_LENGTH= '$headerlength',HEADER_DATE_SEPARATOR= '$headerdateseparator',HEADER_DATE_SEPARATOR_POS1= '$headerdateseparatorpos1',HEADER_DATE_SEPARATOR_POS2= '$headerdateseparatorpos2',HEADER_STRING= '$headerstring',STARTING_VALUE= '$startingvalue',ACTIVE= $active,SEPARATOR= '$separator',TAGS_ENDING_DATA_TYPE= '$tagsendingdatatype',TAGS_ENDING_FORMAT= '$tagsendingformat',TAGS_ENDING_POSITION= '$tagsendingposition',TAGS_ENDING_LENGTH= '$tagsendinglength',TAGS_ENDING_VALUE= '$tagsendingvalue',NETWORK_ID= '$networkid',HEADER_LINES= '$headerlines',MAX_TAG_LINES= '$maxtaglines' "
                    + "  where  ID= $id";
        } else {
            UpdateStat = "update $table set "
                    + " NAME= '$name',PROCESSING_TYPE= '$processingtype',NUMBER_OF_LINES= '$numberoflines',LOADING_FOLDER= '$loadingfolder',STARTING_DATA_TYPE= '$startingdatatype',STARTING_FORMAT= '$startingformat',STARTING_POSITION= '$startingposition',BACKUP_FOLDER= '$backupfolder',COPY_FOLDER= '$copyfolder',IGNORED_LINES= '$ignoredlines',STARTING_LENGTH= '$startinglength',DATE_SEPARATOR_POS1= '$dateseparatorpos1',DATE_SEPARATOR= '$dateseparator',DATE_SEPARATOR_POS2= '$dateseparatorpos2',SERVER_FOLDER= '$serverfolder',HEADER_DATA_TYPE= '$headerdatatype',HEADER_FORMAT= '$headerformat',HEADER_POSITION= '$headerposition',HEADER_LENGTH= '$headerlength',HEADER_DATE_SEPARATOR= '$headerdateseparator',HEADER_DATE_SEPARATOR_POS1= '$headerdateseparatorpos1',HEADER_DATE_SEPARATOR_POS2= '$headerdateseparatorpos2',HEADER_STRING= '$headerstring',STARTING_VALUE= '$startingvalue',ACTIVE= $active,SEPARATOR= '$separator',TAGS_ENDING_DATA_TYPE= '$tagsendingdatatype',TAGS_ENDING_FORMAT= '$tagsendingformat',TAGS_ENDING_POSITION= '$tagsendingposition',TAGS_ENDING_LENGTH= '$tagsendinglength',TAGS_ENDING_VALUE= '$tagsendingvalue',HEADER_LINES= '$headerlines',MAX_TAG_LINES= '$maxtaglines' "
                    + "  where  ID= $id";
        }
        UpdateStat = UpdateStat.replace("$table", "" + TableName);
        UpdateStat = UpdateStat.replace("$id", "" + RecordToUpdate.getid());
        UpdateStat = UpdateStat.replace("$name", "" + RecordToUpdate.getname());
        UpdateStat = UpdateStat.replace("$processingtype", "" + RecordToUpdate.getprocessingtype());
        UpdateStat = UpdateStat.replace("$numberoflines", "" + RecordToUpdate.getnumberoflines());
        UpdateStat = UpdateStat.replace("$loadingfolder", "" + RecordToUpdate.getloadingfolder());
        UpdateStat = UpdateStat.replace("$startingdatatype", "" + RecordToUpdate.getstartingdatatype());
        UpdateStat = UpdateStat.replace("$startingformat", "" + RecordToUpdate.getstartingformat());
        UpdateStat = UpdateStat.replace("$startingposition", "" + RecordToUpdate.getstartingposition());
        UpdateStat = UpdateStat.replace("$backupfolder", "" + RecordToUpdate.getbackupfolder());
        UpdateStat = UpdateStat.replace("$copyfolder", "" + RecordToUpdate.getcopyfolder());
        UpdateStat = UpdateStat.replace("$ignoredlines", "" + RecordToUpdate.getignoredlines());
        UpdateStat = UpdateStat.replace("$startinglength", "" + RecordToUpdate.getstartinglength());
        UpdateStat = UpdateStat.replace("$tagsendingformat", "" + RecordToUpdate.getTagsendfor());
        UpdateStat = UpdateStat.replace("$dateseparatorpos1", "" + RecordToUpdate.getdateseparatorpos1());
        UpdateStat = UpdateStat.replace("$dateseparatorpos2", "" + RecordToUpdate.getdateseparatorpos2());
        UpdateStat = UpdateStat.replace("$dateseparator", "" + RecordToUpdate.getdateseparator());
        UpdateStat = UpdateStat.replace("$serverfolder", "" + RecordToUpdate.getserverfolder());
        UpdateStat = UpdateStat.replace("$headerdatatype", "" + RecordToUpdate.getheaderdatatype());
        UpdateStat = UpdateStat.replace("$headerformat", "" + RecordToUpdate.getheaderformat());
        UpdateStat = UpdateStat.replace("$headerposition", "" + RecordToUpdate.getheaderposition());
        UpdateStat = UpdateStat.replace("$headerlength", "" + RecordToUpdate.getheaderlength());

        UpdateStat = UpdateStat.replace("$headerdateseparatorpos1", "" + RecordToUpdate.getheaderdateseparatorpos1());
        UpdateStat = UpdateStat.replace("$headerdateseparatorpos2", "" + RecordToUpdate.getheaderdateseparatorpos2());
        UpdateStat = UpdateStat.replace("$headerdateseparator", "" + RecordToUpdate.getheaderdateseparator());
        UpdateStat = UpdateStat.replace("$headerstring", "" + RecordToUpdate.getheaderstring());
        UpdateStat = UpdateStat.replace("$startingvalue", "" + RecordToUpdate.getstartingvalue());
        if (RecordToUpdate.getactiveB() == Boolean.TRUE) {
            RecordToUpdate.setactive(1);
        } else {
            RecordToUpdate.setactive(2);
        }
        UpdateStat = UpdateStat.replace("$active", "" + RecordToUpdate.getactive());
        UpdateStat = UpdateStat.replace("$separator", "" + RecordToUpdate.getseparator());
        UpdateStat = UpdateStat.replace("$tagsendingdatatype", "" + RecordToUpdate.gettagsendingdatatype());
        UpdateStat = UpdateStat.replace("$tagsendingformat", "" + RecordToUpdate.getTagsendfor());
        UpdateStat = UpdateStat.replace("$tagsendingposition", "" + RecordToUpdate.gettagsendingposition());
        UpdateStat = UpdateStat.replace("$tagsendinglength", "" + RecordToUpdate.gettagsendinglength());
        UpdateStat = UpdateStat.replace("$tagsendingvalue", "" + RecordToUpdate.gettagsendingvalue());
        UpdateStat = UpdateStat.replace("$networkid", "" + RecordToUpdate.getnetworkid());
        UpdateStat = UpdateStat.replace("$headerlines", "" + RecordToUpdate.getheaderlines());
        UpdateStat = UpdateStat.replace("$maxtaglines", "" + RecordToUpdate.getmaxtaglines());
        MSG = super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table NETWORK_FILE_TEMPLATE", false);
        return MSG;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        String MSG = "";
        cardfiletemplateDTOInter RecordToDelete = (cardfiletemplateDTOInter) obj[0];
        String TableName = (String) obj[1];
        String deleteStat = "delete from $table "
                + "  where  ID= $id";
        deleteStat = deleteStat.replace("$table", "" + TableName);
        deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
        MSG = super.executeUpdate(deleteStat);
        super.postUpdate("Delete Record In Table NETWORK_FILE_TEMPLATE", false);
        return MSG;
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table NETWORK_FILE_TEMPLATE", false);
        return null;
    }

    public int findRecord(Object... obj) throws Throwable {
        super.preSelect();
        cardfiletemplateDTOInter RecordToSelect = (cardfiletemplateDTOInter) obj[0];
        String TableName = (String) obj[1];
        String selectStat = "Select Count(*) valuecount From $table"
                + "  where  UPPER(NAME) = UPPER('$name') OR UPPER(LOADING_FOLDER) = UPPER('$loadingfolder') "
                + "  OR UPPER(BACKUP_FOLDER) = UPPER('$backupfolder') OR UPPER(COPY_FOLDER) = UPPER('$copyfolder')";
        selectStat = selectStat.replace("$table", "" + TableName);
        selectStat = selectStat.replace("$name", "" + RecordToSelect.getname());
        selectStat = selectStat.replace("$loadingfolder", "" + RecordToSelect.getloadingfolder());
        selectStat = selectStat.replace("$backupfolder", "" + RecordToSelect.getbackupfolder());
        selectStat = selectStat.replace("$copyfolder", "" + RecordToSelect.getcopyfolder());
        ResultSet rs = executeQuery(selectStat);
        int count = 0;
        while (rs.next()) {
             count = rs.getInt("valuecount");
        }
        super.postSelect(rs);
        return count;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        cardfiletemplateDTOInter RecordToSelect = (cardfiletemplateDTOInter) obj[0];
        String selectStat = "Select ID,NAME,PROCESSING_TYPE,NUMBER_OF_LINES,LOADING_FOLDER,STARTING_DATA_TYPE,STARTING_FORMAT,STARTING_POSITION,BACKUP_FOLDER,COPY_FOLDER,IGNORED_LINES,STARTING_LENGTH,DATE_SEPARATOR,DATE_SEPARATOR_POS1,DATE_SEPARATOR_POS2,SERVER_FOLDER,HEADER_DATA_TYPE,HEADER_FORMAT,HEADER_POSITION,HEADER_LENGTH,HEADER_DATE_SEPARATOR,HEADER_DATE_SEPARATOR_POS1,HEADER_DATE_SEPARATOR_POS2,HEADER_STRING,STARTING_VALUE,ACTIVE,SEPARATOR,TAGS_ENDING_DATA_TYPE,TAGS_ENDING_FORMAT,TAGS_ENDING_POSITION,TAGS_ENDING_LENGTH,TAGS_ENDING_VALUE,NETWORK_ID,HEADER_LINES,MAX_TAG_LINES From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<cardfiletemplateDTOInter> Records = new ArrayList<cardfiletemplateDTOInter>();
        while (rs.next()) {
            cardfiletemplateDTOInter SelectedRecord = DTOFactory.createcardfiletemplateDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setprocessingtype(rs.getInt("PROCESSING_TYPE"));
            SelectedRecord.setnumberoflines(rs.getString("NUMBER_OF_LINES"));
            SelectedRecord.setloadingfolder(rs.getString("LOADING_FOLDER"));
            SelectedRecord.setstartingdatatype(rs.getInt("STARTING_DATA_TYPE"));
            SelectedRecord.setstartingformat(rs.getString("STARTING_FORMAT"));
            SelectedRecord.setstartingposition(rs.getString("STARTING_POSITION"));
            SelectedRecord.setbackupfolder(rs.getString("BACKUP_FOLDER"));
            SelectedRecord.setcopyfolder(rs.getString("COPY_FOLDER"));
            SelectedRecord.setignoredlines(rs.getString("IGNORED_LINES"));
            SelectedRecord.setstartinglength(rs.getString("STARTING_LENGTH"));
            SelectedRecord.setdateseparator(rs.getString("DATE_SEPARATOR"));
            SelectedRecord.setdateseparatorpos1(rs.getString("DATE_SEPARATOR_POS1"));
            SelectedRecord.setdateseparatorpos2(rs.getString("DATE_SEPARATOR_POS2"));
            SelectedRecord.setserverfolder(rs.getString("SERVER_FOLDER"));
            SelectedRecord.setheaderdatatype(rs.getInt("HEADER_DATA_TYPE"));
            SelectedRecord.setheaderformat(rs.getString("HEADER_FORMAT"));
            SelectedRecord.setheaderposition(rs.getString("HEADER_POSITION"));
            SelectedRecord.setheaderlength(rs.getString("HEADER_LENGTH"));
            SelectedRecord.setheaderdateseparator(rs.getString("HEADER_DATE_SEPARATOR"));
            SelectedRecord.setheaderdateseparatorpos1(rs.getString("HEADER_DATE_SEPARATOR_POS1"));
            SelectedRecord.setheaderdateseparatorpos2(rs.getString("HEADER_DATE_SEPARATOR_POS2"));
            SelectedRecord.setheaderstring(rs.getString("HEADER_STRING"));
            SelectedRecord.setstartingvalue(rs.getString("STARTING_VALUE"));
            SelectedRecord.setactive(rs.getInt("ACTIVE"));
            SelectedRecord.setseparator(rs.getString("SEPARATOR"));
            SelectedRecord.settagsendingdatatype(rs.getInt("TAGS_ENDING_DATA_TYPE"));
            SelectedRecord.setTagsendfor(rs.getString("TAGS_ENDING_FORMAT"));
            SelectedRecord.settagsendingposition(rs.getString("TAGS_ENDING_POSITION"));
            SelectedRecord.settagsendinglength(rs.getString("TAGS_ENDING_LENGTH"));
            SelectedRecord.settagsendingvalue(rs.getString("TAGS_ENDING_VALUE"));
            SelectedRecord.setnetworkid(rs.getInt("NETWORK_ID"));
            SelectedRecord.setheaderlines(rs.getString("HEADER_LINES"));
            SelectedRecord.setmaxtaglines(rs.getString("MAX_TAG_LINES"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        String SearchRecords = (String) obj[0];
        super.preSelect();
        String selectStat = "";
        if (SearchRecords.equals("NETWORK_FILE_TEMPLATE")) {
            selectStat = "Select ID,NAME,PROCESSING_TYPE,NUMBER_OF_LINES,LOADING_FOLDER,STARTING_DATA_TYPE,STARTING_FORMAT,STARTING_POSITION,BACKUP_FOLDER,COPY_FOLDER,IGNORED_LINES,STARTING_LENGTH,DATE_SEPARATOR,DATE_SEPARATOR_POS1,DATE_SEPARATOR_POS2,SERVER_FOLDER,HEADER_DATA_TYPE,HEADER_FORMAT,HEADER_POSITION,HEADER_LENGTH,HEADER_DATE_SEPARATOR,HEADER_DATE_SEPARATOR_POS1,HEADER_DATE_SEPARATOR_POS2,HEADER_STRING,STARTING_VALUE,ACTIVE,SEPARATOR,TAGS_ENDING_DATA_TYPE,TAGS_ENDING_FORMAT,TAGS_ENDING_POSITION,TAGS_ENDING_LENGTH,TAGS_ENDING_VALUE,NETWORK_ID,HEADER_LINES,MAX_TAG_LINES From $table";
        } else {
            selectStat = "Select ID,NAME,PROCESSING_TYPE,NUMBER_OF_LINES,LOADING_FOLDER,STARTING_DATA_TYPE,STARTING_FORMAT,STARTING_POSITION,BACKUP_FOLDER,COPY_FOLDER,IGNORED_LINES,STARTING_LENGTH,DATE_SEPARATOR,DATE_SEPARATOR_POS1,DATE_SEPARATOR_POS2,SERVER_FOLDER,HEADER_DATA_TYPE,HEADER_FORMAT,HEADER_POSITION,HEADER_LENGTH,HEADER_DATE_SEPARATOR,HEADER_DATE_SEPARATOR_POS1,HEADER_DATE_SEPARATOR_POS2,HEADER_STRING,STARTING_VALUE,ACTIVE,SEPARATOR,TAGS_ENDING_DATA_TYPE,TAGS_ENDING_FORMAT,TAGS_ENDING_POSITION,TAGS_ENDING_LENGTH,TAGS_ENDING_VALUE,HEADER_LINES,MAX_TAG_LINES From $table";
        }
        System.out.println(selectStat);
        selectStat = selectStat.replace("$table", "" + SearchRecords);
        ResultSet rs = executeQuery(selectStat);
        List<cardfiletemplateDTOInter> Records = new ArrayList<cardfiletemplateDTOInter>();
        while (rs.next()) {
            cardfiletemplateDTOInter SelectedRecord = DTOFactory.createcardfiletemplateDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setprocessingtype(rs.getInt("PROCESSING_TYPE"));
            SelectedRecord.setnumberoflines(rs.getString("NUMBER_OF_LINES"));
            SelectedRecord.setloadingfolder(rs.getString("LOADING_FOLDER"));
            SelectedRecord.setstartingdatatype(rs.getInt("STARTING_DATA_TYPE"));
            SelectedRecord.setstartingformat(rs.getString("STARTING_FORMAT"));
            SelectedRecord.setstartingposition(rs.getString("STARTING_POSITION"));
            SelectedRecord.setbackupfolder(rs.getString("BACKUP_FOLDER"));
            SelectedRecord.setcopyfolder(rs.getString("COPY_FOLDER"));
            SelectedRecord.setignoredlines(rs.getString("IGNORED_LINES"));
            SelectedRecord.setstartinglength(rs.getString("STARTING_LENGTH"));
            SelectedRecord.setdateseparator(rs.getString("DATE_SEPARATOR"));
            SelectedRecord.setdateseparatorpos1(rs.getString("DATE_SEPARATOR_POS1"));
            SelectedRecord.setdateseparatorpos2(rs.getString("DATE_SEPARATOR_POS2"));
            SelectedRecord.setserverfolder(rs.getString("SERVER_FOLDER"));
            SelectedRecord.setheaderdatatype(rs.getInt("HEADER_DATA_TYPE"));
            SelectedRecord.setheaderformat(rs.getString("HEADER_FORMAT"));
            SelectedRecord.setheaderposition(rs.getString("HEADER_POSITION"));
            SelectedRecord.setheaderlength(rs.getString("HEADER_LENGTH"));
            SelectedRecord.setheaderdateseparator(rs.getString("HEADER_DATE_SEPARATOR"));
            SelectedRecord.setheaderdateseparatorpos1(rs.getString("HEADER_DATE_SEPARATOR_POS1"));
            SelectedRecord.setheaderdateseparatorpos2(rs.getString("HEADER_DATE_SEPARATOR_POS2"));
            SelectedRecord.setheaderstring(rs.getString("HEADER_STRING"));
            SelectedRecord.setstartingvalue(rs.getString("STARTING_VALUE"));
            SelectedRecord.setactive(rs.getInt("ACTIVE"));
            if (SelectedRecord.getactive() == 1) {
                SelectedRecord.setactiveB(Boolean.TRUE);
            } else {
                SelectedRecord.setactiveB(Boolean.FALSE);
            }
            SelectedRecord.setseparator(rs.getString("SEPARATOR"));
            SelectedRecord.settagsendingdatatype(rs.getInt("TAGS_ENDING_DATA_TYPE"));
            SelectedRecord.setTagsendfor(rs.getString("TAGS_ENDING_FORMAT"));
            SelectedRecord.settagsendingposition(rs.getString("TAGS_ENDING_POSITION"));
            SelectedRecord.settagsendinglength(rs.getString("TAGS_ENDING_LENGTH"));
            SelectedRecord.settagsendingvalue(rs.getString("TAGS_ENDING_VALUE"));
            if (SearchRecords.equals("NETWORK_FILE_TEMPLATE")) {
                SelectedRecord.setnetworkid(rs.getInt("NETWORK_ID"));
            }
            SelectedRecord.setheaderlines(rs.getString("HEADER_LINES"));
            SelectedRecord.setmaxtaglines(rs.getString("MAX_TAG_LINES"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public void save(Object... obj) throws SQLException, Throwable {
        List<cardfiletemplateDTOInter> entities = (List<cardfiletemplateDTOInter>) obj[0];
        String insertStat = "insert into NETWORK_FILE_TEMPLATE (ID,NAME,PROCESSING_TYPE,NUMBER_OF_LINES,LOADING_FOLDER,STARTING_DATA_TYPE,STARTING_FORMAT,STARTING_POSITION,BACKUP_FOLDER,COPY_FOLDER,IGNORED_LINES,STARTING_LENGTH,DATE_SEPARATOR,DATE_SEPARATOR_POS1,DATE_SEPARATOR_POS2,SERVER_FOLDER,HEADER_DATA_TYPE,HEADER_FORMAT,HEADER_POSITION,HEADER_LENGTH,HEADER_DATE_SEPARATOR,HEADER_DATE_SEPARATOR_POS1,HEADER_DATE_SEPARATOR_POS2,HEADER_STRING,STARTING_VALUE,ACTIVE,SEPARATOR,TAGS_ENDING_DATA_TYPE,TAGS_ENDING_FORMAT,TAGS_ENDING_POSITION,TAGS_ENDING_LENGTH,TAGS_ENDING_VALUE,NETWORK_ID,HEADER_LINES,MAX_TAG_LINES) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        Connection connection = null;
        PreparedStatement statement = null;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            connection = CoonectionHandler.getInstance().getConnection();
            statement = connection.prepareStatement(insertStat);
            for (int i = 0; i < entities.size(); i++) {
                cardfiletemplateDTOInter RecordtoInsert = entities.get(i);
                statement.setInt(1, RecordtoInsert.getid());
                statement.setString(2, RecordtoInsert.getname());
                statement.setInt(3, RecordtoInsert.getprocessingtype());
                statement.setString(4, RecordtoInsert.getnumberoflines());
                statement.setString(5, RecordtoInsert.getloadingfolder());
                statement.setInt(6, RecordtoInsert.getstartingdatatype());
                statement.setString(7, RecordtoInsert.getstartingformat());
                statement.setString(8, RecordtoInsert.getstartingposition());
                statement.setString(9, RecordtoInsert.getbackupfolder());
                statement.setString(10, RecordtoInsert.getcopyfolder());
                statement.setString(11, RecordtoInsert.getignoredlines());
                statement.setString(12, RecordtoInsert.getstartinglength());
                statement.setString(13, RecordtoInsert.getdateseparator());
                statement.setString(14, RecordtoInsert.getdateseparatorpos1());
                statement.setString(15, RecordtoInsert.getdateseparatorpos2());
                statement.setString(16, RecordtoInsert.getserverfolder());
                statement.setInt(17, RecordtoInsert.getheaderdatatype());
                statement.setString(18, RecordtoInsert.getheaderformat());
                statement.setString(19, RecordtoInsert.getheaderposition());
                statement.setString(20, RecordtoInsert.getheaderlength());
                statement.setString(21, RecordtoInsert.getheaderdateseparator());
                statement.setString(22, RecordtoInsert.getheaderdateseparatorpos1());
                statement.setString(23, RecordtoInsert.getheaderdateseparatorpos2());
                statement.setString(24, RecordtoInsert.getheaderstring());
                statement.setString(25, RecordtoInsert.getstartingvalue());
                statement.setInt(26, RecordtoInsert.getactive());
                statement.setString(27, RecordtoInsert.getseparator());
                statement.setInt(28, RecordtoInsert.gettagsendingdatatype());
                statement.setString(29, RecordtoInsert.getTagsendfor());
                statement.setString(30, RecordtoInsert.gettagsendingposition());
                statement.setString(31, RecordtoInsert.gettagsendinglength());
                statement.setString(32, RecordtoInsert.gettagsendingvalue());
                statement.setInt(33, RecordtoInsert.getnetworkid());
                statement.setString(34, RecordtoInsert.getheaderlines());
                statement.setString(35, RecordtoInsert.getmaxtaglines());
                statement.addBatch();
                if ((i + 1) % 1000 == 0) {
                    statement.executeBatch();
                }
            }
            statement.executeBatch();
        } finally {
            if (statement != null) {
                try {
                    connection.commit();
                    statement.close();
                } catch (SQLException logOrIgnore) {
                }
            }
            if (connection != null) {
                try {
              CoonectionHandler.getInstance().returnConnection(connection);
                } catch (SQLException logOrIgnore) {
                }
            }
        }
    }
}
