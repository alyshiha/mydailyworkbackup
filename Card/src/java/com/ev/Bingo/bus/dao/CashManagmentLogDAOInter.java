/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.bus.dto.CashManagmentLogDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface CashManagmentLogDAOInter {

    List<CashManagmentLogDTOInter> findRecord(Integer User, Date Fromdate, Date Todate) throws Throwable;

    Object insert(String Action) throws Throwable;

}
