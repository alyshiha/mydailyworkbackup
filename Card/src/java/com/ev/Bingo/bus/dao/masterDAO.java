/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.settlementsDTOInter;
import com.ev.Bingo.bus.dto.timeFromSwitchDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author shi7a
 */
public class masterDAO extends BaseDAO implements masterDAOInter {

    @Override
    public Object findRecordsIssuer(Date dateFrom, Date dateTo) throws Throwable {
        super.preSelect();
        String selectStat = "  SELECT   'Matched' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              AMOUNT,\n"
                + "           COUNT (1) NO,\n"
                + "           CURRENCY,\n"
                + "           1 ordering\n"
                + "    FROM   MATCHED_DATA\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 2\n"
                + "           AND CARD_NO NOT LIKE '533813%'\n"
                + "           AND REGEXP_LIKE (CARD_NO, '429742|559444|533813')\n"
                + "GROUP BY   CURRENCY\n"
                + "UNION\n"
                + "  SELECT   'Unmatched Switch' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              AMOUNT,\n"
                + "           COUNT (1) NO,\n"
                + "           CURRENCY,\n"
                + "           2 ordering\n"
                + "    FROM   disputes\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 2\n"
                + "           AND network_id = 2\n"
                + "           AND CARD_NO NOT LIKE '533813%'\n"
                + "           AND REGEXP_LIKE (CARD_NO, '429742|559444|533813')\n"
                + "GROUP BY   CURRENCY\n"
                + "UNION\n"
                + "  SELECT   'Unmatched Network' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              AMOUNT,\n"
                + "           COUNT (1) NO,\n"
                + "           CURRENCY,\n"
                + "           3 ordering\n"
                + "    FROM   disputes\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 2\n"
                + "           AND CARD_NO NOT LIKE '533813%'\n"
                + "           AND REGEXP_LIKE (CARD_NO, '429742|559444|533813')\n"
                + "GROUP BY   CURRENCY\n"
                + "UNION\n"
                + "  SELECT   'Pre-paid Matched' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              AMOUNT,\n"
                + "           COUNT (1) NO,\n"
                + "           CURRENCY,\n"
                + "           4 ordering\n"
                + "    FROM   matched_data\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 2\n"
                + "           AND CARD_NO LIKE '533813%'\n"
                + "GROUP BY   CURRENCY\n"
                + "UNION\n"
                + "  SELECT   'Pre-paid Unmatched Switch' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              AMOUNT,\n"
                + "           COUNT (1) NO,\n"
                + "           CURRENCY,\n"
                + "           5 ordering\n"
                + "    FROM   disputes\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 2\n"
                + "           AND network_id = 2\n"
                + "           AND CARD_NO LIKE '533813%'\n"
                + "GROUP BY   CURRENCY\n"
                + "UNION\n"
                + "  SELECT   'Pre-paid Unmatched Network' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              AMOUNT,\n"
                + "           COUNT (1) NO,\n"
                + "           CURRENCY,\n"
                + "           6 ordering\n"
                + "    FROM   disputes\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 2\n"
                + "           AND CARD_NO LIKE '533813%'\n"
                + "GROUP BY   CURRENCY\n"
                + "ORDER BY   ordering";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$P{DateFrom}", "'" + DateFormatter.changeDateAndTimeFormat(dateFrom) + "'");
        selectStat = selectStat.replace("$P{DateTo}", "'" + DateFormatter.changeDateAndTimeFormat(dateTo) + "'");

        ResultSet rs = executeQuery(selectStat);
        List<settlementsDTOInter> Records = new ArrayList<settlementsDTOInter>();
        while (rs.next()) {
            settlementsDTOInter SelectedRecord = DTOFactory.createsettlementsDTO();
            SelectedRecord.setTitle(rs.getString("Title"));
            SelectedRecord.setPan(rs.getString("CURRENCY"));
            SelectedRecord.setAmount(rs.getBigDecimal("AMOUNT"));
            SelectedRecord.setCount(rs.getInt("NO"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findRecordsAcquier(Date dateFrom, Date dateTo) throws Throwable {
        super.preSelect();
        String selectStat = "  SELECT                /*+ parallel INDEX (MATCHED_DATA NETWORK_REPORT_IDX)*/\n"
                + "        'Matched' TITLE,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              AMOUNT,\n"
                + "           COUNT (1) NO,\n"
                + "           1 ordering,\n"
                + "           currency\n"
                + "    FROM   matched_data\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 2\n"
                + "           AND NOT REGEXP_LIKE (CARD_NO, '559444|526403|533813')\n"
                + "-- AND PROCESS_CODE NOT LIKE '00000'\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT                /*+ parallel INDEX (disputes NETWORK_REPORT_DIS_IDX)*/\n"
                + "        'Unmatched Switch' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           2 ordering,\n"
                + "           currency\n"
                + "    FROM   disputes\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 2\n"
                + "           AND network_id = 2\n"
                + "           AND NOT REGEXP_LIKE (CARD_NO, '559444|526403|533813')\n"
                + "-- AND PROCESS_CODE NOT LIKE '00000'\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT                /*+ parallel INDEX (disputes NETWORK_REPORT_DIS_IDX)*/\n"
                + "        'Unmatched Network' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           2 ordering,\n"
                + "           currency\n"
                + "    FROM   disputes\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 2\n"
                + "           AND NOT REGEXP_LIKE (CARD_NO, '559444|526403|533813')\n"
                + "--AND PROCESS_CODE NOT LIKE '00000'\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT                /*+ parallel INDEX (MATCHED_DATA NETWORK_REPORT_IDX)*/\n"
                + "        'Maestro Matched' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           1 ordering,\n"
                + "           currency\n"
                + "    FROM   matched_data\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 2\n"
                + "           AND NOT REGEXP_LIKE (CARD_NO, '559444|526403|533813')\n"
                + "           --AND SUBSTR (CARD_NO, 1, 6) BETWEEN '981889%' AND '981895%'\n"
                + "           AND PROCESS_CODE LIKE '00000'\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT                /*+ parallel INDEX (disputes NETWORK_REPORT_DIS_IDX)*/\n"
                + "        'Maestro Unmatched Switch' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           2 ordering,\n"
                + "           currency\n"
                + "    FROM   disputes\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 2\n"
                + "           AND network_id = 2\n"
                + "           AND NOT REGEXP_LIKE (CARD_NO, '559444|526403|533813')\n"
                + "           --  AND SUBSTR (CARD_NO, 1, 6) BETWEEN '981889%' AND '981895%'\n"
                + "           AND PROCESS_CODE LIKE '00000'\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT                /*+ parallel INDEX (disputes NETWORK_REPORT_DIS_IDX)*/\n"
                + "        'Maestro Unmatched Network' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           2 ordering,\n"
                + "           currency\n"
                + "    FROM   disputes\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 2\n"
                + "           AND NOT REGEXP_LIKE (CARD_NO, '559444|526403|533813')\n"
                + "           -- AND SUBSTR (CARD_NO, 1, 6) BETWEEN '981889%' AND '981895%'\n"
                + "           AND PROCESS_CODE LIKE '00000'\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT                /*+ parallel INDEX (MATCHED_DATA NETWORK_REPORT_IDX)*/\n"
                + "        'Fawry Matched' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           1 ordering,\n"
                + "           currency\n"
                + "    FROM   matched_data\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 2\n"
                + "           AND NOT REGEXP_LIKE (CARD_NO, '559444|526403|533813')\n"
                + "           AND PROCESS_CODE LIKE '00000'\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT                /*+ parallel INDEX (disputes NETWORK_REPORT_DIS_IDX)*/\n"
                + "        'Fawry Unmatched Switch' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           2 ordering,\n"
                + "           currency\n"
                + "    FROM   disputes\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 2\n"
                + "           AND network_id = 2\n"
                + "           AND NOT REGEXP_LIKE (CARD_NO, '559444|526403|533813')\n"
                + "           AND PROCESS_CODE LIKE '00000'\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT                /*+ parallel INDEX (disputes NETWORK_REPORT_DIS_IDX)*/\n"
                + "        'Fawry Unmatched Network' Title,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) No,\n"
                + "           2 ordering,\n"
                + "           currency\n"
                + "    FROM   disputes\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 2\n"
                + "           AND NOT REGEXP_LIKE (CARD_NO, '559444|526403|533813')\n"
                + "           AND PROCESS_CODE LIKE '00000'\n"
                + "GROUP BY   currency";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$P{DateFrom}", "'" + DateFormatter.changeDateAndTimeFormat(dateFrom) + "'");
        selectStat = selectStat.replace("$P{DateTo}", "'" + DateFormatter.changeDateAndTimeFormat(dateTo) + "'");

        ResultSet rs = executeQuery(selectStat);
        List<settlementsDTOInter> Records = new ArrayList<settlementsDTOInter>();
        while (rs.next()) {
            settlementsDTOInter SelectedRecord = DTOFactory.createsettlementsDTO();
            SelectedRecord.setTitle(rs.getString("TITLE"));
            SelectedRecord.setAmount(rs.getBigDecimal("AMOUNT"));
            SelectedRecord.setPan(rs.getString("currency"));
            SelectedRecord.setCount(rs.getInt("NO"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findRecordstimefromswitch(Date dateFrom, Date dateTo) throws Throwable {
        super.preSelect();
        String selectStat = "  SELECT   'Issuer Not Prepaid' Title,\n"
                + "           NVL (SUM (AMOUNT), 0) amount,\n"
                + "           COUNT (1) NO,\n"
                + "           currency\n"
                + "    FROM   DISPUTES\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND RECORD_TYPE = 2\n"
                + "           AND network_id = 2\n"
                + "           AND CARD_NO NOT LIKE '533813%'\n"
                + "           AND REGEXP_LIKE (CARD_NO, '429742|559444|533813')\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT   'Issuer Prepaid' Title,\n"
                + "           NVL (SUM (AMOUNT), 0) amount,\n"
                + "           COUNT (1) NO,\n"
                + "           currency\n"
                + "    FROM   DISPUTES\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND RECORD_TYPE = 2\n"
                + "           AND network_id = 2\n"
                + "           AND CARD_NO LIKE '533813%'\n"
                + "           AND REGEXP_LIKE (CARD_NO, '429742|559444|533813')\n"
                + "GROUP BY   currency\n"
                + "UNION\n"
                + "  SELECT   'ACQUIRER' Title,\n"
                + "           NVL (SUM (AMOUNT), 0) amount,\n"
                + "           COUNT (1) NO,\n"
                + "           currency\n"
                + "    FROM   DISPUTES\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND RECORD_TYPE = 2\n"
                + "           AND network_id = 2\n"
                + "           AND NOT REGEXP_LIKE (CARD_NO, '429742|559444|533813')\n"
                + "GROUP BY   currency";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$P{DateFrom}", "'" + DateFormatter.changeDateAndTimeFormat(dateFrom) + "'");
        selectStat = selectStat.replace("$P{DateTo}", "'" + DateFormatter.changeDateAndTimeFormat(dateTo) + "'");

        ResultSet rs = executeQuery(selectStat);
        List<timeFromSwitchDTOInter> Records = new ArrayList<timeFromSwitchDTOInter>();
        while (rs.next()) {
            timeFromSwitchDTOInter SelectedRecord = DTOFactory.createtimeFromSwitchDTO();
            SelectedRecord.setCurrency(rs.getString("currency"));
            SelectedRecord.setAcq(rs.getString("Title"));
            SelectedRecord.setAmount(rs.getBigDecimal("amount"));
            SelectedRecord.setCount(rs.getInt("NO"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

}
