package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.settlementsDTOInter;
import com.ev.Bingo.bus.dto.timeFromSwitchDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class settlementsDAO extends BaseDAO implements settlementsDAOInter {

    @Override
    public Object findRecordsIssuer(Date dateFrom, Date dateTo) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT                /*+ parallel INDEX (MATCHED_DATA NETWORK_REPORT_IDX)*/\n"
                + "        'MATCHED' TITLE,\n"
                + "           SUBSTR (card_no, 1, 6) PAN,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) NO\n"
                + "    FROM   MATCHED_DATA\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 3\n"
                + "           AND bank LIKE '%NBE%'\n"
                + "           AND REGEXP_LIKE (card_no,\n"
                + "                            '510474|526403|981803|457376|483469|559444|494606')\n"
                + "GROUP BY   SUBSTR (card_no, 1, 6)\n"
                + "UNION\n"
                + "  SELECT   'Unmatched Network' TITLE,\n"
                + "           SUBSTR (card_no, 1, 6) PAN,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) NO\n"
                + "    FROM   disputes\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 1\n"
                + "           AND network_id = 3\n"
                + "           AND bank LIKE '%NBE%'\n"
                + "           AND REGEXP_LIKE (card_no,\n"
                + "                            '510474|526403|981803|457376|483469|559444|494606')\n"
                + "GROUP BY   SUBSTR (card_no, 1, 6)\n"
                + "UNION\n"
                + "  SELECT   'Unmatched Switch' TITLE,\n"
                + "           SUBSTR (card_no, 1, 6) PAN,\n"
                + "           NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                             1,\n"
                + "                             NVL (AMOUNT, 0),\n"
                + "                             2,\n"
                + "                             -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "              Amount,\n"
                + "           COUNT (1) NO\n"
                + "    FROM   disputes\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND record_type = 2\n"
                + "           AND network_id = 3\n"
                + "           AND bank LIKE '%NBE%'\n"
                + "           AND REGEXP_LIKE (card_no,\n"
                + "                            '510474|526403|981803|457376|483469|559444|494606')\n"
                + "GROUP BY   SUBSTR (card_no, 1, 6)";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$P{DateFrom}", "'" + DateFormatter.changeDateAndTimeFormat(dateFrom) + "'");
        selectStat = selectStat.replace("$P{DateTo}", "'" + DateFormatter.changeDateAndTimeFormat(dateTo) + "'");

        ResultSet rs = executeQuery(selectStat);
        List<settlementsDTOInter> Records = new ArrayList<settlementsDTOInter>();
        while (rs.next()) {
            settlementsDTOInter SelectedRecord = DTOFactory.createsettlementsDTO();
            SelectedRecord.setTitle(rs.getString("TITLE"));
            SelectedRecord.setPan(rs.getString("PAN"));
            SelectedRecord.setAmount(rs.getBigDecimal("Amount"));
            SelectedRecord.setCount(rs.getInt("NO"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findRecordsAcquier(Date dateFrom, Date dateTo) throws Throwable {
        super.preSelect();
        String selectStat = "SELECT   'Response code:112' Title,\n"
                + "         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                           1,\n"
                + "                           NVL (AMOUNT, 0),\n"
                + "                           2,\n"
                + "                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "            Amount,\n"
                + "         COUNT (1) NO,\n"
                + "         1 ordering\n"
                + "  FROM   REJECTED_SWITCH\n"
                + " WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ($P{DateTo},\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND network_id = 3\n"
                + "         AND COLUMN4 LIKE '%NBE%'\n"
                + "         AND RESPONSE_CODE LIKE '112'\n"
                + "UNION\n"
                + "SELECT   'Response code:0 Matched switch' Title,\n"
                + "         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                           1,\n"
                + "                           NVL (AMOUNT, 0),\n"
                + "                           2,\n"
                + "                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "            Amount,\n"
                + "         COUNT (1) NO,\n"
                + "         2 ordering\n"
                + "  FROM   matched_data\n"
                + " WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ($P{DateTo},\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND record_type = 2\n"
                + "         AND network_id = 3\n"
                + "         AND COLUMN4 LIKE '%NBE%'\n"
                + "         AND RESPONSE_CODE LIKE '0%'\n"
                + "UNION\n"
                + "SELECT   'Response code:0 Unmatched switch' Title,\n"
                + "         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                           1,\n"
                + "                           NVL (AMOUNT, 0),\n"
                + "                           2,\n"
                + "                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "            Amount,\n"
                + "         COUNT (1) NO,\n"
                + "         3 ordering\n"
                + "  FROM   disputes\n"
                + " WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ($P{DateTo},\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND record_type = 2\n"
                + "         AND network_id = 3\n"
                + "         AND COLUMN4 LIKE '%NBE%'\n"
                + "         AND RESPONSE_CODE LIKE '0%'\n"
                + "UNION\n"
                + "SELECT   'Others Codes(Network)' Title,\n"
                + "         NVL (SUM (DECODE (TRANSACTION_TYPE_MASTER,\n"
                + "                           1,\n"
                + "                           NVL (AMOUNT, 0),\n"
                + "                           2,\n"
                + "                           -1 * (ABS (NVL (AMOUNT, 0))))), 0)\n"
                + "            Amount,\n"
                + "         COUNT (1) NO,\n"
                + "         4 ordering\n"
                + "  FROM   rejected_network\n"
                + " WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ($P{DateTo},\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')\n"
                + "         AND network_id = 3\n"
                + "         AND COLUMN4 LIKE '%NBE%'\n"
                + "         AND RESPONSE_CODE NOT IN ('001', '000')\n"
                + "ORDER BY   ordering";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$P{DateFrom}", "'" + DateFormatter.changeDateAndTimeFormat(dateFrom) + "'");
        selectStat = selectStat.replace("$P{DateTo}", "'" + DateFormatter.changeDateAndTimeFormat(dateTo) + "'");

        ResultSet rs = executeQuery(selectStat);
        List<settlementsDTOInter> Records = new ArrayList<settlementsDTOInter>();
        while (rs.next()) {
            settlementsDTOInter SelectedRecord = DTOFactory.createsettlementsDTO();
            SelectedRecord.setTitle(rs.getString("Title"));
            SelectedRecord.setAmount(rs.getBigDecimal("Amount"));
            SelectedRecord.setCount(rs.getInt("NO"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findRecordstimefromswitch(Date dateFrom, Date dateTo) throws Throwable {
        super.preSelect();
        String selectStat = "  SELECT   SUM (AMOUNT) AMOUNT,\n"
                + "           COUNT (1) NO,\n"
                + "           column4 ACQUIRIER,\n"
                + "           BANK ISSUER,\n"
                + "           CURRENCY\n"
                + "    FROM   DISPUTES\n"
                + "   WHERE   transaction_date BETWEEN TO_DATE ($P{DateFrom},\n"
                + "                                             'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                AND  TO_DATE ($P{DateTo},\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "           AND RECORD_TYPE = 2\n"
                + "           AND network_id = 3\n"
                + "GROUP BY   column4, BANK, CURRENCY";

        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$P{DateFrom}", "'" + DateFormatter.changeDateAndTimeFormat(dateFrom) + "'");
        selectStat = selectStat.replace("$P{DateTo}", "'" + DateFormatter.changeDateAndTimeFormat(dateTo) + "'");

        ResultSet rs = executeQuery(selectStat);
        List<timeFromSwitchDTOInter> Records = new ArrayList<timeFromSwitchDTOInter>();
        while (rs.next()) {
            timeFromSwitchDTOInter SelectedRecord = DTOFactory.createtimeFromSwitchDTO();
            SelectedRecord.setCurrency(rs.getString("CURRENCY"));
            SelectedRecord.setAcq(rs.getString("ACQUIRIER"));
            SelectedRecord.setIss(rs.getString("ISSUER"));
            SelectedRecord.setAmount(rs.getBigDecimal("AMOUNT"));
            SelectedRecord.setCount(rs.getInt("NO"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

}
