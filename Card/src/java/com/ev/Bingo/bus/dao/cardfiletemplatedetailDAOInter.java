package com.ev.Bingo.bus.dao;

import java.util.Date;
import java.util.List;
import java.sql.SQLException;

public interface cardfiletemplatedetailDAOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object updaterecord(Object... obj) throws Throwable;

    Object deleterecord(int seq) throws Throwable;

    String deleteallrecord(Object... obj) throws Throwable;

    Object findRecord(Object... obj) throws Throwable;

    Object findRecordsList(Object... obj) throws Throwable;

    Object findRecordsAll(Object... obj) throws Throwable;

    Object findRecordsListUpdate(int seq) throws Throwable;

    int save(Object... obj) throws SQLException, Throwable;

    int updatetemplate(int seq, int TemplateID, int networktype) throws Throwable;
}
