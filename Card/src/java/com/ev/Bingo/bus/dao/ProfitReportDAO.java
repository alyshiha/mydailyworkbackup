/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.AtmMachineDTOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.OffusDTOInter;
import com.ev.Bingo.bus.dto.OffusTotalsDTOInter;
import com.ev.Bingo.bus.dto.OnusDTOInter;
import com.ev.Bingo.bus.dto.OnusTotalsDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusInterDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusInterTotalsDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusTotalsDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author shi7a
 */
public class ProfitReportDAO extends BaseDAO implements ProfitReportDAOInter {

    @Override
    public List<AtmMachineDTOInter> findAtmMachineList() throws Throwable {
        super.preSelect();
        String selectStat = "SELECT   application_id,name\n"
                + " FROM   atm_machine@card_atm m\n"
                + " where exists (select 1 from atm_machine where atm_machine.atm_id=m.application_id )";
        ResultSet rs = executeQuery(selectStat);
        List<AtmMachineDTOInter> records = new ArrayList<AtmMachineDTOInter>();
        while (rs.next()) {
            AtmMachineDTOInter record = DTOFactory.createAtmMachineDTO();
            record.setApplicationId(rs.getString("application_id"));
            record.setName(rs.getString("name"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    @Override
    public List<OffusDTOInter> find123OffusTransactions(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable {
        super.preSelect();
        //to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss')
        System.out.println("123OffusTransactions");
        String selectStat = "SELECT   SUM (count1) no,\n"
                + "                         SUM (income) AS RelatedIncome,\n"
                + "                         '123 Withdrawal TXNS' TYPE,terminal\n"
                + "                  FROM   (SELECT /*+parallel*/   COUNT (1) count1, (COUNT (1) * 1.5) income,'123 Withdrawal TXNS' TYPE,terminal\n"
                + "                            FROM   disputes\n"
                + "                           WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                                   AND ((TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                                   '$P{DateFrom}',\n"
                + "                                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                                )\n"
                + "                                                            AND  TO_DATE (\n"
                + "                                                                    '$P{DateTo}',\n"
                + "                                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                                 )) OR (NVL('$P{DateTo}','0') = '0' AND NVL('$P{DateFrom}','0') = '0')) \n"
                + "                                   AND network_id = 3\n"
                + "                                   AND DEC_DUPLICATE (card_no) NOT LIKE '627632%'\n"
                + "                                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                                   AND transaction_type_master <> 2\n"
                + "                                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                                   AND ((SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                                  '$P{SettFrom}',\n"
                + "                                                                  'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                               )\n"
                + "                                                           AND  TO_DATE (\n"
                + "                                                                   '$P{SettTo}',\n"
                + "                                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                                )) OR (NVL('$P{SettTo}','0') = '0' AND NVL('$P{SettFrom}','0') = '0'))"
                + " "
                + "                             group by terminal\n"
                + "                          UNION\n"
                + "                          SELECT  /*+parallel*/  COUNT (1) count1, (COUNT (1) * 1.5) income,'123 Withdrawal TXNS' TYPE,terminal\n"
                + "                            FROM   matched_data\n"
                + "                           WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                                   AND ((TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                                   '$P{DateFrom}',\n"
                + "                                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                                )\n"
                + "                                                            AND  TO_DATE (\n"
                + "                                                                    '$P{DateTo}',\n"
                + "                                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                                 )) OR (NVL('$P{DateTo}','0') = '0' AND NVL('$P{DateFrom}','0') = '0'))\n"
                + "                                   AND network_id = 3\n"
                + "                                   AND DEC_DUPLICATE (card_no) NOT LIKE '627632%'\n"
                + "                                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                                   AND transaction_type_master <> 2\n"
                + "                                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                                   AND ((SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                                  '$P{SettFrom}',\n"
                + "                                                                  'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                               )\n"
                + "                                                           AND  TO_DATE (\n"
                + "                                                                   '$P{SettTo}',\n"
                + "                                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                                )) OR (NVL('$P{SettTo}','0') = '0' AND NVL('$P{SettFrom}','0') = '0')) group by terminal)group by terminal\n"
                + "                UNION\n"
                + "                SELECT   COUNT (1) No,\n"
                + "                         (COUNT (1) * 0.5) AS RelatedIncome,\n"
                + "                         '123 Balance Inquiry TXNS' TYPE,terminal\n"
                + "                  FROM   REJECTED_NETWORK\n"
                + "                 WHERE   ((TRANSACTION_DATE BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                              AND  TO_DATE ('$P{DateTo}',\n"
                + "                                                            'dd-mm-yyyy hh24:mi:ss')) OR (NVL('$P{DateTo}','0') = '0' AND NVL('$P{DateFrom}','0') = '0'))\n"
                + "                         AND network_id = 3\n"
                + "                         AND DEC_DUPLICATE (card_no) NOT LIKE '627632%'\n"
                + "                         AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                         AND transaction_type_master <> 2\n"
                + "                         AND NVL (AMOUNT, 0) = 0\n"
                + "                         AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                         AND ((SETTLEMENT_DATE BETWEEN TO_DATE ('$P{SettFrom}',\n"
                + "                                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                                 AND  TO_DATE ('$P{SettTo}',\n"
                + "                                                               'dd-mm-yyyy hh24:mi:ss')) OR (NVL('$P{SettTo}','0') = '0' AND NVL('$P{SettFrom}','0') = '0')) group by terminal order by no, RelatedIncome";
        selectStat = selectStat.replace("$P{AtmID}", "" + ATMID);
        selectStat = selectStat.replace("$P{DateFrom}", "" + DateFormatter.changeDateAndTimeFormat(TransFromDate));
        selectStat = selectStat.replace("$P{DateTo}", "" + DateFormatter.changeDateAndTimeFormat(TransToDate));
        selectStat = selectStat.replace("$P{SettFrom}", "" + DateFormatter.changeDateAndTimeFormat(SettFromDate));
        selectStat = selectStat.replace("$P{SettTo}", "" + DateFormatter.changeDateAndTimeFormat(SettToDate));
        ResultSet rs = executeQuery(selectStat);
        List<OffusDTOInter> records = new ArrayList<OffusDTOInter>();
        while (rs.next()) {
            OffusDTOInter record = DTOFactory.createOffusDTO();
            record.setCount(rs.getInt("no"));
            record.setRelatedincome(rs.getBigDecimal("RelatedIncome"));
            record.setType(rs.getString("TYPE"));
            record.setTerminal(rs.getString("terminal"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    @Override
    public List<VisaOffusDTOInter> findVisaOffusNationalTransactions(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable {

        System.out.println("VisaOffusNationalTransactions");
        super.preSelect();
        //to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss')
        String selectStat = "SELECT   SUM (count1) transcount,\n"
                + "         SUM (income) income,\n"
                + "         'Visa Withdrawal National' TYPE,terminal\n"
                + "  FROM   (SELECT   /*+parallel*/  COUNT (1) count1, (COUNT (1) * 2) income,terminal\n"
                + "            FROM  disputes\n"
                + "           WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                   AND ((TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                   '$P{DateFrom}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                )\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    '$P{DateTo}',\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )) OR (NVL('$P{DateTo}','0') = '0' AND NVL('$P{DateFrom}','0') = '0'))\n"
                + "                   AND network_id = 1\n"
                + "                   AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "                   AND CURRENCY_id = 1\n"
                + "                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                   AND transaction_type_master <> 2\n"
                + "                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                   AND ((SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                  '$P{SettFrom}',\n"
                + "                                                  'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                               )\n"
                + "                                           AND  TO_DATE (\n"
                + "                                                   '$P{SettTo}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                )) OR (NVL('$P{SettTo}','0') = '0' AND NVL('$P{SettFrom}','0') = '0'))\n"
                + "          group by terminal"
                + "         UNION\n"
                + "          SELECT /*+parallel*/  COUNT (1) count1, (COUNT (1) * 2) income,terminal\n"
                + "            FROM   matched_data\n"
                + "           WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                   AND ((TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                   '$P{DateFrom}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                )\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    '$P{DateTo}',\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )) OR (NVL('$P{DateTo}','0') = '0' AND NVL('$P{DateFrom}','0') = '0'))\n"
                + "                   AND network_id = 1\n"
                + "                   AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "                   AND CURRENCY_id = 1\n"
                + "                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                   AND transaction_type_master <> 2\n"
                + "                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                   AND ((SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                  '$P{SettFrom}',\n"
                + "                                                  'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                               )\n"
                + "                                           AND  TO_DATE (\n"
                + "                                                   '$P{SettTo}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                )) OR (NVL('$P{SettTo}','0') = '0' AND NVL('$P{SettFrom}','0') = '0')) group by terminal)\n"
                + " group by terminal UNION\n"
                + "SELECT   COUNT (1) count1,\n"
                + "         (COUNT (1) * 0.5) income,\n"
                + "         'Visa Decline National' TYPE,terminal\n"
                + "  FROM   rejected_network\n"
                + " WHERE   ((TRANSACTION_DATE BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ('$P{DateTo}',\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')) OR (NVL('$P{DateTo}','0') = '0' AND NVL('$P{DateFrom}','0') = '0'))\n"
                + "         AND network_id = 1\n"
                + "         AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "         AND CURRENCY_id = 1\n"
                + "         AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "         AND transaction_type_master <> 2\n"
                + "         AND NVL (amount, 0) <> 0\n"
                + "         AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "         AND ((SETTLEMENT_DATE BETWEEN TO_DATE ('$P{SettFrom}',\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                 AND  TO_DATE ('$P{SettTo}',\n"
                + "                                               'dd-mm-yyyy hh24:mi:ss')) OR (NVL('$P{SettTo}','0') = '0' AND NVL('$P{SettFrom}','0') = '0')) group by terminal order by transcount, income";
        selectStat = selectStat.replace("$P{AtmID}", "" + ATMID);
        selectStat = selectStat.replace("$P{DateFrom}", "" + DateFormatter.changeDateAndTimeFormat(TransFromDate));
        selectStat = selectStat.replace("$P{DateTo}", "" + DateFormatter.changeDateAndTimeFormat(TransToDate));
        selectStat = selectStat.replace("$P{SettFrom}", "" + DateFormatter.changeDateAndTimeFormat(SettFromDate));
        selectStat = selectStat.replace("$P{SettTo}", "" + DateFormatter.changeDateAndTimeFormat(SettToDate));
        ResultSet rs = executeQuery(selectStat);
        List<VisaOffusDTOInter> records = new ArrayList<VisaOffusDTOInter>();
        while (rs.next()) {
            VisaOffusDTOInter record = DTOFactory.createVisaOffusDTO();
            record.setCount(rs.getInt("transcount"));
            record.setRelatedincome(rs.getBigDecimal("income"));
            record.setType(rs.getString("TYPE"));
            record.setTerminal(rs.getString("terminal"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    @Override
    public List<VisaOffusInterDTOInter> findVisaOffusInternationalTransactions(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable {
        System.out.println("VisaOffusInternationalTransactions");

        super.preSelect();
        //to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss')
        String selectStat = "SELECT   SUM (count1) NO,\n"
                + "         SUM (totalamount) TotalAmount,\n"
                + "         SUM (income) income,\n"
                + "         'Visa Withdrawal International' TYPE,terminal\n"
                + "  FROM   (SELECT /*+parallel*/  COUNT (1) count1,\n"
                + "                   nvl(SUM (amount),0) totalamount,\n"
                + "                   nvl(SUM (amount * 0.01),0) income,terminal\n"
                + "            FROM   disputes\n"
                + "           WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                   AND ((TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                   '$P{DateFrom}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                )\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    '$P{DateTo}',\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )) OR (NVL('$P{DateTo}','0') = '0' AND NVL('$P{DateFrom}','0') = '0'))\n"
                + "                   AND network_id = 1\n"
                + "                   AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "                   AND NVL (CURRENCY_id, 0) <> 1\n"
                + "                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                   AND transaction_type_master <> 2\n"
                + "                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                   AND ((SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                  '$P{SettFrom}',\n"
                + "                                                  'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                               )\n"
                + "                                           AND  TO_DATE (\n"
                + "                                                   '$P{SettTo}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                )) OR (NVL('$P{SettTo}','0') = '0' AND NVL('$P{SettFrom}','0') = '0')) group by terminal\n"
                + "          UNION\n"
                + "          SELECT  /*+parallel*/  COUNT (1) count1,\n"
                + "                   nvl(SUM (amount),0) totalamount,\n"
                + "                   nvl(SUM (amount * 0.01),0) income,terminal\n"
                + "            FROM   matched_data\n"
                + "           WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                   AND ((TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                   '$P{DateFrom}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                )\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    '$P{DateTo}',\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )) OR (NVL('$P{DateTo}','0') = '0' AND NVL('$P{DateFrom}','0') = '0'))\n"
                + "                   AND network_id = 1\n"
                + "                   AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "                   AND NVL (CURRENCY_id, 0) <> 1\n"
                + "                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                   AND transaction_type_master <> 2\n"
                + "                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                   AND ((SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                  '$P{SettFrom}',\n"
                + "                                                  'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                               )\n"
                + "                                           AND  TO_DATE (\n"
                + "                                                   '$P{SettTo}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                )) OR (NVL('$P{SettTo}','0') = '0' AND NVL('$P{SettFrom}','0') = '0')) group by terminal) group by terminal\n"
                + "UNION\n"
                + "SELECT   COUNT (1) NO,\n"
                + "         NULL TotalAmount,\n"
                + "         (COUNT (1) * 0.3) income,\n"
                + "         'Visa Decline Interational' TYPE,terminal\n"
                + "  FROM   rejected_network\n"
                + " WHERE   ((TRANSACTION_DATE BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ('$P{DateTo}',\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')) OR (NVL('$P{DateTo}','0') = '0' AND NVL('$P{DateFrom}','0') = '0'))\n"
                + "         AND network_id = 1\n"
                + "         AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "         AND CURRENCY_id <> 1\n"
                + "         AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "         AND transaction_type_master <> 2\n"
                + "         AND NVL (amount, 0) <> 0\n"
                + "         AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0' )\n"
                + "         AND ((SETTLEMENT_DATE BETWEEN TO_DATE ('$P{SettFrom}',\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                 AND  TO_DATE ('$P{SettTo}',\n"
                + "                                               'dd-mm-yyyy hh24:mi:ss')) OR (NVL('$P{SettTo}','0') = '0' AND NVL('$P{SettFrom}','0') = '0'))\n"
                + " group by terminal UNION\n"
                + "SELECT   COUNT (1) NO,\n"
                + "         NULL TotalAmount,\n"
                + "         (COUNT (1) * 0.3) income,\n"
                + "         'Visa BalanceInquiry Interational' TYPE,terminal\n"
                + "  FROM   rejected_network\n"
                + " WHERE   ((TRANSACTION_DATE BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ('$P{DateTo}',\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')) OR (NVL('$P{DateTo}','0') = '0' AND NVL('$P{DateFrom}','0') = '0'))\n"
                + "         AND network_id = 1\n"
                + "         AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "         AND CURRENCY_id <> 1\n"
                + "         AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "         AND transaction_type_master <> 2\n"
                + "         AND NVL (amount, 0) = 0\n"
                + "         AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "         AND ((SETTLEMENT_DATE BETWEEN TO_DATE ('$P{SettFrom}',\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                 AND  TO_DATE ('$P{SettTo}',\n"
                + "                                               'dd-mm-yyyy hh24:mi:ss')) OR (NVL('$P{SettTo}','0') = '0' AND NVL('$P{SettFrom}','0') = '0')) group by terminal order by NO, TotalAmount";
        selectStat = selectStat.replace("$P{AtmID}", "" + ATMID);
        selectStat = selectStat.replace("$P{DateFrom}", "" + DateFormatter.changeDateAndTimeFormat(TransFromDate));
        selectStat = selectStat.replace("$P{DateTo}", "" + DateFormatter.changeDateAndTimeFormat(TransToDate));
        selectStat = selectStat.replace("$P{SettFrom}", "" + DateFormatter.changeDateAndTimeFormat(SettFromDate));
        selectStat = selectStat.replace("$P{SettTo}", "" + DateFormatter.changeDateAndTimeFormat(SettToDate));
        ResultSet rs = executeQuery(selectStat);
        List<VisaOffusInterDTOInter> records = new ArrayList<VisaOffusInterDTOInter>();
        while (rs.next()) {
            VisaOffusInterDTOInter record = DTOFactory.createVisaOffusInterDTO();
            record.setCount(rs.getInt("NO"));
            record.setIncome(rs.getBigDecimal("income"));
            record.setTotalamount(rs.getBigDecimal("TotalAmount"));
            record.setType(rs.getString("TYPE"));
            record.setTerminal(rs.getString("terminal"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    @Override
    public List<OnusDTOInter> findOnusTransactions(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable {

        System.out.println("OnusTransactions");
        super.preSelect();
        //to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss')
        String selectStat = "SELECT   SUM (count1) Count_Onus, SUM (amount1) Total_Amount, 'Withdrawal Trx' Type,terminal\n"
                + "  FROM   (SELECT /*+parallel*/   COUNT (1) count1, SUM (amount) amount1,terminal\n"
                + "            FROM   disputes\n"
                + "           WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                   AND ((TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                   '$P{DateFrom}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                )\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    '$P{DateTo}',\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )) OR (NVL('$P{DateTo}','0') = '0' AND NVL('$P{DateFrom}','0') = '0'))\n"
                + "                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                   AND network_id in (1,3)\n"
                + "                   AND (DEC_DUPLICATE (card_no) LIKE '627632%'\n"
                + "                        OR DEC_DUPLICATE (card_no) LIKE '457871%')\n"
                + "                   AND transaction_type_master <> 2\n"
                + "                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                   AND ((SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                  '$P{SettFrom}',\n"
                + "                                                  'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                               )\n"
                + "                                           AND  TO_DATE (\n"
                + "                                                   '$P{SettTo}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                )) OR (NVL('$P{SettTo}','0') = '0' AND NVL('$P{SettFrom}','0') = '0')) group by terminal\n"
                + "          UNION\n"
                + "          SELECT /*+parallel*/   COUNT (1) count1, SUM (amount) amount1,terminal\n"
                + "            FROM   matched_data\n"
                + "           WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                   AND ((TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                   '$P{DateFrom}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                )\n"
                + "                                            AND  TO_DATE (\n"
                + "                                                    '$P{DateTo}',\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                 )) OR (NVL('$P{DateTo}','0') = '0' AND NVL('$P{DateFrom}','0') = '0'))\n"
                + "                   AND (DEC_DUPLICATE (card_no) LIKE '627632%'\n"
                + "                        OR DEC_DUPLICATE (card_no) LIKE '457871%')\n"
                + "                   AND transaction_type_master <> 2\n"
                + "                   AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                   AND network_id in (1,3)\n"
                + "                   AND ((SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                  '$P{SettFrom}',\n"
                + "                                                  'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                               )\n"
                + "                                           AND  TO_DATE (\n"
                + "                                                   '$P{SettTo}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                )) OR (NVL('$P{SettTo}','0') = '0' AND NVL('$P{SettFrom}','0') = '0')) group by terminal) group by terminal\n"
                + "UNION\n"
                + "SELECT   COUNT (1) Count_Onus, SUM (amount) Total_Amount, 'Balance Inquiry Trx' Type,terminal\n"
                + "  FROM   rejected_network\n"
                + " WHERE   ((TRANSACTION_DATE BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                           'dd-mm-yyyy hh24:mi:ss')\n"
                + "                              AND  TO_DATE ('$P{DateTo}',\n"
                + "                                            'dd-mm-yyyy hh24:mi:ss')) OR (NVL('$P{DateTo}','0') = '0' AND NVL('$P{DateFrom}','0') = '0'))\n"
                + "         AND (DEC_DUPLICATE (card_no) LIKE '627632%'\n"
                + "              OR DEC_DUPLICATE (card_no) LIKE '457871%')\n"
                + "         AND transaction_type_master <> 2\n"
                + "         AND (terminal = '$P{AtmID}' OR NVL('$P{AtmID}','0') = '0')\n"
                + "                   AND network_id in (1,3)\n"
                + "                   AND nvl(amount,0) = 0\n"
                + "         AND ((SETTLEMENT_DATE BETWEEN TO_DATE ('$P{SettFrom}',\n"
                + "                                              'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                 AND  TO_DATE ('$P{SettTo}',\n"
                + "                                               'dd-mm-yyyy hh24:mi:ss')) OR (NVL('$P{SettTo}','0') = '0' AND NVL('$P{SettFrom}','0') = '0')) group by terminal order by Count_Onus, Total_Amount";
        selectStat = selectStat.replace("$P{AtmID}", "" + ATMID);
        selectStat = selectStat.replace("$P{DateFrom}", "" + DateFormatter.changeDateAndTimeFormat(TransFromDate));
        selectStat = selectStat.replace("$P{DateTo}", "" + DateFormatter.changeDateAndTimeFormat(TransToDate));
        selectStat = selectStat.replace("$P{SettFrom}", "" + DateFormatter.changeDateAndTimeFormat(SettFromDate));
        selectStat = selectStat.replace("$P{SettTo}", "" + DateFormatter.changeDateAndTimeFormat(SettToDate));
        ResultSet rs = executeQuery(selectStat);
        List<OnusDTOInter> records = new ArrayList<OnusDTOInter>();
        while (rs.next()) {
            OnusDTOInter record = DTOFactory.createOnusDTO();
            record.setCount(rs.getInt("Count_Onus"));
            record.setTotalamount(rs.getBigDecimal("Total_Amount"));
            record.setType(rs.getString("Type"));
            record.setTerminal(rs.getString("terminal"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public List<OffusTotalsDTOInter> find123OffusTransactionstotals(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable {
        super.preSelect();
        //to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss')
        System.out.println("123OffusTransactions");
        String selectStat = "  SELECT   SUM (count1) no,\n"
                + "           SUM (income) AS RelatedIncome,\n"
                + "           '123 Withdrawal TXNS' TYPE,\n"
                + "           terminal,\n"
                + "           (SELECT   name\n"
                + "              FROM   atm_machine@card_atm\n"
                + "             WHERE   application_id = terminal)\n"
                + "              terminal_name,\n"
                + "           TRANSACTION_MONTH\n"
                + "    FROM   (  SELECT                                             /*+parallel*/\n"
                + "                    COUNT (1) count1,\n"
                + "                       (COUNT (1) * 1.5) income,\n"
                + "                       '123 Withdrawal TXNS' TYPE,\n"
                + "                       terminal,\n"
                + "                       TO_CHAR (TRANSACTION_DATE, 'MONTH') TRANSACTION_MONTH\n"
                + "                FROM   disputes\n"
                + "               WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                       AND ( (TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                          '$P{DateFrom}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       )\n"
                + "                                                   AND  TO_DATE (\n"
                + "                                                           '$P{DateTo}',\n"
                + "                                                           'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                        ))\n"
                + "                            OR (NVL ('$P{DateFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{DateTo}', '0') = '0'))\n"
                + "                       AND network_id = 3\n"
                + "                       AND DEC_DUPLICATE (card_no) NOT LIKE '627632%'\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND transaction_type_master <> 2\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND ( (SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                         '$P{SettFrom}',\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                      )\n"
                + "                                                  AND  TO_DATE (\n"
                + "                                                          '$P{SettTo}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       ))\n"
                + "                            OR (NVL ('$P{SettFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{SettTo}', '0') = '0'))\n"
                + "            GROUP BY   terminal, TO_CHAR (TRANSACTION_DATE, 'MONTH')\n"
                + "            UNION\n"
                + "              SELECT                                             /*+parallel*/\n"
                + "                    COUNT (1) count1,\n"
                + "                       (COUNT (1) * 1.5) income,\n"
                + "                       '123 Withdrawal TXNS' TYPE,\n"
                + "                       terminal,\n"
                + "                       TO_CHAR (TRANSACTION_DATE, 'MONTH') TRANSACTION_MONTH\n"
                + "                FROM   matched_data\n"
                + "               WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                       AND ( (TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                          '$P{DateFrom}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       )\n"
                + "                                                   AND  TO_DATE (\n"
                + "                                                           '$P{DateTo}',\n"
                + "                                                           'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                        ))\n"
                + "                            OR (NVL ('$P{DateFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{DateTo}', '0') = '0'))\n"
                + "                       AND network_id = 3\n"
                + "                       AND DEC_DUPLICATE (card_no) NOT LIKE '627632%'\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND transaction_type_master <> 2\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND ( (SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                         '$P{SettFrom}',\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                      )\n"
                + "                                                  AND  TO_DATE (\n"
                + "                                                          '$P{SettTo}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       ))\n"
                + "                            OR (NVL ('$P{SettFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{SettTo}', '0') = '0'))\n"
                + "            GROUP BY   terminal, TO_CHAR (TRANSACTION_DATE, 'MONTH'))\n"
                + "GROUP BY   terminal, TRANSACTION_MONTH\n"
                + "UNION\n"
                + "  SELECT   COUNT (1) No,\n"
                + "           (COUNT (1) * 0.5) AS RelatedIncome,\n"
                + "           '123 Balance Inquiry TXNS' TYPE,\n"
                + "           terminal,\n"
                + "           (SELECT   name\n"
                + "              FROM   atm_machine@card_atm\n"
                + "             WHERE   application_id = terminal)\n"
                + "              terminal_name,\n"
                + "           TO_CHAR (TRANSACTION_DATE, 'MONTH') TRANSACTION_MONTH\n"
                + "    FROM   REJECTED_NETWORK\n"
                + "   WHERE   ( (TRANSACTION_DATE BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                                'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                   AND  TO_DATE ('$P{DateTo}',\n"
                + "                                                 'dd-mm-yyyy hh24:mi:ss'))\n"
                + "            OR (NVL ('$P{DateFrom}', '0') = '0'\n"
                + "                AND NVL ('$P{DateTo}', '0') = '0'))\n"
                + "           AND network_id = 3\n"
                + "           AND DEC_DUPLICATE (card_no) NOT LIKE '627632%'\n"
                + "           AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "           AND transaction_type_master <> 2\n"
                + "           AND NVL (AMOUNT, 0) = 0\n"
                + "           AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "           AND ( (SETTLEMENT_DATE BETWEEN TO_DATE ('$P{SettFrom}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                      AND  TO_DATE ('$P{SettTo}',\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'))\n"
                + "                OR (NVL ('$P{SettFrom}', '0') = '0'\n"
                + "                    AND NVL ('$P{SettTo}', '0') = '0'))\n"
                + "GROUP BY   terminal, TO_CHAR (TRANSACTION_DATE, 'MONTH')\n"
                + "ORDER BY   no, RelatedIncome";
        selectStat = selectStat.replace("$P{AtmID}", "" + ATMID);
        selectStat = selectStat.replace("$P{DateFrom}", "" + DateFormatter.changeDateAndTimeFormat(TransFromDate));
        selectStat = selectStat.replace("$P{DateTo}", "" + DateFormatter.changeDateAndTimeFormat(TransToDate));
        selectStat = selectStat.replace("$P{SettFrom}", "" + DateFormatter.changeDateAndTimeFormat(SettFromDate));
        selectStat = selectStat.replace("$P{SettTo}", "" + DateFormatter.changeDateAndTimeFormat(SettToDate));
        ResultSet rs = executeQuery(selectStat);
        List<OffusTotalsDTOInter> records = new ArrayList<OffusTotalsDTOInter>();
        while (rs.next()) {
            OffusTotalsDTOInter record = DTOFactory.createOffusTotalsDTO();
            record.setCount(rs.getInt("no"));
            record.setRelatedIncome(rs.getBigDecimal("RelatedIncome"));
            record.setType(rs.getString("TYPE"));
            record.setTerminal(rs.getString("terminal"));
            record.setTerminalName(rs.getString("terminal_name"));
            record.setTransMonth(rs.getString("TRANSACTION_MONTH"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public List<VisaOffusTotalsDTOInter> findVisaOffusNationalTransactionstotals(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable {

        System.out.println("VisaOffusNationalTransactions");
        super.preSelect();
        //to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss')
        String selectStat
                = "  SELECT   SUM (count1) transcount,\n"
                + "           SUM (income) income,\n"
                + "           'Visa Withdrawal National' TYPE,\n"
                + "           terminal,\n"
                + "           (SELECT   name\n"
                + "              FROM   atm_machine@card_atm\n"
                + "             WHERE   application_id = terminal)\n"
                + "              terminal_name,\n"
                + "           TRANSACTION_MONTH\n"
                + "    FROM   (  SELECT                                             /*+parallel*/\n"
                + "                    COUNT (1) count1,\n"
                + "                       (COUNT (1) * 2) income,\n"
                + "                       terminal,\n"
                + "                       TO_CHAR (TRANSACTION_DATE, 'MONTH') TRANSACTION_MONTH\n"
                + "                FROM   disputes\n"
                + "               WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                       AND ( (TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                          '$P{DateFrom}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       )\n"
                + "                                                   AND  TO_DATE (\n"
                + "                                                           '$P{DateTo}',\n"
                + "                                                           'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                        ))\n"
                + "                            OR (NVL ('$P{DateTo}', '0') = '0'\n"
                + "                                AND NVL ('$P{DateFrom}', '0') = '0'))\n"
                + "                       AND network_id = 1\n"
                + "                       AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "                       AND CURRENCY_id = 1\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND transaction_type_master <> 2\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND ( (SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                         '$P{SettFrom}',\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                      )\n"
                + "                                                  AND  TO_DATE (\n"
                + "                                                          '$P{SettTo}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       ))\n"
                + "                            OR (NVL ('$P{SettFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{SettTo}', '0') = '0'))\n"
                + "            GROUP BY   terminal, TO_CHAR (TRANSACTION_DATE, 'MONTH')\n"
                + "            UNION\n"
                + "              SELECT                                             /*+parallel*/\n"
                + "                    COUNT (1) count1,\n"
                + "                       (COUNT (1) * 2) income,\n"
                + "                       terminal,\n"
                + "                       TO_CHAR (TRANSACTION_DATE, 'MONTH') TRANSACTION_MONTH\n"
                + "                FROM   matched_data\n"
                + "               WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                       AND ( (TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                          '$P{DateFrom}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       )\n"
                + "                                                   AND  TO_DATE (\n"
                + "                                                           '$P{DateTo}',\n"
                + "                                                           'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                        ))\n"
                + "                            OR (NVL ('$P{DateTo}', '0') = '0'\n"
                + "                                AND NVL ('$P{DateFrom}', '0') = '0'))\n"
                + "                       AND network_id = 1\n"
                + "                       AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "                       AND CURRENCY_id = 1\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND transaction_type_master <> 2\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND ( (SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                         '$P{SettFrom}',\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                      )\n"
                + "                                                  AND  TO_DATE (\n"
                + "                                                          '$P{SettTo}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       ))\n"
                + "                            OR (NVL ('$P{SettFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{SettTo}', '0') = '0'))\n"
                + "            GROUP BY   terminal, TO_CHAR (TRANSACTION_DATE, 'MONTH'))\n"
                + "GROUP BY   terminal, TRANSACTION_MONTH\n"
                + "UNION\n"
                + "  SELECT   COUNT (1) count1,\n"
                + "           (COUNT (1) * 0.5) income,\n"
                + "           'Visa Decline National' TYPE,\n"
                + "           terminal,\n"
                + "           (SELECT   name\n"
                + "              FROM   atm_machine@card_atm\n"
                + "             WHERE   application_id = terminal)\n"
                + "              terminal_name,\n"
                + "           TO_CHAR (TRANSACTION_DATE, 'MONTH') TRANSACTION_MONTH\n"
                + "    FROM   rejected_network\n"
                + "   WHERE   ( (TRANSACTION_DATE BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                                'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                   AND  TO_DATE ('$P{DateTo}',\n"
                + "                                                 'dd-mm-yyyy hh24:mi:ss'))\n"
                + "            OR (NVL ('$P{DateTo}', '0') = '0'\n"
                + "                AND NVL ('$P{DateFrom}', '0') = '0'))\n"
                + "           AND network_id = 1\n"
                + "           AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "           AND CURRENCY_id = 1\n"
                + "           AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "           AND transaction_type_master <> 2\n"
                + "           AND NVL (amount, 0) <> 0\n"
                + "           AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "           AND ( (SETTLEMENT_DATE BETWEEN TO_DATE ('$P{SettFrom}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                      AND  TO_DATE ('$P{SettTo}',\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'))\n"
                + "                OR (NVL ('$P{SettFrom}', '0') = '0'\n"
                + "                    AND NVL ('$P{SettTo}', '0') = '0'))\n"
                + "GROUP BY   terminal, TO_CHAR (TRANSACTION_DATE, 'MONTH')\n"
                + "ORDER BY   transcount, income";
        selectStat = selectStat.replace("$P{AtmID}", "" + ATMID);
        selectStat = selectStat.replace("$P{DateFrom}", "" + DateFormatter.changeDateAndTimeFormat(TransFromDate));
        selectStat = selectStat.replace("$P{DateTo}", "" + DateFormatter.changeDateAndTimeFormat(TransToDate));
        selectStat = selectStat.replace("$P{SettFrom}", "" + DateFormatter.changeDateAndTimeFormat(SettFromDate));
        selectStat = selectStat.replace("$P{SettTo}", "" + DateFormatter.changeDateAndTimeFormat(SettToDate));
        ResultSet rs = executeQuery(selectStat);
        List<VisaOffusTotalsDTOInter> records = new ArrayList<VisaOffusTotalsDTOInter>();
        while (rs.next()) {
            VisaOffusTotalsDTOInter record = DTOFactory.createVisaOffusTotalsDTO();
            record.setTransCount(rs.getInt("transcount"));
            record.setIncome(rs.getBigDecimal("income"));
            record.setType(rs.getString("TYPE"));
            record.setTerminal(rs.getString("terminal"));
            record.setTerminalName(rs.getString("terminal_name"));
            record.setTransMonth(rs.getString("TRANSACTION_MONTH"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public List<VisaOffusInterTotalsDTOInter> findVisaOffusInternationalTransactionstotals(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable {
        System.out.println("VisaOffusInternationalTransactions");

        super.preSelect();
        //to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss')
        String selectStat = "  SELECT   SUM (count1) NO,\n"
                + "           SUM (totalamount) TotalAmount,\n"
                + "           SUM (income) income,\n"
                + "           'Visa Withdrawal International' TYPE,\n"
                + "           terminal,\n"
                + "           (SELECT   name\n"
                + "              FROM   atm_machine@card_atm\n"
                + "             WHERE   application_id = terminal)\n"
                + "              terminal_name,\n"
                + "           TRANSACTION_MONTH\n"
                + "    FROM   (  SELECT                                             /*+parallel*/\n"
                + "                    COUNT (1) count1,\n"
                + "                       NVL (SUM (amount), 0) totalamount,\n"
                + "                       NVL (SUM (amount * 0.01), 0) income,\n"
                + "                       terminal,\n"
                + "                       TO_CHAR (TRANSACTION_DATE, 'MONTH') TRANSACTION_MONTH\n"
                + "                FROM   disputes\n"
                + "               WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                       AND ( (TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                          '$P{DateFrom}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       )\n"
                + "                                                   AND  TO_DATE (\n"
                + "                                                           '$P{DateTo}',\n"
                + "                                                           'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                        ))\n"
                + "                            OR (NVL ('$P{DateFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{DateTo}', '0') = '0'))\n"
                + "                       AND network_id = 1\n"
                + "                       AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "                       AND NVL (CURRENCY_id, 0) <> 1\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND transaction_type_master <> 2\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND ( (SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                         '$P{SettFrom}',\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                      )\n"
                + "                                                  AND  TO_DATE (\n"
                + "                                                          '$P{SettTo}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       ))\n"
                + "                            OR (NVL ('$P{SettFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{SettTo}', '0') = '0'))\n"
                + "            GROUP BY   terminal, TO_CHAR (TRANSACTION_DATE, 'MONTH')\n"
                + "            UNION\n"
                + "              SELECT                                             /*+parallel*/\n"
                + "                    COUNT (1) count1,\n"
                + "                       NVL (SUM (amount), 0) totalamount,\n"
                + "                       NVL (SUM (amount * 0.01), 0) income,\n"
                + "                       terminal,\n"
                + "                       TO_CHAR (TRANSACTION_DATE, 'MONTH') TRANSACTION_MONTH\n"
                + "                FROM   matched_data\n"
                + "               WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                       AND ( (TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                          '$P{DateFrom}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       )\n"
                + "                                                   AND  TO_DATE (\n"
                + "                                                           '$P{DateTo}',\n"
                + "                                                           'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                        ))\n"
                + "                            OR (NVL ('$P{DateFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{DateTo}', '0') = '0'))\n"
                + "                       AND network_id = 1\n"
                + "                       AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "                       AND NVL (CURRENCY_id, 0) <> 1\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND transaction_type_master <> 2\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND ( (SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                         '$P{SettFrom}',\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                      )\n"
                + "                                                  AND  TO_DATE (\n"
                + "                                                          '$P{SettTo}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       ))\n"
                + "                            OR (NVL ('$P{SettFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{SettTo}', '0') = '0'))\n"
                + "            GROUP BY   terminal, TO_CHAR (TRANSACTION_DATE, 'MONTH'))\n"
                + "GROUP BY   terminal, TRANSACTION_MONTH\n"
                + "UNION\n"
                + "  SELECT   COUNT (1) NO,\n"
                + "           NULL TotalAmount,\n"
                + "           (COUNT (1) * 0.3) income,\n"
                + "           'Visa Decline Interational' TYPE,\n"
                + "           terminal,\n"
                + "           (SELECT   name\n"
                + "              FROM   atm_machine@card_atm\n"
                + "             WHERE   application_id = terminal)\n"
                + "              terminal_name,\n"
                + "           TO_CHAR (TRANSACTION_DATE, 'MONTH') TRANSACTION_MONTH\n"
                + "    FROM   rejected_network\n"
                + "   WHERE   ( (TRANSACTION_DATE BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                                'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                   AND  TO_DATE ('$P{DateTo}',\n"
                + "                                                 'dd-mm-yyyy hh24:mi:ss'))\n"
                + "            OR (NVL ('$P{DateFrom}', '0') = '0'\n"
                + "                AND NVL ('$P{DateTo}', '0') = '0'))\n"
                + "           AND network_id = 1\n"
                + "           AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "           AND CURRENCY_id <> 1\n"
                + "           AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "           AND transaction_type_master <> 2\n"
                + "           AND NVL (amount, 0) <> 0\n"
                + "           AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "           AND ( (SETTLEMENT_DATE BETWEEN TO_DATE ('$P{SettFrom}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                      AND  TO_DATE ('$P{SettTo}',\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'))\n"
                + "                OR (NVL ('$P{SettFrom}', '0') = '0'\n"
                + "                    AND NVL ('$P{SettTo}', '0') = '0'))\n"
                + "GROUP BY   terminal, TO_CHAR (TRANSACTION_DATE, 'MONTH')\n"
                + "UNION\n"
                + "  SELECT   COUNT (1) NO,\n"
                + "           NULL TotalAmount,\n"
                + "           (COUNT (1) * 0.3) income,\n"
                + "           'Visa BalanceInquiry Interational' TYPE,\n"
                + "           terminal,\n"
                + "           (SELECT   name\n"
                + "              FROM   atm_machine@card_atm\n"
                + "             WHERE   application_id = terminal)\n"
                + "              terminal_name,\n"
                + "           TO_CHAR (TRANSACTION_DATE, 'MONTH') TRANSACTION_MONTH\n"
                + "    FROM   rejected_network\n"
                + "   WHERE   ( (TRANSACTION_DATE BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                                'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                   AND  TO_DATE ('$P{DateTo}',\n"
                + "                                                 'dd-mm-yyyy hh24:mi:ss'))\n"
                + "            OR (NVL ('$P{DateFrom}', '0') = '0'\n"
                + "                AND NVL ('$P{DateTo}', '0') = '0'))\n"
                + "           AND network_id = 1\n"
                + "           AND DEC_DUPLICATE (card_no) NOT LIKE '457871%'\n"
                + "           AND CURRENCY_id <> 1\n"
                + "           AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "           AND transaction_type_master <> 2\n"
                + "           AND NVL (amount, 0) = 0\n"
                + "           AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "           AND ( (SETTLEMENT_DATE BETWEEN TO_DATE ('$P{SettFrom}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                      AND  TO_DATE ('$P{SettTo}',\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'))\n"
                + "                OR (NVL ('$P{SettFrom}', '0') = '0'\n"
                + "                    AND NVL ('$P{SettTo}', '0') = '0'))\n"
                + "GROUP BY   terminal, TO_CHAR (TRANSACTION_DATE, 'MONTH')\n"
                + "ORDER BY   NO, TotalAmount";
        selectStat = selectStat.replace("$P{AtmID}", "" + ATMID);
        selectStat = selectStat.replace("$P{DateFrom}", "" + DateFormatter.changeDateAndTimeFormat(TransFromDate));
        selectStat = selectStat.replace("$P{DateTo}", "" + DateFormatter.changeDateAndTimeFormat(TransToDate));
        selectStat = selectStat.replace("$P{SettFrom}", "" + DateFormatter.changeDateAndTimeFormat(SettFromDate));
        selectStat = selectStat.replace("$P{SettTo}", "" + DateFormatter.changeDateAndTimeFormat(SettToDate));
        ResultSet rs = executeQuery(selectStat);
        List<VisaOffusInterTotalsDTOInter> records = new ArrayList<VisaOffusInterTotalsDTOInter>();
        while (rs.next()) {
            VisaOffusInterTotalsDTOInter record = DTOFactory.createVisaOffusInterTotalsDTO();

            record.setCount(rs.getInt("NO"));
            record.setIncome(rs.getBigDecimal("income"));
            record.setTotalAmount(rs.getBigDecimal("TotalAmount"));
            record.setType(rs.getString("TYPE"));
            record.setTerminal(rs.getString("terminal"));
            record.setTerminalName(rs.getString("terminal_name"));
            record.setTransMonth(rs.getString("TRANSACTION_MONTH"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    public List<OnusTotalsDTOInter> findOnusTransactionstotals(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable {

        System.out.println("OnusTransactions");
        super.preSelect();
        //to_date('$P{DateFrom}','dd.MM.yyyy hh24:mi:ss')
        String selectStat = "/* Formatted on 8/3/2017 12:58:49 PM (QP5 v5.115.810.9015) */\n"
                + "--OnusTransactions\n"
                + "\n"
                + "  SELECT   SUM (count1) Count_Onus,\n"
                + "           SUM (amount1) Total_Amount,\n"
                + "           'Withdrawal Trx' TYPE,\n"
                + "           terminal,\n"
                + "           (SELECT   name\n"
                + "              FROM   atm_machine@card_atm\n"
                + "             WHERE   application_id = terminal)\n"
                + "              terminal_name,\n"
                + "           TRANSACTION_MONTH\n"
                + "    FROM   (  SELECT                                             /*+parallel*/\n"
                + "                    COUNT (1) count1,\n"
                + "                       SUM (amount) amount1,\n"
                + "                       terminal,\n"
                + "                       TO_CHAR (TRANSACTION_DATE, 'MONTH') TRANSACTION_MONTH\n"
                + "                FROM   disputes\n"
                + "               WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                       AND ( (TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                          '$P{DateFrom}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       )\n"
                + "                                                   AND  TO_DATE (\n"
                + "                                                           '$P{DateTo}',\n"
                + "                                                           'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                        ))\n"
                + "                            OR (NVL ('$P{DateFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{DateTo}', '0') = '0'))\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND network_id IN (1, 3)\n"
                + "                       AND (DEC_DUPLICATE (card_no) LIKE '627632%'\n"
                + "                            OR DEC_DUPLICATE (card_no) LIKE '457871%')\n"
                + "                       AND transaction_type_master <> 2\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND ( (SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                         '$P{SettFrom}',\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                      )\n"
                + "                                                  AND  TO_DATE (\n"
                + "                                                          '$P{SettTo}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       ))\n"
                + "                            OR (NVL ('$P{SettFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{SettTo}', '0') = '0'))\n"
                + "            GROUP BY   terminal, TO_CHAR (TRANSACTION_DATE, 'MONTH')\n"
                + "            UNION\n"
                + "              SELECT                                             /*+parallel*/\n"
                + "                    COUNT (1) count1,\n"
                + "                       SUM (amount) amount1,\n"
                + "                       terminal,\n"
                + "                       TO_CHAR (TRANSACTION_DATE, 'MONTH') TRANSACTION_MONTH\n"
                + "                FROM   matched_data\n"
                + "               WHERE   RECORD_TYPE = 1 AND MATCHING_TYPE = 1\n"
                + "                       AND ( (TRANSACTION_DATE BETWEEN TO_DATE (\n"
                + "                                                          '$P{DateFrom}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       )\n"
                + "                                                   AND  TO_DATE (\n"
                + "                                                           '$P{DateTo}',\n"
                + "                                                           'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                        ))\n"
                + "                            OR (NVL ('$P{DateFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{DateTo}', '0') = '0'))\n"
                + "                       AND (DEC_DUPLICATE (card_no) LIKE '627632%'\n"
                + "                            OR DEC_DUPLICATE (card_no) LIKE '457871%')\n"
                + "                       AND transaction_type_master <> 2\n"
                + "                       AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "                       AND network_id IN (1, 3)\n"
                + "                       AND ( (SETTLEMENT_DATE BETWEEN TO_DATE (\n"
                + "                                                         '$P{SettFrom}',\n"
                + "                                                         'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                      )\n"
                + "                                                  AND  TO_DATE (\n"
                + "                                                          '$P{SettTo}',\n"
                + "                                                          'dd-mm-yyyy hh24:mi:ss'\n"
                + "                                                       ))\n"
                + "                            OR (NVL ('$P{SettFrom}', '0') = '0'\n"
                + "                                AND NVL ('$P{SettTo}', '0') = '0'))\n"
                + "            GROUP BY   terminal, TO_CHAR (TRANSACTION_DATE, 'MONTH'))\n"
                + "GROUP BY   terminal, TRANSACTION_MONTH\n"
                + "UNION\n"
                + "  SELECT   COUNT (1) Count_Onus,\n"
                + "           SUM (amount) Total_Amount,\n"
                + "           'Balance Inquiry Trx' TYPE,\n"
                + "           terminal,\n"
                + "           (SELECT   name\n"
                + "              FROM   atm_machine@card_atm\n"
                + "             WHERE   application_id = terminal)\n"
                + "              terminal_name,\n"
                + "           TO_CHAR (TRANSACTION_DATE, 'MONTH') TRANSACTION_MONTH\n"
                + "    FROM   rejected_network\n"
                + "   WHERE   ( (TRANSACTION_DATE BETWEEN TO_DATE ('$P{DateFrom}',\n"
                + "                                                'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                   AND  TO_DATE ('$P{DateTo}',\n"
                + "                                                 'dd-mm-yyyy hh24:mi:ss'))\n"
                + "            OR (NVL ('$P{DateFrom}', '0') = '0'\n"
                + "                AND NVL ('$P{DateTo}', '0') = '0'))\n"
                + "           AND (DEC_DUPLICATE (card_no) LIKE '627632%'\n"
                + "                OR DEC_DUPLICATE (card_no) LIKE '457871%')\n"
                + "           AND transaction_type_master <> 2\n"
                + "           AND (terminal = '$P{AtmID}' OR NVL ('$P{AtmID}', '0') = '0')\n"
                + "           AND network_id IN (1, 3)\n"
                + "           AND NVL (amount, 0) = 0\n"
                + "           AND ( (SETTLEMENT_DATE BETWEEN TO_DATE ('$P{SettFrom}',\n"
                + "                                                   'dd-mm-yyyy hh24:mi:ss')\n"
                + "                                      AND  TO_DATE ('$P{SettTo}',\n"
                + "                                                    'dd-mm-yyyy hh24:mi:ss'))\n"
                + "                OR (NVL ('$P{SettFrom}', '0') = '0'\n"
                + "                    AND NVL ('$P{SettTo}', '0') = '0'))\n"
                + "GROUP BY   terminal, TO_CHAR (TRANSACTION_DATE, 'MONTH')\n"
                + "ORDER BY   Count_Onus, Total_Amount";
        selectStat = selectStat.replace("$P{AtmID}", "" + ATMID);
        selectStat = selectStat.replace("$P{DateFrom}", "" + DateFormatter.changeDateAndTimeFormat(TransFromDate));
        selectStat = selectStat.replace("$P{DateTo}", "" + DateFormatter.changeDateAndTimeFormat(TransToDate));
        selectStat = selectStat.replace("$P{SettFrom}", "" + DateFormatter.changeDateAndTimeFormat(SettFromDate));
        selectStat = selectStat.replace("$P{SettTo}", "" + DateFormatter.changeDateAndTimeFormat(SettToDate));
        ResultSet rs = executeQuery(selectStat);
        List<OnusTotalsDTOInter> records = new ArrayList<OnusTotalsDTOInter>();
        while (rs.next()) {
            OnusTotalsDTOInter record = DTOFactory.createOnusTotalsDTO();
            record.setCount(rs.getInt("Count_Onus"));
            record.setTotalAmount(rs.getBigDecimal("Total_Amount"));
            record.setType(rs.getString("Type"));
            record.setTerminal(rs.getString("terminal"));
            record.setTerminalName(rs.getString("terminal_name"));
            record.setTransMonth(rs.getString("TRANSACTION_MONTH"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }
}
