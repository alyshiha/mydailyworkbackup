/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.bus.dto.userPrivilegeDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface userPrivilegeDAOInter {
    List<String> findPrivelage(Integer userid, String PageName) throws Throwable;

    Boolean findDuplicatesaveRecord(int user, int menu, int prev, int id) throws Throwable;

    Boolean findDuplicateRecord(int user, int menu, int prev) throws Throwable;

    Object delete(Object... obj) throws Throwable;

    List<userPrivilegeDTOInter> findAll() throws Throwable;

    List<userPrivilegeDTOInter> findRecord(String userid) throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;

}
