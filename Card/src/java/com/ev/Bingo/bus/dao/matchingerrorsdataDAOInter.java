package com.ev.Bingo.bus.dao;

import com.ev.Bingo.bus.dto.usersDTOInter;
import java.util.Date;
import java.util.List;
import java.sql.SQLException;

public interface matchingerrorsdataDAOInter {

    void MarkAsDispute(Object... obj) throws Throwable;

    Object findRecordsList(Object... obj) throws Throwable;

    Object findRecordsAll(usersDTOInter UserDTO, Date loadingfrom, Date loadingto, Integer network, Integer recordtypeid) throws Throwable;

    void MarkAsMatched(Object... obj) throws Throwable;
}
