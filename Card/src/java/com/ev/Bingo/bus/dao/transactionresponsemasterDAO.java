package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.transactionresponsemasterDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class transactionresponsemasterDAO extends BaseDAO implements transactionresponsemasterDAOInter {

    protected transactionresponsemasterDAO() {
        super();
        super.setTableName("TRANSACTION_RESPONSE_MASTER");
    }

    //used in system rules
    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        transactionresponsemasterDTOInter RecordToSelect = (transactionresponsemasterDTOInter) obj[0];
        String selectStat = "Select ID,NAME From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        transactionresponsemasterDTOInter SelectedRecord = DTOFactory.createtransactionresponsemasterDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,NAME From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<transactionresponsemasterDTOInter> Records = new ArrayList<transactionresponsemasterDTOInter>();
        while (rs.next()) {
            transactionresponsemasterDTOInter SelectedRecord = DTOFactory.createtransactionresponsemasterDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactionresponsemasterDTOInter RecordToInsert = (transactionresponsemasterDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence("TRANSACTION_RESPONSE_MAS"));
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
            String insertStat = "insert into $table"
                    + " (ID,NAME) "
                    + " values "
                    + " ($id,'$name')";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
            insertStat = insertStat.replace("$name", "" + RecordToInsert.getname());
            try {
                msg = super.executeUpdate(insertStat);
                msg = msg + " Record Has Been Inserted";
                super.postUpdate("Adding New TRANSACTION RESPONSE Master CODE Record With Name " + RecordToInsert.getname(), false);
            } catch (Exception ex) {
                msg = "Please Enter A Unique Code";
            }
        }
        return msg;
    }

    public String ValidateNullList(Object... obj) {
        List<transactionresponsemasterDTOInter> AllRecords = (List<transactionresponsemasterDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (transactionresponsemasterDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getid() == 0) {

                Validate = Validate + " ," + String.valueOf(i);
            }
            if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {

                Validate = Validate + "," + String.valueOf(i);
            }
            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = "Record With Index " + Validate + " Contain Null Name Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    public String ValidateNull(Object... obj) {
        transactionresponsemasterDTOInter RecordToInsert = (transactionresponsemasterDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getid() == 0) {
            Validate = Validate + "   id  ";
        }
        if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {
            Validate = Validate + "   name  ";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required fields";
        }
        return Validate;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactionresponsemasterDTOInter RecordToDelete = (transactionresponsemasterDTOInter) obj[0];
        String msg = ValidateNull(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  ID= $id";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " Master Record Has Been deleted";
            super.postUpdate("Adding New TRANSACTION RESPONSE CODE Master Record With Name " + RecordToDelete.getname(), false);
        }
        return msg;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<transactionresponsemasterDTOInter> entities = (List<transactionresponsemasterDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No TRANSACTION Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "update TRANSACTION_RESPONSE_MASTER set NAME = ? where ID = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        transactionresponsemasterDTOInter RecordtoInsert = entities.get(i);
                        statement.setString(1, RecordtoInsert.getname());
                        statement.setInt(2, RecordtoInsert.getid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " Record has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                           CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
