/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
 
import com.ev.Bingo.bus.dto.UserNetworkDTOInter;
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class UserNetworkDAO extends BaseDAO implements UserNetworkDAOInter {

    protected UserNetworkDAO() {
        super();
        super.setTableName("USER_NETWORK");
    }

    @Override
    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String msg = "";
        UserNetworkDTOInter RecordToInsert = (UserNetworkDTOInter) obj[0];
        String insertStat = "insert into $table"
                + " (userid,networkid) "
                + " values "
                + " ($userid,$networkid)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$userid", "" + RecordToInsert.getUserid());
        insertStat = insertStat.replace("$networkid", "" + RecordToInsert.getNetworkid());
        msg = (String) super.executeUpdate(insertStat);
        super.postUpdate("Adding New User Network Record With Id " + RecordToInsert.getNetworkid() + " To User " + RecordToInsert.getUserid(), false);
        return msg + " User Network Has Been Inserted";
    }

    @Override
    public Boolean ValidateNull(Object... obj) {
        UserNetworkDTOInter RecordToInsert = (UserNetworkDTOInter) obj[0];
        if (RecordToInsert.getUserid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getNetworkid() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        UserNetworkDTOInter RecordToDelete = (UserNetworkDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  userid = $userid AND networkid = $networkid";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$userid", "" + RecordToDelete.getUserid());
        deleteStat = deleteStat.replace("$networkid", "" + RecordToDelete.getNetworkid());
        String result = (String) super.executeUpdate(deleteStat);
        super.postUpdate(result + " Detailed Network Has Been Deleted From Parent ID " + RecordToDelete.getUserid(), false);
        return result + " Detailed Network Has Been Deleted";
    }

    @Override
    public Object findRecordsListCS(Object... obj) throws Throwable {
        super.preSelect();
        usersDTOInter RecordToSelect = (usersDTOInter) obj[0];
        String selectStat = "Select userid,networkid,rowid  From $table"
                + "  where  userid= $userid";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$userid", "" + RecordToSelect.getUserid());
        ResultSet rs = executeQuery(selectStat);
        List<UserNetworkDTOInter> Records = new ArrayList<UserNetworkDTOInter>();
        while (rs.next()) {
            UserNetworkDTOInter SelectedRecord = DTOFactory.createUserNetworkDTO();
            SelectedRecord.setUserid(rs.getInt("userid"));
            SelectedRecord.setNetworkid(rs.getInt("networkid"));
            SelectedRecord.setRowid(rs.getString("rowid"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public String ValidateNullList(Object... obj) {
        List<UserNetworkDTOInter> AllRecords = (List<UserNetworkDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (UserNetworkDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getUserid() == 0) {
                Validate = Validate + "," + String.valueOf(i);
            }
            if (RecordToInsert.getNetworkid() == 0) {
                Validate = Validate + "," + String.valueOf(i);
            }
            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = "Record With Index " + Validate + " Contain Null Card Number Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    @Override
    public Object save(Object... obj) throws SQLException, Throwable {
        List<UserNetworkDTOInter> entities = (List<UserNetworkDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.isEmpty()) {
            mess = "No Columns Found To Be Updated";
        } else {

            String insertStat = "UPDATE user_network SET networkid = ? WHERE rowid = ?";
            Connection connection = null;
            PreparedStatement statement = null;
            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            try {
               connection = CoonectionHandler.getInstance().getConnection();
                statement = connection.prepareStatement(insertStat);
                for (int i = 0; i < entities.size(); i++) {
                    UserNetworkDTOInter RecordtoInsert = entities.get(i);
                    statement.setInt(1, RecordtoInsert.getNetworkid());
                    statement.setString(2, RecordtoInsert.getRowid());
                    statement.addBatch();
                    if ((i + 1) % 1000 == 0) {
                        statement.executeBatch();
                    }
                }
                int[] numUpdates = statement.executeBatch();
                Integer valid = 0, notvalid = 0;

                for (int i = 0; i < numUpdates.length; i++) {
                    if (numUpdates[i] == -2) {
                        valid++;
                        mess = valid + " rows has been updated";
                    } else {
                        notvalid++;
                        mess2 = notvalid + " can`t be updated";
                    }
                }
                if (!mess2.equals("")) {
                    mess = mess + "," + mess2;
                }
            } finally {
                if (statement != null) {
                    try {
                        connection.commit();
                        statement.close();
                    } catch (SQLException logOrIgnore) {
                    }
                }
                if (connection != null) {
                    try {
                CoonectionHandler.getInstance().returnConnection(connection);
                    } catch (SQLException logOrIgnore) {
                    }
                }
            }
            return mess;

        }
        return mess;
    }
}
