package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.blacklistedcardDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class blacklistedcardDAO extends BaseDAO implements blacklistedcardDAOInter {

    protected blacklistedcardDAO() {
        super();
        super.setTableName("BLACK_LISTED_CARD");
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        blacklistedcardDTOInter RecordToDelete = (blacklistedcardDTOInter) obj[0];
        String msg = ValidateNull(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  CARD_NO= "+super.GetEncrypt("$cardno");
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$cardno", "" + RecordToDelete.getcardno());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " record Has Been deleted";
            super.postUpdate("Deleting A Black Listed Record With Card Number " + RecordToDelete.getcardno(), false);
        }
        return msg;
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        blacklistedcardDTOInter RecordToInsert = (blacklistedcardDTOInter) obj[0];
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
//RecordToInsert.setid(super.generateSequence(super.getTableName()));
            String insertStat = "insert into $table"
                    + " (CARD_NO,CUSTOMER_NO,COMMENTS,BLACK_LISTED_DATE) "
                    + " values "
                    + " ("+super.GetEncrypt("$cardno")+",'$customerno','$comments',to_date('$blacklisteddate','dd.MM.yyyy hh24:mi:ss'))";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$cardno", "" + RecordToInsert.getcardno());
            insertStat = insertStat.replace("$customerno", "" + RecordToInsert.getcustomerno());
            insertStat = insertStat.replace("$comments", "" + RecordToInsert.getcomments());
            insertStat = insertStat.replace("$blacklisteddate", "" + DateFormatter.changeDateAndTimeFormat(RecordToInsert.getblacklisteddate()));
            msg = super.executeUpdate(insertStat);
            msg = msg + " Record Has Been Inserted";
            super.postUpdate("Adding New Black Listed Record With Card Number " + RecordToInsert.getcardno(), false);
        }
        return msg;
    }

    public String ValidateNull(Object... obj) {
        blacklistedcardDTOInter RecordToInsert = (blacklistedcardDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getcardno() == null || RecordToInsert.getcardno().equals("")) {
            Validate = Validate + "   Card Number  ";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
        }
        return Validate;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<blacklistedcardDTOInter> entities = (List<blacklistedcardDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Users Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "update BLACK_LISTED_CARD set CUSTOMER_NO = ?,COMMENTS = ?,BLACK_LISTED_DATE = to_date(?,'dd.MM.yyyy hh24:mi:ss') where "+super.GetDecryptCardNo()+" = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                   connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        blacklistedcardDTOInter RecordtoInsert = entities.get(i);
                        statement.setString(1, RecordtoInsert.getcustomerno());
                        statement.setString(2, RecordtoInsert.getcomments());
                        statement.setString(3, DateFormatter.changeDateAndTimeFormat(RecordtoInsert.getblacklisteddate()).toString());
                        statement.setString(4, RecordtoInsert.getcardno());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                    CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }

    public String ValidateNullList(Object... obj) {
        List<blacklistedcardDTOInter> AllRecords = (List<blacklistedcardDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (blacklistedcardDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getcardno() == null || RecordToInsert.getcardno().equals("")) {
                Validate = Validate + "," + String.valueOf(i);
            }
            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = "Record With Index " + Validate + " Contain Null Card Number Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select "+super.GetDecryptCardNo()+" CARD_NO,CUSTOMER_NO,COMMENTS,BLACK_LISTED_DATE From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<blacklistedcardDTOInter> Records = new ArrayList<blacklistedcardDTOInter>();
        while (rs.next()) {
            blacklistedcardDTOInter SelectedRecord = DTOFactory.createblacklistedcardDTO();
            SelectedRecord.setcardno(rs.getString("CARD_NO"));
            SelectedRecord.setcustomerno(rs.getString("CUSTOMER_NO"));
            SelectedRecord.setcomments(rs.getString("COMMENTS"));
            SelectedRecord.setblacklisteddate(rs.getTimestamp("BLACK_LISTED_DATE"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }
}
