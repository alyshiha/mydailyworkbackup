package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.diffamountreportDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class diffamountreportDAO extends BaseDAO implements diffamountreportDAOInter {

    protected diffamountreportDAO() {
        super();
        super.setTableName("DIFF_AMOUNT_REPORT");
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        diffamountreportDTOInter RecordToSelect = (diffamountreportDTOInter) obj[0];
        String selectStat = "Select TRANSDATE,CARDNUMBER,NETWORK,SWITCH,NETWORKDIFF,SWITCHDIFF,REORTID From $table"
                + "  where  ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<diffamountreportDTOInter> Records = new ArrayList<diffamountreportDTOInter>();
        while (rs.next()) {
            diffamountreportDTOInter SelectedRecord = DTOFactory.creatediffamountreportDTO();
            SelectedRecord.settransdate(rs.getTimestamp("TRANSDATE"));
            SelectedRecord.setcardnumber(rs.getString("CARDNUMBER"));
            SelectedRecord.setnetwork(rs.getInt("NETWORK"));
            SelectedRecord.setSwitchvalue(rs.getInt("SWITCH"));
            SelectedRecord.setnetworkdiff(rs.getInt("NETWORKDIFF"));
            SelectedRecord.setswitchdiff(rs.getInt("SWITCHDIFF"));
            SelectedRecord.setreortid(rs.getInt("REORTID"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select TRANSDATE,CARDNUMBER,NETWORK,SWITCH,NETWORKDIFF,SWITCHDIFF,REORTID From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<diffamountreportDTOInter> Records = new ArrayList<diffamountreportDTOInter>();
        while (rs.next()) {
            diffamountreportDTOInter SelectedRecord = DTOFactory.creatediffamountreportDTO();
            SelectedRecord.settransdate(rs.getTimestamp("TRANSDATE"));
            SelectedRecord.setcardnumber(rs.getString("CARDNUMBER"));
            SelectedRecord.setnetwork(rs.getInt("NETWORK"));
            SelectedRecord.setSwitchvalue(rs.getInt("SWITCH"));
            SelectedRecord.setnetworkdiff(rs.getInt("NETWORKDIFF"));
            SelectedRecord.setswitchdiff(rs.getInt("SWITCHDIFF"));
            SelectedRecord.setreortid(rs.getInt("REORTID"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

}
