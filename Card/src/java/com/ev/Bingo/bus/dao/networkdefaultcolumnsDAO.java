package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import com.ev.Bingo.base.util.DateFormatter;
 
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.networkdefaultcolumnsDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class networkdefaultcolumnsDAO extends BaseDAO implements networkdefaultcolumnsDAOInter {

    protected networkdefaultcolumnsDAO() {
        super();
        super.setTableName("NETWORK_DEFAULT_COLUMNS");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        networkdefaultcolumnsDTOInter RecordToInsert = (networkdefaultcolumnsDTOInter) obj[0];
//RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String insertStat = "insert into $table"
                + " (NETWORK_ID,COLUMN_ID,SELECTED,orderby) "
                + " values "
                + " ($networkid,$columnid,$SELECTED,$orderby)";
        insertStat = insertStat.replace("$table", "" + super.getTableName());
        insertStat = insertStat.replace("$networkid", "" + RecordToInsert.getnetworkid());
        insertStat = insertStat.replace("$columnid", "" + RecordToInsert.getcolumnid());
        insertStat = insertStat.replace("$orderby", "" + RecordToInsert.getOrderby());
        if (RecordToInsert.getbselected() == Boolean.FALSE) {
            RecordToInsert.setselected(2);
        } else {
            RecordToInsert.setselected(1);
        }
        insertStat = insertStat.replace("$SELECTED", "" + RecordToInsert.getselected());
        String msg = super.executeUpdate(insertStat);
        super.postUpdate("Column Record With ID " + RecordToInsert.getcolumnid() + " Has Been Inserted To Network View " + RecordToInsert.getnetworkid(), false);

        return msg + "Column Has Been Inserted";
    }

    public Boolean ValidateNull(Object... obj) {
        networkdefaultcolumnsDTOInter RecordToInsert = (networkdefaultcolumnsDTOInter) obj[0];
        if (RecordToInsert.getnetworkid() == 0) {
            return Boolean.FALSE;
        }
        if (RecordToInsert.getcolumnid() == 0) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        networkdefaultcolumnsDTOInter RecordToUpdate = (networkdefaultcolumnsDTOInter) obj[0];
        String UpdateStat = "update $table set "
                + " (NETWORK_ID= $networkid,COLUMN_ID= $columnid,orderby = $orderby) "
                + "  where  NETWORK_ID= $networkid,COLUMN_ID= $columnid";
        UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
        UpdateStat = UpdateStat.replace("$networkid", "" + RecordToUpdate.getnetworkid());
        UpdateStat = UpdateStat.replace("$columnid", "" + RecordToUpdate.getcolumnid());
        UpdateStat = UpdateStat.replace("$orderby", "" + RecordToUpdate.getOrderby());
        super.executeUpdate(UpdateStat);
        super.postUpdate("Update Record In Table NETWORK_DEFAULT_COLUMNS", false);
        return null;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        networkdefaultcolumnsDTOInter RecordToDelete = (networkdefaultcolumnsDTOInter) obj[0];
        String deleteStat = "delete from $table "
                + "  where  NETWORK_ID= $networkid AND COLUMN_ID= $columnid";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        deleteStat = deleteStat.replace("$networkid", "" + RecordToDelete.getnetworkid());
        deleteStat = deleteStat.replace("$columnid", "" + RecordToDelete.getcolumnid());
        String msg = super.executeUpdate(deleteStat);
        super.postUpdate("Column Record With ID " + RecordToDelete.getcolumnid() + " HAs Been Delete From Network View " + RecordToDelete.getnetworkid(), false);
        return msg + " Column Has Been Deleted";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table NETWORK_DEFAULT_COLUMNS", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        networkdefaultcolumnsDTOInter RecordToSelect = (networkdefaultcolumnsDTOInter) obj[0];
        String selectStat = "Select NETWORK_ID,COLUMN_ID,orderby From $table"
                + "  where  NETWORK_ID= $networkid,COLUMN_ID= $columnid";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$networkid", "" + RecordToSelect.getnetworkid());
        selectStat = selectStat.replace("$columnid", "" + RecordToSelect.getcolumnid());
        ResultSet rs = executeQuery(selectStat);
        networkdefaultcolumnsDTOInter SelectedRecord = DTOFactory.createnetworkdefaultcolumnsDTO();
        while (rs.next()) {
            SelectedRecord.setnetworkid(rs.getInt("NETWORK_ID"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.setOrderby(rs.getInt("orderby"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        networksDTOInter RecordToSelect = (networksDTOInter) obj[0];
        String selectStat = "Select ROWID,NETWORK_ID,COLUMN_ID,SELECTED,orderby From $table"
                + "  where  NETWORK_ID= $networkid order by orderby";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$networkid", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<networkdefaultcolumnsDTOInter> Records = new ArrayList<networkdefaultcolumnsDTOInter>();
        while (rs.next()) {
            networkdefaultcolumnsDTOInter SelectedRecord = DTOFactory.createnetworkdefaultcolumnsDTO();
            SelectedRecord.setrowid(rs.getString("ROWID"));
            SelectedRecord.setnetworkid(rs.getInt("NETWORK_ID"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.setselected(rs.getInt("SELECTED"));
            SelectedRecord.setOrderby(rs.getInt("orderby"));
            if (SelectedRecord.getselected() == 1) {
                SelectedRecord.setbselected(Boolean.TRUE);
            } else {
                SelectedRecord.setbselected(Boolean.FALSE);
            };
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }
    
    public Object findcolsList(Object... obj) throws Throwable {
        super.preSelect();
        networksDTOInter RecordToSelect = (networksDTOInter) obj[0];
        String selectStat = "Select (select f.name from file_column_definition f where f.id = COLUMN_ID) COLUMNNames  From $table"
                + "  where  NETWORK_ID= $networkid order by orderby";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$networkid", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<String> Records = new ArrayList<String>();
        while (rs.next()) {
            String SelectedRecord = "";
            SelectedRecord = rs.getString("COLUMNNames");
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select NETWORK_ID,COLUMN_ID,orderby From $table order by orderby";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<networkdefaultcolumnsDTOInter> Records = new ArrayList<networkdefaultcolumnsDTOInter>();
        while (rs.next()) {
            networkdefaultcolumnsDTOInter SelectedRecord = DTOFactory.createnetworkdefaultcolumnsDTO();
            SelectedRecord.setnetworkid(rs.getInt("NETWORK_ID"));
            SelectedRecord.setcolumnid(rs.getInt("COLUMN_ID"));
            SelectedRecord.setOrderby(rs.getInt("orderby"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }
 public Object saveorder(Object... obj) throws SQLException, Throwable {
        List<String> entities = (List<String>) obj[0];
        Integer network = (Integer) obj[1];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Files Found";
        } else {
            String insertStat = "update NETWORK_DEFAULT_COLUMNS set orderby = ? where COLUMN_ID IN (select f.id from file_column_definition f WHERE f.name = ?) and NETWORK_ID = ?";
            Connection connection = null;
            PreparedStatement statement = null;
            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            try {
                connection = CoonectionHandler.getInstance().getConnection();
                statement = connection.prepareStatement(insertStat);
                int order = 1;
                for (int i = 0; i < entities.size(); i++) {
                    statement.setInt(1, order);
                    statement.setString(2, entities.get(i));
                    statement.setInt(3, network);
                    statement.addBatch();
                    order = order + 1;
                    if ((i + 1) % 1000 == 0) {
                        statement.executeBatch();
                    }
                }
                int[] numUpdates = null;
                try {
                    numUpdates = statement.executeBatch();
                } catch (Exception e) {
                    mess = "can not duplicate Column at the same network";
                }
                Integer valid = 0, notvalid = 0;
                if (!"can not duplicate Column at the same network".equals(mess)) {
                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " Record has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                }
            } finally {
                if (statement != null) {
                    try {
                        connection.commit();
                        statement.close();
                    } catch (SQLException logOrIgnore) {
                    }
                }
                if (connection != null) {
                    try {
          CoonectionHandler.getInstance().returnConnection(connection);
                    } catch (SQLException logOrIgnore) {
                    }
                }
            }
            return mess;
        }
        return mess;
    }
    public Object save(Object... obj) throws SQLException, Throwable {
        List<networkdefaultcolumnsDTOInter> entities = (List<networkdefaultcolumnsDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Files Found";
        } else {
            String insertStat = "update NETWORK_DEFAULT_COLUMNS set COLUMN_ID = ?,SELECTED = ?,orderby = ? where rowid = ?";
            Connection connection = null;
            PreparedStatement statement = null;
            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            try {
                connection = CoonectionHandler.getInstance().getConnection();
                
                statement = connection.prepareStatement(insertStat);
                for (int i = 0; i < entities.size(); i++) {
                    networkdefaultcolumnsDTOInter RecordtoInsert = entities.get(i);
                    statement.setInt(1, RecordtoInsert.getcolumnid());
                    if (RecordtoInsert.getbselected() == Boolean.FALSE) {
                        RecordtoInsert.setselected(2);
                    } else {
                        RecordtoInsert.setselected(1);
                    }
                    statement.setInt(2, RecordtoInsert.getselected());
                    statement.setInt(3, RecordtoInsert.getOrderby());
                    statement.setString(4, RecordtoInsert.getrowid());
                    statement.addBatch();
                    if ((i + 1) % 1000 == 0) {
                        statement.executeBatch();
                    }
                }
                int[] numUpdates = null;
                try {
                    numUpdates = statement.executeBatch();
                } catch (Exception e) {
                    mess = "can not duplicate Column at the same network";
                }
                Integer valid = 0, notvalid = 0;
                if (!"can not duplicate Column at the same network".equals(mess)) {
                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " Record has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                }
            } finally {
                if (statement != null) {
                    try {
                        connection.commit();
                        statement.close();
                    } catch (SQLException logOrIgnore) {
                    }
                }
                if (connection != null) {
                    try {
                        CoonectionHandler.getInstance().returnConnection(connection);
                    } catch (SQLException logOrIgnore) {
                    }
                }
            }
            return mess;
        }
        return mess;
    }
}
