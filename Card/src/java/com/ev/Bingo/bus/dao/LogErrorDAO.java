/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.ExceptionLoadingErrorsDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class LogErrorDAO extends BaseDAO implements LogErrorDAOInter{

    @Override
    public Object findExceptionLoadingErrors(String dateFrom, String dateTo) throws Throwable {
        super.preSelect();
        String selectStatment = "SELECT FUNCTION_NAME, ERROR_CODE, ERROR_NAME, ERROR_DATE"
                + " FROM EXCEPTION_CASHMANGMENT_ERRORS"
                + " WHERE ERROR_DATE >= TO_DATE ('$P{DateFrom}', 'dd-mm-yyyy hh24:mi:ss')"
                + " AND ERROR_DATE <= TO_DATE ('$P{DateTo}', 'dd-mm-yyyy hh24:mi:ss')";
        selectStatment = selectStatment.replace("$P{DateFrom}", dateFrom);
        selectStatment = selectStatment.replace("$P{DateTo}", dateTo);
        List<ExceptionLoadingErrorsDTOInter> Records = new ArrayList<ExceptionLoadingErrorsDTOInter>();
        ResultSet rs = executeQueryReport(selectStatment);
        while (rs.next()) {
            ExceptionLoadingErrorsDTOInter record = DTOFactory.createExceptionLoadingErrorsDTO();
            record.setFunctionName(rs.getString("FUNCTION_NAME"));
            record.setErrorCode(rs.getInt("ERROR_CODE"));
            record.setErrorName(rs.getString("ERROR_NAME"));
            record.setErrorDate(rs.getTimestamp("ERROR_DATE"));
            Records.add(record);
        }
        super.postSelect(rs);
        return Records;
    }
}
