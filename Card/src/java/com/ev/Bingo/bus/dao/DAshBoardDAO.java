/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DAshBoardDTOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class DAshBoardDAO extends BaseDAO implements DAshBoardDAOInter {

    protected DAshBoardDAO() {
        super();
    }

    @Override
    public Object findRecordsList(Object... obj) throws Throwable {
        String Match = (String) obj[0];
        super.preSelect();
        String selectStat = "select Network, Disputes, Matched from dashboard WHERE match = $MatchType";
        selectStat = selectStat.replace("$MatchType", Match);
        ResultSet rs = executeQuery(selectStat);
        List<DAshBoardDTOInter> Records = new ArrayList<DAshBoardDTOInter>();
        while (rs.next()) {
            DAshBoardDTOInter SelectedRecord = DTOFactory.createDAshBoardDTO();
            SelectedRecord.setDisputes(rs.getInt("Disputes"));
            SelectedRecord.setMatched(rs.getInt("Matched"));
            SelectedRecord.setNetwork(rs.getString("Network"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

}
