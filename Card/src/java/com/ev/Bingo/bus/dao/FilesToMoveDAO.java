/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.ErrorsMonitorDTOInter;
import com.ev.Bingo.bus.dto.FilesToMoveDTOInter;
import com.ev.Bingo.bus.dto.LoadingMonitorDTOInter;
import com.ev.Bingo.bus.dto.MatchingMonitorDTOInter;
import com.ev.Bingo.bus.dto.ValidationMonitorDTOInter;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class FilesToMoveDAO extends BaseDAO implements FilesToMoveDAOInter {

    protected FilesToMoveDAO() {
        super();
        super.setTableName("files_to_move");
    }

    public Object deletefile(Object... obj) throws Throwable {
        super.preUpdate();
        // databse insert statement usinf conn pool getConn().createStatement();
        List<FilesToMoveDTOInter> machineType = (List<FilesToMoveDTOInter>) obj[0];
        for (FilesToMoveDTOInter rec : machineType) {
            String deleteStat = "delete from " + super.getTableName()
                    + "where id = " + rec.getId();
            super.executeUpdate(deleteStat);
            super.postUpdate(null, true);
        }
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc="Loading count">

    private LoadingMonitorDTOInter findPending() throws Throwable {
        LoadingMonitorDTOInter Result = DTOFactory.createLoadingMonitorDTO();
        super.preSelect();
        String selectStat = "select count(*)count,type from files_to_move where  loaded = 2 group by type order by type";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQueryJobMon(selectStat);
        Result.setPendingHost(0);
        Result.setPendingJournals(0);
        Result.setPendingSwitch(0);
        while (rs.next()) {
            if (rs.getInt("type") == 3) {
                Result.setPendingHost(rs.getInt("count"));
            }
            if (rs.getInt("type") == 1) {
                Result.setPendingJournals(rs.getInt("count"));
            }
            if (rs.getInt("type") == 2) {
                Result.setPendingSwitch(rs.getInt("count"));
            }
        }
        super.postSelect(rs);
        return Result;
    }

    private ValidationMonitorDTOInter findPendingValidation() throws Throwable {
        ValidationMonitorDTOInter Result = DTOFactory.createValidationMonitorDTO();
        super.preSelect();
        String selectStat = "select(select count(*)count from network_not_validated)journal,(select count(*)count from switch_not_validated)switch,(select count(*)count from host_not_validated)host from dual";
        ResultSet rs = executeQueryJobMon(selectStat);
        while (rs.next()) {
            Result.setPendingHost(rs.getInt("host"));
            Result.setPendingJournals(rs.getInt("journal"));
            Result.setPendingSwitch(rs.getInt("switch"));
        }
        super.postSelect(rs);
        return Result;
    }

    private MatchingMonitorDTOInter findPendingMatching() throws Throwable {
        MatchingMonitorDTOInter Result = DTOFactory.createMatchingMonitorDTO();
        super.preSelect();
        String selectStat = "select(select count(*)count from network_validated)journal,(select count(*)count from switch_validated)switch,(select count(*)count from host_validated)host from dual";
        ResultSet rs = executeQueryJobMon(selectStat);
        while (rs.next()) {
            Result.setPendingHost(rs.getInt("host"));
            Result.setPendingJournals(rs.getInt("journal"));
            Result.setPendingSwitch(rs.getInt("switch"));
        }
        super.postSelect(rs);
        return Result;
    }

    private ErrorsMonitorDTOInter findError() throws Throwable {
        ErrorsMonitorDTOInter Result = DTOFactory.createErrorsMonitorDTO();
        super.preSelect();
        String selectStat = "select count(*)count,record_type from MATCHING_ERRORS_DATA group by record_type order by record_type";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQueryJobMon(selectStat);
        Result.setPendingHost(0);
        Result.setPendingJournals(0);
        Result.setPendingSwitch(0);
        while (rs.next()) {
            if (rs.getInt("record_type") == 3) {
                Result.setPendingHost(rs.getInt("count"));
            }
            if (rs.getInt("record_type") == 1) {
                Result.setPendingJournals(rs.getInt("count"));
            }
            if (rs.getInt("record_type") == 2) {
                Result.setPendingSwitch(rs.getInt("count"));
            }
        }
        super.postSelect(rs);
        return Result;
    }

    // <editor-fold defaultstate="collapsed" desc="Validation count">
    // <editor-fold defaultstate="collapsed" desc="Matching count">
    // <editor-fold defaultstate="collapsed" desc="Error count">
    public Object getLoadingMonitor() throws Throwable {
        LoadingMonitorDTOInter lmDTO = DTOFactory.createLoadingMonitorDTO();
        lmDTO = findPending();
        return lmDTO;

    }

    public Object getValidationMonitor() throws Throwable {
        ValidationMonitorDTOInter lmDTO = DTOFactory.createValidationMonitorDTO();
        lmDTO = findPendingValidation();
        return lmDTO;

    }

    public Object getMatchingMonitor() throws Throwable {
        MatchingMonitorDTOInter lmDTO = DTOFactory.createMatchingMonitorDTO();
        lmDTO = findPendingMatching();
        return lmDTO;

    }

    public Object getErrorsMonitor() throws Throwable {
        ErrorsMonitorDTOInter lmDTO = DTOFactory.createErrorsMonitorDTO();
        lmDTO = findError();
        return lmDTO;

    }

    public int findActiveFiles() throws Throwable {
        super.preSelect();
        String selectStat = "select count(*)count from $table where active = 1 and loaded = 2";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQueryJobMon(selectStat);
        int count = 0;
        while (rs.next()) {
            count = rs.getInt("count");
        }
        super.postSelect(rs);
        return count;
    }

    public Object findAllFiles() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select f.file_name,decode(f.type,1,'Journal','2','Switch','3','Host','Other') file_type,ID from files_to_move f";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);

        List<FilesToMoveDTOInter> uDTOL = new ArrayList<FilesToMoveDTOInter>();
        while (rs.next()) {
            FilesToMoveDTOInter uDTO = DTOFactory.createFilesToMoveDTO();
            uDTO.setFileName(rs.getString("file_name"));
            uDTO.setTypename(rs.getString("file_type"));
            uDTO.setId(rs.getInt("ID"));
            uDTOL.add(uDTO);

        }
        super.postSelect(rs);
        return uDTOL;
    }

    public int findFilesToBuckup() throws Throwable {
        super.preSelect();
        String selectStat = "select count(*)count from $table where active = 1 and loaded = 1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQueryJobMon(selectStat);
        int count = 0;
        while (rs.next()) {
            count = rs.getInt("count");
        }
        super.postSelect(rs);
        return count;
    }

    public Object rematch() throws Throwable {
        preCollable();
        CallableStatement stat = super.executeCallableStatmentJobMon("{call matching.rematch()}");
        stat.execute();
        postCollable(stat);
        
        return null;
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table where active = 1 and loaded = 2 order by 1";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQueryJobMon(selectStat);
        List<FilesToMoveDTOInter> uDTOL = new ArrayList<FilesToMoveDTOInter>();
        while (rs.next()) {
            FilesToMoveDTOInter uDTO = DTOFactory.createFilesToMoveDTO();
            uDTO.setActive(rs.getInt("active"));
            uDTO.setFileName(rs.getString("file_name"));
            uDTO.setFileSize(rs.getInt("file_size"));
            uDTO.setType(rs.getInt("type"));

            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

}
