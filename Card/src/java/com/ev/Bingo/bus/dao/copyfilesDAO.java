package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.copyfilesDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class copyfilesDAO extends BaseDAO implements copyfilesDAOInter {

    protected copyfilesDAO() {
        super();
        super.setTableName("COPY_FILES");
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        copyfilesDTOInter RecordToInsert = (copyfilesDTOInter) obj[0];
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
//RecordToInsert.setid(super.generateSequence(super.getTableName()));
            String insertStat = "insert into $table"
                    + " (SOURCE_FOLDER,FOLDER1,FOLDER2) "
                    + " values "
                    + " ('$sourcefolder','$folder1','$folder2')";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$sourcefolder", "" + RecordToInsert.getsourcefolder());
            insertStat = insertStat.replace("$folder1", "" + RecordToInsert.getfolder1());
            insertStat = insertStat.replace("$folder2", "" + RecordToInsert.getfolder2());
            msg = super.executeUpdate(insertStat);
            msg = msg + " Record Has Been Inserted";
            super.postUpdate("Add New Record To COPY_FILES", false);
        }
        return msg;
    }

    public String ValidateNullList(Object... obj) {
        List<copyfilesDTOInter> AllRecords = (List<copyfilesDTOInter>) obj[0];
        String Validate = "Validate";
        for (copyfilesDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getsourcefolder() == null || RecordToInsert.getsourcefolder().equals("")) {
                Validate = Validate + "   sourcefolder  ";
            }
            if (RecordToInsert.getfolder1() == null || RecordToInsert.getfolder1().equals("")) {
                Validate = Validate + "   folder1  ";
            }
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required fields";
        }
        return Validate;

    }

    public String ValidateNull(Object... obj) {
        copyfilesDTOInter RecordToInsert = (copyfilesDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getsourcefolder() == null || RecordToInsert.getsourcefolder().equals("")) {
            Validate = Validate + "   sourcefolder  ";
        }
        if (RecordToInsert.getfolder1() == null || RecordToInsert.getfolder1().equals("")) {
            Validate = Validate + "   folder1  ";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required fields";
        }
        return Validate;
    }

    public Object updaterecord(Object... obj) throws Throwable {
        super.preUpdate();
        copyfilesDTOInter RecordToUpdate = (copyfilesDTOInter) obj[0];
        String msg = ValidateNull(RecordToUpdate);
        if (msg.equals("Validate")) {
            String UpdateStat = "update $table set "
                    + "SOURCE_FOLDER= '$sourcefolder',FOLDER1= '$folder1',FOLDER2= '$folder2'"
                    + "  where  ";
            UpdateStat = UpdateStat.replace("$table", "" + super.getTableName());
            UpdateStat = UpdateStat.replace("$sourcefolder", "" + RecordToUpdate.getsourcefolder());
            UpdateStat = UpdateStat.replace("$folder1", "" + RecordToUpdate.getfolder1());
            UpdateStat = UpdateStat.replace("$folder2", "" + RecordToUpdate.getfolder2());
            msg = super.executeUpdate(UpdateStat);
            msg = msg + " Row Has Been updated";
            super.postUpdate("Update Record In Table COPY_FILES", false);
        }
        ;
        return msg;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        copyfilesDTOInter RecordToDelete = (copyfilesDTOInter) obj[0];
        String msg = ValidateNull(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  SOURCE_FOLDER= '$sourcefolder' and FOLDER1= '$folder1'";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$sourcefolder", "" + RecordToDelete.getsourcefolder());
            deleteStat = deleteStat.replace("$folder1", "" + RecordToDelete.getfolder1());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " Row Has Been deleted";
            super.postUpdate("Delete Record In Table COPY_FILES", false);
        }
        return msg;
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete from $table ";
        deleteStat = deleteStat.replace("$table", "" + super.getTableName());
        super.executeUpdate(deleteStat);
        super.postUpdate("Delete All Record In Table COPY_FILES", false);
        return null;
    }

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        copyfilesDTOInter RecordToSelect = (copyfilesDTOInter) obj[0];
        String selectStat = "Select SOURCE_FOLDER,FOLDER1,FOLDER2 From $table"
                + "  where  ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        copyfilesDTOInter SelectedRecord = DTOFactory.createcopyfilesDTO();
        while (rs.next()) {
            SelectedRecord.setsourcefolder(rs.getString("SOURCE_FOLDER"));
            SelectedRecord.setfolder1(rs.getString("FOLDER1"));
            SelectedRecord.setfolder2(rs.getString("FOLDER2"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        copyfilesDTOInter RecordToSelect = (copyfilesDTOInter) obj[0];
        String selectStat = "Select SOURCE_FOLDER,FOLDER1,FOLDER2 From $table"
                + "  where  ";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<copyfilesDTOInter> Records = new ArrayList<copyfilesDTOInter>();
        while (rs.next()) {
            copyfilesDTOInter SelectedRecord = DTOFactory.createcopyfilesDTO();
            SelectedRecord.setsourcefolder(rs.getString("SOURCE_FOLDER"));
            SelectedRecord.setfolder1(rs.getString("FOLDER1"));
            SelectedRecord.setfolder2(rs.getString("FOLDER2"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select SOURCE_FOLDER,FOLDER1,FOLDER2 From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<copyfilesDTOInter> Records = new ArrayList<copyfilesDTOInter>();
        while (rs.next()) {
            copyfilesDTOInter SelectedRecord = DTOFactory.createcopyfilesDTO();
            SelectedRecord.setsourcefolder(rs.getString("SOURCE_FOLDER"));
            SelectedRecord.setfolder1(rs.getString("FOLDER1"));
            SelectedRecord.setfolder2(rs.getString("FOLDER2"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<copyfilesDTOInter> entities = (List<copyfilesDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Files Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "update COPY_FILES set FOLDER1 = ?,FOLDER2 =? where SOURCE_FOLDER = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        copyfilesDTOInter RecordtoInsert = entities.get(i);
                        statement.setString(1, RecordtoInsert.getfolder1());
                        statement.setString(2, RecordtoInsert.getfolder2());
                        statement.setString(3, RecordtoInsert.getsourcefolder());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " Record has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                            CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
