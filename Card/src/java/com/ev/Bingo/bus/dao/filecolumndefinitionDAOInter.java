package com.ev.Bingo.bus.dao;

import java.util.Date;
import java.util.List;
import java.sql.SQLException;

public interface filecolumndefinitionDAOInter {

    Object findRecordsAll(Object... obj) throws Throwable;

    Object findRecordByColName(Object... obj) throws Throwable;
    
    Object findRecordByColName4(Object... obj) throws Throwable;

    Object findRecordsAllview(Object... obj) throws Throwable;

    Object findRecordByID(Object... obj) throws Throwable;//used in system rules

    String save(Object... obj) throws SQLException, Throwable;
}
