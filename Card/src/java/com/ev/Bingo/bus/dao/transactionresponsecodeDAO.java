package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.transactionresponsecodeDTOInter;
import com.ev.Bingo.bus.dto.transactionresponsemasterDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class transactionresponsecodeDAO extends BaseDAO implements transactionresponsecodeDAOInter {

    protected transactionresponsecodeDAO() {
        super();
        super.setTableName("TRANSACTION_RESPONSE_CODE");
    }

    //used in system rules

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,NAME,DESCRIPTION,AMOUNT_TYPE,DEFAULT_FLAG,MASTER_CODE From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<transactionresponsecodeDTOInter> Records = new ArrayList<transactionresponsecodeDTOInter>();
        while (rs.next()) {
            transactionresponsecodeDTOInter SelectedRecord = DTOFactory.createtransactionresponsecodeDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setdescription(rs.getString("DESCRIPTION"));
            SelectedRecord.setamounttype(rs.getInt("AMOUNT_TYPE"));
            SelectedRecord.setdefaultflag(rs.getInt("DEFAULT_FLAG"));
            SelectedRecord.setmastercode(rs.getInt("MASTER_CODE"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    //used in system rules
    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        transactionresponsecodeDTOInter RecordToSelect = (transactionresponsecodeDTOInter) obj[0];
        String selectStat = "Select ID,NAME,DESCRIPTION,AMOUNT_TYPE,DEFAULT_FLAG,MASTER_CODE From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        transactionresponsecodeDTOInter SelectedRecord = DTOFactory.createtransactionresponsecodeDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setdescription(rs.getString("DESCRIPTION"));
            SelectedRecord.setamounttype(rs.getInt("AMOUNT_TYPE"));
            SelectedRecord.setdefaultflag(rs.getInt("DEFAULT_FLAG"));
            SelectedRecord.setmastercode(rs.getInt("MASTER_CODE"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactionresponsecodeDTOInter RecordToInsert = (transactionresponsecodeDTOInter) obj[0];
        if (RecordToInsert.getdefaultflagB() == Boolean.TRUE) {
            RecordToInsert.setdefaultflag(1);
        } else {
            RecordToInsert.setdefaultflag(2);
        }
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
            String insertStat = "insert into $table"
                    + " (ID,NAME,DESCRIPTION,AMOUNT_TYPE,DEFAULT_FLAG,MASTER_CODE) "
                    + " values "
                    + " ($id,'$name','$description',$amounttype,$defaultflag,$mastercode)";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
            insertStat = insertStat.replace("$name", "" + RecordToInsert.getname());
            insertStat = insertStat.replace("$description", "" + RecordToInsert.getdescription());
            insertStat = insertStat.replace("$amounttype", "" + RecordToInsert.getamounttype());
            insertStat = insertStat.replace("$defaultflag", "" + RecordToInsert.getdefaultflag());
            insertStat = insertStat.replace("$mastercode", "" + RecordToInsert.getmastercode());
            try {
                msg = super.executeUpdate(insertStat);
                msg = msg + " Record Has Been Inserted";
                super.postUpdate("Adding New TRANSACTION RESPONSE CODE Record With Name " + RecordToInsert.getname(), false);
            } catch (Exception ex) {
                msg = "Enter A Unique Response Code";
            }
        }
        return msg;
    }
//ValidateNullList

    public String ValidateNullList(Object... obj) {
        List<transactionresponsecodeDTOInter> AllRecords = (List<transactionresponsecodeDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (transactionresponsecodeDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getid() == 0) {
                Validate = Validate + " ," + String.valueOf(i);
            }
            if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {
                Validate = Validate + " ," + String.valueOf(i);
            }
            if (RecordToInsert.getdefaultflag() == 0) {
                Validate = Validate + "," + String.valueOf(i);
            }
            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = "Record With Index " + Validate + " Contain Null Code Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    public String ValidateNull(Object... obj) {
        transactionresponsecodeDTOInter RecordToInsert = (transactionresponsecodeDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getid() == 0) {
            Validate = Validate + "   Id  ";
        }
        if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {
            Validate = Validate + "   Name  ";
        }
        if (RecordToInsert.getdefaultflag() == 0) {
            Validate = Validate + "   defaultflag  ";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required fields";
        }
        return Validate;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactionresponsecodeDTOInter RecordToDelete = (transactionresponsecodeDTOInter) obj[0];
        String msg = "Validate";
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  ID= $id";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " Record Has Been deleted";
            super.postUpdate("Deleting A TRANSACTION RESPONSE CODE With Name " + RecordToDelete.getname(), false);
        }
        return msg;
    }

    public Object deleterecordByMaster(Object... obj) throws Throwable {
        super.preUpdate();
        transactionresponsemasterDTOInter RecordToDelete = (transactionresponsemasterDTOInter) obj[0];
        String msg = "Validate";
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  master_code = $id";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " Detail Record Has Been deleted";
            super.postUpdate("Deleting A TRANSACTION RESPONSE CODE With Name " + RecordToDelete.getname(), false);
        }
        return msg;
    }

    public Object findRecordsList(Object... obj) throws Throwable {
        super.preSelect();
        transactionresponsemasterDTOInter RecordToSelect = (transactionresponsemasterDTOInter) obj[0];
        String selectStat = "Select ID,NAME,DESCRIPTION,AMOUNT_TYPE,DEFAULT_FLAG,MASTER_CODE From $table"
                + "  where  MASTER_CODE= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        List<transactionresponsecodeDTOInter> Records = new ArrayList<transactionresponsecodeDTOInter>();
        while (rs.next()) {
            transactionresponsecodeDTOInter SelectedRecord = DTOFactory.createtransactionresponsecodeDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            SelectedRecord.setdescription(rs.getString("DESCRIPTION"));
            SelectedRecord.setamounttype(rs.getInt("AMOUNT_TYPE"));
            SelectedRecord.setdefaultflag(rs.getInt("DEFAULT_FLAG"));
            if (SelectedRecord.getdefaultflag() == 1) {
                SelectedRecord.setdefaultflagB(Boolean.TRUE);
            } else {
                SelectedRecord.setdefaultflagB(Boolean.FALSE);
            }
            SelectedRecord.setmastercode(rs.getInt("MASTER_CODE"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<transactionresponsecodeDTOInter> entities = (List<transactionresponsecodeDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Detail Transaction Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "update TRANSACTION_RESPONSE_CODE set NAME = ?,DESCRIPTION =?,AMOUNT_TYPE = ?,DEFAULT_FLAG =? where ID = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        transactionresponsecodeDTOInter RecordtoInsert = entities.get(i);
                        statement.setString(1, RecordtoInsert.getname());
                        statement.setString(2, RecordtoInsert.getdescription());
                        statement.setInt(3, RecordtoInsert.getamounttype());
                        if (RecordtoInsert.getdefaultflagB() == Boolean.TRUE) {
                            statement.setInt(4, 1);
                        } else {
                            statement.setInt(4, 2);
                        }
                        statement.setInt(5, RecordtoInsert.getid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }
                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                            CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
