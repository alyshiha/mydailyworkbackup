package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

public interface blacklistedcardDAOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object findRecordsAll(Object... obj) throws Throwable;

    String save(Object... obj) throws SQLException, Throwable;
}
