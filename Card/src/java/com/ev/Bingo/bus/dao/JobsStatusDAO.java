/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.JobsStatusDTOInter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class JobsStatusDAO extends BaseDAO implements JobsStatusDAOInter {

    protected JobsStatusDAO() {
        super();
        super.setTableName("jobs_status");
    }

    public Object findAll() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select * from $table order by stack_id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<JobsStatusDTOInter> uDTOL = new ArrayList<JobsStatusDTOInter>();
        while (rs.next()) {
            JobsStatusDTOInter uDTO = DTOFactory.createJobsStatusDTO();
            uDTO.setHostValidation(rs.getInt("host_validation"));
            uDTO.setHostValidationJobId(rs.getInt("host_validation_job_id"));
            uDTO.setJournalValidation(rs.getInt("journal_validation"));
            uDTO.setJournalValidationJobId(rs.getInt("journal_validation_job_id"));
            uDTO.setLoading(rs.getInt("loading"));
            uDTO.setLoadingJobId(rs.getInt("loading_job_id"));
            uDTO.setMatching(rs.getInt("matching"));
            uDTO.setMatchingJobId(rs.getInt("matching_job_id"));
            uDTO.setStackId(rs.getInt("stack_id"));
            uDTO.setSwitchValidation(rs.getInt("switch_validation"));
            uDTO.setSwitchValidationJobId(rs.getInt("switch_validation_job_id"));
            uDTOL.add(uDTO);
        }
        super.postSelect(rs);
        return uDTOL;
    }

    public Object findRunning(int jobId) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select this_date from user_jobs where job = $jobId";
        selectStat = selectStat.replace("$jobId", "" + jobId);
        ResultSet rs = executeQuery(selectStat);
        Date thisDate = null;
        if (rs.next()) {
            while (rs.next()) {
                thisDate = rs.getDate("this_date");
            }
            super.postSelect(rs);
            return thisDate;
        } else {
            return false;
        }
    }

    public Object findBroken(int jobId) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select broken from user_jobs where job = $jobId";
        selectStat = selectStat.replace("$jobId", "" + jobId);
        ResultSet rs = executeQuery(selectStat);
        String broken = "";
        while (rs.next()) {
            broken = rs.getString("broken");
        }
        super.postSelect(rs);
        return broken;
    }

    public Object disableJob(boolean status, int process) throws Throwable {
        super.preUpdate();
        if (process == 1) {
            if (!status) {
                String updateStat = "update jobs_status set loading = 1";
                super.executeUpdate(updateStat);
            } else {
                String updateStat = "update jobs_status set loading = 2";
                super.executeUpdate(updateStat);
            }
        } else if (process == 2) {
            if (!status) {
                String updateStat = "update jobs_status set journal_validation = 1";
                super.executeUpdate(updateStat);
            } else {
                String updateStat = "update jobs_status set journal_validation = 2";
                super.executeUpdate(updateStat);
            }
        } else if (process == 3) {
            if (!status) {
                String updateStat = "update jobs_status set switch_validation = 1";
                super.executeUpdate(updateStat);
            } else {
                String updateStat = "update jobs_status set switch_validation = 2";
                super.executeUpdate(updateStat);
            }
        } else if (process == 4) {
            if (!status) {
                String updateStat = "update jobs_status set host_validation = 1";
                super.executeUpdate(updateStat);
            } else {
                String updateStat = "update jobs_status set host_validation = 2";
                super.executeUpdate(updateStat);
            }
        } else if (process == 5) {
            if (!status) {
                String updateStat = "update jobs_status set matching = 1";
                super.executeUpdate(updateStat);
            } else {
                String updateStat = "update jobs_status set matching = 2";
                super.executeUpdate(updateStat);
            }
        }
        super.postUpdate("Job Stoped");
        return null;
    }
}
