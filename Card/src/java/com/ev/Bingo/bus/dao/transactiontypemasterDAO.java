package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import java.sql.ResultSet;
 
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.transactiontypemasterDTOInter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class transactiontypemasterDAO extends BaseDAO implements transactiontypemasterDAOInter {

    protected transactiontypemasterDAO() {
        super();
        super.setTableName("TRANSACTION_TYPE_MASTER");
    }

    //used in system rules

    public Object findRecord(Object... obj) throws Throwable {
        super.preSelect();
        transactiontypemasterDTOInter RecordToSelect = (transactiontypemasterDTOInter) obj[0];
        String selectStat = "Select ID,NAME From $table"
                + "  where  ID= $id";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        selectStat = selectStat.replace("$id", "" + RecordToSelect.getid());
        ResultSet rs = executeQuery(selectStat);
        transactiontypemasterDTOInter SelectedRecord = DTOFactory.createtransactiontypemasterDTO();
        while (rs.next()) {
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
        }
        super.postSelect(rs);
        return SelectedRecord;
    }

    public Object insertrecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactiontypemasterDTOInter RecordToInsert = (transactiontypemasterDTOInter) obj[0];
        RecordToInsert.setid(super.generateSequence(super.getTableName()));
        String msg = ValidateNull(RecordToInsert);
        if (msg.equals("Validate")) {
            String insertStat = "insert into $table"
                    + " (ID,NAME) "
                    + " values "
                    + " ($id,'$name')";
            insertStat = insertStat.replace("$table", "" + super.getTableName());
            insertStat = insertStat.replace("$id", "" + RecordToInsert.getid());
            insertStat = insertStat.replace("$name", "" + RecordToInsert.getname());
            try {
                msg = super.executeUpdate(insertStat);
                msg = msg + " Record Has Been Inserted";
                super.postUpdate("Add New MASTER TRANSACTION TYPE With Name " + RecordToInsert.getname(), false);
            } catch (Exception ex) {
                msg = "Please Enter A Unique Trans Type";
            }
        }
        return msg;
    }

    public String ValidateNullList(Object... obj) {
        List<transactiontypemasterDTOInter> AllRecords = (List<transactiontypemasterDTOInter>) obj[0];
        String Validate = "Validate";
        int i = 1;
        for (transactiontypemasterDTOInter RecordToInsert : AllRecords) {
            if (RecordToInsert.getid() == 0) {
                Validate = Validate + "," + String.valueOf(i);
            }
            if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {
                Validate = Validate + "," + String.valueOf(i);
            }

            i = i + 1;
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = "Record With Index " + Validate + " Contain Null Name Value";
            Validate = Validate.replace("x ,", "x ");
        }
        return Validate;
    }

    public String ValidateNullDelete(Object... obj) {
        transactiontypemasterDTOInter RecordToInsert = (transactiontypemasterDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getid() == 0) {
            Validate = Validate + "   id  ";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required fields";
        }
        return Validate;
    }

    public String ValidateNull(Object... obj) {
        transactiontypemasterDTOInter RecordToInsert = (transactiontypemasterDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getid() == 0) {
            Validate = Validate + "ID ";
        }
        if (RecordToInsert.getname() == null || RecordToInsert.getname().equals("")) {
            Validate = Validate + " Name ";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required fields";
        }
        return Validate;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        super.preUpdate();
        transactiontypemasterDTOInter RecordToDelete = (transactiontypemasterDTOInter) obj[0];
        String msg = ValidateNullDelete(RecordToDelete);
        if (msg.equals("Validate")) {
            String deleteStat = "delete from $table "
                    + "  where  ID= $id";
            deleteStat = deleteStat.replace("$table", "" + super.getTableName());
            deleteStat = deleteStat.replace("$id", "" + RecordToDelete.getid());
            deleteStat = deleteStat.replace("$name", "" + RecordToDelete.getname());
            msg = super.executeUpdate(deleteStat);
            msg = msg + " Master Record Has Been deleted";
            super.postUpdate("Deleting A MASTER TRANSACTION TYPE With Name " + RecordToDelete.getname(), false);
        }
        return msg;
    }

    public Object findRecordsAll(Object... obj) throws Throwable {
        super.preSelect();
        String selectStat = "Select ID,NAME From $table";
        selectStat = selectStat.replace("$table", "" + super.getTableName());
        ResultSet rs = executeQuery(selectStat);
        List<transactiontypemasterDTOInter> Records = new ArrayList<transactiontypemasterDTOInter>();
        while (rs.next()) {
            transactiontypemasterDTOInter SelectedRecord = DTOFactory.createtransactiontypemasterDTO();
            SelectedRecord.setid(rs.getInt("ID"));
            SelectedRecord.setname(rs.getString("NAME"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    public String save(Object... obj) throws SQLException, Throwable {
        List<transactiontypemasterDTOInter> entities = (List<transactiontypemasterDTOInter>) obj[0];
        String mess = "", mess2 = "";
        if (entities.size() == 0) {
            mess = "No Transaction Type Found";
        } else {
            String validate = ValidateNullList(entities);
            if (validate.equals("Validate")) {
                String insertStat = "update TRANSACTION_TYPE_MASTER set NAME = ? where ID = ?";
                Connection connection = null;
                PreparedStatement statement = null;
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                try {
                    connection = CoonectionHandler.getInstance().getConnection();
                    statement = connection.prepareStatement(insertStat);
                    for (int i = 0; i < entities.size(); i++) {
                        transactiontypemasterDTOInter RecordtoInsert = entities.get(i);
                        statement.setString(1, RecordtoInsert.getname());
                        statement.setInt(2, RecordtoInsert.getid());
                        statement.addBatch();
                        if ((i + 1) % 1000 == 0) {
                            statement.executeBatch();
                        }
                    }
                    int[] numUpdates = statement.executeBatch();
                    Integer valid = 0, notvalid = 0;

                    for (int i = 0; i < numUpdates.length; i++) {
                        if (numUpdates[i] == -2) {
                            valid++;
                            mess = valid + " rows has been updated";
                        } else {
                            notvalid++;
                            mess2 = notvalid + " can`t be updated";
                        }
                    }
                    if (!mess2.equals("")) {
                        mess = mess + "," + mess2;
                    }

                } finally {
                    if (statement != null) {
                        try {
                            connection.commit();
                            statement.close();
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                    if (connection != null) {
                        try {
                            CoonectionHandler.getInstance().returnConnection(connection);
                        } catch (SQLException logOrIgnore) {
                        }
                    }
                }
                return mess;
            } else {
                return validate;
            }
        }
        return mess;
    }
}
