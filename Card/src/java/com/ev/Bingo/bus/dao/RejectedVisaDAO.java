package com.ev.Bingo.bus.dao;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.RejectedVisaDTOInter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RejectedVisaDAO extends BaseDAO implements RejectedVisaDAOInter {

    @Override
    public Boolean findDuplicatesaveRecord(String REPORTID, Integer REJECTID) throws Throwable {
        super.preSelect();
        String selectStat = "select REJECTID, REPORT_ID  from REJECT_VISA_REPORTID where REJECTID <> $id and REPORT_ID  = '$name'";
        selectStat = selectStat.replace("$name", REPORTID);
        selectStat = selectStat.replace("$id", "" + REJECTID);
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            super.postSelect(rs);
            return Boolean.TRUE;
        }
        super.postSelect(rs);
        return Boolean.FALSE;
    }

    @Override
    public Boolean findDuplicateRecord(String REPORTID) throws Throwable {
        super.preSelect();
        String selectStat = "select REJECTID, REPORT_ID  from REJECT_VISA_REPORTID where REPORT_ID  = '$name'";
        selectStat = selectStat.replace("$name", REPORTID);
        ResultSet rs = executeQuery(selectStat);
        while (rs.next()) {
            super.postSelect(rs);
            return Boolean.TRUE;
        }
        super.postSelect(rs);
        return Boolean.FALSE;
    }

    @Override
    public List<RejectedVisaDTOInter> findRecord(String REPORTID) throws Throwable {
        super.preSelect();
        String selectStat = "select REJECTID, REPORT_ID  from REJECT_VISA_REPORTID where REPORT_ID  like '%$name%' order by REPORT_ID ";
        selectStat = selectStat.replace("$name", REPORTID);
        ResultSet rs = executeQuery(selectStat);
        List<RejectedVisaDTOInter> records = new ArrayList<RejectedVisaDTOInter>();
        while (rs.next()) {
            RejectedVisaDTOInter record = DTOFactory.createRejectedVisaDTO();
            record.setRejectid(rs.getInt("REJECTID"));
            record.setReportid(rs.getString("REPORT_ID"));
            records.add(record);
        }
        super.postSelect(rs);

        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select record in REJECT_VISA_REPORTID by REPORT_ID like: " + REPORTID);
        return records;
    }

    @Override
    public List<RejectedVisaDTOInter> findAll() throws Throwable {
        super.preSelect();
        String selectStat = "select REJECTID, REPORT_ID  from REJECT_VISA_REPORTID order by REPORT_ID ";
        ResultSet rs = executeQuery(selectStat);
        List<RejectedVisaDTOInter> records = new ArrayList<RejectedVisaDTOInter>();
        while (rs.next()) {
            RejectedVisaDTOInter record = DTOFactory.createRejectedVisaDTO();
            record.setRejectid(rs.getInt("REJECTID"));
            record.setReportid(rs.getString("REPORT_ID"));
            records.add(record);
        }
        super.postSelect(rs);

        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("select all records in REJECT_VISA_REPORTID");
        return records;
    }

    @Override
    public Object insert(Object... obj) throws Throwable {
        super.preUpdate();
        RejectedVisaDTOInter record = (RejectedVisaDTOInter) obj[0];
        record.setRejectid(super.generateSequence("REJECT_VISA_REPORTID"));
        String insertStat = "insert into REJECT_VISA_REPORTID   (REJECTID, REPORT_ID)\n"
                + "values \n"
                + "  ($REJECTID, '$REPORTID')";
        insertStat = insertStat.replace("$REJECTID", "" + record.getRejectid());
        insertStat = insertStat.replace("$REPORTID", "" + record.getReportid());
        super.executeUpdate(insertStat);

        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("insert record in REJECT_VISA_REPORTID with REPORT_ID: " + record.getReportid());
        return null;
    }

    @Override
    public Object update(Object... obj) throws Throwable {
        super.preUpdate();
        RejectedVisaDTOInter record = (RejectedVisaDTOInter) obj[0];
        String updateStat = "update REJECT_VISA_REPORTID\n"
                + "   set REPORT_ID  = '$REPORTID'\n"
                + " where REJECTID = $REJECTID";
        updateStat = updateStat.replace("$REJECTID", "" + record.getRejectid());
        updateStat = updateStat.replace("$REPORTID", "" + record.getReportid());
        super.executeUpdate(updateStat);

        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        logEngin.insert("update record in REJECT_VISA_REPORTID with REPORT_ID: " + record.getReportid());
        return null;
    }

    @Override
    public Object delete(Object... obj) {
        super.preUpdate();
        RejectedVisaDTOInter record = (RejectedVisaDTOInter) obj[0];
        String deleteStat = "delete REJECT_VISA_REPORTID\n"
                + " where REJECTID = $REJECTID";
        deleteStat = deleteStat.replace("$REJECTID", "" + record.getRejectid());
        try {
            super.executeUpdate(deleteStat);
        } catch (SQLException ex) {
            return "This REPORT ID is in use";
        } catch (Exception ex) {
            Logger.getLogger(AccountNameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        CashManagmentLogDAOInter logEngin = DAOFactory.createCashManagmentLogDAO();
        try {
            logEngin.insert("delete record in REJECT_VISA_REPORTID");
        } catch (Throwable ex) {
            Logger.getLogger(AccountNameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Record Has Been Succesfully Deleted";
    }

}
