package com.ev.Bingo.bus.dao;

import java.sql.SQLException;

public interface userprofileDAOInter {

    Object insertrecord(Object... obj) throws Throwable;//userd in user mangement

    Object deleterecord(Object... obj) throws Throwable;

    Object findRecord(Object... obj) throws Throwable;//userd in Menu Genaratioon

    String save(Object... obj) throws SQLException, Throwable;
}
