/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import com.ev.Bingo.bus.dto.NetworkTypeCodeDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface NetworkTypeCodeDAOInter {

    List<NetworkTypeCodeDTOInter> findRecord(int network, String code) throws Throwable ;
            
    Object delete(Object... obj) throws Throwable;

    List<NetworkTypeCodeDTOInter> findAll() throws Throwable;

    Object insert(Object... obj) throws Throwable;

    Object update(Object... obj) throws Throwable;
    
}
