/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.dao;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
 
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.ATMMACHINECASHDTOINTER;
import com.ev.Bingo.bus.dto.BankCodeDTOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.DiffCardAtmJournalDTOInter;
import com.ev.Bingo.bus.dto.DiffCardAtmTransJournalDTOInter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author AlyShiha
 */
public class DiffCardAtmDAO extends BaseDAO implements DiffCardAtmDAOInter {

    @Override
    public Object findMasterRecords(String ATMID, String INDICATION, String JournalType, Date From, Date TO) throws Throwable {
        super.preSelect();
        String selectStat = "select diff_id, card_amount,DIFF_AMOUNT, atm_amount, journal_type, "
                + "atm_id, journal_date, atm_from_date, atm_to_date, credit_account, "
                + "debit_account, indication, branch, DIFF_DATE "
                + "from diff_card_atm_journal where (atm_id = '$atm' Or '$atm' = 'All') "
                + "and (indication = '$indication' Or '$indication' = 'All') "
                + "and journal_date between to_date('$dateFrom','dd.MM.yyyy') and to_date('$dateTo','dd.MM.yyyy') "
                + "and  (journal_type = '$journal' Or '$journal' = 'All')"
                + "order by atm_id,journal_date";
        selectStat = selectStat.replace("$atm", ATMID);
        selectStat = selectStat.replace("$indication", INDICATION);
        selectStat = selectStat.replace("$journal", JournalType);
        selectStat = selectStat.replace("$dateFrom", DateFormatter.changeDateFormat(From));
        selectStat = selectStat.replace("$dateTo", DateFormatter.changeDateFormat(TO));
        ResultSet rs = executeQuery(selectStat);
        List<DiffCardAtmJournalDTOInter> Records = new ArrayList<DiffCardAtmJournalDTOInter>();
        while (rs.next()) {
            DiffCardAtmJournalDTOInter SelectedRecord = DTOFactory.createDiffCardAtmJournalDTO();
            SelectedRecord.setDiffid(rs.getInt("diff_id"));
            SelectedRecord.setDiffamount(rs.getInt("DIFF_AMOUNT"));
            SelectedRecord.setCardamount(rs.getInt("card_amount"));
            SelectedRecord.setAtmamount(rs.getInt("atm_amount"));
            SelectedRecord.setJournaltype(rs.getString("journal_type"));
            SelectedRecord.setAtmid(rs.getString("atm_id"));
            SelectedRecord.setJournaldate(rs.getTimestamp("journal_date"));
            SelectedRecord.setDiffdate(rs.getTimestamp("DIFF_DATE"));
            SelectedRecord.setAtmfromdate(rs.getTimestamp("atm_from_date"));
            SelectedRecord.setAtmtodate(rs.getTimestamp("atm_to_date"));
            SelectedRecord.setCreditaccount(rs.getString("credit_account"));
            SelectedRecord.setDebitaccount(rs.getString("debit_account"));
            SelectedRecord.setIndication(rs.getString("indication"));
            SelectedRecord.setBranch(rs.getString("branch"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public Object findDetailRecords(String DIFFID) throws Throwable {
        super.preSelect();
        String selectStat = "select diff_id, decrypt(card_no) cardno, network_id, transaction_sequence,"
                + " transaction_status, transaction_type, atm_id, transaction_date,"
                + " settlement_date, response_code, customer_account_number,"
                + " reference_no, process_code, amount, settlement_amount,TRANSACTION_SIDE,"
                + " currency, acq, issuer, diff_parent_id"
                + " from diff_card_atm_trans_journal"
                + " where diff_parent_id = $diffid"
                + " order by atm_id,transaction_date";
        selectStat = selectStat.replace("$diffid", DIFFID);
        ResultSet rs = executeQuery(selectStat);
        List<DiffCardAtmTransJournalDTOInter> Records = new ArrayList<DiffCardAtmTransJournalDTOInter>();
        while (rs.next()) {
            DiffCardAtmTransJournalDTOInter SelectedRecord = DTOFactory.createDiffCardAtmTransJournalDTO();
            SelectedRecord.setDiffid(rs.getInt("diff_id"));
            SelectedRecord.setCardno(rs.getString("cardno"));
            SelectedRecord.setTransside(rs.getString("TRANSACTION_SIDE"));
            SelectedRecord.setNetworkid(rs.getString("network_id"));
            SelectedRecord.setTransactionsequence(rs.getString("transaction_sequence"));
            SelectedRecord.setTransactionstatus(rs.getString("transaction_status"));
            SelectedRecord.setTransactiontype(rs.getString("transaction_type"));
            SelectedRecord.setAtmid(rs.getString("atm_id"));
            SelectedRecord.setTransactiondate(rs.getTimestamp("transaction_date"));
            SelectedRecord.setSettlementdate(rs.getTimestamp("settlement_date"));
            SelectedRecord.setResponsecode(rs.getString("reference_no"));
            SelectedRecord.setCustomeraccountnumber(rs.getString("customer_account_number"));
            SelectedRecord.setReferenceno(rs.getString("reference_no"));
            SelectedRecord.setProcesscode(rs.getString("process_code"));
            SelectedRecord.setAmount(rs.getInt("amount"));
            SelectedRecord.setSettlementamount(rs.getInt("settlement_amount"));
            SelectedRecord.setCurrency(rs.getString("currency"));
            SelectedRecord.setAcq(rs.getString("acq"));
            SelectedRecord.setIssuer(rs.getString("issuer"));
            SelectedRecord.setDiffparentid(rs.getInt("diff_parent_id"));
            Records.add(SelectedRecord);
        }
        super.postSelect(rs);
        return Records;
    }

    @Override
    public List<ATMMACHINECASHDTOINTER> findMachineAll() throws Throwable {
        super.preSelect();
        String selectStat = "select distinct atm_id ID from diff_card_atm_journal order by atm_id";
        ResultSet rs = executeQuery(selectStat);
        List<ATMMACHINECASHDTOINTER> records = new ArrayList<ATMMACHINECASHDTOINTER>();
        while (rs.next()) {
            ATMMACHINECASHDTOINTER record = DTOFactory.createATMMACHINECASHDTO();
            record.setAtmname(rs.getString("ID"));
            record.setAtmid(rs.getString("ID"));
            records.add(record);
        }
        super.postSelect(rs);
        return records;
    }

    @Override
    public Integer SaveRecordPrint(DiffCardAtmJournalDTOInter[] entities) throws SQLException, Throwable {
        Integer Index = super.generateSequence("DIFF_JOURNAL_PRINT");
        if (entities.length == 0) {
            Index = 0;
        } else {
            String insertStat = "insert into diff_card_atm_journal_print (printid, diff_id) values (?, ?)";
            Connection connection = null;
            PreparedStatement statement = null;
            try {
             connection =  CoonectionHandler.getInstance().getConnection();
                statement = connection.prepareStatement(insertStat);
                for (int i = 0; i < entities.length; i++) {
                    DiffCardAtmJournalDTOInter RecordtoInsert = entities[i];
                    statement.setInt(1, Index);
                    statement.setInt(2, RecordtoInsert.getDiffid());
                    statement.addBatch();
                    if ((i + 1) % 1000 == 0) {
                        statement.executeBatch();
                    }
                }
                statement.executeBatch();
            } finally {
                if (statement != null) {
                    try {
                        connection.commit();
                        statement.close();
                    } catch (SQLException logOrIgnore) {
                    }
                }
                if (connection != null) {
                    try {
                        CoonectionHandler.getInstance().returnConnection(connection);
                    } catch (SQLException logOrIgnore) {
                    }
                }
            }
            return Index;

        }
        return Index;
    }

    @Override
    public void DeleteRecordPrint(Integer ID) throws Throwable {
        super.preUpdate();
        String deleteStat = "delete diff_card_atm_journal_print where printid = $id";
        deleteStat = deleteStat.replace("$id", "" + ID);
        super.executeUpdate(deleteStat);
    }
    public void recalc() throws Throwable {
        preCollable();
        CallableStatement stat = super.executeCallableStatment("{call CARDS.CHECK_JOURNAL_DIFF()}");
        stat.execute();
        postCollable(stat);
        
    }
}
