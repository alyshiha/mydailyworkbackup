package com.ev.Bingo.bus.dao;

import java.util.Date;
import java.util.List;
import java.sql.SQLException;

public interface networkdefaultcolumnsDAOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object updaterecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object deleteallrecord(Object... obj) throws Throwable;

    Object findRecord(Object... obj) throws Throwable;
Object saveorder(Object... obj) throws SQLException, Throwable ;
    Object findRecordsList(Object... obj) throws Throwable;
    Object findcolsList(Object... obj) throws Throwable;

    Object findRecordsAll(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;
}
