/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.ATMMACHINECASHDTOINTER;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface ATMMACHINEBOInter {

    List<ATMMACHINECASHDTOINTER> findRecord(String name, String id) throws Throwable;

    String deleteRecord(ATMMACHINECASHDTOINTER record);

    List<ATMMACHINECASHDTOINTER> findAllDistinct() throws Throwable;

    List<ATMMACHINECASHDTOINTER> findAll() throws Throwable;

    String insertRecord(String atmid, String atmname);

    String updateRecord(ATMMACHINECASHDTOINTER record);

}
