package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.diffamountreportDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.diffamountreportDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.diffamountreportDTOInter;
import java.text.SimpleDateFormat;

public class diffamountreportBo extends BaseBO implements diffamountreportBOInter {

    protected diffamountreportBo() {
        super();
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<diffamountreportDTOInter> SearchRecords = (List<diffamountreportDTOInter>) obj[0];
        diffamountreportDAOInter engin = DAOFactory.creatediffamountreportDAO();
        List<diffamountreportDTOInter> AllRecords = (List<diffamountreportDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        diffamountreportDAOInter engin = DAOFactory.creatediffamountreportDAO();
        List<diffamountreportDTOInter> AllRecords = (List<diffamountreportDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

}
