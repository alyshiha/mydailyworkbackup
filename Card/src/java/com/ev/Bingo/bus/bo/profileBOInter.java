package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.menulabelsDTOInter;
import java.sql.SQLException;
import java.util.List;
import org.primefaces.model.TreeNode;

public interface profileBOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object insertmenuitem(Object... obj) throws Throwable;

    Object GetAllRecords(Object... obj) throws Throwable;//userd in user mangement

    TreeNode getMenuTree() throws Throwable;

    List<menulabelsDTOInter> getMlDTOL(int profileId) throws Throwable;
}
