package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.transactionresponsecodeDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.transactionresponsecodeDTOInter;
import com.ev.Bingo.bus.dto.transactionresponsemasterDTOInter;

public class transactionresponsecodeBO extends BaseBO implements transactionresponsecodeBOInter {

    protected transactionresponsecodeBO() {
        super();
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        transactionresponsecodeDAOInter engin = DAOFactory.createtransactionresponsecodeDAO();
        List<transactionresponsecodeDTOInter> AllRecords = (List<transactionresponsecodeDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object insertrecord(Object... obj) throws Throwable {
        transactionresponsecodeDTOInter RecordToInsert = (transactionresponsecodeDTOInter) obj[0];
        transactionresponsecodeDAOInter engin = DAOFactory.createtransactionresponsecodeDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<transactionresponsecodeDTOInter> entities = (List<transactionresponsecodeDTOInter>) obj[0];
        transactionresponsecodeDAOInter engin = DAOFactory.createtransactionresponsecodeDAO();
        return engin.save(entities);
    }

    public Object deleterecord(Object... obj) throws Throwable {
        transactionresponsecodeDTOInter RecordToDelete = (transactionresponsecodeDTOInter) obj[0];
        transactionresponsecodeDAOInter engin = DAOFactory.createtransactionresponsecodeDAO();
        return engin.deleterecord(RecordToDelete);
    }

    public Object deleterecordByMaster(Object... obj) throws Throwable {
        transactionresponsemasterDTOInter RecordToDelete = (transactionresponsemasterDTOInter) obj[0];
        transactionresponsecodeDAOInter engin = DAOFactory.createtransactionresponsecodeDAO();
        return engin.deleterecordByMaster(RecordToDelete);
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        transactionresponsemasterDTOInter SearchRecords = (transactionresponsemasterDTOInter) obj[0];
        transactionresponsecodeDAOInter engin = DAOFactory.createtransactionresponsecodeDAO();
        List<transactionresponsecodeDTOInter> AllRecords = (List<transactionresponsecodeDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }
}
