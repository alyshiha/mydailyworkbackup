package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.profileDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.menulabelsDAOInter;
import com.ev.Bingo.bus.dto.menulabelsDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.profileDTOInter;
import com.ev.Bingo.bus.dto.profilemenuDTOInter;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

public class profileBO extends BaseBO implements profileBOInter {

    protected profileBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        profileDTOInter RecordToInsert = (profileDTOInter) obj[0];
        profileDAOInter engin = DAOFactory.createprofileDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object insertmenuitem(Object... obj) throws Throwable {
        profilemenuDTOInter RecordToInsert = (profilemenuDTOInter) obj[0];
        profileDAOInter engin = DAOFactory.createprofileDAO();
        return engin.insertmenuitem(RecordToInsert);
    }

    public List<menulabelsDTOInter> getChildMenu(List<menulabelsDTOInter> Menu, Integer Parent) throws Throwable {

        List<menulabelsDTOInter> Result = new ArrayList<menulabelsDTOInter>();
        for (menulabelsDTOInter MenuItem : Menu) {
            if (MenuItem.getparentid() == Parent) {
                Result.add(MenuItem);
            }

        }

        return Result;
    }

    public List<menulabelsDTOInter> getMlDTOL(int profileId) throws Throwable {
        menulabelsDAOInter mlDAO = DAOFactory.createmenulabelsDAO();
        List<menulabelsDTOInter> mlDTOL = new ArrayList<menulabelsDTOInter>();
        mlDTOL = (List<menulabelsDTOInter>) mlDAO.findRecordsList(profileId);
        return mlDTOL;
    }

    public TreeNode getMenuTree() throws Throwable {
        TreeNode ROOT = new DefaultTreeNode("Root", null);
        menulabelsDAOInter mlDAO = DAOFactory.createmenulabelsDAO();
        List<menulabelsDTOInter> mlDTOL = (List<menulabelsDTOInter>) mlDAO.findParents();
        List<menulabelsDTOInter> menutemp = (List<menulabelsDTOInter>) mlDAO.findRecordsAll();
        TreeNode[] roots = new TreeNode[mlDTOL.size()];
        int i = 0;
        for (menulabelsDTOInter m : mlDTOL) {
            int j = 0;
            roots[i] = new DefaultTreeNode(m, ROOT);
            List<menulabelsDTOInter> mlCH1DTOL = getChildMenu(menutemp, m.getmenuid());
            if (mlCH1DTOL != null) {
                TreeNode[] children1 = new TreeNode[mlCH1DTOL.size()];
                for (menulabelsDTOInter mm : mlCH1DTOL) {
                    int z = 0;
                    children1[j] = new DefaultTreeNode(mm, roots[i]);
                    List<menulabelsDTOInter> mlCH2DTOL = getChildMenu(menutemp, mm.getmenuid());
                    if (mlCH2DTOL != null) {
                        TreeNode[] children2 = new TreeNode[mlCH2DTOL.size()];
                        for (menulabelsDTOInter mmm : mlCH2DTOL) {
                            int x = 0;
                            children2[z] = new DefaultTreeNode(mmm, children1[j]);
                            List<menulabelsDTOInter> mlCH3DTOL = getChildMenu(menutemp, mmm.getmenuid());
                            if (mlCH3DTOL != null) {
                                TreeNode[] children3 = new TreeNode[mlCH3DTOL.size()];
                                for (menulabelsDTOInter mmmm : mlCH3DTOL) {
                                    children3[x] = new DefaultTreeNode(mmmm, children2[z]);
                                    x++;
                                }
                            }
                            z++;
                        }
                    }
                    j++;
                }
            }
            i++;
        }
        return ROOT;
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<profileDTOInter> entities = (List<profileDTOInter>) obj[0];
        profileDAOInter engin = DAOFactory.createprofileDAO();
        return engin.save(entities);
    }

    public Object deleterecord(Object... obj) throws Throwable {
        profileDTOInter RecordToDelete = (profileDTOInter) obj[0];
        profileDAOInter engin = DAOFactory.createprofileDAO();
        return engin.deleterecord(RecordToDelete);
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        profileDAOInter engin = DAOFactory.createprofileDAO();
        List<profileDTOInter> AllRecords = (List<profileDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

}
