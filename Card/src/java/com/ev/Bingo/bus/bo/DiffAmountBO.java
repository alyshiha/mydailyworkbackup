/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.data.DataProviderFactory;
 
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.DiffAmountDAOInter;
import com.ev.Bingo.bus.dto.DiffAmountDTOInter;
import com.ev.Bingo.bus.dto.atmfileDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author AlyShiha
 */
public class DiffAmountBO implements DiffAmountBOInter {

    public Object GetReport(Object... obj) throws Throwable {
        String userName = (String) obj[0];
        String customer = (String) obj[1];
        Date fromDate = (Date) obj[4];
        Date toDate = (Date) obj[5];
        usersDTOInter UserDTO = (usersDTOInter) obj[2];
        List<DiffAmountDTOInter> ListOfResult = (List<DiffAmountDTOInter>) obj[3];
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        JRResultSetDataSource resultSetDataSource;
        params.put("Title", "Matched Transactions With Different Amount");
        DiffAmountDAOInter arrRepDAO = DAOFactory.createDiffAmountDAO();
        Integer index = (Integer) arrRepDAO.save(ListOfResult);
        ResultSet rs = (ResultSet) arrRepDAO.findAllRecordsPrint(index);
        resultSetDataSource = new JRResultSetDataSource(rs);
        params.put("user", userName);
        params.put("customer", customer);
        params.put("dateF", DateFormatter.changeDateAndTimeFormat(fromDate));
        params.put("dateT", DateFormatter.changeDateAndTimeFormat(toDate));
        //params.put("param", param);
        try {
            JasperDesign design = JRXmlLoader.load("C:/CardsReports/DiffAmount.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, resultSetDataSource);
            Calendar c = Calendar.getInstance();
            String uri = "PDFDifferentAmount" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {

                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                resultSetDataSource = null;
                CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());
                rs.close();
             
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }

    }

    public Object findfileall(Object... obj) throws Throwable {
        DiffAmountDAOInter engin = DAOFactory.createDiffAmountDAO();
        List<atmfileDTOInter> AllRecords = (List<atmfileDTOInter>) engin.findfileall();
        return AllRecords;
    }

    @Override
    public Object GetAllRecords(Object... obj) throws Throwable {
        Date fromDate = (Date) obj[0];
        Date toDate = (Date) obj[1];
        Integer network = (Integer) obj[2];
        String filename = (String) obj[3];
        usersDTOInter UserDTO = (usersDTOInter) obj[4];
        DiffAmountDAOInter engin = DAOFactory.createDiffAmountDAO();
        List<DiffAmountDTOInter> AllRecords = (List<DiffAmountDTOInter>) engin.findAllRecords(fromDate, toDate, network, filename, UserDTO);
        return AllRecords;
    }

    public void recalc() throws Throwable {
        DiffAmountDAOInter engin = DAOFactory.createDiffAmountDAO();
         engin.recalc();
    }
}
