package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.transactiontypemasterDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.transactiontypemasterDTOInter;

public class transactiontypemasterBO extends BaseBO implements transactiontypemasterBOInter {

    protected transactiontypemasterBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        transactiontypemasterDTOInter RecordToInsert = (transactiontypemasterDTOInter) obj[0];
        transactiontypemasterDAOInter engin = DAOFactory.createtransactiontypemasterDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<transactiontypemasterDTOInter> entities = (List<transactiontypemasterDTOInter>) obj[0];
        transactiontypemasterDAOInter engin = DAOFactory.createtransactiontypemasterDAO();
        return engin.save(entities);
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        transactiontypemasterDAOInter engin = DAOFactory.createtransactiontypemasterDAO();
        List<transactiontypemasterDTOInter> AllRecords = (List<transactiontypemasterDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        transactiontypemasterDTOInter RecordToDelete = (transactiontypemasterDTOInter) obj[0];
        transactiontypemasterDAOInter engin = DAOFactory.createtransactiontypemasterDAO();
        return engin.deleterecord(RecordToDelete);
    }

}
