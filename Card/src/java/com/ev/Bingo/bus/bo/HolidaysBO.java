/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.HolidaysDAOInter;
import com.ev.Bingo.bus.dto.HolidaysDTOInter;
import java.sql.Connection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author shi7a
 */
public class HolidaysBO implements HolidaysBOInter {
    
    @Override
    public Object printPDFrep(String customer, String PrintedName, String type) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        if ("null".equals(type) || type == null) {
            params.put("holidaytype", "%%");
        } else {
            params.put("holidaytype", "%" + type + "%");
        }
        params.put("customer", customer);
        params.put("userPrint", PrintedName);

        try {
          Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/BingoReports/holidays.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "holidays" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);                
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public List<HolidaysDTOInter> findRecord(String Type,String Name) throws Throwable {
        HolidaysDAOInter engin = DAOFactory.createHolidaysDAO();
        List<HolidaysDTOInter> records = engin.findRecord(Type,Name);
        return records;
    }

    @Override
    public List<HolidaysDTOInter> findAll() throws Throwable {
        HolidaysDAOInter engin = DAOFactory.createHolidaysDAO();
        List<HolidaysDTOInter> records = engin.findAll();
        return records;
    }

    @Override
    public String insertRecord(HolidaysDTOInter record) {
        try {
            HolidaysDAOInter engin = DAOFactory.createHolidaysDAO();
            engin.insert(record);
            return "Record Has Been Succesfully Added";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }
    
    @Override
    public String updateRecord(HolidaysDTOInter record) {
        try {
            HolidaysDAOInter engin = DAOFactory.createHolidaysDAO();
            engin.update(record);
            return "Record Has Been Succesfully Updates";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String deleteRecord(HolidaysDTOInter record) {
        try {
            HolidaysDAOInter engin = DAOFactory.createHolidaysDAO();
            return engin.delete(record);

        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

}
