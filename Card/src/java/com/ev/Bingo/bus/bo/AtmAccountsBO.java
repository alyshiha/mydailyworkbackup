/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;


import DBCONN.CoonectionHandler;
 
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dao.AtmAccountsDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dto.AtmAccountsDTOInter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author AlyShiha
 */
public class AtmAccountsBO implements AtmAccountsBOInter {

    public Object printPDFrep(String customer,  String PrintedName , int atmid, int atmgroupid,int accnameid,String account,String atmidvalue,String atmgroupvalue,String accnamevalue,String branch) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("customer", customer);
        params.put("userPrint", PrintedName);
        params.put("branch", branch);
        params.put("atmid", new BigDecimal(atmid));
        params.put("atmgroupid", new BigDecimal(atmgroupid));
        params.put("accnameid", new BigDecimal(accnameid));
        params.put("account", "%"+account+"%");
        params.put("atmidvalue", atmidvalue);
        params.put("atmgroupvalue", atmgroupvalue);
        params.put("accnamevalue", accnamevalue);
        
        try {
           Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/CardsReports/ATMAccount.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "ATMAccount" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Boolean findDuplicate(String atmid, int accname, String name, int count) throws Throwable {
        AtmAccountsDAOInter engin = DAOFactory.createAtmAccountsDAO();
        Boolean records = engin.findDuplicate(atmid, accname, name, count);
        return records;
    }

    @Override
    public List<AtmAccountsDTOInter> getAtmAccountrecord(int atmid, String accid, String accname, int group, String branch) throws Throwable {
        AtmAccountsDAOInter engin = DAOFactory.createAtmAccountsDAO();
        List<AtmAccountsDTOInter> records = engin.findrecord(atmid, accid, accname, group, branch);
        return records;
    }

    @Override
    public List<String> getBranchs() throws Throwable {
        AtmAccountsDAOInter engin = DAOFactory.createAtmAccountsDAO();
        List<String> records = engin.findAllBranchs();
        return records;
    }

    @Override
    public List<AtmAccountsDTOInter> getAtmAccount(String user) throws Throwable {
        AtmAccountsDAOInter engin = DAOFactory.createAtmAccountsDAO();
        List<AtmAccountsDTOInter> records = engin.findAll( user);
        return records;
    }

    @Override
    public String insertAtmAccount(AtmAccountsDTOInter record) {
        try {
            AtmAccountsDAOInter engin = DAOFactory.createAtmAccountsDAO();
            return engin.insert(record);
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String updateAtmAccount(AtmAccountsDTOInter record) {
        try {
            AtmAccountsDAOInter engin = DAOFactory.createAtmAccountsDAO();
            return engin.update(record);
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String deleteAtmAccount(AtmAccountsDTOInter record) {
        try {
            AtmAccountsDAOInter engin = DAOFactory.createAtmAccountsDAO();
            return engin.delete(record);
             
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }
}
