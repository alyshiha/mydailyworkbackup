/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.RejectedVisaDTOInter;
import java.util.List;

/**
 *
 * @author shi7a
 */
public interface RejectedVisaBOInter {

    String deleteReport(RejectedVisaDTOInter record);

    Boolean findDuplicateRecord(String reportID) throws Throwable;

    Boolean findDuplicatesaveRecord(String reportID, Integer RejectID) throws Throwable;

    List<RejectedVisaDTOInter> getRecord(String reportID) throws Throwable;

    List<RejectedVisaDTOInter> getReport() throws Throwable;

    String insertReport(RejectedVisaDTOInter record);

    Object printPDFrep(String customer, String PrintedName, String ReportID) throws Throwable;

    String updateReport(RejectedVisaDTOInter record);
    
}
