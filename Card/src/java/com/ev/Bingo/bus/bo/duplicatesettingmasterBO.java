package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.duplicatesettingmasterDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.duplicatesettingdetailDTOInter;
import com.ev.Bingo.bus.dto.duplicatesettingmasterDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.duplicatesettingmasterDTOInter;
import java.text.SimpleDateFormat;

public class duplicatesettingmasterBO extends BaseBO implements duplicatesettingmasterBOInter {

    protected duplicatesettingmasterBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        duplicatesettingmasterDTOInter RecordToInsert = (duplicatesettingmasterDTOInter) obj[0];
        duplicatesettingmasterDAOInter engin = DAOFactory.createduplicatesettingmasterDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object deleterecordbyid(Object... obj) throws Throwable {
        duplicatesettingdetailDTOInter RecordToDelete = (duplicatesettingdetailDTOInter) obj[0];
        duplicatesettingmasterDAOInter engin = DAOFactory.createduplicatesettingmasterDAO();
        return engin.deleterecordbyid(RecordToDelete);

    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<duplicatesettingdetailDTOInter> entities = (List<duplicatesettingdetailDTOInter>) obj[0];
        duplicatesettingmasterDAOInter engin = DAOFactory.createduplicatesettingmasterDAO();
        return (String) engin.save(entities);
    }

    public Object deleterecord(Object... obj) throws Throwable {
        duplicatesettingmasterDTOInter RecordToDelete = (duplicatesettingmasterDTOInter) obj[0];
        duplicatesettingmasterDAOInter engin = DAOFactory.createduplicatesettingmasterDAO();
        return engin.deleterecord(RecordToDelete);

    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        duplicatesettingmasterDAOInter engin = DAOFactory.createduplicatesettingmasterDAO();
        List<duplicatesettingmasterDTOInter> AllRecords = (List<duplicatesettingmasterDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

}
