package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.blacklistedcardDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.blacklistedcardDTOInter;

public class blacklistedcardBO extends BaseBO implements blacklistedcardBOInter {

    protected blacklistedcardBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        blacklistedcardDTOInter RecordToInsert = (blacklistedcardDTOInter) obj[0];
        blacklistedcardDAOInter engin = DAOFactory.createblacklistedcardDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<blacklistedcardDTOInter> entities = (List<blacklistedcardDTOInter>) obj[0];
        blacklistedcardDAOInter engin = DAOFactory.createblacklistedcardDAO();
        return engin.save(entities);
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        blacklistedcardDAOInter engin = DAOFactory.createblacklistedcardDAO();
        List<blacklistedcardDTOInter> AllRecords = (List<blacklistedcardDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        blacklistedcardDTOInter RecordToDelete = (blacklistedcardDTOInter) obj[0];
        blacklistedcardDAOInter engin = DAOFactory.createblacklistedcardDAO();
        return engin.deleterecord(RecordToDelete);
    }
}
