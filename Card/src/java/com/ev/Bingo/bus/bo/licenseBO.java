package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.licenseDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.licenseDTOInter;

public class licenseBO extends BaseBO implements licenseBOInter {

    protected licenseBO() {
        super();
    }

    public String GetRecord(Object... obj) throws Throwable {
        licenseDAOInter engin = DAOFactory.createlicenseDAO();
        String AllRecords = (String) engin.GetRecord();
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        licenseDAOInter engin = DAOFactory.createlicenseDAO();
        List<licenseDTOInter> AllRecords = (List<licenseDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

}
