package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.currencyDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.currencyDTOInter;
import com.ev.Bingo.bus.dto.currencymasterDTOInter;

public class currencyBO extends BaseBO implements currencyBOInter {

    protected currencyBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        currencyDTOInter RecordToInsert = (currencyDTOInter) obj[0];
        currencyDAOInter engin = DAOFactory.createcurrencyDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<currencyDTOInter> entities = (List<currencyDTOInter>) obj[0];
        currencyDAOInter engin = DAOFactory.createcurrencyDAO();
        return engin.save(entities);
    }

    public Object deleterecord(Object... obj) throws Throwable {
        currencyDTOInter RecordToDelete = (currencyDTOInter) obj[0];
        currencyDAOInter engin = DAOFactory.createcurrencyDAO();
        return engin.deleterecord(RecordToDelete);
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        currencymasterDTOInter SearchRecords = (currencymasterDTOInter) obj[0];
        currencyDAOInter engin = DAOFactory.createcurrencyDAO();
        List<currencyDTOInter> AllRecords = (List<currencyDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetListOfRecordsCurrency(Object... obj) throws Throwable {
        currencyDTOInter SearchRecords = (currencyDTOInter) obj[0];
        currencyDAOInter engin = DAOFactory.createcurrencyDAO();
        List<currencyDTOInter> AllRecords = (List<currencyDTOInter>) engin.findRecordsListCurrency(SearchRecords);
        return AllRecords;
    }

}
