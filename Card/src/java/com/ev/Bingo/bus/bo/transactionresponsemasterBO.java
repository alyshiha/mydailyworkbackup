package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.transactionresponsemasterDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.transactionresponsemasterDTOInter;

public class transactionresponsemasterBO extends BaseBO implements transactionresponsemasterBOInter {

    protected transactionresponsemasterBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        transactionresponsemasterDTOInter RecordToInsert = (transactionresponsemasterDTOInter) obj[0];
        transactionresponsemasterDAOInter engin = DAOFactory.createtransactionresponsemasterDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<transactionresponsemasterDTOInter> entities = (List<transactionresponsemasterDTOInter>) obj[0];
        transactionresponsemasterDAOInter engin = DAOFactory.createtransactionresponsemasterDAO();
        return engin.save(entities);
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        transactionresponsemasterDAOInter engin = DAOFactory.createtransactionresponsemasterDAO();
        List<transactionresponsemasterDTOInter> AllRecords = (List<transactionresponsemasterDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        transactionresponsemasterDTOInter RecordToDelete = (transactionresponsemasterDTOInter) obj[0];
        transactionresponsemasterDAOInter engin = DAOFactory.createtransactionresponsemasterDAO();
        return engin.deleterecord(RecordToDelete);
    }

}
