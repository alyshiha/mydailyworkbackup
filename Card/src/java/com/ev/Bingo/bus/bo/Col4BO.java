
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dao.BankCodeDAOInter;
import com.ev.Bingo.bus.dao.Col4DAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dto.BankCodeDTOInter;
import com.ev.Bingo.bus.dto.Col4DTOInter;
import java.util.List;

/**
 *
 * @author Aly
 */
public class Col4BO implements Col4BOInter {

    
    @Override
    public Object GetListOfRecords() throws Throwable {
        Col4DAOInter engin = DAOFactory.createCol4DAO();
        List<Col4DTOInter> AllRecords = (List<Col4DTOInter>) engin.findRecordsAll();
        return AllRecords;
    }
}
