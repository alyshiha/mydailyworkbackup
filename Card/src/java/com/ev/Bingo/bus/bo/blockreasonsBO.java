package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.blockreasonsDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.blockreasonsDTOInter;

public class blockreasonsBO extends BaseBO implements blockreasonsBOInter {

    protected blockreasonsBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        blockreasonsDTOInter RecordToInsert = (blockreasonsDTOInter) obj[0];
        blockreasonsDAOInter engin = DAOFactory.createblockreasonsDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<blockreasonsDTOInter> entities = (List<blockreasonsDTOInter>) obj[0];
        blockreasonsDAOInter engin = DAOFactory.createblockreasonsDAO();
        return engin.save(entities);
    }

    public Object deleterecord(Object... obj) throws Throwable {
        blockreasonsDTOInter RecordToDelete = (blockreasonsDTOInter) obj[0];
        blockreasonsDAOInter engin = DAOFactory.createblockreasonsDAO();
        return engin.deleterecord(RecordToDelete);

    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        blockreasonsDAOInter engin = DAOFactory.createblockreasonsDAO();
        List<blockreasonsDTOInter> AllRecords = (List<blockreasonsDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

}
