package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.copyfilesDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.copyfilesDTOInter;

public class copyfilesBO extends BaseBO implements copyfilesBOInter {

    protected copyfilesBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        copyfilesDTOInter RecordToInsert = (copyfilesDTOInter) obj[0];
        copyfilesDAOInter engin = DAOFactory.createcopyfilesDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<copyfilesDTOInter> entities = (List<copyfilesDTOInter>) obj[0];
        copyfilesDAOInter engin = DAOFactory.createcopyfilesDAO();
        return engin.save(entities);
    }

    public Object updaterecord(Object... obj) throws Throwable {
        copyfilesDTOInter RecordToUpdate = (copyfilesDTOInter) obj[0];
        copyfilesDAOInter engin = DAOFactory.createcopyfilesDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        copyfilesDAOInter engin = DAOFactory.createcopyfilesDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        copyfilesDTOInter RecordToDelete = (copyfilesDTOInter) obj[0];
        copyfilesDAOInter engin = DAOFactory.createcopyfilesDAO();
        return engin.deleterecord(RecordToDelete);
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<copyfilesDTOInter> SearchRecords = (List<copyfilesDTOInter>) obj[0];
        copyfilesDAOInter engin = DAOFactory.createcopyfilesDAO();
        List<copyfilesDTOInter> AllRecords = (List<copyfilesDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        copyfilesDAOInter engin = DAOFactory.createcopyfilesDAO();
        List<copyfilesDTOInter> AllRecords = (List<copyfilesDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        copyfilesDTOInter SearchParameter = (copyfilesDTOInter) obj[0];
        copyfilesDAOInter engin = DAOFactory.createcopyfilesDAO();
        copyfilesDTOInter Record = (copyfilesDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
