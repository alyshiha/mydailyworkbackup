package com.ev.Bingo.bus.bo;

import java.sql.SQLException;

public interface transactionresponsemasterBOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object GetAllRecords(Object... obj) throws Throwable;
}
