/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import java.util.Date;

/**
 *
 * @author shi7a
 */
public interface settlementsBOInter {

    Object GetAllRecordsAcquier(Date fromdate, Date todate) throws Throwable;

    Object GetAllRecordsIssuer(Date fromdate, Date todate) throws Throwable;

    Object GetReport(Date fromdate, Date todate, String userName, String customer) throws Throwable;
    
    Object GetAlltimefromswitch(Date fromdate,Date todate) throws Throwable;
}
