/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.AtmMachineDTOInter;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

/**
 *
 * @author shi7a
 */
public interface ProfitReportBOInter {

    String findReport(String Type, Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID, String userName, String customer,String atmid) throws Throwable;
    List<AtmMachineDTOInter> GetAtmMachineList() throws Throwable;
    Hashtable findfileall(String Type, Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable;
    
}
