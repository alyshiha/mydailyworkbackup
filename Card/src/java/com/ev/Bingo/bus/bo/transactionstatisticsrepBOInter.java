package com.ev.Bingo.bus.bo;

import java.sql.SQLException;

public interface transactionstatisticsrepBOInter {

    Object GetReport(Object... obj) throws Throwable;

    Object RunRep(Object... obj) throws SQLException, Throwable;

    Object GetListOfRecords(Object... obj) throws Throwable;

    Object GetAllRecords(Object... obj) throws Throwable;
}
