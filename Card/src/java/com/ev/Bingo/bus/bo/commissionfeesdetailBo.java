package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.commissionfeesdetailDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.commissionfeesDTOInter;
import com.ev.Bingo.bus.dto.commissionfeesdetailDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.commissionfeesdetailDTOInter;
import java.text.SimpleDateFormat;

public class commissionfeesdetailBo extends BaseBO implements commissionfeesdetailBoInter {

    protected commissionfeesdetailBo() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        commissionfeesdetailDTOInter RecordToInsert = (commissionfeesdetailDTOInter) obj[0];
        commissionfeesdetailDAOInter engin = DAOFactory.createcommissionfeesdetailDAO();
        return engin.insertrecord(RecordToInsert);

    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<commissionfeesdetailDTOInter> entities = (List<commissionfeesdetailDTOInter>) obj[0];
        commissionfeesdetailDAOInter engin = DAOFactory.createcommissionfeesdetailDAO();
        return engin.save(entities);
    }

    public Object updaterecord(Object... obj) throws Throwable {
        commissionfeesdetailDTOInter RecordToUpdate = (commissionfeesdetailDTOInter) obj[0];
        commissionfeesdetailDAOInter engin = DAOFactory.createcommissionfeesdetailDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        commissionfeesdetailDAOInter engin = DAOFactory.createcommissionfeesdetailDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        commissionfeesdetailDTOInter RecordToDelete = (commissionfeesdetailDTOInter) obj[0];
        commissionfeesdetailDAOInter engin = DAOFactory.createcommissionfeesdetailDAO();
        return engin.deleterecord(RecordToDelete);

    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        commissionfeesDTOInter SearchRecords = (commissionfeesDTOInter) obj[0];
        commissionfeesdetailDAOInter engin = DAOFactory.createcommissionfeesdetailDAO();
        List<commissionfeesdetailDTOInter> AllRecords = (List<commissionfeesdetailDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object deleteallrecord (Object... obj) throws Throwable {
        commissionfeesDTOInter record = (commissionfeesDTOInter) obj[0];
        commissionfeesdetailDAOInter engin = DAOFactory.createcommissionfeesdetailDAO();
        return (String) engin.deleteallrecord(record);

    }

    public Object GetRecord(Object... obj) throws Throwable {
        commissionfeesdetailDTOInter SearchParameter = (commissionfeesdetailDTOInter) obj[0];
        commissionfeesdetailDAOInter engin = DAOFactory.createcommissionfeesdetailDAO();
        commissionfeesdetailDTOInter Record = (commissionfeesdetailDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
