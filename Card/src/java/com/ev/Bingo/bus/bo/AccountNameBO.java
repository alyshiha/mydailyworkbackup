
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;


import DBCONN.CoonectionHandler;
 
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.AccountNameDAOInter;
import com.ev.Bingo.bus.dto.AccountNameDTOInter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author AlyShiha
 */
public class AccountNameBO implements AccountNameBOInter {

    public Object printPDFrep(String customer, String PrintedName, String Account) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        if ("null".equals(Account) || Account == null) {
            params.put("Account", "%%");
        } else {
            params.put("Account", "%" + Account + "%");
        }
        params.put("customer", customer);
        params.put("userPrint", PrintedName);

        try {
          Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/CardsReports/AccountName.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "AccountName" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
               CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Boolean findDuplicatesaveRecord(String Name, Integer id) throws Throwable {
        AccountNameDAOInter engin = DAOFactory.createAccountNameDAO();
        Boolean record = engin.findDuplicatesaveRecord(Name, id);
        return record;
    }

    @Override
    public Boolean findDuplicateRecord(String Name) throws Throwable {
        AccountNameDAOInter engin = DAOFactory.createAccountNameDAO();
        Boolean record = engin.findDuplicateRecord(Name);
        return record;
    }

    @Override
    public List<AccountNameDTOInter> getRecord(String Name) throws Throwable {
        AccountNameDAOInter engin = DAOFactory.createAccountNameDAO();
        List<AccountNameDTOInter> records = engin.findRecord(Name);
        return records;
    }

    @Override
    public List<AccountNameDTOInter> getNetworks() throws Throwable {
        AccountNameDAOInter engin = DAOFactory.createAccountNameDAO();
        List<AccountNameDTOInter> records = engin.findAll();
        return records;
    }

    @Override
    public String insertNetworks(AccountNameDTOInter record) {
        try {
            AccountNameDAOInter engin = DAOFactory.createAccountNameDAO();
            engin.insert(record);
            return "Record Has Been Succesfully Added";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String updateNetworks(AccountNameDTOInter record) {
        try {
            AccountNameDAOInter engin = DAOFactory.createAccountNameDAO();
            engin.update(record);
            return "Record Has Been Succesfully Updates";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String deleteNetworks(AccountNameDTOInter record) {
        try {
            AccountNameDAOInter engin = DAOFactory.createAccountNameDAO();
            return engin.delete(record).toString();
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }
}
