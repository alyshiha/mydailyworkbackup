/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBOInter;

/**
 *
 * @author Administrator
 */
public interface JobMonitorBOInter extends BaseBOInter {

    int getActiveFiles() throws Throwable;

    Object getErrorMonitor() throws Throwable;

    int getFilesToBuckup() throws Throwable;

    Object getFilesToMove() throws Throwable;

    void disableJob(boolean status, int process) throws Throwable;

    Object getLoadingMonitor() throws Throwable;

    Object getMatchingMonitor() throws Throwable;

    Object getValidationMonitor() throws Throwable;

    boolean getStatus(int process) throws Throwable;

    Object rematch() throws Throwable;

}
