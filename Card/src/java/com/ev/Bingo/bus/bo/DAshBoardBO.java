/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.DAshBoardDAOInter;
import com.ev.Bingo.bus.dto.DAshBoardDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class DAshBoardBO implements DAshBoardBOInter {

    @Override
    public Object findfileall(Object... obj) throws Throwable {
        String Match = (String) obj[0];
        DAshBoardDAOInter engin = DAOFactory.createDAshBoardDAO();
        List<DAshBoardDTOInter> AllRecords = (List<DAshBoardDTOInter>) engin.findRecordsList(Match);
        return AllRecords;
    }
}
