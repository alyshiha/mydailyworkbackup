package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.hostfiletemplateDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.hostfiletemplateDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.hostfiletemplateDTOInter;
import java.text.SimpleDateFormat;

public class hostfiletemplateBO extends BaseBO implements hostfiletemplateBOInter {

    protected hostfiletemplateBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        hostfiletemplateDTOInter RecordToInsert = (hostfiletemplateDTOInter) obj[0];
        hostfiletemplateDAOInter engin = DAOFactory.createhostfiletemplateDAO();
        engin.insertrecord(RecordToInsert);
        return "Insert Done";
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<hostfiletemplateDTOInter> entities = (List<hostfiletemplateDTOInter>) obj[0];
        hostfiletemplateDAOInter engin = DAOFactory.createhostfiletemplateDAO();
        engin.save(entities);
        return "Insert Done";
    }

    public Object updaterecord(Object... obj) throws Throwable {
        hostfiletemplateDTOInter RecordToUpdate = (hostfiletemplateDTOInter) obj[0];
        hostfiletemplateDAOInter engin = DAOFactory.createhostfiletemplateDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        hostfiletemplateDAOInter engin = DAOFactory.createhostfiletemplateDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        hostfiletemplateDTOInter RecordToDelete = (hostfiletemplateDTOInter) obj[0];
        hostfiletemplateDAOInter engin = DAOFactory.createhostfiletemplateDAO();
        engin.deleterecord(RecordToDelete);
        return "record has been deleted";
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<hostfiletemplateDTOInter> SearchRecords = (List<hostfiletemplateDTOInter>) obj[0];
        hostfiletemplateDAOInter engin = DAOFactory.createhostfiletemplateDAO();
        List<hostfiletemplateDTOInter> AllRecords = (List<hostfiletemplateDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        hostfiletemplateDAOInter engin = DAOFactory.createhostfiletemplateDAO();
        List<hostfiletemplateDTOInter> AllRecords = (List<hostfiletemplateDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        hostfiletemplateDTOInter SearchParameter = (hostfiletemplateDTOInter) obj[0];
        hostfiletemplateDAOInter engin = DAOFactory.createhostfiletemplateDAO();
        hostfiletemplateDTOInter Record = (hostfiletemplateDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
