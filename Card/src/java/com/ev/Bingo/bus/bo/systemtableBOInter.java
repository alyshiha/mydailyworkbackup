package com.ev.Bingo.bus.bo;

import java.sql.SQLException;

public interface systemtableBOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object GetAllRecords(Object... obj) throws Throwable;

    Object GetRecord(Object... obj) throws Throwable;//used in main
    
    Object GetExportPath(String Path) throws Throwable;
}
