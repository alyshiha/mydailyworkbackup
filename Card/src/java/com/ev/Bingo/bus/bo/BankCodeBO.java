/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dao.BankCodeDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dto.BankCodeDTOInter;
import java.util.List;

/**
 *
 * @author Aly
 */
public class BankCodeBO implements BankCodeBOInter {

    @Override
    public Object GetListOfRecords() throws Throwable {
        BankCodeDAOInter engin = DAOFactory.createBankCodeDAO();
        List<BankCodeDTOInter> AllRecords = (List<BankCodeDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }
}
