package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.blockedusersDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.blockedusersDTOInter;
import com.ev.Bingo.bus.dto.blockreasonsDTOInter;

public class blockedusersBO extends BaseBO implements blockedusersBOInter {

    protected blockedusersBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        blockedusersDTOInter RecordToInsert = (blockedusersDTOInter) obj[0];
        blockedusersDAOInter engin = DAOFactory.createblockedusersDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object deleterecord(Object... obj) throws Throwable {
        blockedusersDTOInter RecordToDelete = (blockedusersDTOInter) obj[0];
        blockedusersDAOInter engin = DAOFactory.createblockedusersDAO();

        return engin.deleterecord(RecordToDelete);
    }

    public Object findRecordsListReasonID(Object... obj) throws Throwable {
        blockreasonsDTOInter SearchRecords = (blockreasonsDTOInter) obj[0];
        blockedusersDAOInter engin = DAOFactory.createblockedusersDAO();
        List<blockedusersDTOInter> AllRecords = (List<blockedusersDTOInter>) engin.findByReasonID(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        blockedusersDAOInter engin = DAOFactory.createblockedusersDAO();
        List<blockedusersDTOInter> AllRecords = (List<blockedusersDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

}
