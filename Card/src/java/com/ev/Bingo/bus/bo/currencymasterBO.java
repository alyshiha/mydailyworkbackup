package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.currencymasterDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.currencymasterDTOInter;

public class currencymasterBO extends BaseBO implements currencymasterBOInter {

    protected currencymasterBO() {
        super();
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        currencymasterDAOInter engin = DAOFactory.createcurrencymasterDAO();
        List<currencymasterDTOInter> AllRecords = (List<currencymasterDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object insertrecord(Object... obj) throws Throwable {
        currencymasterDTOInter RecordToInsert = (currencymasterDTOInter) obj[0];
        currencymasterDAOInter engin = DAOFactory.createcurrencymasterDAO();
        return engin.insertrecord(RecordToInsert);

    }

    public Object deleterecord(Object... obj) throws Throwable {
        currencymasterDTOInter RecordToDelete = (currencymasterDTOInter) obj[0];
        currencymasterDAOInter engin = DAOFactory.createcurrencymasterDAO();
        return engin.deleterecord(RecordToDelete);
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<currencymasterDTOInter> entities = (List<currencymasterDTOInter>) obj[0];
        currencymasterDAOInter engin = DAOFactory.createcurrencymasterDAO();
        return engin.save(entities);
    }
}
