package com.ev.Bingo.bus.bo;

import java.sql.SQLException;

public interface filecolumndefinitionBOInter {

    Object save(Object... obj) throws SQLException, Throwable;

    Object GetAllRecords(Object... obj) throws Throwable;

    Object findRecordByColName(Object... obj) throws Throwable;
    
     Object findRecordByColName4(Object... obj) throws Throwable;

    Object GetAllRecordsVIEW(Object... obj) throws Throwable;
}
