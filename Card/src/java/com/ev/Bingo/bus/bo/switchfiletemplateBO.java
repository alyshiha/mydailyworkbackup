package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.switchfiletemplateDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.switchfiletemplateDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.switchfiletemplateDTOInter;
import java.text.SimpleDateFormat;

public class switchfiletemplateBO extends BaseBO implements switchfiletemplateBOInter {

    protected switchfiletemplateBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        switchfiletemplateDTOInter RecordToInsert = (switchfiletemplateDTOInter) obj[0];
        switchfiletemplateDAOInter engin = DAOFactory.createswitchfiletemplateDAO();
        engin.insertrecord(RecordToInsert);
        return "Insert Done";
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<switchfiletemplateDTOInter> entities = (List<switchfiletemplateDTOInter>) obj[0];
        switchfiletemplateDAOInter engin = DAOFactory.createswitchfiletemplateDAO();
        engin.save(entities);
        return "Insert Done";
    }

    public Object updaterecord(Object... obj) throws Throwable {
        switchfiletemplateDTOInter RecordToUpdate = (switchfiletemplateDTOInter) obj[0];
        switchfiletemplateDAOInter engin = DAOFactory.createswitchfiletemplateDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        switchfiletemplateDAOInter engin = DAOFactory.createswitchfiletemplateDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        switchfiletemplateDTOInter RecordToDelete = (switchfiletemplateDTOInter) obj[0];
        switchfiletemplateDAOInter engin = DAOFactory.createswitchfiletemplateDAO();
        engin.deleterecord(RecordToDelete);
        return "record has been deleted";
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<switchfiletemplateDTOInter> SearchRecords = (List<switchfiletemplateDTOInter>) obj[0];
        switchfiletemplateDAOInter engin = DAOFactory.createswitchfiletemplateDAO();
        List<switchfiletemplateDTOInter> AllRecords = (List<switchfiletemplateDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        switchfiletemplateDAOInter engin = DAOFactory.createswitchfiletemplateDAO();
        List<switchfiletemplateDTOInter> AllRecords = (List<switchfiletemplateDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        switchfiletemplateDTOInter SearchParameter = (switchfiletemplateDTOInter) obj[0];
        switchfiletemplateDAOInter engin = DAOFactory.createswitchfiletemplateDAO();
        switchfiletemplateDTOInter Record = (switchfiletemplateDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
