/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import com.ev.Bingo.bus.dto.validationrulesDTOInter;
import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public interface validationrulesBOInter {

    String genarateValueString(Integer[] values);

    Object insertrecord(Object... obj) throws Throwable;

    Object createFormula(int column, String opt, String value, validationrulesDTOInter vrDTO) throws Throwable;

    String genarateValueDate(Date d1, Date d2);

    Object getColumnValues(String column) throws Throwable;

    Object GetAllRecords(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;

    Object deleterecord(Object... obj) throws Throwable;

}
