/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.HolidaysDTOInter;
import java.util.List;

/**
 *
 * @author shi7a
 */
public interface HolidaysBOInter {

    String deleteRecord(HolidaysDTOInter record);

    List<HolidaysDTOInter> findAll() throws Throwable;

    List<HolidaysDTOInter> findRecord(String Type,String Name) throws Throwable;

    String insertRecord(HolidaysDTOInter record);

    Object printPDFrep(String customer, String PrintedName, String type) throws Throwable;

    String updateRecord(HolidaysDTOInter record);
    
}
