/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.AtmJournalDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface AtmJournalBOInter {

    Object printrep8(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName,String indication,Boolean realesed) throws Throwable;

    List<AtmJournalDTOInter> findjournalatm(String dateFrom, String dateTo, String NoOfAtms, int export, int group, String Reversal, int user,String indication,Boolean realesed) throws Throwable;

    void recalc(Date selectedDate) throws Throwable;
void recalc2() throws Throwable ;
    String deleteJournal(AtmJournalDTOInter record);

    List<AtmJournalDTOInter> getJournal() throws Throwable;

    List<AtmJournalDTOInter> getJournalrecord(String AppID, int repid, int export, String FromDate) throws Throwable;

    String insertJournal(AtmJournalDTOInter record);

    String updateJournal(List<AtmJournalDTOInter> record, String flag);

}
