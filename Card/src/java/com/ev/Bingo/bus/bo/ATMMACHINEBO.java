/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dao.ATMMACHINEDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dto.ATMMACHINECASHDTOINTER;
import com.ev.Bingo.bus.dto.AtmMachineDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public class ATMMACHINEBO implements ATMMACHINEBOInter {

    @Override
    public List<ATMMACHINECASHDTOINTER> findRecord(String name,String id) throws Throwable {
        ATMMACHINEDAOInter engin = DAOFactory.createATMMACHINEDAO();
        List<ATMMACHINECASHDTOINTER> records = engin.findRecord(name,id);
        return records;
    }

    @Override
    public List<ATMMACHINECASHDTOINTER> findAll() throws Throwable {
        ATMMACHINEDAOInter engin = DAOFactory.createATMMACHINEDAO();
        List<ATMMACHINECASHDTOINTER> records = engin.findAll();
        return records;
    }
    
     @Override
    public List<ATMMACHINECASHDTOINTER> findAllDistinct() throws Throwable {
        ATMMACHINEDAOInter engin = DAOFactory.createATMMACHINEDAO();
        List<ATMMACHINECASHDTOINTER> records = engin.findAllDistinct();
        return records;
    }

    @Override
    public String insertRecord(String atmid,String atmname) {
        try {
            ATMMACHINEDAOInter engin = DAOFactory.createATMMACHINEDAO();
            engin.insert( atmid, atmname);
            return "Record Has Been Succesfully Added";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String updateRecord(ATMMACHINECASHDTOINTER record) {
        try {
            ATMMACHINEDAOInter engin = DAOFactory.createATMMACHINEDAO();
            engin.update(record);
            return "Record Has Been Succesfully Updates";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String deleteRecord(ATMMACHINECASHDTOINTER record) {
        try {
            ATMMACHINEDAOInter engin = DAOFactory.createATMMACHINEDAO();
            return engin.delete(record);

        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }
}
