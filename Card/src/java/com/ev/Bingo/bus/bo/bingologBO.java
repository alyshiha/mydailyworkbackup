package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.bingologDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import java.util.List;
import java.util.Date;
import com.ev.Bingo.bus.dto.bingologDTOInter;

public class bingologBO extends BaseBO implements bingologBOInter {

    protected bingologBO() {
        super();
    }

    public Object GetListOfRecords(Date FromDate, Date ToDate, Integer UserID, Integer ProfileID) throws Throwable {

        String whereClause = "";
        if (FromDate != null) {
            whereClause += " and b.ACTION_DATE > to_date('$dateF','dd.mm.yyyy hh24:mi:ss') ";
            whereClause = whereClause.replace("$dateF", "" + DateFormatter.changeDateAndTimeFormat(FromDate));
        }
        if (ToDate != null) {
            whereClause += " and b.ACTION_DATE < to_date('$dateT','dd.mm.yyyy hh24:mi:ss')";
            whereClause = whereClause.replace("$dateT", "" + DateFormatter.changeDateAndTimeFormat(ToDate));
        }

        if (UserID != 0) {
            whereClause += " and b.user_id = $user";
            whereClause = whereClause.replace("$user", "" + UserID);
        }
        if (ProfileID != 0) {
            whereClause += " and up.profile_id = $profile";
            whereClause = whereClause.replace("$profile", "" + ProfileID);
        }
        bingologDAOInter engin = DAOFactory.createbingologDAO();
        List<bingologDTOInter> AllRecords = (List<bingologDTOInter>) engin.findRecordsList(whereClause);
        return AllRecords;
    }

}
