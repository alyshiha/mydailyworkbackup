package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.switchfiletemplatedetailDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.switchfiletemplatedetailDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.switchfiletemplatedetailDTOInter;
import java.text.SimpleDateFormat;

public class switchfiletemplatedetailBO extends BaseBO implements switchfiletemplatedetailBOInter {

    protected switchfiletemplatedetailBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        switchfiletemplatedetailDTOInter RecordToInsert = (switchfiletemplatedetailDTOInter) obj[0];
        switchfiletemplatedetailDAOInter engin = DAOFactory.createswitchfiletemplatedetailDAO();
        engin.insertrecord(RecordToInsert);
        return "Insert Done";
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<switchfiletemplatedetailDTOInter> entities = (List<switchfiletemplatedetailDTOInter>) obj[0];
        switchfiletemplatedetailDAOInter engin = DAOFactory.createswitchfiletemplatedetailDAO();
        engin.save(entities);
        return "Insert Done";
    }

    public Object updaterecord(Object... obj) throws Throwable {
        switchfiletemplatedetailDTOInter RecordToUpdate = (switchfiletemplatedetailDTOInter) obj[0];
        switchfiletemplatedetailDAOInter engin = DAOFactory.createswitchfiletemplatedetailDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        switchfiletemplatedetailDAOInter engin = DAOFactory.createswitchfiletemplatedetailDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        switchfiletemplatedetailDTOInter RecordToDelete = (switchfiletemplatedetailDTOInter) obj[0];
        switchfiletemplatedetailDAOInter engin = DAOFactory.createswitchfiletemplatedetailDAO();
        engin.deleterecord(RecordToDelete);
        return "record has been deleted";
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<switchfiletemplatedetailDTOInter> SearchRecords = (List<switchfiletemplatedetailDTOInter>) obj[0];
        switchfiletemplatedetailDAOInter engin = DAOFactory.createswitchfiletemplatedetailDAO();
        List<switchfiletemplatedetailDTOInter> AllRecords = (List<switchfiletemplatedetailDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        switchfiletemplatedetailDAOInter engin = DAOFactory.createswitchfiletemplatedetailDAO();
        List<switchfiletemplatedetailDTOInter> AllRecords = (List<switchfiletemplatedetailDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        switchfiletemplatedetailDTOInter SearchParameter = (switchfiletemplatedetailDTOInter) obj[0];
        switchfiletemplatedetailDAOInter engin = DAOFactory.createswitchfiletemplatedetailDAO();
        switchfiletemplatedetailDTOInter Record = (switchfiletemplatedetailDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
