package com.ev.Bingo.bus.bo;

import java.sql.SQLException;

public interface licenseBOInter {

    Object GetAllRecords(Object... obj) throws Throwable;

    String GetRecord(Object... obj) throws Throwable;
}
