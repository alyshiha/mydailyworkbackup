package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.menulabelsDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.menulabelsDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.menulabelsDTOInter;
import java.text.SimpleDateFormat;

public class menulabelsBO extends BaseBO implements menulabelsBOInter {

    protected menulabelsBO() {
        super();
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<menulabelsDTOInter> entities = (List<menulabelsDTOInter>) obj[0];
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        return engin.save(entities);

    }

    public Object insertrecord(Object... obj) throws Throwable {
        menulabelsDTOInter RecordToInsert = (menulabelsDTOInter) obj[0];
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        return engin.insertrecord(RecordToInsert);

    }

    public Object deleterecord(Object... obj) throws Throwable {
        menulabelsDTOInter RecordToDelete = (menulabelsDTOInter) obj[0];
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        return engin.deleterecord(RecordToDelete);

    }

    public Object GetPageListOfRecords(Object... obj) throws Throwable {
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        List<menulabelsDTOInter> AllRecords = (List<menulabelsDTOInter>) engin.findPageRecordsList();
        return AllRecords;
    }

    public Object GetParentListOfRecords(Object... obj) throws Throwable {
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        List<menulabelsDTOInter> AllRecords = (List<menulabelsDTOInter>) engin.findParentRecordsList();
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        List<menulabelsDTOInter> AllRecords = (List<menulabelsDTOInter>) engin.findRecordsAll();
        return AllRecords;
    } public Object GetAllRecords2(Object... obj) throws Throwable {
        menulabelsDAOInter engin = DAOFactory.createmenulabelsDAO();
        List<menulabelsDTOInter> AllRecords = (List<menulabelsDTOInter>) engin.findRecordsAll2();
        return AllRecords;
    }

}
