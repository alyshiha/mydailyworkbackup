package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.transactiontypeDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.transactiontypeDTOInter;
import com.ev.Bingo.bus.dto.transactiontypemasterDTOInter;

public class transactiontypeBO extends BaseBO implements transactiontypeBOInter {

    protected transactiontypeBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        transactiontypeDTOInter RecordToInsert = (transactiontypeDTOInter) obj[0];
        transactiontypeDAOInter engin = DAOFactory.createtransactiontypeDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        transactiontypeDAOInter engin = DAOFactory.createtransactiontypeDAO();
        List<transactiontypeDTOInter> AllRecords = (List<transactiontypeDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<transactiontypeDTOInter> entities = (List<transactiontypeDTOInter>) obj[0];
        transactiontypeDAOInter engin = DAOFactory.createtransactiontypeDAO();
        return engin.save(entities);
    }

    public Object deleterecord(Object... obj) throws Throwable {
        transactiontypeDTOInter RecordToDelete = (transactiontypeDTOInter) obj[0];
        transactiontypeDAOInter engin = DAOFactory.createtransactiontypeDAO();
        return engin.deleterecord(RecordToDelete);
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        transactiontypemasterDTOInter SearchRecords = (transactiontypemasterDTOInter) obj[0];
        transactiontypeDAOInter engin = DAOFactory.createtransactiontypeDAO();
        List<transactiontypeDTOInter> AllRecords = (List<transactiontypeDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

}
