package com.ev.Bingo.bus.bo;

import DBCONN.CoonectionHandler;

import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.RejectedVisaDAOInter;
import com.ev.Bingo.bus.dto.RejectedVisaDTOInter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class RejectedVisaBO implements RejectedVisaBOInter {

    @Override
    public Object printPDFrep(String customer, String PrintedName, String ReportID) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        if ("null".equals(ReportID) || ReportID == null) {
            params.put("ReportIDString", "%%");
        } else {
            params.put("ReportIDString", "%" + ReportID + "%");
        }
        params.put("customer", customer);
        params.put("userPrint", PrintedName);

        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/CardsReports/ReportID.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "ReportID" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public Boolean findDuplicatesaveRecord(String reportID, Integer RejectID) throws Throwable {
        RejectedVisaDAOInter engin = DAOFactory.createRejectedVisaDAO();
        Boolean record = engin.findDuplicatesaveRecord(reportID, RejectID);
        return record;
    }

    @Override
    public Boolean findDuplicateRecord(String reportID) throws Throwable {
        RejectedVisaDAOInter engin = DAOFactory.createRejectedVisaDAO();
        Boolean record = engin.findDuplicateRecord(reportID);
        return record;
    }

    @Override
    public List<RejectedVisaDTOInter> getRecord(String reportID) throws Throwable {
        RejectedVisaDAOInter engin = DAOFactory.createRejectedVisaDAO();
        List<RejectedVisaDTOInter> records = engin.findRecord(reportID);
        return records;
    }

    @Override
    public List<RejectedVisaDTOInter> getReport() throws Throwable {
        RejectedVisaDAOInter engin = DAOFactory.createRejectedVisaDAO();
        List<RejectedVisaDTOInter> records = engin.findAll();
        return records;
    }

    @Override
    public String insertReport(RejectedVisaDTOInter record) {
        try {
            RejectedVisaDAOInter engin = DAOFactory.createRejectedVisaDAO();
            engin.insert(record);
            return "Record Has Been Succesfully Added";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String updateReport(RejectedVisaDTOInter record) {
        try {
            RejectedVisaDAOInter engin = DAOFactory.createRejectedVisaDAO();
            engin.update(record);
            return "Record Has Been Succesfully Updates";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String deleteReport(RejectedVisaDTOInter record) {
        try {
            RejectedVisaDAOInter engin = DAOFactory.createRejectedVisaDAO();
            return engin.delete(record).toString();
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }
}
