package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.usersDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.util.ArrayList;

public class usersBO extends BaseBO implements usersBOInter {

    protected usersBO() {
        super();
    }
////userd in user mangement

    public Object insertrecord(Object... obj) throws Throwable {
        usersDTOInter RecordToInsert = (usersDTOInter) obj[0];
        usersDAOInter engin = DAOFactory.createusersDAO();
        return engin.insertrecord(RecordToInsert);

    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<usersDTOInter> entities = (List<usersDTOInter>) obj[0];
        ArrayList result = (ArrayList)obj[1];
        usersDAOInter engin = DAOFactory.createusersDAO();
        return engin.save(entities,result);

    }

    public Object updateStatus(Object... obj) throws Throwable {
        String RecordToUpdate = (String) obj[0];
        usersDAOInter engin = DAOFactory.createusersDAO();

        return engin.updateStatus(RecordToUpdate);
    }
//Used In ChangePassword

    public Object updaterecord(Object... obj) throws Throwable {
        usersDTOInter RecordToUpdate = (usersDTOInter) obj[0];
        usersDAOInter engin = DAOFactory.createusersDAO();

        return engin.updaterecord(RecordToUpdate);
    }

    /*   public Object updatedashboard(Object... obj) throws Throwable {
     usersDTOInter RecordToUpdate = (usersDTOInter) obj[0];
     usersDAOInter engin = DAOFactory.createusersDAO();
     return engin.updatedashboard(RecordToUpdate);
     }
     public Object updatedashboardunselect(Object... obj) throws Throwable {
     usersDTOInter RecordToUpdate = (usersDTOInter) obj[0];
     usersDAOInter engin = DAOFactory.createusersDAO();
     return engin.updatedashboardunselect(RecordToUpdate);
     }*/
    public Object deleteallrecord(Object... obj) throws Throwable {
        usersDAOInter engin = DAOFactory.createusersDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        usersDTOInter RecordToDelete = (usersDTOInter) obj[0];
        usersDAOInter engin = DAOFactory.createusersDAO();
        return engin.deleterecord(RecordToDelete);

    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<usersDTOInter> SearchRecords = (List<usersDTOInter>) obj[0];
        usersDAOInter engin = DAOFactory.createusersDAO();
        List<usersDTOInter> AllRecords = (List<usersDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    //Used In ChangePassword

    public Object GetAllRecords(Object... obj) throws Throwable {
        usersDAOInter engin = DAOFactory.createusersDAO();
        List<usersDTOInter> AllRecords = (List<usersDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object findAllBlockRecords(Object... obj) throws Throwable {
        usersDAOInter engin = DAOFactory.createusersDAO();
        List<usersDTOInter> AllRecords = (List<usersDTOInter>) engin.findAllBlockRecords();
        return AllRecords;
    }
//userd in user mangement

    public Object findRecordsAlluserManag(Object... obj) throws Throwable {
        usersDAOInter engin = DAOFactory.createusersDAO();
        List<usersDTOInter> AllRecords = (List<usersDTOInter>) engin.findRecordsAlluserManag();
        return AllRecords;
    }

    public Object findAllUnBlockRecords(Object... obj) throws Throwable {
        usersDAOInter engin = DAOFactory.createusersDAO();
        List<usersDTOInter> AllRecords = (List<usersDTOInter>) engin.findAllUnBlockRecords();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        usersDTOInter SearchParameter = (usersDTOInter) obj[0];
        usersDAOInter engin = DAOFactory.createusersDAO();
        usersDTOInter Record = (usersDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
      public Boolean GetRecordpass(Object... obj) throws Throwable {
        usersDTOInter SearchParameter = (usersDTOInter) obj[0];
        String Password = (String) obj[1];
        usersDAOInter engin = DAOFactory.createusersDAO();
        Boolean Record =  engin.findRecordpass(SearchParameter,Password);
        return Record;
    }
//Used In ChangePassword

    public Object GetRecordById(Object... obj) throws Throwable {
        usersDTOInter SearchParameter = (usersDTOInter) obj[0];
        usersDAOInter engin = DAOFactory.createusersDAO();
        usersDTOInter Record = (usersDTOInter) engin.findRecordById(SearchParameter);
        return Record;
    }
//Used In Profile Assign

    public Object GetAllAssignToProfile(Object... obj) throws Throwable {
        Integer SearchParameter = (Integer) obj[0];
        usersDAOInter engin = DAOFactory.createusersDAO();
        List<usersDTOInter> Record = (List<usersDTOInter>) engin.GetAllAssignToProfile(SearchParameter);
        return Record;
    }
//Used In Profile Assign

    public Object GetAllUnAssignToProfile(Object... obj) throws Throwable {

        usersDAOInter engin = DAOFactory.createusersDAO();
        List<usersDTOInter> Record = (List<usersDTOInter>) engin.GetAllUnAssignToProfile();
        return Record;
    }
}
