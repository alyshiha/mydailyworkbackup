/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.LoginDAOInter;
import java.util.Date;

/**
 *
 * @author Administrator
 */
public class LoginBO extends BaseBO implements LoginBOInter {

    public String ValidateLog(String userName, String password, String version, Integer NumOfUsers, Date EndDate) throws Throwable {
        LoginDAOInter LDAO = DAOFactory.createloginDAO();
        String Msg = (String) LDAO.ValidateLog(userName, password, version, NumOfUsers, EndDate);
        return Msg;
    }
}
