/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import java.sql.SQLException;

/**
 *
 * @author AlyShiha
 */
public interface UserNetworkBOInter {

    Object GetListOfRecordsCS(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object insertrecord(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;

}
