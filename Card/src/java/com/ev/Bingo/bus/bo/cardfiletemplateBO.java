package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.cardfiletemplateDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.cardfiletemplateDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.cardfiletemplateDTOInter;
import java.text.SimpleDateFormat;

public class cardfiletemplateBO extends BaseBO implements cardfiletemplateBOInter {

    protected cardfiletemplateBO() {
        super();
    }

    public int insertrecord(Object... obj) throws Throwable {
        cardfiletemplateDTOInter RecordToInsert = (cardfiletemplateDTOInter) obj[0];
        String TableName = (String) obj[1];
        cardfiletemplateDAOInter engin = DAOFactory.createnetworkfiletemplateDAO();
         return engin.insertrecord(RecordToInsert,TableName) ;
          
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<cardfiletemplateDTOInter> entities = (List<cardfiletemplateDTOInter>) obj[0];
        cardfiletemplateDAOInter engin = DAOFactory.createnetworkfiletemplateDAO();
        engin.save(entities);
        return "Insert Done";
    }

    public Object updaterecord(Object... obj) throws Throwable {
        cardfiletemplateDTOInter RecordToUpdate = (cardfiletemplateDTOInter) obj[0];
        String TableName = (String) obj[1];
        cardfiletemplateDAOInter engin = DAOFactory.createnetworkfiletemplateDAO();
        return engin.updaterecord(RecordToUpdate, TableName) + " Record Has been Updated";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        cardfiletemplateDAOInter engin = DAOFactory.createnetworkfiletemplateDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        cardfiletemplateDTOInter RecordToDelete = (cardfiletemplateDTOInter) obj[0];
        String TableName = (String) obj[1];
        cardfiletemplateDAOInter engin = DAOFactory.createnetworkfiletemplateDAO();
        return engin.deleterecord(RecordToDelete, TableName) + " Record Has been Deleted";

    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<cardfiletemplateDTOInter> SearchRecords = (List<cardfiletemplateDTOInter>) obj[0];
        cardfiletemplateDAOInter engin = DAOFactory.createnetworkfiletemplateDAO();
        List<cardfiletemplateDTOInter> AllRecords = (List<cardfiletemplateDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        String SearchRecords = (String) obj[0];
        cardfiletemplateDAOInter engin = DAOFactory.createnetworkfiletemplateDAO();
        List<cardfiletemplateDTOInter> AllRecords = (List<cardfiletemplateDTOInter>) engin.findRecordsAll(SearchRecords);
        return AllRecords;
    }

    public int GetRecord(Object... obj) throws Throwable {
        cardfiletemplateDTOInter SearchParameter = (cardfiletemplateDTOInter) obj[0];
        String TableName = (String) obj[1];
        cardfiletemplateDAOInter engin = DAOFactory.createnetworkfiletemplateDAO();
        return  engin.findRecord(SearchParameter,TableName);
        
    }
}
