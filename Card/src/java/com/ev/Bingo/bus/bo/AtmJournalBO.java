
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import DBCONN.CoonectionHandler;
 
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dao.AtmJournalDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dto.AtmJournalDTOInter;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRReportFont;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignReportFont;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author AlyShiha
 */
public class AtmJournalBO implements AtmJournalBOInter {

    @Override
    public Object printrep8(String user, Date dateF, Date dateT, int atmGroupInt, String customer, String atmId, String AtmName,String indication,Boolean realesed) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("DateFrom", DateFormatter.changeDateFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateFormat(dateT));
        String atmGroup = "";

        atmGroup = "All";

        if ("All".equals(atmId)) {
            atmId = "999999";
        }

        params.put("ATMNAME", AtmName);
        params.put("AtmGroupInt", atmGroupInt);
        params.put("AtmGroup", atmGroup);
        params.put("customer", customer);
        params.put("indication", indication);
        params.put("ATMIDInt", Integer.parseInt(atmId));
        if (realesed) {
            params.put("release", "0");
        } else {
            params.put("release", "8");
        }
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            //Load template to design for tweaking, else load template straight
            // to JasperReport.
            JasperDesign design = JRXmlLoader.load("c:/CardsReports/rep_8.jrxml");

            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);

            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);

            JRReportFont font = new JRDesignReportFont();
            font.setPdfFontName("c:/windows/fonts/arial.ttf");
            font.setPdfEncoding(com.lowagie.text.pdf.BaseFont.IDENTITY_H);
            font.setPdfEmbedded(true);

            Calendar c = Calendar.getInstance();
            String uri;
            uri = "Journal123Visa" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime());
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri + ".pdf");

            try {
                String x = pdfPath + uri + ".pdf";
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public List<AtmJournalDTOInter> getJournalrecord(String AppID, int repid, int export, String FromDate) throws Throwable {
        AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
        List<AtmJournalDTOInter> records = engin.findrecord(AppID, repid, export, FromDate);
        return records;
    }
    
    @Override
    public void recalc(Date selectedDate) throws Throwable {
        AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
        engin.recalc(selectedDate);
    }
        
    @Override
    public void recalc2() throws Throwable {
        AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
        engin.recalc2();
    }

    @Override
    public List<AtmJournalDTOInter> findjournalatm(String dateFrom, String dateTo, String NoOfAtms, int export, int group, String Reversal, int user,String indication,Boolean realesed) throws Throwable {
        AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
        List<AtmJournalDTOInter> records = engin.findrecordjournal(dateFrom, dateTo, NoOfAtms, export, group, Reversal, user,indication,realesed);
        return records;
    }

    @Override
    public List<AtmJournalDTOInter> getJournal() throws Throwable {
        AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
        List<AtmJournalDTOInter> records = engin.findAll();
        return records;
    }

    @Override
    public String insertJournal(AtmJournalDTOInter record) {
        try {
            AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
            engin.insert(record);
            return "Record Has Been Succesfully Added";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String updateJournal(List<AtmJournalDTOInter> record, String flag) {
        try {
            AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
            engin.update(record, flag);
            return "Record Has Been Succesfully Updates";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String deleteJournal(AtmJournalDTOInter record) {
        try {
            AtmJournalDAOInter engin = DAOFactory.createAtmJournalDAO();
            engin.delete(record);
            return "Record Has Been Succesfully Deleted";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }
}
