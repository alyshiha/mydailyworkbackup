/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import DBCONN.CoonectionHandler;
 
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.LogErrorDAOInter;
import com.ev.Bingo.bus.dto.ExceptionLoadingErrorsDTOInter;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRReportFont;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignReportFont;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author AlyShiha
 */
public class LogErrorBO implements LogErrorBOInter {

    @Override
    public Object printExceptionLoadingErrors(String user, Date dateF, Date dateT, String customer) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(dateF));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(dateT));
        params.put("customer", customer);
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design = JRXmlLoader.load("c:/CardsReports/ExceptionLoadingErrors.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            JRReportFont font = new JRDesignReportFont();
            font.setPdfFontName("c:/windows/fonts/arial.ttf");
            font.setPdfEncoding(com.lowagie.text.pdf.BaseFont.IDENTITY_H);
            font.setPdfEmbedded(true);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "ExceptionLoadingErrors" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

    @Override
    public List<ExceptionLoadingErrorsDTOInter> runReportExceptionLoadingErrorsexcel(String dateFrom, String dateTo) throws Throwable {
        LogErrorDAOInter engin = DAOFactory.createLogErrorDAO();
        List<ExceptionLoadingErrorsDTOInter> result = (List<ExceptionLoadingErrorsDTOInter>) engin.findExceptionLoadingErrors(dateFrom, dateTo);
        return result;
    }
}
