/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.usersDTOInter;
import java.util.Date;

/**
 *
 * @author Aly
 */
public interface matchingerrordataBOInter {

    Object GetAllRecords(usersDTOInter UserDTO, Date loadingfrom, Date loadingto, Integer network, Integer recordtypeid) throws Throwable;

    Object GetListOfRecords(Object... obj) throws Throwable;

    void MarkAsDispute(Object... obj) throws Throwable;

    void MarkAsMatched(Object... obj) throws Throwable;

}
