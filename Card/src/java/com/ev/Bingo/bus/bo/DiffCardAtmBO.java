/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.data.DataProviderFactory;
 
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.DiffAmountDAOInter;
import com.ev.Bingo.bus.dao.DiffCardAtmDAOInter;
import com.ev.Bingo.bus.dao.transactionstatisticsrepDAOInter;
import com.ev.Bingo.bus.dto.ATMMACHINECASHDTOINTER;
import com.ev.Bingo.bus.dto.DiffCardAtmJournalDTOInter;
import com.ev.Bingo.bus.dto.DiffCardAtmTransJournalDTOInter;
import com.ev.Bingo.bus.dto.transactionstatisticsrepDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author AlyShiha
 */
public class DiffCardAtmBO implements DiffCardAtmBOInter {

    @Override
    public List<DiffCardAtmJournalDTOInter> findMasterRecords(String ATMID, String INDICATION, String JournalType, Date From, Date TO) throws Throwable {
        DiffCardAtmDAOInter engin = DAOFactory.createDiffCardAtmDAO();
        List<DiffCardAtmJournalDTOInter> records = (List<DiffCardAtmJournalDTOInter>) engin.findMasterRecords(ATMID, INDICATION, JournalType, From, TO);
        return records;
    }
   public void recalc() throws Throwable {
        DiffCardAtmDAOInter engin = DAOFactory.createDiffCardAtmDAO();
         engin.recalc();
    }
    @Override
    public List<DiffCardAtmTransJournalDTOInter> findDetailRecords(String DIFFID) throws Throwable {
        DiffCardAtmDAOInter engin = DAOFactory.createDiffCardAtmDAO();
        List<DiffCardAtmTransJournalDTOInter> records = (List<DiffCardAtmTransJournalDTOInter>) engin.findDetailRecords(DIFFID);
        return records;
    }

    @Override
    public List<ATMMACHINECASHDTOINTER> findMachineAll() throws Throwable {
        DiffCardAtmDAOInter engin = DAOFactory.createDiffCardAtmDAO();
        List<ATMMACHINECASHDTOINTER> records = (List<ATMMACHINECASHDTOINTER>) engin.findMachineAll();
        return records;
    }

    public String exportexcel(DiffCardAtmJournalDTOInter[] SearchRecords) throws Throwable {
        Object[] Space = new Object[1];
        List<String> coloumns = new ArrayList<String>();
        coloumns.add("Card Amount");
        coloumns.add("ATM Amount");
        coloumns.add("Journal Type");
        coloumns.add("Diff Amount");
        coloumns.add("ATM ID");
        coloumns.add("Journal Date");
        coloumns.add("ATM From Date");
        coloumns.add("ATM To Date");
        coloumns.add("CR.Card Account");
        coloumns.add("DR.ATM Account");
        coloumns.add("Indication");
        coloumns.add("Branch");
        coloumns.add("Diff Date");
        List<String> subbcolumns = new ArrayList<String>();
        subbcolumns.add("Card Num");
        subbcolumns.add("Network ID");
        subbcolumns.add("Transaction Sequence");
        subbcolumns.add("Transaction Status");
        subbcolumns.add("Transaction Type");
        subbcolumns.add("ATM ID");
        subbcolumns.add("Transaction Date");
        subbcolumns.add("Settlement Date");
        subbcolumns.add("Response Code");
        subbcolumns.add("Customer Account Number");
        subbcolumns.add("Reference No");
        subbcolumns.add("Process Code");
        subbcolumns.add("Amount");
        subbcolumns.add("Settlement Amount");
        subbcolumns.add("Currency");
        subbcolumns.add("ACQ");
        subbcolumns.add("Issuer");
        subbcolumns.add("Trans. Side");

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("CardATMDiff");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        int index = 0;

        for (DiffCardAtmJournalDTOInter Records : SearchRecords) {

            Object[] coloumnslist = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                coloumnslist[i] = coloumns.get(i);
                sheet.setDefaultColumnWidth(20);
            }
            data.put(index, coloumnslist);
            index = index + 1;

            Object[] coloumnsData = new Object[coloumns.size()];
            for (int i = 0; i < coloumns.size(); i++) {
                if ("Card Amount".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getCardamount()).toString().replace("null", " ");
                }
                if ("ATM Amount".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getAtmamount()).toString().replace("null", "0");
                }
                if ("Journal Type".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getJournaltype()).toString().replace("null", "  ");
                }
                if ("ATM ID".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getAtmid()).toString().replace("null", " ");
                }
                if ("Journal Date".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getJournaldate()).toString().replace("null", " ");
                }
                 if ("Diff Amount".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDiffamount()).toString().replace("null", " ");
                }
                if ("ATM From Date".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getAtmfromdate()).toString().replace("null", " ");
                }
                if ("ATM To Date".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getAtmtodate()).toString().replace("null", " ");
                }
                if ("CR.Card Account".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getCreditaccount()).toString().replace("null", " ");
                }
                if ("DR.ATM Account".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDebitaccount()).toString().replace("null", " ");
                }
                if ("Indication".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getIndication()).toString().replace("null", " ");
                }
                if ("Branch".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getBranch()).toString().replace("null", " ");
                }
                if ("Diff Date".equals(coloumns.get(i))) {
                    coloumnsData[i] = String.valueOf(Records.getDiffdate()).toString().replace("null", " ");
                }
            }
            data.put(index, coloumnsData);
            index = index + 1;

            List<DiffCardAtmTransJournalDTOInter> SubRecords = new ArrayList<DiffCardAtmTransJournalDTOInter>();
            SubRecords = (List<DiffCardAtmTransJournalDTOInter>) findDetailRecords("" + Records.getDiffid());

            if (SubRecords.size() > 0) {

                Object[] subcoloumnslist = new Object[subbcolumns.size()];
                for (int i = 0; i < subbcolumns.size(); i++) {
                    subcoloumnslist[i] = subbcolumns.get(i);
                    sheet.setDefaultColumnWidth(20);
                }
                data.put(index, subcoloumnslist);
                index = index + 1;

                for (DiffCardAtmTransJournalDTOInter subRecord : SubRecords) {

                    Object[] subcoloumnsData = new Object[subbcolumns.size()];
                    for (int i = 0; i < subbcolumns.size(); i++) {
                        if ("Card Num".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getCardno()).toString().replace("null", " ");
                        }
                        if ("Network ID".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getNetworkid()).toString().replace("null", " ");
                        }
                        if ("Transaction Sequence".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransactionsequence()).toString().replace("null", " ");
                        }
                        if ("Transaction Status".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransactionstatus()).toString().replace("null", " ");
                        }
                        if ("Transaction Type".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransactiontype()).toString().replace("null", " ");
                        }
                        if ("ATM ID".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getAtmid()).toString().replace("null", " ");
                        }
                        if ("Transaction Date".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransactiondate()).toString().replace("null", " ");
                        }
                        if ("Settlement Date".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getSettlementdate()).toString().replace("null", " ");
                        }
                        if ("Response Code".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getResponsecode()).toString().replace("null", " ");
                        }
                        if ("Customer Account Number".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getCustomeraccountnumber()).toString().replace("null", " ");
                        }
                        if ("Reference No".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getReferenceno()).toString().replace("null", " ");
                        }
                        if ("Process Code".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getProcesscode()).toString().replace("null", " ");
                        }
                        if ("Amount".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getAmount()).toString().replace("null", " ");
                        }
                        if ("Settlement Amount".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getSettlementamount()).toString().replace("null", " ");
                        }
                        if ("Currency".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getCurrency()).toString().replace("null", " ");
                        }
                        if ("ACQ".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getAcq()).toString().replace("null", " ");
                        }
                        if ("Issuer".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getIssuer()).toString().replace("null", " ");
                        }
                        if ("Trans. Side".equals(subbcolumns.get(i))) {
                            subcoloumnsData[i] = String.valueOf(subRecord.getTransside()).toString().replace("null", " ");
                        }
                    }
                    data.put(index, subcoloumnsData);
                    index = index + 1;
                }

            }
            data.put(index, Space);
            index = index + 1;
        }
        List<Integer> keyset = new ArrayList<Integer>(data.keySet());
        Collections.sort(keyset);
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            Calendar c = Calendar.getInstance();
            String uri = "ATMCardDiff" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".xls";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            pdfPath = pdfPath + uri;
            FileOutputStream out = new FileOutputStream(new File(pdfPath));
            workbook.write(out);
            out.close();
            return pdfPath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public Object GetReport(String user, Date DateFrom, Date DateTo, String customer, String ATMID, String Journal, String Indication, DiffCardAtmJournalDTOInter[] entities) throws Throwable {
        DiffCardAtmDAOInter engin = DAOFactory.createDiffCardAtmDAO();
        Integer index = engin.SaveRecordPrint(entities);
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        params.put("customer", customer);
        params.put("DateFrom", DateFormatter.changeDateFormat(DateFrom));
        params.put("DateTo", DateFormatter.changeDateFormat(DateTo));
        params.put("ATM ID", ATMID);
        params.put("Indication", Indication);
        params.put("SelectedItem", index);
        params.put("journaltype", Journal);
Connection connection = CoonectionHandler.getInstance().getConnection();
        
        try {
            JasperDesign design = JRXmlLoader.load("C:/CardsReports/AtmCardDiff.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, connection);
            Calendar c = Calendar.getInstance();
            String uri = "PDFStatistics" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {

                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(connection);
                engin.DeleteRecordPrint(index);
                return x;
            } catch (Exception ex) {
                engin.DeleteRecordPrint(index);
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            engin.DeleteRecordPrint(index);
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }

    }

}
