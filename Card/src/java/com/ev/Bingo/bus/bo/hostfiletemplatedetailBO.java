package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.hostfiletemplatedetailDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.hostfiletemplatedetailDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.hostfiletemplatedetailDTOInter;
import java.text.SimpleDateFormat;

public class hostfiletemplatedetailBO extends BaseBO implements hostfiletemplatedetailBOInter {

    protected hostfiletemplatedetailBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        hostfiletemplatedetailDTOInter RecordToInsert = (hostfiletemplatedetailDTOInter) obj[0];
        hostfiletemplatedetailDAOInter engin = DAOFactory.createhostfiletemplatedetailDAO();
        engin.insertrecord(RecordToInsert);
        return "Insert Done";
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<hostfiletemplatedetailDTOInter> entities = (List<hostfiletemplatedetailDTOInter>) obj[0];
        hostfiletemplatedetailDAOInter engin = DAOFactory.createhostfiletemplatedetailDAO();
        engin.save(entities);
        return "Insert Done";
    }

    public Object updaterecord(Object... obj) throws Throwable {
        hostfiletemplatedetailDTOInter RecordToUpdate = (hostfiletemplatedetailDTOInter) obj[0];
        hostfiletemplatedetailDAOInter engin = DAOFactory.createhostfiletemplatedetailDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        hostfiletemplatedetailDAOInter engin = DAOFactory.createhostfiletemplatedetailDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        hostfiletemplatedetailDTOInter RecordToDelete = (hostfiletemplatedetailDTOInter) obj[0];
        hostfiletemplatedetailDAOInter engin = DAOFactory.createhostfiletemplatedetailDAO();
        engin.deleterecord(RecordToDelete);
        return "record has been deleted";
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<hostfiletemplatedetailDTOInter> SearchRecords = (List<hostfiletemplatedetailDTOInter>) obj[0];
        hostfiletemplatedetailDAOInter engin = DAOFactory.createhostfiletemplatedetailDAO();
        List<hostfiletemplatedetailDTOInter> AllRecords = (List<hostfiletemplatedetailDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        hostfiletemplatedetailDAOInter engin = DAOFactory.createhostfiletemplatedetailDAO();
        List<hostfiletemplatedetailDTOInter> AllRecords = (List<hostfiletemplatedetailDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        hostfiletemplatedetailDTOInter SearchParameter = (hostfiletemplatedetailDTOInter) obj[0];
        hostfiletemplatedetailDAOInter engin = DAOFactory.createhostfiletemplatedetailDAO();
        hostfiletemplatedetailDTOInter Record = (hostfiletemplatedetailDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
