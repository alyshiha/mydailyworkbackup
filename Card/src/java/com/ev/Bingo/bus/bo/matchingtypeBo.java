package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.matchingtypeDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.matchingtypeDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.matchingtypeDTOInter;
import com.ev.Bingo.bus.dto.matchingtypenetworksDTOInter;
import com.ev.Bingo.bus.dto.matchingtypesettingDTOInter;
import java.text.SimpleDateFormat;

public class matchingtypeBo extends BaseBO implements matchingtypeBoInter {

    protected matchingtypeBo() {
        super();
    }

    public Object RecreateIndex() throws Throwable {
        matchingtypeDAOInter mlDAO = DAOFactory.creatematchingtypeDAO();
        return mlDAO.RecreateIndex();
    }

    @Override
    public Object findAllmatchingtype(Object... obj) throws Throwable {
        matchingtypeDAOInter engin = DAOFactory.creatematchingtypeDAO();
        List<matchingtypeDTOInter> AllRecords = (List<matchingtypeDTOInter>) engin.findAllmatchingtype();
        return AllRecords;
    }

    @Override
    public Object updaterecordmatchingtype(Object... obj) throws Throwable {
        matchingtypeDTOInter RecordToUpdate = (matchingtypeDTOInter) obj[0];
        matchingtypeDAOInter engin = DAOFactory.creatematchingtypeDAO();
        return engin.updaterecordmatchingtype(RecordToUpdate);
    }

    @Override
    public Object findRecordsListmatchingtypenetworks(Object... obj) throws Throwable {
        matchingtypeDTOInter RecordToSelect = (matchingtypeDTOInter) obj[0];
        matchingtypeDAOInter engin = DAOFactory.creatematchingtypeDAO();
        List<matchingtypenetworksDTOInter> AllRecords = (List<matchingtypenetworksDTOInter>) engin.findRecordsListmatchingtypenetworks(RecordToSelect);
        return AllRecords;
    }

    @Override
    public Object deleterecordmatchingtypenetworks(Object... obj) throws Throwable {
        matchingtypenetworksDTOInter RecordToDelete = (matchingtypenetworksDTOInter) obj[0];
        matchingtypeDAOInter engin = DAOFactory.creatematchingtypeDAO();
        return engin.deleterecordmatchingtypenetworks(RecordToDelete);
    }

    @Override
    public Object insertrecordmatchingtypenetworks(Object... obj) throws Throwable {
        matchingtypenetworksDTOInter RecordToInsert = (matchingtypenetworksDTOInter) obj[0];
        matchingtypeDAOInter engin = DAOFactory.creatematchingtypeDAO();
        return engin.insertrecordmatchingtypenetworks(RecordToInsert);
    }

    @Override
    public Object savematchingtypenetworks(Object... obj) throws SQLException, Throwable {
        List<matchingtypenetworksDTOInter> entities = (List<matchingtypenetworksDTOInter>) obj[0];
        matchingtypeDAOInter engin = DAOFactory.creatematchingtypeDAO();
        return engin.savematchingtypenetworks(entities);
    }

    @Override
    public Object findRecordsListmatchingtypesetting(Object... obj) throws Throwable {
        matchingtypenetworksDTOInter SearchRecords = (matchingtypenetworksDTOInter) obj[0];
        matchingtypeDAOInter engin = DAOFactory.creatematchingtypeDAO();
        List<matchingtypesettingDTOInter> AllRecords = (List<matchingtypesettingDTOInter>) engin.findRecordsListmatchingtypesetting(SearchRecords);
        return AllRecords;
    }

    @Override
    public Object deleterecordmatchingtypesetting(Object... obj) throws Throwable {
        matchingtypesettingDTOInter RecordToDelete = (matchingtypesettingDTOInter) obj[0];
        matchingtypeDAOInter engin = DAOFactory.creatematchingtypeDAO();
        return engin.deleterecordmatchingtypesetting(RecordToDelete);
    }

    @Override
    public Object insertrecordmatchingtypesetting(Object... obj) throws Throwable {
        matchingtypesettingDTOInter RecordToInsert = (matchingtypesettingDTOInter) obj[0];
        matchingtypeDAOInter engin = DAOFactory.creatematchingtypeDAO();
        return engin.insertrecordmatchingtypesetting(RecordToInsert);
    }

    @Override
    public Object savematchingtypesetting(Object... obj) throws SQLException, Throwable {
        List<matchingtypesettingDTOInter> entities = (List<matchingtypesettingDTOInter>) obj[0];
        matchingtypeDAOInter engin = DAOFactory.creatematchingtypeDAO();
        return engin.savematchingtypesetting(entities);
    }
}
