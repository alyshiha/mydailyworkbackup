package com.ev.Bingo.bus.bo;

import java.util.Date;
import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.atmfileDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.atmfileDTOInter;

public class atmfileBO extends BaseBO implements atmfileBOInter {

    protected atmfileBO() {
        super();
    }

    public Object GetListOfRecordsSearch(Object... obj) throws Throwable {
        Date loadingfrom = (Date) obj[0];
        Date loadingto = (Date) obj[1];
        Integer FileID = (Integer) obj[2];
        atmfileDAOInter engin = DAOFactory.createatmfileDAO();
        List<atmfileDTOInter> AllRecords = (List<atmfileDTOInter>) engin.findRecordsListSearch(loadingfrom, loadingto, FileID);
        return AllRecords;
    }

}
