package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.networkcodesDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.networkcodesDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.networkcodesDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.text.SimpleDateFormat;

public class networkcodesBO extends BaseBO implements networkcodesBOInter {

    protected networkcodesBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        networkcodesDTOInter RecordToInsert = (networkcodesDTOInter) obj[0];
        networkcodesDAOInter engin = DAOFactory.createnetworkcodesDAO();
        String msg = (String) engin.insertrecord(RecordToInsert);
        return msg;
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<networkcodesDTOInter> entities = (List<networkcodesDTOInter>) obj[0];
        networkcodesDAOInter engin = DAOFactory.createnetworkcodesDAO();
        String msg = (String) engin.save(entities);
        return msg;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        networksDTOInter RecordToDelete = (networksDTOInter) obj[0];
        networkcodesDAOInter engin = DAOFactory.createnetworkcodesDAO();
        String msg = (String) engin.deleterecord(RecordToDelete);
        return msg + " Detailed Network Has Been Deleted";
    }

    public Object deleterecorddetail(Object... obj) throws Throwable {
        networkcodesDTOInter RecordToDelete = (networkcodesDTOInter) obj[0];
        networkcodesDAOInter engin = DAOFactory.createnetworkcodesDAO();
        String msg = (String) engin.deleterecorddetail(RecordToDelete);
        return msg + "Detailed Network Has Been Deleted";
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        networksDTOInter SearchRecords = (networksDTOInter) obj[0];
        networkcodesDAOInter engin = DAOFactory.createnetworkcodesDAO();
        List<networkcodesDTOInter> AllRecords = (List<networkcodesDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

}
