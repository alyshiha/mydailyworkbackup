package com.ev.Bingo.bus.bo;

import java.sql.SQLException;

public interface columnssetupdetailsBOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object deleterecordDetail(Object... obj) throws Throwable;

    Object GetListOfRecordsCS(Object... obj) throws Throwable;

}
