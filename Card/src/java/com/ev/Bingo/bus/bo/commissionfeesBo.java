package com.ev.Bingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.bo.BaseBO;

import java.sql.SQLException;
import com.ev.Bingo.bus.dao.commissionfeesDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;

import com.ev.Bingo.bus.dto.CommFeeReportDTOInter;

import java.util.List;
import com.ev.Bingo.bus.dto.commissionfeesDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class commissionfeesBo extends BaseBO implements commissionfeesBOInter {

    protected commissionfeesBo() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        commissionfeesDTOInter RecordToInsert = (commissionfeesDTOInter) obj[0];
        commissionfeesDAOInter engin = DAOFactory.createcommissionfeesDAO();
        return engin.insertrecord(RecordToInsert);

    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<commissionfeesDTOInter> entities = (List<commissionfeesDTOInter>) obj[0];
        commissionfeesDAOInter engin = DAOFactory.createcommissionfeesDAO();
        return engin.save(entities);

    }

    public Object updaterecord(Object... obj) throws Throwable {
        commissionfeesDTOInter RecordToUpdate = (commissionfeesDTOInter) obj[0];
        commissionfeesDAOInter engin = DAOFactory.createcommissionfeesDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        commissionfeesDAOInter engin = DAOFactory.createcommissionfeesDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        commissionfeesDTOInter RecordToDelete = (commissionfeesDTOInter) obj[0];
        commissionfeesDAOInter engin = DAOFactory.createcommissionfeesDAO();
        return engin.deleterecord(RecordToDelete);

    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        networksDTOInter SearchRecords = (networksDTOInter) obj[0];
        commissionfeesDAOInter engin = DAOFactory.createcommissionfeesDAO();
        List<commissionfeesDTOInter> AllRecords = (List<commissionfeesDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        commissionfeesDAOInter engin = DAOFactory.createcommissionfeesDAO();
        Date fromdate = (Date) obj[0];
        Date todate = (Date) obj[1];
        Integer network = (Integer) obj[2];
        String pin = (String) obj[3];
        Integer user = (Integer) obj[4];
        Boolean showpin = (Boolean) obj[5];
        List<CommFeeReportDTOInter> AllRecords = (List<CommFeeReportDTOInter>) engin.findRecordsAll(fromdate, todate, network, pin, user, showpin);
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        commissionfeesDTOInter SearchParameter = (commissionfeesDTOInter) obj[0];
        commissionfeesDAOInter engin = DAOFactory.createcommissionfeesDAO();
        commissionfeesDTOInter Record = (commissionfeesDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }

    public Object GetReport(Object... obj) throws Throwable {

        Date fromdate = (Date) obj[0];
        Date todate = (Date) obj[1];
        Integer network = (Integer) obj[2];
        String pin = (String) obj[3];
        Integer user = (Integer) obj[4];
        Boolean showpin = (Boolean) obj[5];
        String userName = (String) obj[6];
        String customer = (String) obj[7];
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        //JRResultSetDataSource resultSetDataSource;
        params.put("Title", "Commisions And Fees");
        commissionfeesDAOInter CommFeeDAOEngin = DAOFactory.createcommissionfeesDAO();
        ///ResultSet rs = (ResultSet) CommFeeDAOEngin.findRecordsAllResultSet(fromdate, todate, network, pin, user, showpin);
        //resultSetDataSource = new JRResultSetDataSource(rs);
        params.put("user", userName);
        params.put("customer", customer);
        params.put("dateF", DateFormatter.changeDateAndTimeFormat(fromdate));
        params.put("dateT", DateFormatter.changeDateAndTimeFormat(todate));
        params.put("network", network);
        params.put("pin", pin);
        params.put("userid", user);
        if (showpin) {
            params.put("showpin", 1);
        } else {
            params.put("showpin", 2);
        }
        //params.put("param", param);
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            if (showpin) {
                design = JRXmlLoader.load("C:/CardsReports/CommisionAndFees.jrxml");
            } else {
                design = JRXmlLoader.load("C:/CardsReports/CommisionAndFeesWithoutPin.jrxml");
            }
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri = "CommisionsAndFees" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {

                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }

    }

}
