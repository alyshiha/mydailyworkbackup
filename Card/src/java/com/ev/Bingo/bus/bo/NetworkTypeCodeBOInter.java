/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.NetworkTypeCodeDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface NetworkTypeCodeBOInter {

    List<NetworkTypeCodeDTOInter> getNetworkTypeRecord(int network, String code) throws Throwable;
    
    Object printPDFrep(String customer, String PrintedName, int network, String networksearch, String networkvalue) throws Throwable ;

    String deleteNetworkType(NetworkTypeCodeDTOInter record);

    List<NetworkTypeCodeDTOInter> getNetworkType() throws Throwable;

    String insertNetworkType(NetworkTypeCodeDTOInter record);

    String updateNetworkType(NetworkTypeCodeDTOInter record);

}
