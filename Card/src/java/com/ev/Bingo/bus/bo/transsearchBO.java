/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.bo.BaseBO;
import com.ev.Bingo.base.data.DataProviderFactory;

import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.transsearchDAOInter;
import com.ev.Bingo.bus.dto.TranSearchFieldDTOInter;
import com.ev.Bingo.bus.dto.currencymasterDTOInter;
import com.ev.Bingo.bus.dto.disputesDTOInter;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import com.ev.Bingo.bus.dto.transactionresponsecodeDTOInter;
import com.ev.Bingo.bus.dto.transactiontypeDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import com.ev.Bingo.bus.dto.validationrulesDTOInter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author Aly
 */
public class transsearchBO extends BaseBO implements transsearchBOInter {

    public String getcurencybyid(List<currencymasterDTOInter> currencylist, Integer ID) {
        String Result = "";
        for (currencymasterDTOInter curr : currencylist) {
            if (curr.getid() == ID) {
                return curr.getsymbol();
            }
        }
        return Result;
    }

    public String getrecordid(String ID) {
        String record = ID;
        if ("1".equals(ID)) {
            record = "Network";
        } else if ("2".equals(ID)) {
            record = "Switch";
        } else if ("3".equals(ID)) {
            record = "Host";
        }
        return record;
    }

    public String getusersbyid(List<usersDTOInter> userslist, Integer ID) {
        String Result = "";
        for (usersDTOInter user : userslist) {
            if (user.getUserid()== ID) {
                return user.getUsername();
            }
        }
        return Result;
    }

    public String gettranstypeid(List<transactiontypeDTOInter> transtypelist, Integer ID) {
        String Result = "";
        for (transactiontypeDTOInter Trans : transtypelist) {
            if (Trans.getid() == ID) {
                return Trans.getname();
            }
        }
        return Result;
    }

    public String getvalidationid(List<validationrulesDTOInter> validationrulelist, Integer ID) {
        String Result = "";
        for (validationrulesDTOInter rule : validationrulelist) {
            if (rule.getid() == ID) {
                return rule.getname();
            }
        }
        return Result;
    }

    public String getrespid(List<transactionresponsecodeDTOInter> resplist, Integer ID) {
        String Result = "";
        for (transactionresponsecodeDTOInter Trans : resplist) {
            if (Trans.getid() == ID) {
                return Trans.getname();
            }
        }
        return Result;
    }

    public String getnetworkbyid(List<networksDTOInter> networklist, Integer ID) {
        String Result = "";
        for (networksDTOInter network : networklist) {
            if (network.getid() == ID) {
                return network.getname();
            }
        }
        return Result;
    }

    public List<filecolumndefinitionDTOInter> getcolumnselect(String[] Selectedcol, List<filecolumndefinitionDTOInter> coloumns) {
        List<filecolumndefinitionDTOInter> Result = new ArrayList<filecolumndefinitionDTOInter>();
        for (int i = 0; i < Selectedcol.length; i++) {
            for (int j = 0; j < coloumns.size(); j++) {
                if (Selectedcol[i].toUpperCase().toString().equals(coloumns.get(j).getcolumnname().toUpperCase().toString())) {
                    filecolumndefinitionDTOInter Record = (filecolumndefinitionDTOInter) coloumns.get(j);
                    Record.setColumnvariable(coloumns.get(j).getcolumnname().toLowerCase().toString().replace("_", ""));
                    Result.add(Record);
                    break;
                }
            }
        }
        return Result;
    }

    public Boolean ValidateDates(TranSearchFieldDTOInter searchfields, String Page) throws Throwable {
        Boolean Valid = Boolean.FALSE;
        if ("Between".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null && searchfields.getTransdateto() != null && searchfields.getTransdateto() != null) {
                Valid = Boolean.TRUE;
            }
        } else if ("<>".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null) {
                Valid = Boolean.TRUE;
            }
        } else if ("<".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null) {
                Valid = Boolean.TRUE;
            }
        } else if (">".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null) {
                Valid = Boolean.TRUE;
            }
        } else if (">=".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null) {
                Valid = Boolean.TRUE;
            }
        } else if ("<=".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null) {
                Valid = Boolean.TRUE;
            }
        } else if ("=".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null) {
                Valid = Boolean.TRUE;
            }
        }
        if (!"R".equals(Page)) {
            if ("Between".equals(searchfields.getSetteltdateoperator())) {
                if (searchfields.getSetteltdatefrom() != null && searchfields.getSetteltdateto() != null) {
                    Valid = Boolean.TRUE;
                }
            } else if ("<>".equals(searchfields.getSetteltdateoperator())) {
                if (searchfields.getSetteltdatefrom() != null) {
                    Valid = Boolean.TRUE;
                }
            } else if ("<".equals(searchfields.getSetteltdateoperator())) {
                if (searchfields.getSetteltdatefrom() != null) {
                    Valid = Boolean.TRUE;
                }
            } else if (">".equals(searchfields.getSetteltdateoperator())) {
                if (searchfields.getSetteltdatefrom() != null) {
                    Valid = Boolean.TRUE;
                }
            } else if (">=".equals(searchfields.getSetteltdateoperator())) {
                if (searchfields.getSetteltdatefrom() != null) {
                    Valid = Boolean.TRUE;
                }
            } else if ("<=".equals(searchfields.getSetteltdateoperator())) {
                if (searchfields.getSetteltdatefrom() != null) {
                    Valid = Boolean.TRUE;
                }
            } else if ("=".equals(searchfields.getSetteltdateoperator())) {
                if (searchfields.getSetteltdatefrom() != null) {
                    Valid = Boolean.TRUE;
                }
            }
        }
        if ("Between".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null && searchfields.getLoadingdateto() != null) {
                Valid = Boolean.TRUE;
            }
        } else if ("<>".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null) {
                Valid = Boolean.TRUE;
            }
        } else if ("<".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null) {
                Valid = Boolean.TRUE;
            }
        } else if (">".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null) {
                Valid = Boolean.TRUE;
            }
        } else if (">=".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null) {
                Valid = Boolean.TRUE;
            }
        } else if ("<=".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null) {
                Valid = Boolean.TRUE;
            }
        } else if ("=".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null) {
                Valid = Boolean.TRUE;
            }
        }
        return Valid;
    }

    @Override
    public String[] WhereCondition(TranSearchFieldDTOInter searchfields, List<currencymasterDTOInter> currencys, List<usersDTOInter> userslist, List<transactiontypeDTOInter> transtypelist, List<transactionresponsecodeDTOInter> resplist, List<networksDTOInter> networklist, String[] selectedCol, String Page, usersDTOInter UserDTO, List<validationrulesDTOInter> validationrulelist) throws Throwable {
        String WhereCondition = " where 1 = 1 and";
        String Searchcriertia = "";
        String SortBy = "";
        String returnvalues[] = new String[4];
        if (!"R".equals(Page)) {
            if (!"V".equals(Page)) {
                WhereCondition = WhereCondition + super.GetDecryptLinc() + " = TRANSACTION_ID";
                if (!"".equals(searchfields.getMatchingtypeid()) && searchfields.getMatchingtypeid() != null) {
                    if ("Network - Switch".equals(searchfields.getMatchingtypeid())) {
                        WhereCondition += " and matching_type = 1";
                        Searchcriertia = Searchcriertia + " Matching Type = Network - Switch";
                    } else if ("Switch - Host".equals(searchfields.getMatchingtypeid())) {
                        WhereCondition += " and matching_type = 2";
                        Searchcriertia = Searchcriertia + " Matching Type = Switch - Host";
                    }
                }
            }
        }

        if (!"R".equals(Page)) {
            if (!"V".equals(Page)) {
                if (!"L".equals(Page)) {
                    WhereCondition += " and network_id in (select un.networkid from user_network un where un.userid = $user) ";
                    WhereCondition = WhereCondition.replace("$user", "" + UserDTO.getUserid());
                    if (!"".equals(searchfields.getMasterrecordid()) && searchfields.getMasterrecordid() != null) {
                        if ("Network".equals(searchfields.getMasterrecordid())) {
                            WhereCondition += " and record_type = 1";
                            Searchcriertia = Searchcriertia + " Record Type = Network";
                        } else if ("Switch".equals(searchfields.getMasterrecordid())) {
                            WhereCondition += " and record_type = 2";
                            Searchcriertia = Searchcriertia + " Record Type = Switch";
                        } else if ("Host".equals(searchfields.getMasterrecordid())) {
                            WhereCondition += " and record_type = 3";
                            Searchcriertia = Searchcriertia + " Record Type = Host";
                        }
                    }
                }
            }
        }
        if ("Between".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null && searchfields.getTransdateto() != null && searchfields.getTransdateto() != null) {
                WhereCondition += " and transaction_date between to_date(to_char(nvl(to_date('$col1','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss') and "
                        + "to_date(to_char(nvl(to_date('$col2','dd.mm.yyyy hh24:mi:ss'),sysdate-10000),"
                        + "'dd.mm.yyyy hh24:mi:ss'),'dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom()));
                WhereCondition = WhereCondition.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdateto()));
                Searchcriertia = Searchcriertia + " Transaction Time Between " + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom()) + " & " + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdateto());
            }
        } else if ("<>".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null) {
                WhereCondition += " and transaction_date <> to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom()));
                Searchcriertia = Searchcriertia + " Transaction Time Not = " + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom());
            }
        } else if ("<".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null) {
                WhereCondition += " and transaction_date < to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom()));
                Searchcriertia = Searchcriertia + " Transaction Time Smaller Than " + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom());
            }
        } else if (">".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null) {
                WhereCondition += " and transaction_date > to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom()));
                Searchcriertia = Searchcriertia + " Transaction Time Greater Than " + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom());
            }
        } else if (">=".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null) {
                WhereCondition += " and transaction_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom()));
                Searchcriertia = Searchcriertia + " Transaction Time > Or = " + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom());
            }
        } else if ("<=".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null) {
                WhereCondition += " and transaction_date <= to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom()));
                Searchcriertia = Searchcriertia + " Transaction Time < Or = " + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom());
            }
        } else if ("=".equals(searchfields.getTransdateoperator())) {
            if (searchfields.getTransdatefrom() != null && searchfields.getTransdatefrom() != null) {
                WhereCondition += " and transaction_date = to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom()));
                Searchcriertia = Searchcriertia + " Transaction Time = " + DateFormatter.changeDateAndTimeFormat(searchfields.getTransdatefrom());
            }
        }
        if (!"R".equals(Page)) {
            if (!"V".equals(Page)) {
                if ("Between".equals(searchfields.getSetteltdateoperator())) {
                    if (searchfields.getSetteltdatefrom() != null && searchfields.getSetteltdateto() != null) {
                        WhereCondition += " and settlement_date between to_date('$col1','dd.mm.yyyy hh24:mi:ss') and "
                                + "to_date('$col2','dd.mm.yyyy hh24:mi:ss')";
                        WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom()));
                        WhereCondition = WhereCondition.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdateto()));
                        Searchcriertia = Searchcriertia + " Settlement Time Between " + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom()) + " & " + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdateto());
                    }
                } else if ("<>".equals(searchfields.getSetteltdateoperator())) {
                    if (searchfields.getSetteltdatefrom() != null) {
                        WhereCondition += " and settlement_date <> to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                        WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom()));
                        Searchcriertia = Searchcriertia + " Settlement Time != " + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom());
                    }
                } else if ("<".equals(searchfields.getSetteltdateoperator())) {
                    if (searchfields.getSetteltdatefrom() != null) {
                        WhereCondition += " and settlement_date < to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                        WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom()));
                        Searchcriertia = Searchcriertia + " Settlement Time < " + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom());
                    }
                } else if (">".equals(searchfields.getSetteltdateoperator())) {
                    if (searchfields.getSetteltdatefrom() != null) {
                        WhereCondition += " and settlement_date > to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                        WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom()));
                        Searchcriertia = Searchcriertia + " Settlement Time > " + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom());
                    }
                } else if (">=".equals(searchfields.getSetteltdateoperator())) {
                    if (searchfields.getSetteltdatefrom() != null) {
                        WhereCondition += " and settlement_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                        WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom()));
                        Searchcriertia = Searchcriertia + " Settlement Time > Or = " + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom());
                    }
                } else if ("<=".equals(searchfields.getSetteltdateoperator())) {
                    if (searchfields.getSetteltdatefrom() != null) {
                        WhereCondition += " and settlement_date <= to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                        WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom()));
                        Searchcriertia = Searchcriertia + " Settlement Time < Or = " + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom());
                    }
                } else if ("=".equals(searchfields.getSetteltdateoperator())) {
                    if (searchfields.getSetteltdatefrom() != null) {
                        WhereCondition += " and settlement_date = to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                        WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom()));
                        Searchcriertia = Searchcriertia + " Settlement Time = " + DateFormatter.changeDateAndTimeFormat(searchfields.getSetteltdatefrom());
                    }
                }
            }
        }
        if ("Between".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null && searchfields.getLoadingdateto() != null) {
                WhereCondition += " and loading_date between to_date('$col1','dd.mm.yyyy hh24:mi:ss') and "
                        + "to_date('$col2','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom()));
                WhereCondition = WhereCondition.replace("$col2", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdateto()));
                Searchcriertia = Searchcriertia + " Loading Time Between " + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom()) + " & " + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdateto());
            }
        } else if ("<>".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null) {
                WhereCondition += " and loading_date <> to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom()));
                Searchcriertia = Searchcriertia + " Loading Time != " + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom());
            }
        } else if ("<".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null) {
                WhereCondition += " and loading_date < to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom()));
                Searchcriertia = Searchcriertia + " Loading Time < " + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom());
            }
        } else if (">".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null) {
                WhereCondition += " and loading_date > to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom()));
                Searchcriertia = Searchcriertia + " Loading Time > " + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom());
            }
        } else if (">=".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null) {
                WhereCondition += " and loading_date >= to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom()));
                Searchcriertia = Searchcriertia + " Loading Time >= " + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom());
            }
        } else if ("<=".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null) {
                WhereCondition += " and loading_date <= to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom()));
                Searchcriertia = Searchcriertia + " Loading Time < or = " + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom());
            }
        } else if ("=".equals(searchfields.getLoadingdateoperator())) {
            if (searchfields.getLoadingdatefrom() != null) {
                WhereCondition += " and loading_date = to_date('$col1','dd.mm.yyyy hh24:mi:ss')";
                WhereCondition = WhereCondition.replace("$col1", "" + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom()));
                Searchcriertia = Searchcriertia + " Loading Time = " + DateFormatter.changeDateAndTimeFormat(searchfields.getLoadingdatefrom());
            }
        }
        if ("Between".equals(searchfields.getAmountoperator())) {
            if (searchfields.getAmountfrom() != null && !"".equals(searchfields.getAmountfrom()) && searchfields.getAmountto() != null && !"".equals(searchfields.getAmountto())) {
                WhereCondition += "  and amount between nvl($col1,0) and $col2";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAmountfrom());
                WhereCondition = WhereCondition.replace("$col2", "" + searchfields.getAmountto());
                Searchcriertia = Searchcriertia + " Amount Between " + searchfields.getAmountfrom() + " & " + searchfields.getAmountto();
            }
        } else if ("<>".equals(searchfields.getAmountoperator())) {
            if (searchfields.getAmountfrom() != null && !"".equals(searchfields.getAmountfrom())) {
                WhereCondition += " and amount <> $col1";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAmountfrom());
                Searchcriertia = Searchcriertia + " Amount != " + searchfields.getAmountfrom();
            }
        } else if ("<".equals(searchfields.getAmountoperator())) {
            if (searchfields.getAmountfrom() != null && !"".equals(searchfields.getAmountfrom())) {
                WhereCondition += " and amount < $col1";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAmountfrom());
                Searchcriertia = Searchcriertia + " Amount < " + searchfields.getAmountfrom();
            }
        } else if (">".equals(searchfields.getAmountoperator())) {
            if (searchfields.getAmountfrom() != null && !"".equals(searchfields.getAmountfrom())) {
                WhereCondition += " and amount > $col1";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAmountfrom());
                Searchcriertia = Searchcriertia + " Amount > " + searchfields.getAmountfrom();
            }
        } else if (">=".equals(searchfields.getAmountoperator())) {
            if (searchfields.getAmountfrom() != null && !"".equals(searchfields.getAmountfrom())) {
                WhereCondition += " and amount >= $col1";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAmountfrom());
                Searchcriertia = Searchcriertia + " Amount > Or = " + searchfields.getAmountfrom();
            }
        } else if ("<=".equals(searchfields.getAmountoperator())) {
            if (searchfields.getAmountfrom() != null && !"".equals(searchfields.getAmountfrom())) {
                WhereCondition += " and amount <= $col1";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAmountfrom());
                Searchcriertia = Searchcriertia + " Amount < Or = " + searchfields.getAmountfrom();
            }
        } else if ("=".equals(searchfields.getAmountoperator())) {
            if (searchfields.getAmountfrom() != null && !"".equals(searchfields.getAmountfrom())) {
                WhereCondition += " and amount = $col1";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAmountfrom());
                Searchcriertia = Searchcriertia + " Amount = " + searchfields.getAmountfrom();
            }
        }
        if (!"R".equals(Page)) {
            if (!"V".equals(Page)) {
                if ("Between".equals(searchfields.getSettamountoperator())) {
                    if (searchfields.getSettamountfrom() != null && !"".equals(searchfields.getSettamountfrom()) && searchfields.getSettamountto() != null && !"".equals(searchfields.getSettamountto())) {
                        WhereCondition += "  and settlement_amount between $col1 and $col2";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSettamountfrom());
                        WhereCondition = WhereCondition.replace("$col2", "" + searchfields.getSettamountto());
                        Searchcriertia = Searchcriertia + " Sett. Amount Between " + searchfields.getSettamountfrom() + " & " + searchfields.getSettamountto();
                    }
                } else if ("<>".equals(searchfields.getSettamountoperator())) {
                    if (searchfields.getSettamountfrom() != null && !"".equals(searchfields.getSettamountfrom())) {
                        WhereCondition += " and settlement_amount <> $col1";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSettamountfrom());
                        Searchcriertia = Searchcriertia + " Sett. Amount != " + searchfields.getSettamountfrom();
                    }
                } else if ("<".equals(searchfields.getSettamountoperator())) {
                    if (searchfields.getSettamountfrom() != null && !"".equals(searchfields.getSettamountfrom())) {
                        WhereCondition += " and settlement_amount < $col1";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSettamountfrom());
                        Searchcriertia = Searchcriertia + " Sett. Amount < " + searchfields.getSettamountfrom();
                    }
                } else if (">".equals(searchfields.getSettamountoperator())) {
                    if (searchfields.getSettamountfrom() != null && !"".equals(searchfields.getSettamountfrom())) {
                        WhereCondition += " and settlement_amount > $col1";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSettamountfrom());
                        Searchcriertia = Searchcriertia + " Sett. Amount > " + searchfields.getSettamountfrom();
                    }
                } else if (">=".equals(searchfields.getSettamountoperator())) {
                    if (searchfields.getSettamountfrom() != null && !"".equals(searchfields.getSettamountfrom())) {
                        WhereCondition += " and settlement_amount >= $col1";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSettamountfrom());
                        Searchcriertia = Searchcriertia + " Sett. Amount > Or = " + searchfields.getSettamountfrom();
                    }
                } else if ("<=".equals(searchfields.getSettamountoperator())) {
                    if (searchfields.getSettamountfrom() != null && !"".equals(searchfields.getSettamountfrom())) {
                        WhereCondition += " and settlement_amount <= $col1";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSettamountfrom());
                        Searchcriertia = Searchcriertia + " Sett. Amount < Or = " + searchfields.getSettamountfrom();
                    }
                } else if ("=".equals(searchfields.getSettamountoperator())) {
                    if (searchfields.getSettamountfrom() != null && !"".equals(searchfields.getSettamountfrom())) {
                        WhereCondition += " and settlement_amount = $col1";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSettamountfrom());
                        Searchcriertia = Searchcriertia + " Sett. Amount = " + searchfields.getSettamountfrom();
                    }
                }
            }
        }
        if (!"R".equals(Page)) {
            if (!"V".equals(Page)) {
                if ("Between".equals(searchfields.getAqramountoperator())) {
                    if (searchfields.getAqramountfrom() != null && !"".equals(searchfields.getAqramountfrom()) && searchfields.getAqramountto() != null && !"".equals(searchfields.getAqramountto())) {
                        WhereCondition += "  and acquirer_amount between $col1 and $col2";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAqramountfrom());
                        WhereCondition = WhereCondition.replace("$col2", "" + searchfields.getAqramountto());
                        Searchcriertia = Searchcriertia + " AQR. Amount Between " + searchfields.getAqramountfrom() + " & " + searchfields.getAqramountto();
                    }
                } else if ("<>".equals(searchfields.getAqramountoperator())) {
                    if (searchfields.getAqramountfrom() != null && !"".equals(searchfields.getAqramountfrom())) {
                        WhereCondition += " and acquirer_amount <> $col1";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAqramountfrom());
                        Searchcriertia = Searchcriertia + " AQR. Amount != " + searchfields.getAqramountfrom();
                    }
                } else if ("<".equals(searchfields.getAqramountoperator())) {
                    if (searchfields.getAqramountfrom() != null && !"".equals(searchfields.getAqramountfrom())) {
                        WhereCondition += " and acquirer_amount < $col1";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAqramountfrom());
                        Searchcriertia = Searchcriertia + " AQR. Amount < " + searchfields.getAqramountfrom();
                    }
                } else if (">".equals(searchfields.getAqramountoperator())) {
                    if (searchfields.getAqramountfrom() != null && !"".equals(searchfields.getAqramountfrom())) {
                        WhereCondition += " and acquirer_amount > $col1";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAqramountfrom());
                        Searchcriertia = Searchcriertia + " AQR. Amount > " + searchfields.getAqramountfrom();
                    }
                } else if (">=".equals(searchfields.getAqramountoperator())) {
                    if (searchfields.getAqramountfrom() != null && !"".equals(searchfields.getAqramountfrom())) {
                        WhereCondition += " and acquirer_amount >= $col1";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAqramountfrom());
                        Searchcriertia = Searchcriertia + " AQR. Amount > Or = " + searchfields.getAqramountfrom();
                    }
                } else if ("<=".equals(searchfields.getAqramountoperator())) {
                    if (searchfields.getAqramountfrom() != null && !"".equals(searchfields.getAqramountfrom())) {
                        WhereCondition += " and acquirer_amount <= $col1";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAqramountfrom());
                        Searchcriertia = Searchcriertia + " AQR. Amount < Or = " + searchfields.getAqramountfrom();
                    }
                } else if ("=".equals(searchfields.getAqramountoperator())) {
                    if (searchfields.getAqramountfrom() != null && !"".equals(searchfields.getAqramountfrom())) {
                        WhereCondition += " and acquirer_amount = $col1";
                        WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAqramountfrom());
                        Searchcriertia = Searchcriertia + " AQR. Amount = " + searchfields.getAqramountfrom();
                    }
                }
            }
        }

        if (searchfields.getCurrencyid() != 0 && searchfields.getCurrencyid() != null) {
            if (searchfields.getCurrencyoperator().equals("1")) {
                WhereCondition += " and currency_id = $col1";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getCurrencyid());
                Searchcriertia = Searchcriertia + " Base Currency = " + getcurencybyid(currencys, searchfields.getCurrencyid());
            } else {
                WhereCondition += " and currency_id != $col1";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getCurrencyid());
                Searchcriertia = Searchcriertia + " Base Currency not equal " + getcurencybyid(currencys, searchfields.getCurrencyid());
            }
        }
        if (!"R".equals(Page)) {
            if (!"V".equals(Page)) {
                if (searchfields.getSettcurrencyid() != 0 && searchfields.getSettcurrencyid() != null) {
                    WhereCondition += " and settlement_currency_id = $col1";
                    WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSettcurrencyid());
                    Searchcriertia = Searchcriertia + " Sett. Currency = " + getcurencybyid(currencys, searchfields.getSettcurrencyid());
                }
            }
        }
        if (!"R".equals(Page)) {
            if (!"V".equals(Page)) {
                if (searchfields.getAqccurrencyid() != 0 && searchfields.getAqccurrencyid() != null) {
                    WhereCondition += " and acquirer_currency_id = $col1";
                    WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAqccurrencyid());
                    Searchcriertia = Searchcriertia + " Acquirer. Currency = " + getcurencybyid(currencys, searchfields.getAqccurrencyid());
                }
            }
        }

        if (searchfields.getBlacklistbool() == Boolean.TRUE) {
            WhereCondition += " and " + super.GetDecryptCardNo() + " in (select " + super.GetDecryptCardNo() + " from black_listed_card)";
            Searchcriertia = Searchcriertia + " Card Number Is Black Listed";
        }

        if ("Like".equals(searchfields.getCardnumoperator())) {
            if (searchfields.getCardno() != null && !searchfields.getCardno().equals("")) {
                WhereCondition += " and " + super.GetDecryptCardNo() + " like '%$col1%'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getCardno());
                Searchcriertia = Searchcriertia + " Card Number Like " + searchfields.getCardno();
            }
        } else if ("Not Like".equals(searchfields.getCardnumoperator())) {
            if (searchfields.getCardno() != null && !searchfields.getCardno().equals("")) {
                WhereCondition += " and " + super.GetDecryptCardNo() + " Not Like '%$col1%'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getCardno());
                Searchcriertia = Searchcriertia + " Card Number Not Like " + searchfields.getCardno();
            }
        } else if ("is Null".equals(searchfields.getCardnumoperator())) {
            WhereCondition += " and " + super.GetDecryptCardNo() + " is null";
            Searchcriertia = Searchcriertia + " Card Number Is Null";
        } else if ("is Not Null".equals(searchfields.getCardnumoperator())) {
            WhereCondition += " and " + super.GetDecryptCardNo() + " is not null";
            Searchcriertia = Searchcriertia + " Card Number Is Not Null";
        } else if ("<>".equals(searchfields.getCardnumoperator())) {
            if (searchfields.getCardno() != null && !searchfields.getCardno().equals("")) {
                WhereCondition += " and " + super.GetDecryptCardNo() + " <> '$col1'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getCardno());
                Searchcriertia = Searchcriertia + " Card Number Not = " + searchfields.getCardno();
            }
        } else if ("=".equals(searchfields.getCardnumoperator())) {
            if (searchfields.getCardno() != null && !searchfields.getCardno().equals("")) {
                WhereCondition += " and " + super.GetDecryptCardNo() + " = '$col1'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getCardno());
                Searchcriertia = Searchcriertia + " Card Number = " + searchfields.getCardno();
            }
        }
        if ("Like".equals(searchfields.getAccountnumoperator())) {
            if (searchfields.getAccountno() != null && !searchfields.getAccountno().equals("")) {
                WhereCondition += " and CUSTOMER_ACCOUNT_NUMBER like '%$col1%'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAccountno());
                Searchcriertia = Searchcriertia + " CUSTOMER ACCOUNT NUMBER Like " + searchfields.getAccountno();
            }
        } else if ("Not Like".equals(searchfields.getAccountnumoperator())) {
            if (searchfields.getAccountno() != null && !searchfields.getAccountno().equals("")) {
                WhereCondition += " and CUSTOMER_ACCOUNT_NUMBER Not Like '%$col1%'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAccountno());
                Searchcriertia = Searchcriertia + " CUSTOMER ACCOUNT NUMBER Not Like " + searchfields.getAccountno();
            }
        } else if ("is Null".equals(searchfields.getAccountnumoperator())) {
            WhereCondition += " and CUSTOMER_ACCOUNT_NUMBER is null";
            Searchcriertia = Searchcriertia + " CUSTOMER ACCOUNT NUMBER Is Null ";
        } else if ("is Not Null".equals(searchfields.getAccountnumoperator())) {
            WhereCondition += " and CUSTOMER_ACCOUNT_NUMBER is not null";
            Searchcriertia = Searchcriertia + " CUSTOMER ACCOUNT NUMBER Is Not Null ";
        } else if ("<>".equals(searchfields.getAccountnumoperator())) {
            if (searchfields.getAccountno() != null && !searchfields.getAccountno().equals("")) {
                WhereCondition += " and CUSTOMER_ACCOUNT_NUMBER <> '$col1'";
                Searchcriertia = Searchcriertia + " CUSTOMER ACCOUNT NUMBER != " + searchfields.getAccountno();
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAccountno());
            }
        } else if ("=".equals(searchfields.getAccountnumoperator())) {
            if (searchfields.getAccountno() != null && !searchfields.getAccountno().equals("")) {
                WhereCondition += " and CUSTOMER_ACCOUNT_NUMBER = '$col1'";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAccountno());
                Searchcriertia = Searchcriertia + " CUSTOMER ACCOUNT NUMBER = " + searchfields.getAccountno();
            }
        }
        if (searchfields.getTypecodeid() != -1 && searchfields.getTypecodeid() != null) {
            WhereCondition += " and transaction_type_id = $col1";
            WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getTypecodeid());
            Searchcriertia = Searchcriertia + " Trans Code ID = " + gettranstypeid(transtypelist, searchfields.getTypecodeid());
        }
        if (searchfields.getTypecodename() != null && !"".equals(searchfields.getTypecodename())) {
            WhereCondition += " and lower(transaction_type) like lower('%$col1%')";
            WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getTypecodename());
            Searchcriertia = Searchcriertia + " Trans Code Like " + searchfields.getTypecodename();
        }
        if ("D".equals(Page)) {
            if (searchfields.getDisputeuserid() != 0 && searchfields.getDisputeuserid() != null) {
                WhereCondition += " and dispute_by = $col1";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getDisputeuserid());
                Searchcriertia = Searchcriertia + " Pending By = " + getusersbyid(userslist, searchfields.getDisputeuserid());
            }
        }
        if ("D".equals(Page)) {
            if (searchfields.getReversedby() != 0 && searchfields.getReversedby() != null) {
                Integer MTL = 0;
                String Search = "";
                transsearchDAOInter engin = DAOFactory.createtranssearchDAO();
                if ("Network - Switch".equals(searchfields.getMatchingtypeid())) {
                    MTL = 1;
                } else if ("Switch - Host".equals(searchfields.getMatchingtypeid())) {
                    MTL = 2;
                }
                if (searchfields.getReversedby() == 1) {
                    Search += " get_matching_crateria($MATCHINGTYPELIST,$NETWORK,1)";
                    Search = Search.replace("$NETWORK", "" + searchfields.getNetwork());
                    Search = Search.replace("$MATCHINGTYPELIST", "" + MTL);
                    String MatchingCrt = (String) engin.findMatchCret(Search);
                    WhereCondition += MatchingCrt;
                    Searchcriertia = Searchcriertia + " Reversed By = Only";
                }
                if (searchfields.getReversedby() == 2) {
                    Search += " get_matching_crateria($MATCHINGTYPELIST,$NETWORK,2)";
                    Search = Search.replace("$NETWORK", "" + searchfields.getNetwork());
                    Search = Search.replace("$MATCHINGTYPELIST", "" + MTL);
                    String MatchingCrt = (String) engin.findMatchCret(Search);
                    WhereCondition += MatchingCrt;
                    Searchcriertia = Searchcriertia + " Reversed By = With Match";
                }
                if (searchfields.getReversedby() == 3) {
                    Search += " get_matching_crateria($MATCHINGTYPELIST,$NETWORK,3)";
                    Search = Search.replace("$NETWORK", "" + searchfields.getNetwork());
                    Search = Search.replace("$MATCHINGTYPELIST", "" + MTL);
                    String MatchingCrt = (String) engin.findMatchCret(Search);
                    WhereCondition += MatchingCrt;
                    Searchcriertia = Searchcriertia + " Reversed By = Exclude";
                }
            }
        }
        if (!"R".equals(Page)) {
            if (!"V".equals(Page)) {
                if (searchfields.getSetteltstatus() != 0 && searchfields.getSetteltstatus() != null) {
                    WhereCondition += " and settled_flag = $col1";
                    WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSetteltstatus());
                    Searchcriertia = Searchcriertia + " settled flag = " + searchfields.getSetteltstatus();
                }
            }
        }
        if (!"R".equals(Page)) {
            if (!"V".equals(Page)) {
                if (searchfields.getSetteltbyid() != 0 && searchfields.getSetteltbyid() != null) {
                    WhereCondition += " and settled_user = $col1";
                    WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSetteltbyid());
                    Searchcriertia = Searchcriertia + " Settled By = " + getusersbyid(userslist, searchfields.getSetteltbyid());
                }
            }
        }
        if ("D".equals(Page)) {
            if (searchfields.getViewid() != 0 && searchfields.getViewid() != null) {
                if (searchfields.getViewid() == 1) {
                    WhereCondition += " and not exists (select 1 from disputes d where d.dispute_key = disputes.dispute_key)";
                    Searchcriertia = Searchcriertia + " View By Missing Side";
                } else if (searchfields.getViewid() == 2) {
                    WhereCondition += " and exists (select 1 from disputes d where d.dispute_key = disputes.dispute_key)";
                    Searchcriertia = Searchcriertia + " View By Pending Side";
                }
            }
        }
        if ("=".equals(searchfields.getResponceidoperator())) {
            if (searchfields.getResponceid() != 0) {
                WhereCondition += " and response_code_id = $col1";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getResponceid());
                Searchcriertia = Searchcriertia + " Responce Code = " + getrespid(resplist, searchfields.getResponceid());
            }
        } else if ("<>".equals(searchfields.getResponceidoperator())) {
            if (searchfields.getResponceid() != 0) {
                WhereCondition += " and response_code_id <> $col1";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getResponceid());
                Searchcriertia = Searchcriertia + " Responce Code != " + getrespid(resplist, searchfields.getResponceid());
            }
        }
        if ("=".equals(searchfields.getResponcecodenameoperator())) {
            if (!searchfields.getResponcecodename().equals("") && searchfields.getResponcecodename() != null) {
                WhereCondition += " and lower(response_code) like lower('%$col1%')";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getResponcecodename());
                Searchcriertia = Searchcriertia + " Responce Code Like " + searchfields.getResponcecodename();
            }
        } else if ("<>".equals(searchfields.getResponcecodenameoperator())) {
            if (!searchfields.getResponcecodename().equals("") && searchfields.getResponcecodename() != null) {
                WhereCondition += " and lower(response_code) <> lower('$col1')";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getResponcecodename());
                Searchcriertia = Searchcriertia + " Responce Code Not Like " + searchfields.getResponcecodename();
            }
        }
        if ("Between".equals(searchfields.getSeqoperator())) {
            if (searchfields.getSeqfrom() != null && !searchfields.getSeqfrom().equals("") && searchfields.getSeqto() != null && !searchfields.getSeqto().equals("")) {
                WhereCondition += "  and transaction_sequence_order_by between nvl($col1,0) and nvl($col2,9999999999999999)";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSeqfrom());
                WhereCondition = WhereCondition.replace("$col2", "" + searchfields.getSeqto());
                Searchcriertia = Searchcriertia + " Trace Between" + searchfields.getSeqfrom() + " & " + searchfields.getSeqto();
            }
        } else if ("<>".equals(searchfields.getSeqoperator())) {
            if (searchfields.getSeqfrom() != null && !searchfields.getSeqfrom().equals("")) {
                WhereCondition += " and transaction_sequence_order_by <> nvl($col1,0)";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSeqfrom());
                Searchcriertia = Searchcriertia + " Trace !=" + searchfields.getSeqfrom();
            }
        } else if ("<".equals(searchfields.getSeqoperator())) {
            if (searchfields.getSeqfrom() != null && !searchfields.getSeqfrom().equals("")) {
                WhereCondition += " and transaction_sequence_order_by < nvl($col1,0)";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSeqfrom());
                Searchcriertia = Searchcriertia + " Trace <" + searchfields.getSeqfrom();
            }
        } else if (">".equals(searchfields.getSeqoperator())) {
            if (searchfields.getSeqfrom() != null && !searchfields.getSeqfrom().equals("")) {
                WhereCondition += " and transaction_sequence_order_by > nvl($col1,0)";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSeqfrom());
                Searchcriertia = Searchcriertia + " Trace >" + searchfields.getSeqfrom();
            }
        } else if (">=".equals(searchfields.getSeqoperator())) {
            if (searchfields.getSeqfrom() != null && !searchfields.getSeqfrom().equals("")) {
                WhereCondition += " and transaction_sequence_order_by >= nvl($col1,0)";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSeqfrom());
                Searchcriertia = Searchcriertia + " Trace > Or =" + searchfields.getSeqfrom();
            }
        } else if ("<=".equals(searchfields.getSeqoperator())) {
            if (searchfields.getSeqfrom() != null && !searchfields.getSeqfrom().equals("")) {
                WhereCondition += " and transaction_sequence_order_by <= nvl($col1,0)";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSeqfrom());
                Searchcriertia = Searchcriertia + " Trace < Or =" + searchfields.getSeqfrom();
            }
        } else if ("=".equals(searchfields.getSeqoperator())) {
            if (searchfields.getSeqfrom() != null && !searchfields.getSeqfrom().equals("") && !searchfields.getSeqfrom().equals(0)) {
                WhereCondition += " and transaction_sequence_order_by = nvl($col1,0)";
                WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getSeqfrom());
                Searchcriertia = Searchcriertia + " Trace =" + searchfields.getSeqfrom();
            }
        }
        if (!"R".equals(Page)) {
            if (!"V".equals(Page)) {
                if (searchfields.getRefno() != null && !"".equals(searchfields.getRefno())) {
                    WhereCondition += " and lower(reference_no) like lower('%$col1%')";
                    WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getRefno());
                    Searchcriertia = Searchcriertia + " Reference NO Like " + searchfields.getRefno();
                }
                if (searchfields.getCardnumsuffix() != null && !"".equals(searchfields.getCardnumsuffix())) {
                    WhereCondition += " and lower(card_no_suffix) like lower('%$col1%')";
                    WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getCardnumsuffix());
                    Searchcriertia = Searchcriertia + " Card Suffix like " + searchfields.getCardnumsuffix();
                }
                if (searchfields.getBank() != 0 && searchfields.getBank() != null) {
                    WhereCondition += " and Bank in (select bank from bank_code where id = $col1)";
                    WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getBank());
                    Searchcriertia = Searchcriertia + " Bank = " + searchfields.getBank();
                }
            }
        }
        if (!"R".equals(Page)) {
            if (!"V".equals(Page)) {
                if (searchfields.getAuthno() != null && !"".equals(searchfields.getAuthno())) {
                    WhereCondition += " and lower(authorization_no) like lower('%$col1%')";
                    WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getAuthno());
                    Searchcriertia = Searchcriertia + " Authorization NO Like " + searchfields.getAuthno();
                }
            }
        }
        if (searchfields.getCol1() != null && !"".equals(searchfields.getCol1())) {
            WhereCondition += " and lower(column1) like lower('%$col1%')";
            WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getCol1());
            Searchcriertia = Searchcriertia + " Column 1 Like " + searchfields.getCol1();
        }
        if (!"R".equals(Page)) {
            if (!"V".equals(Page)) {
                if (searchfields.getCol4() != 0 && searchfields.getCol4() != null) {
                    WhereCondition += " and column4 in (select name from col4_code where id = $col1)";
                    WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getCol4());
                    Searchcriertia = Searchcriertia + " Column 4 = " + searchfields.getCol4();
                }
            }
        }
        if (!"R".equals(Page)) {
            if (!"V".equals(Page)) {
                if (searchfields.getNetwork() != 0 && searchfields.getNetwork() != null) {
                    WhereCondition += " and network_id = $col1";
                    WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getNetwork());
                    Searchcriertia = Searchcriertia + " Network = " + getnetworkbyid(networklist, searchfields.getNetwork());
                }
            }
        }
        if (searchfields.getFileid() != 0 && searchfields.getFileid() != null) {
            WhereCondition += " and file_id = $col1";
            WhereCondition = WhereCondition.replace("$col1", "" + searchfields.getFileid());
            Searchcriertia = Searchcriertia + " File Name = " + searchfields.getFilename();
        }
        if ("R".equals(Page)) {
            if (!"V".equals(Page)) {
                if (searchfields.getValidationruleid() != 0) {
                    if ("1".equals(searchfields.getRecordtypeid())) {
                        WhereCondition += " and exists (select 1 from rejected_network_rules where rejected_network_rules.transaction_date = rejected_transactions.transaction_date and rejected_network_rules.transaction_id = rejected_transactions.transaction_id and rejected_network_rules.rule_id = $rule) ";
                        WhereCondition = WhereCondition.replace("$rule", "" + searchfields.getValidationruleid());
                        Searchcriertia = Searchcriertia + " Validation Rule ID = " + getvalidationid(validationrulelist, searchfields.getValidationruleid()) + " Record Type = " + getrecordid(searchfields.getRecordtypeid());
                    } else if ("2".equals(searchfields.getRecordtypeid())) {

                        WhereCondition += " and exists (select 1 from rejected_switch_rules where rejected_switch_rules.transaction_date = rejected_transactions.transaction_date and rejected_switch_rules.transaction_id = rejected_transactions.transaction_id and rejected_switch_rules.rule_id = $rule) ";
                        WhereCondition = WhereCondition.replace("$rule", "" + searchfields.getValidationruleid());
                        Searchcriertia = Searchcriertia + " Validation Rule ID = " + getvalidationid(validationrulelist, searchfields.getValidationruleid()) + " Record Type = " + getrecordid(searchfields.getRecordtypeid());

                    } else if ("3".equals(searchfields.getRecordtypeid())) {
                        WhereCondition += " and exists (select 1 from rejected_host_rules where rejected_host_rules.transaction_date = rejected_transactions.transaction_date and rejected_host_rules.transaction_id = rejected_transactions.transaction_id and rejected_host_rules.rule_id = $rule)  ";
                        WhereCondition = WhereCondition.replace("$rule", "" + searchfields.getValidationruleid());
                        Searchcriertia = Searchcriertia + " Validation Rule ID = " + getvalidationid(validationrulelist, searchfields.getValidationruleid()) + " Record Type = " + getrecordid(searchfields.getRecordtypeid());
                    }
                } else {
                    {
                        if ("1".equals(searchfields.getRecordtypeid())) {
                            WhereCondition += " and exists (select 1 from rejected_network_rules where rejected_network_rules.transaction_date = rejected_transactions.transaction_date and rejected_network_rules.transaction_id = rejected_transactions.transaction_id ) ";
                            Searchcriertia = Searchcriertia + " Record Type = " + getrecordid(searchfields.getRecordtypeid());
                        } else if ("2".equals(searchfields.getRecordtypeid())) {
                            WhereCondition += " and exists (select 1 from rejected_switch_rules where rejected_switch_rules.transaction_date = rejected_transactions.transaction_date and rejected_switch_rules.transaction_id = rejected_transactions.transaction_id ) ";
                            Searchcriertia = Searchcriertia + " Record Type = " + getrecordid(searchfields.getRecordtypeid());
                        } else if ("3".equals(searchfields.getRecordtypeid())) {
                            WhereCondition += " and exists (select 1 from rejected_host_rules where rejected_host_rules.transaction_date = rejected_transactions.transaction_date and rejected_host_rules.transaction_id = rejected_transactions.transaction_id )  ";
                            Searchcriertia = Searchcriertia + " Record Type = " + getrecordid(searchfields.getRecordtypeid());
                        }
                    }
                }
            }
        }
        if (!"".equals(searchfields.getTerminal()) && searchfields.getTerminal() != null) {
            WhereCondition += " and terminal like '%$ter%' ";
            WhereCondition = WhereCondition.replace("$ter", "" + searchfields.getTerminal());
            Searchcriertia = Searchcriertia + " terminal like " + searchfields.getTerminal();
        }
        if (" where 1 = 1 and".equals(WhereCondition)) {
            WhereCondition = "";
        } else {
            WhereCondition = WhereCondition.replace("1 = 1 and and", "");
        }
        if (!"0".equals(searchfields.getSortby()) && searchfields.getSortby() != null) {
            if (searchfields.getSortby().contains("SETTLEMENT_AMOUNT")) {
                searchfields.setSortby(" NVL (SETTLEMENT_AMOUNT, 0) ");
            }
            SortBy = " order by " + searchfields.getSortby() + " " + searchfields.getSorttype();

        } else {
            SortBy = " order by transaction_date ASC";
        }
        String SelectStatment = "";
        if ("V".equals(Page)) {
            SelectStatment = "Select ROWNUM AS rn,LOADING_DATE,file_id,transaction_id,TRANSACTION_DATE,SETTLEMENT_DATE,TRANSACTION_SEQUENCE,TRANSACTION_TYPE," + super.GetDecryptCardNo() + "CARD_NO,AMOUNT,CURRENCY,CURRENCY_ID,CUSTOMER_ACCOUNT_NUMBER,RESPONSE_CODE,RESPONSE_CODE_ID,TRANSACTION_TYPE_ID,TRANSACTION_TIME,TRANSACTION_SEQUENCE_ORDER_BY,COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5 ";

        } else if ("R".equals(Page)) {
            SelectStatment = "Select ROWNUM AS rn,get_rejected_reseaon(record_type,transaction_id) REASON,is_black_list(" + super.GetDecryptCardNo() + ") BLACKLISTED,LOADING_DATE,record_type,file_id,transaction_id,TRANSACTION_DATE,SETTLEMENT_DATE,TRANSACTION_SEQUENCE,TRANSACTION_TYPE," + super.GetDecryptCardNo() + "CARD_NO,AMOUNT,CURRENCY,CURRENCY_ID,CUSTOMER_ACCOUNT_NUMBER,RESPONSE_CODE,RESPONSE_CODE_ID,TRANSACTION_TYPE_ID,TRANSACTION_TIME,TRANSACTION_SEQUENCE_ORDER_BY,COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5 ";

        } else if ("D".equals(Page)) {
            SelectStatment = "Select ROWNUM AS rn,corrective_entry_flag,CORRECTIVE_AMOUNT,matching_type,settled_flag,is_black_list(" + super.GetDecryptCardNo() + ") BLACKLISTED,COMMENTS,LOADING_DATE,dispute_key,record_type,file_id,transaction_id,TRANSACTION_DATE,SETTLEMENT_DATE,TRANSACTION_SEQUENCE,TRANSACTION_TYPE," + super.GetDecryptCardNo() + "CARD_NO,AMOUNT,CURRENCY,CURRENCY_ID,CUSTOMER_ACCOUNT_NUMBER,RESPONSE_CODE,RESPONSE_CODE_ID,TRANSACTION_TYPE_ID,TERMINAL,TRANSACTION_TIME,TRANSACTION_SEQUENCE_ORDER_BY,COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5,TRANSACTION_TYPE_MASTER,RESPONSE_CODE_MASTER,CARD_NO_SUFFIX,ACQUIRER_CURRENCY,ACQUIRER_CURRENCY_ID,ACQUIRER_AMOUNT,SETTLEMENT_CURRENCY,SETTLEMENT_CURRENCY_ID,SETTLEMENT_AMOUNT,AUTHORIZATION_NO,REFERENCE_NO,BANK,NETWORK_CODE,PROCESS_CODE,AREA_CODE,REPORT_ID,NETWORK_COMM,NETWORK_COMM_FLAG,FEE_LEVEL,(select name from networks where id = NETWORK_ID)NETWORK_ID";
        } else if ("M".equals(Page)) {
            SelectStatment = "Select ROWNUM AS rn,matching_type,settled_flag,is_black_list(" + super.GetDecryptCardNo() + ") BLACKLISTED,LOADING_DATE,match_key,record_type,file_id,transaction_id,TRANSACTION_DATE,SETTLEMENT_DATE,TRANSACTION_SEQUENCE,TRANSACTION_TYPE," + super.GetDecryptCardNo() + "CARD_NO,AMOUNT,CURRENCY,CURRENCY_ID,CUSTOMER_ACCOUNT_NUMBER,RESPONSE_CODE,RESPONSE_CODE_ID,TRANSACTION_TYPE_ID,TERMINAL,TRANSACTION_TIME,TRANSACTION_SEQUENCE_ORDER_BY,COLUMN1,COLUMN2,COLUMN3,COLUMN4,COLUMN5,TRANSACTION_TYPE_MASTER,RESPONSE_CODE_MASTER,CARD_NO_SUFFIX,ACQUIRER_CURRENCY,ACQUIRER_CURRENCY_ID,ACQUIRER_AMOUNT,SETTLEMENT_CURRENCY,SETTLEMENT_CURRENCY_ID,SETTLEMENT_AMOUNT,AUTHORIZATION_NO,REFERENCE_NO,BANK,NETWORK_CODE,PROCESS_CODE,AREA_CODE,REPORT_ID,NETWORK_COMM,NETWORK_COMM_FLAG,FEE_LEVEL,(select name from networks where id = NETWORK_ID)NETWORK_ID";
        }
        if (!"0".equals(searchfields.getSortby()) && searchfields.getSortby() != null) {
            SelectStatment = SelectStatment.replace("ROWNUM AS rn", "row_number() OVER (ORDER BY " + searchfields.getSortby() + " " + searchfields.getSorttype() + ") AS rn");
        }
        /*  for (String str : selectedCol) {
         SelectStatment = SelectStatment + str + ",";
         }
         SelectStatment = SelectStatment.substring(0, SelectStatment.length() - 1);
         SelectStatment = "Select " + SelectStatment;*/
        returnvalues[0] = WhereCondition;
        returnvalues[1] = Searchcriertia;
        returnvalues[2] = SortBy;
        returnvalues[3] = SelectStatment;
        return returnvalues;
    }

    public Object findothersideMatched(Object... obj) throws Throwable {
        disputesDTOInter SearchParameter = (disputesDTOInter) obj[0];
        transsearchDAOInter engin = DAOFactory.createtranssearchDAO();
        disputesDTOInter Record = (disputesDTOInter) engin.findothersideMatched(SearchParameter);
        return Record;
    }

    public Object findotherside(Object... obj) throws Throwable {
        disputesDTOInter SearchParameter = (disputesDTOInter) obj[0];
        transsearchDAOInter engin = DAOFactory.createtranssearchDAO();
        disputesDTOInter Record = (disputesDTOInter) engin.findotherside(SearchParameter);
        return Record;
    }

    public List<disputesDTOInter> getselecteditems(String Statment) throws Throwable {
        transsearchDAOInter engin = DAOFactory.createtranssearchDAO();
        List<disputesDTOInter> AllRecords = (List<disputesDTOInter>) engin.getselecteditems(Statment);
        return AllRecords;
    }

    public Object UpdatecorrectiveRecord(Object... obj) throws Throwable {
        disputesDTOInter SearchParameter = (disputesDTOInter) obj[0];
        transsearchDAOInter engin = DAOFactory.createtranssearchDAO();
        String Record = (String) engin.UpdatecorrectiveRecord(SearchParameter);
        return Record;
    }

    public Object UpdatesetteledRecord(Object... obj) throws Throwable {
        disputesDTOInter SearchParameter = (disputesDTOInter) obj[0];
        transsearchDAOInter engin = DAOFactory.createtranssearchDAO();
        String Record = (String) engin.UpdatesetteledRecord(SearchParameter);
        return Record;
    }

    public void correctiveamountupdate(Object... obj) throws Throwable {
        disputesDTOInter SearchParameter = (disputesDTOInter) obj[0];
        transsearchDAOInter engin = DAOFactory.createtranssearchDAO();
        engin.correctiveamountupdate(SearchParameter);
    }

    public void exportRecord(Object... obj) throws Throwable {
        disputesDTOInter SearchParameter = (disputesDTOInter) obj[0];
        transsearchDAOInter engin = DAOFactory.createtranssearchDAO();
        engin.exportRecord(SearchParameter);
    }

    public Object UpdateCommentRecord(Object... obj) throws Throwable {
        disputesDTOInter SearchParameter = (disputesDTOInter) obj[0];
        transsearchDAOInter engin = DAOFactory.createtranssearchDAO();
        String Record = (String) engin.UpdateCommentRecord(SearchParameter);
        return Record;
    }

    public disputesDTOInter[] SortOrder(disputesDTOInter[] entities, String sortby, String sortcol) {
        int[] res = new int[entities.length];
        for (int i = 0; i < entities.length; i++) {
            res[i] = entities[i].getRownum();
        }
        if ("DSC".equals(sortby)) {
            Arrays.sort(res);
        } else {
            Arrays.sort(res);
        }
        disputesDTOInter[] Result = new disputesDTOInter[entities.length];
        for (int u = 0; u < res.length; u++) {
            for (int l = 0; l < entities.length; l++) {
                if (res[u] == entities[l].getRownum()) {
                    Result[u] = entities[l];
                    break;
                }
            }
        }
        return Result;
    }

    public Integer savePrint(Object... obj) throws Throwable {
        disputesDTOInter[] entities = (disputesDTOInter[]) obj[0];
        String PAGE = (String) obj[1];
        String FIELDS = (String) obj[2];
        String sortby = (String) obj[3];
        String sortcol = (String) obj[4];
        transsearchDAOInter engin = DAOFactory.createtranssearchDAO();
        return engin.savePrint((Object) SortOrder(entities, sortby, sortcol), PAGE, FIELDS);
    }

    public Object GetReport(Object... obj) throws Throwable {

        Integer rs = (Integer) obj[0];
        String Headers = (String) obj[1];
        String userName = (String) obj[2];
        String customer = (String) obj[3];
        String Page = (String) obj[4];
        String Search = (String) obj[5];
        String[] Header = Headers.split(",");
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("user", userName);
        params.put("customer", customer);
        params.put("Title", Page.replaceAll("[^a-zA-Z]", "") + " Transaction Report");
        params.put("search", Search);
        params.put("repid", rs.toString());
        int colindex = 1;
        for (int i = 0; i < Header.length; i++) {
            params.put("COLHDR" + colindex, Header[i]);
            colindex = colindex + 1;
        }

        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();

            JasperDesign design = JRXmlLoader.load("C:/CardsReports/Trans_Data_Report.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri = "Trans" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {

                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }

    }

}
