package com.ev.Bingo.bus.bo;

import java.sql.SQLException;

public interface duplicatesettingdetailBOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object GetListOfRecords(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;
}
