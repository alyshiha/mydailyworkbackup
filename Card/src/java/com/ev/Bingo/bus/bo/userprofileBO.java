package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.userprofileDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.userprofileDTOInter;

public class userprofileBO extends BaseBO implements userprofileBOInter {

    protected userprofileBO() {
        super();
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<userprofileDTOInter> entities = (List<userprofileDTOInter>) obj[0];
        String profile = (String) obj[1];
        userprofileDAOInter engin = DAOFactory.createuserprofileDAO();
        return engin.save(entities,profile);
    }

    public Object deleterecord(Object... obj) throws Throwable {
        userprofileDTOInter RecordToDelete = (userprofileDTOInter) obj[0];
        userprofileDAOInter engin = DAOFactory.createuserprofileDAO();
        return engin.deleterecord(RecordToDelete);
    }

}
