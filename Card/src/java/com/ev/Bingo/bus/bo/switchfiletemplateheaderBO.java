package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.switchfiletemplateheaderDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.switchfiletemplateheaderDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.switchfiletemplateheaderDTOInter;
import java.text.SimpleDateFormat;

public class switchfiletemplateheaderBO extends BaseBO implements switchfiletemplateheaderBOInter {

    protected switchfiletemplateheaderBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        switchfiletemplateheaderDTOInter RecordToInsert = (switchfiletemplateheaderDTOInter) obj[0];
        switchfiletemplateheaderDAOInter engin = DAOFactory.createswitchfiletemplateheaderDAO();
        engin.insertrecord(RecordToInsert);
        return "Insert Done";
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<switchfiletemplateheaderDTOInter> entities = (List<switchfiletemplateheaderDTOInter>) obj[0];
        switchfiletemplateheaderDAOInter engin = DAOFactory.createswitchfiletemplateheaderDAO();
        engin.save(entities);
        return "Insert Done";
    }

    public Object updaterecord(Object... obj) throws Throwable {
        switchfiletemplateheaderDTOInter RecordToUpdate = (switchfiletemplateheaderDTOInter) obj[0];
        switchfiletemplateheaderDAOInter engin = DAOFactory.createswitchfiletemplateheaderDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        switchfiletemplateheaderDAOInter engin = DAOFactory.createswitchfiletemplateheaderDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        switchfiletemplateheaderDTOInter RecordToDelete = (switchfiletemplateheaderDTOInter) obj[0];
        switchfiletemplateheaderDAOInter engin = DAOFactory.createswitchfiletemplateheaderDAO();
        engin.deleterecord(RecordToDelete);
        return "record has been deleted";
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<switchfiletemplateheaderDTOInter> SearchRecords = (List<switchfiletemplateheaderDTOInter>) obj[0];
        switchfiletemplateheaderDAOInter engin = DAOFactory.createswitchfiletemplateheaderDAO();
        List<switchfiletemplateheaderDTOInter> AllRecords = (List<switchfiletemplateheaderDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        switchfiletemplateheaderDAOInter engin = DAOFactory.createswitchfiletemplateheaderDAO();
        List<switchfiletemplateheaderDTOInter> AllRecords = (List<switchfiletemplateheaderDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        switchfiletemplateheaderDTOInter SearchParameter = (switchfiletemplateheaderDTOInter) obj[0];
        switchfiletemplateheaderDAOInter engin = DAOFactory.createswitchfiletemplateheaderDAO();
        switchfiletemplateheaderDTOInter Record = (switchfiletemplateheaderDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
