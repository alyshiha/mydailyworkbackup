package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.cardfiletemplatedetailDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.cardfiletemplateDTOInter;
import com.ev.Bingo.bus.dto.cardfiletemplatedetailDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.cardfiletemplatedetailDTOInter;
import java.text.SimpleDateFormat;

public class cardfiletemplatedetailBO extends BaseBO implements cardfiletemplatedetailBOInter {

    protected cardfiletemplatedetailBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        cardfiletemplatedetailDTOInter RecordToInsert = (cardfiletemplatedetailDTOInter) obj[0];
        cardfiletemplatedetailDAOInter engin = DAOFactory.createcardfiletemplatedetailDAO();
        engin.insertrecord(RecordToInsert);
        return "Insert Done";
    }
  public int updatetemplate(int seq, int TemplateID, int networktype) throws Throwable{
        cardfiletemplatedetailDAOInter engin = DAOFactory.createcardfiletemplatedetailDAO();
        return engin.updatetemplate(seq, TemplateID, networktype);
    }
    public int save(Object... obj) throws SQLException, Throwable {
        List<cardfiletemplatedetailDTOInter> entities = (List<cardfiletemplatedetailDTOInter>) obj[0];
        String TableName = (String) obj[1];
        cardfiletemplatedetailDAOInter engin = DAOFactory.createcardfiletemplatedetailDAO();
        return engin.save(entities, TableName);
    }

    public Object updaterecord(Object... obj) throws Throwable {
        cardfiletemplatedetailDTOInter RecordToUpdate = (cardfiletemplatedetailDTOInter) obj[0];
        cardfiletemplatedetailDAOInter engin = DAOFactory.createcardfiletemplatedetailDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public String deleteallrecord(Object... obj) throws Throwable {
        cardfiletemplateDTOInter Record = (cardfiletemplateDTOInter) obj[0];
        String TableName = (String) obj[1];
        cardfiletemplatedetailDAOInter engin = DAOFactory.createcardfiletemplatedetailDAO();
        return engin.deleteallrecord(Record,TableName);
    }

 

    public Object GetListOfRecords(Object... obj) throws Throwable {
        String TableName = (String) obj[0];
        cardfiletemplateDTOInter SearchRecords = (cardfiletemplateDTOInter) obj[1];
        cardfiletemplatedetailDAOInter engin = DAOFactory.createcardfiletemplatedetailDAO();
        List<cardfiletemplatedetailDTOInter> AllRecords = (List<cardfiletemplatedetailDTOInter>) engin.findRecordsList(TableName, SearchRecords);
        return AllRecords;
    }
     public Object findRecordsListUpdate(int seq) throws Throwable {
        cardfiletemplatedetailDAOInter engin = DAOFactory.createcardfiletemplatedetailDAO();
        List<cardfiletemplatedetailDTOInter> AllRecords = (List<cardfiletemplatedetailDTOInter>) engin.findRecordsListUpdate(seq);
        return AllRecords;
    }
    public Object GetAllRecords(Object... obj) throws Throwable {
        cardfiletemplatedetailDAOInter engin = DAOFactory.createcardfiletemplatedetailDAO();
        List<cardfiletemplatedetailDTOInter> AllRecords = (List<cardfiletemplatedetailDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        cardfiletemplatedetailDTOInter SearchParameter = (cardfiletemplatedetailDTOInter) obj[0];
        cardfiletemplatedetailDAOInter engin = DAOFactory.createcardfiletemplatedetailDAO();
        cardfiletemplatedetailDTOInter Record = (cardfiletemplatedetailDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
