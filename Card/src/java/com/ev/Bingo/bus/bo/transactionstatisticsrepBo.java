package com.ev.Bingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.bo.BaseBO;
import com.ev.Bingo.base.data.DataProviderFactory;
 
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.transactionstatisticsrepDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dao.usersDAOInter;
import com.ev.Bingo.bus.dto.transactionstatisticsrepDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.transactionstatisticsrepDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.text.SimpleDateFormat;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class transactionstatisticsrepBo extends BaseBO implements transactionstatisticsrepBOInter {

    protected transactionstatisticsrepBo() {
        super();
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        String SearchRecords = (String) obj[0];
        transactionstatisticsrepDAOInter engin = DAOFactory.createtransactionstatisticsrepDAO();
        List<transactionstatisticsrepDTOInter> AllRecords = (List<transactionstatisticsrepDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        transactionstatisticsrepDAOInter engin = DAOFactory.createtransactionstatisticsrepDAO();
        List<transactionstatisticsrepDTOInter> AllRecords = (List<transactionstatisticsrepDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetReport(Object... obj) throws Throwable {
        String userName = (String) obj[0];
        String customer = (String) obj[1];
        Date fromDate = (Date) obj[4];
        String index = (String) obj[6];
        Date toDate = (Date) obj[5];
        usersDTOInter UserDTO = (usersDTOInter) obj[2];
        List<transactionstatisticsrepDTOInter> ListOfResult = (List<transactionstatisticsrepDTOInter>) obj[3];
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        JRResultSetDataSource resultSetDataSource;
        params.put("Title", "Matched Transactions Statistics");
        transactionstatisticsrepDAOInter arrRepDAO = DAOFactory.createtransactionstatisticsrepDAO();
        ResultSet rs = (ResultSet) arrRepDAO.findRecordsAll(index);
        resultSetDataSource = new JRResultSetDataSource(rs);
        params.put("user", userName);
        params.put("customer", customer);
        params.put("dateF", DateFormatter.changeDateAndTimeFormat(fromDate));
        params.put("dateT", DateFormatter.changeDateAndTimeFormat(toDate));
        //params.put("param", param);
        try {
            JasperDesign design = JRXmlLoader.load("C:/CardsReports/stat.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, resultSetDataSource);
            Calendar c = Calendar.getInstance();
            String uri = "PDFStatistics" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);

            try {

                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                resultSetDataSource = null;
                CoonectionHandler.getInstance().returnConnection(rs.getStatement().getConnection());
                rs.close();
                
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }

    }

    public Object RunRep(Object... obj) throws SQLException, Throwable {
        Date fromDate = (Date) obj[0];
        Date toDate = (Date) obj[1];
        Integer network = (Integer) obj[2];
        String fileid = (String) obj[3];
        transactionstatisticsrepDAOInter engin = DAOFactory.createtransactionstatisticsrepDAO();
        return engin.RunRep(fromDate, toDate, network, fileid);
    }
}
