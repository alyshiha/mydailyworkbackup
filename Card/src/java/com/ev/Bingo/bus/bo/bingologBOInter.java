package com.ev.Bingo.bus.bo;

import java.sql.SQLException;
import java.util.Date;

public interface bingologBOInter {

    Object GetListOfRecords(Date FromDate, Date ToDate, Integer UserID, Integer ProfileID) throws Throwable;

}
