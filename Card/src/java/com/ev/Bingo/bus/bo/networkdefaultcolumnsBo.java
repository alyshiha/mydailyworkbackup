package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.networkdefaultcolumnsDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.networkdefaultcolumnsDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.networkdefaultcolumnsDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.text.SimpleDateFormat;

public class networkdefaultcolumnsBo extends BaseBO implements networkdefaultcolumnsBOInter {

    protected networkdefaultcolumnsBo() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        networkdefaultcolumnsDTOInter RecordToInsert = (networkdefaultcolumnsDTOInter) obj[0];
        networkdefaultcolumnsDAOInter engin = DAOFactory.createnetworkdefaultcolumnsDAO();
        return engin.insertrecord(RecordToInsert);

    }

    public Object saveorder(Object... obj) throws SQLException, Throwable {
        List<String> entities = (List<String>) obj[0];
        Integer network = (Integer) obj[1];
        networkdefaultcolumnsDAOInter engin = DAOFactory.createnetworkdefaultcolumnsDAO();
        return engin.saveorder(entities, network);
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<networkdefaultcolumnsDTOInter> entities = (List<networkdefaultcolumnsDTOInter>) obj[0];
        networkdefaultcolumnsDAOInter engin = DAOFactory.createnetworkdefaultcolumnsDAO();
        return engin.save(entities);
    }

    public Object updaterecord(Object... obj) throws Throwable {
        networkdefaultcolumnsDTOInter RecordToUpdate = (networkdefaultcolumnsDTOInter) obj[0];
        networkdefaultcolumnsDAOInter engin = DAOFactory.createnetworkdefaultcolumnsDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        networkdefaultcolumnsDAOInter engin = DAOFactory.createnetworkdefaultcolumnsDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        networkdefaultcolumnsDTOInter RecordToDelete = (networkdefaultcolumnsDTOInter) obj[0];
        networkdefaultcolumnsDAOInter engin = DAOFactory.createnetworkdefaultcolumnsDAO();
        return engin.deleterecord(RecordToDelete);

    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        networksDTOInter SearchRecords = (networksDTOInter) obj[0];
        networkdefaultcolumnsDAOInter engin = DAOFactory.createnetworkdefaultcolumnsDAO();
        List<networkdefaultcolumnsDTOInter> AllRecords = (List<networkdefaultcolumnsDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetListOfcols(Object... obj) throws Throwable {
        networksDTOInter SearchRecords = (networksDTOInter) obj[0];
        networkdefaultcolumnsDAOInter engin = DAOFactory.createnetworkdefaultcolumnsDAO();
        List<String> AllRecords = (List<String>) engin.findcolsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        networkdefaultcolumnsDAOInter engin = DAOFactory.createnetworkdefaultcolumnsDAO();
        List<networkdefaultcolumnsDTOInter> AllRecords = (List<networkdefaultcolumnsDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        networkdefaultcolumnsDTOInter SearchParameter = (networkdefaultcolumnsDTOInter) obj[0];
        networkdefaultcolumnsDAOInter engin = DAOFactory.createnetworkdefaultcolumnsDAO();
        networkdefaultcolumnsDTOInter Record = (networkdefaultcolumnsDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
