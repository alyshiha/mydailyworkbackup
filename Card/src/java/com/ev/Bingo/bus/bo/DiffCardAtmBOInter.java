/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.ATMMACHINECASHDTOINTER;
import com.ev.Bingo.bus.dto.DiffCardAtmJournalDTOInter;
import com.ev.Bingo.bus.dto.DiffCardAtmTransJournalDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface DiffCardAtmBOInter {

    List<DiffCardAtmTransJournalDTOInter> findDetailRecords(String DIFFID) throws Throwable;

    List<ATMMACHINECASHDTOINTER> findMachineAll() throws Throwable;

    List<DiffCardAtmJournalDTOInter> findMasterRecords(String ATMID, String INDICATION, String JournalType, Date From, Date TO) throws Throwable;
void recalc() throws Throwable;
    String exportexcel(DiffCardAtmJournalDTOInter[] SearchRecords) throws Throwable;

    Object GetReport(String user, Date DateFrom, Date DateTo, String customer, String ATMID, String Journal, String Indication, DiffCardAtmJournalDTOInter[] entities) throws Throwable;
}
