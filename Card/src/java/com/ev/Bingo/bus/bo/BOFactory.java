package com.ev.Bingo.bus.bo;

public class BOFactory {

    public BOFactory() {
    }

    public static ProfitReportBOInter createProfitReportBO(Object... obj) {
        return new ProfitReportBO();
    }

    public static VisaBOInter createVisaBO(Object... obj) {
        return new VisaBO();
    }

    public static masterBOInter createmasterBO(Object... obj) {
        return new masterBO();
    }

    public static settlementsBOInter createsettlementsBO(Object... obj) {
        return new settlementsBO();
    }

    public static RejectedVisaBOInter createRejectedVisaBO(Object... obj) {
        return new RejectedVisaBO();
    }

    public static HolidaysBOInter createHolidaysBO(Object... obj) {
        return new HolidaysBO();
    }

    public static DiffCardAtmBOInter createDiffCardAtmBO(Object... obj) {
        return new DiffCardAtmBO();
    }

    public static AtmJournalBOInter createAtmJournalBO(Object... obj) {
        return new AtmJournalBO();
    }

    public static LogErrorBOInter createLogErrorBO(Object... obj) {
        return new LogErrorBO();
    }

    public static ATMMACHINEBOInter createATMMACHINEBO(Object... obj) {
        return new ATMMACHINEBO();
    }

    public static Col4BOInter createCol4BO(Object... obj) {
        return new Col4BO();
    }

    public static AtmAccountsBOInter createAtmAccountsBO(Object obj) {
        return new AtmAccountsBO();
    }

    public static privilegeStatusNameBOInter createprivilegeStatusNameBO(Object obj) {
        return new privilegeStatusNameBO();
    }

    public static NetworkTypeCodeBOInter createNetworkTypeCodeBO(Object obj) {
        return new NetworkTypeCodeBO();
    }

    public static BankCodeBOInter createBankCodeBO(Object... obj) {
        return new BankCodeBO();
    }

    public static AccountNameBOInter createAccountName(Object obj) {
        return new AccountNameBO();
    }

    public static CashManagmentLogBOInter createCashManagmentLogBO(Object obj) {
        return new CashManagmentLogBO();
    }

    public static userPrivilegeBOInter createuserPrivilegeBO(Object obj) {
        return new userPrivilegeBO();
    }

    public static commissionfeesBOInter createcommissionfeesBO(Object... obj) {
        return new commissionfeesBo();
    }

    public static commissionfeesdetailBoInter createcommissionfeesdetailBO(Object... obj) {
        return new commissionfeesdetailBo();
    }

    public static cardfiletemplatedetailBOInter createcardfiletemplatedetailBO(Object... obj) {
        return new cardfiletemplatedetailBO();
    }

    public static hostfiletemplatedetailBOInter createhostfiletemplatedetailBO(Object... obj) {
        return new hostfiletemplatedetailBO();
    }

    public static switchfiletemplatedetailBOInter createswitchfiletemplatedetailBO(Object... obj) {
        return new switchfiletemplatedetailBO();
    }

    public static filetemplateheaderBOInter createfiletemplateheaderBO(Object... obj) {
        return new filetemplateheaderBO();
    }

    public static networkfiletemplateheaderBOInter createnetworkfiletemplateheaderBO(Object... obj) {
        return new networkfiletemplateheaderBO();
    }

    public static switchfiletemplateheaderBOInter createswitchfiletemplateheaderBO(Object... obj) {
        return new switchfiletemplateheaderBO();
    }

    public static hostfiletemplateBOInter createhostfiletemplateBO(Object... obj) {
        return new hostfiletemplateBO();
    }

    public static switchfiletemplateBOInter createswitchfiletemplateBO(Object... obj) {
        return new switchfiletemplateBO();
    }

    public static cardfiletemplateBOInter createcardfiletemplateBO(Object... obj) {
        return new cardfiletemplateBO();
    }
//transactionstatisticsrepBOInter

    public static transactionstatisticsrepBOInter createtransactionstatisticsrepBO(Object... obj) {
        return new transactionstatisticsrepBo();
    }

    public static diffamountreportBOInter creatediffamountreportBo(Object... obj) {
        return new diffamountreportBo();
    }

    public static UserNetworkBOInter createUserNetworkBO(Object... obj) {
        return new UserNetworkBO();
    }

    public static transactionmanagementBOInter createtransactionmanagementBO(Object... obj) {
        return new transactionmanagementBo();
    }

    public static UserActivityBOInter createUserActivityBO(Object... obj) {
        return new UserActivityBO();
    }

    public static matchingtypeBoInter creatematchingtypeBo(Object... obj) {
        return new matchingtypeBo();
    }

    public static DAshBoardBOInter createDAshBoardBO(Object... obj) {
        return new DAshBoardBO();
    }

    public static DiffAmountBOInter createDiffAmountBO(Object... obj) {
        return new DiffAmountBO();
    }

    public static networkdefaultcolumnsBOInter createnetworkdefaultcolumnsBO(Object... obj) {
        return new networkdefaultcolumnsBo();
    }

    public static transsearchBOInter createtranssearchBO(Object... obj) {
        return new transsearchBO();
    }

    public static duplicatesettingdetailBOInter createduplicatesettingdetailBO(Object... obj) {
        return new duplicatesettingdetailBO();
    }

    public static settlementlogBOInter createsettlementlogBO(Object... obj) {
        return new settlementlogBo();
    }

    public static matchingerrordataBOInter creatematchingerrordataBO(Object... obj) {
        return new matchingerrordataBO();
    }

    public static duplicatesettingmasterBOInter createduplicatesettingmasterBO(Object... obj) {
        return new duplicatesettingmasterBO();
    }

    public static transactiontypeBOInter createtransactiontypeBO(Object... obj) {
        return new transactiontypeBO();
    }

    public static disputerulesBOInter createdisputerulesBO(Object... obj) {
        return new disputerulesBO();
    }

    public static filecolumndefinitionBOInter createfilecolumndefinitionBO(Object... obj) {
        return new filecolumndefinitionBO();
    }

    public static validationrulesBOInter createvalidationrulesBO(Object... obj) {
        return new validationrulesBO();
    }

    public static bingologBOInter createbingologBO(Object... obj) {
        return new bingologBO();
    }

    public static duplicatetransactionsBOInter createduplicatetransactionsBO(Object... obj) {
        return new duplicatetransactionsBO();
    }

    public static atmfileBOInter createatmfileBO(Object... obj) {
        return new atmfileBO();
    }

    public static networksBOInter createnetworksBO(Object... obj) {
        return new networksBO();
    }

    public static networkcodesBOInter createnetworkcodesBO(Object... obj) {
        return new networkcodesBO();
    }

    public static filecolumndefinitionBOInter createfilecolumndefinitionBo(Object... obj) {
        return new filecolumndefinitionBO();
    }

    public static blacklistedcardBOInter createblacklistedcardBO(Object... obj) {
        return new blacklistedcardBO();
    }

    public static currencymasterBOInter createcurrencymasterBO(Object... obj) {
        return new currencymasterBO();
    }

    public static columnssetupBOInter createcolumnssetupBO(Object... obj) {
        return new columnssetupBO();
    }

    public static columnssetupdetailsBOInter createcolumnssetupdetailsBO(Object... obj) {
        return new columnssetupdetailsBO();
    }

    public static JobMonitorBOInter createJobMonitorBO(Object obj) throws Throwable {
        return new JobMonitorBO();
    }

    public static currencyBOInter createcurrencyBO(Object... obj) {
        return new currencyBO();
    }

    public static transactiontypemasterBOInter createtransactiontypemasterBO(Object... obj) {
        return new transactiontypemasterBO();
    }

    public static copyfilesBOInter createcopyfilesBO(Object... obj) {
        return new copyfilesBO();
    }

    public static transactionresponsecodeBOInter createtransactionresponsecodeBO(Object... obj) {
        return new transactionresponsecodeBO();
    }

    public static transactionresponsemasterBOInter createtransactionresponsemasterBO(Object... obj) {
        return new transactionresponsemasterBO();
    }

    public static blockedusersBOInter createblockedusersBO(Object... obj) {
        return new blockedusersBO();
    }

    public static blockreasonsBOInter createblockreasonsBO(Object... obj) {
        return new blockreasonsBO();
    }

    public static LoginBOInter createLoginBO(Object... obj) {
        return new LoginBO();
    }

    public static profilemenuBOInter createprofilemenuBO(Object... obj) {
        return new profilemenuBO();
    }

    public static menulabelsBOInter createmenulabelsBO(Object... obj) {
        return new menulabelsBO();
    }

    public static userprofileBOInter createuserprofileBO(Object... obj) {
        return new userprofileBO();
    }

    public static userpassBOInter createuserpassBO(Object... obj) {
        return new userpassBO();
    }

    public static profileBOInter createprofileBO(Object... obj) {
        return new profileBO();
    }

    public static MainBoInter createMainBo(Object... obj) {
        return new MainBo();
    }

    public static systemtableBOInter createsystemtableBO(Object... obj) {
        return new systemtableBO();
    }

    public static licenseBOInter createlicenseBO(Object... obj) {
        return new licenseBO();
    }

    public static usersBOInter createusersBO(Object... obj) {
        return new usersBO();
    }

}
