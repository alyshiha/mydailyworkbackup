package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.userpassDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.userpassDTOInter;

public class userpassBO extends BaseBO implements userpassBOInter {

    protected userpassBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        userpassDTOInter RecordToInsert = (userpassDTOInter) obj[0];
        userpassDAOInter engin = DAOFactory.createuserpassDAO();

        return engin.insertrecord(RecordToInsert);
    }

    public Object GetRecord(Object... obj) throws Throwable {
        userpassDTOInter SearchParameter = (userpassDTOInter) obj[0];
        userpassDAOInter engin = DAOFactory.createuserpassDAO();
        userpassDTOInter Record = (userpassDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
