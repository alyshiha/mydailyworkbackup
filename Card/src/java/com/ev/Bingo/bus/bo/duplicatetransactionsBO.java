package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.duplicatetransactionsDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dto.atmfileDTOInter;
import java.util.List;
import com.ev.Bingo.bus.dto.duplicatetransactionsDTOInter;

public class duplicatetransactionsBO extends BaseBO implements duplicatetransactionsBOInter {

    protected duplicatetransactionsBO() {
        super();
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        atmfileDTOInter SearchRecords = (atmfileDTOInter) obj[0];
        duplicatetransactionsDAOInter engin = DAOFactory.createduplicatetransactionsDAO();
        List<duplicatetransactionsDTOInter> AllRecords = (List<duplicatetransactionsDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }
}
