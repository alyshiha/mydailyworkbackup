package com.ev.Bingo.bus.bo;

import java.sql.SQLException;

public interface blockedusersBOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object GetAllRecords(Object... obj) throws Throwable;

    Object findRecordsListReasonID(Object... obj) throws Throwable;
}
