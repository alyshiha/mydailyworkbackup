package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.networksDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.text.SimpleDateFormat;

public class networksBO extends BaseBO implements networksBOInter {

    protected networksBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        networksDTOInter RecordToInsert = (networksDTOInter) obj[0];
        networksDAOInter engin = DAOFactory.createnetworksDAO();
        String msg = (String) engin.insertrecord(RecordToInsert);
        return msg + " Network Has Been Inserted";
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<networksDTOInter> entities = (List<networksDTOInter>) obj[0];
        networksDAOInter engin = DAOFactory.createnetworksDAO();
        String msg = (String) engin.save(entities);
        return msg;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        networksDTOInter RecordToDelete = (networksDTOInter) obj[0];
        networksDAOInter engin = DAOFactory.createnetworksDAO();
        String msg = (String) engin.deleterecord(RecordToDelete);
        return msg + " Network Has Been Deleted";
    }

    public Object GetAllUserRecords(Object... obj) throws Throwable {
        networksDAOInter engin = DAOFactory.createnetworksDAO();
        String User = (String) obj[0];
        List<networksDTOInter> AllRecords = (List<networksDTOInter>) engin.findUserRecordsAll(User);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        networksDAOInter engin = DAOFactory.createnetworksDAO();
        List<networksDTOInter> AllRecords = (List<networksDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

}
