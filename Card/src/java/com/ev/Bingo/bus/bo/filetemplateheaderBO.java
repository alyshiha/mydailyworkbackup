package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.filetemplateheaderDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dao.cardfiletemplatedetailDAOInter;
import com.ev.Bingo.bus.dto.cardfiletemplateDTOInter;
import com.ev.Bingo.bus.dto.filetemplateheaderDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.filetemplateheaderDTOInter;
import java.text.SimpleDateFormat;

public class filetemplateheaderBO extends BaseBO implements filetemplateheaderBOInter {

    protected filetemplateheaderBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        filetemplateheaderDTOInter RecordToInsert = (filetemplateheaderDTOInter) obj[0];
        String TableName = (String) obj[1];
        filetemplateheaderDAOInter engin = DAOFactory.createfiletemplateheaderDAO();
        return engin.insertrecord(RecordToInsert, TableName) + " Record Has Been Inserted";
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<filetemplateheaderDTOInter> entities = (List<filetemplateheaderDTOInter>) obj[0];
        String TableName = (String) obj[1];
        filetemplateheaderDAOInter engin = DAOFactory.createfiletemplateheaderDAO();
        return engin.save(entities,TableName);
    }

    public Object updaterecord(Object... obj) throws Throwable {
        filetemplateheaderDTOInter RecordToUpdate = (filetemplateheaderDTOInter) obj[0];
        filetemplateheaderDAOInter engin = DAOFactory.createfiletemplateheaderDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public String deleteallrecord(Object... obj) throws Throwable {
      cardfiletemplateDTOInter Record = (cardfiletemplateDTOInter) obj[0];
        String TableName = (String) obj[1];
        filetemplateheaderDAOInter engin = DAOFactory.createfiletemplateheaderDAO();
        return engin.deleteallrecord(Record,TableName);
    }

    public Object deleterecord(Object... obj) throws Throwable {
        filetemplateheaderDTOInter RecordToInsert = (filetemplateheaderDTOInter) obj[0];
        String TableName = (String) obj[1];
        filetemplateheaderDAOInter engin = DAOFactory.createfiletemplateheaderDAO();
        return engin.deleterecord(RecordToInsert, TableName) + " Record Has Been Deleted";
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        String TableName = (String) obj[0];
        cardfiletemplateDTOInter SearchTemplate = (cardfiletemplateDTOInter) obj[1];
        filetemplateheaderDAOInter engin = DAOFactory.createfiletemplateheaderDAO();
        List<filetemplateheaderDTOInter> AllRecords = (List<filetemplateheaderDTOInter>) engin.findRecordsList(TableName, SearchTemplate);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        filetemplateheaderDAOInter engin = DAOFactory.createfiletemplateheaderDAO();
        List<filetemplateheaderDTOInter> AllRecords = (List<filetemplateheaderDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        filetemplateheaderDTOInter SearchParameter = (filetemplateheaderDTOInter) obj[0];
        filetemplateheaderDAOInter engin = DAOFactory.createfiletemplateheaderDAO();
        filetemplateheaderDTOInter Record = (filetemplateheaderDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
