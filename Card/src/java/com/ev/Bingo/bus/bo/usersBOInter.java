package com.ev.Bingo.bus.bo;

import java.sql.SQLException;

public interface usersBOInter {

    Object findRecordsAlluserManag(Object... obj) throws Throwable;//userd in user mangement

    Object insertrecord(Object... obj) throws Throwable;//userd in user mangement

    Object save(Object... obj) throws SQLException, Throwable;//userd in user mangement

    Object updaterecord(Object... obj) throws Throwable;//Used In ChangePassword
    
     Boolean GetRecordpass(Object... obj) throws Throwable ;

    Object deleteallrecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;//userd in user mangement

    Object GetListOfRecords(Object... obj) throws Throwable;

    Object GetRecord(Object... obj) throws Throwable;

    Object GetRecordById(Object... obj) throws Throwable;//Used In ChangePassword

    Object GetAllRecords(Object... obj) throws Throwable; //Used In ChangePassword

    Object GetAllAssignToProfile(Object... obj) throws Throwable;//Used In Profile Assign

    Object GetAllUnAssignToProfile(Object... obj) throws Throwable;//Used In Profile Assign

    Object updateStatus(Object... obj) throws Throwable;//used in login

    Object findAllBlockRecords(Object... obj) throws Throwable;//Used In Block User 

    Object findAllUnBlockRecords(Object... obj) throws Throwable;//Used In Block User 
}
