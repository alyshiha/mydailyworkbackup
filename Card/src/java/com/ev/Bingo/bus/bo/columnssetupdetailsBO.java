package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.columnssetupdetailsDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.columnssetupdetailsDTOInter;
import java.util.ArrayList;
import com.ev.Bingo.bus.dto.columnssetupDTOInter;
import java.util.List;
import com.ev.Bingo.bus.dto.columnssetupdetailsDTOInter;
import java.text.SimpleDateFormat;

public class columnssetupdetailsBO extends BaseBO implements columnssetupdetailsBOInter {

    protected columnssetupdetailsBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        columnssetupdetailsDTOInter RecordToInsert = (columnssetupdetailsDTOInter) obj[0];
        columnssetupdetailsDAOInter engin = DAOFactory.createcolumnssetupdetailsDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<columnssetupdetailsDTOInter> entities = (List<columnssetupdetailsDTOInter>) obj[0];
        columnssetupdetailsDAOInter engin = DAOFactory.createcolumnssetupdetailsDAO();
        return (String) engin.save(entities);
    }

    public Object deleterecord(Object... obj) throws Throwable {
        columnssetupDTOInter RecordToDelete = (columnssetupDTOInter) obj[0];
        columnssetupdetailsDAOInter engin = DAOFactory.createcolumnssetupdetailsDAO();
        return engin.deleterecord(RecordToDelete);

    }

    public Object deleterecordDetail(Object... obj) throws Throwable {
        columnssetupdetailsDTOInter RecordToDelete = (columnssetupdetailsDTOInter) obj[0];
        columnssetupdetailsDAOInter engin = DAOFactory.createcolumnssetupdetailsDAO();
        return engin.deleterecorddetail(RecordToDelete);
    }

    public Object GetListOfRecordsCS(Object... obj) throws Throwable {
        columnssetupDTOInter SearchRecords = (columnssetupDTOInter) obj[0];
        columnssetupdetailsDAOInter engin = DAOFactory.createcolumnssetupdetailsDAO();
        List<columnssetupdetailsDTOInter> AllRecords = (List<columnssetupdetailsDTOInter>) engin.findRecordsListCS(SearchRecords);
        return AllRecords;
    }

}
