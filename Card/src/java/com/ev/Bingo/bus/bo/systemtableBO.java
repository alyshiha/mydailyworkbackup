package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.systemtableDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.systemtableDTOInter;

public class systemtableBO extends BaseBO implements systemtableBOInter {

    protected systemtableBO() {
        super();
    }
 public Object GetExportPath(String Path) throws Throwable {
       
        systemtableDAOInter engin = DAOFactory.createsystemtableDAO();
        systemtableDTOInter Record = (systemtableDTOInter) engin.findRecordexport(Path);
        return Record;
    }
    public Object GetRecord(Object... obj) throws Throwable {
        systemtableDTOInter SearchParameter = (systemtableDTOInter) obj[0];
        systemtableDAOInter engin = DAOFactory.createsystemtableDAO();
        systemtableDTOInter Record = (systemtableDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        systemtableDAOInter engin = DAOFactory.createsystemtableDAO();
        List<systemtableDTOInter> AllRecords = (List<systemtableDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        systemtableDTOInter RecordToDelete = (systemtableDTOInter) obj[0];
        systemtableDAOInter engin = DAOFactory.createsystemtableDAO();
        return engin.deleterecord(RecordToDelete);
    }

    public Object insertrecord(Object... obj) throws Throwable {
        systemtableDTOInter RecordToInsert = (systemtableDTOInter) obj[0];
        systemtableDAOInter engin = DAOFactory.createsystemtableDAO();
        return engin.insertrecord(RecordToInsert);

    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<systemtableDTOInter> entities = (List<systemtableDTOInter>) obj[0];
        systemtableDAOInter engin = DAOFactory.createsystemtableDAO();
        return engin.save(entities);

    }
}
