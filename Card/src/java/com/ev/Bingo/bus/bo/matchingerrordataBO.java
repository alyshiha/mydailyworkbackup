/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.matchingerrorsdataDAOInter;
import com.ev.Bingo.bus.dto.disputesDTOInter;
import com.ev.Bingo.bus.dto.matchingerrorsdataDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Aly
 */
public class matchingerrordataBO implements matchingerrordataBOInter {

    public Object GetListOfRecords(Object... obj) throws Throwable {
        matchingerrorsdataDTOInter SearchRecords = (matchingerrorsdataDTOInter) obj[0];
        matchingerrorsdataDAOInter engin = DAOFactory.creatematchingerrorsdataDAO();
        List<disputesDTOInter> AllRecords = (List<disputesDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(usersDTOInter UserDTO, Date loadingfrom, Date loadingto, Integer network, Integer recordtypeid) throws Throwable {
        matchingerrorsdataDAOInter engin = DAOFactory.creatematchingerrorsdataDAO();
        List<matchingerrorsdataDTOInter> AllRecords = (List<matchingerrorsdataDTOInter>) engin.findRecordsAll(UserDTO, loadingfrom, loadingto, network, recordtypeid);
        return AllRecords;
    }

    public void MarkAsMatched(Object... obj) throws Throwable {
        matchingerrorsdataDTOInter RecordToInsert = (matchingerrorsdataDTOInter) obj[0];
        disputesDTOInter SecondRecordToInsert = (disputesDTOInter) obj[1];
        matchingerrorsdataDAOInter engin = DAOFactory.creatematchingerrorsdataDAO();
        engin.MarkAsMatched(RecordToInsert, SecondRecordToInsert);
    }

    public void MarkAsDispute(Object... obj) throws Throwable {
        matchingerrorsdataDTOInter RecordToInsert = (matchingerrorsdataDTOInter) obj[0];
        disputesDTOInter SecondRecordToInsert = (disputesDTOInter) obj[1];
        usersDTOInter user = (usersDTOInter) obj[2];
        matchingerrorsdataDAOInter engin = DAOFactory.creatematchingerrorsdataDAO();
        engin.MarkAsDispute(RecordToInsert, SecondRecordToInsert, user);
    }
}
