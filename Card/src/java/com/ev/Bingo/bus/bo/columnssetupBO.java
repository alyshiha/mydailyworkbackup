package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.columnssetupDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.columnssetupDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.columnssetupDTOInter;
import com.ev.Bingo.bus.dto.columnssetupdetailsDTOInter;
import java.text.SimpleDateFormat;

public class columnssetupBO extends BaseBO implements columnssetupBOInter {

    protected columnssetupBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        columnssetupDTOInter RecordToInsert = (columnssetupDTOInter) obj[0];
        columnssetupDAOInter engin = DAOFactory.createcolumnssetupDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<columnssetupDTOInter> entities = (List<columnssetupDTOInter>) obj[0];
        columnssetupDAOInter engin = DAOFactory.createcolumnssetupDAO();
        return (String) engin.save(entities);
    }

    public Object deleterecord(Object... obj) throws Throwable {
        columnssetupDTOInter RecordToDelete = (columnssetupDTOInter) obj[0];
        columnssetupDAOInter engin = DAOFactory.createcolumnssetupDAO();
        return engin.deleterecord(RecordToDelete);
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        columnssetupDAOInter engin = DAOFactory.createcolumnssetupDAO();
        List<columnssetupDTOInter> AllRecords = (List<columnssetupDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

}
