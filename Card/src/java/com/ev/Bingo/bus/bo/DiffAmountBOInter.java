/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

/**
 *
 * @author AlyShiha
 */
public interface DiffAmountBOInter {

    Object GetReport(Object... obj) throws Throwable;

    Object GetAllRecords(Object... obj) throws Throwable;

    Object findfileall(Object... obj) throws Throwable;
    
    void recalc() throws Throwable ;
}
