/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.LoginDAOInter;
import com.ev.Bingo.bus.dao.menulabelsDAOInter;
import com.ev.Bingo.bus.dao.userprofileDAOInter;
import com.ev.Bingo.bus.dto.menulabelsDTOInter;
import com.ev.Bingo.bus.dto.userprofileDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class MainBo extends BaseBO implements MainBoInter {

    public List<menulabelsDTOInter> getChildMenu(List<menulabelsDTOInter> Menu, Integer Parent) throws Throwable {

        List<menulabelsDTOInter> Result = new ArrayList<menulabelsDTOInter>();
        for (menulabelsDTOInter MenuItem : Menu) {
            if (MenuItem.getparentid() == Parent) {
                Result.add(MenuItem);
            }

        }

        return Result;
    }

    public Boolean ValidatePage(int userID, String page) throws Throwable {
        menulabelsDAOInter LDAO = DAOFactory.createmenulabelsDAO();
        return LDAO.findmenuitemcheck(userID, page);
    }

    public String generateMenu(usersDTOInter userId) throws Throwable {
        userprofileDAOInter upDAO = DAOFactory.createuserprofileDAO();
        userprofileDTOInter upDTO = (userprofileDTOInter) upDAO.findRecord(userId);
        //      profilemenuDAOInter pmiDAO = DAOFactory.createprofilemenuDAO();
// List<profilemenuDTOInter> pmiDTOL = (List<profilemenuDTOInter>) pmiDAO.findRecordsList(upDTO.getprofileid());
        menulabelsDAOInter mlDAO = DAOFactory.createmenulabelsDAO();
        List<menulabelsDTOInter> Menu = (List<menulabelsDTOInter>) mlDAO.findmenu(userId.getUserid());
        String menuItems = "";
        List<menulabelsDTOInter> mlParent = (List<menulabelsDTOInter>) mlDAO.findParents(upDTO.getprofileid());
        Integer Parent = 0;
        for (menulabelsDTOInter d : mlParent) {
            if (!d.getenglishlabel().contains("DashBoard")) {
                Parent = d.getmenuid();

                if (getChildMenu(Menu, Parent).size() != 0) {
                    //----------second level--------------------------
                    menuItems += " <li class=\"parent\">";
                    menuItems += " <a href=\"#\" $page>$label</a>";
                    menuItems += " <ul class=\"sub-menu\">";
                    if (d.getpage() != null) {
                        menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('" + d.getpage() + "');\"");
                    } else {
                        menuItems = menuItems.replace("$page", " ");
                    }
                    menuItems = menuItems.replace("$label", d.getenglishlabel());

                    for (menulabelsDTOInter chi : getChildMenu(Menu, Parent)) {

                        //------------------------------------------------
                        if (!getChildMenu(Menu, Parent).isEmpty()) {
                            //-----------third and the last level--------------------
                            Parent = chi.getmenuid();
                            menuItems += " <li><a class=\"parent\" href=\"#\" $page><i class=\"icon-file-alt\"></i> $childLabel</a> <ul class=\"sub-menu\">";
                            menuItems = menuItems.replace("$childLabel", chi.getenglishlabel());
                            if (chi.getpage() != null) {
                                menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('" + chi.getpage() + "');\"");
                            } else {
                                menuItems = menuItems.replace("$page", " ");
                            }
                            for (menulabelsDTOInter chiOf : getChildMenu(Menu, Parent)) {
                                menuItems += "<li><a href=\"#\" $page>$childLabel</a></li>";
                                if (chiOf.getpage() != null) {
                                    menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('" + chiOf.getpage() + "');\"");
                                } else {
                                    menuItems = menuItems.replace("$page", " ");
                                }

                                menuItems = menuItems.replace("$childLabel", chiOf.getenglishlabel());

                            }
                            menuItems += "</ul></li>";
                        } else {
                            Parent = chi.getmenuid();
                            menuItems += "<li><a href=\"#\" $page><i class=\"icon-wrench\"></i> $childLabel</a></li>";
                            if (chi.getpage() != null) {
                                menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('" + chi.getpage() + "');\"");
                            } else {
                                menuItems = menuItems.replace("$page", " ");
                            }

                            menuItems = menuItems.replace("$childLabel", chi.getenglishlabel());
                        }
                    }
                    menuItems += "</ul></li>";

                } else {
                    //--------first level------------------------------------
                    menuItems += "<li><a href=\"#\" $page>$label</a></li>";
                    if (d.getpage() != null) {
                        menuItems = menuItems.replace("$page", "onClick=\"handleIFRAME('" + d.getpage() + "');\"");
                    } else {
                        menuItems = menuItems.replace("$page", " ");
                    }
                    menuItems = menuItems.replace("$label", d.getenglishlabel());
                    //----------------------------------------------------------------
                }
            }
        }

        return menuItems;
    }
}
