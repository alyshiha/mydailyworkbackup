package com.ev.Bingo.bus.bo;

import java.sql.SQLException;

public interface cardfiletemplatedetailBOInter {

    Object insertrecord(Object... obj) throws Throwable;

    int save(Object... obj) throws SQLException, Throwable;

    Object findRecordsListUpdate(int seq) throws Throwable;

    int updatetemplate(int seq, int TemplateID, int networktype) throws Throwable;

    Object updaterecord(Object... obj) throws Throwable;

    String deleteallrecord(Object... obj) throws Throwable;

    

    Object GetListOfRecords(Object... obj) throws Throwable;

    Object GetRecord(Object... obj) throws Throwable;

    Object GetAllRecords(Object... obj) throws Throwable;
}
