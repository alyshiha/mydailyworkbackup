/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.TranSearchFieldDTOInter;
import com.ev.Bingo.bus.dto.currencymasterDTOInter;
import com.ev.Bingo.bus.dto.disputesDTOInter;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import com.ev.Bingo.bus.dto.transactionresponsecodeDTOInter;
import com.ev.Bingo.bus.dto.transactiontypeDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import com.ev.Bingo.bus.dto.validationrulesDTOInter;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author Aly
 */
public interface transsearchBOInter {
    void correctiveamountupdate(Object... obj) throws Throwable;
void exportRecord(Object... obj) throws Throwable ;
    Integer savePrint(Object... obj) throws Throwable;
Object UpdatecorrectiveRecord(Object... obj) throws Throwable;
    Object UpdatesetteledRecord(Object... obj) throws Throwable;

    Object GetReport(Object... obj) throws Throwable;

    Boolean ValidateDates(TranSearchFieldDTOInter searchfields, String Page) throws Throwable;

    List<filecolumndefinitionDTOInter> getcolumnselect(String[] Selectedcol, List<filecolumndefinitionDTOInter> coloumns);

    String[] WhereCondition(TranSearchFieldDTOInter searchfields, List<currencymasterDTOInter> currencys, List<usersDTOInter> userslist, List<transactiontypeDTOInter> transtypelist, List<transactionresponsecodeDTOInter> resplist, List<networksDTOInter> networklist, String[] selectedCol, String Page, usersDTOInter UserDTO, List<validationrulesDTOInter> validationrulelist) throws Throwable;

    List<disputesDTOInter> getselecteditems(String Statment) throws Throwable;

    Object findotherside(Object... obj) throws Throwable;

    Object findothersideMatched(Object... obj) throws Throwable;

    Object UpdateCommentRecord(Object... obj) throws Throwable;

}
