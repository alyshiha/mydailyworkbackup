package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.settlementlogDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.disputesDTOInter;
import com.ev.Bingo.bus.dto.settlementlogDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.settlementlogDTOInter;
import com.ev.Bingo.bus.dto.settlementlogsearchDTOInter;
import java.text.SimpleDateFormat;

public class settlementlogBo extends BaseBO implements settlementlogBOInter {

    protected settlementlogBo() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        disputesDTOInter RecordToInsert = (disputesDTOInter) obj[0];
        settlementlogDAOInter engin = DAOFactory.createsettlementlogDAO();
        engin.insertrecord(RecordToInsert);
        return "Insert Done";
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        settlementlogsearchDTOInter SearchRecords = (settlementlogsearchDTOInter) obj[0];
        settlementlogDAOInter engin = DAOFactory.createsettlementlogDAO();
        List<settlementlogDTOInter> AllRecords = (List<settlementlogDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

}
