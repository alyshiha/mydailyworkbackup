package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.duplicatesettingdetailDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.duplicatesettingdetailDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.duplicatesettingdetailDTOInter;
import com.ev.Bingo.bus.dto.duplicatesettingmasterDTOInter;
import java.text.SimpleDateFormat;

public class duplicatesettingdetailBO extends BaseBO implements duplicatesettingdetailBOInter {

    protected duplicatesettingdetailBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        duplicatesettingdetailDTOInter RecordToInsert = (duplicatesettingdetailDTOInter) obj[0];
        duplicatesettingdetailDAOInter engin = DAOFactory.createduplicatesettingdetailDAO();
        return engin.insertrecord(RecordToInsert);

    }

    public Object deleterecord(Object... obj) throws Throwable {
        duplicatesettingmasterDTOInter RecordToDelete = (duplicatesettingmasterDTOInter) obj[0];
        duplicatesettingdetailDAOInter engin = DAOFactory.createduplicatesettingdetailDAO();
        return engin.deleterecord(RecordToDelete);

    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        duplicatesettingmasterDTOInter SearchRecords = (duplicatesettingmasterDTOInter) obj[0];
        duplicatesettingdetailDAOInter engin = DAOFactory.createduplicatesettingdetailDAO();
        List<duplicatesettingdetailDTOInter> AllRecords = (List<duplicatesettingdetailDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<duplicatesettingmasterDTOInter> entities = (List<duplicatesettingmasterDTOInter>) obj[0];
        duplicatesettingdetailDAOInter engin = DAOFactory.createduplicatesettingdetailDAO();
        return (String) engin.save(entities);
    }
}
