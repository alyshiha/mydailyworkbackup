/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.ExceptionLoadingErrorsDTOInter;
import java.util.Date;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface LogErrorBOInter {

    Object printExceptionLoadingErrors(String user, Date dateF, Date dateT, String customer) throws Throwable;

    List<ExceptionLoadingErrorsDTOInter> runReportExceptionLoadingErrorsexcel(String dateFrom, String dateTo) throws Throwable;
    
}
