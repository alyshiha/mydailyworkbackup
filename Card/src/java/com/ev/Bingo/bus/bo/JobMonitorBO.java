/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.FilesToMoveDAOInter;
import com.ev.Bingo.bus.dao.JobsStatusDAOInter;
import com.ev.Bingo.bus.dto.ErrorsMonitorDTOInter;
import com.ev.Bingo.bus.dto.FilesToMoveDTOInter;
import com.ev.Bingo.bus.dto.JobsStatusDTOInter;
import com.ev.Bingo.bus.dto.LoadingMonitorDTOInter;
import com.ev.Bingo.bus.dto.MatchingMonitorDTOInter;
import com.ev.Bingo.bus.dto.ValidationMonitorDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class JobMonitorBO extends BaseBO implements JobMonitorBOInter {

    protected JobMonitorBO() {
        super();
    }

    public Object getLoadingMonitor() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        LoadingMonitorDTOInter lmDTO = (LoadingMonitorDTOInter) ftmDAO.getLoadingMonitor();
        return lmDTO;
    }

    public Object getValidationMonitor() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        ValidationMonitorDTOInter lmDTO = (ValidationMonitorDTOInter) ftmDAO.getValidationMonitor();
        return lmDTO;
    }

    public Object getMatchingMonitor() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        MatchingMonitorDTOInter lmDTO = (MatchingMonitorDTOInter) ftmDAO.getMatchingMonitor();
        return lmDTO;
    }

    public Object getErrorMonitor() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        ErrorsMonitorDTOInter lmDTO = (ErrorsMonitorDTOInter) ftmDAO.getErrorsMonitor();
        return lmDTO;
    }

    public int getActiveFiles() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        int i = ftmDAO.findActiveFiles();
        return i;
    }

    public int getFilesToBuckup() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        int i = ftmDAO.findFilesToBuckup();
        return i;
    }

    public Object rematch() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        return ftmDAO.rematch();
    }

    public Object getFilesToMove() throws Throwable {
        FilesToMoveDAOInter ftmDAO = DAOFactory.createFilesToMoveDAO(null);
        List<FilesToMoveDTOInter> ftmDTOL = (List<FilesToMoveDTOInter>) ftmDAO.findAll();
        return ftmDTOL;
    }

    public void disableJob(boolean status, int process) throws Throwable {
        JobsStatusDAOInter jsDAO = DAOFactory.createJobsStatusDAO(null);
        jsDAO.disableJob(status, process);
    }

    public boolean getStatus(int process) throws Throwable {
        JobsStatusDAOInter jsDAO = DAOFactory.createJobsStatusDAO(null);
        List<JobsStatusDTOInter> jsDTOL = (List<JobsStatusDTOInter>) jsDAO.findAll();
        JobsStatusDTOInter jsDTO = jsDTOL.get(0);
        if (process == 1) {
            if (jsDTO.getLoading() == 1) {
                return false;
            } else {
                return true;
            }
        } else if (process == 2) {
            if (jsDTO.getJournalValidation() == 1) {
                return false;
            } else {
                return true;
            }
        } else if (process == 3) {
            if (jsDTO.getSwitchValidation() == 1) {
                return false;
            } else {
                return true;
            }
        } else if (process == 4) {
            if (jsDTO.getHostValidation() == 1) {
                return false;
            } else {
                return true;
            }
        } else if (process == 5) {
            if (jsDTO.getMatching() == 1) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public static void main(String[] args) throws Throwable {
    }
}
