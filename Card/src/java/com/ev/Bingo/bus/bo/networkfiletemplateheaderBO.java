package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.networkfiletemplateheaderDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.networkfiletemplateheaderDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.networkfiletemplateheaderDTOInter;
import java.text.SimpleDateFormat;

public class networkfiletemplateheaderBO extends BaseBO implements networkfiletemplateheaderBOInter {

    protected networkfiletemplateheaderBO() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        networkfiletemplateheaderDTOInter RecordToInsert = (networkfiletemplateheaderDTOInter) obj[0];
        networkfiletemplateheaderDAOInter engin = DAOFactory.createnetworkfiletemplateheaderDAO();
        engin.insertrecord(RecordToInsert);
        return "Insert Done";
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<networkfiletemplateheaderDTOInter> entities = (List<networkfiletemplateheaderDTOInter>) obj[0];
        networkfiletemplateheaderDAOInter engin = DAOFactory.createnetworkfiletemplateheaderDAO();
        engin.save(entities);
        return "Insert Done";
    }

    public Object updaterecord(Object... obj) throws Throwable {
        networkfiletemplateheaderDTOInter RecordToUpdate = (networkfiletemplateheaderDTOInter) obj[0];
        networkfiletemplateheaderDAOInter engin = DAOFactory.createnetworkfiletemplateheaderDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        networkfiletemplateheaderDAOInter engin = DAOFactory.createnetworkfiletemplateheaderDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        networkfiletemplateheaderDTOInter RecordToDelete = (networkfiletemplateheaderDTOInter) obj[0];
        networkfiletemplateheaderDAOInter engin = DAOFactory.createnetworkfiletemplateheaderDAO();
        engin.deleterecord(RecordToDelete);
        return "record has been deleted";
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<networkfiletemplateheaderDTOInter> SearchRecords = (List<networkfiletemplateheaderDTOInter>) obj[0];
        networkfiletemplateheaderDAOInter engin = DAOFactory.createnetworkfiletemplateheaderDAO();
        List<networkfiletemplateheaderDTOInter> AllRecords = (List<networkfiletemplateheaderDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        networkfiletemplateheaderDAOInter engin = DAOFactory.createnetworkfiletemplateheaderDAO();
        List<networkfiletemplateheaderDTOInter> AllRecords = (List<networkfiletemplateheaderDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        networkfiletemplateheaderDTOInter SearchParameter = (networkfiletemplateheaderDTOInter) obj[0];
        networkfiletemplateheaderDAOInter engin = DAOFactory.createnetworkfiletemplateheaderDAO();
        networkfiletemplateheaderDTOInter Record = (networkfiletemplateheaderDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
