/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.DiffAmountDAOInter;
import com.ev.Bingo.bus.dao.ProfitReportDAOInter;
import com.ev.Bingo.bus.dto.AtmMachineDTOInter;
import com.ev.Bingo.bus.dto.OffusDTOInter;
import com.ev.Bingo.bus.dto.OffusTotalsDTOInter;
import com.ev.Bingo.bus.dto.OnusDTOInter;
import com.ev.Bingo.bus.dto.OnusTotalsDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusInterDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusInterTotalsDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusTotalsDTOInter;
import com.ev.Bingo.bus.dto.atmfileDTOInter;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author shi7a
 */
public class ProfitReportBO implements ProfitReportBOInter {

    private Object GetReportAll(Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID, String userName, String customer, String Report, String atmid) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", userName);
        params.put("customer", customer);
        params.put("AtmID", ATMID);
        params.put("ATMNAME", atmid);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(TransFromDate));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(TransToDate));
        params.put("SettFrom", DateFormatter.changeDateAndTimeFormat(SettFromDate));
        params.put("SettTo", DateFormatter.changeDateAndTimeFormat(SettToDate));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load(Report);

            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri = "Profit" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }

    }

    @Override
    public String findReport(String Type, Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID, String userName, String customer, String atmid) throws Throwable {
        if (Type.equals("ONUS")) {
            return (String) GetReportAll(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID, userName, customer, "C:/CardsReports/ProfitOnus.jrxml", atmid);
        } else if (Type.equals("OFFUS")) {
            return (String) GetReportAll(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID, userName, customer, "C:/CardsReports/ProfitOffus.jrxml", atmid);
        } else {
            return (String) GetReportAll(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID, userName, customer, "C:/CardsReports/Profit.jrxml", atmid);
        }
    }

    @Override
    public Hashtable findfileall(String Type, Date TransFromDate, Date TransToDate, Date SettFromDate, Date SettToDate, String ATMID) throws Throwable {
        Hashtable AllRecords = new Hashtable();
        ProfitReportDAOInter engin = DAOFactory.createProfitReportDAO();
        if (Type.equals("ONUS")) {
            List<OnusDTOInter> AllOnusRecords = (List<OnusDTOInter>) engin.findOnusTransactions(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            List<OnusTotalsDTOInter> AllOnusTotalsRecords = (List<OnusTotalsDTOInter>) engin.findOnusTransactionstotals(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            AllRecords.put("OnusRecords", AllOnusRecords);
            AllRecords.put("OnusRecordsTotals", AllOnusTotalsRecords);
        } else if (Type.equals("OFFUS")) {
            List<OffusDTOInter> AllOffusRecords = (List<OffusDTOInter>) engin.find123OffusTransactions(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            List<OffusTotalsDTOInter> AllOffusTotalsRecords = (List<OffusTotalsDTOInter>) engin.find123OffusTransactionstotals(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            List<VisaOffusInterDTOInter> AllVisaOffusInternationalRecords = (List<VisaOffusInterDTOInter>) engin.findVisaOffusInternationalTransactions(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            List<VisaOffusInterTotalsDTOInter> AllVisaOffusInternationalTotalsRecords = (List<VisaOffusInterTotalsDTOInter>) engin.findVisaOffusInternationalTransactionstotals(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            List<VisaOffusDTOInter> AllVisaOffusRecords = (List<VisaOffusDTOInter>) engin.findVisaOffusNationalTransactions(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            List<VisaOffusTotalsDTOInter> AllVisaOffusTotalsRecords = (List<VisaOffusTotalsDTOInter>) engin.findVisaOffusNationalTransactionstotals(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            AllRecords.put("OffusRecords", AllOffusRecords);
            AllRecords.put("OffusRecordsTotals", AllOffusTotalsRecords);
            AllRecords.put("VisaOffusInternationalRecords", AllVisaOffusInternationalRecords);
            AllRecords.put("VisaOffusInternationalRecordsTotals", AllVisaOffusInternationalTotalsRecords);
            AllRecords.put("VisaOffusRecords", AllVisaOffusRecords);
            AllRecords.put("VisaOffusRecordsTotals", AllVisaOffusTotalsRecords);
        } else {
            List<OnusDTOInter> AllOnusRecords = (List<OnusDTOInter>) engin.findOnusTransactions(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            List<OnusTotalsDTOInter> AllOnusTotalsRecords = (List<OnusTotalsDTOInter>) engin.findOnusTransactionstotals(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            List<OffusDTOInter> AllOffusRecords = (List<OffusDTOInter>) engin.find123OffusTransactions(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            List<OffusTotalsDTOInter> AllOffusTotalsRecords = (List<OffusTotalsDTOInter>) engin.find123OffusTransactionstotals(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            List<VisaOffusInterDTOInter> AllVisaOffusInternationalRecords = (List<VisaOffusInterDTOInter>) engin.findVisaOffusInternationalTransactions(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            List<VisaOffusInterTotalsDTOInter> AllVisaOffusInternationalTotalsRecords = (List<VisaOffusInterTotalsDTOInter>) engin.findVisaOffusInternationalTransactionstotals(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            List<VisaOffusDTOInter> AllVisaOffusRecords = (List<VisaOffusDTOInter>) engin.findVisaOffusNationalTransactions(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            List<VisaOffusTotalsDTOInter> AllVisaOffusTotalsRecords = (List<VisaOffusTotalsDTOInter>) engin.findVisaOffusNationalTransactionstotals(TransFromDate, TransToDate, SettFromDate, SettToDate, ATMID);
            AllRecords.put("OnusRecords", AllOnusRecords);
            AllRecords.put("OnusRecordsTotals", AllOnusTotalsRecords);
            AllRecords.put("OffusRecords", AllOffusRecords);
            AllRecords.put("OffusRecordsTotals", AllOffusTotalsRecords);
            AllRecords.put("VisaOffusInternationalRecords", AllVisaOffusInternationalRecords);
            AllRecords.put("VisaOffusInternationalRecordsTotals", AllVisaOffusInternationalTotalsRecords);
            AllRecords.put("VisaOffusRecords", AllVisaOffusRecords);
            AllRecords.put("VisaOffusRecordsTotals", AllVisaOffusTotalsRecords);
        }
        return AllRecords;
    }

    @Override
    public List<AtmMachineDTOInter> GetAtmMachineList() throws Throwable {
        ProfitReportDAOInter engin = DAOFactory.createProfitReportDAO();
        List<AtmMachineDTOInter> AllRecords = (List<AtmMachineDTOInter>) engin.findAtmMachineList();
        return AllRecords;
    }
}
