package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.transactionmanagementDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.transactionmanagementDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.transactionmanagementDTOInter;
import java.text.SimpleDateFormat;

public class transactionmanagementBo extends BaseBO implements transactionmanagementBOInter {

    protected transactionmanagementBo() {
        super();
    }

    public Object insertrecord(Object... obj) throws Throwable {
        transactionmanagementDTOInter RecordToInsert = (transactionmanagementDTOInter) obj[0];
        transactionmanagementDAOInter engin = DAOFactory.createtransactionmanagementDAO();
        engin.insertrecord(RecordToInsert);
        return "Insert Done";
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<transactionmanagementDTOInter> entities = (List<transactionmanagementDTOInter>) obj[0];
        transactionmanagementDAOInter engin = DAOFactory.createtransactionmanagementDAO();
        engin.save(entities);
        return "Insert Done";
    }

    public Object updaterecord(Object... obj) throws Throwable {
        transactionmanagementDTOInter RecordToUpdate = (transactionmanagementDTOInter) obj[0];
        transactionmanagementDAOInter engin = DAOFactory.createtransactionmanagementDAO();
        engin.updaterecord(RecordToUpdate);
        return "Update Done";
    }

    public Object deleteallrecord(Object... obj) throws Throwable {
        transactionmanagementDAOInter engin = DAOFactory.createtransactionmanagementDAO();
        engin.deleteallrecord();
        return "Delete Complete";
    }

    public Object deleterecord(Object... obj) throws Throwable {
        transactionmanagementDTOInter RecordToDelete = (transactionmanagementDTOInter) obj[0];
        transactionmanagementDAOInter engin = DAOFactory.createtransactionmanagementDAO();
        engin.deleterecord(RecordToDelete);
        return "record has been deleted";
    }

    public Object GetListOfRecords(Object... obj) throws Throwable {
        List<transactionmanagementDTOInter> SearchRecords = (List<transactionmanagementDTOInter>) obj[0];
        transactionmanagementDAOInter engin = DAOFactory.createtransactionmanagementDAO();
        List<transactionmanagementDTOInter> AllRecords = (List<transactionmanagementDTOInter>) engin.findRecordsList(SearchRecords);
        return AllRecords;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        transactionmanagementDAOInter engin = DAOFactory.createtransactionmanagementDAO();
        transactionmanagementDTOInter AllRecords = (transactionmanagementDTOInter) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetRecord(Object... obj) throws Throwable {
        transactionmanagementDTOInter SearchParameter = (transactionmanagementDTOInter) obj[0];
        transactionmanagementDAOInter engin = DAOFactory.createtransactionmanagementDAO();
        transactionmanagementDTOInter Record = (transactionmanagementDTOInter) engin.findRecord(SearchParameter);
        return Record;
    }
}
