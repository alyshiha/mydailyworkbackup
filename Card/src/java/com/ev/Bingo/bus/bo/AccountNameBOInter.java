/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.AccountNameDTOInter;
import com.ev.Bingo.bus.dto.AccountNameDTOInter;
import java.util.List;

/**
 *
 * @author AlyShiha
 */
public interface AccountNameBOInter {

    Boolean findDuplicatesaveRecord(String Name, Integer id) throws Throwable;

    Boolean findDuplicateRecord(String Name) throws Throwable;
    Object printPDFrep(String customer,  String PrintedName ,String Account) throws Throwable;

    List<AccountNameDTOInter> getRecord(String Name) throws Throwable;

    String deleteNetworks(AccountNameDTOInter record);

    List<AccountNameDTOInter> getNetworks() throws Throwable;

    String insertNetworks(AccountNameDTOInter record);

    String updateNetworks(AccountNameDTOInter record);

}
