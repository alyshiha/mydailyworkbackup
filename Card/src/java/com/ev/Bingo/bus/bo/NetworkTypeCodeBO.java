
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;


import DBCONN.CoonectionHandler;
 
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.NetworkTypeCodeDAOInter;
import com.ev.Bingo.bus.dto.NetworkTypeCodeDTOInter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author AlyShiha
 */
public class NetworkTypeCodeBO implements NetworkTypeCodeBOInter {

    @Override
    public List<NetworkTypeCodeDTOInter> getNetworkTypeRecord(int network, String code) throws Throwable {
        NetworkTypeCodeDAOInter engin = DAOFactory.createNetworkTypeCodeDAO();
        List<NetworkTypeCodeDTOInter> records = engin.findRecord(network, code);
        return records;
    }

    @Override
    public List<NetworkTypeCodeDTOInter> getNetworkType() throws Throwable {
        NetworkTypeCodeDAOInter engin = DAOFactory.createNetworkTypeCodeDAO();
        List<NetworkTypeCodeDTOInter> records = engin.findAll();
        return records;
    }

    @Override
    public String insertNetworkType(NetworkTypeCodeDTOInter record) {
        try {
            NetworkTypeCodeDAOInter engin = DAOFactory.createNetworkTypeCodeDAO();
            engin.insert(record);
            return "Record Has Been Succesfully Added";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String updateNetworkType(NetworkTypeCodeDTOInter record) {
        try {
            NetworkTypeCodeDAOInter engin = DAOFactory.createNetworkTypeCodeDAO();
            engin.update(record);
            return "Record Has Been Succesfully Updates";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    @Override
    public String deleteNetworkType(NetworkTypeCodeDTOInter record) {
        try {
            NetworkTypeCodeDAOInter engin = DAOFactory.createNetworkTypeCodeDAO();
            engin.delete(record);
            return "Record Has Been Succesfully Deleted";
        } catch (Throwable ex) {
            return ex.getMessage();
        }
    }

    public Object printPDFrep(String customer, String PrintedName, int network, String networksearch, String networkvalue) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("customer", customer);
        params.put("userPrint", PrintedName);
        params.put("networkid", new BigDecimal(network));
        params.put("network", "%"+networksearch +"%");
        params.put("networkvalue", networkvalue);

        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("c:/CardsReports/Networks.jrxml");
            //Compile the JRXML Report Template
            jasperReport = JasperCompileManager.compileReport(design);
            //Fill template with set parameters and data source data
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri;
            uri = "Networks" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }
    }

}
