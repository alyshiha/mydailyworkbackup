package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.UserNetworkDAOInter;
import com.ev.Bingo.bus.dto.UserNetworkDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.util.List;

public class UserNetworkBO extends BaseBO implements UserNetworkBOInter {

    protected UserNetworkBO() {
        super();
    }

    @Override
    public Object insertrecord(Object... obj) throws Throwable {
        UserNetworkDTOInter RecordToInsert = (UserNetworkDTOInter) obj[0];
        UserNetworkDAOInter engin = DAOFactory.createUserNetworkDAO();
        return engin.insertrecord(RecordToInsert);
    }

    @Override
    public Object save(Object... obj) throws SQLException, Throwable {
        List<UserNetworkDTOInter> entities = (List<UserNetworkDTOInter>) obj[0];
        UserNetworkDAOInter engin = DAOFactory.createUserNetworkDAO();
        return (String) engin.save(entities);
    }

    @Override
    public Object deleterecord(Object... obj) throws Throwable {
        UserNetworkDTOInter RecordToDelete = (UserNetworkDTOInter) obj[0];
        UserNetworkDAOInter engin = DAOFactory.createUserNetworkDAO();
        return engin.deleterecord(RecordToDelete);

    }

    @Override
    public Object GetListOfRecordsCS(Object... obj) throws Throwable {
        usersDTOInter SearchRecords = (usersDTOInter) obj[0];
        UserNetworkDAOInter engin = DAOFactory.createUserNetworkDAO();
        List<UserNetworkDTOInter> AllRecords = (List<UserNetworkDTOInter>) engin.findRecordsListCS(SearchRecords);
        return AllRecords;
    }

}
