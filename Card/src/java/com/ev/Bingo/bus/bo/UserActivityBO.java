/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.UserActivtyDAOInter;
import com.ev.Bingo.bus.dto.UserActivityDTOInter;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class UserActivityBO implements UserActivityBOInter {

    public Object GetListOfRecords(Integer user) throws Throwable {
        UserActivtyDAOInter engin = DAOFactory.createUserActivtyDAO();
        List<UserActivityDTOInter> AllRecords = (List<UserActivityDTOInter>) engin.findRecordsList(user);
        return AllRecords;
    }
}
