package com.ev.Bingo.bus.bo;

import java.sql.SQLException;

public interface copyfilesBOInter {

    Object insertrecord(Object... obj) throws Throwable;

    Object save(Object... obj) throws SQLException, Throwable;

    Object updaterecord(Object... obj) throws Throwable;

    Object deleteallrecord(Object... obj) throws Throwable;

    Object deleterecord(Object... obj) throws Throwable;

    Object GetListOfRecords(Object... obj) throws Throwable;

    Object GetRecord(Object... obj) throws Throwable;

    Object GetAllRecords(Object... obj) throws Throwable;
}
