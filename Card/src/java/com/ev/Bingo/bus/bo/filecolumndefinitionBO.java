package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.filecolumndefinitionDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import java.util.ArrayList;
import java.util.List;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import java.text.SimpleDateFormat;

public class filecolumndefinitionBO extends BaseBO implements filecolumndefinitionBOInter {

    protected filecolumndefinitionBO() {
        super();
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<filecolumndefinitionDTOInter> entities = (List<filecolumndefinitionDTOInter>) obj[0];
        filecolumndefinitionDAOInter engin = DAOFactory.createfilecolumndefinitionDAO();
        return engin.save(entities);
    }

    public Object findRecordByColName(Object... obj) throws Throwable {
        filecolumndefinitionDAOInter engin = DAOFactory.createfilecolumndefinitionDAO();
        filecolumndefinitionDTOInter Records = (filecolumndefinitionDTOInter) engin.findRecordByColName();
        return Records;
    }

     public Object findRecordByColName4(Object... obj) throws Throwable {
        filecolumndefinitionDAOInter engin = DAOFactory.createfilecolumndefinitionDAO();
        filecolumndefinitionDTOInter Records = (filecolumndefinitionDTOInter) engin.findRecordByColName4();
        return Records;
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        filecolumndefinitionDAOInter engin = DAOFactory.createfilecolumndefinitionDAO();
        List<filecolumndefinitionDTOInter> AllRecords = (List<filecolumndefinitionDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object GetAllRecordsVIEW(Object... obj) throws Throwable {
        Integer RecordToSelect = (Integer) obj[0];
        filecolumndefinitionDAOInter engin = DAOFactory.createfilecolumndefinitionDAO();
        List<filecolumndefinitionDTOInter> AllRecords = (List<filecolumndefinitionDTOInter>) engin.findRecordsAllview(RecordToSelect);
        return AllRecords;
    }
}
