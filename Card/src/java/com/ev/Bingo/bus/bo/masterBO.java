/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.bo;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.masterDAOInter;
import com.ev.Bingo.bus.dto.settlementsDTOInter;
import com.ev.Bingo.bus.dto.timeFromSwitchDTOInter;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author shi7a
 */
public class masterBO implements masterBOInter {

    @Override
    public Object GetReport(Date fromdate, Date todate, String userName, String customer) throws Throwable {
        JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", userName);
        params.put("customer", customer);
        params.put("DateFrom", DateFormatter.changeDateAndTimeFormat(fromdate));
        params.put("DateTo", DateFormatter.changeDateAndTimeFormat(todate));
        try {
            Connection conn = CoonectionHandler.getInstance().getConnection();
            JasperDesign design;
            design = JRXmlLoader.load("C:/CardsReports/Master.jrxml");
            jasperReport = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(jasperReport, params, conn);
            Calendar c = Calendar.getInstance();
            String uri = "Master" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".pdf";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            JasperExportManager.exportReportToPdfFile(jasperPrint, pdfPath + uri);
            try {
                String x = pdfPath + uri;
                jasperReport = null;
                design = null;
                jasperPrint = null;
                params = null;
                CoonectionHandler.getInstance().returnConnection(conn);
                return x;
            } catch (Exception ex) {
                ex.printStackTrace();
                return "fail";
            }
        } catch (JRException ex) {
            ex.printStackTrace();
            String connectMsg = "Could not create the report " + ex.getMessage() + " " + ex.getLocalizedMessage();
            return "fail";
        }

    }

    @Override
    public Object GetAllRecordsAcquier(Date fromdate, Date todate) throws Throwable {
        masterDAOInter engin = DAOFactory.createmasterDAO();
        List<settlementsDTOInter> AllRecords = (List<settlementsDTOInter>) engin.findRecordsAcquier(fromdate, todate);
        return AllRecords;
    }

    @Override
    public Object GetAllRecordsIssuer(Date fromdate, Date todate) throws Throwable {
        masterDAOInter engin = DAOFactory.createmasterDAO();
        List<settlementsDTOInter> AllRecords = (List<settlementsDTOInter>) engin.findRecordsIssuer(fromdate, todate);
        return AllRecords;
    }

    @Override
    public Object GetAlltimefromswitch(Date fromdate, Date todate) throws Throwable {
        masterDAOInter engin = DAOFactory.createmasterDAO();
        List<timeFromSwitchDTOInter> AllRecords = (List<timeFromSwitchDTOInter>) engin.findRecordstimefromswitch(fromdate, todate);
        return AllRecords;
    }
}
