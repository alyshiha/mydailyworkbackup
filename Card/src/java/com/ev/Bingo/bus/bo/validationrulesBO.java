package com.ev.Bingo.bus.bo;

import com.ev.Bingo.bus.dto.transactiontypeDTOInter;
import com.ev.Bingo.base.bo.BaseBO;
import com.ev.Bingo.base.util.DateFormatter;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.validationrulesDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import com.ev.Bingo.bus.dao.currencymasterDAOInter;
import com.ev.Bingo.bus.dao.filecolumndefinitionDAOInter;
import com.ev.Bingo.bus.dao.transactionresponsecodeDAOInter;
import com.ev.Bingo.bus.dao.transactionresponsemasterDAOInter;
import com.ev.Bingo.bus.dao.transactiontypeDAOInter;
import com.ev.Bingo.bus.dao.transactiontypemasterDAOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.currencymasterDTOInter;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import com.ev.Bingo.bus.dto.transactionresponsecodeDTOInter;
import com.ev.Bingo.bus.dto.transactionresponsemasterDTOInter;
import com.ev.Bingo.bus.dto.transactiontypemasterDTOInter;
import java.util.List;
import com.ev.Bingo.bus.dto.validationrulesDTOInter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class validationrulesBO extends BaseBO implements validationrulesBOInter {

    protected validationrulesBO() {
        super();
    }

    public filecolumndefinitionDTOInter getColumnName(int columnId) throws Throwable {
        filecolumndefinitionDAOInter fcdDAO = DAOFactory.createfilecolumndefinitionDAO();
        filecolumndefinitionDTOInter fcdDTO = (filecolumndefinitionDTOInter) fcdDAO.findRecordByID(columnId);
        return fcdDTO;
    }

    public String genarateValueString(Integer[] values) {
        String result = "";
        int i = 0;
        for (Object o : values) {

            result += String.valueOf(o);
            if (i != values.length - 1) {
                result = result + ",";
                i++;
            }
        }
        return result;
    }

    public String genarateValueDate(Date d1, Date d2) {
        String result = DateFormatter.changeDateAndTimeFormat(d1) + " and " + DateFormatter.changeDateAndTimeFormat(d2);
        return result;
    }

    public Object insertrecord(Object... obj) throws Throwable {
        validationrulesDTOInter RecordToInsert = (validationrulesDTOInter) obj[0];
        validationrulesDAOInter engin = DAOFactory.createvalidationrulesDAO();
        return engin.insertrecord(RecordToInsert);
    }

    public Object createFormula(int column, String opt, String value, validationrulesDTOInter vrDTO) throws Throwable {

        validationrulesDAOInter vrDAO = DAOFactory.createvalidationrulesDAO();

        String formula = "";
        String formulaCode = "#Column Value#";

        filecolumndefinitionDTOInter columnDTO = (filecolumndefinitionDTOInter) getColumnName(column);

        formula += columnDTO.getname() + " " + opt;
        formulaCode += " " + opt;

        if (value != null || !value.equals("")) {
            String[] separate = value.split(",");
            if (opt.equals("in") || opt.equals("not in")) {
                formula += "(";
                formulaCode += "(";
                for (int i = 0; i < separate.length; i++) {
                    String stringVlaue = getValues(columnDTO.getcolumnname(), Integer.valueOf(separate[i]));
                    if (stringVlaue == null) {
                        if (i == separate.length - 1) {
                            formula += separate[i] + ")";
                            formulaCode += "'" + separate[i] + "'" + ")";
                        } else {
                            formula += separate[i] + ",";
                            formulaCode += "'" + separate[i] + "'" + ",";
                        }
                    } else {
                        if (i == separate.length - 1) {
                            formula += stringVlaue + ")";
                            formulaCode += separate[i] + ")";
                        } else {
                            formula += stringVlaue + ",";
                            formulaCode += separate[i] + ",";
                        }
                    }
                }
            } else if (opt.equals("=") || opt.equals(">") || opt.equals("<") || opt.equals("<=") || opt.equals("<=")) {
                if (isValidDate(value.toString())) {
                    formulaCode += "to_date('" + value + "','dd-mm-yyyy hh24:mi:ss')";
                    formula += " " + value;
                } else if (isValidNumber(value.toString())) {
                    formulaCode += "" + value + "";
                    formula += " " + value;
                } else {
                    formulaCode += "'" + value + "'";
                    formula += " " + value;
                }
                // formulaCode += " " + "'" + value + "'";
            } else if (opt.equals("between")) {
                String[] separated = value.split("and");
                formula += " " + separated[0] + " and " + separated[1];
                formulaCode += " to_date('" + separated[0] + "','dd-mm-yyyy hh24:mi:ss') and to_date('" + separated[1] + "','dd-mm-yyyy hh24:mi:ss')";
            }

        }
        boolean valid = vrDAO.checkFormula(formulaCode, columnDTO.getdatatype());
        if (valid) {
            vrDTO.setformula(formula);
            vrDTO.setformulacode(formulaCode);
            return "done";
        } else {
            return "Invalid Formula";
        }
    }

    private String getValues(String column, int valueId) throws Throwable {

        if ("RESPONSE_CODE_ID".equals(column)) {
            transactionresponsecodeDAOInter trcDAO = DAOFactory.createtransactionresponsecodeDAO();
            transactionresponsecodeDTOInter RecordToSelect = DTOFactory.createtransactionresponsecodeDTO();
            RecordToSelect.setid(valueId);
            transactionresponsecodeDTOInter trcDTO = (transactionresponsecodeDTOInter) trcDAO.findRecord(RecordToSelect);
            return trcDTO.getname();
        } else if ("RESPONSE_CODE_MASTER".equals(column)) {
            transactionresponsemasterDAOInter trmDAO = DAOFactory.createtransactionresponsemasterDAO();
            transactionresponsemasterDTOInter Record = DTOFactory.createtransactionresponsemasterDTO();
            Record.setid(valueId);
            transactionresponsemasterDTOInter trmDTOL = (transactionresponsemasterDTOInter) trmDAO.findRecord(Record);
            return trmDTOL.getname();
        } else if ("TRANSACTION_TYPE_ID".equals(column)) {
            transactiontypeDAOInter ttDAO = DAOFactory.createtransactiontypeDAO();
            transactiontypeDTOInter Record3 = DTOFactory.createtransactiontypeDTO();
            Record3.setid(valueId);
            transactiontypeDTOInter ttDTOL = (transactiontypeDTOInter) ttDAO.findRecord(Record3);
            return ttDTOL.getname();
        } else if ("TRANSACTION_TYPE_MASTER".equals(column)) {
            transactiontypemasterDAOInter ttmDAO = DAOFactory.createtransactiontypemasterDAO();
            transactiontypemasterDTOInter Record4 = DTOFactory.createtransactiontypemasterDTO();
            Record4.setid(valueId);
            transactiontypemasterDTOInter ttmDTOL = (transactiontypemasterDTOInter) ttmDAO.findRecord(Record4);
            return ttmDTOL.getname();
        } else if ("CURRENCY_ID".equals(column)) {
            currencymasterDAOInter cmDAO = DAOFactory.createcurrencymasterDAO();
            currencymasterDTOInter Record5 = DTOFactory.createcurrencymasterDTO();
            Record5.setid(valueId);
            currencymasterDTOInter cmDTO = (currencymasterDTOInter) cmDAO.findRecord(Record5);
            return cmDTO.getsymbol();
        } else if ("SETTLEMENT_CURRENCY_ID".equals(column)) {
            currencymasterDAOInter cmDAO = DAOFactory.createcurrencymasterDAO();
            currencymasterDTOInter Record5 = DTOFactory.createcurrencymasterDTO();
            Record5.setid(valueId);
            currencymasterDTOInter cmDTO = (currencymasterDTOInter) cmDAO.findRecord(Record5);
            return cmDTO.getsymbol();
        } else if ("ACQUIRER_CURRENCY_ID".equals(column)) {
            currencymasterDAOInter cmDAO = DAOFactory.createcurrencymasterDAO();
            currencymasterDTOInter Record5 = DTOFactory.createcurrencymasterDTO();
            Record5.setid(valueId);
            currencymasterDTOInter cmDTO = (currencymasterDTOInter) cmDAO.findRecord(Record5);
            return cmDTO.getsymbol();
        } else {
            return "";
        }
    }

    public boolean isValidDate(String inDate) {

        if (inDate == null) {
            return false;
        }

        //set the format to use as a constructor argument
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        if (inDate.trim().length() != dateFormat.toPattern().length()) {
            return false;
        }

        dateFormat.setLenient(false);

        try {
            //parse the inDate parameter
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }

    public boolean isValidNumber(String Input) {
        try {
            int x = Integer.parseInt(Input);
            return true;
        } catch (NumberFormatException nFE) {
            return false;
        }
    }

    public Object getColumnValues(String column) throws Throwable {

        if ("RESPONSE_CODE_ID".equals(column)) {
            transactionresponsecodeDAOInter engin = DAOFactory.createtransactionresponsecodeDAO();
            List<transactionresponsecodeDTOInter> AllRecords = (List<transactionresponsecodeDTOInter>) engin.findRecordsAll();
            return AllRecords;
        } else if ("RESPONSE_CODE_MASTER".equals(column)) {
            transactionresponsemasterDAOInter engin = DAOFactory.createtransactionresponsemasterDAO();
            List<transactionresponsemasterDTOInter> AllRecords = (List<transactionresponsemasterDTOInter>) engin.findRecordsAll();
            return AllRecords;
        } else if ("TRANSACTION_TYPE_ID".equals(column)) {
            transactiontypeDAOInter engin = DAOFactory.createtransactiontypeDAO();
            List<transactiontypeDTOInter> AllRecords = (List<transactiontypeDTOInter>) engin.findRecordsAll();
            return AllRecords;
        } else if ("TRANSACTION_TYPE_MASTER".equals(column)) {
            transactiontypemasterDAOInter engin = DAOFactory.createtransactiontypemasterDAO();
            List<transactiontypemasterDTOInter> AllRecords = (List<transactiontypemasterDTOInter>) engin.findRecordsAll();
            return AllRecords;
        } else if ("CURRENCY_ID".equals(column)) {
            currencymasterDAOInter engin = DAOFactory.createcurrencymasterDAO();
            List<currencymasterDTOInter> AllRecords = (List<currencymasterDTOInter>) engin.findRecordsAll();
            return AllRecords;
        } else if ("SETTLEMENT_CURRENCY_ID".equals(column)) {
            currencymasterDAOInter engin = DAOFactory.createcurrencymasterDAO();
            List<currencymasterDTOInter> AllRecords = (List<currencymasterDTOInter>) engin.findRecordsAll();
            return AllRecords;
        } else if ("ACQUIRER_CURRENCY_ID".equals(column)) {
            currencymasterDAOInter engin = DAOFactory.createcurrencymasterDAO();
            List<currencymasterDTOInter> AllRecords = (List<currencymasterDTOInter>) engin.findRecordsAll();
            return AllRecords;
        } else {
            return null;
        }
    }

    public Object GetAllRecords(Object... obj) throws Throwable {
        validationrulesDAOInter engin = DAOFactory.createvalidationrulesDAO();
        List<validationrulesDTOInter> AllRecords = (List<validationrulesDTOInter>) engin.findRecordsAll();
        return AllRecords;
    }

    public Object deleterecord(Object... obj) throws Throwable {
        validationrulesDTOInter RecordToDelete = (validationrulesDTOInter) obj[0];
        validationrulesDAOInter engin = DAOFactory.createvalidationrulesDAO();
        return engin.deleterecord(RecordToDelete);
    }

    public Object save(Object... obj) throws SQLException, Throwable {
        List<validationrulesDTOInter> entities = (List<validationrulesDTOInter>) obj[0];
        validationrulesDAOInter engin = DAOFactory.createvalidationrulesDAO();
        return engin.save(entities);
    }
}
