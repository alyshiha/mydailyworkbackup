package com.ev.Bingo.bus.bo;

import com.ev.Bingo.base.bo.BaseBO;
import java.sql.SQLException;
import com.ev.Bingo.bus.dao.profilemenuDAOInter;
import com.ev.Bingo.bus.dao.DAOFactory;
import java.util.List;
import com.ev.Bingo.bus.dto.profilemenuDTOInter;

public class profilemenuBO extends BaseBO implements profilemenuBOInter {

    protected profilemenuBO() {
        super();
    }

    public Object deleterecord(Object... obj) throws Throwable {
        profilemenuDTOInter RecordToDelete = (profilemenuDTOInter) obj[0];
        profilemenuDAOInter engin = DAOFactory.createprofilemenuDAO();
        return engin.deleterecord(RecordToDelete);

    }

}
