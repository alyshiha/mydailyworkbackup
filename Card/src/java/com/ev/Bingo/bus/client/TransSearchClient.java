/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.base.client.ExportExcel;
import com.ev.Bingo.base.client.HashContainer;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.base.util.ReportingTool;
import com.ev.Bingo.bus.bo.ATMMACHINEBOInter;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.BankCodeBOInter;
import com.ev.Bingo.bus.bo.Col4BOInter;
import com.ev.Bingo.bus.bo.DiffAmountBOInter;
import com.ev.Bingo.bus.bo.currencymasterBOInter;
import com.ev.Bingo.bus.bo.filecolumndefinitionBOInter;
import com.ev.Bingo.bus.bo.networksBOInter;
import com.ev.Bingo.bus.bo.settlementlogBOInter;
import com.ev.Bingo.bus.bo.transactionresponsecodeBOInter;
import com.ev.Bingo.bus.bo.transactionstatisticsrepBOInter;
import com.ev.Bingo.bus.bo.transactiontypeBOInter;
import com.ev.Bingo.bus.bo.transsearchBOInter;
import com.ev.Bingo.bus.bo.usersBOInter;
import com.ev.Bingo.bus.bo.validationrulesBOInter;
import com.ev.Bingo.bus.dto.ATMMACHINECASHDTOINTER;
import com.ev.Bingo.bus.dto.BankCodeDTOInter;
import com.ev.Bingo.bus.dto.Col4DTOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.TranSearchFieldDTOInter;
import com.ev.Bingo.bus.dto.TransSearchOperatorsDTOInter;
import com.ev.Bingo.bus.dto.atmfileDTOInter;
import com.ev.Bingo.bus.dto.currencymasterDTOInter;
import com.ev.Bingo.bus.dto.disputesDTOInter;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import com.ev.Bingo.bus.dto.transactionresponsecodeDTOInter;
import com.ev.Bingo.bus.dto.transactiontypeDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import com.ev.Bingo.bus.dto.validationrulesDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import java.text.*;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Administrator
 */
public class TransSearchClient extends BaseBean {

    private disputesDTOInter dAtt;
    private String corramount, exportflag;
    private Boolean viewtranscode, pendindby, viewtrans, searchby, buttonsflag, nootherside, fileFlag1, seqtoshow, setamounttoshow, aqramounttoshow, showTotal, disputefalg, matchedflagsearch, rejectedflagsearch, rejectedflag, matchedflag, trancedatetoshow, loadingdatetoshow, settdatetoshow, amounttoshow, commonflag, cardFlag, emptyFlag, accountFlag, matchTabRej, visaTabRej, rejTabRej, dispTabRej, selectFlag, deselectFlag;
    private String[] selectedSearch;
    private Integer noResult;
    private transsearchBOInter TransactionBO;
    private String amount, column1, column4, message, correctiveState, settledState, fileselectedprint, textfile, filetrans, showSide, othersideheader, selectedpage, searchcriteria, masterString, temp, panelHeader, dispheader, matchheader, visaheader, rejectedheader, transcomment, commentMessage, searchcriteriad, searchcriteriam, searchcriteriar;
    private List<String> matchingrecordlist, matchingtypelist, searchoperatorlist2, searchoperatorlist1, searchoperatorlist3;
    private TransSearchOperatorsDTOInter searchoperators;
    private List<transactiontypeDTOInter> transtypelist;
    private List<currencymasterDTOInter> currencylist;
    private List<validationrulesDTOInter> validationrulelist;
    private List<transactionresponsecodeDTOInter> responcelist;
    private List<usersDTOInter> userslist;
    private currencymasterBOInter CurrencyMasterEngin;
    private DiffAmountBOInter DiffAmountBOEngin;
    private List<atmfileDTOInter> filelist;
    private filecolumndefinitionBOInter columnEngin;
    private transactiontypeBOInter transtypeEngin;
    private validationrulesBOInter validationruleEngin;
    private List<ATMMACHINECASHDTOINTER> atmrecordsList;
    private ATMMACHINEBOInter atmengin;
    private BankCodeBOInter bankEngin;
    private Col4BOInter col4Engin;
    private networksBOInter networkEngin;
    private settlementlogBOInter setteledengin;
    private filecolumndefinitionDTOInter colnamedto;
    private List<networksDTOInter> networklist;
    private List<filecolumndefinitionDTOInter> columnslist, sortcolumnslist, selectedColoumns;
    private usersBOInter UsersEngin;
    private transactionresponsecodeBOInter transactionresponsecodeEngin;
    private TranSearchFieldDTOInter searchfields;
    private List<validationrulesDTOInter> ValidationrulelistJournal, ValidationrulelistSwitch, ValidationrulelistHost;
    private String[] selectedCol, searchcriteriaresult;
    private List<disputesDTOInter> allSearchRecords;
    private List<disputesDTOInter> disputeRecords;
    private List<disputesDTOInter> rejectedRecords;
    private List<disputesDTOInter> matchedRecords;
    private disputesDTOInter[] selectedrows;
    private disputesDTOInter detailedRecord, othersideDTO, CommentDTO, CommentOtherSideDTO;
    private HashContainer SetteltContainer, CorrectiveContainer;
    private int setstateSearch, setcorrectivesearch;
    private List<BankCodeDTOInter> banknames;
    private List<Col4DTOInter> col4names;

    public List<Col4DTOInter> getCol4names() {
        return col4names;
    }

    public void setCol4names(List<Col4DTOInter> col4names) {
        this.col4names = col4names;
    }

    public Boolean getPendindby() {
        return pendindby;
    }

    public String getCorramount() {
        return corramount;
    }

    public String getExportflag() {
        return exportflag;
    }

    public void setExportflag(String exportflag) {
        this.exportflag = exportflag;
    }

    public void setCorramount(String corramount) {
        this.corramount = corramount;
    }

    public void setPendindby(Boolean pendindby) {
        this.pendindby = pendindby;
    }

    public String getColumn4() {
        return column4;
    }

    public void setColumn4(String column4) {
        this.column4 = column4;
    }

    public String getColumn1() {
        return column1;
    }

    public void setColumn1(String column1) {
        this.column1 = column1;
    }

    public Boolean getViewtranscode() {
        return viewtranscode;
    }

    public void setViewtranscode(Boolean viewtranscode) {
        this.viewtranscode = viewtranscode;
    }

    public Boolean getSearchby() {
        return searchby;
    }

    public void setSearchby(Boolean searchby) {
        this.searchby = searchby;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSettledState() {
        return settledState;
    }

    public void setSettledState(String settledState) {
        this.settledState = settledState;
    }

    public HashContainer getSetteltContainer() {
        return SetteltContainer;
    }

    public void setSetteltContainer(HashContainer SetteltContainer) {
        this.SetteltContainer = SetteltContainer;
    }

    public String getOthersideheader() {
        return othersideheader;
    }

    public Boolean getButtonsflag() {
        return buttonsflag;
    }

    public void setButtonsflag(Boolean buttonsflag) {
        this.buttonsflag = buttonsflag;
    }

    public Boolean getFileFlag1() {
        return fileFlag1;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setFileFlag1(Boolean fileFlag1) {
        this.fileFlag1 = fileFlag1;
    }

    public String getFiletrans() {
        return filetrans;
    }

    public void setFiletrans(String filetrans) {
        this.filetrans = filetrans;
    }

    public String getShowSide() {
        return showSide;
    }

    public void setShowSide(String showSide) {
        this.showSide = showSide;
    }

    public String getFileselectedprint() {
        return fileselectedprint;
    }

    public List<filecolumndefinitionDTOInter> getSortcolumnslist() {
        return sortcolumnslist;
    }

    public void setSortcolumnslist(List<filecolumndefinitionDTOInter> sortcolumnslist) {
        this.sortcolumnslist = sortcolumnslist;
    }

    public void setFileselectedprint(String fileselectedprint) {
        this.fileselectedprint = fileselectedprint;
    }

    public String getTextfile() {
        return textfile;
    }

    public void setTextfile(String textfile) {
        this.textfile = textfile;
    }

    public disputesDTOInter getDetailedRecord() {
        return detailedRecord;
    }

    public void setDetailedRecord(disputesDTOInter detailedRecord) {
        this.detailedRecord = detailedRecord;
    }

    public void setOthersideheader(String othersideheader) {
        this.othersideheader = othersideheader;
    }

    public disputesDTOInter getOthersideDTO() {
        return othersideDTO;
    }

    public void setOthersideDTO(disputesDTOInter othersideDTO) {
        this.othersideDTO = othersideDTO;
    }

    public disputesDTOInter getCommentOtherSideDTO() {
        return CommentOtherSideDTO;
    }

    public void setCommentOtherSideDTO(disputesDTOInter CommentOtherSideDTO) {
        this.CommentOtherSideDTO = CommentOtherSideDTO;
    }

    public String getCommentMessage() {
        return commentMessage;
    }

    public void setCommentMessage(String commentMessage) {
        this.commentMessage = commentMessage;
    }

    public String getTranscomment() {
        return transcomment;
    }

    public void setTranscomment(String transcomment) {
        this.transcomment = transcomment;
    }

    public disputesDTOInter getCommentDTO() {
        return CommentDTO;
    }

    public void setCommentDTO(disputesDTOInter CommentDTO) {
        this.CommentDTO = CommentDTO;
    }

    public String getSearchcriteria() {
        return searchcriteria;
    }

    public void setSearchcriteria(String searchcriteria) {
        this.searchcriteria = searchcriteria;
    }

    public disputesDTOInter[] getSelectedrows() {
        return selectedrows;
    }

    public void setSelectedrows(disputesDTOInter[] selectedrows) {
        this.selectedrows = selectedrows;
    }

    public List<BankCodeDTOInter> getBanknames() {
        return banknames;
    }

    public void setBanknames(List<BankCodeDTOInter> banknames) {
        this.banknames = banknames;
    }

    public String[] getSelectedCol() {
        return selectedCol;
    }

    public void setSelectedCol(String[] selectedCol) {
        this.selectedCol = selectedCol;
    }

    public filecolumndefinitionBOInter getColumnEngin() {
        return columnEngin;
    }

    public void setColumnEngin(filecolumndefinitionBOInter columnEngin) {
        this.columnEngin = columnEngin;
    }

    public networksBOInter getNetworkEngin() {
        return networkEngin;
    }

    public void setNetworkEngin(networksBOInter networkEngin) {
        this.networkEngin = networkEngin;
    }

    public List<filecolumndefinitionDTOInter> getColumnslist() {
        return columnslist;
    }

    public void setColumnslist(List<filecolumndefinitionDTOInter> columnslist) {
        this.columnslist = columnslist;
    }

    public Boolean getSeqtoshow() {
        return seqtoshow;
    }

    public void setSeqtoshow(Boolean seqtoshow) {
        this.seqtoshow = seqtoshow;
    }

    public Boolean getSetamounttoshow() {
        return setamounttoshow;
    }

    public void setSetamounttoshow(Boolean setamounttoshow) {
        this.setamounttoshow = setamounttoshow;
    }

    public Boolean getAqramounttoshow() {
        return aqramounttoshow;
    }

    public void setAqramounttoshow(Boolean aqramounttoshow) {
        this.aqramounttoshow = aqramounttoshow;
    }

    public Boolean getShowTotal() {
        return showTotal;
    }

    public void setShowTotal(Boolean showTotal) {
        this.showTotal = showTotal;
    }

    public Boolean getDisputefalg() {
        return disputefalg;
    }

    public void setDisputefalg(Boolean disputefalg) {
        this.disputefalg = disputefalg;
    }

    public int getSetstateSearch() {
        return setstateSearch;
    }

    public Boolean getVisaTabRej() {
        return visaTabRej;
    }

    public void setVisaTabRej(Boolean visaTabRej) {
        this.visaTabRej = visaTabRej;
    }

    public String getCorrectiveState() {
        return correctiveState;
    }

    public void setCorrectiveState(String correctiveState) {
        this.correctiveState = correctiveState;
    }

    public String getVisaheader() {
        return visaheader;
    }

    public void setVisaheader(String visaheader) {
        this.visaheader = visaheader;
    }

    public String getSearchcriteriad() {
        return searchcriteriad;
    }

    public void setSearchcriteriad(String searchcriteriad) {
        this.searchcriteriad = searchcriteriad;
    }

    public String getSearchcriteriam() {
        return searchcriteriam;
    }

    public void setSearchcriteriam(String searchcriteriam) {
        this.searchcriteriam = searchcriteriam;
    }

    public String getSearchcriteriar() {
        return searchcriteriar;
    }

    public void setSearchcriteriar(String searchcriteriar) {
        this.searchcriteriar = searchcriteriar;
    }

    public List<ATMMACHINECASHDTOINTER> getAtmrecordsList() {
        return atmrecordsList;
    }

    public void setAtmrecordsList(List<ATMMACHINECASHDTOINTER> atmrecordsList) {
        this.atmrecordsList = atmrecordsList;
    }

    public ATMMACHINEBOInter getAtmengin() {
        return atmengin;
    }

    public void setAtmengin(ATMMACHINEBOInter atmengin) {
        this.atmengin = atmengin;
    }

    public BankCodeBOInter getBankEngin() {
        return bankEngin;
    }

    public void setBankEngin(BankCodeBOInter bankEngin) {
        this.bankEngin = bankEngin;
    }

    public Col4BOInter getCol4Engin() {
        return col4Engin;
    }

    public void setCol4Engin(Col4BOInter col4Engin) {
        this.col4Engin = col4Engin;
    }

    public HashContainer getCorrectiveContainer() {
        return CorrectiveContainer;
    }

    public void setCorrectiveContainer(HashContainer CorrectiveContainer) {
        this.CorrectiveContainer = CorrectiveContainer;
    }

    public int getSetcorrectivesearch() {
        return setcorrectivesearch;
    }

    public void setSetcorrectivesearch(int setcorrectivesearch) {
        this.setcorrectivesearch = setcorrectivesearch;
    }

    public void setSetstateSearch(int setstateSearch) {
        this.setstateSearch = setstateSearch;
    }

    public Boolean getRejectedflag() {
        return rejectedflag;
    }

    public void setRejectedflag(Boolean rejectedflag) {
        this.rejectedflag = rejectedflag;
    }

    public Boolean getMatchedflag() {
        return matchedflag;
    }

    public void setMatchedflag(Boolean matchedflag) {
        this.matchedflag = matchedflag;
    }

    public Boolean getTrancedatetoshow() {
        return trancedatetoshow;
    }

    public void setTrancedatetoshow(Boolean trancedatetoshow) {
        this.trancedatetoshow = trancedatetoshow;
    }

    public Boolean getSettdatetoshow() {
        return settdatetoshow;
    }

    public void setSettdatetoshow(Boolean settdatetoshow) {
        this.settdatetoshow = settdatetoshow;
    }

    public Boolean getAmounttoshow() {
        return amounttoshow;
    }

    public void setAmounttoshow(Boolean amounttoshow) {
        this.amounttoshow = amounttoshow;
    }

    public Boolean getCommonflag() {
        return commonflag;
    }

    public void setCommonflag(Boolean commonflag) {
        this.commonflag = commonflag;
    }

    public Boolean getCardFlag() {
        return cardFlag;
    }

    public void setCardFlag(Boolean cardFlag) {
        this.cardFlag = cardFlag;
    }

    public Boolean getEmptyFlag() {
        return emptyFlag;
    }

    public void setEmptyFlag(Boolean emptyFlag) {
        this.emptyFlag = emptyFlag;
    }

    public Boolean getAccountFlag() {
        return accountFlag;
    }

    public void setAccountFlag(Boolean accountFlag) {
        this.accountFlag = accountFlag;
    }

    public String[] getSelectedSearch() {
        return selectedSearch;
    }

    public void setSelectedSearch(String[] selectedSearch) {
        this.selectedSearch = selectedSearch;
    }

    public Integer getNoResult() {
        return noResult;
    }

    public void setNoResult(Integer noResult) {
        this.noResult = noResult;
    }

    public String getSelectedpage() {
        return selectedpage;
    }

    public void setSelectedpage(String selectedpage) {
        this.selectedpage = selectedpage;
    }

    public Boolean getLoadingdatetoshow() {
        return loadingdatetoshow;
    }

    public void setLoadingdatetoshow(Boolean loadingdatetoshow) {
        this.loadingdatetoshow = loadingdatetoshow;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public validationrulesBOInter getValidationruleEngin() {
        return validationruleEngin;
    }

    public void setValidationruleEngin(validationrulesBOInter validationruleEngin) {
        this.validationruleEngin = validationruleEngin;
    }

    public List<validationrulesDTOInter> getValidationrulelistJournal() {
        return ValidationrulelistJournal;
    }

    public void setValidationrulelistJournal(List<validationrulesDTOInter> ValidationrulelistJournal) {
        this.ValidationrulelistJournal = ValidationrulelistJournal;
    }

    public List<validationrulesDTOInter> getValidationrulelistSwitch() {
        return ValidationrulelistSwitch;
    }

    public void setValidationrulelistSwitch(List<validationrulesDTOInter> ValidationrulelistSwitch) {
        this.ValidationrulelistSwitch = ValidationrulelistSwitch;
    }

    public List<validationrulesDTOInter> getValidationrulelistHost() {
        return ValidationrulelistHost;
    }

    public void setValidationrulelistHost(List<validationrulesDTOInter> ValidationrulelistHost) {
        this.ValidationrulelistHost = ValidationrulelistHost;
    }

    public String getMasterString() {
        return masterString;
    }

    public void setMasterString(String masterString) {
        this.masterString = masterString;
    }

    public List<String> getMatchingrecordlist() {
        return matchingrecordlist;
    }

    public Boolean getNootherside() {
        return nootherside;
    }

    public void setNootherside(Boolean nootherside) {
        this.nootherside = nootherside;
    }

    public void setMatchingrecordlist(List<String> matchingrecordlist) {
        this.matchingrecordlist = matchingrecordlist;
    }

    public List<String> getMatchingtypelist() {
        return matchingtypelist;
    }

    public String[] getSearchcriteriaresult() {
        return searchcriteriaresult;
    }

    public void setSearchcriteriaresult(String[] searchcriteriaresult) {
        this.searchcriteriaresult = searchcriteriaresult;
    }

    public void setMatchingtypelist(List<String> matchingtypelist) {
        this.matchingtypelist = matchingtypelist;
    }

    public List<String> getSearchoperatorlist2() {
        return searchoperatorlist2;
    }

    public Boolean getViewtrans() {
        return viewtrans;
    }

    public void setViewtrans(Boolean viewtrans) {
        this.viewtrans = viewtrans;
    }

    public filecolumndefinitionDTOInter getColnamedto() {
        return colnamedto;
    }

    public void setColnamedto(filecolumndefinitionDTOInter colnamedto) {
        this.colnamedto = colnamedto;
    }

    public List<filecolumndefinitionDTOInter> getSelectedColoumns() {
        return selectedColoumns;
    }

    public disputesDTOInter getdAtt() {
        return dAtt;
    }

    public void setdAtt(disputesDTOInter dAtt) {
        this.dAtt = dAtt;
    }

    public void setSelectedColoumns(List<filecolumndefinitionDTOInter> selectedColoumns) {
        this.selectedColoumns = selectedColoumns;
    }

    public void setSearchoperatorlist2(List<String> searchoperatorlist2) {
        this.searchoperatorlist2 = searchoperatorlist2;
    }

    public List<String> getSearchoperatorlist1() {
        return searchoperatorlist1;
    }

    public void setSearchoperatorlist1(List<String> searchoperatorlist1) {
        this.searchoperatorlist1 = searchoperatorlist1;
    }

    public List<String> getSearchoperatorlist3() {
        return searchoperatorlist3;
    }

    public void setSearchoperatorlist3(List<String> searchoperatorlist3) {
        this.searchoperatorlist3 = searchoperatorlist3;
    }

    public TransSearchOperatorsDTOInter getSearchoperators() {
        return searchoperators;
    }

    public void setSearchoperators(TransSearchOperatorsDTOInter searchoperators) {
        this.searchoperators = searchoperators;
    }

    public List<transactiontypeDTOInter> getTranstypelist() {
        return transtypelist;
    }

    public void setTranstypelist(List<transactiontypeDTOInter> transtypelist) {
        this.transtypelist = transtypelist;
    }

    public List<currencymasterDTOInter> getCurrencylist() {
        return currencylist;
    }

    public void setCurrencylist(List<currencymasterDTOInter> currencylist) {
        this.currencylist = currencylist;
    }

    public List<transactionresponsecodeDTOInter> getResponcelist() {
        return responcelist;
    }

    public void setResponcelist(List<transactionresponsecodeDTOInter> responcelist) {
        this.responcelist = responcelist;
    }

    public List<usersDTOInter> getUserslist() {
        return userslist;
    }

    public void setUserslist(List<usersDTOInter> userslist) {
        this.userslist = userslist;
    }

    public currencymasterBOInter getCurrencyMasterEngin() {
        return CurrencyMasterEngin;
    }

    public void setCurrencyMasterEngin(currencymasterBOInter CurrencyMasterEngin) {
        this.CurrencyMasterEngin = CurrencyMasterEngin;
    }

    public transactiontypeBOInter getTranstypeEngin() {
        return transtypeEngin;
    }

    public void setTranstypeEngin(transactiontypeBOInter transtypeEngin) {
        this.transtypeEngin = transtypeEngin;
    }

    public usersBOInter getUsersEngin() {
        return UsersEngin;
    }

    public void setUsersEngin(usersBOInter UsersEngin) {
        this.UsersEngin = UsersEngin;
    }

    public transactionresponsecodeBOInter getTransactionresponsecodeEngin() {
        return transactionresponsecodeEngin;
    }

    public void setTransactionresponsecodeEngin(transactionresponsecodeBOInter transactionresponsecodeEngin) {
        this.transactionresponsecodeEngin = transactionresponsecodeEngin;
    }

    public DiffAmountBOInter getDiffAmountBOEngin() {
        return DiffAmountBOEngin;
    }

    public void setDiffAmountBOEngin(DiffAmountBOInter DiffAmountBOEngin) {
        this.DiffAmountBOEngin = DiffAmountBOEngin;
    }

    public List<atmfileDTOInter> getFilelist() {
        return filelist;
    }

    public void setFilelist(List<atmfileDTOInter> filelist) {
        this.filelist = filelist;
    }

    public settlementlogBOInter getSetteledengin() {
        return setteledengin;
    }

    public void setSetteledengin(settlementlogBOInter setteledengin) {
        this.setteledengin = setteledengin;
    }

    public TranSearchFieldDTOInter getSearchfields() {
        return searchfields;
    }

    public void setSearchfields(TranSearchFieldDTOInter searchfields) {
        this.searchfields = searchfields;
    }

    public List<validationrulesDTOInter> getValidationrulelist() {
        return validationrulelist;
    }

    public void setValidationrulelist(List<validationrulesDTOInter> validationrulelist) {
        this.validationrulelist = validationrulelist;
    }

    public List<networksDTOInter> getNetworklist() {
        return networklist;
    }

    public void setNetworklist(List<networksDTOInter> networklist) {
        this.networklist = networklist;
    }

    public Boolean getMatchTabRej() {
        return matchTabRej;
    }

    public void setMatchTabRej(Boolean matchTabRej) {
        this.matchTabRej = matchTabRej;
    }

    public Boolean getRejTabRej() {
        return rejTabRej;
    }

    public void setRejTabRej(Boolean rejTabRej) {
        this.rejTabRej = rejTabRej;
    }

    public Boolean getDispTabRej() {
        return dispTabRej;
    }

    public void setDispTabRej(Boolean dispTabRej) {
        this.dispTabRej = dispTabRej;
    }

    public transsearchBOInter getTransactionBO() {
        return TransactionBO;
    }

    public Boolean getSelectFlag() {
        return selectFlag;
    }

    public void setSelectFlag(Boolean selectFlag) {
        this.selectFlag = selectFlag;
    }

    public Boolean getDeselectFlag() {
        return deselectFlag;
    }

    public void setDeselectFlag(Boolean deselectFlag) {
        this.deselectFlag = deselectFlag;
    }

    public void setTransactionBO(transsearchBOInter TransactionBO) {
        this.TransactionBO = TransactionBO;
    }

    public String getPanelHeader() {
        return panelHeader;
    }

    public void setPanelHeader(String panelHeader) {
        this.panelHeader = panelHeader;
    }

    public String getDispheader() {
        return dispheader;
    }

    public void setDispheader(String dispheader) {
        this.dispheader = dispheader;
    }

    public String getMatchheader() {
        return matchheader;
    }

    public void setMatchheader(String matchheader) {
        this.matchheader = matchheader;
    }

    public String getRejectedheader() {
        return rejectedheader;
    }

    public void setRejectedheader(String rejectedheader) {
        this.rejectedheader = rejectedheader;
    }

    public List<disputesDTOInter> getAllSearchRecords() {
        return allSearchRecords;
    }

    public void setAllSearchRecords(List<disputesDTOInter> allSearchRecords) {
        this.allSearchRecords = allSearchRecords;
    }

    public List<disputesDTOInter> getDisputeRecords() {
        return disputeRecords;
    }

    public void setDisputeRecords(List<disputesDTOInter> disputeRecords) {
        this.disputeRecords = disputeRecords;
    }

    public List<disputesDTOInter> getRejectedRecords() {
        return rejectedRecords;
    }

    public void setRejectedRecords(List<disputesDTOInter> rejectedRecords) {
        this.rejectedRecords = rejectedRecords;
    }

    public List<disputesDTOInter> getMatchedRecords() {
        return matchedRecords;
    }

    public void setMatchedRecords(List<disputesDTOInter> matchedRecords) {
        this.matchedRecords = matchedRecords;
    }

    public Boolean getMatchedflagsearch() {
        return matchedflagsearch;
    }

    public void setMatchedflagsearch(Boolean matchedflagsearch) {
        this.matchedflagsearch = matchedflagsearch;
    }

    public Boolean getRejectedflagsearch() {
        return rejectedflagsearch;
    }

    public void setRejectedflagsearch(Boolean rejectedflagsearch) {
        this.rejectedflagsearch = rejectedflagsearch;
    }

    public TransSearchClient() throws Throwable {
        GetAccess();
        atmengin = BOFactory.createATMMACHINEBO();
        selectedSearch = new String[4];
        selectedSearch[1] = "D";
        allSearchRecords = new ArrayList<disputesDTOInter>();
        disputeRecords = new ArrayList<disputesDTOInter>();
        banknames = new ArrayList<BankCodeDTOInter>();
        selectedColoumns = new ArrayList<filecolumndefinitionDTOInter>();
        rejectedRecords = new ArrayList<disputesDTOInter>();
        matchedRecords = new ArrayList<disputesDTOInter>();
        searchfields = DTOFactory.createtransearchfieldDTO();
        DiffAmountBOEngin = BOFactory.createDiffAmountBO();
        col4Engin = BOFactory.createCol4BO();
        bankEngin = BOFactory.createBankCodeBO();
        col4names = (List<Col4DTOInter>) col4Engin.GetListOfRecords();
        banknames = (List<BankCodeDTOInter>) bankEngin.GetListOfRecords();
        filelist = (List<atmfileDTOInter>) DiffAmountBOEngin.findfileall();
        matchingtypelist = findmatchingtype();
        matchingrecordlist = findmasterrecord("Network - Switch");
        validationruleEngin = BOFactory.createvalidationrulesBO();
        setteledengin = BOFactory.createsettlementlogBO();
        fileFlag1 = Boolean.FALSE;
        buttonsflag = Boolean.FALSE;
        detailedRecord = DTOFactory.createdisputesDTO();
        validationrulelist = new ArrayList<validationrulesDTOInter>();
        CreateValidationlistmethod();
        validationrulelist = (List<validationrulesDTOInter>) findvalidationrule("1");
        searchoperators = DTOFactory.createtranssearchoperatorsDTO();
        searchoperatorlist1 = searchoperators.getRulesValue1();
        searchoperatorlist3 = searchoperators.getRulesValue3();
        searchoperatorlist2 = searchoperators.getRulesValue2();
        CurrencyMasterEngin = BOFactory.createcurrencymasterBO();
        TransactionBO = BOFactory.createtranssearchBO();
        columnEngin = BOFactory.createfilecolumndefinitionBO();
        colnamedto = (filecolumndefinitionDTOInter) columnEngin.findRecordByColName();
        column1 = colnamedto.getname();
        colnamedto = (filecolumndefinitionDTOInter) columnEngin.findRecordByColName4();
        column4 = colnamedto.getname();
        columnslist = (List<filecolumndefinitionDTOInter>) columnEngin.GetAllRecordsVIEW(999);
        transactionresponsecodeEngin = BOFactory.createtransactionresponsecodeBO();
        transtypeEngin = BOFactory.createtransactiontypeBO();
        UsersEngin = BOFactory.createusersBO();
        networkEngin = BOFactory.createnetworksBO();
        networklist = (List<networksDTOInter>) networkEngin.GetAllUserRecords("" + super.GetUser().getUserid());
        userslist = (List<usersDTOInter>) UsersEngin.GetAllRecords();
        responcelist = (List<transactionresponsecodeDTOInter>) transactionresponsecodeEngin.GetAllRecords();
        currencylist = (List<currencymasterDTOInter>) CurrencyMasterEngin.GetAllRecords();
        transtypelist = (List<transactiontypeDTOInter>) transtypeEngin.GetAllRecords();
        noResult = 0;
        selectFlag = Boolean.FALSE;
        deselectFlag = Boolean.FALSE;
        matchTabRej = Boolean.FALSE;
        rejTabRej = Boolean.FALSE;
        visaTabRej = Boolean.FALSE;
        dispTabRej = Boolean.FALSE;
        selectedpage = "Pending";
        emptyFlag = Boolean.FALSE;
        accountFlag = Boolean.TRUE;
        cardFlag = Boolean.TRUE;
        searchby = Boolean.FALSE;
        viewtrans = Boolean.FALSE;
        viewtranscode = Boolean.FALSE;
        pendindby = Boolean.TRUE;
        noResult = 0;
        searchfields.setMasterrecordid("Switch");
        searchfields.setTransdateoperator("Between");
        searchfields.setCurrencyoperator("1");
        searchfields.setTransdatefrom(super.getYesterdaytrans("20:00:00"));
        searchfields.setTransdateto(super.getTodaytrans("20:00:00"));
        trancedatetoshow = Boolean.TRUE;
        disputefalg = Boolean.TRUE;
        commonflag = Boolean.TRUE;
        rejectedflag = Boolean.FALSE;
        rejectedflagsearch = Boolean.FALSE;
        matchedflagsearch = Boolean.FALSE;
        matchedflag = Boolean.FALSE;
        searchfields.setErrorid(0);
        searchfields.setSeqoperator("=");
        searchfields.setSortby("0");
        panelHeader = "Pending  Transactions";
        showTotal = Boolean.TRUE;
        selectedCol = colselected(columnslist);
        sortcolumnslist = TransactionBO.getcolumnselect(selectedCol, columnslist);
        amount = "Get.Amount";
        //atmrecordsList = atmengin.findAllDistinct();
        atmrecordsList = new ArrayList();
        SetteltContainer = new HashContainer();
        CorrectiveContainer = new HashContainer();
        settledState = "Set Settlement Status";
        exportflag = "Select Records To Export";
        correctiveState = "Set Corrective Status";
    }

    public String[] colselected(List<filecolumndefinitionDTOInter> col) {
        List<String> columns = new ArrayList<String>();
        String[] result;
        for (int i = 0; i < col.size(); i++) {
            filecolumndefinitionDTOInter item = col.get(i);
            if (item.getselected() == 1) {
                columns.add(item.getcolumnname());
            }
        }
        result = new String[columns.size()];
        for (int z = 0; z < columns.size(); z++) {
            result[z] = columns.get(z);
        }
        return result;
    }

    public Boolean pageselected(String Page) {
        for (String Record : selectedSearch) {
            if (Record == null ? Page == null : Record.equals(Page)) {
                return true;
            }
        }
        return false;
    }

    public void checkSearchincollapse() {
        if (pageselected("D")) {
            selectedpage = "Pending";
        } else if (pageselected("M")) {
            selectedpage = "Matched";
        } else {
            selectedpage = "Rejected";
        }
        if (pageselected("R")) {
            rejectedflag = Boolean.TRUE;
        } else {
            rejectedflag = Boolean.FALSE;
        }
        if (pageselected("D") || pageselected("M")) {
            disputefalg = Boolean.TRUE;
        } else {
            disputefalg = Boolean.FALSE;
        }
        if (pageselected("D") || pageselected("M")) {
            commonflag = Boolean.TRUE;
        } else {
            commonflag = Boolean.FALSE;
        }
        if (!pageselected("R") & !pageselected("M") & !pageselected("D")) {
            noResult = 1;
        } else {
            noResult = 0;
        }
        if (pageselected("R")) {
            rejectedflagsearch = Boolean.TRUE;
            if (pageselected("M")) {
                rejectedflagsearch = Boolean.FALSE;
            }
            if (pageselected("D")) {
                rejectedflagsearch = Boolean.FALSE;
            }
        } else {
            rejectedflagsearch = Boolean.FALSE;
        }
        if (pageselected("M")) {
            matchedflagsearch = Boolean.TRUE;
            if (pageselected("D")) {
                matchedflagsearch = Boolean.FALSE;
            }
        } else {
            matchedflagsearch = Boolean.FALSE;
        }
    }

    public void checkSearchin(AjaxBehaviorEvent e) {
        if (pageselected("D")) {
            selectedpage = "Pending";
        } else if (pageselected("M")) {
            selectedpage = "Matched";
        } else if (pageselected("V")) {
            selectedpage = "Visa";
        } else {
            selectedpage = "Rejected";
        }
        if (pageselected("R")) {
            rejectedflag = Boolean.TRUE;
        } else {
            rejectedflag = Boolean.FALSE;
        }
        if (pageselected("D") || pageselected("M")) {
            disputefalg = Boolean.TRUE;
        } else {
            disputefalg = Boolean.FALSE;
        }
        if (pageselected("D") || pageselected("M")) {
            commonflag = Boolean.TRUE;
        } else {
            commonflag = Boolean.FALSE;
        }
        if (pageselected("V") & !pageselected("R") & !pageselected("M") & !pageselected("D")) {
            noResult = 1;
        } else {
            noResult = 0;
        }
        if (pageselected("R") || pageselected("V")) {
            if (pageselected("R")) {
                rejectedflagsearch = Boolean.TRUE;
            } else {
                rejectedflagsearch = Boolean.FALSE;
            }
            if (pageselected("M")) {
                rejectedflagsearch = Boolean.FALSE;
            }
            if (pageselected("D")) {
                rejectedflagsearch = Boolean.FALSE;
            }
        } else {
            rejectedflagsearch = Boolean.FALSE;
        }
        if (pageselected("M")) {
            matchedflagsearch = Boolean.TRUE;
            if (pageselected("D")) {
                matchedflagsearch = Boolean.FALSE;
            }
        } else {
            matchedflagsearch = Boolean.FALSE;
        }
    }

    public void matchingTypeChange(ValueChangeEvent e) {
        matchingrecordlist = findmasterrecord(e.getNewValue().toString());
    }

    public List<String> findmatchingtype() {
        List<String> matchingtype = new ArrayList<String>();
        matchingtype.add("Network - Switch");
        matchingtype.add("Switch - Host");
        return matchingtype;
    }

    public void showOtherSide(ActionEvent e) throws Throwable {
        if (selectedpage.contains("Pending")) {
            disputesDTOInter mainside = (disputesDTOInter) e.getComponent().getAttributes().get("otherSide");
            othersideDTO = (disputesDTOInter) TransactionBO.findotherside(mainside);
        }
        if (selectedpage.contains("Matched")) {
            disputesDTOInter mainside = (disputesDTOInter) e.getComponent().getAttributes().get("otherSide");
            othersideDTO = (disputesDTOInter) TransactionBO.findothersideMatched(mainside);
        }
        if (othersideDTO.getrecordtype() == 1) {
            othersideheader = "N";
        } else if (othersideDTO.getrecordtype() == 2) {
            othersideheader = "S";
        } else if (othersideDTO.getrecordtype() == 3) {
            othersideheader = "H";
        }
    }

    public List<String> findmasterrecord(String MatchingType) {
        List<String> masterrecord = new ArrayList<String>();
        if ("Network - Switch".equals(MatchingType)) {
            masterrecord.add("Network");
            masterrecord.add("Switch");
        }
        if ("Switch - Host".equals(MatchingType)) {
            masterrecord.add("Switch");
            masterrecord.add("Host");
        }
        return masterrecord;
    }

    public void CreateValidationlistmethod() throws Throwable {
        List<validationrulesDTOInter> ValidationrulelistTemp = (List<validationrulesDTOInter>) validationruleEngin.GetAllRecords();
        ValidationrulelistJournal = new ArrayList<validationrulesDTOInter>();
        ValidationrulelistSwitch = new ArrayList<validationrulesDTOInter>();
        ValidationrulelistHost = new ArrayList<validationrulesDTOInter>();
        for (validationrulesDTOInter record : ValidationrulelistTemp) {
            if (record.getvaldationtype() == 1) {
                ValidationrulelistJournal.add(record);
            } else if (record.getvaldationtype() == 2) {
                ValidationrulelistSwitch.add(record);
            } else if (record.getvaldationtype() == 3) {
                ValidationrulelistHost.add(record);
            }
        }
    }

    public void masterRecordChange(ValueChangeEvent e) {
        masterString = e.getNewValue().toString();
    }

    public void settelmentDateOpChange(ValueChangeEvent e) {
        searchfields.setSetteltdateoperator(e.getNewValue().toString());
        if ("Between".equals(searchfields.getSetteltdateoperator())) {
            settdatetoshow = Boolean.TRUE;
        } else {
            settdatetoshow = Boolean.FALSE;
        }
    }

    public void transactionDateOpChange(ValueChangeEvent e) {
        searchfields.setTransdateoperator(e.getNewValue().toString());
        if ("Between".equals(searchfields.getTransdateoperator())) {
            trancedatetoshow = Boolean.TRUE;
        } else {
            trancedatetoshow = Boolean.FALSE;
        }
    }

    public void loadingDateOpChange(ValueChangeEvent e) {
        searchfields.setLoadingdateoperator(e.getNewValue().toString());
        if ("Between".equals(searchfields.getLoadingdateoperator())) {
            loadingdatetoshow = Boolean.TRUE;
        } else {
            loadingdatetoshow = Boolean.FALSE;
        }
    }

    public void cardChange(ValueChangeEvent e) {
        String value = (String) e.getNewValue();
        if (value.equals("is Null") || value.equals("is Not Null")) {
            cardFlag = Boolean.FALSE;
            emptyFlag = Boolean.TRUE;
        } else {
            cardFlag = Boolean.TRUE;
            emptyFlag = Boolean.FALSE;
        }
    }

    public void aqramountOpChange(ValueChangeEvent e) {
        searchfields.setAqramountoperator(e.getNewValue().toString());
        if ("Between".equals(searchfields.getAqramountoperator())) {
            aqramounttoshow = Boolean.TRUE;
        } else {
            aqramounttoshow = Boolean.FALSE;
        }
    }

    public void amountOpChange(ValueChangeEvent e) {
        searchfields.setAmountoperator(e.getNewValue().toString());
        if ("Between".equals(searchfields.getAmountoperator())) {
            amounttoshow = Boolean.TRUE;
        } else {
            amounttoshow = Boolean.FALSE;
        }
    }

    public void seqOpChange(ValueChangeEvent e) {
        searchfields.setSeqoperator(e.getNewValue().toString());
        if ("Between".equals(searchfields.getSeqoperator())) {
            seqtoshow = Boolean.TRUE;
        } else {
            seqtoshow = Boolean.FALSE;
        }
    }

    public void settamountOpChange(ValueChangeEvent e) {
        searchfields.setSettamountoperator(e.getNewValue().toString());
        if ("Between".equals(searchfields.getSettamountoperator())) {
            setamounttoshow = Boolean.TRUE;
        } else {
            setamounttoshow = Boolean.FALSE;
        }
    }

    public void transvalue(ValueChangeEvent e) throws Throwable {
        Integer value = Integer.valueOf(e.getNewValue().toString());
        if (value == -1) {
            viewtrans = Boolean.FALSE;
            viewtranscode = Boolean.FALSE;
            searchfields.setTypecodeid(0);
        } else {
            searchfields.setTypecodename("");
            viewtrans = Boolean.FALSE;
            viewtranscode = Boolean.TRUE;
        }
    }

    public void transvaluecode(AjaxBehaviorEvent e) throws Throwable {
        String value = searchfields.getTypecodename();
        if ("".equals(value)) {
            viewtrans = Boolean.FALSE;
            viewtranscode = Boolean.FALSE;
        } else {
            viewtrans = Boolean.TRUE;
            viewtranscode = Boolean.FALSE;
            searchfields.setTypecodeid(-1);
        }
    }

    public void viewcol(ValueChangeEvent e) throws Throwable {
        Integer value = Integer.valueOf(e.getNewValue().toString());
        if (value == 0) {
            value = 999;
        }
        pendindby = Boolean.FALSE;
        columnslist = (List<filecolumndefinitionDTOInter>) columnEngin.GetAllRecordsVIEW(value);
        selectedCol = colselected(columnslist);
        sortcolumnslist = TransactionBO.getcolumnselect(selectedCol, columnslist);
        if (value == 999) {
            pendindby = Boolean.TRUE;
            searchfields.setReversedby(0);
        }
    }

    public void genaratecol() {
        sortcolumnslist = TransactionBO.getcolumnselect(selectedCol, columnslist);
    }

    public void findCommentotherside(ActionEvent e) {

        CommentDTO = (disputesDTOInter) e.getComponent().getAttributes().get("commentAttrother");
        transcomment = CommentDTO.getcomments();
    }

    public void setrecordflag() {
        if ("Network".equals(searchfields.getMasterrecordid())) {
            fileFlag1 = Boolean.FALSE;
            showSide = "Network";
            othersideheader = "SW";
        } else if ("Switch".equals(searchfields.getMasterrecordid())) {
            fileFlag1 = Boolean.TRUE;
            showSide = "SW";
            othersideheader = "Host";
        } else {
            fileFlag1 = Boolean.FALSE;
            showSide = "Host";
            othersideheader = "SW";
        }
    }

    public void accountChange(ValueChangeEvent e) {
        String value = (String) e.getNewValue();
        if (value.equals("is Null") || value.equals("is Not Null")) {
            accountFlag = Boolean.FALSE;
        } else {
            accountFlag = Boolean.TRUE;
        }
    }

    public void recordTypeChange(ValueChangeEvent e) {
        validationrulelist = (List<validationrulesDTOInter>) findvalidationrule((String) e.getNewValue());
        temp = e.getNewValue().toString();
    }

    public List<validationrulesDTOInter> findvalidationrule(String recordtype) {
        List<validationrulesDTOInter> temp = new ArrayList<validationrulesDTOInter>();
        if ("1".equals(recordtype)) {
            temp = ValidationrulelistJournal;
        } else if ("2".equals(recordtype)) {
            temp = ValidationrulelistSwitch;
        } else if ("3".equals(recordtype)) {
            temp = ValidationrulelistHost;
        }
        return temp;
    }

    public void pageChange(TabChangeEvent e) throws Throwable {
        selectedpage = (String) e.getTab().getTitle();

        if (selectedpage.contains("Visa")) {
            disputefalg = Boolean.FALSE;
            commonflag = Boolean.FALSE;
            amount = "Get.Amount";
            rejectedflag = Boolean.FALSE;
            matchedflag = Boolean.FALSE;
            selectedrows = null;
            searchcriteria = searchcriteriar;
            panelHeader = "R.Visa Transactions";
            allSearchRecords = rejectedRecords;
            DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:transTable");
            repTable.setSelection(null);
            selectFlag = Boolean.TRUE;
            deselectFlag = Boolean.FALSE;
            calcAmount();

        } else if (selectedpage.contains("Pending")) {
            amount = "Get.Amount";
            disputefalg = Boolean.TRUE;
            commonflag = Boolean.TRUE;
            rejectedflag = Boolean.FALSE;
            matchedflag = Boolean.FALSE;
            selectedrows = null;
            searchcriteria = searchcriteriad;
            panelHeader = "Pending Transactions";
            allSearchRecords = disputeRecords;
            DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:transTable");
            repTable.setSelection(null);
            selectFlag = Boolean.TRUE;
            deselectFlag = Boolean.FALSE;
            calcAmount();

        } else if (selectedpage.contains("Rejected")) {
            disputefalg = Boolean.FALSE;
            commonflag = Boolean.FALSE;
            amount = "Get.Amount";
            rejectedflag = Boolean.TRUE;
            matchedflag = Boolean.FALSE;
            selectedrows = null;
            searchcriteria = searchcriteriar;
            panelHeader = "Rejected Transactions";
            allSearchRecords = rejectedRecords;
            DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:transTable");
            repTable.setSelection(null);
            selectFlag = Boolean.TRUE;
            deselectFlag = Boolean.FALSE;
            calcAmount();
        } else if (selectedpage.contains("Matched")) {
            disputefalg = Boolean.FALSE;
            commonflag = Boolean.TRUE;
            rejectedflag = Boolean.FALSE;
            amount = "Get.Amount";
            matchedflag = Boolean.TRUE;
            selectedrows = null;
            searchcriteria = searchcriteriam;
            panelHeader = "Matched Transactions";
            allSearchRecords = matchedRecords;
            DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:transTable");
            repTable.setSelection(null);
            selectFlag = Boolean.TRUE;
            deselectFlag = Boolean.FALSE;
            calcAmount();
        }

    }

    public void RowSelect(SelectEvent e) {
        disputesDTOInter SelectedItem = (disputesDTOInter) e.getObject();
        setstateSearch = SetteltContainer.add(String.valueOf(SelectedItem.getRownum()), SelectedItem.getsettledflag());
        setcorrectivesearch = CorrectiveContainer.add(String.valueOf(SelectedItem.getRownum()), SelectedItem.getCorrectiveentryflag());
        boolean reexport = Boolean.FALSE;
        if (selectedrows.length > 0) {
            for (disputesDTOInter selected : selectedrows) {
                if (selected.getCorrectiveentryflag() == 1) {
                    if (!reexport) {
                        exportflag = "Export Selected";
                    } else {
                        exportflag = "Select Records To Export";
                        break;
                    }
                } else if (selected.getCorrectiveentryflag() > 2) {
                    exportflag = "ReExport Selected";
                    settledState = "Set Settlement Status";
                    reexport = Boolean.TRUE;
                } else {
                    exportflag = "Select Records To Export";
                    break;
                }
            }
        } else {
            exportflag = "Select Records To Export";
        }
        if (setstateSearch != 3) {
            if (!"ReExport Selected".equals(exportflag)) {
                if (setstateSearch == 1) {
                    settledState = "Mark as not settled";
                } else if (setstateSearch == 2) {
                    settledState = "Mark as settled";
                }
            } else {
                settledState = "Set Settlement Status";
            }
        } else {
            settledState = "Set Settlement Status";
        }

        if (setcorrectivesearch != 3) {
            if (setcorrectivesearch == 1) {
                correctiveState = "Mark as not Corrective";
            } else if (setcorrectivesearch == 0) {
                correctiveState = "Mark as Corrective";
            }
        } else {
            correctiveState = "Set Corrective Status";
        }
    }

    public void rowUnSelect(UnselectEvent e) {
        disputesDTOInter SelectedItem = (disputesDTOInter) e.getObject();
        setstateSearch = SetteltContainer.remove(String.valueOf(SelectedItem.getRownum()));
        setcorrectivesearch = CorrectiveContainer.remove(String.valueOf(SelectedItem.getRownum()));
        boolean reexport = Boolean.FALSE;
        if (selectedrows.length > 0) {
            for (disputesDTOInter selected : selectedrows) {
                if (selected.getCorrectiveentryflag() == 1) {
                    if (!reexport) {
                        exportflag = "Export Selected";
                    } else {
                        exportflag = "Select Records To Export";
                        break;
                    }
                } else if (selected.getCorrectiveentryflag() > 2) {
                    exportflag = "ReExport Selected";
                    settledState = "Set Settlement Status";
                    reexport = Boolean.TRUE;
                } else {
                    exportflag = "Select Records To Export";
                    break;
                }
            }
        } else {
            exportflag = "Select Records To Export";
        }
        if (setcorrectivesearch != 3) {
            if (setcorrectivesearch == 1) {
                correctiveState = "Mark as not Corrective";
            } else if (setcorrectivesearch == 0) {
                correctiveState = "Mark as Corrective";
            }
        } else {
            correctiveState = "Set Corrective Status";
        }

        if (setstateSearch != 3) {
            if (!"ReExport Selected".equals(exportflag)) {
                if (setstateSearch == 1) {
                    settledState = "Mark as not settled";
                } else if (setstateSearch == 2) {
                    settledState = "Mark as settled";
                }
            } else {
                settledState = "Set Settlement Status";
            }
        } else {
            settledState = "Set Settlement Status";
        }
        System.out.println("rowunSelected " + SelectedItem.getsettledflag());
    }

    public void markas() throws Throwable {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String dd = dateFormat.format(cal.getTime());
        Date settime = (Date) dateFormat.parse(dd);
        if ("Mark as settled".equals(settledState)) {
            for (disputesDTOInter selectedrow : selectedrows) {
                selectedrow.setsettledflag(1);
                selectedrow.setsettleduser(super.GetUser().getUserid());
                selectedrow.setsettleddate(settime);
                selectedrow.setsettlementdate(settime);
                TransactionBO.UpdatesetteledRecord(selectedrow);
                if (searchfields.getSetteltstatus() == 2) {
                    allSearchRecords.remove(selectedrow);
                }
                setstateSearch = SetteltContainer.remove(String.valueOf(selectedrow.getRownum()));

                setteledengin.insertrecord(selectedrow);
            }
            settledState = "Mark as not settled";
            message = "Transaction Has Been Settled";
        } else if ("Mark as not settled".equals(settledState)) {
            for (disputesDTOInter selectedrow : selectedrows) {
                selectedrow.setsettledflag(2);
                selectedrow.setsettleduser(super.GetUser().getUserid());
                selectedrow.setsettleddate(settime);
                selectedrow.setsettlementdate(settime);
                TransactionBO.UpdatesetteledRecord(selectedrow);
                if (searchfields.getSetteltstatus() == 1) {
                    allSearchRecords.remove(selectedrow);
                }
                setstateSearch = SetteltContainer.remove(String.valueOf(selectedrow.getRownum()));
                setteledengin.insertrecord(selectedrow);
            }
            settledState = "Mark as settled";
            message = "Transaction Has Been UnSettled";
        } else if ("Set Settlement Status".equals(settledState)) {
            message = "Please Select Records with Same Status";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));

    }

    public void markascorr() throws Throwable {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String dd = dateFormat.format(cal.getTime());
        Date settime = (Date) dateFormat.parse(dd);
        if ("Mark as Corrective".equals(correctiveState)) {
            for (disputesDTOInter selectedrow : selectedrows) {
                selectedrow.setsettledflag(1);
                selectedrow.setCorrectiveentryflag(1);
                selectedrow.setsettleduser(super.GetUser().getUserid());
                selectedrow.setsettleddate(settime);
                selectedrow.setsettlementdate(settime);
                TransactionBO.UpdatesetteledRecord(selectedrow);
                TransactionBO.UpdatecorrectiveRecord(selectedrow);
                if (searchfields.getSetteltstatus() == 2) {
                    allSearchRecords.remove(selectedrow);
                }
                setcorrectivesearch = CorrectiveContainer.remove(String.valueOf(selectedrow.getRownum()));
                setteledengin.insertrecord(selectedrow);
            }
            selectedrows = null;
            CorrectiveContainer = new HashContainer();
            correctiveState = "Mark as Corrective";
            message = "Transaction Has Been corrective";
        } else if ("Mark as not Corrective".equals(correctiveState)) {
            for (disputesDTOInter selectedrow : selectedrows) {
                selectedrow.setCorrectiveentryflag(0);
                selectedrow.setsettledflag(0);
                selectedrow.setsettleduser(super.GetUser().getUserid());
                selectedrow.setsettleddate(settime);
                selectedrow.setsettlementdate(settime);
                TransactionBO.UpdatesetteledRecord(selectedrow);
                TransactionBO.UpdatecorrectiveRecord(selectedrow);
                if (searchfields.getSetteltstatus() == 1) {
                    allSearchRecords.remove(selectedrow);
                }

                setcorrectivesearch = CorrectiveContainer.remove(String.valueOf(selectedrow.getRownum()));
                setteledengin.insertrecord(selectedrow);
            }
            selectedrows = null;
            CorrectiveContainer = new HashContainer();
            correctiveState = "Mark as Corrective";
            message = "Transaction Has Been UnCorrective";
        } else if ("Set corrective Status".equals(correctiveState)) {
            message = "Please Select Records with Same Status";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));

    }

    public void selectAllRows() {
        DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:transTable");
        repTable.setSelection(allSearchRecords.toArray());
        selectFlag = Boolean.FALSE;
        deselectFlag = Boolean.TRUE;
        settledState = "Set Settlement Status";
        correctiveState = "Set Corrective Status";
        if (panelHeader.contains("Pending Transactions")) {
            message = "Settlement Operation Will Be Disabled";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
        SetteltContainer = new HashContainer();
        CorrectiveContainer = new HashContainer();
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void selectFlagsChange() {
        selectFlag = Boolean.TRUE;
        deselectFlag = Boolean.FALSE;
    }

    public void findComment(ActionEvent e) {
        CommentDTO = (disputesDTOInter) e.getComponent().getAttributes().get("commentAttr");
        transcomment = CommentDTO.getcomments();
    }

    public void updateComment() throws Throwable {
        String[] inputs = {transcomment};
        String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE", "OR", "AND", "IN", "NOT IN"};
        Boolean found = Boolean.FALSE;
        for (String input : inputs) {
            if (input != null) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace("-", "").replace("%", "").replace("=", "").replace(">", "").replace("<", "").replace("Between", "").replace("Like", "").replace("Not Like", "").replace("\\", "").replace(".", "").replace(":", "").replace("is Null", "").replace("is Not Null", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
        }
        if (!found) {
            CommentDTO.setcomments(transcomment);
            commentMessage = (String) TransactionBO.UpdateCommentRecord(CommentDTO);
        } else {
            message = "Please Enter Valid Data";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
            return;
        }

    }

    public void setrecordflagRejected() {
        if ("1".equals(searchfields.getRecordtypeid())) {

            fileFlag1 = Boolean.FALSE;
            showSide = "Network";
            othersideheader = "SW";
        } else if ("2".equals(searchfields.getRecordtypeid())) {

            fileFlag1 = Boolean.TRUE;
            showSide = "SW";
            othersideheader = "Network";
        } else {

            fileFlag1 = Boolean.FALSE;
            showSide = "GL";
        }
    }

    public void doSearch() throws Throwable {
        if (searchfields.getErrorcodename() == null) {
            searchfields.setErrorcodename("");
        }
        if (searchfields.getAtmidoperator() == null) {
            searchfields.setAtmidoperator("");
        }
        if (searchfields.getAtmidauto() == null) {
            searchfields.setAtmidauto("");
        }
        if (searchfields.getAqramountto() == null) {
            searchfields.setAqramountto("");
        }
        if (searchfields.getAmountto() == null) {
            searchfields.setAqramountto("");
        }
        if (searchfields.getSettamountto() == null) {
            searchfields.setSettamountto("");
        }
        if (searchfields.getSeqto() == null) {
            searchfields.setSeqto("");
        }
        if (searchfields.getViewid() == null) {
            searchfields.setViewid(0);
        }
        if (searchfields.getDisputeuserid() == null) {
            searchfields.setDisputeuserid(0);
        }
        if (searchfields.getCol4() == null) {
            searchfields.setCol4(0);
        }
        if (searchfields.getBank() == null) {
            searchfields.setBank(0);
        }
        if (searchfields.getNetwork() == null) {
            searchfields.setNetwork(0);
        }
        if (searchfields.getSetteltstatus() == null) {
            searchfields.setSetteltstatus(0);
        }
        if (searchfields.getSetteltbyid() == null) {
            searchfields.setSetteltbyid(0);
        }
        exportflag = "Select Records To Export";
        String[] inputs = {searchfields.getAccountno(), searchfields.getTypecodeid().toString(), searchfields.getCol4().toString(),
            searchfields.getBank().toString(), searchfields.getDisputeuserid().toString(), searchfields.getNetwork().toString(),
            searchfields.getViewid().toString(), searchfields.getSetteltstatus().toString(), searchfields.getSetteltbyid().toString(),
            searchfields.getCurrencyid().toString(), searchfields.getAqccurrencyid().toString(), searchfields.getSettcurrencyid().toString(), searchfields.getSorttype(), searchfields.getResponcecodename(), searchfields.getSeqfrom(), searchfields.getSeqto(),
            searchfields.getRefno(), searchfields.getSettamountfrom(), searchfields.getSettamountto(), searchfields.getTypecodename(),
            searchfields.getAccountnumoperator(), searchfields.getSortcol(),
            searchfields.getSortorder(), searchfields.getSortby(),
            searchfields.getSorttype(),
            searchfields.getAmountfrom(), searchfields.getAmountoperator(), searchfields.getAmountto(),
            searchfields.getAqramountfrom(), searchfields.getAqramountoperator(), searchfields.getAqramountto(), searchfields.getAtmidauto(),
            searchfields.getAtmidoperator(), searchfields.getAuthno(), searchfields.getCardno(), searchfields.getCardnumoperator(),
            searchfields.getCardnumsuffix(), searchfields.getCol1(), searchfields.getErrorcodename()};
        String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE", "OR", "AND", "IN", "NOT IN"};
        Boolean found = Boolean.FALSE;
        for (String input : inputs) {
            if (input != null) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", "").replace("-", "").replace("%", "").replace("_", "").replace("=", "").replace(">", "").replace("<", "").replace("Between", "").replace("Like", "").replace("Not Like", "").replace("\\", "").replace(".", "").replace(":", "").replace("is Null", "").replace("is Not Null", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            if (!input.toUpperCase().contains("TERMINAL")) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
            }
        }
        if (!found) {

            selectFlag = Boolean.TRUE;
            deselectFlag = Boolean.FALSE;
            buttonsflag = Boolean.TRUE;
            if (searchfields.getFilename() != null && !"".equals(searchfields.getFilename())) {
                searchfields.setFileid(getfileid(searchfields.getFilename()));
                if (searchfields.getFileid().equals(0)) {
                    message = "Invalid File Name";
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage(message));
                    return;
                }
            } else {
                searchfields.setFileid(0);
            }

            usersDTOInter UserDTO = (usersDTOInter) super.GetUser();
            amount = "Get.Amount";
            if (pageselected("V")) {
                if (!TransactionBO.ValidateDates(searchfields, "V")) {
                    message = "Please select at least one Date or more from search criteria";
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage(message));
                    return;
                } else {
                    commonflag = Boolean.FALSE;
                    panelHeader = "R.Visa Transactions";
                    rejectedRecords.clear();
                    setrecordflagRejected();
                    searchfields.setRecordtypeid(temp);
                    selectedColoumns = new ArrayList<filecolumndefinitionDTOInter>();
                    selectedColoumns = TransactionBO.getcolumnselect(selectedCol, columnslist);
                    searchcriteriaresult = TransactionBO.WhereCondition(searchfields, currencylist, userslist, transtypelist, responcelist, networklist, selectedCol, "V", UserDTO, validationrulelist);
                    if (searchcriteriaresult[2].contains("NETWORK_ID") || searchcriteriaresult[2].contains("FEE_LEVEL") || searchcriteriaresult[2].contains("NETWORK_COMM")) {
                        searchcriteriaresult[2] = "";
                    }
                    String selectstat = searchcriteriaresult[3] + " From REJECTED_VISA " + searchcriteriaresult[0] + searchcriteriaresult[2];
                    searchcriteriar = searchcriteriaresult[1];
                    searchcriteria = searchcriteriar;
                    rejectedRecords = (List<disputesDTOInter>) TransactionBO.getselecteditems(selectstat);
                    rejectedheader = "R.Visa " + thousseparator(BigDecimal.valueOf(rejectedRecords.size()));
                    allSearchRecords = rejectedRecords;
                    calcAmount();
                    rejTabRej = Boolean.TRUE;
                    rejectedflag = Boolean.TRUE;
                    selectedpage = "R.Visa";
                }
            } else {
                rejectedRecords.clear();
                rejTabRej = Boolean.FALSE;
                visaTabRej = Boolean.FALSE;
                rejectedflag = Boolean.FALSE;
            }

            if (pageselected("R")) {
                if (!TransactionBO.ValidateDates(searchfields, "R")) {
                    message = "Please select at least one Date or more from search criteria";
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage(message));
                    return;
                } else {
                    commonflag = Boolean.FALSE;
                    panelHeader = "Rejected Transactions";
                    rejectedRecords.clear();
                    setrecordflagRejected();
                    searchfields.setRecordtypeid(temp);
                    selectedColoumns = new ArrayList<filecolumndefinitionDTOInter>();
                    selectedColoumns = TransactionBO.getcolumnselect(selectedCol, columnslist);
                    searchcriteriaresult = TransactionBO.WhereCondition(searchfields, currencylist, userslist, transtypelist, responcelist, networklist, selectedCol, "R", UserDTO, validationrulelist);
                    if (searchcriteriaresult[2].contains("NETWORK_ID") || searchcriteriaresult[2].contains("FEE_LEVEL") || searchcriteriaresult[2].contains("NETWORK_COMM")) {
                        searchcriteriaresult[2] = "";
                    }
                    String selectstat = searchcriteriaresult[3] + " From rejected_transactions " + searchcriteriaresult[0] + searchcriteriaresult[2];
                    searchcriteriar = searchcriteriaresult[1];
                    searchcriteria = searchcriteriar;
                    rejectedRecords = (List<disputesDTOInter>) TransactionBO.getselecteditems(selectstat);
                    rejectedheader = "Rejected " + thousseparator(BigDecimal.valueOf(rejectedRecords.size()));
                    allSearchRecords = rejectedRecords;
                    calcAmount();
                    rejTabRej = Boolean.TRUE;
                    rejectedflag = Boolean.TRUE;
                    selectedpage = "Rejected";
                }
            } else {
                rejectedRecords.clear();
                rejTabRej = Boolean.FALSE;
                visaTabRej = Boolean.FALSE;
                rejectedflag = Boolean.FALSE;
            }
            if (pageselected("M")) {
                if (!TransactionBO.ValidateDates(searchfields, "M")) {
                    message = "Please select at least one Date or more from search criteria";
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage(message));
                    return;
                } else {
                    panelHeader = "Matched Transactions";
                    matchedRecords.clear();
                    commonflag = Boolean.TRUE;
                    searchfields.setMasterrecordid(masterString);
                    setrecordflag();
                    selectedColoumns = new ArrayList<filecolumndefinitionDTOInter>();
                    selectedColoumns = TransactionBO.getcolumnselect(selectedCol, columnslist);
                    searchcriteriaresult = TransactionBO.WhereCondition(searchfields, currencylist, userslist, transtypelist, responcelist, networklist, selectedCol, "M", UserDTO, validationrulelist);
                    String selectstat = searchcriteriaresult[3] + " From Matched_Data " + searchcriteriaresult[0] + searchcriteriaresult[2];
                    searchcriteriam = searchcriteriaresult[1];
                    searchcriteria = searchcriteriam;
                    matchedRecords = (List<disputesDTOInter>) TransactionBO.getselecteditems(selectstat);
                    matchheader = "Matched " + thousseparator(BigDecimal.valueOf(matchedRecords.size()));
                    allSearchRecords = matchedRecords;
                    calcAmount();
                    rejectedflag = Boolean.FALSE;
                    matchTabRej = Boolean.TRUE;
                    matchedflag = Boolean.TRUE;
                    selectedpage = "Matched";
                }
            } else {
                matchedRecords.clear();
                matchTabRej = Boolean.FALSE;
                matchedflag = Boolean.FALSE;
            }
            if (pageselected("D")) {
                if (!TransactionBO.ValidateDates(searchfields, "D")) {
                    message = "Please select at least one Date or more from search criteria";
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage(message));
                    return;
                } else {
                    panelHeader = "Pending Transactions";
                    SetteltContainer = new HashContainer();
                    CorrectiveContainer = new HashContainer();
                    disputeRecords.clear();
                    commonflag = Boolean.TRUE;
                    searchfields.setMasterrecordid(masterString);
                    setrecordflag();
                    selectedColoumns = new ArrayList<filecolumndefinitionDTOInter>();
                    selectedColoumns = TransactionBO.getcolumnselect(selectedCol, columnslist);
                    searchcriteriaresult = TransactionBO.WhereCondition(searchfields, currencylist, userslist, transtypelist, responcelist, networklist, selectedCol, "D", UserDTO, validationrulelist);
                    String selectstat = searchcriteriaresult[3] + " From Disputes " + searchcriteriaresult[0] + searchcriteriaresult[2];
                    searchcriteriad = searchcriteriaresult[1];
                    searchcriteria = searchcriteriad;
                    disputeRecords = (List<disputesDTOInter>) TransactionBO.getselecteditems(selectstat);
                    dispheader = "Pending " + thousseparator(BigDecimal.valueOf(disputeRecords.size()));
                    allSearchRecords = disputeRecords;
                    calcAmount();
                    dispTabRej = Boolean.TRUE;
                    disputefalg = Boolean.TRUE;
                    rejectedflag = Boolean.FALSE;
                    selectedpage = "Pending";
                }
            } else {
                disputeRecords.clear();
                dispTabRej = Boolean.FALSE;
                disputefalg = Boolean.FALSE;
            }
            searchby = Boolean.TRUE;
            RequestContext.getCurrentInstance().execute("collapse()");
            TabView repTable = (TabView) FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:tabs");
            repTable.setActiveIndex(0);
            ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:transTable"))).setFirst(0);
        } else {
            message = "Please Enter Valid Data";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
            return;
        }
    }

    public String thousseparator(BigDecimal total) {
        String StringNumber;
        NumberFormat nf = NumberFormat.getInstance();
        if (total != null) {
            StringNumber = nf.format(total);
        } else {
            StringNumber = "0";
        }
        return StringNumber;
    }

    public void calcAmount() throws Throwable {
        //IntSummaryStatistics stats = primes.stream().mapToInt((x) -> x).summaryStatistics();
        BigDecimal IntAmount = BigDecimal.ZERO;
        for (disputesDTOInter record : allSearchRecords) {

            IntAmount = IntAmount.add(new BigDecimal(Float.toString(record.getamount())));
        }
        amount = "Total Amount = " + thousseparator(IntAmount);
    }

    public void exportexcel(Object d) throws Throwable {
        ExportExcel sm = new ExportExcel();
        filecolumndefinitionDTOInter rejreason = DTOFactory.createfilecolumndefinitionDTO();
        if (selectedpage.contains("Rejected")) {
            rejreason.setcolumnname("get_rejected_reseaon(record_type,transaction_id) REASON");
            rejreason.setname("Reason");
            selectedColoumns.add(rejreason);
        }
        String path = sm.exportexcel(selectedColoumns, selectedrows, PropertyReader.getProperty("ev.installation.path.pdf") + selectedpage + ".xls", searchfields.getSortby());
        if (selectedpage.contains("Rejected")) {
            selectedColoumns.remove(rejreason);
        }
        prepDownload(path);
        message = "Export Done At Path " + path;
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));

    }

    public void save() {
        if (searchby == Boolean.TRUE) {
            checkSearchincollapse();
        }

        searchby = Boolean.FALSE;
        RequestContext.getCurrentInstance().execute("collapse()");

    }

    public String getField(List<filecolumndefinitionDTOInter> selected, String selectedpage) throws Throwable {
        String Field = "";
        for (filecolumndefinitionDTOInter coloumn : selected) {
            Field += coloumn.getcolumnname() + ",";
        }
        Field = Field.toLowerCase();
        if (selectedpage.contains("Matched")) {
            Field = Field.replace("dispute_key,", "");
            Field = Field.replace("dispute_reason,", "");
            Field = Field.replace("dispute_by,", "");
            Field = Field.replace("dispute_date,", "");
            Field = Field.replace("dispute_with_roles,", "");
            Field = Field.replace("corrective_entry_flag,", "");
            Field = Field.replace("reverse_flag,", "");
        } else if (selectedpage.contains("Rejected")) {
            Field = Field.replace("dispute_key,", "");
            Field = Field.replace("dispute_reason,", "");
            Field = Field.replace("dispute_by,", "");
            Field = Field.replace("dispute_date,", "");
            Field = Field.replace("dispute_with_roles,", "");
            Field = Field.replace("corrective_entry_flag,", "");
            Field = Field.replace("card_no_suffix,", "");
            Field = Field.replace("reverse_flag,", "");
            Field = Field.replace("settled_flag,", "");
            Field = Field.replace("settled_date,", "");
            Field = Field.replace("settled_user,", "");
            Field = Field.replace("comments,", "");
            Field = Field.replace("response_code_master,", "");
            Field = Field.replace("abs_amount,", "");
            Field = Field.replace("terminal,", "");
            Field = Field.replace("network_id,", "");
            Field = Field.replace("acquirer_currency,", "");
            Field = Field.replace("acquirer_currency_id,", "");
            Field = Field.replace("acquirer_amount,", "");
            Field = Field.replace("column4,", "");
            Field = Field.replace("settlement_currency,", "");
            Field = Field.replace("settlement_currency_id,", "");
            Field = Field.replace("settlement_amount,", "");
            Field = Field.replace("authorization_no,", "");
            Field = Field.replace("reference_no,", "");
            Field = Field.replace("bank,", "");
            Field = Field.replace("transaction_id,", "");
            Field = Field.replace("licence_key,", "");
            Field = Field.replace("network_code,", "");
            Field = Field.replace("process_code,", "");
            Field = Field.replace("area_code,", "");
            Field = Field.replace("report_id,", "");
            Field = Field.replace("network_comm,", "");
            Field = Field.replace("network_comm_flag,", "");
            Field = Field.replace("fee_level,", "");
        }
        return Field;
    }

    public String getHeader(String FIELDS, List<filecolumndefinitionDTOInter> selected) throws Throwable {
        String[] Header = FIELDS.split(",");
        String Headers = "";
        for (String Head : Header) {
            for (filecolumndefinitionDTOInter Col : selected) {
                if (Head.equals(Col.getcolumnname().toLowerCase())) {
                    Headers = Headers + Col.getname() + ",";
                }
            }
        }
        return Headers;
    }

    public void printreport(Object d) throws Throwable {

        filecolumndefinitionDTOInter rejreason = DTOFactory.createfilecolumndefinitionDTO();
        if (selectedpage.contains("Rejected")) {
            rejreason.setcolumnname("REASON");
            rejreason.setname("Reason");
            if (selectedColoumns.size() < 10) {
                selectedColoumns.add(rejreason);
            } else {
                selectedColoumns.add(9, rejreason);
            }
        }
        String FIELDS = getField(selectedColoumns, selectedpage);
        Integer RS = (Integer) TransactionBO.savePrint((Object) selectedrows, selectedpage, FIELDS, searchfields.getSortby(), searchfields.getSorttype());
        String Headers = getHeader(FIELDS, selectedColoumns);
        String path = (String) TransactionBO.GetReport(RS, Headers, super.GetUser().getUsername(), super.GetClient(), selectedpage, searchcriteria);
        if (selectedpage.contains("Rejected")) {
            selectedColoumns.remove(rejreason);
        }

        prepDownload(path);
        message = "PDF Report Path " + path;
        selectedrows = null;
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public List<String> completeTextatm(String query) {
        List<String> results = new ArrayList<String>();
        for (int i = 0; i < atmrecordsList.size(); i++) {
            if (atmrecordsList.get(i).getAtmname().toString().contains(query)) {
                results.add(atmrecordsList.get(i).getAtmname().toString());
            }
            if (results.size() > 5) {
                return results;
            }
        }
        return results;
    }

    public List<String> completeText(String query) {
        List<String> results = new ArrayList<String>();
        for (int i = 0; i < filelist.size(); i++) {
            if (filelist.get(i).getname().toString().contains(query)) {
                results.add(filelist.get(i).getname().toString());
            }
            if (results.size() > 5) {
                return results;
            }
        }
        return results;
    }

    public Integer getfileid(String name) {
        Integer ID = 0;
        for (atmfileDTOInter rec : filelist) {
            if (rec.getname().toLowerCase().equals(name.toLowerCase())) {
                return rec.getid();
            }
        }
        return ID;
    }

    public boolean handleCorramount(disputesDTOInter rec) {
        if (rec.getCorrectiveentryflag() == 4) {
            return false;
        } else {
            return true;
        }
    }

    public void updateCorramountPop(ActionEvent e) {
        dAtt = (disputesDTOInter) e.getComponent().getAttributes().get("coramount_dis");;
        corramount = dAtt.getCorrectiveamount();
    }

    public void updateCorr() {
        try {
            if (dAtt.getCorrectiveentryflag() != 3) {
                dAtt.setCorrectiveamount(corramount);
                TransactionBO.correctiveamountupdate(dAtt);
            }
        } catch (Throwable ex) {
            Logger.getLogger(TransSearchClient.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void export() throws Throwable {
        if (exportflag.equals("Export Selected") || exportflag.equals("ReExport Selected")) {
            for (disputesDTOInter selected : selectedrows) {
                selected.setCorrectiveentryflag(2);
                TransactionBO.exportRecord(selected);

            }
            selectedrows = null;
            CorrectiveContainer = new HashContainer();
            message = "Record Has Been Exported";
        } else {
            message = "Invalid Selection";
        }

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public boolean correctivepriv() throws Throwable {
        if (super.GetUser().getShowcorrectiveentry() == 1) {
            return false;
        } else {
            return true;
        }

    }

    public boolean exportpriv() throws Throwable {
        if (super.GetUser().getShowexport() == 1) {
            return false;
        } else {
            return true;
        }
    }
}
