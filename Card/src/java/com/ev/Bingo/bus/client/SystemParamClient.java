/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.systemtableBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.systemtableDTOInter;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Administrator
 */
public class SystemParamClient extends BaseBean {

    private List<systemtableDTOInter> spList;
    private systemtableBOInter spObject;
    private systemtableDTOInter spAttr;
    private systemtableDTOInter spAttrUp;
    private systemtableDTOInter newSP;
    private String message, oldValue;
    private Boolean showConfirm, showAdd, disableadd;

    private Integer listSize;

    public Boolean getDisableadd() {
        return disableadd;
    }

    public void setDisableadd(Boolean disableadd) {
        this.disableadd = disableadd;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public systemtableDTOInter getNewSP() {
        return newSP;
    }

    public void setNewSP(systemtableDTOInter newSP) {
        this.newSP = newSP;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public systemtableDTOInter getSpAttr() {
        return spAttr;
    }

    public void setSpAttr(systemtableDTOInter spAttr) {
        this.spAttr = spAttr;
    }

    public systemtableDTOInter getSpAttrUp() {
        return spAttrUp;
    }

    public void setSpAttrUp(systemtableDTOInter spAttrUp) {
        this.spAttrUp = spAttrUp;
    }

    public List<systemtableDTOInter> getSpList() {
        return spList;
    }

    public void setSpList(List<systemtableDTOInter> spList) {
        this.spList = spList;
    }

    public systemtableBOInter getSpObject() {
        return spObject;
    }

    public void setSpObject(systemtableBOInter spObject) {
        this.spObject = spObject;
    }

    public SystemParamClient() throws Throwable {
        super();
        GetAccess();
        spObject = BOFactory.createsystemtableBO();
        spList = (List<systemtableDTOInter>) spObject.GetAllRecords();
        showAdd = true;
        showConfirm = false;
        disableadd = true;
        listSize = spList.size();
    }

    public void fillDelete(ActionEvent e) {
        spAttr = (systemtableDTOInter) e.getComponent().getAttributes().get("removedRow");
        message = "";
    }

    public void deleteRecord() throws Throwable {
        message = (String) spObject.deleterecord(spAttr);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
        if (message.contains("Record Has Been")) {
            spList.remove(spAttr);
            showAdd = true;
            showConfirm = false;
        }
        if (spAttr.getparametername() == null || spAttr.getparametername().equals("")) {
            spList.remove(spAttr);
            showAdd = true;
            showConfirm = false;
            message = "Record has been removed";

            context.addMessage(null, new FacesMessage(message));
        }
    }

    public void addRecord() {
        newSP = DTOFactory.createsystemtableDTO();
        spList.add(0, newSP);
        showAdd = false;
        showConfirm = true;
        disableadd = false;
    }

    public void confirmRecord() throws Throwable {

        message = (String) spObject.insertrecord(newSP);
        if (message.equals("Validate Feild name is a requiered field Feild value is a requiered field")) {
            message = " Name is a required field ";
        }
        if (message.equals("Validate Feild value is a requiered field")) {
            message = " Value is a required field ";
        }

        if (message.contains("Row Has Been Inserted")) {
            showAdd = true;
            showConfirm = false;
            disableadd = true;
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void saveAll() throws Throwable {
        if (showConfirm == true) {
            message = "Please Confirm First";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } else {
            message = (String) spObject.save(spList);
            message = "Records are Updated";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }
}
