
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.ATMMACHINEBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.ATMMACHINECASHDTOINTER;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Aly
 */
@ManagedBean(name = "atmMachineView")
@ViewScoped
public class ATMMACHINeClient extends BaseBean implements Serializable {

    private ATMMACHINECASHDTOINTER selectedRecord, newRecord;
    private List<ATMMACHINECASHDTOINTER> recordsList;
    private int listcount;
    private String atmname, atmid;
    private List<String> privelageList;
    private ATMMACHINEBOInter engin;

    public ATMMACHINECASHDTOINTER getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(ATMMACHINECASHDTOINTER selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public ATMMACHINECASHDTOINTER getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(ATMMACHINECASHDTOINTER newRecord) {
        this.newRecord = newRecord;
    }

    public List<ATMMACHINECASHDTOINTER> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<ATMMACHINECASHDTOINTER> recordsList) {
        this.recordsList = recordsList;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public String getAtmname() {
        return atmname;
    }

    public void setAtmname(String atmname) {
        this.atmname = atmname;
    }

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    public List<String> getPrivelageList() {
        return privelageList;
    }

    public void setPrivelageList(List<String> privelageList) {
        this.privelageList = privelageList;
    }

    public ATMMACHINEBOInter getEngin() {
        return engin;
    }

    public void setEngin(ATMMACHINEBOInter engin) {
        this.engin = engin;
    }

    @PostConstruct
    public void init() {
        try {
            atmname = "";
            atmid = "";
            engin = BOFactory.createATMMACHINEBO();
            recordsList = engin.findAll();
            listcount = recordsList.size();
            privelageList = super.findPrivelage(super.GetUser().getUserid(), "atmmachinecash.xhtml");
        } catch (Throwable ex) {
            Logger.getLogger(privilegeStatusClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public void searchrecords() {
        try {
            if (atmname == null) {
                atmname = "";
            }
            if (atmid == null) {
                atmid = "";
            }
            recordsList = engin.findRecord(atmname, atmid);
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(privilegeStatusClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean insertPriv() {
        return super.findPrivelageOperation(privelageList, "INSERT");
    }

    public void addRecord() {
        newRecord = DTOFactory.createATMMACHINECASHDTO();
    }

    public void saveRecord() throws SQLException, Throwable {
        String message = "";

        message = engin.insertRecord(newRecord.getAtmid(), newRecord.getAtmname());
        searchrecords();
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("carDialog2.hide();");

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean updatePriv() {
        return super.findPrivelageOperation(privelageList, "UPDATE");
    }

    public void updateRecord() throws SQLException, Throwable {
        String message = "";
        message = engin.updateRecord(selectedRecord);
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("carDialog.hide();");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean deletePriv() {
        return super.findPrivelageOperation(privelageList, "DELETE");
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printreportPDF() throws Exception, Throwable {

        // String path = (String) engin.printPDFrep(super.GetClient(), super.GetUser().getusername(), atmname);
        //prepDownload(path);
    }

    public void deleteRecord() throws SQLException {
        String message = engin.deleteRecord(selectedRecord);
        recordsList.remove(selectedRecord);
        listcount = recordsList.size();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public List<String> completeContains(String query) {
        List<String> filtered = new ArrayList<String>();
        for (int i = 0; i < recordsList.size(); i++) {
            ATMMACHINECASHDTOINTER rec = recordsList.get(i);
            if (rec.getAtmname().toLowerCase().contains(query.toLowerCase())) {
                filtered.add(rec.getAtmname());
            }
        }
        return filtered;
    }

    public Boolean ValidateAccount(String AccountNumber) {
        if (AccountNumber.trim().length() == 16) {
            if (AccountNumber.trim().substring(0, 3).matches("[A-Z]*")) {
                if (AccountNumber.trim().substring(3, 16).matches("\\d+")) {
                    return Boolean.TRUE;
                }
            }
        }
        return Boolean.FALSE;
    }
}
