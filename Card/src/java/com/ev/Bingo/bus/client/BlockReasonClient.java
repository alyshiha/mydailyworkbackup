/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.blockedusersBOInter;
import com.ev.Bingo.bus.bo.blockreasonsBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.blockedusersDTOInter;
import com.ev.Bingo.bus.dto.blockreasonsDTOInter;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Administrator
 */
public class BlockReasonClient extends BaseBean {

    private Boolean showAdd, showConfirm;
    private Integer listSize;
    private List<blockreasonsDTOInter> brList;
    private blockreasonsBOInter brObject;
    private String message;
    private blockreasonsDTOInter brAttr;
    private blockedusersBOInter bUAttr;
    private blockreasonsDTOInter newBlock;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<blockreasonsDTOInter> getBrList() {
        return brList;
    }

    public void setBrList(List<blockreasonsDTOInter> brList) {
        this.brList = brList;
    }

    public blockreasonsBOInter getBrObject() {
        return brObject;
    }

    public void setBrObject(blockreasonsBOInter brObject) {
        this.brObject = brObject;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public BlockReasonClient() throws Throwable {
        super();
        GetAccess();
        brObject = BOFactory.createblockreasonsBO();
        bUAttr = BOFactory.createblockedusersBO();
        brList = (List<blockreasonsDTOInter>) brObject.GetAllRecords();
        showAdd = true;
        showConfirm = false;
        listSize = brList.size();
    }

    public void fillDel(ActionEvent e) {
        brAttr = (blockreasonsDTOInter) e.getComponent().getAttributes().get("removedRow");
    }

    public void fillUp(ActionEvent e) {
        brAttr = (blockreasonsDTOInter) e.getComponent().getAttributes().get("updateRow");
    }

    public void deleteBlockReason(ActionEvent e) throws Throwable {
        List<blockedusersDTOInter> HasUsers = (List<blockedusersDTOInter>) bUAttr.findRecordsListReasonID(brAttr);
        if (HasUsers.size() > 0) {
            message = "Can't delete, there are users Block By This User this profile";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } else {
            if (brAttr.getreasonid() != 0) {
                message = (String) brObject.deleterecord(brAttr);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(message));
                if (message.contains("Record Has Been")) {
                    brList.remove(brAttr);
                    showAdd = true;
                    showConfirm = false;
                }
            } else {
                brList.remove(brAttr);
                showAdd = true;
                showConfirm = false;
                message = "Record Has Been Removed";
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(message));
            }
        }
    }

    public void AddBlockReason() {
        if (brList == null) {
            brList = new ArrayList<blockreasonsDTOInter>();
        }
        newBlock = DTOFactory.createblockreasonsDTO();
        brList.add(0, newBlock);
        showAdd = false;
        showConfirm = true;
    }

    public void ConfirmBlockReason() throws Throwable {
        message = (String) brObject.insertrecord(newBlock);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
        if (message.contains("Record Has")) {
            showAdd = true;
            showConfirm = false;
        }
    }

    public void saveAll() throws Throwable {
        if (showConfirm == false) {
            message = (String) brObject.save(brList);
            message = "Records are Updated";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } else {
            message = "please confirm first";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }
}
