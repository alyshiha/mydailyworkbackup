/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.transactiontypeBOInter;
import com.ev.Bingo.bus.bo.transactiontypemasterBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.transactiontypeDTOInter;
import com.ev.Bingo.bus.dto.transactiontypemasterDTOInter;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author Administrator
 */
public class TransactionTypeClient extends BaseBean {

    private transactiontypeBOInter cdObject;
    private transactiontypemasterBOInter TMObject;
    private List<transactiontypemasterDTOInter> cdList;
    private List<transactiontypeDTOInter> cdDetailsList;
    private transactiontypemasterDTOInter timeShiftAttr;
    private transactiontypemasterDTOInter timeShiftAttr1;
    private transactiontypemasterDTOInter selectedMT;
    private transactiontypemasterDTOInter newRecord;
    private transactiontypeDTOInter newRecord1;
    private transactiontypeDTOInter timeShiftDetailsAttr, timeShiftDetailsAttr1;
    private Integer myInt;
    private Boolean showAdd, showAdd1;
    private Boolean showConfirm, showConfirm1;
    private String message;
    private String message1;

    public List<transactiontypeDTOInter> getCdDetailsList() {
        return cdDetailsList;
    }

    public void setCdDetailsList(List<transactiontypeDTOInter> cdDetailsList) {
        this.cdDetailsList = cdDetailsList;
    }

    public List<transactiontypemasterDTOInter> getCdList() {
        return cdList;
    }

    public void setCdList(List<transactiontypemasterDTOInter> cdList) {
        this.cdList = cdList;
    }

    public transactiontypeBOInter getCdObject() {
        return cdObject;
    }

    public void setCdObject(transactiontypeBOInter cdObject) {
        this.cdObject = cdObject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public Integer getMyInt() {
        return myInt;
    }

    public void setMyInt(Integer myInt) {
        this.myInt = myInt;
    }

    public transactiontypemasterDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(transactiontypemasterDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public transactiontypeDTOInter getNewRecord1() {
        return newRecord1;
    }

    public void setNewRecord1(transactiontypeDTOInter newRecord1) {
        this.newRecord1 = newRecord1;
    }

    public transactiontypemasterDTOInter getSelectedMT() {
        return selectedMT;
    }

    public void setSelectedMT(transactiontypemasterDTOInter selectedMT) {
        this.selectedMT = selectedMT;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public transactiontypemasterDTOInter getTimeShiftAttr() {
        return timeShiftAttr;
    }

    public void setTimeShiftAttr(transactiontypemasterDTOInter timeShiftAttr) {
        this.timeShiftAttr = timeShiftAttr;
    }

    public transactiontypemasterDTOInter getTimeShiftAttr1() {
        return timeShiftAttr1;
    }

    public void setTimeShiftAttr1(transactiontypemasterDTOInter timeShiftAttr1) {
        this.timeShiftAttr1 = timeShiftAttr1;
    }

    public transactiontypeDTOInter getTimeShiftDetailsAttr() {
        return timeShiftDetailsAttr;
    }

    public void setTimeShiftDetailsAttr(transactiontypeDTOInter timeShiftDetailsAttr) {
        this.timeShiftDetailsAttr = timeShiftDetailsAttr;
    }

    public transactiontypemasterBOInter getTMObject() {
        return TMObject;
    }

    public void setTMObject(transactiontypemasterBOInter TMObject) {
        this.TMObject = TMObject;
    }

    public transactiontypeDTOInter getTimeShiftDetailsAttr1() {
        return timeShiftDetailsAttr1;
    }

    public void setTimeShiftDetailsAttr1(transactiontypeDTOInter timeShiftDetailsAttr1) {
        this.timeShiftDetailsAttr1 = timeShiftDetailsAttr1;
    }

    public TransactionTypeClient() throws Throwable {
        super();
        GetAccess();
        cdObject = BOFactory.createtransactiontypeBO();
        TMObject = BOFactory.createtransactiontypemasterBO();
        cdList = (List<transactiontypemasterDTOInter>) TMObject.GetAllRecords();
        timeShiftAttr1 = DTOFactory.createtransactiontypemasterDTO();
        showAdd = true;
        showConfirm = false;
        showAdd1 = true;
        showConfirm1 = false;
        cdDetailsList = new ArrayList<transactiontypeDTOInter>();
    }

    public void fillDelete(ActionEvent e) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("This Process Might Take Long Time"));
        timeShiftAttr1 = (transactiontypemasterDTOInter) e.getComponent().getAttributes().get("removedRow");
        message = "";
        message1 = "";
    }

    public void deleteRecord() {
        try {
            String Temp = "";
            Integer count;
            if (timeShiftAttr1.getid() == 0) {
                message = "Record Has Been Removed";
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(message));
                cdList.remove(timeShiftAttr1);
                cdDetailsList.clear();
                showAdd = true;
                showConfirm = false;
            }

            cdDetailsList = (List<transactiontypeDTOInter>) cdObject.GetListOfRecords(timeShiftAttr1);
            Temp = (String) TMObject.deleterecord(timeShiftAttr1);
            if (Temp.contains("Record Has Been deleted")) {
                message = Temp;
                count = cdDetailsList.size();
                message = message + " " + count + " Transaction Type Detail Has Been Deleted";
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(message));
                cdList.remove(timeShiftAttr1);
                cdDetailsList.clear();
                showAdd = true;
                showConfirm = false;

            }
        } catch (Throwable ex) {
            message = "child record found";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }

    public void onRowSelect1(SelectEvent e) throws Throwable {
        selectedMT = DTOFactory.createtransactiontypemasterDTO();
        selectedMT = (transactiontypemasterDTOInter) e.getObject();
        cdDetailsList = (List<transactiontypeDTOInter>) cdObject.GetListOfRecords(selectedMT);
        message = "";
        message1 = "";
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void onRowUnSelect1(UnselectEvent e) throws Throwable {
        cdDetailsList = null;
        message = "";
        message1 = "";
        showAdd1 = false;
        showConfirm1 = false;
    }

    public void addMasterTrans() {
        newRecord = DTOFactory.createtransactiontypemasterDTO();
        cdList.add(0, newRecord);
        selectedMT = newRecord;
        showAdd = false;
        showConfirm = true;
        message = "";
        message1 = "";
        cdDetailsList.clear();
    }

    public void confirmAdd() throws Throwable {
        message = (String) TMObject.insertrecord(newRecord);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
        if (message.contains("Record Has Been Inserted")) {
            showAdd = true;
            showConfirm = false;

        }
    }

    public void saveAll() throws Throwable {
        if (showAdd == true) {
            message = (String) TMObject.save(cdList);
            message = "Records are Updated";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));

        } else {
            message = "PLZ Confirm First";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }

    public void fillDelete1(ActionEvent e) {
        timeShiftDetailsAttr1 = (transactiontypeDTOInter) e.getComponent().getAttributes().get("removedRow1");
        message1 = "";
        message = "";
    }

    public void deleteRecord1() throws Throwable {

        message1 = (String) cdObject.deleterecord(timeShiftDetailsAttr1);
        if (message1.contains("Record Has Been deleted")) {
            cdDetailsList.remove(timeShiftDetailsAttr1);
            showAdd1 = true;
            showConfirm1 = false;
        }
        if (message1.contains("  id  ")) {
            cdDetailsList.remove(timeShiftDetailsAttr1);
            showAdd1 = true;
            showConfirm1 = false;
            message1 = "Record Has Been Removed";

        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }

    public void addTransTypeDetail() {
        newRecord1 = DTOFactory.createtransactiontypeDTO();
        newRecord1.settypemaster(selectedMT.getid());
        newRecord1.settypecategory(1);
        cdDetailsList.add(0, newRecord1);

        showAdd1 = false;
        showConfirm1 = true;
        message1 = "";
        message = "";
    }

    public void confirmAdd1() throws Throwable {
        message1 = (String) cdObject.insertrecord(newRecord1);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
        if (message1.contains("Record Has Been Inserted")) {
            showAdd1 = true;
            showConfirm1 = false;

        }
    }

    public void saveAllDetail() throws Throwable {
        if (showAdd1 == true) {
            message1 = (String) cdObject.save(cdDetailsList);
            message1 = "Records are Updated";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message1));

        } else {
            message1 = "PLZ Confirm First";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message1));
        }
    }
}
