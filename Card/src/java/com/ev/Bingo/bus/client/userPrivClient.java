/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;


import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.menulabelsBOInter;
import com.ev.Bingo.bus.bo.privilegeStatusNameBOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import com.ev.Bingo.bus.dto.menulabelsDTOInter;
import com.ev.Bingo.bus.dto.privilegeStatusNameDTOInter;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author AlyShiha
 */
@ManagedBean(name = "userPrivView")
@ViewScoped
public class userPrivClient extends BaseBean implements Serializable {

    private int userscount;
    private int pagescount;
    private int privcount;

    private privilegeStatusNameDTOInter[] selectedprivilege;
    private menulabelsDTOInter selectedpage;
    private usersDTOInter selecteduser;

    private List<usersDTOInter> usersList;
    private List<menulabelsDTOInter> pagesList;
    private List<privilegeStatusNameDTOInter> privilegeList;
    private List<String> privList;

    
    private menulabelsBOInter enginmenuLabel;
    private privilegeStatusNameBOInter enginprivilegeStatus;

    public int getUserscount() {
        return userscount;
    }

    public void setUserscount(int userscount) {
        this.userscount = userscount;
    }

    public int getPagescount() {
        return pagescount;
    }

    public void setPagescount(int pagescount) {
        this.pagescount = pagescount;
    }

    public int getPrivcount() {
        return privcount;
    }

    public void setPrivcount(int privcount) {
        this.privcount = privcount;
    }

    public privilegeStatusNameDTOInter[] getSelectedprivilege() {
        return selectedprivilege;
    }

    public void setSelectedprivilege(privilegeStatusNameDTOInter[] selectedprivilege) {
        this.selectedprivilege = selectedprivilege;
    }

    public menulabelsDTOInter getSelectedpage() {
        return selectedpage;
    }

    public void setSelectedpage(menulabelsDTOInter selectedpage) {
        this.selectedpage = selectedpage;
    }

    public usersDTOInter getSelecteduser() {
        return selecteduser;
    }

    public void setSelecteduser(usersDTOInter selecteduser) {
        this.selecteduser = selecteduser;
    }

    public List<usersDTOInter> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<usersDTOInter> usersList) {
        this.usersList = usersList;
    }

    public List<menulabelsDTOInter> getPagesList() {
        return pagesList;
    }

    public void setPagesList(List<menulabelsDTOInter> pagesList) {
        this.pagesList = pagesList;
    }

    public List<privilegeStatusNameDTOInter> getPrivilegeList() {
        return privilegeList;
    }

    public void setPrivilegeList(List<privilegeStatusNameDTOInter> privilegeList) {
        this.privilegeList = privilegeList;
    }

    public List<String> getPrivList() {
        return privList;
    }

    public void setPrivList(List<String> privList) {
        this.privList = privList;
    }

  

    public menulabelsBOInter getEnginmenuLabel() {
        return enginmenuLabel;
    }

    public void setEnginmenuLabel(menulabelsBOInter enginmenuLabel) {
        this.enginmenuLabel = enginmenuLabel;
    }

    public privilegeStatusNameBOInter getEnginprivilegeStatus() {
        return enginprivilegeStatus;
    }

    public void setEnginprivilegeStatus(privilegeStatusNameBOInter enginprivilegeStatus) {
        this.enginprivilegeStatus = enginprivilegeStatus;
    }

    @PostConstruct
    public void init() {
        try {
            enginmenuLabel = BOFactory.createmenulabelsBO();
            enginprivilegeStatus = BOFactory.createprivilegeStatusNameBO(null);

            usersList = super.GetUsers();
            pagesList = (List<menulabelsDTOInter>) enginmenuLabel.GetAllRecords2();
            privilegeList = enginprivilegeStatus.findAll();
            privList = super.findPrivelage(super.GetUser().getUserid(), "");

            userscount = usersList.size();
            pagescount = pagesList.size();
            privcount = privilegeList.size();
        } catch (Throwable ex) {
            Logger.getLogger(userPrivClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void handleMasterChange() throws Throwable {
        if (selecteduser != null && selectedpage != null) {
            System.out.println(selecteduser.getUsername()+ " " + selectedpage.getenglishlabel());
            DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:singleDT3");
            repTable.setSelection(enginprivilegeStatus.findprivassigned("" + selecteduser.getUserid(), "" + selectedpage.getmenuid()).toArray());
        } else {
            selectedprivilege = null;
            System.out.println("No");
        }
    }

    public void saveChange() throws Throwable {
        String result = enginprivilegeStatus.deleteprivassigned("" + selecteduser.getUserid(), "" + selectedpage.getmenuid());
        if ("Record Has Been Succesfully Deleted".equals(result)) {
            for (privilegeStatusNameDTOInter selectedprivilegeitem : selectedprivilege) {
                enginprivilegeStatus.insertpriv("" + selecteduser.getUserid(), "" + selectedpage.getmenuid(), "" + selectedprivilegeitem.getPrev_id());
            }
            result = "Record Has Been Succesfully Saved";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", result));
        System.out.println(selecteduser.getUsername()+ " " + selectedpage.getenglishlabel() + " " + selectedprivilege.length);
    }
}
