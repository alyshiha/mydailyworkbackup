/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.blockedusersBOInter;
import com.ev.Bingo.bus.bo.blockreasonsBOInter;
import com.ev.Bingo.bus.bo.usersBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.blockedusersDTOInter;
import com.ev.Bingo.bus.dto.blockreasonsDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import javax.faces.event.ActionEvent;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Administrator
 */
public class BlockUserClient extends BaseBean {

    private blockedusersBOInter buObject;
    private usersBOInter userObject;
    private blockreasonsBOInter BlockResonObject;
    private List<usersDTOInter> buList, bList;
    private List<blockedusersDTOInter> unbuList;
    private blockedusersDTOInter buAttr;
    private List<blockreasonsDTOInter> reasonList;
    private usersDTOInter holdUser;
    private blockedusersDTOInter unholdUser;
    private Integer myInt, listSize, listSize1, userid, reasonid;
    private String message;

    public Integer getReasonid() {
        return reasonid;
    }

    public void setReasonid(Integer reasonid) {
        this.reasonid = reasonid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public List<usersDTOInter> getbList() {
        return bList;
    }

    public void setbList(List<usersDTOInter> bList) {
        this.bList = bList;
    }

    public Integer getListSize1() {
        return listSize1;
    }

    public void setListSize1(Integer listSize1) {
        this.listSize1 = listSize1;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public blockedusersDTOInter getBuAttr() {
        return buAttr;
    }

    public void setBuAttr(blockedusersDTOInter buAttr) {
        this.buAttr = buAttr;
    }

    public blockedusersBOInter getBuObject() {
        return buObject;
    }

    public void setBuObject(blockedusersBOInter buObject) {
        this.buObject = buObject;
    }

    public usersDTOInter getHoldUser() {
        return holdUser;
    }

    public void setHoldUser(usersDTOInter holdUser) {
        this.holdUser = holdUser;
    }

    public blockedusersDTOInter getUnholdUser() {
        return unholdUser;
    }

    public void setUnholdUser(blockedusersDTOInter unholdUser) {
        this.unholdUser = unholdUser;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public List<blockedusersDTOInter> getUnbuList() {
        return unbuList;
    }

    public void setUnbuList(List<blockedusersDTOInter> unbuList) {
        this.unbuList = unbuList;
    }

    public Integer getMyInt() {
        return myInt;
    }

    public void setMyInt(Integer myInt) {
        this.myInt = myInt;
    }

    public List<blockreasonsDTOInter> getReasonList() {
        return reasonList;
    }

    public void setReasonList(List<blockreasonsDTOInter> reasonList) {
        this.reasonList = reasonList;
    }

    public List<usersDTOInter> getBuList() {
        return buList;
    }

    public void setBuList(List<usersDTOInter> buList) {
        this.buList = buList;
    }

    /**
     * Creates a new instance of BlockUserClient
     */
    public BlockUserClient() throws Throwable {
        super();
        GetAccess();
        buObject = BOFactory.createblockedusersBO();
        userObject = BOFactory.createusersBO();
        BlockResonObject = BOFactory.createblockreasonsBO();
        buList = (List<usersDTOInter>) userObject.findAllUnBlockRecords();
        reasonList = (List<blockreasonsDTOInter>) BlockResonObject.GetAllRecords();
        bList = (List<usersDTOInter>) userObject.findAllBlockRecords();
        unbuList = (List<blockedusersDTOInter>) buObject.GetAllRecords();
        buAttr = DTOFactory.createblockedusersDTO();
        listSize = buList.size();
        listSize1 = unbuList.size();
    }

    public void fillBlockedUser(ActionEvent e) throws Throwable {
        holdUser = (usersDTOInter) e.getComponent().getAttributes().get("blockedUser");

    }

    public void fillUnBlockedUser(ActionEvent e) throws Throwable {
        
        unholdUser = (blockedusersDTOInter) e.getComponent().getAttributes().get("unblockedUser");
        System.out.println(unholdUser.getUsername());
    }

    public void unBlockUser(ActionEvent e) throws Throwable {

        message = (String) buObject.deleterecord(unholdUser);
        if (message.contains("Row Has Been ")) {
            unbuList.remove(unholdUser);
            message = "User " + unholdUser.getUsername() + " Has Been UnBlocked";
        }
        listSize1 = listSize1 - 1;
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void blockUser() throws Throwable {
        buAttr.setReasonid(myInt);
        buAttr.setUserid(holdUser.getUserid());
        buAttr.setUsername(holdUser.getUseraname());
        message = (String) buObject.insertrecord(buAttr);
        if (message.contains("Row Has Been Inserted")) {
            message = "User " + holdUser.getUsername()+ " Has Been Blocked";
            buList.remove(holdUser);
        }
        listSize = listSize - 1;
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));

    }
}
