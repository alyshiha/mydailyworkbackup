/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import DBCONN.ConnectionPool;
import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.client.BaseBean;

import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.LoginBOInter;
import com.ev.Bingo.bus.bo.licenseBOInter;
import com.ev.Bingo.bus.bo.usersBOInter;
import com.ev.Bingo.bus.dto.licenseDTOInter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JFileChooser;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Administrator
 */
public class LoginClient extends BaseBean implements Serializable {

    public LoginClient() {
        FacesContext context2 = FacesContext.getCurrentInstance();
        HttpSession session2 = (HttpSession) context2.getExternalContext().getSession(true);
        HttpServletResponse response2 = (HttpServletResponse) context2.getExternalContext().getResponse();
        response2.addHeader("X-Frame-Options", "DENY");
        try {
            //super.clearsessionattributes();
        } catch (Throwable ex) {
            Logger.getLogger(LoginClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    String userName, password, loading, bankName;
    private Boolean lincform;

    public Boolean getLincform() {
        return lincform;
    }

    public void setLincform(Boolean lincform) {
        this.lincform = lincform;
    }

    public final void test() throws InterruptedException {
        File fin = new File("C:/Program Files/Apache Software Foundation/Tomcat 7.0/webapps/AtmBingo/PDF/");
        File[] finlist = fin.listFiles();
        for (int n = 0; n < finlist.length; n++) {
            if (finlist[n].isFile()) {
                System.gc();
                Thread.sleep(2000);
                finlist[n].delete();
            }
        }
    }

    public String getLoading() {
        return loading;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public void setLoading(String loading) {
        this.loading = loading;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    private String version = "2.1.1";

    public Integer SetLoginTrial() throws Throwable {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("LoginTrial") == null) {
            session.setAttribute("LoginTrial", (Integer) 1);
        }
        Integer trial = (Integer) session.getAttribute("LoginTrial");
        trial = trial + 1;
        session.setAttribute("LoginTrial", trial);
        return trial;
    }

    public String validateLogin() throws Throwable {
        Thread.sleep(1000);
        super.clearsessionattributes();
        super.OpenHTTPSession();
        FacesContext context2 = FacesContext.getCurrentInstance();
        HttpSession session2 = (HttpSession) context2.getExternalContext().getSession(true);
        HttpServletResponse response2 = (HttpServletResponse) context2.getExternalContext().getResponse();
        session2.setAttribute("username", (String) userName);
        //super.createconnection();

        response2.addHeader("X-Frame-Options", "DENY");
        FacesContext context = FacesContext.getCurrentInstance();
        //if (CoonectionHandler.getInstance().validateuser()) {

            licenseBOInter LicBO = BOFactory.createlicenseBO();
            List<licenseDTOInter> LDTO = (List<licenseDTOInter>) LicBO.GetAllRecords();
            if (!LDTO.isEmpty()) {
                super.setLicenseSessions(LDTO.get(0));
                super.SetIP();
                LoginBOInter LogBo = BOFactory.createLoginBO();
                usersBOInter UserBO = BOFactory.createusersBO();
                String LoginResult = LogBo.ValidateLog(userName, password, LDTO.get(0).getversion(), LDTO.get(0).getnoofusers(), LDTO.get(0).getenddate());
                if (LoginResult.equals("Wrong user name") || LoginResult.equals("Wrong Password")) {
                    LoginResult = "authentication error";
                }
                //CreateDTO CDTO = new CreateDTO();
                if (LoginResult.contains("asswor")) {
                    if (SetLoginTrial() > 3) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "User Has Been Locked", ""));
                        UserBO.updateStatus(userName);
                    } else if (LoginResult.equals("Password Expired")) {
                        super.SetUser(userName);
                        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                        response.sendRedirect("faces/PasswordExpire.xhtml");
                    } else {
                        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Wrong Password ", ""));
                    }
                    //CoonectionHandler.getInstance().freeuser(userName);
                    return "Fail";
                } else {
                    if (LDTO.get(0).getversion().equals(version)) {
                        if (LoginResult.equals("Valid")) {
                            super.SetIP();
                            super.SetUser(userName);
                            super.SetLogin();
                            CoonectionHandler.getInstance().returnAllConnection();
//                            CoonectionHandler.getInstance().adduser();
                            return "Success";
                        } else if (LoginResult.equals("New User")) {
                            super.SetUser(userName);
                            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                            response.sendRedirect("faces/ChangePassword.xhtml");
                        } else {
                            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, LoginResult, ""));
//                            CoonectionHandler.getInstance().freeuser(userName);
                            return "Fail";
                        }
                    } else {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "uncompatable version", ""));
                  //      CoonectionHandler.getInstance().freeuser(userName);
                        return "Fail";
                    }
                }
            } else {
                HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                response.sendRedirect("faces/LincCreator.xhtml");
            }
            //CoonectionHandler.getInstance().freeuser(userName);
            return "Fail";
       
    }
    private JFileChooser chooser;

    public JFileChooser getChooser() {
        return chooser;
    }

    public void setChooser(JFileChooser chooser) {
        this.chooser = chooser;
    }

    private Part file;
    private String fileContent;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public void upload(Integer page) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (file != null) {
            String MSG = getprop(file);
            if ("license Has Been Uploaded".equals(MSG)) {
                try {
                    if (page == 2) {
                        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
                        clearsessionattributes();
                        session.invalidate();
                        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                        response.sendRedirect("faces/Login.xhtml");
                    } else {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "license Has Been Uploaded", ""));
                    }
                } catch (Throwable ex) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error In Redirecting To Login Page", ""));
                }
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, MSG, ""));
            }
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please Select File", ""));
        }

    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    public void carregarArquivo(FileUploadEvent event)
            throws FileNotFoundException, IOException {

        FacesMessage msg = new FacesMessage("Sucesso", event.getFile()
                .getFileName() + " foi carregado.");
        FacesContext.getCurrentInstance().addMessage("teste", msg);

        String caminho = FacesContext.getCurrentInstance().getExternalContext()
                .getRealPath("" + "\\fotos\\" + event.getFile().getFileName());

        byte[] conteudo = event.getFile().getContents();
        FileOutputStream fos = new FileOutputStream(caminho);
        fos.write(conteudo);
        fos.close();

    }
    /* 
     public void browseFolder() throws ClassNotFoundException, SQLException, ParseException {
     if (file != null) {
     getprop(file);
     FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
     FacesContext.getCurrentInstance().addMessage(null, message);
     }

     FileDialog fc;
     fc = new FileDialog(new Frame(), "Open file", FileDialog.LOAD);
     fc.toFront();
     fc.setDirectory(".");
     fc.setLocation(50, 50);
     fc.setVisible(true);
     String path = fc.getDirectory() + fc.getFile();
     if (!"nullnull".equals(path)) {
     getprop(path);
     return Boolean.TRUE;
     } else {
     return Boolean.FALSE;
     }
     } catch (ClassNotFoundException ex) {
     Logger.getLogger(LoginClient.class.getName()).log(Level.SEVERE, null, ex);
     return Boolean.FALSE;
     } catch (SQLException ex) {
     Logger.getLogger(LoginClient.class.getName()).log(Level.SEVERE, null, ex);
     return Boolean.FALSE;
     } catch (ParseException ex) {
     Logger.getLogger(LoginClient.class.getName()).log(Level.SEVERE, null, ex);
     return Boolean.FALSE;
     }
     */
//    }

    private String updatelinccards(String NumOfUsers, String nooftran, String version, String enddate, String LincCode) {
        Connection conn = null;
        try {
            conn = CoonectionHandler.getInstance().getConnection();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LoginClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(LoginClient.class.getName()).log(Level.SEVERE, null, ex);
        }

        Statement statement;
        try {
            statement = conn.createStatement();
        } catch (SQLException ex) {
            return "Unable To Create Statment";
        }
        int msg = 0;
        String Query = "update license\n"
                + "set no_of_users = '" + NumOfUsers + "',\n"
                + "no_of_tran = '" + nooftran + "',\n"
                + "version = '" + version + "',\n"
                + "end_date = to_date('" + enddate.substring(0, 10).replace("-", ".") + "','yyyy.mm.dd'),\n"
                + "license_key = '" + LincCode + "'";
        try {
            msg = statement.executeUpdate(Query);
            CoonectionHandler.getInstance().returnConnection(conn);
        } catch (SQLException ex) {
            return "Invalid File Input";
        } catch (Exception ex) {
            return "Connection Failed";
        }
        if (msg > 0) {
            return "license Has Been Uploaded";
        } else {
            return "license Error";
        }
    }

    private String getprop(Part path) {
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = path.getInputStream();
            if (input == null) {
                return "File Is Empty";
            }
            prop.load(input);
            String NumOfUsers = prop.getProperty("NumOfUsers");
            String NumOfTransactions = prop.getProperty("NumOfTransactions");
            String Version = prop.getProperty("Version");
            String EndDate = prop.getProperty("EndDate");
            String LincCode = prop.getProperty("LincCode");
            if (NumOfUsers != null && !"".equals(NumOfUsers) && NumOfTransactions != null && !"".equals(NumOfTransactions) && Version != null && !"".equals(Version) && EndDate != null && !"".equals(EndDate) && LincCode != null && !"".equals(LincCode)) {
                String updatemsg = updatelinccards(NumOfUsers, NumOfTransactions, Version, EndDate, LincCode);
                return updatemsg;
            } else {
                return "Invalid LICENSE file";
            }

        } catch (IOException ex) {
            return ex.getMessage();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
