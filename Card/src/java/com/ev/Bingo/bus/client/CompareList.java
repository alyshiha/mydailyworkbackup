/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;

/**
 *
 * @author Aly.Shiha
 */
public class CompareList {

   

 public static ArrayList getLog(List<?> beforeList, List<?> afterList, String UniqueValue) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        
        ArrayList result = new ArrayList();
        for (Object beforeRecord : beforeList) {
            Object beforeKey = (Object) BeanUtils.describe(beforeRecord).get(UniqueValue);
            Boolean RecordFound = Boolean.FALSE;
            for (Object afterRecord : afterList) {
                if (BeanUtils.describe(afterRecord).get(UniqueValue).equals(beforeKey)) {
                    ArrayList temp = compareBeans(beforeRecord, afterRecord);                   
                    RecordFound = Boolean.TRUE;
                    result.add("Record Key:" + beforeKey);
                    if (temp.size() > 0) {
                        result.addAll(temp);
                    } else {
                        result.add("No Change");
                    }
                    afterList.remove(afterRecord);
                    break;
                }
            }
            if (!RecordFound) {
                result.add("Record Key:" + beforeKey);
                result.add("Has Been Deleted");
            }
        }
        return result;
    }

    public static ArrayList compareBeans(Object oldBean,Object newBean) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        if (newBean.getClass() != oldBean.getClass()) {
            throw new IllegalArgumentException("The beans must be from the same Class!");
        }
        ArrayList changesArr = new ArrayList();
        Iterator keysIt = BeanUtils.describe(newBean).keySet().iterator();
        while (keysIt.hasNext()) {
            String key = (String) keysIt.next();
            Object oldValue = PropertyUtils.getProperty(oldBean, key);
            Object newValue = PropertyUtils.getProperty(newBean, key);
            if (oldValue != newValue) {// Ignores when both are "null"
                if (((oldValue != null) && !oldValue.equals(newValue))
                        || ((newValue != null) && !newValue.equals(oldValue))) {
                    changesArr.add("Field:" + key + " has been updated from " + oldValue + " to " + newValue);
                }
            }
        }
        return changesArr;
    }

    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
    }

}
