/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.commissionfeesBOInter;
import com.ev.Bingo.bus.bo.commissionfeesdetailBoInter;
import com.ev.Bingo.bus.bo.networksBOInter;
import com.ev.Bingo.bus.bo.transactiontypeBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.commissionfeesDTOInter;
import com.ev.Bingo.bus.dto.commissionfeesdetailDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import com.ev.Bingo.bus.dto.transactiontypeDTOInter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author Aly
 */
public class commfeeClient extends BaseBean {

    private List<networksDTOInter> nL;
    private networksBOInter NBOEngin;
    private networksDTOInter selectednetwork;
    private commissionfeesBOInter CFBOEngin;
    private List<commissionfeesDTOInter> cfL;
    private commissionfeesDTOInter temprecordCF;
    private commissionfeesdetailBoInter CFDBOEngin;
    private List<commissionfeesdetailDTOInter> cfdL;
    private commissionfeesdetailDTOInter temprecordCFD;
    private String message;
    private Boolean showAdd, showConfirm, showAdd1, showConfirm1;
    private List<transactiontypeDTOInter> transtypelist;
    private transactiontypeBOInter transtypeEngin;

    public List<networksDTOInter> getnL() {
        return nL;
    }

    public void setnL(List<networksDTOInter> nL) {
        this.nL = nL;
    }

    public networksBOInter getNBOEngin() {
        return NBOEngin;
    }

    public void setNBOEngin(networksBOInter NBOEngin) {
        this.NBOEngin = NBOEngin;
    }

    public networksDTOInter getSelectednetwork() {
        return selectednetwork;
    }

    public void setSelectednetwork(networksDTOInter selectednetwork) {
        this.selectednetwork = selectednetwork;
    }

    public commissionfeesBOInter getCFBOEngin() {
        return CFBOEngin;
    }

    public void setCFBOEngin(commissionfeesBOInter CFBOEngin) {
        this.CFBOEngin = CFBOEngin;
    }

    public List<commissionfeesDTOInter> getCfL() {
        return cfL;
    }

    public void setCfL(List<commissionfeesDTOInter> cfL) {
        this.cfL = cfL;
    }

    public commissionfeesDTOInter getTemprecordCF() {
        return temprecordCF;
    }

    public void setTemprecordCF(commissionfeesDTOInter temprecordCF) {
        this.temprecordCF = temprecordCF;
    }

    public commissionfeesdetailBoInter getCFDBOEngin() {
        return CFDBOEngin;
    }

    public void setCFDBOEngin(commissionfeesdetailBoInter CFDBOEngin) {
        this.CFDBOEngin = CFDBOEngin;
    }

    public List<commissionfeesdetailDTOInter> getCfdL() {
        return cfdL;
    }

    public void setCfdL(List<commissionfeesdetailDTOInter> cfdL) {
        this.cfdL = cfdL;
    }

    public commissionfeesdetailDTOInter getTemprecordCFD() {
        return temprecordCFD;
    }

    public void setTemprecordCFD(commissionfeesdetailDTOInter temprecordCFD) {
        this.temprecordCFD = temprecordCFD;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public List<transactiontypeDTOInter> getTranstypelist() {
        return transtypelist;
    }

    public void setTranstypelist(List<transactiontypeDTOInter> transtypelist) {
        this.transtypelist = transtypelist;
    }

    public transactiontypeBOInter getTranstypeEngin() {
        return transtypeEngin;
    }

    public void setTranstypeEngin(transactiontypeBOInter transtypeEngin) {
        this.transtypeEngin = transtypeEngin;
    }

    public commfeeClient() throws Throwable {
        GetAccess();
        NBOEngin = BOFactory.createnetworksBO();
        CFBOEngin = BOFactory.createcommissionfeesBO();
        CFDBOEngin = BOFactory.createcommissionfeesdetailBO();
        transtypeEngin = BOFactory.createtransactiontypeBO();
        transtypelist = (List<transactiontypeDTOInter>) transtypeEngin.GetAllRecords();
        nL = (List<networksDTOInter>) NBOEngin.GetAllRecords();
        cfL = new ArrayList<commissionfeesDTOInter>();
        cfdL = new ArrayList<commissionfeesdetailDTOInter>();
        showAdd = true;
        showConfirm = false;
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void onRowSelect1(SelectEvent e) throws Throwable {
        selectednetwork = (networksDTOInter) e.getObject();
        cfL = (List<commissionfeesDTOInter>) CFBOEngin.GetListOfRecords(selectednetwork);
        cfdL.clear();
        temprecordCF = null;
        showAdd1 = true;
        showConfirm1 = false;
        showAdd = true;
        showConfirm = false;
    }

    public void onRowUnSelect1(UnselectEvent e) {
        cfdL.clear();
        selectednetwork = null;

    }

    public void onRowSelect2(SelectEvent e) throws Throwable {
        temprecordCF = (commissionfeesDTOInter) e.getObject();
        cfdL = (List<commissionfeesdetailDTOInter>) CFDBOEngin.GetListOfRecords(temprecordCF);
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void onRowUnSelect2(UnselectEvent e) {
        temprecordCF = null;
    }

    public void fillAttr(ActionEvent e) {
        temprecordCF = (commissionfeesDTOInter) e.getComponent().getAttributes().get("removedRow");
    }

    public void addComm() {
        temprecordCF = DTOFactory.createcommissionfeesDTO();
        temprecordCF.setnetworkid(selectednetwork.getid());
        cfL.add(0, temprecordCF);
        cfdL.clear();
        showAdd = false;
        showConfirm = true;
    }

    public void confirmAddComm() throws Throwable {
        if (!"".equals(temprecordCF.getban())) {
            try {
                message = (String) CFBOEngin.insertrecord(temprecordCF);
            } catch (Throwable ex) {
                message = "Can`t insert Duplicate Pin To Same Network";
            }
            if (message.contains("Has Been Inserted")) {
                cfdL = (List<commissionfeesdetailDTOInter>) CFDBOEngin.GetListOfRecords(temprecordCF);
                showAdd = true;
                showConfirm = false;
            }
        } else {
            message = "Please Insert Pin";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void fillAttrdel(ActionEvent e) {
        temprecordCFD = (commissionfeesdetailDTOInter) e.getComponent().getAttributes().get("removedMatching");
    }

    public void addDetail() {
        temprecordCFD = DTOFactory.createcommissionfeesdetailDTO();
        temprecordCFD.setcommissionfeesid(temprecordCF.getid());
        temprecordCFD.settranstype(-1);
        cfdL.add(0, temprecordCFD);
        showAdd1 = false;
        showConfirm1 = true;
    }

    public void confirmDetail() throws Throwable {
        if (temprecordCFD.gettranstype() != -1) {
            if (temprecordCFD.getcomm() != 0 || temprecordCFD.getfees() != 0) {
                try {
                    message = (String) CFDBOEngin.insertrecord(temprecordCFD);
                } catch (Throwable ex) {
                    message = "Can`t insert Duplicate Transaction Type To Same Pin";
                }
                if (message.contains("Record Has Been Inserted")) {
                    showAdd1 = true;
                    showConfirm1 = false;
                    cfdL = (List<commissionfeesdetailDTOInter>) CFDBOEngin.GetListOfRecords(temprecordCF);
                }
            } else {
                message = "Please Insert Commision Or Fees";
            }
        } else {
            message = "Please Select Transaction Type";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void deletepin(ActionEvent e) throws Throwable {
        if (!"".equals(temprecordCF.getid())) {
            message = (String) CFDBOEngin.deleteallrecord(temprecordCF);
            if (message.contains("Record Has Been Deleted")) {
                cfL.remove(temprecordCF);
                message = message + " " + CFBOEngin.deleterecord(temprecordCF);
                cfdL.clear();
                showAdd = true;
                showConfirm = false;
            }
        } else {
            cfL.remove(temprecordCF);
            cfdL.clear();
            showAdd = true;
            showConfirm = false;
            message = "Record Has Been Removed";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void deletedetail(ActionEvent e) throws Throwable {
        if (!"".equals(temprecordCFD.getid())) {
            message = (String) CFDBOEngin.deleterecord(temprecordCFD);
            if (message.contains("Record Has Been Deleted")) {
                cfdL.remove(temprecordCFD);
                showAdd1 = true;
                showConfirm1 = false;
            }
        } else {
            cfdL.remove(temprecordCFD);
            showAdd1 = true;
            showConfirm1 = false;
            message = "Record Has Been Removed";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void saveAllDetails() throws Throwable {
        if (true == showAdd1) {
            Boolean nullvalues = Boolean.FALSE;
            for (commissionfeesdetailDTOInter re : cfdL) {
                if(re.getcomm() == 0 && re.getfees() == 0 ){
                nullvalues = Boolean.TRUE;
                message = "Please Insert Commision Or Fees";
                }
            }
            if (nullvalues == Boolean.FALSE) {
                message = (String) CFDBOEngin.save(cfdL);
            }
        } else {
            message = "Please Confirm First";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void saveAll() throws Throwable {
        if (true == showAdd) {
            message = (String) CFBOEngin.save(cfL);
        } else {
            message = "Please Confirm First";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }
}
