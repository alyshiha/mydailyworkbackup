package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.RejectedVisaBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.RejectedVisaDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;

@ManagedBean(name = "rejectedVisaView")
@ViewScoped
public class RejectedVisaClient extends BaseBean implements Serializable {

    private RejectedVisaDTOInter selectedRecord, newRecord;
    private List<RejectedVisaDTOInter> recordsList;
    private int listcount;
    private String reportid;
    private RejectedVisaBOInter engin;

    public RejectedVisaDTOInter getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(RejectedVisaDTOInter selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public RejectedVisaDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(RejectedVisaDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public List<RejectedVisaDTOInter> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<RejectedVisaDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public String getReportid() {
        return reportid;
    }

    public void setReportid(String reportid) {
        this.reportid = reportid;
    }

    public RejectedVisaBOInter getEngin() {
        return engin;
    }

    public void setEngin(RejectedVisaBOInter engin) {
        this.engin = engin;
    }

    @PostConstruct
    public void init() {
        try {
            reportid = "";
            engin = BOFactory.createRejectedVisaBO();
            recordsList = engin.getReport();
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(AccountNameClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void searchrecords() {
        try {
            if (reportid == null) {
                reportid = "";
            }
            recordsList = engin.getRecord(reportid);
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(AccountNameClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addRecord() {
        newRecord = DTOFactory.createRejectedVisaDTO();
    }

    public void saveRecord() throws SQLException, Throwable {
        String message = "";
        if (!engin.findDuplicateRecord(newRecord.getReportid())) {
            message = engin.insertReport(newRecord);
            searchrecords();
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("carDialog2.hide();");

        } else {
            message = "can`t Duplicate Report ID";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public void updateRecord() throws SQLException, Throwable {
        String message = "";
        if (!engin.findDuplicatesaveRecord(selectedRecord.getReportid(), selectedRecord.getRejectid())) {

            message = engin.updateReport(selectedRecord);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("carDialog.hide();");

        } else {
            message = "can`t Duplicate Report ID";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public void deleteRecord() throws SQLException {
        String message = engin.deleteReport(selectedRecord);
        if (message.contains("Succesfully")) {
            recordsList.remove(selectedRecord);
            listcount = recordsList.size();
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public List<String> completeContains(String query) {
        List<String> filtered = new ArrayList<String>();
        for (int i = 0; i < recordsList.size(); i++) {
            RejectedVisaDTOInter rec = recordsList.get(i);
            if (rec.getReportid().toLowerCase().contains(query.toLowerCase())) {
                filtered.add(rec.getReportid());
            }
        }
        return filtered;
    }

    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printreportPDF() throws Exception, Throwable {
        String path = (String) engin.printPDFrep(super.GetClient(), super.GetUser().getUsername(), reportid);
        prepDownload(path);
    }
}
