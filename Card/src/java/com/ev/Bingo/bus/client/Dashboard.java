/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.DAshBoardBOInter;
import com.ev.Bingo.bus.bo.networkcodesBOInter;
import com.ev.Bingo.bus.bo.networksBOInter;
import com.ev.Bingo.bus.bo.transactionmanagementBOInter;
import com.ev.Bingo.bus.dto.DAshBoardDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.io.Serializable;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

public class Dashboard extends BaseBean {

    public static final String[] POSSIBLE_THEMES
            = {"afterdark", "afternoon", "afterwork", "aristo",
                "black-tie", "blitzer", "bluesky", "casablanca",
                "cruze", "cupertino", "dark-hive", "dot-luv",
                "eggplant", "excite-bike", "flick", "glass-x",
                "home", "hot-sneaks", "humanity", "le-frog",
                "midnight", "mint-choc", "overcast", "pepper-grinder",
                "redmond", "rocket", "sam", "smoothness",
                "south-street", "start", "sunny", "swanky-purse",
                "trontastic", "twitter bootstrap", "ui-darkness",
                "ui-lightness", "vader"};

    public String[] getThemes() {
        return (POSSIBLE_THEMES);
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    private String theme;
    private CartesianChartModel barModel;
    private DAshBoardBOInter NBOEngin;
    private List<DAshBoardDTOInter> Networks;
    private String matchingtypeid;

    public DAshBoardBOInter getNBOEngin() {
        return NBOEngin;
    }

    public void setNBOEngin(DAshBoardBOInter NBOEngin) {
        this.NBOEngin = NBOEngin;
    }

    public List<DAshBoardDTOInter> getNetworks() {
        return Networks;
    }

    public void setNetworks(List<DAshBoardDTOInter> Networks) {
        this.Networks = Networks;
    }

    public String getMatchingtypeid() {
        return matchingtypeid;
    }

    public void setMatchingtypeid(String matchingtypeid) {
        this.matchingtypeid = matchingtypeid;
    }

    public CartesianChartModel getBarModel() {
        return barModel;
    }

    public void setBarModel(CartesianChartModel barModel) {
        this.barModel = barModel;
    }

    private void createCategoryModel(List<DAshBoardDTOInter> Networks) throws Throwable {
        barModel = new CartesianChartModel();
        ChartSeries time1 = new ChartSeries();
        ChartSeries time2 = new ChartSeries();
        time1.setLabel("Matched");
        time2.setLabel("Pending");
        for (DAshBoardDTOInter Network : Networks) {
            time1.set(Network.getNetwork(), Network.getMatched());
            time2.set(Network.getNetwork(), Network.getDisputes());
        }
        barModel.addSeries(time1);
        barModel.addSeries(time2);
    }

    public void matchingTypeChange(ValueChangeEvent e) throws Throwable {
        matchingtypeid = e.getNewValue().toString();
        runchart(matchingtypeid);
    }

    private void runchart(String Match) throws Throwable {
        Networks = (List<DAshBoardDTOInter>) NBOEngin.findfileall(Match);
        createCategoryModel(Networks);
    }

    public Dashboard() throws Throwable {
        FacesContext context2 = FacesContext.getCurrentInstance();
        HttpSession session2 = (HttpSession) context2.getExternalContext().getSession(true);
        HttpServletResponse response2 = (HttpServletResponse) context2.getExternalContext().getResponse();
        response2.addHeader("X-Frame-Options", "SAMEORIGIN");
        NBOEngin = BOFactory.createDAshBoardBO();
        matchingtypeid = "1";
        runchart("1");
        theme = "flick";
    }
}
