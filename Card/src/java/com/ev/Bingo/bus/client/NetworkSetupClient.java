/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.networkcodesBOInter;
import com.ev.Bingo.bus.bo.networksBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.networkcodesDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Administrator
 */
public class NetworkSetupClient extends BaseBean {

    private Boolean showAdd, showAdd1, showConfirm, showConfirm1;
    private networksBOInter NBOEngin;
    private networkcodesBOInter NCBOEngin;
    private List<networksDTOInter> nL;
    private List<networkcodesDTOInter> nCL;
    private networksDTOInter selectedrow, newrecord;
    private networkcodesDTOInter selecteddetailrow, newrecord1;
    private String message, message1;

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public networksBOInter getNBOEngin() {
        return NBOEngin;
    }

    public void setNBOEngin(networksBOInter NBOEngin) {
        this.NBOEngin = NBOEngin;
    }

    public networkcodesBOInter getNCBOEngin() {
        return NCBOEngin;
    }

    public void setNCBOEngin(networkcodesBOInter NCBOEngin) {
        this.NCBOEngin = NCBOEngin;
    }

    public List<networksDTOInter> getnL() {
        return nL;
    }

    public void setnL(List<networksDTOInter> nL) {
        this.nL = nL;
    }

    public List<networkcodesDTOInter> getnCL() {
        return nCL;
    }

    public void setnCL(List<networkcodesDTOInter> nCL) {
        this.nCL = nCL;
    }

    public networksDTOInter getSelectedrow() {
        return selectedrow;
    }

    public void setSelectedrow(networksDTOInter selectedrow) {
        this.selectedrow = selectedrow;
    }

    public networksDTOInter getNewrecord() {
        return newrecord;
    }

    public void setNewrecord(networksDTOInter newrecord) {
        this.newrecord = newrecord;
    }

    public networkcodesDTOInter getSelecteddetailrow() {
        return selecteddetailrow;
    }

    public void setSelecteddetailrow(networkcodesDTOInter selecteddetailrow) {
        this.selecteddetailrow = selecteddetailrow;
    }

    public networkcodesDTOInter getNewrecord1() {
        return newrecord1;
    }

    public void setNewrecord1(networkcodesDTOInter newrecord1) {
        this.newrecord1 = newrecord1;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public NetworkSetupClient() throws Throwable {
        super();GetAccess();
        NBOEngin = BOFactory.createnetworksBO();
        NCBOEngin = BOFactory.createnetworkcodesBO();
        nL = (List<networksDTOInter>) NBOEngin.GetAllRecords();
        showAdd = true;
        showConfirm = false;
        showAdd1 = true;
        showConfirm1 = false;
        nCL = new ArrayList<networkcodesDTOInter>();
    }

    public void onRowSelect(SelectEvent e) throws Throwable {
        selectedrow = DTOFactory.createnetworksDTO();
        selectedrow = (networksDTOInter) e.getObject();
        nCL = (List<networkcodesDTOInter>) NCBOEngin.GetListOfRecords(selectedrow);
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void fillDelete(ActionEvent e) {
        selectedrow = (networksDTOInter) e.getComponent().getAttributes().get("removedRow");
    }

    public void deleteRecord() throws Throwable {
        String deleteddetailedrecordcount = "", deletedrecordcount = "";
        if (selectedrow.getid() != 0) {
            deleteddetailedrecordcount = (String) NCBOEngin.deleterecord(selectedrow);
            if (deleteddetailedrecordcount.contains("Network Has Been Deleted")) {
                deletedrecordcount = (String) NBOEngin.deleterecord(selectedrow);
                if (!deletedrecordcount.contains("ORA-02292")) {
                    nL.remove(selectedrow);
                    nCL.clear();
                    message = deletedrecordcount + " " + deleteddetailedrecordcount;
                } else {
                    nCL.clear();
                    message = "Can't Delete Network Transaction Record Found";

                }
                showAdd = true;
                showConfirm = false;
            }
        } else {
            nL.remove(selectedrow);
            nCL.clear();
            showAdd = true;
            showConfirm = false;
            message = "Network Has Been Removed";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void fillDelete1(ActionEvent e) {
        selecteddetailrow = (networkcodesDTOInter) e.getComponent().getAttributes().get("removedRow1");
    }

    public void deleteRecord1() throws Throwable {
        if (selecteddetailrow.getnetworkcode() == null || selecteddetailrow.getnetworkcode().equals("")) {
            message1 = "Detailed Network Has Been Removed";
            nCL.remove(selecteddetailrow);
        } else {
            message1 = (String) NCBOEngin.deleterecorddetail(selecteddetailrow);
            if (message1.contains("Detailed Network Has Been Deleted")) {
                nCL.remove(selecteddetailrow);
                showAdd1 = true;
                showConfirm1 = false;
            }
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }

    public void addnetwork() {
        newrecord = DTOFactory.createnetworksDTO();
        nL.add(0, newrecord);
        selectedrow = newrecord;
        nCL.clear();
        showAdd = false;
        showConfirm = true;
    }

    public void confirmAdd() throws Throwable {
        if (!"".equals(newrecord.getname())) {
            message = (String) NBOEngin.insertrecord(newrecord);
            if (message.contains("Has Been")) {
                showAdd = true;
                showConfirm = false;
                nL = (List<networksDTOInter>) NBOEngin.GetAllRecords();
                nCL.clear();
                selectedrow = null;
            }
        } else {
            message = "Name Is A required Field";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void addcol2() {
        newrecord1 = DTOFactory.createnetworkcodesDTO();
        nCL.add(0, newrecord1);
        newrecord1.setnetworkid(selectedrow.getid());
        showAdd1 = false;
        showConfirm1 = true;
    }

    public void confirmAdd1() throws Throwable {
        if (!"".equals(newrecord1.getnetworkcode())) {
            message1 = (String) NCBOEngin.insertrecord(newrecord1);
            if (message1.contains("Network Code Has Been Inserted")) {
                showAdd1 = true;
                showConfirm1 = false;
            }
        } else {
            message1 = "Network Code Is A required Field";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }

    public void saveAll() throws Throwable {
        message = (String) NBOEngin.save(nL);
        message = "Records are Updated";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void saveAll1() throws Throwable {
        message1 = (String) NCBOEngin.save(nCL);
        message1 = "Records are Updated";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }
}
