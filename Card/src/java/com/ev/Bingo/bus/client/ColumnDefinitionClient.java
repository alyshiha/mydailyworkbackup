/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.filecolumndefinitionBOInter;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Administrator
 */
public class ColumnDefinitionClient extends BaseBean {

    private filecolumndefinitionBOInter colObject;
    private List<filecolumndefinitionDTOInter> colDefList;
    private filecolumndefinitionDTOInter colAttr;
    private String message;
    private Integer listSize;

    public filecolumndefinitionDTOInter getColAttr() {
        return colAttr;
    }

    public void setColAttr(filecolumndefinitionDTOInter colAttr) {
        this.colAttr = colAttr;
    }

    public List<filecolumndefinitionDTOInter> getColDefList() {
        return colDefList;
    }

    public void setColDefList(List<filecolumndefinitionDTOInter> colDefList) {
        this.colDefList = colDefList;
    }

    public filecolumndefinitionBOInter getColObject() {
        return colObject;
    }

    public void setColObject(filecolumndefinitionBOInter colObject) {
        this.colObject = colObject;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ColumnDefinitionClient() throws Throwable {
        super();
        GetAccess();
        colObject = BOFactory.createfilecolumndefinitionBo();
        colDefList = (List<filecolumndefinitionDTOInter>) colObject.GetAllRecords();
        listSize = colDefList.size();
    }

    public void saveAll() throws Throwable {
        message = (String) colObject.save(colDefList);
        message = "Records are Updated";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));

    }
}
