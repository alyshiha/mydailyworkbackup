/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.blacklistedcardBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.blacklistedcardDTOInter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Administrator
 */
public class BlackListedCardsClient extends BaseBean {

    private blacklistedcardBOInter blObject;
    private List<blacklistedcardDTOInter> blList;
    private blacklistedcardDTOInter blUpdateAttr, blDeleteAttr;
    private blacklistedcardDTOInter newBL;
    private Boolean showAdd, showConfirm;
    private String message;
    private String oldCard;
    private Integer listSize;

    public blacklistedcardDTOInter getBlDeleteAttr() {
        return blDeleteAttr;
    }

    public void setBlDeleteAttr(blacklistedcardDTOInter blDeleteAttr) {
        this.blDeleteAttr = blDeleteAttr;
    }

    public List<blacklistedcardDTOInter> getBlList() {
        return blList;
    }

    public void setBlList(List<blacklistedcardDTOInter> blList) {
        this.blList = blList;
    }

    public blacklistedcardBOInter getBlObject() {
        return blObject;
    }

    public void setBlObject(blacklistedcardBOInter blObject) {
        this.blObject = blObject;
    }

    public blacklistedcardDTOInter getBlUpdateAttr() {
        return blUpdateAttr;
    }

    public void setBlUpdateAttr(blacklistedcardDTOInter blUpdateAttr) {
        this.blUpdateAttr = blUpdateAttr;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public blacklistedcardDTOInter getNewBL() {
        return newBL;
    }

    public void setNewBL(blacklistedcardDTOInter newBL) {
        this.newBL = newBL;
    }

    public String getOldCard() {
        return oldCard;
    }

    public void setOldCard(String oldCard) {
        this.oldCard = oldCard;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public BlackListedCardsClient() throws Throwable {
        super();
        GetAccess();
        blObject = BOFactory.createblacklistedcardBO();
        blList = (List<blacklistedcardDTOInter>) blObject.GetAllRecords();
        showAdd = true;
        showConfirm = false;
        listSize = blList.size();
    }

    public void fillDelete(ActionEvent e) {
        blDeleteAttr = (blacklistedcardDTOInter) e.getComponent().getAttributes().get("removedRow");
    }

    public void deleteRecord() throws Throwable {
        String[] inputs = {blDeleteAttr.getcardno(), blDeleteAttr.getcomments(), blDeleteAttr.getcustomerno()};
        String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
        Boolean found = Boolean.FALSE;
        for (String input : inputs) {
            if (input != null) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
        }
        if (!found) {
            if (blDeleteAttr.getcardno() == null || blDeleteAttr.getcardno().equals("")) {
                blList.remove(blDeleteAttr);
                showAdd = true;
                showConfirm = false;
                listSize = listSize - 1;
                message = "Record Has Been Removed";
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(message));

            } else {
                message = (String) blObject.deleterecord(blDeleteAttr);

                if (message.contains("Has Been deleted")) {
                    blList.remove(blDeleteAttr);
                    showAdd = true;
                    showConfirm = false;
                    listSize = listSize - 1;
                }
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(message));
            }

        } else {
            message = "Please Enter Valid Data";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }

    public void addRecord() {
        if (blList == null) {
            blList = new ArrayList<blacklistedcardDTOInter>();
        }
        newBL = DTOFactory.createblacklistedcardDTO();
        blList.add(0, newBL);
        showAdd = false;
        showConfirm = true;
    }

    public void confirmRecord() {
        String[] inputs = {newBL.getcardno(), newBL.getcomments(), newBL.getcustomerno()};
        String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
        Boolean found = Boolean.FALSE;
        for (String input : inputs) {
            if (input != null) {
                boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                if (b == false) {
                    found = Boolean.TRUE;
                }
                if (!"".equals(input)) {
                    for (String validate1 : validate) {
                        if (input.toUpperCase().contains(validate1.toUpperCase())) {
                            found = Boolean.TRUE;
                        }
                    }
                }
            }
        }
        if (!found) {
            try {
                if ("Not Found".equals(TestColName(newBL.getcardno()))) {
                    message = (String) blObject.insertrecord(newBL);
                } else {
                    message = "Record  already exists";
                }
            } catch (Throwable ex) {
                message = "Record  already exists";
            }
            if (message.contains("Has Been Inserted")) {
                showAdd = true;
                showConfirm = false;
                listSize = listSize + 1;
            } else {
                message = message + " is a required field";
            }
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } else {
            message = "Please Enter Valid Data";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }

    public Boolean checkDulicate() {
        Boolean duplicates = Boolean.FALSE;
        int counter = 0;
        for (int j = 0; j < blList.size(); j++) {
            counter = 0;
            for (int k = 0; k < blList.size(); k++) {
                if (blList.get(k).getcardno().equals(blList.get(j).getcardno())) {
                    if (counter == 0) {
                        counter++;
                    } else {
                        duplicates = Boolean.TRUE;
                        return duplicates;
                    }
                }
            }
        }
        return duplicates;
    }

    public String TestColName(String ColName) {
        String MSG = "";
        int j = 0;
        for (int i = 0; i < blList.size(); i++) {
            if (blList.get(i).getcardno().equals(ColName)) {
                if (j > 1) {
                    return "Found";
                }
                j++;
            }
        }
        return "Not Found";
    }

    public void saveAll() throws Throwable {
        Boolean found = Boolean.FALSE;
        for (blacklistedcardDTOInter blList1 : blList) {

            String[] inputs = {blList1.getcardno(), blList1.getcomments(), blList1.getcustomerno()};
            String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};

            for (String input : inputs) {
                if (input != null) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
            }
        }
        if (!found) {

            if (showAdd == true) {
                if (!checkDulicate()) {
                    message = (String) blObject.save(blList);
                    message = "Records are Updated";
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage(message));
                } else {
                    message = "Duplicate Records";
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage(message));
                }
            } else {
                message = "PLZ Confirm First";
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(message));
            }
        } else {
            message = "Please Enter Valid Data";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }
}
