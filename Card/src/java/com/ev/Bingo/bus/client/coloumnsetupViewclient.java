
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.columnssetupBOInter;
import com.ev.Bingo.bus.bo.columnssetupdetailsBOInter;
import com.ev.Bingo.bus.bo.filecolumndefinitionBOInter;
import com.ev.Bingo.bus.bo.networkdefaultcolumnsBOInter;
import com.ev.Bingo.bus.bo.networksBOInter;
import com.ev.Bingo.bus.dto.*;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Administrator
 */
public class coloumnsetupViewclient extends BaseBean {

    private networksBOInter NBOEngin;
    private networkdefaultcolumnsBOInter CSDBOEngin;
    private filecolumndefinitionBOInter FCDBOEngin;
    private Boolean showAdd, showAdd1, showConfirm, showConfirm1;
    private List<networksDTOInter> cSL;
    private networksDTOInter selectedrow, selectedrowdelete, newrecord, selectedrowinsert;
    private List<networkdefaultcolumnsDTOInter> cSDL;
    private networkdefaultcolumnsDTOInter selectedrowdetaildelete, newrecord1, selectedrowdetailinsert;
    private String message, message1;
    public List<filecolumndefinitionDTOInter> fcdl;
    private List<String> cols;

    public networksBOInter getNBOEngin() {
        return NBOEngin;
    }

    public void setNBOEngin(networksBOInter NBOEngin) {
        this.NBOEngin = NBOEngin;
    }

    public networkdefaultcolumnsBOInter getCSDBOEngin() {
        return CSDBOEngin;
    }

    public void setCSDBOEngin(networkdefaultcolumnsBOInter CSDBOEngin) {
        this.CSDBOEngin = CSDBOEngin;
    }

    public List<networksDTOInter> getcSL() {
        return cSL;
    }

    public void setcSL(List<networksDTOInter> cSL) {
        this.cSL = cSL;
    }

    public networksDTOInter getSelectedrow() {
        return selectedrow;
    }

    public void setSelectedrow(networksDTOInter selectedrow) {
        this.selectedrow = selectedrow;
    }

    public networksDTOInter getSelectedrowdelete() {
        return selectedrowdelete;
    }

    public void setSelectedrowdelete(networksDTOInter selectedrowdelete) {
        this.selectedrowdelete = selectedrowdelete;
    }

    public networksDTOInter getNewrecord() {
        return newrecord;
    }

    public void setNewrecord(networksDTOInter newrecord) {
        this.newrecord = newrecord;
    }

    public networksDTOInter getSelectedrowinsert() {
        return selectedrowinsert;
    }

    public void setSelectedrowinsert(networksDTOInter selectedrowinsert) {
        this.selectedrowinsert = selectedrowinsert;
    }

    public List<networkdefaultcolumnsDTOInter> getcSDL() {
        return cSDL;
    }

    public void setcSDL(List<networkdefaultcolumnsDTOInter> cSDL) {
        this.cSDL = cSDL;
    }

    public networkdefaultcolumnsDTOInter getSelectedrowdetaildelete() {
        return selectedrowdetaildelete;
    }

    public void setSelectedrowdetaildelete(networkdefaultcolumnsDTOInter selectedrowdetaildelete) {
        this.selectedrowdetaildelete = selectedrowdetaildelete;
    }

    public networkdefaultcolumnsDTOInter getNewrecord1() {
        return newrecord1;
    }

    public void setNewrecord1(networkdefaultcolumnsDTOInter newrecord1) {
        this.newrecord1 = newrecord1;
    }

    public networkdefaultcolumnsDTOInter getSelectedrowdetailinsert() {
        return selectedrowdetailinsert;
    }

    public void setSelectedrowdetailinsert(networkdefaultcolumnsDTOInter selectedrowdetailinsert) {
        this.selectedrowdetailinsert = selectedrowdetailinsert;
    }

    public filecolumndefinitionBOInter getFCDBOEngin() {
        return FCDBOEngin;
    }

    public void setFCDBOEngin(filecolumndefinitionBOInter FCDBOEngin) {
        this.FCDBOEngin = FCDBOEngin;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public List<filecolumndefinitionDTOInter> getFcdl() {
        return fcdl;
    }

    public void setFcdl(List<filecolumndefinitionDTOInter> fcdl) {
        this.fcdl = fcdl;
    }

    public List<String> getCols() {
        return cols;
    }

    public void setCols(List<String> cols) {
        this.cols = cols;
    }

    public coloumnsetupViewclient() throws Throwable {
        super();
        GetAccess();
        cols = new ArrayList<String>();

        FCDBOEngin = BOFactory.createfilecolumndefinitionBo();
        NBOEngin = BOFactory.createnetworksBO();
        CSDBOEngin = BOFactory.createnetworkdefaultcolumnsBO();
        fcdl = (List<filecolumndefinitionDTOInter>) FCDBOEngin.GetAllRecords();
        networksDTOInter defaultrow = DTOFactory.createnetworksDTO();
        defaultrow.setid(999);
        defaultrow.setname("Standard");
        cSL = (List<networksDTOInter>) NBOEngin.GetAllRecords();
        cSL.add(0, defaultrow);
        cSDL = new ArrayList<networkdefaultcolumnsDTOInter>();
        showAdd = true;
        showConfirm = false;
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void onRowSelect(SelectEvent e) throws Throwable {
        selectedrow = DTOFactory.createnetworksDTO();
        selectedrow = (networksDTOInter) e.getObject();
        cSDL = (List<networkdefaultcolumnsDTOInter>) CSDBOEngin.GetListOfRecords(selectedrow);
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void fillDelete1(ActionEvent e) {
        selectedrowdetaildelete = (networkdefaultcolumnsDTOInter) e.getComponent().getAttributes().get("removedRow1");
    }

    public void deleteRecord1() throws Throwable {
        message1 = (String) CSDBOEngin.deleterecord(selectedrowdetaildelete);
        if (message1.contains(" Column Has Been Deleted")) {
            cols = (List<String>) CSDBOEngin.GetListOfcols(selectedrow);
            CSDBOEngin.saveorder(cols, selectedrow.getid());
            cSDL = (List<networkdefaultcolumnsDTOInter>) CSDBOEngin.GetListOfRecords(selectedrow);
            showAdd1 = true;
            showConfirm1 = false;
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }

    public void addcol2() {
        newrecord1 = DTOFactory.createnetworkdefaultcolumnsDTO();
        cSDL.add(0, newrecord1);
        newrecord1.setnetworkid(selectedrow.getid());
        newrecord1.setOrderby(cSDL.size());
        selectedrowdetailinsert = newrecord1;
        showAdd1 = false;
        showConfirm1 = true;
    }

    public void confirmAdd1() throws Throwable {
        message1 = "";
        if (newrecord1.getnetworkid() != 0) {
            for (networkdefaultcolumnsDTOInter record : cSDL) {
                if (record.getcolumnid() == newrecord1.getcolumnid() && record.getnetworkid() == newrecord1.getnetworkid() && record.getrowid() != null) {
                    message1 = "can not duplicate Column at the same network";
                }
            }
            Boolean CheckFlag = Boolean.FALSE;
            int count = 0;
            for (networkdefaultcolumnsDTOInter record : cSDL) {
                for (networkdefaultcolumnsDTOInter subrecord : cSDL) {
                    if (record.getOrderby() == subrecord.getOrderby()) {
                        count = count + 1;
                        if (count == 2) {
                            message1 = "can not duplicate Column Order at the same network";
                            CheckFlag = Boolean.TRUE;
                        }
                    }
                }
                count = 0;
            }
            if (!message1.contains("can not duplicate Column at the same network") && !message1.contains("can not duplicate Column Order at the same network")) {
                newrecord1.setrowid("newrec");
                message1 = (String) CSDBOEngin.insertrecord(newrecord1);
                if (message1.contains("Column Has Been")) {
                    cSDL = (List<networkdefaultcolumnsDTOInter>) CSDBOEngin.GetListOfRecords(selectedrow);
                    showAdd1 = true;
                    showConfirm1 = false;
                }
            }
        } else {
            message1 = "Please Select Network Name";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }

    public void findsort() throws Throwable {
        cols = (List<String>) CSDBOEngin.GetListOfcols(selectedrow);
    }

    public void updatesort() throws Throwable {
        message1 = (String) CSDBOEngin.saveorder(cols, selectedrow.getid());
        cSDL = (List<networkdefaultcolumnsDTOInter>) CSDBOEngin.GetListOfRecords(selectedrow);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }

    public void saveAll1() throws Throwable {
        Boolean CheckFlag = Boolean.FALSE;
        int count = 0;
        for (networkdefaultcolumnsDTOInter record : cSDL) {
            for (networkdefaultcolumnsDTOInter subrecord : cSDL) {
                if (record.getOrderby() == subrecord.getOrderby()) {
                    count = count + 1;
                    if (count == 2) {
                        message1 = "can not duplicate Column Order at the same network";
                        CheckFlag = Boolean.TRUE;
                    }
                }
            }
            count = 0;
        }
        if (!CheckFlag) {
            message1 = (String) CSDBOEngin.save(cSDL);
            if (!"can not duplicate Column at the same network".contains(message1)) {
                message1 = "Records are Updated";
            }
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }
}
