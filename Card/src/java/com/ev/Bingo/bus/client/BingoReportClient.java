/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.base.client.profitreport;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.DiffAmountBOInter;
import com.ev.Bingo.bus.bo.ProfitReportBOInter;
import com.ev.Bingo.bus.bo.VisaBOInter;
import com.ev.Bingo.bus.bo.commissionfeesBOInter;
import com.ev.Bingo.bus.bo.masterBOInter;
import com.ev.Bingo.bus.bo.networksBOInter;
import com.ev.Bingo.bus.bo.settlementsBOInter;
import com.ev.Bingo.bus.bo.transactionstatisticsrepBOInter;
import com.ev.Bingo.bus.dto.ATMMACHINECASHDTOINTER;
import com.ev.Bingo.bus.dto.AtmMachineDTOInter;
import com.ev.Bingo.bus.dto.CommFeeReportDTOInter;
import com.ev.Bingo.bus.dto.DiffAmountDTOInter;
import com.ev.Bingo.bus.dto.OffusDTOInter;
import com.ev.Bingo.bus.dto.OffusTotalsDTOInter;
import com.ev.Bingo.bus.dto.OnusDTOInter;
import com.ev.Bingo.bus.dto.OnusTotalsDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusInterDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusInterTotalsDTOInter;
import com.ev.Bingo.bus.dto.VisaOffusTotalsDTOInter;
import com.ev.Bingo.bus.dto.atmfileDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import com.ev.Bingo.bus.dto.settlementsDTOInter;
import com.ev.Bingo.bus.dto.timeFromSwitchDTOInter;
import com.ev.Bingo.bus.dto.transactionstatisticsrepDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author AlyShiha
 */
public class BingoReportClient extends BaseBean {

    private Date fromDate, toDate, settfromDate, setttoDate;
    private List<DiffAmountDTOInter> printDiffAmountresult;
    private List<CommFeeReportDTOInter> printcommfeeresult;
    private List<settlementsDTOInter> settmasterresult;
    private List<OffusDTOInter> AllOffusRecords;
    private List<VisaOffusInterDTOInter> AllVisaOffusInternationalRecords;
    private List<VisaOffusDTOInter> AllVisaOffusRecords;
    private List<OnusDTOInter> AllOnusRecords;
    private List<OffusTotalsDTOInter> AllOffusRecordstotals;
    private List<VisaOffusInterTotalsDTOInter> AllVisaOffusInternationalRecordstotals;
    private List<VisaOffusTotalsDTOInter> AllVisaOffusRecordstotals;
    private List<OnusTotalsDTOInter> AllOnusRecordstotals;
    private List<settlementsDTOInter> settdetailresult;
    private List<timeFromSwitchDTOInter> setttimeresult;
    private List<transactionstatisticsrepDTOInter> printtransstatresult;
    private int network;
    private String filename, pin, type, atmid;
    private DiffAmountBOInter DiffAmountBOEngin;
    private commissionfeesBOInter CommFeeBOEngin;
    private settlementsBOInter SettlementsBOEngin;
    private ProfitReportBOInter profitEngin;
    private masterBOInter MasterBOEngin;
    private VisaBOInter VisaBOEngin;
    private transactionstatisticsrepBOInter transstatBOEngin;
    private List<atmfileDTOInter> filelist;
    private List<AtmMachineDTOInter> atmmachinelist;
    private Boolean showpin, onus, offus;
    private usersDTOInter UserDTO;

    public List<CommFeeReportDTOInter> getPrintcommfeeresult() {
        return printcommfeeresult;
    }

    public Boolean getShowpin() {
        return showpin;
    }

    public void setShowpin(Boolean showpin) {
        this.showpin = showpin;
    }

    public List<settlementsDTOInter> getSettmasterresult() {
        return settmasterresult;
    }

    public void setSettmasterresult(List<settlementsDTOInter> settmasterresult) {
        this.settmasterresult = settmasterresult;
    }

    public List<settlementsDTOInter> getSettdetailresult() {
        return settdetailresult;
    }

    public void setSettdetailresult(List<settlementsDTOInter> settdetailresult) {
        this.settdetailresult = settdetailresult;
    }

    public commissionfeesBOInter getCommFeeBOEngin() {
        return CommFeeBOEngin;
    }

    public Boolean getOnus() {
        return onus;
    }

    public void setOnus(Boolean onus) {
        this.onus = onus;
    }

    public Boolean getOffus() {
        return offus;
    }

    public void setOffus(Boolean offus) {
        this.offus = offus;
    }

    public void setCommFeeBOEngin(commissionfeesBOInter CommFeeBOEngin) {
        this.CommFeeBOEngin = CommFeeBOEngin;
    }

    public settlementsBOInter getSettlementsBOEngin() {
        return SettlementsBOEngin;
    }

    public void setSettlementsBOEngin(settlementsBOInter SettlementsBOEngin) {
        this.SettlementsBOEngin = SettlementsBOEngin;
    }

    public void setPrintcommfeeresult(List<CommFeeReportDTOInter> printcommfeeresult) {
        this.printcommfeeresult = printcommfeeresult;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public List<OffusTotalsDTOInter> getAllOffusRecordstotals() {
        return AllOffusRecordstotals;
    }

    public void setAllOffusRecordstotals(List<OffusTotalsDTOInter> AllOffusRecordstotals) {
        this.AllOffusRecordstotals = AllOffusRecordstotals;
    }

    public List<VisaOffusInterTotalsDTOInter> getAllVisaOffusInternationalRecordstotals() {
        return AllVisaOffusInternationalRecordstotals;
    }

    public void setAllVisaOffusInternationalRecordstotals(List<VisaOffusInterTotalsDTOInter> AllVisaOffusInternationalRecordstotals) {
        this.AllVisaOffusInternationalRecordstotals = AllVisaOffusInternationalRecordstotals;
    }

    public List<VisaOffusTotalsDTOInter> getAllVisaOffusRecordstotals() {
        return AllVisaOffusRecordstotals;
    }

    public void setAllVisaOffusRecordstotals(List<VisaOffusTotalsDTOInter> AllVisaOffusRecordstotals) {
        this.AllVisaOffusRecordstotals = AllVisaOffusRecordstotals;
    }

    public List<OnusTotalsDTOInter> getAllOnusRecordstotals() {
        return AllOnusRecordstotals;
    }

    public void setAllOnusRecordstotals(List<OnusTotalsDTOInter> AllOnusRecordstotals) {
        this.AllOnusRecordstotals = AllOnusRecordstotals;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getFilename() {
        return filename;
    }

    public List<DiffAmountDTOInter> getPrintDiffAmountresult() {
        return printDiffAmountresult;
    }

    public void setPrintDiffAmountresult(List<DiffAmountDTOInter> printDiffAmountresult) {
        this.printDiffAmountresult = printDiffAmountresult;
    }

    public DiffAmountBOInter getDiffAmountBOEngin() {
        return DiffAmountBOEngin;
    }

    public void setDiffAmountBOEngin(DiffAmountBOInter DiffAmountBOEngin) {
        this.DiffAmountBOEngin = DiffAmountBOEngin;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getNetwork() {
        return network;
    }

    public void setNetwork(int network) {
        this.network = network;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public List<atmfileDTOInter> getFilelist() {
        return filelist;
    }

    public void setFilelist(List<atmfileDTOInter> filelist) {
        this.filelist = filelist;
    }
    private List<networksDTOInter> networklist;

    public usersDTOInter getUserDTO() {
        return UserDTO;
    }

    public void setUserDTO(usersDTOInter UserDTO) {
        this.UserDTO = UserDTO;
    }

    public List<networksDTOInter> getNetworklist() {
        return networklist;
    }

    public void setNetworklist(List<networksDTOInter> networklist) {
        this.networklist = networklist;
    }
    private networksBOInter networkEngin;

    public List<transactionstatisticsrepDTOInter> getPrinttransstatresult() {
        return printtransstatresult;
    }

    public void setPrinttransstatresult(List<transactionstatisticsrepDTOInter> printtransstatresult) {
        this.printtransstatresult = printtransstatresult;
    }

    public transactionstatisticsrepBOInter getTransstatBOEngin() {
        return transstatBOEngin;
    }

    public void setTransstatBOEngin(transactionstatisticsrepBOInter transstatBOEngin) {
        this.transstatBOEngin = transstatBOEngin;
    }

    public masterBOInter getMasterBOEngin() {
        return MasterBOEngin;
    }

    public void setMasterBOEngin(masterBOInter MasterBOEngin) {
        this.MasterBOEngin = MasterBOEngin;
    }

    public VisaBOInter getVisaBOEngin() {
        return VisaBOEngin;
    }

    public void setVisaBOEngin(VisaBOInter VisaBOEngin) {
        this.VisaBOEngin = VisaBOEngin;
    }

    public networksBOInter getNetworkEngin() {
        return networkEngin;
    }

    public void setNetworkEngin(networksBOInter networkEngin) {
        this.networkEngin = networkEngin;
    }

    public List<timeFromSwitchDTOInter> getSetttimeresult() {
        return setttimeresult;
    }

    public void setSetttimeresult(List<timeFromSwitchDTOInter> setttimeresult) {
        this.setttimeresult = setttimeresult;
    }

    public Date getSettfromDate() {
        return settfromDate;
    }

    public void setSettfromDate(Date settfromDate) {
        this.settfromDate = settfromDate;
    }

    public Date getSetttoDate() {
        return setttoDate;
    }

    public void setSetttoDate(Date setttoDate) {
        this.setttoDate = setttoDate;
    }

    public List<OffusDTOInter> getAllOffusRecords() {
        return AllOffusRecords;
    }

    public void setAllOffusRecords(List<OffusDTOInter> AllOffusRecords) {
        this.AllOffusRecords = AllOffusRecords;
    }

    public List<VisaOffusInterDTOInter> getAllVisaOffusInternationalRecords() {
        return AllVisaOffusInternationalRecords;
    }

    public void setAllVisaOffusInternationalRecords(List<VisaOffusInterDTOInter> AllVisaOffusInternationalRecords) {
        this.AllVisaOffusInternationalRecords = AllVisaOffusInternationalRecords;
    }

    public List<VisaOffusDTOInter> getAllVisaOffusRecords() {
        return AllVisaOffusRecords;
    }

    public void setAllVisaOffusRecords(List<VisaOffusDTOInter> AllVisaOffusRecords) {
        this.AllVisaOffusRecords = AllVisaOffusRecords;
    }

    public List<OnusDTOInter> getAllOnusRecords() {
        return AllOnusRecords;
    }

    public void setAllOnusRecords(List<OnusDTOInter> AllOnusRecords) {
        this.AllOnusRecords = AllOnusRecords;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<AtmMachineDTOInter> getAtmmachinelist() {
        return atmmachinelist;
    }

    public void setAtmmachinelist(List<AtmMachineDTOInter> atmmachinelist) {
        this.atmmachinelist = atmmachinelist;
    }

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    public ProfitReportBOInter getProfitEngin() {
        return profitEngin;
    }

    public void setProfitEngin(ProfitReportBOInter profitEngin) {
        this.profitEngin = profitEngin;
    }

    public BingoReportClient() throws Throwable {
        super();
        GetAccess();
        DiffAmountBOEngin = BOFactory.createDiffAmountBO();
        CommFeeBOEngin = BOFactory.createcommissionfeesBO();
        SettlementsBOEngin = BOFactory.createsettlementsBO();
        MasterBOEngin = BOFactory.createmasterBO();
        VisaBOEngin = BOFactory.createVisaBO();
        transstatBOEngin = BOFactory.createtransactionstatisticsrepBO();
        profitEngin = BOFactory.createProfitReportBO();
        filelist = (List<atmfileDTOInter>) DiffAmountBOEngin.findfileall();
        printDiffAmountresult = new ArrayList<DiffAmountDTOInter>();
        printcommfeeresult = new ArrayList<CommFeeReportDTOInter>();
        settmasterresult = new ArrayList<settlementsDTOInter>();
        settdetailresult = new ArrayList<settlementsDTOInter>();
        setttimeresult = new ArrayList<timeFromSwitchDTOInter>();
        printtransstatresult = new ArrayList<transactionstatisticsrepDTOInter>();
        AllOffusRecords = new ArrayList<OffusDTOInter>();
        AllVisaOffusInternationalRecords = new ArrayList<VisaOffusInterDTOInter>();
        AllVisaOffusRecords = new ArrayList<VisaOffusDTOInter>();
        AllOnusRecords = new ArrayList<OnusDTOInter>();
        AllOffusRecordstotals = new ArrayList<OffusTotalsDTOInter>();
        AllVisaOffusInternationalRecordstotals = new ArrayList<VisaOffusInterTotalsDTOInter>();
        AllVisaOffusRecordstotals = new ArrayList<VisaOffusTotalsDTOInter>();
        AllOnusRecordstotals = new ArrayList<OnusTotalsDTOInter>();
        UserDTO = (usersDTOInter) super.GetUser();
        networkEngin = BOFactory.createnetworksBO();
        type = "All";
        setType("All");
        atmid = "0";
        filename = "";
        setFromDate(super.getYesterdaytrans("20:00:00"));
        setToDate(super.getTodaytrans("20:00:00"));
        setSettfromDate(super.getYesterdaytrans("20:00:00"));
        setSetttoDate(super.getTodaytrans("20:00:00"));
        networklist = (List<networksDTOInter>) networkEngin.GetAllUserRecords("" + super.GetUser().getUserid());
        showpin = Boolean.FALSE;
        onus = Boolean.FALSE;
        offus = Boolean.FALSE;
        atmmachinelist = profitEngin.GetAtmMachineList();
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printDiffAmount() throws Throwable {
        if (!printDiffAmountresult.isEmpty()) {
            String fileid = "";
            fileid = getfileid(filename);

            if (!"".equals(fileid)) {
                RequestContext.getCurrentInstance().execute("statusDialog.show()");
                String path = (String) DiffAmountBOEngin.GetReport(super.GetUser().getUsername(), super.GetClient(), UserDTO, printDiffAmountresult, fromDate, toDate);
                prepDownload(path);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("PDF Report Path " + path));
                RequestContext.getCurrentInstance().execute("statusDialog.hide()");
            } else {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("File Not Found"));
            }
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("No Data To Print"));
        }
    }

    public List<String> completeText(String query) {
        List<String> results = new ArrayList<String>();
        for (atmfileDTOInter filelist1 : filelist) {
            if (filelist1.getname().contains(query)) {
                results.add(filelist1.getname());
            }
            if (results.size() > 5) {
                break;
            }
        }
        return results;
    }

    public String getfileid(String name) {
        String ID = "0";
        if (name == null || "".equals(name)) {
            return "0";
        }
        for (atmfileDTOInter rec : filelist) {
            if (rec.getname().toLowerCase().equals(name.toLowerCase())) {
                return "" + rec.getid();
            }
        }
        return ID;
    }

    public void recalc() {
        try {
            DiffAmountBOEngin.recalc();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Has Been Calculate Succesfully"));
        } catch (Throwable ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void printDiffAmountexcel() throws Throwable {
        String fileid = "";
        if (!"".equals(filename)) {
            fileid = getfileid(filename);
            printDiffAmountresult = (List<DiffAmountDTOInter>) DiffAmountBOEngin.GetAllRecords(fromDate, toDate, network, fileid, UserDTO);
            ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results"))).setFirst(0);
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("File Not Found"));
        }
    }

    private String index;

    public void printtransstatexcel() throws Throwable {
        String fileid = "";
        if (!"".equals(filename)) {
            fileid = getfileid(filename);
            if ("0".equals(fileid)) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Please Check File Name"));
            } else {
                index = (String) transstatBOEngin.RunRep(fromDate, toDate, network, fileid);
                printtransstatresult = (List<transactionstatisticsrepDTOInter>) transstatBOEngin.GetListOfRecords(index);
                ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results"))).setFirst(0);
            }
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("File Is A Required Field"));
        }
    }

    public void printtransstat() throws Throwable {
        if (!printtransstatresult.isEmpty()) {
            String fileid = "";
            fileid = getfileid(filename);

            if (!"".equals(fileid)) {
                if ("0".equals(fileid)) {
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage("Please Check File Name"));
                } else {
                    RequestContext.getCurrentInstance().execute("statusDialog.show()");
                    String path = (String) transstatBOEngin.GetReport(super.GetUser().getUsername(), super.GetClient(), UserDTO, printDiffAmountresult, fromDate, toDate, index);
                    prepDownload(path);
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage("PDF Report Path " + path));
                    RequestContext.getCurrentInstance().execute("statusDialog.hide()");
                }
            } else {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("File Not Found"));
            }
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("No Data To Print"));
        }
    }

    public void printCommFeeexcel() throws Throwable {
        printcommfeeresult = (List<CommFeeReportDTOInter>) CommFeeBOEngin.GetAllRecords(fromDate, toDate, network, pin, UserDTO.getUserid(), showpin);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results"))).setFirst(0);
    }

    public void printMasterexcel() throws Throwable {
        settmasterresult = (List<settlementsDTOInter>) MasterBOEngin.GetAllRecordsIssuer(fromDate, toDate);
        settdetailresult = (List<settlementsDTOInter>) MasterBOEngin.GetAllRecordsAcquier(fromDate, toDate);
        setttimeresult = (List<timeFromSwitchDTOInter>) MasterBOEngin.GetAlltimefromswitch(fromDate, toDate);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results"))).setFirst(0);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results2"))).setFirst(0);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results3"))).setFirst(0);
    }

    public void printMaster() throws Throwable {
        RequestContext.getCurrentInstance().execute("statusDialog.show()");
        String path = (String) MasterBOEngin.GetReport(fromDate, toDate, super.GetUser().getUsername(), super.GetClient());
        prepDownload(path);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("PDF Report Path " + path));
        RequestContext.getCurrentInstance().execute("statusDialog.hide()");
    }

    public void printVisaexcel() throws Throwable {
        settmasterresult = (List<settlementsDTOInter>) VisaBOEngin.GetAllRecordsIssuer(fromDate, toDate);
        settdetailresult = (List<settlementsDTOInter>) VisaBOEngin.GetAllRecordsAcquier(fromDate, toDate);
        setttimeresult = (List<timeFromSwitchDTOInter>) VisaBOEngin.GetAlltimefromswitch(fromDate, toDate);
        setttimeresult = (List<timeFromSwitchDTOInter>) VisaBOEngin.GetAllcommision(fromDate, toDate);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results"))).setFirst(0);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results2"))).setFirst(0);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results3"))).setFirst(0);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results4"))).setFirst(0);
    }

    public void printVisa() throws Throwable {
        RequestContext.getCurrentInstance().execute("statusDialog.show()");
        String path = (String) VisaBOEngin.GetReport(fromDate, toDate, super.GetUser().getUsername(), super.GetClient());
        prepDownload(path);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("PDF Report Path " + path));
        RequestContext.getCurrentInstance().execute("statusDialog.hide()");
    }

    public void printSettlementsexcel() throws Throwable {
        settmasterresult = (List<settlementsDTOInter>) SettlementsBOEngin.GetAllRecordsIssuer(fromDate, toDate);
        settdetailresult = (List<settlementsDTOInter>) SettlementsBOEngin.GetAllRecordsAcquier(fromDate, toDate);
        setttimeresult = (List<timeFromSwitchDTOInter>) SettlementsBOEngin.GetAlltimefromswitch(fromDate, toDate);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results"))).setFirst(0);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results2"))).setFirst(0);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results3"))).setFirst(0);
    }

    public void printSettlements() throws Throwable {
        RequestContext.getCurrentInstance().execute("statusDialog.show()");
        String path = (String) SettlementsBOEngin.GetReport(fromDate, toDate, super.GetUser().getUsername(), super.GetClient());
        prepDownload(path);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("PDF Report Path " + path));
        RequestContext.getCurrentInstance().execute("statusDialog.hide()");
    }

    public void printCommFee() throws Throwable {
        RequestContext.getCurrentInstance().execute("statusDialog.show()");
        String path = (String) CommFeeBOEngin.GetReport(fromDate, toDate, network, pin, UserDTO.getUserid(), showpin, super.GetUser().getUsername(), super.GetClient());
        prepDownload(path);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("PDF Report Path " + path));
        RequestContext.getCurrentInstance().execute("statusDialog.hide()");
    }

    public void printProfitexcel() throws Throwable {

        Hashtable AllRecords = profitEngin.findfileall(type, fromDate, toDate, settfromDate, setttoDate, atmid);
        if (type.equals("ONUS")) {
            //    AllOnusRecords = (List<OnusDTOInter>) AllRecords.get("OnusRecords");
            AllOnusRecordstotals = (List<OnusTotalsDTOInter>) AllRecords.get("OnusRecordsTotals");
            AllOffusRecords.clear();
            AllVisaOffusRecords.clear();
            AllVisaOffusInternationalRecords.clear();
            AllOffusRecordstotals.clear();
            AllVisaOffusRecordstotals.clear();
            AllVisaOffusInternationalRecordstotals.clear();
            onus = Boolean.TRUE;
            offus = Boolean.FALSE;
        } else if (type.equals("OFFUS")) {
            //  AllOffusRecords = (List<OffusDTOInter>) AllRecords.get("OffusRecords");
            //AllVisaOffusInternationalRecords = (List<VisaOffusInterDTOInter>) AllRecords.get("VisaOffusInternationalRecords");
            //AllVisaOffusRecords = (List<VisaOffusDTOInter>) AllRecords.get("VisaOffusRecords");
            AllOffusRecordstotals = (List<OffusTotalsDTOInter>) AllRecords.get("OffusRecordsTotals");
            AllVisaOffusInternationalRecordstotals = (List<VisaOffusInterTotalsDTOInter>) AllRecords.get("VisaOffusInternationalRecordsTotals");
            AllVisaOffusRecordstotals = (List<VisaOffusTotalsDTOInter>) AllRecords.get("VisaOffusRecordsTotals");
            AllOnusRecords.clear();
            AllOnusRecordstotals.clear();
            onus = Boolean.FALSE;
            offus = Boolean.TRUE;
        } else {
            //AllOffusRecords = (List<OffusDTOInter>) AllRecords.get("OffusRecords");
            //AllVisaOffusInternationalRecords = (List<VisaOffusInterDTOInter>) AllRecords.get("VisaOffusInternationalRecords");
            //AllVisaOffusRecords = (List<VisaOffusDTOInter>) AllRecords.get("VisaOffusRecords");
            //AllOnusRecords = (List<OnusDTOInter>) AllRecords.get("OnusRecords");
            AllOnusRecordstotals = (List<OnusTotalsDTOInter>) AllRecords.get("OnusRecordsTotals");
            AllOffusRecordstotals = (List<OffusTotalsDTOInter>) AllRecords.get("OffusRecordsTotals");
            AllVisaOffusInternationalRecordstotals = (List<VisaOffusInterTotalsDTOInter>) AllRecords.get("VisaOffusInternationalRecordsTotals");
            AllVisaOffusRecordstotals = (List<VisaOffusTotalsDTOInter>) AllRecords.get("VisaOffusRecordsTotals");
            onus = Boolean.TRUE;
            offus = Boolean.TRUE;
        }

        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results5"))).setFirst(0);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results6"))).setFirst(0);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results7"))).setFirst(0);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:results8"))).setFirst(0);
    }

    public void printProfit() throws Throwable {

        RequestContext.getCurrentInstance().execute("statusDialog.show()");
        String path = (String) profitEngin.findReport(type, fromDate, toDate, settfromDate, setttoDate, atmid, super.GetUser().getUsername(), super.GetClient(),getATMName(atmid));
        prepDownload(path);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("PDF Report Path " + path));
        RequestContext.getCurrentInstance().execute("statusDialog.hide()");
    }

    public void excelProfit() throws Throwable {
        printProfitexcel();
        profitreport PR = new profitreport();
        RequestContext.getCurrentInstance().execute("statusDialog.show()");
        String path = (String) PR.exportexcel(AllOnusRecordstotals, AllOffusRecordstotals, AllVisaOffusRecordstotals, AllVisaOffusInternationalRecordstotals, AllOnusRecordstotals, AllOffusRecordstotals, AllVisaOffusRecordstotals, AllVisaOffusInternationalRecordstotals);
        prepDownload(path);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("PDF Report Path " + path));
        RequestContext.getCurrentInstance().execute("statusDialog.hide()");
    }

    public String getATMName(String ID) {
        for (AtmMachineDTOInter atmrecord : atmmachinelist) {
            if (atmrecord.getApplicationId().equals(ID)) {
                return atmrecord.getName();
            }
        }
        return "UnAssigned";
    }
}
