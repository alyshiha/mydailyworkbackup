/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.blacklistedcardBOInter;
import com.ev.Bingo.bus.bo.filecolumndefinitionBOInter;
import com.ev.Bingo.bus.bo.matchingtypeBoInter;
import com.ev.Bingo.bus.bo.networksBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.blacklistedcardDTOInter;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import com.ev.Bingo.bus.dto.matchingtypeDTOInter;
import com.ev.Bingo.bus.dto.matchingtypenetworksDTO;
import com.ev.Bingo.bus.dto.matchingtypenetworksDTOInter;
import com.ev.Bingo.bus.dto.matchingtypesettingDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author Administrator
 */
public class MatchingSettingsClient extends BaseBean {

    private matchingtypeBoInter MTBOEngin;
    private filecolumndefinitionBOInter FCDBOEngin;
    private List<matchingtypeDTOInter> matchingMTL;
    private List<matchingtypesettingDTOInter> matchingMTSL;
    private List<matchingtypenetworksDTOInter> matchingMTML;
    private List<networksDTOInter> nL;
    private matchingtypesettingDTOInter selecteditemMTS, newrecordmt;
    private matchingtypenetworksDTOInter selecteditemMTN, newrecord;
    private matchingtypeDTOInter selecteditemMT;
    private networksBOInter NBOEngin;
    private String message;
    private Boolean showAdd, showConfirm, showAdd1, showConfirm1;
    private List<filecolumndefinitionDTOInter> fCDL;

    public matchingtypeBoInter getMTBOEngin() {
        return MTBOEngin;
    }

    public void setMTBOEngin(matchingtypeBoInter MTBOEngin) {
        this.MTBOEngin = MTBOEngin;
    }

    public filecolumndefinitionBOInter getFCDBOEngin() {
        return FCDBOEngin;
    }

    public void setFCDBOEngin(filecolumndefinitionBOInter FCDBOEngin) {
        this.FCDBOEngin = FCDBOEngin;
    }

    public List<matchingtypeDTOInter> getMatchingMTL() {
        return matchingMTL;
    }

    public void setMatchingMTL(List<matchingtypeDTOInter> matchingMTL) {
        this.matchingMTL = matchingMTL;
    }

    public List<matchingtypesettingDTOInter> getMatchingMTSL() {
        return matchingMTSL;
    }

    public void setMatchingMTSL(List<matchingtypesettingDTOInter> matchingMTSL) {
        this.matchingMTSL = matchingMTSL;
    }

    public List<matchingtypenetworksDTOInter> getMatchingMTML() {
        return matchingMTML;
    }

    public void setMatchingMTML(List<matchingtypenetworksDTOInter> matchingMTML) {
        this.matchingMTML = matchingMTML;
    }

    public List<networksDTOInter> getnL() {
        return nL;
    }

    public void setnL(List<networksDTOInter> nL) {
        this.nL = nL;
    }

    public matchingtypesettingDTOInter getSelecteditemMTS() {
        return selecteditemMTS;
    }

    public void setSelecteditemMTS(matchingtypesettingDTOInter selecteditemMTS) {
        this.selecteditemMTS = selecteditemMTS;
    }

    public matchingtypesettingDTOInter getNewrecordmt() {
        return newrecordmt;
    }

    public void setNewrecordmt(matchingtypesettingDTOInter newrecordmt) {
        this.newrecordmt = newrecordmt;
    }

    public matchingtypenetworksDTOInter getSelecteditemMTN() {
        return selecteditemMTN;
    }

    public void setSelecteditemMTN(matchingtypenetworksDTOInter selecteditemMTN) {
        this.selecteditemMTN = selecteditemMTN;
    }

    public matchingtypenetworksDTOInter getNewrecord() {
        return newrecord;
    }

    public void setNewrecord(matchingtypenetworksDTOInter newrecord) {
        this.newrecord = newrecord;
    }

    public matchingtypeDTOInter getSelecteditemMT() {
        return selecteditemMT;
    }

    public void setSelecteditemMT(matchingtypeDTOInter selecteditemMT) {
        this.selecteditemMT = selecteditemMT;
    }

    public networksBOInter getNBOEngin() {
        return NBOEngin;
    }

    public void setNBOEngin(networksBOInter NBOEngin) {
        this.NBOEngin = NBOEngin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public List<filecolumndefinitionDTOInter> getfCDL() {
        return fCDL;
    }

    public void setfCDL(List<filecolumndefinitionDTOInter> fCDL) {
        this.fCDL = fCDL;
    }

    public MatchingSettingsClient() throws Throwable {
        GetAccess();
        MTBOEngin = BOFactory.creatematchingtypeBo();
        NBOEngin = BOFactory.createnetworksBO();
        FCDBOEngin = BOFactory.createfilecolumndefinitionBo();
        nL = (List<networksDTOInter>) NBOEngin.GetAllRecords();
        matchingMTL = (List<matchingtypeDTOInter>) MTBOEngin.findAllmatchingtype();
        fCDL = (List<filecolumndefinitionDTOInter>) FCDBOEngin.GetAllRecords();
        matchingMTML = new ArrayList<matchingtypenetworksDTOInter>();
        matchingMTSL = new ArrayList<matchingtypesettingDTOInter>();
        showAdd = true;
        showConfirm = false;
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void onRowSelect1(SelectEvent e) throws Throwable {
        selecteditemMT = (matchingtypeDTOInter) e.getObject();
        matchingMTML = (List<matchingtypenetworksDTOInter>) MTBOEngin.findRecordsListmatchingtypenetworks(selecteditemMT);
        matchingMTSL.clear();
    }

    public void onRowUnSelect1(UnselectEvent e) {
        matchingMTML.clear();
        selecteditemMT = null;
    }

    public void onRowSelect2(SelectEvent e) throws Throwable {
        selecteditemMTN = (matchingtypenetworksDTOInter) e.getObject();
        matchingMTSL = (List<matchingtypesettingDTOInter>) MTBOEngin.findRecordsListmatchingtypesetting(selecteditemMTN);
    }

    public void onRowUnSelect2(UnselectEvent e) {
        selecteditemMTN = null;
        matchingMTSL.clear();
    }

    public void fillAttr(ActionEvent e) {
        selecteditemMTN = (matchingtypenetworksDTO) e.getComponent().getAttributes().get("removedRow");
    }

    public void addNetwork() {
        newrecord = DTOFactory.creatematchingtypenetworksDTO();
        newrecord.setRowid("newrec");
        matchingMTML.add(0, newrecord);
        selecteditemMTN = newrecord;
        matchingMTSL.clear();
        newrecord.setmatchingtype(selecteditemMT.getid());
        showAdd = false;
        showConfirm = true;
    }

    public void confirmAddNetwork() throws Throwable {
        if (newrecord.getmatchingtype() != 0) {
            newrecord.setRowid("newrec");
            message = (String) MTBOEngin.insertrecordmatchingtypenetworks(newrecord);
            if (message.contains("Network Has Been Inserted")) {
                matchingMTML = (List<matchingtypenetworksDTOInter>) MTBOEngin.findRecordsListmatchingtypenetworks(selecteditemMT);
                showAdd = true;
                showConfirm = false;
            }
        } else {
            message = "Please Select Matching Type";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void fillAttrdel(ActionEvent e) {
        selecteditemMTS = (matchingtypesettingDTOInter) e.getComponent().getAttributes().get("removedMatching");
    }

    public void addMatchingType() {
        newrecordmt = DTOFactory.creatematchingtypesettingDTO();
        newrecordmt.setRowid("newrec");
        matchingMTSL.add(0, newrecordmt);
        selecteditemMTS = newrecordmt;
        newrecordmt.setnetworkid(selecteditemMTN.getnetworkid());
        newrecordmt.setid(selecteditemMT.getid());
        showAdd1 = false;
        showConfirm1 = true;
    }

    public void confirmAddMatchingType() throws Throwable {
        if (newrecordmt.getnetworkid() != 0) {
            newrecordmt.setRowid("newrec");
            message = (String) MTBOEngin.insertrecordmatchingtypesetting(newrecordmt);
            if (message.contains("Record Has Been Inserted")) {
                showAdd1 = true;
                showConfirm1 = false;
                matchingMTSL = (List<matchingtypesettingDTOInter>) MTBOEngin.findRecordsListmatchingtypesetting(selecteditemMTN);
            }
        } else {
            message = "Please Select Network Name";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void deleteMachineType(ActionEvent e) throws Throwable {
        if (!"newrec".equals(selecteditemMTN.getRowid())) {
            message = (String) MTBOEngin.deleterecordmatchingtypenetworks(selecteditemMTN);
            if (message.contains("Record Has Been Deleted")) {
                matchingMTML.remove(selecteditemMTN);
                matchingMTSL.clear();
                showAdd = true;
                showConfirm = false;
                matchingMTSL.clear();
            }
        } else {
            matchingMTML.remove(selecteditemMTN);
            matchingMTSL.clear();
            showAdd = true;
            showConfirm = false;
            matchingMTSL.clear();
            message = "Record Has Been Removed";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void deleteMatchingSetting(ActionEvent e) throws Throwable {
        if (!"newrec".equals(selecteditemMTS.getRowid())) {
            message = (String) MTBOEngin.deleterecordmatchingtypesetting(selecteditemMTS);
            if (message.contains("Record Has Been Deleted")) {
                matchingMTSL.remove(selecteditemMTS);
                showAdd1 = true;
                showConfirm1 = false;
            }
        } else {
            matchingMTSL.remove(selecteditemMTS);
            showAdd1 = true;
            showConfirm1 = false;
            message = "Record Has Been Removed";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public static Integer findDuplicates(List<matchingtypenetworksDTOInter> listContainingDuplicates) {

        final Set<Integer> setToReturn = new HashSet<Integer>();
        final Set<Integer> set1 = new HashSet<Integer>();

        for (matchingtypenetworksDTOInter yourInt : listContainingDuplicates) {
            if (!set1.add(yourInt.getnetworkid())) {
                setToReturn.add(yourInt.getnetworkid());
            }
        }
        return setToReturn.size();
    }

    public void saveAll() throws Throwable {
        Integer s = findDuplicates(matchingMTML);
        if (s > 0) {
            message = "Duplicate Network";
        } else {
            message = (String) MTBOEngin.savematchingtypenetworks(matchingMTML);
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void saveAllms() throws Throwable {
        message = (String) MTBOEngin.savematchingtypesetting(matchingMTSL);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void fillMatch(ActionEvent e) {
        selecteditemMT = (matchingtypeDTOInter) e.getComponent().getAttributes().get("updateMatchingType");
    }

    public void updateMatchingType(ActionEvent e) throws Throwable {
        message = (String) MTBOEngin.updaterecordmatchingtype(selecteditemMT);
        FacesContext context = FacesContext.getCurrentInstance();
        message = message + " Record Has Been Updated";
        context.addMessage(null, new FacesMessage(message));
    }

    public void RecreateIndex() throws Throwable {
        message = (String) MTBOEngin.RecreateIndex();
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

}
