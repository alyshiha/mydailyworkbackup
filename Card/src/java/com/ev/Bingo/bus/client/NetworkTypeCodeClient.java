
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.NetworkTypeCodeBOInter;
import com.ev.Bingo.bus.bo.filecolumndefinitionBOInter;
import com.ev.Bingo.bus.bo.networksBOInter;
import com.ev.Bingo.bus.dto.ColumnDTOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.NetworkTypeCodeDTOInter;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Aly
 */
@ManagedBean(name = "networkTypeCodeView")
@ViewScoped
public class NetworkTypeCodeClient extends BaseBean implements Serializable {

    private NetworkTypeCodeDTOInter selectedRecord, newRecord;
    private List<NetworkTypeCodeDTOInter> recordsList;
    private List<networksDTOInter> networkrecordsList;
    private List<filecolumndefinitionDTOInter> columnList;
    private List<String> privelageList;
    private int listcount;
    private String code;
    private int network;
    private NetworkTypeCodeBOInter engin;
    private networksBOInter networkengin;
    private filecolumndefinitionBOInter colObject;

    public List<filecolumndefinitionDTOInter> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<filecolumndefinitionDTOInter> columnList) {
        this.columnList = columnList;
    }

    public NetworkTypeCodeDTOInter getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(NetworkTypeCodeDTOInter selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public NetworkTypeCodeDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(NetworkTypeCodeDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public List<NetworkTypeCodeDTOInter> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<NetworkTypeCodeDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public List<networksDTOInter> getNetworkrecordsList() {
        return networkrecordsList;
    }

    public void setNetworkrecordsList(List<networksDTOInter> networkrecordsList) {
        this.networkrecordsList = networkrecordsList;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getNetwork() {
        return network;
    }

    public void setNetwork(int network) {
        this.network = network;
    }

    public NetworkTypeCodeBOInter getEngin() {
        return engin;
    }

    public void setEngin(NetworkTypeCodeBOInter engin) {
        this.engin = engin;
    }

    public networksBOInter getNetworkengin() {
        return networkengin;
    }

    public void setNetworkengin(networksBOInter networkengin) {
        this.networkengin = networkengin;
    }

    @PostConstruct
    public void init() {
        try {
            code = "";
            network = 0;
            engin = BOFactory.createNetworkTypeCodeBO(null);
            networkengin = BOFactory.createnetworksBO(null);

            networkrecordsList = (List<networksDTOInter>) networkengin.GetAllRecords();
            recordsList = engin.getNetworkType();
            colObject = BOFactory.createfilecolumndefinitionBo();
            columnList = (List<filecolumndefinitionDTOInter>) colObject.GetAllRecords();

            privelageList = super.findPrivelage(super.GetUser().getUserid(), "NetworkTypeCode.xhtml");
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(NetworkTypeCodeClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public void searchrecords() {
        try {
            recordsList = engin.getNetworkTypeRecord(network, code);
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(NetworkTypeCodeClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean insertPriv() {
        return super.findPrivelageOperation(privelageList, "INSERT");
    }

    public void addRecord() {
        newRecord = DTOFactory.createNetworkTypeCodeDTO();
    }

    public void saveRecord() throws SQLException {
        String message = isValid(newRecord.getCode());
        if (message.contains("Done")) {
            message = engin.insertNetworkType(newRecord);
            recordsList.add(newRecord);
            listcount = recordsList.size();
        }
        searchrecords();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean updatePriv() {
        return super.findPrivelageOperation(privelageList, "UPDATE");
    }

    public void updateRecord() throws SQLException {
        String message = isValid(selectedRecord.getCode());
        if (message.contains("Done")) {
            message = engin.updateNetworkType(selectedRecord);
        }
        searchrecords();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean deletePriv() {
        return super.findPrivelageOperation(privelageList, "DELETE");
    }

    public void deleteRecord() throws SQLException {
        String message = engin.deleteNetworkType(selectedRecord);
        recordsList.remove(selectedRecord);
        listcount = recordsList.size();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public String getColumnName(Integer ID) {
        for (filecolumndefinitionDTOInter column : columnList) {
            if (column.getid() == ID) {
                return column.getname();
            }
        }
        return "UnAssigned";
    }

    public String getNetworkName(Integer ID) {
        for (networksDTOInter networkrecord : networkrecordsList) {
            if (networkrecord.getid() == ID) {
                return networkrecord.getname();
            }
        }
        return "UnAssigned";
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printreportPDF() throws Exception, Throwable {

        String path = (String) engin.printPDFrep(super.GetClient(), super.GetUser().getUsername(), network, code, getNetworkName(network));
        prepDownload(path);
    }

    public String isValid(String s) {
        try {
            Integer.parseInt(s);
            if (s.length() != 6) {
                return "Network Code Must Consist Of 6 Numbers";
            } else {
                return "Done";
            }
        } catch (NumberFormatException e) {
            return "Network Code Must Be Digits Only";
        } catch (NullPointerException e) {
            return "Network Code Is A Required Field";
        }
    }
}
