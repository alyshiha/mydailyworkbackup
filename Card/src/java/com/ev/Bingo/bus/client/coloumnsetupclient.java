/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.columnssetupBOInter;
import com.ev.Bingo.bus.bo.columnssetupdetailsBOInter;
import com.ev.Bingo.bus.bo.filecolumndefinitionBOInter;
import com.ev.Bingo.bus.dto.*;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Administrator
 */
public class coloumnsetupclient extends BaseBean {

    private columnssetupBOInter CSBOEngin;
    private columnssetupdetailsBOInter CSDBOEngin;
    private filecolumndefinitionBOInter FCDBOEngin;
    private Boolean showAdd, showAdd1, showConfirm, showConfirm1;
    private List<columnssetupDTOInter> cSL;
    private columnssetupDTOInter selectedrow, selectedrowdelete, newrecord, selectedrowinsert;
    private List<columnssetupdetailsDTOInter> cSDL;
    private columnssetupdetailsDTOInter selectedrowdetaildelete, newrecord1, selectedrowdetailinsert;
    private String message, message1;
    private List<filecolumndefinitionDTOInter> fCDL;

    public columnssetupBOInter getCSBOEngin() {
        return CSBOEngin;
    }

    public void setCSBOEngin(columnssetupBOInter CSBOEngin) {
        this.CSBOEngin = CSBOEngin;
    }

    public columnssetupdetailsBOInter getCSDBOEngin() {
        return CSDBOEngin;
    }

    public void setCSDBOEngin(columnssetupdetailsBOInter CSDBOEngin) {
        this.CSDBOEngin = CSDBOEngin;
    }

    public filecolumndefinitionBOInter getFCDBOEngin() {
        return FCDBOEngin;
    }

    public void setFCDBOEngin(filecolumndefinitionBOInter FCDBOEngin) {
        this.FCDBOEngin = FCDBOEngin;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public List<columnssetupDTOInter> getcSL() {
        return cSL;
    }

    public void setcSL(List<columnssetupDTOInter> cSL) {
        this.cSL = cSL;
    }

    public columnssetupDTOInter getSelectedrow() {
        return selectedrow;
    }

    public void setSelectedrow(columnssetupDTOInter selectedrow) {
        this.selectedrow = selectedrow;
    }

    public columnssetupDTOInter getSelectedrowdelete() {
        return selectedrowdelete;
    }

    public void setSelectedrowdelete(columnssetupDTOInter selectedrowdelete) {
        this.selectedrowdelete = selectedrowdelete;
    }

    public columnssetupDTOInter getNewrecord() {
        return newrecord;
    }

    public void setNewrecord(columnssetupDTOInter newrecord) {
        this.newrecord = newrecord;
    }

    public columnssetupDTOInter getSelectedrowinsert() {
        return selectedrowinsert;
    }

    public void setSelectedrowinsert(columnssetupDTOInter selectedrowinsert) {
        this.selectedrowinsert = selectedrowinsert;
    }

    public List<columnssetupdetailsDTOInter> getcSDL() {
        return cSDL;
    }

    public void setcSDL(List<columnssetupdetailsDTOInter> cSDL) {
        this.cSDL = cSDL;
    }

    public columnssetupdetailsDTOInter getSelectedrowdetaildelete() {
        return selectedrowdetaildelete;
    }

    public void setSelectedrowdetaildelete(columnssetupdetailsDTOInter selectedrowdetaildelete) {
        this.selectedrowdetaildelete = selectedrowdetaildelete;
    }

    public columnssetupdetailsDTOInter getNewrecord1() {
        return newrecord1;
    }

    public void setNewrecord1(columnssetupdetailsDTOInter newrecord1) {
        this.newrecord1 = newrecord1;
    }

    public columnssetupdetailsDTOInter getSelectedrowdetailinsert() {
        return selectedrowdetailinsert;
    }

    public void setSelectedrowdetailinsert(columnssetupdetailsDTOInter selectedrowdetailinsert) {
        this.selectedrowdetailinsert = selectedrowdetailinsert;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public List<filecolumndefinitionDTOInter> getfCDL() {
        return fCDL;
    }

    public void setfCDL(List<filecolumndefinitionDTOInter> fCDL) {
        this.fCDL = fCDL;
    }

    public coloumnsetupclient() throws Throwable {
        super();
        GetAccess();
        FCDBOEngin = BOFactory.createfilecolumndefinitionBo();
        CSBOEngin = BOFactory.createcolumnssetupBO();
        CSDBOEngin = BOFactory.createcolumnssetupdetailsBO();
        fCDL = (List<filecolumndefinitionDTOInter>) FCDBOEngin.GetAllRecords();
        cSL = (List<columnssetupDTOInter>) CSBOEngin.GetAllRecords();
        cSDL = new ArrayList<columnssetupdetailsDTOInter>();
        showAdd = true;
        showConfirm = false;
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void onRowSelect(SelectEvent e) throws Throwable {
        selectedrow = DTOFactory.createcolumnssetupDTO();
        selectedrow = (columnssetupDTO) e.getObject();
        cSDL = (List<columnssetupdetailsDTOInter>) CSDBOEngin.GetListOfRecordsCS(selectedrow);
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void fillDelete(ActionEvent e) {
        selectedrowdelete = (columnssetupDTO) e.getComponent().getAttributes().get("removedRow");
    }

    public void deleteRecord() throws Throwable {
        String deleteddetailedrecordcount = "", deletedrecordcount = "";
        if (selectedrowdelete.getid() != 0) {
            deleteddetailedrecordcount = (String) CSDBOEngin.deleterecord(selectedrowdelete);
            if (deleteddetailedrecordcount.contains("Column Has Been Deleted")) {
                deletedrecordcount = (String) CSBOEngin.deleterecord(selectedrowdelete);
                message = deletedrecordcount + " " + deleteddetailedrecordcount;
                cSL.remove(selectedrowdelete);
                cSDL.clear();
                showAdd = true;
                showConfirm = false;
            }
        } else {
            cSL.remove(selectedrowdelete);
            cSDL.clear();
            showAdd = true;
            showConfirm = false;
            message = "Column Has Been Removed";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void fillDelete1(ActionEvent e) {
        selectedrowdetaildelete = (columnssetupdetailsDTOInter) e.getComponent().getAttributes().get("removedRow1");
    }

    public void deleteRecord1() throws Throwable {
        message1 = (String) CSDBOEngin.deleterecordDetail(selectedrowdetaildelete);
        if (message1.contains("Detailed Column Has Been Deleted")) {
            cSDL.remove(selectedrowdetaildelete);
            showAdd1 = true;
            showConfirm1 = false;
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }

    public void addcol() {
        newrecord = DTOFactory.createcolumnssetupDTO();
        cSL.add(0, newrecord);
        selectedrow = newrecord;
        showAdd = false;
        showConfirm = true;
        cSDL.clear();
    }

    public void confirmAdd() throws Throwable {
        if (!"".equals(newrecord.getname())) {
            message = (String) CSBOEngin.insertrecord(newrecord);
            if (message.contains("Coloumn Has Been")) {
                showAdd = true;
                showConfirm = false;
                selectedrow = null;
                cSL = (List<columnssetupDTOInter>) CSBOEngin.GetAllRecords();
                cSDL.clear();
            }
        } else {
            message = "Please Insert Column Name";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void addcol2() {
        newrecord1 = DTOFactory.createcolumnssetupdetailsDTO();
        cSDL.add(0, newrecord1);
        newrecord1.setdisplaycolumnid(selectedrow.getid());
        selectedrowdetailinsert = newrecord1;
        showAdd1 = false;
        showConfirm1 = true;
    }

    public void confirmAdd1() throws Throwable {
        if (newrecord1.getdisplaycolumnid() != 0) {
            message1 = (String) CSDBOEngin.insertrecord(newrecord1);
            if (message1.contains("Column Has Been")) {
                showAdd1 = true;
                showConfirm1 = false;
            }
        } else {
            message1 = "Please Select Column Name";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }

    public void saveAll() throws Throwable {
        message = (String) CSBOEngin.save(cSL);
        message = "Records are Updated";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void saveAll1() throws Throwable {
        message1 = (String) CSDBOEngin.save(cSDL);
        message1 = "Records are Updated";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }
}
