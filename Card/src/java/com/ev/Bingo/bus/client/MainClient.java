/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import DBCONN.ConnectionPool;
import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.MainBoInter;
import com.ev.Bingo.bus.bo.licenseBOInter;
import com.ev.Bingo.bus.dto.licenseDTOInter;
import com.ev.Bingo.bus.dto.systemtableDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.io.Serializable;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class MainClient extends BaseBean implements Serializable {

    String message, menuItems, userName, bankName, iframeSrc, timeOut, loading, user;
    TimeZone timeZone;

    public void onIdle() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                "No activity.", "What are you doing over there?"));
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getIframeSrc() {

        return iframeSrc;
    }

    public void setIframeSrc(String iframeSrc) {
        System.out.println(iframeSrc);
        this.iframeSrc = iframeSrc;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(String menuItems) {
        this.menuItems = menuItems;
    }
    private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public MainClient() throws Throwable {
        super();
        super.Testlogin();
        systemtableDTOInter stDTO = (systemtableDTOInter) super.Getseesiontime();
        timeOut = stDTO.getparametervalue();
        System.out.println(timeOut);
        usersDTOInter UserDTO = (usersDTOInter) super.GetUser();
        licenseBOInter LicBO = BOFactory.createlicenseBO();
        List<licenseDTOInter> LDTO = (List<licenseDTOInter>) LicBO.GetAllRecords();
        version = LDTO.get(0).getversion();
        MainBoInter MainBO = BOFactory.createMainBo();
        menuItems = MainBO.generateMenu(UserDTO);
        System.out.println(menuItems);
        userName = UserDTO.getUsername();
        user = UserDTO.getLogonname();
        timeZone = TimeZone.getDefault();
        bankName = super.GetClient();
        FacesContext context2 = FacesContext.getCurrentInstance();
        HttpSession session2 = (HttpSession) context2.getExternalContext().getSession(true);
        HttpServletResponse response2 = (HttpServletResponse) context2.getExternalContext().getResponse();
        response2.addHeader("X-Frame-Options", "DENY");
        if ("1".equals(LicBO.GetRecord())) {
            message = "Some transaction Out Of License";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
        iframeSrc = "DashBoard.xhtml";

    }

    public String logout() throws Throwable {
        Thread.sleep(1000);
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        Thread.sleep(1000);
//        CoonectionHandler.getInstance().freeuser(user);
        clearsessionattributes();
        //session.invalidate();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        response.sendRedirect("Login.xhtml");
        return "Logout";
    }

    public void testClick() {
        System.out.println("close");
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
            clearsessionattributes();
            session.invalidate();
        } catch (Throwable ex) {
            Logger.getLogger(MainClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
