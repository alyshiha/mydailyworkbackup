/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.profileBOInter;
import com.ev.Bingo.bus.bo.usersBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.profileDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import static java.util.Collections.copy;
import java.util.List;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Collections;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author Administrator
 */
public class SystemUsersClient extends BaseBean {

    private usersBOInter brObject;
    private profileBOInter ProfObject;
    private List<usersDTOInter> brList;
    private List<usersDTOInter> old;
    private Boolean showAdd, showConfirm;
    private List<profileDTOInter> profileList;
    private Integer profileInt, listSize;
    private String message, eName, uName, password, confirmPassword;
    private usersDTOInter brAttr, brAttrUp, newBlock;
    private CompareList CompareEngin;

    public usersDTOInter getNewBlock() {
        return newBlock;
    }

    public void setNewBlock(usersDTOInter newBlock) {
        this.newBlock = newBlock;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public usersDTOInter getBrAttr() {
        return brAttr;
    }

    public void setBrAttr(usersDTOInter brAttr) {
        this.brAttr = brAttr;
    }

    public usersDTOInter getBrAttrUp() {
        return brAttrUp;
    }

    public void setBrAttrUp(usersDTOInter brAttrUp) {
        this.brAttrUp = brAttrUp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public profileBOInter getProfObject() {
        return ProfObject;
    }

    public void setProfObject(profileBOInter ProfObject) {
        this.ProfObject = ProfObject;
    }

    public List<usersDTOInter> getBrList() {
        return brList;
    }

    public void setBrList(List<usersDTOInter> brList) {
        this.brList = brList;
    }

    public usersBOInter getBrObject() {
        return brObject;
    }

    public void setBrObject(usersBOInter brObject) {
        this.brObject = brObject;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public Integer getProfileInt() {
        return profileInt;
    }

    public void setProfileInt(Integer profileInt) {
        this.profileInt = profileInt;
    }

    public List<profileDTOInter> getProfileList() {
        return profileList;
    }

    public void setProfileList(List<profileDTOInter> profileList) {
        this.profileList = profileList;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public SystemUsersClient() throws Throwable {
        super();
        GetAccess();
        brObject = BOFactory.createusersBO();
        ProfObject = BOFactory.createprofileBO();
        brList = (List<usersDTOInter>) brObject.findRecordsAlluserManag();
        old = (List<usersDTOInter>) brObject.findRecordsAlluserManag();
        showAdd = true;
        showConfirm = false;
        profileList = (List<profileDTOInter>) ProfObject.GetAllRecords();
        profileInt = profileList.get(0).getprofileid();
        listSize = brList.size();

    }

    public static <T extends Object> List<T> cloneList(List<T> list) {
        return ((List<T>) ((ArrayList<T>) list).clone());
    }

    public void fillDel(ActionEvent e) {
        brAttr = (usersDTOInter) e.getComponent().getAttributes().get("removedRow");
    }

    public String FindProfile(Integer ProfileID) {
        for (profileDTOInter Prof : profileList) {
            if (Prof.getprofileid() == ProfileID) {
                return Prof.getprofilename();
            }
        }
        return "No Profile Assigned";
    }

    public boolean FindUserName(String userName) {
        for (usersDTOInter User : brList) {
            if (User.getLogonname().toUpperCase().equals(userName)) {
                return true;
            }
        }
        return false;
    }

    public void deleteBlockReason() throws Throwable {
        message = (String) brObject.deleterecord(brAttr);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
        if (message.contains("Record Has Been")) {
            old = (List<usersDTOInter>) brObject.findRecordsAlluserManag();
            brList.remove(brAttr);
            showAdd = true;
            showConfirm = false;
        }
    }

    public void resetVars() {
        eName = "";
        uName = "";
        password = "";
        confirmPassword = "";

    }

    public void saveAll() throws Throwable {
        ArrayList result = CompareList.getLog(old, new ArrayList<>(brList), "userid");
        System.out.println(result);
        message = (String) brObject.save(brList,result);
        old = (List<usersDTOInter>) brObject.findRecordsAlluserManag();
        
        message = "Records are Updated";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));

    }

    public void confirmInsert() throws Throwable {

        newBlock = DTOFactory.createusersDTO();
        brList = (List<usersDTOInter>) brObject.findRecordsAlluserManag();

        if (!password.equals(confirmPassword)) {
            message = "Passwords don't match";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } else if (FindUserName(uName.toUpperCase()) == true) {
            message = "User Name aleady Found";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } else {
            newBlock.setUsername(eName);
            newBlock.setUseraname(eName);
            newBlock.setProfilename(profileInt.toString());
            newBlock.setLogonname(uName);
            newBlock.setUserpassword(password);
            newBlock.setBoollockedstate(Boolean.FALSE);
            newBlock.setLockedstate(1);
            newBlock.setProfilename(FindProfile(profileInt));
            newBlock.setProfileid(profileInt);
            message = (String) brObject.insertrecord(newBlock);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
            if (message.contains("Row Has Been"));
            {
                old = (List<usersDTOInter>) brObject.findRecordsAlluserManag();
                brList.add(0, newBlock);
                eName = "";
                uName = "";
                password = "";
                profileInt = 0;
            }
        }

    }
}
