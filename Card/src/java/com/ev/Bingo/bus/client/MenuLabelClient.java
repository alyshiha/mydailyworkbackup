/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.menulabelsBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.menulabelsDTOInter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Aly
 */
public class MenuLabelClient extends BaseBean {

    private menulabelsBOInter BoEngin;
    private List<menulabelsDTOInter> parents, pages;
    private List<String> fileList;
    private List<menulabelsDTOInter> allRecords;
    private List<String> newPages;
    private Boolean showConfirm, showAdd;
    private String message, englishLabel, page;
    private int type, parent;
    private menulabelsDTOInter selectedRec, newrecord;

    public String getEnglishLabel() {
        return englishLabel;
    }

    public void setEnglishLabel(String englishLabel) {
        this.englishLabel = englishLabel;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public menulabelsBOInter getBoEngin() {
        return BoEngin;
    }

    public void setBoEngin(menulabelsBOInter BoEngin) {
        this.BoEngin = BoEngin;
    }

    public List<menulabelsDTOInter> getParents() {
        return parents;
    }

    public void setParents(List<menulabelsDTOInter> parents) {
        this.parents = parents;
    }

    public List<String> getFileList() {
        return fileList;
    }

    public void setFileList(List<String> fileList) {
        this.fileList = fileList;
    }

    public List<menulabelsDTOInter> getAllRecords() {
        return allRecords;
    }

    public void setAllRecords(List<menulabelsDTOInter> allRecords) {
        this.allRecords = allRecords;
    }

    public List<String> getNewPages() {
        return newPages;
    }

    public void setNewPages(List<String> newPages) {
        this.newPages = newPages;
    }

    public Boolean isShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean isShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public menulabelsDTOInter getSelectedRec() {
        return selectedRec;
    }

    public void setSelectedRec(menulabelsDTOInter selectedRec) {
        this.selectedRec = selectedRec;
    }

    public List<menulabelsDTOInter> getPages() {
        return pages;
    }

    public void setPages(List<menulabelsDTOInter> pages) {
        this.pages = pages;
    }

    public static List<String> listFilesForFolder() {
        File folder = new File("C:\\Users\\AlyShiha\\Desktop\\CardProj\\Card\\web");
        List<String> FileName = new ArrayList<String>();
        for (final File fileEntry : folder.listFiles()) {
            FileName.add(fileEntry.getName());
        }
        return FileName;
    }

    public static List<String> ListtoString(List<menulabelsDTOInter> List) {
        List<String> StringList = new ArrayList<String>();
        for (menulabelsDTOInter Item : List) {
            StringList.add(Item.getpage());
        }
        return StringList;
    }

    public static List<String> FilterPages(List<String> HDRecord, List<String> DBRecord) {
        List<String> FileName = new ArrayList<String>();
        for (String HDfile : HDRecord) {
            if (!DBRecord.contains(HDfile)) {
                FileName.add(HDfile);
            }
        }
        return FileName;
    }

    public MenuLabelClient() throws Throwable {
        super();
        GetAccess();
        BoEngin = BOFactory.createmenulabelsBO();
        parents = (List<menulabelsDTOInter>) BoEngin.GetParentListOfRecords();
        pages = (List<menulabelsDTOInter>) BoEngin.GetPageListOfRecords();
        fileList = listFilesForFolder();
        allRecords = (List<menulabelsDTOInter>) BoEngin.GetAllRecords();
        newPages = FilterPages(fileList, ListtoString(pages));
        showAdd = true;
        showConfirm = false;
    }

    public void fillDelete(ActionEvent e) {
        selectedRec = (menulabelsDTOInter) e.getComponent().getAttributes().get("removedRow");
        message = "";
    }

    public void deleteRecord() throws Throwable {
        if (selectedRec.getmenuid() != 0) {
            message = (String) BoEngin.deleterecord(selectedRec);
            if (message.contains("Record Has Been Deleted")) {
                allRecords.remove(selectedRec);
                showAdd = true;
                showConfirm = false;

            }
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } else {
            message = "Please Select Record";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }

    public void saveAll() throws Throwable {

        message = (String) BoEngin.save(allRecords);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void addRecord() throws Throwable {
        newrecord = DTOFactory.createmenulabelsDTO();
        if (!"".equals(englishLabel) && !"0".equals(page)) {
            newrecord.setenglishlabel(englishLabel);
            newrecord.setpage(page);
            newrecord.setparentid(parent);
            newrecord.settype(type);
            message = (String) BoEngin.insertrecord(newrecord);
            if (message.contains("Row Has Been Inserted")) {
                allRecords.add(newrecord);
            }
            englishLabel = "";
            page = "";
            parent = 0;
            type = 0;
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } else {
            message = "You Have To Choose Page And English Name";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }

    }

}
