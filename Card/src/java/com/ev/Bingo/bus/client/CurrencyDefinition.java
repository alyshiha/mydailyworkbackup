/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.currencyBOInter;
import com.ev.Bingo.bus.bo.currencymasterBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.currencyDTOInter;
import com.ev.Bingo.bus.dto.currencymasterDTOInter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Administrator
 */
public class CurrencyDefinition extends BaseBean {

    private currencymasterBOInter cdObject;
    private currencyBOInter cObject;
    private List<currencymasterDTOInter> cdList;
    private List<currencyDTOInter> cdDetailsList;
    private currencymasterDTOInter timeShiftAttr;
    private currencymasterDTOInter timeShiftAttr1;
    private currencyDTOInter CurrencyDetail;
    private currencymasterDTOInter selectedMT;
    private currencymasterDTOInter newRecord;
    private currencyDTOInter newRecord1;
    private currencyDTOInter timeShiftDetailsAttr, timeShiftDetailsAttr1;
    private Integer myInt;
    private Boolean showAdd, showAdd1;
    private Boolean showConfirm, showConfirm1;
    private String message;
    private String message1;

    public List<currencyDTOInter> getCdDetailsList() {
        return cdDetailsList;
    }

    public void setCdDetailsList(List<currencyDTOInter> cdDetailsList) {
        this.cdDetailsList = cdDetailsList;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public Integer getMyInt() {
        return myInt;
    }

    public void setMyInt(Integer myInt) {
        this.myInt = myInt;
    }

    public currencymasterDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(currencymasterDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public currencyDTOInter getNewRecord1() {
        return newRecord1;
    }

    public void setNewRecord1(currencyDTOInter newRecord1) {
        this.newRecord1 = newRecord1;
    }

    public currencymasterDTOInter getSelectedMT() {
        return selectedMT;
    }

    public void setSelectedMT(currencymasterDTOInter selectedMT) {
        this.selectedMT = selectedMT;
    }

    public currencymasterDTOInter getTimeShiftAttr() {
        return timeShiftAttr;
    }

    public void setTimeShiftAttr(currencymasterDTOInter timeShiftAttr) {
        this.timeShiftAttr = timeShiftAttr;
    }

    public currencymasterDTOInter getTimeShiftAttr1() {
        return timeShiftAttr1;
    }

    public void setTimeShiftAttr1(currencymasterDTOInter timeShiftAttr1) {
        this.timeShiftAttr1 = timeShiftAttr1;
    }

    public currencyDTOInter getTimeShiftDetailsAttr() {
        return timeShiftDetailsAttr;
    }

    public void setTimeShiftDetailsAttr(currencyDTOInter timeShiftDetailsAttr) {
        this.timeShiftDetailsAttr = timeShiftDetailsAttr;
    }

    public currencyDTOInter getTimeShiftDetailsAttr1() {
        return timeShiftDetailsAttr1;
    }

    public void setTimeShiftDetailsAttr1(currencyDTOInter timeShiftDetailsAttr1) {
        this.timeShiftDetailsAttr1 = timeShiftDetailsAttr1;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<currencymasterDTOInter> getCdList() {
        return cdList;
    }

    public void setCdList(List<currencymasterDTOInter> cdList) {
        this.cdList = cdList;
    }

    public currencymasterBOInter getCdObject() {
        return cdObject;
    }

    public void setCdObject(currencymasterBOInter cdObject) {
        this.cdObject = cdObject;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public CurrencyDefinition() throws Throwable {
        super();
        GetAccess();
        cdObject = BOFactory.createcurrencymasterBO();
        cdList = (List<currencymasterDTOInter>) cdObject.GetAllRecords();
        cObject = BOFactory.createcurrencyBO();
        cdDetailsList = new ArrayList<currencyDTOInter>();
        showAdd = true;
        showConfirm = false;
        showAdd1 = true;
        showConfirm1 = false;
    }

    public String ValidateNullcurrencymaster(Object... obj) {
        currencymasterDTOInter RecordToInsert = (currencymasterDTOInter) obj[0];
        String Validate = "Validate";
        if (RecordToInsert.getid() == 0) {
            Validate = Validate + "   id  ";
        }
        if (!Validate.equals("Validate")) {
            Validate = Validate.replace("Validate", "");
            Validate = Validate + "is a required fields";
        }
        return Validate;
    }

    public void onRowSelect1(SelectEvent e) throws Throwable {
        selectedMT = DTOFactory.createcurrencymasterDTO();
        selectedMT = (currencymasterDTOInter) e.getObject();
        String Validate = ValidateNullcurrencymaster(selectedMT);
        if (Validate.equals("Validate")) {
            cdDetailsList = (List<currencyDTOInter>) cObject.GetListOfRecords(selectedMT);
            showAdd1 = true;
            showConfirm1 = false;
        } else {
            message = Validate;

        }
    }

    public void fillDelete(ActionEvent e) {
        timeShiftAttr1 = (currencymasterDTOInter) e.getComponent().getAttributes().get("removedRow");
    }

    public void deleteRecord() throws Throwable {
        String Temp = "";
        Integer count;
        cdDetailsList = (List<currencyDTOInter>) cObject.GetListOfRecords(timeShiftAttr1);
        Temp = (String) cdObject.deleterecord(timeShiftAttr1);
        if (Temp.contains("currency With")) {
            message = Temp;
            count = cdDetailsList.size();
            message = message + count.toString() + " Currency Detail Has Been Deleted";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
            cdList.remove(timeShiftAttr1);
            cdDetailsList.clear();
            showAdd = true;
            showConfirm = false;
        }
        if (timeShiftAttr1.getid() == 0) {
            cdList.remove(timeShiftAttr1);
            cdDetailsList.clear();
            showAdd = true;
            showConfirm = false;
            message = "Currency has been Removed";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }

    }

    public void fillDelete1(ActionEvent e) {
        CurrencyDetail = (currencyDTOInter) e.getComponent().getAttributes().get("removedRow1");
    }

    public void deleteRecord1() throws Throwable {
        message1 = (String) cObject.deleterecord(CurrencyDetail);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
        if (message1.contains(" Currency Detail Has Been deleted")) {
            cdDetailsList.remove(CurrencyDetail);
            showAdd1 = true;
            showConfirm1 = false;
        }

    }

    public void addTimeShift1() {
        newRecord1 = DTOFactory.createcurrencyDTO();
        cdDetailsList.add(0, newRecord1);
        showAdd1 = false;
        showConfirm1 = true;
    }

    public void confirmAdd1() throws Throwable {
        String[] inputs = {newRecord1.getabbreviation(), "" + newRecord1.getdefaultcurrency(), "" + newRecord1.getmasterid(), "" + newRecord1.getid()};
        String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
        Boolean found = Boolean.FALSE;
        for (String input : inputs) {
            boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
            if (b == false) {
                found = Boolean.TRUE;
            }
            if (!"".equals(input)) {
                for (String validate1 : validate) {
                    if (input.toUpperCase().contains(validate1.toUpperCase())) {
                        found = Boolean.TRUE;
                    }
                }
            }
        }
        if (!found) {

            newRecord1.setmasterid(selectedMT.getid());
            List<currencyDTOInter> RecList = (List<currencyDTOInter>) cObject.GetListOfRecordsCurrency(newRecord1);
            Integer CountOfDef = 0;
            for (currencyDTOInter Rec : RecList) {
                if (Rec.isDefaultcurrencyB() == Boolean.TRUE) {
                    CountOfDef++;
                }
            }
            if (newRecord1.isDefaultcurrencyB() == Boolean.TRUE) {
                CountOfDef++;
            }
            if (CountOfDef < 2) {
                message1 = (String) cObject.insertrecord(newRecord1);
                super.showMessage(message1);
                if (message1.contains("Record Has Been")) {
                    showAdd1 = true;
                    showConfirm1 = false;
                }
            } else {
                message1 = "Can't insert more than one default flag";
                super.showMessage(message1);
            }
        } else {
            message1 = "Please Enter Valid Data";
               super.showMessage(message1);
        }

    }

    public void saveAll() throws Throwable {

        Boolean Gfound = Boolean.FALSE;
        for (currencyDTOInter Recvalidate : cdDetailsList) {
            if (!Gfound) {
                String[] inputs = {Recvalidate.getabbreviation(), "" + Recvalidate.getdefaultcurrency(), "" + Recvalidate.getid(), "" + Recvalidate.getmasterid()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (found) {
                    Gfound = Boolean.TRUE;
                }
            }
        }
        if (!Gfound) {
            Integer CountOfDef = 0;
            for (currencyDTOInter Rec : cdDetailsList) {
                if (Rec.isDefaultcurrencyB() == Boolean.TRUE) {
                    CountOfDef++;
                }
            }
            if (CountOfDef < 2) {
                message1 = (String) cObject.save(cdDetailsList);
                message1 = "Records are Updated";
                super.showMessage(message1);
            } else {
                message1 = "Can't insert more than one default flag";
                super.showMessage(message1);
            }
        } else {
            message1 = "Please Enter Valid Data";
               super.showMessage(message1);
        }

    }

    public void addTimeShift() {
        newRecord = DTOFactory.createcurrencymasterDTO();
        cdList.add(0, newRecord);
        cdDetailsList.clear();
        selectedMT = newRecord;
        showAdd = false;
        showConfirm = true;
    }

    public void confirmAdd() throws Throwable {
        String[] inputs = {newRecord.getname(), "" + newRecord.getsymbol(), "" + "" + newRecord.getdefaultcurrency()};
        String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
        Boolean found = Boolean.FALSE;
        for (String input : inputs) {
            boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
            if (b == false) {
                found = Boolean.TRUE;
            }
            if (!"".equals(input)) {
                for (String validate1 : validate) {
                    if (input.toUpperCase().contains(validate1.toUpperCase())) {
                        found = Boolean.TRUE;
                    }
                }
            }
        }
        if (!found) {
            List<currencymasterDTOInter> RecList = (List<currencymasterDTOInter>) cdObject.GetAllRecords();
            Integer CountOfDef = 0;
            for (currencymasterDTOInter Rec : RecList) {
                if (Rec.getdefaultcurrency() == 1) {
                    CountOfDef++;
                }
            }
            if (newRecord.getdefaultcurrencyB() == Boolean.TRUE) {
                CountOfDef++;
            }
            if (CountOfDef < 2) {
                message = (String) cdObject.insertrecord(newRecord);
                super.showMessage(message);
                if (message.contains("Record Has Been")) {
                    showAdd = true;
                    showConfirm = false;
                }
            } else {
                message = "Can't insert more than one default flag";
                super.showMessage(message);
            }
        } else {
            message = "Please Enter Valid Data";
               super.showMessage(message);
        }

    }

    public void saveAll1() throws Throwable {
        Boolean Gfound = Boolean.FALSE;
        for (currencymasterDTOInter Recvalidate : cdList) {
            if (!Gfound) {
                String[] inputs = {Recvalidate.getname().concat("a"), Recvalidate.getsymbol().concat("a"), "" + Recvalidate.getdefaultcurrency(), "" + Recvalidate.getid()};
                String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
                Boolean found = Boolean.FALSE;
                for (String input : inputs) {
                    boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
                    if (b == false) {
                        found = Boolean.TRUE;
                    }
                    if (!"".equals(input)) {
                        for (String validate1 : validate) {
                            if (input.toUpperCase().contains(validate1.toUpperCase())) {
                                found = Boolean.TRUE;
                            }
                        }
                    }
                }
                if (found) {
                    Gfound = Boolean.TRUE;
                }
            }
        }
        if (!Gfound) {
            Integer CountOfDef = 0;
            for (currencymasterDTOInter Rec : cdList) {
                if (Rec.getdefaultcurrencyB() == Boolean.TRUE) {
                    CountOfDef++;
                }
            }
            if (CountOfDef < 2) {
                message = (String) cdObject.save(cdList);
                message = "Records are Updated";
                super.showMessage(message);
            } else {
                message = "Can't insert more than one default flag";
                super.showMessage(message);
            }
        } else {
            message = "Please Enter Valid Data";
               super.showMessage(message);
        }

    }
}
