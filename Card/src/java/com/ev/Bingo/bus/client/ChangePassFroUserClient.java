/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.userpassBOInter;
import com.ev.Bingo.bus.bo.usersBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.userpassDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class ChangePassFroUserClient extends BaseBean {

    private String oldPass, message, newPass, confPass;
    private Boolean dialogFlag = false;

    public Boolean getDialogFlag() {
        return dialogFlag;
    }

    public void setDialogFlag(Boolean dialogFlag) {
        this.dialogFlag = dialogFlag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getConfPass() {
        return confPass;
    }

    public void setConfPass(String confPass) {
        this.confPass = confPass;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public ChangePassFroUserClient() {
        GetAccess();
    }

    public void save() throws Throwable {
        usersBOInter UBO = BOFactory.createusersBO();
        if (UBO.GetRecordpass(super.GetUser(), oldPass)) {
            if (!newPass.matches("(?=^.{8,}$)(?=.*\\d)(?=.*\\W+)(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$")) {
                message = "The input password volates the policy, please make sure that your password follows the following rules:"
                        + "Password length is 9 characters or more."
                        + "It contains one or more capital letter(s)."
                        + "It contains a combination of characters, numbers and special characters.";
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(message));
                return;
            } else if (!newPass.equals(confPass)) {
                message = "Password Mismatch";
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(message));
                return;
            }
            userpassDTOInter UPDTO = DTOFactory.createuserpassDTO();
            userpassDTOInter NUPDTO = DTOFactory.createuserpassDTO();
            UPDTO.setuserid(super.GetUser().getUserid());
            UPDTO.setpass(newPass);
            userpassBOInter UPBO = BOFactory.createuserpassBO();
            NUPDTO = (userpassDTOInter) UPBO.GetRecord(UPDTO);
            if (NUPDTO.getchangedate() != null) {
                message = "This Password Has Been Entered Before At " + NUPDTO.getchangedate();
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(message));
                return;
            }
            NUPDTO.setuserid(UPDTO.getuserid());
            NUPDTO.setpass(newPass);
            message = (String) UPBO.insertrecord(NUPDTO);
            FacesContext context = FacesContext.getCurrentInstance();

            if (message.contains("Row Has Been")) {
                usersDTOInter UDTO = DTOFactory.createusersDTO();
                UDTO = super.GetUser();
                UDTO.setUserpassword(newPass);
                message = (String) UBO.updaterecord(UDTO);
               // CoonectionHandler.getInstance().freeuser(UDTO.getLogonname());
                context.addMessage(null, new FacesMessage(message));
                return;
            } else {
                return;
            }
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Invalid User Password"));
        }
    }
}
