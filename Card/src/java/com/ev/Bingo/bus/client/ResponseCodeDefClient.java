/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import javax.faces.event.ActionEvent;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.transactionresponsecodeBOInter;
import com.ev.Bingo.bus.bo.transactionresponsemasterBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.transactionresponsecodeDTOInter;
import com.ev.Bingo.bus.dto.transactionresponsemasterDTOInter;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author Administrator
 */
public class ResponseCodeDefClient extends BaseBean {

    private transactionresponsemasterBOInter TRMBO;
    private transactionresponsecodeBOInter TRCBO;
    private List<transactionresponsemasterDTOInter> cdList;
    private List<transactionresponsecodeDTOInter> cdDetailsList;
    private transactionresponsemasterDTOInter timeShiftAttr;
    private transactionresponsemasterDTOInter timeShiftAttr1;
    private transactionresponsemasterDTOInter selectedMT;
    private transactionresponsemasterDTOInter newRecord;
    private transactionresponsecodeDTOInter newRecord1;
    private transactionresponsecodeDTOInter timeShiftDetailsAttr, timeShiftDetailsAttr1;
    private Integer myInt;
    private Boolean showAdd, showAdd1;
    private Boolean showConfirm, showConfirm1;
    private String message;
    private String message1;

    public transactionresponsecodeBOInter getTRCBO() {
        return TRCBO;
    }

    public void setTRCBO(transactionresponsecodeBOInter TRCBO) {
        this.TRCBO = TRCBO;
    }

    public transactionresponsemasterBOInter getTRMBO() {
        return TRMBO;
    }

    public void setTRMBO(transactionresponsemasterBOInter TRMBO) {
        this.TRMBO = TRMBO;
    }

    public List<transactionresponsecodeDTOInter> getCdDetailsList() {
        return cdDetailsList;
    }

    public void setCdDetailsList(List<transactionresponsecodeDTOInter> cdDetailsList) {
        this.cdDetailsList = cdDetailsList;
    }

    public List<transactionresponsemasterDTOInter> getCdList() {
        return cdList;
    }

    public void setCdList(List<transactionresponsemasterDTOInter> cdList) {
        this.cdList = cdList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public Integer getMyInt() {
        return myInt;
    }

    public void setMyInt(Integer myInt) {
        this.myInt = myInt;
    }

    public transactionresponsemasterDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(transactionresponsemasterDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public transactionresponsecodeDTOInter getNewRecord1() {
        return newRecord1;
    }

    public void setNewRecord1(transactionresponsecodeDTOInter newRecord1) {
        this.newRecord1 = newRecord1;
    }

    public transactionresponsemasterDTOInter getSelectedMT() {
        return selectedMT;
    }

    public void setSelectedMT(transactionresponsemasterDTOInter selectedMT) {
        this.selectedMT = selectedMT;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public transactionresponsemasterDTOInter getTimeShiftAttr() {
        return timeShiftAttr;
    }

    public void setTimeShiftAttr(transactionresponsemasterDTOInter timeShiftAttr) {
        this.timeShiftAttr = timeShiftAttr;
    }

    public transactionresponsemasterDTOInter getTimeShiftAttr1() {
        return timeShiftAttr1;
    }

    public void setTimeShiftAttr1(transactionresponsemasterDTOInter timeShiftAttr1) {
        this.timeShiftAttr1 = timeShiftAttr1;
    }

    public transactionresponsecodeDTOInter getTimeShiftDetailsAttr() {
        return timeShiftDetailsAttr;
    }

    public void setTimeShiftDetailsAttr(transactionresponsecodeDTOInter timeShiftDetailsAttr) {
        this.timeShiftDetailsAttr = timeShiftDetailsAttr;
    }

    public transactionresponsecodeDTOInter getTimeShiftDetailsAttr1() {
        return timeShiftDetailsAttr1;
    }

    public void setTimeShiftDetailsAttr1(transactionresponsecodeDTOInter timeShiftDetailsAttr1) {
        this.timeShiftDetailsAttr1 = timeShiftDetailsAttr1;
    }

    public ResponseCodeDefClient() throws Throwable {
        super();
        GetAccess();
        TRMBO = BOFactory.createtransactionresponsemasterBO();
        TRCBO = BOFactory.createtransactionresponsecodeBO();
        cdList = new ArrayList<transactionresponsemasterDTOInter>();
        cdDetailsList = new ArrayList<transactionresponsecodeDTOInter>();
        cdList = (List<transactionresponsemasterDTOInter>) TRMBO.GetAllRecords();
        showAdd = true;
        showConfirm = false;
        showAdd1 = false;
        showConfirm1 = false;
    }

    public void addTimeShift() {
        newRecord = DTOFactory.createtransactionresponsemasterDTO();
        cdList.add(0, newRecord);
        showAdd = false;
        showConfirm = true;
        message = "";
        message1 = "";
    }

    public void confirmAdd() throws Throwable {
        message = (String) TRMBO.insertrecord(newRecord);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
        if (message.contains("Record Has Been Inserted")) {
            showAdd = true;
            showConfirm = false;
            message1 = "";

        }
    }

    public void fillDelete(ActionEvent e) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("This Process Might Take Long Time"));
        timeShiftAttr1 = (transactionresponsemasterDTOInter) e.getComponent().getAttributes().get("removedRow");
        message = "";
        message1 = "";
    }

    public void deleteRecord() throws Throwable {
        if (timeShiftAttr1.getid() != 0) {
            message = (String) TRCBO.deleterecordByMaster(timeShiftAttr1);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
            message1 = "";
            if (message.contains("Detail Record Has Been deleted")) {
                cdDetailsList.clear();
                message = (String) TRMBO.deleterecord(timeShiftAttr1) + " " + message;

                context.addMessage(null, new FacesMessage(message));
                message1 = "";
                if (message.contains("Master Record Has Been deleted")) {
                    cdList.remove(timeShiftAttr1);

                }
            }
        } else {
            cdList.remove(timeShiftAttr1);
            message = "Master Record Has Been Removed";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
            message1 = "";
        }
    }

    public void saveAllMaster() throws Throwable {
        message = (String) TRMBO.save(cdList);
        message = "Records are Updated";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));

    }

    public void onRowSelect1(SelectEvent e) throws Throwable {
        selectedMT = DTOFactory.createtransactionresponsemasterDTO();
        selectedMT = (transactionresponsemasterDTOInter) e.getObject();
        cdDetailsList = (List<transactionresponsecodeDTOInter>) TRCBO.GetListOfRecords(selectedMT);
        message = "";
        message1 = "";
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void onRowUnSelect1(UnselectEvent e) throws Throwable {
        cdDetailsList = null;
        message = "";
        message1 = "";
        showAdd1 = false;
        showConfirm1 = false;
    }

    public void addTimeShift1() {
        newRecord1 = DTOFactory.createtransactionresponsecodeDTO();
        cdDetailsList.add(0, newRecord1);
        showAdd1 = false;
        showConfirm1 = true;
        message1 = "";
        message = "";
    }

    public void confirmAdd1() throws Throwable {
        newRecord1.setmastercode(selectedMT.getid());
        message1 = (String) TRCBO.insertrecord(newRecord1);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
        if (message1.contains("Record Has Been Inserted")) {
            showAdd1 = true;
            showConfirm1 = false;
            message = "";
//            super.RefreshResponceDetail();
        }
    }

    public void fillDelete1(ActionEvent e) {
        timeShiftDetailsAttr1 = (transactionresponsecodeDTOInter) e.getComponent().getAttributes().get("removedRow1");
        message1 = "";
        message = "";
    }

    public void deleteRecord1() throws Throwable {
        if (timeShiftDetailsAttr1.getid() != 0) {
            message1 = (String) TRCBO.deleterecord(timeShiftDetailsAttr1);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message1));
            if (message1.contains("Record Has Been deleted")) {
                cdDetailsList.remove(timeShiftDetailsAttr1);
                showAdd1 = true;
                showConfirm1 = false;
                message = "";
                //super.RefreshResponceDetail();
            }
        } else {
            cdDetailsList.remove(timeShiftDetailsAttr1);
            message1 = "Detail Record Has Been Removed";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message1));
            message = "";
        }

    }

    public void saveAllDetail() throws Throwable {
        message1 = (String) TRCBO.save(cdDetailsList);
        message = "Records are Updated";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
        //super.RefreshResponceDetail();
    }
}
