/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.filecolumndefinitionBOInter;
import com.ev.Bingo.bus.bo.matchingerrordataBOInter;
import com.ev.Bingo.bus.bo.networksBOInter;
import com.ev.Bingo.bus.bo.transsearchBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.currencymasterDTOInter;
import com.ev.Bingo.bus.dto.disputesDTOInter;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import com.ev.Bingo.bus.dto.matchingerrorsdataDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import org.primefaces.component.datatable.DataTable;

import org.primefaces.event.SelectEvent;

/**
 *
 * @author Aly
 */
public class matchingerrordataClient extends BaseBean {

    private List<filecolumndefinitionDTOInter> columnslist, selectedColoumns;
    private Date fromDate, toDate;
    private String[] selectedCol;
    private List<networksDTOInter> networklist;
    private networksBOInter networkEngin;
    public matchingerrorsdataDTOInter selectederror;
    public disputesDTOInter selecteddispute;
    public filecolumndefinitionBOInter FCDEngin;
    public List<matchingerrorsdataDTOInter> errorrecordslist;
    public List<filecolumndefinitionDTOInter> columns;
    public List<disputesDTOInter> disputerecordslist;
    public matchingerrordataBOInter BOEngin;
    public String message;
    public Integer network, recordtypeid;
    public boolean matchedFlag, disputeFlag;
    public usersDTOInter UserDTO;
    private transsearchBOInter TransactionBO;

    public Integer getRecordtypeid() {
        return recordtypeid;
    }

    public void setRecordtypeid(Integer recordtypeid) {
        this.recordtypeid = recordtypeid;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public List<filecolumndefinitionDTOInter> getColumns() {
        return columns;
    }

    public void setColumns(List<filecolumndefinitionDTOInter> columns) {
        this.columns = columns;
    }

    public matchingerrorsdataDTOInter getSelectederror() {
        return selectederror;
    }

    public void setSelectederror(matchingerrorsdataDTOInter selectederror) {
        this.selectederror = selectederror;
    }

    public disputesDTOInter getSelecteddispute() {
        return selecteddispute;
    }

    public void setSelecteddispute(disputesDTOInter selecteddispute) {
        this.selecteddispute = selecteddispute;
    }

    public List<matchingerrorsdataDTOInter> getErrorrecordslist() {
        return errorrecordslist;
    }

    public void setErrorrecordslist(List<matchingerrorsdataDTOInter> errorrecordslist) {
        this.errorrecordslist = errorrecordslist;
    }

    public List<disputesDTOInter> getDisputerecordslist() {
        return disputerecordslist;
    }

    public void setDisputerecordslist(List<disputesDTOInter> disputerecordslist) {
        this.disputerecordslist = disputerecordslist;
    }

    public matchingerrordataBOInter getBOEngin() {
        return BOEngin;
    }

    public void setBOEngin(matchingerrordataBOInter BOEngin) {
        this.BOEngin = BOEngin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isMatchedFlag() {
        return matchedFlag;
    }

    public void setMatchedFlag(boolean matchedFlag) {
        this.matchedFlag = matchedFlag;
    }

    public boolean isDisputeFlag() {
        return disputeFlag;
    }

    public void setDisputeFlag(boolean disputeFlag) {
        this.disputeFlag = disputeFlag;
    }

    public List<filecolumndefinitionDTOInter> getColumnslist() {
        return columnslist;
    }

    public void setColumnslist(List<filecolumndefinitionDTOInter> columnslist) {
        this.columnslist = columnslist;
    }

    public List<filecolumndefinitionDTOInter> getSelectedColoumns() {
        return selectedColoumns;
    }

    public void setSelectedColoumns(List<filecolumndefinitionDTOInter> selectedColoumns) {
        this.selectedColoumns = selectedColoumns;
    }

    public String[] getSelectedCol() {
        return selectedCol;
    }

    public void setSelectedCol(String[] selectedCol) {
        this.selectedCol = selectedCol;
    }

    public filecolumndefinitionBOInter getFCDEngin() {
        return FCDEngin;
    }

    public void setFCDEngin(filecolumndefinitionBOInter FCDEngin) {
        this.FCDEngin = FCDEngin;
    }

    public usersDTOInter getUserDTO() {
        return UserDTO;
    }

    public void setUserDTO(usersDTOInter UserDTO) {
        this.UserDTO = UserDTO;
    }

    public transsearchBOInter getTransactionBO() {
        return TransactionBO;
    }

    public void setTransactionBO(transsearchBOInter TransactionBO) {
        this.TransactionBO = TransactionBO;
    }

    public List<networksDTOInter> getNetworklist() {
        return networklist;
    }

    public void setNetworklist(List<networksDTOInter> networklist) {
        this.networklist = networklist;
    }

    public networksBOInter getNetworkEngin() {
        return networkEngin;
    }

    public void setNetworkEngin(networksBOInter networkEngin) {
        this.networkEngin = networkEngin;
    }

    public Integer getNetwork() {
        return network;
    }

    public void setNetwork(Integer network) {
        this.network = network;
    }

    public matchingerrordataClient() throws Throwable {
        super();
        GetAccess();
        BOEngin = BOFactory.creatematchingerrordataBO();
        TransactionBO = BOFactory.createtranssearchBO();
        FCDEngin = BOFactory.createfilecolumndefinitionBO();
        selectedColoumns = new ArrayList<filecolumndefinitionDTOInter>();
        columnslist = (List<filecolumndefinitionDTOInter>) FCDEngin.GetAllRecordsVIEW(999);
        selectedCol = colselected(columnslist);
        UserDTO = (usersDTOInter) super.GetUser();
        matchedFlag = Boolean.TRUE;
        disputeFlag = Boolean.TRUE;
        selecteddispute = DTOFactory.createdisputesDTO();
        selectederror = DTOFactory.creatematchingerrorsdataDTO();
        networkEngin = BOFactory.createnetworksBO();
        networklist = (List<networksDTOInter>) networkEngin.GetAllUserRecords("" + super.GetUser().getUserid());
        disputerecordslist = new ArrayList<disputesDTOInter>();
        recordtypeid = 0;
        network = 0;

    }

    public void viewcol(ValueChangeEvent e) throws Throwable {
        Integer value = Integer.valueOf(e.getNewValue().toString());
        if (value == 0) {
            value = 999;
        }
        columnslist = (List<filecolumndefinitionDTOInter>) FCDEngin.GetAllRecordsVIEW(value);
        selectedCol = colselected(columnslist);
    }

    public void fillerror(SelectEvent e) throws Throwable {
        selectederror = (matchingerrorsdataDTOInter) e.getObject();
        disputerecordslist = (List<disputesDTOInter>) BOEngin.GetListOfRecords(selectederror);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:mach_typ_dt"))).setFirst(0);
    }

    public void filldispute(SelectEvent e) {

        selecteddispute = (disputesDTOInter) e.getObject();
    }

    public void markAsMatched() throws Throwable {
        if (selectederror == null && selecteddispute == null) {
            message = "You have to Select Records First";
        } else {
            if (selecteddispute != null) {
                if (selectederror != null) {
                    BOEngin.MarkAsMatched(selectederror, selecteddispute);
                    errorrecordslist = (List<matchingerrorsdataDTOInter>) BOEngin.GetAllRecords(UserDTO, fromDate, toDate, network, recordtypeid);
                    errorrecordslist.remove(selectederror);
                    disputerecordslist.clear();
                    selecteddispute = null;
                    selectederror = null;
                    message = "Done Succesfully";

                } else {
                    message = "You Have To Select a Record From Suggested Matches";
                }
            } else {
                message = "You Have To Select a Record From Suggested Matches";
            }
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void markAsDispute() throws Throwable {
        if (selectederror == null && selecteddispute == null) {
            message = "You have to Select Records First";
        } else {
            if (selectederror != null) {
                BOEngin.MarkAsDispute(selectederror, selecteddispute, UserDTO);
                errorrecordslist.remove(selectederror);
                disputerecordslist.clear();
                selecteddispute = null;
                selectederror = null;
                message = "Done Succesfully";
            } else {
                message = "You Have To Select a Record From Suggested Matches";
            }
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public String[] colselected(List<filecolumndefinitionDTOInter> col) {
        List<String> columns = new ArrayList<String>();
        String[] result;
        for (int i = 0; i < col.size(); i++) {
            filecolumndefinitionDTOInter item = col.get(i);
            if (item.getselected() == 1) {
                columns.add(item.getcolumnname());
            }
        }
        result = new String[columns.size()];
        for (int z = 0; z < columns.size(); z++) {
            result[z] = columns.get(z);
        }
        return result;
    }

    public void doSearch() throws Throwable {
        selectedColoumns = new ArrayList<filecolumndefinitionDTOInter>();
        selectedColoumns = TransactionBO.getcolumnselect(selectedCol, columnslist);
        errorrecordslist = (List<matchingerrorsdataDTOInter>) BOEngin.GetAllRecords(UserDTO, fromDate, toDate, network, recordtypeid);
        selectederror = null;
        disputerecordslist.clear();
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:mat_typ_dt"))).setFirst(0);

    }
}
