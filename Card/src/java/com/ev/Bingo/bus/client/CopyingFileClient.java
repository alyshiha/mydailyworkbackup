/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.copyfilesBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.copyfilesDTOInter;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Administrator
 */
public class CopyingFileClient extends BaseBean {

    private copyfilesBOInter cfObject;
    private copyfilesDTOInter cfUpdateAttr;
    private copyfilesDTOInter cfDeleteAttr;
    private copyfilesDTOInter newCF;
    private List<copyfilesDTOInter> cfList;
    private String message;
    private String oldSource;
    private Boolean showAdd;
    private Boolean showConfirm;
    private Integer listSize;

    public copyfilesDTOInter getCfDeleteAttr() {
        return cfDeleteAttr;
    }

    public void setCfDeleteAttr(copyfilesDTOInter cfDeleteAttr) {
        this.cfDeleteAttr = cfDeleteAttr;
    }

    public List<copyfilesDTOInter> getCfList() {
        return cfList;
    }

    public void setCfList(List<copyfilesDTOInter> cfList) {
        this.cfList = cfList;
    }

    public copyfilesBOInter getCfObject() {
        return cfObject;
    }

    public void setCfObject(copyfilesBOInter cfObject) {
        this.cfObject = cfObject;
    }

    public copyfilesDTOInter getCfUpdateAttr() {
        return cfUpdateAttr;
    }

    public void setCfUpdateAttr(copyfilesDTOInter cfUpdateAttr) {
        this.cfUpdateAttr = cfUpdateAttr;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public copyfilesDTOInter getNewCF() {
        return newCF;
    }

    public void setNewCF(copyfilesDTOInter newCF) {
        this.newCF = newCF;
    }

    public String getOldSource() {
        return oldSource;
    }

    public void setOldSource(String oldSource) {
        this.oldSource = oldSource;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public CopyingFileClient() throws Throwable {
        super();
        GetAccess();
        cfObject = BOFactory.createcopyfilesBO();
        cfList = (List<copyfilesDTOInter>) cfObject.GetAllRecords();
        showAdd = true;
        showConfirm = false;
        listSize = cfList.size();
    }

    public void fillDelete(ActionEvent e) {
        cfDeleteAttr = (copyfilesDTOInter) e.getComponent().getAttributes().get("removedRow");
    }

    public void deleteRecord() throws Throwable {
        message = (String) cfObject.deleterecord(cfDeleteAttr);
        if (!message.contains("Row Has Been deleted")) {
            message = "Record Has been removed";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
        cfList.remove(cfDeleteAttr);
        showAdd = true;
        showConfirm = false;
    }

    public void addRecord() {
        if (cfList == null) {
            cfList = new ArrayList<copyfilesDTOInter>();
        }
        newCF = DTOFactory.createcopyfilesDTO();
        cfList.add(newCF);
        showAdd = false;
        showConfirm = true;
    }

    public void confirmRecord() throws Throwable {
        message = (String) cfObject.insertrecord(newCF);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
        if (message.contains("Record Has Been Inserted")) {
            showAdd = true;
            showConfirm = false;
        }
    }

    public void saveAll() throws Throwable {
        if (showConfirm == false) {
            message = (String) cfObject.save(cfList);
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } else {
            message = "Please Confirm First";
            super.showMessage(message);
        }
    }
}
