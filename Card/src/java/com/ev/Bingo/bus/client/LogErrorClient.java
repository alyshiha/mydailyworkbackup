/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.LogErrorBOInter;
import com.ev.Bingo.bus.dto.ExceptionLoadingErrorsDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author AlyShiha
 */
@ManagedBean(name = "LogView")
@ViewScoped
public class LogErrorClient extends BaseBean {

    private Date fromDate, toDate;
    private List<ExceptionLoadingErrorsDTOInter> exceptionLoadingErrorsList;
    private String message, userName;
    private LogErrorBOInter logerrorEngin;
    private DefaultStreamedContent download;

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public List<ExceptionLoadingErrorsDTOInter> getExceptionLoadingErrorsList() {
        return exceptionLoadingErrorsList;
    }

    public void setExceptionLoadingErrorsList(List<ExceptionLoadingErrorsDTOInter> exceptionLoadingErrorsList) {
        this.exceptionLoadingErrorsList = exceptionLoadingErrorsList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public LogErrorBOInter getLogerrorEngin() {
        return logerrorEngin;
    }

    public void setLogerrorEngin(LogErrorBOInter logerrorEngin) {
        this.logerrorEngin = logerrorEngin;
    }

    @PostConstruct
    public void init() {
        try {
            toDate = super.getYesterdaytrans((String) "00:00:00");
            fromDate = super.getTodaytrans((String) "00:00:00");
            logerrorEngin = BOFactory.createLogErrorBO();
            userName = super.GetUser().getLogonname();
        } catch (Throwable ex) {
            Logger.getLogger(LogErrorClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printExceptionLoadingErrorsexcel() {
        try {
            if (fromDate == null || toDate == null) {
                message = "Please Insert Required Dates";
            } else {
                message = "";
                exceptionLoadingErrorsList = (List<ExceptionLoadingErrorsDTOInter>) logerrorEngin.runReportExceptionLoadingErrorsexcel(DateFormatter.changeDateAndTimeFormat(fromDate), DateFormatter.changeDateAndTimeFormat(toDate));
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

    public void printExceptionLoadingErrorspdf() {
        try {
            if (fromDate == null || toDate == null) {
                message = "Please Insert Required Dates";
            } else {
                message = "";
                String path = (String) logerrorEngin.printExceptionLoadingErrors(userName, fromDate, toDate, super.GetClient());
                prepDownload(path);
                message = "Done Succesfully";
            }
        } catch (Throwable ex) {
            message = ex.getMessage();
        }
    }

}
