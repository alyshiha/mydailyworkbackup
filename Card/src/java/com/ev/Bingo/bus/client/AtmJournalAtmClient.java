/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.base.util.PropertyReader;
import com.ev.Bingo.bus.bo.ATMMACHINEBOInter;
import com.ev.Bingo.bus.bo.AtmJournalBOInter;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.currencymasterBOInter;
import com.ev.Bingo.bus.bo.systemtableBOInter;
import com.ev.Bingo.bus.dto.ATMMACHINECASHDTOINTER;
import com.ev.Bingo.bus.dto.AtmGroupDTOInter;
import com.ev.Bingo.bus.dto.AtmJournalDTOInter;
import com.ev.Bingo.bus.dto.AtmMachineDTOInter;
import com.ev.Bingo.bus.dto.currencymasterDTOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.systemtableDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author AlyShiha
 */
@ManagedBean(name = "AtmJournalAtmView")
@ViewScoped
public class AtmJournalAtmClient extends BaseBean implements Serializable {

    private Date selectedDate;
    private Date datefrom;
    private Date dateto;
    private String atmid;
    private int atmgroup;
    private usersDTOInter user;
    private List<ATMMACHINECASHDTOINTER> atmrecordsList;
    private ATMMACHINEBOInter atmengin;
    private AtmJournalBOInter engin;
    private Boolean export;
    private int listcount;
    private String type, indication;
    private List<AtmJournalDTOInter> operationselectedRecord;
    private List<currencymasterDTOInter> currencyList;
    private currencymasterBOInter cdObject;
    private AtmJournalDTOInter[] selectedRecord;
    private List<AtmJournalDTOInter> recordsList;
    private List<String> privelageList;
    private String excelcoloumns[] = {"ATM ID", "TRANSACTION.TYPE", "DEBIT.ACCT.NO", "DEBIT.CURRENCY", "DEBIT.AMOUNT", "DEBIT.VALUE.DATE", "CREDIT.VALUE.DATE", "DEBIT.THEIR.REF", "CREDIT.ACC.NO", "ORDERING.BANK", "PROFIT.CENTER.DEPT", "COMMISION.CODE", "COMPANY"};
    private Boolean realesed;

    public Date getDatefrom() {
        return datefrom;
    }

    public String getIndication() {
        return indication;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    public void setDatefrom(Date datefrom) {
        this.datefrom = datefrom;
    }

    public Date getDateto() {
        return dateto;
    }

    public Date getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(Date selectedDate) {
        this.selectedDate = selectedDate;
    }

    public void setDateto(Date dateto) {
        this.dateto = dateto;
    }

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    public int getAtmgroup() {
        return atmgroup;
    }

    public void setAtmgroup(int atmgroup) {
        this.atmgroup = atmgroup;
    }

    public usersDTOInter getUser() {
        return user;
    }

    public void setUser(usersDTOInter user) {
        this.user = user;
    }

    public List<ATMMACHINECASHDTOINTER> getAtmrecordsList() {
        return atmrecordsList;
    }

    public void setAtmrecordsList(List<ATMMACHINECASHDTOINTER> atmrecordsList) {
        this.atmrecordsList = atmrecordsList;
    }

    public ATMMACHINEBOInter getAtmengin() {
        return atmengin;
    }

    public void setAtmengin(ATMMACHINEBOInter atmengin) {
        this.atmengin = atmengin;
    }

    public AtmJournalBOInter getEngin() {
        return engin;
    }

    public void setEngin(AtmJournalBOInter engin) {
        this.engin = engin;
    }

    public Boolean getExport() {
        return export;
    }

    public void setExport(Boolean export) {
        this.export = export;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<AtmJournalDTOInter> getOperationselectedRecord() {
        return operationselectedRecord;
    }

    public void setOperationselectedRecord(List<AtmJournalDTOInter> operationselectedRecord) {
        this.operationselectedRecord = operationselectedRecord;
    }

    public List<currencymasterDTOInter> getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(List<currencymasterDTOInter> currencyList) {
        this.currencyList = currencyList;
    }

    public currencymasterBOInter getCdObject() {
        return cdObject;
    }

    public void setCdObject(currencymasterBOInter cdObject) {
        this.cdObject = cdObject;
    }

    public AtmJournalDTOInter[] getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(AtmJournalDTOInter[] selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public List<AtmJournalDTOInter> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<AtmJournalDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public List<String> getPrivelageList() {
        return privelageList;
    }

    public void setPrivelageList(List<String> privelageList) {
        this.privelageList = privelageList;
    }

    public Boolean getRealesed() {
        return realesed;
    }

    public void setRealesed(Boolean realesed) {
        this.realesed = realesed;
    }

    public String[] getExcelcoloumns() {
        return excelcoloumns;
    }

    public void setExcelcoloumns(String[] excelcoloumns) {
        this.excelcoloumns = excelcoloumns;
    }

    @PostConstruct
    public void init() {

        try {
            atmid = "All";
            if (isSunday()) {
                dateto = new Date();
                datefrom = super.getTodaytrans((String) "00:00:00");
            } else {
                datefrom = super.getYesterdaytrans((String) "00:00:00");
                dateto = new Date();

            }
            selectedDate = super.getBeforYesterdaytrans((String) "00:00:00");
            user = super.GetUser();
            atmgroup = 0;
            realesed = Boolean.FALSE;
            cdObject = BOFactory.createcurrencymasterBO();
            atmengin = BOFactory.createATMMACHINEBO();
            atmrecordsList = (List<ATMMACHINECASHDTOINTER>) atmengin.findAll();
            currencyList = (List<currencymasterDTOInter>) cdObject.GetAllRecords();
            engin = BOFactory.createAtmJournalBO(null);
            export = Boolean.TRUE;
            type = "0";
            indication = "all";
            recordsList = new ArrayList<AtmJournalDTOInter>();
            privelageList = super.findPrivelage(super.GetUser().getUserid(), "AtmJournalAtm.xhtml");
            if (super.findPrivelageOperation(privelageList, "VALIDATE")) {
                recordsList = engin.findjournalatm(DateFormatter.changeDateFormat(datefrom), DateFormatter.changeDateFormat(dateto), atmid, 1, atmgroup, type, super.GetUser().getUserid(), indication, realesed);
            }
            selectAllRecord();
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(AtmJournalAtmClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Boolean markPriv() {
        if (super.findPrivelageOperation(privelageList, "CHECK")) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    private void selectAllRecord() {
        DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("form:singleDT2");
        repTable.setSelection(recordsList.toArray());
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public Boolean releasedPriv() {
        return super.findPrivelageOperation(privelageList, "RELEASE");
    }

    public void recalc() {
        try {
            engin.recalc(selectedDate);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Has Been Calculate Succesfully"));
        } catch (Throwable ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void recalc2() {
        try {
            engin.recalc2();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Has Been Calculate Succesfully"));
        } catch (Throwable ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void searchrecords() {
        try {
            selectedRecord = null;
            recordsList = engin.findjournalatm(DateFormatter.changeDateFormat(datefrom), DateFormatter.changeDateFormat(dateto), atmid, 2, atmgroup, type, super.GetUser().getUserid(), indication, realesed);
            selectAllRecord();
            listcount = recordsList.size();
        } catch (Throwable ex) {
        }
    }

    public String getATMName(Integer ID) {
        for (ATMMACHINECASHDTOINTER atmrecord : atmrecordsList) {
            if (atmrecord.getId() == ID) {
                return atmrecord.getAtmname();
            }
        }
        return "UnAssigned";
    }

    public String getCurrencyName(Integer ID) {
        for (currencymasterDTOInter currencyrecord : currencyList) {
            if (currencyrecord.getid() == ID) {
                return currencyrecord.getsymbol();
            }
        }
        return "UnAssigned";
    }

    public String exportexcel(String[] coloumns, List<AtmJournalDTOInter> Records) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("ATM Journal");
        Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
        Object[] coloumnslist = new Object[coloumns.length];
        int index = 0;
        for (String coloumn : coloumns) {
            coloumnslist[index] = coloumn;
            sheet.setDefaultColumnWidth(20);
            index = index + 1;
        }
        data.put(1, coloumnslist);
        index = 2;
        for (AtmJournalDTOInter Record : Records) {
            Object[] coloumnsData = new Object[coloumns.length];
            for (int i = 0; i < coloumns.length; i++) {
                if ("ATM ID".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getAtmid());
                }
                if ("TRANSACTION.TYPE".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf("AC");
                }
                if ("DEBIT.ACCT.NO".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getDebitaccount());
                }
                if ("DEBIT.CURRENCY".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(getCurrencyName(Record.getCurrency()));
                }
                if ("DEBIT.AMOUNT".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getAmount());
                }
                if ("DEBIT.VALUE.DATE".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getJournaldate());
                }
                if ("CREDIT.VALUE.DATE".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getJournaldate());
                }
                if ("DEBIT.THEIR.REF".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getJournalrefernce());
                }

                if ("CREDIT.ACC.NO".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getCreditaccount());
                }
                if ("ORDERING.BANK".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf("UNBE");
                }
                if ("PROFIT.CENTER.DEPT".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf("11100000");
                }
                if ("COMMISION.CODE".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf("WAIVE");
                }
                if ("COMPANY".equals(coloumns[i].toUpperCase())) {
                    coloumnsData[i] = String.valueOf(Record.getBranch());
                }

            }
            data.put(index, coloumnsData);
            index = index + 1;
        }
        Set<Integer> keyset = data.keySet();
        int rownum = 0;
        for (Integer key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            Calendar c = Calendar.getInstance();
            String uri = "Journal123Visa" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".xls";
            String pdfPath = PropertyReader.getProperty("ev.installation.path.pdf");
            pdfPath = pdfPath + uri;
            FileOutputStream out = new FileOutputStream(new File(pdfPath));
            workbook.write(out);
            out.close();
            return pdfPath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Boolean confirmPriv() {
        return super.findPrivelageOperation(privelageList, "VALIDATE");
    }

    public void confirmTransaction() throws Throwable {
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord.length != 0) {
            for (AtmJournalDTOInter record : selectedRecord) {
                if (record.getExportflag() == 1) {
                    record.setExportflag(2);
                    operationselectedRecord.add(record);
                }
            }
            engin.updateJournal(operationselectedRecord, "2");
            selectedRecord = null;

            listcount = recordsList.size();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Has Been Confirmed Succesfully"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Confirm"));
        }

    }

    public void cancelTransaction() throws Throwable {
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord.length != 0) {
            for (AtmJournalDTOInter record : selectedRecord) {
                record.setExportflag(1);
                operationselectedRecord.add(record);
            }
            engin.updateJournal(operationselectedRecord, "1");
            selectedRecord = null;

            listcount = recordsList.size();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Has Been Cancel Succesfully"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Cancel"));
        }

    }

    public Boolean datesPriv() {
        if (super.findPrivelageOperation(privelageList, "SEARCH_DATE")) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    public Boolean exportPriv() {
        return super.findPrivelageOperation(privelageList, "EXCUTE");
    }

    public Boolean reexportPriv() {
        return super.findPrivelageOperation(privelageList, "REEXECUTE");
    }

    public void exportCSV() throws Throwable {
        Boolean flag = false;
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord.length != 0) {

            for (AtmJournalDTOInter record : selectedRecord) {

                if (record.getExportflag() == 2) {
                    record.setExportflag(3);
                    operationselectedRecord.add(record);
                    flag = true;
                }
            }
            engin.updateJournal(operationselectedRecord, "3");
            String path = writeCSV(operationselectedRecord);
            // prepDownload(path);
            int len = selectedRecord.length;
            selectedRecord = null;

            listcount = recordsList.size();
            if (flag) {
                if (operationselectedRecord.size() == len) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Journal Has Been Exported Successfully"));
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Some records can`t be excuted"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Some records can`t be excuted"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Export"));

        }

    }

    public void reexportCSV() throws Throwable {
        Boolean flag = Boolean.FALSE;
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord.length != 0) {

            for (AtmJournalDTOInter record : selectedRecord) {

                if (record.getExportflag() == 3 || record.getExportflag() == 4) {
                    record.setExportflag(4);
                    operationselectedRecord.add(record);
                    flag = Boolean.TRUE;
                }
            }
            engin.updateJournal(operationselectedRecord, "4");
            String path = writeCSV(operationselectedRecord);

//prepDownload(path);
            int len = selectedRecord.length;
            selectedRecord = null;

            listcount = recordsList.size();
            if (flag) {
                if (operationselectedRecord.size() == len) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Journal Has Been Exported Successfully"));
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Some records can`t be excuted"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Execute First"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Confirm Record To Export"));

        }

    }

    public void exportexcel() throws Throwable {
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        List<AtmJournalDTOInter> exportRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord.length != 0) {

            for (AtmJournalDTOInter record : selectedRecord) {
                operationselectedRecord.add(record);
                exportRecord.add(record);
            }
            String path = exportexcel(excelcoloumns, exportRecord);
            prepDownload(path);
            selectedRecord = null;
            listcount = recordsList.size();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Report Has Been Exported Succesfully"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Confirm Record To Export"));
        }

    }

    //write from duplicate array of courses to a CSV file
    public String thousandSeparator(int amount) {
        return String.format("%,d", amount);
    }

    public String writeCSV(List<AtmJournalDTOInter> Records) throws Exception, Throwable {
        systemtableBOInter systemtableengin;
        systemtableengin = BOFactory.createsystemtableBO();
        Calendar c = Calendar.getInstance();
        String uri = "\\AtmJournalatm" + DateFormatter.changeDateAndTimeFormatForReport(c.getTime()) + ".Csv";
        String pdfPath = (String) ((systemtableDTOInter) systemtableengin.GetExportPath("JOURNAL_EXPORT_PATH")).getparametervalue();
        pdfPath = pdfPath + uri;
        //create a File class object and give the file the name employees.csv
        java.io.File courseCSV = new java.io.File(pdfPath);

        //Create a Printwriter text output stream and link it to the CSV File
        java.io.PrintWriter outfile = new java.io.PrintWriter(courseCSV);

        //Iterate the elements actually being used
        for (AtmJournalDTOInter Record : Records) {
            outfile.write(Record.toCSVString(","));
        }//end for

        outfile.close();
        return pdfPath;
    } //end writeCSV()

    public String getNowDate() {
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return sdf.format(date);
    }

    public Boolean isSunday() {
        // Get calendar set to current date and time
        Calendar c = Calendar.getInstance();
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public String GetStatusField(int status) {
        String statusString;
        switch (status) {
            case 1:
                statusString = "Pending";
                break;
            case 2:
                statusString = "Validated";
                break;
            case 3:
                statusString = "Executed";
                break;
            case 4:
                statusString = "REExecuted";
                break;
            case 8:
                statusString = "Released";
                break;
            default:
                statusString = "UnKnown";
                break;
        }
        return statusString;
    }

    public void printreportpdf() {
        try {
            if (datefrom == null || dateto == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Date From & Date To Is A Required Fields"));
            } else {
                String path = (String) engin.printrep8(super.GetUser().getUsername(), datefrom, dateto, atmgroup, super.GetClient(), atmid.toString(), atmid, indication, realesed);
                prepDownload(path);
            }
        } catch (Throwable ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", ex.getMessage()));
        }
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void releaseTransaction() throws Throwable {
        operationselectedRecord = new ArrayList<AtmJournalDTOInter>();
        if (selectedRecord.length != 0) {
            for (AtmJournalDTOInter record : selectedRecord) {
                record.setExportflag(8);
                operationselectedRecord.add(record);
            }
            engin.updateJournal(operationselectedRecord, "8");
            selectedRecord = null;

            listcount = recordsList.size();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Records Has Been Released Succesfully"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Please Select Record To Release"));
        }

    }

}
