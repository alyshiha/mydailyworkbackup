/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.usersBOInter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Aly
 */
public class themeClient {

    public static final String[] POSSIBLE_THEMES
            = {"afterdark", "afternoon", "afterwork", "aristo",
                "black-tie", "blitzer", "bluesky", "casablanca",
                "cruze", "cupertino", "dark-hive", "dot-luv",
                "eggplant", "excite-bike", "flick", "glass-x",
                "home", "hot-sneaks", "humanity", "le-frog",
                "midnight", "mint-choc", "overcast", "pepper-grinder",
                "redmond", "rocket", "sam", "smoothness",
                "south-street", "start", "sunny", "swanky-purse",
                "trontastic", "twitter bootstrap", "ui-darkness",
                "ui-lightness", "vader"};

    public String[] getThemes() {
        return (POSSIBLE_THEMES);
    }


    public void Settheme(String deftheme) throws Throwable {
        usersBOInter ubo = BOFactory.createusersBO();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.setAttribute("theme", deftheme);
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String Gettheme() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        if (session.getAttribute("theme") == null) {
            theme = "flick";
            return theme;
        } else {
            theme = session.getAttribute("theme").toString();
            return theme;
        }
    }
    private String theme;

   

    public themeClient() {
        Gettheme();
    }

}
