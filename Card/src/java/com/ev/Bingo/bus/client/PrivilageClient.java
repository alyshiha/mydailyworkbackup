/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;

import com.ev.Bingo.bus.bo.profileBOInter;
import com.ev.Bingo.bus.bo.profilemenuBOInter;
import com.ev.Bingo.bus.bo.usersBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.itembingoDTOInter;
import com.ev.Bingo.bus.dto.menulabelsDTOInter;
import com.ev.Bingo.bus.dto.profileDTOInter;
import com.ev.Bingo.bus.dto.profilemenuDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author ISLAM
 */
public class PrivilageClient extends BaseBean {

    private List<profileDTOInter> profileList;
    private profileDTOInter selectedProfile, upAttr, delAttr, newProfile;
    private List<menulabelsDTOInter> profileMenuList;
    private List<itembingoDTOInter> profileItemsList, currentItemModes;
    private itembingoDTOInter selectedItem;
    private menulabelsDTOInter selectedMenu, delItem;
    private profilemenuBOInter ProfileMenuBBO;
    private profilemenuDTOInter ProfMenuDTO;
    private TreeNode profileMenu;
    private String message, message1;
    private Boolean showAdd, showConfirm;
    private profileBOInter profileBO;
    private usersBOInter userBO;

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public List<itembingoDTOInter> getCurrentItemModes() {
        return currentItemModes;
    }

    public void setCurrentItemModes(List<itembingoDTOInter> currentItemModes) {
        this.currentItemModes = currentItemModes;
    }

    public List<itembingoDTOInter> getProfileItemsList() {
        return profileItemsList;
    }

    public void setProfileItemsList(List<itembingoDTOInter> profileItemsList) {
        this.profileItemsList = profileItemsList;
    }

    public itembingoDTOInter getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(itembingoDTOInter selectedItem) {
        this.selectedItem = selectedItem;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public profileDTOInter getNewProfile() {
        return newProfile;
    }

    public void setNewProfile(profileDTOInter newProfile) {
        this.newProfile = newProfile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public profileDTOInter getDelAttr() {
        return delAttr;
    }

    public void setDelAttr(profileDTOInter delAttr) {
        this.delAttr = delAttr;
    }

    public profileDTOInter getUpAttr() {
        return upAttr;
    }

    public void setUpAttr(profileDTOInter upAttr) {
        this.upAttr = upAttr;
    }

    public List<profileDTOInter> getProfileList() {
        return profileList;
    }

    public void setProfileList(List<profileDTOInter> profileList) {
        this.profileList = profileList;
    }

    public TreeNode getProfileMenu() {
        return profileMenu;
    }

    public void setProfileMenu(TreeNode profileMenu) {
        this.profileMenu = profileMenu;
    }

    public List<menulabelsDTOInter> getProfileMenuList() {
        return profileMenuList;
    }

    public void setProfileMenuList(List<menulabelsDTOInter> profileMenuList) {
        this.profileMenuList = profileMenuList;
    }

    public menulabelsDTOInter getSelectedMenu() {
        return selectedMenu;
    }

    public void setSelectedMenu(menulabelsDTOInter selectedMenu) {
        this.selectedMenu = selectedMenu;
    }

    public profileDTOInter getSelectedProfile() {
        return selectedProfile;
    }

    public void setSelectedProfile(profileDTOInter selectedProfile) {
        this.selectedProfile = selectedProfile;
    }

    /**
     * Creates a new instance of PrivilageClient1
     */
    public PrivilageClient() throws Throwable {
        super();
        GetAccess();
        profileBO = BOFactory.createprofileBO();
        ProfileMenuBBO = BOFactory.createprofilemenuBO();
        ProfMenuDTO = DTOFactory.createprofilemenuDTO();
        userBO = BOFactory.createusersBO();

        profileList = (List<profileDTOInter>) profileBO.GetAllRecords();
        selectedProfile = DTOFactory.createprofileDTO();
        profileMenu = new DefaultTreeNode();
        profileMenu = profileBO.getMenuTree();
        showAdd = true;
        showConfirm = false;
    }

    public void onRowSelect1(SelectEvent e) throws Throwable {
        selectedProfile = (profileDTOInter) e.getObject();
        profileMenuList = (List<menulabelsDTOInter>) profileBO.getMlDTOL(selectedProfile.getprofileid());
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:itemTable"))).setFirst(0);
    }

    public void onRowUnSelect1(UnselectEvent e) {
        profileMenuList.clear();
        selectedProfile = null;
    }

    public void onRowSelect2(SelectEvent e) throws Throwable {
    }

    public void onRowUnSelect2(UnselectEvent e) {
    }

    public void addProfile() throws Throwable {

        if (profileList == null) {
            profileList = new ArrayList<profileDTOInter>();
        }
        newProfile = DTOFactory.createprofileDTO();
        profileList.add(0, newProfile);
        showAdd = false;
        showConfirm = true;
    }

    public void addConfirm() throws Throwable {
        message = (String) profileBO.insertrecord(newProfile);
        if (message.contains("Profile Has Been Inserted")) {
            showAdd = true;
            showConfirm = false;
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void fillItemMc(ActionEvent e) {
        delItem = (menulabelsDTOInter) e.getComponent().getAttributes().get("updateRowMenuItem");
    }

    public void fillDel(ActionEvent e) {
        delAttr = (profileDTOInter) e.getComponent().getAttributes().get("removedRow");
    }

    public void deleteProfile(ActionEvent e) throws Throwable {
        if (delAttr.getprofileid() != 0) {
            List<usersDTOInter> HasUsers = (List<usersDTOInter>) userBO.GetAllAssignToProfile(delAttr.getprofileid());
            if (HasUsers.size() > 0) {
                message = "Can't Delete, There Are Users Under This Profile";
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(message));
            } else {

                message = (String) profileBO.deleterecord(delAttr);
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(message));
                if (message.contains("Profile Has Been Deleted")) {
                    profileList.remove(delAttr);
                    showAdd = true;
                    showConfirm = false;
                }
            }
        } else {
            profileList.remove(delAttr);
            showAdd = true;
            showConfirm = false;
            message = "Record Has Been Removed";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }

    public void saveAll() throws Throwable {
        if (showConfirm == false) {
            message = (String) profileBO.save(profileList);

            message = "Records are Updated";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } else {
            message = "please confirm first";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }

    public void doTransferBack() throws Throwable {
        ProfMenuDTO.setmenuid(delItem.getmenuid());
        ProfMenuDTO.setprofileid(selectedProfile.getprofileid());
        message1 = (String) ProfileMenuBBO.deleterecord(ProfMenuDTO);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
        if (message1.contains("Page Has Been UnAssigned")) {
            profileMenuList.remove(delItem);
        }
    }

    public void transferMenu(ActionEvent e) {
        selectedMenu = (menulabelsDTOInter) e.getComponent().getAttributes().get("updateRowMenu");
    }

    public void doTransfer() throws Throwable {
        profilemenuDTOInter holdMenu = DTOFactory.createprofilemenuDTO();
        holdMenu.setmenuid(selectedMenu.getmenuid());
        holdMenu.setprofileid(selectedProfile.getprofileid());
        message1 = (String) profileBO.insertmenuitem(holdMenu);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
        if (message1.contains("Page Has Been Assigned")) {
            profileMenuList.add(0, selectedMenu);
        }
    }
}
