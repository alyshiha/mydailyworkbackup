/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.settlementlogBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.settlementlogDTOInter;
import com.ev.Bingo.bus.dto.settlementlogsearchDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author Administrator
 */
public class SettelmentLogClient extends BaseBean {

    private Date fromDate, toDate;
    private Integer userId, setteldId;
    private settlementlogBOInter bingoBo;
    private List<usersDTOInter> userList;
    private List<settlementlogDTOInter> logList;
    private String message;
    private String cardNo;

    public settlementlogBOInter getBingoBo() {
        return bingoBo;
    }

    public void setBingoBo(settlementlogBOInter bingoBo) {
        this.bingoBo = bingoBo;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public List<settlementlogDTOInter> getLogList() {
        return logList;
    }

    public void setLogList(List<settlementlogDTOInter> logList) {
        this.logList = logList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getSetteldId() {
        return setteldId;
    }

    public void setSetteldId(Integer setteldId) {
        this.setteldId = setteldId;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<usersDTOInter> getUserList() {
        return userList;
    }

    public void setUserList(List<usersDTOInter> userList) {
        this.userList = userList;
    }

    public SettelmentLogClient() throws Throwable {
        GetAccess();
        bingoBo = BOFactory.createsettlementlogBO();
        userList = (List<usersDTOInter>) super.GetUsers();
        logList = new ArrayList<settlementlogDTOInter>();
    }

    public void doSearch() throws Throwable {
        String[] inputs = {cardNo};
        String[] validate = {"UNION", "SELECT", "INSERT", "UPDATE", "DELETE", "DROP", "ALTER", "CREATE"};
        Boolean found = Boolean.FALSE;
        for (String input : inputs) {
            boolean b = Pattern.matches("^[a-zA-Z0-9]*$", input.replace(" ", ""));
            if (b == false) {
                found = Boolean.TRUE;
            }
            if (!"".equals(input)) {
                for (String validate1 : validate) {
                    if (input.toUpperCase().contains(validate1.toUpperCase())) {
                        found = Boolean.TRUE;
                    }
                }
            }
        }
        if (!found) {

            settlementlogsearchDTOInter SearchParameters = DTOFactory.createsettlementlogsearchDTO();
            SearchParameters.setTransactiondatefrom(fromDate);
            SearchParameters.setTransactiondateto(toDate);
            SearchParameters.setSetteledid(setteldId);
            SearchParameters.setUserid(userId);
            SearchParameters.setCardno(cardNo);
            SearchParameters.setUserloged(super.GetUser());
            //fromDate, toDate, userId, setteldId, cardNo, ATMID
            logList = (List<settlementlogDTOInter>) bingoBo.GetListOfRecords(SearchParameters);
            ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:mat_typ_dt"))).setFirst(0);

        } else {
            message = "Please Enter Valid Data";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }
}
