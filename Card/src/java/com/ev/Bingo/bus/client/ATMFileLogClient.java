/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.atmfileBOInter;
import com.ev.Bingo.bus.bo.duplicatetransactionsBOInter;
import com.ev.Bingo.bus.dto.atmfileDTOInter;
import com.ev.Bingo.bus.dto.duplicatetransactionsDTOInter;
import java.util.ArrayList;
import javax.faces.event.ActionEvent;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author Administrator
 */
public class ATMFileLogClient extends BaseBean {

    private atmfileBOInter atmObject;
    private duplicatetransactionsBOInter DupTransObject;
    private List<atmfileDTOInter> atmFileList;
    private atmfileDTOInter atmAttr;
    private List<duplicatetransactionsDTOInter> duplicateList;
    private Integer listSize, fileType, template;
    private Date fromDate, toDate;
    private String message;

    public duplicatetransactionsBOInter getDupTransObject() {
        return DupTransObject;
    }

    public void setDupTransObject(duplicatetransactionsBOInter DupTransObject) {
        this.DupTransObject = DupTransObject;
    }

    public atmfileDTOInter getAtmAttr() {
        return atmAttr;
    }

    public void setAtmAttr(atmfileDTOInter atmAttr) {
        this.atmAttr = atmAttr;
    }

    public List<atmfileDTOInter> getAtmFileList() {
        return atmFileList;
    }

    public void setAtmFileList(List<atmfileDTOInter> atmFileList) {
        this.atmFileList = atmFileList;
    }

    public atmfileBOInter getAtmObject() {
        return atmObject;
    }

    public void setAtmObject(atmfileBOInter atmObject) {
        this.atmObject = atmObject;
    }

    public List<duplicatetransactionsDTOInter> getDuplicateList() {
        return duplicateList;
    }

    public void setDuplicateList(List<duplicatetransactionsDTOInter> duplicateList) {
        this.duplicateList = duplicateList;
    }

    public Integer getFileType() {
        return fileType;
    }

    public void setFileType(Integer fileType) {
        this.fileType = fileType;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Integer getListSize() {
        return listSize;
    }

    public void setListSize(Integer listSize) {
        this.listSize = listSize;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getTemplate() {
        return template;
    }

    public void setTemplate(Integer template) {
        this.template = template;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public ATMFileLogClient() throws Throwable {
        
        super();
        GetAccess();
        atmObject = BOFactory.createatmfileBO();
        DupTransObject = BOFactory.createduplicatetransactionsBO();
        duplicateList = new ArrayList<duplicatetransactionsDTOInter>();
    }

    public void doSearch() throws Throwable {

        atmFileList = (List<atmfileDTOInter>) atmObject.GetListOfRecordsSearch(fromDate, toDate, fileType);
        listSize = atmFileList.size();
        duplicateList.clear();
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:blockReason"))).setFirst(0);
    }

    public void showDuplicatedItem(ActionEvent e) throws Throwable {
        atmAttr = (atmfileDTOInter) e.getComponent().getAttributes().get("duplicatedATM");
        duplicateList = (List<duplicatetransactionsDTOInter>) DupTransObject.GetListOfRecords(atmAttr);
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:duplicated"))).setFirst(0);
    }
}
