/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.cardfiletemplateBOInter;
import com.ev.Bingo.bus.bo.cardfiletemplatedetailBOInter;
import com.ev.Bingo.bus.bo.columnssetupBOInter;
import com.ev.Bingo.bus.bo.filecolumndefinitionBOInter;
import com.ev.Bingo.bus.bo.filetemplateheaderBOInter;
import com.ev.Bingo.bus.bo.networksBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.cardfiletemplateDTOInter;
import com.ev.Bingo.bus.dto.cardfiletemplatedetailDTOInter;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import com.ev.Bingo.bus.dto.filetemplateheaderDTOInter;
import com.ev.Bingo.bus.dto.networkfiletemplateheaderDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.swing.JFileChooser;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author Aly
 */
public class ATMFileTemplateClient extends BaseBean {

    private List<cardfiletemplateDTOInter> templatenameList;
    private cardfiletemplateDTOInter selectedtemplate, Deletetemplate, updatetemplate, newrecord;
    private cardfiletemplateBOInter templateBO;
    private filetemplateheaderBOInter headertemplateBO;
    private String message, message2, ChosenItem;
    private Boolean showAdd, showConfirm, networkflag, showAdd1, showConfirm1;
    private JFileChooser chooser;
    private List<networksDTOInter> networklist;
    private networksBOInter networkEngin;
    private List<filetemplateheaderDTOInter> headertemplateList;
    private networkfiletemplateheaderDTOInter updatetemplateHeader;
    private filetemplateheaderDTOInter newrecordHeader, DeletetemplateHeader;
    private columnssetupBOInter FCDBOEngin;
    private List<filecolumndefinitionDTOInter> columnlist;
    private List<cardfiletemplatedetailDTOInter> filetemplatedetailList;
    private cardfiletemplatedetailDTOInter DeletetemplateDetail;
    private cardfiletemplatedetailBOInter detailtemplateBO;

    public cardfiletemplatedetailDTOInter getDeletetemplateDetail() {
        return DeletetemplateDetail;
    }

    public void setDeletetemplateDetail(cardfiletemplatedetailDTOInter DeletetemplateDetail) {
        this.DeletetemplateDetail = DeletetemplateDetail;
    }

    public cardfiletemplatedetailBOInter getDetailtemplateBO() {
        return detailtemplateBO;
    }

    public void setDetailtemplateBO(cardfiletemplatedetailBOInter detailtemplateBO) {
        this.detailtemplateBO = detailtemplateBO;
    }

    public List<cardfiletemplatedetailDTOInter> getFiletemplatedetailList() {
        return filetemplatedetailList;
    }

    public void setFiletemplatedetailList(List<cardfiletemplatedetailDTOInter> filetemplatedetailList) {
        this.filetemplatedetailList = filetemplatedetailList;
    }

    public filetemplateheaderDTOInter getNewrecordHeader() {
        return newrecordHeader;
    }

    public filetemplateheaderBOInter getHeadertemplateBO() {
        return headertemplateBO;
    }

    public void setHeadertemplateBO(filetemplateheaderBOInter headertemplateBO) {
        this.headertemplateBO = headertemplateBO;
    }

    public columnssetupBOInter getFCDBOEngin() {
        return FCDBOEngin;
    }

    public void setFCDBOEngin(columnssetupBOInter FCDBOEngin) {
        this.FCDBOEngin = FCDBOEngin;
    }

    public List<filecolumndefinitionDTOInter> getColumnlist() {
        return columnlist;
    }

    public void setColumnlist(List<filecolumndefinitionDTOInter> columnlist) {
        this.columnlist = columnlist;
    }

    public void setNewrecordHeader(filetemplateheaderDTOInter newrecordHeader) {
        this.newrecordHeader = newrecordHeader;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public filetemplateheaderDTOInter getDeletetemplateHeader() {
        return DeletetemplateHeader;
    }

    public void setDeletetemplateHeader(filetemplateheaderDTOInter DeletetemplateHeader) {
        this.DeletetemplateHeader = DeletetemplateHeader;
    }

    public networkfiletemplateheaderDTOInter getUpdatetemplateHeader() {
        return updatetemplateHeader;
    }

    public void setUpdatetemplateHeader(networkfiletemplateheaderDTOInter updatetemplateHeader) {
        this.updatetemplateHeader = updatetemplateHeader;
    }

    public List<filetemplateheaderDTOInter> getHeadertemplateList() {
        return headertemplateList;
    }

    public void setHeadertemplateList(List<filetemplateheaderDTOInter> headertemplateList) {
        this.headertemplateList = headertemplateList;
    }

    public cardfiletemplateBOInter getTemplateBO() {
        return templateBO;
    }

    public void setTemplateBO(cardfiletemplateBOInter templateBO) {
        this.templateBO = templateBO;
    }

    public String getChosenItem() {
        return ChosenItem;
    }

    public void setChosenItem(String ChosenItem) {
        this.ChosenItem = ChosenItem;
    }

    public JFileChooser getChooser() {
        return chooser;
    }

    public void setChooser(JFileChooser chooser) {
        this.chooser = chooser;
    }

    public List<cardfiletemplateDTOInter> getTemplatenameList() {
        return templatenameList;
    }

    public void setTemplatenameList(List<cardfiletemplateDTOInter> templatenameList) {
        this.templatenameList = templatenameList;
    }

    public cardfiletemplateDTOInter getSelectedtemplate() {
        return selectedtemplate;
    }

    public void setSelectedtemplate(cardfiletemplateDTOInter selectedtemplate) {
        this.selectedtemplate = selectedtemplate;
    }

    public cardfiletemplateDTOInter getDeletetemplate() {
        return Deletetemplate;
    }

    public void setDeletetemplate(cardfiletemplateDTOInter Deletetemplate) {
        this.Deletetemplate = Deletetemplate;
    }

    public cardfiletemplateDTOInter getUpdatetemplate() {
        return updatetemplate;
    }

    public void setUpdatetemplate(cardfiletemplateDTOInter updatetemplate) {
        this.updatetemplate = updatetemplate;
    }

    public cardfiletemplateDTOInter getNewrecord() {
        return newrecord;
    }

    public void setNewrecord(cardfiletemplateDTOInter newrecord) {
        this.newrecord = newrecord;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage2() {
        return message2;
    }

    public void setMessage2(String message2) {
        this.message2 = message2;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public List<networksDTOInter> getNetworklist() {
        return networklist;
    }

    public void setNetworklist(List<networksDTOInter> networklist) {
        this.networklist = networklist;
    }

    public networksBOInter getNetworkEngin() {
        return networkEngin;
    }

    public void setNetworkEngin(networksBOInter networkEngin) {
        this.networkEngin = networkEngin;
    }

    public Boolean getNetworkflag() {
        return networkflag;
    }

    public void setNetworkflag(Boolean networkflag) {
        this.networkflag = networkflag;
    }

    public ATMFileTemplateClient() throws Throwable {
        GetAccess();
        templateBO = BOFactory.createcardfiletemplateBO();
        FCDBOEngin = BOFactory.createcolumnssetupBO();
        headertemplateBO = BOFactory.createfiletemplateheaderBO();
        detailtemplateBO = BOFactory.createcardfiletemplatedetailBO();
        columnlist = (List<filecolumndefinitionDTOInter>) FCDBOEngin.GetAllRecords();
        templatenameList = new ArrayList<cardfiletemplateDTOInter>();
        filetemplatedetailList = new ArrayList<cardfiletemplatedetailDTOInter>();
        headertemplateList = new ArrayList<filetemplateheaderDTOInter>();
        selectedtemplate = DTOFactory.createcardfiletemplateDTO();
        ChosenItem = "Network";
        templatenameList = (List<cardfiletemplateDTOInter>) templateBO.GetAllRecords(FindTableName(ChosenItem));
        if (templatenameList.size() > 0) {
            selectedtemplate = templatenameList.get(0);
            headertemplateList = (List<filetemplateheaderDTOInter>) headertemplateBO.GetListOfRecords(FindTableName(ChosenItem), selectedtemplate);
            filetemplatedetailList = (List<cardfiletemplatedetailDTOInter>) detailtemplateBO.GetListOfRecords(FindTableName(ChosenItem), selectedtemplate);
        } else {
            selectedtemplate = null;
        }
        networkEngin = BOFactory.createnetworksBO();

        networklist = (List<networksDTOInter>) networkEngin.GetAllUserRecords("" + super.GetUser().getUserid());
        showConfirm = false;
        showAdd = true;
        showConfirm1 = false;
        showAdd1 = true;
        message = "";
        message2 = "";
        networkflag = Boolean.FALSE;
    }

    public String FindTableName(String TabName) {
        String TableName = "";
        if (TabName.equals("Network")) {
            TableName = "NETWORK_FILE_TEMPLATE";
            networkflag = Boolean.FALSE;
        } else if (TabName.equals("Switch")) {
            TableName = "SWITCH_FILE_TEMPLATE";
            networkflag = Boolean.TRUE;
        } else if (TabName.equals("GL")) {
            TableName = "HOST_FILE_TEMPLATE";
            networkflag = Boolean.TRUE;
        }
        return TableName;
    }

    public void recordtypeChange(TabChangeEvent e) throws Throwable {
        ChosenItem = e.getTab().getTitle().toString();
        templatenameList = (List<cardfiletemplateDTOInter>) templateBO.GetAllRecords(FindTableName(ChosenItem));
        selectedtemplate = DTOFactory.createcardfiletemplateDTO();
        Deletetemplate = null;
        updatetemplate = null;
        headertemplateList = null;
        filetemplatedetailList = null;
        if (templatenameList.size() > 0) {
            selectedtemplate = templatenameList.get(0);
            headertemplateList = (List<filetemplateheaderDTOInter>) headertemplateBO.GetListOfRecords(FindTableName(ChosenItem), selectedtemplate);
            filetemplatedetailList = (List<cardfiletemplatedetailDTOInter>) detailtemplateBO.GetListOfRecords(FindTableName(ChosenItem), selectedtemplate);
        } else {
            selectedtemplate = null;
        }
        message = "";
        message2 = "";
    }

    public void templatenameSelect(SelectEvent e) throws Throwable {
        selectedtemplate = (cardfiletemplateDTOInter) e.getObject();
        headertemplateList = (List<filetemplateheaderDTOInter>) headertemplateBO.GetListOfRecords(FindTableName(ChosenItem), selectedtemplate);
        filetemplatedetailList = (List<cardfiletemplatedetailDTOInter>) detailtemplateBO.GetListOfRecords(FindTableName(ChosenItem), selectedtemplate);
        message = "";
        message2 = "";
    }

    public void fillDeleteTemplate(ActionEvent e) {
        Deletetemplate = (cardfiletemplateDTOInter) e.getComponent().getAttributes().get("removedRow");
    }

    public void fillupdateTemplate(ActionEvent e) {
        updatetemplate = (cardfiletemplateDTOInter) e.getComponent().getAttributes().get("updateRow");
    }

    public int FindNetworkID(String TabName) {
        int TableName = 0;
        if (TabName.equals("Network")) {
            TableName = 1;
        } else if (TabName.equals("Switch")) {
            TableName = 2;
        } else if (TabName.equals("GL")) {
            TableName = 3;
        }
        return TableName;
    }

    public String validateintvaluesHeader(filetemplateheaderDTOInter searchfields) {
        String Result = "Valid";
        if (!isintvalue(searchfields.getlength())) {
            Result = Result + "Length ";
        }
        if (!isintvalue(searchfields.getposition())) {
            Result = Result + "Position ";
        }
        if (!"Valid".equals(Result)) {
            Result = Result + " Non Integer Values";
            Result = Result.replace("Valid", "");
        }
        return Result;
    }

    public String validatenullheader(filetemplateheaderDTOInter searchfields) {
        String result = "";
        if (searchfields.getlength() == null || "".equals(searchfields.getlength())) {
            result = result + "Length ";
        }
        if (searchfields.getposition() == null || "".equals(searchfields.getposition())) {
            result = result + "Position ";
        }
        if (result == null || "".equals(result)) {
            result = "Valid";
        } else {
            result = "Please Enter The Required Field(s)";
        }
        return result;
    }

    public String validatenull(cardfiletemplateDTOInter searchfields) {
        String result = "";
        if (searchfields.getname() == null || "".equals(searchfields.getname())) {
            result = result + "Name ";
        }
        if (searchfields.getloadingfolder() == null || "".equals(searchfields.getloadingfolder())) {
            result = result + "Loading Folder ";
        }
        if (searchfields.getbackupfolder() == null || "".equals(searchfields.getbackupfolder())) {
            result = result + "Backup Folder ";
        }
        if (searchfields.getcopyfolder() == null || "".equals(searchfields.getcopyfolder())) {
            result = result + "Copy Folder ";
        }
        if (result == null || "".equals(result)) {
            result = "Valid";
        } else {
            result = "Please Enter The Required Field(s)";
        }
        return result;
    }

    public void addNewTemplate() throws Throwable {
        newrecord = DTOFactory.createcardfiletemplateDTO();
        newrecord.setid(99);
        newrecord.setprocessingtype(1);
        newrecord.setnumberoflines("0");
        newrecord.setignoredlines("0");
        newrecord.setactive(1);
        newrecord.setactiveB(Boolean.TRUE);
        newrecord.setmaxtaglines("0");
        newrecord.settagsendingvalue("");
        templatenameList.add(newrecord);
        showAdd = false;
        showConfirm = true;
        message = "";
        message2 = "";
        selectedtemplate = newrecord;
        filetemplatedetailList = new ArrayList<cardfiletemplatedetailDTOInter>();
        headertemplateList = new ArrayList<filetemplateheaderDTOInter>();
    }

    public void confirmAddTemplate() throws Throwable {
        message = validatenull(newrecord);
        message2 = "";
        if ("Valid".equals(message)) {
            message = validateintvalues(newrecord);
            if (templateBO.GetRecord(newrecord, FindTableName(ChosenItem)) == 0) {
                if ("Valid".equals(message)) {
                    newrecord.setid(templateBO.insertrecord(newrecord, FindTableName(ChosenItem)));
                    selectedtemplate = newrecord;
                    headertemplateList = (List<filetemplateheaderDTOInter>) headertemplateBO.GetListOfRecords(FindTableName(ChosenItem), selectedtemplate);
                    filetemplatedetailList = (List<cardfiletemplatedetailDTOInter>) detailtemplateBO.GetListOfRecords(FindTableName(ChosenItem), selectedtemplate);
                    showConfirm = false;
                    showAdd = true;
                    message = "1 Record has been Inserted";
                }
            } else {
                message = "Please Enter Unique Template Name and Paths";
            }
        }

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void deleteRecordTemplate() throws Throwable {

        message = "";
        message2 = "";
        if (Deletetemplate.getid() != 99) {
            String detailcount = detailtemplateBO.deleteallrecord(selectedtemplate, FindTableName(ChosenItem));
            String headercount = headertemplateBO.deleteallrecord(selectedtemplate, FindTableName(ChosenItem));
            String templatecount = (String) templateBO.deleterecord(Deletetemplate, FindTableName(ChosenItem));
            message = templatecount + " With " + headercount + " Header & " + detailcount + " Detailed Record";
            if (message.contains("Record Has been Deleted")) {
                templatenameList.remove(Deletetemplate);
                headertemplateList.clear();
                filetemplatedetailList.clear();
            }
        } else {
            templatenameList.remove(Deletetemplate);
            headertemplateList.clear();
            filetemplatedetailList.clear();
            showConfirm = false;
            showAdd = true;
            message = "1 Record Has been Removed";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public boolean isintvalue(String s) {
        try {
            if (s == null || "".equals(s)) {
                return true;
            } else {
                Integer.parseInt(s);
            }
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public String validateintvalues(cardfiletemplateDTOInter searchfields) {
        String Result = "Valid";
        if (!isintvalue(searchfields.getnumberoflines())) {
            Result = Result + "Number Of Lines ";
        }
        if (!isintvalue(searchfields.getstartingposition())) {
            Result = Result + "Starting Position ";
        }
        if (!isintvalue(searchfields.getignoredlines())) {
            Result = Result + "Ignored Lines ";
        }
        if (!isintvalue(searchfields.getstartinglength())) {
            Result = Result + "Starting Length ";
        }
        if (!isintvalue(searchfields.getdateseparatorpos1())) {
            Result = Result + "Date Separator Pos1 ";
        }
        if (!isintvalue(searchfields.getdateseparatorpos2())) {
            Result = Result + "Date Separator Pos2 ";
        }
        if (!isintvalue(searchfields.getheaderlength())) {
            Result = Result + "Header Length ";
        }
        if (!isintvalue(searchfields.getheaderdateseparatorpos1())) {
            Result = Result + "Header Date Separator Pos1 ";
        }
        if (!isintvalue(searchfields.getheaderdateseparatorpos2())) {
            Result = Result + "Header Date Separator Pos2 ";
        }
        if (!isintvalue(searchfields.gettagsendingposition())) {
            Result = Result + "Tag ending Position ";
        }
        if (!isintvalue(searchfields.gettagsendinglength())) {
            Result = Result + "Tag ending Length ";
        }
        if (!isintvalue(searchfields.getheaderlines())) {
            Result = Result + "Header Lines ";
        }
        if (!isintvalue(searchfields.getmaxtaglines())) {
            Result = Result + "Max Tag Lines ";
        }
        if (!"Valid".equals(Result)) {
            Result = Result + " Non Integer Values";
            Result = Result.replace("Valid", "");
        }
        return Result;
    }

    public void updateRecordTemplate() throws Throwable {

        message = validatenull(updatetemplate);
        message2 = "";
        if ("Valid".equals(message)) {
            message = validateintvalues(updatetemplate);
            if (templateBO.GetRecord(updatetemplate, FindTableName(ChosenItem)) < 2) {
                if ("Valid".equals(message)) {
                    message = (String) templateBO.updaterecord(updatetemplate, FindTableName(ChosenItem));
                }
            } else {
                message = "Please Enter Unique Template Name and Paths";
            }
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void browseFolder0() {
        System.out.println("test");
    }

    public void browseFolder() {
        chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(null);
        if (chooser.getSelectedFile().toString() != null) {
            selectedtemplate.setloadingfolder(chooser.getSelectedFile().toString());
        }
    }

    public void browseFolder1() {
        chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(null);
        if (chooser.getSelectedFile().toString() != null) {
            selectedtemplate.setbackupfolder(chooser.getSelectedFile().toString());
        }
    }

    public void browseFolder2() {
        chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(null);
        if (chooser.getSelectedFile().toString() != null) {
            selectedtemplate.setcopyfolder(chooser.getSelectedFile().toString());
        }
    }

    public void browseFolder3() {
        chooser = new JFileChooser();
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(null);
        if (chooser.getSelectedFile().toString() != null) {
            selectedtemplate.setserverfolder(chooser.getSelectedFile().toString());
        }
    }

    public void fillDeleteTemplateHeader(ActionEvent e) {
        DeletetemplateHeader = (filetemplateheaderDTOInter) e.getComponent().getAttributes().get("removedRow1");
    }

    public void SaveChangesHeader() throws Throwable {
        message2 = "";
        message = (String) headertemplateBO.save(headertemplateList, FindTableName(ChosenItem));
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void addNewTemplateHeader() throws Throwable {
        newrecordHeader = DTOFactory.createfiletemplateheaderDTO();
        newrecordHeader.settemplate(selectedtemplate.getid());
        newrecordHeader.setid(99);
        headertemplateList.add(newrecordHeader);
        showConfirm1 = true;
        showAdd1 = false;
        message = "";
        message2 = "";
    }

    public void confirmAddTemplateHeader() throws Throwable {
        message = validatenullheader(newrecordHeader);
        message2 = "";
        if ("Valid".equals(message)) {
            message = validateintvaluesHeader(newrecordHeader);
            if ("Valid".equals(message)) {
                message = (String) headertemplateBO.insertrecord(newrecordHeader, FindTableName(ChosenItem));
                message = "1 Record has been Inserted";
                showConfirm1 = false;
                showAdd1 = true;
            }
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void deleteRecordTemplateHeader() throws Throwable {
        message = "";
        message2 = "";
        if (DeletetemplateHeader.getid() != 99) {
            message = (String) headertemplateBO.deleterecord(DeletetemplateHeader, FindTableName(ChosenItem));
            if (message.contains("Record Has Been Deleted")) {
                headertemplateList.remove(DeletetemplateHeader);
                message = "1 Record Has been Deleted";
            }
        } else {
            headertemplateList.remove(DeletetemplateHeader);
            showConfirm1 = false;
            showAdd1 = true;
            message = "1 Record Has been Removed";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void fillDeleteTemplateDetail(ActionEvent e) {
        DeletetemplateDetail = (cardfiletemplatedetailDTOInter) e.getComponent().getAttributes().get("removedRow3");
    }

    public void deleteRecordTemplateDetail() throws Throwable {
        message = "";
        message2 = "";
        DeletetemplateDetail.setlinenumber("");
        
        DeletetemplateDetail.setMandatoryboolean(Boolean.FALSE);
        DeletetemplateDetail.setNegativeamountflagboolean(Boolean.FALSE);
        DeletetemplateDetail.setDecimalposboolean(Boolean.FALSE);
        DeletetemplateDetail.setposition("");
        DeletetemplateDetail.setlength("");
        DeletetemplateDetail.setformat("");
        DeletetemplateDetail.setformat2("");
        DeletetemplateDetail.settagstring("");
        DeletetemplateDetail.setstartingposition("");
        DeletetemplateDetail.setdatatype(0);
    }

    public void SaveChanges() throws Throwable {
        message = "";
        message2 = "";
        filetemplatedetailList = (List<cardfiletemplatedetailDTOInter>) detailtemplateBO.findRecordsListUpdate(detailtemplateBO.updatetemplate(detailtemplateBO.save(filetemplatedetailList, FindTableName(ChosenItem)), selectedtemplate.getid(), FindNetworkID(ChosenItem)));
        message2 = filetemplatedetailList.size() + " Records Has Been Saved";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message2));
    }
}
