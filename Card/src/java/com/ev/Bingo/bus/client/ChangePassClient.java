/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.userpassBOInter;
import com.ev.Bingo.bus.bo.usersBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.userpassDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author Administrator
 */
public class ChangePassClient extends BaseBean {

    private usersBOInter UBO;
    private List<usersDTOInter> uDTOL;
    private usersDTOInter selectedUDTO;
    private String oldPass;
    private String confPass, newPass;
    private Integer userInt;
    private String message;

    public usersBOInter getUBO() {
        return UBO;
    }

    public void setUBO(usersBOInter UBO) {
        this.UBO = UBO;
    }

    public List<usersDTOInter> getuDTOL() {
        return uDTOL;
    }

    public void setuDTOL(List<usersDTOInter> uDTOL) {
        this.uDTOL = uDTOL;
    }

    public usersDTOInter getSelectedUDTO() {
        return selectedUDTO;
    }

    public void setSelectedUDTO(usersDTOInter selectedUDTO) {
        this.selectedUDTO = selectedUDTO;
    }

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public String getConfPass() {
        return confPass;
    }

    public void setConfPass(String confPass) {
        this.confPass = confPass;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public Integer getUserInt() {
        return userInt;
    }

    public void setUserInt(Integer userInt) {
        this.userInt = userInt;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ChangePassClient() throws Throwable {
        GetAccess();
        UBO = BOFactory.createusersBO();
        uDTOL = (List<usersDTOInter>) UBO.GetAllRecords();
        selectedUDTO = DTOFactory.createusersDTO();
    }

    public void changeUser(ValueChangeEvent e) {
        try {
            int id = (Integer) e.getNewValue();
            selectedUDTO.setUserid(id);
            selectedUDTO = (usersDTOInter) UBO.GetRecordById(selectedUDTO);
        } catch (Throwable ex) {
            Logger.getLogger(ChangePassClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void save() throws Throwable {
        if ("".equals(oldPass) || oldPass == null) {
                message = "Please Enter Old Password";
            }
        else if (!newPass.equals(confPass)) {
            message = "New Password and Confirm Password should be the same";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
            return;
        } else if (!newPass.matches("(?=^.{8,}$)(?=.*\\d)(?=.*\\W+)(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$")) {
            message = "The input password volates the policy, please make sure that your password follows the following rules:"
                    + "Password length is 9 characters or more."
                    + "It contains one or more capital letter(s)."
                    + "It contains a combination of characters, numbers and special characters.";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
            return;
        }
        userpassDTOInter UPDTO = DTOFactory.createuserpassDTO();
        userpassDTOInter NUPDTO = DTOFactory.createuserpassDTO();
        UPDTO.setuserid(selectedUDTO.getUserid());
        UPDTO.setpass(newPass);
        userpassBOInter UPBO = BOFactory.createuserpassBO();
        NUPDTO = (userpassDTOInter) UPBO.GetRecord(UPDTO);
        if (NUPDTO.getchangedate() != null) {
            message = "This Password Has Been Entered Before At " + NUPDTO.getchangedate();
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
            return;
        }
        NUPDTO.setuserid(UPDTO.getuserid());
        NUPDTO.setpass(newPass);
        message = (String) UPBO.insertrecord(NUPDTO);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
        if (message.contains("Row Has Been")) {
            usersDTOInter UDTO = DTOFactory.createusersDTO();
            UDTO = selectedUDTO;
            UDTO.setUserpassword(newPass);
            message = (String) UBO.updaterecord(UDTO);
            return;
        } else {
            return;
        }
    }
}
