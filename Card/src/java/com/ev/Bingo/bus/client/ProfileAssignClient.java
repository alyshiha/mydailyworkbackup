/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.profileBOInter;
import com.ev.Bingo.bus.bo.userprofileBOInter;
import com.ev.Bingo.bus.bo.usersBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.profileDTOInter;
import com.ev.Bingo.bus.dto.userprofileDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Administrator
 */
public class ProfileAssignClient extends BaseBean {

    private profileBOInter profileBO;
    private userprofileBOInter userprofileBO;
    private List<profileDTOInter> profileList;
    private List<userprofileDTOInter> SavedList;
    private userprofileDTOInter profileItem, insertrec;
    private userprofileDTOInter upDTO;
    private List<String> source, target;
    private DualListModel<String> pickValues;
    private Integer profileInt;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DualListModel<String> getPickValues() {
        return pickValues;
    }

    public void setPickValues(DualListModel<String> pickValues) {
        this.pickValues = pickValues;
    }

    public Integer getProfileInt() {
        return profileInt;
    }

    public void setProfileInt(Integer profileInt) {
        this.profileInt = profileInt;
    }

    public List<profileDTOInter> getProfileList() {
        return profileList;
    }

    public void setProfileList(List<profileDTOInter> profileList) {
        this.profileList = profileList;
    }

    /**
     * Creates a new instance of ProfileAssignClient
     */
    public ProfileAssignClient() throws Throwable {
        super();
        GetAccess();
        profileBO = BOFactory.createprofileBO();
        profileList = (List<profileDTOInter>) profileBO.GetAllRecords();
        pickValues = new DualListModel<String>();
    }

    public void changeValue(ValueChangeEvent e) throws Throwable {
        profileInt = (Integer) e.getNewValue();
        usersBOInter uDAO = BOFactory.createusersBO();
        List<usersDTOInter> hasProfile = (List<usersDTOInter>) uDAO.GetAllAssignToProfile(profileInt);
        List<usersDTOInter> hasNotProfile = (List<usersDTOInter>) uDAO.GetAllUnAssignToProfile();
        DualListModel pickList = new DualListModel();
        source = new ArrayList<String>();
        target = new ArrayList<String>();
        for (usersDTOInter u : hasProfile) {
            source.add(u.getLogonname());
        }
        for (usersDTOInter u : hasNotProfile) {
            target.add(u.getLogonname());
        }
        pickList.setSource(target);
        pickList.setTarget(source);
        pickValues = (DualListModel<String>) pickList;
    }

    public void saveProfile() {
        try {
            
            String profile = "";
            for (profileDTOInter profileList1 : profileList) {
                if(profileList1.getprofileid() == profileInt){
                    profile = profileList1.getprofilename();
                }
            }
            profileItem = DTOFactory.createuserprofileDTO();
            profileBO = BOFactory.createprofileBO();
            SavedList = new ArrayList<userprofileDTOInter>();
            userprofileBO = BOFactory.createuserprofileBO();
            profileItem.setprofileid(profileInt);
            userprofileBO.deleterecord(profileItem);
            for (String u : pickValues.getTarget()) {
                insertrec = DTOFactory.createuserprofileDTO();
                insertrec.setprofileid(profileInt);
                insertrec.setusername(u);
                SavedList.add(insertrec);
            }
            message = (String) userprofileBO.save(SavedList,profile);
            if (pickValues.getSource().size() > 0 && pickValues.getTarget().isEmpty()) {
                message = "User Has Been Removed";
            }
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } catch (Throwable ex) {
        }
    }

    public void resetVars() {
        message = "";
    }
}
