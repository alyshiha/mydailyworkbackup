/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.bingologBOInter;
import com.ev.Bingo.bus.bo.profileBOInter;
import com.ev.Bingo.bus.bo.userprofileBOInter;
import com.ev.Bingo.bus.bo.usersBOInter;
import com.ev.Bingo.bus.dto.bingologDTOInter;
import com.ev.Bingo.bus.dto.profileDTOInter;
import com.ev.Bingo.bus.dto.userprofileDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.util.Date;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import org.primefaces.component.datatable.DataTable;

/**
 *
 * @author Administrator
 */
public class BingoLogClient extends BaseBean {

    private Date fromDate, toDate;
    private Integer userId, profileId;
    private bingologBOInter bingoBo;
    private usersBOInter UserbingoBo;
    private profileBOInter ProfilebingoBo;
    private userprofileBOInter UserProfilebingoBo;
    private List<usersDTOInter> userList;
    private List<profileDTOInter> profileList;
    private List<bingologDTOInter> logList;
    private List<userprofileDTOInter> UserProfile;
    private String message;

    public List<userprofileDTOInter> getUserProfile() {
        return UserProfile;
    }

    public void setUserProfile(List<userprofileDTOInter> UserProfile) {
        this.UserProfile = UserProfile;
    }

    public bingologBOInter getBingoBo() {
        return bingoBo;
    }

    public void setBingoBo(bingologBOInter bingoBo) {
        this.bingoBo = bingoBo;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public List<bingologDTOInter> getLogList() {
        return logList;
    }

    public void setLogList(List<bingologDTOInter> logList) {
        this.logList = logList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getProfileId() {
        return profileId;
    }

    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }

    public List<profileDTOInter> getProfileList() {
        return profileList;
    }

    public void setProfileList(List<profileDTOInter> profileList) {
        this.profileList = profileList;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<usersDTOInter> getUserList() {
        return userList;
    }

    public void setUserList(List<usersDTOInter> userList) {
        this.userList = userList;
    }

    public profileBOInter getProfilebingoBo() {
        return ProfilebingoBo;
    }

    public void setProfilebingoBo(profileBOInter ProfilebingoBo) {
        this.ProfilebingoBo = ProfilebingoBo;
    }

    public userprofileBOInter getUserProfilebingoBo() {
        return UserProfilebingoBo;
    }

    public void setUserProfilebingoBo(userprofileBOInter UserProfilebingoBo) {
        this.UserProfilebingoBo = UserProfilebingoBo;
    }

    public usersBOInter getUserbingoBo() {
        return UserbingoBo;
    }

    public void setUserbingoBo(usersBOInter UserbingoBo) {
        this.UserbingoBo = UserbingoBo;
    }

    public BingoLogClient() throws Throwable {
        GetAccess();
        bingoBo = BOFactory.createbingologBO();
        ProfilebingoBo = BOFactory.createprofileBO();
        UserbingoBo = BOFactory.createusersBO();
        UserProfilebingoBo = BOFactory.createuserprofileBO();
        UserbingoBo = BOFactory.createusersBO();
        userList = (List<usersDTOInter>) UserbingoBo.GetAllRecords();
        profileList = (List<profileDTOInter>) ProfilebingoBo.GetAllRecords();
        fromDate = super.getYesterdaytrans("20:00:00") ;
        toDate = super.getTodaytrans("20:00:00");
        
    }

    public void FilterUsers(ValueChangeEvent e) throws Throwable {
        if (Integer.parseInt(e.getNewValue().toString()) != 0) {
            userList = (List<usersDTOInter>) UserbingoBo.GetAllAssignToProfile(Integer.parseInt(e.getNewValue().toString()));
        } else {
            userList = (List<usersDTOInter>) UserbingoBo.GetAllRecords();
        }
    }

    public void doSearch() throws Throwable {

        logList = (List<bingologDTOInter>) bingoBo.GetListOfRecords(fromDate, toDate, userId, profileId);
        message = "";
        ((DataTable) (FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:mat_typ_dt"))).setFirst(0);
    }
}
