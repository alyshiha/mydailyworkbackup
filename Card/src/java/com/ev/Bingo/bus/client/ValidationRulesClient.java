/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.base.util.DateFormatter;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.filecolumndefinitionBOInter;
import com.ev.Bingo.bus.bo.validationrulesBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import com.ev.Bingo.bus.dto.validationrulesDTOInter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author AlyShiha
 */
public class ValidationRulesClient extends BaseBean {

    private validationrulesBOInter VREngin;
    private List<validationrulesDTOInter> vrList;
    private List<filecolumndefinitionDTOInter> fcdList;
    private filecolumndefinitionDTOInter colitem;
    private filecolumndefinitionBOInter FCDEngin;
    private validationrulesDTOInter drDelAttr, newRecord;
    private String message;
    private Integer fcdInt, addType, ruleSelect;
    private Boolean updateFlag, activeBool, dateSelected, pickBool, betweenDate, disFlag, valFlag, formulaFlag, addFormula, betweenFlag, nullOperation;
    private String formulaStatus, formulaName, checkUp, columnNam, operationStr, firstText, andValue, secondValue, allFormula, colname;
    private Date firstTextDate, secondValueDate;
    private List<String> operationValues;
    private List<Object> columnValues;
    private Integer[] valuesList;
    private Integer Type;

    public Integer getFcdInt() {
        return fcdInt;
    }

    public void setFcdInt(Integer fcdInt) {
        this.fcdInt = fcdInt;
    }

    public Integer getAddType() {
        return addType;
    }

    public void setAddType(Integer addType) {
        this.addType = addType;
    }

    public Integer getRuleSelect() {
        return ruleSelect;
    }

    public void setRuleSelect(Integer ruleSelect) {
        this.ruleSelect = ruleSelect;
    }

    public Boolean getValFlag() {
        return valFlag;
    }

    public void setValFlag(Boolean valFlag) {
        this.valFlag = valFlag;
    }

    public Boolean getFormulaFlag() {
        return formulaFlag;
    }

    public void setFormulaFlag(Boolean formulaFlag) {
        this.formulaFlag = formulaFlag;
    }

    public Boolean getAddFormula() {
        return addFormula;
    }

    public void setAddFormula(Boolean addFormula) {
        this.addFormula = addFormula;
    }

    public String getFormulaName() {
        return formulaName;
    }

    public void setFormulaName(String formulaName) {
        this.formulaName = formulaName;
    }

    public String getCheckUp() {
        return checkUp;
    }

    public void setCheckUp(String checkUp) {
        this.checkUp = checkUp;
    }

    public String getOperationStr() {
        return operationStr;
    }

    public void setOperationStr(String operationStr) {
        this.operationStr = operationStr;
    }

    public String getFirstText() {
        return firstText;
    }

    public void setFirstText(String firstText) {
        this.firstText = firstText;
    }

    public String getAndValue() {
        return andValue;
    }

    public void setAndValue(String andValue) {
        this.andValue = andValue;
    }

    public String getSecondValue() {
        return secondValue;
    }

    public void setSecondValue(String secondValue) {
        this.secondValue = secondValue;
    }

    public String getAllFormula() {
        return allFormula;
    }

    public void setAllFormula(String allFormula) {
        this.allFormula = allFormula;
    }

    public String getColname() {
        return colname;
    }

    public void setColname(String colname) {
        this.colname = colname;
    }

    public Date getFirstTextDate() {
        return firstTextDate;
    }

    public void setFirstTextDate(Date firstTextDate) {
        this.firstTextDate = firstTextDate;
    }

    public Date getSecondValueDate() {
        return secondValueDate;
    }

    public void setSecondValueDate(Date secondValueDate) {
        this.secondValueDate = secondValueDate;
    }

    public Integer[] getValuesList() {
        return valuesList;
    }

    public void setValuesList(Integer[] valuesList) {
        this.valuesList = valuesList;
    }

    public Integer getType() {
        return Type;
    }

    public void setType(Integer Type) {
        this.Type = Type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getUpdateFlag() {
        return updateFlag;
    }

    public void setUpdateFlag(Boolean updateFlag) {
        this.updateFlag = updateFlag;
    }

    public Boolean getActiveBool() {
        return activeBool;
    }

    public void setActiveBool(Boolean activeBool) {
        this.activeBool = activeBool;
    }

    public Boolean getDateSelected() {
        return dateSelected;
    }

    public void setDateSelected(Boolean dateSelected) {
        this.dateSelected = dateSelected;
    }

    public Boolean getPickBool() {
        return pickBool;
    }

    public void setPickBool(Boolean pickBool) {
        this.pickBool = pickBool;
    }

    public Boolean getBetweenDate() {
        return betweenDate;
    }

    public void setBetweenDate(Boolean betweenDate) {
        this.betweenDate = betweenDate;
    }

    public Boolean getDisFlag() {
        return disFlag;
    }

    public void setDisFlag(Boolean disFlag) {
        this.disFlag = disFlag;
    }

    public Boolean getBetweenFlag() {
        return betweenFlag;
    }

    public void setBetweenFlag(Boolean betweenFlag) {
        this.betweenFlag = betweenFlag;
    }

    public Boolean getNullOperation() {
        return nullOperation;
    }

    public void setNullOperation(Boolean nullOperation) {
        this.nullOperation = nullOperation;
    }

    public String getFormulaStatus() {
        return formulaStatus;
    }

    public void setFormulaStatus(String formulaStatus) {
        this.formulaStatus = formulaStatus;
    }

    public String getColumnNam() {
        return columnNam;
    }

    public void setColumnNam(String columnNam) {
        this.columnNam = columnNam;
    }

    public List<String> getOperationValues() {
        return operationValues;
    }

    public void setOperationValues(List<String> operationValues) {
        this.operationValues = operationValues;
    }

    public List<Object> getColumnValues() {
        return columnValues;
    }

    public void setColumnValues(List<Object> columnValues) {
        this.columnValues = columnValues;
    }

    public validationrulesBOInter getVREngin() {
        return VREngin;
    }

    public void setVREngin(validationrulesBOInter VREngin) {
        this.VREngin = VREngin;
    }

    public List<validationrulesDTOInter> getVrList() {
        return vrList;
    }

    public void setVrList(List<validationrulesDTOInter> vrList) {
        this.vrList = vrList;
    }

    public List<filecolumndefinitionDTOInter> getFcdList() {
        return fcdList;
    }

    public void setFcdList(List<filecolumndefinitionDTOInter> fcdList) {
        this.fcdList = fcdList;
    }

    public filecolumndefinitionBOInter getFCDEngin() {
        return FCDEngin;
    }

    public void setFCDEngin(filecolumndefinitionBOInter FCDEngin) {
        this.FCDEngin = FCDEngin;
    }

    public validationrulesDTOInter getDrDelAttr() {
        return drDelAttr;
    }

    public void setDrDelAttr(validationrulesDTOInter drDelAttr) {
        this.drDelAttr = drDelAttr;
    }

    public ValidationRulesClient() throws Throwable {
        GetAccess();
        VREngin = BOFactory.createvalidationrulesBO();
        FCDEngin = BOFactory.createfilecolumndefinitionBO();
        vrList = (List<validationrulesDTOInter>) VREngin.GetAllRecords();
        fcdList = (List<filecolumndefinitionDTOInter>) FCDEngin.GetAllRecords();
        colitem = DTOFactory.createfilecolumndefinitionDTO();
        colitem.setname("Choose one");
        colitem.setid(0);
        fcdList.add(colitem);
        fcdInt = 0;
        formulaFlag = true;
        addFormula = false;
        betweenFlag = false;
        nullOperation = false;
        betweenDate = false;
        dateSelected = false;
        pickBool = false;
        columnNam = "Trans Date";
        andValue = "AND";
        disFlag = true;
        valFlag = false;
        operationValues = new ArrayList<String>();
        columnValues = new ArrayList<Object>();
    }

    public void fillDelete(ActionEvent e) {
        drDelAttr = (validationrulesDTOInter) e.getComponent().getAttributes().get("removedRow");
    }

    public void deleteRecord() throws Throwable {
        message = (String) VREngin.deleterecord(drDelAttr);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
        vrList.remove(drDelAttr);
    }

    public void SaveChangesValidation() throws Throwable {
        message = (String) VREngin.save(vrList);
        message = "Records are Updated";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void addPopup() throws Throwable {
        addType = 0;
        fcdInt = 0;
        formulaName = "";
        activeBool = false;
        firstTextDate = null;
        secondValueDate = null;
        firstText = "";
        if (columnNam.toUpperCase().contains("DATE") || columnNam.toUpperCase().contains("TIME")) {
            if (columnValues == null) {
                columnValues = new ArrayList<Object>();
            }
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add(">=");
            operationValues.add("<=");
            operationValues.add("between");
            dateSelected = true;
            betweenDate = false;
            nullOperation = false;
            betweenFlag = false;
            pickBool = false;
        } else {
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add("in");
            operationValues.add("not in");
            operationValues.add("null");
            operationValues.add("is not null");
            columnValues = (List<Object>) VREngin.getColumnValues(GetCol(fcdInt));
            if (columnValues == null) {
                columnValues = new ArrayList<Object>();
                dateSelected = false;
                betweenDate = false;
                nullOperation = true;
                betweenFlag = false;
                pickBool = false;
            } else {
                dateSelected = false;
                betweenDate = false;
                nullOperation = false;
                betweenFlag = false;
                pickBool = true;
            }

        }

        formulaName = "";
        addType = 1;
        activeBool = Boolean.FALSE;
        fcdInt = 0;
        formulaFlag = true;
        addFormula = false;
        betweenFlag = false;
        nullOperation = false;
        betweenDate = false;
        dateSelected = false;
        pickBool = false;
        columnNam = "Trans Date";
        andValue = "AND";
        disFlag = true;
        valFlag = false;
        operationValues = new ArrayList<String>();
        columnValues = new ArrayList<Object>();
        updateFlag = false;
        formulaStatus = "Add";
    }

    public void valueEffectinteger(int select) throws Throwable {
        fcdInt = (Integer) select;
        columnNam = GetColName(fcdInt);
        message = "";
        if (columnNam.toUpperCase().contains("DATE") || columnNam.toUpperCase().contains("TIME")) {
            if (columnValues == null) {
                columnValues = new ArrayList<Object>();
            }
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add(">=");
            operationValues.add("<=");
            operationValues.add("between");
            dateSelected = true;
            betweenDate = false;
            nullOperation = false;
            betweenFlag = false;
            pickBool = false;
        } else {
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add("in");
            operationValues.add("not in");
            operationValues.add("null");
            operationValues.add("is not null");
            columnValues = (List<Object>) VREngin.getColumnValues(GetCol(fcdInt));
            columnNam = GetColName(fcdInt);
            if (columnValues == null || columnValues.size() == 0) {
                columnValues = new ArrayList<Object>();
                dateSelected = false;
                betweenDate = false;
                nullOperation = true;
                betweenFlag = false;
                pickBool = false;
            } else {
                dateSelected = false;
                betweenDate = false;
                nullOperation = false;
                betweenFlag = false;
                pickBool = true;
            }

        }
        updateFlag = false;
        formulaStatus = "Add";
    }

    public void valueEffect(ValueChangeEvent e) throws Throwable {
        fcdInt = (Integer) e.getNewValue();
        columnNam = GetColName(fcdInt);
        message = "";
        if (columnNam.toUpperCase().contains("DATE") || columnNam.toUpperCase().contains("TIME")) {
            if (columnValues == null) {
                columnValues = new ArrayList<Object>();
            }
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add(">=");
            operationValues.add("<=");
            operationValues.add("between");
            dateSelected = true;
            betweenDate = false;
            nullOperation = false;
            betweenFlag = false;
            pickBool = false;
        } else {
            operationValues.clear();
            columnValues.clear();
            operationValues.add("=");
            operationValues.add("in");
            operationValues.add("not in");
            operationValues.add("null");
            operationValues.add("is not null");
            columnValues = (List<Object>) VREngin.getColumnValues(GetCol(fcdInt));
            columnNam = GetColName(fcdInt);
            if (columnValues == null || columnValues.size() == 0) {
                columnValues = new ArrayList<Object>();
                dateSelected = false;
                betweenDate = false;
                nullOperation = true;
                betweenFlag = false;
                pickBool = false;
            } else {
                dateSelected = false;
                betweenDate = false;
                nullOperation = false;
                betweenFlag = false;
                pickBool = true;
            }

        }
        updateFlag = false;
        formulaStatus = "Add";
    }

    public String GetColName(Integer ColID) {
        for (filecolumndefinitionDTOInter File : fcdList) {
            if (File.getid() == ColID) {
                return File.getname();
            }
        }
        return "";
    }

    public String GetCol(Integer ColID) {
        for (filecolumndefinitionDTOInter File : fcdList) {
            if (File.getid() == ColID) {
                return File.getcolumnname();
            }
        }
        return "";
    }

    public void operationChange(ValueChangeEvent e) {
        operationStr = (String) e.getNewValue();

        if (columnNam.toUpperCase().contains("DATE")) {
            if (operationStr.equals("between")) {
                dateSelected = true;
                betweenDate = true;
                nullOperation = false;
                betweenFlag = false;
                pickBool = false;
            } else {
                dateSelected = true;
                betweenDate = false;
                nullOperation = false;
                betweenFlag = false;
                pickBool = false;
            }
        } else if (columnValues.isEmpty() || columnValues == null) {
            if (operationStr.contains("null") || operationStr.contains("is not null")) {
                dateSelected = false;
                betweenDate = false;
                nullOperation = true;
                betweenFlag = false;
                pickBool = false;
            } else {
                dateSelected = false;
                betweenDate = false;
                nullOperation = true;
                betweenFlag = false;
                pickBool = false;
            }
        } else {
            dateSelected = false;
            betweenDate = false;
            nullOperation = false;
            betweenFlag = false;
            pickBool = true;
        }
    }

    public void addRecord() {
        if (fcdInt != 0) {
            if (!"".equals(formulaName)) {
                try {
                    if (vrList == null) {
                        vrList = new ArrayList<validationrulesDTOInter>();
                    }

                    newRecord = DTOFactory.createvalidationrulesDTO();
                    if (activeBool == true) {
                        newRecord.setactive(1);
                    } else {
                        newRecord.setactive(2);
                    }

                    newRecord.setvaldationtype(addType);
                    newRecord.setcolumnid(fcdInt);
                    newRecord.setcolumnname(GetColName(fcdInt));
                    newRecord.setname(formulaName);
                    newRecord.setActiveB(activeBool);
                    if (columnNam.toUpperCase().contains("DATE")) {
                        if (operationStr.contains("between")) {
                            String dateValues = VREngin.genarateValueDate(firstTextDate, secondValueDate);
                            if (VREngin.createFormula(fcdInt, operationStr, dateValues, newRecord) == "Invalid Formula") {
                                message = "Invalid Formula";
                                FacesContext context = FacesContext.getCurrentInstance();
                                context.addMessage(null, new FacesMessage(message));
                            } else {
                                vrList.add(newRecord);
                                message = (String) VREngin.insertrecord(newRecord);
                                FacesContext context = FacesContext.getCurrentInstance();
                                context.addMessage(null, new FacesMessage(message));

                            }
                        } else {

                            if (VREngin.createFormula(fcdInt, operationStr, DateFormatter.changeDateAndTimeFormat(firstTextDate), newRecord) == "Invalid Formula") {
                                message = "Invalid Formula";
                                FacesContext context = FacesContext.getCurrentInstance();
                                context.addMessage(null, new FacesMessage(message));
                            } else {
                                vrList.add(newRecord);
                                message = (String) VREngin.insertrecord(newRecord);
                                FacesContext context = FacesContext.getCurrentInstance();
                                context.addMessage(null, new FacesMessage(message));
                            }
                        }
                    }

                    if (pickBool) {
                        String pickValye = VREngin.genarateValueString(valuesList);
                        VREngin.createFormula(fcdInt, operationStr, pickValye, newRecord);
                        vrList.add(newRecord);
                        newRecord.setcolumnname(columnNam);
                        message = (String) VREngin.insertrecord(newRecord);
                        FacesContext context = FacesContext.getCurrentInstance();
                        context.addMessage(null, new FacesMessage(message));
                    } else {
                        if (!columnNam.toUpperCase().contains("DATE")) {
                            VREngin.createFormula(fcdInt, operationStr, firstText, newRecord);
                            vrList.add(newRecord);
                            message = (String) VREngin.insertrecord(newRecord);
                            FacesContext context = FacesContext.getCurrentInstance();
                            context.addMessage(null, new FacesMessage(message));
                        }
                    }

                } catch (Throwable ex) {
                    message = ex.getMessage();
                    FacesContext context = FacesContext.getCurrentInstance();
                    context.addMessage(null, new FacesMessage(message));
                }
            } else {
                message = "Name is a Required Field";
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(message));
            }
        } else {
            message = "Column Name is a Required Field";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }

}
