/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.licenseBOInter;
import com.ev.Bingo.bus.bo.transactionmanagementBOInter;
import com.ev.Bingo.bus.dto.licenseDTOInter;
import com.ev.Bingo.bus.dto.transactionmanagementDTOInter;
import java.util.ArrayList;
import java.util.List;
import org.primefaces.model.chart.MeterGaugeChartModel;

public class ChartView {

    private MeterGaugeChartModel model;
    private transactionmanagementBOInter TMEngin;
    private List<licenseDTOInter> LDTO;

    public ChartView() throws Throwable {
       
        TMEngin = BOFactory.createtransactionmanagementBO();
        transactionmanagementDTOInter TM = (transactionmanagementDTOInter) TMEngin.GetAllRecords();
        licenseBOInter LicBO = BOFactory.createlicenseBO();
        LDTO = (List<licenseDTOInter>) LicBO.GetAllRecords();
        List<Number> intervals = new ArrayList<Number>() {
            {
                add((Integer.valueOf(LDTO.get(0).getnooftrans()) / 100) * 15);
                add((Integer.valueOf(LDTO.get(0).getnooftrans()) / 100) * 70);
                add((Integer.valueOf(LDTO.get(0).getnooftrans()) / 100) * 90);
                add((Integer.valueOf(LDTO.get(0).getnooftrans()) / 100) * 100);
            }
        };
        model = new MeterGaugeChartModel();
        model.setIntervals(intervals);
        model.setValue(TM.gettranno());
    }

    public void setModel(MeterGaugeChartModel model) {
        this.model = model;
    }

    public MeterGaugeChartModel getModel() {
        return model;
    }
}
