/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.duplicatesettingdetailBOInter;
import com.ev.Bingo.bus.bo.duplicatesettingmasterBOInter;
import com.ev.Bingo.bus.bo.filecolumndefinitionBOInter;
import com.ev.Bingo.bus.bo.networksBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.duplicatesettingdetailDTO;
import com.ev.Bingo.bus.dto.duplicatesettingdetailDTOInter;
import com.ev.Bingo.bus.dto.duplicatesettingmasterDTOInter;
import com.ev.Bingo.bus.dto.filecolumndefinitionDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Administrator
 */
public class DuplicateSettingClient extends BaseBean {

    private networksBOInter NBOEngin;
    private duplicatesettingmasterBOInter DSMBOEngin;
    private duplicatesettingdetailBOInter DSDBOEngin;
    private List<networksDTOInter> nL;
    private List<duplicatesettingmasterDTOInter> dSML;
    private duplicatesettingmasterDTOInter selectedrow, newrecord;
    private duplicatesettingdetailDTOInter selecteddetailrow, newrecord1;
    private List<duplicatesettingdetailDTOInter> dSDL;
    private Boolean showAdd, showConfirm, showAdd1, showConfirm1;
    private String message, message1;
    private filecolumndefinitionBOInter colObject;
    private List<filecolumndefinitionDTOInter> colDefList;

    public networksBOInter getNBOEngin() {
        return NBOEngin;
    }

    public void setNBOEngin(networksBOInter NBOEngin) {
        this.NBOEngin = NBOEngin;
    }

    public duplicatesettingmasterBOInter getDSMBOEngin() {
        return DSMBOEngin;
    }

    public void setDSMBOEngin(duplicatesettingmasterBOInter DSMBOEngin) {
        this.DSMBOEngin = DSMBOEngin;
    }

    public duplicatesettingdetailBOInter getDSDBOEngin() {
        return DSDBOEngin;
    }

    public void setDSDBOEngin(duplicatesettingdetailBOInter DSDBOEngin) {
        this.DSDBOEngin = DSDBOEngin;
    }

    public List<networksDTOInter> getnL() {
        return nL;
    }

    public void setnL(List<networksDTOInter> nL) {
        this.nL = nL;
    }

    public List<duplicatesettingmasterDTOInter> getdSML() {
        return dSML;
    }

    public void setdSML(List<duplicatesettingmasterDTOInter> dSML) {
        this.dSML = dSML;
    }

    public duplicatesettingmasterDTOInter getSelectedrow() {
        return selectedrow;
    }

    public void setSelectedrow(duplicatesettingmasterDTOInter selectedrow) {
        this.selectedrow = selectedrow;
    }

    public duplicatesettingmasterDTOInter getNewrecord() {
        return newrecord;
    }

    public void setNewrecord(duplicatesettingmasterDTOInter newrecord) {
        this.newrecord = newrecord;
    }

    public duplicatesettingdetailDTOInter getSelecteddetailrow() {
        return selecteddetailrow;
    }

    public void setSelecteddetailrow(duplicatesettingdetailDTOInter selecteddetailrow) {
        this.selecteddetailrow = selecteddetailrow;
    }

    public duplicatesettingdetailDTOInter getNewrecord1() {
        return newrecord1;
    }

    public void setNewrecord1(duplicatesettingdetailDTOInter newrecord1) {
        this.newrecord1 = newrecord1;
    }

    public List<duplicatesettingdetailDTOInter> getdSDL() {
        return dSDL;
    }

    public void setdSDL(List<duplicatesettingdetailDTOInter> dSDL) {
        this.dSDL = dSDL;
    }

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public List<filecolumndefinitionDTOInter> getColDefList() {
        return colDefList;
    }

    public void setColDefList(List<filecolumndefinitionDTOInter> colDefList) {
        this.colDefList = colDefList;
    }

    public DuplicateSettingClient() throws Throwable {
        super();
        GetAccess();
        NBOEngin = BOFactory.createnetworksBO();
        DSMBOEngin = BOFactory.createduplicatesettingmasterBO();
        DSDBOEngin = BOFactory.createduplicatesettingdetailBO();
        colObject = BOFactory.createfilecolumndefinitionBo();
        colDefList = (List<filecolumndefinitionDTOInter>) colObject.GetAllRecords();
        dSML = (List<duplicatesettingmasterDTOInter>) DSMBOEngin.GetAllRecords();
        dSDL = new ArrayList<duplicatesettingdetailDTOInter>();
        nL = (List<networksDTOInter>) NBOEngin.GetAllRecords();
        List<String> recordtype = new ArrayList<String>();
        recordtype.add("Network");
        recordtype.add("Switch");
        recordtype.add("Host");
        showAdd = true;
        showConfirm = false;
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void onRowSelect(SelectEvent e) throws Throwable {
        selectedrow = DTOFactory.createduplicatesettingmasterDTO();
        selectedrow = (duplicatesettingmasterDTOInter) e.getObject();
        dSDL = (List<duplicatesettingdetailDTOInter>) DSDBOEngin.GetListOfRecords(selectedrow);
        showAdd = true;
        showConfirm = false;
    }

    public void fillDelete(ActionEvent e) {
        selectedrow = (duplicatesettingmasterDTOInter) e.getComponent().getAttributes().get("removedRow");
    }

    public void deleteRecord() throws Throwable {
        String deleteddetailedrecordcount = "", deletedrecordcount = "";
        if (selectedrow.getid() != 0) {
            deleteddetailedrecordcount = (String) DSDBOEngin.deleterecord(selectedrow);
            if (deleteddetailedrecordcount.contains("Has Been Deleted")) {
                deletedrecordcount = (String) DSMBOEngin.deleterecord(selectedrow);
                message = deletedrecordcount + " " + deleteddetailedrecordcount;
                dSML.remove(selectedrow);
                dSDL.clear();
                showAdd = true;
                showConfirm = false;
            }
        } else {
            dSML.remove(selectedrow);
            dSDL.clear();
            showAdd = true;
            showConfirm = false;
            message = "Record Has Been Removed";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void fillDelete1(ActionEvent e) {
        selecteddetailrow = (duplicatesettingdetailDTOInter) e.getComponent().getAttributes().get("removedRow1");
    }

    public void deleteRecord1() throws Throwable {
        message1 = (String) DSMBOEngin.deleterecordbyid(selecteddetailrow);
        if (message1.contains("Has Been Deleted")) {
            dSDL.remove(selecteddetailrow);
            showAdd1 = true;
            showConfirm1 = false;
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }

    public void addnetwork() {
        newrecord = DTOFactory.createduplicatesettingmasterDTO();
        dSML.add(0, newrecord);
        selectedrow = newrecord;
        showAdd = false;
        showConfirm = true;
        dSDL.clear();
    }

    public void confirmAdd() throws Throwable {
        message = (String) DSMBOEngin.insertrecord(newrecord);
        if (message.contains("has been inserted")) {
            showAdd = true;
            showConfirm = false;
        } else {
            message = "Can't Duplicate Record With Same Network & Record Type";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void addcol2() {
        newrecord1 = DTOFactory.createduplicatesettingdetailDTO();
        dSDL.add(0, newrecord1);
        newrecord1.setid(selectedrow.getid());
        showAdd1 = false;
        showConfirm1 = true;
    }

    public void confirmAdd1() throws Throwable {

        message1 = (String) DSDBOEngin.insertrecord(newrecord1);
        if (message1.contains("Column Has Been")) {
            showAdd1 = true;
            showConfirm1 = false;
        }

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }

    public void saveAll() throws Throwable {
        try {
            message = (String) DSMBOEngin.save(dSML);
        } catch (Exception ex) {
            message = "Can't Duplicate Record With Same Network & Record Type";
        }
        message = "Records are Updated";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void saveAll1() throws Throwable {
        message1 = (String) DSDBOEngin.save(dSDL);
        message1 = "Records are Updated";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }
}
