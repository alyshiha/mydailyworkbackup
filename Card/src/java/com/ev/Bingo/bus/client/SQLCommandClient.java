/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import DBCONN.CoonectionHandler;
import com.ev.Bingo.base.dao.BaseDAO;
 
import static com.ev.Bingo.bus.client.SQLCommandClient.toList;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Aly
 */
public class SQLCommandClient extends BaseDAO {

    private List resultSetData, colNames;
    private List<String> ColoumnList, tableList;
    private int ColCount, RowsCount;
    private String sqlstatament, message, tableName;

    public List getResultSetData() {
        return resultSetData;
    }

    public void setResultSetData(List resultSetData) {
        this.resultSetData = resultSetData;
    }

    public List getColNames() {
        return colNames;
    }

    public void setColNames(List colNames) {
        this.colNames = colNames;
    }

    public List<String> getColoumnList() {
        return ColoumnList;
    }

    public void setColoumnList(List<String> ColoumnList) {
        this.ColoumnList = ColoumnList;
    }

    public List<String> getTableList() {
        return tableList;
    }

    public void setTableList(List<String> tableList) {
        this.tableList = tableList;
    }

    public int getColCount() {
        return ColCount;
    }

    public void setColCount(int ColCount) {
        this.ColCount = ColCount;
    }

    public int getRowsCount() {
        return RowsCount;
    }

    public void setRowsCount(int RowsCount) {
        this.RowsCount = RowsCount;
    }

    public String getSqlstatament() {
        return sqlstatament;
    }

    public void setSqlstatament(String sqlstatament) {
        this.sqlstatament = sqlstatament;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SQLCommandClient() throws Throwable {
        resultSetData = new ArrayList();

        colNames = new ArrayList();
        tableList = findAllTables();
    }

    public static final List toList(ResultSet rs, List wantedColumnNames) throws SQLException {
        List rows = new ArrayList();

        int numWantedColumns = wantedColumnNames.size();
        while (rs.next()) {
            Map row = new LinkedHashMap();

            for (int i = 0; i < numWantedColumns; ++i) {
                String columnName = (String) wantedColumnNames.get(i);
                Object value = rs.getObject(columnName);
                row.put(columnName, value);
            }

            rows.add(row);
        }

        return rows;
    }

    public int ColumnCount(ResultSet resultset) throws Throwable {
        return resultset.getMetaData().getColumnCount();
    }

    public List ColumnNames(ResultSet resultset, int numcols) throws Throwable {
        List colNames = new ArrayList();
        List resultSetData = new ArrayList();
        for (int i = 1; i < numcols; i++) {
            colNames.add(resultset.getMetaData().getColumnName(i));
        }
        return colNames;
    }

    public List<String> findAllTables() throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select distinct t.TABLE_NAME from all_tab_columns t where t.user = 'CARDS'";
        ResultSet result = executeQuery(selectStat);
        List<String> Tables = new ArrayList<String>();
        while (result.next()) {
            Tables.add(result.getString(1));
        }
        super.postSelect(result);
        return Tables;
    }

    public List<String> findAllColoumns(String TableName) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select t.COLUMN_NAME,t.DATA_TYPE from all_tab_columns t where t.user = 'CARDS' and t.TABLE_NAME = '$TName'";
        selectStat = selectStat.replace("$TName", "" + TableName);
        ResultSet result = executeQuery(selectStat);
        List<String> Col = new ArrayList<String>();
        while (result.next()) {
            Col.add(result.getString(1));
        }
        super.postSelect(result);
        return Col;
    }

    public String GetVariable(List<String> Col) {
        String var = "", VarName = "", DataType = "";
        String[] ColDetail = new String[3];

        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            VarName = ColDetail[0].toLowerCase();
            DataType = ColDetail[1].toUpperCase();
            if (DataType.equals("NUMBER")) {
                if (i != Col.size() - 1) {
                    var = var + "$" + VarName + ",";
                } else {
                    var = var + "$" + VarName;
                }
            } else if (DataType.equals("VARCHAR2")) {
                if (i != Col.size() - 1) {
                    var = var + "'$" + VarName + "',";
                } else {
                    var = var + "'$" + VarName + "'";
                }
            } else if (DataType.equals("DATE")) {
                if (i != Col.size() - 1) {
                    var = var + "'$" + VarName + "',";
                } else {
                    var = var + "'$" + VarName + "'";
                }
            }
        }
        return var;
    }

    public void ExcuteCallableStatment() {
        try {
            super.preCollable();
            CallableStatement stat = super.executeCallableStatment("{call" + sqlstatament + "}");
            stat.execute();
            super.postCollable(stat);
            
        } catch (SQLException ex) {
            Logger.getLogger(SQLCommandClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SQLCommandClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void ExcuteBatch() {
        try {
            super.preUpdate();
            message = super.executeUpdate(sqlstatament);
            message = message + " Row Has Been Effected";
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } catch (SQLException ex) {
            message = ex.getMessage();
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } catch (Exception ex) {
            Logger.getLogger(SQLCommandClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void SearchResults() {
        try {
            super.preSelect();
            ResultSet resultset = executeQuery(sqlstatament);//from DB      
            ColCount = ColumnCount(resultset);
            colNames = ColumnNames(resultset, ColCount);
            resultSetData = toList(resultset, colNames);
            RowsCount = resultSetData.size();
            super.postSelect(resultset);
        } catch (SQLException ex) {
            message = ex.getMessage();
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        } catch (Throwable ex) {
            message = ex.getMessage();
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }

    public List<String> findAllColoumnsandType(String TableName) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "select t.COLUMN_NAME,t.DATA_TYPE from all_tab_columns t where t.user = 'CARDS' and t.TABLE_NAME = '$TName'";
        selectStat = selectStat.replace("$TName", "" + TableName);
        ResultSet result = executeQuery(selectStat);
        List<String> Col = new ArrayList<String>();
        while (result.next()) {
            Col.add(result.getString(1).replace("_", "").toLowerCase() + "-" + result.getString(2) + "-" + result.getString(1));
        }
        super.postSelect(result);
        return Col;
    }

    public String GetColoumnName(List<String> Col) {
        String var = "", ColName = "";
        String[] ColDetail = new String[3];

        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            ColName = ColDetail[2];
            if (i != Col.size() - 1) {
                var = var + ColName + ",";
            } else {
                var = var + ColName;
            }
        }
        return var;
    }

    public String GetUpdateStatment(List<String> Col) {
        String var = "", ColName = "", VarName = "", DataType = "";
        String[] ColDetail = new String[3];

        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            ColName = ColDetail[2];
            VarName = ColDetail[0].toLowerCase();
            DataType = ColDetail[1].toUpperCase();
            if (DataType.equals("NUMBER")) {
                if (i != Col.size() - 1) {
                    var = var + ColName + "= $" + VarName + ",";
                } else {
                    var = var + ColName + "= $" + VarName;
                }
            } else if (DataType.equals("VARCHAR2")) {
                if (i != Col.size() - 1) {
                    var = var + ColName + "= '$" + VarName + "',";
                } else {
                    var = var + ColName + "= '$" + VarName + "'";
                }
            } else if (DataType.equals("DATE")) {
                if (i != Col.size() - 1) {
                    var = var + ColName + "= '$" + VarName + "',";
                } else {
                    var = var + ColName + "= '$" + VarName + "'";
                }
            }
        }
        return var;
    }

    public List<String> findAllKeys(String TableName) throws Throwable {
        super.preSelect();
        // databse insert statement usinf conn pool getConn().createStatement();
        String selectStat = "SELECT cols.column_name,(select t.DATA_TYPE from all_tab_columns t where t.user = 'CARDS' and t.TABLE_NAME = cons.TABLE_NAME and t.COLUMN_NAME = cols.COLUMN_NAME) DataType"
                + " FROM all_constraints cons, all_cons_columns cols"
                + " WHERE cons.constraint_type = 'P'"
                + " AND cons.constraint_name = cols.constraint_name"
                + " AND cons.user = cols.user"
                + " AND cons.user = 'CARDS'"
                + " AND cons.TABLE_NAME = '$TName'";
        selectStat = selectStat.replace("$TName", "" + TableName);
        ResultSet result = executeQuery(selectStat);
        List<String> Col = new ArrayList<String>();
        while (result.next()) {
            Col.add(result.getString(1) + "-" + result.getString(2));
        }
        super.postSelect(result);
        return Col;
    }

    public String GetKeys(String Table) throws Throwable {

        List<String> Col = new ArrayList<String>();
        Col = findAllKeys(Table);
        String var = "";
        String ColName = "";
        String VarName = "";
        String DataType = "";
        String[] ColDetail = new String[3];
        for (int i = 0; i < Col.size(); i++) {
            ColDetail = Col.get(i).split("-");
            ColName = ColDetail[0];
            DataType = ColDetail[1].toUpperCase();
            VarName = ColName.replace("_", "").toLowerCase();
            if (DataType.equals("NUMBER")) {
                if (i != Col.size() - 1) {
                    var = var + ColName + "= $" + VarName + "AND";
                } else {
                    var = var + ColName + "= $" + VarName;
                }
            } else if (DataType.equals("VARCHAR2")) {
                if (i != Col.size() - 1) {
                    var = var + ColName + "= '$" + VarName + "'AND";
                } else {
                    var = var + ColName + "= '$" + VarName + "'";
                }
            } else if (DataType.equals("DATE")) {
                if (i != Col.size() - 1) {
                    var = var + ColName + "= '$" + VarName + "'AND";
                } else {
                    var = var + ColName + "= '$" + VarName + "'";
                }
            }
        }
        return var;

    }

    public void getResults() {
        if (sqlstatament.toUpperCase().contains("INSERT") || sqlstatament.toUpperCase().contains("UPDATE") || sqlstatament.toUpperCase().contains("DELETE")) {
            ExcuteBatch();
        } else {
            SearchResults();
        }
    }

    public void generateSelectStat() {
        try {
            sqlstatament = "";
            colNames = findAllColoumns(tableName);
            for (int i = 0; i < colNames.size(); i++) {
                String Col = colNames.get(i).toString();
                sqlstatament = sqlstatament + Col + ",";
            }
            sqlstatament = sqlstatament.substring(0, sqlstatament.length() - 1);
            sqlstatament = "SELECT " + sqlstatament.toUpperCase() + " FROM " + tableName.toUpperCase();
        } catch (Throwable ex) {
            message = ex.getMessage();
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }

    public void genarateInsertStat() {
        String ColoumnInsert, ColoumnVariable;
        try {
            sqlstatament = "";
            List<String> Col = findAllColoumnsandType(tableName);
            String InsertColName = GetColoumnName(Col);
            String ColVariable = GetVariable(Col);
            sqlstatament = "insert into " + tableName + " ( " + InsertColName + " ) " + " values " + " ( " + ColVariable + " ) ";
            sqlstatament = sqlstatament.toUpperCase();
        } catch (Throwable ex) {
            message = ex.getMessage();
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }

    public void genarateUpdateStat() {
        try {
            sqlstatament = "";
            List<String> Col = findAllColoumnsandType(tableName);
            String UpdateColName = GetUpdateStatment(Col);
            String UpdateColVariABLE = GetKeys(tableName);
            sqlstatament = "update " + tableName + " set  " + UpdateColName + " where " + UpdateColVariABLE;
            sqlstatament = sqlstatament.toUpperCase();
        } catch (Throwable ex) {
            message = ex.getMessage();
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }

    public void genarateDeleteStat() {
        try {
            sqlstatament = "";
            String UpdateColVariABLE = GetKeys(tableName);
            sqlstatament = "delete from  " + tableName + " where " + UpdateColVariABLE;
            sqlstatament = sqlstatament.toUpperCase();
        } catch (Throwable ex) {
            message = ex.getMessage();
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(message));
        }
    }

    public void changeTable(ValueChangeEvent e) {
        tableName = (String) e.getNewValue();
    }
}
