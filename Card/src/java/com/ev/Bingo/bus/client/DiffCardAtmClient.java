/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.DiffCardAtmBOInter;
import com.ev.Bingo.bus.dto.ATMMACHINECASHDTOINTER;
import com.ev.Bingo.bus.dto.DiffCardAtmJournalDTOInter;
import com.ev.Bingo.bus.dto.DiffCardAtmTransJournalDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author AlyShiha
 */
public class DiffCardAtmClient extends BaseBean {

    private DiffCardAtmBOInter BOEngin;
    private List<DiffCardAtmJournalDTOInter> masterRecords;
    private List<DiffCardAtmTransJournalDTOInter> detailRecords;
    private List<ATMMACHINECASHDTOINTER> atmRecords;
    private DiffCardAtmJournalDTOInter[] selectedRow;
    private List<String> indicationRecords;
    private String selectedIndication;
    private List<String> journalRecords;
    private String selectedjournal;
    private Date dateFrom, dateTo;
    private String atmid;
    private int masterListCount, detailListCount;

    public DiffCardAtmBOInter getBOEngin() {
        return BOEngin;
    }

    public void setBOEngin(DiffCardAtmBOInter BOEngin) {
        this.BOEngin = BOEngin;
    }

    public List<DiffCardAtmJournalDTOInter> getMasterRecords() {
        return masterRecords;
    }

    public void setMasterRecords(List<DiffCardAtmJournalDTOInter> masterRecords) {
        this.masterRecords = masterRecords;
    }

    public List<DiffCardAtmTransJournalDTOInter> getDetailRecords() {
        return detailRecords;
    }

    public void setDetailRecords(List<DiffCardAtmTransJournalDTOInter> detailRecords) {
        this.detailRecords = detailRecords;
    }

    public List<ATMMACHINECASHDTOINTER> getAtmRecords() {
        return atmRecords;
    }

    public void setAtmRecords(List<ATMMACHINECASHDTOINTER> atmRecords) {
        this.atmRecords = atmRecords;
    }

    public List<String> getJournalRecords() {
        return journalRecords;
    }

    public void setJournalRecords(List<String> journalRecords) {
        this.journalRecords = journalRecords;
    }

    public String getSelectedjournal() {
        return selectedjournal;
    }

    public void setSelectedjournal(String selectedjournal) {
        this.selectedjournal = selectedjournal;
    }

    public DiffCardAtmJournalDTOInter[] getSelectedRow() {
        return selectedRow;
    }

    public void setSelectedRow(DiffCardAtmJournalDTOInter selectedRow[]) {
        this.selectedRow = selectedRow;
    }

    public List<String> getIndicationRecords() {
        return indicationRecords;
    }

    public void setIndicationRecords(List<String> indicationRecords) {
        this.indicationRecords = indicationRecords;
    }

    public String getSelectedIndication() {
        return selectedIndication;
    }

    public void setSelectedIndication(String selectedIndication) {
        this.selectedIndication = selectedIndication;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getAtmid() {
        return atmid;
    }

    public void setAtmid(String atmid) {
        this.atmid = atmid;
    }

    public int getMasterListCount() {
        return masterListCount;
    }

    public void setMasterListCount(int masterListCount) {
        this.masterListCount = masterListCount;
    }

    public int getDetailListCount() {
        return detailListCount;
    }

    public void setDetailListCount(int detailListCount) {
        this.detailListCount = detailListCount;
    }

    public DiffCardAtmClient() throws Throwable {
        super();
        BOEngin = BOFactory.createDiffCardAtmBO();
        masterRecords = new ArrayList<DiffCardAtmJournalDTOInter>();
        detailRecords = new ArrayList<DiffCardAtmTransJournalDTOInter>();
        indicationRecords = new ArrayList<String>();
        journalRecords = new ArrayList<String>();
        atmRecords = new ArrayList<ATMMACHINECASHDTOINTER>();
        atmid = "All";
        if (isSunday()) {
            dateTo = new Date();
            dateFrom = super.getBeforYesterdaytrans((String) "00:00:00");
        } else {
            dateTo = new Date();
            dateFrom = super.getYesterdaytrans((String) "00:00:00");
        }
        indicationRecords.add("VISA_MATCHED");
        indicationRecords.add("123_MATCHED");
        selectedIndication = "All";
        journalRecords.add("CARD > ATM");
        journalRecords.add("ATM > CARD");
        selectedjournal = "All";
        selectedRow = null;
        masterListCount = 0;
        detailListCount = 0;
        atmRecords = (List<ATMMACHINECASHDTOINTER>) BOEngin.findMachineAll();
        masterRecords = (List<DiffCardAtmJournalDTOInter>) BOEngin.findMasterRecords(atmid, selectedIndication, selectedjournal, dateFrom, dateTo);
        masterListCount = masterRecords.size();
    }

    public void handleMasterChange(SelectEvent e) throws Throwable {
        for (DiffCardAtmJournalDTOInter record : selectedRow) {
            record.setFlag(0);
        }
        ((DiffCardAtmJournalDTOInter) e.getObject()).setFlag(1);
        detailRecords = (List<DiffCardAtmTransJournalDTOInter>) BOEngin.findDetailRecords("" + ((DiffCardAtmJournalDTOInter) (e.getObject())).getDiffid());
        detailListCount = detailRecords.size();
    }

    public void onRowUnSelect(UnselectEvent e) {
        ((DiffCardAtmJournalDTOInter) e.getObject()).setFlag(0);
        for (DiffCardAtmJournalDTOInter record : selectedRow) {
            record.setFlag(0);
        }
        detailRecords = null;
        detailListCount = 0;
    }

    public Boolean isSunday() {
        // Get calendar set to current date and time
        Calendar c = Calendar.getInstance();
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public void doSearch() throws Throwable {
        masterRecords = (List<DiffCardAtmJournalDTOInter>) BOEngin.findMasterRecords(atmid, selectedIndication, selectedjournal, dateFrom, dateTo);
        masterListCount = masterRecords.size();
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void exportexcel() throws Throwable {
        if (selectedRow.length > 0) {
            String path = (String) BOEngin.exportexcel(selectedRow);
            prepDownload(path);
        }
    }

    public void exportpdf() throws Throwable {
        if (selectedRow.length > 0) {
            String path = (String) BOEngin.GetReport(super.GetUser().getUsername(), dateFrom, dateTo, super.GetClient(), atmid, selectedjournal, selectedIndication, selectedRow);
            prepDownload(path);
        }
    }

    public void selectAllRows() {
        DataTable repTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(":form1:mat_typ_dt");
        repTable.setSelection(masterRecords.toArray());
        for (DiffCardAtmJournalDTOInter record : selectedRow) {
            record.setFlag(0);
        }
        detailRecords = null;
        detailListCount = 0;
    }

    public void deselectAllRows() {
        for (DiffCardAtmJournalDTOInter record : selectedRow) {
            record.setFlag(0);
        }
        selectedRow = null;
        detailRecords = null;
        detailListCount = 0;
    }

    public void recalc() throws Throwable {

        BOEngin.recalc();

    }
}
