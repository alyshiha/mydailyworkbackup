
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.AccountNameBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.AccountNameDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Aly
 */
@ManagedBean(name = "accountNameView")
@ViewScoped
public class AccountNameClient extends BaseBean implements Serializable {

    private AccountNameDTOInter selectedRecord, newRecord;
    private List<AccountNameDTOInter> recordsList;
    private int listcount;
    private String atmname;
    private AccountNameBOInter engin;
    private List<String> privelageList;

    public AccountNameDTOInter getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(AccountNameDTOInter selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public AccountNameDTOInter getNewRecord() {
        return newRecord;
    }

    public void setNewRecord(AccountNameDTOInter newRecord) {
        this.newRecord = newRecord;
    }

    public List<AccountNameDTOInter> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<AccountNameDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public String getAtmname() {
        return atmname;
    }

    public void setAtmname(String atmname) {
        this.atmname = atmname;
    }

    public AccountNameBOInter getEngin() {
        return engin;
    }

    public void setEngin(AccountNameBOInter engin) {
        this.engin = engin;
    }

    @PostConstruct
    public void init() {
        try {
            atmname = "";
            engin = BOFactory.createAccountName(null);
            recordsList = engin.getNetworks();
            listcount = recordsList.size();
            privelageList = super.findPrivelage(super.GetUser().getUserid(), "AccountName.xhtml");
        } catch (Throwable ex) {
            Logger.getLogger(AccountNameClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public void searchrecords() {
        try {
            if (atmname == null) {
                atmname = "";
            }
            recordsList = engin.getRecord(atmname);
            listcount = recordsList.size();
        } catch (Throwable ex) {
            Logger.getLogger(AccountNameClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean insertPriv() {
        return super.findPrivelageOperation(privelageList, "INSERT");
    }

    public void addRecord() {
        newRecord = DTOFactory.createAccountNameDTO();
    }

    public void saveRecord() throws SQLException, Throwable {
        String message = "";
        if (!engin.findDuplicateRecord(newRecord.getSymbol())) {
            message = engin.insertNetworks(newRecord);
            searchrecords();
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("carDialog2.hide();");

        } else {
            message = "can`t Duplicate Account Name";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean updatePriv() {
        return super.findPrivelageOperation(privelageList, "UPDATE");
    }

    public void updateRecord() throws SQLException, Throwable {
        String message = "";
        if (!engin.findDuplicatesaveRecord(selectedRecord.getSymbol(), selectedRecord.getId())) {

            message = engin.updateNetworks(selectedRecord);
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("carDialog.hide();");

        } else {
            message = "can`t Duplicate Account Name";
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public Boolean deletePriv() {
        return super.findPrivelageOperation(privelageList, "DELETE");
    }

    public void deleteRecord() throws SQLException {
        String message = engin.deleteNetworks(selectedRecord);
        if (message.contains("Succesfully")) {
            recordsList.remove(selectedRecord);
            listcount = recordsList.size();
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", message));
    }

    public List<String> completeContains(String query) {
        List<String> filtered = new ArrayList<String>();
        for (int i = 0; i < recordsList.size(); i++) {
            AccountNameDTOInter rec = recordsList.get(i);
            if (rec.getName().toLowerCase().contains(query.toLowerCase())) {
                filtered.add(rec.getName());
            }
        }
        return filtered;
    }

    public Boolean ValidateAccount(String AccountNumber) {
        if (AccountNumber.trim().length() == 16) {
            if (AccountNumber.trim().substring(0, 3).matches("[A-Z]*")) {
                if (AccountNumber.trim().substring(3, 16).matches("\\d+")) {
                    return Boolean.TRUE;
                }
            }
        }
        return Boolean.FALSE;
    }

    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public void printreportPDF() throws Exception, Throwable {

        String path = (String) engin.printPDFrep(super.GetClient(), super.GetUser().getUsername(), atmname);
        prepDownload(path);
    }
}
