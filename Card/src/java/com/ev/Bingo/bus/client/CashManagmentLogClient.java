/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;


import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.CashManagmentLogBOInter;
import com.ev.Bingo.bus.dto.CashManagmentLogDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;

@ManagedBean(name = "cashManagmentLogView")
@ViewScoped
public class CashManagmentLogClient extends BaseBean implements Serializable {

    private List<CashManagmentLogDTOInter> recordsList;
    private List<usersDTOInter> usersList;
    private Date fromDate, toDate;
    private int userID;
    private CashManagmentLogBOInter engin;
    private int listcount;
    private List<String> privelageList;

    public List<CashManagmentLogDTOInter> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<CashManagmentLogDTOInter> recordsList) {
        this.recordsList = recordsList;
    }

    public List<usersDTOInter> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<usersDTOInter> usersList) {
        this.usersList = usersList;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public CashManagmentLogBOInter getEngin() {
        return engin;
    }

    public void setEngin(CashManagmentLogBOInter engin) {
        this.engin = engin;
    }

    
    public int getListcount() {
        return listcount;
    }

    public void setListcount(int listcount) {
        this.listcount = listcount;
    }

    public List<String> getPrivelageList() {
        return privelageList;
    }

    public void setPrivelageList(List<String> privelageList) {
        this.privelageList = privelageList;
    }

    @PostConstruct
    public void init() {
        try {
            toDate = super.getYesterdaytrans((String) "00:00:00");
            fromDate = super.getTodaytrans((String) "00:00:00");
            userID = 0;
            usersList = super.GetUsers();
            engin = BOFactory.createCashManagmentLogBO(null);
            recordsList = new ArrayList<CashManagmentLogDTOInter>();
            listcount = recordsList.size();
            privelageList = super.findPrivelage(super.GetUser().getUserid(), "CashManagmentLog.xhtml");
        } catch (Throwable ex) {
            Logger.getLogger(CashManagmentLogClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean searchPriv() {
        return super.findPrivelageOperation(privelageList, "SEARCH");
    }

    public void searchrecords() {
        try {
            recordsList = engin.findRecord(userID, fromDate, toDate);
            listcount = recordsList.size();
        } catch (Throwable ex) {
            
        }
    }
    private DefaultStreamedContent download;

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public DefaultStreamedContent getDownload() throws Exception {
        return download;
    }

    public void prepDownload(String Path) throws Exception {
        File file = new File(Path);
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        setDownload(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
    }

    public String searchUserName(Integer UserID) {
        for (usersDTOInter userRecord : usersList) {
            if (userRecord.getUserid()== UserID) {
                return userRecord.getUsername();
            }
        }
        return "All Users";
    }

    public void printreportPDF() throws Exception, Throwable {

        String path = (String) engin.printPDFrep(userID, fromDate, toDate, super.GetClient(), searchUserName(userID), super.GetUser().getUsername());
        prepDownload(path);
    }

    public Boolean datesPriv() {
        if (super.findPrivelageOperation(privelageList, "SEARCH_DATE")) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }
}
