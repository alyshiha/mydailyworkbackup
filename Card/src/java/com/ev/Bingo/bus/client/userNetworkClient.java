
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.UserNetworkBOInter;
import com.ev.Bingo.bus.bo.networksBOInter;
import com.ev.Bingo.bus.bo.usersBOInter;
import com.ev.Bingo.bus.dto.DTOFactory;
import com.ev.Bingo.bus.dto.UserNetworkDTOInter;
import com.ev.Bingo.bus.dto.networksDTOInter;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Administrator
 */
public class userNetworkClient extends BaseBean {

    private Boolean showAdd, showAdd1, showConfirm, showConfirm1;
    private String message, message1;
    private List<networksDTOInter> nL;
    private List<UserNetworkDTOInter> unL;
    private networksBOInter nEngin;
    private usersBOInter UBOEngin;
    private UserNetworkBOInter UNBOEngin;
    private List<usersDTOInter> uL;
    private usersDTOInter selectedrow;
    private UserNetworkDTOInter selectedrowdetaildelete, newrecord1, selectedrowdetailinsert;

    public Boolean getShowAdd() {
        return showAdd;
    }

    public void setShowAdd(Boolean showAdd) {
        this.showAdd = showAdd;
    }

    public Boolean getShowAdd1() {
        return showAdd1;
    }

    public void setShowAdd1(Boolean showAdd1) {
        this.showAdd1 = showAdd1;
    }

    public Boolean getShowConfirm() {
        return showConfirm;
    }

    public void setShowConfirm(Boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    public Boolean getShowConfirm1() {
        return showConfirm1;
    }

    public void setShowConfirm1(Boolean showConfirm1) {
        this.showConfirm1 = showConfirm1;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public List<networksDTOInter> getnL() {
        return nL;
    }

    public void setnL(List<networksDTOInter> nL) {
        this.nL = nL;
    }

    public List<UserNetworkDTOInter> getUnL() {
        return unL;
    }

    public void setUnL(List<UserNetworkDTOInter> unL) {
        this.unL = unL;
    }

    public networksBOInter getnEngin() {
        return nEngin;
    }

    public void setnEngin(networksBOInter nEngin) {
        this.nEngin = nEngin;
    }

    public usersBOInter getUBOEngin() {
        return UBOEngin;
    }

    public void setUBOEngin(usersBOInter UBOEngin) {
        this.UBOEngin = UBOEngin;
    }

    public List<usersDTOInter> getuL() {
        return uL;
    }

    public void setuL(List<usersDTOInter> uL) {
        this.uL = uL;
    }

    public usersDTOInter getSelectedrow() {
        return selectedrow;
    }

    public void setSelectedrow(usersDTOInter selectedrow) {
        this.selectedrow = selectedrow;
    }

    public UserNetworkDTOInter getSelectedrowdetaildelete() {
        return selectedrowdetaildelete;
    }

    public void setSelectedrowdetaildelete(UserNetworkDTOInter selectedrowdetaildelete) {
        this.selectedrowdetaildelete = selectedrowdetaildelete;
    }

    public UserNetworkDTOInter getNewrecord1() {
        return newrecord1;
    }

    public void setNewrecord1(UserNetworkDTOInter newrecord1) {
        this.newrecord1 = newrecord1;
    }

    public UserNetworkDTOInter getSelectedrowdetailinsert() {
        return selectedrowdetailinsert;
    }

    public void setSelectedrowdetailinsert(UserNetworkDTOInter selectedrowdetailinsert) {
        this.selectedrowdetailinsert = selectedrowdetailinsert;
    }

    public userNetworkClient() throws Throwable {
        super();
        GetAccess();
        UBOEngin = BOFactory.createusersBO();
        uL = (List<usersDTOInter>) UBOEngin.GetAllRecords();
        nEngin = BOFactory.createnetworksBO();
        nL = (List<networksDTOInter>) nEngin.GetAllRecords();
        unL = new ArrayList<UserNetworkDTOInter>();
        UNBOEngin = BOFactory.createUserNetworkBO();
        showAdd = true;
        showConfirm = false;
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void onRowSelect(SelectEvent e) throws Throwable {
        selectedrow = DTOFactory.createusersDTO();
        selectedrow = (usersDTOInter) e.getObject();
        unL = (List<UserNetworkDTOInter>) UNBOEngin.GetListOfRecordsCS(selectedrow);
        showAdd1 = true;
        showConfirm1 = false;
    }

    public void fillDelete1(ActionEvent e) {
        selectedrowdetaildelete = (UserNetworkDTOInter) e.getComponent().getAttributes().get("removedRow1");
    }

    public void deleteRecord1() throws Throwable {
        message1 = (String) UNBOEngin.deleterecord(selectedrowdetaildelete);
        unL.remove(selectedrowdetaildelete);
        showAdd1 = true;
        showConfirm1 = false;

        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }

    public void addcol2() {
        newrecord1 = DTOFactory.createUserNetworkDTO();
        unL.add(0, newrecord1);
        newrecord1.setUserid(selectedrow.getUserid());
        selectedrowdetailinsert = newrecord1;
        showAdd1 = false;
        showConfirm1 = true;
    }

    public void confirmAdd1() throws Throwable {
        message1 = "";
        if (newrecord1.getUserid() != 0) {
            for (UserNetworkDTOInter record : unL) {
                if (record.getNetworkid() == newrecord1.getNetworkid() && record.getUserid() == newrecord1.getUserid() && null != record.getRowid()) {
                    message1 = "can not duplicate Network at the same User";
                }
            }
            if (!message1.contains("can not duplicate Network at the same User")) {
                newrecord1.setRowid("newrec");
                message1 = (String) UNBOEngin.insertrecord(newrecord1);
                if (message1.contains("Has Been")) {
                    showAdd1 = true;
                    showConfirm1 = false;
                }
            }
        } else {
            message1 = "Please Select User Name";
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }

    public void saveAll1() throws Throwable {
        message1 = (String) UNBOEngin.save(unL);
        message1 = "Records are Updated";
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message1));
    }
}
