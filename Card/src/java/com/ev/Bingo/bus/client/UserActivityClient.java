/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ev.Bingo.bus.client;

import com.ev.Bingo.base.client.BaseBean;
import com.ev.Bingo.bus.bo.BOFactory;
import com.ev.Bingo.bus.bo.UserActivityBOInter;
import com.ev.Bingo.bus.dto.UserActivityDTOInter;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

/**
 *
 * @author Aly
 */
public class UserActivityClient extends BaseBean {

    private CartesianChartModel dispModel;
    private CartesianChartModel settModel;
    private UserActivityBOInter BOengin;

    public UserActivityBOInter getBOengin() {
        return BOengin;
    }

    public void setBOengin(UserActivityBOInter BOengin) {
        this.BOengin = BOengin;
    }

    public CartesianChartModel getDispModel() {
        return dispModel;
    }

    public CartesianChartModel getSettModel() {
        return settModel;
    }

    public UserActivityClient() {
        super();
        GetAccess();
        BOengin = BOFactory.createUserActivityBO();
        createModel();
    }

    public void itemSelect(ItemSelectEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item selected",
                "Item Index: " + event.getItemIndex() + ", Series Index:" + event.getSeriesIndex());

        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    private void createModel() {
        try {
            dispModel = new CartesianChartModel();
            settModel = new CartesianChartModel();
            ChartSeries Settelt = new ChartSeries();
            Settelt.setLabel("Setteled");
            ChartSeries Dispute = new ChartSeries();
            Dispute.setLabel("Pending");
            List<UserActivityDTOInter> AllRecords = (List<UserActivityDTOInter>) BOengin.GetListOfRecords(super.GetUser().getUserid());
            for (UserActivityDTOInter Record : AllRecords) {
                if ("D".equals(Record.getType())) {
                    Dispute.set(Record.getUsername(), Record.getTranscount());
                }
                if ("S".equals(Record.getType())) {
                    Settelt.set(Record.getUsername(), Record.getTranscount());
                }
            }
            dispModel.addSeries(Dispute);
            settModel.addSeries(Settelt);
        } catch (Throwable ex) {
            Logger.getLogger(UserActivityClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
