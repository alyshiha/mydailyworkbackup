/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBCONN;

import com.ev.Bingo.base.dao.BaseDAO;
import com.ev.Bingo.bus.dto.usersDTOInter;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AlyShiha
 */
public class ConnectionMap implements Serializable{

    private Boolean state;
    private String time;

   
    public ConnectionMap(Boolean state, String time) {
        this.state = state;
        this.time = time;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    

}
