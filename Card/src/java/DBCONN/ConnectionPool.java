/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBCONN;

import com.ev.Bingo.base.util.PropertyReader;
import java.io.Serializable;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

public class ConnectionPool implements Serializable {

//    private Hashtable connections;
//    private int increment, initialConnections;
    private String dbURL, user, password;
    private DataSource ds;
private int i = 0;
    public ConnectionPool() {

        // Load the specified driver class
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup(PropertyReader.getProperty("ev.db_datasource"));
        } catch (NamingException ex) {
            Logger.getLogger(ConnectionPool.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public synchronized Connection getConnection(Integer UserID) throws SQLException, InterruptedException {
i++;
        Thread.sleep(100);
        return ds.getConnection();

    }

    public synchronized Connection getConnection() throws SQLException, InterruptedException {
        i++;
        Thread.sleep(100);
        return ds.getConnection();

    }

    public synchronized void returnConnection(Connection returned) {
        try {
            i--;
           
            Thread.sleep(100);
            returned.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (InterruptedException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public synchronized void returnAllConnection() {

    }
}
