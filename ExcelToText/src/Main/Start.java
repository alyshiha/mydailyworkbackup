/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import static Main.Start.Properties;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import xlsx.prop.PropDTO;
import xlsx.prop.ReadPropertiesXmlFile;

/**
 *
 * @author AlyShiha
 */
public class Start extends Thread {

    static PropDTO Properties = ReadPropertiesXmlFile.readProperties();

    public static void main(String[] args) {
        System.setProperty("logfile.name", Properties.getLogFolder());
        Engin EnginStarter = new Engin();
        EnginStarter.Start();
    }
}
