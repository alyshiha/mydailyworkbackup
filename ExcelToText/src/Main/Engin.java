/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.commons.io.FilenameUtils;
import xlsx.prop.PropDTO;
import xlsx.prop.ReadPropertiesXmlFile;
import xlsx.util.FilesCollection;

public class Engin {

    static PropDTO Properties = ReadPropertiesXmlFile.readProperties();
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Engin.class);

    public void Start() {

        try {
            String[] FromFolder = Properties.getFromFolder().split(";");
            String[] ToFolder = Properties.getToFolder().split(";");
            String[] BackupFolder = Properties.getBackUpFolder().split(";");
            logger.error("1");
            for (int i = 0; i < FromFolder.length; i++) {
                if (new File(FromFolder[i]).listFiles().length > 0) {
                    logger.error("2");
                    File tempfile = new File(FromFolder[i] + "\\vbtest.vbs");
                    // if file doesnt exists, then create it
                    logger.error("3");
                    if (!tempfile.exists()) {
                        tempfile.createNewFile();
                    }
                    String content = "Dim oExcel\n"
                            + "Set oExcel = CreateObject(\"Excel.Application\")\n"
                            + "Dim oBook\n"
                            + "Set oBook = oExcel.Workbooks.Open(Wscript.Arguments.Item(0))\n"
                            + "oBook.SaveAs WScript.Arguments.Item(1), 6\n"
                            + "oBook.Close False\n"
                            + "oExcel.Quit";
                    logger.error("4");
                    FileWriter fw = new FileWriter(tempfile.getAbsoluteFile());
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write(content);
                    bw.close();
                    for (File file : new File(FromFolder[i]).listFiles()) {
                        if (!FilenameUtils.removeExtension(file.getName()).equals("vbtest")) {
                            String processcomm = "\"C:\\Windows\\SysWOW64\\cscript.exe\" \"" + FromFolder[i] + "\\vbtest.vbs\" \"" + file.getAbsolutePath() + "\" \"" + ToFolder[i] + "\\" + FilenameUtils.removeExtension(file.getName()) + ".csv\"";
                            logger.error(processcomm);
                            logger.error("5");
                            Process process = Runtime.getRuntime().exec(processcomm);
                            logger.error("6");
                            process.waitFor();
                            FilesCollection FC = new FilesCollection();
                            FC.CreateDirectory(BackupFolder[i]);
                            FC.MoveFile(file.getAbsolutePath());
                        }
                    }
                    tempfile.deleteOnExit();
                }
            }
            
        } catch (FileNotFoundException e) {
            logger.error("File not found: " + e.getMessage());
        } catch (IOException e) { // catch all IOExceptions not handled by previous catch blocks
            logger.error("General I/O exception: " + e.getMessage());
            e.printStackTrace();
        } catch (InterruptedException ex) {
            logger.error("Failed to compute sum");
        }
    }
}
