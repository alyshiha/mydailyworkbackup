/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xlsx.util;



import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.dc.pr.PathStroker;

/**
 *
 * @author Aly
 */
public class FilesCollection {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FilesCollection.class);
    private String FinalPath;

    public void MoveFile(String FromFile) {
        File afile = new File(FromFile);
        File tofile = new File(FinalPath + "\\" + afile.getName());
        if (afile.renameTo(tofile)) {
            logger.info("File is moved successful!");
        } else {
            logger.info(FromFile);
            logger.info(FinalPath + "\\" + afile.getName());
            logger.error("File is failed to move!");
        }

    }

    public void CreateDirectory(String Path) {
        File files = new File(CreateDirectoryPath(Path));
        if (!files.exists()) {
            if (files.mkdirs()) {
                logger.info("Multiple directories are created!");
            } else {
                logger.error("Failed to create multiple directories!");
            }
        }
        FinalPath = files.getPath();
    }

    public String CreateDirectoryPath(String Path) {
        // create a calendar
        Calendar cal = Calendar.getInstance();
        // get the value of all the calendar date fields.
        Path = Path + "\\" + cal.get(Calendar.YEAR) + "\\" + (cal.get(Calendar.MONTH) + 1) + "\\" + cal.get(Calendar.DATE);
        return Path;
    }

   
}
