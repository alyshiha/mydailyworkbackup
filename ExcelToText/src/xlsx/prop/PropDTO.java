/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xlsx.prop;

/**
 *
 * @author AlyShiha
 */
public class PropDTO {

    private String FromFolder;
    private String ToFolder;
    private String BackUpFolder;
    private String LogFolder;

    public PropDTO(String FromFolder, String ToFolder, String BackUpFolder,String LogFolder) {
        this.FromFolder = FromFolder;
        this.ToFolder = ToFolder;
        this.BackUpFolder = BackUpFolder;
        this.LogFolder = LogFolder;
    }

    public String getLogFolder() {
        return LogFolder;
    }

    public void setLogFolder(String LogFolder) {
        this.LogFolder = LogFolder;
    }

    public String getBackUpFolder() {
        return BackUpFolder;
    }

    public void setBackUpFolder(String BackUpFolder) {
        this.BackUpFolder = BackUpFolder;
    }

    public String getFromFolder() {
        return FromFolder;
    }

    public void setFromFolder(String FromFolder) {
        this.FromFolder = FromFolder;
    }

    public String getToFolder() {
        return ToFolder;
    }

    public void setToFolder(String ToFolder) {
        this.ToFolder = ToFolder;
    }

}
