/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.mq.datamodel;

import java.util.Date;
import java.util.Map;

/**
 *
 * @author shi7a
 */
public class OutputDefinition {

    private String runningfile;
    private String filetype;
    private Date parsingtime;
    private String extension;
    private String extra;
    private Map<String, String> Header;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public Map<String, String> getHeader() {
        return Header;
    }

    public void setHeader(Map<String, String> Header) {
        this.Header = Header;
    }

    public String getRunningfile() {
        return runningfile;
    }

    public void setRunningfile(String runningfile) {
        this.runningfile = runningfile;
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public Date getParsingtime() {
        return parsingtime;
    }

    public void setParsingtime(Date parsingtime) {
        this.parsingtime = parsingtime;
    }

}
