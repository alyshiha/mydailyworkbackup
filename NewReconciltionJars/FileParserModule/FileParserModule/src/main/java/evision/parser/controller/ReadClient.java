/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.parser.controller;

import evision.mq.datamodel.OutputDefinition;
import evision.mq.service.RabbitMqConfig;
import evision.parser.datamodel.PathDefinition;
import evision.parser.service.FileReader;
import evision.parser.util.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.utils.SerializationUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author Aly
 */
public class ReadClient {

    private static FileReader Reader;
    private static RabbitTemplate rabbitTemplate;
    private static Integer synchronizedCounter;

    public ReadClient() {

        ApplicationContext ctx = new AnnotationConfigApplicationContext(RabbitMqConfig.class);
        rabbitTemplate = ctx.getBean(RabbitTemplate.class);
    }
    private static Hashtable<String, File> TempFiles = new Hashtable<String, File>();

    private static Boolean MultiTemplateStarter(PathDefinition RunningTemplate, String RunningFile) throws IOException {
        Boolean MultiTemplate = Boolean.FALSE;
        String[] templates = RunningTemplate.getFiletemplate().split(";");
        for (int i = 0; i < templates.length; i++) {
            String template = templates[i];
            if (i != 0) {
                File file = new File(new File(RunningFile).getParent() + "\\Temp" + i + ".txt");
                file.createNewFile();
                TempFiles.put(template, new File(file.getPath()));
                MultiTemplate = Boolean.TRUE;
            } else {
                TempFiles.put(template, new File(RunningFile));
            }
        }
        return MultiTemplate;
    }

    private static void clearFiles() {
        Iterator it = TempFiles.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (pair.getValue().toString().contains("Temp")) {
                ((File) pair.getValue()).delete();
            }
            it.remove(); // avoids a ConcurrentModificationException
        }
    }

    public static void run(OutputDefinition MSG) {
        try {
            PathDefinition running = new PathDefinition();
            List<PathDefinition> PathDefinitionList = (List<PathDefinition>) XStreamTranslator.getInstance().toObject(new File(".\\ConfigFiles\\PathSetup.xml"));
            for (PathDefinition PathDefinitionRecord : PathDefinitionList) {
                if (PathDefinitionRecord.getFiletype().equals(MSG.getFiletype())) {
                    running = PathDefinitionRecord;
                }
            }

            String filename = new File(MSG.getRunningfile()).getName();
            rabbitTemplate.setRoutingKey(RabbitMqConfig.SIMPLE_MESSAGE_QUEUE_OUT);
            Boolean Multi = MultiTemplateStarter(running, MSG.getRunningfile());
            Iterator it = TempFiles.entrySet().iterator();
            int TempIndex = 0;
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                File TempFile = new File(new File(pair.getValue().toString()).getParent() + "\\Temp" + ++TempIndex + ".txt");
                Reader = new FileReader((File) pair.getValue(), pair.getKey().toString(), running.getFiletype());
                Reader.ConvertFile();
                while (!Reader.End) {
                    Object Trans = null;
                    if ((Trans = (Object) Reader.ReadTrans(Multi, TempFile)) != null) {
                        SyncList(Trans, filename, pair.getKey().toString(), running.getFiletype(), MSG);
                    }
                }
                Reader.Close();
            }
            clearFiles();
            rabbitTemplate.setRoutingKey(RabbitMqConfig.SIMPLE_MESSAGE_QUEUE_File);
            rabbitTemplate.convertAndSend(MSG);
        } catch (IOException ex) {
            Logger.getLogger(ReadClient.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void SyncList(Object Trans, String FileName, String TemplateName, String FileType, OutputDefinition MSG) {
        try {
            MessageProperties mProperties = new MessageProperties();
            mProperties.setHeader("templatename", TemplateName);
            mProperties.setHeader("filename", FileName);
            mProperties.setHeader("loadingtime", new Date());
            mProperties.setHeader("transid", java.util.UUID.randomUUID().toString());
            mProperties.setHeader("filetype", FileType);
            //loop a Map
            if (MSG.getHeader() != null) {
                for (Map.Entry<String, String> entry : MSG.getHeader().entrySet()) {
                    mProperties.setHeader(entry.getKey(), entry.getValue());
                }
            }

            Message msg = new Message(SerializationUtils.serialize(Trans), mProperties);
            rabbitTemplate.convertAndSend(msg);

        } catch (Exception ex) {
            Logger.getLogger(ReadClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
