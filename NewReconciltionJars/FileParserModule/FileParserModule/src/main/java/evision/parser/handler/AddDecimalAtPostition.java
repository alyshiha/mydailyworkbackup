/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.parser.handler;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.Properties;

import org.beanio.types.BigDecimalTypeHandler;
import org.beanio.types.ConfigurableTypeHandler;
import org.beanio.types.TypeConversionException;
import org.beanio.types.TypeHandler;

/**
 * Configurable type handler for BigDecimal that is a fixed width hard signed
 * (in first char) format. Parsing is delegated to existing
 * BigDecimalTypeHandler once input data and formats are "adjusted"
 */
public class AddDecimalAtPostition implements ConfigurableTypeHandler {

    private static final String DEFAULT_FORMAT = "###################.##";
    private int DECIMAL_POSITION = 2;

    private String format;
    private String padChar = "0";
    private int hardLength = 10;
    private BigDecimalTypeHandler bdHandler;

    public AddDecimalAtPostition() {
        bdHandler = new BigDecimalTypeHandler();
        bdHandler.setPattern(DEFAULT_FORMAT);
    }

    @Override
    public Object parse(String input) throws TypeConversionException {
        String text = input.trim();
        if (text == null || text.isEmpty()) {
            return null;
        }
        // Number format does not like an explicit '+' so nuke if there as redundant anyway.
        char firstCharacter = text.charAt(0);
        if (firstCharacter == '+') {
            text = text.substring(1);
        }
        return bdHandler.parse("" + new BigDecimal(new BigInteger(bdHandler.parse(text).toString()), Integer.valueOf(DECIMAL_POSITION)));
    }

    @Override
    public String format(Object value) {
        if (value == null) {
            return "";
        }
        DecimalFormat df = new DecimalFormat(DEFAULT_FORMAT);
        BigDecimal decimal = new BigDecimal(new BigInteger(df.format(value)), Integer.valueOf(DECIMAL_POSITION)).stripTrailingZeros();
        String decimalValue = bdHandler.format(decimal);
        if (decimal.compareTo(BigDecimal.ZERO) < 0) {
            return "-" + padRemovingNegative(decimalValue);
        } else {
            return "+" + padRemovingNegative(decimalValue);
        }
    }

    /**
     * Add the pad character to the beginning of the String to the hardLength-1
     * (-1 to account for sign character) first removing an included minus sign
     * if present.
     *
     * @param input The string to be padded.
     * @return The padded output.
     */
    public String padRemovingNegative(String input) {
        String output = input.startsWith("-") ? input.substring(1) : input;
        if (output.length() == hardLength - 1) {
            return output;
        }
        if (output.length() > hardLength - 1) {
            return output.substring(output.length() - hardLength - 1);
        }
        int diff = hardLength - 1 - output.length();
        for (int i = 0; i < diff; i++) {
            output = padChar + output;
        }
        return output;
    }

    /**
     * @see org.beanio.types.TypeHandler#getType()
     */
    @Override
    public Class<?> getType() {
        return BigDecimal.class;
    }

    /**
     * @see
     * org.beanio.types.ConfigurableTypeHandler#newInstance(java.util.Properties)
     */
    @Override
    public TypeHandler newInstance(Properties properties) {
        String formatProperty = properties.getProperty(FORMAT_SETTING);
        if (formatProperty == null || formatProperty.isEmpty()) {
            return this;
        }
        AddDecimalAtPostition handler = new AddDecimalAtPostition();
        // Format looks like this "S000000000" which is designed to parse/format a number like "+000001234" or
        // "-000001234". The length of the format is stored as the fixed width with the remaining format used for actual
        // parsing. The following line strips the "S" from the format the remainder of which should be a valid 'normal'
        // Number Format
        handler.setFormat(formatProperty.substring(1));
        hardLength = formatProperty.length();
        return handler;
    }

    /**
     * @return the format
     */
    public String getFormat() {
        return format;
    }

    /**
     * @param format the format to set
     */
    public void setFormat(String format) {
        if (format != null && !format.isEmpty()) {
            if (format.contains("D")) {
                this.DECIMAL_POSITION = Integer.parseInt(format.substring(format.toUpperCase().indexOf("D") - 1, format.toUpperCase().indexOf("D")));
                bdHandler.setPattern(format.replace("D", "").replace("" + this.DECIMAL_POSITION, ""));
            } else {
                this.format = format;
            }
        } else {
            this.format = DEFAULT_FORMAT;
            bdHandler.setPattern(format);
        }
    }

    /**
     * @return the hardLength
     */
    public int getHardLength() {
        return hardLength;
    }

    /**
     * @param hardLength the hardLength to set
     */
    public void setHardLength(int hardLength) {
        this.hardLength = hardLength;
    }
}
