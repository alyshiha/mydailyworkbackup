/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.parser.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.beanio.BeanIOConfigurationException;
import org.beanio.BeanIOException;
import org.beanio.BeanReader;
import org.beanio.BeanReaderException;
import org.beanio.BeanReaderIOException;
import org.beanio.InvalidRecordException;
import org.beanio.StreamFactory;

/**
 *
 * @author Aly
 */
public class FileReader {

    private StreamFactory factory = null;
    private BeanReader inReader = null;
    private File file;
    private String TemplateFile;
    private String TemplateName;
    public Boolean End;

    public FileReader(File file, String TemplateFile, String TemplateName) {
        this.file = file;
        this.TemplateFile = TemplateFile;
        this.TemplateName = TemplateName;
    }

    public void loadResource(String resource) {
        InputStream in = null;
        factory = StreamFactory.newInstance();
        try {
            in = new FileInputStream(resource);
            if (in == null) {
                throw new BeanIOConfigurationException("BeanIO mapping file '" + resource + "' not found on classpath");
            }
            factory.load(in);
        } catch (IOException ex) {
            throw new BeanIOException("Failed to load from the file system", ex);
        }
    }

    public BeanReader createReader(String name, File file) {

        if (!factory.isMapped(name)) {
            throw new IllegalArgumentException("No stream mapping configured for name '" + name + "'");
        }
        try {
            return factory.createReader(name, file);
        } catch (BeanReaderIOException ex) {
            throw new BeanReaderException("Failed to open file '" + file.getName() + "' for reading", ex);
        } catch (IllegalArgumentException ex) {
            throw ex;
        }
    }

    public void ConvertFile() {
        loadResource(TemplateFile);
        inReader = createReader(TemplateName, file);
        End = Boolean.FALSE;
    }

    public Object ReadTrans(Boolean MultiTemplate, File TempFilePath) {
        try {
            Map<?, ?> record = null;
            if ((record = (Map<?, ?>) inReader.read()) != null) {
                return record;
            } else {
                End = Boolean.TRUE;
            }
        } catch (InvalidRecordException ex) {
            if (ex.getContext().hasRecordErrors()) {
                for (String error : ex.getContext().getRecordErrors()) {
                    System.out.println("File Name:" + file.getName() + " " + error);
                }
            }
            if (ex.getContext().hasFieldErrors()) {
                for (String field : ex.getContext().getFieldErrors().keySet()) {
                    for (String error : ex.getContext().getFieldErrors(field)) {
                        System.out.println("File Name:" + file.getName() + " " + error + " at field " + field + " of Value " + ex.getContext().getFieldText(field) + " line number " + ex.getContext().getLineNumber());
                    }
                }
                if (MultiTemplate) {
                    try {
                        Files.write(TempFilePath.toPath(), ex.getContext().getRecordText().getBytes(), StandardOpenOption.APPEND);
                        Files.write(TempFilePath.toPath(), "\n".getBytes(), StandardOpenOption.APPEND);
                    } catch (IOException ex1) {
                        Logger.getLogger(FileReader.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                }
                System.out.println("Record Text: " + ex.getContext().getRecordText());
            }
        } catch (Exception ex) {
            System.out.println("Record Text: " + ex.getMessage());
        }
        return null;
    }

    public void Close() {
        inReader.close();
    }

}
