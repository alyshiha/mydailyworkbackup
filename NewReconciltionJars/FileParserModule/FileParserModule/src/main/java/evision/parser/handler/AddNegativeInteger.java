
        /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.parser.handler;

import java.util.Properties;
import org.beanio.types.ConfigurableTypeHandler;
import org.beanio.types.TypeConversionException;
import org.beanio.types.TypeHandler;

public class AddNegativeInteger implements ConfigurableTypeHandler {

    @Override
    public TypeHandler newInstance(Properties properties) throws IllegalArgumentException {
        return new AddNegativeDecimal();
    }

    //parse and format are not yet implemented. 
    @Override
    public Object parse(String input) throws TypeConversionException {
        String text = input.trim();
        if (text == null || text.isEmpty()) {
            return null;
        }
        return Integer.parseInt(text) * -1;
    }

    @Override
    public Class<?> getType() {
        return Integer.class;
    }

    public String format(Object o) {
        return o.toString();
    }
}
