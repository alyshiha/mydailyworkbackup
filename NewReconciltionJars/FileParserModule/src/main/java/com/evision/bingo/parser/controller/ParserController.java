/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.parser.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.ParserConfig;
import com.evision.bingo.core.model.ParserConfigurable;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.parser.model.ParserMapper;
import com.evision.bingo.parser.service.ParserService;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.JsonMessageConverter;

/**
 *
 * @since 22/6/2017
 * @author Khaled Khalil
 *
 * Parser Controller
 */
public class ParserController extends BaseParserController {

    private ParserService paserService = null;

    public ParserController(ParserConfig parserConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<ParserConfigurable> subConfigsList, List<BaseController> eventsHandlerList) {
        super(new LogHandler(parserConfig.getLoggerPathList(), systemLogger), new QueueHandler(parserConfig.getQueuesList(), rabbitTemplatesMap), parserConfig, subConfigsList, eventsHandlerList);

        paserService = new ParserService(queueHandler, logHandler, eventsHandlerList);
    }

    @Override
    synchronized boolean doCollect() {
        if (getObj() != null) {
            JsonMessageConverter jsonConverter = new JsonMessageConverter();
            QueueMessage message = (QueueMessage) jsonConverter.fromMessage((Message) getObj());

            ParserMapper parserMapper = null;

            //find the matching mapper
            for (ParserConfigurable configurable : subConfigList) {
                if (((ParserMapper) configurable).getFileType().equals(message.getFiletype())) {
                    parserMapper = (ParserMapper) configurable;
                    break;
                }
            }

            if (parserMapper != null) {
                return paserService.parse(parserMapper, message);
            }
        }
        return false;
    }

    @Override
    public void stop() {
        //do nothing
    }
}
