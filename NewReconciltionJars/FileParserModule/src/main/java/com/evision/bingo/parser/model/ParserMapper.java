/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.parser.model;

import com.evision.bingo.core.model.ParserConfigurable;

/**
 *
 * @since 22/6/2017
 * @author Aly Shiha
 *
 * Parser Mapping Config
 */
public class ParserMapper implements ParserConfigurable {

    private String fileType;
    private String fileTemplate;

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileTemplate() {
        return fileTemplate;
    }

    public void setFileTemplate(String fileTemplate) {
        this.fileTemplate = fileTemplate;
    }
}
