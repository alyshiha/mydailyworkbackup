/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.parser.handler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.beanio.types.ConfigurableTypeHandler;
import org.beanio.types.TypeConversionException;
import org.beanio.types.TypeHandler;

/**
 * @since 22/6/2017
 * @author Aly Shiha
 *
 */
public class MultiDateiFormat implements ConfigurableTypeHandler {

    private String[] DATE_FORMATS;

    public String[] getDATE_FORMATS() {
        return DATE_FORMATS;
    }

    public void setDATE_FORMATS(String[] DATE_FORMATS) {
        this.DATE_FORMATS = DATE_FORMATS;
    }

    @Override
    public TypeHandler newInstance(final Properties properties) throws IllegalArgumentException {
        MultiDateiFormat handler = null;
        try {
            handler = (MultiDateiFormat) clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(MultiDateiFormat.class.getName()).log(Level.SEVERE, null, ex);
        }
        setDATE_FORMATS(properties.getProperty("Formats").split(";"));
        return handler;
    }

    //parse and format are not yet implemented. 
    @Override
    public Object parse(final String input) throws TypeConversionException {
        String text = input.trim();
        if (text == null || text.isEmpty()) {
            return null;
        }
        for (String formatString : DATE_FORMATS) {
            try {
                return new SimpleDateFormat(formatString).parse(text);
            } catch (ParseException e) {
            }
        }
        return "invalid format";
    }

    public Class<?> getType() {
        return Date.class;
    }

    public String format(Object o) {
        return o.toString();
    }
}
