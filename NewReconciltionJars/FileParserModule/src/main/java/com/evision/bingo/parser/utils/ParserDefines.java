/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.parser.utils;

/**
 * @since 18/5/2017
 * @author Khaled Khalil
 *
 * Constants of the converter system
 */
public class ParserDefines {
    public static final String QUEUE_ACTION_SOURCE = "Queue";
}