/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.parser.handler;

import java.util.Properties;
import org.beanio.types.ConfigurableTypeHandler;
import org.beanio.types.TypeHandler;

/**
 * @since 22/6/2017
 * @author Aly Shiha
 *
 */
public class CutStringAtLength implements ConfigurableTypeHandler {

    private int cutFrom;
    private int cutTo;

    @Override
    public TypeHandler newInstance(final Properties properties) throws IllegalArgumentException {

        final String cutFromStr = properties.getProperty("cutFrom");
        if (cutFromStr == null || "".equals(cutFromStr)) {
            return this;
        }
        final int cutFrom = Integer.parseInt(cutFromStr);
        if (cutFrom == this.cutFrom) {
            return this;
        }

        final String cutToStr = properties.getProperty("cutTo");
        if (cutToStr == null || "".equals(cutToStr)) {
            return this;
        }
        final int cutTo = Integer.parseInt(cutToStr);
        if (cutTo == this.cutTo) {
            return this;
        }

        try {
            final CutStringAtLength handler = (CutStringAtLength) clone();
            handler.setCutFrom(cutFrom);
            handler.setCutTo(cutTo);
            return handler;
        } catch (final CloneNotSupportedException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public String parse(final String text) {
        String result = text;
        if (result != null && result.length() > cutTo && cutFrom < cutTo) {
            result = result.substring(cutFrom, cutTo);
        }
        return result;
    }

    public int getCutFrom() {
        return cutFrom;
    }

    public void setCutFrom(int cutFrom) {
        this.cutFrom = cutFrom;
    }

    public int getCutTo() {
        return cutTo;
    }

    public void setCutTo(int cutTo) {
        this.cutTo = cutTo;
    }

    public String format(Object o) {
        return o.toString();
    }

    public Class<?> getType() {
        return String.class;
    }
}
