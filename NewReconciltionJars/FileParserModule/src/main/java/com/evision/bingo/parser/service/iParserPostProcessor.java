/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.parser.service;

import com.evision.bingo.core.controller.BaseController;

/**
 * @since 22/6/2017
 * @author Khaled Khalil
 *
 * base interface for post-processors
 */
public interface iParserPostProcessor {
    public boolean process(BaseController baseController);
}
