/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.parser.service;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.DBLoaderConfig;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.parser.model.ParserMapper;
import java.util.List;

/**
 * @since 22/6/2017
 * @author Khaled Khalil
 *
 */
public abstract class BaseParserService {

    protected LogHandler logHandler;
    protected QueueHandler queueHandler;
    protected List<BaseController> eventsHandlerList;

    public BaseParserService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        this.queueHandler = queueHandler;
        this.logHandler = logHandler;
        this.eventsHandlerList = eventsHandlerList;
    }

    public abstract boolean parse(ParserMapper parserMapper, QueueMessage message);

    public abstract boolean connect(DBLoaderConfig config);

    public abstract void disconnect();

    public void submitEvent(EventModel event) {
        for (BaseController controller : eventsHandlerList) {
            controller.handleRequest(event);
        }
    }

    public LogHandler getLogHandler() {
        return logHandler;
    }

    public void setLogHandler(LogHandler logHandler) {
        this.logHandler = logHandler;
    }

    public QueueHandler getQueueHandler() {
        return queueHandler;
    }

    public void setQueueHandler(QueueHandler queueHandler) {
        this.queueHandler = queueHandler;
    }

    public List<BaseController> getEventsHandlerList() {
        return eventsHandlerList;
    }

    public void setEventsHandlerList(List<BaseController> eventsHandlerList) {
        this.eventsHandlerList = eventsHandlerList;
    }
}
