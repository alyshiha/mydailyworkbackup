/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.parser.handler;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.beanio.types.ConfigurableTypeHandler;
import org.beanio.types.TypeConversionException;

/**
 * @since 22/6/2017
 * @author Aly Shiha
 *
 */
public class DateFormats implements ConfigurableTypeHandler {

    private String pattern = null;
    private String[] date_format;

    public String[] getDate_format() {
        return date_format;
    }

    public void setDate_format(String[] date_format) {
        this.date_format = date_format;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public Date parse(String text) throws TypeConversionException {
        if ("".equals(text)) {
            return null;
        }
        for (String formatString : date_format) {
            ParsePosition pp = new ParsePosition(0);
            Date date = new SimpleDateFormat(formatString).parse(text, pp);
            if (date != null) {
                return date;
            }
        }
        throw new TypeConversionException("Invalid date Formats");
    }

    public String format(Object value) {
        return value.toString();
    }

    public DateFormats newInstance(Properties properties) throws IllegalArgumentException {
        String pattern = properties.getProperty(FORMAT_SETTING);
        if (pattern == null || "".equals(pattern)) {
            return this;
        }
        if (pattern.equals(this.pattern)) {
            return this;
        }
        DateFormats handler = new DateFormats();
        handler.setDate_format(pattern.split(";"));
        return handler;
    }

    @Override
    public Class<?> getType() {
        return Date.class;
    }

}
