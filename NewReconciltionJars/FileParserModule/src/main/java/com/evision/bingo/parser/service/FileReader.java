/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.parser.service;

import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import org.beanio.BeanReader;
import org.beanio.InvalidRecordException;
import org.beanio.StreamFactory;

/**
 *
 * @since 2/7/2017
 * @author Aly Shiha
 *
 * File Reader Service
 */
public class FileReader {

    private File file;
    private final String templateFile;
    private final String templateName;
    private boolean end;
    private final LogHandler logHandler;
    private StreamFactory factory = null;
    private BeanReader inReader = null;

    public FileReader(LogHandler logHandler, File file, String templateFile, String templateName) {
        this.file = file;
        this.templateFile = templateFile;
        this.templateName = templateName;
        this.logHandler = logHandler;
    }

    public void convertFile() throws IOException {
        loadResource(templateFile);
        inReader = createReader(templateName, file);
        end = false;
    }

    private void loadResource(String resource) throws FileNotFoundException, IOException {
        InputStream in = null;
        factory = StreamFactory.newInstance();
        in = new FileInputStream(resource);
        if (in == null) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(String.format(LogMessages.getMessage(LogMessages.PARSER_FAIL_IN_READING_MAPPER_FILE), resource)));
        } else {
            factory.load(in);
        }
    }

    private BeanReader createReader(String name, File file) {
        if (!factory.isMapped(name)) {
            throw new IllegalArgumentException(String.format(LogMessages.getMessage(LogMessages.PARSER_NO_STREAM_MAPPING), name));
        }
        return factory.createReader(name, file);
    }

    public boolean isEnd() {
        return end;
    }

    public void close() {
        inReader.close();
    }

    public Object readTrans() {
        try {
            Map<?, ?> record = null;
            if ((record = (Map<?, ?>) inReader.read()) != null) {
                return record;
            } else {
                end = true;
            }
        } catch (InvalidRecordException ex) {
            if (ex.getContext().hasRecordErrors()) {
                for (String error : ex.getContext().getRecordErrors()) {
                    logHandler.logException("File Name:" + file.getName() + " " + error, ex);
                }
            }
            if (ex.getContext().hasFieldErrors()) {
                for (String field : ex.getContext().getFieldErrors().keySet()) {
                    for (String error : ex.getContext().getFieldErrors(field)) {
                        logHandler.logException("File Name:" + file.getName() + " " + error + " at field " + field + " of Value " + ex.getContext().getFieldText(field) + " line number " + ex.getContext().getLineNumber(), ex);
                    }
                }
            }
        }
        return null;
    }
}