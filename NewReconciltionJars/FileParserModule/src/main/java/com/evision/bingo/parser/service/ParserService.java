/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.parser.service;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.DBLoaderConfig;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.parser.model.ParserMapper;
import com.evision.bingo.parser.utils.ParserDefines;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.amqp.core.MessageProperties;

/**
 *
 * @since 2/7/2017
 * @author Aly Shiha
 *
 * Parser Service
 */
public class ParserService extends BaseParserService {

    private List<String> templateFiles;
    private FileReader reader;
    private ObjectMapper oMapper = new ObjectMapper();

    public ParserService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, eventsHandlerList);
    }

    @Override
    public boolean parse(ParserMapper parserMapper, QueueMessage message) {

        templateFiles = new ArrayList();

        Calendar cal = Calendar.getInstance();
        Date time = cal.getTime();

        EventModel eventModel = new EventModel();
        eventModel.setTime(new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(time));
        eventModel.setAction(EventModel.EVENT.FILE_PARSE);
        eventModel.setActionDetails(message.getRunningfile());
        eventModel.setActionSource(ParserDefines.QUEUE_ACTION_SOURCE);

        try {
            String[] templates = parserMapper.getFileTemplate().split(";");
            String templatesStr = "";
            for (int i = 0; i < templates.length; i++) {
                templateFiles.add(templates[i]);

                templatesStr += templates[i];
                if (i != templateFiles.size() - 1) {
                    templatesStr += ",";
                }
            }
            logHandler.logInfo(String.format(LogMessages.getMessage(LogMessages.PARSER_PARSE_FILE), message.getRunningfile(), templatesStr));

            Iterator iterator = templateFiles.iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                reader = new FileReader(logHandler, new File(message.getRunningfile()), key, parserMapper.getFileType());
                reader.convertFile();
                while (!reader.isEnd()) {
                    Object transactionObj = null;
                    if ((transactionObj = reader.readTrans()) != null) {
                        syncList(transactionObj, message.getRunningfile(), key, message);
                    }
                }
                reader.close();
            }
            eventModel.setActionStatus(EventModel.STATUS.SUCCESS);
        } catch (IOException ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
            eventModel.setActionStatus(EventModel.STATUS.FAIL);
        } catch (Exception ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
            eventModel.setActionStatus(EventModel.STATUS.FAIL);
        } finally {
            submitEvent(eventModel);
            if (eventModel.getActionStatus().equals(EventModel.STATUS.SUCCESS)) {
                return true;
            } else {
                return false;
            }
        }
    }

    private void syncList(Object trans, String fileName, String templateName, QueueMessage message) {
        try {
            String dateFields = "";

            for (Object key : ((Map) trans).keySet()) {
                if (((Map) trans).get(key) != null && ((Map) trans).get(key).getClass().toString().toLowerCase().contains("date")) {
                    dateFields += key + ",";
                }
            }

            MessageProperties mProperties = new MessageProperties();
            mProperties.setHeader("templateName", templateName);
            mProperties.setHeader("fileName", fileName);
            mProperties.setHeader("loadingTime", new Date());
            mProperties.setHeader("transId", java.util.UUID.randomUUID().toString());
            mProperties.setHeader("fileType", message.getFiletype());
            mProperties.setHeader("dateFields", dateFields);
            //loop a Map
            if (message.getHeader() != null) {
                for (Object entry : message.getHeader().entrySet()) {
                    mProperties.setHeader(((Map.Entry) entry).getKey().toString(), ((Map.Entry) entry).getValue());
                }
            }

            queueHandler.sendMessage(trans, mProperties);

        } catch (Exception ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
        }
    }

    @Override
    public boolean connect(DBLoaderConfig config) {
        return true;
    }

    @Override
    public void disconnect() {
    }
}
