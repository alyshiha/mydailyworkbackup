/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.parser.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.ParserConfig;
import com.evision.bingo.core.model.ParserConfigurable;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.parser.service.iParserPostProcessor;
import com.evision.bingo.parser.service.iParserPreProcessor;
import java.util.ArrayList;
import java.util.List;

/**
 * @since 22/6/2017
 * @author Khaled Khalil
 *
 * This is the base class of parsers from different types to bingo readable
 */
public abstract class BaseParserController extends BaseController {

    //collection of registered pre-processors
    private List<iParserPreProcessor> registeredPreProcessors;

    //collection of registered post-processors
    private List<iParserPostProcessor> registeredPostProcessors;

    protected List<ParserConfigurable> subConfigList = new ArrayList();

    BaseParserController(LogHandler logHandler, QueueHandler queueHandler, ParserConfig parserConfig, List<ParserConfigurable> subConfigList, List<BaseController> eventsHandlerList) {
        super(logHandler, queueHandler, parserConfig, eventsHandlerList);

        this.subConfigList = subConfigList;

        init();
    }

    // init processors collections
    @Override
    public final void init() {
        registeredPreProcessors = new ArrayList<>();
        registeredPostProcessors = new ArrayList<>();
    }

    //force clear memory     
    @Override
    public final void destroy() {
        registeredPreProcessors.clear();
        registeredPostProcessors.clear();

        registeredPreProcessors = null;
        registeredPostProcessors = null;
    }

    //register a pre-processor
    final void registerPreProcessor(iParserPreProcessor preProcessor) {
        registeredPreProcessors.add(preProcessor);
    }

    //unregister a pre-processor
    final void unregisterPreProcessor(iParserPreProcessor preProcessor) {
        registeredPreProcessors.remove(preProcessor);
    }

    //register a post-processor
    final void registerPostProcessor(iParserPostProcessor postProcessor) {
        registeredPostProcessors.add(postProcessor);
    }

    //unregister a post-processor
    final void unregisterPostProcessor(iParserPostProcessor postProcessor) {
        registeredPostProcessors.remove(postProcessor);
    }

    //main load function that forces the work chain
    @Override
    public synchronized final boolean handleRequest(Object obj) {
        this.setObj(obj);
        if (preCollect()) {
            if (doCollect()) {
                return postCollect();
            }
        }
        return false;
    }

    //call of all registered pre-processors
    synchronized final boolean preCollect() {
        if (!registeredPreProcessors.isEmpty()) {
            for (iParserPreProcessor preProcessor : registeredPreProcessors) {
                if (!preProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //call of all registered post-processors
    synchronized final boolean postCollect() {
        if (!registeredPostProcessors.isEmpty()) {
            for (iParserPostProcessor postProcessor : registeredPostProcessors) {
                if (!postProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //method of collect to be overidden
    abstract boolean doCollect();
}
