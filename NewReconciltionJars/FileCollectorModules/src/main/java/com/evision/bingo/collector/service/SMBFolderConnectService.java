/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.collector.service;

import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.collector.model.SMBFolderConfig;
import com.evision.bingo.collector.utils.CollectorDefines;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.CollectorConfigurable;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.core.utils.Utils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

/**
 * @since 1/4/2017
 * @author Aly Shiha
 *
 * This is an SMB tool to connect to SMB folders to get files copied from them
 */
public class SMBFolderConnectService extends BaseConnectService {

    public SMBFolderConnectService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, eventsHandlerList);
    }

    @Override
    public boolean connect(CollectorConfigurable params) {
        return true;
    }

    @Override
    public void disconnect() {

    }

    @Override
    public void processFiles(CollectorConfigurable config) {
        try {
            NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, ((SMBFolderConfig) config).getUsername(), ((SMBFolderConfig) config).getPassword()); //This class stores and encrypts NTLM user credentials.

            SmbFile[] files = new SmbFile(((SMBFolderConfig) config).getSourcefolder(), auth).listFiles(); //This class represents a resource on an SMB network.
            readFileContent(files, ((SMBFolderConfig) config));
        } catch (MalformedURLException | SmbException e) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), e);
        }
    }

    private boolean readFileContent(SmbFile[] files, SMBFolderConfig setting) {
        String destFilename;
        FileOutputStream fileOutputStream;
        InputStream fileInputStream;
        byte[] buf;
        int len;
        
        if(files != null && files.length != 0){
            logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SMB_CONNECT) + Defines.SYSTEM_SPACE + setting.getSourcefolder());
        }
        
        for (SmbFile smbFile : files) {
            logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SMB_NEW_FILE_FOUND) + Defines.SYSTEM_SPACE + smbFile.getName());

            Calendar cal = Calendar.getInstance();
            Date time = cal.getTime();

            EventModel eventModel = new EventModel();
            eventModel.setTime(new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(time));
            eventModel.setAction(EventModel.EVENT.FILE_COLLECT);
            eventModel.setActionDetails(smbFile.getName());
            eventModel.setActionSource(setting.getSourcefolder());

            try {
                File destFolder = Utils.getDestinationFolder(setting.getDestinationfolder(), CollectorDefines.DEFAULT_SMB_COLLECTOR_DESITNATION_FOLDER);
                destFilename = destFolder.getPath() + Defines.SYSTEM_FILE_SEPARATOR + smbFile.getName();

                File destFile = new File(destFilename);
                if (destFile.exists()) {
                    logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(LogMessages.getMessage(LogMessages.COLLECTOR_EXISTING_FILE_IN_DISTINATION_FOLDER) + Defines.SYSTEM_SPACE + destFilename));
                    eventModel.setActionStatus(EventModel.STATUS.FAIL);
                } else {
                    fileOutputStream = new FileOutputStream(destFilename);
                    fileInputStream = smbFile.getInputStream();
                    logHandler.logInfo(String.format(LogMessages.getMessage(LogMessages.COLLECTOR_SMB_MOVE_FILE), smbFile.getName(), destFolder.getPath()));
                    buf = new byte[16 * 1024 * 1024];
                    while ((len = fileInputStream.read(buf)) > 0) {
                        fileOutputStream.write(buf, 0, len);
                    }
                    fileInputStream.close();
                    fileOutputStream.close();
                    smbFile.delete();

                    logHandler.logInfo(String.format(LogMessages.getMessage(LogMessages.COLLECTOR_SMB_ADD_FILE_TO_QUEUES), smbFile.getName()));
                    QueueMessage record = new QueueMessage();
                    record.setRunningfile(destFilename);
                    record.setFiletype(setting.getFiletype());
                    record.setTime(time);
                    record.setExtension(setting.getExtension().toLowerCase());
                    queueHandler.sendMessage(record);
                    eventModel.setActionStatus(EventModel.STATUS.SUCCESS);
                }
            } catch (SmbException | FileNotFoundException e) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), e);
                eventModel.setActionStatus(EventModel.STATUS.FAIL);
                return false;
            } catch (IOException e) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), e);
                eventModel.setActionStatus(EventModel.STATUS.FAIL);
                return false;
            } finally {
                submitEvent(eventModel);
            }
        }
        return true;
    }
}
