/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.evision.bingo.collector.service;

import com.evision.bingo.collector.model.SFTPConfig;
import com.evision.bingo.collector.utils.CollectorDefines;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.CollectorConfigurable;
import com.evision.bingo.core.model.EventModel;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.core.utils.Utils;
import java.io.File;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

/**
 * @since 1/4/2017
 * @author Aly Shiha
 *
 * This is the SFTP Service
 */
public class SFTPConnectService extends BaseConnectService {

    public SFTPConnectService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, eventsHandlerList);
    }

    private JSch jsch = null;
    private Session session = null;
    private Channel channel = null;
    private ChannelSftp c = null;

    /**
     * Connect to the server and do some commands.
     *
     * @param param SFTPConfig to listen to
     * @return
     */
    @Override
    public boolean connect(CollectorConfigurable params) {
        try {
            jsch = new JSch();
            session = jsch.getSession(((SFTPConfig) params).getLogin(), ((SFTPConfig) params).getServer(), ((SFTPConfig) params).getPort());
            session.setPassword(((SFTPConfig) params).getPassword().getBytes(Charset.forName("ISO-8859-1")));
            Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);

            session.connect();

            // Initializing a channel
            channel = session.openChannel(CollectorDefines.SYSTEM_DEFAULT_SFTP_CHANNEL_NAME);
            channel.connect();
            c = (ChannelSftp) channel;
            return true;
        } catch (JSchException e) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), e);
        }
        return false;
    }

    @Override
    public void processFiles(CollectorConfigurable config) {
        if (c == null || session == null || !session.isConnected() || !c.isConnected()) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), null);
        }

        try {
            c.cd(((SFTPConfig) config).getSourcefolder());
            Vector<ChannelSftp.LsEntry> list = c.ls("*");
            for (ChannelSftp.LsEntry entry : list) {
                if (!entry.getFilename().contains(CollectorDefines.SYSTEM_SFTP_READ_FILE_POSTFIX)) {

                    logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SFTP_NEW_FILE_FOUND) + Defines.SYSTEM_SPACE + entry.getFilename());

                    Calendar cal = Calendar.getInstance();
                    Date time = cal.getTime();

                    EventModel eventModel = new EventModel();
                    eventModel.setTime(new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(time));
                    eventModel.setAction(EventModel.EVENT.FILE_COLLECT);
                    eventModel.setActionDetails(entry.getFilename());
                    eventModel.setActionSource(CollectorDefines.SFTP + Defines.SYSTEM_SPACE + ((SFTPConfig) config).getSourcefolder());

                    try {
                        //create destination path if not found
                        File desFolder = Utils.getDestinationFolder(((SFTPConfig) config).getDestinationfolder(), CollectorDefines.DEFAULT_SFTP_COLLECTOR_DESITNATION_FOLDER);
                        File desFile = new File(desFolder.getPath() + Defines.SYSTEM_FILE_SEPARATOR + entry.getFilename());
                        if (desFile.exists()) {
                            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(LogMessages.getMessage(LogMessages.COLLECTOR_EXISTING_FILE_IN_DISTINATION_FOLDER) + Defines.SYSTEM_SPACE + desFolder.getPath()));
                            eventModel.setActionStatus(EventModel.STATUS.FAIL);
                        } else {
                            boolean fileFound = true;
                            try {
                                c.ls(entry.getFilename() + CollectorDefines.SYSTEM_SFTPKEY_READ_FILE_POSTFIX);
                            } catch (SftpException ex) {
                                fileFound = false;
                            }
                            if (fileFound) {
                                //existing file in the source with the same name after renaming
                                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(LogMessages.getMessage(LogMessages.COLLECTOR_EXISTING_FILE_IN_SOURCE_FOLDER) + Defines.SYSTEM_SPACE + entry.getFilename() + CollectorDefines.SYSTEM_SFTPKEY_READ_FILE_POSTFIX));
                                eventModel.setActionStatus(EventModel.STATUS.FAIL);
                            } else {
                                c.get(entry.getFilename(), desFolder.getPath() + Defines.SYSTEM_FILE_SEPARATOR + entry.getFilename());
                                logHandler.logInfo(String.format(LogMessages.getMessage(LogMessages.COLLECTOR_SFTP_COPY_FILE), entry.getFilename(), desFolder.getPath()));

                                c.rename(entry.getFilename(), entry.getFilename() + CollectorDefines.SYSTEM_SFTP_READ_FILE_POSTFIX);
                                QueueMessage record = new QueueMessage();
                                record.setRunningfile(desFolder.getPath() + Defines.SYSTEM_FILE_SEPARATOR + entry.getFilename());
                                record.setFiletype(((SFTPConfig) config).getFiletype());
                                record.setTime(new Date());
                                record.setExtension(((SFTPConfig) config).getExtension().toLowerCase());
                                logHandler.logInfo(String.format(LogMessages.getMessage(LogMessages.COLLECTOR_SFTP_ADD_FILE_TO_QUEUES), entry.getFilename()));
                                queueHandler.sendMessage(record);
                                eventModel.setActionStatus(EventModel.STATUS.SUCCESS);
                            }
                        }
                    } catch (SftpException ex) {
                        logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                        eventModel.setActionStatus(EventModel.STATUS.FAIL);
                    } finally {
                        submitEvent(eventModel);
                    }
                }
            }
        } catch (SftpException e) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), e);
        }
    }

    @Override
    public void disconnect() {
        if (c != null) {
            c.disconnect();
        }
        if (channel != null) {
            channel.disconnect();
        }
        if (session != null) {
            session.disconnect();
        }
    }
}
