/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.collector.controller;

import com.evision.bingo.collector.model.LocalFolderConfig;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.collector.service.LocalFolderConnectService;
import com.evision.bingo.collector.utils.CollectorDefines;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.model.CollectorConfigurable;
import com.evision.bingo.core.model.CollectorConfig;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @since 1/4/2017
 * @author Aly Shiha
 *
 * This is the controller to collect files from local folders
 */
public final class LocalFolderCollectorController extends BaseCollectorController {

    private ScheduledExecutorService service = null;

    public LocalFolderCollectorController(CollectorConfig collectorConfig, CollectorConfigurable localFolderConfigRecord, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<BaseController> eventsHandlerList) {
        super(new LogHandler(collectorConfig.getLoggerPathList(), systemLogger), new QueueHandler(collectorConfig.getQueuesList(), rabbitTemplatesMap), localFolderConfigRecord, eventsHandlerList);
    }

    @Override
    synchronized boolean doCollect() {

        logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_LOCAL_FOLDER_REGISTER_PATH) + Defines.SYSTEM_SPACE + ((LocalFolderConfig) config).getSourcefolder());
        Runnable runnable = new Runnable() {
            public void run() {
                //run the task of monitoring the shared folder
                new LocalFolderConnectService(queueHandler, logHandler, eventsHandlerList).processFiles((CollectorConfigurable) config);
            }
        };
        logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_START_LISTENING));
        service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(runnable, 0, CollectorDefines.COLLECTOR_SCHEDULER_PERIOD_IN_SECONDS, TimeUnit.SECONDS);
        return true;
    }

    @Override
    public void stop() {
        try {
            logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_STOP_LISTENING));
            service.shutdown();
            super.destroy();
        } catch (Exception ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
        }
    }
}