/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.collector.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.CollectorConfigurable;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.ArrayList;
import java.util.List;
import com.evision.bingo.collector.service.iCollectorPostProcessor;
import com.evision.bingo.collector.service.iCollectorPreProcessor;

/**
 * @since 20/4/2017
 * @author Khaled Khalil
 *
 * This is the base loader of files from different sources. it forces work chain
 * of preprocessor, load then postprocessor all messages received into different
 * files or event database tables
 */
public abstract class BaseCollectorController extends BaseController {

    //collection of registered pre-processors
    private List<iCollectorPreProcessor> registeredPreProcessors;

    //collection of registered post-processors
    private List<iCollectorPostProcessor> registeredPostProcessors;

    BaseCollectorController(LogHandler logHandler, QueueHandler queueHandler, CollectorConfigurable config, List<BaseController> eventsHandlerList) {
        super(logHandler, queueHandler, config, eventsHandlerList);

        init();
    }

    // init processors collections
    @Override
    public final void init() {
        registeredPreProcessors = new ArrayList<>();
        registeredPostProcessors = new ArrayList<>();
    }

    //force clear memory     
    @Override
    public final void destroy() {
        registeredPreProcessors.clear();
        registeredPostProcessors.clear();

        registeredPreProcessors = null;
        registeredPostProcessors = null;
    }

    //register a pre-processor
    final void registerPreProcessor(iCollectorPreProcessor preProcessor) {
        registeredPreProcessors.add(preProcessor);
    }

    //unregister a pre-processor
    final void unregisterPreProcessor(iCollectorPreProcessor preProcessor) {
        registeredPreProcessors.remove(preProcessor);
    }

    //register a post-processor
    final void registerPostProcessor(iCollectorPostProcessor postProcessor) {
        registeredPostProcessors.add(postProcessor);
    }

    //unregister a post-processor
    final void unregisterPostProcessor(iCollectorPostProcessor postProcessor) {
        registeredPostProcessors.remove(postProcessor);
    }

    //main load function that forces the work chain
    @Override
    public final synchronized boolean handleRequest(Object obj) {
        this.setObj(obj);
        if (preCollect()) {
            if (doCollect()) {
                return postCollect();
            }
        }
        return false;
    }

    //call of all registered pre-processors
    final synchronized boolean preCollect() {
        if (!registeredPreProcessors.isEmpty()) {
            for (iCollectorPreProcessor preProcessor : registeredPreProcessors) {
                if (!preProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //call of all registered post-processors
    final synchronized boolean postCollect() {
        if (!registeredPostProcessors.isEmpty()) {
            for (iCollectorPostProcessor postProcessor : registeredPostProcessors) {
                if (!postProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //method of collect to be overidden
    abstract boolean doCollect();
}
