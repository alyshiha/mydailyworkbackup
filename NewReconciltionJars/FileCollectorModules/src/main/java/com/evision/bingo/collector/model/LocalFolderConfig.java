/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.collector.model;

import com.evision.bingo.core.model.CollectorConfigurable;

/**
 * @since 1/4/2017
 * @author Aly Shiha
 *
 * Model for Local Folder Collector Configuration
 */
public class LocalFolderConfig implements CollectorConfigurable {

    private String sourcefolder;
    private String destinationfolder;
    private String filetype;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
    private String extension;

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public String getSourcefolder() {
        return sourcefolder;
    }

    public void setSourcefolder(String sourcefolder) {
        this.sourcefolder = sourcefolder;
    }

    public String getDestinationfolder() {
        return destinationfolder;
    }

    public void setDestinationfolder(String destinationfolder) {
        this.destinationfolder = destinationfolder;
    }
}
