/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.collector.service;

import com.evision.bingo.collector.model.LocalFolderConfig;
import com.evision.bingo.collector.utils.CollectorDefines;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.CollectorConfigurable;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.core.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @since 1/4/2017
 * @author Aly Shiha
 *
 * This is the watch service to listen for folders changes
 */
public class LocalFolderConnectService extends BaseConnectService {

    @Override
    public boolean connect(CollectorConfigurable params) {
        return true;
    }

    @Override
    public void disconnect() {

    }

    public LocalFolderConnectService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, eventsHandlerList);
    }

    @Override
    public void processFiles(CollectorConfigurable config /*Path source, Path dist, String filetype, String extension*/) {

        //create the folder path if not created
        File destinationFile = Utils.getDestinationFolder(((LocalFolderConfig) config).getDestinationfolder(), CollectorDefines.DEFAULT_LOCAL_COLLECTOR_DESITNATION_FOLDER);

        //get the source folder
        File sourceFolder = new File(((LocalFolderConfig) config).getSourcefolder());

        //loop through the files in the source folder
        for (File fileEntry : sourceFolder.listFiles()) {
            if (fileEntry.isFile()) { // if is a file not a directory
                //move file to running folder
                while (!new File(fileEntry.getPath()).renameTo(new File(fileEntry.getPath()))) {
                    try {
                        // Cannot read from file, windows still working on it.
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                    }
                }
                logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_LOCAL_FOLDER_NEW_FILE_FOUND) + Defines.SYSTEM_SPACE + fileEntry.getName());

                Calendar cal = Calendar.getInstance();
                Date time = cal.getTime();

                EventModel eventModel = new EventModel();
                eventModel.setTime(new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(time));
                eventModel.setAction(EventModel.EVENT.FILE_COLLECT);
                eventModel.setActionDetails(fileEntry.getName());
                eventModel.setActionSource(((LocalFolderConfig) config).getSourcefolder());

                try {
                    QueueMessage record = new QueueMessage();
                    record.setRunningfile(destinationFile.getPath() + Defines.SYSTEM_FILE_SEPARATOR + fileEntry.getName());
                    record.setFiletype(((LocalFolderConfig) config).getFiletype());
                    record.setTime(time);

                    logHandler.logInfo(String.format(LogMessages.getMessage(LogMessages.COLLECTOR_LOCAL_FOLDER_MOVE_FILE), fileEntry.toPath(), Paths.get(destinationFile.getPath())));

                    File destFile = new File(destinationFile.getPath() + Defines.SYSTEM_FILE_SEPARATOR + fileEntry.getName());
                    if (destFile.exists()) {
                        logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(LogMessages.getMessage(LogMessages.COLLECTOR_EXISTING_FILE_IN_DISTINATION_FOLDER) + Defines.SYSTEM_SPACE + fileEntry.getName()));
                        eventModel.setActionStatus(EventModel.STATUS.FAIL);
                    } else {
                        Files.move(fileEntry.toPath(), Paths.get(destinationFile.getPath() + Defines.SYSTEM_FILE_SEPARATOR + fileEntry.getName()));
                        logHandler.logInfo(String.format(LogMessages.getMessage(LogMessages.COLLECTOR_LOCAL_FOLDER_ADD_FILE_TO_QUEUES), fileEntry.toPath()));
                        record.setExtension(((LocalFolderConfig) config).getExtension().toLowerCase());
                        queueHandler.sendMessage(record);
                        eventModel.setActionStatus(EventModel.STATUS.SUCCESS);
                    }

                } catch (IOException ex) {
                    logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                    eventModel.setActionStatus(EventModel.STATUS.FAIL);
                } finally {
                    submitEvent(eventModel);
                }
            }
        }
    }
}
