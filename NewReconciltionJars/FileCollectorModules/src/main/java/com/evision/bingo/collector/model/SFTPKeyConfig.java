/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.collector.model;

import com.evision.bingo.core.model.CollectorConfigurable;

/**
 * @since 26/4/2017
 * @author Khaled Khalil
 *
 * Model for SFTP with Key Collector Configuration
 */
public class SFTPKeyConfig implements CollectorConfigurable {

    private String server;
    private int port;
    private String login;
    private String keyPath;
    private String sourcefolder;
    private String destinationfolder;
    private String filetype;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
    private String extension;

    public String getKeyPath() {
        return keyPath;
    }

    public void setKeyPath(String keyPath) {
        this.keyPath = keyPath;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSourcefolder() {
        return sourcefolder;
    }

    public void setSourcefolder(String sourcefolder) {
        this.sourcefolder = sourcefolder;
    }

    public String getDestinationfolder() {
        return destinationfolder;
    }

    public void setDestinationfolder(String destinationfolder) {
        this.destinationfolder = destinationfolder;
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }
}
