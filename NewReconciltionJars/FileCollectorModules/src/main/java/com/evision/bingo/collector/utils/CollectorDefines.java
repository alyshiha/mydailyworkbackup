/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.collector.utils;

/**
 * @since 24/4/2017
 * @author Khaled Khalil
 *
 * Constants of the system
 */
public class CollectorDefines {
    public static final String SYSTEM_DEFAULT_SFTP_CHANNEL_NAME = "sftp";
    public static final String SYSTEM_SFTP_READ_FILE_POSTFIX = "evRead";
    public static final String SYSTEM_SFTPKEY_READ_FILE_POSTFIX = "evRead";
    public static final int COLLECTOR_SCHEDULER_PERIOD_IN_SECONDS = 10;
    public static final String DEFAULT_LOCAL_COLLECTOR_DESITNATION_FOLDER = "DefaultLocalCollectedFiles";
    public static final String DEFAULT_SMB_COLLECTOR_DESITNATION_FOLDER = "DefaultSMBCollectedFiles";
    public static final String DEFAULT_SFTP_COLLECTOR_DESITNATION_FOLDER = "DefaultSFTPCollectedFiles";
    public static final String DEFAULT_SFTP_KEY_COLLECTOR_DESITNATION_FOLDER = "DefaultSFTPKeyCollectedFiles";
    public static final String SFTP = "SFTP";
    public static final String SFTP_KEY = "SFTP Key";
}
