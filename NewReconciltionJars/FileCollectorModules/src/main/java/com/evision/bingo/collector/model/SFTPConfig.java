/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.collector.model;

import com.evision.bingo.core.model.CollectorConfigurable;

/**
 * @since 1/4/2017
 * @author Aly Shiha
 *
 * This is the SFTP Config model
 */
public class SFTPConfig implements CollectorConfigurable {

    private String server;
    private int port;
    private String login;
    private String password;
    private String sourcefolder;
    private String destinationfolder;
    private String filetype;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
    private String extension;

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public String getSourcefolder() {
        return sourcefolder;
    }

    public void setSourcefolder(String sourcefolder) {
        this.sourcefolder = sourcefolder;
    }

    public String getDestinationfolder() {
        return destinationfolder;
    }

    public void setDestinationfolder(String destinationfolder) {
        this.destinationfolder = destinationfolder;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
