/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.collector.service;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.CollectorConfigurable;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;

/**
 * @since 20/4/2017
 * @author Khaled Khalil
 *
 * BaseConnectService
 */
public abstract class BaseConnectService {

    protected LogHandler logHandler;
    protected QueueHandler queueHandler;
    protected List<BaseController> eventsHandlerList;

    public BaseConnectService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        this.queueHandler = queueHandler;
        this.logHandler = logHandler;
        this.eventsHandlerList = eventsHandlerList;
    }

    public abstract void processFiles(CollectorConfigurable config);

    public abstract boolean connect(CollectorConfigurable config);

    public abstract void disconnect();

    public void submitEvent(EventModel event) {
        for (BaseController controller : eventsHandlerList) {
            controller.handleRequest(event);
        }
    }
}