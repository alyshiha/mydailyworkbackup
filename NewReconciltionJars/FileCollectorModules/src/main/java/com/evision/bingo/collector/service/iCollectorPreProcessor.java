/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.collector.service;

import com.evision.bingo.core.controller.BaseController;

/**
 * @since 20/4/2017
 * @author Khaled Khalil
 *
 * base interface for pre-processors
 */
public interface iCollectorPreProcessor {

    public boolean process(BaseController baseController);
}
