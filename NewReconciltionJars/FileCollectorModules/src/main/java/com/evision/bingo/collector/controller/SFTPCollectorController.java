/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.collector.controller;

import com.evision.bingo.collector.model.SFTPConfig;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.collector.service.SFTPConnectService;
import com.evision.bingo.collector.utils.CollectorDefines;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.CollectorConfig;
import com.evision.bingo.core.model.CollectorConfigurable;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

public final class SFTPCollectorController extends BaseCollectorController {

    private ScheduledExecutorService service;

    public SFTPCollectorController(CollectorConfig collectorConfig, CollectorConfigurable sftpConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<BaseController> eventsHandlerList) {
        super(new LogHandler(collectorConfig.getLoggerPathList(), systemLogger), new QueueHandler(collectorConfig.getQueuesList(), rabbitTemplatesMap), sftpConfig, eventsHandlerList);
    }

    @Override
    synchronized boolean doCollect() {
        Runnable runnable = new Runnable() {
            public void run() {
                SFTPConnectService connectService = new SFTPConnectService(queueHandler, logHandler, eventsHandlerList);
                try {
//                    logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SFTP_CONNECT) + Defines.SYSTEM_SPACE + ((SFTPConfig) config).getServer());
                    if (connectService.connect((CollectorConfigurable) config)) {
                        connectService.processFiles((CollectorConfigurable) config);
                    }
                } catch (Exception e) {
                    logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), e);
                } finally {
//                    logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SFTP_DISCONNECT) + Defines.SYSTEM_SPACE + ((SFTPConfig) config).getServer());
                    connectService.disconnect();
                }

            }
        };
        logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SFTP_REGISTER) + Defines.SYSTEM_SPACE + ((SFTPConfig) config).getSourcefolder());
        service = Executors.newSingleThreadScheduledExecutor();
        logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_START_LISTENING));
        service.scheduleAtFixedRate(runnable, 0, CollectorDefines.COLLECTOR_SCHEDULER_PERIOD_IN_SECONDS, TimeUnit.SECONDS);
        return true;
    }

    @Override
    public void stop() {
        try {
            logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_STOP_LISTENING));
            service.shutdown();
            super.destroy();
        } catch (Exception ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
        }
    }
}
