/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.collector.controller;

import com.evision.bingo.collector.model.SFTPKeyConfig;
import com.evision.bingo.collector.service.SFTPKeyConnectService;
import com.evision.bingo.collector.utils.CollectorDefines;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.CollectorConfig;
import com.evision.bingo.core.model.CollectorConfigurable;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @since 26/4/2017
 * @author Khaled Khalil
 *
 * This is the controller to collect files from SFTP with Key
 */
public final class SFTPKeyCollectorController extends BaseCollectorController {

    private ScheduledExecutorService service;

    public SFTPKeyCollectorController(CollectorConfig collectorConfig, CollectorConfigurable sftpKeyConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<BaseController> eventsHandlerList) {
        super(new LogHandler(collectorConfig.getLoggerPathList(), systemLogger), new QueueHandler(collectorConfig.getQueuesList(), rabbitTemplatesMap), sftpKeyConfig, eventsHandlerList);
    }

    @Override
    synchronized boolean doCollect() {
        Runnable runnable = new Runnable() {
            public void run() {
                SFTPKeyConnectService connectService = new SFTPKeyConnectService(queueHandler, logHandler, eventsHandlerList);
                try {
//                    logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SFTPKEY_CONNECT) + Defines.SYSTEM_SPACE + ((SFTPKeyConfig) config).getServer());
                    if (connectService.connect((CollectorConfigurable) config)) {
                        connectService.processFiles((CollectorConfigurable) config);
                    }
                } catch (Exception e) {
                    logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), e);
                } finally {
//                    logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SFTPKEY_DISCONNECT) + Defines.SYSTEM_SPACE + ((SFTPKeyConfig) config).getServer());
                    connectService.disconnect();
                }
            }
        };
        logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SFTPKEY_REGISTER) + Defines.SYSTEM_SPACE + ((SFTPKeyConfig) config).getSourcefolder());
        service = Executors.newSingleThreadScheduledExecutor();
        logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_START_LISTENING));
        service.scheduleAtFixedRate(runnable, 0, CollectorDefines.COLLECTOR_SCHEDULER_PERIOD_IN_SECONDS, TimeUnit.SECONDS);
        return true;
    }

    @Override
    public void stop() {
        try {
            logHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_STOP_LISTENING));
            service.shutdown();
            super.destroy();
        } catch (Exception ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
        }
    }
}
