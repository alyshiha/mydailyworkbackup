/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.runner;

import evision.local.controller.LocalClient;
import evision.sftp.controller.SftpClient;
import evision.share.controller.smbClient;

/**
 *
 * @author shi7a
 */
public class Runner {

    public static void main(String[] args) {
        //local folder listner
        Thread localthread = new Thread(new LocalClient());
        //sftp server listner
        Thread sftpthread = new Thread(new SftpClient());
        //shared folder listner
        Thread smbthread = new Thread(new smbClient());

        localthread.start();
//        sftpthread.start();
//        smbthread.start();

    }
}
