/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.util;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import com.thoughtworks.xstream.XStream;

public final class XStreamTranslator {

    private XStream xstream = null;

    private XStreamTranslator() {
        xstream = new XStream();
    }

    
    public String toXMLString(Object object) {
        return xstream.toXML(object);
    }

 
    public Object toObject(String xml) {
        return (Object) xstream.fromXML(xml);
    }

    public static XStreamTranslator getInstance() {
        return new XStreamTranslator();
    }

    public Object toObject(File xmlFile) throws IOException {
        return xstream.fromXML(new FileReader(xmlFile));
    }


    public void toXMLFile(Object objTobeXMLTranslated, String fileName) throws IOException {
        FileWriter writer = new FileWriter(fileName);
        xstream.toXML(objTobeXMLTranslated, writer);
        writer.close();
    }

    public void toXMLFile(Object objTobeXMLTranslated, String fileName, List omitFieldsRegXList) throws IOException {
        xstreamInitializeSettings(objTobeXMLTranslated, omitFieldsRegXList);
        toXMLFile(objTobeXMLTranslated, fileName);
    }

  
    public void xstreamInitializeSettings(Object objTobeXMLTranslated, List omitFieldsRegXList) {
        if (omitFieldsRegXList != null && omitFieldsRegXList.size() > 0) {
            Iterator itr = omitFieldsRegXList.iterator();
            while (itr.hasNext()) {
                String omitEx = itr.next().toString();
                xstream.omitField(objTobeXMLTranslated.getClass(), omitEx);
            }
        }
    }

    public void toXMLFile(Object objTobeXMLTranslated) throws IOException {
        toXMLFile(objTobeXMLTranslated, objTobeXMLTranslated.getClass().getName() + ".xml");
    }
}
