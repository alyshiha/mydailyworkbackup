package evision.sftp.controller;

import evision.sftp.datamodel.SFTPConfig;
import evision.sftp.service.sftpEngine;
import evision.util.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import org.apache.log4j.Logger;

public class SftpClient extends Thread {

    private static List<SFTPConfig> configlist;
    private final static Logger logger = Logger.getLogger(SftpClient.class);

    @Override
    public void run() {
        RunSFTP();
    }

    public void RunSFTP() {
        try {
            Runnable runnable = new Runnable() {
                public void run() {
                    for (Iterator<SFTPConfig> it = configlist.iterator(); it.hasNext();) {
                        SFTPConfig configRecord = it.next();
                        sftpEngine client = new sftpEngine();
                        client.connect(configRecord);
                        try {
                            client.retrieveFile(configRecord.getSourcefolder(), configRecord.getDestinationfolder(), configRecord.getFiletype());
                        } catch (Exception e) {
                            logger.error(e.getMessage());
                        } finally {
                            client.disconnect();
                        }
                    }
                }
            };
            configlist = (List<SFTPConfig>) XStreamTranslator.getInstance().toObject(new File("./ConfigFiles/SFTP.xml"));
            ScheduledExecutorService service = Executors
                    .newSingleThreadScheduledExecutor();
            service.scheduleAtFixedRate(runnable, 0, 10, TimeUnit.SECONDS);
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

}
