/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.sftp.service;

import evision.sftp.datamodel.SFTPConfig;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import evision.mq.datamodel.OutputDefinition;
import evision.mq.service.RabbitMqConfig;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Properties;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author shi7a
 */
public class sftpEngine {

    private final static Logger logger = Logger.getLogger(sftpEngine.class);

    private JSch jsch = null;
    private Session session = null;
    private Channel channel = null;
    private ChannelSftp c = null;
    private RabbitTemplate rabbitTemplate;

    public sftpEngine() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(RabbitMqConfig.class);
        rabbitTemplate = ctx.getBean(RabbitTemplate.class);
    }

    /**
     * Connects to the server and does some commands.
     */
    public void connect(SFTPConfig param) {
        try {
            logger.debug("Initializing jsch");
            jsch = new JSch();
            session = jsch.getSession(param.getLogin(), param.getServer(), param.getPort());

            // Java 6 version
            session.setPassword(param.getPassword().getBytes(Charset.forName("ISO-8859-1")));

            // Java 5 version
            // session.setPassword(password.getBytes("ISO-8859-1"));
            logger.debug("Jsch set to StrictHostKeyChecking=no");
            Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);

            logger.info("Connecting to " + param.getServer() + ":" + param.getPort());
            session.connect();
            logger.info("Connected !");

            // Initializing a channel
            logger.debug("Opening a channel ...");
            channel = session.openChannel("sftp");
            channel.connect();
            c = (ChannelSftp) channel;
            logger.debug("Channel sftp opened");

        } catch (JSchException e) {
            logger.error("", e);
        }
    }

    public void uploadFile(String sourceFile, String destinationFile) {
        if (c == null || session == null || !session.isConnected() || !c.isConnected()) {
            logger.error("Connection to server is closed. Open it first.");
        }

        try {
            logger.debug("Uploading file to server");
            c.put(sourceFile, destinationFile);
            logger.info("Upload successfull.");
        } catch (SftpException e) {
            logger.error(e);
        }
    }

  
    public void retrieveFile(String sourceFile, String destinationFile, String filetype) {

        if (c == null || session == null || !session.isConnected() || !c.isConnected()) {
            logger.error("Connection to server is closed. Open it first.");
        }

        try {
            c.cd(sourceFile);
            logger.debug("Downloading file to server");
            Vector<ChannelSftp.LsEntry> list = c.ls("*");
            for (ChannelSftp.LsEntry entry : list) {
                if (!entry.getFilename().contains("EV.Reco.Done")) {
                    c.get(entry.getFilename(), destinationFile + "\\" + entry.getFilename());
                    //c.rm(entry.getFilename());
                    c.rename(entry.getFilename(), entry.getFilename() + "EV.Reco.Done");
                    OutputDefinition record = new OutputDefinition();
                    record.setRunningfile(destinationFile + "\\" + entry.getFilename());
                    record.setFiletype(filetype);
                    record.setParsingtime(new Date());
                    rabbitTemplate.convertAndSend(record);
                }
            }
            logger.info("Download successfull.");
        } catch (SftpException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void disconnect() {
        if (c != null) {
            logger.debug("Disconnecting sftp channel");
            c.disconnect();
        }
        if (channel != null) {
            logger.debug("Disconnecting channel");
            channel.disconnect();
        }
        if (session != null) {
            logger.debug("Disconnecting session");
            session.disconnect();
        }
    }
}
