/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.share.service;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import evision.mq.datamodel.OutputDefinition;
import evision.mq.service.RabbitMqConfig;
import evision.share.datamodel.SmbConfig;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Date;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbAuthException;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import org.apache.log4j.Logger;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SmbConnect {

private final static Logger log = Logger.getLogger(SmbConnect.class);
    private RabbitTemplate rabbitTemplate;

    public SmbConnect() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(RabbitMqConfig.class);
        rabbitTemplate = ctx.getBean(RabbitTemplate.class);
    }

    public void copyFiles(SmbConfig setting) {
        try {
            NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, setting.getUsername(), setting.getPassword()); //This class stores and encrypts NTLM user credentials.

            SmbFile[] files = new SmbFile(setting.getSourcefolder(), auth).listFiles(); //This class represents a resource on an SMB network.
            readFileContent(files, setting);
        } catch (MalformedURLException e) {
            log.error("SMB.exists MalformedURLException for " + this.toString() + ": " + e.getMessage());
        } catch (SmbAuthException sae) {
            log.error("SmbAuthException : " + sae.getMessage());
        } catch (SmbException e) {
            log.error("SMB.exists SmbException for " + this.toString() + ": " + e.getMessage());
        }
    }

    private boolean readFileContent(SmbFile[] Files, SmbConfig setting) {
        String destFilename;
        FileOutputStream fileOutputStream;
        InputStream fileInputStream;
        byte[] buf;
        int len;
        for (SmbFile smbFile : Files) {
            destFilename = setting.getDestinationfolder() + "\\" + smbFile.getName();
            log.error("         copying " + smbFile.getName());
            try {
                fileOutputStream = new FileOutputStream(destFilename);
                fileInputStream = smbFile.getInputStream();
                buf = new byte[16 * 1024 * 1024];
                while ((len = fileInputStream.read(buf)) > 0) {
                    fileOutputStream.write(buf, 0, len);
                }
                fileInputStream.close();
                fileOutputStream.close();
                smbFile.delete();
            } catch (SmbException e) {
                log.error("Exception during copyNetworkFilesToLocal stream to output, SMP issue: " + e.getMessage(), e);
                e.printStackTrace();
                return false;
            } catch (FileNotFoundException e) {
                log.error("Exception during copyNetworkFilesToLocal stream to output, file not found: " + e.getMessage(), e);
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                log.error("Exception during copyNetworkFilesToLocal stream to output, IO problem: " + e.getMessage(), e);
                e.printStackTrace();
                return false;
            }
            OutputDefinition record = new OutputDefinition();
            record.setRunningfile(destFilename);
            record.setFiletype(setting.getFiletype());
            record.setParsingtime(new Date());
            rabbitTemplate.convertAndSend(record);
        }
        return false;
    }

}
