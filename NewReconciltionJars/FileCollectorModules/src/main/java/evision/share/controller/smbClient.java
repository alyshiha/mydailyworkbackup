/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.share.controller;

import evision.share.datamodel.SmbConfig;
import evision.share.service.SmbConnect;
import evision.util.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

public class smbClient extends Thread {

    private static List<SmbConfig> SmbConfigList;
    private final static Logger logger = Logger.getLogger(smbClient.class);

    @Override
    public void run() {
        RunSMB();
    }

    public void RunSMB() {
        try {
            Runnable runnable = new Runnable() {
                public void run() {
                    for (SmbConfig SmbConfigRecord : SmbConfigList) {
                        // task to run goes here
                        new SmbConnect().copyFiles(SmbConfigRecord);
                    }
                }
            };
            SmbConfigList = (List<SmbConfig>) XStreamTranslator.getInstance().toObject(new File("./ConfigFiles/Smb.xml"));
            ScheduledExecutorService service = Executors
                    .newSingleThreadScheduledExecutor();
            service.scheduleAtFixedRate(runnable, 0, 11, TimeUnit.SECONDS);
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

}
