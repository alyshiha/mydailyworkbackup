package evision.local.service;

import evision.mq.datamodel.OutputDefinition;
import evision.mq.service.RabbitMqConfig;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class WatchRecursive {

    private Map<WatchKey, String[]> keyPathMap = new HashMap<>();
    private RabbitTemplate rabbitTemplate;
    private final static Logger logger = Logger.getLogger(WatchRecursive.class);

    public WatchRecursive() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(RabbitMqConfig.class);
        rabbitTemplate = ctx.getBean(RabbitTemplate.class);
    }

    public void registerDir(String[] path, WatchService watchService) throws
            IOException {
        //check validaty of the given path
        if (!Files.isDirectory(Paths.get(path[0]), LinkOption.NOFOLLOW_LINKS)) {
            return;
        }

        System.out.println("registering: " + Paths.get(path[0]));

        WatchKey key = Paths.get(path[0]).register(watchService,
                StandardWatchEventKinds.ENTRY_CREATE);
        //intialize the map with all folders
        keyPathMap.put(key, path);

        for (File f : Paths.get(path[0]).toFile().listFiles()) {
            registerDir(new String[]{f.getAbsolutePath(), path[1], path[2]}, watchService);
        }
    }

    public void startListening(WatchService watchService) throws Exception {
        while (true) {
            //waiting file
            WatchKey queuedKey = watchService.take();
            for (WatchEvent<?> watchEvent : queuedKey.pollEvents()) {
                //do something useful here
                if (watchEvent.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
                    //this is not a complete path
                    Path path = (Path) watchEvent.context();
                    //get complete path
                    path = Paths.get(keyPathMap.get(queuedKey)[0]).resolve(path);
                    movefile(Paths.get(keyPathMap.get(queuedKey)[0] + "//" + watchEvent.context()), Paths.get(keyPathMap.get(queuedKey)[1] + "//" + watchEvent.context()), keyPathMap.get(queuedKey)[2]);
                    registerDir(new String[]{path.toString(), keyPathMap.get(queuedKey)[1], keyPathMap.get(queuedKey)[2]}, watchService);
                }
            }
            if (!queuedKey.reset()) {
                keyPathMap.remove(queuedKey);
            }
            if (keyPathMap.isEmpty()) {
                break;
            }
        }
    }

    private void movefile(Path source, Path dist, String filetype) throws InterruptedException {

        try {
            OutputDefinition record = new OutputDefinition();
            record.setRunningfile(dist.toString());
            record.setFiletype(filetype);
            record.setParsingtime(new Date());
            //move file to running folder
            while (!new File(source.toUri()).renameTo(new File(source.toUri()))) {
                // Cannot read from file, windows still working on it.
                Thread.sleep(100);
            }
            Files.move(source, dist, StandardCopyOption.REPLACE_EXISTING);
            rabbitTemplate.convertAndSend(record);
        } catch (NoSuchFileException x) {
            logger.error(source + "no exists", x);
        } catch (DirectoryNotEmptyException x) {
            logger.error(source + "directory not empty", x);
        } catch (IOException x) {
            logger.error(source + "file permission issue", x);
        }
    }

}
