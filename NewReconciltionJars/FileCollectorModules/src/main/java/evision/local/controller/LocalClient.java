/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.local.controller;

import evision.local.datamodel.LocalFolderConfig;
import evision.local.service.WatchRecursive;
import evision.util.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.WatchService;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author shi7a
 */
public class LocalClient extends Thread {

    private final static Logger logger = Logger.getLogger(LocalClient.class);

    @Override
    public void run() {
        runlocal();
    }

    public void runlocal() {
        try {
            List<LocalFolderConfig> LocalFolderConfigList = (List<LocalFolderConfig>) XStreamTranslator.getInstance().toObject(new File("./ConfigFiles/LocalFolder.xml"));
            WatchRecursive wr = new WatchRecursive();
            try (WatchService watchService = FileSystems.getDefault().newWatchService()) {
                for (LocalFolderConfig LocalFolderConfigRecord : LocalFolderConfigList) {
                    wr.registerDir(new String[]{LocalFolderConfigRecord.getSourcefolder(), LocalFolderConfigRecord.getDestinationfolder(), LocalFolderConfigRecord.getFiletype()}, watchService);
                }
                wr.startListening(watchService);
            } catch (Exception ex) {
                logger.error("Exception Occur :", ex);
            }
        } catch (IOException ex) {
            logger.error("Exception Occur :", ex);
        }
    }
    
}
