/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.local.datamodel;

/**
 *
 * @author shi7a
 */
public class LocalFolderConfig {

    private String sourcefolder;
    private String destinationfolder;
    private String filetype;

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public String getSourcefolder() {
        return sourcefolder;
    }

    public void setSourcefolder(String sourcefolder) {
        this.sourcefolder = sourcefolder;
    }

    public String getDestinationfolder() {
        return destinationfolder;
    }

    public void setDestinationfolder(String destinationfolder) {
        this.destinationfolder = destinationfolder;
    }

}
