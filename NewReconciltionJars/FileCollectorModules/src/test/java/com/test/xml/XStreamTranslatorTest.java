/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.xml;

import evision.util.XStreamTranslator;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class XStreamTranslatorTest {

    SampleObject sampleObj;
    XStreamTranslator xStreamTranslatorInst;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        sampleObj = new SampleObject();
        xStreamTranslatorInst = XStreamTranslator.getInstance();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void simpleObjectToXMLStringNotNullTest() {
        String xml = xStreamTranslatorInst.toXMLString(sampleObj);
        assertNotNull(xml);
    }

    @Test
    public void simpleObjectToXMLStringVerifyTest() {
        sampleObj.setName("Test");
        assertEquals("Test", sampleObj.getName());
        sampleObj.setTestInt(9);
        assertEquals(9, sampleObj.getTestInt());
        String xml = xStreamTranslatorInst.toXMLString(sampleObj);
        String expected = getExpectedStringOutOfSampleObject();
        assertEquals(expected, xml.replaceAll("[\\n\\s\\t]+", ""));
    }

    @Test
    public void xmlToObjectVerifyTest() {
        String xml = getExpectedStringOutOfSampleObject();
        SampleObject sampleObj = (SampleObject) xStreamTranslatorInst.toObject(xml);
        assertNotNull(sampleObj);
    }

    @Test(expected = IOException.class)
    public void xmlToAnyObjectFromFileThatNotExists() throws IOException {
        SampleObject sampleObj = (SampleObject) xStreamTranslatorInst.toObject(new File("src\\test\\resources\\testNoFile.xml"));
        assertNotNull(sampleObj);
        assertEquals("somename", sampleObj.getName());
    }

    @Test
    public void xmlToAnyObjectFromFile() throws IOException {
        SampleObject sampleObj = (SampleObject) xStreamTranslatorInst.toObject(new File("src\\test\\resources\\testSampleObject.xml"));
        assertNotNull(sampleObj);
        assertEquals("Test", sampleObj.getName());
    }

    @Test
    public void objToXmlFileTestForNotNull() throws IOException {
        SampleObject sampleObj = new SampleObject();
        sampleObj.setName("Test2");
        assertEquals("Test2", sampleObj.getName());
        sampleObj.setTestInt(99);
        assertEquals(99, sampleObj.getTestInt());
        xStreamTranslatorInst.toXMLFile(sampleObj);
        File file = new File(sampleObj.getClass().getName() + ".xml");
        assertTrue(file.exists());
        String sample = FileUtils.readFileToString(file);
        assertNotNull(sample);
    }

    @Test
    public void objToXmlFileCreate() throws IOException {
        SampleObject sampleObj = new SampleObject();
        sampleObj.setName("Test2");
        assertEquals("Test2", sampleObj.getName());
        sampleObj.setTestInt(99);
        assertEquals(99, sampleObj.getTestInt());
        xStreamTranslatorInst.toXMLFile(sampleObj);
        File file = new File(sampleObj.getClass().getName() + ".xml");
        assertTrue(file.exists());
        String sample = FileUtils.readFileToString(file);
        assertNotNull(sample);
        assertEquals(getExpectedStringOutOfSampleObject2(), sample.replaceAll("[\\n\\s\\t]+", ""));
    }

    private String getExpectedStringOutOfSampleObject() {
        return "<com.test.xml.SampleObject><name>Test</name><testInt>9</testInt></com.test.xml.SampleObject>";
    }

    private String getExpectedStringOutOfSampleObject2() {
        return "<com.test.xml.SampleObject><name>Test2</name><testInt>99</testInt></com.test.xml.SampleObject>";
    }
}
