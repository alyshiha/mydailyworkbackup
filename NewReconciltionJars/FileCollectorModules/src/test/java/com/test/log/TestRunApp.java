/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.log;

import org.apache.log4j.Logger;

public class TestRunApp {

    final static Logger logger = Logger.getLogger(TestRunApp.class);

    public static void main(String[] args) {

        TestRunApp obj = new TestRunApp();
        obj.runMe("mkyong");

    }

    private void runMe(String parameter) {

        if (logger.isDebugEnabled()) {
            logger.debug("This is debug : " + parameter);
        }

        if (logger.isInfoEnabled()) {
            logger.info("This is info : " + parameter);
        }

        logger.warn("This is warn : " + parameter);
        logger.error("This is error : " + parameter);
        logger.fatal("This is fatal : " + parameter);

    }

}
