package com.test.sftp;

import evision.sftp.service.sftpEngine;
import evision.sftp.datamodel.SFTPConfig;
import org.apache.sshd.SshServer;
import org.apache.sshd.common.NamedFactory;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.command.ScpCommandFactory;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.sftp.SftpSubsystem;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class SftpClientTest {

    private SshServer sshd;
    private sftpEngine test;
    private SFTPConfig setupparam;
    private String server = "127.0.0.1";
    private String login = "tester";
    private String password = "password";
    
    
    @Before
    public void setUp() throws IOException {
        // Init sftp server stuff
        sshd = SshServer.setUpDefaultServer();
        sshd.setPasswordAuthenticator(new MyPasswordAuthenticator());
        sshd.setPublickeyAuthenticator(new MyPublickeyAuthenticator());
        sshd.setKeyPairProvider(new SimpleGeneratorHostKeyProvider());
        sshd.setSubsystemFactories(Arrays.<NamedFactory<Command>>asList(new SftpSubsystem.Factory()));
        sshd.setCommandFactory(new ScpCommandFactory());

        sshd.start();

        setupparam = new SFTPConfig();
        setupparam.setServer(server);
        setupparam.setPort(22);
        setupparam.setLogin(login);
        setupparam.setPassword(password);
        // Init tested class
        test = new sftpEngine();
        // Clean existing files from previous runs
        cleanFiles();
    }

    @After
    public void tearDown() throws InterruptedException {
        sshd.stop();
        // Clean existing files from previous runs
        cleanFiles();
    }

    @Test
    public void testRetrieveFile() throws Exception {
//        test.connect(setupparam);
//        test.uploadFile("src\\test\\resources\\upload.txt", "target\\uploaded.txt");
//        test.retrieveFile("target\\uploaded.txt", "target\\downloaded.txt","network");
//        test.disconnect();
//
//        File downloaded = new File("target\\downloaded.txt");
//        Assert.assertTrue(downloaded.exists());
    }

    private void cleanFiles() {
        File uploaded = new File("target\\uploaded.txt");
        if (uploaded.exists()) {
            uploaded.delete();
        }

        File downloaded = new File("target\\downloaded.txt");
        if (downloaded.exists()) {
            downloaded.delete();
        }
    }
}
