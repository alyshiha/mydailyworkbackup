/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.backup.datamodel;

/**
 *
 * @author shi7a
 */
public class PathDefinition {

    private String filetype;
    private String backupfolder;

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public String getBackupfolder() {
        return backupfolder;
    }

    public void setBackupfolder(String backupfolder) {
        this.backupfolder = backupfolder;
    }

}
