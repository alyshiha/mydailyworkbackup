/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.backup.controller;

import evision.backup.datamodel.PathDefinition;
import evision.backup.service.FilesCollection;
import evision.mq.datamodel.OutputDefinition;
import evision.mq.service.RabbitMqConfig;
import evision.util.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author Aly
 */
public class ReadClient {

    private static RabbitTemplate rabbitTemplate;

    public ReadClient() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(RabbitMqConfig.class);
        rabbitTemplate = ctx.getBean(RabbitTemplate.class);
    }

    public static void run(OutputDefinition MSG) {
        try {
            List<PathDefinition> PathDefinitionList = (List<PathDefinition>) XStreamTranslator.getInstance().toObject(new File("ConfigFiles\\PathSetup.xml"));
            for (PathDefinition PathDefinitionRecord : PathDefinitionList) {
                if (PathDefinitionRecord.getFiletype().equals(MSG.getFiletype())) {
                    FilesCollection FC = new FilesCollection();
                    FC.CreateDirectory(PathDefinitionRecord.getBackupfolder());
                    FC.MoveFile(MSG.getRunningfile());
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ReadClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
