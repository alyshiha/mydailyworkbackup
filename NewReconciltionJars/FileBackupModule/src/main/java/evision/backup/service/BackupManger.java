/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.backup.service;

import evision.mq.datamodel.OutputDefinition;
import evision.backup.controller.BackupClient;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.support.converter.JsonMessageConverter;

public class BackupManger implements MessageListener {

    private JsonMessageConverter converter = new JsonMessageConverter();

    @Override
    public void onMessage(Message message) {
            BackupClient.run((OutputDefinition) converter.fromMessage(message));       
    }
}
