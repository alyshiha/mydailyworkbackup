/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.backup.service;

import evision.mq.datamodel.OutputDefinition;
import evision.backup.controller.ReadClient;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.support.converter.JsonMessageConverter;

public class ParserManger implements MessageListener {

    private JsonMessageConverter converter = new JsonMessageConverter();

    @Override
    public void onMessage(Message message) {
            ReadClient.run((OutputDefinition) converter.fromMessage(message));       
    }
}
