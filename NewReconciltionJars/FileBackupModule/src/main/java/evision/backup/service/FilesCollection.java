/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.backup.service;

import java.io.File;
import java.util.Calendar;

/**
 *
 * @author Aly
 */
public class FilesCollection {

    private String FinalPath;
    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FilesCollection.class);

    public void MoveFile(String FromFile) {
        try {
            File afile = new File(FromFile);
            if (afile.renameTo(new File(FinalPath + "\\" + afile.getName()))) {
                logger.error("File " + FromFile + " is moved successful!");
            } else {
                logger.error("File " + FromFile + " is failed to move!");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CreateDirectory(String Path) {
        File files = new File(CreateDirectoryPath(Path));
        if (!files.exists()) {
            if (files.mkdirs()) {
                logger.error("Multiple directories are created!");
            } else {
                logger.error("Failed to create multiple directories!");
            }
        }
        FinalPath = files.getPath();
    }

    public String CreateDirectoryPath(String Path) {
        // create a calendar
        Calendar cal = Calendar.getInstance();
        // get the value of all the calendar date fields.
        Path = Path + "\\" + cal.get(Calendar.YEAR) + "\\" + (cal.get(Calendar.MONTH) + 1) + "\\" + cal.get(Calendar.DATE);
        return Path;
    }

}
