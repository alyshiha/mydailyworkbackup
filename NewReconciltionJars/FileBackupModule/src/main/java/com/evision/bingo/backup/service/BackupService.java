/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.backup.service;

import com.evision.bingo.backup.model.BackupFolderConfig;
import com.evision.bingo.backup.utils.BackupDefines;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.BackupConfigurable;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.core.utils.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @since 5/6/2017
 * @author Khaled Khalil
 *
 * Backup Service
 */
public class BackupService extends BaseBackupService {

    List<String> filesListInDir = new ArrayList<String>();

    public BackupService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, eventsHandlerList);
    }

    @Override
    public boolean processFiles(BackupConfigurable config, QueueMessage message) {
        Calendar cal = Calendar.getInstance();
        Date time = cal.getTime();

        EventModel eventModel = new EventModel();
        eventModel.setTime(new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(time));
        eventModel.setAction(EventModel.EVENT.FILE_BACKUP);
        eventModel.setActionDetails(message.getRunningfile());
        eventModel.setActionSource(BackupDefines.QUEUE_ACTION_SOURCE);

        String destinationFolderString = ((BackupFolderConfig) config).getBackupFolder()
                + Defines.SYSTEM_FILE_SEPARATOR + cal.get(Calendar.YEAR) + Defines.SYSTEM_FILE_SEPARATOR + (cal.get(Calendar.MONTH) + 1)
                + Defines.SYSTEM_FILE_SEPARATOR + cal.get(Calendar.DATE);

        File destinationFolder = new File(destinationFolderString);

        File source = new File(message.getRunningfile());
        source.exists();

        String zipFileNameWithPath = destinationFolder + Defines.SYSTEM_FILE_SEPARATOR;
        File zipFile = new File(zipFileNameWithPath);
        boolean zipFileFound = false;
        String randomStr = "";
        if (zipFile.exists()) {  //if zip file found then rename it with a random string
            zipFileFound = true;
            randomStr = Utils.randomAlphanumeric(8);
        }
        if (source.isFile()) {
            zipFileNameWithPath += Utils.removeFileExtension(source.getName()) + randomStr + "." + BackupDefines.ZIP_EXTENSION;
        } else {
            zipFileNameWithPath += source.getName() + "-" + randomStr + "." + BackupDefines.ZIP_EXTENSION;
        }

        logHandler.logInfo(String.format(LogMessages.getMessage(LogMessages.BACKUP_COPY_SOURCE_CONTENTS_TO_DETINATION) + Defines.SYSTEM_SPACE, message.getRunningfile(), zipFileNameWithPath));
        destinationFolder.mkdirs();

        FileOutputStream fos = null;
        ZipOutputStream zos = null;

        if (source.isFile()) {
            try {
                ZipEntry ze = new ZipEntry(source.getName());

                fos = new FileOutputStream(zipFileNameWithPath);
                zos = new ZipOutputStream(fos);

                zos.putNextEntry(ze);
                //read the file and write to ZipOutputStream
                FileInputStream fis = new FileInputStream(source);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = fis.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }

                //Close the zip entry to write to zip file
                zos.closeEntry();
                //Close resources
                fis.close();
                eventModel.setActionStatus(EventModel.STATUS.SUCCESS);
            } catch (IOException ex) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                eventModel.setActionStatus(EventModel.STATUS.FAIL);
            } finally {
                if (zos != null) {
                    try {
                        zos.close();
                    } catch (IOException ex) {
                        logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                    }
                }
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException ex) {
                        logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                    }
                }
                submitEvent(eventModel);
            }
            if (eventModel.getActionStatus().equals(EventModel.STATUS.FAIL)) {
                return false;
            } else {

                return true;
            }
        } else if (source.isDirectory()) {

            try {
                //get all folder contents
                populateFilesList(source);

                fos = new FileOutputStream(zipFileNameWithPath);
                zos = new ZipOutputStream(fos);
                for (String filePath : filesListInDir) {
                    //for ZipEntry we need to keep only relative file path, so we used substring on absolute path
                    ZipEntry ze = new ZipEntry(filePath.substring(source.getAbsolutePath().length() + 1, filePath.length()));
                    zos.putNextEntry(ze);
                    //read the file and write to ZipOutputStream
                    FileInputStream fis = new FileInputStream(filePath);
                    byte[] buffer = new byte[1024];
                    int len;
                    while ((len = fis.read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                    zos.closeEntry();
                    fis.close();
                }

                eventModel.setActionStatus(EventModel.STATUS.SUCCESS);
            } catch (IOException ex) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                eventModel.setActionStatus(EventModel.STATUS.FAIL);
            } finally {
                if (zos != null) {
                    try {
                        zos.close();
                    } catch (IOException ex) {
                        logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                    }
                }
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException ex) {
                        logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                    }
                }
                submitEvent(eventModel);
            }
            if (eventModel.getActionStatus().equals(EventModel.STATUS.FAIL)) {
                return false;
            } else {

                return true;
            }
        }

        eventModel.setActionStatus(EventModel.STATUS.FAIL);
        submitEvent(eventModel);
        return false;
    }

    @Override
    public boolean connect(BackupConfigurable config) {
        return true;
    }

    @Override
    public void disconnect() {
        //do nothing
    }

    private void populateFilesList(File dir) throws IOException {
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                filesListInDir.add(file.getAbsolutePath());
            } else {
                populateFilesList(file);
            }
        }
    }
}
