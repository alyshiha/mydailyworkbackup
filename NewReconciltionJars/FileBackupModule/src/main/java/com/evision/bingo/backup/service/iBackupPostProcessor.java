/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.backup.service;

import com.evision.bingo.core.controller.BaseController;

/**
 * @since 30/5/2017
 * @author Khaled Khalil
 *
 * base interface for post-processors
 */
public interface iBackupPostProcessor {
    public boolean process(BaseController baseController);
}
