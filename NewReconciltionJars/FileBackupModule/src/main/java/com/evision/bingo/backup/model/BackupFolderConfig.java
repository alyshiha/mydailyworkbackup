/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.backup.model;

import com.evision.bingo.core.model.BackupConfigurable;

/**
 * @since 30/5/2017
 * @author Khaled Khalil
 */
public class BackupFolderConfig implements BackupConfigurable {

    private String fileType;
    private String backupFolder;

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getBackupFolder() {
        return backupFolder;
    }

    public void setBackupFolder(String backupFolder) {
        this.backupFolder = backupFolder;
    }
}