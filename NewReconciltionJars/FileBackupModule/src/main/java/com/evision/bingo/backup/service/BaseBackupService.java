/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.backup.service;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.BackupConfigurable;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;

/**
 * @since 5/6/2017
 * @author Khaled Khalil
 *
 * BaseBackupService
 */
public abstract class BaseBackupService {

    protected LogHandler logHandler;
    protected QueueHandler queueHandler;
    protected List<BaseController> eventsHandlerList;

    public BaseBackupService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        this.queueHandler = queueHandler;
        this.logHandler = logHandler;
        this.eventsHandlerList = eventsHandlerList;
    }

    public abstract boolean processFiles(BackupConfigurable config, QueueMessage message);

    public abstract boolean connect(BackupConfigurable config);

    public abstract void disconnect();

    public void submitEvent(EventModel event) {
        for (BaseController controller : eventsHandlerList) {
            controller.handleRequest(event);
        }
    }
}
