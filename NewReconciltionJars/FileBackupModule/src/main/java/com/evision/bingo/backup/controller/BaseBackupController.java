/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.backup.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.backup.service.iBackupPostProcessor;
import com.evision.bingo.backup.service.iBackupPreProcessor;
import com.evision.bingo.core.model.BackupConfig;
import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.ArrayList;
import java.util.List;

/**
 * @since 30/5/2017
 * @author Khaled Khalil
 *
 */
public abstract class BaseBackupController extends BaseController {

    //collection of registered pre-processors
    private List<iBackupPreProcessor> registeredPreProcessors;

    //collection of registered post-processors
    private List<iBackupPostProcessor> registeredPostProcessors;

    protected List<Configurable> subConfigList = new ArrayList();

    public BaseBackupController(LogHandler logHandler, QueueHandler queueHandler, BackupConfig backupConfig, List<Configurable> subConfigListParam, List<BaseController> eventsHandlerList) {
        super(logHandler, queueHandler, backupConfig, eventsHandlerList);

        if (subConfigListParam != null && !subConfigListParam.isEmpty()) {
            for (Configurable config : subConfigListParam) {
                this.subConfigList.add(config);
            }
        }

        init();
    }

    @Override
    public final void init() {
        registeredPreProcessors = new ArrayList<>();
        registeredPostProcessors = new ArrayList<>();
    }

    @Override
    public synchronized final boolean handleRequest(Object obj) {
        this.setObj(obj);
        if (preCollect()) {
            if (doCollect()) {
                return postCollect();
            }
        }
        return false;
    }

    @Override
    public final void destroy() {
        registeredPreProcessors.clear();
        registeredPostProcessors.clear();

        registeredPreProcessors = null;
        registeredPostProcessors = null;
    }

    //register a pre-processor
    final void registerPreProcessor(iBackupPreProcessor preProcessor) {
        registeredPreProcessors.add(preProcessor);
    }

    //unregister a pre-processor
    final void unregisterPreProcessor(iBackupPreProcessor preProcessor) {
        registeredPreProcessors.remove(preProcessor);
    }

    //register a post-processor
    final void registerPostProcessor(iBackupPostProcessor postProcessor) {
        registeredPostProcessors.add(postProcessor);
    }

    //unregister a post-processor
    final void unregisterPostProcessor(iBackupPostProcessor postProcessor) {
        registeredPostProcessors.remove(postProcessor);
    }

    //call of all registered pre-processors
    final synchronized boolean preCollect() {
        if (!registeredPreProcessors.isEmpty()) {
            for (iBackupPreProcessor preProcessor : registeredPreProcessors) {
                if (!preProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //call of all registered post-processors
    final synchronized boolean postCollect() {
        if (!registeredPostProcessors.isEmpty()) {
            for (iBackupPostProcessor postProcessor : registeredPostProcessors) {
                if (!postProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //method of collect to be overidden
    abstract boolean doCollect();

}
