/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.backup.utils;

/**
 * @since 5/6/2017
 * @author Khaled Khalil
 *
 * Constants of the system
 */
public class BackupDefines {
    public static final String QUEUE_ACTION_SOURCE = "Queue";
    public static final String ZIP_EXTENSION = "zip";
}
