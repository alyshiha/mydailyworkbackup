/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.backup.controller;

import com.evision.bingo.backup.model.BackupFolderConfig;
import com.evision.bingo.backup.service.BackupService;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.BackupConfig;
import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.JsonMessageConverter;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 * backup controller
 */
public class BackupController extends BaseBackupController {

    public BackupController(BackupConfig backupConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<Configurable> subConfigListParam, List<BaseController> eventsHandlerList) {
        super(new LogHandler(backupConfig.getLoggerPathList(), systemLogger), new QueueHandler(backupConfig.getQueuesList(), rabbitTemplatesMap), backupConfig, subConfigListParam, eventsHandlerList);
    }

    @Override
    synchronized boolean doCollect() {
        if (getObj() != null) {
            JsonMessageConverter jsonConverter = new JsonMessageConverter();
            QueueMessage message = (QueueMessage) jsonConverter.fromMessage((Message)getObj());

            BackupService backupService = new BackupService(queueHandler, logHandler, eventsHandlerList);
            //check supported extensions
            for (Configurable bConfig : subConfigList) {
                BackupFolderConfig backupFolderConfig = (BackupFolderConfig) bConfig;
                if (message.getFiletype().equals(backupFolderConfig.getFileType())) {
                    if (backupService.processFiles(backupFolderConfig, message)) {
                        queueHandler.sendMessage(message);
                        return true;
                    }
                }
            }
        }

        return false;
    }

    @Override
    public void stop() {
        ///do nothing
    }
}
