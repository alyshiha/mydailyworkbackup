/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.disputerules.service;

import evision.mq.service.RabbitMqConfig;
import evision.disputerules.datamodel.validationrules;
import evision.disputerules.util.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.utils.SerializationUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author shi7a
 */
public class ValidationEngine {

    private static String RunningType = "";
    private static List<validationrules> RulesDefinitionList = new ArrayList<validationrules>();
    private static String Reason = "";
    private static RabbitTemplate rabbitTemplate;
    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ValidationEngine.class);

    public ValidationEngine() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(RabbitMqConfig.class);
        rabbitTemplate = ctx.getBean(RabbitTemplate.class);
    }

    private static Boolean IsNotValidString(validationrules RulesDefinitionRecord, String Value) {
        switch (RulesDefinitionRecord.getOperator()) {
            //string not in a;b;c
            case "Not IN":
                if (Arrays.asList(RulesDefinitionRecord.getValue().split(";")).contains(Value)) {
                    Reason += RulesDefinitionRecord.getFieldname() + " IN " + RulesDefinitionRecord.getValue() + "Value Equal " + Value;
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            //string in a;b;c
            case "IN":
                if (!Arrays.asList(RulesDefinitionRecord.getValue().split(";")).contains(Value)) {
                    Reason += RulesDefinitionRecord.getFieldname() + " Not IN " + RulesDefinitionRecord.getValue() + "Value Equal " + Value;
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            //string equal aly
            case "Equal":
                if (!Value.equals(RulesDefinitionRecord.getValue())) {
                    Reason += RulesDefinitionRecord.getFieldname() + " Not Equal " + RulesDefinitionRecord.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            //string not equal aly
            case "Not Equal":
                if (Value.equals(RulesDefinitionRecord.getValue())) {
                    Reason += RulesDefinitionRecord.getFieldname() + " Equal " + RulesDefinitionRecord.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Like":
                if (Value.contains(RulesDefinitionRecord.getValue())) {
                    Reason += RulesDefinitionRecord.getFieldname() + " Value : " + Value + " Witch Not Contain " + RulesDefinitionRecord.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Not Like":
                if (!Value.contains(RulesDefinitionRecord.getValue())) {
                    Reason += RulesDefinitionRecord.getFieldname() + " Value : " + Value + " Witch Contain " + RulesDefinitionRecord.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Is Not Null":
                if (Value == null || Value == "" || Value.isEmpty() == Boolean.TRUE) {
                    Reason += RulesDefinitionRecord.getFieldname() + " Is Null";
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Is Null":
                if (Value != null || Value != "" || Value.isEmpty() == Boolean.FALSE) {
                    Reason += RulesDefinitionRecord.getFieldname() + " Value Equal " + Value + " Witch Is Not Null";
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            default:
                logger.error(new IllegalArgumentException("Invalid Operator: " + RulesDefinitionRecord.getOperator()));
                return Boolean.FALSE;
        }
    }

    private static Boolean IsNotValidDecimal(validationrules ValdationRule, BigDecimal Value) {
        switch (ValdationRule.getOperator()) {
            case "Equal":
                if (Value.compareTo(new BigDecimal(ValdationRule.getValue())) != 0) {
                    Reason += ValdationRule.getFieldname() + "  Not Equal " + ValdationRule.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Not Equal":
                if (Value.compareTo(new BigDecimal(ValdationRule.getValue())) == 0) {
                    Reason += ValdationRule.getFieldname() + " Equal " + ValdationRule.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Like":
                if (!Value.toString().contains(ValdationRule.getValue())) {
                    Reason += ValdationRule.getFieldname() + " Value Equal " + Value + " Witch Not Contain " + ValdationRule.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Not Like":
                if (Value.toString().contains(ValdationRule.getValue())) {
                    Reason += ValdationRule.getFieldname() + " Value Equal " + Value + " Witch Contain " + ValdationRule.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Is Null":
                if (Value != null) {
                    Reason += ValdationRule.getFieldname() + " Is Not Null";
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Is Not Null":
                if (Value == null) {
                    Reason += ValdationRule.getFieldname() + " Value Equal " + Value + " Witch Is Null";
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Greater Than":
                if (Value.compareTo(new BigDecimal(ValdationRule.getValue())) == -1) {
                    Reason += ValdationRule.getFieldname() + " Value Equal " + Value + " Witch Smaller Than " + ValdationRule.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Smaller Than":
                if (Value.compareTo(new BigDecimal(ValdationRule.getValue())) == 1) {
                    Reason += ValdationRule.getFieldname() + " Value Equal " + Value + " Witch Greater Than " + ValdationRule.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            default:
                logger.error(new IllegalArgumentException("Invalid Operator: " + ValdationRule.getOperator()));
                return Boolean.FALSE;
        }
    }

    private static Boolean IsNotValidInteger(validationrules ValdationRule, Integer Value) {
        switch (ValdationRule.getOperator()) {
            case "Equal":
                if (Value.compareTo(new Integer(ValdationRule.getValue())) != 0) {
                    Reason += ValdationRule.getFieldname() + " Not Equal " + ValdationRule.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Not Equal":
                if (Value.compareTo(new Integer(ValdationRule.getValue())) == 0) {
                    Reason += ValdationRule.getFieldname() + " Equal " + ValdationRule.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Greater Than":
                if (Value.compareTo(new Integer(ValdationRule.getValue())) == -1) {
                    Reason += ValdationRule.getFieldname() + " Value Equal " + Value + " Witch Smaller Than " + ValdationRule.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            case "Smaller Than":
                if (Value.compareTo(new Integer(ValdationRule.getValue())) == 1) {
                    Reason += ValdationRule.getFieldname() + " Value Equal " + Value + " Witch Greater Than " + ValdationRule.getValue();
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            default:
                logger.error(new IllegalArgumentException("Invalid Operator: " + ValdationRule.getOperator()));
                return Boolean.FALSE;
        }
    }

    public static void ValidateTrans(Map<?, ?> Transaction) {
        for (validationrules validationrulesRecord : RulesDefinitionList) {
            Object Value = Transaction.get(validationrulesRecord.getFieldname());
            String DataType = Value.getClass().getTypeName();
            if (DataType.toLowerCase().equals("java.lang.string")) {
                if (IsNotValidString(validationrulesRecord, Value.toString().trim())) {
                    Reason = Reason + " || ";
                }
            } else if (DataType.toLowerCase().equals("java.math.bigdecimal")) {
                if (IsNotValidDecimal(validationrulesRecord, new BigDecimal(Value.toString().trim()))) {
                    Reason = Reason + " || ";
                }
            } else if (DataType.toLowerCase().equals("java.lang.integer")) {
                IsNotValidInteger(validationrulesRecord, new Integer(Value.toString().trim()));
            }
        }
    }

    private static void loadRules(String FileType) {
        if (!FileType.equals(RunningType)) {
            try {
                RunningType = FileType;
                RulesDefinitionList.clear();
                for (validationrules RulesDefinitionRecord : (List<validationrules>) XStreamTranslator.getInstance().toObject(new File(".\\ConfigFiles\\DisputeRules.xml"))) {
                    if (RulesDefinitionRecord.getFiletype().equals(RunningType)) {
                        RulesDefinitionList.add(RulesDefinitionRecord);
                    }
                }

            } catch (IOException ex) {
                logger.error(ex.getMessage());
            }
        }
    }

    public static void Run(Message message) {
        loadRules(message.getMessageProperties().getHeaders().get("filetype").toString());
        //System.out.println(SerializationUtils.deserialize(message.getBody()).getClass());
        ValidateTrans((Map<?, ?>) SerializationUtils.deserialize(message.getBody()));
        if (!"".equals(Reason)) {
            message.getMessageProperties().getHeaders().put("disputereason", Reason);
            rabbitTemplate.setRoutingKey(RabbitMqConfig.SIMPLE_MESSAGE_QUEUE_OUT_INVALID);
            rabbitTemplate.convertAndSend(message);
            Reason = "";
        } else {
            rabbitTemplate.setRoutingKey(RabbitMqConfig.SIMPLE_MESSAGE_QUEUE_OUT_VALID);
            rabbitTemplate.convertAndSend(message);
        }
    }
}
