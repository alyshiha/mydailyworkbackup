/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.xml;

import evision.matching.datamodel.DBConnection;
import evision.util.XStreamTranslator;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.tree.RowMapper;
import javax.swing.tree.TreePath;
import oracle.jdbc.pool.OracleDataSource;
import org.apache.poi.ss.formula.functions.Address;
import org.springframework.amqp.core.Message;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

/**
 *
 * @author shi7a
 */
public class Select {

    public static void main(String[] args) {
        try {
            FileType first = new FileType();
            first.setTemplateName("EBC");
            List<String> FirstTemplateFields = new ArrayList<String>();
            FirstTemplateFields.add("otppan");
            FirstTemplateFields.add("transactionamount");
            FirstTemplateFields.add("validto");
            first.setTemplateFields(FirstTemplateFields);

            FileType second = new FileType();
            second.setTemplateName("Visa");
            List<String> SecondTemplateFields = new ArrayList<String>();
            SecondTemplateFields.add("sourceoffundpan");
            SecondTemplateFields.add("originalamount");
            SecondTemplateFields.add("txdatetime");
            second.setTemplateFields(SecondTemplateFields);

            List<FileType> firstConfig = new ArrayList<FileType>();
            firstConfig.add(first);
            firstConfig.add(second);

            FileType third = new FileType();
            third.setTemplateName("EBC");
            List<String> ThirdTemplateFields = new ArrayList<String>();
            ThirdTemplateFields.add("otppan");
            ThirdTemplateFields.add("transactionamount");
            ThirdTemplateFields.add("validto");
            third.setTemplateFields(ThirdTemplateFields);

            FileType fourth = new FileType();
            fourth.setTemplateName("Visa");
            List<String> FourthTemplateFields = new ArrayList<String>();
            FourthTemplateFields.add("sourceoffundpan");
            FourthTemplateFields.add("originalamount");
            FourthTemplateFields.add("txdatetime");
            fourth.setTemplateFields(FourthTemplateFields);

            List<FileType> secondconfig = new ArrayList<FileType>();
            secondconfig.add(third);
            secondconfig.add(fourth);

            List<List<FileType>> config = new ArrayList<List<FileType>>();
            config.add(firstConfig);
            config.add(secondconfig);

            XStreamTranslator.getInstance().toXMLFile(config, "F:\\matchingxml.xml");
        } catch (IOException ex) {
            Logger.getLogger(Select.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
