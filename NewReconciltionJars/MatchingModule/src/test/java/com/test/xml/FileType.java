/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.xml;

import java.util.List;

/**
 *
 * @author shi7a
 */
public class FileType {

    private String TemplateName;
    private List<String> TemplateFields;

    public String getTemplateName() {
        return TemplateName;
    }

    public void setTemplateName(String TemplateName) {
        this.TemplateName = TemplateName;
    }

    public List<String> getTemplateFields() {
        return TemplateFields;
    }

    public void setTemplateFields(List<String> TemplateFields) {
        this.TemplateFields = TemplateFields;
    }

}
