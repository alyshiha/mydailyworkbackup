
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.googlecode.cqengine.ConcurrentIndexedCollection;
import com.googlecode.cqengine.IndexedCollection;
import com.googlecode.cqengine.attribute.SimpleAttribute;
import com.googlecode.cqengine.index.navigable.NavigableIndex;
import com.googlecode.cqengine.query.Query;
import static com.googlecode.cqengine.query.QueryFactory.and;
import static com.googlecode.cqengine.query.QueryFactory.equal;
import static com.googlecode.cqengine.query.QueryFactory.lessThan;
import com.googlecode.cqengine.query.option.QueryOptions;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import oracle.jdbc.pool.OracleDataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author shi7a
 */
public class ObjectQueryLab {

    private static JdbcTemplate jdbcTemplate;

    private static OracleDataSource dataSource() throws SQLException {
        OracleDataSource dataSource = new OracleDataSource();
        dataSource.setUser("cards");
        dataSource.setPassword("cards");
        dataSource.setURL("jdbc:oracle:thin:@192.168.1.79:1521/db");
        dataSource.setDriverType("oracle.jdbc.driver.OracleDriver");
        dataSource.setImplicitCachingEnabled(true);
        dataSource.setFastConnectionFailoverEnabled(true);
        return dataSource;
    }
    private static List<Map<String, Object>> records;

    public static void loadValues() {
        ObjectQueryLab engin = new ObjectQueryLab();
        jdbcTemplate.query("select nvl(licence_key,'none') FILETYPE,nvl(card_no,'none') MATCHKEY,nvl(transaction_sequence,'none') TRANSID from disputes where record_type = 1", new ResultSetExtractor() {
            @Override
            public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                records = Lists.newArrayList();

                while (rs.next()) {
                    records.add(engin.createTrans(rs.getString(1), rs.getString(2), rs.getString(3)));
                }
                return null;
            }
        });
    }

    @SuppressWarnings({"unchecked"})
    public static void main(String[] args) throws SQLException {
        jdbcTemplate = new JdbcTemplate(dataSource());
        ObjectQueryLab matchengin = new ObjectQueryLab();
        loadValues();
        matchengin.queryWithCpengine(records);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void queryWithCpengine(List<Map<String, Object>> records) {
        IndexedCollection<Map<String, Object>> collection = new ConcurrentIndexedCollection<Map<String, Object>>();
        collection.addAll(records);

        Map<String, SimpleAttribute> attributes = new ImmutableMap.Builder<String, SimpleAttribute>()
                .put("filetype", new MapAttribute("filetype"))
                .put("matchkey", new MapAttribute("matchkey"))
                .put("transid", new MapAttribute("transid"))
                .build();
        collection.addIndex(NavigableIndex.onAttribute(attributes.get("matchkey")));
        for (int i = 0; i < 1000000; i++) {
            Query<Map<String, Object>> query1 = equal(attributes.get("matchkey"), "9B3C877792F73DF7C2F973D8787A879DCEFCF5781DF86BA1AA4E4F2F409772E1");
            for (Map<String, Object> record : collection.retrieve(query1)) {
                //System.out.println(record);
            }
        }
    }

    private Map<String, Object> createTrans(String FILETYPE, String MATCHKEY, String TRANSID) {
        return ImmutableMap.<String, Object>builder()
                .put("filetype", FILETYPE)
                .put("matchkey", MATCHKEY)
                .put("transid", TRANSID)
                .build();
    }

    private class MapAttribute extends SimpleAttribute<Map<String, Object>, Object> {

        private String attrPath;

        public MapAttribute(String attrPath) {
            this.attrPath = attrPath;
        }

        @Override
        public Object getValue(Map<String, Object> object, QueryOptions queryOptions) {
            for (Map.Entry<String, Object> entrySet : object.entrySet()) {
                if (entrySet.getKey().equals(this.attrPath)) {
                    return entrySet.getValue();
                }
            }
            return "";
        }
    }
}
