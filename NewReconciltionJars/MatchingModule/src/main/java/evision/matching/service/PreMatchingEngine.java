package evision.matching.service;

import evision.matching.datamodel.DBConnection;
import evision.matching.datamodel.FileType;
import evision.util.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.pool.OracleDataSource;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.utils.SerializationUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

public class PreMatchingEngine {

    private static JdbcTemplate jdbcTemplate;
    public static Hashtable<String, String> MatchingSettings;
    public static HashMap<String, Dictionary> PendingRecord;

    public static void matchingtemplatesetter(DBConnection DBConnectionRecords) {
        try {
            jdbcTemplate = new JdbcTemplate(dataSource(DBConnectionRecords));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static OracleDataSource dataSource(DBConnection DBConnectionRecords) throws SQLException {
        OracleDataSource dataSource = new OracleDataSource();
        dataSource.setUser("atm");
        dataSource.setPassword("atm");
        dataSource.setURL("jdbc:oracle:thin:@192.168.31.161:1521/DB");
        dataSource.setDriverType("oracle.jdbc.driver.OracleDriver");
        dataSource.setImplicitCachingEnabled(true);
        dataSource.setFastConnectionFailoverEnabled(true);
        return dataSource;
    }

    public static void ReadSettings(String TemplateName) {
        try {
            String Fields = "";
            MatchingSettings = new Hashtable<String, String>();

            List<FileType> matchingRules = (List<FileType>) XStreamTranslator.getInstance().toObject(new File("ConfigFiles\\matchingSettings.xml"));
            for (FileType PairRules : matchingRules) {
                if (PairRules.getTemplateName().equals(TemplateName)) {
                    Fields = "";
                    for (String OtherFields : PairRules.getOtherTemplateFields()) {
                        Fields = Fields + OtherFields + "||";
                    }
                    MatchingSettings.put(PairRules.getOtherTemplateName(), Fields.substring(0, Fields.length() - 2));
                } else if (PairRules.getOtherTemplateName().equals(TemplateName)) {
                    Fields = "";
                    for (String OtherFields : PairRules.getTemplateFields()) {
                        Fields = Fields + OtherFields + "||";
                    }
                    MatchingSettings.put(PairRules.getTemplateName(), Fields.substring(0, Fields.length() - 2));
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(PreMatchingEngine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void loadValues(String FileType, String MatchField) {
        jdbcTemplate.query("select FILETYPE," + MatchField + " MATCHKEY,TRANSID from NEWRECONCILATION WHERE FILETYPE = '" + FileType + "'", new ResultSetExtractor() {
            @Override
            public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                while (rs.next()) {
                    if (PendingRecord.containsKey(rs.getString(1)) == Boolean.FALSE) {
                        PendingRecord.put(rs.getString(1), new Hashtable());
                    }
                    if (PendingRecord.get(rs.getString(1)).get(rs.getString(2)) == null) {
                        PendingRecord.get(rs.getString(1)).put(rs.getString(2), (new ArrayList<String>()));
                    }
                    ((ArrayList<String>) (PendingRecord.get(rs.getString(1)).get(rs.getString(2)))).add(rs.getString(3));
                }
                return null;
            }
        });
    }

}
