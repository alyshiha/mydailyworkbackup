package evision.matching.service;


import evision.matching.datamodel.FileType;
import evision.util.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.utils.SerializationUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author shi7a
 */
public class MatchingSettingsConfig {

    private static String setStructure(String[] FieldDefination) {
        switch (FieldDefination[1].toLowerCase()) {
            case "string":
                return FieldDefination[0];
            case "bigdecimal":
                return "replace(to_char(" + FieldDefination[0] + ",99999999999.99),' ','0')";
            case "integer":
                return "replace(to_char(" + FieldDefination[0] + ",99999999999),' ','0')";
            case "date":
                return "to_char(" + FieldDefination[0] + ",'dd/MM/yyyy HH24:MI:SS')";
            default:
                throw new IllegalArgumentException("Invalid DataType");
        }
    }

    public static Hashtable<String, String> ReadSettings(String TemplateName) {
        try {
            String Fields = "";
            Hashtable<String, String> MatchingSettings = new Hashtable<String, String>();

            List<FileType> matchingRules = (List<FileType>) XStreamTranslator.getInstance().toObject(new File(".\\ConfigFiles\\matchingSettings.xml"));
            for (FileType PairRules : matchingRules) {
                if (PairRules.getTemplateName().equals(TemplateName)) {
                    Fields = "";
                    for (String OtherFields : PairRules.getOtherTemplateFields()) {
                        Fields = Fields + setStructure(OtherFields.split(",")) + "||";
                    }
                    MatchingSettings.put(PairRules.getOtherTemplateName(), Fields.substring(0, Fields.length() - 2));
                } else if (PairRules.getOtherTemplateName().equals(TemplateName)) {
                    Fields = "";
                    for (String OtherFields : PairRules.getTemplateFields()) {
                        Fields = Fields + setStructure(OtherFields.split(",")) + "||";
                    }
                    MatchingSettings.put(PairRules.getTemplateName(), Fields.substring(0, Fields.length() - 2));
                }
            }
            return MatchingSettings;
        } catch (IOException ex) {
            Logger.getLogger(MatchingSettingsConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Hashtable<String, List<String[]>> ReadMasterSettings(String TemplateName) {
        try {
            List<String[]> Fields;
            Hashtable<String, List<String[]>> MatchingSettings = new Hashtable<String, List<String[]>>();

            List<FileType> matchingRules = (List<FileType>) XStreamTranslator.getInstance().toObject(new File(".\\ConfigFiles\\matchingSettings.xml"));
            for (FileType PairRules : matchingRules) {
                if (PairRules.getTemplateName().equals(TemplateName)) {
                    Fields = new ArrayList<String[]>();
                    for (String MasterFields : PairRules.getTemplateFields()) {
                        Fields.add(MasterFields.split(","));
                    }
                    MatchingSettings.put(PairRules.getOtherTemplateName(), Fields);
                } else if (PairRules.getOtherTemplateName().equals(TemplateName)) {
                    Fields = new ArrayList<String[]>();
                    for (String OtherFields : PairRules.getOtherTemplateFields()) {
                        Fields.add(OtherFields.split(","));
                    }
                    MatchingSettings.put(PairRules.getTemplateName(), Fields);
                }
            }
            return MatchingSettings;
        } catch (IOException ex) {
            Logger.getLogger(MatchingSettingsConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static Map<?, ?> deserialize(Message MSG) {
        return (Map<?, ?>) SerializationUtils.deserialize(MSG.getBody());
    }

    private static final DecimalFormat decimalFormat = new DecimalFormat("000000000000.00");
    private static final DecimalFormat numberFormat = new DecimalFormat("000000000000");

    public static Map<String, String> setSearchValue(Message MSG, Hashtable<String, List<String[]>> MasterMatchingSettings) {

        //Create HashMap
        Map<String, String> Values = new HashMap<String, String>();
        Set<String> keys = MasterMatchingSettings.keySet();
        //Obtaining iterator over set entries
        Iterator<String> itr = keys.iterator();
        //Displaying Key and value pairs
        while (itr.hasNext()) {
            String SearchValue = "";
            // Getting Key
            String Type = itr.next();
            for (Iterator<String[]> it = ((List<String[]>) MasterMatchingSettings.get(Type)).iterator(); it.hasNext();) {
                String[] Field = it.next();
                Object Value = deserialize(MSG).get((String) Field[0]);
                switch ((String) Field[1].toLowerCase()) {
                    case "string":
                        SearchValue = SearchValue + String.valueOf(Value);
                        break;
                    case "bigdecimal":
                        SearchValue = SearchValue + decimalFormat.format((BigDecimal) Value);
                        break;
                    case "integer":
                        SearchValue = SearchValue + numberFormat.format((Integer) Value);
                        break;
                    case "date":
                        SearchValue = SearchValue + (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).format((Date) Value);
                        break;
                    default:
                        throw new IllegalArgumentException("Invalid DataType");
                }
            }
            Values.put(Type, SearchValue);
        }
        return Values;
    }

}
