package evision.matching.service;

import static com.googlecode.cqengine.query.QueryFactory.equal;
import com.googlecode.cqengine.ConcurrentIndexedCollection;
import com.googlecode.cqengine.IndexedCollection;
import com.googlecode.cqengine.index.hash.HashIndex;
import com.googlecode.cqengine.query.Query;
import evision.matching.datamodel.DBConnection;
import evision.matching.datamodel.MatchingTemplate;
import evision.mq.service.RabbitMqConfig;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.pool.OracleDataSource;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

public class MatchingEngine {

    private static String RunningSide = "idel";
    private static Boolean isDispute;
    private static JdbcTemplate jdbcTemplate;
    private static Hashtable<String, IndexedCollection<MatchingTemplate>> OtherSides;
    private static String[] SidesToMatch;
    private static Hashtable<String, List<String[]>> MasterMatchingSettings;

    public static void TryToMatch(Message MSG, String GivenType, RabbitTemplate rabbitTemplate) throws Exception {
        if (!GivenType.equals(RunningSide)) {
            Hashtable<String, String> PreMatchingSettings = MatchingSettingsConfig.ReadSettings(GivenType);
            SidesToMatch = PreMatchingSettings.keySet().toArray(new String[PreMatchingSettings.keySet().size()]);
            MasterMatchingSettings = MatchingSettingsConfig.ReadMasterSettings(GivenType);
            PrematchRunner(GivenType, SidesToMatch, PreMatchingSettings);
        }
        MSG.getMessageProperties().getHeaders().put("matchedlegscount", SidesToMatch.length);
        SearchForRecord(MatchingSettingsConfig.setSearchValue(MSG, MasterMatchingSettings), MSG, rabbitTemplate);

    }

    private static void SearchForRecord(Map<String, String> Keys, Message MSG, RabbitTemplate rabbitTemplate) throws Exception {
        for (Map.Entry<String, String> entrySet : Keys.entrySet()) {
            indexedSearchForEquals(entrySet.getKey(), entrySet.getValue(), MSG, rabbitTemplate);
        }
        if (((int) MSG.getMessageProperties().getHeaders().get("matchedlegscount")) >= 0) {
            rabbitTemplate.setRoutingKey(RabbitMqConfig.SIMPLE_MESSAGE_QUEUE_Pending);
            rabbitTemplate.convertAndSend(MSG);
            //System.out.println("Insert Into Pending" + MSG);
        } else {
            rabbitTemplate.setRoutingKey(RabbitMqConfig.SIMPLE_MESSAGE_QUEUE_Matched);
            rabbitTemplate.convertAndSend(MSG);
        }
    }

    private static void PrematchRunner(String GivenType, String[] SidesToMatch, Hashtable<String, String> PreMatchingSettings) throws Exception {
        //If Given Side Not Equal RunningSide
        jdbcTemplate = new JdbcTemplate(dataSource());
        buildIndexedCollection(SidesToMatch);
        loadValues(SidesToMatch, PreMatchingSettings);
        RunningSide = GivenType;
    }

    private static OracleDataSource dataSource() throws SQLException {
        OracleDataSource dataSource = new OracleDataSource();
        dataSource.setUser("mvisa");
        dataSource.setPassword("a");
        dataSource.setURL("jdbc:oracle:thin:@192.168.1.89:1521/b2db");
        dataSource.setDriverType("oracle.jdbc.driver.OracleDriver");
        dataSource.setImplicitCachingEnabled(true);
        dataSource.setFastConnectionFailoverEnabled(true);
        return dataSource;
    }

    private static void loadValues(String[] SidesToMatch, Hashtable<String, String> PreMatchingSettings) {
        for (String Side : SidesToMatch) {
            jdbcTemplate.query("select transid," + PreMatchingSettings.get(Side).toString() + ",matchedlegscount from NEWRECONCILATION WHERE FileType = '" + Side + "'", new ResultSetExtractor() {
                @Override
                public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                    while (rs.next()) {
                        ((IndexedCollection<MatchingTemplate>) (OtherSides.get(Side))).add(new MatchingTemplate(rs.getString(1), rs.getString(2), rs.getString(3)));
                    }
                    return null;
                }
            });
        }
    }

    private static void indexedSearchForEquals(String Side, String Key, Message MSG, RabbitTemplate rabbitTemplate) throws Exception {
        Query query = equal(MatchingTemplate.MATCHING_KEY, Key);
        
        for (Iterator it = ((IndexedCollection<MatchingTemplate>) (OtherSides.get(Side))).retrieve(query).iterator(); it.hasNext();) {
            MatchingTemplate record = (MatchingTemplate) it.next();
            if (it.hasNext()) {
                MSG.getMessageProperties().getHeaders().remove("matchedlegscount");
                MSG.getMessageProperties().getHeaders().put("otherside", Side);
                MSG.getMessageProperties().getHeaders().put("errorreason", "To Many Records To Match");
                rabbitTemplate.setRoutingKey(RabbitMqConfig.SIMPLE_MESSAGE_QUEUE_Matching_Error);
                rabbitTemplate.convertAndSend(MSG);
                //System.out.println("Matched Error For Transaction Id:" + MSG);
                break;
            } else {
                MSG.getMessageProperties().getHeaders().put("matchedlegscount", (Integer) (MSG.getMessageProperties().getHeaders().get("matchedlegscount")) - 1);
                //System.out.println("Matched From Transaction Id:" + Key + "To Transaction Id:" + record.getTransactionid());
                break;
            }
        }
    }

    private static void buildIndexedCollection(String[] SidesToMatch) throws Exception {
        OtherSides = new Hashtable<String, IndexedCollection<MatchingTemplate>>();
        for (String side : SidesToMatch) {
            OtherSides.put(side, new ConcurrentIndexedCollection<MatchingTemplate>());
            ((IndexedCollection<MatchingTemplate>) (OtherSides.get(side))).addIndex(HashIndex.onAttribute(MatchingTemplate.MATCHING_KEY));
        }
    }

}
