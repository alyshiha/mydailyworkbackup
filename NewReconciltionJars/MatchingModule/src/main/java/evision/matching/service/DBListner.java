/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.matching.service;

import evision.matching.controller.MatchingClient;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

public class DBListner implements MessageListener {
    @Override
    public void onMessage(Message message) {
        MatchingClient.run(message,message.getMessageProperties().getHeaders().get("filetype").toString());
    }
}
