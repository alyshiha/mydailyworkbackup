/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.matching.datamodel;

import java.util.List;

/**
 *
 * @author shi7a
 */
public class FileType {

    private String TemplateName;
    private List<String> TemplateFields;
    private List<String> TemplateColumns;
    private String OtherTemplateName;
    private List<String> OtherTemplateFields;
    private List<String> OtherTemplateColumns;

    public List<String> getTemplateColumns() {
        return TemplateColumns;
    }

    public void setTemplateColumns(List<String> TemplateColumns) {
        this.TemplateColumns = TemplateColumns;
    }

    public List<String> getOtherTemplateColumns() {
        return OtherTemplateColumns;
    }

    public void setOtherTemplateColumns(List<String> OtherTemplateColumns) {
        this.OtherTemplateColumns = OtherTemplateColumns;
    }

    public String getOtherTemplateName() {
        return OtherTemplateName;
    }

    public void setOtherTemplateName(String OtherTemplateName) {
        this.OtherTemplateName = OtherTemplateName;
    }

    public List<String> getOtherTemplateFields() {
        return OtherTemplateFields;
    }

    public void setOtherTemplateFields(List<String> OtherTemplateFields) {
        this.OtherTemplateFields = OtherTemplateFields;
    }

    public String getTemplateName() {
        return TemplateName;
    }

    public void setTemplateName(String TemplateName) {
        this.TemplateName = TemplateName;
    }

    public List<String> getTemplateFields() {
        return TemplateFields;
    }

    public void setTemplateFields(List<String> TemplateFields) {
        this.TemplateFields = TemplateFields;
    }

}
