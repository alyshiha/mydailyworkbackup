/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.matching.controller;

import evision.matching.datamodel.DBConnection;
import evision.matching.service.MatchingEngine;
import evision.mq.service.RabbitMqConfig;
import evision.util.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author Aly
 */
public class MatchingClient {

    private static RabbitAdmin rabbitadmin;
    private static RabbitTemplate rabbitTemplate;
    private static DBConnection DBConnectionRecords;
    private static MatchingEngine engine;

    public MatchingClient() {
        try {
            ApplicationContext ctx = new AnnotationConfigApplicationContext(RabbitMqConfig.class);
            rabbitTemplate = ctx.getBean(RabbitTemplate.class);
            rabbitadmin = new RabbitAdmin(rabbitTemplate.getConnectionFactory());
            DBConnectionRecords = ((List<DBConnection>) XStreamTranslator.getInstance().toObject(new File(".\\ConfigFiles\\DBConnection.xml"))).get(0);
        } catch (IOException ex) {
            Logger.getLogger(MatchingClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void run(Message MSG, String FileType) {
        try {
            MatchingEngine.TryToMatch(MSG, FileType,rabbitTemplate);
        } catch (Exception ex) {
            Logger.getLogger(MatchingClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
