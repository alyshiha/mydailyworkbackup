package evision.mq.service;

import evision.matching.service.MatchingListner;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.memorynotfound.rabbitmq")
public class RabbitMqConfig {

    public static final String SIMPLE_MESSAGE_QUEUE_ReadyToMatch = "Evision.Transaction.ReadyToMatch";
    public static final String SIMPLE_MESSAGE_QUEUE_Pending = "Evision.Transaction.Pending";
    public static final String SIMPLE_MESSAGE_QUEUE_Matched = "Evision.Transaction.Matched";
    public static final String SIMPLE_MESSAGE_QUEUE_Matching_Error = "Evision.Transaction.Error";

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("localhost");
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        return connectionFactory;

    }

    @Bean
    public Queue simpleQueueDispute() {
        return new Queue(SIMPLE_MESSAGE_QUEUE_ReadyToMatch);
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        return template;
    }

    @Bean
    public SimpleMessageListenerContainer listenerContainer() {
        SimpleMessageListenerContainer listenerContainer = new SimpleMessageListenerContainer();
        listenerContainer.setConnectionFactory(connectionFactory());
        listenerContainer.setQueues(simpleQueueDispute());
        listenerContainer.setMessageConverter(jsonMessageConverter());
        listenerContainer.setMessageListener(new MatchingListner());
        listenerContainer.setAcknowledgeMode(AcknowledgeMode.AUTO);
        listenerContainer.setStartConsumerMinInterval(1);
        return listenerContainer;
    }

}
