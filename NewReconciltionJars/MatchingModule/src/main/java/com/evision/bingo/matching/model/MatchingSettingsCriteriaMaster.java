/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.matching.model;

import com.evision.bingo.core.model.MatchingConfigurable;
import java.util.List;

/**
 *
 * @since 18/7/2017
 * @author Aly Shiha
 *
 * Matching Master Config
 */
public class MatchingSettingsCriteriaMaster implements MatchingConfigurable{

    private String mapField;
    private String otherSideDBField;
    private String otherSideTable;
    private String criteriaOperator;
    private List<MatchingSettingsCriteriaDetails> criteriaDetails;
    private String userComment;

    public String getMapField() {
        return mapField;
    }

    public void setMapField(String mapField) {
        this.mapField = mapField;
    }

    public String getOtherSideTable() {
        return otherSideTable;
    }

    public void setOtherSideTable(String otherSideTable) {
        this.otherSideTable = otherSideTable;
    }

    public String getCriteriaOperator() {
        return criteriaOperator;
    }

    public void setCriteriaOperator(String criteriaOperator) {
        this.criteriaOperator = criteriaOperator;
    }

    public List<MatchingSettingsCriteriaDetails> getCriteriaDetails() {
        return criteriaDetails;
    }

    public void setCriteriaDetails(List<MatchingSettingsCriteriaDetails> criteriaDetails) {
        this.criteriaDetails = criteriaDetails;
    }

    public String getUserComment() {
        return userComment;
    }

    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }

    public String getOtherSideDBField() {
        return otherSideDBField;
    }

    public void setOtherSideDBField(String otherSideDBField) {
        this.otherSideDBField = otherSideDBField;
    }
}
