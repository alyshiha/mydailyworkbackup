/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.matching.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.model.MatchingConfig;
import com.evision.bingo.core.model.MatchingConfigurable;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.matching.service.iMatchingPostProcessor;
import com.evision.bingo.matching.service.iMatchingPreProcessor;
import java.util.ArrayList;
import java.util.List;

/**
 * @since 31/7/2017
 * @author Khaled Khalil
 *
 */
public abstract class BaseMatchingController extends BaseController {
    //collection of registered pre-processors

    private List<iMatchingPreProcessor> registeredPreProcessors;

    //collection of registered post-processors
    private List<iMatchingPostProcessor> registeredPostProcessors;

    protected List<MatchingConfigurable> subConfigList = new ArrayList();

    public BaseMatchingController(LogHandler logHandler, QueueHandler queueHandler, MatchingConfig matchingConfig, List<MatchingConfigurable> subConfigListParam, List<BaseController> eventsHandlerList) {
        super(logHandler, queueHandler, matchingConfig, eventsHandlerList);

        if (subConfigListParam != null && !subConfigListParam.isEmpty()) {
            for (Configurable config : subConfigListParam) {
                this.subConfigList.add((MatchingConfigurable) config);
            }
        }

        init();
    }

    @Override
    public final void init() {
        registeredPreProcessors = new ArrayList<>();
        registeredPostProcessors = new ArrayList<>();
    }

    @Override
    public synchronized final boolean handleRequest(Object obj) {
        this.setObj(obj);
        if (preCollect()) {
            if (doCollect()) {
                return postCollect();
            }
        }
        return false;
    }

    @Override
    public final void destroy() {
        registeredPreProcessors.clear();
        registeredPostProcessors.clear();

        registeredPreProcessors = null;
        registeredPostProcessors = null;
    }

    //register a pre-processor
    final void registerPreProcessor(iMatchingPreProcessor preProcessor) {
        registeredPreProcessors.add(preProcessor);
    }

    //unregister a pre-processor
    final void unregisterPreProcessor(iMatchingPreProcessor preProcessor) {
        registeredPreProcessors.remove(preProcessor);
    }

    //register a post-processor
    final void registerPostProcessor(iMatchingPostProcessor postProcessor) {
        registeredPostProcessors.add(postProcessor);
    }

    //unregister a post-processor
    final void unregisterPostProcessor(iMatchingPostProcessor postProcessor) {
        registeredPostProcessors.remove(postProcessor);
    }

    //call of all registered pre-processors
    final synchronized boolean preCollect() {
        if (!registeredPreProcessors.isEmpty()) {
            for (iMatchingPreProcessor preProcessor : registeredPreProcessors) {
                if (!preProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //call of all registered post-processors
    final synchronized boolean postCollect() {
        if (!registeredPostProcessors.isEmpty()) {
            for (iMatchingPostProcessor postProcessor : registeredPostProcessors) {
                if (!postProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //method of collect to be overidden
    abstract boolean doCollect();
}
