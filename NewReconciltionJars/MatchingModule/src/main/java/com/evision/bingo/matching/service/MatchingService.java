/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.matching.service;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.DBLoaderConfig;
import com.evision.bingo.core.model.MatchingConfigurable;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.matching.model.MatchingSettingsCriteriaDetails;
import com.evision.bingo.matching.model.MatchingSettingsCriteriaMaster;
import com.evision.bingo.matching.model.MatchingSettingsGroup;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.googlecode.cqengine.query.Query;
import oracle.jdbc.pool.OracleDataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import com.googlecode.cqengine.ConcurrentIndexedCollection;
import com.googlecode.cqengine.IndexedCollection;
import com.googlecode.cqengine.attribute.Attribute;
import static com.googlecode.cqengine.query.QueryFactory.mapAttribute;
import java.util.ArrayList;
import java.util.Date;
import org.codehaus.jackson.map.ObjectMapper;
import com.googlecode.cqengine.index.navigable.NavigableIndex;
import java.util.Enumeration;
import org.springframework.amqp.core.MessageProperties;

/**
 *
 * @author Aly-Shiha
 */
public class MatchingService extends BaseMatchingService {

    private String currentRunningType = "";
    private HashMap<String, Object[]> otherSideData = new HashMap<String, Object[]>();
    private String query = "";
    private JdbcTemplate jdbcTemplate;
    private Dictionary data, result;
    private Map<String, Object> sampledata;
    private Map<String, Attribute> attributes;

    public MatchingService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, eventsHandlerList);

        jdbcTemplate = new JdbcTemplate(dataSource());
    }

    @Override
    public boolean match(Map messageData, MessageProperties messageProperties, List<MatchingConfigurable> matchingConfigList) {
        try {
            for (MatchingConfigurable configurable : matchingConfigList) {
                MatchingSettingsGroup fileGroup = (MatchingSettingsGroup) configurable;

                if (fileGroup.getGroupType().toLowerCase().equals(((String) messageProperties.getHeaders().get("fileType")).toLowerCase())) {
                    if (!currentRunningType.equals(((String) messageProperties.getHeaders().get("fileType")).toLowerCase())) {
                        runPreMatch(fileGroup);
                        currentRunningType = ((String) messageProperties.getHeaders().get("fileType")).toLowerCase();
                    }

                    IndexedCollection<Map<String, Object>> OutPut = new ConcurrentIndexedCollection<Map<String, Object>>();
                    Boolean FirstRound = Boolean.TRUE;
                    for (MatchingSettingsCriteriaMaster criteriasGroup : fileGroup.getCriterias()) {
                        Query<Map<String, Object>> query = CriteriaCreator.getTypeOfOperator(criteriasGroup, (Map<String, Attribute>) otherSideData.get(criteriasGroup.getOtherSideTable())[1], messageProperties.getHeaders());
                        OutPut = queryWithCpengine(query, (Dictionary) otherSideData.get(criteriasGroup.getOtherSideTable())[0], (String) messageProperties.getHeaders().get(criteriasGroup.getMapField().toLowerCase()), OutPut, FirstRound);
                        FirstRound = Boolean.FALSE;
                        if (OutPut.size() > 0) {
                            Map<String, Object> MResult = new HashMap<String, Object>();
                            MResult.put("UserComment", criteriasGroup.getUserComment());
                            MResult.put("MatchingTime", new Date());
                            MResult.put("GroupName", fileGroup.getGroupName());
                            MResult.put("TransactionID", messageProperties.getHeaders().get("transId").toString());
                            List<String> SubTransResult = new ArrayList<String>();
                            for (Map<String, Object> OutPutRecord : OutPut) {
                                SubTransResult.add(OutPutRecord.get("AMOUNT").toString());
                            }
                            MResult.put("OtherSideID", SubTransResult);
                            break;
                        }
                    }
                    if (OutPut.size() == 0) {
                        Map<String, Object> DResult = new HashMap<String, Object>();
                        DResult.put("DisputeTime", new Date());
                        DResult.put("TransactionID", messageProperties.getHeaders().get("transId").toString());
                    }
                }
            }
        } catch (Exception ex) {
            logHandler.logException(query, ex);
        }
        return true;
    }

    private OracleDataSource dataSource() {
        OracleDataSource dataSource = null;
        try {
            dataSource = new OracleDataSource();
            dataSource.setUser("cards");
            dataSource.setPassword("cards");
            dataSource.setURL("jdbc:oracle:thin:@192.168.42.159:1521/db");
            dataSource.setDriverType("oracle.jdbc.driver.OracleDriver");
            dataSource.setImplicitCachingEnabled(true);
            dataSource.setFastConnectionFailoverEnabled(true);
        } catch (SQLException ex) {
            logHandler.logException(query, ex);
        }
        return dataSource;
    }

    private void runPreMatch(MatchingSettingsGroup fileGroup) throws SQLException {
        otherSideData.clear();

        for (MatchingSettingsCriteriaMaster criteriaGroup : fileGroup.getCriterias()) {
            //set query
            query = generateQuery(criteriaGroup);

            //load other side data
            data = loadValues();

            //create dynamic map from the result
            Map<String, Attribute> attributes = createAttributes();

            //create index
            indexGenerator(data, attributes, criteriaGroup.getOtherSideDBField());
            assignResult(criteriaGroup.getOtherSideTable(), data, attributes);
        }
    }

    private void assignResult(String FileType, Dictionary Data, Map<String, Attribute> Attributes) {
        Object[] SideData = new Object[2];
        SideData[0] = Data;
        SideData[1] = Attributes;
        otherSideData.put(FileType, SideData);
    }

    private IndexedCollection<Map<String, Object>> queryWithCpengine(com.googlecode.cqengine.query.Query<Map<String, Object>> Query, Dictionary dictionary, String MapKey, IndexedCollection<Map<String, Object>> OutPut, Boolean FirstRound) {
        IndexedCollection<Map<String, Object>> Result = new ConcurrentIndexedCollection<Map<String, Object>>();
        if (FirstRound) {
            IndexedCollection<Map<String, Object>> Records = (IndexedCollection<Map<String, Object>>) dictionary.get(MapKey);
            if (Records != null) {
                for (Map<String, Object> record : Records.retrieve(Query)) {
                    Result.add(record);
                }
            }
        } else {
            for (Map<String, Object> record : OutPut.retrieve(Query)) {
                Result.add(record);
            }
        }

        return Result;
    }

    private void indexGenerator(Dictionary Data, Map<String, Attribute> Attributes, String IndexGroup) {
        attributes = Attributes;
        //create index and group by to enhance searching
        createIndex(IndexGroup, Data);
    }

    private void createIndex(String attribute, Dictionary Records) {
        Enumeration<String> e = Records.keys();
        while (e.hasMoreElements()) {
            IndexedCollection<Map<String, Object>> temp = (IndexedCollection<Map<String, Object>>) Records.get(e.nextElement());
            temp.addIndex(NavigableIndex.onAttribute(attributes.get(attribute.toUpperCase())));
        }
    }

    private Map<String, Attribute> createAttributes() throws SQLException {
        com.google.common.collect.ImmutableMap.Builder<String, Attribute> attribute;
        attribute = com.google.common.collect.ImmutableMap.builder();
        for (Map.Entry<String, Object> data : sampledata.entrySet()) {
            if (data.getValue() == null) {
                attribute.put(data.getKey(), mapAttribute(data.getKey(), String.class));
            } else {
                attribute.put(data.getKey(), mapAttribute(data.getKey(), data.getValue().getClass()));
            }
        }
        return attribute.build();
    }

    private Dictionary loadValues() {
        jdbcTemplate.query(query, new ResultSetExtractor() {
            @Override
            public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                result = createTrans(rs);
                return result;
            }
        });
        return result;
    }

    private Dictionary createTrans(ResultSet rs) {
        try {
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();
            Map<String, Object> row = null;
            while (rs.next()) {
                row = new HashMap<String, Object>(columns);
                for (int i = 1; i <= columns; ++i) {
                    row.put(md.getColumnName(i), rs.getObject(i));
                }
                if (result.get(rs.getObject(1)) == null) {
                    sampledata = row;
                    IndexedCollection<Map<String, Object>> records = new ConcurrentIndexedCollection<Map<String, Object>>();
                    records.add(row);
                    result.put(rs.getObject(1), records);
                } else {
                    IndexedCollection<Map<String, Object>> records = (IndexedCollection<Map<String, Object>>) result.get(rs.getObject(1));
                    records.add(row);
                    result.put(rs.getObject(1), records);
                }
            }
            return result;
        } catch (SQLException ex) {
            logHandler.logException(query, ex);
        }
        return null;
    }

    private String generateQuery(MatchingSettingsCriteriaMaster criteriaGroup) {
        String query = "select  /*+ parallel(4) */ " + criteriaGroup.getOtherSideDBField();
        for (MatchingSettingsCriteriaDetails criteria : criteriaGroup.getCriteriaDetails()) {
            query = query + "," + criteria.getDataBaseField();
        }
        query = query + " from " + criteriaGroup.getOtherSideTable();
        return query;
    }

    @Override
    public boolean connect(DBLoaderConfig config) {
        return true;
    }

    @Override
    public void disconnect() {
    }
}
