/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.matching.model;

import com.evision.bingo.core.model.MatchingConfigurable;
import java.util.List;

/**
 *
 * @since 18/7/2017
 * @author Aly Shiha
 *
 * Matching Group Config
 */
public class MatchingSettingsGroup implements MatchingConfigurable{
    private String groupName;
    private String groupType;
    private int priorityLevel;
    private boolean isActive;
    private List<MatchingSettingsCriteriaMaster> criterias;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public int getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(int priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public List<MatchingSettingsCriteriaMaster> getCriterias() {
        return criterias;
    }

    public void setCriterias(List<MatchingSettingsCriteriaMaster> criterias) {
        this.criterias = criterias;
    }
}