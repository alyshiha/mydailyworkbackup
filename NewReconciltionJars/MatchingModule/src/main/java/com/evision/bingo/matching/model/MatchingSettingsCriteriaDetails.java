/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.matching.model;

import com.evision.bingo.core.model.MatchingConfigurable;

/**
 *
 * @since 18/7/2017
 * @author Aly Shiha
 *
 * Matching Mapping Config
 */
public class MatchingSettingsCriteriaDetails implements MatchingConfigurable {

    private String dataBaseField;
    private String mapField;
    private String operatorAlias;
    private int addValue;
    private int subtractValue;
    private boolean isTrueOperator;

    public String getDataBaseField() {
        return dataBaseField;
    }

    public void setDataBaseField(String dataBaseField) {
        this.dataBaseField = dataBaseField;
    }

    public String getMapField() {
        return mapField;
    }

    public void setMapField(String mapField) {
        this.mapField = mapField;
    }

    public String getOperatorAlias() {
        return operatorAlias;
    }

    public void setOperatorAlias(String operatorAlias) {
        this.operatorAlias = operatorAlias;
    }

    public Integer getAddValue() {
        return addValue;
    }

    public void setAddValue(int addValue) {
        this.addValue = addValue;
    }

    public Integer getSubtractValue() {
        return subtractValue;
    }

    public void setSubtractValue(int subtractValue) {
        this.subtractValue = subtractValue;
    }

    public boolean isIsTrueOperator() {
        return isTrueOperator;
    }

    public void setIsTrueOperator(boolean isTrueOperator) {
        this.isTrueOperator = isTrueOperator;
    }
}
