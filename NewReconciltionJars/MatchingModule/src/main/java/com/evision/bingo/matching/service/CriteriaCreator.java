/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.matching.service;

import com.evision.bingo.matching.model.MatchingSettingsCriteriaDetails;
import com.evision.bingo.matching.model.MatchingSettingsCriteriaMaster;
import com.googlecode.cqengine.attribute.Attribute;
import com.googlecode.cqengine.query.Query;
import com.googlecode.cqengine.query.QueryFactory;
import java.math.BigDecimal;
import java.util.Map;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.time.DateUtils;

/**
 *
 * @author Aly-Shiha
 */
public class CriteriaCreator {

    private static <T> T convertInstanceOfObject(Object o, String ClassName) throws ClassNotFoundException {
        try {
            Class<T> clazz = getMyType(ClassName);
            return clazz.cast(o);
        } catch (ClassCastException e) {
            return null;
        }
    }

    private static Class getMyType(String ClassName) throws ClassNotFoundException {
        return Class.forName(ClassName.replace("class ", ""));
    }

    public static Query<Map<String, Object>> getTypeOfOperator(MatchingSettingsCriteriaMaster Master, Map<String, Attribute> attributes, Map<?, ?> GivenRecord) {
        try {
            List<Query<Map<String, Object>>> userQueries = new ArrayList<Query<Map<String, Object>>>();
            for (MatchingSettingsCriteriaDetails DetailRecord : Master.getCriteriaDetails()) {
                switch (DetailRecord.getOperatorAlias()) {
                    case "Equality":
                        if (DetailRecord.isIsTrueOperator()) {
                            userQueries.add(QueryFactory.equal(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase())));
                        } else {
                            userQueries.add(QueryFactory.not(QueryFactory.equal(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()))));
                        }
                        break;
                    case "Less Than":
                        if (DetailRecord.isIsTrueOperator()) {

                            userQueries.add(QueryFactory.lessThan(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString())));

                        } else {
                            userQueries.add(com.googlecode.cqengine.query.QueryFactory.not(QueryFactory.lessThan(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString()))));
                        }
                        break;
                    case "Greater Than":
                        if (DetailRecord.isIsTrueOperator()) {
                            userQueries.add(QueryFactory.greaterThan(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString())));
                        } else {
                            userQueries.add(com.googlecode.cqengine.query.QueryFactory.not(QueryFactory.greaterThan(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString()))));
                        }
                        break;
                    case "Less Than Or Equal":
                        if (DetailRecord.isIsTrueOperator()) {
                            userQueries.add(QueryFactory.lessThanOrEqualTo(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString())));
                        } else {
                            userQueries.add(com.googlecode.cqengine.query.QueryFactory.not(QueryFactory.lessThanOrEqualTo(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString()))));
                        }
                        break;
                    case "Greater Than Or Equal":
                        if (DetailRecord.isIsTrueOperator()) {
                            userQueries.add(QueryFactory.greaterThanOrEqualTo(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString())));
                        } else {
                            userQueries.add(com.googlecode.cqengine.query.QueryFactory.not(QueryFactory.greaterThanOrEqualTo(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString()))));
                        }
                        break;
                    case "Between Date":
                        if (DetailRecord.isIsTrueOperator()) {
                            userQueries.add(QueryFactory.between(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), DateUtils.addMinutes((Date) GivenRecord.get(DetailRecord.getMapField().toLowerCase()), DetailRecord.getSubtractValue() * -1), DateUtils.addMinutes((Date) GivenRecord.get(DetailRecord.getMapField().toLowerCase()), DetailRecord.getAddValue())));
                        } else {
                            userQueries.add(QueryFactory.not(QueryFactory.between(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), DateUtils.addMinutes((Date) GivenRecord.get(DetailRecord.getMapField().toLowerCase()), DetailRecord.getSubtractValue() * -1), DateUtils.addMinutes((Date) GivenRecord.get(DetailRecord.getMapField().toLowerCase()), DetailRecord.getAddValue()))));
                        }
                        break;
                    case "Between BigDecimal":
                        if (DetailRecord.isIsTrueOperator()) {
                            userQueries.add(QueryFactory.between(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), ((BigDecimal) GivenRecord.get(DetailRecord.getMapField().toLowerCase())).add(new BigDecimal(DetailRecord.getSubtractValue()).multiply(new BigDecimal(-1))), ((BigDecimal) GivenRecord.get(DetailRecord.getMapField().toLowerCase())).add(new BigDecimal(DetailRecord.getAddValue()).multiply(new BigDecimal(1)))));
                        } else {
                            userQueries.add(com.googlecode.cqengine.query.QueryFactory.not(QueryFactory.between(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), ((BigDecimal) GivenRecord.get(DetailRecord.getMapField().toLowerCase())).add(new BigDecimal(DetailRecord.getSubtractValue().toString()).multiply(new BigDecimal(-1))), ((BigDecimal) GivenRecord.get(DetailRecord.getMapField().toLowerCase())).add(new BigDecimal(DetailRecord.getAddValue().toString()).multiply(new BigDecimal(1))))));
                        }
                        break;
                    case "Starts With":
                        if (DetailRecord.isIsTrueOperator()) {
                            userQueries.add(QueryFactory.startsWith(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString())));
                        } else {
                            userQueries.add(com.googlecode.cqengine.query.QueryFactory.not(QueryFactory.startsWith(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString()))));
                        }
                        break;
                    case "Ends With":
                        if (DetailRecord.isIsTrueOperator()) {
                            userQueries.add(QueryFactory.endsWith(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString())));
                        } else {
                            userQueries.add(com.googlecode.cqengine.query.QueryFactory.not(QueryFactory.endsWith(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString()))));
                        }
                        break;
                    case "Contains":
                        if (DetailRecord.isIsTrueOperator()) {
                            userQueries.add(QueryFactory.contains(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString())));
                        } else {
                            userQueries.add(com.googlecode.cqengine.query.QueryFactory.not(QueryFactory.contains(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), convertInstanceOfObject(GivenRecord.get(DetailRecord.getMapField().toLowerCase()), GivenRecord.get(DetailRecord.getMapField().toLowerCase()).getClass().toString()))));
                        }
                        break;
                    case "Regular Expression":
                        if (DetailRecord.isIsTrueOperator()) {
                            userQueries.add(QueryFactory.matchesRegex(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), ""));
                        } else {
                            userQueries.add(com.googlecode.cqengine.query.QueryFactory.not(QueryFactory.matchesRegex(attributes.get(DetailRecord.getDataBaseField().toUpperCase()), "")));
                        }
                        break;
                    case "Has":
                        if (DetailRecord.isIsTrueOperator()) {
                            userQueries.add(QueryFactory.has(attributes.get(DetailRecord.getDataBaseField().toUpperCase())));
                        } else {
                            userQueries.add(com.googlecode.cqengine.query.QueryFactory.not(QueryFactory.has(attributes.get(DetailRecord.getDataBaseField().toUpperCase()))));
                        }
                        break;
                    default:
                        throw new IllegalArgumentException("Invalid Operator: " + DetailRecord.getOperatorAlias());
                }
            }
            Query<Map<String, Object>> finalQuery;
            if (Master.getCriteriaOperator().equals("And")) {
                if (userQueries.size() == 1) {
                    finalQuery = userQueries.get(0);
                } else if (userQueries.size() == 2) {
                    finalQuery = com.googlecode.cqengine.query.QueryFactory.and(userQueries.get(0), userQueries.get(1));
                } else {
                    finalQuery = com.googlecode.cqengine.query.QueryFactory.and(userQueries.get(0), userQueries.get(1), userQueries.subList(2, userQueries.size()));
                }
            } else {
                finalQuery = com.googlecode.cqengine.query.QueryFactory.or(userQueries.get(0), userQueries.get(1), userQueries.subList(2, userQueries.size()));
            }

            return finalQuery;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CriteriaCreator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
