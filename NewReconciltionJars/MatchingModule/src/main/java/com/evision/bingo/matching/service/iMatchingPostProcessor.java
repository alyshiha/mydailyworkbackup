/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.matching.service;

import com.evision.bingo.core.controller.BaseController;

/**
 * @since 31/7/2017
 * @author Khaled Khalil
 *
 * base interface for post-processors
 */
public interface iMatchingPostProcessor {

    public boolean process(BaseController baseController);
}
