/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.matching.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.MatchingConfig;
import com.evision.bingo.core.model.MatchingConfigurable;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.matching.model.MatchingSettingsGroup;
import com.evision.bingo.matching.service.MatchingService;
import com.evision.bingo.matching.utils.MatchingDefines;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.JsonMessageConverter;

/**
 * @since 31/7/2017
 * @author Khaled Khalil
 *
 */
public class MatchingController extends BaseMatchingController {

    private MatchingService matchingService = null;

    public MatchingController(MatchingConfig matchingConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<MatchingConfigurable> subConfigsList, List<BaseController> eventsHandlerList) {
        super(new LogHandler(matchingConfig.getLoggerPathList(), systemLogger), new QueueHandler(matchingConfig.getQueuesList(), rabbitTemplatesMap), matchingConfig, subConfigsList, eventsHandlerList);

        matchingService = new MatchingService(queueHandler, logHandler, eventsHandlerList);
    }

    @Override
    synchronized boolean doCollect() {
        if (getObj() != null) {
            JsonMessageConverter jsonConverter = new JsonMessageConverter();
            Message msg = (Message) getObj();
            Map messageMap = (Map) jsonConverter.fromMessage(msg);

            MatchingConfigurable matchingConfig = null;

            //find the matching mapper
            for (MatchingConfigurable configurable : subConfigList) {
                if (((MatchingSettingsGroup) configurable).getGroupType().equals(msg.getMessageProperties().getHeaders().get(MatchingDefines.FILE_TYPE_HEADER))) {
                    matchingConfig = (MatchingConfigurable) configurable;
                    break;
                }
            }

            if (matchingConfig != null) {
                return matchingService.match(messageMap, msg.getMessageProperties(), subConfigList);
            }
        }
        return false;
    }

    @Override
    public void stop() {
        //do nothing
    }
}
