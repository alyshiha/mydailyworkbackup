/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.validation.model;

import com.evision.bingo.core.model.BaseValidationSet;
import java.util.List;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public class ValidationSet extends BaseValidationSet {

    private String state;
    private List<ValidationRule> validationRulesList;
    private String fileType;
    private String fileTemplate;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<ValidationRule> getValidationRulesList() {
        return validationRulesList;
    }

    public void setValidationRulesList(List<ValidationRule> validationRulesList) {
        this.validationRulesList = validationRulesList;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileTemplate() {
        return fileTemplate;
    }

    public void setFileTemplate(String fileTemplate) {
        this.fileTemplate = fileTemplate;
    }

  
}
