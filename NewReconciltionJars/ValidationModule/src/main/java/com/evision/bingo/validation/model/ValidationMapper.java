/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.validation.model;

import com.evision.bingo.core.model.BaseValidationSet;
import com.evision.bingo.core.model.ValidationConfigurable;
import java.util.List;

/**
 *
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 * Validation Mapping Config
 */
public class ValidationMapper implements ValidationConfigurable {

    private String fileType;
    private String fileTemplate;
    private List<BaseValidationSet> validationSetList;

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileTemplate() {
        return fileTemplate;
    }

    public void setFileTemplate(String fileTemplate) {
        this.fileTemplate = fileTemplate;
    }

    @Override
    public void setValidationSetsList(List<BaseValidationSet> validationSetList) {
        this.validationSetList = validationSetList;
    }

    @Override
    public List<BaseValidationSet> getValidationSetsList() {
        return validationSetList;
    }
}
