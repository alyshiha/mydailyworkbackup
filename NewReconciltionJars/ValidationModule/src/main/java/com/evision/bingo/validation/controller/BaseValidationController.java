/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.validation.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.ValidationConfig;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.validation.service.iValidationPostProcessor;
import com.evision.bingo.validation.service.iValidationPreProcessor;
import java.util.ArrayList;
import java.util.List;
import com.evision.bingo.core.model.ValidationConfigurable;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public abstract class BaseValidationController extends BaseController {

    //collection of registered pre-processors
    private List<iValidationPreProcessor> registeredPreProcessors;

    //collection of registered post-processors
    private List<iValidationPostProcessor> registeredPostProcessors;

    protected List<ValidationConfigurable> subConfigList = new ArrayList();

    BaseValidationController(LogHandler logHandler, QueueHandler queueHandler, ValidationConfig validationConfig, List<ValidationConfigurable> subConfigList, List<BaseController> eventsHandlerList) {
        super(logHandler, queueHandler, validationConfig, eventsHandlerList);

        this.subConfigList = subConfigList;

        init();
    }

    // init processors collections
    @Override
    public final void init() {
        registeredPreProcessors = new ArrayList<>();
        registeredPostProcessors = new ArrayList<>();
    }

    //force clear memory     
    @Override
    public final void destroy() {
        registeredPreProcessors.clear();
        registeredPostProcessors.clear();

        registeredPreProcessors = null;
        registeredPostProcessors = null;
    }

    //register a pre-processor
    final void registerPreProcessor(iValidationPreProcessor preProcessor) {
        registeredPreProcessors.add(preProcessor);
    }

    //unregister a pre-processor
    final void unregisterPreProcessor(iValidationPreProcessor preProcessor) {
        registeredPreProcessors.remove(preProcessor);
    }

    //register a post-processor
    final void registerPostProcessor(iValidationPostProcessor postProcessor) {
        registeredPostProcessors.add(postProcessor);
    }

    //unregister a post-processor
    final void unregisterPostProcessor(iValidationPostProcessor postProcessor) {
        registeredPostProcessors.remove(postProcessor);
    }

    //main load function that forces the work chain
    @Override
    public synchronized final boolean handleRequest(Object obj) {
        this.setObj(obj);
        if (preCollect()) {
            if (doCollect()) {
                return postCollect();
            }
        }
        return false;
    }

    //call of all registered pre-processors
    synchronized final boolean preCollect() {
        if (!registeredPreProcessors.isEmpty()) {
            for (iValidationPreProcessor preProcessor : registeredPreProcessors) {
                if (!preProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //call of all registered post-processors
    synchronized final boolean postCollect() {
        if (!registeredPostProcessors.isEmpty()) {
            for (iValidationPostProcessor postProcessor : registeredPostProcessors) {
                if (!postProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //method of collect to be overidden
    abstract boolean doCollect();
}
