/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.validation.model;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public class ValidationRule {

    private String fieldName;
    private String operator;
    private String value;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
