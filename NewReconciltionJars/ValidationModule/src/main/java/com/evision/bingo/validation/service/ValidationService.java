/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.validation.service;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.BaseValidationSet;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.validation.model.ValidationRule;
import com.evision.bingo.validation.model.ValidationSet;
import com.evision.bingo.validation.utils.ValidationDefines;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.amqp.core.MessageProperties;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public class ValidationService extends BaseValidationService {

    private String reason = "";
    private ObjectMapper oMapper = new ObjectMapper();

    public ValidationService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, eventsHandlerList);
    }

    @Override
    public boolean validate(List<BaseValidationSet> validationSetsList, Map messageMap, MessageProperties messageProperties, String templateName) {
        for (BaseValidationSet validationSet : validationSetsList) {
            String reason = applyValidationSet((ValidationSet) validationSet, messageMap);
            messageProperties.setHeader(ValidationDefines.REASON_HEADER, reason);
            logHandler.logInfo(String.format(LogMessages.getMessage(LogMessages.VALIDATION_VALIDATE_TRANSACTION_AGAINST_VALIDATION_SET), ((ValidationSet) validationSet).getState(), templateName));
            if (reason.isEmpty()) {
                queueHandler.sendMessage(((ValidationSet) validationSet).getState(), messageMap, messageProperties);
            }
        }
        return true;
    }

    private String checkString(ValidationRule validationRule, String value) {
        switch (validationRule.getOperator().toUpperCase()) {
            //string not in a;b;c
            case ValidationDefines.OPERATOR_NOT_IN:
                if (Arrays.asList(validationRule.getValue().split(ValidationDefines.VALUE_SEPARATOR)).contains(value)) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_IN + Defines.SYSTEM_SPACE + validationRule.getValue() + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value;
                }
                break;
            //string in a;b;c
            case ValidationDefines.OPERATOR_IN:
                if (!Arrays.asList(validationRule.getValue().split(ValidationDefines.VALUE_SEPARATOR)).contains(value)) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_NOT_IN + Defines.SYSTEM_SPACE + validationRule.getValue() + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value;
                }
                break;
            //string equal aly
            case ValidationDefines.OPERATOR_EQUAL:
                if (!value.equals(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            //string not equal aly
            case ValidationDefines.OPERATOR_NOT_EQUAL:
                if (value.equals(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_LIKE:
                if (!value.contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_NOT_LIKE:
                if (value.contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_IS_NOT_NULL:
                if (value == null || value.isEmpty()) {
                    return validationRule.getFieldName() + ValidationDefines.OPERATOR_IS_NULL;
                }
                break;
            case ValidationDefines.OPERATOR_IS_NULL:
                if (value != null || !value.isEmpty()) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            default:
                return ValidationDefines.INVALID_OPERATOR;
        }
        return "";
    }

    private String checkDecimal(ValidationRule validationRule, BigDecimal value) {
        switch (validationRule.getOperator().toUpperCase()) {
            case ValidationDefines.OPERATOR_EQUAL:
                if (value.compareTo(new BigDecimal(validationRule.getValue())) != 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_NOT_EQUAL:
                if (value.compareTo(new BigDecimal(validationRule.getValue())) == 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_LIKE:
                if (!value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_NOT_LIKE:
                if (value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_IS_NULL:
                if (value != null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            case ValidationDefines.OPERATOR_IS_NOT_NULL:
                if (value == null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_NULL;
                }
                break;
            case ValidationDefines.OPERATOR_GREATER_THAN:
                if (value.compareTo(new BigDecimal(validationRule.getValue())) == -1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_SMALLER_THAN:
                if (value.compareTo(new BigDecimal(validationRule.getValue())) == 1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            default:
                return ValidationDefines.INVALID_OPERATOR;
        }

        return "";
    }

    private String checkInteger(ValidationRule validationRule, Integer value) {
        switch (validationRule.getOperator().toUpperCase()) {
            case ValidationDefines.OPERATOR_EQUAL:
                if (value.compareTo(new Integer(validationRule.getValue())) != 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_NOT_EQUAL:
                if (value.compareTo(new Integer(validationRule.getValue())) == 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_LIKE:
                if (!value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_NOT_LIKE:
                if (value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_IS_NULL:
                if (value != null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            case ValidationDefines.OPERATOR_IS_NOT_NULL:
                if (value == null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_NULL;
                }
                break;
            case ValidationDefines.OPERATOR_GREATER_THAN:
                if (value.compareTo(new Integer(validationRule.getValue())) == -1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_SMALLER_THAN:
                if (value.compareTo(new Integer(validationRule.getValue())) == 1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            default:
                return ValidationDefines.INVALID_OPERATOR;
        }

        return "";
    }

    private String checkFloat(ValidationRule validationRule, Float value) {
        switch (validationRule.getOperator().toUpperCase()) {
            case ValidationDefines.OPERATOR_EQUAL:
                if (value.compareTo(new Float(validationRule.getValue())) != 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_NOT_EQUAL:
                if (value.compareTo(new Float(validationRule.getValue())) == 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_LIKE:
                if (!value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_NOT_LIKE:
                if (value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_IS_NULL:
                if (value != null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            case ValidationDefines.OPERATOR_IS_NOT_NULL:
                if (value == null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_NULL;
                }
                break;
            case ValidationDefines.OPERATOR_GREATER_THAN:
                if (value.compareTo(new Float(validationRule.getValue())) == -1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_SMALLER_THAN:
                if (value.compareTo(new Float(validationRule.getValue())) == 1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            default:
                return ValidationDefines.INVALID_OPERATOR;
        }

        return "";
    }

    private String checkDouble(ValidationRule validationRule, Double value) {
        switch (validationRule.getOperator().toUpperCase()) {
            case ValidationDefines.OPERATOR_EQUAL:
                if (value.compareTo(new Double(validationRule.getValue())) != 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_NOT_EQUAL:
                if (value.compareTo(new Double(validationRule.getValue())) == 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_LIKE:
                if (!value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_NOT_LIKE:
                if (value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_IS_NULL:
                if (value != null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            case ValidationDefines.OPERATOR_IS_NOT_NULL:
                if (value == null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_NULL;
                }
                break;
            case ValidationDefines.OPERATOR_GREATER_THAN:
                if (value.compareTo(new Double(validationRule.getValue())) == -1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case ValidationDefines.OPERATOR_SMALLER_THAN:
                if (value.compareTo(new Double(validationRule.getValue())) == 1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            default:
                return ValidationDefines.INVALID_OPERATOR;
        }

        return "";
    }

    private String checkDate(ValidationRule validationRule, String strValue) {
        DateFormat dateFormat = new SimpleDateFormat(ValidationDefines.DATE_FORMAT);
        Date dateValue = null;
        try {
            dateValue = dateFormat.parse(strValue);
        } catch (ParseException ex) {
            return ValidationDefines.INVALID_FIELD_VALUE;
        }

        try {

            switch (validationRule.getOperator().toUpperCase()) {
                case ValidationDefines.OPERATOR_EQUAL:
                    if (dateValue.compareTo(dateFormat.parse(validationRule.getValue())) != 0) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case ValidationDefines.OPERATOR_NOT_EQUAL:
                    if (dateValue.compareTo(dateFormat.parse(validationRule.getValue())) == 0) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case ValidationDefines.OPERATOR_LIKE:
                    if (!dateValue.toString().contains(validationRule.getValue())) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case ValidationDefines.OPERATOR_NOT_LIKE:
                    if (dateValue.toString().contains(validationRule.getValue())) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case ValidationDefines.OPERATOR_IS_NULL:
                    if (dateValue != null) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_IS_NOT_NULL;
                    }
                    break;
                case ValidationDefines.OPERATOR_IS_NOT_NULL:
                    if (dateValue == null) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_NULL;
                    }
                    break;
                case ValidationDefines.OPERATOR_GREATER_THAN:
                    if (dateValue.compareTo(dateFormat.parse(validationRule.getValue())) == -1) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case ValidationDefines.OPERATOR_SMALLER_THAN:
                    if (dateValue.compareTo(dateFormat.parse(validationRule.getValue())) == 1) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + ValidationDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + ValidationDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                default:
                    return ValidationDefines.INVALID_OPERATOR;
            }
        } catch (ParseException ex) {
            return ValidationDefines.INVALID_RULE_VALUE;
        }

        return "";
    }

    private String applyValidationSet(ValidationSet validationSet, Map<String, Object> message) {
        String reason = "";

        for (ValidationRule validationRule : validationSet.getValidationRulesList()) {
            String fieldName = validationRule.getFieldName();
            Object fieldValue = message.get(fieldName);
            String fieldDataType = fieldValue.getClass().getTypeName();

            switch (fieldDataType.toLowerCase()) {
                case ValidationDefines.STRING_FIELD_TYPE_CLASS:
                    reason += checkString(validationRule, fieldValue.toString().trim()) + Defines.SYSTEM_SPACE + ValidationDefines.OPERATOR_OR + Defines.SYSTEM_SPACE;
                    break;
                case ValidationDefines.DECIMAL_FIELD_TYPE_CLASS:
                    reason += checkDecimal(validationRule, new BigDecimal(fieldValue.toString().trim()));
                    break;
                case ValidationDefines.INTEGER_FIELD_TYPE_CLASS:
                    reason += checkInteger(validationRule, new Integer(fieldValue.toString().trim()));
                    break;
                case ValidationDefines.FLOAT_FIELD_TYPE_CLASS:
                    reason += checkFloat(validationRule, new Float(fieldValue.toString().trim()));
                    break;
                case ValidationDefines.DOUBLE_FIELD_TYPE_CLASS:
                    reason += checkDouble(validationRule, new Double(fieldValue.toString().trim()));
                    break;
                case ValidationDefines.DATE_FIELD_TYPE_CLASS:
                    reason += checkDate(validationRule, fieldValue.toString().trim());
                    break;
                default:
                    break;
            }
        }

        return reason;
    }

    @Override
    public boolean connect() {
        return true;
    }

    @Override
    public void disconnect() {
    }
}
