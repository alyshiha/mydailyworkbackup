/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.validation.utils;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public final class ValidationDefines {

    public static final String REASON_HEADER = "reason";
    public static final String FILE_TYPE_HEADER = "fileType";
    public static final String VALUE_SEPARATOR = ";";
    public static final String STRING_FIELD_TYPE_CLASS = "java.lang.string";
    public static final String DECIMAL_FIELD_TYPE_CLASS = "java.math.bigdecimal";
    public static final String INTEGER_FIELD_TYPE_CLASS = "java.lang.integer";
    public static final String FLOAT_FIELD_TYPE_CLASS = "java.lang.float";
    public static final String DOUBLE_FIELD_TYPE_CLASS = "java.lang.double";
    public static final String DATE_FIELD_TYPE_CLASS = "java.util.Date";
    public static final String OPERATOR_OR = "||";
    public static final String OPERATOR_NOT_IN = "NOT IN";
    public static final String OPERATOR_IN = "IN";
    public static final String OPERATOR_EQUAL = "EQUAL";
    public static final String OPERATOR_NOT_EQUAL = "NOT EQUAL";
    public static final String OPERATOR_LIKE = "LIKE";
    public static final String OPERATOR_NOT_LIKE = "NOT LIKE";
    public static final String OPERATOR_IS_NOT_NULL = "IS NOT NULL";
    public static final String OPERATOR_IS_NULL = "IS NULL";
    public static final String OPERATOR_GREATER_THAN = "GREATER THAN";
    public static final String OPERATOR_SMALLER_THAN = "SMALLER THAN";
    public static final String OPERATOR_DATE_AFTER = "DATE AFTER";
    public static final String OPERATOR_DATE_BEFORE = "DATE BEFORE";
    public static final String VALUE_EQUALS = "Value Equals";
    public static final String WHICH_CONTAINS = "which Contains";
    public static final String WHICH_IS_NULL = "which is null";
    public static final String WHICH_NOT_CONTAINS = "which Not Contains";
    public static final String WHICH_IS_GREATER_THAN = "which is greater than";
    public static final String WHICH_IS_SMALLER_THAN = "which is smaller than";
    public static final String INVALID_OPERATOR = "Invalid Operator";
    public static final String INVALID_FIELD_VALUE = "Invalid Field Value";
    public static final String INVALID_RULE_VALUE = "Invalid Rule Value";
    public static final String DATE_FORMAT = "mm/dd/yyyy";
}