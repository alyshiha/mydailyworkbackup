/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.validation.service;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.BaseValidationSet;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.core.MessageProperties;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public abstract class BaseValidationService {

    protected LogHandler logHandler;
    protected QueueHandler queueHandler;
    protected List<BaseController> eventsHandlerList;

    public BaseValidationService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        this.queueHandler = queueHandler;
        this.logHandler = logHandler;
        this.eventsHandlerList = eventsHandlerList;
    }

    public abstract boolean validate(List<BaseValidationSet> validationSetList, Map messageMap, MessageProperties messageProperties, String templateName);

    public abstract boolean connect();

    public abstract void disconnect();

    public void submitEvent(EventModel event) {
        for (BaseController controller : eventsHandlerList) {
            controller.handleRequest(event);
        }
    }

    public LogHandler getLogHandler() {
        return logHandler;
    }

    public void setLogHandler(LogHandler logHandler) {
        this.logHandler = logHandler;
    }

    public QueueHandler getQueueHandler() {
        return queueHandler;
    }

    public void setQueueHandler(QueueHandler queueHandler) {
        this.queueHandler = queueHandler;
    }

    public List<BaseController> getEventsHandlerList() {
        return eventsHandlerList;
    }

    public void setEventsHandlerList(List<BaseController> eventsHandlerList) {
        this.eventsHandlerList = eventsHandlerList;
    }
}
