/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.validation.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.ValidationConfig;
import com.evision.bingo.core.model.ValidationConfigurable;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.validation.service.DateHandlerPreProcessor;
import com.evision.bingo.validation.service.ValidationService;
import com.evision.bingo.validation.utils.ValidationDefines;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public class ValidationController extends BaseValidationController {

    private ValidationService validationService = null;

    public ValidationController(ValidationConfig validationConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<ValidationConfigurable> subConfigsList, List<BaseController> eventsHandlerList) {
        super(new LogHandler(validationConfig.getLoggerPathList(), systemLogger), new QueueHandler(validationConfig.getQueuesList(), rabbitTemplatesMap), validationConfig, subConfigsList, eventsHandlerList);

        validationService = new ValidationService(queueHandler, logHandler, eventsHandlerList);
        registerPreProcessor(new DateHandlerPreProcessor());
    }

    @Override
    synchronized boolean doCollect() {
        if (getObj() != null && getExtraObj() != null) {
            Map msgMap = (Map) getObj();
            MessageProperties msgProperties = (MessageProperties) getExtraObj();

            //find the validation config
            for (ValidationConfigurable configurable : subConfigList) {
                if (configurable.getFileType().equals(msgProperties.getHeaders().get(ValidationDefines.FILE_TYPE_HEADER))) {
                    return validationService.validate(configurable.getValidationSetsList(), msgMap, msgProperties, configurable.getFileType());
                }
            }
        }
        return false;
    }

    @Override
    public void stop() {
    }
}
