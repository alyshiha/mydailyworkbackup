/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dispute.service;

import com.evision.bingo.core.controller.BaseController;
import java.util.Date;
import java.util.Map;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.support.converter.JsonMessageConverter;

/**
 * @since 9/8/2017
 * @author Khaled Khalil
 *
 */
public class DateHandlerPreProcessor implements iDisputePreProcessor {

    @Override
    public boolean process(BaseController baseController) {
        if (baseController.getObj() != null) {
            Message msg = (Message) baseController.getObj();
            JsonMessageConverter jsonConverter = new JsonMessageConverter();
            Map msgMap = (Map) jsonConverter.fromMessage(msg);

            String dateFieldsStr = (String) msg.getMessageProperties().getHeaders().get("dateFields");
            String[] dateFields = dateFieldsStr.split(",");

            if (dateFields == null || dateFields.length == 0) {
                return true;
            }

            for (Object fieldName : msgMap.keySet()) {
                for (String dateField : dateFields) {
                    if (fieldName.toString().equals(dateField)) {
                        msgMap.put(fieldName, new Date((Long) msgMap.get(fieldName)));
                    }
                }
            }
            baseController.setObj(msgMap);
            baseController.setExtraObj(msg.getMessageProperties());
        }
        return true;
    }
}
