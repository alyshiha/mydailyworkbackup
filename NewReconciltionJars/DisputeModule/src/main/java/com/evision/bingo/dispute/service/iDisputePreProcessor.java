/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dispute.service;

import com.evision.bingo.core.controller.BaseController;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 * base interface for pre-processors
 */
public interface iDisputePreProcessor {

    public boolean process(BaseController baseController);
}
