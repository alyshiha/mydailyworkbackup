/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dispute.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.BaseDisputeSet;
import com.evision.bingo.core.model.DisputeConfig;
import com.evision.bingo.core.model.DisputeConfigurable;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.dispute.service.DisputeService;
import com.evision.bingo.dispute.utils.DisputeDefines;
import com.evision.bingo.dispute.service.DateHandlerPreProcessor;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public class DisputeController extends BaseDisputeController {

    private DisputeService disputeService = null;

    public DisputeController(DisputeConfig disputeConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<DisputeConfigurable> subConfigsList, List<BaseController> eventsHandlerList) {
        super(new LogHandler(disputeConfig.getLoggerPathList(), systemLogger), new QueueHandler(disputeConfig.getQueuesList(), rabbitTemplatesMap), disputeConfig, subConfigsList, eventsHandlerList);

        disputeService = new DisputeService(queueHandler, logHandler, eventsHandlerList);
        registerPreProcessor(new DateHandlerPreProcessor());
    }

    @Override
    synchronized boolean doCollect() {
      if (getObj() != null && getExtraObj() != null) {
            Map msgMap = (Map) getObj();
            MessageProperties msgProperties = (MessageProperties) getExtraObj();

            for (DisputeConfigurable configurable : subConfigList) {
                if (configurable.getFileType().equals(msgProperties.getHeaders().get(DisputeDefines.FILE_TYPE_HEADER))) {
                    return disputeService.validate((List<BaseDisputeSet>) configurable.getDisputeSetsList(), msgMap, msgProperties, configurable.getFileType());
                }
            }
        }
        return false;
    }

    @Override
    public void stop() {
    }
}
