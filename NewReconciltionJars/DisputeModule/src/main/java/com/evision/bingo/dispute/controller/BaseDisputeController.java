/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dispute.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.DisputeConfig;
import com.evision.bingo.core.model.DisputeConfigurable;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.dispute.service.iDisputePostProcessor;
import com.evision.bingo.dispute.service.iDisputePreProcessor;
import java.util.ArrayList;
import java.util.List;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public abstract class BaseDisputeController extends BaseController {

    //collection of registered pre-processors
    private List<iDisputePreProcessor> registeredPreProcessors;

    //collection of registered post-processors
    private List<iDisputePostProcessor> registeredPostProcessors;

    protected List<DisputeConfigurable> subConfigList = new ArrayList();

    BaseDisputeController(LogHandler logHandler, QueueHandler queueHandler, DisputeConfig disputeConfig, List<DisputeConfigurable> subConfigList, List<BaseController> eventsHandlerList) {
        super(logHandler, queueHandler, disputeConfig, eventsHandlerList);

        this.subConfigList = subConfigList;

        init();
    }

    // init processors collections
    @Override
    public final void init() {
        registeredPreProcessors = new ArrayList<>();
        registeredPostProcessors = new ArrayList<>();
    }

    //force clear memory     
    @Override
    public final void destroy() {
        registeredPreProcessors.clear();
        registeredPostProcessors.clear();

        registeredPreProcessors = null;
        registeredPostProcessors = null;
    }

    //register a pre-processor
    final void registerPreProcessor(iDisputePreProcessor preProcessor) {
        registeredPreProcessors.add(preProcessor);
    }

    //unregister a pre-processor
    final void unregisterPreProcessor(iDisputePreProcessor preProcessor) {
        registeredPreProcessors.remove(preProcessor);
    }

    //register a post-processor
    final void registerPostProcessor(iDisputePostProcessor postProcessor) {
        registeredPostProcessors.add(postProcessor);
    }

    //unregister a post-processor
    final void unregisterPostProcessor(iDisputePostProcessor postProcessor) {
        registeredPostProcessors.remove(postProcessor);
    }

    //main load function that forces the work chain
    @Override
    public synchronized final boolean handleRequest(Object obj) {
        this.setObj(obj);
        if (preCollect()) {
            if (doCollect()) {
                return postCollect();
            }
        }
        return false;
    }

    //call of all registered pre-processors
    synchronized final boolean preCollect() {
        if (!registeredPreProcessors.isEmpty()) {
            for (iDisputePreProcessor preProcessor : registeredPreProcessors) {
                if (!preProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //call of all registered post-processors
    synchronized final boolean postCollect() {
        if (!registeredPostProcessors.isEmpty()) {
            for (iDisputePostProcessor postProcessor : registeredPostProcessors) {
                if (!postProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //method of collect to be overidden
    abstract boolean doCollect();
}
