/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dispute.model;

import com.evision.bingo.core.model.BaseDisputeSet;
import com.evision.bingo.core.model.DisputeConfigurable;
import java.util.List;

/**
 *
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 * Validation Mapping Config
 */
public class DisputeMapper implements DisputeConfigurable {

    private String fileType;
    private String fileTemplate;
    private List<BaseDisputeSet> disputeSetList;

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileTemplate() {
        return fileTemplate;
    }

    public void setFileTemplate(String fileTemplate) {
        this.fileTemplate = fileTemplate;
    }

    @Override
    public void setDisputeSetsList(List<BaseDisputeSet> disputeSetList) {
        this.disputeSetList = disputeSetList;
    }

    @Override
    public List<BaseDisputeSet> getDisputeSetsList() {
        return disputeSetList;
    }
}
