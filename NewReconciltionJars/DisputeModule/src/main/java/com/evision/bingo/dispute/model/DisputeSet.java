/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dispute.model;

import com.evision.bingo.core.model.BaseDisputeSet;
import com.evision.bingo.core.model.BaseValidationSet;
import java.util.List;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public class DisputeSet extends BaseDisputeSet {

    private String state;
    private List<DisputeRule> disputeRulesList;
    private String fileType;
    private String fileTemplate;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<DisputeRule> getDisputeRulesList() {
        return disputeRulesList;
    }

    public void setDisputeRulesList(List<DisputeRule> disputeRulesList) {
        this.disputeRulesList = disputeRulesList;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileTemplate() {
        return fileTemplate;
    }

    public void setFileTemplate(String fileTemplate) {
        this.fileTemplate = fileTemplate;
    }

}
