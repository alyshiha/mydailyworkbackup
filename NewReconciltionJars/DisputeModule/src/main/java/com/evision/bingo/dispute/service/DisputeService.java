/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dispute.service;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.BaseDisputeSet;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.dispute.model.DisputeRule;
import com.evision.bingo.dispute.model.DisputeSet;
import com.evision.bingo.dispute.utils.DisputeDefines;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.amqp.core.MessageProperties;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public class DisputeService extends BaseDisputeService {

    private String reason = "";
    private ObjectMapper oMapper = new ObjectMapper();

    public DisputeService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, eventsHandlerList);
    }

    @Override
    public boolean validate(List<BaseDisputeSet> disputeSetsList, Map messageMap, MessageProperties messageProperties, String templateName) {
        for (BaseDisputeSet validationSet : disputeSetsList) {
            String reason = applyValidationSet((DisputeSet) validationSet, messageMap);
            messageProperties.setHeader(DisputeDefines.REASON_HEADER, reason);
            logHandler.logInfo(String.format(LogMessages.getMessage(LogMessages.DISPUTE_VALIDATE_TRANSACTION_AGAINST_DISPUTE_SET), ((DisputeSet) validationSet).getState(), templateName));
            if (reason.isEmpty()) {
                queueHandler.sendMessage(((DisputeSet) validationSet).getState(), messageMap, messageProperties);
            }
        }
        return true;
    }

    private String checkString(DisputeRule validationRule, String value) {
        switch (validationRule.getOperator()) {
            //string not in a;b;c
            case DisputeDefines.OPERATOR_NOT_IN:
                if (Arrays.asList(validationRule.getValue().split(DisputeDefines.VALUE_SEPARATOR)).contains(value)) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_IN + Defines.SYSTEM_SPACE + validationRule.getValue() + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value;
                }
                break;
            //string in a;b;c
            case DisputeDefines.OPERATOR_IN:
                if (!Arrays.asList(validationRule.getValue().split(DisputeDefines.VALUE_SEPARATOR)).contains(value)) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_NOT_IN + Defines.SYSTEM_SPACE + validationRule.getValue() + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value;
                }
                break;
            //string equal aly
            case DisputeDefines.OPERATOR_EQUAL:
                if (!value.equals(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            //string not equal aly
            case DisputeDefines.OPERATOR_NOT_EQUAL:
                if (value.equals(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_LIKE:
                if (!value.contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_NOT_LIKE:
                if (value.contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_IS_NOT_NULL:
                if (value == null || value.isEmpty()) {
                    return validationRule.getFieldName() + DisputeDefines.OPERATOR_IS_NULL;
                }
                break;
            case DisputeDefines.OPERATOR_IS_NULL:
                if (value != null || !value.isEmpty()) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            default:
                return DisputeDefines.INVALID_OPERATOR;
        }
        return "";
    }

    private String checkDecimal(DisputeRule validationRule, BigDecimal value) {
        switch (validationRule.getOperator()) {
            case DisputeDefines.OPERATOR_EQUAL:
                if (value.compareTo(new BigDecimal(validationRule.getValue())) != 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_NOT_EQUAL:
                if (value.compareTo(new BigDecimal(validationRule.getValue())) == 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_LIKE:
                if (!value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_NOT_LIKE:
                if (value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_IS_NULL:
                if (value != null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            case DisputeDefines.OPERATOR_IS_NOT_NULL:
                if (value == null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_NULL;
                }
                break;
            case DisputeDefines.OPERATOR_GREATER_THAN:
                if (value.compareTo(new BigDecimal(validationRule.getValue())) == -1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_SMALLER_THAN:
                if (value.compareTo(new BigDecimal(validationRule.getValue())) == 1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            default:
                return DisputeDefines.INVALID_OPERATOR;
        }

        return "";
    }

    private String checkInteger(DisputeRule validationRule, Integer value) {
        switch (validationRule.getOperator()) {
            case DisputeDefines.OPERATOR_EQUAL:
                if (value.compareTo(new Integer(validationRule.getValue())) != 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_NOT_EQUAL:
                if (value.compareTo(new Integer(validationRule.getValue())) == 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_LIKE:
                if (!value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_NOT_LIKE:
                if (value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_IS_NULL:
                if (value != null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            case DisputeDefines.OPERATOR_IS_NOT_NULL:
                if (value == null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_NULL;
                }
                break;
            case DisputeDefines.OPERATOR_GREATER_THAN:
                if (value.compareTo(new Integer(validationRule.getValue())) == -1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_SMALLER_THAN:
                if (value.compareTo(new Integer(validationRule.getValue())) == 1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            default:
                return DisputeDefines.INVALID_OPERATOR;
        }

        return "";
    }

    private String checkFloat(DisputeRule validationRule, Float value) {
        switch (validationRule.getOperator()) {
            case DisputeDefines.OPERATOR_EQUAL:
                if (value.compareTo(new Float(validationRule.getValue())) != 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_NOT_EQUAL:
                if (value.compareTo(new Float(validationRule.getValue())) == 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_LIKE:
                if (!value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_NOT_LIKE:
                if (value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_IS_NULL:
                if (value != null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            case DisputeDefines.OPERATOR_IS_NOT_NULL:
                if (value == null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_NULL;
                }
                break;
            case DisputeDefines.OPERATOR_GREATER_THAN:
                if (value.compareTo(new Float(validationRule.getValue())) == -1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_SMALLER_THAN:
                if (value.compareTo(new Float(validationRule.getValue())) == 1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            default:
                return DisputeDefines.INVALID_OPERATOR;
        }

        return "";
    }

    private String checkDouble(DisputeRule validationRule, Double value) {
        switch (validationRule.getOperator().toUpperCase()) {
            case DisputeDefines.OPERATOR_EQUAL:
                if (value.compareTo(new Double(validationRule.getValue())) != 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_NOT_EQUAL:
                if (value.compareTo(new Double(validationRule.getValue())) == 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_LIKE:
                if (!value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_NOT_LIKE:
                if (value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_IS_NULL:
                if (value != null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            case DisputeDefines.OPERATOR_IS_NOT_NULL:
                if (value == null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_NULL;
                }
                break;
            case DisputeDefines.OPERATOR_GREATER_THAN:
                if (value.compareTo(new Double(validationRule.getValue())) == -1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case DisputeDefines.OPERATOR_SMALLER_THAN:
                if (value.compareTo(new Double(validationRule.getValue())) == 1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            default:
                return DisputeDefines.INVALID_OPERATOR;
        }

        return "";
    }

    private String checkDate(DisputeRule validationRule, String strValue) {
        DateFormat dateFormat = new SimpleDateFormat(DisputeDefines.DATE_FORMAT);
        Date dateValue = null;
        try {
            dateValue = dateFormat.parse(strValue);
        } catch (ParseException ex) {
            return DisputeDefines.INVALID_FIELD_VALUE;
        }

        try {

            switch (validationRule.getOperator()) {
                case DisputeDefines.OPERATOR_EQUAL:
                    if (dateValue.compareTo(dateFormat.parse(validationRule.getValue())) != 0) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case DisputeDefines.OPERATOR_NOT_EQUAL:
                    if (dateValue.compareTo(dateFormat.parse(validationRule.getValue())) == 0) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case DisputeDefines.OPERATOR_LIKE:
                    if (!dateValue.toString().contains(validationRule.getValue())) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case DisputeDefines.OPERATOR_NOT_LIKE:
                    if (dateValue.toString().contains(validationRule.getValue())) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case DisputeDefines.OPERATOR_IS_NULL:
                    if (dateValue != null) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_IS_NOT_NULL;
                    }
                    break;
                case DisputeDefines.OPERATOR_IS_NOT_NULL:
                    if (dateValue == null) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_NULL;
                    }
                    break;
                case DisputeDefines.OPERATOR_GREATER_THAN:
                    if (dateValue.compareTo(dateFormat.parse(validationRule.getValue())) == -1) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case DisputeDefines.OPERATOR_SMALLER_THAN:
                    if (dateValue.compareTo(dateFormat.parse(validationRule.getValue())) == 1) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + DisputeDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + DisputeDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                default:
                    return DisputeDefines.INVALID_OPERATOR;
            }
        } catch (ParseException ex) {
            return DisputeDefines.INVALID_RULE_VALUE;
        }

        return "";
    }

    private String applyValidationSet(DisputeSet validationSet, Map<String, Object> message) {
        String reason = "";

        for (DisputeRule validationRule : validationSet.getDisputeRulesList()) {
            String fieldName = validationRule.getFieldName();
            Object fieldValue = message.get(fieldName);
            String fieldDataType = fieldValue.getClass().getTypeName();

            switch (fieldDataType.toLowerCase()) {
                case DisputeDefines.STRING_FIELD_TYPE_CLASS:
                    reason += checkString(validationRule, fieldValue.toString().trim()) + Defines.SYSTEM_SPACE + DisputeDefines.OPERATOR_OR + Defines.SYSTEM_SPACE;
                    break;
                case DisputeDefines.DECIMAL_FIELD_TYPE_CLASS:
                    reason += checkDecimal(validationRule, new BigDecimal(fieldValue.toString().trim()));
                    break;
                case DisputeDefines.INTEGER_FIELD_TYPE_CLASS:
                    reason += checkInteger(validationRule, new Integer(fieldValue.toString().trim()));
                    break;
                case DisputeDefines.FLOAT_FIELD_TYPE_CLASS:
                    reason += checkFloat(validationRule, new Float(fieldValue.toString().trim()));
                    break;
                case DisputeDefines.DOUBLE_FIELD_TYPE_CLASS:
                    reason += checkDouble(validationRule, new Double(fieldValue.toString().trim()));
                    break;
                case DisputeDefines.DATE_FIELD_TYPE_CLASS:
                    reason += checkDate(validationRule, fieldValue.toString().trim());
                    break;
                default:
                    break;
            }
        }

        return reason;
    }

    @Override
    public boolean connect() {
        return true;
    }

    @Override
    public void disconnect() {
    }
}
