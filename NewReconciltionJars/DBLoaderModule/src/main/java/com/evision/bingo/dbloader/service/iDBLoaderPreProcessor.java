/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dbloader.service;

import com.evision.bingo.core.controller.BaseController;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 * base interface for pre-processors
 */
public interface iDBLoaderPreProcessor {
     public boolean process(BaseController baseController);
}
