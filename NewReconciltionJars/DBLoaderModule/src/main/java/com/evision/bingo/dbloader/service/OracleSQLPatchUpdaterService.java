/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dbloader.service;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.DBConnectionConfig;
import com.evision.bingo.core.model.DBLoaderConfig;
import com.evision.bingo.core.model.DBMapperDetail;
import com.evision.bingo.core.model.DBMapperMaster;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.core.utils.Utils;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;
import oracle.ucp.jdbc.ValidConnection;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 */
public class OracleSQLPatchUpdaterService extends BaseDBLoaderService {

    private PoolDataSource pds;
    private int statementsCounter = 0;
    private Connection conn = null;
    private boolean validDBData = true;
    private Statement stmt = null;
    private String stmtHeader = "", stmtBody = "";
    private String javaDateFormat = "yyyy-MM-dd HH:mm:ss";
    private String oracleDateFormat = "YYYY-MM-DD HH24:MI:SS";
    private SimpleDateFormat dateFormatter = new SimpleDateFormat(javaDateFormat);

    public OracleSQLPatchUpdaterService(QueueHandler queueHandler, LogHandler logHandler, DBLoaderConfig dbLoaderConfig, String dbConnectionName, String dbMapperName, int commitThreshold, List<DBConnectionConfig> dbConnectionsList, List<DBMapperMaster> dbMapperMastersList, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, dbLoaderConfig, dbConnectionName, dbMapperName, commitThreshold, dbConnectionsList, dbMapperMastersList, eventsHandlerList);

        if (!setDBConnection()) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(LogMessages.getMessage(LogMessages.DBLOADER_INVALID_DB_CONNECTION)));
            validDBData = false;
        }

        if (!setDBMapperMaster()) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(LogMessages.getMessage(LogMessages.DBLOADER_INVALID_DB_MAPPER)));
            validDBData = false;
        }
    }

    @Override
    public boolean loadMessage(Map objectMap) {
        if (!validDBData) {
            return false;
        }
        try {
            if (conn == null || conn.isClosed()) {
                conn = pds.getConnection();
                conn.setAutoCommit(true);
            }

            String objStr = "";
            for (String key : (Set<String>) objectMap.keySet()) {
                objStr += key + ":" + objectMap.get(key) + Defines.SYSTEM_SPACE + ",";
            }
            logHandler.logInfo(LogMessages.getMessage(LogMessages.DBLOADER_LOAD_RECORD_IN_MEMORY) + Defines.SYSTEM_SPACE + objStr);

            if (stmt == null) {
                stmtHeader = "insert into " + dbMapperMaster.getTableName() + " (";
                for (DBMapperDetail detail : dbMapperMaster.getDetailsList()) {
                    stmtHeader += detail.getDatabase() + ",";
                }
                stmtHeader = stmtHeader.substring(0, stmtHeader.length() - 1);
                stmtHeader += ") values (";

                stmt = conn.createStatement();
            }

            stmtBody = "";

            for (DBMapperDetail detail : dbMapperMaster.getDetailsList()) {
                Object val = objectMap.get(detail.getField());
                if (val != null) {
                    if (val.getClass().toString().toLowerCase().indexOf("string") != -1) {
                        stmtBody += "'" + (String) val + "',";
                    } else if (val.getClass().toString().toLowerCase().indexOf("date") != -1) {
                        stmtBody += "to_date('" + dateFormatter.format((Date) val) + "','" + oracleDateFormat + "'),";
                    } else if (val.getClass().toString().toLowerCase().indexOf("int") != -1) {
                        stmtBody += val + ",";
                    } else if (val.getClass().toString().toLowerCase().indexOf("float") != -1) {
                        stmtBody += val + ",";
                    } else if (val.getClass().toString().toLowerCase().indexOf("double") != -1) {
                        stmtBody += val + ",";
                    } else if (val.getClass().toString().toLowerCase().indexOf("long") != -1) {
                        stmtBody += val + ",";
                    } else {
                        stmtBody += val + ",";
                    }
                } else {
                    stmtBody += "Null,";
                }
            }
            stmtBody = stmtBody.substring(0, stmtBody.length() - 1) + ")";
            stmt.addBatch(stmtHeader + stmtBody);
            statementsCounter++;
            if (statementsCounter >= commitThreshold) {
                commit();
            }

            return true;
        } catch (SQLException ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);

            try {
                if (conn != null || !((ValidConnection) conn).isValid()) {// take the appropriate action            {
                    ((ValidConnection) conn).setInvalid();
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ex1) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
            }
        }
        return false;
    }

    @Override
    public boolean connect(DBLoaderConfig config) {

        if (pds == null) {
            try {
                pds = PoolDataSourceFactory.getPoolDataSource();
                pds.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
                pds.setURL("jdbc:oracle:thin:@//" + dbConnectionConfig.getIp() + ":" + dbConnectionConfig.getPort() + "/" + dbConnectionConfig.getInstance());
                pds.setUser(dbConnectionConfig.getUsername());
                pds.setPassword(dbConnectionConfig.getPassword());
                pds.setMinPoolSize(1);
                pds.setMaxPoolSize(25);
                pds.setConnectionPoolName(Utils.randomAlphanumeric(8));
                pds.setFastConnectionFailoverEnabled(true);

                conn = pds.getConnection();
                return true;
            } catch (SQLException ex) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);

                try {
                    if (conn != null || !((ValidConnection) conn).isValid()) {// take the appropriate action            {
                        ((ValidConnection) conn).setInvalid();
                        conn.close();
                        conn = null;
                    }
                } catch (SQLException ex1) {
                    logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                }
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void disconnect() {
        try {
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            if (conn != null) {
                conn.close();
                conn = null;
            }
            if (pds != null) {
                pds = null;
            }

        } catch (SQLException ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
        }
    }

    public void commit() {
        if (conn != null && stmt != null) {
            try {
                logHandler.logInfo(LogMessages.getMessage(LogMessages.DBLOADER_SUBMIT_RECORDS));
                int[] res = stmt.executeBatch();
                stmt.close();
                stmt = null;
                conn.close();
                conn = null;
                statementsCounter = 0;
            } catch (SQLException ex) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
            }
        }
    }
}
