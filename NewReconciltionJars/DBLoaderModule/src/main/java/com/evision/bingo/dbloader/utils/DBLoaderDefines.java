/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dbloader.utils;

/**
 * @since 17/7/2017
 * @author Khaled Khalil
 */
public class DBLoaderDefines {

    public static final int AUTO_COMMIT_DELAY = 60;
}
