/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dbloader.model;

/**
 * @since 1/6/2017
 * @author Aly.Shiha
 *
 */
public class RunningMap {

    private String runningFile = "";
    private String[] header;
    private String control = "";
    private String tableName = "";

    public String getRunningFile() {
        return runningFile;
    }

    public void setRunningFile(String runningFile) {
        this.runningFile = runningFile;
    }

    public String[] getHeader() {
        return header;
    }

    public void setHeader(String[] header) {
        this.header = header;
    }

    public String getControl() {
        return control;
    }

    public void setControl(String control) {
        this.control = control;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}