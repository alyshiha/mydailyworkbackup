/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dbloader.service;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.DBConnectionConfig;
import com.evision.bingo.core.model.DBLoaderConfig;
import com.evision.bingo.core.model.DBMapperMaster;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;
import java.util.Map;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 */
public abstract class BaseDBLoaderService {

    protected LogHandler logHandler;
    protected QueueHandler queueHandler;
    protected List<BaseController> eventsHandlerList;
    protected List<DBConnectionConfig> dbConnectionsList;
    protected List<DBMapperMaster> dbMapperMastersList;
    protected String dbConnectionName;
    protected String dbMapperName;
    protected DBConnectionConfig dbConnectionConfig;
    protected DBMapperMaster dbMapperMaster;
    protected int commitThreshold;
    protected DBLoaderConfig dbLoaderConfig;

    public BaseDBLoaderService(QueueHandler queueHandler, LogHandler logHandler, DBLoaderConfig dbLoaderConfig, String dbConnectionName, String dbMapperName, int commitThreshold, List<DBConnectionConfig> dbConnectionsList, List<DBMapperMaster> dbMapperMastersList, List<BaseController> eventsHandlerList) {
        this.queueHandler = queueHandler;
        this.logHandler = logHandler;
        this.eventsHandlerList = eventsHandlerList;
        this.dbConnectionsList = dbConnectionsList;
        this.dbMapperMastersList = dbMapperMastersList;
        this.dbConnectionName = dbConnectionName;
        this.dbMapperName = dbMapperName;
        this.commitThreshold = commitThreshold;
        this.dbLoaderConfig = dbLoaderConfig;
    }

    protected boolean setDBConnection() {
        for (DBConnectionConfig dbConn : dbConnectionsList) {
            if (dbConn.getName().toLowerCase().equals(dbConnectionName.toLowerCase())) {
                dbConnectionConfig = dbConn;
                return true;
            }
        }
        return false;
    }

    protected boolean setDBMapperMaster() {
        for (DBMapperMaster dbMapperMaster : dbMapperMastersList) {
            if (dbMapperMaster.getName().toLowerCase().equals(dbMapperName.toLowerCase())) {
                this.dbMapperMaster = dbMapperMaster;
                return true;
            }
        }
        return false;
    }

    public abstract boolean loadMessage(Map messageMap);

    public abstract boolean connect(DBLoaderConfig config);

    public abstract void disconnect();

    public void submitEvent(EventModel event) {
        for (BaseController controller : eventsHandlerList) {
            controller.handleRequest(event);
        }
    }
}
