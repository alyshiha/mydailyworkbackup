/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dbloader.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.DBConnectionConfig;
import com.evision.bingo.core.model.DBLoaderConfig;
import com.evision.bingo.core.model.DBMapperMaster;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.dbloader.service.iDBLoaderPostProcessor;
import com.evision.bingo.dbloader.service.iDBLoaderPreProcessor;
import java.util.ArrayList;
import java.util.List;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 * base db loader controller
 */
public abstract class BaseDBLoaderController extends BaseController {

    //collection of registered pre-processors
    private List<iDBLoaderPreProcessor> registeredPreProcessors;

    //collection of registered post-processors
    private List<iDBLoaderPostProcessor> registeredPostProcessors;

    private List<DBConnectionConfig> dbConnectionsList;
    private List<DBMapperMaster> dbMapperMastersList;

    public BaseDBLoaderController(LogHandler logHandler, QueueHandler queueHandler, DBLoaderConfig dbLoaderConfig, List<DBConnectionConfig> dbConnectionsList, List<DBMapperMaster> dbMapperMastersList, List<BaseController> eventsHandlerList) {
        super(logHandler, queueHandler, dbLoaderConfig, eventsHandlerList);

        this.dbConnectionsList = dbConnectionsList;
        this.dbMapperMastersList = dbMapperMastersList;

        init();
    }

    @Override
    public final void init() {
        registeredPreProcessors = new ArrayList<>();
        registeredPostProcessors = new ArrayList<>();
    }

    @Override
    public final synchronized boolean handleRequest(Object obj) {
        this.setObj(obj);
        if (preCollect()) {
            if (doCollect()) {
                return postCollect();
            }
        }
        return false;
    }

    @Override
    public final void destroy() {
        registeredPreProcessors.clear();
        registeredPostProcessors.clear();

        registeredPreProcessors = null;
        registeredPostProcessors = null;
    }

    //register a pre-processor
    final void registerPreProcessor(iDBLoaderPreProcessor preProcessor) {
        registeredPreProcessors.add(preProcessor);
    }

    //unregister a pre-processor
    final void unregisterPreProcessor(iDBLoaderPreProcessor preProcessor) {
        registeredPreProcessors.remove(preProcessor);
    }

    //register a post-processor
    final void registerPostProcessor(iDBLoaderPostProcessor postProcessor) {
        registeredPostProcessors.add(postProcessor);
    }

    //unregister a post-processor
    final void unregisterPostProcessor(iDBLoaderPostProcessor postProcessor) {
        registeredPostProcessors.remove(postProcessor);
    }

    //call of all registered pre-processors
    final synchronized boolean preCollect() {
        if (!registeredPreProcessors.isEmpty()) {
            for (iDBLoaderPreProcessor preProcessor : registeredPreProcessors) {
                if (!preProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //call of all registered post-processors
    final synchronized boolean postCollect() {
        if (!registeredPostProcessors.isEmpty()) {
            for (iDBLoaderPostProcessor postProcessor : registeredPostProcessors) {
                if (!postProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //method of collect to be overidden
    abstract boolean doCollect();
}
