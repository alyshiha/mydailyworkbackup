/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dbloader.service;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.DBConnectionConfig;
import com.evision.bingo.core.model.DBLoaderConfig;
import com.evision.bingo.core.model.DBMapperDetail;
import com.evision.bingo.core.model.DBMapperMaster;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.core.utils.Utils;
import com.evision.bingo.dbloader.model.RunningMap;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.support.converter.JsonMessageConverter;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.prefs.CsvPreference;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 * oracle sql loader service
 */
public class OracleSQLLoaderService extends BaseDBLoaderService {

    private enum ExitCode {
        SUCCESS, FAIL, WARN, FATAL, UNKNOWN
    }

    private boolean IS_WINDOWS = true;
    private RunningMap runningMapData;
    private boolean validDBData = true;
    private static List<Map<String, Object>> listOfMaps;
    private int mapsCounter = 0;

    public OracleSQLLoaderService(QueueHandler queueHandler, LogHandler logHandler, DBLoaderConfig dbLoaderConfig, String dbConnectionName, String dbMapperName, int commitThreshold, List<DBConnectionConfig> dbConnectionsList, List<DBMapperMaster> dbMapperMastersList, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, dbLoaderConfig, dbConnectionName, dbMapperName, commitThreshold, dbConnectionsList, dbMapperMastersList, eventsHandlerList);

        runningMapData = new RunningMap();
        listOfMaps = new ArrayList<Map<String, Object>>();
        IS_WINDOWS = System.getProperty("os.name").toLowerCase().contains("win");

        if (!setDBConnection()) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(LogMessages.getMessage(LogMessages.DBLOADER_INVALID_DB_CONNECTION)));
            validDBData = false;
        }

        if (!setDBMapperMaster()) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(LogMessages.getMessage(LogMessages.DBLOADER_INVALID_DB_MAPPER)));
            validDBData = false;
        }

        runningMapData.setTableName(dbMapperMaster.getTableName());
        runningMapData.setRunningFile(dbMapperName);
    }

    @Override
    public boolean loadMessage(Map data) {
        if (!validDBData) {
            return false;
        }
        if (data == null) { // failed to parse the message body
            return false;
        }

        String objStr = "";
        for (String key : (Set<String>) data.keySet()) {
            objStr += key + ":" + data.get(key) + Defines.SYSTEM_SPACE;
        }
        logHandler.logInfo(LogMessages.getMessage(LogMessages.DBLOADER_LOAD_RECORD_IN_MEMORY) + Defines.SYSTEM_SPACE + objStr);

        //add data to the map
        if (runningMapData.getControl().isEmpty()) {
            addDataToRunningMap(data);
        }

        listOfMaps.add(data);
        mapsCounter++;

        //check if reach the limit to run the sql loader or not
        if (mapsCounter >= commitThreshold) {
            return commit();
        }
        return true;
    }

    private void addDataToRunningMap(Map<String, Object> data) {
        String controlFileContent = "(\n";
        runningMapData.setHeader(data.keySet().toArray(new String[data.keySet().size()]));
        for (Map.Entry<String, Object> entrySet : data.entrySet()) {
            for (DBMapperDetail mapperDetail : dbMapperMaster.getDetailsList()) {
                if (mapperDetail.getField().toLowerCase().equals(entrySet.getKey().toLowerCase())) {
                    if (entrySet.getValue().getClass().toString().toLowerCase().contains("date")) {
                        controlFileContent += mapperDetail.getDatabase() + " TIMESTAMP WITH TIME ZONE \"DY MON dd hh24:mi:ss TZR YYYY\",\n";
                    } else {
                        controlFileContent += mapperDetail.getDatabase() + ",\n";
                    }
                }
            }
        }
        runningMapData.setControl(controlFileContent.substring(0, controlFileContent.length() - 2) + "\n)");
    }

    @Override
    public boolean connect(DBLoaderConfig config) {
        return true;
    }

    @Override
    public void disconnect() {
    }

    private Results createAndRunControlFile() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        File filePath = new File(String.format("%s\\%s\\%s.%s", new File(".\\Loader"), System.currentTimeMillis(), Utils.randomAlphanumeric(8), "dat"));

        if (!filePath.exists()) {
            filePath.getParentFile().mkdirs();
        }

        filePath.createNewFile();

        FileWriter fw = new FileWriter(filePath);
        StringWriter sw = new StringWriter();
        CsvMapWriter writer = new CsvMapWriter(sw, CsvPreference.STANDARD_PREFERENCE);
        for (Map<String, Object> data : listOfMaps) {
            writer.write(data, data.keySet().toArray(new String[data.keySet().size()]));
        }
        writer.close();
        fw.write(sw.toString());
        fw.close();

        return bulkLoad(filePath, dbConnectionConfig.getUsername(), dbConnectionConfig.getPassword(), dbConnectionConfig.getIp(), dbConnectionConfig.getInstance());
    }

    private Results bulkLoad(File dataFile, String username, String password, String ip, String instance) throws IOException {
        final File initialDirectory = dataFile.getParentFile();
        final String dataFileName = dataFile.getName();
        final String controlFileName = dataFileName + ".ctl";
        final String logFileName = dataFileName + ".log";
        final String badFileName = dataFileName + ".bad";
        final String discardFileName = dataFileName + ".discard";

        final File controlFile = new File(initialDirectory, controlFileName);
        final String controlFileContents = createControlFile(dataFileName, badFileName, discardFileName, runningMapData);
        Files.write(controlFile.toPath(), controlFileContents.getBytes(), StandardOpenOption.CREATE_NEW);

        final ExitCode exitCode = runSqlLdrProcess(initialDirectory, dataFileName + ".stdout.log",
                dataFileName + ".stderr.log", controlFileName, logFileName, username, password, ip, instance);

        Results ret = new Results(
                exitCode,
                controlFile,
                new File(initialDirectory, logFileName),
                new File(initialDirectory, badFileName),
                new File(initialDirectory, discardFileName)
        );
        return ret;
    }

    private String createControlFile(String dataFileName, String badFileName, String discardFileName,
            RunningMap parameters) {
        String control = "load data infile '" + dataFileName + "'\n"
                + "badfile '" + badFileName + "'\n"
                + "discardfile '" + discardFileName + "'\n"
                + "append\n"
                + "into table " + parameters.getTableName() + "\n"
                + "fields terminated by ','\n"; // comma separated fields
        return control + parameters.getControl();
    }

    private ExitCode runSqlLdrProcess(File initialDir, String stdoutLogFile, String stderrLogFile,
            String controlFile, String logFile, String username, String password, String ip, String instance) throws IOException {
        final ProcessBuilder pb = new ProcessBuilder(
                "sqlldr",
                "control='" + controlFile + "'",
                "log='" + logFile + "'",
                "userid=" + username + "/" + password + "@" + ip + "/" + instance,
                "silent=header"
        );
        pb.directory(initialDir);
        if (stdoutLogFile != null) {
            pb.redirectOutput(ProcessBuilder.Redirect.appendTo(new File(initialDir, stdoutLogFile)));
        }
        if (stderrLogFile != null) {
            pb.redirectError(ProcessBuilder.Redirect.appendTo(new File(initialDir, stderrLogFile)));
        }
        final Process process = pb.start();
        try {
            process.waitFor(); // TODO may implement here timeout mechanism and progress monitor instead of just blocking the caller thread.
        } catch (InterruptedException ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
        }

        final int exitCode = process.exitValue();

        // Exit codes are OS dependent. Convert them to our OS independent.
        switch (exitCode) {
            case 0:
                return ExitCode.SUCCESS;
            case 1:
                return ExitCode.FAIL;
            case 2:
                return ExitCode.WARN;
            case 3:
                return IS_WINDOWS ? ExitCode.FAIL : ExitCode.FATAL;
            case 4:
                return ExitCode.FATAL;
            default:
                return ExitCode.UNKNOWN;

        }
    }

    public static class Results {

        public final ExitCode exitCode;
        public final File controlFile;
        public final File logFile;
        public final File badFile;
        public final File discardFile;

        public Results(ExitCode exitCode, File controlFile, File logFile, File badFile, File discardFile) {
            this.exitCode = exitCode;
            this.controlFile = controlFile;
            this.logFile = logFile;
            this.badFile = badFile;
            this.discardFile = discardFile;
        }
    }

    public boolean commit() {
        try {
            logHandler.logInfo(LogMessages.getMessage(LogMessages.DBLOADER_SUBMIT_RECORDS));
            //write the control file
            Results results = createAndRunControlFile();

            listOfMaps.clear();
            mapsCounter = 0;
            return true;
        } catch (SQLException | IOException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
            return false;
        }
    }
}
