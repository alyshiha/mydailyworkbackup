/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.dbloader.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.DBConnectionConfig;
import com.evision.bingo.core.model.DBLoaderConfig;
import com.evision.bingo.core.model.DBMapperMaster;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.dbloader.service.DateHandlerPreProcessor;
import com.evision.bingo.dbloader.service.OracleSQLPatchUpdaterService;
import com.evision.bingo.dbloader.utils.DBLoaderDefines;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 */
public class OracleSQLPatchUpdateController extends BaseDBLoaderController {

    private OracleSQLPatchUpdaterService oracleSQLPatchUpdaterService = null;
    private ScheduledExecutorService service = null;
    private Runnable autoCommitRunnable = null;

    public OracleSQLPatchUpdateController(DBLoaderConfig dbLoaderConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<DBConnectionConfig> dbConnectionsList, List<DBMapperMaster> dbMapperMastersList, List<BaseController> eventsHandlerList) {
        super(new LogHandler(dbLoaderConfig.getLoggerPathList(), systemLogger), new QueueHandler(dbLoaderConfig.getQueuesList(), rabbitTemplatesMap), dbLoaderConfig, dbConnectionsList, dbMapperMastersList, eventsHandlerList);

        oracleSQLPatchUpdaterService = new OracleSQLPatchUpdaterService(queueHandler, logHandler, dbLoaderConfig, dbLoaderConfig.getDbConnection(), dbLoaderConfig.getMapper(), Integer.parseInt(dbLoaderConfig.getCommitThreshold()), dbConnectionsList, dbMapperMastersList, eventsHandlerList);

        autoCommitRunnable = new Runnable() {
            public void run() {
                oracleSQLPatchUpdaterService.commit();
            }
        };
        service = Executors.newSingleThreadScheduledExecutor();
        registerPreProcessor(new DateHandlerPreProcessor());
    }

    @Override
    synchronized boolean doCollect() {
        if (getObj() != null && getExtraObj() != null) {
            if (oracleSQLPatchUpdaterService.connect((DBLoaderConfig) config) && oracleSQLPatchUpdaterService.loadMessage((Map) getObj())) {
                service.schedule(autoCommitRunnable, DBLoaderDefines.AUTO_COMMIT_DELAY, TimeUnit.SECONDS);
            }
        }
        return true;
    }

    @Override
    public void stop() {
        try {
            service.shutdown();
        } catch (Exception ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
        }
        oracleSQLPatchUpdaterService.disconnect();
    }
}