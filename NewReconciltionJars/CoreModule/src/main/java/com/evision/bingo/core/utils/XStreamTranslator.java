/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.utils;

import com.evision.bingo.core.model.BackupConfig;
import com.evision.bingo.core.model.BackupSystemConfig;
import com.evision.bingo.core.model.CollectorConfig;
import com.evision.bingo.core.model.CollectorSystemConfig;
import com.evision.bingo.core.model.ConverterConfig;
import com.evision.bingo.core.model.ConverterSystemConfig;
import com.evision.bingo.core.model.Extension;
import com.evision.bingo.core.model.LoggerConfig;
import com.evision.bingo.core.model.Queue;
import com.evision.bingo.core.model.QueueConfig;
import com.evision.bingo.core.model.QueuingSystemConfig;
import com.evision.bingo.core.model.SystemConfig;
import com.evision.bingo.core.model.FieldDetails;
import com.evision.bingo.core.model.FieldsMaster;
import com.evision.bingo.core.model.HeaderConversionTemplate;
import com.evision.bingo.core.model.UnstructuredConversionTemplate;
import com.evision.bingo.core.model.DBConnectionConfig;
import com.evision.bingo.core.model.DBConnectionSystemConfig;
import com.evision.bingo.core.model.DBLoaderConfig;
import com.evision.bingo.core.model.DBLoaderSystemConfig;
import com.evision.bingo.core.model.DBMapperConfig;
import com.evision.bingo.core.model.DBMapperDetail;
import com.evision.bingo.core.model.DBMapperMaster;
import com.evision.bingo.core.model.DBMapperSystemConfig;
import com.evision.bingo.core.model.DisputeConfig;
import com.evision.bingo.core.model.EventsHandlerConfig;
import com.evision.bingo.core.model.MatchingConfig;
import com.evision.bingo.core.model.MatchingSystemConfig;
import com.evision.bingo.core.model.ParserConfig;
import com.evision.bingo.core.model.RejectedConfig;
import com.evision.bingo.core.model.SystemEventsHandlerConfig;
import com.evision.bingo.core.model.ValidationConfig;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import com.thoughtworks.xstream.XStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @since 1/4/2017
 * @author Aly Shiha
 *
 * This is a utility class to wrap the XStream library for loading xml files
 */
public final class XStreamTranslator {

    private XStream xstream = null;
    private Map<String, Class> xmlModelsAliases = new HashMap();

    private XStreamTranslator() {
        //all models to be aliased to avoid writing the full package name
        xstream = new XStream();
        xmlModelsAliases.put("SystemConfig", SystemConfig.class);
        xmlModelsAliases.put("LoggerConfig", LoggerConfig.class);
        xmlModelsAliases.put("SystemEventsHandlerConfig", SystemEventsHandlerConfig.class);
        xmlModelsAliases.put("EventsHandlerConfig", EventsHandlerConfig.class);
        xmlModelsAliases.put("QueuingSystemConfig", QueuingSystemConfig.class);
        xmlModelsAliases.put("QueueConfig", QueueConfig.class);
        xmlModelsAliases.put("Queue", Queue.class);
        xmlModelsAliases.put("CollectorSystemConfig", CollectorSystemConfig.class);
        xmlModelsAliases.put("CollectorConfig", CollectorConfig.class);
        xmlModelsAliases.put("ConverterSystemConfig", ConverterSystemConfig.class);
        xmlModelsAliases.put("ConverterConfig", ConverterConfig.class);
        xmlModelsAliases.put("Extension", Extension.class);
        xmlModelsAliases.put("UnstructuredTemplate", UnstructuredConversionTemplate.class);
        xmlModelsAliases.put("FieldsMaster", FieldsMaster.class);
        xmlModelsAliases.put("FieldDetails", FieldDetails.class);
        xmlModelsAliases.put("HeaderTemplate", HeaderConversionTemplate.class);
        xmlModelsAliases.put("BackupSystemConfig", BackupSystemConfig.class);
        xmlModelsAliases.put("BackupConfig", BackupConfig.class);
        xmlModelsAliases.put("DBConnectionConfig", DBConnectionConfig.class);
        xmlModelsAliases.put("DBConnectionSystemConfig", DBConnectionSystemConfig.class);
        xmlModelsAliases.put("DBLoaderConfig", DBLoaderConfig.class);
        xmlModelsAliases.put("DBLoaderSystemConfig", DBLoaderSystemConfig.class);
        xmlModelsAliases.put("DBMapperConfig", DBMapperConfig.class);
        xmlModelsAliases.put("DBMapperMaster", DBMapperMaster.class);
        xmlModelsAliases.put("DBMapperDetail", DBMapperDetail.class);
        xmlModelsAliases.put("DBMapperSystemConfig", DBMapperSystemConfig.class);
        xmlModelsAliases.put("ParserConfig", ParserConfig.class);
        xmlModelsAliases.put("ValidationConfig", ValidationConfig.class);
        xmlModelsAliases.put("DisputeConfig", DisputeConfig.class);
        xmlModelsAliases.put("RejectedConfig", RejectedConfig.class);
        xmlModelsAliases.put("MatchingSystemConfig", MatchingSystemConfig.class);
        xmlModelsAliases.put("MatchingConfig", MatchingConfig.class);

        for (String key : xmlModelsAliases.keySet()) {
            xstream.alias(key, xmlModelsAliases.get(key));
        }
    }

    public String toXMLString(Object object) {
        return xstream.toXML(object);
    }

    public Object toObject(String xml) {
        return (Object) xstream.fromXML(xml);
    }

    public static XStreamTranslator getInstance() {
        return new XStreamTranslator();
    }

    public Object toObject(File xmlFile) throws IOException {
        return xstream.fromXML(new FileReader(xmlFile));
    }

    public void toXMLFile(Object objTobeXMLTranslated, String fileName) throws IOException {
        FileWriter writer = new FileWriter(fileName);
        xstream.toXML(objTobeXMLTranslated, writer);
        writer.close();
    }

    public void toXMLFile(Object objTobeXMLTranslated, String fileName, List omitFieldsRegXList) throws IOException {
        xstreamInitializeSettings(objTobeXMLTranslated, omitFieldsRegXList);
        toXMLFile(objTobeXMLTranslated, fileName);
    }

    public void xstreamInitializeSettings(Object objTobeXMLTranslated, List omitFieldsRegXList) {
        if (omitFieldsRegXList != null && omitFieldsRegXList.size() > 0) {
            Iterator itr = omitFieldsRegXList.iterator();
            while (itr.hasNext()) {
                String omitEx = itr.next().toString();
                xstream.omitField(objTobeXMLTranslated.getClass(), omitEx);
            }
        }
    }

    public void toXMLFile(Object objTobeXMLTranslated) throws IOException {
        toXMLFile(objTobeXMLTranslated, objTobeXMLTranslated.getClass().getName() + ".xml");
    }
}
