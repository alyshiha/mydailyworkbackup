/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

/**
 * @since 20/4/2017
 * @author Khaled Khalil
 *
 * Interface for collectors system configuration objects
 */
public interface CollectorConfigurable extends Configurable {}
