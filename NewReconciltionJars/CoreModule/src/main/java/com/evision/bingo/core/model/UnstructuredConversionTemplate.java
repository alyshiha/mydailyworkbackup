/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 1/5/2017
 * @author Aly Shiha
 *
 */
public class UnstructuredConversionTemplate extends BaseConverterTemplate {

    private String startString;
    private int startPositionIndex;
    private String endString;
    private int endPositionIndex;
    private String skipHeader;
    private List<String> resetTagList;
    private List<FieldsMaster> constantTemplatesList;
    private List<FieldsMaster> constantTransactionList;
    private List<List<FieldsMaster>> detailTemplatesList;

    public String getStartString() {
        return startString;
    }

    public void setStartString(String startString) {
        this.startString = startString;
    }

    public int getStartPositionIndex() {
        return startPositionIndex;
    }

    public void setStartPositionIndex(int startPositionIndex) {
        this.startPositionIndex = startPositionIndex;
    }

    public String getEndString() {
        return endString;
    }

    public void setEndString(String endString) {
        this.endString = endString;
    }

    public int getEndPositionIndex() {
        return endPositionIndex;
    }

    public void setEndPositionIndex(int endPositionIndex) {
        this.endPositionIndex = endPositionIndex;
    }

    public String getSkipHeader() {
        return skipHeader;
    }

    public void setSkipHeader(String skipHeader) {
        this.skipHeader = skipHeader;
    }

    public List<String> getResetTagList() {
        return resetTagList;
    }

    public void setResetTagList(List<String> resetTagList) {
        this.resetTagList = resetTagList;
    }

    public List<FieldsMaster> getConstantTemplatesList() {
        return constantTemplatesList;
    }

    public void setConstantTemplatesList(List<FieldsMaster> constantTemplatesList) {
        this.constantTemplatesList = constantTemplatesList;
    }

    public List<FieldsMaster> getConstantTransactionList() {
        return constantTransactionList;
    }

    public void setConstantTransactionList(List<FieldsMaster> constantTransactionList) {
        this.constantTransactionList = constantTransactionList;
    }

    public List<List<FieldsMaster>> getDetailTemplatesList() {
        return detailTemplatesList;
    }

    public void setDetailTemplatesList(List<List<FieldsMaster>> detailTemplatesList) {
        this.detailTemplatesList = detailTemplatesList;
    }
}
