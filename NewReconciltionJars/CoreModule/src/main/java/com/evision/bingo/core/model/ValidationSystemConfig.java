/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 4/7/2017
 * @author Khaled Khalil
 *
 * Model for system transaction validation configuration
 */
public class ValidationSystemConfig implements Configurable {

    private List<ValidationConfig> validationConfigList;

    public List<ValidationConfig> getValidationConfigList() {
        return validationConfigList;
    }

    public void setValidationConfigList(List<ValidationConfig> validationConfigList) {
        this.validationConfigList = validationConfigList;
    }
}
