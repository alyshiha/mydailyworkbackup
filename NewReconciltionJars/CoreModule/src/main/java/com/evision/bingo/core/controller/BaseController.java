/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.controller;

import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;

/**
 * @since 20/4/2017
 * @author Khaled Khalil
 *
 * This is the base controller class for all system controllers
 */
public abstract class BaseController implements Runnable {

    //logger object
    protected LogHandler logHandler;
    protected QueueHandler queueHandler;
    protected Configurable config;
    private Object obj;
    private Object extraObj;
    protected List<BaseController> eventsHandlerList;

    public BaseController(LogHandler logger, QueueHandler queueHandler, Configurable config, List<BaseController> eventsHandlerList) {
        this.logHandler = logger;
        this.queueHandler = queueHandler;
        this.config = config;
        this.eventsHandlerList = eventsHandlerList;
    }

    @Override
    public void run() {
        handleRequest(obj);
    }

    //init function
    public abstract void init();

    //handle request function
    public abstract boolean handleRequest(Object obj);

    //clear memory space function as controller is being destroyed
    public abstract void destroy();

    //stop the listening process or interupt current running process
    public abstract void stop();

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public Object getExtraObj() {
        return extraObj;
    }

    public void setExtraObj(Object extraObj) {
        this.extraObj = extraObj;
    }
}
