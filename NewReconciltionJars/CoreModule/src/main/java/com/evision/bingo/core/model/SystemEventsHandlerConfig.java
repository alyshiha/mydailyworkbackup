/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 29/5/2017
 * @author Khaled Khalil
 *
 * Model for system events handler
 */
public class SystemEventsHandlerConfig implements Configurable {

    private List<EventsHandlerConfig> eventHandlersList;

    public List<EventsHandlerConfig> getEventHandlersList() {
        return eventHandlersList;
    }

    public void setEventHandlersList(List<EventsHandlerConfig> eventHandlersList) {
        this.eventHandlersList = eventHandlersList;
    }

}
