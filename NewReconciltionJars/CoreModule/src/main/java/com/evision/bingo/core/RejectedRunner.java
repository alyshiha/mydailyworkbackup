/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.BaseRejectedSet;
import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.model.Queue;
import com.evision.bingo.core.model.QueueConfig;
import com.evision.bingo.core.model.QueuingSystemConfig;
import com.evision.bingo.core.model.RejectedConfig;
import com.evision.bingo.core.model.RejectedConfigurable;
import com.evision.bingo.core.model.RejectedSystemConfig;
import com.evision.bingo.core.model.SystemConfig;
import com.evision.bingo.core.service.RabbitMQConfig;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.core.utils.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.JsonMessageConverter;

/**
 * @since 4/7/2017
 * @author Khaled Khalil
 *
 */
public class RejectedRunner implements BaseModuleRunner {

    private final List<BaseController> rejectedControllers = new ArrayList<>();
    private final Map<String, RabbitTemplate> rabbitTemplatesMap = new HashMap<>();
    private LogHandler systemLogHandler;

    @Override
    public void run(SystemConfig systemConfig, LogHandler systemLogHandler, List<BaseController> eventsHandlerList) {
        //configure the queue engine
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.REJECTED_SETUP_QUEUE));
        for (QueueConfig queueConfig : ((QueuingSystemConfig) systemConfig.getQueuingSystemConfig()).getQueuesList()) {
            rabbitTemplatesMap.put(queueConfig.getName(), RabbitMQConfig.getRabbitTemplateInstance(queueConfig.getHostname(), queueConfig.getUsername(), queueConfig.getPassword()));
        }

        this.systemLogHandler = systemLogHandler;

        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.REJECTED_SETUP_QUEUE_COMPLETE));

        for (RejectedConfig rejectedConfig : ((RejectedSystemConfig) systemConfig.getRejectedSystemConfig()).getRejectedConfigList()) {
            if (rejectedConfig.getActive() != null && !rejectedConfig.getActive().isEmpty() && Defines.ACTIVE.TRUE.equals(Defines.ACTIVE.valueOf(rejectedConfig.getActive().toUpperCase()))) {
                try {
                    Class<?> rejectedClass = Class.forName(rejectedConfig.getClassname());
                    systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.REJECTED_SETUP_TRANSACTION_REJECTED) + Defines.SYSTEM_SPACE + rejectedClass.toString());
                    Constructor<?> controllerConstructor = rejectedClass.getConstructor(RejectedConfig.class, Map.class, LogHandler.class, List.class, List.class);

                    List<RejectedConfigurable> rejectedMappersList = (List<RejectedConfigurable>) XStreamTranslator.getInstance().toObject(new File(rejectedConfig.getPath()));

                    //now loop for the sub config templates and get them loaded
                    if (rejectedMappersList != null && !rejectedMappersList.isEmpty()) {
                        for (RejectedConfigurable subConfig : rejectedMappersList) {
                            subConfig.setRejectedSetsList((List<BaseRejectedSet>) XStreamTranslator.getInstance().toObject(new File(subConfig.getFileTemplate())));
                        }
                    }

                    BaseController rejectedController = (BaseController) controllerConstructor.newInstance(rejectedConfig, rabbitTemplatesMap, systemLogHandler, rejectedMappersList, eventsHandlerList);

                    setupListener(rejectedConfig.getQueuesList(), rejectedController, rabbitTemplatesMap, systemConfig.getRejectedSystemConfig(), rejectedConfig);

                    Thread tempLocalThread = new Thread(rejectedController);
                    tempLocalThread.start();
                    rejectedControllers.add(rejectedController);
                } catch (InvocationTargetException | IllegalAccessException | InstantiationException | IOException | ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalArgumentException ex) {
                    systemLogHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                }
            }
        }

        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.REJECTED_SETUP_TRANSACTION_REJECTED_COMPLETE));

//        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.REJECTED_SETUP_QUEUES_LISTENERS));
//        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.REJECTED_SETUP_QUEUES_LISTENERS_COMPLETE));
    }

    @Override
    public void setupListener(List<Queue> queuesListParam, BaseController controller, Map<String, RabbitTemplate> rabbitTemplatesMap, Configurable extra1, Configurable extra2) {
        //add listeners to queues for messages
        for (String nameKey : rabbitTemplatesMap.keySet()) {
            RabbitTemplate template = rabbitTemplatesMap.get(nameKey);

            SimpleMessageListenerContainer listenerContainer = new SimpleMessageListenerContainer();
            listenerContainer.setConnectionFactory(template.getConnectionFactory());

            ArrayList<org.springframework.amqp.core.Queue> queuesNames = new ArrayList();
            ArrayList<Queue> queuesList = new ArrayList();
            for (RejectedConfig rejectedConfig : ((RejectedSystemConfig) extra1).getRejectedConfigList()) {
                if (rejectedConfig.getName().equals(((RejectedConfig) extra2).getName())) {
                    for (Queue queue : rejectedConfig.getQueuesList()) {
                        if (queue.getName().equals(nameKey) && Queue.QueueActionType.valueOf(queue.getAction()).equals(Queue.QueueActionType.CONSUME)) {
                            queuesNames.add(new org.springframework.amqp.core.Queue(queue.getKey()));
                            queuesList.add(queue);
                        }
                    }
                }
            }
            org.springframework.amqp.core.Queue[] queues = new org.springframework.amqp.core.Queue[queuesNames.size()];
            for (int i = 0; i < queuesNames.size(); i++) {
                queues[i] = queuesNames.get(i);
            }

            //create the queues as the listener will not work if not existing
            QueueHandler queueHandler = new QueueHandler(queuesList, rabbitTemplatesMap);
            queueHandler.createQueuesIfNotCreated();

            //add the listener for messages
            listenerContainer.setQueues(queues);
            listenerContainer.setMessageConverter(new JsonMessageConverter());
            listenerContainer.setConcurrentConsumers(MainRunner.SYSTEM_CONCURRENT_QUEUES_CONSUMER);
            listenerContainer.setPrefetchCount(MainRunner.SYSTEM_CONCURRENT_QUEUES_CONSUMER);
            listenerContainer.setAcknowledgeMode(AcknowledgeMode.AUTO);
            listenerContainer.setDefaultRequeueRejected(true);
            listenerContainer.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message msg) {
                    controller.handleRequest(msg);
                }
            });
            listenerContainer.start();
        }
    }

    @Override
    public void stop() {
        for (BaseController cc : rejectedControllers) {
            systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.REJECTED_SHUTDOWN_TRANSACTION_REJECTED) + Defines.SYSTEM_SPACE + cc.toString());
            cc.stop();
        }
    }

    @Override
    public List<BaseController> getControllers() {
        return rejectedControllers;
    }
}
