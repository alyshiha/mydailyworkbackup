/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 29/5/2017
 * @author Khaled Khalil
 *
 * Model for system events handler
 */
public class EventsHandlerConfig implements Configurable {

    private String classname;
    private List<Queue> queuesList;
    private String active;
    private List<LoggerConfig> loggerPathList;

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public List<Queue> getQueuesList() {
        return queuesList;
    }

    public void setQueuesList(List<Queue> queuesList) {
        this.queuesList = queuesList;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public List<LoggerConfig> getLoggerPathList() {
        return loggerPathList;
    }

    public void setLoggerPathList(List<LoggerConfig> loggerPathList) {
        this.loggerPathList = loggerPathList;
    }
}
