/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 1/5/2017
 * @author Aly Shiha
 *
 */
public class FieldsMaster {

    private String header;
    private String mandatory;
    private List<FieldDetails> detailsList;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public List<FieldDetails> getDetailsList() {
        return detailsList;
    }

    public void setDetailsList(List<FieldDetails> detailsList) {
        this.detailsList = detailsList;
    }

}
