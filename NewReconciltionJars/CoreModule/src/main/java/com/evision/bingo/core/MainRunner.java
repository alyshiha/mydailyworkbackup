/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core;

import com.evision.bingo.core.model.SystemConfig;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.MessagesHandler;
import com.evision.bingo.core.utils.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import org.boris.winrun4j.AbstractService;
import org.boris.winrun4j.ServiceException;

/**
 * @since 1/4/2017
 * @author Aly Shiha
 *
 * This is the main runner class to load all the core system classes in memory
 * this class is called from the windows service
 */
public final class MainRunner extends AbstractService {

    public static int SYSTEM_CONCURRENT_QUEUES_CONSUMER = Defines.DEFAULT_SYSTEM_CONCURRENT_QUEUES_CONSUMER;
    public static String SYSTEM_LOG_LAYOUT_PATTERN = Defines.DEFAULT_SYSTEM_LOG_LAYOUT_PATTERN;

    private static LogHandler systemLogHandler;
    private static SystemConfig systemConfig;
    private static MainRunner mainRunner;
    private final BaseModuleRunner systemEventsHandlerRunner;
    private final BaseModuleRunner collectorRunner;
    private final BaseModuleRunner converterRunner;
    private final BaseModuleRunner backupRunner;
    private final BaseModuleRunner dbLoaderRunner;
    private final BaseModuleRunner parserRunner;
    private final BaseModuleRunner validationRunner;
    private final BaseModuleRunner disputeRunner;
    private final BaseModuleRunner rejectedRunner;
    private final BaseModuleRunner matchingRunner;

    MainRunner() {
        collectorRunner = new CollectorRunner();
        converterRunner = new ConverterRunner();
        backupRunner = new BackupRunner();
        dbLoaderRunner = new DBLoaderRunner();
        parserRunner = new ParserRunner();
        validationRunner = new ValidationRunner();
        disputeRunner = new DisputeRunner();
        rejectedRunner = new RejectedRunner();
        matchingRunner = new MatchingRunner();

        systemEventsHandlerRunner = new SystemEventsHandlerRunner();
    }

    public int doRequest(int request) throws ServiceException {
        switch (request) {
            case SERVICE_CONTROL_STOP:
            case SERVICE_CONTROL_SHUTDOWN:
                shutdown();
                shutdown = true;

                break;
        }
        return 0;
    }

    public int serviceMain(String[] args) throws ServiceException {
        mainRunner = new MainRunner();

        // loading configuration file
        try {
            //load the configuration xml file of the system
            systemConfig = (SystemConfig) XStreamTranslator.getInstance().toObject(new File(Defines.SYSTEM_CONFIG_PATH));
            setStaticVariable(systemConfig);
            systemLogHandler = new LogHandler(systemConfig.getLoggerConfig());
            MessagesHandler.initMessagesHandler(systemLogHandler);
            systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_START));
            systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_LOAD_SYSTEM_CONFIGURATION));
            systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_LOAD_SYSTEM_MESSAGES));
        } catch (IOException ex) {
            systemLogHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
        }
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_LOAD_SYSTEM_MESSAGES_COMPLETE));
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_LOAD_SYSTEM_CONFIGURATION_COMPLETE));

        if (systemConfig != null) {
            //running the system events controller 
            if (systemConfig.getSystemEventsHandlerConfig() != null) {
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_EVENT_HANDLER_START));
                systemEventsHandlerRunner.run(systemConfig, systemLogHandler, null);
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_EVENT_HANDLER_START_COMPLETE));
            }

            // running the collector module
            if (systemConfig.getCollectorSystemConfig() != null) {
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_START));
                collectorRunner.run(systemConfig, systemLogHandler, systemEventsHandlerRunner.getControllers());
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_START_COMPLETE));
            }

            //running the converter module
            if (systemConfig.getConverterSystemConfig() != null) {
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_START));
                converterRunner.run(systemConfig, systemLogHandler, systemEventsHandlerRunner.getControllers());
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_START_COMPLETE));
            }

            //running the backup module
            if (systemConfig.getBackupSystemConfig() != null) {
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.BACKUP_START));
                backupRunner.run(systemConfig, systemLogHandler, systemEventsHandlerRunner.getControllers());
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.BACKUP_START_COMPLETE));
            }

            //running the dbLoader module
            if (systemConfig.getDbLoaderSystemConfig() != null) {
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.DBLOADER_START));
                dbLoaderRunner.run(systemConfig, systemLogHandler, systemEventsHandlerRunner.getControllers());
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.DBLOADER_START_COMPLETE));
            }

            //running the parser module
            if (systemConfig.getParserSystemConfig() != null) {
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.PARSER_START));
                parserRunner.run(systemConfig, systemLogHandler, systemEventsHandlerRunner.getControllers());
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.PARSER_START_COMPLETE));
            }

            //running the transaction validation module
            if (systemConfig.getValidationSystemConfig() != null) {
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.VALIDATION_START));
                validationRunner.run(systemConfig, systemLogHandler, systemEventsHandlerRunner.getControllers());
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.VALIDATION_START_COMPLETE));
            }

            //running the dispute module
            if (systemConfig.getDisputeSystemConfig() != null) {
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.DISPUTE_START));
                disputeRunner.run(systemConfig, systemLogHandler, systemEventsHandlerRunner.getControllers());
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.DISPUTE_START_COMPLETE));
            }

            //running the rejected module
            if (systemConfig.getRejectedSystemConfig() != null) {
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.REJECTED_START));
                rejectedRunner.run(systemConfig, systemLogHandler, systemEventsHandlerRunner.getControllers());
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.REJECTED_START_COMPLETE));
            }

            //running the matching module
            if (systemConfig.getMatchingSystemConfig() != null) {
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.MATCHING_START));
                matchingRunner.run(systemConfig, systemLogHandler, systemEventsHandlerRunner.getControllers());
                systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.MATCHING_START_COMPLETE));
            }
        }

        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_START_COMPLETE));

        return 0;
    }

    public static void shutdown() {
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_SHUTDOWN));

        //stopping the events handler 
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_EVENT_HANDLER_SHUTDOWN));
        mainRunner.systemEventsHandlerRunner.stop();
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_EVENT_HANDLER_SHUTDOWN_COMPLETE));

        // stopping the collector module
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SHUTDOWN));
        mainRunner.collectorRunner.stop();
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SHUTDOWN_COMPLETE));

        //stopping the convertor module
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_SHUTDOWN));
        mainRunner.converterRunner.stop();
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_SHUTDOWN_COMPLETE));

        //stopping the backup module
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.BACKUP_SHUTDOWN));
        mainRunner.backupRunner.stop();
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.BACKUP_SHUTDOWN_COMPLETE));

        //stopping the dbloader module
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.DBLOADER_SHUTDOWN));
        mainRunner.dbLoaderRunner.stop();
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.DBLOADER_SHUTDOWN_COMPLETE));

        //stopping the parser module
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.PARSER_SHUTDOWN));
        mainRunner.parserRunner.stop();
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.PARSER_SHUTDOWN_COMPLETE));

        //stopping the transaction validation module
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.VALIDATION_SHUTDOWN));
        mainRunner.validationRunner.stop();
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.VALIDATION_SHUTDOWN_COMPLETE));

        //stopping the transaction dispute module
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.DISPUTE_SHUTDOWN));
        mainRunner.disputeRunner.stop();
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.DISPUTE_SHUTDOWN_COMPLETE));

        //stopping the transaction rejected module
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.REJECTED_SHUTDOWN));
        mainRunner.rejectedRunner.stop();
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.REJECTED_SHUTDOWN_COMPLETE));

        //stopping the transaction matching module
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.MATCHING_SHUTDOWN));
        mainRunner.matchingRunner.stop();
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.MATCHING_SHUTDOWN_COMPLETE));

        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_SHUTDOWN_COMPLETE));
    }

    private void setStaticVariable(SystemConfig systemConfig) {
        this.SYSTEM_CONCURRENT_QUEUES_CONSUMER = Integer.parseInt(systemConfig.getConcurrent_queues_consumer());
        this.SYSTEM_LOG_LAYOUT_PATTERN = systemConfig.getSystem_log_layout_pattern();
    }
}
