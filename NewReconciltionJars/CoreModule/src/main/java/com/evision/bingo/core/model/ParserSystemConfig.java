/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 22/6/2017
 * @author Khaled Khalil
 *
 * Model for system parser configuration
 */
public class ParserSystemConfig implements Configurable{

    private List<ParserConfig> parserConfigList;

    public List<ParserConfig> getParserConfigList() {
        return parserConfigList;
    }

    public void setParserConfigList(List<ParserConfig> parserConfigList) {
        this.parserConfigList = parserConfigList;
    }
}
