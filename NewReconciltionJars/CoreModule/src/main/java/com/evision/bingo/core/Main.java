/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core;

/**
 * @since 10/5/2017
 * @author Khaled Khalil
 *
 * run the program from command line and IDE debug
 */
public class Main {

    public static void main(String[] args) {
        try {
            MainRunner runner = new MainRunner();
            runner.serviceMain(args);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
