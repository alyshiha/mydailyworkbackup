/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 * Model for system db mapper configuration
 */
public class DBMapperSystemConfig implements Configurable {

    private List<DBMapperConfig> dbMapperMastersList;

    public List<DBMapperConfig> getDbMappersList() {
        return dbMapperMastersList;
    }

    public void setDbMappersList(List<DBMapperConfig> dbMappersList) {
        this.dbMapperMastersList = dbMappersList;
    }
}
