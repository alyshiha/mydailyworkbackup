/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

/**
 * @since 20/4/2017
 * @author Khaled Khalil
 *
 * Model for system Configuration
 */
public class SystemConfig {

    private String concurrent_queues_consumer;
    private String system_log_layout_pattern;
    private CollectorSystemConfig collectorSystemConfig;
    private ConverterSystemConfig converterSystemConfig;
    private QueuingSystemConfig queuingSystemConfig;
    private LoggerConfig loggerConfig;
    private SystemEventsHandlerConfig systemEventsHandlerConfig;
    private BackupSystemConfig backupSystemConfig;
    private DBConnectionSystemConfig dbConnectionSystemConfig;
    private DBMapperSystemConfig dbMapperSystemConfig;
    private DBLoaderSystemConfig dbLoaderSystemConfig;
    private ParserSystemConfig parserSystemConfig;
    private ValidationSystemConfig validationSystemConfig;
    private DisputeSystemConfig disputeSystemConfig;
    private RejectedSystemConfig rejectedSystemConfig;
    private MatchingSystemConfig matchingSystemConfig;

    public ConverterSystemConfig getConverterSystemConfig() {
        return converterSystemConfig;
    }

    public void setConvertorSystemConfig(ConverterSystemConfig converterSystemConfig) {
        this.converterSystemConfig = converterSystemConfig;
    }

    public CollectorSystemConfig getCollectorSystemConfig() {
        return collectorSystemConfig;
    }

    public void setCollectorSystemConfig(CollectorSystemConfig collectorSystemConfig) {
        this.collectorSystemConfig = collectorSystemConfig;
    }

    public LoggerConfig getLoggerConfig() {
        return loggerConfig;
    }

    public void setLoggerConfig(LoggerConfig loggerConfig) {
        this.loggerConfig = loggerConfig;
    }

    public QueuingSystemConfig getQueuingSystemConfig() {
        return queuingSystemConfig;
    }

    public void setQueuingSystemConfig(QueuingSystemConfig queuingSystemConfig) {
        this.queuingSystemConfig = queuingSystemConfig;
    }

    public SystemEventsHandlerConfig getSystemEventsHandlerConfig() {
        return systemEventsHandlerConfig;
    }

    public void setSystemEventsHandlerConfig(SystemEventsHandlerConfig systemEventsHandlerConfig) {
        this.systemEventsHandlerConfig = systemEventsHandlerConfig;
    }

    public BackupSystemConfig getBackupSystemConfig() {
        return backupSystemConfig;
    }

    public void setBackupSystemConfig(BackupSystemConfig backupSystemConfig) {
        this.backupSystemConfig = backupSystemConfig;
    }

    public DBConnectionSystemConfig getDbSystemConfig() {
        return dbConnectionSystemConfig;
    }

    public void setDbSystemConfig(DBConnectionSystemConfig dbSystemConfig) {
        this.dbConnectionSystemConfig = dbSystemConfig;
    }

    public DBMapperSystemConfig getDbMapperSystemConfig() {
        return dbMapperSystemConfig;
    }

    public void setDbMapperSystemConfig(DBMapperSystemConfig dbMapperSystemConfig) {
        this.dbMapperSystemConfig = dbMapperSystemConfig;
    }

    public DBLoaderSystemConfig getDbLoaderSystemConfig() {
        return dbLoaderSystemConfig;
    }

    public void setDbLoaderSystemConfig(DBLoaderSystemConfig dbLoaderSystemConfig) {
        this.dbLoaderSystemConfig = dbLoaderSystemConfig;
    }

    public String getConcurrent_queues_consumer() {
        return concurrent_queues_consumer;
    }

    public void setConcurrent_queues_consumer(String concurrent_queues_consumer) {
        this.concurrent_queues_consumer = concurrent_queues_consumer;
    }

    public DBConnectionSystemConfig getDbConnectionSystemConfig() {
        return dbConnectionSystemConfig;
    }

    public void setDbConnectionSystemConfig(DBConnectionSystemConfig dbConnectionSystemConfig) {
        this.dbConnectionSystemConfig = dbConnectionSystemConfig;
    }

    public String getSystem_log_layout_pattern() {
        return system_log_layout_pattern;
    }

    public void setSystem_log_layout_pattern(String system_log_layout_pattern) {
        this.system_log_layout_pattern = system_log_layout_pattern;
    }

    public ParserSystemConfig getParserSystemConfig() {
        return parserSystemConfig;
    }

    public void setParserSystemConfig(ParserSystemConfig parserSystemConfig) {
        this.parserSystemConfig = parserSystemConfig;
    }

    public ValidationSystemConfig getValidationSystemConfig() {
        return validationSystemConfig;
    }

    public void setValidationSystemConfig(ValidationSystemConfig validationSystemConfig) {
        this.validationSystemConfig = validationSystemConfig;
    }

    public DisputeSystemConfig getDisputeSystemConfig() {
        return disputeSystemConfig;
    }

    public void setDisputeSystemConfig(DisputeSystemConfig disputeSystemConfig) {
        this.disputeSystemConfig = disputeSystemConfig;
    }

    public RejectedSystemConfig getRejectedSystemConfig() {
        return rejectedSystemConfig;
    }

    public void setRejectedSystemConfig(RejectedSystemConfig rejectedSystemConfig) {
        this.rejectedSystemConfig = rejectedSystemConfig;
    }

    public MatchingSystemConfig getMatchingSystemConfig() {
        return matchingSystemConfig;
    }

    public void setMatchingSystemConfig(MatchingSystemConfig matchingSystemConfig) {
        this.matchingSystemConfig = matchingSystemConfig;
    }
}
