/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.BaseConverterTemplate;
import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.model.ConverterConfig;
import com.evision.bingo.core.model.ConverterConfigurable;
import com.evision.bingo.core.model.ConverterSystemConfig;
import com.evision.bingo.core.model.Queue;
import com.evision.bingo.core.model.QueueConfig;
import com.evision.bingo.core.model.QueuingSystemConfig;
import com.evision.bingo.core.model.SystemConfig;
import com.evision.bingo.core.service.RabbitMQConfig;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.core.utils.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.JsonMessageConverter;

/**
 * @since 17/5/2017
 * @author Khaled Khalil
 *
 * This is the converter runner class that reads configurations of the
 * converters and get them loaded
 */
public class ConverterRunner implements BaseModuleRunner {

    private final List<BaseController> converterControllers = new ArrayList<>();
    private final Map<String, RabbitTemplate> rabbitTemplatesMap = new HashMap<>();
    private LogHandler systemLogHandler;

    @Override
    public void run(SystemConfig systemConfig, LogHandler systemLogHandler, List<BaseController> eventsHandlerList) {
        //configure the queue engine
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_SETUP_QUEUE));
        for (QueueConfig queueConfig : ((QueuingSystemConfig) systemConfig.getQueuingSystemConfig()).getQueuesList()) {
            rabbitTemplatesMap.put(queueConfig.getName(), RabbitMQConfig.getRabbitTemplateInstance(queueConfig.getHostname(), queueConfig.getUsername(), queueConfig.getPassword()));
        }

        this.systemLogHandler = systemLogHandler;

        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_SETUP_QUEUE_COMPLETE));

        for (ConverterConfig converterConfig : ((ConverterSystemConfig) systemConfig.getConverterSystemConfig()).getConverterConfigList()) {
            if (converterConfig.getActive() != null && !converterConfig.getActive().isEmpty() && Defines.ACTIVE.TRUE.equals(Defines.ACTIVE.valueOf(converterConfig.getActive().toUpperCase()))) {
                try {
                    Class<?> converterClass = Class.forName(converterConfig.getClassname());
                    systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_SETUP_CONVERTER) + Defines.SYSTEM_SPACE + converterClass.toString());
                    Constructor<?> controllerConstructor = converterClass.getConstructor(ConverterConfig.class, Map.class, LogHandler.class, List.class, List.class);

                    //load the sub config if any
                    List<ConverterConfigurable> subConfigList = null;
                    if (converterConfig.getPath() != null && !converterConfig.getPath().equals("")) {
                        subConfigList = (List<ConverterConfigurable>) XStreamTranslator.getInstance().toObject(new File(converterConfig.getPath()));

                        //now loop for the sub config templates and get them loaded
                        if (subConfigList != null && !subConfigList.isEmpty()) {
                            for (ConverterConfigurable subConfig : subConfigList) {
                                Class<?> templateClass = Class.forName(subConfig.getTemplateClassname());
                                subConfig.setTemplate((BaseConverterTemplate) templateClass.cast(XStreamTranslator.getInstance().toObject(new File(subConfig.getPath()))));
                            }
                        }
                    }

                    BaseController converterController = (BaseController) controllerConstructor.newInstance(converterConfig, rabbitTemplatesMap, systemLogHandler, subConfigList, eventsHandlerList);

                    setupListener(converterConfig.getQueuesList(), converterController, rabbitTemplatesMap, systemConfig.getConverterSystemConfig(), converterConfig);

                    Thread tempLocalThread = new Thread(converterController);
                    tempLocalThread.start();
                    converterControllers.add(converterController);
                } catch (InvocationTargetException | IllegalAccessException | InstantiationException | IOException | ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalArgumentException ex) {
                    systemLogHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                }
            }
        }

        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_SETUP_CONVERTER_COMPLETE));

//        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_SETUP_QUEUES_LISTENERS));
//        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_SETUP_QUEUES_LISTENERS_COMPLETE));
    }

    @Override
    public void setupListener(List<Queue> queuesListParam, BaseController controller, Map<String, RabbitTemplate> rabbitTemplatesMap, Configurable extra1, Configurable extra2) {
        //add listeners to queues for messages
        for (String nameKey : rabbitTemplatesMap.keySet()) {
            RabbitTemplate template = rabbitTemplatesMap.get(nameKey);

            SimpleMessageListenerContainer listenerContainer = new SimpleMessageListenerContainer();
            listenerContainer.setConnectionFactory(template.getConnectionFactory());

            ArrayList<org.springframework.amqp.core.Queue> queuesNames = new ArrayList();
            ArrayList<Queue> queuesList = new ArrayList();
            for (ConverterConfig converterConfig : ((ConverterSystemConfig) extra1).getConverterConfigList()) {
                if (converterConfig.getName().equals(((ConverterConfig) extra2).getName())) {
                    for (Queue queue : converterConfig.getQueuesList()) {
                        if (queue.getName().equals(nameKey) && Queue.QueueActionType.valueOf(queue.getAction()).equals(Queue.QueueActionType.CONSUME)) {
                            queuesNames.add(new org.springframework.amqp.core.Queue(queue.getKey()));
                            queuesList.add(queue);
                        }
                    }
                }
            }
            org.springframework.amqp.core.Queue[] queues = new org.springframework.amqp.core.Queue[queuesNames.size()];
            for (int i = 0; i < queuesNames.size(); i++) {
                queues[i] = queuesNames.get(i);
            }

            //create the queues as the listener will not work if not existing
            QueueHandler queueHandler = new QueueHandler(queuesList, rabbitTemplatesMap);
            queueHandler.createQueuesIfNotCreated();

            //add the listener for messages
            listenerContainer.setQueues(queues);
            listenerContainer.setMessageConverter(new JsonMessageConverter());
            listenerContainer.setConcurrentConsumers(MainRunner.SYSTEM_CONCURRENT_QUEUES_CONSUMER);
            listenerContainer.setPrefetchCount(MainRunner.SYSTEM_CONCURRENT_QUEUES_CONSUMER);
            listenerContainer.setAcknowledgeMode(AcknowledgeMode.AUTO);
            listenerContainer.setDefaultRequeueRejected(true);
            listenerContainer.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message msg) {
                    controller.handleRequest(msg);
                }
            });
            listenerContainer.start();
        }
    }

    @Override
    public void stop() {
        for (BaseController cc : converterControllers) {
            systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_SHUTDOWN_CONVERTER) + Defines.SYSTEM_SPACE + cc.toString());
            cc.stop();
        }
    }

    @Override
    public List<BaseController> getControllers() {
        return converterControllers;
    }
}
