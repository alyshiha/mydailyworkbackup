/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.utils;

import com.evision.bingo.core.model.Queue;
import com.evision.bingo.core.model.QueueMessage;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.JsonMessageConverter;

/**
 * @since 24/4/2017
 * @author Khaled Khalil
 *
 * This is the main queue handler class to manage the messages retrieval and
 * submission
 */
public final class QueueHandler {

    List<Queue> queues;
    Map<String, RabbitTemplate> rabbitQueues;
    JsonMessageConverter jsonConverter;

    public QueueHandler(List<Queue> queues, Map rabbitQueues) {
        this.queues = queues;
        this.rabbitQueues = rabbitQueues;
        jsonConverter = new JsonMessageConverter();

        createQueuesIfNotCreated();
    }

    public void createQueuesIfNotCreated() {
        //make sure that all queues were created in RabbitMQ
        if (queues != null && queues.size() > 0 && rabbitQueues != null && rabbitQueues.size() > 0) {
            for (Queue q : queues) {
                AmqpAdmin admin = new RabbitAdmin(this.rabbitQueues.get(q.getName()).getConnectionFactory());
                admin.declareQueue(new org.springframework.amqp.core.Queue(q.getKey()));
            }
        }
    }

    public synchronized void sendMessage(QueueMessage record) {
        if (queues != null && queues.size() > 0 && rabbitQueues != null && rabbitQueues.size() > 0) {
            for (Queue q : queues) {
                if (Queue.QueueActionType.valueOf(q.getAction()).equals(Queue.QueueActionType.PRODUCE)) {
                    rabbitQueues.get(q.getName()).send(q.getKey(), jsonConverter.toMessage(record, new MessageProperties()));
                }
            }
        }
    }

    public synchronized void sendMessage(QueueMessage record, MessageProperties messageProperties) {
        if (queues != null && queues.size() > 0 && rabbitQueues != null && rabbitQueues.size() > 0) {
            for (Queue q : queues) {
                if (Queue.QueueActionType.valueOf(q.getAction()).equals(Queue.QueueActionType.PRODUCE)) {
                    rabbitQueues.get(q.getName()).send(q.getKey(), jsonConverter.toMessage(record, messageProperties));
                }
            }
        }
    }

    public synchronized void sendMessage(Message msg) {
        if (queues != null && queues.size() > 0 && rabbitQueues != null && rabbitQueues.size() > 0) {
            for (Queue q : queues) {
                if (Queue.QueueActionType.valueOf(q.getAction()).equals(Queue.QueueActionType.PRODUCE)) {
                    rabbitQueues.get(q.getName()).send(q.getKey(), msg);
                }
            }
        }
    }

    public synchronized void sendMessage(String state, QueueMessage record) {
        if (queues != null && queues.size() > 0 && rabbitQueues != null && rabbitQueues.size() > 0) {
            for (Queue q : queues) {
                if (Queue.QueueActionType.valueOf(q.getAction()).equals(Queue.QueueActionType.PRODUCE) && q.getState().equals(state)) {
                    rabbitQueues.get(q.getName()).send(q.getKey(), jsonConverter.toMessage(record, new MessageProperties()));
                }
            }
        }
    }

    public synchronized void sendMessage(String state, QueueMessage record, MessageProperties messageProperties) {
        if (queues != null && queues.size() > 0 && rabbitQueues != null && rabbitQueues.size() > 0) {
            for (Queue q : queues) {
                if (Queue.QueueActionType.valueOf(q.getAction()).equals(Queue.QueueActionType.PRODUCE) && q.getState().equals(state)) {
                    rabbitQueues.get(q.getName()).send(q.getKey(), jsonConverter.toMessage(record, messageProperties));
                }
            }
        }
    }

    public synchronized void sendMessage(Object record) {
        if (queues != null && queues.size() > 0 && rabbitQueues != null && rabbitQueues.size() > 0) {
            for (Queue q : queues) {
                if (Queue.QueueActionType.valueOf(q.getAction()).equals(Queue.QueueActionType.PRODUCE)) {
                    rabbitQueues.get(q.getName()).send(q.getKey(), jsonConverter.toMessage(record, new MessageProperties()));
                }
            }
        }
    }

    public synchronized void sendMessage(Object record, MessageProperties messageProperties) {
        if (queues != null && queues.size() > 0 && rabbitQueues != null && rabbitQueues.size() > 0) {
            for (Queue q : queues) {
                if (Queue.QueueActionType.valueOf(q.getAction()).equals(Queue.QueueActionType.PRODUCE)) {
                    rabbitQueues.get(q.getName()).send(q.getKey(), jsonConverter.toMessage(record, messageProperties));
                }
            }
        }
    }

    public synchronized void sendMessage(String state, Object record) {
        if (queues != null && queues.size() > 0 && rabbitQueues != null && rabbitQueues.size() > 0) {
            for (Queue q : queues) {
                if (Queue.QueueActionType.valueOf(q.getAction()).equals(Queue.QueueActionType.PRODUCE) && q.getState().equals(state)) {
                    rabbitQueues.get(q.getName()).send(q.getKey(), jsonConverter.toMessage(record, new MessageProperties()));
                }
            }
        }
    }

    public synchronized void sendMessage(String state, Object record, MessageProperties messageProperties) {
        if (queues != null && queues.size() > 0 && rabbitQueues != null && rabbitQueues.size() > 0) {
            for (Queue q : queues) {
                if (Queue.QueueActionType.valueOf(q.getAction()).equals(Queue.QueueActionType.PRODUCE) && q.getState().equals(state)) {
                    rabbitQueues.get(q.getName()).send(q.getKey(), jsonConverter.toMessage(record, messageProperties));
                }
            }
        }
    }

    public synchronized void sendEventMessage(Object event) {
        if (queues != null && queues.size() > 0 && rabbitQueues != null && rabbitQueues.size() > 0) {
            for (Queue q : queues) {
                if (Queue.QueueActionType.valueOf(q.getAction()).equals(Queue.QueueActionType.PRODUCE)) {
                    rabbitQueues.get(q.getName()).send(q.getKey(), jsonConverter.toMessage(event, new MessageProperties()));
                }
            }
        }
    }

    public synchronized void sendEventMessage(Object event, MessageProperties messagePropertie) {
        if (queues != null && queues.size() > 0 && rabbitQueues != null && rabbitQueues.size() > 0) {
            for (Queue q : queues) {
                if (Queue.QueueActionType.valueOf(q.getAction()).equals(Queue.QueueActionType.PRODUCE)) {
                    rabbitQueues.get(q.getName()).send(q.getKey(), jsonConverter.toMessage(event, messagePropertie));
                }
            }
        }
    }

    public synchronized Properties getQueueProperties(String qName) {
        AmqpAdmin admin = new RabbitAdmin(this.rabbitQueues.get(qName).getConnectionFactory());
        return admin.getQueueProperties(qName);
    }
}
