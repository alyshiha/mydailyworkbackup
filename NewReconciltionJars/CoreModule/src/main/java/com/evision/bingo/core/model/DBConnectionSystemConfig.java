/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 * Model for system db configuration
 */
public class DBConnectionSystemConfig implements Configurable {

    private List<DBConnectionConfig> dbConnectionList;

    public List<DBConnectionConfig> getDbConnectionsList() {
        return dbConnectionList;
    }

    public void setDbConnectionsList(List<DBConnectionConfig> dbConnectionsList) {
        this.dbConnectionList = dbConnectionsList;
    }
}
