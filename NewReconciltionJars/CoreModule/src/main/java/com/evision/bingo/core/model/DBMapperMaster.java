/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 * Model for system field db mapper master configuration
 */
public class DBMapperMaster implements Configurable {

    private String name;
    private String tableName;
    private List<DBMapperDetail> detailsList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<DBMapperDetail> getDetailsList() {
        return detailsList;
    }

    public void setDetailsList(List<DBMapperDetail> detailsList) {
        this.detailsList = detailsList;
    }
}
