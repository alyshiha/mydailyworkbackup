/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 12/7/2017
 * @author Khaled Khalil
 *
 * Model for system transaction dispute configuration
 */
public class DisputeSystemConfig implements Configurable {

    private List<DisputeConfig> disputeConfigList;

    public List<DisputeConfig> getDisputeConfigList() {
        return disputeConfigList;
    }

    public void setDisputeConfigList(List<DisputeConfig> disputeConfigList) {
        this.disputeConfigList = disputeConfigList;
    }
}
