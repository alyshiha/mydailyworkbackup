/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.utils;

import java.io.File;
import java.security.SecureRandom;

/**
 * @since 21/5/2017
 * @author Khaled Khalil
 *
 * common utils
 */
public class Utils {

    private static final String randomDomain = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public static String randomAlphanumeric(int size) {
        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder(size);
        for (int i = 0; i < size; i++) {
            sb.append(randomDomain.charAt(random.nextInt(randomDomain.length())));
        }
        return sb.toString();
    }

    public static String removeFileExtension(String filename) {
        return filename.substring(0, filename.lastIndexOf('.'));
    }

    public static File getDestinationFolder(String folderName, String defaultFolder) {
        File destinationFolder;
        if (folderName == null || folderName.trim().isEmpty()) {
            destinationFolder = new File(defaultFolder);
        } else {
            destinationFolder = new File(folderName);
        }

        if (!destinationFolder.exists()) {
            destinationFolder.mkdirs();
        }

        return destinationFolder;
    }
}
