/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 30/7/2017
 * @author Khaled Khalil
 *
 * Model for system matching configuration
 */
public class MatchingSystemConfig implements Configurable {
     private List<MatchingConfig> matchingConfigList;

    public List<MatchingConfig> getMatchingConfigList() {
        return matchingConfigList;
    }

    public void setMatchingConfigList(List<MatchingConfig> matchingConfigList) {
        this.matchingConfigList = matchingConfigList;
    }
}
