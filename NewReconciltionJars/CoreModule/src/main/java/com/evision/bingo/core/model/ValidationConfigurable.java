/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 4/7/2017
 * @author Khaled Khalil
 *
 * Interface for validation system configuration objects
 */
public interface ValidationConfigurable extends Configurable {

    public void setFileType(String fileType);

    public String getFileType();

    public void setFileTemplate(String fileTemplate);

    public String getFileTemplate();

    public void setValidationSetsList(List<BaseValidationSet> validationSetsList);

    public List<BaseValidationSet> getValidationSetsList();
}
