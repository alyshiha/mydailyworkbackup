/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 * Model for db mapper configuration
 */
public class DBMapperConfig implements Configurable {

    private List<DBMapperMaster> dbMapperMastersList;

    public List<DBMapperMaster> getDbMapperMastersList() {
        return dbMapperMastersList;
    }

    public void setDbMapperMastersList(List<DBMapperMaster> dbMapperMastersList) {
        this.dbMapperMastersList = dbMapperMastersList;
    }

}
