/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 1/5/2017
 * @author Aly.Shiha
 *
 * Model for file header template
 */
public class HeaderConversionTemplate extends BaseConverterTemplate {

    private int numberOfLines;
    private List<FieldsMaster> detailTemplatesList;

    public int getNumberOfLines() {
        return numberOfLines;
    }

    public void setNumberOfLines(int numberOfLines) {
        this.numberOfLines = numberOfLines;
    }

    public List<FieldsMaster> getDetailTemplatesList() {
        return detailTemplatesList;
    }

    public void setDetailTemplatesList(List<FieldsMaster> detailTemplatesList) {
        this.detailTemplatesList = detailTemplatesList;
    }

}
