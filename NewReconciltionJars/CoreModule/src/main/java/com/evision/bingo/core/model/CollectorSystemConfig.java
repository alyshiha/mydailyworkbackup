/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 20/4/2017
 * @author Khaled Khalil
 *
 * Model for system collectors configuration
 */
public class CollectorSystemConfig implements Configurable {

    private List<CollectorConfig> collectorConfigList;

    public List<CollectorConfig> getCollectorConfigList() {
        return collectorConfigList;
    }

    public void setCollectorConfigList(List<CollectorConfig> collectorConfigList) {
        this.collectorConfigList = collectorConfigList;
    }
}
