/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.utils;

import com.evision.bingo.core.MainRunner;
import com.evision.bingo.core.model.LoggerConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

/**
 * @since 20/4/2017
 * @author Khaled Khalil
 *
 * This is the main systemLogger class to log all messages received into
 * different files or event database tables
 */
public final class LogHandler {

    private Logger systemLogger;
    private final List<Logger> registeredLoggersList = new ArrayList<>();

    public LogHandler(List<LoggerConfig> loggerConfigList, LogHandler systemLogHandler) {
        systemLogger = systemLogHandler.systemLogger;
        if (loggerConfigList != null && loggerConfigList.size() > 0) {
            for (LoggerConfig loggerConfigRecord : loggerConfigList) {
                Logger logger = Logger.getLogger(UUID.randomUUID().toString());
                logger.setAdditivity(false);
                logger.addAppender(getRollingFileAppender(loggerConfigRecord, 1));
                registeredLoggersList.add(logger);
            }
        }
    }

    private RollingFileAppender getRollingFileAppender(LoggerConfig loggerConfig, int maxBackupIndex) {
        RollingFileAppender systemLoggerFileAppender = new RollingFileAppender();
        systemLoggerFileAppender.setName("");
        systemLoggerFileAppender.setFile(loggerConfig.getPath());
        systemLoggerFileAppender.setImmediateFlush(true);
        systemLoggerFileAppender.setMaxBackupIndex(maxBackupIndex);
        systemLoggerFileAppender.setBufferedIO(false);
        systemLoggerFileAppender.setBufferSize(1024);
        systemLoggerFileAppender.setMaxFileSize(loggerConfig.getSize());
        systemLoggerFileAppender.setLayout(new PatternLayout(MainRunner.SYSTEM_LOG_LAYOUT_PATTERN));
        systemLoggerFileAppender.setThreshold(Level.toLevel(loggerConfig.getLevel()));
        systemLoggerFileAppender.setAppend(true);
        systemLoggerFileAppender.activateOptions();
        return systemLoggerFileAppender;
    }

    public LogHandler(LoggerConfig systemLoggerConfig) {
        //setup the system systemLogger
        systemLogger = Logger.getRootLogger();
        systemLogger.setLevel(Level.ALL);
        systemLogger.setAdditivity(false);
        systemLogger.addAppender(getRollingFileAppender(systemLoggerConfig, 5));        
    }

    //observer pattern
    //collection of iLogger 
    public synchronized void logInfo(String message) {
        systemLogger.info(message);
        for (Logger logger : registeredLoggersList) {
            logger.info(message);
        }
    }

    public synchronized void logException(String message, Exception e) {
        if (e != null) {
            systemLogger.error(message, e);
        } else {
            systemLogger.error(message);
        }
        for (Logger logger : registeredLoggersList) {
            logger.error(message, e);
        }
    }
}
