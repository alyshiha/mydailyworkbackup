/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

/**
 * @since 20/4/2017
 * @author Khaled Khalil
 *
 * Model for queue
 */
public class Queue implements Configurable {

    public static enum QueueActionType {
        PRODUCE, CONSUME
    }

    private String name;
    private String key;
    private String action;
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
