/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 4/7/2017
 * @author Khaled Khalil
 *
 * Model for Transaction Rejected config
 */
public class RejectedConfig implements Configurable {

    private String name;
    private String classname;
    private List<Queue> queuesList;
    private List<LoggerConfig> loggerPathList;
    private String path;
    private String active;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public List<Queue> getQueuesList() {
        return queuesList;
    }

    public void setQueuesList(List<Queue> queuesList) {
        this.queuesList = queuesList;
    }

    public List<LoggerConfig> getLoggerPathList() {
        return loggerPathList;
    }

    public void setLoggerPathList(List<LoggerConfig> loggerPathList) {
        this.loggerPathList = loggerPathList;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
