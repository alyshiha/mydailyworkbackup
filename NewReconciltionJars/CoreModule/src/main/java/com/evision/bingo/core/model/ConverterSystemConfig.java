/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 17/5/2017
 * @author Khaled Khalil
 *
 * Model for system converters configuration
 */
public class ConverterSystemConfig implements Configurable {

    public List<ConverterConfig> getConverterConfigList() {
        return converterConfigList;
    }

    public void setConvertorConfigList(List<ConverterConfig> converterConfigList) {
        this.converterConfigList = converterConfigList;
    }
    private List<ConverterConfig> converterConfigList;
}
