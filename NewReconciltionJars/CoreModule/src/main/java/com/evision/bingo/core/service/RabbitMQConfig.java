/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.service;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @since 1/4/2017
 * @author Aly Shiha
 *
 * This is the main config class for the RabbitMQ
 */
public final class RabbitMQConfig {

    private static CachingConnectionFactory connectionFactory = null;

    private static void setConnectionFactoryInstance(String host, String username, String password) {
        if (connectionFactory == null) {
            connectionFactory = new CachingConnectionFactory(host);
            connectionFactory.setPublisherConfirms(true);
            connectionFactory.setPublisherReturns(true);
            connectionFactory.setUsername(username);
            connectionFactory.setPassword(password);
//            connectionFactory.setCacheMode(CachingConnectionFactory.CacheMode.CONNECTION);
        }
    }

    public static RabbitTemplate getRabbitTemplateInstance(String host, String username, String password) {

        setConnectionFactoryInstance(host, username, password);

        RabbitTemplate template = new RabbitTemplate(connectionFactory);
//        template.setMessageConverter(new JsonMessageConverter());
//        template.setMandatory(true); //make sure that we recieve a confirmation of messages sent
//        template.setReturnCallback(new RabbitTemplate.ReturnCallback() {
//            @Override
//            public void returnedMessage(Message msg, int i, String string, String string1, String string2) {
////                System.out.println(msg + " " + i + " " + string + " " + string1 + " " + string2);
//            }
//
//        });
//        template.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
//            @Override
//            public void confirm(CorrelationData cd, boolean bln, String string) {
////                 System.out.println(cd + " " + bln + " " + string);
//            }
//        });
        return template;
    }
}
