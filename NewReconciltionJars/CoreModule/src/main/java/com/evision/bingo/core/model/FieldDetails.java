/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

/**
 * @since 1/5/2017
 * @author Aly Shiha
 *
 */
public class FieldDetails {

    private String subStrings;
    private int subStringsIndex;
    private int line;
    private int beginIndex;
    private int endIndex;
    private String dataType;
    private String format;
    private String specialCharacter;

    public String getSubStrings() {
        return subStrings;
    }

    public void setSubStrings(String subStrings) {
        this.subStrings = subStrings;
    }

    public int getSubStringsIndex() {
        return subStringsIndex;
    }

    public void setSubStringsIndex(int subStringsIndex) {
        this.subStringsIndex = subStringsIndex;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public int getBeginIndex() {
        return beginIndex;
    }

    public void setBeginIndex(int beginIndex) {
        this.beginIndex = beginIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getSpecialCharacter() {
        return specialCharacter;
    }

    public void setSpecialCharacter(String specialCharacter) {
        this.specialCharacter = specialCharacter;
    }
}
