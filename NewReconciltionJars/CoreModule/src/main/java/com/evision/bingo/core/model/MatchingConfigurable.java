/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

/**
 * @since 30/7/2017
 * @author Khaled Khalil
 *
 * Interface for matching system configuration objects
 */
public interface MatchingConfigurable extends Configurable{
    
}
