/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

/**
 * @since 30/5/2017
 * @author Khaled Khalil
 *
 * Interface for backup system configuration objects
 */
public interface BackupConfigurable extends Configurable {

}
