/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 * Model for system db loader configuration
 */
public class DBLoaderConfig implements Configurable {

    private String name;
    private String dbConnection;
    private String mapper;
    private String classname;
    private String active;
    private String commitThreshold;
    private List<Queue> queuesList;
    private List<LoggerConfig> loggerPathList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDbConnection() {
        return dbConnection;
    }

    public void setDbConnection(String dbConnection) {
        this.dbConnection = dbConnection;
    }

    public String getMapper() {
        return mapper;
    }

    public void setMapper(String mapper) {
        this.mapper = mapper;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public List<Queue> getQueuesList() {
        return queuesList;
    }

    public void setQueuesList(List<Queue> queuesList) {
        this.queuesList = queuesList;
    }

    public List<LoggerConfig> getLoggerPathList() {
        return loggerPathList;
    }

    public void setLoggerPathList(List<LoggerConfig> loggerPathList) {
        this.loggerPathList = loggerPathList;
    }

    public String getCommitThreshold() {
        return commitThreshold;
    }

    public void setCommitThreshold(String commitThreshold) {
        this.commitThreshold = commitThreshold;
    }
}
