/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 * Model for system field db mapper detail configuration
 */
public class DBMapperDetail implements Configurable {

    private String database;
    private String field;

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
