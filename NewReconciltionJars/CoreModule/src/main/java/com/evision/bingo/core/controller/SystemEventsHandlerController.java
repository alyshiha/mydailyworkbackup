/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.controller;

import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.model.EventsHandlerConfig;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 *
 * @author Evision
 */
public class SystemEventsHandlerController extends BaseController {

    public SystemEventsHandlerController(EventsHandlerConfig eventHandlerConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<BaseController> eventsHandlerList) {
        super(new LogHandler(eventHandlerConfig.getLoggerPathList(), systemLogger), new QueueHandler(eventHandlerConfig.getQueuesList(), rabbitTemplatesMap), eventHandlerConfig, eventsHandlerList);

        init();
    }

    @Override
    public void init() {
    }

    @Override
    public boolean handleRequest(Object obj) {
        if (obj != null) {
            EventModel message = (EventModel) obj;
            ObjectMapper oMapper = new ObjectMapper();

            logHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_EVENT_LOG_EVENT_MESSAGE) + Defines.SYSTEM_SPACE
                    + LogMessages.getMessage(LogMessages.SYSTEM_EVENT_TIME) + Defines.SYSTEM_SPACE + message.getTime() + Defines.SYSTEM_SPACE
                    + LogMessages.getMessage(LogMessages.SYSTEM_EVENT_ACTION) + Defines.SYSTEM_SPACE + message.getAction() + Defines.SYSTEM_SPACE
                    + LogMessages.getMessage(LogMessages.SYSTEM_EVENT_ACTION_SOURCE) + Defines.SYSTEM_SPACE + message.getActionSource() + Defines.SYSTEM_SPACE
                    + LogMessages.getMessage(LogMessages.SYSTEM_EVENT_ACTION_DETAILS) + Defines.SYSTEM_SPACE + message.getActionDetails() + Defines.SYSTEM_SPACE
                    + LogMessages.getMessage(LogMessages.SYSTEM_EVENT_ACTION_STATUS) + Defines.SYSTEM_SPACE + message.getActionStatus());
            Map<String, Object> mappedEventModel = oMapper.convertValue(message, Map.class);

            String dateFields = "";
            for (Object key : mappedEventModel.keySet()) {
                if (mappedEventModel.get(key) != null && mappedEventModel.get(key).getClass().toString().toLowerCase().contains("date")) {
                    dateFields += key + ",";
                }
            }
            MessageProperties mProperties = new MessageProperties();
            mProperties.setHeader("dateFields", dateFields);

            queueHandler.sendEventMessage(mappedEventModel, mProperties);
        }
        return true;
    }

    @Override
    public void destroy() {
    }

    @Override
    public void stop() {
    }
}
