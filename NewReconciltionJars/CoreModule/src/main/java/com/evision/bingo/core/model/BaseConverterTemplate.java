/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

/**
 * @since 25/5/2017
 * @author Khaled Khalil
 *
 */
public class BaseConverterTemplate {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}