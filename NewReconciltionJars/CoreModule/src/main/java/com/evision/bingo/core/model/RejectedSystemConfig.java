/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 12/7/2017
 * @author Khaled Khalil
 *
 * Model for system transaction rejected configuration
 */
public class RejectedSystemConfig implements Configurable {

    private List<RejectedConfig> rejectedConfigList;

    public List<RejectedConfig> getRejectedConfigList() {
        return rejectedConfigList;
    }

    public void setRejectedConfigList(List<RejectedConfig> rejectedConfigList) {
        this.rejectedConfigList = rejectedConfigList;
    }
}
