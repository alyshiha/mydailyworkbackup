/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.model.EventsHandlerConfig;
import com.evision.bingo.core.model.Queue;
import com.evision.bingo.core.model.QueueConfig;
import com.evision.bingo.core.model.QueuingSystemConfig;
import com.evision.bingo.core.model.SystemConfig;
import com.evision.bingo.core.service.RabbitMQConfig;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 *
 * @author Evision
 */
public class SystemEventsHandlerRunner implements BaseModuleRunner {

    private final List<BaseController> eventHandlerControllers = new ArrayList<>();
    private final Map<String, RabbitTemplate> rabbitTemplatesMap = new HashMap<>();
    private LogHandler systemLogHandler;

    @Override
    public void run(SystemConfig systemConfig, LogHandler systemLogHandler, List<BaseController> eventsHandlerList) {
        //configure the queue engine
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_EVENT_HANDLER_SETUP_QUEUE));
        for (QueueConfig queueConfig : ((QueuingSystemConfig) systemConfig.getQueuingSystemConfig()).getQueuesList()) {
            rabbitTemplatesMap.put(queueConfig.getName(), RabbitMQConfig.getRabbitTemplateInstance(queueConfig.getHostname(), queueConfig.getUsername(), queueConfig.getPassword()));
        }

        this.systemLogHandler = systemLogHandler;

        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_EVENT_HANDLER_SETUP_QUEUE_COMPLETE));

        for (EventsHandlerConfig eventsHandlerConfig : systemConfig.getSystemEventsHandlerConfig().getEventHandlersList()) {
            if (eventsHandlerConfig.getActive() != null && !eventsHandlerConfig.getActive().isEmpty() && Defines.ACTIVE.TRUE.equals(Defines.ACTIVE.valueOf(eventsHandlerConfig.getActive().toUpperCase()))) {
                try {
                    Class<?> controllerClass = Class.forName(eventsHandlerConfig.getClassname());
                    systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_EVENT_HANDLER_SETUP));
                    Constructor<?> controllerConstructor = controllerClass.getConstructor(EventsHandlerConfig.class, Map.class, LogHandler.class, List.class);

                    BaseController eventsHandlerController = (BaseController) controllerConstructor.newInstance(eventsHandlerConfig, rabbitTemplatesMap, systemLogHandler, null);
                    Thread tempLocalThread = new Thread(eventsHandlerController);
                    tempLocalThread.start();
                    eventHandlerControllers.add(eventsHandlerController);
                } catch (InvocationTargetException | ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException ex) {
                    systemLogHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                }
            }
        }

        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_EVENT_HANDLER_SETUP_COMPLETE));
    }

    @Override
    public void setupListener(List<Queue> queuesListParam, BaseController controller, Map<String, RabbitTemplate> rabbitTemplatesMap, Configurable extra1, Configurable extra2) {
    }

    @Override
    public void stop() {
        for (BaseController cc : eventHandlerControllers) {
            systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.SYSTEM_EVENT_HANDLER_SHUTDOWN) + Defines.SYSTEM_SPACE + cc.toString());
            cc.stop();
        }
    }

    @Override
    public List<BaseController> getControllers() {
        return eventHandlerControllers;
    }
}
