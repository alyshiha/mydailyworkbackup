/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

/**
 * @since 22/6/2017
 * @author Khaled Khalil
 *
 * Interface for parser system configuration objects
 */
public interface ParserConfigurable extends Configurable {}
