/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @since 27/4/2017
 * @author Khaled Khalil
 *
 * This is the main handler for properties entries
 */
public final class MessagesHandler {

    private final static Properties prop = new Properties();

    public static void initMessagesHandler(LogHandler logHandler) throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = MessagesHandler.class.getClassLoader().getResourceAsStream(Defines.SYSTEM_MESSAGES_PROPOERTIES_PATH);
            if (inputStream != null) {
                prop.load(inputStream);
            }
        } catch (Exception e) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), e);
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    public synchronized static String getMessage(String key) {
        return prop.getProperty(key);
    }
}
