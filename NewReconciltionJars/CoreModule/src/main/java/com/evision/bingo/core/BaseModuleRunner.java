/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.model.Queue;
import com.evision.bingo.core.model.SystemConfig;
import com.evision.bingo.core.utils.LogHandler;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @since 20/4/2017
 * @author Khaled Khalil
 *
 * Abstract for modules runners
 */
public interface BaseModuleRunner {

    void run(SystemConfig systemConfig, LogHandler logger, List<BaseController> eventsHandlerList);

    void stop();

    List<BaseController> getControllers();

    void setupListener(List<Queue> queuesListParam, BaseController controller, Map<String, RabbitTemplate> rabbitTemplatesMap, Configurable extra1, Configurable extra2);
}