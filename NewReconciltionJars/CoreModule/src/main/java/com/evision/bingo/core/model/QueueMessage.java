/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.Date;
import java.util.Map;

/**
 * @since 1/4/2017
 * @author Aly Shiha
 *
 * Model for the messaging
 */
public class QueueMessage {

    private String runningfile;
    private String filetype;
    private Date loadingTime;
    private Date parsingTime;
    private String extension;
    private Map header;
    private String extraField1;
    private String extraField2;
    private String extraField3;

    public String getRunningfile() {
        return runningfile;
    }

    public void setRunningfile(String runningfile) {
        this.runningfile = runningfile;
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public Date getTime() {
        return loadingTime;
    }

    public void setTime(Date time) {
        this.loadingTime = time;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Map getHeader() {
        return header;
    }

    public void setHeader(Map header) {
        this.header = header;
    }

    public String getExtraField1() {
        return extraField1;
    }

    public void setExtraField1(String extraField1) {
        this.extraField1 = extraField1;
    }

    public String getExtraField2() {
        return extraField2;
    }

    public void setExtraField2(String extraField2) {
        this.extraField2 = extraField2;
    }

    public String getExtraField3() {
        return extraField3;
    }

    public void setExtraField3(String extraField3) {
        this.extraField3 = extraField3;
    }

    public Date getParsingTime() {
        return parsingTime;
    }

    public void setParsingTime(Date parsingTime) {
        this.parsingTime = parsingTime;
    }
}
