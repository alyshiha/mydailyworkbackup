/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.utils;

/**
 * @since 24/4/2017
 * @author Khaled Khalil
 *
 * Constants of the system
 */
public class Defines {
    
    public static final String SYSTEM_SPACE = " ";
    public static final String SYSTEM_CONFIG_PATH = "conf/bingo.conf";
    public static final String SYSTEM_MESSAGES_PROPOERTIES_PATH = "messages.properties";
    public static final String SYSTEM_FILE_SEPARATOR = "\\";
    public static final int DEFAULT_SYSTEM_CONCURRENT_QUEUES_CONSUMER = 1;
    public static final String DEFAULT_SYSTEM_LOG_LAYOUT_PATTERN = "%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n";

    public enum ACTIVE {
        TRUE, FALSE;
    }
}
