/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 12/6/2017
 * @author Khaled Khalil
 *
 * Model for system db loader configuration
 */
public class DBLoaderSystemConfig implements Configurable {

    private List<DBLoaderConfig> dbLoaderConfigList;

    public List<DBLoaderConfig> getDbLoaderConfigList() {
        return dbLoaderConfigList;
    }

    public void setDbLoaderConfigList(List<DBLoaderConfig> dbLoaderConfigList) {
        this.dbLoaderConfigList = dbLoaderConfigList;
    }
}
