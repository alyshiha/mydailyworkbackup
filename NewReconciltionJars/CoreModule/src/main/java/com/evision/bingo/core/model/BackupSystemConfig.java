/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 30/5/2017
 * @author Khaled Khalil
 *
 * Model for system backup configuration
 */
public class BackupSystemConfig implements Configurable {

    private List<BackupConfig> backupConfigList;

    public List<BackupConfig> getBackupConfigList() {
        return backupConfigList;
    }

    public void setBackupConfigList(List<BackupConfig> backupConfigList) {
        this.backupConfigList = backupConfigList;
    }
}
