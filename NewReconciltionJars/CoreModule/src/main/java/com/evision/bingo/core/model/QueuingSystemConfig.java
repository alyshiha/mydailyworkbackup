/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 20/4/2017
 * @author Khaled Khalil
 *
 * Model for system queues configuration
 */
public class QueuingSystemConfig implements Configurable {

    public List<QueueConfig> getQueuesList() {
        return queuesList;
    }

    public void setQueuesList(List<QueueConfig> queuesList) {
        this.queuesList = queuesList;
    }

    private List<QueueConfig> queuesList;
}
