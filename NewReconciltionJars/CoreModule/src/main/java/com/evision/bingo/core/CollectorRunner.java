/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.CollectorConfig;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.model.CollectorConfigurable;
import com.evision.bingo.core.model.CollectorSystemConfig;
import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.model.Queue;
import com.evision.bingo.core.model.QueuingSystemConfig;
import com.evision.bingo.core.model.QueueConfig;
import com.evision.bingo.core.model.SystemConfig;
import com.evision.bingo.core.service.RabbitMQConfig;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @since 1/4/2017
 * @author Aly Shiha
 *
 * This is the collector runner class that reads configurations of the
 * collectors and get them loaded
 */
public class CollectorRunner implements BaseModuleRunner {

    private final List<BaseController> collectorControllers = new ArrayList<>();
    private final Map<String, RabbitTemplate> rabbitTemplatesMap = new HashMap<>();
    private LogHandler systemLogHandler;

    @Override
    public void run(SystemConfig systemConfig, LogHandler systemLogHandler, List<BaseController> eventsHandlerList) {
        //configure the queue engine
        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SETUP_QUEUE));
        for (QueueConfig queueConfig : ((QueuingSystemConfig) systemConfig.getQueuingSystemConfig()).getQueuesList()) {
            rabbitTemplatesMap.put(queueConfig.getName(), RabbitMQConfig.getRabbitTemplateInstance(queueConfig.getHostname(), queueConfig.getUsername(), queueConfig.getPassword()));
        }

        this.systemLogHandler = systemLogHandler;

        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SETUP_QUEUE_SETUP_COMPLETE));

        for (CollectorConfig collectorConfig : ((CollectorSystemConfig) systemConfig.getCollectorSystemConfig()).getCollectorConfigList()) {
            if (collectorConfig.getActive() != null && !collectorConfig.getActive().isEmpty() && Defines.ACTIVE.TRUE.equals(Defines.ACTIVE.valueOf(collectorConfig.getActive().toUpperCase()))) {
                try {
                    Class<?> controllerClass = Class.forName(collectorConfig.getClassname());
                    systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SETUP_COLLECTOR) + Defines.SYSTEM_SPACE + controllerClass.toString());
                    Constructor<?> controllerConstructor = controllerClass.getConstructor(CollectorConfig.class, CollectorConfigurable.class, Map.class, LogHandler.class, List.class);

                    List<CollectorConfigurable> configList = (List<CollectorConfigurable>) XStreamTranslator.getInstance().toObject(new File(collectorConfig.getPath()));
                    for (CollectorConfigurable configRecord : configList) {
                        BaseController collectorController = (BaseController) controllerConstructor.newInstance(collectorConfig, configRecord, rabbitTemplatesMap, systemLogHandler, eventsHandlerList);
                        Thread tempLocalThread = new Thread(collectorController);
                        tempLocalThread.start();
                        collectorControllers.add(collectorController);
                    }
                } catch (IOException | ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    systemLogHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                }
            }
        }

        systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SETUP_COLLECTOR_COMPLETE));
    }

    @Override
    public void setupListener(List<Queue> queuesListParam, BaseController controller, Map<String, RabbitTemplate> rabbitTemplatesMap, Configurable extra1, Configurable extra2) {
    }

    @Override
    public void stop() {
        for (BaseController cc : collectorControllers) {
            systemLogHandler.logInfo(LogMessages.getMessage(LogMessages.COLLECTOR_SHUTDOWN_COLLECTOR) + Defines.SYSTEM_SPACE + cc.toString());
            cc.stop();
        }
    }

    @Override
    public List<BaseController> getControllers() {
        return collectorControllers;
    }
}
