/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

/**
 * @since 20/4/2017
 * @author Khaled Khalil
 *
 * Interface for converters system configuration objects
 */
public interface ConverterConfigurable extends Configurable {

    public String getTemplateClassname();

    public void setTemplate(BaseConverterTemplate baseConverterTemplate);

    public String getPath();
}
