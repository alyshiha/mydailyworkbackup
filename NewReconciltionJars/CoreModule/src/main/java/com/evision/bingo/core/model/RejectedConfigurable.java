/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

import java.util.List;

/**
 * @since 12/7/2017
 * @author Khaled Khalil
 *
 * Interface for rejected system configuration objects
 */
public interface RejectedConfigurable extends Configurable {

    public void setFileType(String fileType);

    public String getFileType();

    public void setFileTemplate(String fileTemplate);

    public String getFileTemplate();

    public void setRejectedSetsList(List<BaseRejectedSet> rejectedSetsList);

    public List<BaseRejectedSet> getRejectedSetsList();
}
