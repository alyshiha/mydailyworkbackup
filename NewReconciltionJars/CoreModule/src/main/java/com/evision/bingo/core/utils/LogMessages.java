/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.utils;

/**
 * @since 20/4/2017
 * @author Khaled Khalil
 *
 * This is the text of all messages through to the log file in case of any
 * change of languages needed
 */
public class LogMessages {

    public static final String EXCEPTION_PREFIX = "exception.prefix";

    //System
    public static final String SYSTEM_START = "system.start";
    public static final String SYSTEM_START_COMPLETE = "system.start_complete";
    public static final String SYSTEM_SHUTDOWN = "system.shutdown";
    public static final String SYSTEM_SHUTDOWN_COMPLETE = "system.shutdown_complete";
    public static final String SYSTEM_LOAD_SYSTEM_CONFIGURATION = "system.load_system_configuration";
    public static final String SYSTEM_LOAD_SYSTEM_CONFIGURATION_COMPLETE = "system.load_system_configuration_complete";
    public static final String SYSTEM_LOAD_SYSTEM_MESSAGES = "system.load_system_messages";
    public static final String SYSTEM_LOAD_SYSTEM_MESSAGES_COMPLETE = "system.load_system_messages_complete";
    public static final String SYSTEM_EVENT_HANDLER_START = "system.start_event_handler";
    public static final String SYSTEM_EVENT_HANDLER_START_COMPLETE = "system.start_event_handler_complete";
    public static final String SYSTEM_EVENT_HANDLER_SETUP = "system.setup_event_handler";
    public static final String SYSTEM_EVENT_HANDLER_SETUP_COMPLETE = "system.setup_event_handler_complete";
    public static final String SYSTEM_EVENT_HANDLER_SETUP_QUEUE = "system.setup_event_handler_queue";
    public static final String SYSTEM_EVENT_HANDLER_SETUP_QUEUE_COMPLETE = "system.setup_event_handler_queue_complete";
    public static final String SYSTEM_EVENT_HANDLER_SHUTDOWN = "system.shutdown_events_handler";
    public static final String SYSTEM_EVENT_HANDLER_SHUTDOWN_COMPLETE = "system.shutdown_events_handler_complete";
    public static final String SYSTEM_EVENT_LOG_EVENT_MESSAGE = "system.log_event_message";
    public static final String SYSTEM_EVENT_TIME = "system.event_time";
    public static final String SYSTEM_EVENT_ACTION = "system.event_action";
    public static final String SYSTEM_EVENT_ACTION_SOURCE = "system.event_action_source";
    public static final String SYSTEM_EVENT_ACTION_DETAILS = "system.event_action_details";
    public static final String SYSTEM_EVENT_ACTION_STATUS = "system.event_action_status";

    //collector
    public static final String COLLECTOR_START = "collector.start";
    public static final String COLLECTOR_START_COMPLETE = "collector.start_complete";
    public static final String COLLECTOR_SHUTDOWN = "collector.shutdown";
    public static final String COLLECTOR_SHUTDOWN_COMPLETE = "collector.shutdown_complete";
    public static final String COLLECTOR_SETUP_QUEUE = "collector.setup_queue";
    public static final String COLLECTOR_SETUP_QUEUE_SETUP_COMPLETE = "collector.setup_queue_complete";
    public static final String COLLECTOR_SETUP_COLLECTOR = "collector.setup_collector";
    public static final String COLLECTOR_SETUP_COLLECTOR_COMPLETE = "collector.setup_collector_complete";
    public static final String COLLECTOR_SHUTDOWN_COLLECTOR = "collector.shutdown_collector";
    public static final String COLLECTOR_START_LISTENING = "collector.start_listening";
    public static final String COLLECTOR_STOP_LISTENING = "collector.stop_listening";
    public static final String COLLECTOR_LOCAL_FOLDER_REGISTER_PATH = "collector.register_local_path";
    public static final String COLLECTOR_SMB_FOLDER_REGISTER_PATH = "collector.register_smb_path";
    public static final String COLLECTOR_SFTP_REGISTER = "collector.register_sftp";
    public static final String COLLECTOR_SFTP_CONNECT = "collector.sftp_connect";
    public static final String COLLECTOR_SFTP_DISCONNECT = "collector.sftp_disconnect";
    public static final String COLLECTOR_SFTPKEY_REGISTER = "collector.register_sftpkey";
    public static final String COLLECTOR_SFTPKEY_CONNECT = "collector.sftpkey_connect";
    public static final String COLLECTOR_SFTPKEY_DISCONNECT = "collector.sftpkey_disconnect";
    public static final String COLLECTOR_LOCAL_FOLDER_NEW_FILE_FOUND = "collector.local_folder_new_file_found";
    public static final String COLLECTOR_LOCAL_FOLDER_MOVE_FILE = "collector.local_folder_move_file";
    public static final String COLLECTOR_LOCAL_FOLDER_ADD_FILE_TO_QUEUES = "collector.local_folder_add_to_queues";
    public static final String COLLECTOR_SMB_CONNECT = "collector.smb_connect";
    public static final String COLLECTOR_SMB_NEW_FILE_FOUND = "collector.smb_new_file_found";
    public static final String COLLECTOR_SMB_MOVE_FILE = "collector.smb_move_file";
    public static final String COLLECTOR_SMB_ADD_FILE_TO_QUEUES = "collector.smb_add_to_queues";
    public static final String COLLECTOR_SFTP_NEW_FILE_FOUND = "collector.sftp_new_file_found";
    public static final String COLLECTOR_SFTP_COPY_FILE = "collector.sftp_copy_file";
    public static final String COLLECTOR_SFTP_ADD_FILE_TO_QUEUES = "collector.sftp_add_to_queues";
    public static final String COLLECTOR_SFTPKEY_NEW_FILE_FOUND = "collector.sftpkey_new_file_found";
    public static final String COLLECTOR_SFTPKEY_COPY_FILE = "collector.sftpkey_copy_file";
    public static final String COLLECTOR_SFTPKEY_ADD_FILE_TO_QUEUES = "collector.sftpkey_add_to_queues";
    public static final String COLLECTOR_EXISTING_FILE_IN_DISTINATION_FOLDER = "collector.existing_file_in_distination_folder";
    public static final String COLLECTOR_EXISTING_FILE_IN_SOURCE_FOLDER = "collector.exisitng_file_in_source_folder";

    //converter
    public static final String CONVERTER_START = "converter.start";
    public static final String CONVERTER_START_COMPLETE = "converter.start_complete";
    public static final String CONVERTER_SHUTDOWN = "converter.shutdown";
    public static final String CONVERTER_SHUTDOWN_COMPLETE = "converter.shutdown_complete";
    public static final String CONVERTER_SETUP_QUEUE = "converter.setup_queue";
    public static final String CONVERTER_SETUP_QUEUE_COMPLETE = "converter.setup_queue_complete";
    public static final String CONVERTER_SETUP_CONVERTER = "converter.converter_setup";
    public static final String CONVERTER_SETUP_CONVERTER_COMPLETE = "converter.converter_setup_complete";
    public static final String CONVERTER_SETUP_QUEUES_LISTENERS = "converter.setup_queues_listeners";
    public static final String CONVERTER_SETUP_QUEUES_LISTENERS_COMPLETE = "converter.setup_queues_listeners_complete";
    public static final String CONVERTER_SHUTDOWN_CONVERTER = "converter.shutdown_converter";
    public static final String CONVERTER_CONVERT_EXCEL_FILE = "converter.convert_excel_file";
    public static final String CONVERTER_FAILED_TO_CREATE_VBS_FILE = "converter.failed_to_create_vbs_file";
    public static final String CONVERTER_CONVERT_ACCESS_FILE = "converter.convert_access_file";
    public static final String CONVERTER_FILE_NOT_DELETED = "converter.file_not_deleted";
    public static final String CONVERTER_HEADER_NOT_FOUND = "converter.header_not_found";
    public static final String CONVERTER_CONVERT_UNSTRUCTURED_FILE = "converter.convert_unstructured_file";
    public static final String CONVERTER_PASS_FILE = "converter.pass_file";
    public static final String CONVERTER_CONVERT_HEADER_FILE = "converter.convert_header_file";
    public static final String CONVERTER_EXISTING_FILE_IN_DISTINATION_FOLDER = "converter.existing_file_in_distination_folder";
    public static final String CONVERTER_EXISTING_FOLDER_IN_DISTINATION_FOLDER = "converter.existing_folder_in_distination_folder";

    //backup
    public static final String BACKUP_START = "backup.start";
    public static final String BACKUP_START_COMPLETE = "backup.start_complete";
    public static final String BACKUP_SHUTDOWN = "backup.shutdown";
    public static final String BACKUP_SHUTDOWN_COMPLETE = "backup.shutdown_complete";
    public static final String BACKUP_SHUTDOWN_BACKUP = "backup.shutdown_backup";
    public static final String BACKUP_SETUP_QUEUE = "backup.setup_queue";
    public static final String BACKUP_SETUP_QUEUE_COMPLETE = "backup.setup_queue_complete";
    public static final String BACKUP_SETUP_BACKUP = "backup.backup_setup";
    public static final String BACKUP_SETUP_BACKUP_COMPLETE = "backup.backup_setup_complete";
    public static final String BACKUP_COPY_SOURCE_CONTENTS_TO_DETINATION = "backup.copy_source_contents_to_destination";
    public static final String BACKUP_SETUP_QUEUES_LISTENERS = "backup.setup_queues_listeners";
    public static final String BACKUP_SETUP_QUEUES_LISTENERS_COMPLETE = "backup.setup_queues_listeners_complete";

    //db loader
    public static final String DBLOADER_START = "dbloader.start";
    public static final String DBLOADER_START_COMPLETE = "dbloader.start_complete";
    public static final String DBLOADER_SHUTDOWN = "dbloader.shutdown";
    public static final String DBLOADER_SHUTDOWN_COMPLETE = "dbloader.shutdown_complete";
    public static final String DBLOADER_SETUP_QUEUE = "dbloader.setup_queue";
    public static final String DBLOADER_SETUP_QUEUE_COMPLETE = "dbloader.setup_queue_complete";
    public static final String DBLOADER_SETUP_DBLOADER = "dbloader.setup_dbloader";
    public static final String DBLOADER_SHUTDOWN_DBLOADER = "dbloader.shutdown_dbloader";
    public static final String DBLOADER_SETUP_DBLOADER_COMPLETE = "dbloader.dbloader_setup_complete";
    public static final String DBLOADER_SETUP_QUEUES_LISTENERS = "dbloader.setup_queues_listeners";
    public static final String DBLOADER_SETUP_QUEUES_LISTENERS_COMPLETE = "dbloader.setup_queues_listeners_complete";
    public static final String DBLOADER_INVALID_DB_CONNECTION = "dbloader.invalid_db_connection";
    public static final String DBLOADER_INVALID_DB_MAPPER = "dbloader.invalid_db_mapper";
    public static final String DBLOADER_SUBMIT_RECORDS = "dbloader.submit_records";
    public static final String DBLOADER_LOAD_RECORD_IN_MEMORY = "dbloader.load_record_in_memory";

    //parser
    public static final String PARSER_START = "parser.start";
    public static final String PARSER_START_COMPLETE = "parser.start_complete";
    public static final String PARSER_SHUTDOWN = "parser.shutdown";
    public static final String PARSER_SHUTDOWN_COMPLETE = "parser.shutdown_complete";
    public static final String PARSER_SETUP_QUEUE = "parser.setup_queue";
    public static final String PARSER_SETUP_QUEUE_COMPLETE = "parser.setup_queue_complete";
    public static final String PARSER_SETUP_PARSER = "parser.setup_parser";
    public static final String PARSER_SHUTDOWN_PARSER = "parser.shutdown_parser";
    public static final String PARSER_SETUP_PARSER_COMPLETE = "parser.parser_setup_complete";
    public static final String PARSER_SETUP_QUEUES_LISTENERS = "parser.setup_queues_listeners";
    public static final String PARSER_SETUP_QUEUES_LISTENERS_COMPLETE = "parser.setup_queues_listeners_complete";
    public static final String PARSER_FAIL_IN_READING_MAPPER_FILE = "parser.fail_reading_mapper_file";
    public static final String PARSER_NO_STREAM_MAPPING = "parser.no_stream_mapping";
    public static final String PARSER_PARSE_FILE = "parser.parse_file";

    //transaction validation
    public static final String VALIDATION_START = "validation.start";
    public static final String VALIDATION_START_COMPLETE = "validation.start_complete";
    public static final String VALIDATION_SHUTDOWN = "validation.shutdown";
    public static final String VALIDATION_SHUTDOWN_COMPLETE = "validation.shutdown_complete";
    public static final String VALIDATION_SETUP_QUEUE = "validation.setup_queue";
    public static final String VALIDATION_SETUP_QUEUE_COMPLETE = "validation.setup_queue_complete";
    public static final String VALIDATION_SETUP_TRANSACTION_VALIDATION = "validation.setup_transactionvalidation";
    public static final String VALIDATION_SHUTDOWN_TRANSACTION_VALIDATION = "validation.shutdown_transactionvalidation";
    public static final String VALIDATION_SETUP_TRANSACTION_VALIDATION_COMPLETE = "validation.transactionvalidation_setup_complete";
    public static final String VALIDATION_SETUP_QUEUES_LISTENERS = "validation.setup_queues_listeners";
    public static final String VALIDATION_SETUP_QUEUES_LISTENERS_COMPLETE = "validation.setup_queues_listeners_complete";
    public static final String VALIDATION_VALIDATE_TRANSACTION_AGAINST_VALIDATION_SET = "validation.validate_transaction_against_validationset";

    //transaction dispute
    public static final String DISPUTE_START = "dispute.start";
    public static final String DISPUTE_START_COMPLETE = "dispute.start_complete";
    public static final String DISPUTE_SHUTDOWN = "dispute.shutdown";
    public static final String DISPUTE_SHUTDOWN_COMPLETE = "dispute.shutdown_complete";
    public static final String DISPUTE_SETUP_QUEUE = "dispute.setup_queue";
    public static final String DISPUTE_SETUP_QUEUE_COMPLETE = "dispute.setup_queue_complete";
    public static final String DISPUTE_SETUP_TRANSACTION_DISPUTE = "dispute.setup_transactiondispute";
    public static final String DISPUTE_SHUTDOWN_TRANSACTION_DISPUTE = "dispute.shutdown_transactiondispute";
    public static final String DISPUTE_SETUP_TRANSACTION_DISPUTE_COMPLETE = "dispute.transactiondispute_setup_complete";
    public static final String DISPUTE_SETUP_QUEUES_LISTENERS = "dispute.setup_queues_listeners";
    public static final String DISPUTE_SETUP_QUEUES_LISTENERS_COMPLETE = "dispute.setup_queues_listeners_complete";
    public static final String DISPUTE_VALIDATE_TRANSACTION_AGAINST_DISPUTE_SET = "dispute.validate_transaction_against_disputeset";

    //rejected transaction
    public static final String REJECTED_START = "rejected.start";
    public static final String REJECTED_START_COMPLETE = "rejected.start_complete";
    public static final String REJECTED_SHUTDOWN = "rejected.shutdown";
    public static final String REJECTED_SHUTDOWN_COMPLETE = "rejected.shutdown_complete";
    public static final String REJECTED_SETUP_QUEUE = "rejected.setup_queue";
    public static final String REJECTED_SETUP_QUEUE_COMPLETE = "rejected.setup_queue_complete";
    public static final String REJECTED_SETUP_TRANSACTION_REJECTED = "rejected.setup_transactionrejected";
    public static final String REJECTED_SHUTDOWN_TRANSACTION_REJECTED = "rejected.shutdown_transactionrejected";
    public static final String REJECTED_SETUP_TRANSACTION_REJECTED_COMPLETE = "rejected.transactionrejected_setup_complete";
    public static final String REJECTED_SETUP_QUEUES_LISTENERS = "rejected.setup_queues_listeners";
    public static final String REJECTED_SETUP_QUEUES_LISTENERS_COMPLETE = "rejected.setup_queues_listeners_complete";
    public static final String REJECTED_VALIDATE_TRANSACTION_AGAINST_REJECTED_SET = "rejected.validate_transaction_against_rejectedset";

    //matching
    public static final String MATCHING_START = "matching.start";
    public static final String MATCHING_START_COMPLETE = "matching.start_complete";
    public static final String MATCHING_SHUTDOWN = "matching.shutdown";
    public static final String MATCHING_SHUTDOWN_COMPLETE = "matching.shutdown_complete";
    public static final String MATCHING_SHUTDOWN_MATCHING = "matching.shutdown_matching";
    public static final String MATCHING_SETUP_QUEUE = "matching.setup_queue";
    public static final String MATCHING_SETUP_QUEUE_COMPLETE = "matching.setup_queue_complete";
    public static final String MATCHING_SETUP_MATCHING = "matching.matching_setup";
    public static final String MATCHING_SETUP_MATCHING_COMPLETE = "matching.matching_setup_complete";
    public static final String MATCHING_COPY_SOURCE_CONTENTS_TO_DETINATION = "matching.copy_source_contents_to_destination";
    public static final String MATCHING_SETUP_QUEUES_LISTENERS = "matching.setup_queues_listeners";
    public static final String MATCHING_SETUP_QUEUES_LISTENERS_COMPLETE = "matching.setup_queues_listeners_complete";

    public synchronized static String getMessage(String key) {
        return MessagesHandler.getMessage(key);
    }
}
