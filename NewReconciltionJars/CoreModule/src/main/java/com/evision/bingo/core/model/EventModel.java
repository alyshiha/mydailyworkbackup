/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.core.model;

/**
 * @since 30/5/2017
 * @author Khaled Khalil
 *
 * This is the event model for event information
 */
public class EventModel {

    public String getActionSource() {
        return actionSource;
    }

    public void setActionSource(String actionSource) {
        this.actionSource = actionSource;
    }

    public enum EVENT {
        FILE_COLLECT("File Collect"), FILE_CONVERT("File Convert"), FILE_BACKUP("File Backup"), FILE_PARSE(" File Parse");

        private String event;

        EVENT(String event) {
            this.event = event;
        }

        @Override
        public String toString() {
            return event;
        }
    };

    public enum STATUS {
        SUCCESS("Success"), FAIL("Fail");
        private String status;

        STATUS(String status) {
            this.status = status;
        }

        @Override
        public String toString() {
            return status;
        }
    }

    private String time;
    private EVENT action;
    private String actionSource;
    private String actionDetails;
    private STATUS actionStatus = STATUS.FAIL;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public EVENT getAction() {
        return action;
    }

    public void setAction(EVENT action) {
        this.action = action;
    }

    public String getActionDetails() {
        return actionDetails;
    }

    public void setActionDetails(String actionDetails) {
        this.actionDetails = actionDetails;
    }

    public STATUS getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(STATUS actionStatus) {
        this.actionStatus = actionStatus;
    }
}
