/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.controller;

import com.evision.bingo.core.model.ConverterConfig;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.converter.service.ExcelConvertService;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.model.Extension;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.JsonMessageConverter;

/**
 * @since 1/5/2017
 * @author Aly Shiha
 *
 * This is the controller to convert excel files
 */
public final class ExcelConverterController extends BaseConverterController {

    public ExcelConverterController(ConverterConfig converterConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<Configurable> subConfigList, List<BaseController> eventsHandlerList) {
        super(new LogHandler(converterConfig.getLoggerPathList(), systemLogger), new QueueHandler(converterConfig.getQueuesList(), rabbitTemplatesMap), converterConfig, subConfigList, eventsHandlerList);
    }

    @Override
    synchronized boolean doCollect() {
        if (getObj() != null) {
            JsonMessageConverter jsonConverter = new JsonMessageConverter();
            QueueMessage message = (QueueMessage) jsonConverter.fromMessage((Message)getObj());

            ExcelConvertService excelConvertService = new ExcelConvertService(queueHandler, logHandler, eventsHandlerList);
            //check supported extensions
            List<Extension> extensionsList = ((ConverterConfig) config).getExtensionsList();
            boolean isSupportedExtension = false;
            for (Extension ext : extensionsList) {
                if (message.getExtension().toLowerCase().equals(ext.getExtension().toLowerCase())) {
                    isSupportedExtension = true;
                    break;
                }
            }
            if (isSupportedExtension) { //supported extensions then convert
                if (excelConvertService.convert(message)) { //succeeded conversion
                    excelConvertService.clear();
                    queueHandler.sendMessage(message);
                    return true;
                } else {
                    excelConvertService.clear();
                }
            }
        }
        return false;
    }

    @Override
    public void stop() {
        /// do nothing
    }
}
