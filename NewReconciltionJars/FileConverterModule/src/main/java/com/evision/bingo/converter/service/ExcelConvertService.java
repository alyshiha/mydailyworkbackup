/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.service;

import com.evision.bingo.converter.utils.ConverterDefines;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.core.utils.Utils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @since 1/5/2017
 * @author Aly Shiha
 *
 */
public class ExcelConvertService extends BaseConvertService {

    private File tempFile;

    public ExcelConvertService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, eventsHandlerList);
    }

    @Override
    public void clear() {
        if (tempFile != null) {
            tempFile.deleteOnExit();
        }
    }

    @Override
    public boolean convert(QueueMessage message) {
        logHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_CONVERT_EXCEL_FILE) + Defines.SYSTEM_SPACE + message.getRunningfile());

        Calendar cal = Calendar.getInstance();
        Date time = cal.getTime();

        EventModel eventModel = new EventModel();
        eventModel.setTime(new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(time));
        eventModel.setAction(EventModel.EVENT.FILE_CONVERT);
        eventModel.setActionDetails(message.getRunningfile());
        eventModel.setActionSource(ConverterDefines.QUEUE_ACTION_SOURCE);

        if (!createVBSFile()) {
            logHandler.logException(LogMessages.getMessage(LogMessages.CONVERTER_FAILED_TO_CREATE_VBS_FILE) + Defines.SYSTEM_SPACE + message.getRunningfile(), null);
            eventModel.setActionStatus(EventModel.STATUS.FAIL);
            submitEvent(eventModel);
            return false;
        } else {
            try {
                File directory = new File(Utils.removeFileExtension(message.getRunningfile()) + Defines.SYSTEM_FILE_SEPARATOR);
                if (directory.exists()) {
                    logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(LogMessages.getMessage(LogMessages.CONVERTER_EXISTING_FOLDER_IN_DISTINATION_FOLDER) + Defines.SYSTEM_SPACE + directory.getName()));
                    eventModel.setActionStatus(EventModel.STATUS.FAIL);
                } else {
                    directory.mkdirs();

                    String processcomm = "\"" + System.getenv("SystemRoot") + Defines.SYSTEM_FILE_SEPARATOR + "SysWOW64" + Defines.SYSTEM_FILE_SEPARATOR + "cscript.exe\" \"" + tempFile.getAbsolutePath() + "\" \"" + message.getRunningfile() + "\" \"" + directory.getPath() + Defines.SYSTEM_FILE_SEPARATOR + "\"";
                    Process process = Runtime.getRuntime().exec(processcomm);
                    process.waitFor();

                    //delete the excel file as not needed any more
                    File existingFile = new File(message.getRunningfile());
                    boolean isDeleted = existingFile.delete();
                    if (!isDeleted) {
                        logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(LogMessages.CONVERTER_FILE_NOT_DELETED));
                    }

                    message.setTime(time);
                    message.setRunningfile(directory.getPath());
                    message.setExtension("csv");

                    eventModel.setActionStatus(EventModel.STATUS.SUCCESS);
                }
            } catch (IOException | InterruptedException ex) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
                eventModel.setActionStatus(EventModel.STATUS.FAIL);
            } finally {
                submitEvent(eventModel);
                if (eventModel.getActionStatus().equals(EventModel.STATUS.SUCCESS)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    private boolean createVBSFile() {
        BufferedWriter bw = null;
        try {
            //create a temp file
            tempFile = File.createTempFile(ConverterDefines.CONVERTER_EXCEL_TEMP_FILE_NAME, ".vbs");
            //write it
            bw = new BufferedWriter(new FileWriter(tempFile));
            bw.write(ConverterDefines.CONVERTER_EXCEL_VBS_CODE);
            bw.close();
        } catch (IOException ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
            return false;
        } finally {
            try {
                bw.close();
            } catch (IOException ex) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
            }
        }
        return true;
    }
}
