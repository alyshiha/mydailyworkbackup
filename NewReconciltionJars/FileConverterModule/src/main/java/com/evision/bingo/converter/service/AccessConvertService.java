/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.service;

import com.evision.bingo.converter.utils.ConverterDefines;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.core.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import net.ucanaccess.jdbc.UcanaccessConnection;

/**
 * @since 1/5/2017
 * @author Aly Shiha
 *
 */
public class AccessConvertService extends BaseConvertService {

    private Connection connection;
    private Statement statement;
    private List<String> tableNames = new ArrayList();
    private String fileName = "";

    public AccessConvertService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, eventsHandlerList);
    }

    @Override
    public boolean convert(QueueMessage message) {
        logHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_CONVERT_ACCESS_FILE) + Defines.SYSTEM_SPACE + message.getRunningfile());

        Calendar cal = Calendar.getInstance();
        Date time = cal.getTime();

        EventModel eventModel = new EventModel();
        eventModel.setTime(new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(time));
        eventModel.setAction(EventModel.EVENT.FILE_CONVERT);
        eventModel.setActionDetails(message.getRunningfile());
        eventModel.setActionSource(ConverterDefines.QUEUE_ACTION_SOURCE);

        try {
            File directory = new File(Utils.removeFileExtension(message.getRunningfile()) + Defines.SYSTEM_FILE_SEPARATOR);
            if (directory.exists()) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(LogMessages.getMessage(LogMessages.CONVERTER_EXISTING_FOLDER_IN_DISTINATION_FOLDER) + Defines.SYSTEM_SPACE + directory.getName()));
                eventModel.setActionStatus(EventModel.STATUS.FAIL);
            } else {
                directory.mkdirs();

                //Loading or registering Oracle JDBC driver class
                Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
                //Create and get connection using DriverManager class
                connection = DriverManager.getConnection("jdbc:ucanaccess://" + message.getRunningfile() + ";memory=false");

                //get all table names
                DatabaseMetaData m = connection.getMetaData();
                ResultSet tables = m.getTables(null, null, "%", new String[]{"TABLE"});
                while (tables.next()) {
                    tableNames.add(tables.getString("TABLE_NAME"));
                }

                //Creating JDBC Statement 
                statement = connection.createStatement();

                //loop for tables and get the result
                for (String tablename : tableNames) {
                    ResultSet resultSet = statement.executeQuery(ConverterDefines.ACCESS_TABLE_QUERY + tablename);

                    //write to file
                    ResultSetMetaData metaData = resultSet.getMetaData();
                    int columns = metaData.getColumnCount();
                    PrintWriter writer = new PrintWriter(directory.getPath() + Defines.SYSTEM_FILE_SEPARATOR + tablename + ".csv");
                    while (resultSet.next()) {
                        for (int i = 1; i <= columns; ++i) {
                            writer.print(resultSet.getString(i) + ConverterDefines.ACCESS_FIELDS_SEPARATOR);
                        }
                        writer.println();
                    }
                    writer.close();

                    resultSet.close();
                }

                fileName = message.getRunningfile();

                message.setRunningfile(directory.getPath());
                message.setExtension("csv");
                message.setTime(time);

                eventModel.setActionStatus(EventModel.STATUS.SUCCESS);
            }
        } catch (IOException | ClassNotFoundException | SQLException ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
            eventModel.setActionStatus(EventModel.STATUS.FAIL);
        } finally {
            submitEvent(eventModel);
            if (eventModel.getActionStatus().equals(EventModel.STATUS.SUCCESS)) {
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public void clear() {
        if (null != connection) {//close connections
            try {
                // cleanup resources, once after processing
                statement.close();

                // and then finally close connection
                connection.close();
                ((UcanaccessConnection) connection).unloadDB();
            } catch (SQLException ex) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
            }
            //delete the access file as not needed any more
            File existingFile = new File(fileName);
            boolean isDeleted = existingFile.delete();
            if (!isDeleted) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(LogMessages.CONVERTER_FILE_NOT_DELETED));
            }
        }
    }
}
