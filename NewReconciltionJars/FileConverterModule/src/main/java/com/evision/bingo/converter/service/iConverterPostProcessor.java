/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.service;

import com.evision.bingo.core.controller.BaseController;

/**
 * @since 17/5/2017
 * @author Khaled Khalil
 *
 * base interface for post-processors
 */
public interface iConverterPostProcessor {

    public boolean process(BaseController baseController);
}
