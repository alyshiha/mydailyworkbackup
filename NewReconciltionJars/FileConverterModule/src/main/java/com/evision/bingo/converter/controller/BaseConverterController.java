/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.controller;

import com.evision.bingo.converter.service.iConverterPostProcessor;
import com.evision.bingo.converter.service.iConverterPreProcessor;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.model.ConverterConfig;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.ArrayList;
import java.util.List;

/**
 * @since 17/5/2017
 * @author Khaled Khalil
 *
 * This is the base class of converters from different types to bingo readable
 * format
 */
public abstract class BaseConverterController extends BaseController {

    //collection of registered pre-processors
    private List<iConverterPreProcessor> registeredPreProcessors;

    //collection of registered post-processors
    private List<iConverterPostProcessor> registeredPostProcessors;

    protected List<Configurable> subConfigList = new ArrayList();

    BaseConverterController(LogHandler logHandler, QueueHandler queueHandler, ConverterConfig converterConfig, List<Configurable> subConfigListParam, List<BaseController> eventsHandlerList) {
        super(logHandler, queueHandler, converterConfig, eventsHandlerList);

        if (subConfigListParam != null && !subConfigListParam.isEmpty()) {
            for (Configurable config : subConfigListParam) {
                this.subConfigList.add(config);
            }
        }

        init();
    }

    // init processors collections
    @Override
    public final void init() {
        registeredPreProcessors = new ArrayList<>();
        registeredPostProcessors = new ArrayList<>();
    }

    //force clear memory     
    @Override
    public final void destroy() {
        registeredPreProcessors.clear();
        registeredPostProcessors.clear();

        registeredPreProcessors = null;
        registeredPostProcessors = null;
    }

    //register a pre-processor
    final void registerPreProcessor(iConverterPreProcessor preProcessor) {
        registeredPreProcessors.add(preProcessor);
    }

    //unregister a pre-processor
    final void unregisterPreProcessor(iConverterPreProcessor preProcessor) {
        registeredPreProcessors.remove(preProcessor);
    }

    //register a post-processor
    final void registerPostProcessor(iConverterPostProcessor postProcessor) {
        registeredPostProcessors.add(postProcessor);
    }

    //unregister a post-processor
    final void unregisterPostProcessor(iConverterPostProcessor postProcessor) {
        registeredPostProcessors.remove(postProcessor);
    }

    //main load function that forces the work chain
    @Override
    public synchronized final boolean handleRequest(Object obj) {
        this.setObj(obj);
        if (preCollect()) {
            if (doCollect()) {
                return postCollect();
            }
        }
        return false;
    }

    //call of all registered pre-processors
    synchronized final boolean preCollect() {
        if (!registeredPreProcessors.isEmpty()) {
            for (iConverterPreProcessor preProcessor : registeredPreProcessors) {
                if (!preProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //call of all registered post-processors
    synchronized final boolean postCollect() {
        if (!registeredPostProcessors.isEmpty()) {
            for (iConverterPostProcessor postProcessor : registeredPostProcessors) {
                if (!postProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //method of collect to be overidden
    abstract boolean doCollect();
}
