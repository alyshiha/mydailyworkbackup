/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.controller;

import com.evision.bingo.converter.model.ConverterTemplateConfig;
import com.evision.bingo.converter.service.HeaderConvertService;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.model.ConverterConfig;
import com.evision.bingo.core.model.Extension;
import com.evision.bingo.core.model.HeaderConversionTemplate;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.JsonMessageConverter;
import org.springframework.amqp.core.Message;

/**
 * @since 1/5/2017
 * @author Aly Shiha
 *
 * This is the controller to convert the files with headers
 */
public class HeaderConverterController extends BaseConverterController {

    public HeaderConverterController(ConverterConfig converterConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<Configurable> subConfigList, List<BaseController> eventsHandlerList) {
        super(new LogHandler(converterConfig.getLoggerPathList(), systemLogger), new QueueHandler(converterConfig.getQueuesList(), rabbitTemplatesMap), converterConfig, subConfigList, eventsHandlerList);
    }

    @Override
    synchronized boolean doCollect() {
        if (getObj() != null) {
            JsonMessageConverter jsonConverter = new JsonMessageConverter();
            QueueMessage message = (QueueMessage) jsonConverter.fromMessage((Message) getObj());

            String extension = "";

            //check supported extensions
            List<Extension> extensionsList = ((ConverterConfig) config).getExtensionsList();
            boolean isSupportedExtension = false;
            for (Extension ext : extensionsList) {
                if (message.getExtension().toLowerCase().equals(ext.getExtension().toLowerCase())) {
                    isSupportedExtension = true;
                    extension = message.getExtension();
                    break;
                }
            }
            if (isSupportedExtension) { //supported extensions then convert
                for (Configurable sConfig : subConfigList) {
                    ConverterTemplateConfig subConfig = (ConverterTemplateConfig) sConfig;

                    //need to select the template that match the extension to convert the file
                    for (Extension ext : subConfig.getExtensionsList()) {
                        if (ext.getExtension().equals(extension)) {
                            HeaderConversionTemplate template = (HeaderConversionTemplate) subConfig.getTemplate();
                            HeaderConvertService headerConvertService = new HeaderConvertService(queueHandler, logHandler, eventsHandlerList);
                            headerConvertService.setTemplate(template);

                            if (headerConvertService.convert(message)) { //succeeded conversion
                                headerConvertService.clear();
                                queueHandler.sendMessage(message);
                                return true;
                            } else {
                                headerConvertService.clear();
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void stop() {
        //do nothing
    }
}
