/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.model.ConverterConfig;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.JsonMessageConverter;

/**
 * @since 17/5/2017
 * @author Khaled Khalil
 *
 * This is the converter that will do nothing except pass the record to the next
 * stage
 */
public final class PassConverterController extends BaseConverterController {

    public PassConverterController(ConverterConfig converterConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<Configurable> subConfigList, List<BaseController> eventsHandlerList) {
        super(new LogHandler(converterConfig.getLoggerPathList(), systemLogger), new QueueHandler(converterConfig.getQueuesList(), rabbitTemplatesMap), converterConfig, subConfigList, eventsHandlerList);
    }

    @Override
    synchronized boolean doCollect() {
        if (getObj() != null) {
            JsonMessageConverter jsonConverter = new JsonMessageConverter();
            QueueMessage message = (QueueMessage) jsonConverter.fromMessage((Message) getObj());
            logHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_PASS_FILE) + Defines.SYSTEM_SPACE + message.getRunningfile());

            queueHandler.sendMessage(message);
            return true;
        }
        return false;
    }

    @Override
    public void stop() {
        //do nothing
    }
}
