/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.model;

import com.evision.bingo.core.model.BaseConverterTemplate;
import com.evision.bingo.core.model.ConverterConfigurable;
import com.evision.bingo.core.model.Extension;
import java.util.List;

/**
 * @since 22/5/2017
 * @author Khaled Khalil
 *
 * Model for the unstructured templates configuration
 */
public class ConverterTemplateConfig implements ConverterConfigurable {

    private String path;
    private List<Extension> extensionsList;
    private BaseConverterTemplate template;
    private String templateClassname;

    public BaseConverterTemplate getTemplate() {
        return template;
    }
    
    @Override
    public void setTemplate(BaseConverterTemplate template) {
        this.template = template;
    }
    
    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Extension> getExtensionsList() {
        return extensionsList;
    }

    public void setExtensionsList(List<Extension> extensionsList) {
        this.extensionsList = extensionsList;
    }
    
    @Override
    public String getTemplateClassname() {
        return templateClassname;
    }

    public void setTemplateClassname(String templateClassname) {
        this.templateClassname = templateClassname;
    }
}
