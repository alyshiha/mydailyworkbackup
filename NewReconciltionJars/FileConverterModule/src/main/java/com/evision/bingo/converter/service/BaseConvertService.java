/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.service;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;

/**
 * @since 18/5/2017
 * @author Khaled Khalil
 *
 * BaseConvertService
 */
public abstract class BaseConvertService {

    protected LogHandler logHandler;
    protected QueueHandler queueHandler;
    protected List<BaseController> eventsHandlerList;

    public BaseConvertService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        this.queueHandler = queueHandler;
        this.logHandler = logHandler;
        this.eventsHandlerList = eventsHandlerList;
    }

    public abstract boolean convert(QueueMessage message);

    public abstract void clear();

    public void submitEvent(EventModel event) {
        for (BaseController controller : eventsHandlerList) {
            controller.handleRequest(event);
        }
    }
}
