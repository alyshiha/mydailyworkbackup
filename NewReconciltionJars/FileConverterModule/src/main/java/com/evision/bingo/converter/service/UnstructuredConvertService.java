/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.service;

import com.evision.bingo.converter.utils.ConverterDefines;
import com.evision.bingo.converter.utils.ValidateData;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.model.FieldDetails;
import com.evision.bingo.core.model.FieldsMaster;
import com.evision.bingo.core.model.UnstructuredConversionTemplate;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.core.utils.Utils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * @since 1/5/2017
 * @author Aly Shiha
 *
 */
public class UnstructuredConvertService extends BaseConvertService {

    private UnstructuredConversionTemplate template;
    private Map<String, String> headerRecord = new HashMap();

    public UnstructuredConvertService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, eventsHandlerList);
    }

    public void setTemplate(UnstructuredConversionTemplate template) {
        this.template = template;
    }

    @Override
    public boolean convert(QueueMessage message) {
        logHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_CONVERT_UNSTRUCTURED_FILE) + Defines.SYSTEM_SPACE + message.getRunningfile());

        Calendar cal = Calendar.getInstance();
        Date time = cal.getTime();

        EventModel eventModel = new EventModel();
        eventModel.setTime(new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(time));
        eventModel.setAction(EventModel.EVENT.FILE_CONVERT);
        eventModel.setActionDetails(message.getRunningfile());
        eventModel.setActionSource(ConverterDefines.QUEUE_ACTION_SOURCE);

        try {
            //read header if any
            if (ConverterDefines.SKIP_HEADER.valueOf(template.getSkipHeader().toUpperCase()).equals(ConverterDefines.SKIP_HEADER.FALSE)) {//found a header 
                headerRecord.putAll(parseHeader(new BufferedReader(new FileReader(message.getRunningfile())), template.getConstantTemplatesList()));
            }

            File csvFile = new File(Utils.removeFileExtension(message.getRunningfile()) + ".csv");
            if (csvFile.exists()) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(LogMessages.getMessage(LogMessages.CONVERTER_EXISTING_FILE_IN_DISTINATION_FOLDER) + Defines.SYSTEM_SPACE + csvFile.getName()));
                eventModel.setActionStatus(EventModel.STATUS.FAIL);
            } else {
                csvFile.createNewFile();

                //read remaining contents of the file
                BufferedReader fileReader = new BufferedReader(new FileReader(message.getRunningfile()));
                BufferedWriter csvFileWriter = new BufferedWriter(new FileWriter(csvFile, true));
                while (true) {
                    Map<String, String> record = parseNextRecord(fileReader);
                    //check for empty record as a end of file
                    if (record == null) {
                        break;
                    }
                    if (record.isEmpty()) {
                        continue;
                    } else { //add Record to Result
                        writeContents(record, csvFileWriter);
                    }
                }

                csvFileWriter.close();
                fileReader.close();

                //delete the unstructured file as not needed any more
                File existingFile = new File(message.getRunningfile());
                boolean isDeleted = existingFile.delete();
                if (!isDeleted) {
                    logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(String.format(LogMessages.getMessage(LogMessages.CONVERTER_FILE_NOT_DELETED), message.getRunningfile())));
                }

                message.setRunningfile(csvFile.getPath());
                message.setExtension("csv");
                message.setTime(time);

                eventModel.setActionStatus(EventModel.STATUS.SUCCESS);
            }
        } catch (IOException ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
            eventModel.setActionStatus(EventModel.STATUS.FAIL);
        } finally {
            submitEvent(eventModel);
            if (eventModel.getActionStatus().equals(EventModel.STATUS.SUCCESS)) {
                return true;
            } else {
                return false;
            }
        }

    }

    private void writeContents(Map<String, String> record, BufferedWriter csvFileWriter) throws IOException {
        StringBuilder structured = new StringBuilder();
        //Sort transaction according to field name
        Map<String, String> sortedMap = new TreeMap<String, String>((Map<? extends String, ? extends String>) record);
        for (Map.Entry<String, String> entry : sortedMap.entrySet()) {
            //create transaction string 
            structured.append(entry.getValue()).append(";");
        }
        //new line as transaction end
        structured.append(System.lineSeparator());
        //export data to distination file

        csvFileWriter.write(structured.toString());
        csvFileWriter.flush();
    }

    private Map<String, String> parseHeader(BufferedReader reader, List<FieldsMaster> constantTemplates) throws IOException {
        HashMap<String, String> headerStructure = new HashMap<>();

        String line;

        //get field template
        for (FieldsMaster fieldData : constantTemplates) {
            boolean isMandated = Boolean.valueOf(fieldData.getMandatory().toLowerCase());
            boolean foundFieldData = false;

            //loop on each field detail
            for (FieldDetails fieldDetails : fieldData.getDetailsList()) {

                //loop in tranaction lines
                while (!foundFieldData && (line = reader.readLine()) != null) {
                    //if line contain the given substring
                    int subStringIndex = line.toLowerCase().indexOf(fieldDetails.getSubStrings().toLowerCase());
                    //get specific location occourding to template
                    if ((subStringIndex == fieldDetails.getSubStringsIndex() && fieldDetails.getSubStringsIndex() != -1)
                            || (subStringIndex > -1 && fieldDetails.getSubStringsIndex() == -1)) {
                        String value = line.substring(fieldDetails.getBeginIndex(), fieldDetails.getEndIndex());
                        //Check If Data Match then check its format
                        if (ValidateData.Validate(fieldDetails.getDataType(), value, fieldDetails.getFormat(), fieldDetails.getSpecialCharacter())) {
                            if (!value.trim().isEmpty() && isMandated) {
                                headerStructure.put(fieldData.getHeader().trim(), value.trim());
                                foundFieldData = true;
                            }
                        }
                    }
                }
            }
        }

        // close the reader 
        reader.close();

        return headerStructure;
    }

    private Map<String, String> parseNextRecord(BufferedReader fileReader) throws IOException {
        HashMap<String, String> record = new HashMap();
        Map<String, String> constantTransactions = new HashMap();

        //retreive next transaction data from file
        Map<Integer, String> transactionLinesMap = getNextTransactionLines(fileReader, constantTransactions);

        if (transactionLinesMap != null) {
            //if no trasaction file return empty map 
            if (!transactionLinesMap.isEmpty()) {
                //extract field from given transaction
                Map<String, String> mappedTransactionFields = mapFields(transactionLinesMap);
                if (!mappedTransactionFields.isEmpty()) {
                    record.putAll(mappedTransactionFields);
                    record.putAll(constantTransactions);
                }
            }

            //add the header if any
            if (!headerRecord.isEmpty() && !record.isEmpty()) {
                record.putAll(headerRecord);
            }

            return record;
        } else {
            return null;
        }
    }

    private Map<Integer, String> getNextTransactionLines(BufferedReader fileReader, Map<String, String> constantTransactions) throws IOException {
        // create new variable to store tranaction in memory
        Map<Integer, String> transactionData = new HashMap();
        //line key
        int recordNumber = 1;
        String line;
        // Loop through each line, stashing the line into our line variable.
        while ((line = fileReader.readLine()) != null) {

            //get costant values
            setTransactionConstant(line, template.getConstantTransactionList(), constantTransactions);

            //get the starting index of the transaction start if -1 it mean contain in any index
            int startIndex = line.toLowerCase().indexOf(template.getStartString().toLowerCase());
            if ((startIndex == template.getStartPositionIndex() && template.getStartPositionIndex() != -1)
                    || (startIndex > -1 && template.getStartPositionIndex() == -1)) {
                transactionData.clear();
                continue;
            }

            //get the ending index of the transaction  if -1 it means contain in any index
            int endIndex = line.toLowerCase().indexOf(template.getEndString().toLowerCase());
            if ((endIndex == template.getEndPositionIndex() && template.getEndPositionIndex() != -1)
                    || (endIndex > -1 && template.getEndPositionIndex() == -1)) {
                break;
            }

            if (containsResetTag(line)) {
                //reset the transaction if found a reset tag
                transactionData.clear();
            }

            transactionData.put(recordNumber++, line);
        }

        if (line == null) {
            return null;
        } else {
            return transactionData;
        }
    }

    private void setTransactionConstant(String line, List<FieldsMaster> constantTransactionField, Map<String, String> constantTransactions) {
        for (FieldsMaster fieldMaster : constantTransactionField) {
            if (!constantTransactions.containsKey(fieldMaster.getHeader())) {
                //loop on each field detail
                for (FieldDetails fieldDetails : fieldMaster.getDetailsList()) {
                    //if line contain the given substring
                    int foundIndex = line.toLowerCase().indexOf(fieldDetails.getSubStrings().toLowerCase());
                    //get specific location occourding to template
                    if ((foundIndex == fieldDetails.getSubStringsIndex() && fieldDetails.getSubStringsIndex() != -1)
                            || (foundIndex > -1 && fieldDetails.getSubStringsIndex() == -1)) {
                        String value = line.substring(fieldDetails.getBeginIndex(), fieldDetails.getEndIndex());
                        //Check If Data Match It`s Stracture
                        if (ValidateData.Validate(fieldDetails.getDataType(), value.trim(), fieldDetails.getFormat(), fieldDetails.getSpecialCharacter())) {
                            constantTransactions.put(fieldMaster.getHeader(), value.trim());
                            break;
                        }
                    }
                }
            }
        }
    }

    private boolean containsResetTag(String lineFound) {
        //check if line contain a defined reset tag
        return template.getResetTagList().stream().filter(k -> lineFound.contains(k)).collect(Collectors.toList()).size() > 0;
    }

    private Map<String, String> mapFields(Map<Integer, String> transactionLinesMap) {
        Map<String, String> finalStucture = new HashMap<String, String>();
        //Loop inside the field template
        for (List<FieldsMaster> fieldsData : template.getDetailTemplatesList()) {
            //get field template
            for (FieldsMaster fieldData : fieldsData) {
                boolean fieldDataFound = false;
                finalStucture.put(fieldData.getHeader(), "");
                //loop on each field detail
                for (FieldDetails details : fieldData.getDetailsList()) {
                    //stop if fond with right format
                    if (!fieldDataFound) {
                        //loop in tranaction lines
                        for (Integer line : ((Set<Integer>) transactionLinesMap.keySet())) {
                            //if line contain the given substring
                            int foundIndex = transactionLinesMap.get(line).toString().toLowerCase().indexOf(details.getSubStrings().toLowerCase());
                            //get specific location occourding to template
                            if ((foundIndex == details.getSubStringsIndex() && details.getSubStringsIndex() != -1)
                                    || (foundIndex > -1 && details.getSubStringsIndex() == -1)) {
                                String Value = transactionLinesMap.get(line + details.getLine()).toString().substring(details.getBeginIndex(), details.getEndIndex());
                                //Check If Data Match It`s Stracture
                                if (ValidateData.Validate(details.getDataType(), Value, details.getFormat(), details.getSpecialCharacter())) {
                                    fieldDataFound = true;
                                    finalStucture.put(fieldData.getHeader(), Value.trim());
                                }
                            }
                        }
                    }
                }
            }
            //Check Mandatory Field from template
            if (isMandatory(finalStucture)) {
                return finalStucture;
            }
        }
        //no valid transaction return empty transaction
        return Collections.emptyMap();
    }

    private Boolean isMandatory(Map<String, String> transactionMap) {
        boolean state = true;
        for (List<FieldsMaster> fieldsData : template.getDetailTemplatesList()) {
            for (FieldsMaster details : fieldsData) {
                if (transactionMap.get(details.getHeader()).trim().equals("") && details.getMandatory().toLowerCase().equals("true")) {
                    state = false;
                    break;
                }
            }
        }

        return state;
    }

    public void clear() {
        // do nothing
    }
}
