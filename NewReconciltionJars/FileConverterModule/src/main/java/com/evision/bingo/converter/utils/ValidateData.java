/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @since 1/5/2017
 * @author Aly.Shiha
 */
public final class ValidateData {

    public static boolean Validate(String dataType, String data, String dateFormat, String specialCharacter) {
        boolean result = false;
        ConverterDefines.DATA_TYPE type = ConverterDefines.DATA_TYPE.valueOf(dataType.toUpperCase());
        switch (type) {
            case DATE:
                result = isDate(data, dateFormat);
                break;
            case DECIMAL:
                result = isDecimal(data);
                break;
            case NUMBER:
                result = isNumber(data);
                break;
            case ALPHA:
                result = isAlpha(data);
                break;
            case ALPHA_NUMERIC:
                result = isAlphaNumeric(data);
                break;
            case NUMERICSPECIALCHARACTER:
                result = isNumericSpecialCharacter(data, specialCharacter);
                break;
            case ALPHASPECIALCHARACTER:
                result = isAlphaSpecialCharacter(data, specialCharacter);
                break;
            case TEXT:
                result = Boolean.TRUE;
                break;
            case CONTAINS:
                result = contains(data, specialCharacter);
                break;
            default:
                break;
        }
        return result;
    }

    private static Boolean contains(String data, String specialCharacter) {
        return data.contains(specialCharacter);
    }

    private static Boolean isAlphaSpecialCharacter(String data, String specialCharacter) {
        return data.matches("[A-Za-z" + specialCharacter + "]*");
    }

    private static Boolean isNumericSpecialCharacter(String data, String specialCharacter) {
        return data.matches("[0-9" + specialCharacter + "]*");
    }

    private static Boolean isAlphaNumeric(String data) {
        return data.matches("[a-zA-Z0-9]*");
    }

    private static Boolean isAlpha(String data) {
        return data.matches("[a-zA-Z]*");
    }

    private static Boolean isNumber(String data) {
        try {
            double d = Double.parseDouble(data);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private static Boolean isDecimal(String data) {
        return data.matches("\\d*\\.?\\d+");
    }

    private static boolean isDate(String data, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        try {
            df.parse(data);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }
}
