/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.utils;

/**
 * @since 18/5/2017
 * @author Khaled Khalil
 *
 * Constants of the converter system
 */
public class ConverterDefines {

    public static final int CONVERTER_SCHEDULER_PERIOD_IN_SECONDS = 10;
    public static final String CONVERTER_EXCEL_TEMP_FILE_NAME = "Temp";
    public static final String QUEUE_ACTION_SOURCE = "Queue";
    public static final String CONVERTER_EXCEL_VBS_CODE = "Dim strFilename  \n"
            + "Dim toFilename  \n"
            + "Dim objFSO  \n"
            + "Set objFSO = CreateObject(\"scripting.filesystemobject\")  \n"
            + "strFilename = WScript.Arguments.Item(0)  \n"
            + "toFilename = WScript.Arguments.Item(1)  \n"
            + "If objFSO.fileexists(strFilename) Then  \n"
            + "  Call Writefile(strFilename)  \n"
            + "Else  \n"
            + "  wscript.echo \"no such file!\"  \n"
            + "End If  \n"
            + "Set objFSO = Nothing  \n"
            + "\n"
            + "Sub Writefile(ByVal strFilename)  \n"
            + "Dim objExcel  \n"
            + "Dim objWB  \n"
            + "Dim objws  \n"
            + "\n"
            + "Set objExcel = CreateObject(\"Excel.Application\")  \n"
            + "Set objWB = objExcel.Workbooks.Open(strFilename)  \n"
            + "\n"
            + "For Each objws In objWB.Sheets  \n"
            + "  objws.Copy  \n"
            + "  objExcel.ActiveWorkbook.SaveAs toFilename & objws.Name & \".csv\", 6  \n"
            + "  objExcel.ActiveWorkbook.Close False  \n"
            + "Next \n"
            + "\n"
            + "objWB.Close False  \n"
            + "objExcel.Quit  \n"
            + "Set objExcel = Nothing  \n"
            + "End Sub  ";
    public static final String ACCESS_TABLE_QUERY = "Select * from ";
    public static final String ACCESS_FIELDS_SEPARATOR = ";";

    public enum SKIP_HEADER {
        TRUE, FALSE;
    }

    public enum DATA_TYPE {
        DATE, DECIMAL, NUMBER, ALPHA, ALPHA_NUMERIC,
        NUMERICSPECIALCHARACTER, ALPHASPECIALCHARACTER,
        TEXT, CONTAINS;
    }
}