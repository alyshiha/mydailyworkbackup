/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.service;

import com.evision.bingo.converter.utils.ConverterDefines;
import com.evision.bingo.converter.utils.ValidateData;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.EventModel;
import com.evision.bingo.core.model.FieldDetails;
import com.evision.bingo.core.model.FieldsMaster;
import com.evision.bingo.core.model.HeaderConversionTemplate;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @since 1/5/2017
 * @author Aly Shiha
 *
 */
public class HeaderConvertService extends BaseConvertService {

    private HeaderConversionTemplate template;

    public void setTemplate(HeaderConversionTemplate template) {
        this.template = template;
    }

    public HeaderConvertService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, eventsHandlerList);
    }

    @Override
    public boolean convert(QueueMessage message) {
        logHandler.logInfo(LogMessages.getMessage(LogMessages.CONVERTER_CONVERT_HEADER_FILE) + Defines.SYSTEM_SPACE + message.getRunningfile());

        Calendar cal = Calendar.getInstance();
        Date time = cal.getTime();

        EventModel eventModel = new EventModel();
        eventModel.setTime(new SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(time));
        eventModel.setAction(EventModel.EVENT.FILE_CONVERT);
        eventModel.setActionDetails(message.getRunningfile());
        eventModel.setActionSource(ConverterDefines.QUEUE_ACTION_SOURCE);

        try {
            //read remaining contents of the file
            BufferedReader fileReader = new BufferedReader(new FileReader(message.getRunningfile()));
            Map<String, String> header = parseNextRecord(fileReader);
            //check for empty record as a end of file
            if (header == null || header.isEmpty()) {
                logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), new Exception(String.format(LogMessages.getMessage(LogMessages.CONVERTER_HEADER_NOT_FOUND), message.getRunningfile())));
                eventModel.setActionStatus(EventModel.STATUS.FAIL);
            } else {
                message.setHeader(header);

                eventModel.setActionStatus(EventModel.STATUS.SUCCESS);
            }
        } catch (IOException ex) {
            logHandler.logException(LogMessages.getMessage(LogMessages.EXCEPTION_PREFIX), ex);
            eventModel.setActionStatus(EventModel.STATUS.FAIL);
        } finally {
            submitEvent(eventModel);
            if (eventModel.getActionStatus().equals(EventModel.STATUS.SUCCESS)) {
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public void clear() {
        //do nothing
    }

    private Map<String, String> parseNextRecord(BufferedReader fileReader) throws IOException {
        //retreive next transaction data from file
        Map<Integer, String> transactionLinesMap = getNextTransactionLines(fileReader);

        if (transactionLinesMap != null && !transactionLinesMap.isEmpty()) {
            //extract field from given transaction
            Map<String, String> mappedTransactionFields = mapFields(transactionLinesMap);
            return mappedTransactionFields;
        } else {
            return null;
        }
    }

    private Map<Integer, String> getNextTransactionLines(BufferedReader fileReader) throws IOException {
        // create new variable to store tranaction in memory
        Map<Integer, String> transactionData = new HashMap();
        //line key
        int recordNumber = 1;
        String line;
        // Loop through each line, stashing the line into our line variable.
        while ((line = fileReader.readLine()) != null) {

            //get the starting index of the transaction start if -1 it mean contain in any index
            if (recordNumber <= template.getNumberOfLines()) {
                transactionData.put(recordNumber++, line);
            } else {
                break;
            }
        }
        return transactionData;
    }

    private Map<String, String> mapFields(Map<Integer, String> transactionLinesMap) {
        Map<String, String> finalStucture = new HashMap<String, String>();

        //get field template
        for (FieldsMaster fieldData : template.getDetailTemplatesList()) {
            boolean fieldDataFound = false;
            finalStucture.put(fieldData.getHeader(), "");
            //loop on each field detail
            for (FieldDetails details : fieldData.getDetailsList()) {
                //stop if fond with right format
                if (!fieldDataFound) {
                    //loop in tranaction lines
                    for (Integer line : ((Set<Integer>) transactionLinesMap.keySet())) {
                        //if line contain the given substring
                        int foundIndex = transactionLinesMap.get(line).toString().toLowerCase().indexOf(details.getSubStrings().toLowerCase());
                        //get specific location occourding to template
                        if ((foundIndex == details.getSubStringsIndex() && details.getSubStringsIndex() != -1)
                                || (foundIndex > -1 && details.getSubStringsIndex() == -1)) {
                            String value = transactionLinesMap.get(line + details.getLine()).toString().substring(details.getBeginIndex(), details.getEndIndex());
                            //Check If Data Match It`s Stracture
                            if (ValidateData.Validate(details.getDataType(), value, details.getFormat(), details.getSpecialCharacter())) {
                                fieldDataFound = true;
                                finalStucture.put(fieldData.getHeader(), value.trim());
                            }
                        }
                    }
                }
            }

            //Check Mandatory Field from template
            if (!isMandatory(finalStucture)) {
//                finalStucture.remove(fieldData.getHeader());
                  finalStucture.clear();
            }
        }

        return finalStucture;
    }

    private Boolean isMandatory(Map<String, String> transactionMap) {
        boolean state = true;
        for (FieldsMaster details : template.getDetailTemplatesList()) {
            if (transactionMap.get(details.getHeader()) != null) {
                if (transactionMap.get(details.getHeader()).trim().equals("") && details.getMandatory().toLowerCase().equals("true")) {
                    state = false;
                    break;
                }
            }
        }
        return state;
    }
}