/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.converter.controller;

import com.evision.bingo.converter.service.AccessConvertService;
import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.Configurable;
import com.evision.bingo.core.model.ConverterConfig;
import com.evision.bingo.core.model.Extension;
import com.evision.bingo.core.model.QueueMessage;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.JsonMessageConverter;

/**
 * @since 1/5/2017
 * @author Aly Shiha
 *
 * This is the controller to convert access files
 */
public class AccessConverterController extends BaseConverterController {

    public AccessConverterController(ConverterConfig converterConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<Configurable> subConfigList, List<BaseController> eventsHandlerList) {
        super(new LogHandler(converterConfig.getLoggerPathList(), systemLogger), new QueueHandler(converterConfig.getQueuesList(), rabbitTemplatesMap), converterConfig, subConfigList, eventsHandlerList);
    }

    @Override
    synchronized boolean doCollect() {
        if (getObj() != null) {
            JsonMessageConverter jsonConverter = new JsonMessageConverter();
            QueueMessage message = (QueueMessage) jsonConverter.fromMessage((Message)getObj());
            
            AccessConvertService accessConvertService = new AccessConvertService(queueHandler, logHandler, eventsHandlerList);
            //check supported extensions
            List<Extension> extensionsList = ((ConverterConfig) config).getExtensionsList();
            boolean isSupportedExtension = false;
            for (Extension ext : extensionsList) {
                if (message.getExtension().toLowerCase().equals(ext.getExtension().toLowerCase())) {
                    isSupportedExtension = true;
                    break;
                }
            }
            if (isSupportedExtension) { //supported extensions then convert
                if (accessConvertService.convert(message)) { //succeeded conversion
                    accessConvertService.clear();
                    queueHandler.sendMessage(message);
                    return true;
                } else {
                    accessConvertService.clear();
                }
            }

        }
        return false;
    }

    @Override
    public void stop() {
        // do nothing
    }
}
