/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.excel.controller;

import evision.mq.datamodel.OutputDefinition;
import evision.mq.service.RabbitMqConfig;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import org.apache.commons.io.FilenameUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ExcelClient {

    private static RabbitTemplate rabbitTemplate;
    private static final String content = "Dim oExcel\n"
            + "Set oExcel = CreateObject(\"Excel.Application\")\n"
            + "Dim oBook\n"
            + "Set oBook = oExcel.Workbooks.Open(Wscript.Arguments.Item(0))\n"
            + "oBook.SaveAs WScript.Arguments.Item(1), 6\n"
            + "oBook.Close False\n"
            + "oExcel.Quit";

    public ExcelClient() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(RabbitMqConfig.class);
        rabbitTemplate = ctx.getBean(RabbitTemplate.class);
    }
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ExcelClient.class);

    public static void run(OutputDefinition Source) {

        try {
            File FromFolder = new File(Source.getRunningfile());
            File tempfile = new File(FromFolder.getParent() + "\\vbtest.vbs");
            // if file doesnt exists, then create it
            createfile(tempfile);

            FileWriter fw = new FileWriter(tempfile.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();
            String processcomm = "\"C:\\Windows\\SysWOW64\\cscript.exe\" \"" + FromFolder.getParent() + "\\vbtest.vbs\" \"" + FromFolder.getAbsolutePath() + "\" \"" + FromFolder.getParent() + "\\" + FilenameUtils.removeExtension(FromFolder.getName()) + ".csv\"";
            Process process = Runtime.getRuntime().exec(processcomm);
            process.waitFor();
            deletefile(tempfile);
            SendToQueue(FromFolder, Source);
        } catch (FileNotFoundException e) {
            logger.error("File not found: " + e.getMessage());
        } catch (IOException e) { // catch all IOExceptions not handled by previous catch blocks
            logger.error("General I/O exception: " + e.getMessage());
            e.printStackTrace();
        } catch (InterruptedException ex) {
            logger.error("Failed to compute sum");
        }
    }

    private static void SendToQueue(File FromFolder, OutputDefinition Source) {
        rabbitTemplate.setRoutingKey(RabbitMqConfig.SIMPLE_MESSAGE_QUEUE_OUT);
        Source.setRunningfile(FromFolder.getPath());
        rabbitTemplate.convertAndSend(Source);
        rabbitTemplate.setRoutingKey(RabbitMqConfig.SIMPLE_MESSAGE_QUEUE_BACKUP);
        Source.setRunningfile(FromFolder.getParent() + "\\" + FilenameUtils.removeExtension(FromFolder.getName()) + ".csv");
        rabbitTemplate.convertAndSend(Source);
    }

    private static void createfile(File tempfile) throws IOException {
        if (!tempfile.exists()) {
            tempfile.createNewFile();
        }
    }

    private static void deletefile(File tempfile) throws IOException {
        tempfile.delete();
    }

}
