/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.excel.service;

import evision.excel.controller.ExcelClient;
import evision.mq.datamodel.OutputDefinition;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.support.converter.JsonMessageConverter;


/**
 *
 * @author Aly.Shiha
 */
public class ExcelManger implements MessageListener {

    private JsonMessageConverter converter = new JsonMessageConverter();

    @Override
    public void onMessage(Message message) {
        ExcelClient.run((OutputDefinition) converter.fromMessage(message));
    }
}
