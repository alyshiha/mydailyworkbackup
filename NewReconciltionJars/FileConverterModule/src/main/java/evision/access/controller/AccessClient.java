/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.access.controller;

import evision.mq.datamodel.OutputDefinition;
import evision.mq.service.RabbitMqConfig;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.ucanaccess.jdbc.UcanaccessConnection;
import org.apache.commons.io.FilenameUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author Aly.Shiha
 */
public class AccessClient {

    static Connection connection;
    static Statement statement;
    private static RabbitTemplate rabbitTemplate;

    public AccessClient() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(RabbitMqConfig.class);
        rabbitTemplate = ctx.getBean(RabbitTemplate.class);
    }

    public static void run(OutputDefinition msg) {
        File file = new File(msg.getRunningfile());
        try {
            DriverManager.registerDriver(new net.ucanaccess.jdbc.UcanaccessDriver());
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver").newInstance();
            String database = "jdbc:ucanaccess://" + file.getAbsolutePath() + ";memory=false";
            connection = DriverManager.getConnection(database, "", "");
            buildStatement();
            executeQuery(file.getName(), file);
            statement.close();
            connection.close();
            ((UcanaccessConnection) connection).unloadDB();
            SendToQueue(file, msg);
        } catch (SecurityException | SQLException | ClassNotFoundException | InstantiationException | FileNotFoundException | IllegalAccessException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void buildStatement() throws SQLException {
        statement = connection.createStatement();
    }

    public static void executeQuery(String FileName, File file) throws SQLException, FileNotFoundException {

//        boolean foundResults = statement.execute("SELECT * FROM " + Properties.getTableName());
        boolean foundResults = statement.execute("SELECT * FROM ");
        if (foundResults) {
            ResultSet set = statement.getResultSet();
            if (set != null) {
                displayResults(set, FileName, file);
            }
        } else {
            connection.close();
        }
    }

    public static void displayResults(ResultSet rs, String FileName, File file) throws SQLException, FileNotFoundException {
        ResultSetMetaData metaData = rs.getMetaData();
        int columns = metaData.getColumnCount();
        PrintWriter writer = new PrintWriter(file.getParent() + "/" + FileName + ".txt");
        while (rs.next()) {
            for (int i = 1; i <= columns; ++i) {
                writer.print(rs.getString(i) + ";");
            }
            writer.println();
        }
        writer.close();

    }

    private static void SendToQueue(File FromFolder, OutputDefinition Source) {
        rabbitTemplate.setRoutingKey(RabbitMqConfig.SIMPLE_MESSAGE_QUEUE_OUT);
        Source.setRunningfile(FromFolder.getPath());
        rabbitTemplate.convertAndSend(Source);
        rabbitTemplate.setRoutingKey(RabbitMqConfig.SIMPLE_MESSAGE_QUEUE_BACKUP);
        Source.setRunningfile(FromFolder.getParent() + "\\" + FilenameUtils.removeExtension(FromFolder.getName()) + ".csv");
        rabbitTemplate.convertAndSend(Source);
    }
}
