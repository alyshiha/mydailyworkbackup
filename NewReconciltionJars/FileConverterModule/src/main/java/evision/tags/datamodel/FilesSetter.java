/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.tags.datamodel;

/**
 *
 * @author Aly.Shiha
 */
public class FilesSetter {

    private String Source;
    private String Distination;
    private String Template;

    public FilesSetter() {
    }

    public FilesSetter(String Source, String Distination, String Template) {
        this.Source = Source;
        this.Distination = Distination;
        this.Template = Template;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String Source) {
        this.Source = Source;
    }

    public String getDistination() {
        return Distination;
    }

    public void setDistination(String Distination) {
        this.Distination = Distination;
    }

    public String getTemplate() {
        return Template;
    }

    public void setTemplate(String Template) {
        this.Template = Template;
    }

}
