/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.tags.datamodel;

import java.util.List;

/**
 *
 * @author Aly.Shiha
 */
public class FieldsMaster {

    private String Header;
    private List<FieldDetails> Details;

    public FieldsMaster() {
    }

    public FieldsMaster(String Header, List<FieldDetails> Details) {
        this.Header = Header;
        this.Details = Details;
    }

    public String getHeader() {
        return Header;
    }

    public void setHeader(String Header) {
        this.Header = Header;
    }

    public List<FieldDetails> getDetails() {
        return Details;
    }

    public void setDetails(List<FieldDetails> Details) {
        this.Details = Details;
    }

}
