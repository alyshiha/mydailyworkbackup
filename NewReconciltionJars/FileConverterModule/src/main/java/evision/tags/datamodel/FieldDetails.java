/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.tags.datamodel;

/**
 *
 * @author Aly.Shiha
 */
public class FieldDetails {

    private Integer index;
    private String SubStrings;
    private Integer Line;
    private Integer BeginIndex;
    private Integer endIndex;
    private String DataType;
    private String Format;
    private String SpecialCharacter;

    public FieldDetails() {
    }

    public FieldDetails(Integer index,String SubStrings, Integer Line, Integer BeginIndex, Integer endIndex, String DataType, String Format, String SpecialCharacter) {
        this.SubStrings = SubStrings;
        this.Line = Line;
        this.BeginIndex = BeginIndex;
        this.endIndex = endIndex;
        this.DataType = DataType;
        this.Format = Format;
        this.SpecialCharacter = SpecialCharacter;
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getDataType() {
        return DataType;
    }

    public void setDataType(String DataType) {
        this.DataType = DataType;
    }

    public String getFormat() {
        return Format;
    }

    public void setFormat(String Format) {
        this.Format = Format;
    }

    public String getSpecialCharacter() {
        return SpecialCharacter;
    }

    public void setSpecialCharacter(String SpecialCharacter) {
        this.SpecialCharacter = SpecialCharacter;
    }

    public String getSubStrings() {
        return SubStrings;
    }

    public void setSubStrings(String SubStrings) {
        this.SubStrings = SubStrings;
    }

    public Integer getLine() {
        return Line;
    }

    public void setLine(Integer Line) {
        this.Line = Line;
    }

    public Integer getBeginIndex() {
        return BeginIndex;
    }

    public void setBeginIndex(Integer BeginIndex) {
        this.BeginIndex = BeginIndex;
    }

    public Integer getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(Integer endIndex) {
        this.endIndex = endIndex;
    }

}
