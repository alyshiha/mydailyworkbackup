/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.tags.datamodel;

import java.util.List;

/**
 *
 * @author Aly.Shiha
 */
public class Template {

    private String Name;
    private String StartString;
    private String EndString;
    private List<String> ResetTag;
    private List<List<FieldsMaster>> DetailTemplates;

    public Template() {
    }

    public Template(String Name, String StartString, String EndString, List<String> ResetTag, List<List<FieldsMaster>> DetailTemplates) {
        this.Name = Name;
        this.StartString = StartString;
        this.EndString = EndString;
        this.ResetTag = ResetTag;
        this.DetailTemplates = DetailTemplates;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getStartString() {
        return StartString;
    }

    public void setStartString(String StartString) {
        this.StartString = StartString;
    }

    public String getEndString() {
        return EndString;
    }

    public void setEndString(String EndString) {
        this.EndString = EndString;
    }

    public List<String> getResetTag() {
        return ResetTag;
    }

    public void setResetTag(List<String> ResetTag) {
        this.ResetTag = ResetTag;
    }

    public List<List<FieldsMaster>> getDetailTemplates() {
        return DetailTemplates;
    }

    public void setDetailTemplates(List<List<FieldsMaster>> DetailTemplates) {
        this.DetailTemplates = DetailTemplates;
    }




}
