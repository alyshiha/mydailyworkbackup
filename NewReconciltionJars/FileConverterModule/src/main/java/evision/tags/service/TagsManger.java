/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.tags.service;

import evision.tags.controller.TagsClient;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.support.converter.JsonMessageConverter;

/**
 *
 * @author Aly.Shiha
 */
public class TagsManger implements MessageListener {

    private JsonMessageConverter converter = new JsonMessageConverter();

    @Override
    public void onMessage(Message message) {
        TagsClient.run();
    }
}
