/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.tags.controller;

import evision.tags.datamodel.FieldsMaster;
import evision.tags.datamodel.FilesSetter;
import evision.tags.datamodel.Template;
import evision.util.XStreamTranslator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author Aly.Shiha
 */
public class TagsClient {

    private static Template template;
    private static List<List<FieldsMaster>> DetailTemplateList = new ArrayList<List<FieldsMaster>>();
    private static List<FieldsMaster> DetailTemplate = new ArrayList<FieldsMaster>();
    private static List<FieldsMaster> SecDetailTemplate = new ArrayList<FieldsMaster>();
    private static FilesSetter FileSettings;

    public static void run() {
        // <editor-fold defaultstate="collapsed" desc="Cairo Bank Template">
        /*
         ////////////////////////////////////////////  First Field Detail Template //////////////////////////////////////
         List<FieldDetails> details1 = new ArrayList<FieldDetails>();
         details1.add(new FieldDetails("WITHDRA", 0, 0, 10, "Date", "dd/MM/yyyy", ""));
         details1.add(new FieldDetails("TRANSFE", 0, 0, 10, "Date", "dd/MM/yyyy", ""));
         details1.add(new FieldDetails("BAL.INQ", 0, 0, 10, "Date", "dd/MM/yyyy", ""));
         details1.add(new FieldDetails("FAW-REC", 0, 0, 10, "Date", "dd/MM/yyyy", ""));
         DetailTemplate.add(new FieldsMaster("Record1", details1));

         List<FieldDetails> details2 = new ArrayList<FieldDetails>();
         details2.add(new FieldDetails("WITHDRA", 0, 11, 16, "Date", "HH:mm", ""));
         details2.add(new FieldDetails("TRANSFE", 0, 11, 16, "Date", "HH:mm", ""));
         details2.add(new FieldDetails("BAL.INQ", 0, 11, 16, "Date", "HH:mm", ""));
         details2.add(new FieldDetails("FAW-REC", 0, 11, 16, "Date", "HH:mm", ""));
         DetailTemplate.add(new FieldsMaster("Record2", details2));

         List<FieldDetails> details3 = new ArrayList<FieldDetails>();
         details3.add(new FieldDetails("WITHDRA", 1, 1, 16, "NumericSpecialCharacter", "", "*"));
         details3.add(new FieldDetails("TRANSFE", 1, 1, 16, "NumericSpecialCharacter", "", "*"));
         details3.add(new FieldDetails("BAL.INQ", 1, 1, 16, "NumericSpecialCharacter", "", "*"));
         details3.add(new FieldDetails("FAW-REC", 1, 1, 16, "NumericSpecialCharacter", "", "*"));
         DetailTemplate.add(new FieldsMaster("Record3", details3));

         List<FieldDetails> details4 = new ArrayList<FieldDetails>();
         details4.add(new FieldDetails("WITHDRA", 1, 20, 30, "Decimals", "", ""));
         details4.add(new FieldDetails("TRANSFE", 1, 20, 30, "Decimals", "", ""));
         details4.add(new FieldDetails("BAL.INQ", 1, 20, 30, "Decimals", "", ""));
         details4.add(new FieldDetails("FAW-REC", 1, 20, 30, "Decimals", "", ""));
         DetailTemplate.add(new FieldsMaster("Record4", details4));

         List<FieldDetails> details5 = new ArrayList<FieldDetails>();
         details5.add(new FieldDetails("WITHDRA", 0, 23, 28, "Number", "", ""));
         details5.add(new FieldDetails("TRANSFE", 0, 23, 28, "Number", "", ""));
         details5.add(new FieldDetails("BAL.INQ", 0, 23, 28, "Number", "", ""));
         details5.add(new FieldDetails("FAW-REC", 0, 23, 28, "Number", "", ""));
         DetailTemplate.add(new FieldsMaster("Record5", details5));

         List<FieldDetails> details6 = new ArrayList<FieldDetails>();
         details6.add(new FieldDetails("WITHDRA", 2, 30, 35, "Number", "", ""));
         details6.add(new FieldDetails("TRANSFE", 2, 30, 35, "Number", "", ""));
         details6.add(new FieldDetails("BAL.INQ", 2, 30, 35, "Number", "", ""));
         details6.add(new FieldDetails("FAW-REC", 2, 30, 35, "Number", "", ""));
         DetailTemplate.add(new FieldsMaster("Record6", details6));

         List<FieldDetails> details7 = new ArrayList<FieldDetails>();
         details7.add(new FieldDetails("WITHDRA", 1, 30, 33, "Alpha", "", ""));
         details7.add(new FieldDetails("TRANSFE", 1, 30, 33, "Alpha", "", ""));
         details7.add(new FieldDetails("BAL.INQ", 1, 30, 33, "Alpha", "", ""));
         details7.add(new FieldDetails("FAW-REC", 1, 30, 33, "Alpha", "", ""));
         DetailTemplate.add(new FieldsMaster("Record7", details7));

         List<FieldDetails> details8 = new ArrayList<FieldDetails>();
         details8.add(new FieldDetails("WITHDRA", 0, 17, 21, "Number", "", ""));
         details8.add(new FieldDetails("TRANSFE", 0, 17, 21, "Number", "", ""));
         details8.add(new FieldDetails("BAL.INQ", 0, 17, 21, "Number", "", ""));
         details8.add(new FieldDetails("FAW-REC", 0, 17, 21, "Number", "", ""));
         DetailTemplate.add(new FieldsMaster("Record8", details8));

         List<FieldDetails> details9 = new ArrayList<FieldDetails>();
         details9.add(new FieldDetails("WITHDRA", 0, 29, 36, "AlphaSpecialCharacter", "", "."));
         details9.add(new FieldDetails("TRANSFE", 0, 29, 36, "AlphaSpecialCharacter", "", "."));
         details9.add(new FieldDetails("BAL.INQ", 0, 29, 36, "AlphaSpecialCharacter", "", "."));
         details9.add(new FieldDetails("FAW-REC", 0, 29, 36, "AlphaSpecialCharacter", "", ".-"));
         DetailTemplate.add(new FieldsMaster("Record9", details9));

         ////////////////////////////////////////////  Second Field Detail Template //////////////////////////////////////
         List<FieldDetails> details10 = new ArrayList<FieldDetails>();
         details10.add(new FieldDetails("WITHDRAWAL", -2, 0, 10, "Date", "dd/MM/yyyy", ""));
         details10.add(new FieldDetails("MINI.STAT", -2, 0, 10, "Date", "dd/MM/yyyy", ""));
         details10.add(new FieldDetails("BAL.INQ", -2, 0, 10, "Date", "dd/MM/yyyy", ""));
         details10.add(new FieldDetails("TRANSFER", -2, 0, 10, "Date", "dd/MM/yyyy", ""));
         details10.add(new FieldDetails("FAW-RECHAR", -2, 0, 10, "Date", "dd/MM/yyyy", ""));
         SecDetailTemplate.add(new FieldsMaster("Record1", details10));

         List<FieldDetails> details11 = new ArrayList<FieldDetails>();
         details11.add(new FieldDetails("WITHDRAWAL", -2, 11, 16, "Date", "HH:mm", ""));
         details11.add(new FieldDetails("MINI.STAT", -2, 11, 16, "Date", "HH:mm", ""));
         details11.add(new FieldDetails("BAL.INQ", -2, 11, 16, "Date", "HH:mm", ""));
         details11.add(new FieldDetails("TRANSFER", -2, 11, 16, "Date", "HH:mm", ""));
         details11.add(new FieldDetails("FAW-RECHAR", -2, 11, 16, "Date", "HH:mm", ""));
         SecDetailTemplate.add(new FieldsMaster("Record2", details11));

         List<FieldDetails> details12 = new ArrayList<FieldDetails>();
         details12.add(new FieldDetails("WITHDRAWAL", -1, 1, 16, "NumericSpecialCharacter", "", "*"));
         details12.add(new FieldDetails("MINI.STAT", -1, 1, 16, "NumericSpecialCharacter", "", "*"));
         details12.add(new FieldDetails("BAL.INQ", -1, 1, 16, "NumericSpecialCharacter", "", "*"));
         details12.add(new FieldDetails("TRANSFER", -1, 1, 16, "NumericSpecialCharacter", "", "*"));
         details12.add(new FieldDetails("FAW-RECHAR", -1, 1, 16, "NumericSpecialCharacter", "", "*"));
         SecDetailTemplate.add(new FieldsMaster("Record3", details12));

         List<FieldDetails> details13 = new ArrayList<FieldDetails>();
         details13.add(new FieldDetails("WITHDRAWAL", -1, 20, 29, "Decimals", "", ""));
         details13.add(new FieldDetails("MINI.STAT", -1, 20, 29, "Decimals", "", ""));
         details13.add(new FieldDetails("BAL.INQ", -1, 20, 29, "Decimals", "", ""));
         details13.add(new FieldDetails("TRANSFER", -1, 20, 29, "Decimals", "", ""));
         details13.add(new FieldDetails("FAW-RECHAR", -1, 20, 29, "Decimals", "", ""));
         SecDetailTemplate.add(new FieldsMaster("Record4", details13));

         List<FieldDetails> details14 = new ArrayList<FieldDetails>();
         details14.add(new FieldDetails("WITHDRAWAL", -2, 28, 35, "Number", "", ""));
         details14.add(new FieldDetails("MINI.STAT", -2, 28, 35, "Number", "", ""));
         details14.add(new FieldDetails("BAL.INQ", -2, 28, 35, "Number", "", ""));
         details14.add(new FieldDetails("TRANSFER", -2, 28, 35, "Number", "", ""));
         details14.add(new FieldDetails("FAW-RECHAR", -2, 28, 35, "Number", "", ""));
         SecDetailTemplate.add(new FieldsMaster("Record5", details14));

         List<FieldDetails> details15 = new ArrayList<FieldDetails>();
         details15.add(new FieldDetails("WITHDRAWAL", 1, 30, 35, "Number", "", ""));
         details15.add(new FieldDetails("MINI.STAT", 1, 30, 35, "Number", "", ""));
         details15.add(new FieldDetails("BAL.INQ", 1, 30, 35, "Number", "", ""));
         details15.add(new FieldDetails("TRANSFER", 1, 30, 35, "Number", "", ""));
         details15.add(new FieldDetails("FAW-RECHAR", 1, 30, 35, "Number", "", ""));
         SecDetailTemplate.add(new FieldsMaster("Record6", details15));

         List<FieldDetails> details16 = new ArrayList<FieldDetails>();
         details16.add(new FieldDetails("WITHDRAWAL", -1, 30, 33, "Alpha", "", ""));
         details16.add(new FieldDetails("MINI.STAT", -1, 30, 33, "Alpha", "", ""));
         details16.add(new FieldDetails("BAL.INQ", -1, 30, 33, "Alpha", "", ""));
         details16.add(new FieldDetails("TRANSFER", -1, 30, 33, "Alpha", "", ""));
         details16.add(new FieldDetails("FAW-RECHAR", -1, 30, 33, "Alpha", "", ""));
         SecDetailTemplate.add(new FieldsMaster("Record7", details16));

         List<FieldDetails> details17 = new ArrayList<FieldDetails>();
         details17.add(new FieldDetails("WITHDRAWAL", -2, 20, 26, "Number", "", ""));
         details17.add(new FieldDetails("MINI.STAT", -2, 20, 26, "Number", "", ""));
         details17.add(new FieldDetails("BAL.INQ", -2, 20, 26, "Number", "", ""));
         details17.add(new FieldDetails("TRANSFER", -2, 20, 26, "Number", "", ""));
         details17.add(new FieldDetails("FAW-RECHAR", -2, 20, 26, "Number", "", ""));
         SecDetailTemplate.add(new FieldsMaster("Record8", details17));

         List<FieldDetails> details18 = new ArrayList<FieldDetails>();
         details18.add(new FieldDetails("WITHDRAWAL", 0, 0, 11, "AlphaSpecialCharacter", "", "."));
         details18.add(new FieldDetails("MINI.STAT", 0, 0, 11, "AlphaSpecialCharacter", "", "."));
         details18.add(new FieldDetails("BAL.INQ", 0, 0, 11, "AlphaSpecialCharacter", "", "."));
         details18.add(new FieldDetails("TRANSFER", 0, 0, 11, "AlphaSpecialCharacter", "", "."));
         details18.add(new FieldDetails("FAW-RECHAR", 0, 0, 11, "AlphaSpecialCharacter", "", ".-"));
         SecDetailTemplate.add(new FieldsMaster("Record9", details18));

         DetailTemplateList.add(DetailTemplate);
         DetailTemplateList.add(SecDetailTemplate);

         template = new Template("MyTestTemplate", "TRANSACTION STARTED", "TRANSACTION END", "", DetailTemplateList);
     
         List<FieldDetails> Details14 = new ArrayList<FieldDetails>();
         Details14.add(new FieldDetails(1, "OPERATION OK", -3, 5, 15, "Date", "dd/MM/yyyy", ""));
         DetailTemplate.add(new FieldsMaster("transactiondate", Details14));

         List<FieldDetails> Details15 = new ArrayList<FieldDetails>();
         Details15.add(new FieldDetails(2, "OPERATION OK", -3, 16, 21, "Date", "HH:mm", ""));
         DetailTemplate.add(new FieldsMaster("transactiontime", Details15));

         List<FieldDetails> Details16 = new ArrayList<FieldDetails>();
         Details16.add(new FieldDetails(3, "OPERATION OK", -2, 0, 35, "Text", "", ""));
         DetailTemplate.add(new FieldsMaster("error", Details16));

         List<FieldDetails> Details0 = new ArrayList<FieldDetails>();
         Details0.add(new FieldDetails(4, "ESCROW:", 0, 10, 13, "Number", "", ""));
         DetailTemplate.add(new FieldsMaster("escrow", Details0));

         List<FieldDetails> Details1 = new ArrayList<FieldDetails>();
         Details1.add(new FieldDetails(5, "VAULTED:", 0, 10, 13, "Number", "", ""));
         DetailTemplate.add(new FieldsMaster("valuted", Details1));

         List<FieldDetails> Details2 = new ArrayList<FieldDetails>();
         Details2.add(new FieldDetails(6, "REFUNDED:", 0, 10, 13, "Number", "", ""));
         DetailTemplate.add(new FieldsMaster("refunded", Details2));

         List<FieldDetails> Details3 = new ArrayList<FieldDetails>();
         Details3.add(new FieldDetails(7, "REJECTS:", 0, 8, 11, "Number", "", ""));
         DetailTemplate.add(new FieldsMaster("rejected", Details3));

         List<String> ResetTags = new ArrayList<String>();
         ResetTags.add("OPERATION OK");
         ResetTags.add("DEPOSIT");

         DetailTemplateList.add(DetailTemplate);
         template = new Template("MyTestTemplate", "TRANSACTION STARTED", "TRANSACTION END", ResetTags, DetailTemplateList);

         try {
         XStreamTranslator.getInstance().toXMLFile(template, "E:\\test.xml");
         } catch (IOException ex) {
         System.out.println(ex.getMessage());
         }
        
         FileSettings = new FilesSetter("C:\\Users\\Aly.Shiha\\Desktop\\Loader\\From", "C:\\Users\\Aly.Shiha\\Desktop\\Loader\\To", "C:\\Users\\Aly.Shiha\\Desktop\\Loader\\Temp.xml");
         */
        // </editor-fold>
        try {
            FileSettings = (FilesSetter) XStreamTranslator.getInstance().toObject(new File("./ConfigFiles/File.xml"));
            for (final File file : new File(FileSettings.getSource()).listFiles()) {
                long currenttime = System.currentTimeMillis();
                new File(FileSettings.getDistination().concat("\\" + currenttime)).mkdir();
                template = (Template) XStreamTranslator.getInstance().toObject(new File(FileSettings.getTemplate()));
                Writer writer = new FileWriter(FileSettings.getDistination().concat("\\" + currenttime + "\\" + FilenameUtils.removeExtension(file.getName()) + ".Dat"));
                searchfile.ReadFile(file.getAbsolutePath(), template, writer);
                writer.close();
                SQLLoader.Run(FileSettings.getDistination().concat("\\" + currenttime + "\\" + FilenameUtils.removeExtension(file.getName()) + ".Dat"));
                file.delete();

            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
