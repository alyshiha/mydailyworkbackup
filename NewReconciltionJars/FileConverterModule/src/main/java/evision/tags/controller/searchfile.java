package evision.tags.controller;

// Import io so we can use file objects
import evision.tags.datamodel.FieldDetails;
import evision.tags.datamodel.FieldsMaster;
import evision.tags.datamodel.Template;
import evision.util.ValidateData;
import java.io.*;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class searchfile {

    private static Hashtable Transaction = new Hashtable();
    private static String Separator = ";";
    private static Map TransactionData = new HashMap();

    public static void ReadFile(String FilePath, Template template, Writer writer) {
        try {
            // Open the file c:\test.txt as a buffered reader
            BufferedReader bf = new BufferedReader(new FileReader(FilePath));
            // Start a line count and declare a string to hold our current line.
            int linecount = 0;
            String line;
            Boolean TransactionFound = Boolean.FALSE;
            // Loop through each line, stashing the line into our line variable.
            while ((line = bf.readLine()) != null) {
                // Increment the count and find the index of the word

                int Startindex = line.toLowerCase().indexOf(template.getStartString().toLowerCase());
                int Endindex = line.toLowerCase().indexOf(template.getEndString().toLowerCase());

                // If greater than -1, means we found the word
                if (Startindex > -1) {
                    if (TransactionFound) {
                        Transaction.clear();
                    }
                    Transaction.put(linecount++, line);
                    TransactionFound = Boolean.TRUE;
                } else if (TransactionFound) {
                    Transaction.put(linecount++, line);
                    if (Endindex > -1) {
                        for (List<FieldsMaster> DetailTemplate : template.getDetailTemplates()) {
                            ExtractFields(DetailTemplate, DetailTemplate.size(), template.getResetTag());
                        }
                        WriteData(writer, template.getDetailTemplates().get(0).size());
                        linecount = 0;
                        Transaction.clear();
                        TransactionFound = Boolean.FALSE;
                    }
                }
            }
            // Close the file after done searching
            bf.close();
        } catch (IOException e) {
            System.out.println("IO Error Occurred: " + e.toString());
        }
    }

    private static void ExtractFields(List<FieldsMaster> DetailTemplate, int FieldsCount, List<String> ResetTag) {
        for (int Key = 0; Key < Transaction.size(); Key++) {
            String Value;
            if (Transaction.get(Key) != null) {
                Value = Transaction.get(Key).toString();
            } else {
                Value = " ";
            }
            for (String Tag : ResetTag) {
                if (Value.toLowerCase().contains(Tag.toLowerCase()) && Tag.length() > 0) {
                    TransactionData = new HashMap();
                }
            }
            for (FieldsMaster SubStrings : DetailTemplate) {
                for (FieldDetails SubDetails : SubStrings.getDetails()) {
                    if (Value.toLowerCase().contains(SubDetails.getSubStrings().toLowerCase())) {

                        String DataNeeded = (String) Transaction.get(Key + SubDetails.getLine());
                        if (DataNeeded != null) {
                            try {
                                String FieldData = DataNeeded.substring(SubDetails.getBeginIndex(), SubDetails.getEndIndex()).trim();
                                if (ValidateData.Validate(SubDetails.getDataType(), FieldData, SubDetails.getFormat(), SubDetails.getSpecialCharacter())) {
                                    TransactionData.put(SubDetails.getIndex(), FieldData);
                                }
                            } catch (IndexOutOfBoundsException e) {
                                System.out.println(e.getMessage());
                            }
                        }
                        break;
                    }
                }

            }

        }

    }

    public static void WriteData(Writer writer, int size) throws IOException {
        //loop a Map
        int index = 1;
        String Data = "";
        Iterator<Entry<Integer, String>> iterator = TransactionData.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Integer, String> entry = (Map.Entry<Integer, String>) iterator.next();
            while (index != entry.getKey()) {
                Data = Data + Separator;
                index++;
            }
            Data = Data + entry.getValue() + Separator;
            index++;
        }

        if (Data.contains(";")) {
            while (index < size+1) {
                Data = Data + Separator;
                index++;
            }
            writer.write(Data);
            writer.write("\n");
            System.out.println(Data);
        }
    }
}
