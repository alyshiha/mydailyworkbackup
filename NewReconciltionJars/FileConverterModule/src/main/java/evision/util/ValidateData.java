/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

/**
 *
 * @author Aly.Shiha
 */
public class ValidateData {

    public static Boolean Validate(String DataType, String Data, String Format, String SpecialCharacter) {
        Boolean Result = Boolean.FALSE;
        switch (DataType) {
            case "Date":
                Result = isDate(Data, Format);
                break;
            case "Decimals":
                Result = isDecimals(Data);
                break;
            case "Number":
                Result = isNumber(Data);
                break;
            case "Alpha":
                Result = isAlpha(Data);
                break;
            case "AlphaNumeric":
                Result = isAlphaNumeric(Data);
                break;
            case "NumericSpecialCharacter":
                Result = isNumericSpecialCharacter(Data, SpecialCharacter);
                break;
            case "AlphaSpecialCharacter":
                Result = isAlphaSpecialCharacter(Data, SpecialCharacter);
                break;
            case "Text":
                Result = Boolean.TRUE;
                break;
        }
        return Result;
    }

    private static Boolean isAlphaSpecialCharacter(String Data, String SpecialCharacter) {
        return Data.matches("[A-Za-z" + SpecialCharacter + "]*");
    }

    private static Boolean isNumericSpecialCharacter(String Data, String SpecialCharacter) {
        return Data.matches("[0-9" + SpecialCharacter + "]*");
    }

    private static Boolean isAlphaNumeric(String Data) {
        return Data.matches("[a-zA-Z0-9]*");
    }

    private static Boolean isAlpha(String Data) {
        return Data.matches("[a-zA-Z]*");
    }

    private static Boolean isNumber(String Data) {
        try {
            double d = Double.parseDouble(Data);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private static Boolean isDecimals(String Data) {
        return Data.matches("\\d*\\.?\\d+");
    }

    private static Boolean isDate(String Data, String Format) {
        SimpleDateFormat df = new SimpleDateFormat(Format);
        try {
            df.parse(Data);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }
}
