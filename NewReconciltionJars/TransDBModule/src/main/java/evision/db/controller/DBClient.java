/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.db.controller;

import evision.db.datamodel.DBConnection;
import evision.db.service.DBEngine;
import evision.mq.service.RabbitMqConfig;
import evision.util.XStreamTranslator;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.utils.SerializationUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author Aly
 */
public class DBClient {

    private static RabbitTemplate rabbitTemplate;
    private static RabbitAdmin rabbitadmin;
    private static DBConnection DBConnectionRecords;
    private static HashMap<String, List<Message>> Queues = new HashMap<String, List<Message>>();
    private static HashMap<String, String> Querys = new HashMap<String, String>();

    public DBClient() {
        try {
            ApplicationContext ctx = new AnnotationConfigApplicationContext(RabbitMqConfig.class);
            rabbitTemplate = ctx.getBean(RabbitTemplate.class);
            rabbitadmin = new RabbitAdmin(rabbitTemplate.getConnectionFactory());
            DBConnectionRecords = ((List<DBConnection>) XStreamTranslator.getInstance().toObject(new File(".\\ConfigFiles\\DBConnection.xml"))).get(0);
        } catch (IOException ex) {
            Logger.getLogger(DBClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static Map<?, ?> deserialize(Message MSG) {
        return (Map<?, ?>) SerializationUtils.deserialize(MSG.getBody());
    }

    private static String GenerateQuery(Message Record, String QueueName, String FileType) {
        String Columns = "";
        String Values = "";
        for (Map.Entry<? extends Object, ? extends Object> entrySet : deserialize(Record).entrySet()) {
            Object value = entrySet.getValue();
            if (value == null) {
                value = "java.lang.string";
            }
            switch (value.getClass().getTypeName().toLowerCase()) {
                case "java.lang.string":
                    Columns = Columns + entrySet.getKey() + ",";
                    Values = Values + "?,";
                    break;
                case "java.math.bigdecimal":
                    Columns = Columns + entrySet.getKey() + ",";
                    Values = Values + "?,";
                    break;
                case "java.lang.integer":
                    Columns = Columns + entrySet.getKey() + ",";
                    Values = Values + "?,";
                    break;
                case "java.util.date":
                    Columns = Columns + entrySet.getKey() + ",";
                    Values = Values + "to_date(?,'dd.mm.yyyy hh24:mi:ss'),";
                    break;
                default:
                    throw new IllegalArgumentException("Invalid DataType");
            }
        }
        for (Map.Entry<String, Object> entryRecord : Record.getMessageProperties().getHeaders().entrySet()) {
            switch (entryRecord.getValue().getClass().getName().toLowerCase()) {
                case "java.lang.string":
                    Columns = Columns + entryRecord.getKey() + ",";
                    Values = Values + "?,";
                    break;
                case "java.math.bigdecimal":
                    Columns = Columns + entryRecord.getKey() + ",";
                    Values = Values + "?,";
                    break;
                case "java.lang.integer":
                    Columns = Columns + entryRecord.getKey() + ",";
                    Values = Values + "?,";
                    break;
                case "java.util.date":
                    Columns = Columns + entryRecord.getKey() + ",";
                    Values = Values + "to_date(?,'dd.mm.yyyy hh24:mi:ss'),";
                    break;
                default:
                    throw new IllegalArgumentException("Invalid DataType");
            }
        }
        return "insert into " + FileType + QueueName.replaceAll("Evision.Transaction.", "_") + " (" + Columns.substring(0, Columns.length() - 1) + ") values (" + Values.substring(0, Values.length() - 1) + ")";
    }

    private static void HandleQueues(Message Record, String QueueName, String FileType) {
        if (!Queues.containsKey(QueueName + FileType)) {
            Queues.put(QueueName + FileType, new ArrayList<Message>());
            Querys.put(QueueName + FileType, GenerateQuery(Record, QueueName, FileType));
        }
    }

    private static void AddTrans(Message MSG, String QueueName) {
        Queues.get(QueueName).add(MSG);
    }

    private static Boolean IsEmptyQueue(String QueueName, String FileType) {
        if (rabbitadmin.getQueueProperties(QueueName).get("QUEUE_MESSAGE_COUNT").toString().equals("0")) {
            return Boolean.TRUE;
        } else {
            if (Queues.get(QueueName + FileType).size() > 10000) {
                return Boolean.TRUE;
            } else {
                return Boolean.FALSE;
            }
        }
    }

    public static void run(Message MSG, String QueueName, String FileType) {
        HandleQueues(MSG, QueueName, FileType);
        AddTrans(MSG, QueueName + FileType);
        if (IsEmptyQueue(QueueName, FileType)) {
            DBEngine Engine = new DBEngine(DBConnectionRecords);
            Engine.batchUpdate(Queues.get(QueueName + FileType), Querys.get(QueueName + FileType));
            Queues.get(QueueName + FileType).clear();
        }
    }
}
