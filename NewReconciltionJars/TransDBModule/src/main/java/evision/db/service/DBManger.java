/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evision.db.service;

import evision.db.controller.DBClient;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

public class DBManger implements MessageListener {
    @Override
    public void onMessage(Message message) {
        DBClient.run(message,message.getMessageProperties().getConsumerQueue(),message.getMessageProperties().getHeaders().get("filetype").toString());
    }
}
