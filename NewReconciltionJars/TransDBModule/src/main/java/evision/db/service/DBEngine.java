package evision.db.service;

import evision.db.datamodel.DBConnection;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import oracle.jdbc.pool.OracleDataSource;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.utils.SerializationUtils;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

public class DBEngine {

    private JdbcTemplate jdbcTemplate;
    private int StatmentIndex;

    public DBEngine(DBConnection DBConnectionRecords) {
        try {
            jdbcTemplate = new JdbcTemplate(dataSource(DBConnectionRecords));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private OracleDataSource dataSource(DBConnection DBConnection) throws SQLException {
        OracleDataSource dataSource = new OracleDataSource();
        dataSource.setUser(DBConnection.getUser());
        dataSource.setPassword(DBConnection.getPassword());
        dataSource.setURL("jdbc:oracle:thin:@" + DBConnection.getIp() + ":" + DBConnection.getPort() + "/" + DBConnection.getSchema());
        dataSource.setDriverType("oracle.jdbc.driver.OracleDriver");
        dataSource.setImplicitCachingEnabled(true);
        dataSource.setFastConnectionFailoverEnabled(true);
        return dataSource;
    }

    public String changeDateAndTimeFormat(java.util.Date date) {
        if (date != null) {
            SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            String newDate = df.format(date);
            return newDate;
        } else {
            return "";
        }
    }

    private Map<?, ?> deserialize(Message MSG) {
        return (Map<?, ?>) SerializationUtils.deserialize(MSG.getBody());
    }

    public int[] batchUpdate(List<Message> Records, String Query) {
        return jdbcTemplate.batchUpdate(Query,
                new BatchPreparedStatementSetter() {
                    @Override
                    public void setValues(PreparedStatement ps, int index) throws SQLException {
                        StatmentIndex = 0;
                        for (Map.Entry<? extends Object, ? extends Object> entrySet : deserialize(Records.get(index)).entrySet()) {
                            StatmentIndex++;
                            Object value = entrySet.getValue();
                            if (value == null) {
                                value = "java.lang.string";
                            }
                            switch (value.getClass().getTypeName().toLowerCase()) {
                                case "java.lang.string":
                                    ps.setString(StatmentIndex, (String) value);
                                    break;
                                case "java.math.bigdecimal":
                                    ps.setBigDecimal(StatmentIndex, (BigDecimal) value);
                                    break;
                                case "java.lang.integer":
                                    ps.setInt(StatmentIndex, (Integer) value);
                                    break;
                                case "java.util.date":
                                    ps.setString(StatmentIndex, changeDateAndTimeFormat((Date) value));
                                    break;
                                default:
                                    throw new IllegalArgumentException("Invalid DataType");
                            }
                        }
                        for (Map.Entry<String, Object> entryRecord : Records.get(index).getMessageProperties().getHeaders().entrySet()) {
                            StatmentIndex++;
                            switch (entryRecord.getValue().getClass().getName().toLowerCase()) {
                                case "java.lang.string":
                                    ps.setString(StatmentIndex, (String) entryRecord.getValue());
                                    break;
                                case "java.math.bigdecimal":
                                    ps.setBigDecimal(StatmentIndex, (BigDecimal) entryRecord.getValue());
                                    break;
                                case "java.lang.integer":
                                    ps.setInt(StatmentIndex, (Integer) entryRecord.getValue());
                                    break;
                                case "java.util.date":
                                    ps.setString(StatmentIndex, changeDateAndTimeFormat((Date) entryRecord.getValue()));
                                    break;
                                default:
                                    throw new IllegalArgumentException("Invalid DataType");
                            }
                        }
                    }

                    @Override
                    public int getBatchSize() {
                        return Records.size();
                    }
                });
    }
}
