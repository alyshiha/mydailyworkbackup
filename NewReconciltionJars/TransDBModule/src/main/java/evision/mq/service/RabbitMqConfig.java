package evision.mq.service;

import evision.db.service.DBManger;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.memorynotfound.rabbitmq")
public class RabbitMqConfig {

    private static final String SIMPLE_MESSAGE_QUEUE_INVALID = "Evision.Transaction.INValid";
    private static final String SIMPLE_MESSAGE_QUEUE_DISPUTE = "Evision.Transaction.Dispute";
    private static final String SIMPLE_MESSAGE_QUEUE_PENDING = "Evision.Transaction.Valid";
    private static final String SIMPLE_MESSAGE_QUEUE_MATCHED = "Evision.Transaction.ReadyToMatch";

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("localhost");
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        return connectionFactory;

    }

    @Bean
    public Queue simpleQueueMatched() {
        return new Queue(SIMPLE_MESSAGE_QUEUE_MATCHED);
    }

    @Bean
    public Queue simpleQueuePending() {
        return new Queue(SIMPLE_MESSAGE_QUEUE_PENDING);
    }

    @Bean
    public Queue simpleQueueInValid() {
        return new Queue(SIMPLE_MESSAGE_QUEUE_INVALID);
    }

    @Bean
    public Queue simpleQueueDispute() {
        return new Queue(SIMPLE_MESSAGE_QUEUE_DISPUTE);
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        return template;
    }

    @Bean
    public SimpleMessageListenerContainer listenerContainer() {
        SimpleMessageListenerContainer listenerContainer = new SimpleMessageListenerContainer();
        listenerContainer.setConnectionFactory(connectionFactory());
        listenerContainer.setQueues(simpleQueueDispute(), simpleQueueInValid(), simpleQueuePending(), simpleQueueMatched());
        listenerContainer.setMessageConverter(jsonMessageConverter());
        listenerContainer.setMessageListener(new DBManger());
        listenerContainer.setAcknowledgeMode(AcknowledgeMode.AUTO);
        listenerContainer.setStartConsumerMinInterval(1);
        return listenerContainer;
    }

}
