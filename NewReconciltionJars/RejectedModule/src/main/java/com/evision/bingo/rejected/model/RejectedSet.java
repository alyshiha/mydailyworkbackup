/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.rejected.model;

import com.evision.bingo.core.model.BaseRejectedSet;
import java.util.List;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public class RejectedSet extends BaseRejectedSet {

    private String state;
    private List<RejectedRule> rejectedRulesList;
    private String fileType;
    private String fileTemplate;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<RejectedRule> getRejectedRulesList() {
        return rejectedRulesList;
    }

    public void setRejectedRulesList(List<RejectedRule> rejectedRulesList) {
        this.rejectedRulesList = rejectedRulesList;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileTemplate() {
        return fileTemplate;
    }

    public void setFileTemplate(String fileTemplate) {
        this.fileTemplate = fileTemplate;
    }

}
