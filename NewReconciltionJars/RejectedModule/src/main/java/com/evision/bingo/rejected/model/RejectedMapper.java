/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.rejected.model;

import com.evision.bingo.core.model.BaseRejectedSet;
import com.evision.bingo.core.model.RejectedConfigurable;
import java.util.List;

/**
 *
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 * Validation Mapping Config
 */
public class RejectedMapper implements RejectedConfigurable {

    private String fileType;
    private String fileTemplate;
    private List<BaseRejectedSet> rejectedSetList;

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileTemplate() {
        return fileTemplate;
    }

    public void setFileTemplate(String fileTemplate) {
        this.fileTemplate = fileTemplate;
    }

    @Override
    public void setRejectedSetsList(List<BaseRejectedSet> rejectedSetList) {
        this.rejectedSetList = rejectedSetList;
    }

    @Override
    public List<BaseRejectedSet> getRejectedSetsList() {
        return rejectedSetList;
    }
}
