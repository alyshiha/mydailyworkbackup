/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.rejected.service;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.BaseRejectedSet;
import com.evision.bingo.core.utils.Defines;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.LogMessages;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.rejected.model.RejectedRule;
import com.evision.bingo.rejected.model.RejectedSet;
import com.evision.bingo.rejected.utils.RejectedDefines;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.amqp.core.MessageProperties;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public class RejectedService extends BaseRejectedService {

    private String reason = "";
    private ObjectMapper oMapper = new ObjectMapper();

    public RejectedService(QueueHandler queueHandler, LogHandler logHandler, List<BaseController> eventsHandlerList) {
        super(queueHandler, logHandler, eventsHandlerList);
    }

    @Override
    public boolean validate(List<BaseRejectedSet> rejectedSetsList, Map messageMap, MessageProperties messageProperties, String templateName) {
        for (BaseRejectedSet rejectedSet : rejectedSetsList) {
            String reason = applyValidationSet((RejectedSet) rejectedSet, messageMap);
            messageProperties.setHeader(RejectedDefines.REASON_HEADER, reason);
            logHandler.logInfo(String.format(LogMessages.getMessage(LogMessages.REJECTED_VALIDATE_TRANSACTION_AGAINST_REJECTED_SET), ((RejectedSet) rejectedSet).getState(), templateName));
            if (reason.isEmpty()) {
                queueHandler.sendMessage(((RejectedSet) rejectedSet).getState(), messageMap, messageProperties);
            }
        }
        return true;
    }

    private String checkString(RejectedRule validationRule, String value) {
        switch (validationRule.getOperator()) {
            //string not in a;b;c
            case RejectedDefines.OPERATOR_NOT_IN:
                if (Arrays.asList(validationRule.getValue().split(RejectedDefines.VALUE_SEPARATOR)).contains(value)) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_IN + Defines.SYSTEM_SPACE + validationRule.getValue() + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value;
                }
                break;
            //string in a;b;c
            case RejectedDefines.OPERATOR_IN:
                if (!Arrays.asList(validationRule.getValue().split(RejectedDefines.VALUE_SEPARATOR)).contains(value)) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_NOT_IN + Defines.SYSTEM_SPACE + validationRule.getValue() + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value;
                }
                break;
            //string equal aly
            case RejectedDefines.OPERATOR_EQUAL:
                if (!value.equals(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            //string not equal aly
            case RejectedDefines.OPERATOR_NOT_EQUAL:
                if (value.equals(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_LIKE:
                if (!value.contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_NOT_LIKE:
                if (value.contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_IS_NOT_NULL:
                if (value == null || value.isEmpty()) {
                    return validationRule.getFieldName() + RejectedDefines.OPERATOR_IS_NULL;
                }
                break;
            case RejectedDefines.OPERATOR_IS_NULL:
                if (value != null || !value.isEmpty()) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            default:
                return RejectedDefines.INVALID_OPERATOR;
        }
        return "";
    }

    private String checkDecimal(RejectedRule validationRule, BigDecimal value) {
        switch (validationRule.getOperator()) {
            case RejectedDefines.OPERATOR_EQUAL:
                if (value.compareTo(new BigDecimal(validationRule.getValue())) != 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_NOT_EQUAL:
                if (value.compareTo(new BigDecimal(validationRule.getValue())) == 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_LIKE:
                if (!value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_NOT_LIKE:
                if (value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_IS_NULL:
                if (value != null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            case RejectedDefines.OPERATOR_IS_NOT_NULL:
                if (value == null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_NULL;
                }
                break;
            case RejectedDefines.OPERATOR_GREATER_THAN:
                if (value.compareTo(new BigDecimal(validationRule.getValue())) == -1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_SMALLER_THAN:
                if (value.compareTo(new BigDecimal(validationRule.getValue())) == 1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            default:
                return RejectedDefines.INVALID_OPERATOR;
        }

        return "";
    }

    private String checkInteger(RejectedRule validationRule, Integer value) {
        switch (validationRule.getOperator()) {
            case RejectedDefines.OPERATOR_EQUAL:
                if (value.compareTo(new Integer(validationRule.getValue())) != 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_NOT_EQUAL:
                if (value.compareTo(new Integer(validationRule.getValue())) == 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_LIKE:
                if (!value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_NOT_LIKE:
                if (value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_IS_NULL:
                if (value != null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            case RejectedDefines.OPERATOR_IS_NOT_NULL:
                if (value == null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_NULL;
                }
                break;
            case RejectedDefines.OPERATOR_GREATER_THAN:
                if (value.compareTo(new Integer(validationRule.getValue())) == -1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_SMALLER_THAN:
                if (value.compareTo(new Integer(validationRule.getValue())) == 1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            default:
                return RejectedDefines.INVALID_OPERATOR;
        }

        return "";
    }

    private String checkFloat(RejectedRule validationRule, Float value) {
        switch (validationRule.getOperator()) {
            case RejectedDefines.OPERATOR_EQUAL:
                if (value.compareTo(new Float(validationRule.getValue())) != 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_NOT_EQUAL:
                if (value.compareTo(new Float(validationRule.getValue())) == 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_LIKE:
                if (!value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_NOT_LIKE:
                if (value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_IS_NULL:
                if (value != null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            case RejectedDefines.OPERATOR_IS_NOT_NULL:
                if (value == null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_NULL;
                }
                break;
            case RejectedDefines.OPERATOR_GREATER_THAN:
                if (value.compareTo(new Float(validationRule.getValue())) == -1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_SMALLER_THAN:
                if (value.compareTo(new Float(validationRule.getValue())) == 1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            default:
                return RejectedDefines.INVALID_OPERATOR;
        }

        return "";
    }

    private String checkDouble(RejectedRule validationRule, Double value) {
        switch (validationRule.getOperator().toUpperCase()) {
            case RejectedDefines.OPERATOR_EQUAL:
                if (value.compareTo(new Double(validationRule.getValue())) != 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_NOT_EQUAL:
                if (value.compareTo(new Double(validationRule.getValue())) == 0) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_LIKE:
                if (!value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_NOT_LIKE:
                if (value.toString().contains(validationRule.getValue())) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_IS_NULL:
                if (value != null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_IS_NOT_NULL;
                }
                break;
            case RejectedDefines.OPERATOR_IS_NOT_NULL:
                if (value == null) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_NULL;
                }
                break;
            case RejectedDefines.OPERATOR_GREATER_THAN:
                if (value.compareTo(new Double(validationRule.getValue())) == -1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            case RejectedDefines.OPERATOR_SMALLER_THAN:
                if (value.compareTo(new Double(validationRule.getValue())) == 1) {
                    return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + value + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                }
                break;
            default:
                return RejectedDefines.INVALID_OPERATOR;
        }

        return "";
    }

    private String checkDate(RejectedRule validationRule, String strValue) {
        DateFormat dateFormat = new SimpleDateFormat(RejectedDefines.DATE_FORMAT);
        Date dateValue = null;
        try {
            dateValue = dateFormat.parse(strValue);
        } catch (ParseException ex) {
            return RejectedDefines.INVALID_FIELD_VALUE;
        }

        try {

            switch (validationRule.getOperator()) {
                case RejectedDefines.OPERATOR_EQUAL:
                    if (dateValue.compareTo(dateFormat.parse(validationRule.getValue())) != 0) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_NOT_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case RejectedDefines.OPERATOR_NOT_EQUAL:
                    if (dateValue.compareTo(dateFormat.parse(validationRule.getValue())) == 0) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_EQUAL + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case RejectedDefines.OPERATOR_LIKE:
                    if (!dateValue.toString().contains(validationRule.getValue())) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_NOT_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case RejectedDefines.OPERATOR_NOT_LIKE:
                    if (dateValue.toString().contains(validationRule.getValue())) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_CONTAINS + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case RejectedDefines.OPERATOR_IS_NULL:
                    if (dateValue != null) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_IS_NOT_NULL;
                    }
                    break;
                case RejectedDefines.OPERATOR_IS_NOT_NULL:
                    if (dateValue == null) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_NULL;
                    }
                    break;
                case RejectedDefines.OPERATOR_GREATER_THAN:
                    if (dateValue.compareTo(dateFormat.parse(validationRule.getValue())) == -1) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_SMALLER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                case RejectedDefines.OPERATOR_SMALLER_THAN:
                    if (dateValue.compareTo(dateFormat.parse(validationRule.getValue())) == 1) {
                        return validationRule.getFieldName() + Defines.SYSTEM_SPACE + RejectedDefines.VALUE_EQUALS + Defines.SYSTEM_SPACE + strValue + Defines.SYSTEM_SPACE + RejectedDefines.WHICH_IS_GREATER_THAN + Defines.SYSTEM_SPACE + validationRule.getValue();
                    }
                    break;
                default:
                    return RejectedDefines.INVALID_OPERATOR;
            }
        } catch (ParseException ex) {
            return RejectedDefines.INVALID_RULE_VALUE;
        }

        return "";
    }

    private String applyValidationSet(RejectedSet validationSet, Map<String, Object> message) {
        String reason = "";

        for (RejectedRule validationRule : validationSet.getRejectedRulesList()) {
            String fieldName = validationRule.getFieldName();
            Object fieldValue = message.get(fieldName);
            String fieldDataType = fieldValue.getClass().getTypeName();

            switch (fieldDataType.toLowerCase()) {
                case RejectedDefines.STRING_FIELD_TYPE_CLASS:
                    reason += checkString(validationRule, fieldValue.toString().trim()) + Defines.SYSTEM_SPACE + RejectedDefines.OPERATOR_OR + Defines.SYSTEM_SPACE;
                    break;
                case RejectedDefines.DECIMAL_FIELD_TYPE_CLASS:
                    reason += checkDecimal(validationRule, new BigDecimal(fieldValue.toString().trim()));
                    break;
                case RejectedDefines.INTEGER_FIELD_TYPE_CLASS:
                    reason += checkInteger(validationRule, new Integer(fieldValue.toString().trim()));
                    break;
                case RejectedDefines.FLOAT_FIELD_TYPE_CLASS:
                    reason += checkFloat(validationRule, new Float(fieldValue.toString().trim()));
                    break;
                case RejectedDefines.DOUBLE_FIELD_TYPE_CLASS:
                    reason += checkDouble(validationRule, new Double(fieldValue.toString().trim()));
                    break;
                case RejectedDefines.DATE_FIELD_TYPE_CLASS:
                    reason += checkDate(validationRule, fieldValue.toString().trim());
                    break;
                default:
                    break;
            }
        }

        return reason;
    }

    @Override
    public boolean connect() {
        return true;
    }

    @Override
    public void disconnect() {
    }
}
