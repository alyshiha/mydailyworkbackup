/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.rejected.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.RejectedConfig;
import com.evision.bingo.core.model.RejectedConfigurable;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import java.util.ArrayList;
import java.util.List;
import com.evision.bingo.rejected.service.iRejectedPreProcessor;
import com.evision.bingo.rejected.service.iRejectedPostProcessor;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public abstract class BaseRejectedController extends BaseController {

    //collection of registered pre-processors
    private List<iRejectedPreProcessor> registeredPreProcessors;

    //collection of registered post-processors
    private List<iRejectedPostProcessor> registeredPostProcessors;

    protected List<RejectedConfigurable> subConfigList = new ArrayList();

    BaseRejectedController(LogHandler logHandler, QueueHandler queueHandler, RejectedConfig rejectedConfig, List<RejectedConfigurable> subConfigList, List<BaseController> eventsHandlerList) {
        super(logHandler, queueHandler, rejectedConfig, eventsHandlerList);

        this.subConfigList = subConfigList;

        init();
    }

    // init processors collections
    @Override
    public final void init() {
        registeredPreProcessors = new ArrayList<>();
        registeredPostProcessors = new ArrayList<>();
    }

    //force clear memory     
    @Override
    public final void destroy() {
        registeredPreProcessors.clear();
        registeredPostProcessors.clear();

        registeredPreProcessors = null;
        registeredPostProcessors = null;
    }

    //register a pre-processor
    final void registerPreProcessor(iRejectedPreProcessor preProcessor) {
        registeredPreProcessors.add(preProcessor);
    }

    //unregister a pre-processor
    final void unregisterPreProcessor(iRejectedPreProcessor preProcessor) {
        registeredPreProcessors.remove(preProcessor);
    }

    //register a post-processor
    final void registerPostProcessor(iRejectedPostProcessor postProcessor) {
        registeredPostProcessors.add(postProcessor);
    }

    //unregister a post-processor
    final void unregisterPostProcessor(iRejectedPostProcessor postProcessor) {
        registeredPostProcessors.remove(postProcessor);
    }

    //main load function that forces the work chain
    @Override
    public synchronized final boolean handleRequest(Object obj) {
        this.setObj(obj);
        if (preCollect()) {
            if (doCollect()) {
                return postCollect();
            }
        }
        return false;
    }

    //call of all registered pre-processors
    synchronized final boolean preCollect() {
        if (!registeredPreProcessors.isEmpty()) {
            for (iRejectedPreProcessor preProcessor : registeredPreProcessors) {
                if (!preProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //call of all registered post-processors
    synchronized final boolean postCollect() {
        if (!registeredPostProcessors.isEmpty()) {
            for (iRejectedPostProcessor postProcessor : registeredPostProcessors) {
                if (!postProcessor.process(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    //method of collect to be overidden
    abstract boolean doCollect();
}
