/*
 * This code is copyright (c) 2017 eVision
 */
package com.evision.bingo.rejected.controller;

import com.evision.bingo.core.controller.BaseController;
import com.evision.bingo.core.model.BaseRejectedSet;
import com.evision.bingo.core.model.RejectedConfig;
import com.evision.bingo.core.model.RejectedConfigurable;
import com.evision.bingo.core.utils.LogHandler;
import com.evision.bingo.core.utils.QueueHandler;
import com.evision.bingo.rejected.service.DateHandlerPreProcessor;
import com.evision.bingo.rejected.service.RejectedService;
import com.evision.bingo.rejected.utils.RejectedDefines;
import java.util.List;
import java.util.Map;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * @since 9/7/2017
 * @author Khaled Khalil
 *
 */
public class RejectedController extends BaseRejectedController {

    private RejectedService rejectedService = null;

    public RejectedController(RejectedConfig rejectedConfig, Map<String, RabbitTemplate> rabbitTemplatesMap, LogHandler systemLogger, List<RejectedConfigurable> subConfigsList, List<BaseController> eventsHandlerList) {
        super(new LogHandler(rejectedConfig.getLoggerPathList(), systemLogger), new QueueHandler(rejectedConfig.getQueuesList(), rabbitTemplatesMap), rejectedConfig, subConfigsList, eventsHandlerList);

        rejectedService = new RejectedService(queueHandler, logHandler, eventsHandlerList);
        
        registerPreProcessor(new DateHandlerPreProcessor());
    }

    @Override
    synchronized boolean doCollect() {
        if (getObj() != null && getExtraObj() != null) {
            Map msgMap = (Map) getObj();
            MessageProperties msgProperties = (MessageProperties) getExtraObj();

            for (RejectedConfigurable configurable : subConfigList) {
                if (configurable.getFileType().equals(msgProperties.getHeaders().get(RejectedDefines.FILE_TYPE_HEADER))) {
                    return rejectedService.validate((List<BaseRejectedSet>) configurable.getRejectedSetsList(), msgMap, msgProperties, configurable.getFileType());
                }
            }
        }
        return false;
    }

    @Override
    public void stop() {
    }
}
